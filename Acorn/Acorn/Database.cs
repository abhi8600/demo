using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.Odbc;
using System.IO;
using System.Collections.Generic;
using System.Web.SessionState;
using System.Security.AccessControl;
using System.Text;
using System.Threading;
using Acorn.Utils;
using Performance_Optimizer_Administration;
using System.Diagnostics;
using Acorn.DBConnection;
using System.Data.Common;
using Acorn.SAMLSSO;

namespace Acorn
{
    public class Database
    {
        public static int MAX_VARCHAR_SIZE = 2048;
        public static string VERSION_TABLE_NAME = "VERSION";
        public static string USER_TABLE_NAME = "USERS";
        public static string SPACE_TABLE_NAME = "SPACES";
        public static string USER_SPACE_INXN = "USER_SPACE_INXN";
        public static string INVITES_TABLE_NAME = "INVITES";
        public static string TEMPLATES_TABLE_NAME = "TEMPLATES";
        public static string CATEGORIES_TABLE_NAME = "CATEGORIES";
        public static string ATTACHMENTS_TABLE_NAME = "ATTACHMENTS";
        public static string RATINGS_TABLE_NAME = "RATINGS";
        public static string EVENT_LOG_TABLE_NAME = "EVENTLOG";
        public static string PRODUCTS_TABLE = "PRODUCTS";
        public static string USER_PRODUCT_TABLE = "USER_PRODUCT_INXN";
        public static string PUBLISH_TABLE = "PUBLISH";
        public static string PARTNER_CODE_TABLE = "PARTNER_CODES";
        public static string SFORCE_SCHEDULE_TABLE = "SFORCE_SCHEDULE";
        public static string SCHEDULED_LOAD_TABLE = "LOAD_SCHEDULE";
        public static string IMAGE_TABLE = "IMAGES";
        public static string PROXY_TABLE = "PROXY_REGISTRATIONS";
        public static string SCHEDULED_REPORTS_TABLE = "SCHEDULED_REPORTS";
        public static string DELIVERY_LOG = "DELIVERY_LOG";
        public static string ALLOWED_USER_DOMAINS_TABLE = "ALLOWED_USER_DOMAINS";
        public static string CREATED_USERS_TABLE = "CREATED_USERS";
        public static string PREFERENCES_TABLE = "PREFERENCES";
        public static string PROXY_USERS = "PROXY_USERS";
        public static string ALLOWED_IPS = "ALLOWED_IPS";
        public static string ACL_INFO_TABLE = "ACL_INFO";
        public static string SPACE_GROUP_TABLE = "SPACE_GROUP_INXN";
        public static string GROUP_USER_TABLE = "GROUP_USER_INXN";
        public static string GROUP_ACL_TABLE = "GROUP_ACL_INXN";
        public static string ACCOUNTS_TABLE = "ACCOUNTS";
        public static string CURRENT_LOGINS_TABLE = "CURRENT_LOGINS";
        public static string ACCOUNT_OVERRIDE_PARAMETERS_TABLE = "ACCOUNT_OVERRIDE_PARAMETERS";
        public static string SSO_TOKENS_TABLE = "SSO_TOKENS";
        public static string UPLOAD_TOKENS_TABLE = "UPLOAD_TOKENS";
        public static string SERVER_STATUS_TABLE = "SERVER_STATUS";
        public static string RELEASE_INFO_TABLE = "RELEASE_INFO";
        public static string PROCESS_VERSIONS_TABLE = "PROCESS_VERSIONS_INFO";
        public static string PASSWORD_RESET_REQUEST_TABLE = "PASSWORD_RESET_REQUEST";
        public static string SYSTEM_MESSAGES_TABLE = "SYSTEM_MESSAGES";
        public static string RUNNING_PROCESSES_TABLE = "RUNNING_PROCESSES";
        public static string SPACES_CONFIG_TABLE = "SPACES_CONFIG";
        public static string DELIVERY_OPS_TABLE = "DELIVERY_OPS";
        public static string SPACE_OPS_TABLE = "SPACE_OPERATIONS";
        public static string LAST_LOAD_HISTORY = "LAST_LOAD_HISTORY";
        public static string OPENID_USER_TABLE = "OPENID_USER";
        public static string SERVER_INFO_TABLE = "SERVER_INFO";
        public static string APP_LOGS_INFO_TABLE = "APP_LOGS_INFO";
        public static string PASSWORD_POLICY_TABLE = "PASSWORD_POLICY";
        public static string PASSWORD_HISTORY_TABLE = "PASSWORD_HISTORY";
        public static string PACKAGES_INFO = "PACKAGES_INFO";
        public static string PACKAGE_GROUP_INXN = "PACKAGE_GROUP_INXN";
        public static string PACKAGE_USAGE_INXN = "PACKAGE_USAGE_INXN";
        public static string GLOBAL_PROPERTIES_TABLE = "GLOBAL_PROPERTIES";
        public static string CONNECTOR_INSTANCES_TABLE = "ConnectorInstances";
        public static string CONNECTOR_MASTER_TABLE = "ConnectorsMaster";
        public static string CONNECTOR_ACTIVITY_TABLE = "ConnectorActivity";
        public static string CONNECTOR_PARAMETER_TABLE = "ConnectorParameters";
        public static string SAML_IDP_TABLE = "SAML_IDP";
        public static string SUB_DOMAIN_REGISTER_TABLE = "SUB_DOMAIN_INFO";
        public static string LOCALE_TABLE = "LOCALES";
        public static string AWS_REGIONS = "AWS_REGIONS";
        public static string SAML_ACCOUNT_MAPPING = "SAML_ACCOUNT_INXN";

        public static int MaxUsernameChar = 20;
        public static int MaxPasswordChar = 20;
        public static int MaxEmailChar = 20;
        public static int DefaultProductID;
        public enum ProductType { Edition, Storage, Invitations };
        private static string PRODUCT_FILENAME = "config/Products.config";
        private static string PREFERENCES_FILENAME = "config/Preferences.config";
        private static string ACL_FILENAME = "config/ACLs.config";
        private static string SPACE_OPS_FILENAME = "config/SpaceOps.config";
        private static string LOCALE_FILENAME = "config/Locale.config";

        public static int OLD_QUERY_LANGUAGE = 0;
        public static int NEW_QUERY_LANGUAGE = 1;

        public static int MAX_QUERY_ROWS = 10000;

        public static int MAX_QUERY_TIMEOUT = 300;           // 5 minutes
        private static int COPY_TABLE_TIMEOUT = 4 * 60 * 60; // 4 hours
        private static int DROP_TABLE_TIMEOUT = 60 * 60;     // 1 hour
        private static int CREATE_INDEX_TIMEOUT = 60 * 60;   // 1 hour
        private static int GET_TABLES_TIMEOUT = 300;         // 5 minutes

        public static string INFOBRIGHT = "Infobright";
        public static string MICROSOFT_ANALYSIS_SERVICES = "Microsoft Analysis Services (XMLA)";
        public static string PENTAHO_MONDRIAN = "Pentaho (Mondrian) Analysis Services (XMLA)";
        public static string HYPERIAN_ESSBASE = "Hyperion Essbase (XMLA)";
        public static string SAP_BW = "SAP BW (XMLA)";
        public static string ORACLE = "Oracle";
        public static string PARACCEL = "ParAccel";
        public static string MEMDB = "MemDB";
        public static string REDSHIFT = "Redshift";
        public static string HANA = "SAP Hana";
        public static string ORACLE_JDBC_DRIVER_NAME = "oracle.jdbc.OracleDriver";
        public static string REDSHIFT_JDBC_DRIVER_NAME = "org.postgresql.Driver.Redshift";
        public static readonly int REDSHIFT_BASE_SPACE_CONFIG = 100000;

        public static string DEFAULT_CONNECTION = "Default Connection";
        public static string MEM_AGG_CONNECTION = "In-memory Aggregate";

        public static string PADB_CONNECTIONSTRING_SUFFIX = "PADB_ConnectionString_Suffix";

        public static string mainSchema = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["MainSchema"];
        private static string datafileloc = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["OracleDatafileDirectory"];
        private static string CurrentPasswordHashVersion = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["CurrentPasswordHashVersion"];

        public static DateTime truncateDateTime(DateTime dt)
        {
            return dt.AddMilliseconds(-dt.Millisecond);
        }

        public static bool tableExistsUsingSelect(QueryConnection conn, string schema, string tableName)
        {
            bool exists = false;
            QueryCommand cmd = null;
            QueryReader reader = null;

            string query = null;
            if (conn.supportsLimit())
            {
                query = "SELECT * FROM " + getTableName(schema, tableName) + " LIMIT 0";
            }
            else if (conn.supportsRowNum())
            {
                query = "SELECT COUNT(1) FROM " + getTableName(schema, tableName) + " WHERE ROWNUM = 0";
            }
            else
            {
                query = "SELECT TOP 0 * FROM " + getTableName(schema, tableName);
            }

            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandTimeout = GET_TABLES_TIMEOUT;
                cmd.CommandText = query;
                reader = cmd.ExecuteReader();
                exists = true;
            }
            catch (Exception ex)
            {
                exists = false;
                bool showLogError = true;
                if (ex.Message != null && (ex.Message.Contains("Invalid object name") || ex.Message.Contains("doesn't exist") || ex.Message.Contains("does not exist") || ex.Message.Contains("invalid table name")
                    || ex.Message.Contains("Unknown table:") || ex.Message.Contains("ORA-00942: table or view does not exist")))
                {
                    showLogError = false;
                }
                if (showLogError)
                {
                    Global.systemLog.Warn("Error while looking for table exists " + tableName + " ", ex);
                    throw ex;
                }
            }
            finally
            {
                try
                {
                    if (reader != null)
                    {
                        reader.Close();
                    }
                    if (cmd != null)
                    {
                        cmd.Dispose();
                    }
                }
                catch (Exception)
                { }
            }
            return exists;
        }

        public static bool tableExists(QueryConnection conn, string schema, string tableName)
        {
            List<string> tlist = null;
            return tableExists(conn, schema, tableName, ref tlist);
        }

        public static bool tableExists(QueryConnection conn, string schema, string tableName, ref List<string> schemaTable)
        {
            if (schemaTable == null)
            {
                schemaTable = Database.getTables(conn, schema);
            }
            if (schemaTable != null)
            {
                foreach (string s in schemaTable)
                {
                    if (s.ToLower() == tableName.ToLower())
                        return true;
                }
            }
            return false;
        }

        public static void dropTable(QueryConnection conn, string schema, string tableName)
        {
            if (!tableExists(conn, schema, tableName))
                return;

            QueryCommand cmd = null;
            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandTimeout = DROP_TABLE_TIMEOUT;
                cmd.CommandText = "DROP TABLE " + (schema == null ? tableName : schema + "." + tableName);
                Global.systemLog.Info(cmd.CommandText);
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Global.systemLog.Error(ex);
            }
            finally
            {
                if (cmd != null)
                    cmd.Dispose();
            }
        }

        public static void createTable(QueryConnection conn, string schema, string tableName, List<string[]> columns, bool dropIfExists)
        {
            createTable(conn, schema, tableName, columns, dropIfExists, null);
        }

        public static void createTable(QueryConnection conn, string schema, string tableName, List<string[]> columns, bool dropIfExists, string constraint)
        {
            if (dropIfExists && tableExists(conn, schema, tableName))
            {
                dropTable(conn, schema, tableName);
            }
            QueryCommand cmd = null;
            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = "CREATE TABLE " + (schema == null ? tableName : schema + "." + tableName) + " (";
                for (int i = 0; i < columns.Count; i++)
                {
                    if (i > 0)
                    {
                        cmd.CommandText += ",";
                    }
                    if (conn.ConnectionString.Contains("SQL Server") && (columns[i][0] == "User" || columns[i][0] == "Schema" || columns[i][0] == "Default"))
                        cmd.CommandText += "[" + columns[i][0] + "] " + columns[i][1];
                    else
                        cmd.CommandText += columns[i][0] + " " + columns[i][1];
                }

                if (constraint != null && constraint.Trim().Length > 0)
                {
                    cmd.CommandText += ",";
                    cmd.CommandText += "CONSTRAINT " + constraint;
                }

                cmd.CommandText += ")";
                Global.systemLog.Info(cmd.CommandText);
                cmd.ExecuteNonQuery();
            }
            finally
            {
                if (cmd != null)
                    cmd.Dispose();
            }
        }

        public static void createTableOnInfobright(QueryConnection conn, string schema, string tableName, List<string[]> columns, bool dropIfExists)
        {
            if (dropIfExists && tableExists(conn, schema, tableName))
            {
                dropTable(conn, schema, tableName);
            }
            QueryCommand cmd = null;
            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = "CREATE TABLE " + (schema == null ? tableName : schema + "." + tableName) + " (";
                for (int i = 0; i < columns.Count; i++)
                {
                    if (i > 0)
                    {
                        cmd.CommandText += ",";
                    }
                    cmd.CommandText += columns[i][0] + " " + columns[i][1];
                }
                cmd.CommandText += ")";
                Global.systemLog.Info(cmd.CommandText);
                cmd.ExecuteNonQuery();
            }
            finally
            {
                if (cmd != null)
                    cmd.Dispose();
            }
        }

        public static void createIndex(QueryConnection conn, string schema, string tableName, string indexName, string[] columns, string[] includedColumns)
        {
            bool isOracle = conn.isDBTypeOracle();
            if (isOracle)
            {
                indexName = indexName + "_" + tableName;
                for (int i = 0; i < columns.Length; i++)
                {
                    indexName = indexName + "_" + columns[i];
                }
                indexName = schema + "." + "IDX_" + Math.Abs(indexName.GetHashCode());
            }
            string query = "CREATE" + (isOracle ? " " : " NONCLUSTERED ") + "INDEX " + indexName + " ON " + schema + "." + tableName + "(";
            for (int i = 0; i < columns.Length; i++)
                query += (i > 0 ? "," : "") + columns[i] + " ASC";
            query += ")";
            if (includedColumns != null && includedColumns.Length > 0 && !isOracle)
            {
                query += " INCLUDE (";
                for (int i = 0; i < includedColumns.Length; i++)
                    query += (i > 0 ? "," : "") + includedColumns[i];
                query += ")";
            }

            QueryCommand cmd = null;
            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = query;
                cmd.CommandTimeout = CREATE_INDEX_TIMEOUT;
                Global.systemLog.Info(cmd.CommandText);
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                // don't fail if you can't create the index, it may already exist
                Global.systemLog.Info("Index creation failed, may be okay if it already exists - ", ex);
            }
            finally
            {
                if (cmd != null)
                    cmd.Dispose();
            }
        }

        public static void createIndex(QueryConnection conn, string schema, string tableName, string indexName, string[] columns)
        {
            createIndex(conn, schema, tableName, indexName, columns, null);
        }

        public static void createIndex(QueryConnection conn, DatabaseIndex di)
        {
            createIndex(conn, di.SchemaName, di.TableName, di.Name, di.Columns, di.IncludedColumns);
        }

        public static void createLogTable(QueryConnection conn, string schema)
        {
            if (!tableExists(conn, schema, EVENT_LOG_TABLE_NAME))
            {
                List<string[]> columns = new List<string[]>();
                if (conn.supportsIdentity())
                    columns.Add(new string[] { "ID", "int IDENTITY (1,1) NOT NULL" });
                else
                    columns.Add(new string[] { "ID", "int NOT NULL" });
                columns.Add(new string[] { conn.getReservedName("Date"), conn.getDateTimeType() });
                columns.Add(new string[] { conn.getReservedName("Level"), conn.getVarcharType(15, false) });
                columns.Add(new string[] { conn.getReservedName("User"), conn.getVarcharType(128, false) });
                columns.Add(new string[] { "ItemID", conn.getVarcharType(128, false) });
                columns.Add(new string[] { "Message", conn.getVarcharType(2000, false) });
                columns.Add(new string[] { "Exception", conn.getVarcharType(2000, false) });
                createTable(conn, schema, EVENT_LOG_TABLE_NAME, columns, false);
            }
        }

        public static void createTxnCommandHistoryTable(QueryConnection conn, Space sp, bool isIB)
        {
            List<string[]> columns = new List<string[]>();
            columns.Add(new string[] { "TM", conn.getDateTimeType() });
            columns.Add(new string[] { "COMMAND_TYPE", conn.getVarcharType(30, false) });
            columns.Add(new string[] { "STEP", conn.getVarcharType(30, false) });
            columns.Add(new string[] { "SUBSTEP", conn.getVarcharType(1024, false) });
            columns.Add(new string[] { "PROCESSINGGROUP", conn.getVarcharType(1024, false) });
            columns.Add(new string[] { "ITERATION", conn.getVarcharType(20, false) });
            columns.Add(new string[] { "STATUS", "INTEGER" });
            columns.Add(new string[] { "NUMROWS", conn.getBigIntType() });
            columns.Add(new string[] { "NUMERRORS", conn.getBigIntType() });
            columns.Add(new string[] { "NUMWARNINGS", conn.getBigIntType() });
            columns.Add(new string[] { "DURATION", conn.getBigIntType() });
            columns.Add(new string[] { "MESSAGE", conn.getVarcharType(1000, false) });
            if (isIB)
                Database.createTableOnInfobright(conn, sp.Schema, Status.tableName, columns, false);
            else
                Database.createTable(conn, sp.Schema, Status.tableName, columns, false);
        }

        public static void createTables(QueryConnection conn, string schema)
        {
            List<string> schemaTable = null;
            bool newDB = false;
            bool isOracle = conn.isDBTypeOracle();
            string maxVarcharType = conn.getVarcharType(-1, true);
            string maxNVarcharType = conn.getNvarcharType(-1, true);
            string uniqueIdentifierType = conn.getUniqueIdentiferType();
            string datetimeType = conn.getDateTimeType();
            string bigIntType = conn.getBigIntType();
            if (!tableExists(conn, schema, USER_TABLE_NAME, ref schemaTable))
            {
                newDB = true;
                List<string[]> columns = new List<string[]>();
                columns.Add(new string[] { "PKID", conn.getUniqueIdentiferType() + " NOT NULL PRIMARY KEY" });
                columns.Add(new string[] { "Username", conn.getVarcharType(255, false) + " NOT NULL" });
                columns.Add(new string[] { "Email", conn.getVarcharType(128, false) + " NOT NULL" });
                columns.Add(new string[] { "FirstName", conn.getVarcharType(60, false) });
                columns.Add(new string[] { "LastName", conn.getVarcharType(60, false) });
                columns.Add(new string[] { "Title", conn.getVarcharType(60, false) });
                columns.Add(new string[] { "Phone", conn.getVarcharType(20, false) });
                columns.Add(new string[] { "Company", conn.getVarcharType(60, false) });
                columns.Add(new string[] { "City", conn.getVarcharType(60, false) });
                columns.Add(new string[] { "State", conn.getVarcharType(60, false) });
                columns.Add(new string[] { "Zip", conn.getVarcharType(60, false) });
                columns.Add(new string[] { "Country", conn.getVarcharType(60, false) });
                columns.Add(new string[] { conn.getReservedName("Comment"), conn.getVarcharType(255, false) });
                columns.Add(new string[] { "Password", conn.getVarcharType(128, false) + " NOT NULL" });
                columns.Add(new string[] { "PasswordQuestion", conn.getVarcharType(255, false) });
                columns.Add(new string[] { "PasswordAnswer", "varchar (255)" });
                columns.Add(new string[] { "IsApproved", conn.getBooleanType("IsApproved") });
                columns.Add(new string[] { "LastActivityDate", datetimeType });
                columns.Add(new string[] { "LastLoginDate", datetimeType });
                columns.Add(new string[] { "LastPasswordChangedDate", datetimeType });
                columns.Add(new string[] { "CreationDate", datetimeType });
                columns.Add(new string[] { "IsOnLine", conn.getBooleanType("IsOnLine") });
                columns.Add(new string[] { "IsLockedOut", conn.getBooleanType("IsLockedOut") });
                columns.Add(new string[] { "LastLockedOutDate", datetimeType });
                if (conn.isDBTypeOracle())
                {
                    columns.Add(new string[] { "FailedPwdAttemptCount", "int" });
                    columns.Add(new string[] { "FailedPwdAttemptWindowStart", datetimeType });
                    columns.Add(new string[] { "FailedPwdAnsAttemptCount", "int" });
                    columns.Add(new string[] { "FailedPwdAnsAttemptWindowStart", datetimeType });
                }
                else
                {
                    columns.Add(new string[] { "FailedPasswordAttemptCount", "int" });
                    columns.Add(new string[] { "FailedPasswordAttemptWindowStart", datetimeType });
                    columns.Add(new string[] { "FailedPasswordAnswerAttemptCount", "int" });
                    columns.Add(new string[] { "FailedPasswordAnswerAttemptWindowStart", datetimeType });
                }
                columns.Add(new string[] { "NumShares", "int" });
                columns.Add(new string[] { "StepByStep", conn.getBooleanType("StepByStep") });
                columns.Add(new string[] { "ContactStatus", "int" });
                columns.Add(new string[] { "ConnectString", conn.getVarcharType(1024, false) });
                columns.Add(new string[] { "DatabaseType", conn.getVarcharType(60, false) });
                columns.Add(new string[] { "DatabaseDriver", conn.getVarcharType(60, false) });
                columns.Add(new string[] { "DatabaseLoadDir", maxVarcharType });
                columns.Add(new string[] { "AdminUser", conn.getVarcharType(60, false) });
                columns.Add(new string[] { "AdminPWD", conn.getVarcharType(60, false) });
                columns.Add(new string[] { "RepositoryAdmin", conn.getBooleanType("RepositoryAdmin") });
                columns.Add(new string[] { "Department", conn.getVarcharType(50, false) });
                columns.Add(new string[] { "Industry", conn.getVarcharType(50, false) });
                columns.Add(new string[] { "OrgSize", conn.getVarcharType(15, false) });
                columns.Add(new string[] { "LikelyUse", conn.getVarcharType(20, false) });
                columns.Add(new string[] { "DefaultSpace", conn.getNvarcharType(255, false) });
                columns.Add(new string[] { "DefaultSpaceId", uniqueIdentifierType });
                columns.Add(new string[] { "DefaultDashboards", conn.getBooleanType("DefaultDashboards") });
                columns.Add(new string[] { "LOGO_IMAGE_ID", uniqueIdentifierType });
                columns.Add(new string[] { "HeaderBackground", bigIntType });
                columns.Add(new string[] { "PartnerCode", "varchar(60)" });
                columns.Add(new string[] { "CreateNewUserRights", conn.getBooleanType("CreateNewUserRights") });
                columns.Add(new string[] { "QueryConnectString", conn.getVarcharType(120, false) });
                columns.Add(new string[] { "QueryDatabaseType", conn.getVarcharType(60, false) });
                columns.Add(new string[] { "QueryDatabaseDriver", conn.getVarcharType(60, false) });
                columns.Add(new string[] { "QueryUser", conn.getVarcharType(60, false) });
                columns.Add(new string[] { "QueryPWD", conn.getVarcharType(60, false) });
                columns.Add(new string[] { "QueryConnectionName", "varchar (60)" });
                columns.Add(new string[] { "ADMIN_ACCOUNT_ID", uniqueIdentifierType });
                columns.Add(new string[] { "MANAGED_ACCOUNT_ID", uniqueIdentifierType });
                columns.Add(new string[] { "Operations", conn.getBooleanType("Operations") });
                columns.Add(new string[] { "Site", conn.getVarcharType(256, false) });
                columns.Add(new string[] { "ReleaseType", "INT" });
                columns.Add(new string[] { "Disabled", conn.getBooleanType("Disabled") });
                columns.Add(new string[] { "HTMLInterface", conn.getBooleanType("HTMLInterface") });
                columns.Add(new string[] { "SpaceType", "int" });
                columns.Add(new string[] { "ExternalDBEnabled", conn.getBooleanType("ExternalDBEnabled") });
                columns.Add(new string[] { "DenyAddSpace", conn.getBooleanType("DenyAddSpace") });
                createTable(conn, schema, USER_TABLE_NAME, columns, false);
            }
            if (!tableExists(conn, schema, SPACE_TABLE_NAME, ref schemaTable))
            {
                List<string[]> columns = new List<string[]>();
                columns.Add(new string[] { "ID", conn.getUniqueIdentiferType() + " NOT NULL PRIMARY KEY" });
                columns.Add(new string[] { "Name", conn.getVarcharType(255, false) + " NOT NULL" });
                columns.Add(new string[] { "Comments", maxVarcharType });
                columns.Add(new string[] { conn.getMSSQLReservedName("Schema"), conn.getVarcharType(255, false) });
                columns.Add(new string[] { "Directory", maxVarcharType });
                columns.Add(new string[] { "Automatic", conn.getBooleanType("Automatic") });
                columns.Add(new string[] { "LoadNumber", "int" });
                columns.Add(new string[] { "SourceTemplateName", conn.getVarcharType(60, false) });
                columns.Add(new string[] { "Type", "int" });
                columns.Add(new string[] { "ConnectString", conn.getVarcharType(1024, false) });
                columns.Add(new string[] { "DatabaseType", conn.getVarcharType(60, false) });
                columns.Add(new string[] { "DatabaseDriver", conn.getVarcharType(60, false) });
                columns.Add(new string[] { "DatabaseLoadDir", maxVarcharType });
                columns.Add(new string[] { "AdminUser", conn.getVarcharType(60, false) });
                columns.Add(new string[] { "AdminPWD", conn.getVarcharType(60, false) });
                columns.Add(new string[] { "Active", conn.getBooleanType("Active") });
                columns.Add(new string[] { "Folded", conn.getBooleanType("Folded") });
                columns.Add(new string[] { "TestMode", conn.getBooleanType("TestMode") });
                columns.Add(new string[] { "Locked", conn.getBooleanType("Locked") });
                columns.Add(new string[] { "Demo", conn.getBooleanType("Demo") });
                columns.Add(new string[] { "SSO", conn.getBooleanType("SSO") });
                columns.Add(new string[] { "SSOPWD", conn.getVarcharType(128, false) });
                columns.Add(new string[] { "QueryConnectString", conn.getVarcharType(120, false) });
                columns.Add(new string[] { "QueryDatabaseType", conn.getVarcharType(60, false) });
                columns.Add(new string[] { "QueryDatabaseDriver", conn.getVarcharType(60, false) });
                columns.Add(new string[] { "QueryUser", conn.getVarcharType(60, false) });
                columns.Add(new string[] { "QueryPWD", conn.getVarcharType(60, false) });
                columns.Add(new string[] { "QueryConnectionName", conn.getVarcharType(60, false) });
                columns.Add(new string[] { "MaxQueryRows", "int" });
                columns.Add(new string[] { "MaxQueryTimeout", "int" });
                columns.Add(new string[] { "QueryLanguageVersion", "int" });
                columns.Add(new string[] { "MappingModifiedDate", datetimeType });
                columns.Add(new string[] { "UsageTracking", conn.getBooleanType("UsageTracking") });
                columns.Add(new string[] { "EngineCommand", conn.getVarcharType(1024, false) });
                columns.Add(new string[] { "DeleteDataCommand", conn.getVarcharType(1024, false) });
                columns.Add(new string[] { "ProcessVersion", "int" });
                columns.Add(new string[] { "AppVersion", "int" });
                columns.Add(new string[] { "Available", "int" });
                columns.Add(new string[] { "UseDynamicGroups", conn.getBooleanType("UseDynamicGroups") });
                columns.Add(new string[] { "Deleted", conn.getBooleanType("Deleted") });
                columns.Add(new string[] { "AllowRetryFailedLoad", conn.getBooleanType("AllowRetryFailedLoad") });
                columns.Add(new string[] { "SFDCVersion", "int" });
                columns.Add(new string[] { "DeletedDate", datetimeType });
                columns.Add(new string[] { "DisallowExternalScheduler", conn.getBooleanType("DisallowExternalScheduler") });
                columns.Add(new string[] { "SFDCNumThreads", "int" });
                columns.Add(new string[] { "Discovery", conn.getBooleanType("Discovery") });
                columns.Add(new string[] { "MapNullsToZero", conn.getBooleanType("MapNullsToZero") });
                columns.Add(new string[] { "AWSAccessKeyId", conn.getVarcharType(256, false) });
                columns.Add(new string[] { "AWSSecretKey", conn.getVarcharType(256, false) });
                columns.Add(new string[] { "ReportFrom", conn.getVarcharType(128, false) });
                columns.Add(new string[] { "ReportSubject", conn.getVarcharType(255, false) });
                columns.Add(new string[] { "DBTimeZone", conn.getVarcharType(255, false) });
                columns.Add(new string[] { "CreatedDate", datetimeType });
                createTable(conn, schema, SPACE_TABLE_NAME, columns, false);
            }
            if (!tableExists(conn, schema, USER_SPACE_INXN, ref schemaTable))
            {
                List<string[]> columns = new List<string[]>();
                columns.Add(new string[] { "USER_ID", uniqueIdentifierType });
                columns.Add(new string[] { "SPACE_ID", uniqueIdentifierType });
                columns.Add(new string[] { "Owner", conn.getBooleanType("Owner") });
                columns.Add(new string[] { "Admin", conn.getBooleanType("Admin") });
                columns.Add(new string[] { "Adhoc", conn.getBooleanType("Adhoc") });
                columns.Add(new string[] { "Dashboards", conn.getBooleanType("Dashboards") });
                columns.Add(new string[] { "StartDate", datetimeType });
                createTable(conn, schema, USER_SPACE_INXN, columns, false);
                createIndex(conn, schema, USER_SPACE_INXN, "IDX1", new string[] { "USER_ID" });
                createIndex(conn, schema, USER_SPACE_INXN, "IDX2", new string[] { "SPACE_ID" });
            }
            if (!tableExists(conn, schema, INVITES_TABLE_NAME, ref schemaTable))
            {
                List<string[]> columns = new List<string[]>();
                columns.Add(new string[] { "Email", conn.getVarcharType(128, false) });
                columns.Add(new string[] { "SPACE_ID", uniqueIdentifierType });
                columns.Add(new string[] { "InviteDate", datetimeType });
                columns.Add(new string[] { "InviteUser", uniqueIdentifierType });
                createTable(conn, schema, INVITES_TABLE_NAME, columns, false);
                createIndex(conn, schema, INVITES_TABLE_NAME, "IDX1", new string[] { "Email", "SPACE_ID" });
            }
            if (!tableExists(conn, schema, PRODUCTS_TABLE, ref schemaTable))
            {
                List<string[]> columns = new List<string[]>();
                columns.Add(new string[] { "PRODUCT_ID", "int NOT NULL PRIMARY KEY" });
                columns.Add(new string[] { "Name", conn.getVarcharType(80, false) });
                columns.Add(new string[] { "Description", conn.getVarcharType(2000, false) });
                columns.Add(new string[] { conn.getReservedName("Default"), conn.getBooleanType(conn.getReservedName("Default")) });
                columns.Add(new string[] { "Type", "int" });
                columns.Add(new string[] { "AccountType", "int" });
                columns.Add(new string[] { "Category", conn.getVarcharType(80, false) });
                columns.Add(new string[] { "PricingDetails", conn.getVarcharType(2000, false) });
                columns.Add(new string[] { "PricePerMonth", "decimal(9,2)" });
                columns.Add(new string[] { "HasPromotion", conn.getBooleanType("HasPromotion") });
                columns.Add(new string[] { "PromotionCode", conn.getVarcharType(80, false) });
                columns.Add(new string[] { "PromotionPrice", "decimal(9,2)" });
                columns.Add(new string[] { "PromotionEndDate", datetimeType });
                columns.Add(new string[] { "PromotionDurationDays", "int" });
                columns.Add(new string[] { "RowLimit", "int" });
                columns.Add(new string[] { "DataLimit", bigIntType });
                columns.Add(new string[] { "ShareLimit", "int" });
                columns.Add(new string[] { "ShowAsOption", conn.getBooleanType("ShowAsOption") });
                columns.Add(new string[] { "MaxAllowableData", bigIntType });
                columns.Add(new string[] { "MaxAllowableRows", "int" });
                columns.Add(new string[] { "MaxPublishData", bigIntType });
                columns.Add(new string[] { "MaxPublishRows", "int" });
                columns.Add(new string[] { "MaxPublishDataPerDay", bigIntType });
                columns.Add(new string[] { "MaxPublishRowsPerDay", "int" });
                columns.Add(new string[] { "MaxPublishTime", "int" });
                columns.Add(new string[] { "DefaultActive", conn.getBooleanType("DefaultActive") });
                columns.Add(new string[] { "SingleUse", conn.getBooleanType("SingleUse") });
                columns.Add(new string[] { "MaxScriptStatements", bigIntType });
                columns.Add(new string[] { "MaxInputRows", "int" });
                columns.Add(new string[] { "MaxOutputRows", "int" });
                columns.Add(new string[] { "ScriptQueryTimeout", "int" });
                columns.Add(new string[] { "MaxDeliveredReportsPerDay", "int" });
                createTable(conn, schema, PRODUCTS_TABLE, columns, false);
            }
            if (!tableExists(conn, schema, USER_PRODUCT_TABLE, ref schemaTable))
            {
                List<string[]> columns = new List<string[]>();
                columns.Add(new string[] { "PRODUCT_ID", "int" });
                columns.Add(new string[] { "USER_ID", uniqueIdentifierType });
                columns.Add(new string[] { "StartDate", datetimeType });
                columns.Add(new string[] { "EndDate", datetimeType });
                columns.Add(new string[] { "Quantity", "int" });
                columns.Add(new string[] { "Active", conn.getBooleanType("Active") });
                createTable(conn, schema, USER_PRODUCT_TABLE, columns, false);
                createIndex(conn, schema, USER_PRODUCT_TABLE, "IDX1", new string[] { "USER_ID" });
                createIndex(conn, schema, USER_PRODUCT_TABLE, "IDX2", new string[] { "PRODUCT_ID" });
            }
            if (!tableExists(conn, schema, TEMPLATES_TABLE_NAME, ref schemaTable))
            {
                List<string[]> columns = new List<string[]>();
                columns.Add(new string[] { "TEMPLATE_ID", conn.getUniqueIdentiferType() + " NOT NULL PRIMARY KEY" });
                columns.Add(new string[] { "CREATOR_ID", uniqueIdentifierType });
                columns.Add(new string[] { "CreateDate", datetimeType });
                columns.Add(new string[] { "Name", conn.getVarcharType(60, false) });
                columns.Add(new string[] { "Category", conn.getVarcharType(60, false) });
                columns.Add(new string[] { "NumUses", "int" });
                columns.Add(new string[] { "Automatic", conn.getBooleanType("Automatic") });
                columns.Add(new string[] { "Private", conn.getBooleanType("Private") });
                columns.Add(new string[] { "Comments", maxVarcharType });
                createTable(conn, schema, TEMPLATES_TABLE_NAME, columns, false);
                if (!conn.isDBTypeOracle())
                {
                    createIndex(conn, schema, TEMPLATES_TABLE_NAME, "IDX1", new string[] { "TEMPLATE_ID" });
                }
                createIndex(conn, schema, TEMPLATES_TABLE_NAME, "IDX2", new string[] { "CREATOR_ID" });
            }
            if (!tableExists(conn, schema, CATEGORIES_TABLE_NAME, ref schemaTable))
            {
                List<string[]> columns = new List<string[]>();
                columns.Add(new string[] { "Category", conn.getVarcharType(60, false) });
                columns.Add(new string[] { "Subcategory", conn.getVarcharType(60, false) });
                createTable(conn, schema, CATEGORIES_TABLE_NAME, columns, false);
                QueryCommand cmd = conn.CreateCommand();
                cmd.CommandText = "INSERT INTO " + schema + "." + CATEGORIES_TABLE_NAME +
                    " (Category, Subcategory) VALUES (?,?)";
                cmd.Parameters.Add("Category", OdbcType.VarChar);
                cmd.Parameters.Add("Subcategory", OdbcType.VarChar);
                cmd.Parameters[0].Value = "Personal Finance";
                cmd.Parameters[1].Value = System.DBNull.Value;
                cmd.ExecuteNonQuery();
                cmd.Parameters[0].Value = "Sales";
                cmd.ExecuteNonQuery();
                cmd.Parameters[0].Value = "Accounting";
                cmd.ExecuteNonQuery();
                cmd.Parameters[0].Value = "Web/Internet";
                cmd.ExecuteNonQuery();
                cmd.Parameters[0].Value = "Business";
                cmd.ExecuteNonQuery();
                cmd.Parameters[0].Value = "Other";
                cmd.ExecuteNonQuery();
                cmd.Dispose();
            }
            if (!tableExists(conn, schema, ATTACHMENTS_TABLE_NAME, ref schemaTable))
            {
                List<string[]> columns = new List<string[]>();
                columns.Add(new string[] { "TEMPLATE_ID", uniqueIdentifierType });
                columns.Add(new string[] { "Filename", conn.getVarcharType(128, false) });
                createTable(conn, schema, ATTACHMENTS_TABLE_NAME, columns, false);
                createIndex(conn, schema, ATTACHMENTS_TABLE_NAME, "IDX1", new string[] { "TEMPLATE_ID" });
            }
            if (!tableExists(conn, schema, RATINGS_TABLE_NAME, ref schemaTable))
            {
                List<string[]> columns = new List<string[]>();
                columns.Add(new string[] { "TEMPLATE_ID", uniqueIdentifierType });
                columns.Add(new string[] { "USER_ID", uniqueIdentifierType });
                columns.Add(new string[] { "Rating", "int" });
                createTable(conn, schema, RATINGS_TABLE_NAME, columns, false);
                createIndex(conn, schema, RATINGS_TABLE_NAME, "IDX1", new string[] { "TEMPLATE_ID" });
                createIndex(conn, schema, RATINGS_TABLE_NAME, "IDX2", new string[] { "USER_ID" });
            }
            if (!tableExists(conn, schema, PUBLISH_TABLE, ref schemaTable))
            {
                List<string[]> columns = new List<string[]>();
                columns.Add(new string[] { "SPACE_ID", uniqueIdentifierType });
                columns.Add(new string[] { "LoadNumber", "int" });
                columns.Add(new string[] { "StartPublishDate", datetimeType }); // Actual date
                columns.Add(new string[] { "PublishDate", datetimeType }); // Actual date
                columns.Add(new string[] { "LoadDate", datetimeType }); // Date picked by user
                columns.Add(new string[] { "NumRows", "int" });
                columns.Add(new string[] { "DataSize", bigIntType });
                columns.Add(new string[] { "TableName", conn.getVarcharType(255, false) });
                columns.Add(new string[] { "LoadGroup", conn.getVarcharType(255, false) });
                createTable(conn, schema, PUBLISH_TABLE, columns, false);
                createIndex(conn, schema, PUBLISH_TABLE, "IDX1", new string[] { "SPACE_ID" });
            }
            if (!tableExists(conn, schema, PARTNER_CODE_TABLE, ref schemaTable))
            {
                List<string[]> columns = new List<string[]>();
                columns.Add(new string[] { "Code", conn.getVarcharType(60, false) });
                columns.Add(new string[] { "Name", conn.getVarcharType(120, false) });
                createTable(conn, schema, PARTNER_CODE_TABLE, columns, false);
            }
            if (!tableExists(conn, schema, VERSION_TABLE_NAME, ref schemaTable))
            {
                List<string[]> columns = new List<string[]>();
                columns.Add(new string[] { "CurrentVersion", "int" });
                createTable(conn, schema, VERSION_TABLE_NAME, columns, false);
                if (newDB)
                    updateCurrentVersion(conn, schema, Global.CURRENT_VERSION);
            }
            if (!tableExists(conn, schema, SCHEDULED_LOAD_TABLE, ref schemaTable))
            {
                List<string[]> columns = new List<string[]>();
                columns.Add(new string[] { "SPACE_ID", uniqueIdentifierType });
                columns.Add(new string[] { "OBJ_ID", uniqueIdentifierType });
                columns.Add(new string[] { "Module", conn.getVarcharType(40, false) });
                columns.Add(new string[] { "NextDate", datetimeType });
                columns.Add(new string[] { "WorkTime", datetimeType });
                columns.Add(new string[] { "WorkingServer", conn.getVarcharType(40, false) });
                columns.Add(new string[] { "Parameters", maxVarcharType });
                columns.Add(new string[] { "Status", conn.getVarcharType(100, false) });
                createTable(conn, schema, SCHEDULED_LOAD_TABLE, columns, false);
                createIndex(conn, schema, SCHEDULED_LOAD_TABLE, "IDX1", new string[] { "SPACE_ID" });
            }
            if (!tableExists(conn, schema, IMAGE_TABLE, ref schemaTable))
            {
                List<string[]> columns = new List<string[]>();
                columns.Add(new string[] { "IMAGE_ID", uniqueIdentifierType });
                columns.Add(new string[] { "Type", conn.getVarcharType(10, false) });
                columns.Add(new string[] { "Image", isOracle ? "blob" : maxVarcharType });
                createTable(conn, schema, IMAGE_TABLE, columns, false);
            }
            if (!tableExists(conn, schema, PROXY_TABLE, ref schemaTable))
            {
                List<string[]> columns = new List<string[]>();
                columns.Add(new string[] { "ID", conn.getVarcharType(200, false) });
                columns.Add(new string[] { "Server", conn.getVarcharType(120, false) });
                columns.Add(new string[] { "Port", "Int" });
                columns.Add(new string[] { "RegistrationDate", datetimeType });
                createTable(conn, schema, PROXY_TABLE, columns, false);
            }
            if (!tableExists(conn, schema, SCHEDULED_REPORTS_TABLE, ref schemaTable))
            {
                List<string[]> columns = new List<string[]>();
                columns.Add(new string[] { "SPACE_ID", uniqueIdentifierType });
                columns.Add(new string[] { "SCHEDULE_ID", uniqueIdentifierType });
                columns.Add(new string[] { "USER_ID", uniqueIdentifierType });
                columns.Add(new string[] { "ReportPath", conn.getVarcharType(255, false) });
                columns.Add(new string[] { "TriggerReportPath", conn.getVarcharType(255, false) });
                columns.Add(new string[] { "ToReportPath", conn.getVarcharType(255, false) });
                columns.Add(new string[] { "Interval", "Int" });
                columns.Add(new string[] { "DayOfWeek", "Int" });
                columns.Add(new string[] { "DayOfMonth", "Int" });
                columns.Add(new string[] { "Hour", "Int" });
                columns.Add(new string[] { "Minute", "Int" });
                columns.Add(new string[] { "Type", conn.getVarcharType(128, false) });
                columns.Add(new string[] { "Subject", conn.getVarcharType(255, false) });
                columns.Add(new string[] { "Body", maxVarcharType });
                columns.Add(new string[] { "List", conn.getVarcharType(2048, false) });
                createTable(conn, schema, SCHEDULED_REPORTS_TABLE, columns, false);
                createIndex(conn, schema, SCHEDULED_REPORTS_TABLE, "IDX1", new string[] { "SPACE_ID" });
                createIndex(conn, schema, SCHEDULED_REPORTS_TABLE, "IDX2", new string[] { "SCHEDULE_ID" });
            }
            if (!tableExists(conn, schema, DELIVERY_LOG, ref schemaTable))
            {
                List<string[]> columns = new List<string[]>();
                columns.Add(new string[] { "SPACE_ID", uniqueIdentifierType });
                columns.Add(new string[] { "ReportPath", conn.getVarcharType(255, false) });
                columns.Add(new string[] { "NumDeliveries", "Int" });
                columns.Add(new string[] { "DeliveryDate", datetimeType });
                createTable(conn, schema, DELIVERY_LOG, columns, false);
                createIndex(conn, schema, DELIVERY_LOG, "IDX1", new string[] { "SPACE_ID" });
            }
            if (!tableExists(conn, schema, ALLOWED_USER_DOMAINS_TABLE, ref schemaTable))
            {
                List<string[]> columns = new List<string[]>();
                columns.Add(new string[] { "USER_ID", uniqueIdentifierType });
                columns.Add(new string[] { "UsernameMask", conn.getVarcharType(255, false) });
                createTable(conn, schema, ALLOWED_USER_DOMAINS_TABLE, columns, false);
                createIndex(conn, schema, ALLOWED_USER_DOMAINS_TABLE, "IDX1", new string[] { "USER_ID" });
            }
            if (!tableExists(conn, schema, CREATED_USERS_TABLE, ref schemaTable))
            {
                List<string[]> columns = new List<string[]>();
                columns.Add(new string[] { "USER_ID", uniqueIdentifierType });
                columns.Add(new string[] { "CreatedByUser", uniqueIdentifierType });
                columns.Add(new string[] { "CreatedOnDate", datetimeType });
                createTable(conn, schema, CREATED_USERS_TABLE, columns, false);
                createIndex(conn, schema, CREATED_USERS_TABLE, "IDX1", new string[] { "USER_ID" });
            }
            // Broken out to separate method so that it can be used directly in unit tests
            createUserPreferenceTable(conn, schema, ref schemaTable, false);

            if (!tableExists(conn, schema, PROXY_USERS, ref schemaTable))
            {
                List<string[]> columns = new List<string[]>();
                columns.Add(new string[] { "USER_ID", uniqueIdentifierType });
                columns.Add(new string[] { "PROXY_USER_ID", uniqueIdentifierType });
                columns.Add(new string[] { "RepositoryAdmin", conn.getBooleanType("RepositoryAdmin") });
                columns.Add(new string[] { "Operations", conn.getBooleanType("Operations") });
                columns.Add(new string[] { "ExpirationDate", datetimeType });
                createTable(conn, schema, PROXY_USERS, columns, false);
                createIndex(conn, schema, PROXY_USERS, "IDX1", new string[] { "USER_ID" });
            }
            if (!tableExists(conn, schema, ALLOWED_IPS, ref schemaTable))
            {
                List<string[]> columns = new List<string[]>();
                columns.Add(new string[] { "USER_ID", conn.getUniqueIdentiferType() });
                columns.Add(new string[] { "ACCOUNT_ID", conn.getUniqueIdentiferType() });
                columns.Add(new string[] { "IPAddress", conn.getVarcharType(32, false) });
                createTable(conn, schema, ALLOWED_IPS, columns, false);
                createIndex(conn, schema, ALLOWED_IPS, "IDX1", new string[] { "USER_ID" });
                createIndex(conn, schema, ALLOWED_IPS, "IDX2", new string[] { "ACCOUNT_ID" });
            }

            if (!tableExists(conn, schema, ACL_INFO_TABLE, ref schemaTable))
            {
                List<string[]> columns = new List<string[]>();
                columns.Add(new string[] { "ID", "Int" });
                columns.Add(new string[] { "Name", conn.getVarcharType(100, false) });
                columns.Add(new string[] { "CreatedDate", datetimeType });
                columns.Add(new string[] { "ModifiedDate", datetimeType });
                createTable(conn, schema, ACL_INFO_TABLE, columns, false);
                createIndex(conn, schema, ACL_INFO_TABLE, "IDX1", new string[] { "ID" });
            }

            if (!tableExists(conn, schema, SPACE_GROUP_TABLE, ref schemaTable))
            {
                List<string[]> columns = new List<string[]>();
                columns.Add(new string[] { "SPACE_ID", uniqueIdentifierType });
                columns.Add(new string[] { "GROUP_ID", uniqueIdentifierType });
                columns.Add(new string[] { "GroupName", conn.getVarcharType(255, false) });
                columns.Add(new string[] { "InternalGroup", conn.getBooleanType("InternalGroup") });
                columns.Add(new string[] { "CreatedDate", datetimeType });
                columns.Add(new string[] { "ModifiedDate", datetimeType });
                createTable(conn, schema, SPACE_GROUP_TABLE, columns, false);
                createIndex(conn, schema, SPACE_GROUP_TABLE, "IDX1", new string[] { "SPACE_ID" });
                createIndex(conn, schema, SPACE_GROUP_TABLE, "IDX2", new string[] { "GROUP_ID" });
            }

            if (!tableExists(conn, schema, GROUP_USER_TABLE, ref schemaTable))
            {
                List<string[]> columns = new List<string[]>();
                columns.Add(new string[] { "GROUP_ID", uniqueIdentifierType });
                columns.Add(new string[] { "USER_ID", uniqueIdentifierType });
                columns.Add(new string[] { "CreatedDate", datetimeType });
                columns.Add(new string[] { "ModifiedDate", datetimeType });
                createTable(conn, schema, GROUP_USER_TABLE, columns, false);
                createIndex(conn, schema, GROUP_USER_TABLE, "IDX1", new string[] { "GROUP_ID" });
                createIndex(conn, schema, GROUP_USER_TABLE, "IDX2", new string[] { "USER_ID" });
            }

            if (!tableExists(conn, schema, GROUP_ACL_TABLE, ref schemaTable))
            {
                List<string[]> columns = new List<string[]>();
                columns.Add(new string[] { "GROUP_ID", uniqueIdentifierType });
                columns.Add(new string[] { "ACL_ID", "Int" });
                columns.Add(new string[] { "CreatedDate", datetimeType });
                columns.Add(new string[] { "ModifiedDate", datetimeType });
                createTable(conn, schema, GROUP_ACL_TABLE, columns, false);
                createIndex(conn, schema, GROUP_ACL_TABLE, "IDX1", new string[] { "GROUP_ID" });
            }

            if (!tableExists(conn, schema, ACCOUNTS_TABLE, ref schemaTable))
            {
                List<string[]> columns = new List<string[]>();
                columns.Add(new string[] { "ACCOUNT_ID", conn.getUniqueIdentiferType() + " NOT NULL PRIMARY KEY" });
                columns.Add(new string[] { "Name", conn.getVarcharType(255, false) });
                columns.Add(new string[] { "ExpirationDate", datetimeType });
                columns.Add(new string[] { "MaxConcurrentLogins", "Int" });
                columns.Add(new string[] { "MaxUsers", "Int" });
                columns.Add(new string[] { "IsEnterprise", conn.getBooleanType("IsEnterprise") });
                columns.Add(new string[] { "CreatedDate", datetimeType });
                columns.Add(new string[] { "ModifiedDate", datetimeType });
                columns.Add(new string[] { "Disabled", conn.getBooleanType("Disabled") });
                createTable(conn, schema, ACCOUNTS_TABLE, columns, false);
            }

            if (!tableExists(conn, schema, CURRENT_LOGINS_TABLE, ref schemaTable))
            {
                List<string[]> columns = new List<string[]>();
                columns.Add(new string[] { "USER_ID", uniqueIdentifierType });
                columns.Add(new string[] { "Remote_IP", conn.getVarcharType(255, false) });
                columns.Add(new string[] { "Login_Time", datetimeType });
                columns.Add(new string[] { "Logout_Time", datetimeType });
                columns.Add(new string[] { "Session_ID", conn.getVarcharType(255, false) });
                columns.Add(new string[] { "Host_Server", conn.getVarcharType(255, false) });
                createTable(conn, schema, CURRENT_LOGINS_TABLE, columns, false);
                createIndex(conn, schema, CURRENT_LOGINS_TABLE, "IDX1", new string[] { "Session_ID" });
                createIndex(conn, schema, CURRENT_LOGINS_TABLE, "IDX2", new string[] { "Logout_Time" });
            }

            if (!tableExists(conn, schema, ACCOUNT_OVERRIDE_PARAMETERS_TABLE, ref schemaTable))
            {
                List<string[]> columns = new List<string[]>();
                columns.Add(new string[] { "ACCOUNT_ID", uniqueIdentifierType });
                columns.Add(new string[] { "PARAMETER_NAME", conn.getVarcharType(255, false) });
                columns.Add(new string[] { "PARAMETER_VALUE", conn.getVarcharType(255, false) });
                createTable(conn, schema, ACCOUNT_OVERRIDE_PARAMETERS_TABLE, columns, false);
                createIndex(conn, schema, ACCOUNT_OVERRIDE_PARAMETERS_TABLE, "IDX1", new string[] { "ACCOUNT_ID" });
            }

            if (!tableExists(conn, schema, SSO_TOKENS_TABLE, ref schemaTable))
            {
                List<string[]> columns = new List<string[]>();
                columns.Add(new string[] { "TOKEN_ID", conn.getUniqueIdentiferType() + " NOT NULL PRIMARY KEY" });
                columns.Add(new string[] { "End_Of_Life", datetimeType });
                columns.Add(new string[] { "USER_ID", uniqueIdentifierType });
                columns.Add(new string[] { "SPACE_ID", uniqueIdentifierType });
                columns.Add(new string[] { "SessionVariables", maxNVarcharType });
                columns.Add(new string[] { "Used", conn.getBooleanType("Used") });
                columns.Add(new string[] { "SUPERUSER", conn.getBooleanType("SUPERUSER") });
                columns.Add(new string[] { "REPADMIN", conn.getBooleanType("REPADMIN") });
                columns.Add(new string[] { "TYPE", conn.getVarcharType(32, false) });
                columns.Add(new string[] { "HOSTADDR", conn.getVarcharType(64, false) });
                columns.Add(new string[] { "TIMEOUT", "INT" });
                columns.Add(new string[] { "LOGOUTPAGE", conn.getVarcharType(-1, true) });
                createTable(conn, schema, SSO_TOKENS_TABLE, columns, false);
                if (!conn.isDBTypeOracle())
                {
                    createIndex(conn, schema, SSO_TOKENS_TABLE, "IDX1", new string[] { "TOKEN_ID" });
                }
            }

            if (!tableExists(conn, schema, SERVER_STATUS_TABLE, ref schemaTable))
            {
                List<string[]> columns = new List<string[]>();
                columns.Add(new string[] { "HOSTADDR", conn.getVarcharType(64, false) + " PRIMARY KEY" });
                columns.Add(new string[] { "STATUSFLAG", "INT NOT NULL" });
                columns.Add(new string[] { "MESSAGE", conn.getVarcharType(512, false) });
                createTable(conn, schema, SERVER_STATUS_TABLE, columns, false);
            }

            if (!tableExists(conn, schema, RELEASE_INFO_TABLE, ref schemaTable))
            {
                List<string[]> columns = new List<string[]>();
                columns.Add(new string[] { "ID", "INT PRIMARY KEY" });
                columns.Add(new string[] { "Name", conn.getVarcharType(1028, false) });
                columns.Add(new string[] { "URL", conn.getVarcharType(2048, false) });
                columns.Add(new string[] { "BCRegExp", conn.getVarcharType(2048, false) });
                columns.Add(new string[] { "Enabled", conn.getBooleanType("Enabled") });
                createTable(conn, schema, RELEASE_INFO_TABLE, columns, false);
            }

            if (!tableExists(conn, schema, PROCESS_VERSIONS_TABLE, ref schemaTable))
            {
                List<string[]> columns = new List<string[]>();
                columns.Add(new string[] { "ID", "INT PRIMARY KEY" });
                columns.Add(new string[] { "Name", conn.getVarcharType(1028, false) });
                columns.Add(new string[] { "Release", conn.getVarcharType(1028, false) });
                columns.Add(new string[] { "EngineCommand", conn.getVarcharType(2048, false) });
                columns.Add(new string[] { "DeleteCommand", conn.getVarcharType(2048, false) });
                columns.Add(new string[] { "Visible", conn.getBooleanType("Visible") });
                columns.Add(new string[] { "SupportSFDCExtract", conn.getBooleanType("SupportSFDCExtract") });
                columns.Add(new string[] { "SupportsRetryLoad", conn.getBooleanType("SupportsRetryLoad") });
                columns.Add(new string[] { "IsProcessingGroupAware", conn.getBooleanType("IsProcessingGroupAware") });
                createTable(conn, schema, PROCESS_VERSIONS_TABLE, columns, false);
            }

            if (!tableExists(conn, schema, PASSWORD_RESET_REQUEST_TABLE, ref schemaTable))
            {
                List<string[]> columns = new List<string[]>();
                columns.Add(new string[] { "TOKEN_ID", uniqueIdentifierType });
                columns.Add(new string[] { "USER_ID", uniqueIdentifierType });
                columns.Add(new string[] { "END_OF_LIFE", datetimeType });
                columns.Add(new string[] { "USED", conn.getBooleanType("USED") });
                createTable(conn, schema, PASSWORD_RESET_REQUEST_TABLE, columns, false);
            }

            if (!tableExists(conn, schema, CONNECTOR_MASTER_TABLE, ref schemaTable))
            {
                List<string[]> columns = new List<string[]>();
                columns.Add(new string[] { "ID", "INT PRIMARY KEY" });
                columns.Add(new string[] { "ControllerID", conn.getVarcharType(50, false) + " NOT NULL" });
                columns.Add(new string[] { "ConnectorID", "INT NOT NULL" });
                columns.Add(new string[] { "ConnectorName", conn.getVarcharType(255, false) + " NOT NULL" });
                columns.Add(new string[] { "ConnectorAPIVersion", conn.getVarcharType(50, false) + " NOT NULL" });
                columns.Add(new string[] { "ConnectionString", conn.getVarcharType(1000, false) + " NOT NULL" });
                columns.Add(new string[] { "IsDefaultVersion", conn.getBooleanType("IsDefaultVersion") });
                createTable(conn, schema, CONNECTOR_MASTER_TABLE, columns, false);
                createIndex(conn, schema, CONNECTOR_MASTER_TABLE, "IDX1", new string[] { "ID" });
            }

            if (!tableExists(conn, schema, CONNECTOR_INSTANCES_TABLE, ref schemaTable))
            {
                List<string[]> columns = new List<string[]>();
                columns.Add(new string[] { "InstanceID", "INT PRIMARY KEY" });
                columns.Add(new string[] { "ControllerID", conn.getVarcharType(50, false) + " NOT NULL" });
                columns.Add(new string[] { "ConnectorID", "INT NOT NULL" });
                columns.Add(new string[] { "HostName", conn.getVarcharType(100, false) + " NOT NULL" });
                columns.Add(new string[] { "ConnectorServerURL", conn.getVarcharType(1000, false) + " NOT NULL" });
                columns.Add(new string[] { "ConnectorApplication", conn.getVarcharType(1000, false) + " NOT NULL" });
                columns.Add(new string[] { "IsActive", conn.getBooleanType("IsActive") });
                columns.Add(new string[] { "Disabled", conn.getBooleanType("Disabled") });
                createTable(conn, schema, CONNECTOR_INSTANCES_TABLE, columns, false);
                createIndex(conn, schema, CONNECTOR_INSTANCES_TABLE, "IDX1", new string[] { "InstanceID" });
            }

            if (!tableExists(conn, schema, CONNECTOR_ACTIVITY_TABLE, ref schemaTable))
            {
                List<string[]> columns = new List<string[]>();
                columns.Add(new string[] { "ID", "INT PRIMARY KEY" });
                columns.Add(new string[] { "InstanceID", "INT NOT NULL" });
                columns.Add(new string[] { "Client", conn.getVarcharType(50, false) });
                columns.Add(new string[] { "SpaceID", conn.getUniqueIdentiferType() });
                columns.Add(new string[] { "UniqueID", conn.getUniqueIdentiferType() });
                columns.Add(new string[] { "RequestType", conn.getVarcharType(50, false) });
                columns.Add(new string[] { "Status", "INT" });
                columns.Add(new string[] { "StartTime", datetimeType });
                columns.Add(new string[] { "FinishTime", datetimeType });
                createTable(conn, schema, CONNECTOR_ACTIVITY_TABLE, columns, false);
                createIndex(conn, schema, CONNECTOR_ACTIVITY_TABLE, "IDX1", new string[] { "ID" });
            }

            if (!tableExists(conn, schema, CONNECTOR_PARAMETER_TABLE, ref schemaTable))
            {
                List<string[]> columns = new List<string[]>();
                columns.Add(new string[] { "ParameterID", "INT PRIMARY KEY" });
                columns.Add(new string[] { "ControllerID", conn.getVarcharType(50, false) + " NOT NULL" });
                columns.Add(new string[] { "ConnectorID", "INT NOT NULL" });
                columns.Add(new string[] { "ParameterName", conn.getVarcharType(255, false) + " NOT NULL " });
                columns.Add(new string[] { "ParameterValue", conn.getVarcharType(1000, false) });
                createTable(conn, schema, CONNECTOR_PARAMETER_TABLE, columns, false);
                createIndex(conn, schema, CONNECTOR_PARAMETER_TABLE, "IDX1", new string[] { "ParameterID" });
            }

            if (!tableExists(conn, schema, LOCALE_TABLE, ref schemaTable))
            {
                List<string[]> columns = new List<string[]>();
                columns.Add(new string[] { "LocaleID", conn.getVarcharType(10, false) + " PRIMARY KEY" });
                columns.Add(new string[] { "Description", conn.getNvarcharType(2000, false) });
                createTable(conn, schema, LOCALE_TABLE, columns, false);
            }

            createRunningProcessTable(conn, schema);
            createMessagesTable(conn, schema);
            createSpacesConfigTable(conn, schema);
            createSpaceOperationsTable(conn, schema);
            createDeliveryOpsTable(conn, schema);
            createLastLoadHistoryTable(conn, schema);
            createOpenIdUsersTable(conn, schema);
            createServerInfoTable(conn, schema);
            createAppLogsInfoTable(conn, schema);
            createPasswordPolicyTable(conn, schema);
            createPasswordHistoryTable(conn, schema);
            createUploadTokensTable(conn, schema, ref schemaTable);
            createPackageGroupTable(conn, schema, ref schemaTable);
            createPackageUsageable(conn, schema, ref schemaTable);
            createGlobalPropertiesTable(conn, schema);
            createSamlInfoTable(conn, schema);
            createSubDomainInfoTable(conn, schema);
            createAWSRegionsTable(conn, schema, ref schemaTable);
            createSamlAccountMappingTable(conn, schema, ref schemaTable);
        }

        private static void createAWSRegionsTable(QueryConnection conn, string schema, ref List<string> schemaTable)
        {
            if (!tableExists(conn, schema, AWS_REGIONS, ref schemaTable))
            {
                List<string[]> columns = new List<string[]>();
                columns.Add(new string[] { "ID", "INT PRIMARY KEY" });
                columns.Add(new string[] { "Name", conn.getNvarcharType(1028, false) });
                columns.Add(new string[] { "TimeBucket", conn.getNvarcharType(2048, false) });
                columns.Add(new string[] { "UploadBucket", conn.getNvarcharType(2048, false) });
                columns.Add(new string[] { "Active", conn.getBooleanType("Active") });
                columns.Add(new string[] { "CreatedDate", conn.getDateTimeType() });
                columns.Add(new string[] { "ModifiedDate", conn.getDateTimeType() });
                createTable(conn, schema, AWS_REGIONS, columns, false);
            }
        }

        private static void createSamlAccountMappingTable(QueryConnection conn, string schema, ref List<string> schemaTable)
        {
            if (!tableExists(conn, schema, SAML_ACCOUNT_MAPPING, ref schemaTable))
            {
                List<string[]> columns = new List<string[]>();                
                columns.Add(new string[] { "ID", conn.getUniqueIdentiferType() + " NOT NULL PRIMARY KEY" });
                columns.Add(new string[] { "SAMLID", conn.getUniqueIdentiferType() });
                columns.Add(new string[] { "ACCOUNTID", conn.getUniqueIdentiferType() });
                columns.Add(new string[] { "CreatedDate", conn.getDateTimeType() });
                columns.Add(new string[] { "ModifiedDate", conn.getDateTimeType() });
                createTable(conn, schema, SAML_ACCOUNT_MAPPING, columns, false);
                createIndex(conn, schema, SAML_ACCOUNT_MAPPING, "IDX1", new string[] { "ID" });
            }
        }


        public static void createGlobalPropertiesTable(QueryConnection conn, string schema)
        {
            if (!Database.tableExists(conn, schema, GLOBAL_PROPERTIES_TABLE))
            {
                List<string[]> columns = new List<string[]>();
                columns.Add(new string[] { "ID", conn.getUniqueIdentiferType() });
                columns.Add(new string[] { "Account_ID", conn.getUniqueIdentiferType() });
                columns.Add(new string[] { "PropertyName", conn.getVarcharType(50, false) });
                columns.Add(new string[] { "PropertyValue", conn.getVarcharType(1024, false) });
                columns.Add(new string[] { "Enable", conn.getBooleanType("Enable") });
                columns.Add(new string[] { "DateCreated", conn.getDateTimeType() });
                columns.Add(new string[] { "DateModified", conn.getDateTimeType() });
                columns.Add(new string[] { "Comments", conn.getVarcharType(-1, true) });
                Database.createTable(conn, schema, GLOBAL_PROPERTIES_TABLE, columns, false);
            }
        }

        public static void createPackageUsageable(QueryConnection conn, string schema, ref List<string> schemaTable)
        {
            if (!tableExists(conn, schema, PACKAGE_USAGE_INXN, ref schemaTable))
            {
                string maxVarcharType = conn.getVarcharType(-1, true);
                string uniqueIdentifierType = conn.getUniqueIdentiferType();
                string datetimeType = conn.getDateTimeType();

                List<string[]> columns = new List<string[]>();
                columns.Add(new string[] { "PACKAGE_ID", uniqueIdentifierType });
                columns.Add(new string[] { "CHILD_SPACE_ID", uniqueIdentifierType });
                columns.Add(new string[] { "CreatedDate", datetimeType });
                columns.Add(new string[] { "CreatedBy", conn.getVarcharType(64, false) });
                columns.Add(new string[] { "ModifiedDate", datetimeType });
                columns.Add(new string[] { "ModifiedBy", conn.getVarcharType(64, false) });
                columns.Add(new string[] { "PARENT_SPACE_ID", uniqueIdentifierType });
                columns.Add(new string[] { "Available", conn.getBooleanType("Available") });
                string constraint = "pk_PackageUsageID PRIMARY KEY (PACKAGE_ID, CHILD_SPACE_ID)";
                createTable(conn, schema, PACKAGE_USAGE_INXN, columns, false, constraint);
            }
        }

        public static void createPackageGroupTable(QueryConnection conn, string schema, ref List<string> schemaTable)
        {
            if (!tableExists(conn, schema, PACKAGE_GROUP_INXN, ref schemaTable))
            {
                string maxVarcharType = conn.getVarcharType(-1, true);
                string uniqueIdentifierType = conn.getUniqueIdentiferType();
                string datetimeType = conn.getDateTimeType();

                List<string[]> columns = new List<string[]>();
                columns.Add(new string[] { "PACKAGE_ID", uniqueIdentifierType });
                columns.Add(new string[] { "GROUP_ID", uniqueIdentifierType });
                columns.Add(new string[] { "SPACE_ID", uniqueIdentifierType });
                columns.Add(new string[] { "CreatedDate", datetimeType });
                columns.Add(new string[] { "CreatedBy", conn.getVarcharType(64, false) });
                columns.Add(new string[] { "ModifiedDate", datetimeType });
                columns.Add(new string[] { "ModifiedBy", conn.getVarcharType(64, false) });
                string constraint = "pk_PackageGroupID PRIMARY KEY (PACKAGE_ID, GROUP_ID)";
                createTable(conn, schema, PACKAGE_GROUP_INXN, columns, false, constraint);
            }
        }

        public static void createLastLoadHistoryTable(QueryConnection conn, string schema)
        {
            if (!Database.tableExists(conn, schema, LAST_LOAD_HISTORY))
            {
                List<string[]> columns = new List<string[]>();
                columns.Add(new string[] { "TM", conn.getDateTimeType() });
                columns.Add(new string[] { "SPACE_ID", conn.getUniqueIdentiferType() });
                columns.Add(new string[] { "LoadNumber", "INT" });
                columns.Add(new string[] { "LoadGroup", conn.getVarcharType(1024, false) });
                columns.Add(new string[] { "LoadDate", conn.getDateTimeType() });
                columns.Add(new string[] { "ProcessingGroups", conn.getVarcharType(1024, false) });
                Database.createTable(conn, schema, LAST_LOAD_HISTORY, columns, false);
            }
        }

        public static void createRunningProcessTable(QueryConnection conn, string schema)
        {
            if (!Database.tableExists(conn, schema, RUNNING_PROCESSES_TABLE))
            {
                List<string[]> columns = new List<string[]>();
                columns.Add(new string[] { "SPACE_ID", conn.getUniqueIdentiferType() });
                columns.Add(new string[] { "SERVER_IP", conn.getVarcharType(50, false) });
                columns.Add(new string[] { "SERVER_PORT", conn.getVarcharType(10, false) });
                columns.Add(new string[] { "TYPE", conn.getVarcharType(100, false) });
                columns.Add(new string[] { "Source", conn.getVarcharType(100, false) });
                columns.Add(new string[] { "PID", conn.getVarcharType(100, false) });
                columns.Add(new string[] { "Info", conn.getVarcharType(256, false) });
                columns.Add(new string[] { "CreatedDate", conn.getDateTimeType() });
                Database.createTable(conn, schema, RUNNING_PROCESSES_TABLE, columns, false);
            }
        }

        public static void createDeliveryOpsTable(QueryConnection conn, string schema)
        {
            if (!Database.tableExists(conn, schema, DELIVERY_OPS_TABLE))
            {
                List<string[]> columns = new List<string[]>();
                columns.Add(new string[] { "ID", conn.getUniqueIdentiferType() });
                columns.Add(new string[] { "SPACE_ID", conn.getUniqueIdentiferType() });
                columns.Add(new string[] { "PATH", conn.getVarcharType(-1, true) });
                columns.Add(new string[] { "TYPE", conn.getVarcharType(-1, true) });
                columns.Add(new string[] { "DONE", conn.getVarcharType(-1, true) });
                Database.createTable(conn, schema, DELIVERY_OPS_TABLE, columns, false);
            }
        }
        public static void createSpaceOperationsTable(QueryConnection conn, string schema)
        {
            if (!tableExists(conn, schema, SPACE_OPS_TABLE))
            {
                List<string[]> columns = new List<string[]>();
                columns.Add(new string[] { "ID", "Int" });
                columns.Add(new string[] { "Name", conn.getVarcharType(256, false) });
                columns.Add(new string[] { "CreatedDate", conn.getDateTimeType() });
                columns.Add(new string[] { "ModifiedDate", conn.getDateTimeType() });
                createTable(conn, schema, SPACE_OPS_TABLE, columns, false);
                createIndex(conn, schema, SPACE_OPS_TABLE, "IDX1", new string[] { "ID" });
            }
        }

        public static void createSpacesConfigTable(QueryConnection conn, string schema)
        {
            if (!tableExists(conn, schema, SPACES_CONFIG_TABLE))
            {
                List<string[]> columns = new List<string[]>();
                columns.Add(new string[] { "Type", "Int" });
                columns.Add(new string[] { "URL", conn.getVarcharType(256, false) });
                columns.Add(new string[] { "LocalURL", conn.getVarcharType(256, false) });
                columns.Add(new string[] { "DatabaseConnectString", conn.getVarcharType(1024, false) });
                columns.Add(new string[] { "DatabaseType", conn.getVarcharType(256, false) });
                columns.Add(new string[] { "DatabaseDriver", conn.getVarcharType(1024, false) });
                columns.Add(new string[] { "DatabaseLoadDir", conn.getVarcharType(1024, false) });
                columns.Add(new string[] { "AdminUser", conn.getVarcharType(64, false) });
                columns.Add(new string[] { "AdminPwd", conn.getVarcharType(128, false) });
                columns.Add(new string[] { "EngineCommand", conn.getVarcharType(1024, false) });
                columns.Add(new string[] { "DeleteDataCommand", conn.getVarcharType(1024, false) });
                columns.Add(new string[] { "QueryDatabaseConnectString", conn.getVarcharType(1024, false) });
                columns.Add(new string[] { "QueryDatabaseType", conn.getVarcharType(256, false) });
                columns.Add(new string[] { "QueryDatabaseDriver", conn.getVarcharType(1024, false) });
                columns.Add(new string[] { "QueryUser", conn.getVarcharType(64, false) });
                columns.Add(new string[] { "QueryPwd", conn.getVarcharType(128, false) });
                columns.Add(new string[] { "QueryConnectionName", conn.getVarcharType(128, false) });
                columns.Add(new string[] { "AWSAccessKeyId", conn.getVarcharType(256, false) });
                columns.Add(new string[] { "AWSSecretKey", conn.getVarcharType(256, false) });
                columns.Add(new string[] { "Name", conn.getNvarcharType(256, false) });
                columns.Add(new string[] { "Description", conn.getNvarcharType(-1, true) });
                columns.Add(new string[] { "DBTimeZone", conn.getVarcharType(255, false) });
                columns.Add(new string[] { "RegionID", "INT" });
                createTable(conn, schema, SPACES_CONFIG_TABLE, columns, false);
            }
        }

        public static void createMessagesTable(QueryConnection conn, string schema)
        {
            if (!tableExists(conn, schema, SYSTEM_MESSAGES_TABLE))
            {
                List<string[]> columns = new List<string[]>();
                columns.Add(new string[] { "ENTRY_TIME", conn.getDateTimeType() });
                columns.Add(new string[] { "EXPIRATION_TIME", conn.getDateTimeType() });
                columns.Add(new string[] { "ACCOUNT_ID", conn.getUniqueIdentiferType() });
                columns.Add(new string[] { "MESSAGE", conn.getVarcharType(2000, false) });
                createTable(conn, schema, SYSTEM_MESSAGES_TABLE, columns, false);
            }
        }

        public static void createOpenIdUsersTable(QueryConnection conn, string schema)
        {
            if (!tableExists(conn, schema, OPENID_USER_TABLE))
            {
                List<string[]> columns = new List<string[]>();
                columns.Add(new string[] { "OPENID", conn.getVarcharType(256, false) });
                columns.Add(new string[] { "USERID", conn.getUniqueIdentiferType() });
                createTable(conn, schema, OPENID_USER_TABLE, columns, false);
            }
        }

        public static void createSamlInfoTable(QueryConnection conn, string schema)
        {
            if (!tableExists(conn, schema, SAML_IDP_TABLE))
            {
                List<string[]> columns = new List<string[]>();
                columns.Add(new string[] { "ID", conn.getUniqueIdentiferType() });
                columns.Add(new string[] { "IDPNAME", conn.getVarcharType(256, false) });
                columns.Add(new string[] { "IDPISSUERID", conn.getVarcharType(256, false) });
                columns.Add(new string[] { "CERTIFICATE", conn.getVarcharType(4056, true) });
                columns.Add(new string[] { "TIMEOUT", "INT" });
                columns.Add(new string[] { "LOGOUTPAGE", conn.getVarcharType(1024, true) });
                columns.Add(new string[] { "ERRORPAGE", conn.getVarcharType(1024, true) });
                columns.Add(new string[] { "ACTIVE", conn.getBooleanType("ACTIVE") });
                columns.Add(new string[] { "IDPURL", conn.getVarcharType(1024, true) });
                columns.Add(new string[] { "SIGNIDPREQUEST", conn.getBooleanType("SIGNIDPREQUEST") });
                columns.Add(new string[] { "IDPBINDING", conn.getVarcharType(1024, true) });
                columns.Add(new string[] { "SPINITIATED", conn.getBooleanType("SPINITIATED") });
                columns.Add(new string[] { "ACCOUNTID", conn.getUniqueIdentiferType() });
                createTable(conn, schema, SAML_IDP_TABLE, columns, false);
            }
        }

        public static void createSubDomainInfoTable(QueryConnection conn, string schema)
        {
            if (!tableExists(conn, schema, SUB_DOMAIN_REGISTER_TABLE))
            {
                List<string[]> columns = new List<string[]>();
                columns.Add(new string[] { "ID", conn.getUniqueIdentiferType() });
                columns.Add(new string[] { "DOMAIN", conn.getVarcharType(1024, true) });
                columns.Add(new string[] { "SAMLIDPNAME", conn.getVarcharType(256, false) });
                columns.Add(new string[] { "AVAILABLE", conn.getBooleanType("AVAILABLE") });
                createTable(conn, schema, SUB_DOMAIN_REGISTER_TABLE, columns, false);
            }
        }


        public static void createServerInfoTable(QueryConnection conn, string schema)
        {
            if (!tableExists(conn, schema, SERVER_INFO_TABLE))
            {
                List<string[]> columns = new List<string[]>();
                columns.Add(new string[] { "SERVERID", conn.getVarcharType(1024, false) + " PRIMARY KEY" });
                columns.Add(new string[] { "DESCRIPTION", conn.getVarcharType(1024, false) });
                columns.Add(new string[] { "ACORNLOG", conn.getVarcharType(1024, false) });
                columns.Add(new string[] { "SMIWEBLOG", conn.getVarcharType(1024, false) });
                columns.Add(new string[] { "SCHEDULERWEBLOG", conn.getVarcharType(1024, false) });
                columns.Add(new string[] { "CONNECTORWEBLOG", conn.getVarcharType(1024, false) });
                createTable(conn, schema, SERVER_INFO_TABLE, columns, false);
            }
        }

        public static void createAppLogsInfoTable(QueryConnection conn, string schema)
        {
            if (!tableExists(conn, schema, APP_LOGS_INFO_TABLE))
            {
                List<string[]> columns = new List<string[]>();
                columns.Add(new string[] { "ID", "INT PRIMARY KEY" });
                columns.Add(new string[] { "NAME", conn.getVarcharType(1024, false) });
                columns.Add(new string[] { "DESCRIPTION", conn.getVarcharType(1024, false) });
                columns.Add(new string[] { "LOGDIR", conn.getVarcharType(1024, false) });
                columns.Add(new string[] { "LOGPREFIX", conn.getVarcharType(1024, false) });                
                createTable(conn, schema, APP_LOGS_INFO_TABLE, columns, false);
            }
        }

        public static void createPasswordPolicyTable(QueryConnection conn, string schema)
        {
            if (!tableExists(conn, schema, Database.PASSWORD_POLICY_TABLE))
            {
                List<string[]> columns = new List<string[]>();
                columns.Add(new string[] { "POLICYID", conn.getUniqueIdentiferType() + " PRIMARY KEY" });
                columns.Add(new string[] { "ACCOUNTID", conn.getUniqueIdentiferType() });
                columns.Add(new string[] { "DESCRIPTION", conn.getVarcharType(1024, false) });
                columns.Add(new string[] { "REGULAREXPRESSION", conn.getVarcharType(1024, false) });
                columns.Add(new string[] { "MINLENGTH", "INT" });
                columns.Add(new string[] { "MAXLENGTH", "INT" });
                columns.Add(new string[] { "CONTAINSMIXEDCASE", conn.getBooleanType("CONTAINSMIXEDCASE") });
                columns.Add(new string[] { "CONTAINSSPECIAL", conn.getBooleanType("CONTAINSSPECIAL") });
                columns.Add(new string[] { "CONTAINSNUMERIC", conn.getBooleanType("CONTAINSNUMERIC") });
                columns.Add(new string[] { "DOESNOTCONTAINUSERNAME", conn.getBooleanType("DOESNOTCONTAINUSERNAME") });
                columns.Add(new string[] { "CHANGEONFIRSTUSE", conn.getBooleanType("CHANGEONFIRSTUSE") });
                columns.Add(new string[] { "HISTORYLENGTH", "INT" });
                columns.Add(new string[] { "EXPIRATIONDAYS", "INT" });
                columns.Add(new string[] { "HASHALGORITHM", conn.getVarcharType(255, false) });
                columns.Add(new string[] { "MAXFAILEDLOGINATTEMPTS", "INT" });
                columns.Add(new string[] { "FAILEDLOGINWINDOW", "INT" });
                createTable(conn, schema, Database.PASSWORD_POLICY_TABLE, columns, false);
            }
        }

        public static void createPasswordHistoryTable(QueryConnection conn, string schema)
        {
            if (!tableExists(conn, schema, Database.PASSWORD_HISTORY_TABLE))
            {
                List<string[]> columns = new List<string[]>();
                columns.Add(new string[] { "HISTORYID", conn.getUniqueIdentiferType() + " PRIMARY KEY" });
                columns.Add(new string[] { "USERID", conn.getUniqueIdentiferType() });
                columns.Add(new string[] { "HASHEDPASSWORD", conn.getVarcharType(1024, false) });
                columns.Add(new string[] { "CHANGEDATE", conn.getDateTimeType() });
                createTable(conn, schema, Database.PASSWORD_HISTORY_TABLE, columns, false);
                createIndex(conn, schema, Database.PASSWORD_HISTORY_TABLE, "IDX1", new string[] { "USERID" });
            }
        }

        public static void createUserPreferenceTable(QueryConnection conn,
                                                     string schema,
                                                     ref List<string> schemaTable,
                                                     bool dropTableFirst)
        {
            if (dropTableFirst)
            {
                dropTable(conn, schema, PREFERENCES_TABLE);
            }
            if (!tableExists(conn, schema, PREFERENCES_TABLE, ref schemaTable))
            {
                List<string[]> columns = new List<string[]>();
                columns.Add(new string[] { "PreferenceType", "int NOT NULL" });
                columns.Add(new string[] { "USER_ID", conn.getUniqueIdentiferType() + " NOT NULL" });
                columns.Add(new string[] { "SPACE_ID", conn.getUniqueIdentiferType() + " NOT NULL" });
                columns.Add(new string[] { "PreferenceKey", conn.getVarcharType(255, false) + " NOT NULL" });
                columns.Add(new string[] { "PreferenceValue", conn.getVarcharType(8000, false) });
                createTable(conn, schema, PREFERENCES_TABLE, columns, false);
                createIndex(conn, schema, PREFERENCES_TABLE, "IDX1", new string[] { "USER_ID" });
            }
        }

        public static void createUploadTokensTable(QueryConnection conn, string schema, ref List<string> schemaTable)
        {
            if (!tableExists(conn, schema, UPLOAD_TOKENS_TABLE, ref schemaTable))
            {
                string maxVarcharType = conn.getVarcharType(-1, true);
                string uniqueIdentifierType = conn.getUniqueIdentiferType();
                string datetimeType = conn.getDateTimeType();

                List<string[]> columns = new List<string[]>();
                columns.Add(new string[] { "TOKEN_ID", uniqueIdentifierType + " NOT NULL PRIMARY KEY" });
                columns.Add(new string[] { "End_Of_Life", datetimeType });
                columns.Add(new string[] { "USER_ID", uniqueIdentifierType });
                columns.Add(new string[] { "SPACE_ID", uniqueIdentifierType });
                columns.Add(new string[] { "Parameters", maxVarcharType });
                columns.Add(new string[] { "Used", conn.getBooleanType("Used") });
                columns.Add(new string[] { "TYPE", conn.getVarcharType(32, false) });
                columns.Add(new string[] { "HOSTADDR", conn.getVarcharType(64, false) });
                createTable(conn, schema, UPLOAD_TOKENS_TABLE, columns, false);
                if (!conn.isDBTypeOracle())
                {
                    createIndex(conn, schema, UPLOAD_TOKENS_TABLE, "IDX1", new string[] { "TOKEN_ID" });
                }
            }
        }

        public static void setupNewUser(QueryConnection conn, string schema, string username, string email, List<string> productIDs, DateTime endDate)
        {
            Database.setupNewUser(conn, mainSchema, username, null, null,
                        username, null, null, null, null, null, null, null,
                        false, 0, null, null, null, null,
                        productIDs, endDate);
        }

        public static void setupNewUser(QueryConnection conn, string schema, string username,
            string firstname, string lastname, string email, string title, string company, string phone,
            string city, string state, string zip, string country, bool stepbystep, int contactStatus,
            string department, string industry, string orgsize, string likelyusage, List<string> productIDs, DateTime endDate)
        {
            Dictionary<string, DateTime> productIDEndDateMapping = new Dictionary<string, DateTime>();
            if (productIDs != null && productIDs.Count > 0)
            {
                foreach (string prodId in productIDs)
                {
                    productIDEndDateMapping.Add(prodId, endDate);
                }
            }

            setupNewUser(conn, schema, username, firstname, lastname, email, title, company, phone,
                city, state, zip, country, stepbystep, contactStatus, department, industry, orgsize, likelyusage, productIDEndDateMapping);
        }


        public static void setupNewUser(QueryConnection conn, string schema, string username,
            string firstname, string lastname, string email, string title, string company, string phone,
            string city, string state, string zip, string country, bool stepbystep, int contactStatus,
            string department, string industry, string orgsize, string likelyusage, Dictionary<string, DateTime> productIDEndDateMapping)
        {
            bool isOracle = conn.isDBTypeOracle();
            QueryCommand cmd = conn.CreateCommand();
            cmd.CommandText = "UPDATE " + schema + "." + USER_TABLE_NAME +
                " SET NumShares=?," +
                "FirstName=?,LastName=?,Email=?,Title=?,Company=?,Phone=?," +
                "City=?,State=?,Zip=?,Country=?,StepByStep=?,ContactStatus=?," +
                "Department=?,Industry=?,OrgSize=?,LikelyUse=?,RepositoryAdmin=0,CreateNewUserRights=0,Operations=0"
                + " WHERE Username=?";
            cmd.Parameters.Add("@NumShares", OdbcType.Int).Value = 0;
            DbParameter fname = cmd.Parameters.Add("@FirstName", OdbcType.VarChar);
            if (firstname != null)
                fname.Value = firstname.Substring(0, Math.Min(60, firstname.Length));
            else
                fname.Value = System.DBNull.Value;
            DbParameter lname = cmd.Parameters.Add("@LastName", OdbcType.VarChar);
            if (lastname != null)
                lname.Value = lastname.Substring(0, Math.Min(60, lastname.Length));
            else
                lname.Value = System.DBNull.Value;
            DbParameter eparm = cmd.Parameters.Add("@Email", OdbcType.VarChar);
            if (email != null)
                eparm.Value = email.Substring(0, Math.Min(60, email.Length));
            else
                eparm.Value = System.DBNull.Value;
            DbParameter tparm = cmd.Parameters.Add("@Title", OdbcType.VarChar);
            if (title != null)
                tparm.Value = title.Substring(0, Math.Min(60, title.Length));
            else
                tparm.Value = System.DBNull.Value;
            DbParameter comp = cmd.Parameters.Add("@Company", OdbcType.VarChar);
            if (company != null)
                comp.Value = company.Substring(0, Math.Min(60, company.Length));
            else
                comp.Value = System.DBNull.Value;
            DbParameter ph = cmd.Parameters.Add("@Phone", OdbcType.VarChar);
            if (phone != null)
                ph.Value = phone.Substring(0, Math.Min(20, phone.Length));
            else
                ph.Value = System.DBNull.Value;
            DbParameter pcity = cmd.Parameters.Add("@City", OdbcType.VarChar);
            if (city != null)
                pcity.Value = city.Substring(0, Math.Min(60, city.Length));
            else
                pcity.Value = System.DBNull.Value;
            DbParameter pstate = cmd.Parameters.Add("@State", OdbcType.VarChar);
            if (state != null)
                pstate.Value = state.Substring(0, Math.Min(60, state.Length));
            else
                pstate.Value = System.DBNull.Value;
            DbParameter pzip = cmd.Parameters.Add("@Zip", OdbcType.VarChar);
            if (zip != null)
                pzip.Value = zip.Substring(0, Math.Min(60, zip.Length));
            else
                pzip.Value = System.DBNull.Value;
            DbParameter pcountry = cmd.Parameters.Add("@Country", OdbcType.VarChar);
            if (country != null)
                pcountry.Value = country.Substring(0, Math.Min(60, country.Length));
            else
                pcountry.Value = System.DBNull.Value;
            cmd.Parameters.Add("@StepByStep", conn.getODBCBooleanType()).Value = conn.getBooleanValue(stepbystep);
            cmd.Parameters.Add("@ContactStatus", OdbcType.Int).Value = contactStatus;
            DbParameter pdept = cmd.Parameters.Add("@Department", OdbcType.VarChar);
            if (department != null)
                pdept.Value = department;
            else
                pdept.Value = System.DBNull.Value;
            DbParameter pind = cmd.Parameters.Add("@Industry", OdbcType.VarChar);
            if (industry != null)
                pind.Value = industry;
            else
                pind.Value = System.DBNull.Value;
            DbParameter porg = cmd.Parameters.Add("@OrgSize", OdbcType.VarChar);
            if (orgsize != null)
                porg.Value = orgsize;
            else
                porg.Value = System.DBNull.Value;
            DbParameter plikely = cmd.Parameters.Add("@LikelyUse", OdbcType.VarChar);
            if (likelyusage != null)
                plikely.Value = likelyusage;
            else
                plikely.Value = System.DBNull.Value;
            cmd.Parameters.Add("@Username", OdbcType.NVarChar).Value = username;
            int count = cmd.ExecuteNonQuery();
            if (count == 0)
            {
                throw new Exception("Trying to setup a new user that does not exist - " + username);
            }
            cmd.Parameters.Clear();
            cmd.CommandText = "SELECT PKID FROM " + schema + "." + USER_TABLE_NAME + " WHERE Username=?";
            cmd.Parameters.Add("Username", OdbcType.NVarChar).Value = username;
            Object obj = cmd.ExecuteScalar();
            if (obj == null)
            {
                throw new Exception("Trying to setup a new user that does not exist - " + username);
            }
            Guid id = isOracle ? new Guid((string)obj) : (Guid)obj;

            if (productIDEndDateMapping != null && productIDEndDateMapping.Count > 0)
            {
                cmd.CommandText = "INSERT INTO " + schema + "." + USER_PRODUCT_TABLE + " (USER_ID,PRODUCT_ID,StartDate,EndDate,Quantity,Active) VALUES (?,?,?,?,?,1)";
                cmd.Parameters.Clear();
                cmd.Parameters.Add("@UserID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(id);
                DbParameter param = cmd.Parameters.Add("@ProductID", OdbcType.Int);
                cmd.Parameters.Add("@Startdate", OdbcType.DateTime).Value = truncateDateTime(DateTime.Now);
                DbParameter endParam = cmd.Parameters.Add("@EndDate", OdbcType.DateTime);

                cmd.Parameters.Add("@Quantity", OdbcType.Int).Value = 1;
                foreach (KeyValuePair<string, DateTime> prod in productIDEndDateMapping)
                {
                    param.Value = prod.Key;
                    DateTime endDate = prod.Value;
                    if (endDate != null && endDate > DateTime.Now)
                    {
                        endParam.Value = truncateDateTime(endDate);
                    }
                    else
                    {
                        endParam.Value = System.DBNull.Value;
                    }

                    cmd.ExecuteNonQuery();
                }
            }
            cmd.Dispose();
        }

        public static User getUser(QueryConnection conn, string schema, string username)
        {
            return (getUser(conn, schema, username, Guid.Empty, null));
        }

        public static User getUser(QueryConnection conn, string schema, string username, bool filterActiveProducts)
        {
            return (getUser(conn, schema, username, Guid.Empty, null, filterActiveProducts));
        }

        public static User getUserByEmail(QueryConnection conn, string schema, string email)
        {
            return (getUser(conn, schema, null, Guid.Empty, email));
        }

        public static User getUserById(QueryConnection conn, string schema, Guid id)
        {
            return (getUser(conn, schema, null, id, null));
        }

        public static User getUser(QueryConnection conn, string schema, string username, Guid userID, string email)
        {
            return (getUser(conn, schema, username, userID, email, true));
        }

        public static User getUser(QueryConnection conn, string schema, string username, Guid userID, string email, bool filterInactiveProducts)
        {
            return getUser(conn, schema, username, userID, email, filterInactiveProducts, true);
        }

        public static User getUser(QueryConnection conn, string schema, string username, Guid userID, string email, bool filterInactiveProducts, bool fetchAcls)
        {
            string userProductWhereClause = null;
            if (filterInactiveProducts)
            {
                // Only get user if it has 1 or more active products in user_product_table , default behavior             
                userProductWhereClause = "WHERE (A.EndDate IS NULL OR A.EndDate > " + conn.getCurrentTimestamp() + ") AND A.Active=1";
            }
            else
            {
                // Get user regarding of the product status in user_product_table
                // This can be used e.g. during user management activites to retrieve "Disabled" users
                userProductWhereClause = "";
            }

            QueryCommand cmd = null;
            QueryReader reader = null;
            User u = new User();
            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText =
                    "SELECT PKID,B.AccountType,B.RowLimit,B.DataLimit,B.ShareLimit,NumShares,Username,FirstName,LastName,Email," +  // 0-9
                    "Title,Company,Phone,City,State,Zip,Country,StepByStep,ContactStatus,B.MaxAllowableData," +                     // 10-19
                    "B.MaxAllowableRows,B.MaxPublishData,B.MaxPublishRows,B.MaxPublishDataPerDay,B.MaxPublishRowsPerDay," +         // 20-24
                    "B.MaxScriptStatements,B.MaxInputRows,B.MaxOutputRows,B.ScriptQueryTimeout,B.MaxDeliveredReportsPerDay," +      // 25-29
                    "A.ConnectString,A.DatabaseType,A.DatabaseDriver,A.DatabaseLoadDir,A.AdminUser," +                              // 30-34
                    "A.AdminPWD,A.QueryConnectString,A.QueryDatabaseType,A.QueryDatabaseDriver,A.QueryConnectionName," +            // 35-39
                    "A.QueryUser,A.QueryPwd,A.Password,A.RepositoryAdmin,A.DefaultSpace,A.DefaultSpaceId," +                        // 40-44
                    "A.DefaultDashboards,A.LOGO_IMAGE_ID,A.HeaderBackground,A.PartnerCode,A.CreateNewUserRights," +                 // 45-49
                    "A.ADMIN_ACCOUNT_ID, A.MANAGED_ACCOUNT_ID, A.Operations, A.ReleaseType, A.Disabled, A.HTMLInterface, A.SpaceType, A.ExternalDBEnabled, A.DenyAddSpace FROM " +                     // 50+
                    schema + "." + USER_TABLE_NAME +
                    " A INNER JOIN (SELECT USER_ID,MAX(AccountType) AS AccountType,MAX(RowLimit) AS RowLimit,MAX(DataLimit) AS DataLimit,SUM(ShareLimit*Quantity) AS ShareLimit,0 AS PricePerMonth," +
                    "MAX(MaxAllowableData) AS MaxAllowableData,MAX(MaxAllowableRows) AS MaxAllowableRows,MAX(MaxPublishData) AS MaxPublishData,MAX(MaxPublishRows) AS MaxPublishRows,MAX(MaxPublishDataPerDay) AS MaxPublishDataPerDay,MAX(MaxPublishRowsPerDay) AS MaxPublishRowsPerDay,MAX(MaxScriptStatements) AS MaxScriptStatements,MAX(MaxInputRows) AS MaxInputRows,MAX(MaxOutputRows) AS MaxOutputRows,MAX(ScriptQueryTimeout) AS ScriptQueryTimeout,MAX(MaxDeliveredReportsPerDay) AS MaxDeliveredReportsPerDay FROM "
                    + schema + "." + USER_PRODUCT_TABLE + " A INNER JOIN " + schema + "." + PRODUCTS_TABLE + " B ON A.PRODUCT_ID=B.PRODUCT_ID " + userProductWhereClause + " GROUP BY USER_ID) B ON A.PKID=B.USER_ID";

                if (username != null)
                {
                    cmd.CommandText += " WHERE Username=?";
                    cmd.Parameters.Add("USER_ID", OdbcType.NVarChar).Value = username;
                }
                else if (email != null)
                {
                    cmd.CommandText += " WHERE Email=?";
                    cmd.Parameters.Add("Email", OdbcType.VarChar).Value = email;
                }
                else
                {
                    cmd.CommandText += " WHERE PKID=?";
                    cmd.Parameters.Add("PKID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(userID);
                }
                reader = cmd.ExecuteReader();
                if (!reader.HasRows)
                    return null;
                u.Username = username;

                reader.Read();

                u.ID = reader.GetGuid(0);
                u.AccountType = reader.GetInt32(1);
                u.ShareLimit = reader.IsDBNull(4) ? Int32.MaxValue : reader.GetInt32(4);
                u.NumShares = reader.IsDBNull(5) ? 0 : reader.GetInt32(5);
                u.Username = reader.GetString(6);
                u.FirstName = reader.IsDBNull(7) ? null : reader.GetString(7);
                u.LastName = reader.IsDBNull(8) ? null : reader.GetString(8);
                u.Email = reader.IsDBNull(9) ? null : reader.GetString(9);
                u.Title = reader.IsDBNull(10) ? null : reader.GetString(10);
                u.Company = reader.IsDBNull(11) ? null : reader.GetString(11);
                u.Phone = reader.IsDBNull(12) ? null : reader.GetString(12);
                u.City = reader.IsDBNull(13) ? null : reader.GetString(13);
                u.State = reader.IsDBNull(14) ? null : reader.GetString(14);
                u.Zip = reader.IsDBNull(15) ? null : reader.GetString(15);
                u.Country = reader.IsDBNull(16) ? null : reader.GetString(16);
                u.StepByStep = reader.IsDBNull(17) ? false : reader.GetBoolean(17);
                u.ContactStatus = reader.IsDBNull(18) ? 0 : reader.GetInt32(18);
                if (!reader.IsDBNull(25))
                    u.MaxScriptStatements = reader.GetInt64(25);
                if (!reader.IsDBNull(26))
                    u.MaxInputRows = reader.GetInt32(26);
                if (!reader.IsDBNull(27))
                    u.MaxOutputRows = reader.GetInt32(27);
                if (!reader.IsDBNull(28))
                    u.ScriptQueryTimeout = reader.GetInt32(28);
                if (!reader.IsDBNull(29))
                    u.MaxDeliveredReportsPerDay = reader.GetInt32(29);

                if (!reader.IsDBNull(30))
                    u.DatabaseConnectString = reader.GetString(30);
                if (!reader.IsDBNull(31))
                    u.DatabaseType = reader.GetString(31);
                if (!reader.IsDBNull(32))
                    u.DatabaseDriver = reader.GetString(32);
                if (!reader.IsDBNull(33))
                    u.DatabaseLoadDir = reader.GetString(33);
                if (!reader.IsDBNull(34))
                    u.AdminUser = reader.GetString(34);
                if (!reader.IsDBNull(35))
                    u.AdminPwd = Performance_Optimizer_Administration.MainAdminForm.decrypt(reader.GetString(35),
                    Performance_Optimizer_Administration.MainAdminForm.SecretPassword);

                u.QueryDatabaseConnectString = reader.IsDBNull(36) ? null : reader.GetString(36);
                u.QueryDatabaseType = reader.IsDBNull(37) ? null : reader.GetString(37);
                u.QueryDatabaseDriver = reader.IsDBNull(38) ? null : reader.GetString(38);
                u.QueryConnectionName = reader.IsDBNull(39) ? null : reader.GetString(39);
                u.QueryUser = reader.IsDBNull(40) ? null : reader.GetString(40);
                u.QueryPwd = reader.IsDBNull(41) ? null : Performance_Optimizer_Administration.MainAdminForm.decrypt(reader.GetString(41),
                Performance_Optimizer_Administration.MainAdminForm.SecretPassword);

                if (!reader.IsDBNull(42))
                    u.Password = reader.GetString(42);
                if (!reader.IsDBNull(43))
                    u.RepositoryAdmin = reader.GetBoolean(43);
                if (!reader.IsDBNull(44))
                    u.DefaultSpace = reader.GetString(44);
                if (!reader.IsDBNull(45))
                    u.DefaultSpaceId = reader.GetGuid(45);
                if (!reader.IsDBNull(46))
                    u.DefaultDashboards = reader.GetBoolean(46);
                if (!reader.IsDBNull(47))
                    u.LogoImageID = reader.GetGuid(47);
                if (!reader.IsDBNull(48))
                    u.HeaderBackground = reader.GetInt64(48);
                if (!reader.IsDBNull(49))
                    u.PartnerCode = reader.GetString(49);
                if (!reader.IsDBNull(50))
                    u.CreateNewUserRights = reader.GetBoolean(50);
                if (!reader.IsDBNull(51))
                {
                    u.AdminAccountId = reader.GetGuid(51);
                }
                if (!reader.IsDBNull(52))
                {
                    u.ManagedAccountId = reader.GetGuid(52);
                }
                if (!reader.IsDBNull(53))
                {
                    u.OperationsFlag = reader.GetBoolean(53);
                }
                if (!reader.IsDBNull(54))
                {
                    u.ReleaseType = reader.GetInt32(54);
                }
                if (!reader.IsDBNull(55))
                {
                    u.Disabled = reader.GetBoolean(55);
                }
                if (reader.IsDBNull(56))
                {
                    u.HTMLInterface = false;
                }
                else
                {
                    u.HTMLInterface = reader.GetBoolean(56);
                }
                if (reader.IsDBNull(57))
                {
                    u.SpaceType = -1;
                }
                else
                {
                    u.SpaceType = reader.GetInt32(57);
                }

                if (reader.IsDBNull(58))
                {
                    u.ExternalDBEnabled = false;
                }
                else
                {
                    u.ExternalDBEnabled = reader.GetBoolean(58);
                }
                if (reader.IsDBNull(59))
                {
                    u.DenyAddSpace = false;
                }
                else
                {
                    u.DenyAddSpace = reader.GetBoolean(59);
                }
            }
            finally
            {
                if (reader != null)
                    reader.Close();
                if (cmd != null)
                    cmd.Dispose();
            }

            //set user ACLs
            if (fetchAcls)
            {
                u.UserACLs = getACLsForUser(conn, schema, u);
                if (u.ID != null)
                {
                    if (u.ManagedAccountId != null && u.ManagedAccountId != Guid.Empty)
                    {
                        u.actDetails = getAccountDetails(conn, schema, u.ManagedAccountId, u);
                    }
                }
            }
            return u;
        }

        /**
         * get the account override parameters
         */
        private static Dictionary<string, string> getOverrideParameters(QueryConnection conn, string schema, Guid accountID)
        {
            QueryCommand cmd = conn.CreateCommand();
            QueryReader reader = null;
            Dictionary<string, string> overrideParameters = new Dictionary<string, string>();
            try
            {
                cmd.CommandText =
                    "SELECT B.PARAMETER_NAME, B.PARAMETER_VALUE FROM " +
                    schema + "." + ACCOUNTS_TABLE +
                    " A INNER JOIN " + schema + "." + ACCOUNT_OVERRIDE_PARAMETERS_TABLE + " B ON B.ACCOUNT_ID=A.ACCOUNT_ID";
                cmd.CommandText += " WHERE A.ACCOUNT_ID=?";
                cmd.Parameters.Add("ACCOUNT_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(accountID);
                reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        string name = reader.GetString(0);
                        string value = reader.GetString(1);
                        if (name != null && value != null)
                        {
                            overrideParameters.Add(name, value);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Exception thrown while getting override parameters", ex);
            }
            finally
            {
                try
                {
                    if (reader != null)
                    {
                        reader.Close();
                    }
                    if (cmd != null)
                    {
                        cmd.Dispose();
                    }
                }
                catch (Exception)
                {
                    //Global.systemLog.Warn("Cannot close cmd/result object. Let system handle", ex2);
                }
            }
            return overrideParameters;
        }



        public static Dictionary<string, string> getOverrideParametersLogo(QueryConnection conn, string schema, Guid accountID)
        {
            Dictionary<string, string> overrideParameters = new Dictionary<string, string>();
            try
            {

                {
                    QueryCommand cmd = conn.CreateCommand();
                    QueryReader reader = null;
                    cmd.CommandText = "SELECT PARAMETER_NAME, PARAMETER_VALUE FROM " + schema + "." + ACCOUNT_OVERRIDE_PARAMETERS_TABLE;
                    cmd.CommandText += " WHERE ACCOUNT_ID=?";
                    cmd.Parameters.Add("ACCOUNT_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(Guid.Empty);
                    reader = cmd.ExecuteReader();
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            string name = null;
                            string value = null;

                            if (!reader.IsDBNull(0))
                                name = reader.GetString(0);
                            else
                                name = string.Empty;

                            if (!reader.IsDBNull(1))
                                value = reader.GetString(01);
                            else
                                value = string.Empty;

                            if (name != null && value != null)
                            {

                                if (name.Equals(OverrideParameterNames.LOGO_PATH))
                                {
                                    if (!overrideParameters.ContainsKey(OverrideParameterNames.LOGO_PATH))
                                    {
                                        overrideParameters.Add(OverrideParameterNames.LOGO_PATH, value);
                                    }
                                }

                                if (name.Equals(OverrideParameterNames.FAVICON_ICON))
                                {
                                    if (!overrideParameters.ContainsKey(OverrideParameterNames.FAVICON_ICON))
                                    {
                                        overrideParameters.Add(OverrideParameterNames.FAVICON_ICON, value);
                                    }
                                }



                            }
                        }
                    }

                    try
                    {
                        if (reader != null)
                        {
                            reader.Close();
                        }
                        if (cmd != null)
                        {
                            cmd.Dispose();
                        }
                    }
                    catch (Exception)
                    {
                        //Global.systemLog.Warn("Cannot close cmd/result object. Let system handle", ex2);
                    }

                }

                if (accountID != Guid.Empty && accountID != null)
                {
                    QueryCommand cmd = conn.CreateCommand();
                    QueryReader reader = null;
                    cmd.CommandText =
                    "SELECT B.PARAMETER_NAME, B.PARAMETER_VALUE FROM " + schema + "." + ACCOUNTS_TABLE +
                    " A INNER JOIN " + schema + "." + ACCOUNT_OVERRIDE_PARAMETERS_TABLE + " B ON B.ACCOUNT_ID=A.ACCOUNT_ID";
                    cmd.CommandText += " WHERE A.ACCOUNT_ID=?";
                    cmd.Parameters.Add("ACCOUNT_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(accountID);
                    reader = cmd.ExecuteReader();
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            string name = null;
                            string value = null;

                            if (!reader.IsDBNull(0))
                                name = reader.GetString(0);
                            else
                                name = string.Empty;

                            if (!reader.IsDBNull(1))
                                value = reader.GetString(01);
                            else
                                value = string.Empty;

                            if (name != null && value != null)
                            {
                                if (name.Equals(OverrideParameterNames.LOGO_PATH))
                                {
                                    if (overrideParameters.ContainsKey(OverrideParameterNames.LOGO_PATH))
                                    {
                                        overrideParameters[OverrideParameterNames.LOGO_PATH] = value;
                                    }
                                    else
                                    {
                                        overrideParameters.Add(OverrideParameterNames.LOGO_PATH, value);
                                    }
                                }

                                if (name.Equals(OverrideParameterNames.FAVICON_ICON))
                                {
                                    if (overrideParameters.ContainsKey(OverrideParameterNames.FAVICON_ICON))
                                    {
                                        overrideParameters[OverrideParameterNames.FAVICON_ICON] = value;
                                    }
                                    else
                                    {
                                        overrideParameters.Add(OverrideParameterNames.FAVICON_ICON, value);
                                    }
                                }
                            }
                        }
                        try
                        {
                            if (reader != null)
                            {
                                reader.Close();
                            }
                            if (cmd != null)
                            {
                                cmd.Dispose();
                            }
                        }
                        catch (Exception)
                        {
                            //Global.systemLog.Warn("Cannot close cmd/result object. Let system handle", ex2);
                        }

                    }

                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Exception thrown while getting override parameters logo", ex);
            }


            return overrideParameters;
        }

        /**
         * set the value of an account override parameter
         * - delete if the value is 'null'
         * - replace if the key exists
         */
        public static void setOverrideParameter(QueryConnection conn, string schema, Guid accountID, string key, string value)
        {
            QueryCommand cmd = conn.CreateCommand();
            try
            {
                // delete first
                cmd.CommandText = "DELETE FROM " + schema + "." + ACCOUNT_OVERRIDE_PARAMETERS_TABLE + " WHERE ACCOUNT_ID=? AND PARAMETER_NAME=?";
                cmd.Parameters.Add("ACCOUNT_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(accountID);
                cmd.Parameters.Add("PARAMETER_NAME", OdbcType.VarChar).Value = key;
                cmd.ExecuteNonQuery();

                if (value != null)
                {
                    // then add
                    cmd.Parameters.Clear();
                    cmd.CommandText = "INSERT INTO " + schema + "." + ACCOUNT_OVERRIDE_PARAMETERS_TABLE + " (ACCOUNT_ID, PARAMETER_NAME, PARAMETER_VALUE) VALUES (?,?,?)";
                    cmd.Parameters.Add("@ACCOUNT_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(accountID);
                    cmd.Parameters.Add("@PARAMETER_NAME", OdbcType.VarChar).Value = key;
                    cmd.Parameters.Add("@PARAMETER_VALUE", OdbcType.VarChar).Value = value;
                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Exception thrown while setting override parameters", ex);
            }
            finally
            {
                try
                {
                    if (cmd != null)
                    {
                        cmd.Dispose();
                    }
                }
                catch (Exception)
                {
                    //Global.systemLog.Warn("Cannot close cmd/result object. Let system handle", ex2);
                }
            }
        }

        public class SpaceUsage
        {
            public bool active;
            public long numRows;
            public long dataSize;
        }

        /*
         * Return usage statistics for a given user
         */
        public static SpaceUsage getTotalSpaceUsage(QueryConnection conn, string schema, User u, bool active)
        {
            QueryCommand cmd = null;
            QueryReader reader = null;
            SpaceUsage su = new SpaceUsage();
            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = "SELECT SUM(CAST(C.NumRows AS " + conn.getBigIntType() + ")),SUM(C.DataSize) FROM " + schema + "." + SPACE_TABLE_NAME +
                    " A INNER JOIN " + schema + "." + USER_SPACE_INXN + " B ON A.ID=B.SPACE_ID" +
                    " INNER JOIN " + schema + "." + PUBLISH_TABLE + " C ON A.ID=C.SPACE_ID" +
                    " WHERE B.USER_ID=? AND B.Owner=1 AND A.Deleted IS NULL AND A.Active=?";
                cmd.Parameters.Add("USER_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(u.ID);
                cmd.Parameters.Add("Active", conn.getODBCBooleanType()).Value = conn.getBooleanValue(active);
                reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    su.active = active;
                    su.numRows = reader.IsDBNull(0) ? 0 : reader.GetInt64(0);
                    su.dataSize = reader.IsDBNull(1) ? 0 : reader.GetInt64(1);
                }
            }
            finally
            {
                if (reader != null)
                    reader.Close();
                if (cmd != null)
                    cmd.Dispose();
            }
            return su;
        }

        /*
         * Return usage statistics for a given user by space
         */
        public static Dictionary<Guid, SpaceUsage> getSpaceUsage(QueryConnection conn, string schema, User u)
        {
            QueryCommand cmd = conn.CreateCommand();
            cmd.CommandText = "SELECT A.ID,A.Active,SUM(CAST(C.NumRows AS " + conn.getBigIntType() + ")),SUM(C.DataSize) FROM " + schema + "." + SPACE_TABLE_NAME +
                " A INNER JOIN " + schema + "." + USER_SPACE_INXN + " B ON A.ID=B.SPACE_ID" +
                " INNER JOIN " + schema + "." + PUBLISH_TABLE + " C ON A.ID=C.SPACE_ID" +
                " WHERE A.Deleted IS NULL AND B.USER_ID=? AND B.Owner=1 GROUP BY A.ID,A.Active";
            cmd.Parameters.Add("USER_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(u.ID);
            Dictionary<Guid, SpaceUsage> result = new Dictionary<Guid, SpaceUsage>();
            QueryReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                Guid id = reader.GetGuid(0);
                SpaceUsage su = new SpaceUsage();
                su.active = reader.GetBoolean(1);
                su.numRows = reader.IsDBNull(2) ? 0 : reader.GetInt64(2);
                su.dataSize = reader.IsDBNull(3) ? 0 : reader.GetInt64(3);
                result.Add(id, su);
            }
            reader.Close();
            cmd.Dispose();
            return (result);
        }

        // update the password in the database, could also update password history here, and do check
        public static bool updateUserPassword(QueryConnection conn, string schema, string uname, string encryptedPassword)
        {
            // check if the password is in password history
            // update the password in the database
            QueryCommand cmd = conn.CreateCommand();
            cmd.CommandText = "UPDATE " + schema + "." + USER_TABLE_NAME +
                                " SET Password = ?, LastPasswordChangedDate = ? " +
                                " WHERE Username = ?";
            cmd.Parameters.Add("@Password", OdbcType.VarChar, 1024).Value = encryptedPassword;
            cmd.Parameters.Add("@LastPasswordChangedDate", OdbcType.DateTime).Value = DateTime.Now;
            cmd.Parameters.Add("@Username", OdbcType.NVarChar, 255).Value = uname.ToLower();
            int rowsAffected = cmd.ExecuteNonQuery();
            if (rowsAffected > 0)
                return true;
            return false;
        }

        public static bool enableUser(QueryConnection conn, string schema, User u, bool enable)
        {
            // old disable (expire all products)
            List<Product> plist = Database.getProducts(conn, schema, u.ID, true);
            if (enable)
                Database.enableProducts(conn, schema, u.ID, plist);
            else
                Database.disableProducts(conn, schema, u.ID, plist);

            // new disable
            QueryCommand cmd = null;
            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = "UPDATE " + schema + "." + USER_TABLE_NAME +
                                    " SET Disabled = ? WHERE Username = ?";
                cmd.Parameters.Add("@Disabled", conn.getODBCBooleanType()).Value = conn.getBooleanValue(!enable);
                cmd.Parameters.Add("@Username", OdbcType.NVarChar, 255).Value = u.Username.ToLower();
                int rowsAffected = cmd.ExecuteNonQuery();
                if (rowsAffected > 0)
                    return true;
                return false;
            }
            finally
            {
                if (cmd != null)
                    cmd.Dispose();
            }
        }

        public static bool deleteUser(QueryConnection conn, string schema, User u)
        {
            // delete from all tables the reference the user
            QueryCommand cmd = null;
            try
            {
                cmd = conn.CreateCommand();
                string[] tables = { Database.USER_SPACE_INXN, Database.USER_PRODUCT_TABLE, Database.RATINGS_TABLE_NAME, Database.SCHEDULED_REPORTS_TABLE,  Database.ALLOWED_USER_DOMAINS_TABLE, Database.ALLOWED_IPS,
                                  Database.CREATED_USERS_TABLE, Database.PROXY_USERS, Database.GROUP_USER_TABLE, Database.CURRENT_LOGINS_TABLE, Database.SSO_TOKENS_TABLE, Database.PASSWORD_RESET_REQUEST_TABLE,
                                  Database.PREFERENCES_TABLE, Database.UPLOAD_TOKENS_TABLE };
                foreach (string table in tables)
                {
                    cmd.Parameters.Clear();
                    cmd.CommandText = "DELETE FROM " + schema + "." + table + " WHERE USER_ID = ?";
                    cmd.Parameters.Add("USER_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(u.ID);
                    cmd.ExecuteNonQuery();
                }

                string[] tables2 = { Database.OPENID_USER_TABLE, Database.PASSWORD_HISTORY_TABLE };
                foreach (string table2 in tables2)
                {
                    cmd.Parameters.Clear();
                    cmd.CommandText = "DELETE FROM " + schema + "." + table2 + " WHERE USERID = ?";
                    cmd.Parameters.Add("USERID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(u.ID);
                    cmd.ExecuteNonQuery();
                }

                cmd.Parameters.Clear();
                cmd.CommandText = "DELETE FROM " + schema + "." + Database.TEMPLATES_TABLE_NAME + " WHERE CREATOR_ID = ?";
                cmd.Parameters.Add("CREATOR_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(u.ID);
                cmd.ExecuteNonQuery();

                cmd.Parameters.Clear();
                cmd.CommandText = "DELETE FROM " + schema + "." + Database.PROXY_USERS + " WHERE PROXY_USER_ID = ?";
                cmd.Parameters.Add("PROXY_USER_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(u.ID);
                cmd.ExecuteNonQuery();

                cmd.Parameters.Clear();
                cmd.CommandText = "DELETE FROM " + schema + "." + Database.USER_TABLE_NAME + " WHERE PKID = ?";
                cmd.Parameters.Add("PKID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(u.ID);
                cmd.ExecuteNonQuery();
            }
            finally
            {
                if (cmd != null)
                    cmd.Dispose();
            }
            return true;
        }

        public static bool enableAccount(QueryConnection conn, string schema, Guid accountID, bool enable)
        {
            QueryCommand cmd = null;
            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = "UPDATE " + schema + "." + Database.ACCOUNTS_TABLE +
                                    " SET Disabled = ? WHERE ACCOUNT_ID = ?";
                cmd.Parameters.Add("@Disabled", conn.getODBCBooleanType()).Value = conn.getBooleanValue(!enable);
                DbParameter accountId = cmd.Parameters.Add("@ACCOUNT_ID", conn.getODBCUniqueIdentiferType());
                accountId.Value = conn.getUniqueIdentifer(accountID);
                int rowsAffected = cmd.ExecuteNonQuery();
                if (rowsAffected > 0)
                    return true;
                return false;
            }
            finally
            {
                if (cmd != null)
                    cmd.Dispose();
            }
        }

        public static void updateUser(QueryConnection conn, string schema, User u)
        {
            QueryCommand cmd = null;
            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = "UPDATE " + schema + "." + USER_TABLE_NAME +
                    " SET Email=?,NumShares=?," +
                    "FirstName=?,LastName=?,Title=?,Company=?,Phone=?," +
                    "City=?,State=?,Zip=?,Country=?,StepByStep=?,ContactStatus=?," +
                    "ConnectString=?,DatabaseType=?,DatabaseDriver=?,DatabaseLoadDir=?,AdminUser=?,AdminPWD=?,QueryConnectString=?,QueryDatabaseType=?,QueryDatabaseDriver=?,QueryConnectionName=?,QueryUser=?,QueryPwd=?,"
                    + "DefaultSpace=?,DefaultSpaceId=?,DefaultDashboards=?,LOGO_IMAGE_ID=?,HeaderBackground=?,PartnerCode=?,CreateNewUserRights=?, " +
                    "ADMIN_ACCOUNT_ID=?,MANAGED_ACCOUNT_ID=? WHERE Username=?";
                DbParameter email = cmd.Parameters.Add("@Email", OdbcType.VarChar);
                if (u.Email == null)
                    email.Value = System.DBNull.Value;
                else
                    email.Value = u.Email;
                cmd.Parameters.Add("@NumShares", OdbcType.Int).Value = u.NumShares;
                DbParameter pfirstName = cmd.Parameters.Add("@FirstName", OdbcType.VarChar);
                string firstName = u.FirstName;
                if (firstName != null)
                    pfirstName.Value = firstName.Substring(0, Math.Min(60, firstName.Length));
                else
                    pfirstName.Value = System.DBNull.Value;
                DbParameter plastName = cmd.Parameters.Add("@LastName", OdbcType.VarChar);
                string lastName = u.LastName;
                if (lastName != null)
                    plastName.Value = lastName.Substring(0, Math.Min(60, lastName.Length));
                else
                    plastName.Value = System.DBNull.Value;
                DbParameter ptitle = cmd.Parameters.Add("@Title", OdbcType.VarChar);
                string title = u.Title;
                if (title != null)
                    ptitle.Value = title.Substring(0, Math.Min(60, title.Length));
                else
                    ptitle.Value = System.DBNull.Value;
                DbParameter pcompany = cmd.Parameters.Add("@Company", OdbcType.VarChar);
                string company = u.Company;
                if (company != null)
                    pcompany.Value = company.Substring(0, Math.Min(60, company.Length));
                else
                    pcompany.Value = System.DBNull.Value;
                DbParameter pphone = cmd.Parameters.Add("@Phone", OdbcType.VarChar);
                string phone = u.Phone;
                if (phone != null)
                    pphone.Value = phone.Substring(0, Math.Min(20, phone.Length));
                else
                    pphone.Value = System.DBNull.Value;
                DbParameter pcity = cmd.Parameters.Add("@City", OdbcType.VarChar);
                string city = u.City;
                if (city != null)
                    pcity.Value = city.Substring(0, Math.Min(100, city.Length));
                else
                    pcity.Value = System.DBNull.Value;
                DbParameter pstate = cmd.Parameters.Add("@State", OdbcType.VarChar);
                string state = u.State;
                if (state != null)
                    pstate.Value = state.Substring(0, Math.Min(100, state.Length));
                else
                    pstate.Value = System.DBNull.Value;
                DbParameter pzip = cmd.Parameters.Add("@Zip", OdbcType.VarChar);
                string zip = u.Zip;
                if (zip != null)
                    pzip.Value = zip.Substring(0, Math.Min(60, zip.Length));
                else
                    pzip.Value = System.DBNull.Value;
                DbParameter pcountry = cmd.Parameters.Add("@Country", OdbcType.VarChar);
                string country = u.Country;
                if (country != null)
                    pcountry.Value = country.Substring(0, Math.Min(60, country.Length));
                else
                    pcountry.Value = System.DBNull.Value;
                cmd.Parameters.Add("@StepByStep", conn.getODBCBooleanType()).Value = conn.getBooleanValue(u.StepByStep);
                cmd.Parameters.Add("@ContactStatus", OdbcType.Int).Value = u.ContactStatus;
                DbParameter dbconn = cmd.Parameters.Add("@ConnectString", OdbcType.VarChar);
                if (u.DatabaseConnectString == null)
                    dbconn.Value = System.DBNull.Value;
                else
                    dbconn.Value = u.DatabaseConnectString;
                DbParameter dbtype = cmd.Parameters.Add("@DatabaseType", OdbcType.VarChar);
                if (u.DatabaseType == null)
                    dbtype.Value = System.DBNull.Value;
                else
                    dbtype.Value = u.DatabaseType;
                DbParameter dbdriver = cmd.Parameters.Add("@DatabaseDriver", OdbcType.VarChar);
                if (u.DatabaseDriver == null)
                    dbdriver.Value = System.DBNull.Value;
                else
                    dbdriver.Value = u.DatabaseDriver;
                DbParameter dbdir = cmd.Parameters.Add("@DatabaseLoadDir", OdbcType.VarChar);
                if (u.DatabaseLoadDir == null)
                    dbdir.Value = System.DBNull.Value;
                else
                    dbdir.Value = u.DatabaseLoadDir;
                DbParameter adminu = cmd.Parameters.Add("@AdminUser", OdbcType.VarChar);
                if (u.AdminUser == null)
                    adminu.Value = System.DBNull.Value;
                else
                    adminu.Value = u.AdminUser;
                DbParameter adminp = cmd.Parameters.Add("@AdminPwd", OdbcType.VarChar);
                if (u.AdminPwd == null)
                    adminp.Value = System.DBNull.Value;
                else
                    adminp.Value = Performance_Optimizer_Administration.MainAdminForm.encrypt(u.AdminPwd,
                                            Performance_Optimizer_Administration.MainAdminForm.SecretPassword);
                DbParameter qdbconn = cmd.Parameters.Add("@QueryConnectString", OdbcType.VarChar);
                if (u.QueryDatabaseConnectString == null)
                    qdbconn.Value = System.DBNull.Value;
                else
                    qdbconn.Value = u.QueryDatabaseConnectString;
                DbParameter qdbtype = cmd.Parameters.Add("@QueryDatabaseType", OdbcType.VarChar);
                if (u.QueryDatabaseType == null)
                    qdbtype.Value = System.DBNull.Value;
                else
                    qdbtype.Value = u.QueryDatabaseType;
                DbParameter qdbdriver = cmd.Parameters.Add("@QueryDatabaseDriver", OdbcType.VarChar);
                if (u.QueryDatabaseDriver == null)
                    qdbdriver.Value = System.DBNull.Value;
                else
                    qdbdriver.Value = u.QueryDatabaseDriver;
                DbParameter qcname = cmd.Parameters.Add("@QueryConnectionName", OdbcType.VarChar);
                if (u.QueryConnectionName == null)
                    qcname.Value = System.DBNull.Value;
                else
                    qcname.Value = u.QueryConnectionName;
                DbParameter qadminu = cmd.Parameters.Add("@QueryUser", OdbcType.VarChar);
                if (u.QueryUser == null)
                    qadminu.Value = System.DBNull.Value;
                else
                    qadminu.Value = u.QueryUser;
                DbParameter qadminp = cmd.Parameters.Add("@QueryPwd", OdbcType.VarChar);
                if (u.QueryPwd == null)
                    qadminp.Value = System.DBNull.Value;
                else
                    qadminp.Value = Performance_Optimizer_Administration.MainAdminForm.encrypt(u.QueryPwd,
                                            Performance_Optimizer_Administration.MainAdminForm.SecretPassword);
                // this is why random users get repository admin access - this is set to true when somebody superusers in
                // there is no UI to set this value so it should not be updated here
                // DbParameter repadmin = cmd.Parameters.Add("@RepositoryAdmin", OdbcType.Bit);
                // repadmin.Value = u.RepositoryAdmin;
                DbParameter defaultspace = cmd.Parameters.Add("@DefaultSpace", OdbcType.NVarChar);
                if (u.DefaultSpace == null)
                    defaultspace.Value = System.DBNull.Value;
                else
                    defaultspace.Value = u.DefaultSpace;
                DbParameter defaultspaceid = cmd.Parameters.Add("@DefaultSpaceId", conn.getODBCUniqueIdentiferType());
                if (u.DefaultSpaceId != null && u.DefaultSpaceId != Guid.Empty)
                    defaultspaceid.Value = conn.getUniqueIdentifer(u.DefaultSpaceId);
                else
                    defaultspaceid.Value = System.DBNull.Value;
                DbParameter defaultdashboards = cmd.Parameters.Add("@DefaultDashboards", conn.getODBCBooleanType());
                defaultdashboards.Value = conn.getBooleanValue(u.DefaultDashboards);
                DbParameter logoid = cmd.Parameters.Add("@LogoID", conn.getODBCUniqueIdentiferType());
                if (u.LogoImageID != null && u.LogoImageID != Guid.Empty)
                    logoid.Value = conn.getUniqueIdentifer(u.LogoImageID);
                else
                    logoid.Value = System.DBNull.Value;
                DbParameter headerBackground = cmd.Parameters.Add("@HeaderBackground", conn.getODBCBigIntType());
                if (u.HeaderBackground > 0)
                    headerBackground.Value = u.HeaderBackground;
                else
                    headerBackground.Value = System.DBNull.Value;
                DbParameter partnercode = cmd.Parameters.Add("@PartnerCode", OdbcType.VarChar);
                if (u.PartnerCode == null || u.PartnerCode.Length == 0)
                    partnercode.Value = System.DBNull.Value;
                else
                    partnercode.Value = u.PartnerCode;
                DbParameter createuserrights = cmd.Parameters.Add("@CreateNewUserRights", conn.getODBCBooleanType());
                createuserrights.Value = conn.getBooleanValue(u.CreateNewUserRights);

                DbParameter adminAccountId = cmd.Parameters.Add("@ADMIN_ACCOUNT_ID", conn.getODBCUniqueIdentiferType());
                if (u.AdminAccountId != null && !u.AdminAccountId.Equals(Guid.Empty))
                {
                    adminAccountId.Value = conn.getUniqueIdentifer(u.AdminAccountId);
                }
                else
                {
                    adminAccountId.Value = System.DBNull.Value;
                }
                DbParameter managedAccountId = cmd.Parameters.Add("@MANAGED_ACCOUNT_ID", conn.getODBCUniqueIdentiferType());
                if (u.ManagedAccountId != null && !u.ManagedAccountId.Equals(Guid.Empty))
                {
                    managedAccountId.Value = conn.getUniqueIdentifer(u.ManagedAccountId);
                }
                else
                {
                    managedAccountId.Value = System.DBNull.Value;
                }
                cmd.Parameters.Add("@Username", OdbcType.NVarChar).Value = u.Username;
                cmd.ExecuteNonQuery();
            }
            finally
            {
                if (cmd != null)
                    cmd.Dispose();
            }
        }

        public static bool updateNonDBSpaceParams(QueryConnection conn, string schema, Space sp)
        {
            QueryCommand cmd = null;
            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = "UPDATE " + schema + "." + SPACE_TABLE_NAME +
                    " SET Name=?,Comments=?,Automatic=?,LoadNumber=?,SourceTemplateName=?," +
                    "Active=?,Folded=?,TestMode=?,Locked=?," +
                    "SSO=?,SSOPWD=?,MaxQueryRows=?,MaxQueryTimeout=?,QueryLanguageVersion=?,UsageTracking=?,ProcessVersion=?, " +
                    "AppVersion=?, UseDynamicGroups=?, AllowRetryFailedLoad=?, SFDCVersion=?, Discovery=?, MapNullsToZero=?, AWSAccessKeyId=?, AWSSecretKey=?, " +
                    "ReportFrom=?, ReportSubject=? " +
                    "WHERE ID=?";
                cmd.Parameters.Add("@Name", OdbcType.NVarChar).Value = sp.Name;
                cmd.Parameters.Add("@Comments", OdbcType.NVarChar).Value = sp.Comments == null ? (object)System.DBNull.Value : sp.Comments;
                cmd.Parameters.Add("@Automatic", conn.getODBCBooleanType()).Value = conn.getBooleanValue(sp.Automatic);
                cmd.Parameters.Add("@LoadNumber", OdbcType.Int).Value = sp.LoadNumber;
                cmd.Parameters.Add("@SourceTemplateName", OdbcType.VarChar).Value = sp.SourceTemplateName == null ?
                    (object)System.DBNull.Value : sp.SourceTemplateName;

                cmd.Parameters.Add("@Active", conn.getODBCBooleanType()).Value = conn.getBooleanValue(sp.Active);
                cmd.Parameters.Add("@Folded", conn.getODBCBooleanType()).Value = conn.getBooleanValue(sp.Folded);
                cmd.Parameters.Add("@TestMode", conn.getODBCBooleanType()).Value = conn.getBooleanValue(sp.TestMode);
                cmd.Parameters.Add("@Locked", conn.getODBCBooleanType()).Value = conn.getBooleanValue(sp.Locked);
                cmd.Parameters.Add("@SSO", conn.getODBCBooleanType()).Value = conn.getBooleanValue(sp.SSO);
                cmd.Parameters.Add("@SSOPwd", OdbcType.VarChar).Value = sp.SSOPassword == null ? (object)System.DBNull.Value : Performance_Optimizer_Administration.MainAdminForm.encrypt(sp.SSOPassword, Performance_Optimizer_Administration.MainAdminForm.SecretPassword);
                cmd.Parameters.Add("@MaxQueryRows", OdbcType.Int).Value = sp.MaxQueryRows;
                cmd.Parameters.Add("@MaxQueryTimeout", OdbcType.Int).Value = sp.MaxQueryTimeout;
                cmd.Parameters.Add("@QueryLanguageVersion", OdbcType.Int).Value = sp.QueryLanguageVersion;
                cmd.Parameters.Add("@UsageTracking", conn.getODBCBooleanType()).Value = conn.getBooleanValue(sp.UsageTracking);
                DbParameter processVersionParam = cmd.Parameters.Add("@ProcessVersion", OdbcType.Int);
                if (sp.ProcessVersionID <= 0)
                {
                    processVersionParam.Value = System.DBNull.Value;
                }
                else
                {
                    processVersionParam.Value = sp.ProcessVersionID;
                }
                DbParameter appVersionParam = cmd.Parameters.Add("@AppVersion", OdbcType.Int);
                if (sp.AppVersion <= 0)
                {
                    appVersionParam.Value = 49;
                }
                else
                {
                    appVersionParam.Value = sp.AppVersion;
                }
                cmd.Parameters.Add("@UseDynamicGroups", conn.getODBCBooleanType()).Value = conn.getBooleanValue(sp.UseDynamicGroups);
                cmd.Parameters.Add("@AllowRetryFailedLoad", conn.getODBCBooleanType()).Value = conn.getBooleanValue(sp.AllowRetryFailedLoad);
                DbParameter sfdcVersionParam = cmd.Parameters.Add("@SFDCVersion", OdbcType.Int);
                if (sp.SFDCVersionID <= 0)
                {
                    sfdcVersionParam.Value = System.DBNull.Value;
                }
                else
                {
                    sfdcVersionParam.Value = sp.SFDCVersionID;
                }

                cmd.Parameters.Add("@Discovery", conn.getODBCBooleanType()).Value = conn.getBooleanValue(sp.DiscoveryMode);
                cmd.Parameters.Add("@MapNullsToZero", conn.getODBCBooleanType()).Value = conn.getBooleanValue(sp.MapNullsToZero);

                cmd.Parameters.Add("@AWSAccessKeyId", OdbcType.VarChar).Value = sp.awsAccessKeyId == null ? (object)System.DBNull.Value : sp.awsAccessKeyId;
                cmd.Parameters.Add("@AWSSecretKey", OdbcType.VarChar).Value = sp.awsSecretKey == null ? (object)System.DBNull.Value : sp.awsSecretKey;

                cmd.Parameters.Add("@ReportFrom", OdbcType.VarChar).Value = sp.ScheduleFrom == null || sp.ScheduleFrom.Trim().Length == 0 ? (object)System.DBNull.Value : sp.ScheduleFrom;
                cmd.Parameters.Add("@ReportSubject", OdbcType.VarChar).Value = sp.ScheduleSubject == null || sp.ScheduleSubject.Trim().Length == 0 ? (object)System.DBNull.Value : sp.ScheduleSubject;
                // end of parameters

                // WHERE CLAUSE
                cmd.Parameters.Add("@SPID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(sp.ID);

                Global.systemLog.Info(cmd.CommandText);
                int result = cmd.ExecuteNonQuery();
                return result == 1;
            }
            finally
            {
                if (cmd != null)
                    cmd.Dispose();
            }
        }

        public static bool updateSpace(QueryConnection conn, string schema, Space sp)
        {
            QueryCommand cmd = null;
            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = "UPDATE " + schema + "." + SPACE_TABLE_NAME +
                    " SET Name=?,Comments=?," + conn.getMSSQLReservedName("Schema") + "=?,Directory=?,Automatic=?,LoadNumber=?,SourceTemplateName=?,Type=?," +
                    "ConnectString=?,DatabaseType=?,DatabaseDriver=?,DatabaseLoadDir=?,AdminUser=?,AdminPwd=?," +
                    "QueryConnectString=?,QueryDatabaseType=?,QueryDatabaseDriver=?,QueryConnectionName=?,QueryUser=?,QueryPwd=?," +
                    "Active=?,Folded=?,TestMode=?,Locked=?," +
                    "SSO=?,SSOPWD=?,MaxQueryRows=?,MaxQueryTimeout=?,QueryLanguageVersion=?,UsageTracking=?,ProcessVersion=?, " +
                    "AppVersion=?, UseDynamicGroups=?, AllowRetryFailedLoad=?, SFDCVersion=?, Discovery=?, MapNullsToZero=?, AWSAccessKeyId=?, AWSSecretKey=?, " +
                    "ReportFrom=?, ReportSubject=? " +
                    "WHERE ID=?";
                cmd.Parameters.Add("@Name", OdbcType.NVarChar).Value = sp.Name;
                cmd.Parameters.Add("@Comments", OdbcType.NVarChar).Value = sp.Comments == null ? (object)System.DBNull.Value : sp.Comments;
                cmd.Parameters.Add("@Schema", OdbcType.VarChar).Value = sp.Schema;
                cmd.Parameters.Add("@Directory", OdbcType.VarChar).Value = sp.Directory;
                cmd.Parameters.Add("@Automatic", conn.getODBCBooleanType()).Value = conn.getBooleanValue(sp.Automatic);
                cmd.Parameters.Add("@LoadNumber", OdbcType.Int).Value = sp.LoadNumber;
                cmd.Parameters.Add("@SourceTemplateName", OdbcType.VarChar).Value = sp.SourceTemplateName == null ?
                    (object)System.DBNull.Value : sp.SourceTemplateName;
                cmd.Parameters.Add("@Type", OdbcType.Int).Value = sp.Type;
                string connectString = sp.ConnectString;
                if (connectString != null && isDBTypeParAccel(connectString))
                {
                    string connectStringSuffix = getPADBConnectionStringSuffix(conn, schema, sp);
                    if (connectStringSuffix != null && !connectStringSuffix.Trim().Equals("") && sp.ConnectString.EndsWith(connectStringSuffix))
                        connectString = sp.ConnectString.Substring(0, sp.ConnectString.LastIndexOf(connectStringSuffix));
                }
                cmd.Parameters.Add("@ConnectString", OdbcType.VarChar).Value = connectString;
                cmd.Parameters.Add("@DatabaseType", OdbcType.VarChar).Value = sp.DatabaseType;
                cmd.Parameters.Add("@DatabaseDriver", OdbcType.VarChar).Value = sp.DatabaseDriver;
                cmd.Parameters.Add("@DatabaseLoadDir", OdbcType.VarChar).Value = sp.DatabaseLoadDir;
                cmd.Parameters.Add("@AdminUser", OdbcType.VarChar).Value = sp.AdminUser;
                cmd.Parameters.Add("@AdminPwd", OdbcType.VarChar).Value = Performance_Optimizer_Administration.MainAdminForm.encrypt(sp.AdminPwd, Performance_Optimizer_Administration.MainAdminForm.SecretPassword);

                cmd.Parameters.Add("@QueryConnectString", OdbcType.VarChar).Value = sp.QueryConnectString == null ? (object)System.DBNull.Value : sp.QueryConnectString;
                cmd.Parameters.Add("@QueryDatabaseType", OdbcType.VarChar).Value = sp.QueryDatabaseType == null ? (object)System.DBNull.Value : sp.QueryDatabaseType;
                cmd.Parameters.Add("@QueryDatabaseDriver", OdbcType.VarChar).Value = sp.QueryDatabaseDriver == null ? (object)System.DBNull.Value : sp.QueryDatabaseDriver;
                cmd.Parameters.Add("@QueryConnectionName", OdbcType.VarChar).Value = sp.QueryConnectionName == null ? (object)System.DBNull.Value : sp.QueryConnectionName;
                cmd.Parameters.Add("@QueryUser", OdbcType.VarChar).Value = sp.QueryUser == null ? (object)System.DBNull.Value : sp.QueryUser;
                cmd.Parameters.Add("@QueryPwd", OdbcType.VarChar).Value = sp.QueryPwd == null ? (object)System.DBNull.Value : Performance_Optimizer_Administration.MainAdminForm.encrypt(sp.QueryPwd, Performance_Optimizer_Administration.MainAdminForm.SecretPassword);

                cmd.Parameters.Add("@Active", conn.getODBCBooleanType()).Value = conn.getBooleanValue(sp.Active);
                cmd.Parameters.Add("@Folded", conn.getODBCBooleanType()).Value = conn.getBooleanValue(sp.Folded);
                cmd.Parameters.Add("@TestMode", conn.getODBCBooleanType()).Value = conn.getBooleanValue(sp.TestMode);
                cmd.Parameters.Add("@Locked", conn.getODBCBooleanType()).Value = conn.getBooleanValue(sp.Locked);
                cmd.Parameters.Add("@SSO", conn.getODBCBooleanType()).Value = conn.getBooleanValue(sp.SSO);
                cmd.Parameters.Add("@SSOPwd", OdbcType.VarChar).Value = sp.SSOPassword == null ? (object)System.DBNull.Value : Performance_Optimizer_Administration.MainAdminForm.encrypt(sp.SSOPassword, Performance_Optimizer_Administration.MainAdminForm.SecretPassword);
                cmd.Parameters.Add("@MaxQueryRows", OdbcType.Int).Value = sp.MaxQueryRows;
                cmd.Parameters.Add("@MaxQueryTimeout", OdbcType.Int).Value = sp.MaxQueryTimeout;
                cmd.Parameters.Add("@QueryLanguageVersion", OdbcType.Int).Value = sp.QueryLanguageVersion;
                cmd.Parameters.Add("@UsageTracking", conn.getODBCBooleanType()).Value = conn.getBooleanValue(sp.UsageTracking);
                DbParameter processVersionParam = cmd.Parameters.Add("@ProcessVersion", OdbcType.Int);
                if (sp.ProcessVersionID <= 0)
                {
                    processVersionParam.Value = System.DBNull.Value;
                }
                else
                {
                    processVersionParam.Value = sp.ProcessVersionID;
                }
                DbParameter appVersionParam = cmd.Parameters.Add("@AppVersion", OdbcType.Int);
                if (sp.AppVersion <= 0)
                {
                    appVersionParam.Value = 49;
                }
                else
                {
                    appVersionParam.Value = sp.AppVersion;
                }
                cmd.Parameters.Add("@UseDynamicGroups", conn.getODBCBooleanType()).Value = conn.getBooleanValue(sp.UseDynamicGroups);
                cmd.Parameters.Add("@AllowRetryFailedLoad", conn.getODBCBooleanType()).Value = conn.getBooleanValue(sp.AllowRetryFailedLoad);
                DbParameter sfdcVersionParam = cmd.Parameters.Add("@SFDCVersion", OdbcType.Int);
                if (sp.SFDCVersionID <= 0)
                {
                    sfdcVersionParam.Value = System.DBNull.Value;
                }
                else
                {
                    sfdcVersionParam.Value = sp.SFDCVersionID;
                }

                cmd.Parameters.Add("@Discovery", conn.getODBCBooleanType()).Value = conn.getBooleanValue(sp.DiscoveryMode);
                cmd.Parameters.Add("@MapNullsToZero", conn.getODBCBooleanType()).Value = conn.getBooleanValue(sp.MapNullsToZero);

                cmd.Parameters.Add("@AWSAccessKeyId", OdbcType.VarChar).Value = sp.awsAccessKeyId == null ? (object)System.DBNull.Value : sp.awsAccessKeyId;
                cmd.Parameters.Add("@AWSSecretKey", OdbcType.VarChar).Value = sp.awsSecretKey == null ? (object)System.DBNull.Value : sp.awsSecretKey;

                cmd.Parameters.Add("@ReportFrom", OdbcType.VarChar).Value = sp.ScheduleFrom == null || sp.ScheduleFrom.Trim().Length == 0 ? (object)System.DBNull.Value : sp.ScheduleFrom;
                cmd.Parameters.Add("@ReportSubject", OdbcType.VarChar).Value = sp.ScheduleSubject == null || sp.ScheduleSubject.Trim().Length == 0 ? (object)System.DBNull.Value : sp.ScheduleSubject;
                // end of parameters

                // WHERE CLAUSE
                cmd.Parameters.Add("@SPID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(sp.ID);

                Global.systemLog.Info(cmd.CommandText);
                int result = cmd.ExecuteNonQuery();
                return result == 1;
            }
            finally
            {
                if (cmd != null)
                    cmd.Dispose();
            }
        }

        public static bool updateSpaceSFDCVersion(QueryConnection conn, String schema, Space sp, int SFDCVersionID)
        {
            QueryCommand cmd = null;
            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = "UPDATE " + schema + "." + SPACE_TABLE_NAME +
                    " SET SFDCVersion=? " +
                    " WHERE ID=?";

                DbParameter sfdcVersionParam = cmd.Parameters.Add("@SFDCVersion", OdbcType.Int);
                if (SFDCVersionID <= 0)
                {
                    sfdcVersionParam.Value = System.DBNull.Value;
                }
                else
                {
                    sfdcVersionParam.Value = SFDCVersionID;
                }

                cmd.Parameters.Add("@SPID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(sp.ID);

                Global.systemLog.Info("Updating sfdc version :" + cmd.CommandText + ": ID=" + sp.ID);
                int result = cmd.ExecuteNonQuery();
                return result == 1;
            }
            finally
            {
                if (cmd != null)
                    cmd.Dispose();
            }
        }

        public static void createSpace(QueryConnection conn, string schema, User adminUser, Space s)
        {
            QueryCommand cmd = null;
            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = "INSERT INTO " + schema + "." + SPACE_TABLE_NAME +
                    " (ID,Name,Comments," + conn.getMSSQLReservedName("Schema") + ",Directory,Automatic,LoadNumber,SourceTemplateName,Type,ConnectString,"
                    + "DatabaseType,DatabaseDriver,DatabaseLoadDir,AdminUser,AdminPwd,Active,Folded,TestMode,Locked,Demo,"
                    + "SSO,SSOPWD,QueryLanguageVersion,MaxQueryRows,MaxQueryTimeout,UsageTracking,ProcessVersion,UseDynamicGroups,AllowRetryFailedLoad,SFDCVersion,"
                    + "DisallowExternalScheduler,Discovery,MapNullsToZero,AWSAccessKeyId,AWSSecretKey,DBTimeZone,CreatedDate) "
                    + "VALUES "
                    + "(?,?,?,?,?,?,?,?,?,?,"
                    + "?,?,?,?,?,?,?,?,?,?,"
                    + "?,?,?,?,?,?,?,?,?,?,"
                    + "?,?,?,?,?,?," + conn.getCurrentTimestamp() + ")";
                cmd.Parameters.Add("ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(s.ID);
                cmd.Parameters.Add("Name", OdbcType.NVarChar).Value = s.Name;
                cmd.Parameters.Add("Comments", OdbcType.NVarChar).Value = s.Comments == null ? (object)System.DBNull.Value : s.Comments;
                cmd.Parameters.Add("Schema", OdbcType.VarChar).Value = s.Schema;
                cmd.Parameters.Add("Directory", OdbcType.VarChar).Value = s.Directory;
                cmd.Parameters.Add("Automatic", conn.getODBCBooleanType()).Value = conn.getBooleanValue(s.Automatic);
                cmd.Parameters.Add("LoadNumber", OdbcType.Int).Value = s.LoadNumber;
                cmd.Parameters.Add("SourceTemplateName", OdbcType.VarChar).Value = s.SourceTemplateName == null ? (object)System.DBNull.Value : s.SourceTemplateName;
                cmd.Parameters.Add("Type", OdbcType.Int).Value = s.Type;
                string connectString = s.ConnectString;
                if (connectString != null && isDBTypeParAccel(connectString))
                {
                    string connectStringSuffix = getGlobalProperty(conn, schema, adminUser.ManagedAccountId, PADB_CONNECTIONSTRING_SUFFIX, true);
                    if (connectStringSuffix != null && !connectStringSuffix.Trim().Equals("") && s.ConnectString.EndsWith(connectStringSuffix))
                        connectString = s.ConnectString.Substring(0, s.ConnectString.LastIndexOf(connectStringSuffix));
                }
                cmd.Parameters.Add("ConnectString", OdbcType.VarChar).Value = connectString == null ? (object)System.DBNull.Value : connectString;
                cmd.Parameters.Add("DatabaseType", OdbcType.VarChar).Value = s.DatabaseType == null ? (object)System.DBNull.Value : s.DatabaseType;
                cmd.Parameters.Add("DatabaseDriver", OdbcType.VarChar).Value = s.DatabaseDriver == null ? (object)System.DBNull.Value : s.DatabaseDriver;
                cmd.Parameters.Add("DatabaseLoadDir", OdbcType.VarChar).Value = s.DatabaseLoadDir == null ? (object)System.DBNull.Value : s.DatabaseLoadDir;
                cmd.Parameters.Add("AdminUser", OdbcType.VarChar).Value = s.AdminUser == null ? (object)System.DBNull.Value : s.AdminUser;
                cmd.Parameters.Add("AdminPwd", OdbcType.VarChar).Value = s.AdminPwd == null ? (object)System.DBNull.Value : Performance_Optimizer_Administration.MainAdminForm.encrypt(s.AdminPwd, Performance_Optimizer_Administration.MainAdminForm.SecretPassword);
                cmd.Parameters.Add("Active", OdbcType.Bit).Value = s.Active;
                cmd.Parameters.Add("Folded", OdbcType.Bit).Value = s.Folded;
                cmd.Parameters.Add("TestMode", OdbcType.Bit).Value = s.TestMode;
                cmd.Parameters.Add("Locked", OdbcType.Bit).Value = s.Locked;
                cmd.Parameters.Add("Demo", OdbcType.Bit).Value = false;
                cmd.Parameters.Add("SSO", OdbcType.Bit).Value = s.SSO;
                cmd.Parameters.Add("SSOPWD", OdbcType.VarChar).Value = s.SSOPassword == null ? (object)System.DBNull.Value : Performance_Optimizer_Administration.MainAdminForm.encrypt(s.SSOPassword, Performance_Optimizer_Administration.MainAdminForm.SecretPassword);
                cmd.Parameters.Add("QueryLanguageVersion", OdbcType.Int).Value = s.QueryLanguageVersion;
                cmd.Parameters.Add("MaxQueryRows", OdbcType.Int).Value = s.MaxQueryRows;
                cmd.Parameters.Add("MaxQueryTimeout", OdbcType.Int).Value = s.MaxQueryTimeout;
                cmd.Parameters.Add("UsageTracking", OdbcType.Bit).Value = s.UsageTracking;
                DbParameter processVersionParam = cmd.Parameters.Add("ProcessVersion", OdbcType.Int);
                if (s.ProcessVersionID <= 0)
                {
                    processVersionParam.Value = System.DBNull.Value;
                }
                else
                {
                    processVersionParam.Value = s.ProcessVersionID;
                }
                cmd.Parameters.Add("UseDynamicGroups", OdbcType.Bit).Value = s.UseDynamicGroups;
                cmd.Parameters.Add("AllowRetryFailedLoad", OdbcType.Bit).Value = s.AllowRetryFailedLoad;
                DbParameter sfdcVesionParam = cmd.Parameters.Add("SFDCVersion", OdbcType.Int);
                if (s.SFDCVersionID <= 0)
                {
                    sfdcVesionParam.Value = System.DBNull.Value;
                }
                else
                {
                    sfdcVesionParam.Value = s.SFDCVersionID;
                }

                // Putting null in case it's false
                DbParameter disallowParam = cmd.Parameters.Add("DisallowExternalScheduler", OdbcType.Bit);
                if (!s.DisallowExternalScheduler)
                {
                    disallowParam.Value = System.DBNull.Value;
                }
                else
                {
                    disallowParam.Value = true;
                }

                cmd.Parameters.Add("Discovery", conn.getODBCBooleanType()).Value = conn.getBooleanValue(s.DiscoveryMode);
                cmd.Parameters.Add("MapNullsToZero", conn.getODBCBooleanType()).Value = conn.getBooleanValue(s.MapNullsToZero);

                cmd.Parameters.Add("AWSAccessKeyId", OdbcType.VarChar).Value = s.awsAccessKeyId == null ? (object)System.DBNull.Value : s.awsAccessKeyId;
                cmd.Parameters.Add("AWSSecretKey", OdbcType.VarChar).Value = s.awsSecretKey == null ? (object)System.DBNull.Value : s.awsSecretKey;
                cmd.Parameters.Add("DBTimeZone", OdbcType.VarChar).Value = s.dbTimeZone == null ? (object)System.DBNull.Value : s.dbTimeZone;
                cmd.ExecuteNonQuery();
            }
            finally
            {
                if (cmd != null)
                    cmd.Dispose();
            }
            addSpaceMembership(conn, schema, adminUser.ID, s.ID, true, true);
            Directory.CreateDirectory(s.Directory);
            Directory.CreateDirectory(Path.Combine(s.Directory, "data"));
            Directory.CreateDirectory(Path.Combine(s.Directory, "catalog"));
            Directory.CreateDirectory(Path.Combine(s.Directory, "cache"));
            Directory.CreateDirectory(Path.Combine(s.Directory, Util.DCCONFIG_DIR));
            // Create the database schema
            QueryConnection tconn = null;
            try
            {
                bool exists = isDBSchemaExist(s.getFullConnectString(), s.DatabaseType, s.Schema);
                tconn = ConnectionPool.getConnection(s.getFullConnectString());
                if (s.DatabaseType == "Infobright")
                {
                    QueryCommand tcmd = tconn.CreateCommand();
                    tcmd.CommandText = "CREATE DATABASE IF NOT EXISTS " + s.Schema;
                    tcmd.ExecuteNonQuery();
                    tcmd.Dispose();
                }
                else if (s.DatabaseType == ORACLE)
                {
                    QueryCommand tcmd = tconn.CreateCommand();
                    tcmd.CommandTimeout = GET_TABLES_TIMEOUT;
                    if (!exists)
                    {
                        string randomPassword = Util.GetRandomPasswordForOracle(14); //Max identifier length for oracle is 17
                        string createUserSQL = "CREATE USER " + s.Schema + " IDENTIFIED BY " + randomPassword;
                        if (datafileloc != null && datafileloc.Trim().Length > 0)
                        {
                            string datafile = datafileloc + "/" + s.Schema + ".dbf";
                            string createTablespaceQuery = "CREATE TABLESPACE " + s.Schema + " DATAFILE '" + datafile + "'" +
                                " SIZE 1M AUTOEXTEND ON MAXSIZE UNLIMITED EXTENT MANAGEMENT LOCAL SEGMENT SPACE MANAGEMENT AUTO";
                            tcmd.CommandText = createTablespaceQuery;
                            Global.systemLog.Debug("Create tablespace query for space " + s.ID.ToString() + " query: " + createTablespaceQuery);
                            tcmd.ExecuteNonQuery();
                            createUserSQL = createUserSQL + " DEFAULT TABLESPACE " + s.Schema;
                        }
                        tcmd.CommandText = createUserSQL;
                        Global.systemLog.Debug("Create user query for space " + s.ID.ToString() + " query: " + createUserSQL.Replace(randomPassword, "<password>")); //don't log password
                        tcmd.ExecuteNonQuery();
                        string grantTablespaceQuotaQuery = "GRANT UNLIMITED TABLESPACE TO " + s.Schema;
                        tcmd.CommandText = grantTablespaceQuotaQuery;
                        Global.systemLog.Debug("Grant tablespace quota query for space " + s.ID.ToString() + " query: " + grantTablespaceQuotaQuery);
                        tcmd.ExecuteNonQuery();
                    }
                    tcmd.Dispose();
                }
                else if (s.DatabaseType == "MemDB")
                {
                    // Do nothing - no need to create a schema
                }
                else if (tconn.isDBTypeParAccel() || tconn.isDBTypeSAPHana())
                {
                    QueryCommand tcmd = tconn.CreateCommand();
                    tcmd.CommandTimeout = GET_TABLES_TIMEOUT;
                    if (!exists)
                    {
                        tcmd.CommandText = "CREATE SCHEMA " + s.Schema;
                        tcmd.ExecuteNonQuery();
                    }
                    tcmd.Dispose();
                }
                else
                {
                    QueryCommand tcmd = tconn.CreateCommand();
                    tcmd.CommandTimeout = GET_TABLES_TIMEOUT;
                    if (!exists)
                    {
                        tcmd.CommandText = "CREATE SCHEMA [" + s.Schema + "] AUTHORIZATION [" + schema + "]";
                        tcmd.ExecuteNonQuery();
                    }
                    tcmd.Dispose();
                }
            }
            finally
            {
                ConnectionPool.releaseConnection(tconn);
            }
        }

        public static bool isDBSchemaExist(string connectstr, string dbtype, string schemaname)
        {
            QueryConnection tconn = null;
            bool exists = false;
            try
            {
                tconn = Util.getConnectionWithVariedRetries(connectstr, dbtype);
                if (dbtype == "Infobright")
                {
                    QueryCommand tcmd = tconn.CreateCommand();
                    tcmd.CommandText = "SELECT * FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME='" + schemaname + "'";
                    tcmd.CommandTimeout = GET_TABLES_TIMEOUT;
                    QueryReader reader = tcmd.ExecuteReader();
                    exists = reader.HasRows;
                    tcmd.Dispose();
                }
                else if (dbtype == ORACLE)
                {
                    QueryCommand tcmd = tconn.CreateCommand();
                    string selUserQuery = "SELECT * FROM all_users WHERE username='" + schemaname.ToUpper() + "'";
                    tcmd.CommandText = selUserQuery;
                    tcmd.CommandTimeout = GET_TABLES_TIMEOUT;
                    QueryReader reader = tcmd.ExecuteReader();
                    exists = reader.HasRows;
                    reader.Close();
                    tcmd.Dispose();
                }
                else if (dbtype == "MemDB")
                {
                    // Do nothing - no need to create a schema
                }
                else if (tconn.isDBTypeParAccel() || dbtype == REDSHIFT)
                {
                    QueryCommand tcmd = tconn.CreateCommand();
                    tcmd.CommandText = "SELECT * FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME='" + schemaname.ToLower() + "'";
                    tcmd.CommandTimeout = GET_TABLES_TIMEOUT;
                    QueryReader reader = tcmd.ExecuteReader();
                    exists = reader.HasRows;
                    reader.Close();
                    tcmd.Dispose();
                }
                else if (tconn.isDBTypeSAPHana())
                {
                    QueryCommand tcmd = tconn.CreateCommand();
                    tcmd.CommandText = "SELECT * FROM SYS.SCHEMAS WHERE SCHEMA_NAME='" + schemaname.ToUpper() + "'";
                    tcmd.CommandTimeout = GET_TABLES_TIMEOUT;
                    QueryReader reader = tcmd.ExecuteReader();
                    exists = reader.HasRows;
                    reader.Close();
                    tcmd.Dispose();
                }
                else
                {
                    QueryCommand tcmd = tconn.CreateCommand();
                    tcmd.CommandText = "SELECT * FROM SYS.SCHEMAS WHERE NAME='" + schemaname + "'";
                    tcmd.CommandTimeout = GET_TABLES_TIMEOUT;
                    QueryReader reader = tcmd.ExecuteReader();
                    exists = reader.HasRows;
                    tcmd.Dispose();
                }
            }
            finally
            {
                ConnectionPool.releaseConnection(tconn);
            }
            return exists;
        }

        /*
         * Return is db schema already used by other space
         */
        public static bool isDBSchemaInUse(QueryConnection conn, string schema, string spaceSchema)
        {
            QueryCommand cmd = conn.CreateCommand();
            cmd.CommandText = "SELECT * FROM " + schema + "." + SPACE_TABLE_NAME +
                " WHERE " + conn.getMSSQLReservedName("Schema") + "='" + spaceSchema + "'";
            QueryReader reader = cmd.ExecuteReader();
            bool inUse = reader.HasRows;
            reader.Close();
            cmd.Dispose();
            return (inUse);
        }

        public static void addSpaceMembership(QueryConnection conn, string schema, Guid user_ID, Guid space_ID, bool adminflag, bool ownerflag)
        {
            QueryCommand cmd = null;
            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = "INSERT INTO " + schema + "." + USER_SPACE_INXN +
                    " (USER_ID,SPACE_ID,Owner,Admin,Adhoc,Dashboards,StartDate) VALUES (?,?,?,?,?,?,?)";
                cmd.Parameters.Add("USER_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(user_ID);
                cmd.Parameters.Add("SPACE_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(space_ID);
                cmd.Parameters.Add("Owner", conn.getODBCBooleanType()).Value = conn.getBooleanValue(ownerflag);
                cmd.Parameters.Add("Admin", conn.getODBCBooleanType()).Value = conn.getBooleanValue(adminflag);
                cmd.Parameters.Add("Adhoc", conn.getODBCBooleanType()).Value = conn.getBooleanValue(false);
                cmd.Parameters.Add("Dashboards", conn.getODBCBooleanType()).Value = conn.getBooleanValue(true);
                DateTime dtnow = DateTime.Now;
                dtnow = dtnow.AddMilliseconds(-dtnow.Millisecond);
                cmd.Parameters.Add("StartDate", OdbcType.DateTime).Value = dtnow;
                cmd.ExecuteNonQuery();
            }
            finally
            {
                if (cmd != null)
                    cmd.Dispose();
            }
        }

        public static void setShareQuotaUsage(QueryConnection conn, string schema, User u)
        {
            QueryCommand cmd = null;
            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = "SELECT COUNT(DISTINCT A.USER_ID) FROM " + schema + "." + USER_SPACE_INXN +
                    " A INNER JOIN " + schema + "." + USER_SPACE_INXN + " B ON A.SPACE_ID=B.SPACE_ID" +
                    " WHERE B.USER_ID=? AND B.Owner=1";
                cmd.Parameters.Add("USER_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(u.ID);
                int distinctUsers = Int32.Parse(cmd.ExecuteScalar().ToString());
                u.NumShares = distinctUsers == 0 ? 0 : distinctUsers - 1; // Cannot count self, but if no space created, then 0
            }
            finally
            {
                if (cmd != null)
                    cmd.Dispose();
            }
        }

        public static void removeSpaceMembership(QueryConnection conn, string schema, Guid user_ID, Guid space_ID)
        {
            QueryCommand cmd = null;
            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = "DELETE FROM " + schema + "." + USER_SPACE_INXN +
                    " WHERE USER_ID=? AND SPACE_ID=?";
                cmd.Parameters.Add("USER_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(user_ID);
                cmd.Parameters.Add("SPACE_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(space_ID);
                cmd.ExecuteNonQuery();
            }
            finally
            {
                if (cmd != null)
                    cmd.Dispose();
            }
        }

        public static void addInvitation(QueryConnection conn, string schema, Space s, User inviteUser, string email)
        {
            QueryCommand cmd = null;
            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = "SELECT COUNT(*) FROM " + schema + "." + INVITES_TABLE_NAME +
                    " A WHERE A.SPACE_ID=? AND A.Email=?";
                cmd.Parameters.Add("SPACE_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(s.ID);
                cmd.Parameters.Add("Email", OdbcType.VarChar).Value = email;
                int result = Int32.Parse(cmd.ExecuteScalar().ToString());
                if (result == 0)
                {
                    cmd.Parameters.Clear();
                    cmd.CommandText = "INSERT INTO " + schema + "." + INVITES_TABLE_NAME +
                        " (SPACE_ID,Email,InviteDate,InviteUser) VALUES (?,?,?,?)";
                    cmd.Parameters.Add("SPACE_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(s.ID);
                    cmd.Parameters.Add("Email", OdbcType.VarChar).Value = email;
                    cmd.Parameters.Add("InviteDate", OdbcType.DateTime).Value = DateTime.Now;
                    cmd.Parameters.Add("InviteUser", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(inviteUser.ID);
                    cmd.ExecuteNonQuery();
                }
            }
            finally
            {
                if (cmd != null)
                    cmd.Dispose();
            }
        }

        public static void removeInvitation(QueryConnection conn, string schema, Guid space_ID, string email)
        {
            QueryCommand cmd = null;
            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = "DELETE FROM " + schema + "." + INVITES_TABLE_NAME +
                    " WHERE " + (space_ID != Guid.Empty ? "SPACE_ID=? AND " : "") + "Email=?";
                if (space_ID != Guid.Empty)
                    cmd.Parameters.Add("SPACE_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(space_ID);
                cmd.Parameters.Add("Email", OdbcType.VarChar).Value = email;
                cmd.ExecuteNonQuery();
            }
            finally
            {
                if (cmd != null)
                    cmd.Dispose();
            }
        }

        private static List<Invitation> getInvitations(QueryConnection conn, string schema, string email, Space sp)
        {
            QueryCommand cmd = null;
            QueryReader reader = null;
            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = "SELECT SPACE_ID,A.Email,InviteDate,C.Email AS InviteUserEmail,B.Name FROM " + schema + "." + INVITES_TABLE_NAME +
                    " A INNER JOIN " + schema + "." + SPACE_TABLE_NAME + " B ON A.SPACE_ID=B.ID" +
                    " INNER JOIN " + schema + "." + USER_TABLE_NAME + " C ON A.InviteUser=C.PKID" +
                    " WHERE B.Deleted IS NULL AND " + (email != null ? "A.Email=?" : "") + (sp != null ? "A.SPACE_ID=?" : "");
                if (email != null)
                    cmd.Parameters.Add("Email", OdbcType.VarChar).Value = email;
                else
                    cmd.Parameters.Add("SPACE_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(sp.ID);
                reader = cmd.ExecuteReader();
                List<Invitation> ilist = new List<Invitation>();
                while (reader.Read())
                {
                    Invitation inv = new Invitation();
                    inv.SpaceID = reader.GetGuid(0);
                    inv.Email = reader.GetString(1);
                    inv.InviteDate = reader.GetDateTime(2);
                    inv.InviteUserEmail = reader.GetString(3);
                    inv.SpaceName = reader.GetString(4);
                    ilist.Add(inv);
                }
                return (ilist);
            }
            finally
            {
                if (reader != null)
                    reader.Close();
                if (cmd != null)
                    cmd.Dispose();
            }
        }

        public static bool getNonUserInvitation(QueryConnection conn, string schema, string email)
        {
            QueryReader reader = null;
            QueryCommand cmd = null;
            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = "SELECT Email FROM " + schema + "." + INVITES_TABLE_NAME +
                    " A WHERE A.Email=?";
                cmd.Parameters.Add("Email", OdbcType.VarChar).Value = email;
                reader = cmd.ExecuteReader();
                bool result = false;
                while (reader.Read())
                {
                    result = true;
                    break;
                }
                return result;
            }
            finally
            {
                if (reader != null)
                    reader.Close();
                if (cmd != null)
                    cmd.Dispose();
            }
        }

        public static List<Invitation> getInvitations(QueryConnection conn, string schema, string email)
        {
            return (getInvitations(conn, schema, email, null));
        }

        public static List<Invitation> getInvitations(QueryConnection conn, string schema, Space sp)
        {
            return (getInvitations(conn, schema, null, sp));
        }

        public static void respondToInvitation(QueryConnection conn, string schema, User u, Guid space_ID, bool accept)
        {
            removeInvitation(conn, schema, space_ID, u.Email);
            if (accept)
                addSpaceMembership(conn, schema, u.ID, space_ID, false, false);
        }

        public static Space getSpace(QueryConnection conn, string spaceName, User u, bool active, String schema)
        {
            List<Acorn.SpaceMembership> spaces = Acorn.Database.getSpaces(conn, schema, u, active);
            foreach (SpaceMembership s in spaces)
            {
                if (s.Space.Name.Equals(spaceName))
                    return s.Space;
            }
            return null; // could not find the space by the given name
        }

        public static List<SpaceMembership> getSpaces(QueryConnection conn, string schema, User u, bool active)
        {
            List<SpaceMembership> mlist = new List<SpaceMembership>();
            QueryCommand cmd = conn.CreateCommand();
            cmd.CommandText =
                "SELECT A.ID,A.Name,A.Comments,A." + conn.getMSSQLReservedName("Schema") + ",A.Directory,A.LoadNumber,A.Automatic,A.Type,A.ConnectString,A.DatabaseType,A.DatabaseDriver,A.DatabaseLoadDir,A.AdminUser,A.AdminPwd,A.QueryConnectString,A.QueryDatabaseType,A.QueryDatabaseDriver,A.QueryConnectionName,A.QueryUser,A.QueryPwd,"
                + "A.Active,A.Folded,A.TestMode,A.Locked,A.SSO,A.SSOPWD,A.QueryLanguageVersion,A.MaxQueryRows,A.MaxQueryTimeout,"
                + "B.Admin,B.Adhoc,B.Dashboards,B.StartDate,B.Owner,D.Username,A.UsageTracking,A.ProcessVersion,A.AppVersion,A.UseDynamicGroups,A.AllowRetryFailedLoad, A.SFDCVersion,"
                + "A.DisallowExternalScheduler, A.SFDCNumThreads, A.Discovery, A.MapNullsToZero, A.AWSAccessKeyId, A.AWSSecretKey, A.ReportFrom, A.ReportSubject, A.DBTimeZone FROM "
                + schema + "." + SPACE_TABLE_NAME +
                " A INNER JOIN " + schema + "." + USER_SPACE_INXN +
                " B ON A.ID=B.SPACE_ID" +
                " INNER JOIN " + schema + "." + USER_SPACE_INXN +
                " C ON A.ID=C.SPACE_ID AND C.Owner=1" +
                " INNER JOIN " + schema + "." + USER_TABLE_NAME +
                " D ON C.USER_ID=D.PKID" +
                " WHERE A.Deleted IS NULL AND B.USER_ID=?";
            cmd.Parameters.Add("USER_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(u.ID);
            QueryReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                Space s = new Space();
                s.ID = reader.GetGuid(0);
                s.Name = reader.IsDBNull(1) ? null : reader.GetString(1);
                s.Comments = reader.IsDBNull(2) ? null : reader.GetString(2);
                s.Schema = reader.IsDBNull(3) ? null : reader.GetString(3);
                s.Directory = reader.IsDBNull(4) ? null : reader.GetString(4);
                s.LoadNumber = reader.GetInt32(5);
                s.Automatic = reader.GetBoolean(6);
                s.Type = reader.GetInt32(7);
                s.ConnectString = reader.IsDBNull(8) ? null : reader.GetString(8);
                s.DatabaseType = reader.IsDBNull(9) ? null : reader.GetString(9);
                s.DatabaseDriver = reader.IsDBNull(10) ? null : reader.GetString(10);
                s.DatabaseLoadDir = reader.IsDBNull(11) ? null : reader.GetString(11);
                s.AdminUser = reader.IsDBNull(12) ? null : reader.GetString(12);
                s.AdminPwd = reader.IsDBNull(13) ? null : Performance_Optimizer_Administration.MainAdminForm.decrypt(reader.GetString(13),
                Performance_Optimizer_Administration.MainAdminForm.SecretPassword);
                s.QueryConnectString = reader.IsDBNull(14) ? null : reader.GetString(14);
                s.QueryDatabaseType = reader.IsDBNull(15) ? null : reader.GetString(15);
                s.QueryDatabaseDriver = reader.IsDBNull(16) ? null : reader.GetString(16);
                s.QueryConnectionName = reader.IsDBNull(17) ? null : reader.GetString(17);
                s.QueryUser = reader.IsDBNull(18) ? null : reader.GetString(18);
                s.QueryPwd = reader.IsDBNull(19) ? null : Performance_Optimizer_Administration.MainAdminForm.decrypt(reader.GetString(19),
                Performance_Optimizer_Administration.MainAdminForm.SecretPassword);
                s.Active = reader.GetBoolean(20);
                s.Folded = reader.GetBoolean(21);
                s.TestMode = reader.GetBoolean(22);
                s.Locked = reader.GetBoolean(23);
                s.SSO = reader.IsDBNull(24) ? false : reader.GetBoolean(24);
                s.SSOPassword = reader.IsDBNull(25) ? null : Performance_Optimizer_Administration.MainAdminForm.decrypt(reader.GetString(25),
                        Performance_Optimizer_Administration.MainAdminForm.SecretPassword);
                s.QueryLanguageVersion = reader.IsDBNull(26) ? OLD_QUERY_LANGUAGE : reader.GetInt32(26);
                s.MaxQueryRows = reader.IsDBNull(27) ? MAX_QUERY_ROWS : reader.GetInt32(27);
                s.MaxQueryTimeout = reader.IsDBNull(28) ? MAX_QUERY_TIMEOUT : reader.GetInt32(28);
                SpaceMembership m = new SpaceMembership();
                m.Space = s;
                m.Administrator = reader.IsDBNull(29) ? false : reader.GetBoolean(29);
                m.User = u;
                m.Adhoc = reader.IsDBNull(30) ? false : reader.GetBoolean(30);
                m.Dashboards = reader.IsDBNull(31) ? true : reader.GetBoolean(31);
                m.StartDate = reader.GetDateTime(32);
                m.Owner = reader.IsDBNull(33) ? false : reader.GetBoolean(33);
                m.OwnerUsername = reader.IsDBNull(34) ? null : reader.GetString(34);
                s.UsageTracking = reader.IsDBNull(35) ? false : reader.GetBoolean(35);
                s.ProcessVersionID = reader.IsDBNull(36) ? Util.SPACE_PROCESS_VERSION_UNDEFINED : reader.GetInt32(36);
                s.AppVersion = reader.IsDBNull(37) ? 49 : reader.GetInt32(37);
                s.UseDynamicGroups = reader.IsDBNull(38) ? true : reader.GetBoolean(38);
                s.AllowRetryFailedLoad = reader.IsDBNull(39) ? false : reader.GetBoolean(39);
                s.SFDCVersionID = reader.IsDBNull(40) ? -1 : reader.GetInt32(40);
                s.DisallowExternalScheduler = reader.IsDBNull(41) ? false : reader.GetBoolean(41);
                s.SFDCNumThreads = reader.IsDBNull(42) ? -1 : reader.GetInt32(42);
                s.DiscoveryMode = reader.IsDBNull(43) ? false : reader.GetBoolean(43);
                s.MapNullsToZero = reader.IsDBNull(44) ? false : reader.GetBoolean(44);

                s.awsAccessKeyId = reader.IsDBNull(45) ? null : reader.GetString(45);
                s.awsSecretKey = reader.IsDBNull(46) ? null : reader.GetString(46);
                s.ScheduleFrom = reader.IsDBNull(47) ? null : reader.GetString(47);
                s.ScheduleSubject = reader.IsDBNull(48) ? null : reader.GetString(48);
                s.dbTimeZone = reader.IsDBNull(49) ? null : reader.GetString(49);
                if (!active || s.Active)
                    mlist.Add(m);
            }
            reader.Close();
            cmd.Dispose();
            mlist.Sort(SpaceMembership.compareByName);
            foreach (SpaceMembership sm in mlist)
            {
                Space s = sm.Space;
                if (Database.isDBTypeParAccel(s.ConnectString))
                {
                    string connectStringSuffix = getPADBConnectionStringSuffix(conn, schema, s);
                    if (connectStringSuffix != null && !connectStringSuffix.Trim().Equals("") && !s.ConnectString.EndsWith(connectStringSuffix))
                    {
                        s.ConnectString += connectStringSuffix;
                    }
                }
            }
            return (mlist);
        }


        public static Space getSpace(QueryConnection conn, string schema, Guid space_id)
        {
            return getSpace(conn, schema, space_id, null);
        }

        public static Space getSpace(QueryConnection conn, string schema, Guid space_id, string spaceDirectory)
        {
            QueryCommand cmd = conn.CreateCommand();
            cmd.CommandText =
                "SELECT A.ID,A.Name,A.Comments,A." + conn.getMSSQLReservedName("Schema") + ",A.Directory,A.LoadNumber,A.Automatic,A.Type,A.ConnectString,A.DatabaseType,A.DatabaseDriver,A.DatabaseLoadDir,A.AdminUser,A.AdminPwd,"
                + "A.QueryConnectString,A.QueryDatabaseType,A.QueryDatabaseDriver,A.QueryConnectionName,A.QueryUser,A.QueryPwd,"
                + "A.Active,A.Folded,A.TestMode,A.Locked,A.Demo,A.SSO,A.SSOPWD,A.QueryLanguageVersion,A.MaxQueryRows,A.MaxQueryTimeout,A.UsageTracking,A.ProcessVersion,A.AppVersion,"
                + "A.UseDynamicGroups,A.AllowRetryFailedLoad, A.SFDCVersion, A.DisallowExternalScheduler,"
                + "A.SFDCNumThreads, A.Discovery, A.MapNullsToZero, A.AWSAccessKeyId, A.AWSSecretKey, A.ReportFrom, A.ReportSubject, A.DBTimeZone FROM "
                + schema + "." + SPACE_TABLE_NAME +
                " A WHERE A.Deleted IS NULL ";
            if (space_id != null && space_id != Guid.Empty)
            {
                cmd.CommandText = cmd.CommandText + " AND A.ID=?";
                cmd.Parameters.Add("ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(space_id);

            }
            else if (spaceDirectory != null && spaceDirectory.Length > 0)
            {
                cmd.CommandText = cmd.CommandText + " AND A.Directory=?";
                cmd.Parameters.Add("Directory", OdbcType.VarChar).Value = spaceDirectory;
            }
            else
            {
                Global.systemLog.Error("Invalid space id or directory given : spaceID : " + space_id + "  : spaceDirectory : " + spaceDirectory);
                return null;
            }
            QueryReader reader = cmd.ExecuteReader();
            Space s = null;
            if (reader.Read())
            {
                s = new Space();
                s.ID = reader.GetGuid(0);
                s.Name = reader.IsDBNull(1) ? null : reader.GetString(1);
                s.Comments = reader.IsDBNull(2) ? null : reader.GetString(2);
                s.Schema = reader.IsDBNull(3) ? null : reader.GetString(3);
                s.Directory = reader.IsDBNull(4) ? null : reader.GetString(4);
                s.LoadNumber = reader.GetInt32(5);
                s.Automatic = reader.GetBoolean(6);
                s.Type = reader.GetInt32(7);
                s.ConnectString = reader.IsDBNull(8) ? null : reader.GetString(8);
                s.DatabaseType = reader.IsDBNull(9) ? null : reader.GetString(9);
                s.DatabaseDriver = reader.IsDBNull(10) ? null : reader.GetString(10);
                s.DatabaseLoadDir = reader.IsDBNull(11) ? null : reader.GetString(11);
                s.AdminUser = reader.IsDBNull(12) ? null : reader.GetString(12);
                s.AdminPwd = reader.IsDBNull(13) ? null : Performance_Optimizer_Administration.MainAdminForm.decrypt(reader.GetString(13),
                Performance_Optimizer_Administration.MainAdminForm.SecretPassword);
                s.QueryConnectString = reader.IsDBNull(14) ? null : reader.GetString(14);
                s.QueryDatabaseType = reader.IsDBNull(15) ? null : reader.GetString(15);
                s.QueryDatabaseDriver = reader.IsDBNull(16) ? null : reader.GetString(16);
                s.QueryConnectionName = reader.IsDBNull(17) ? null : reader.GetString(17);
                s.QueryUser = reader.IsDBNull(18) ? null : reader.GetString(18);
                s.QueryPwd = reader.IsDBNull(19) ? null : Performance_Optimizer_Administration.MainAdminForm.decrypt(reader.GetString(19),
                Performance_Optimizer_Administration.MainAdminForm.SecretPassword);
                s.Active = reader.GetBoolean(20);
                s.Folded = reader.GetBoolean(21);
                s.TestMode = reader.GetBoolean(22);
                s.Locked = reader.GetBoolean(23);
                s.Demo = reader.GetBoolean(24);
                s.SSO = reader.IsDBNull(25) ? false : reader.GetBoolean(25);
                s.SSOPassword = reader.IsDBNull(26) ? null : Performance_Optimizer_Administration.MainAdminForm.decrypt(reader.GetString(26),
                        Performance_Optimizer_Administration.MainAdminForm.SecretPassword);
                s.QueryLanguageVersion = reader.IsDBNull(27) ? OLD_QUERY_LANGUAGE : reader.GetInt32(27);
                s.MaxQueryRows = reader.IsDBNull(28) ? MAX_QUERY_ROWS : reader.GetInt32(28);
                s.MaxQueryTimeout = reader.IsDBNull(29) ? MAX_QUERY_TIMEOUT : reader.GetInt32(29);
                s.UsageTracking = reader.IsDBNull(30) ? false : reader.GetBoolean(30);
                s.ProcessVersionID = reader.IsDBNull(31) ? Util.SPACE_PROCESS_VERSION_UNDEFINED : reader.GetInt32(31);
                s.AppVersion = reader.IsDBNull(32) ? 49 : reader.GetInt32(32);
                s.UseDynamicGroups = reader.IsDBNull(33) ? true : reader.GetBoolean(33);
                s.AllowRetryFailedLoad = reader.IsDBNull(34) ? false : reader.GetBoolean(34);
                s.SFDCVersionID = reader.IsDBNull(35) ? -1 : reader.GetInt32(35);
                s.DisallowExternalScheduler = reader.IsDBNull(36) ? false : reader.GetBoolean(36);
                s.SFDCNumThreads = reader.IsDBNull(37) ? -1 : reader.GetInt32(37);
                s.DiscoveryMode = reader.IsDBNull(38) ? false : reader.GetBoolean(38);
                s.MapNullsToZero = reader.IsDBNull(39) ? false : reader.GetBoolean(39);

                s.awsAccessKeyId = reader.IsDBNull(40) ? null : reader.GetString(40);
                s.awsSecretKey = reader.IsDBNull(41) ? null : reader.GetString(41);
                s.ScheduleFrom = reader.IsDBNull(42) ? null : reader.GetString(42);
                s.ScheduleSubject = reader.IsDBNull(43) ? null : reader.GetString(43);
                s.dbTimeZone = reader.IsDBNull(44) ? null : reader.GetString(44);
            }
            reader.Close();
            cmd.Dispose();

            if (s != null && isDBTypeParAccel(s.ConnectString))
            {
                string connectStringSuffix = getPADBConnectionStringSuffix(conn, schema, s);
                if (connectStringSuffix != null && !connectStringSuffix.Trim().Equals("") && !s.ConnectString.EndsWith(connectStringSuffix))
                {
                    s.ConnectString += connectStringSuffix;
                }
            }
            return (s);
        }

        public static string getPADBConnectionStringSuffix(QueryConnection conn, string schema, Space sp)
        {
            User owner = Util.getSpaceOwner(sp);
            return getGlobalProperty(conn, schema, owner.ManagedAccountId, PADB_CONNECTIONSTRING_SUFFIX, true);
        }

        public static string getGlobalProperty(QueryConnection conn, string schema, Guid accountID, string propertyName, bool checkGlobal)
        {
            string propertyValue = null;
            propertyValue = getGlobalPropertyForAccount(conn, schema, accountID, propertyName);
            if ((propertyValue == null || propertyValue.Trim().Equals("")) && checkGlobal)
            {
                propertyValue = getGlobalPropertyForAccount(conn, schema, Guid.Empty, propertyName);
            }
            return propertyValue;
        }

        public static string getGlobalPropertyForAccount(QueryConnection conn, string schema, Guid accountID, string propertyName)
        {
            QueryCommand cmd = conn.CreateCommand();
            cmd.CommandText = "SELECT PropertyValue FROM " + schema + "." + GLOBAL_PROPERTIES_TABLE +
                " WHERE PropertyName=? AND Account_ID=?";
            cmd.Parameters.Add("@PropertyName", OdbcType.VarChar).Value = propertyName;
            cmd.Parameters.Add("@Account_ID", OdbcType.UniqueIdentifier).Value = accountID;
            QueryReader reader = cmd.ExecuteReader();
            string propertyValue = null;
            if (reader.Read())
            {
                propertyValue = reader.IsDBNull(0) ? null : reader.GetString(0);
            }
            cmd.Dispose();
            return propertyValue;
        }

        /**
         * insert a single space configuration entry
         */
        public static void createSpaceConfig(QueryConnection conn, string schema, SpaceConfig sc)
        {
            QueryCommand cmd = null;
            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = "INSERT INTO " + schema + "." + SPACES_CONFIG_TABLE + " ("
                        + "Type,URL,LocalURL,"
                        + "DatabaseConnectString,DatabaseType,DatabaseDriver,DatabaseLoadDir,"
                        + "AdminUser,AdminPwd,EngineCommand,DeleteDataCommand,"
                        + "QueryDatabaseConnectString,QueryDatabaseType,QueryDatabaseDriver,"
                        + "QueryUser,QueryPwd,QueryConnectionName,AWSAccessKeyId,AWSSecretKey, "
                        + "Name, Description, DBTimeZone, RegionID)"
                        + " VALUES("
                        + "?,?,?," // type, url, localurl
                        + "?,?,?,?," // database*
                        + "?,?,?,?," // admin*, *command
                        + "?,?,?,?,?,?," // query*
                        + "?,?," // AWS
                        + "?,?,?,?)"; // name, description, dbTimeZone, regionid
                // set parameters
                DbParameter ptype = cmd.Parameters.Add("Type", OdbcType.Int);
                DbParameter purl = cmd.Parameters.Add("URL", OdbcType.VarChar);
                DbParameter plocalurl = cmd.Parameters.Add("LocalURL", OdbcType.VarChar);

                DbParameter pdbcs = cmd.Parameters.Add("DatabaseConnectString", OdbcType.VarChar);
                DbParameter pdt = cmd.Parameters.Add("DatabaseType", OdbcType.VarChar);
                DbParameter pddr = cmd.Parameters.Add("DatabaseDriver", OdbcType.VarChar);
                DbParameter pdldr = cmd.Parameters.Add("DatabaseLoadDir", OdbcType.VarChar);

                DbParameter pau = cmd.Parameters.Add("AdminUser", OdbcType.VarChar);
                DbParameter pap = cmd.Parameters.Add("AdminPwd", OdbcType.VarChar);
                DbParameter pec = cmd.Parameters.Add("EngineCommand", OdbcType.VarChar);
                DbParameter pddc = cmd.Parameters.Add("DeleteDataCommand", OdbcType.VarChar);

                DbParameter pqdbcs = cmd.Parameters.Add("QueryDatabaseConnectString", OdbcType.VarChar);
                DbParameter pqdbt = cmd.Parameters.Add("QueryDatabaseType", OdbcType.VarChar);
                DbParameter pqdbdr = cmd.Parameters.Add("QueryDatabaseDriver", OdbcType.VarChar);
                DbParameter pqdu = cmd.Parameters.Add("QueryUser", OdbcType.VarChar);
                DbParameter pqdp = cmd.Parameters.Add("QueryPwd", OdbcType.VarChar);
                DbParameter pqdcn = cmd.Parameters.Add("QueryConnectionName", OdbcType.VarChar);
                DbParameter paaki = cmd.Parameters.Add("AWSAccessKeyId", OdbcType.VarChar);
                DbParameter pasak = cmd.Parameters.Add("AWSSecretKey", OdbcType.VarChar);
                DbParameter pname = cmd.Parameters.Add("Name", OdbcType.NVarChar);
                DbParameter pdesc = cmd.Parameters.Add("Description", OdbcType.NVarChar);
                DbParameter pdbtz = cmd.Parameters.Add("DBTimeZone", OdbcType.NVarChar);
                DbParameter pRegion = cmd.Parameters.Add("RegionID", OdbcType.Int);

                ptype.Value = sc.Type;
                if (sc.URL != null)
                    purl.Value = sc.URL;
                else
                    purl.Value = System.DBNull.Value;

                if (sc.LocalURL != null)
                    plocalurl.Value = sc.LocalURL;
                else
                    plocalurl.Value = System.DBNull.Value;

                pdbcs.Value = sc.DatabaseConnectString;
                pdt.Value = sc.DatabaseType;
                pddr.Value = sc.DatabaseDriver;
                pdldr.Value = sc.DatabaseLoadDir;
                pau.Value = Performance_Optimizer_Administration.MainAdminForm.encrypt(sc.AdminUser, Performance_Optimizer_Administration.MainAdminForm.SecretPassword);
                pap.Value = Performance_Optimizer_Administration.MainAdminForm.encrypt(sc.AdminPwd, Performance_Optimizer_Administration.MainAdminForm.SecretPassword);
                if (sc.EngineCommand != null)
                    pec.Value = sc.EngineCommand;
                else
                    pec.Value = System.DBNull.Value;
                if (sc.DeleteDataCommand != null)
                    pddc.Value = sc.DeleteDataCommand;
                else
                    pddc.Value = System.DBNull.Value;
                if (sc.QueryDatabaseConnectString != null)
                {
                    pqdbcs.Value = sc.QueryDatabaseConnectString;
                    pqdbt.Value = sc.QueryDatabaseType;
                    pqdbdr.Value = sc.QueryDatabaseDriver;
                    pqdu.Value = Performance_Optimizer_Administration.MainAdminForm.encrypt(sc.QueryUser, Performance_Optimizer_Administration.MainAdminForm.SecretPassword);
                    pqdp.Value = Performance_Optimizer_Administration.MainAdminForm.encrypt(sc.QueryPwd, Performance_Optimizer_Administration.MainAdminForm.SecretPassword);
                    pqdcn.Value = sc.QueryConnectionName;
                }
                else
                {
                    pqdbcs.Value = System.DBNull.Value;
                    pqdbt.Value = System.DBNull.Value;
                    pqdbdr.Value = System.DBNull.Value;
                    pqdu.Value = System.DBNull.Value;
                    pqdp.Value = System.DBNull.Value;
                    pqdcn.Value = System.DBNull.Value;
                }
                if (sc.awsAccessKeyId != null)
                    paaki.Value = sc.awsAccessKeyId;
                else
                    paaki.Value = System.DBNull.Value;

                if (sc.awsSecretKey != null)
                    pasak.Value = sc.awsSecretKey;
                else
                    pasak.Value = System.DBNull.Value;


                if (sc.Name != null)
                    pname.Value = sc.Name;
                else
                    pname.Value = System.DBNull.Value;

                if (sc.Description != null)
                    pdesc.Value = sc.Description;
                else
                    pdesc.Value = System.DBNull.Value;

                if (sc.dbTimeZone != null)
                    pdbtz.Value = sc.dbTimeZone;
                else
                    pdbtz.Value = System.DBNull.Value;

                if (sc.regionId > 0)
                    pRegion.Value = sc.regionId;
                else
                    pRegion.Value = System.DBNull.Value;
                cmd.ExecuteNonQuery();
            }
            finally
            {
                if (cmd != null)
                    cmd.Dispose();
            }
        }

        public static void removeSpaceConfig(QueryConnection conn, string schema, int typeID)
        {
            QueryCommand cmd = null;
            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = "DELETE FROM " + schema + "." + SPACES_CONFIG_TABLE +
                    " WHERE Type=?";
                cmd.Parameters.Add("Type", OdbcType.Int).Value = typeID;
                cmd.ExecuteNonQuery();
            }
            finally
            {
                if (cmd != null)
                    cmd.Dispose();
            }
        }

        /// <summary>
        /// The list is a shallow list with only limited set of information in space membership
        /// Only use it if the information required is limited to space config (type) details
        /// </summary>
        /// <param name="conn"></param>
        /// <param name="schema"></param>
        /// <param name="sc"></param>
        /// <param name="accountID"></param>
        /// <returns></returns>
        public static List<SpaceMembership> getSpaceTypesUsage(QueryConnection conn, string schema, SpaceConfig sc, Guid accountID)
        {
            QueryCommand cmd = null;
            List<SpaceMembership> response = new List<SpaceMembership>();
            try
            {
                cmd = conn.CreateCommand();
                string query =
                "SELECT A.ID,A.Name,A." + conn.getMSSQLReservedName("Schema") + ",A.Directory,A.Type,A.ConnectString, "
                + "A.DatabaseType,A.DatabaseDriver,A.DatabaseLoadDir,A.AdminUser,A.AdminPwd,"
                + "A.QueryConnectString,A.QueryDatabaseType,A.QueryDatabaseDriver,A.QueryConnectionName,A.QueryUser,A.QueryPwd,"
                + "A.Active, A.AWSAccessKeyId, A.AWSSecretKey,"
                + "B.Admin, B.Owner,C.Username FROM "
                + schema + "." + SPACE_TABLE_NAME +
                " A INNER JOIN " + schema + "." + USER_SPACE_INXN +
                " B ON A.ID=B.SPACE_ID AND B.Owner=1" +
                " INNER JOIN " + schema + "." + USER_TABLE_NAME +
                " C ON B.USER_ID=C.PKID" +
                " WHERE A.Deleted IS NULL AND A.Type=?";
                cmd.Parameters.Add("@Type", OdbcType.Int).Value = sc.Type;
                if (accountID != Guid.Empty)
                {
                    query = query + " AND C.MANAGED_ACCOUNT_ID = ?";
                    cmd.Parameters.Add("@MANAGED_ACCOUNT_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(accountID);
                }
                cmd.CommandText = query;
                QueryReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    Space s = new Space();
                    int i = 0;
                    s.ID = reader.GetGuid(i); i++;
                    s.Name = reader.IsDBNull(i) ? null : reader.GetString(i); i++;
                    s.Schema = reader.IsDBNull(i) ? null : reader.GetString(i); i++;
                    s.Directory = reader.IsDBNull(i) ? null : reader.GetString(i); i++;
                    s.Type = reader.GetInt32(i); i++;
                    s.ConnectString = reader.IsDBNull(i) ? null : reader.GetString(i); i++;
                    s.DatabaseType = reader.IsDBNull(i) ? null : reader.GetString(i); i++;
                    s.DatabaseDriver = reader.IsDBNull(i) ? null : reader.GetString(i); i++;
                    s.DatabaseLoadDir = reader.IsDBNull(i) ? null : reader.GetString(i); i++;
                    s.AdminUser = reader.IsDBNull(i) ? null : reader.GetString(i); i++;
                    s.AdminPwd = Performance_Optimizer_Administration.MainAdminForm.decrypt(reader.GetString(i),
                    Performance_Optimizer_Administration.MainAdminForm.SecretPassword); i++;
                    s.QueryConnectString = reader.IsDBNull(i) ? null : reader.GetString(i); i++;
                    s.QueryDatabaseType = reader.IsDBNull(i) ? null : reader.GetString(i); i++;
                    s.QueryDatabaseDriver = reader.IsDBNull(i) ? null : reader.GetString(i); i++;
                    s.QueryConnectionName = reader.IsDBNull(i) ? null : reader.GetString(i); i++;
                    s.QueryUser = reader.IsDBNull(i) ? null : reader.GetString(i); i++;
                    s.QueryPwd = reader.IsDBNull(i) ? null : Performance_Optimizer_Administration.MainAdminForm.decrypt(reader.GetString(i),
                    Performance_Optimizer_Administration.MainAdminForm.SecretPassword); i++;
                    s.Active = reader.GetBoolean(i); i++;
                    s.awsAccessKeyId = reader.IsDBNull(i) ? null : reader.GetString(i); i++;
                    s.awsSecretKey = reader.IsDBNull(i) ? null : reader.GetString(i); i++;
                    SpaceMembership sm = new SpaceMembership();
                    sm.Space = s;
                    sm.Administrator = reader.GetBoolean(i); i++;
                    sm.Owner = reader.GetBoolean(i); i++;
                    sm.OwnerUsername = reader.GetString(i); i++;
                    response.Add(sm);
                }
                return response;
            }
            finally
            {
                if (cmd != null)
                    cmd.Dispose();
            }
        }

        public static AWSRegion getRedshiftRegionDetails(QueryConnection conn, string schema, string regionName)
        {
            QueryCommand cmd = null;
            QueryReader result = null;
            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = "SELECT ID, Name, TimeBucket, UploadBucket, Active FROM " + getTableName(schema, AWS_REGIONS) +
                    " WHERE Name=?";
                cmd.Parameters.Add("Name", OdbcType.NVarChar).Value = regionName;
                result = cmd.ExecuteReader();
                while (result.Read())
                {
                    AWSRegion aRegion = new AWSRegion();
                    aRegion.ID = result.GetInt32(0);
                    aRegion.Name = result.GetString(1);
                    aRegion.TimeBucket = result.GetString(2);
                    aRegion.UploadBucket = result.GetString(3);
                    aRegion.Active = result.GetBoolean(4);
                    return aRegion;
                }
                return null;
            }
            finally
            {
                try
                {
                    if (result != null)
                        result.Close();
                    if (cmd != null)
                        cmd.Dispose();
                }
                catch (Exception) { }
            }
        }

        public static AWSRegion getRedshiftRegionDetails(QueryConnection conn, string schema, int regionId)
        {
            QueryCommand cmd = null;
            QueryReader result = null;
            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = "SELECT ID, Name, TimeBucket, UploadBucket, Active FROM " + getTableName(schema, AWS_REGIONS) +
                    " WHERE ID=?";
                cmd.Parameters.Add("ID", OdbcType.Int).Value = regionId;
                result = cmd.ExecuteReader();
                while (result.Read())
                {
                    AWSRegion aRegion = new AWSRegion();
                    aRegion.ID = result.GetInt32(0);
                    aRegion.Name = result.GetString(1);
                    aRegion.TimeBucket = result.GetString(2);
                    aRegion.UploadBucket = result.GetString(3);
                    aRegion.Active = result.GetBoolean(4);
                    return aRegion;
                }
                return null;
            }
            finally
            {
                try
                {
                    if (result != null)
                        result.Close();
                    if (cmd != null)
                        cmd.Dispose();
                }
                catch (Exception) { }
            }
        }

        public static int getMaxRedshiftRegionID(QueryConnection conn, string schema)
        {
            QueryCommand cmd = null;
            try
            {
                bool isOracle = conn.isDBTypeOracle();
                cmd = conn.CreateCommand();
                cmd.CommandText = "SELECT Max(ID) FROM " + getTableName(schema, AWS_REGIONS);
                object o = cmd.ExecuteScalar();
                if (o == null || o is DBNull)
                    return (-1);
                else
                {
                    return isOracle ? Int32.Parse(o.ToString()) : ((int)o);
                }
            }
            finally
            {
                if (cmd != null)
                    cmd.Dispose();
            }
        }

        public static void addRedshiftAWSRegion(QueryConnection conn, string schema, int id, string name, string timeBucket, string uploadBucket, bool active)
        {
            QueryCommand cmd = null;
            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = "INSERT INTO " + getTableName(schema, AWS_REGIONS) +
                    " (ID, Name, TimeBucket, UploadBucket, Active, CreatedDate) VALUES (?,?,?,?,?," + conn.getCurrentTimestamp() + ")";
                cmd.Parameters.Add("ID", OdbcType.Int).Value = id;
                cmd.Parameters.Add("Name", OdbcType.VarChar).Value = name;
                cmd.Parameters.Add("TimeBucket", OdbcType.VarChar).Value = timeBucket;
                cmd.Parameters.Add("UploadBucket", OdbcType.VarChar).Value = uploadBucket;
                cmd.Parameters.Add("Active", conn.getODBCBooleanType()).Value = conn.getBooleanValue(active);
                cmd.ExecuteNonQuery();
            }
            finally
            {
                if (cmd != null)
                    cmd.Dispose();
            }
        }



        public static List<AWSRegion> getRedshiftAvailableRegions(QueryConnection conn, string schema)
        {
            List<AWSRegion> awsRegions = new List<AWSRegion>();
            QueryCommand cmd = null;
            QueryReader result = null;
            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = "SELECT ID, Name, TimeBucket, UploadBucket, Active FROM " + getTableName(schema, AWS_REGIONS);
                result = cmd.ExecuteReader();
                while (result.Read())
                {
                    bool anyNull = false;
                    for (int i = 0; i < 5; i++)
                    {
                        if (result.IsDBNull(i))
                        {
                            Global.systemLog.Warn("Null value for " + result.GetName(i));
                            anyNull = true;
                            break;
                        }
                    }
                    if (anyNull)
                    {
                        continue;
                    }

                    AWSRegion aRegion = new AWSRegion();
                    aRegion.ID = result.GetInt32(0);
                    aRegion.Name = result.GetString(1);
                    aRegion.TimeBucket = result.GetString(2);
                    aRegion.UploadBucket = result.GetString(3);
                    aRegion.Active = result.GetBoolean(4);
                    awsRegions.Add(aRegion);
                }
                return awsRegions;
            }
            finally
            {
                try
                {
                    if (result != null)
                        result.Close();
                    if (cmd != null)
                        cmd.Dispose();
                }
                catch (Exception) { }
            }
        }


        public static void updateRedshiftClusterSpaceConfig(QueryConnection conn, string schema, SpaceConfig sc, OdbcTransaction transaction)
        {
            QueryCommand cmd = null;
            try
            {
                cmd = conn.CreateCommand();
                if (transaction != null)
                    cmd.Transaction = transaction;
                cmd.CommandText = "UPDATE " + schema + "." + SPACES_CONFIG_TABLE
                        + " set DatabaseConnectString=?,DatabaseLoadDir=?,"
                        + " AdminUser=?,AdminPwd=?,"
                        + " AWSAccessKeyId=?,AWSSecretKey=?,Name=?,Description=?,RegionID=? "
                        + " WHERE Type=? And DatabaseType=?";
                // set parameters                
                DbParameter pdbcs = cmd.Parameters.Add("DatabaseConnectString", OdbcType.VarChar);
                DbParameter pdldr = cmd.Parameters.Add("DatabaseLoadDir", OdbcType.VarChar);
                DbParameter pau = cmd.Parameters.Add("AdminUser", OdbcType.VarChar);
                DbParameter pap = cmd.Parameters.Add("AdminPwd", OdbcType.VarChar);

                DbParameter paaki = cmd.Parameters.Add("AWSAccessKeyId", OdbcType.VarChar);
                DbParameter pasak = cmd.Parameters.Add("AWSSecretKey", OdbcType.VarChar);
                DbParameter pname = cmd.Parameters.Add("Name", OdbcType.NVarChar);
                DbParameter pdesc = cmd.Parameters.Add("Description", OdbcType.NVarChar);
                DbParameter pregion = cmd.Parameters.Add("RegionID", OdbcType.Int);

                // where parameter
                DbParameter ptype = cmd.Parameters.Add("Type", OdbcType.Int);
                DbParameter pdt = cmd.Parameters.Add("DatabaseType", OdbcType.VarChar);

                pdbcs.Value = sc.DatabaseConnectString;
                pdldr.Value = sc.DatabaseLoadDir;
                pau.Value = Performance_Optimizer_Administration.MainAdminForm.encrypt(sc.AdminUser, Performance_Optimizer_Administration.MainAdminForm.SecretPassword);
                pap.Value = Performance_Optimizer_Administration.MainAdminForm.encrypt(sc.AdminPwd, Performance_Optimizer_Administration.MainAdminForm.SecretPassword);

                if (sc.awsAccessKeyId != null)
                    paaki.Value = sc.awsAccessKeyId;
                else
                    paaki.Value = System.DBNull.Value;

                if (sc.awsSecretKey != null)
                    pasak.Value = sc.awsSecretKey;
                else
                    pasak.Value = System.DBNull.Value;

                pname.Value = sc.Name;
                if (sc.Description != null)
                    pdesc.Value = sc.Description;
                else
                    pdesc.Value = System.DBNull.Value;

                if (sc.regionId > 0)
                    pregion.Value = sc.regionId;
                else
                    pregion.Value = System.DBNull.Value;

                ptype.Value = sc.Type;
                pdt.Value = Database.REDSHIFT;
                cmd.ExecuteNonQuery();
            }
            finally
            {
                if (cmd != null)
                    cmd.Dispose();
            }
        }


        public static void updateSpaceConfigInSpacesTable(QueryConnection conn, string schema, Guid spaceID, SpaceConfig sc, OdbcTransaction transaction)
        {
            QueryCommand cmd = null;
            try
            {
                cmd = conn.CreateCommand();
                if (transaction != null)
                {
                    cmd.Transaction = transaction;
                }
                cmd.CommandText = "UPDATE " + schema + "." + SPACE_TABLE_NAME
                        + " set ConnectString=?,DatabaseType=?,DatabaseDriver=?,DatabaseLoadDir=?,"
                        + "AdminUser=?,AdminPwd=?,"
                        + "AWSAccessKeyId=?,AWSSecretKey=?,DBTimeZone=? "
                        + " WHERE Type=? AND ID=?";
                // set parameters                
                DbParameter pdbcs = cmd.Parameters.Add("ConnectString", OdbcType.VarChar);
                DbParameter pdt = cmd.Parameters.Add("DatabaseType", OdbcType.VarChar);
                DbParameter pddr = cmd.Parameters.Add("DatabaseDriver", OdbcType.VarChar);
                DbParameter pdldr = cmd.Parameters.Add("DatabaseLoadDir", OdbcType.VarChar);

                DbParameter pau = cmd.Parameters.Add("AdminUser", OdbcType.VarChar);
                DbParameter pap = cmd.Parameters.Add("AdminPwd", OdbcType.VarChar);

                DbParameter paaki = cmd.Parameters.Add("AWSAccessKeyId", OdbcType.VarChar);
                DbParameter pasak = cmd.Parameters.Add("AWSSecretKey", OdbcType.VarChar);
                DbParameter pdbtz = cmd.Parameters.Add("DBTimeZone", OdbcType.VarChar);

                // where parameter
                DbParameter ptype = cmd.Parameters.Add("Type", OdbcType.Int);
                DbParameter pspaceid = cmd.Parameters.Add("ID", conn.getODBCUniqueIdentiferType());

                pdbcs.Value = sc.DatabaseConnectString;
                pdt.Value = sc.DatabaseType;
                pddr.Value = sc.DatabaseDriver;
                pdldr.Value = sc.DatabaseLoadDir;
                pau.Value = sc.AdminUser;
                pap.Value = Performance_Optimizer_Administration.MainAdminForm.encrypt(sc.AdminPwd, Performance_Optimizer_Administration.MainAdminForm.SecretPassword);

                if (sc.awsAccessKeyId != null)
                    paaki.Value = sc.awsAccessKeyId;
                else
                    paaki.Value = System.DBNull.Value;

                if (sc.awsSecretKey != null)
                    pasak.Value = sc.awsSecretKey;
                else
                    pasak.Value = System.DBNull.Value;

                if (sc.dbTimeZone != null)
                    pdbtz.Value = sc.dbTimeZone;
                else
                    pdbtz.Value = System.DBNull.Value;

                ptype.Value = sc.Type;
                pspaceid.Value = conn.getUniqueIdentifer(spaceID);
                cmd.ExecuteNonQuery();
            }
            finally
            {
                if (cmd != null)
                    cmd.Dispose();
            }
        }

        public static int getMaxSpaceConfigTypeID(QueryConnection conn, string schema)
        {
            QueryCommand cmd = null;
            try
            {
                bool isOracle = conn.isDBTypeOracle();
                cmd = conn.CreateCommand();
                cmd.CommandText = "SELECT Max(Type) FROM " + schema + "." + SPACES_CONFIG_TABLE;
                object o = cmd.ExecuteScalar();
                if (o == null)
                    return (-1);
                else
                {
                    return isOracle ? Int32.Parse(o.ToString()) : ((int)o);
                }
            }
            finally
            {
                if (cmd != null)
                    cmd.Dispose();
            }
        }

        /**
         * insert new space configuration entries (from the spaces.config file) if they don't exist in the SPACES_CONFIG table
         * - this DOES NOT update existing entries
         * - long term we will remove the .config files
         */
        public static void addNewSpaceConfigs(QueryConnection conn, string schema, Dictionary<int, SpaceConfig> configurations)
        {
            List<int> missing = new List<int>();
            List<int> changed = new List<int>();
            foreach (int type in configurations.Keys)
            {
                try
                {
                    // determine what is missing and what has changed (for manaul updating)
                    SpaceConfig sc = getSpaceConfig(conn, schema, type);
                    if (sc == null)
                        missing.Add(type);
                    else if (!sc.Equals(configurations[type]))
                    {
                        changed.Add(type);
                    }
                }
                catch (Exception ex)
                {
                    Global.systemLog.Warn(ex, ex);
                }
            }

            if (missing.Count == 0)
            {
                Global.systemLog.Info("No new entries added to the " + SPACES_CONFIG_TABLE + " table");
            }
            else
            {
                foreach (int tp in missing)
                {
                    // set specific values
                    SpaceConfig sc = configurations[tp];
                    createSpaceConfig(conn, mainSchema, sc);
                }
                Global.systemLog.Info("Added " + missing.Count + " entries to the " + SPACES_CONFIG_TABLE + " table");
            }

            if (changed.Count > 0)
            {
                Global.systemLog.Info(changed.Count + " entries changed in the spaces.config file, but they are not being updated automatically, please manually update the " + SPACES_CONFIG_TABLE + " table if necessary");
            }
        }

        public static SpaceConfig getSpaceConfig(QueryConnection conn, string schema, int type)
        {
            SpaceConfig s = null;
            QueryCommand cmd = null;
            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = "SELECT A.Type,A.URL,A.LocalURL,A.DatabaseConnectString,A.DatabaseType,A.DatabaseDriver,A.DatabaseLoadDir,A.AdminUser,A.AdminPwd,"
                 + "A.EngineCommand,A.DeleteDataCommand,A.QueryDatabaseConnectString,A.QueryDatabaseType,A.QueryDatabaseDriver,A.QueryUser,A.QueryPwd,A.QueryConnectionName,"
                 + "A.AWSAccessKeyId,A.AWSSecretKey, A.Name, A.Description, A.DBTimeZone, A.RegionID"
                 + " FROM " + schema + "." + SPACES_CONFIG_TABLE + " A WHERE A.Type=?";
                cmd.Parameters.Add("Type", OdbcType.Int).Value = type;
                QueryReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    s = new SpaceConfig();
                    int i = 0;
                    s.Type = reader.GetInt32(i); i++;
                    s.URL = reader.IsDBNull(i) ? null : reader.GetString(i); i++;
                    s.LocalURL = reader.IsDBNull(i) ? null : reader.GetString(i); i++;
                    s.DatabaseConnectString = reader.IsDBNull(i) ? null : reader.GetString(i); i++;
                    s.DatabaseType = reader.IsDBNull(i) ? null : reader.GetString(i); i++;
                    s.DatabaseDriver = reader.IsDBNull(i) ? null : reader.GetString(i); i++;
                    s.DatabaseLoadDir = reader.IsDBNull(i) ? null : reader.GetString(i); i++;
                    s.AdminUser = reader.IsDBNull(i) ? null : Performance_Optimizer_Administration.MainAdminForm.decrypt(reader.GetString(i), Performance_Optimizer_Administration.MainAdminForm.SecretPassword); i++;
                    s.AdminPwd = reader.IsDBNull(i) ? null : Performance_Optimizer_Administration.MainAdminForm.decrypt(reader.GetString(i), Performance_Optimizer_Administration.MainAdminForm.SecretPassword); i++;
                    s.EngineCommand = reader.IsDBNull(i) ? null : reader.GetString(i); i++;
                    s.DeleteDataCommand = reader.IsDBNull(i) ? null : reader.GetString(i); i++;
                    s.QueryDatabaseConnectString = reader.IsDBNull(i) ? null : reader.GetString(i); i++;
                    s.QueryDatabaseType = reader.IsDBNull(i) ? null : reader.GetString(i); i++;
                    s.QueryDatabaseDriver = reader.IsDBNull(i) ? null : reader.GetString(i); i++;
                    s.QueryUser = reader.IsDBNull(i) ? null : Performance_Optimizer_Administration.MainAdminForm.decrypt(reader.GetString(i), Performance_Optimizer_Administration.MainAdminForm.SecretPassword); i++;
                    s.QueryPwd = reader.IsDBNull(i) ? null : Performance_Optimizer_Administration.MainAdminForm.decrypt(reader.GetString(i), Performance_Optimizer_Administration.MainAdminForm.SecretPassword); i++;
                    s.QueryConnectionName = reader.IsDBNull(i) ? null : reader.GetString(i); i++;
                    s.awsAccessKeyId = reader.IsDBNull(i) ? null : reader.GetString(i); i++;
                    s.awsSecretKey = reader.IsDBNull(i) ? null : reader.GetString(i); i++;
                    s.Name = reader.IsDBNull(i) ? null : reader.GetString(i); i++;
                    s.Description = reader.IsDBNull(i) ? null : reader.GetString(i); i++;
                    s.dbTimeZone = reader.IsDBNull(i) ? null : reader.GetString(i); i++;
                    s.regionId = reader.IsDBNull(i) ? -1 : reader.GetInt32(i); i++;
                }
            }
            finally
            {
                if (cmd != null)
                    cmd.Dispose();
            }
            return s;
        }

        /**
         * get a new space configuration type
         * - uses a SEQUENCE
         * - CREATE SEQUENCE spaceTypeSequence START WITH 1000000 INCREMENT BY 1
         * - where 10000 is the starting point for the automatically generated values, pick a value larger than any value to be used for the manually created types
         * - XXX only works for SQL Server 2012 right now
         */
        public static int getNextSpaceType(QueryConnection conn, string schema)
        {
            string SPACE_TYPE_SEQUENCE = "spaceTypeSequence";
            QueryCommand cmd = conn.CreateCommand();
            try
            {
                cmd.CommandText = "select NEXT VALUE FOR " + schema + "." + SPACE_TYPE_SEQUENCE;
                QueryReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    return reader.GetInt32(0);
                }
            }
            finally
            {
                if (cmd != null)
                    cmd.Dispose();
            }
            return -1;
        }

        public static void addProxyUser(QueryConnection conn, string schema, User user, User proxy, bool repAdmin)
        {
            addProxyUser(conn, schema, user, proxy, repAdmin, DateTime.MaxValue);
        }

        public static void addProxyUser(QueryConnection conn, string schema, User user, User proxy, bool repAdmin, DateTime ExpirationDate)
        {
            QueryCommand cmd = conn.CreateCommand();
            if (ExpirationDate == DateTime.MaxValue)
            {
                cmd.CommandText = "INSERT INTO "
                                    + schema + "." + PROXY_USERS
                                    + " (USER_ID,PROXY_USER_ID,RepositoryAdmin) VALUES(?,?,?)";
            }
            else
            {
                cmd.CommandText = "INSERT INTO "
                    + schema + "." + PROXY_USERS
                    + " (USER_ID,PROXY_USER_ID,RepositoryAdmin,ExpirationDate) VALUES(?,?,?,?)";
            }
            cmd.Parameters.Add("USER_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(user.ID);
            cmd.Parameters.Add("PROXY_USER_ID", conn.getODBCUniqueIdentiferType()).Value = (proxy == null) ? (object)System.DBNull.Value : conn.getUniqueIdentifer(proxy.ID);
            cmd.Parameters.Add("RepositoryAdmin", conn.getODBCBooleanType()).Value = (repAdmin ? 1 : 0);
            if (ExpirationDate != DateTime.MaxValue)
                cmd.Parameters.Add("ExpirationDate", OdbcType.DateTime).Value = ExpirationDate;
            cmd.ExecuteNonQuery();
            cmd.Dispose();
            Global.systemLog.Info("added proxy user record: " + user.Username + ", " + ((proxy == null) ? "global" : proxy.Username));
        }

        public static bool removeProxyUser(QueryConnection conn, string schema, User user, User proxy)
        {
            QueryCommand cmd = conn.CreateCommand();
            cmd.CommandText = "DELETE FROM "
                                + schema + "." + PROXY_USERS
                                + " WHERE USER_ID=? AND PROXY_USER_ID=?";
            cmd.Parameters.Add("USER_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(user.ID);
            cmd.Parameters.Add("PROXY_USER_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(proxy.ID);
            int count = cmd.ExecuteNonQuery();
            cmd.Dispose();
            if (count == 0)
                return false;
            Global.systemLog.Info("removed proxy user record: " + user.Username + ", " + proxy.Username);
            return true;
        }

        public static bool isProxy(QueryConnection conn, string schema, User u, User proxy, bool inTrustedNetblock, ref bool repAdmin)
        {
            QueryCommand cmd = null;
            QueryReader reader = null;
            try
            {
                cmd = conn.CreateCommand();

                // try specific query first
                cmd.CommandText =
                     "SELECT RepositoryAdmin FROM "
                     + schema + "." + PROXY_USERS +
                     " WHERE USER_ID=? AND PROXY_USER_ID=? AND (ExpirationDate IS NULL OR ExpirationDate > ?)";
                cmd.Parameters.Add("USER_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(u.ID);
                cmd.Parameters.Add("PROXY_USER_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(proxy.ID);
                cmd.Parameters.Add("ExpirationDate", OdbcType.DateTime).Value = DateTime.Now;
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    repAdmin = reader.GetBoolean(0);
                    return true;
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Debug(ex.Message);
                return false;
            }
            finally
            {
                if (reader != null)
                    reader.Close();
                if (cmd != null)
                    cmd.Dispose();
            }

            // try global, but not allowed outside of trusted netblocks
            if (!inTrustedNetblock)
                return false;

            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText =
                     "SELECT RepositoryAdmin FROM "
                     + schema + "." + PROXY_USERS +
                     " WHERE USER_ID=? AND PROXY_USER_ID IS NULL AND (ExpirationDate IS NULL OR ExpirationDate > ?)";
                cmd.Parameters.Add("USER_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(u.ID);
                cmd.Parameters.Add("ExpirationDate", OdbcType.DateTime).Value = DateTime.Now;
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    repAdmin = reader.GetBoolean(0);
                    return true;
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Debug(ex.Message);
                return false;
            }
            finally
            {
                if (reader != null)
                    reader.Close();
                if (cmd != null)
                    cmd.Dispose();
            }
            return false;
        }

        public static bool isSpaceAdmin(QueryConnection conn, string schema, User user, Space space)
        {
            List<SpaceMembership> mlist = Database.getSpaces(conn, schema, user, true);
            for (int i = 0; i < mlist.Count; i++)
            {
                if (mlist[i].Space.ID == space.ID)
                {
                    return mlist[i].Administrator;
                }
            }
            return false;
        }

        /**
         * add ip address and netblock for an account or user
         * - set accountId or userId to Guid.Empty is not setting that item
         * - ipaddress is a either an ip address (a.b.c.d) or a netblock (a.b.c.d/n)
         */
        public static void addAllowedIP(QueryConnection conn, string schema, Guid accountId, Guid userId, string ipaddress)
        {
            QueryCommand cmd = null;
            try
            {
                cmd = conn.CreateCommand();
                if (accountId != Guid.Empty)
                {
                    cmd.CommandText = "INSERT INTO "
                                        + schema + "." + ALLOWED_IPS
                                        + " (ACCOUNT_ID, IPAddress) VALUES(?,?)";
                    cmd.Parameters.Add("ACCOUNT_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(accountId);
                    cmd.Parameters.Add("IPAddress", OdbcType.VarChar).Value = ipaddress;
                    cmd.ExecuteNonQuery();
                }
                else if (userId != Guid.Empty)
                {
                    cmd.CommandText = "INSERT INTO "
                                        + schema + "." + ALLOWED_IPS
                                        + " (USER_ID, IPAddress) VALUES(?,?)";
                    cmd.Parameters.Add("USER_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(userId);
                    cmd.Parameters.Add("IPAddress", OdbcType.VarChar).Value = ipaddress;
                    cmd.ExecuteNonQuery();
                }
            }
            finally
            {
                if (cmd != null)
                    cmd.Dispose();
            }
            Global.systemLog.Info("added allowed ip record: " + accountId + '/' + userId + ", " + ipaddress);
        }

        /**
         * remove ip address and netblock for an account or user
         * - set accountId or userId to Guid.Empty is not setting that item
         * - ipaddress is a either an ip address (a.b.c.d) or a netblock (a.b.c.d/n)
         */
        public static bool removeAllowedIP(QueryConnection conn, string schema, Guid accountId, Guid userId, string ipaddress)
        {
            QueryCommand cmd = null;
            try
            {
                cmd = conn.CreateCommand();
                if (accountId != Guid.Empty)
                {
                    cmd.CommandText = "DELETE FROM "
                                        + schema + "." + ALLOWED_IPS
                                        + " WHERE ACCOUNT_ID=? AND IPAddress=?";
                    cmd.Parameters.Add("ACCOUNT_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(accountId);
                    cmd.Parameters.Add("IPAddress", OdbcType.VarChar).Value = ipaddress;
                    int count = cmd.ExecuteNonQuery();
                    if (count > 0)
                    {
                        Global.systemLog.Info("removed allowed ip record: " + accountId + ", " + ipaddress);
                        return true;
                    }
                }
                else if (userId != Guid.Empty)
                {
                    cmd.CommandText = "DELETE FROM "
                                        + schema + "." + ALLOWED_IPS
                                        + " WHERE USER_ID=? AND IPAddress=?";
                    cmd.Parameters.Add("USER_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(userId);
                    cmd.Parameters.Add("IPAddress", OdbcType.VarChar).Value = ipaddress;
                    int count = cmd.ExecuteNonQuery();
                    if (count > 0)
                    {
                        Global.systemLog.Info("removed allowed ip record: " + userId + ", " + ipaddress);
                        return true;
                    }
                }
                return false;
            }
            finally
            {
                if (cmd != null)
                    cmd.Dispose();
            }
        }

        /**
         * return a list of ip addresses/netblocks for an account and/or user
         */
        public static List<string> getAllowedIPs(QueryConnection conn, string schema, Guid accountId, Guid userId)
        {
            List<string> result = new List<string>();
            QueryCommand cmd = null;
            QueryReader reader = null;
            try
            {
                cmd = conn.CreateCommand();
                if (accountId != Guid.Empty)
                {
                    cmd.CommandText = "SELECT IPAddress FROM "
                                            + schema + "." + ALLOWED_IPS +
                                            " WHERE ACCOUNT_ID=?";
                    cmd.Parameters.Add("ACCOUNT_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(accountId);
                    reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        result.Add(reader.GetString(0));
                    }
                }
                if (userId != Guid.Empty)
                {
                    cmd.Parameters.Clear();
                    cmd.CommandText = "SELECT IPAddress FROM "
                                        + schema + "." + ALLOWED_IPS +
                                        " WHERE USER_ID=?";
                    cmd.Parameters.Add("USER_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(userId);
                    if (reader != null)
                        reader.Close();
                    reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        result.Add(reader.GetString(0));
                    }
                }
            }
            finally
            {
                if (reader != null)
                    reader.Close();
                if (cmd != null)
                    cmd.Dispose();
            }
            return result;
        }

        public static List<string> getContentDirectories(QueryConnection conn, string schema)
        {
            List<string> slist = new List<string>();
            QueryCommand cmd = conn.CreateCommand();
            cmd.CommandText =
                "SELECT Directory FROM "
                + schema + "." + SPACE_TABLE_NAME;
            try
            {
                QueryReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    slist.Add(reader.GetString(0));
                }
                reader.Close();
            }
            catch (OdbcException)
            {
                Thread.Sleep(3000);
                QueryReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    slist.Add(reader.GetString(0));
                }
                reader.Close();
            }
            return slist;
        }

        /*
         * Get a list of all spaces
         */
        public static List<Space> getSpaces(QueryConnection conn, string schema)
        {
            List<Space> slist = new List<Space>();
            QueryCommand cmd = conn.CreateCommand();
            cmd.CommandText =
                "SELECT A.ID,A.Name,A.Comments,A." + conn.getMSSQLReservedName("Schema") + ",A.Directory,A.LoadNumber,A.Automatic,A.Type,A.ConnectString,A.DatabaseType,A.DatabaseDriver,A.DatabaseLoadDir,A.AdminUser,A.AdminPwd,"
                + "A.QueryConnectString,A.QueryDatabaseType,A.QueryDatabaseDriver,A.QueryConnectionName,A.QueryUser,A.QueryPwd,A.Active,A.Folded,A.TestMode,A.Locked,A.SSO,A.SSOPWD,"
                + "A.Discovery, A.MapNullsToZero, A.AWSAccessKeyId, A.AWSSecretKey, A.ReportFrom, A.ReportSubject, A.DBTimeZone FROM "
                + schema + "." + SPACE_TABLE_NAME + " A WHERE A.Deleted IS NULL";
            QueryReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                Space s = new Space();
                s.ID = reader.GetGuid(0);
                s.Name = reader.IsDBNull(1) ? null : reader.GetString(1);
                s.Comments = reader.IsDBNull(2) ? null : reader.GetString(2);
                s.Schema = reader.IsDBNull(3) ? null : reader.GetString(3);
                s.Directory = reader.IsDBNull(4) ? null : reader.GetString(4);
                s.LoadNumber = reader.GetInt32(5);
                s.Automatic = reader.GetBoolean(6);
                s.Type = reader.GetInt32(7);
                s.ConnectString = reader.IsDBNull(8) ? null : reader.GetString(8);
                s.DatabaseType = reader.IsDBNull(9) ? null : reader.GetString(9);
                s.DatabaseDriver = reader.IsDBNull(10) ? null : reader.GetString(10);
                s.DatabaseLoadDir = reader.IsDBNull(11) ? null : reader.GetString(11);
                s.AdminUser = reader.IsDBNull(12) ? null : reader.GetString(12);
                s.AdminPwd = Performance_Optimizer_Administration.MainAdminForm.decrypt(reader.GetString(13),
                Performance_Optimizer_Administration.MainAdminForm.SecretPassword);
                s.QueryConnectString = reader.IsDBNull(14) ? null : reader.GetString(14);
                s.QueryDatabaseType = reader.IsDBNull(15) ? null : reader.GetString(15);
                s.QueryDatabaseDriver = reader.IsDBNull(16) ? null : reader.GetString(16);
                s.QueryConnectionName = reader.IsDBNull(17) ? null : reader.GetString(17);
                s.QueryUser = reader.IsDBNull(18) ? null : reader.GetString(18);
                s.QueryPwd = reader.IsDBNull(19) ? null : Performance_Optimizer_Administration.MainAdminForm.decrypt(reader.GetString(19),
                Performance_Optimizer_Administration.MainAdminForm.SecretPassword);
                s.Active = reader.GetBoolean(20);
                s.Folded = reader.GetBoolean(21);
                s.TestMode = reader.GetBoolean(22);
                s.Locked = reader.GetBoolean(23);
                s.SSO = reader.IsDBNull(24) ? false : reader.GetBoolean(24);
                s.SSOPassword = reader.IsDBNull(25) ? null : Performance_Optimizer_Administration.MainAdminForm.decrypt(reader.GetString(25),
                        Performance_Optimizer_Administration.MainAdminForm.SecretPassword);
                s.DiscoveryMode = reader.IsDBNull(26) ? false : reader.GetBoolean(26);
                s.MapNullsToZero = reader.IsDBNull(27) ? false : reader.GetBoolean(27);

                s.awsAccessKeyId = reader.IsDBNull(28) ? null : reader.GetString(28);
                s.awsSecretKey = reader.IsDBNull(29) ? null : reader.GetString(29);
                s.ScheduleFrom = reader.IsDBNull(30) ? null : reader.GetString(30);
                s.ScheduleSubject = reader.IsDBNull(31) ? null : reader.GetString(31);
                s.dbTimeZone = reader.IsDBNull(31) ? null : reader.GetString(31);

                slist.Add(s);
            }
            reader.Close();
            cmd.Dispose();
            foreach (Space s in slist)
            {
                if (isDBTypeParAccel(s.ConnectString))
                {
                    string connectStringSuffix = getPADBConnectionStringSuffix(conn, schema, s);
                    if (connectStringSuffix != null && !connectStringSuffix.Trim().Equals("") && !s.ConnectString.EndsWith(connectStringSuffix))
                    {
                        s.ConnectString += connectStringSuffix;
                    }
                }
            }
            return (slist);
        }


        /// <summary>
        /// Gets the space membership record for a given space and user from user_space_inxn table
        /// </summary>
        /// <param name="conn"></param>
        /// <param name="schema"></param>
        /// <param name="u"></param>
        /// <param name="spaceID"></param>
        /// <returns>shallow space membership record. Note not all space and user fields are populated</returns>
        public static SpaceMembership getSpaceMembershipRecord(QueryConnection conn, string schema, User u, Guid spaceID)
        {
            QueryCommand cmd = null;
            QueryReader reader = null;
            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = "SELECT A.USER_ID, A.SPACE_ID, A.StartDate, A.Owner, A.Admin FROM "
                    + schema + "." + USER_SPACE_INXN + " A " +
                    " WHERE A.USER_ID=?  AND A.SPACE_ID=?";
                cmd.Parameters.Add("USER_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(u.ID);
                cmd.Parameters.Add("SPACE_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(spaceID);

                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    User su = new User();
                    su.ID = reader.GetGuid(0);
                    Space s = new Space();
                    s.ID = reader.GetGuid(1);
                    SpaceMembership sm = new SpaceMembership();
                    sm.User = su;
                    sm.StartDate = reader.GetDateTime(2);
                    sm.Owner = reader.IsDBNull(3) ? false : reader.GetBoolean(3);
                    sm.Administrator = reader.IsDBNull(4) ? false : reader.GetBoolean(4);
                    sm.Space = s;
                    return sm;
                }
                return null;
            }
            finally
            {
                if (reader != null)
                {
                    reader.Close();
                }

                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }
        }

        /*
         * Returns a list of space memberships - Note, not all fields in space and user are populated
         */
        public static List<SpaceMembership> getUserSpaceMemberships(QueryConnection conn, string schema, User u, Guid spaceID)
        {
            List<SpaceMembership> mlist = new List<SpaceMembership>();
            QueryCommand cmd = conn.CreateCommand();
            cmd.CommandText = "SELECT A.PKID,A.Username,B.SPACE_ID,B.StartDate,D.Name,B.Owner,B.Admin FROM "
                + schema + "." + USER_TABLE_NAME +
                " A INNER JOIN " + schema + "." + USER_SPACE_INXN + " B ON A.PKID=B.USER_ID" +
                " INNER JOIN " + schema + "." + USER_SPACE_INXN + " C ON C.SPACE_ID=B.SPACE_ID" +
                " INNER JOIN " + schema + "." + SPACE_TABLE_NAME + " D ON D.ID=B.SPACE_ID" +
                " WHERE D.Deleted IS NULL AND C.USER_ID=?";
            cmd.Parameters.Add("USER_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(u.ID);
            if (spaceID != Guid.Empty)
            {
                cmd.CommandText += " AND B.SPACE_ID=?";
                cmd.Parameters.Add("SPACE_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(spaceID);
            }
            QueryReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                User su = new User();
                su.ID = reader.GetGuid(0);
                su.Username = reader.GetString(1);
                Space s = new Space();
                s.ID = reader.GetGuid(2);
                SpaceMembership sm = new SpaceMembership();
                sm.User = su;
                sm.StartDate = reader.GetDateTime(3);
                s.Name = reader.IsDBNull(4) ? null : reader.GetString(4);
                sm.Owner = reader.IsDBNull(5) ? false : reader.GetBoolean(5);
                sm.Administrator = reader.IsDBNull(6) ? false : reader.GetBoolean(6);
                sm.Space = s;
                mlist.Add(sm);
            }
            reader.Close();
            cmd.Dispose();
            return (mlist);
        }

        public static List<SpaceMembership> getUsers(QueryConnection conn, string schema, Space sp)
        {
            return getUsers(conn, schema, sp, true);
        }

        /**
         * fullUserInfo if false, only sets the ID and Username in the User field of the SpaceMembership record
         * - note if fullUserInfo is false, we will include 'expired' users in this list (probably okay since we are moving away from that)
         */
        public static List<SpaceMembership> getUsers(QueryConnection conn, string schema, Space sp, bool fullUserInfo)
        {
            List<SpaceMembership> mlist = new List<SpaceMembership>();

            QueryCommand cmd = conn.CreateCommand();
            cmd.CommandText = "SELECT B.USER_ID,C.USERNAME,B.Admin,B.Owner FROM "
                + schema + "." + SPACE_TABLE_NAME + " A "
                + "INNER JOIN " + schema + "." + USER_SPACE_INXN + " B ON A.ID=B.SPACE_ID "
                + "INNER JOIN " + schema + "." + Database.USER_TABLE_NAME + " C ON B.USER_ID=C.PKID "
                + "WHERE B.SPACE_ID=?";
            cmd.Parameters.Add("SPACE_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(sp.ID);
            QueryReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                SpaceMembership m = new SpaceMembership();
                m.Space = sp;
                User u = new User();
                u.ID = reader.GetGuid(0);
                u.Username = reader.GetString(1);
                m.User = u;
                m.Administrator = reader.GetBoolean(2);
                m.Owner = reader.GetBoolean(3);
                mlist.Add(m);
            }
            reader.Close();
            cmd.Dispose();

            if (fullUserInfo)
            {
                List<SpaceMembership> finallist = new List<SpaceMembership>();
                for (int i = 0; i < mlist.Count; i++)
                {
                    User u = getUser(conn, schema, null, mlist[i].User.ID, null);
                    if (u != null)
                    {
                        mlist[i].User = u;
                        finallist.Add(mlist[i]);
                    }
                }
                return finallist;
            }
            return mlist;

        }

        public static void migrateSpaceToLocal(Space sp, MainAdminForm maf, QueryConnection adminConn, Connection leConn, User u)
        {
            Util.addLoadGroup(maf, leConn.LoadGroup);
            Util.addScriptGroup(maf, leConn.LoadGroup);
            foreach (StagingTable st in maf.stagingTableMod.getAllStagingTables())
            {
                if (st.LoadGroups != null && st.LoadGroups.Length > 0)
                {
                    for (int i = 0; i < st.LoadGroups.Length; i++)
                    {
                        if (st.LoadGroups[i].Equals(Util.LOAD_GROUP_NAME))
                        {
                            st.LoadGroups[i] = leConn.LoadGroup;
                            break;
                        }
                    }
                }
            }
            //update publish table with loadgroup
            QueryCommand cmd = adminConn.CreateCommand();
            cmd.CommandText = "UPDATE " + mainSchema + "." + PUBLISH_TABLE +
                              " SET LoadGroup = '" + leConn.LoadGroup + "'" +
                              " WHERE SPACE_ID=? AND LoadGroup='" + Util.LOAD_GROUP_NAME + "'";
            cmd.Parameters.Add("SpaceID", adminConn.getODBCUniqueIdentiferType()).Value = adminConn.getUniqueIdentifer(sp.ID);
            cmd.ExecuteNonQuery();
            Global.systemLog.Info("Publish history updated for space: " + sp.ID + " migrated to local using loadgroup " + leConn.LoadGroup);
            //update TXN_COMMAND_HISTORY table with loadgroup
            QueryConnection dataconn = null;
            try
            {
                dataconn = ConnectionPool.getConnection(sp.getFullConnectString());
                if (dataconn != null)
                {
                    cmd = dataconn.CreateCommand();
                    cmd.CommandText = "UPDATE " + sp.Schema + ".TXN_COMMAND_HISTORY" +
                        " SET SUBSTEP='" + leConn.LoadGroup + "'" +
                        " WHERE SUBSTEP IS NULL OR SUBSTEP='" + Util.LOAD_GROUP_NAME + "'";
                    cmd.ExecuteNonQuery();
                    cmd.CommandText = "UPDATE " + sp.Schema + ".TXN_COMMAND_HISTORY" +
                                            " SET SUBSTEP='" + leConn.LoadGroup + ": ' + SUBSTRING(SUBSTEP, 8, 256)" +
                                            " WHERE SUBSTEP LIKE '" + Util.LOAD_GROUP_NAME + ": %'";
                    cmd.ExecuteNonQuery();
                    Global.systemLog.Info("TXN_COMMAND_HISTORY updated for space: " + sp.ID + " migrated to local using loadgroup " + leConn.LoadGroup);
                }
            }
            finally
            {
                ConnectionPool.releaseConnection(dataconn);
            }
            Dictionary<string, int> curLoadNumbers = null;
            foreach (StagingTable st in maf.stagingTableMod.getStagingTables())
            {
                // If there are loadgroups other than ACORN, get them
                if (st.LoadGroups != null && st.LoadGroups.Length > (Array.IndexOf<string>(st.LoadGroups, "ACORN") >= 0 ? 1 : 0))
                {
                    curLoadNumbers = Database.getCurrentLoadNumbers(adminConn, mainSchema, sp.ID);
                    break;
                }
            }
            Util.buildApplication(maf, mainSchema, DateTime.Now, sp.LoadNumber, curLoadNumbers, sp, null);
            Util.saveApplication(maf, sp, null, u);

            //Now generate dat files for each table
            List<object> olist = new List<object>();
            foreach (MeasureTable mt in maf.measureTablesList)
            {
                if (!mt.AutoGenerated)
                    continue;
                if (mt.InheritTable != null && mt.InheritTable.Length > 0)
                    continue;
                if (mt.TableSource.Schema != null && mt.TableSource.Schema.Length > 0 && mt.TableSource.Schema != sp.Schema)
                    continue;
                if (!leConn.Name.Equals(mt.TableSource.Connection))
                    continue;
                olist.Add(mt);
            }
            foreach (DimensionTable dt in maf.dimensionTablesList)
            {
                if (!dt.AutoGenerated)
                    continue;
                if (dt.InheritTable != null && dt.InheritTable.Length > 0)
                    continue;
                if (dt.TableSource.Schema != null && dt.TableSource.Schema.Length > 0 && dt.TableSource.Schema != sp.Schema)
                    continue;
                if (!leConn.Name.Equals(dt.TableSource.Connection))
                    continue;
                olist.Add(dt);
            }
            if (Database.tableExists(dataconn, sp.Schema, "TXN_SNAPSHOT_POLICY"))
            {
                olist.Add("TXN_SNAPSHOT_POLICY");
            }
            if (Database.tableExists(dataconn, sp.Schema, "TXN_COMMAND_HISTORY"))
            {
                olist.Add("TXN_COMMAND_HISTORY");
            }
            string targetDir = sp.Directory + "\\data\\migrate";
            if (Directory.Exists(targetDir))
                Directory.Delete(targetDir, true);
            Directory.CreateDirectory(targetDir);
            List<string> tablesToPopulate = new List<string>();
            foreach (object o in olist)
            {
                string tname = null;
                if (typeof(MeasureTable).Equals(o.GetType()))
                {
                    tname = ((MeasureTable)o).TableSource.Tables[0].PhysicalName;
                }
                else if (typeof(DimensionTable).Equals(o.GetType()))
                    tname = ((DimensionTable)o).TableSource.Tables[0].PhysicalName;
                else if (typeof(string).Equals(o.GetType()))
                    tname = (string)o;

                string connStr = sp.getFullConnectString();
                string server = getDataConnectionServer(sp);
                string username = sp.AdminUser;
                string password = sp.AdminPwd;
                ProcessStartInfo psi = new ProcessStartInfo("bcp");
                psi.Arguments = dataconn.Database + "." + sp.Schema + "." + tname + " out \"" + targetDir + "\\" +
                    tname + ".dat\" -S" + server + " -U" + username + " -P" + password + " -N";
                string logArguments = dataconn.Database + "." + sp.Schema + "." + tname + " out \"" + targetDir + "\\" +
                    tname + ".dat\" -S" + server + " -U" + username + " -P ***** -N";
                psi.WindowStyle = ProcessWindowStyle.Hidden;
                psi.RedirectStandardOutput = false;
                psi.CreateNoWindow = true;
                psi.UseShellExecute = false;
                psi.RedirectStandardInput = false;
                psi.RedirectStandardError = false;
                psi.WorkingDirectory = targetDir;
                Global.systemLog.Info("Executing command: " + psi.FileName + " " + logArguments);
                Process p = Process.Start(psi);
                p.WaitForExit();
                if (p.ExitCode != 0)
                {
                    Global.systemLog.Warn("The command failed. This might be okay if there is a table in the repository that is not in the database.");
                }
                else
                {
                    tablesToPopulate.Add(tname + ".dat");
                }
            }
            if (sp.ConnectString.ToLower().StartsWith("jdbc:sqlserver://"))
            {
                StringBuilder sb = new StringBuilder();
                List<DatabaseIndex> dbindices = Database.getIndices(dataconn, sp.Schema);
                List<DatabaseIndex> indices = new List<DatabaseIndex>();
                if (dbindices != null && dbindices.Count > 0)
                {
                    foreach (DatabaseIndex di in dbindices)
                    {
                        if (di.TableName.Equals("TXN_COMMAND_HISTORY") || di.TableName.Equals("TXN_SNAPSHOT_POLICY"))
                            continue;
                        if (tablesToPopulate.Contains(di.TableName + ".dat"))
                            indices.Add(di);
                    }
                }
                sb.Append("<DatabaseIndices>").AppendLine();
                if (indices != null && indices.Count > 0)
                {
                    foreach (DatabaseIndex di in indices)
                    {
                        sb.Append("\t").Append("<DatabaseIndex>").AppendLine();
                        sb.Append("\t\t").Append("<Name>").Append(di.Name).Append("</Name>").AppendLine();
                        sb.Append("\t\t").Append("<SchemaName>").Append(di.SchemaName).Append("</SchemaName>").AppendLine();
                        sb.Append("\t\t").Append("<TableName>").Append(di.TableName).Append("</TableName>").AppendLine();
                        sb.Append("\t\t").Append("<Columns>").AppendLine();
                        foreach (string colName in di.Columns)
                            sb.Append("\t\t\t").Append("<Column>").Append(colName).Append("</Column>").AppendLine();
                        sb.Append("\t\t").Append("</Columns>").AppendLine();
                        sb.Append("\t\t").Append("<IncludedColumns>").AppendLine();
                        foreach (string incColName in di.IncludedColumns)
                            sb.Append("\t\t\t").Append("<IncludedColumn>").Append(incColName).Append("</IncludedColumn>").AppendLine();
                        sb.Append("\t\t").Append("</IncludedColumns>").AppendLine();
                        sb.Append("\t").Append("</DatabaseIndex>").AppendLine();
                    }
                }
                sb.Append("</DatabaseIndices>").AppendLine();
                if (File.Exists(sp.Directory + "\\data\\migrate\\DatabaseIndices.xml"))
                    File.Delete(sp.Directory + "\\data\\migrate\\DatabaseIndices.xml");
                File.WriteAllText(sp.Directory + "\\data\\migrate\\DatabaseIndices.xml", sb.ToString());
            }
            /* Now issue live access command migratespacetolocal which will create schema and time tables,
             * perform generateschema to create all tables and then download .dat files and use them to 
             * populate data in tables on local db
             */
            LocalETLConfig leConfig = null;
            foreach (LocalETLConfig letlConfig in BirstConnectUtil.getLocalETLConfigsForSpace(sp).Values)
            {
                if (letlConfig.getLocalETLConnection().Equals(leConn.Name))
                {
                    leConfig = letlConfig;
                    break;
                }
            }
            StringBuilder timeCommands = BirstConnectUtil.getLiveAccessTimeCommands(sp, sp.LoadNumber, leConfig.getApplicationDirectory());
            File.WriteAllText(sp.Directory + "\\generatetime." + leConfig.getLocalETLLoadGroup() + ".properties", timeCommands.ToString());
            StringBuilder timeOverrideProperties = BirstConnectUtil.getLiveAccessOverrideProperties(sp, maf, leConfig.getApplicationDirectory(), leConfig.getLocalETLConnection(), true);
            File.WriteAllText(sp.Directory + "\\customer.time." + leConfig.getLocalETLLoadGroup() + ".properties", timeOverrideProperties.ToString());
            StringBuilder overrideProperties = BirstConnectUtil.getLiveAccessOverrideProperties(sp, maf, leConfig.getApplicationDirectory(), leConfig.getLocalETLConnection(), false);
            File.WriteAllText(sp.Directory + "\\customer." + leConfig.getLocalETLLoadGroup() + ".properties", overrideProperties.ToString());
            if (LiveAccessUtil.migratespacetolocal(sp, leConn.Name, tablesToPopulate))
            {
                //drop all tables on server except TXN_COMMAND_HISTORY
                foreach (object o in olist)
                {
                    string tname = null;
                    if (typeof(MeasureTable).Equals(o.GetType()))
                        tname = ((MeasureTable)o).TableSource.Tables[0].PhysicalName;
                    else if (typeof(DimensionTable).Equals(o.GetType()))
                        tname = ((DimensionTable)o).TableSource.Tables[0].PhysicalName;
                    else if (typeof(string).Equals(o.GetType()))
                        tname = (string)o;
                    if (tname.Equals("TXN_COMMAND_HISTORY"))
                        continue;
                    Database.dropTable(dataconn, sp.Schema, tname);
                }
                Global.systemLog.Debug("Space " + sp.ID.ToString() + " successfully migrated to local connection " + leConn.VisibleName);
            }
            else
            {
                throw new Exception("Space " + sp.ID.ToString() + " could not be successfully migrated to local connection " + leConn.VisibleName);
            }
        }

        private static string getDataConnectionServer(Space sp)
        {
            if (!sp.ConnectString.ToLower().StartsWith("jdbc:sqlserver://"))
            {
                Global.systemLog.Error("Can not migrate space to/from a non-SQL Server instance");
                return null;
            }
            string temp = sp.ConnectString;
            int index = temp.IndexOf(';');  // separator after hostname
            int length = index - 17;        // 17 is the length of "jdbc:sqlserver://"
            return temp.Substring(17, length).Replace(':', ','); // Connect strings to BCP are host,port not host:port
        }

        public static Space copySpaceToNewSpace(Space oldsp, HttpSessionState Session, String newSpaceName, String newSpaceSchemaName, String newSpaceDescription)
        {
            Global.systemLog.Info("Beginning copy of space: " + oldsp.ID + "/" + oldsp.Name + " to " + newSpaceName);
            User u = (User)Session["User"];
            QueryConnection conn = null;
            QueryConnection spconn = null;
            Space sp = null; // newly created space
            SpaceOperationStatus oldSpOperation = null;
            SpaceOperationStatus spOperation = null;
            try
            {
                oldSpOperation = new SpaceOperationStatus(oldsp, u);
                oldSpOperation.logBeginStatus(SpaceOpsUtils.OP_COPY_FROM);
                conn = ConnectionPool.getConnection();
                spconn = Util.getConnectionWithVariedRetries(oldsp.getFullConnectString(), oldsp.DatabaseType);
                // Get a list of database tables and indexes
                bool hasLoadedData = Status.hasLoadedData(oldsp, null);
                List<DatabaseIndex> dilist = !hasLoadedData || oldsp.DatabaseType == "Infobright" ? null : Database.getIndices(spconn, oldsp.Schema);
                List<string> tlist = !hasLoadedData ? null : Database.getTables(spconn, oldsp.Schema);
                // Create the space in the same directory
                DirectoryInfo d = new DirectoryInfo(oldsp.Directory);
                DirectoryInfo oldd = d;
                d = d.Parent;
                sp = new Space(u, oldsp.Type, oldsp.ProcessVersionID, oldsp.SFDCVersionID, d.FullName, newSpaceSchemaName);
                sp.Type = oldsp.Type;
                sp.Name = newSpaceName;
                sp.Comments = newSpaceDescription;
                sp.Automatic = oldsp.Automatic;

                sp.ConnectString = oldsp.ConnectString;
                sp.DatabaseType = oldsp.DatabaseType;
                sp.DatabaseDriver = oldsp.DatabaseDriver;
                sp.setDatabaseLoadDir(oldsp.DatabaseLoadDir.Substring(0, oldsp.DatabaseLoadDir.Length - oldsp.ID.ToString().Length - 1));
                sp.AdminUser = oldsp.AdminUser;
                sp.AdminPwd = oldsp.AdminPwd;
                sp.QueryConnectionName = oldsp.QueryConnectionName;
                sp.QueryConnectString = oldsp.QueryConnectString;
                sp.QueryDatabaseDriver = oldsp.QueryDatabaseDriver;
                sp.QueryDatabaseType = oldsp.QueryDatabaseType;
                sp.QueryPwd = oldsp.QueryPwd;
                sp.QueryUser = oldsp.QueryUser;
                sp.Active = oldsp.Active;
                sp.LoadNumber = oldsp.LoadNumber;
                sp.QueryLanguageVersion = oldsp.QueryLanguageVersion;
                sp.MaxQueryRows = oldsp.MaxQueryRows;
                sp.MaxQueryTimeout = oldsp.MaxQueryTimeout;
                sp.UsageTracking = oldsp.UsageTracking;
                sp.ProcessVersionID = oldsp.ProcessVersionID;
                sp.UseDynamicGroups = oldsp.UseDynamicGroups;
                sp.AllowRetryFailedLoad = oldsp.AllowRetryFailedLoad;
                sp.SFDCVersionID = oldsp.SFDCVersionID;
                sp.DisallowExternalScheduler = oldsp.DisallowExternalScheduler;
                sp.SFDCNumThreads = oldsp.SFDCNumThreads;
                sp.DiscoveryMode = oldsp.DiscoveryMode;
                sp.MapNullsToZero = oldsp.MapNullsToZero;
                sp.awsAccessKeyId = oldsp.awsAccessKeyId;
                sp.awsSecretKey = oldsp.awsSecretKey;
                sp.ScheduleFrom = oldsp.ScheduleFrom;
                sp.ScheduleSubject = oldsp.ScheduleSubject;
                sp.dbTimeZone = oldsp.dbTimeZone;

                Database.createSpace(conn, mainSchema, u, sp);
                Global.systemLog.Info("New space: " + sp.ID + " created");

                if (isDBTypeParAccel(sp.ConnectString))
                {
                    string connectStringSuffix = getPADBConnectionStringSuffix(conn, mainSchema, sp);
                    if (connectStringSuffix != null && !connectStringSuffix.Trim().Equals("") && !sp.ConnectString.EndsWith(connectStringSuffix))
                    {
                        sp.ConnectString += connectStringSuffix;
                    }
                }

                spOperation = new SpaceOperationStatus(sp, u);
                spOperation.logBeginStatus(SpaceOpsUtils.OP_COPY_TO_NEW);
                MainAdminForm maf = (MainAdminForm)Session["MAF"];
                copySpaceContentsIntoSpace(conn, spconn, oldsp, sp, tlist, dilist, u, maf);

                // update publish table
                QueryCommand cmd = conn.CreateCommand();
                cmd.CommandText = "INSERT INTO " + mainSchema + "." + PUBLISH_TABLE +
                                  " (SPACE_ID,LoadNumber,PublishDate,LoadDate,NumRows,DataSize,TableName,LoadGroup) " +
                                  " SELECT \'" + sp.ID + "\',LoadNumber,PublishDate,LoadDate,NumRows,DataSize,TableName,LoadGroup FROM " +
                                    mainSchema + "." + PUBLISH_TABLE + " WHERE SPACE_ID=?";
                cmd.Parameters.Add("SpaceID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(oldsp.ID);
                cmd.ExecuteNonQuery();
                Global.systemLog.Info("Publish history updated for copy from space: " + oldsp.ID + " to space: " + sp.ID);
                // Change the application name to reflect the correct user and make sure current user is the owner of the 
                // space. It could be different if the copied space was shared one with Admin Priveleges.
                Util.updateCopiedRepository(sp, u);
                // Copy group/user/acl info. Note u supplied should be owner.
                Util.copyGroupUserACLInfo(oldsp, sp, u, true);
                Util.copyReplicatePackagesInfo(conn, mainSchema, oldsp, sp, u);
                UserAndGroupUtils.updatedUserGroupMappingFile(sp);
                Global.systemLog.Info("Finished copy of space: " + oldsp.ID + "/" + oldsp.Name + " to " + newSpaceName);
                return sp;
            }
            catch (Exception e)
            {
                Global.systemLog.Error("Error copying space: " + oldsp.ID, e);
                return null;
            }
            finally
            {
                if (oldSpOperation != null)
                {
                    oldSpOperation.logEndStatus(SpaceOpsUtils.OP_COPY_FROM);
                }
                if (spOperation != null)
                {
                    spOperation.logEndStatus(SpaceOpsUtils.OP_COPY_TO_NEW);
                }
                ConnectionPool.releaseConnection(spconn);
                ConnectionPool.releaseConnection(conn);
            }
        }

        /**
         * version of copyspace that allows fine grained control over what is copied
         */
        public static void copySpaceToExistingSpace(Space fromSpace, Space toSpace, User u, bool replicate, HashSet<string> options)
        {
            Global.systemLog.Info("starting " + Util.copyReplicateLabel(replicate) + " to " + toSpace.ID + "/" + toSpace.Name);
            QueryConnection conn = null;
            QueryConnection datastoreConnection = null;
            try
            {
                datastoreConnection = Util.getConnectionWithVariedRetries(fromSpace.getFullConnectString(), fromSpace.DatabaseType);

                // Get a list of database tables and indexes
                List<DatabaseIndex> dilist = new List<DatabaseIndex>();
                List<string> tlist = new List<string>();
                if (fromSpace.DatabaseType != "Infobright") // infobright does not support indexes
                {
                    if (Util.includeDatawarehouseIndices(options))
                    {
                        dilist.AddRange(Database.getIndices(datastoreConnection, fromSpace.Schema, "DW\\_%"));
                    }
                    if (Util.includeStagingIndices(options))
                    {
                        dilist.AddRange(Database.getIndices(datastoreConnection, fromSpace.Schema, "ST\\_%"));
                    }
                }
                if (Util.includeDatawarehouseTables(options))
                {
                    tlist.AddRange(Database.getTables(datastoreConnection, fromSpace.Schema, "DW\\_%"));
                }
                if (Util.includeStagingTables(options))
                {
                    tlist.AddRange(Database.getTables(datastoreConnection, fromSpace.Schema, "ST\\_%"));
                }
                if (Util.includeAggregates(options))
                {
                    tlist.AddRange(Database.getAggregateTables(datastoreConnection, fromSpace));
                }
                if (Util.includeTxnTables(options))
                {
                    tlist.AddRange(Database.getTables(datastoreConnection, fromSpace.Schema, "TXN\\_%"));
                }

                // update the space settings based upon the 'from' space
                bool spaceUpdated = false;
                if (options.Contains("settings-basic"))
                {
                    toSpace.Active = fromSpace.Active;
                    toSpace.Automatic = fromSpace.Automatic;
                    toSpace.QueryLanguageVersion = fromSpace.QueryLanguageVersion;
                    toSpace.ProcessVersionID = fromSpace.ProcessVersionID;
                    toSpace.SFDCVersionID = fromSpace.SFDCVersionID;
                    if (Util.includeDatawarehouseTables(options))
                    {
                        toSpace.LoadNumber = fromSpace.LoadNumber; // if not including tables, no need for updated load number or publish history
                    }
                    toSpace.DiscoveryMode = fromSpace.DiscoveryMode;
                    toSpace.MapNullsToZero = fromSpace.MapNullsToZero;
                    spaceUpdated = true;
                }

                if (options.Contains("settings-sso"))
                {
                    toSpace.SSO = fromSpace.SSO;
                    toSpace.SSOPassword = fromSpace.SSOPassword;

                    spaceUpdated = true;
                }

                if (options.Contains("settings-other"))
                {
                    // options that we might not want to copy
                    toSpace.MaxQueryRows = fromSpace.MaxQueryRows;
                    toSpace.MaxQueryTimeout = fromSpace.MaxQueryTimeout;
                    toSpace.UsageTracking = fromSpace.UsageTracking;
                    toSpace.AllowRetryFailedLoad = fromSpace.AllowRetryFailedLoad;
                    toSpace.DisallowExternalScheduler = fromSpace.DisallowExternalScheduler;
                    toSpace.SFDCNumThreads = fromSpace.SFDCNumThreads;
                    spaceUpdated = true;
                }

                conn = ConnectionPool.getConnection();

                if (spaceUpdated)
                    Database.updateNonDBSpaceParams(conn, mainSchema, toSpace);

                if (options.Contains("useunloadfeature"))
                {
                    // copy the datastore
                    if (tlist.Count > 0 || dilist.Count > 0)
                    {
                        string prefix = null;
                        if (Util.includeDatawarehouseTables(options) && Util.includeStagingTables(options))
                            prefix = "%";
                        else if (Util.includeDatawarehouseTables(options))
                            prefix = "DW_%";
                        else if (Util.includeStagingTables(options))
                            prefix = "ST_%";
                        else if (Util.includeTxnTables(options))
                            prefix = "TXN_%";
                        if (tlist.Count > 0 && prefix != null)
                            deleteSpacePublishedDataInTheCloud(datastoreConnection, toSpace, prefix);

                        // Copy database tables
                        if (tlist != null)
                        {
                            bool newRep;
                            MainAdminForm maf = Util.loadRepository(toSpace, out newRep);
                            Global.systemLog.Info("Copying tables [" + string.Join(",", tlist) + "] from space: " + fromSpace.ID + " to space: " + toSpace.ID + " using Processing Engine");
                            NewSpaceService.copyTablesLoadUnload(toSpace, fromSpace, maf, tlist);
                        }
                    }

                    // copy the files and directories
                    Util.copyReplicateSpaceFilesAndDirectoriesIntoSpace(conn, mainSchema, fromSpace, toSpace, u, replicate, options);
                }
                else
                {
                    initializeRepository(toSpace);

                    // copy the files and directories
                    Util.copyReplicateSpaceFilesAndDirectoriesIntoSpace(conn, mainSchema, fromSpace, toSpace, u, replicate, options);

                    // copy the datastore
                    if (tlist.Count > 0 || dilist.Count > 0)
                    {
                        string prefix = null;
                        if (Util.includeDatawarehouseTables(options) && Util.includeStagingTables(options))
                            prefix = "%";
                        else if (Util.includeDatawarehouseTables(options))
                            prefix = "DW_%";
                        else if (Util.includeStagingTables(options))
                            prefix = "ST_%";
                        else if (Util.includeTxnTables(options))
                            prefix = "TXN_%";
                        if (tlist.Count > 0 && prefix != null)
                            deleteSpacePublishedDataInTheCloud(datastoreConnection, toSpace, prefix);

                        // Copy database tables
                        copySpaceDataStoreIntoSpace(datastoreConnection, fromSpace, toSpace, tlist, dilist);
                    }
                }

                if (Util.includeDatawarehouseTables(options))
                {
                    // update publish table
                    QueryCommand cmd = conn.CreateCommand();
                    cmd.CommandText = "DELETE FROM " + mainSchema + "." + PUBLISH_TABLE +
                                      " WHERE SPACE_ID=?";
                    DbParameter param = cmd.Parameters.Add("SpaceID", conn.getODBCUniqueIdentiferType());
                    param.Value = conn.getUniqueIdentifer(toSpace.ID);
                    Global.systemLog.Debug(cmd.CommandText);
                    cmd.ExecuteNonQuery();
                    cmd = conn.CreateCommand();
                    cmd.CommandText = "INSERT INTO " + mainSchema + "." + PUBLISH_TABLE +
                                      " (SPACE_ID,LoadNumber,PublishDate,LoadDate,NumRows,DataSize,TableName,LoadGroup) " +
                                      " SELECT \'" + toSpace.ID + "\',LoadNumber,PublishDate,LoadDate,NumRows,DataSize,TableName,LoadGroup FROM " +
                                        mainSchema + "." + PUBLISH_TABLE + " WHERE SPACE_ID=?";
                    Global.systemLog.Debug(cmd.CommandText);
                    param = cmd.Parameters.Add("SpaceID", conn.getODBCUniqueIdentiferType());
                    param.Value = conn.getUniqueIdentifer(fromSpace.ID);
                    cmd.ExecuteNonQuery();
                    Global.systemLog.Info("publish history copied to " + toSpace.ID);
                }

                if (options.Contains("settings-permissions"))
                {
                    // Copy group/user/acl info. Note that the user supplied (u) supplied should be owner.
                    Util.copyReplicateSpaceGroupsIntoSpace(conn, mainSchema, fromSpace, toSpace, u, replicate, false);
                }

                if (options.Contains("settings-membership"))
                {
                    // Copy space membership and user/group membership
                    Util.copyReplicateSpaceMembership(conn, mainSchema, fromSpace, toSpace, replicate, false);
                }
            }
            catch (Exception e)
            {
                Global.systemLog.Error("Error copying space to " + toSpace.ID, e);
                throw e;
            }
            finally
            {
                if (conn != null)
                {
                    Database.updateMappingModifiedDate(conn, mainSchema, toSpace);
                    UserAndGroupUtils.saveUserAndGroupToFile(toSpace);
                }
                // free up the spaces and release the connections                
                ConnectionPool.releaseConnection(datastoreConnection);
                ConnectionPool.releaseConnection(conn);
            }
        }

        private static void copySpaceDataStoreIntoSpace(QueryConnection spconn, Space fromSpace, Space toSpace, List<string> tlist, List<DatabaseIndex> dilist)
        {
            // copy the database tables (specific list determined elsewhere, based upon option settings)
            foreach (string tname in tlist)
            {
                bool isMySQL = fromSpace.ConnectString.Contains("jdbc:mysql");
                Database.copyTable(spconn, tname, fromSpace.Schema, toSpace.Schema, isMySQL);
                Global.systemLog.Info("Copying table [" + tname + "] to " + toSpace.ID);
            }

            // copy the database indices (specific list determined elsewhere, based upon option settings)
            foreach (DatabaseIndex di in dilist)
            {
                di.SchemaName = toSpace.Schema;
                Global.systemLog.Info("Copying index [" + di.Name + "] to " + toSpace.ID);
                Database.createIndex(spconn, di);
            }
        }

        public static Space copySpaceToExistingSpace(Space oldsp, User u, Space sp, MainAdminForm maf)
        {
            Global.systemLog.Info("Beginning copy of space: " + oldsp.ID + "/" + oldsp.Name + " to " + sp.Name);
            QueryConnection conn = null;
            QueryConnection spconn = null;
            SpaceOperationStatus oldSpaceOperation = null;
            SpaceOperationStatus spaceOperation = null;
            try
            {
                oldSpaceOperation = new SpaceOperationStatus(oldsp, u);
                oldSpaceOperation.logBeginStatus(SpaceOpsUtils.OP_COPY_FROM);
                spaceOperation = new SpaceOperationStatus(sp, u);
                spaceOperation.logBeginStatus(SpaceOpsUtils.OP_COPY_TO_EXISTING);
                conn = ConnectionPool.getConnection();
                spconn = Util.getConnectionWithVariedRetries(oldsp.getFullConnectString(), oldsp.DatabaseType);
                // Get a list of database tables and indexes
                bool hasLoadedData = Status.hasLoadedData(oldsp, null);
                List<DatabaseIndex> dilist = !hasLoadedData || oldsp.DatabaseType == "Infobright" ? null : Database.getIndices(spconn, oldsp.Schema);
                List<string> tlist = !hasLoadedData ? null : Database.getTables(spconn, oldsp.Schema);
                sp.Active = oldsp.Active;
                sp.LoadNumber = oldsp.LoadNumber;

                // Delete old data
                Database.deleteSpacePublishedData(spconn, sp);
                copySpaceContentsIntoSpace(conn, spconn, oldsp, sp, tlist, dilist, u, maf);

                // update publish table
                QueryCommand cmd = conn.CreateCommand();
                cmd.CommandText = "DELETE FROM " + mainSchema + "." + PUBLISH_TABLE +
                                  " WHERE SPACE_ID=?";
                DbParameter param = cmd.Parameters.Add("SpaceID", conn.getODBCUniqueIdentiferType());
                param.Value = conn.getUniqueIdentifer(sp.ID);
                Global.systemLog.Debug(cmd.CommandText);
                cmd.ExecuteNonQuery();
                cmd = conn.CreateCommand();
                cmd.CommandText = "INSERT INTO " + mainSchema + "." + PUBLISH_TABLE +
                                  " (SPACE_ID,LoadNumber,PublishDate,LoadDate,NumRows,DataSize,TableName,LoadGroup) " +
                                  " SELECT \'" + sp.ID + "\',LoadNumber,PublishDate,LoadDate,NumRows,DataSize,TableName,LoadGroup FROM " +
                                    mainSchema + "." + PUBLISH_TABLE + " WHERE SPACE_ID=?";
                Global.systemLog.Debug(cmd.CommandText);
                param = cmd.Parameters.Add("SpaceID", conn.getODBCUniqueIdentiferType());
                param.Value = conn.getUniqueIdentifer(oldsp.ID);
                cmd.ExecuteNonQuery();
                Global.systemLog.Info("Publish history updated for copy from space: " + oldsp.ID + " to space: " + sp.ID);
                // Copy group/user/acl info. Note u supplied should be owner.
                Util.copyGroupUserACLInfo(oldsp, sp, u, false);
                Util.copyReplicatePackagesInfo(conn, mainSchema, oldsp, sp, u);
                Global.systemLog.Info("Finished copy of space: " + oldsp.ID + "/" + oldsp.Name + " to " + sp.Name);
                return sp;
            }
            catch (Exception e)
            {
                Global.systemLog.Error("Error copying space: " + oldsp.ID, e);
                return null;
            }
            finally
            {
                if (oldSpaceOperation != null)
                {
                    oldSpaceOperation.logEndStatus(SpaceOpsUtils.OP_COPY_FROM);
                }

                if (spaceOperation != null)
                {
                    spaceOperation.logEndStatus(SpaceOpsUtils.OP_COPY_TO_EXISTING);
                }
                ConnectionPool.releaseConnection(spconn);
                ConnectionPool.releaseConnection(conn);
            }
        }

        private static void copyHistoricalRepositories(Space fromSpace, Space toSpace, User u)
        {
            DirectoryInfo oldd = new DirectoryInfo(fromSpace.Directory);
            FileInfo[] files = oldd.GetFiles("repository*.xml.*.save");
            // Copy historical repositories
            if (files != null && files.Length > 0)
            {
                foreach (FileInfo fi in files)
                {
                    massageSpaceParamsOnCopy(fromSpace, toSpace, fi.FullName, Path.Combine(toSpace.Directory, fi.Name), u);
                }
            }
        }

        static ulong tmpCounter = 0;
        public static void massageSpaceParamsOnCopy(Space fromSpace, Space toSpace, string fromFilePath, string toFilePath, User u)
        {
            ulong value = ++tmpCounter; // should be atomic
            string tname = toFilePath + ".tmp" + value;

            StreamReader reader = null;
            StreamWriter writer = null;
            bool moveOnCompletion = false;
            try
            {
                reader = new StreamReader(fromFilePath);
                writer = new StreamWriter(tname);
                string line = null;
                while ((line = reader.ReadLine()) != null)
                {
                    line = line.Replace(fromSpace.Directory, toSpace.Directory);
                    line = line.Replace(fromSpace.Schema, toSpace.Schema);
                    writer.WriteLine(line);
                }
                moveOnCompletion = true;
            }
            finally
            {
                if (reader != null)
                {
                    reader.Close();
                }
                if (writer != null)
                {
                    writer.Close();
                }
                if (moveOnCompletion)
                {
                    if (File.Exists(toFilePath))
                    {
                        File.Delete(toFilePath);
                    }
                    File.Move(tname, toFilePath);
                    File.Delete(tname);
                }
            }
        }

        private static void copySpaceContentsIntoSpace(QueryConnection adminConn, QueryConnection spconn, Space oldsp, Space sp, List<string> tlist, List<DatabaseIndex> dilist, User user, MainAdminForm maf)
        {
            DirectoryInfo oldd = new DirectoryInfo(oldsp.Directory);
            string[] repfiles = { Util.REPOSITORY_DEV, Util.REPOSITORY_PROD };
            foreach (string file in repfiles)
            {
                string fromSpaceFile = Path.Combine(oldsp.Directory, file);
                if (File.Exists(fromSpaceFile))
                {
                    string toSpaceFile = Path.Combine(sp.Directory, file);
                    massageSpaceParamsOnCopy(oldsp, sp, fromSpaceFile, toSpaceFile, user);
                }
            }

            Global.systemLog.Info("Repository copied for space: " + oldsp.ID + " to space: " + sp.ID);
            // clean up .save files for fromSpace also..takes care of the use case where user might not have logged in via Admin UI 
            // and have not triggered the pruning of .save files
            try
            {
                Util.cleanupOldFiles(oldsp);
                Global.systemLog.Info("clean up old files for fromspace: " + oldsp.ID);
            }
            catch (Exception ex)
            {
                Global.systemLog.Warn("Continuing to next step : Problem clearing up old files for fromSpace : " + oldsp.Name, ex);
            }

            // clean up .bin, .save, .abin files from the tospace
            Util.cleanupRepositoryTrash(sp);
            copyHistoricalRepositories(oldsp, sp, user);
            Global.systemLog.Info(".save Repositories copied for space: " + oldsp.ID + " to space: " + sp.ID);

            // Copy Birst Connect config
            string dcTargetDir = sp.Directory + "\\" + Util.DCCONFIG_DIR;
            if (!Directory.Exists(dcTargetDir))
                Directory.CreateDirectory(dcTargetDir);
            if (Directory.Exists(oldsp.Directory + "\\" + Util.DCCONFIG_DIR))
            {
                DirectoryInfo source = new DirectoryInfo(oldsp.Directory + "\\" + Util.DCCONFIG_DIR);
                FileInfo[] sfInfo = source.GetFiles();
                if (sfInfo != null)
                {
                    foreach (FileInfo fi in sfInfo)
                    {
                        Util.FileCopy(source.FullName + "\\" + fi.Name, dcTargetDir + "\\" + fi.Name, true);
                    }
                }
            }
            else if (File.Exists(oldsp.Directory + "\\" + Util.DCCONFIG_FILE_NAME))
            {
                Util.FileCopy(oldsp.Directory + "\\" + Util.DCCONFIG_FILE_NAME, dcTargetDir + "\\" + Util.DCCONFIG_FILE_NAME, true);
                //moving oldsp/dcconfig.xml to oldsp/dcconfig directory
                BirstConnectUtil.moveFileToDirectory(oldsp, Util.DCCONFIG_DIR, Util.DCCONFIG_FILE_NAME);
            }

            // Copy connectors folder
            if (Directory.Exists(oldsp.Directory + "\\" + Util.CONNECTOR_CONFIG_DIR))
            {
                string connTargetDir = sp.Directory + "\\" + Util.CONNECTOR_CONFIG_DIR;
                if (!Directory.Exists(connTargetDir))
                    Directory.CreateDirectory(connTargetDir);
                DirectoryInfo source = new DirectoryInfo(oldsp.Directory + "\\" + Util.CONNECTOR_CONFIG_DIR);
                FileInfo[] sfInfo = source.GetFiles();
                if (sfInfo != null)
                {
                    foreach (FileInfo fi in sfInfo)
                    {
                        Util.FileCopy(source.FullName + "\\" + fi.Name, connTargetDir + "\\" + fi.Name, true);
                    }
                }
            }

            // DO NOT COPY PACKAGES.XML. This will mess up the usage of UIDs in different spaces
            string[] files = { "CustomGeoMaps.xml", "DrillMaps.xml", "sforce.xml", "spacesettings.xml", "DashboardStyleSettings.xml", "SavedExpressions.xml" };
            foreach (string file in files)
            {
                if (File.Exists(oldsp.Directory + "\\" + file))
                    Util.FileCopy(oldsp.Directory + "\\" + file, sp.Directory + "\\" + file, true);
            }

            string[] directories = { "attachments", "custom-subject-areas", "data", "mapping", "DashboardTemplates", "DashboardBackgroundImage" };
            foreach (string dir in directories)
            {
                if (Directory.Exists(oldsp.Directory + "\\" + dir))
                    Util.copyDir(oldsp.Directory + "\\" + dir, sp.Directory + "\\" + dir);
            }

            // Copy catalog
            if (Directory.Exists(oldsp.Directory + "\\catalog"))
            {
                if (Util.isDirectoryLengthSupported(oldsp.Directory + "\\catalog")) //Execute Acorn command to copy catalog dir
                {
                    Util.copyDir(oldsp.Directory + "\\catalog", sp.Directory + "\\catalog\\", new string[] { ".jasper", ".smijasper", ".jasper440" });
                }
                else
                {   //execute perfEngine command to copy catalog dir
                    if (Util.getProcessingEngineVersion(oldsp) > 4)
                    {
                        Util.copyCatalogDir(oldsp, oldsp.Directory + "\\catalog", sp.Directory + "\\catalog\\", new string[] { ".jasper", ".smijasper", ".jasper440" }, null, false);
                    }
                    else
                    {
                        Global.systemLog.Warn("Copying of catalog directory failed as the specified path, file name, or both are too long. The fully qualified file name must be less than 260 characters, and the directory name must be less than 248 characters.\n To support this, please upgrade processing engine version to 5");
                        throw new Exception("Copying of catalog directory failed as the specified path, file name, or both are too long. The fully qualified file name must be less than 260 characters, and the directory name must be less than 248 characters.");
                    }
                }
            }

            Global.systemLog.Info("Config files and data copied for space: " + oldsp.ID + " to space: " + sp.ID);

            // Copy database tables
            if (tlist != null)
            {
                if (oldsp.DatabaseType != null && (oldsp.DatabaseType == Database.PARACCEL || oldsp.DatabaseType == Database.REDSHIFT))
                {
                    Global.systemLog.Info("Copying tables [" + string.Join(",", tlist) + "] from space: " + oldsp.ID + " to space: " + sp.ID + " using Processing Engine");
                    NewSpaceService.copyTablesUsingCmdFile(sp, tlist, oldsp.Schema, sp.Schema, maf);
                }
                else
                {
                    foreach (string tname in tlist)
                    {
                        bool isMySQL = oldsp.ConnectString.Contains("jdbc:mysql");
                        Database.copyTable(spconn, tname, oldsp.Schema, sp.Schema, isMySQL);
                        Global.systemLog.Info("Copying table [" + tname + "] from space: " + oldsp.ID + " to space: " + sp.ID);
                    }
                }
            }
            // Create indices
            if (dilist != null)
                foreach (DatabaseIndex di in dilist)
                {
                    di.SchemaName = sp.Schema;
                    Global.systemLog.Info("Copying index [" + di.Name + "] from space: " + oldsp.ID + " to space: " + sp.ID);
                    Database.createIndex(spconn, di);
                }

            //Now check if the old sp contains any local configuration, if so, copy the local data to new space
            Dictionary<string, LocalETLConfig> leconfigs = BirstConnectUtil.getLocalETLConfigsForSpace(oldsp);
            if (leconfigs.Count > 0)
            {
                Global.systemLog.Debug("Copying space data using local configurations from space: " + oldsp.ID.ToString() + " to space: " + sp.ID.ToString());
                foreach (LocalETLConfig leConfig in leconfigs.Values)
                {
                    string connectionName = leConfig.getLocalETLConnection();
                    if (!Status.hasLoadedData(oldsp, leConfig.getLocalETLLoadGroup()))
                    {
                        Global.systemLog.Debug("No local data loaded for space " + oldsp.ID.ToString() + " for Local LiveAccess connection " + connectionName);
                        continue;
                    }
                    ProxyRegistration pr = null;
                    pr = Database.getProxyRegistration(adminConn, mainSchema, oldsp.ID.ToString() + "/" + connectionName);
                    if (pr == null)
                    {
                        throw new Exception("Cannot copy space - Failed to obtain ProxyRegistration for :" + oldsp.ID.ToString() + "/" + connectionName);
                    }
                    if (!LiveAccessUtil.isRealTimeConnectionActive(pr, oldsp, connectionName))
                    {
                        throw new Exception("Cannot copy space - Local LiveAccess connection " + connectionName + " is not alive");
                    }
                    Global.systemLog.Debug("Copying space data using local connection " + connectionName + " from space: " + oldsp.ID.ToString() + " to space: " + sp.ID.ToString());
                    if (!LiveAccessUtil.copySpaceDataToNewSpace(oldsp, connectionName, sp.Schema, sp.ID.ToString()))
                    {
                        throw new Exception("Cannot copy local data using Local LiveAccess connection " + connectionName);
                    }
                    // change the schemaname for realtimeconnection in new space's dcconfig and repositories
                    BirstConnectUtil.updateSchemaNameForLocalLiveAccessConnections(sp, oldsp.Schema, sp.Schema);
                }
            }

            sp.invalidateDashboardCache();
        }

        public static void deleteSpacePublishedData(QueryConnection conn, Space sp)
        {
            deleteSpacePublishedData(conn, sp, true);
        }

        private static void deleteSpacePublishedDataInTheCloud(QueryConnection conn, Space sp, string prefix)
        {
            deleteSpacePublishedDataInTheCloud(conn, sp, prefix, true);
        }

        private static void deleteSpacePublishedDataInTheCloud(QueryConnection conn, Space sp, string prefix, bool deleteDataOnPartitionConnections)
        {
            deleteSpacePublishedDataInTheCloud(conn, sp, prefix, deleteDataOnPartitionConnections, false);
        }

        private static void deleteSpacePublishedDataInTheCloud(QueryConnection conn, Space sp, string prefix, bool deleteDataOnPartitionConnections, bool createNewConnection)
        {
            QueryCommand cmd = null;
            QueryReader reader = null;
            List<string> tables = new List<string>();
            QueryConnection deleteDataConn = conn;
            try
            {
                if (createNewConnection)
                {
                    deleteDataConn = Util.getConnectionWithVariedRetries(sp.getFullConnectString(), sp.DatabaseType);
                }
                cmd = deleteDataConn.CreateCommand();
                if (deleteDataConn.isDBTypeOracle())
                    cmd.CommandText = "SELECT TABLE_NAME FROM ALL_TABLES WHERE OWNER=? AND TABLE_NAME LIKE ?";
                else if (deleteDataConn.isDBTypeSAPHana())
                    cmd.CommandText = "SELECT TABLE_NAME FROM SYS.TABLES WHERE SCHEMA_NAME=? AND TABLE_NAME LIKE ?";
                else
                    cmd.CommandText = "SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA=? AND TABLE_NAME LIKE ?";
                String schema = sp.Schema;
                String pfx = prefix;
                if (deleteDataConn.isDBTypeSAPHana())
                {
                    schema = schema.ToUpper();
                    pfx = prefix.ToUpper();
                }
                else if (deleteDataConn.isDBTypeParAccel())
                {
                    schema = schema.ToLower();
                    pfx = prefix.ToUpper();
                }
                cmd.Parameters.Add("OWNER", OdbcType.VarChar).Value = schema;
                cmd.Parameters.Add("TABLE_NAME", OdbcType.VarChar).Value = pfx;

                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    tables.Add(reader.GetString(0));
                }
            }
            finally
            {
                if (reader != null)
                    reader.Close();
                if (cmd != null)
                    cmd.Dispose();
            }
            foreach (string tname in tables)
            {
                dropTable(deleteDataConn, sp.Schema, tname);
                Global.systemLog.Info("Dropped table [" + tname + "] from " + sp.ID);
            }
            //now check if space is partitioned, if so, delete all published data from each partition connection
            if (deleteDataOnPartitionConnections)
            {
                bool newRepository = false;
                MainAdminForm maf = Util.loadRepository(sp, out newRepository);
                if (maf == null)
                    return;
                if (maf.IsPartitioned)
                {
                    string[] partitionConnectionNames = maf.getPartitionConnectionNames("Default Connection");
                    if (partitionConnectionNames != null)
                    {
                        foreach (string partitionConn in partitionConnectionNames)
                        {
                            QueryConnection pConn = null;
                            try
                            {
                                Global.systemLog.Info("Deleting Data on partition connection - " + partitionConn);
                                DatabaseConnection dConn = maf.connection.findConnection(partitionConn);
                                string password = Performance_Optimizer_Administration.MainAdminForm.decrypt(dConn.Password, Performance_Optimizer_Administration.MainAdminForm.SecretPassword);
                                pConn = ConnectionPool.getConnection(sp.getFullConnectString(dConn.ConnectString, dConn.UserName, password), false);
                                deleteSpacePublishedDataInTheCloud(pConn, sp, prefix, false);
                            }
                            finally
                            {
                                ConnectionPool.releaseConnection(pConn);
                            }
                        }
                    }
                }
            }
        }

        private static void deleteSpacePublishedData(QueryConnection conn, Space sp, bool deleteLocalData)
        {
            if (deleteLocalData) // passing false from delete-space because we alerady sent seperate delete space request to live access and space/schema should be deleted at local database
            {
                Dictionary<string, LocalETLConfig> localETLConfigs = BirstConnectUtil.getLocalETLConfigsForSpace(sp);
                if (localETLConfigs != null && localETLConfigs.Count > 0)
                {
                    bool newRepository = false;
                    MainAdminForm maf = Util.loadRepository(sp, out newRepository);
                    if (maf == null)
                        return;
                    foreach (string lg in localETLConfigs.Keys)
                    {
                        LocalETLConfig leConfig = localETLConfigs[lg];
                        StringBuilder commands = LiveAccessUtil.getLiveAccessDeleteAllCommands(sp, leConfig.getApplicationDirectory());
                        File.WriteAllText(sp.Directory + "\\delete." + lg + ".properties", commands.ToString());
                        StringBuilder overrideProperties = BirstConnectUtil.getLiveAccessOverrideProperties(sp, maf, leConfig.getApplicationDirectory(), leConfig.getLocalETLConnection(), false);
                        File.WriteAllText(sp.Directory + "\\customer." + lg + ".properties", overrideProperties.ToString());
                        Global.systemLog.Debug("Live access delete all published started for load group " + lg + " and connection " + leConfig.getLocalETLConnection());
                        bool compeletedDeleteAllOnLiveAccess = LiveAccessUtil.sendCommandToLiveAccess(sp, leConfig.getLocalETLConnection(), "liveaccessdeletealldata");
                        if (!compeletedDeleteAllOnLiveAccess)
                        {
                            Global.systemLog.Error("Live access delete all published could not be completed for load group: " + lg + ". Can not continue with delete all pubshied data for space " + sp.ID);
                            return;
                        }
                        Global.systemLog.Debug("Live access delete all published completed for load group " + lg + " and connection " + leConfig.getLocalETLConnection());
                    }
                }
            }

            deleteSpacePublishedDataInTheCloud(conn, sp, "%");
        }

        // XXX this should be implemented, along with the data directory removal via an separate operations run 'archive/delete' process
        // XXX it is no longer called on delete space (to deal with customers who 'accidentally' delete and then ask operations to restore)
        /*
        private static void deleteSchema(QueryConnection conn, string schema, Space sp, System.Web.SessionState.HttpSessionState session, bool deleteLocalData)
        {
            string name = null;
            if (session != null)
            {
                User u = (User)session["user"];
                if (u != null)
                    name = u.Username;
            }
            Global.systemLog.Info("Beginning delete of schema: " + sp.ID + " by user: " + name);
            QueryCommand cmd = conn.CreateCommand();
            cmd.CommandText = "DELETE FROM " + schema + "." + SPACE_TABLE_NAME + " WHERE [ID]='" + sp.ID + "'";
            cmd.ExecuteNonQuery();
            cmd.CommandText = "DELETE FROM " + schema + "." + USER_SPACE_INXN + " WHERE [SPACE_ID]='" + sp.ID + "'";
            cmd.ExecuteNonQuery();
            cmd.CommandText = "DELETE FROM " + schema + "." + PUBLISH_TABLE + " WHERE [SPACE_ID]='" + sp.ID + "'";
            cmd.ExecuteNonQuery();
            cmd.CommandText = "DELETE FROM " + schema + "." + SCHEDULED_LOAD_TABLE + " WHERE [SPACE_ID]='" + sp.ID + "'";
            cmd.ExecuteNonQuery();
            // Deleting space group acl information from database
            // Deleting group_acl information for the space groups
            cmd.CommandText = "DELETE FROM " + getTableName(schema, GROUP_ACL_TABLE) +
                " WHERE [GROUP_ID] IN " + 
                "(SELECT [GROUP_ID] FROM " + getTableName(schema, SPACE_GROUP_TABLE) + 
                " WHERE [SPACE_ID]= '" + sp.ID + "')";
            cmd.ExecuteNonQuery();

            // Deleting group_user info for the space groups
            cmd.CommandText = "DELETE FROM " + getTableName(schema, GROUP_USER_TABLE) +
                " WHERE [GROUP_ID] IN " +
                "(SELECT [GROUP_ID] FROM " + getTableName(schema, SPACE_GROUP_TABLE) +
                " WHERE [SPACE_ID]= '" + sp.ID + "')";
            cmd.ExecuteNonQuery();

            // Deleting space_group 
            cmd.CommandText = "DELETE FROM " + getTableName(schema, SPACE_GROUP_TABLE) +
                " WHERE [SPACE_ID] = '" + sp.ID + "'";
            cmd.ExecuteNonQuery();

            // Delete the database schema
            QueryConnection tconn = null;
            try
            {
                tconn = ConnectionPool.getConnection(sp.getFullConnectString());
                QueryCommand tcmd = tconn.CreateCommand();
                if (sp.DatabaseType == "Infobright")
                {
                    deleteSpacePublishedData(tconn, sp);
                    tcmd.CommandText = "DROP DATABASE IF EXISTS " + sp.Schema;
                    tcmd.ExecuteNonQuery();
                    cmd.Dispose();
                    tcmd.Dispose();
                }
                else if (sp.DatabaseType == ORACLE)
                {
                    deleteSpacePublishedData(tconn, sp, deleteLocalData);
                    if (isOraTableSpaceExists(tconn, sp.Schema))
                    {
                        string dropTablesSpaceQuery = "DROP TABLESPACE " + sp.Schema + " INCLUDING CONTENTS AND DATAFILES CASCADE CONSTRAINTS";
                        Global.systemLog.Debug("Drop tablespace query for space " + sp.ID.ToString() + " query: " + dropTablesSpaceQuery);
                        tcmd.CommandText = dropTablesSpaceQuery;
                        tcmd.ExecuteNonQuery();
                    }
                    string dropUserQuery = "DROP USER " + sp.Schema + " CASCADE";
                    Global.systemLog.Debug("Drop user query for space " + sp.ID.ToString() + " query: " + dropUserQuery);
                    tcmd.CommandText = dropUserQuery;
                    tcmd.ExecuteNonQuery();
                    cmd.Dispose();
                    tcmd.Dispose();
                }
                else
                {
                    deleteSpacePublishedData(tconn, sp, deleteLocalData);
                    tcmd.CommandText = "DROP SCHEMA [" + sp.Schema + "]";
                    tcmd.ExecuteNonQuery();
                    cmd.Dispose();
                    tcmd.Dispose();
                }
            }
            finally
            {
                ConnectionPool.releaseConnection(tconn);
            }

            // Delete Infobright query schema if necessary
            if (sp.QueryConnectionName != null && sp.QueryConnectionName == "IB Connection")
            {
                tconn = null;
                try
                {
                    tconn = ConnectionPool.getConnection(sp.getQueryFullConnectString());
                    QueryCommand tcmd = tconn.CreateCommand();
                    tcmd.CommandText = "DROP DATABASE IF EXISTS " + sp.Schema;
                    tcmd.ExecuteNonQuery();
                    cmd.Dispose();
                    tcmd.Dispose();
                }
                finally
                {
                    ConnectionPool.releaseConnection(tconn);
                }
            }
            return;
        }
         */

        public static bool deleteSpace(QueryConnection conn, string schema, Space sp, System.Web.SessionState.HttpSessionState session)
        {
            string name = null;
            if (session != null)
            {
                User u = (User)session["user"];
                if (u != null)
                    name = u.Username;
            }
            /*
            Global.systemLog.Info("Beginning delete of space: " + sp.ID + " by user: " + name);
            //deleting local etl space
            try
            {
                Dictionary<string, LocalETLConfig> localETLConfigs = BirstConnectUtil.getLocalETLConfigsForSpace(sp);
                if (localETLConfigs != null && localETLConfigs.Count > 0)
                {
                    foreach (string lg in localETLConfigs.Keys)
                    {
                        MainAdminForm maf = null;
                        if (session != null)
                        {
                            maf = (MainAdminForm)session["MAF"];
                        }
                        if (maf == null)
                        {
                            bool newRepository;
                            maf = Util.loadRepository(sp, out newRepository);
                        }
                        LocalETLConfig leConfig = localETLConfigs[lg];
                        StringBuilder commands = LiveAccessUtil.getLiveAccessDeleteSpaceCommands(sp, leConfig.getApplicationDirectory());
                        File.WriteAllText(sp.Directory + "\\delete." + lg + ".properties", commands.ToString());
                        StringBuilder overrideProperties = BirstConnectUtil.getLiveAccessOverrideProperties(sp, maf, leConfig.getApplicationDirectory(), leConfig.getLocalETLConnection(), false);
                        File.WriteAllText(sp.Directory + "\\customer." + lg + ".properties", overrideProperties.ToString());
                        Global.systemLog.Debug("Live access delete space started for load group " + lg + " and connection " + leConfig.getLocalETLConnection());
                        bool compeletedDeleteSpaceOnLiveAccess = LiveAccessUtil.sendCommandToLiveAccess(sp, leConfig.getLocalETLConnection(), "liveaccessdeletespace");
                        if (!compeletedDeleteSpaceOnLiveAccess)
                        {
                            Global.systemLog.Error("Live access delete space could not be compeleted for load group: " + lg + ". Can not continue with delete space " + sp.ID);
                            return (false);
                        }
                        Global.systemLog.Debug("Live access delete space completed load group " + lg + " and connection " + leConfig.getLocalETLConnection());
                    }
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Unable to delete live access space: " + sp.Name + ":" + sp.ID + ":" + ex.ToString());
                return false;
            }
             */
            try
            {
                // do not delete the data or directory, just mark the schema as deleted
                QueryCommand cmd = conn.CreateCommand();
                cmd.CommandText = "UPDATE " + schema + "." + SPACE_TABLE_NAME + " SET Deleted=?, DeletedDate=? WHERE ID= ? ";
                cmd.Parameters.Add("Deleted", conn.getODBCBooleanType()).Value = conn.getBooleanValue(true);
                cmd.Parameters.Add("DeletedDate", OdbcType.DateTime).Value = truncateDateTime(DateTime.Now);
                cmd.Parameters.Add("ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(sp.ID);
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex2)
            {
                Global.systemLog.Info("Space: " + sp.Name + "/" + sp.ID + " failed to be deleted by user: " + name, ex2);
                return false;
            }
            Global.systemLog.Info("Space: " + sp.Name + "/" + sp.ID + " successfully deleted by user: " + name);
            return true;
        }

        public static void addTemplate(QueryConnection conn, string schema, Template t)
        {
            QueryCommand cmd = conn.CreateCommand();
            cmd.CommandText = "INSERT INTO " + schema + "." + TEMPLATES_TABLE_NAME +
                " (TEMPLATE_ID,CREATOR_ID,Name,Category,Comments,CreateDate,Automatic,Private,NumUses) VALUES (?,?,?,?,?,?,?,?,0)";
            cmd.Parameters.Add("TEMPLATE_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(t.ID);
            cmd.Parameters.Add("CREATOR_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(t.Creator_ID);
            cmd.Parameters.Add("Name", OdbcType.VarChar).Value = t.Name;
            cmd.Parameters.Add("Category", OdbcType.VarChar).Value = t.Category;
            if (t.Comments != null)
                cmd.Parameters.Add("Comments", OdbcType.VarChar).Value = t.Comments;
            else
                cmd.Parameters.Add("Comments", OdbcType.VarChar).Value = System.DBNull.Value;
            cmd.Parameters.Add("CreateDate", OdbcType.DateTime).Value = t.CreateDate;
            cmd.Parameters.Add("Automatic", conn.getODBCBooleanType()).Value = conn.getBooleanValue(t.Automatic);
            cmd.Parameters.Add("Private", conn.getODBCBooleanType()).Value = conn.getBooleanValue(t.Private);
            cmd.ExecuteNonQuery();
            cmd.Dispose();
            if (t.Attachments != null)
                foreach (string ta in t.Attachments)
                    addTemplateAttachment(conn, schema, t, ta);
        }

        public static void addTemplateAttachment(QueryConnection conn, string schema, Template t, string attachment)
        {
            QueryCommand cmd = conn.CreateCommand();
            cmd.CommandText = "INSERT INTO " + schema + "." + ATTACHMENTS_TABLE_NAME +
                " (TEMPLATE_ID,Filename) VALUES (?,?)";
            cmd.Parameters.Add("TEMPLATE_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(t.ID);
            cmd.Parameters.Add("Filename", OdbcType.VarChar).Value = attachment;
            cmd.ExecuteNonQuery();
            cmd.Dispose();
        }

        public static void removeTemplate(QueryConnection conn, string schema, Guid template_ID)
        {
            QueryCommand cmd = conn.CreateCommand();
            cmd.CommandText = "DELETE FROM " + schema + "." + TEMPLATES_TABLE_NAME +
            " WHERE TEMPLATE_ID=?";
            cmd.Parameters.Add("TEMPLATE_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(template_ID);
            cmd.ExecuteNonQuery();
            cmd.CommandText = "DELETE FROM " + schema + "." + ATTACHMENTS_TABLE_NAME +
            " WHERE TEMPLATE_ID=?";
            cmd.ExecuteNonQuery();
            cmd.Dispose();
        }

        private static void setTemplateCommand(QueryConnection conn, QueryCommand cmd, string schema, string category, Guid ID, Guid session_user_ID, Guid rating_user_ID, string sortExpression)
        {
            cmd.CommandText =
                "SELECT A.TEMPLATE_ID,CREATOR_ID,Name,Category,Comments,CreateDate,Automatic,Private,NumUses,AVG(CAST(B.Rating AS FLOAT)) AvgRating" +
                (rating_user_ID != Guid.Empty ? ",AVG(C.Rating) UserRating" : "") + " FROM " +
                schema + "." + TEMPLATES_TABLE_NAME + " A LEFT OUTER JOIN " +
                schema + "." + RATINGS_TABLE_NAME + " B ON A.TEMPLATE_ID=B.TEMPLATE_ID";
            if (rating_user_ID != Guid.Empty)
            {
                cmd.CommandText += " LEFT OUTER JOIN " +
                schema + "." + RATINGS_TABLE_NAME + " C ON A.TEMPLATE_ID=C.TEMPLATE_ID AND C.USER_ID=?";
                cmd.Parameters.Add("RATING_USER_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(rating_user_ID);
            }
            cmd.CommandText += " WHERE";
            bool first = true;
            if (category != null)
            {
                first = false;
                cmd.CommandText += " A.Category=?";
                cmd.Parameters.Add("Category", OdbcType.VarChar).Value = category;
            }
            if (ID != Guid.Empty)
            {
                if (first)
                    first = false;
                else
                    cmd.CommandText += " AND";
                cmd.CommandText += " A.TEMPLATE_ID=?";
                cmd.Parameters.Add("TEMPLATE_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(ID);
            }
            if (session_user_ID != Guid.Empty)
            {
                if (first)
                    first = false;
                else
                    cmd.CommandText += " AND";
                cmd.CommandText += " (A.CREATOR_ID=? OR A.Private<>1)";
                cmd.Parameters.Add("SESSION_USER_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(session_user_ID);
            }
            cmd.CommandText += " GROUP BY A.TEMPLATE_ID,CREATOR_ID,Name,Category,Comments,CreateDate,Automatic,Private,NumUses";
            if (sortExpression != null)
                cmd.CommandText += " ORDER BY " + sortExpression;
        }

        public static List<Template> getTemplates(QueryConnection conn, string schema, string category, Guid ID, Guid user_ID)
        {
            QueryCommand cmd = conn.CreateCommand();
            setTemplateCommand(conn, cmd, schema, category, ID, user_ID, Guid.Empty, null);
            List<Template> tlist = new List<Template>();
            QueryReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                Template t = new Template();
                t.ID = reader.GetGuid(0);
                t.Creator_ID = reader.GetGuid(1);
                t.Name = reader.GetString(2);
                t.Category = reader.GetString(3);
                if (!reader.IsDBNull(4))
                    t.Comments = reader.GetString(4);
                t.CreateDate = reader.GetDateTime(5);
                t.Automatic = reader.GetBoolean(6);
                t.Private = reader.GetBoolean(7);
                t.NumUses = reader.GetInt32(8);
                if (!reader.IsDBNull(9))
                    t.AvgRating = reader.GetDouble(9);
                tlist.Add(t);
            }
            reader.Close();
            foreach (Template t in tlist)
            {
                cmd.CommandText = "SELECT Filename FROM " + schema + "." + ATTACHMENTS_TABLE_NAME +
                    " A WHERE A.TEMPLATE_ID=?";
                cmd.Parameters.Add("TEMPLATE_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(t.ID);
                reader = cmd.ExecuteReader();
                List<string> talist = new List<string>();
                while (reader.Read())
                    talist.Add(reader.GetString(0));
                reader.Close();
                t.Attachments = talist;
            }
            cmd.Dispose();
            return (tlist);
        }

        public static DataTable getTemplateList(QueryConnection conn, string schema, string category, Guid ID, Guid user_ID, Guid rating_user_ID, string sortExpression)
        {
            DataTable dt = new DataTable("Templates");
            dt.Columns.Add("TEMPLATE_ID", typeof(Guid));
            dt.Columns.Add("CREATOR_ID", typeof(Guid));
            dt.Columns.Add("Name");
            dt.Columns.Add("Category");
            dt.Columns.Add("Comments");
            dt.Columns.Add("CreateDate", typeof(DateTime));
            dt.Columns.Add("Automatic", typeof(bool));
            dt.Columns.Add("Private", typeof(bool));
            dt.Columns.Add("NumUses", typeof(int));
            dt.Columns.Add("AvgRating", typeof(float));
            dt.Columns.Add("UserRating", typeof(float));
            QueryCommand cmd = conn.CreateCommand();
            setTemplateCommand(conn, cmd, schema, category, ID, user_ID, rating_user_ID, sortExpression);
            QueryReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                DataRow dr = dt.NewRow();
                dr[0] = reader.GetGuid(0);
                dr[1] = reader.GetGuid(1);
                dr[2] = reader.GetString(2);
                dr[3] = reader.GetString(3);
                dr[4] = reader.IsDBNull(4) ? null : reader.GetString(4);
                dr[5] = reader.GetDateTime(5);
                dr[6] = reader.GetBoolean(6);
                dr[7] = reader.GetBoolean(7);
                dr[8] = reader.GetInt32(8);
                if (!reader.IsDBNull(9))
                    dr[9] = reader.GetDouble(9);
                if (rating_user_ID != Guid.Empty && !reader.IsDBNull(10))
                    dr[10] = reader.GetInt32(10);
                dt.Rows.Add(dr);
            }
            reader.Close();
            cmd.Dispose();
            return (dt);
        }

        public static void updateTemplate(QueryConnection conn, string schema, Template t)
        {
            QueryCommand cmd = conn.CreateCommand();
            cmd.CommandText = "UPDATE " + schema + "." + TEMPLATES_TABLE_NAME + " SET Name=?,Category=?,Comments=?,Automatic=?,Private=?,NumUses=?"
                + " WHERE TEMPLATE_ID='" + t.ID + "'";
            cmd.Parameters.Add("@Name", OdbcType.VarChar).Value = t.Name;
            cmd.Parameters.Add("@Category", OdbcType.VarChar).Value = t.Category;
            cmd.Parameters.Add("@Comments", OdbcType.VarChar).Value = t.Comments;
            cmd.Parameters.Add("@Automatic", conn.getODBCBooleanType()).Value = conn.getBooleanValue(t.Automatic);
            cmd.Parameters.Add("@Private", conn.getODBCBooleanType()).Value = conn.getBooleanValue(t.Private);
            cmd.Parameters.Add("@NumUses", OdbcType.Int).Value = t.NumUses;
            cmd.ExecuteNonQuery();
        }

        public static DataTable getCategories(QueryConnection conn, string schema, Guid session_user_ID)
        {
            QueryCommand cmd = conn.CreateCommand();
            cmd.CommandText = "SELECT A.Category,SUM(CASE WHEN B.CREATOR_ID=? OR B.Private<>1 THEN 1 ELSE 0 END) CNT FROM " + schema + "." + CATEGORIES_TABLE_NAME +
                " A LEFT OUTER JOIN " + schema + "." + TEMPLATES_TABLE_NAME + " B ON A.Category=B.Category" +
                " GROUP BY A.Category";
            cmd.Parameters.Add("SESSION_USER_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(session_user_ID);
            QueryReader reader = cmd.ExecuteReader();
            DataTable dt = new DataTable("Categories");
            dt.Columns.Add("Category");
            dt.Columns.Add("Count", typeof(int));
            while (reader.Read())
            {
                DataRow dr = dt.NewRow();
                dr[0] = reader.GetString(0);
                dr[1] = reader.GetInt32(1);
                dt.Rows.Add(dr);
            }
            reader.Close();
            cmd.Dispose();
            return (dt);
        }

        public static void addRating(QueryConnection conn, string schema, Guid templateID, Guid userID, int rating)
        {
            QueryCommand cmd = conn.CreateCommand();
            cmd.CommandText = "SELECT COUNT(*) FROM " + schema + "." + RATINGS_TABLE_NAME + " WHERE USER_ID=? AND TEMPLATE_ID=?";
            cmd.Parameters.Add("USER_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(userID);
            cmd.Parameters.Add("TEMPLATE_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(templateID);
            int result = Int32.Parse(cmd.ExecuteScalar().ToString());
            if (result == 0)
            {
                cmd.Parameters.Clear();
                cmd.CommandText = "INSERT INTO " + schema + "." + RATINGS_TABLE_NAME +
                    " (TEMPLATE_ID,USER_ID,Rating) VALUES (?,?,?)";
                cmd.Parameters.Add("TEMPLATE_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(templateID);
                cmd.Parameters.Add("USER_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(userID);
                cmd.Parameters.Add("Rating", OdbcType.Int).Value = rating;
                cmd.ExecuteNonQuery();
            }
            else
            {
                cmd.Parameters.Clear();
                cmd.CommandText = "UPDATE " + schema + "." + RATINGS_TABLE_NAME + " SET Rating=?" +
                    " WHERE TEMPLATE_ID=? AND USER_ID=?";
                cmd.Parameters.Add("Rating", OdbcType.Int).Value = rating;
                cmd.Parameters.Add("TEMPLATE_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(templateID);
                cmd.Parameters.Add("USER_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(userID);
                cmd.ExecuteNonQuery();
            }
            cmd.Dispose();
        }

        public static bool loadProductsTable(QueryConnection conn, string schema, string path)
        {
            QueryCommand cmd = null;
            HashSet<int> plist = new HashSet<int>();
            // Get a list of existing products
            int count = 0;
            while (conn.State != ConnectionState.Open)
            {
                Thread.Sleep(1000);
                count++;
                if (count > 20)
                {
                    Global.systemLog.Error("Loading the product table failed, the connection is closed");
                    return false;
                }
            }
            if (tableExists(conn, schema, PRODUCTS_TABLE))
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = "SELECT PRODUCT_ID FROM " + schema + "." + PRODUCTS_TABLE;
                QueryReader preader = cmd.ExecuteReader();
                while (preader.Read())
                {
                    plist.Add(preader.GetInt32(0));
                }
                preader.Close();
            }
            else
            {
                createTables(conn, schema);
                cmd = conn.CreateCommand();
            }

            string filename = path + "\\" + PRODUCT_FILENAME;
            if (!File.Exists(filename))
            {
                Global.systemLog.Error("No product file: " + filename);
                return false;
            }
            Global.systemLog.Info("Loading products.config from '" + filename + "'");

            // Read in parameters for each space type
            StreamReader reader = new StreamReader(filename);
            string line = null;
            cmd.CommandText = "INSERT INTO " + schema + "." + PRODUCTS_TABLE +
                " (PRODUCT_ID,Name,Description," + conn.getReservedName("Default") + ",Type,AccountType,Category,PricingDetails," +
                "PricePerMonth,HasPromotion,PromotionCode,PromotionPrice,PromotionEndDate," +
                "PromotionDurationDays,RowLimit,DataLimit,ShareLimit,ShowAsOption," +
                "MaxAllowableData,MaxAllowableRows,MaxPublishData,MaxPublishRows,MaxPublishDataPerDay,MaxPublishRowsPerDay,MaxPublishTime," +
                "DefaultActive,SingleUse,MaxScriptStatements,MaxInputRows,MaxOutputRows,ScriptQueryTimeout,MaxDeliveredReportsPerDay) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
            DbParameter product = cmd.Parameters.Add("PRODUCT_ID", OdbcType.Int);
            DbParameter name = cmd.Parameters.Add("Name", OdbcType.VarChar);
            DbParameter desc = cmd.Parameters.Add("Description", OdbcType.VarChar);
            DbParameter defaultProd = cmd.Parameters.Add(conn.getReservedName("Default"), conn.getODBCBooleanType());
            DbParameter type = cmd.Parameters.Add("Type", OdbcType.Int);
            DbParameter accounttype = cmd.Parameters.Add("AccountType", OdbcType.Int);
            DbParameter category = cmd.Parameters.Add("Category", OdbcType.VarChar);
            DbParameter pdetails = cmd.Parameters.Add("PricingDetails", OdbcType.VarChar);
            DbParameter ppm = cmd.Parameters.Add("PricePerMonth", OdbcType.Decimal);
            DbParameter haspromo = cmd.Parameters.Add("HasPromotion", conn.getODBCBooleanType());
            DbParameter promocode = cmd.Parameters.Add("PromotionCode", OdbcType.VarChar);
            DbParameter promotionppm = cmd.Parameters.Add("PromotionPrice", OdbcType.Decimal);
            DbParameter promotionend = cmd.Parameters.Add("PromotionEndDate", OdbcType.DateTime);
            DbParameter promotionduration = cmd.Parameters.Add("PromotionDurationDays", OdbcType.Int);
            DbParameter rowlimit = cmd.Parameters.Add("RowLimit", OdbcType.Int);
            DbParameter datalimit = cmd.Parameters.Add("DataLimit", conn.getODBCBigIntType());
            DbParameter sharelimit = cmd.Parameters.Add("ShareLimit", OdbcType.Int);
            DbParameter showoption = cmd.Parameters.Add("ShowAsOption", conn.getODBCBooleanType());
            DbParameter maxallowabledata = cmd.Parameters.Add("MaxAllowableData", conn.getODBCBigIntType());
            DbParameter maxallowablerows = cmd.Parameters.Add("MaxAllowableRow", OdbcType.Int);
            DbParameter maxpublishdata = cmd.Parameters.Add("MaxPublishData", conn.getODBCBigIntType());
            DbParameter maxpublishrows = cmd.Parameters.Add("MaxPublishRows", OdbcType.Int);
            DbParameter maxpublishdataperday = cmd.Parameters.Add("MaxPublishDataPerDay", conn.getODBCBigIntType());
            DbParameter maxpublishrowsperday = cmd.Parameters.Add("MaxPublishRowsPerDay", OdbcType.Int);
            DbParameter maxpublishtime = cmd.Parameters.Add("MaxPublishTime", OdbcType.Int);
            DbParameter defaultactive = cmd.Parameters.Add("DefaultActive", conn.getODBCBooleanType());
            DbParameter singleuse = cmd.Parameters.Add("SingleUSe", conn.getODBCBooleanType());
            DbParameter maxscriptstatements = cmd.Parameters.Add("MaxScriptStatements", conn.getODBCBigIntType());
            DbParameter maxinputrows = cmd.Parameters.Add("MaxInputRows", OdbcType.Int);
            DbParameter maxoutputrows = cmd.Parameters.Add("MaxOutputRows", OdbcType.Int);
            DbParameter scriptquerytimeout = cmd.Parameters.Add("ScriptQueryTimeout", OdbcType.Int);
            DbParameter maxdeliveredreportsperday = cmd.Parameters.Add("MaxDeliveredReportsPerDay", OdbcType.Int);
            while ((line = reader.ReadLine()) != null)
            {
                if (line.StartsWith("//") || line.Equals(""))
                    continue;
                string[] cols = line.Split(new char[] { '\t' });
                int prodid = int.Parse(cols[0]);
                if (plist.Contains(prodid))
                    continue;
                product.Value = prodid;
                name.Value = cols[1].Trim(new char[] { ' ', '"' });
                desc.Value = cols[2].Trim(new char[] { ' ', '"' });
                defaultProd.Value = conn.getBooleanValue(bool.Parse(cols[3]));
                type.Value = cols[4];
                accounttype.Value = cols[5];
                category.Value = cols[6];
                pdetails.Value = cols[7];
                ppm.Value = decimal.Parse(cols[8]);
                haspromo.Value = conn.getBooleanValue(bool.Parse(cols[9]));
                promocode.Value = cols[10];
                if (cols[11].Length > 0)
                    promotionppm.Value = decimal.Parse(cols[11]);
                else
                    promotionppm.Value = System.DBNull.Value;
                if (cols[12].Length > 0)
                {
                    DateTime dt;
                    Performance_Optimizer_Administration.Util.tryDateTimeParse(cols[12], out dt);
                    promotionend.Value = dt;
                }
                else
                    promotionend.Value = System.DBNull.Value;
                if (cols[13].Length > 0)
                    promotionduration.Value = int.Parse(cols[13]);
                else
                    promotionduration.Value = System.DBNull.Value;
                if (cols[14].Length > 0)
                    rowlimit.Value = int.Parse(cols[14]);
                else
                    rowlimit.Value = System.DBNull.Value;
                if (cols[15].Length > 0)
                    datalimit.Value = long.Parse(cols[15]);
                else
                    datalimit.Value = System.DBNull.Value;
                if (cols[16].Length > 0)
                    sharelimit.Value = int.Parse(cols[16]);
                else
                    sharelimit.Value = System.DBNull.Value;
                showoption.Value = conn.getBooleanValue(bool.Parse(cols[17]));
                if (cols[18].Length > 0)
                    maxallowabledata.Value = long.Parse(cols[18]);
                else
                    maxallowabledata.Value = System.DBNull.Value;
                if (cols[19].Length > 0)
                    maxallowablerows.Value = int.Parse(cols[19]);
                else
                    maxallowablerows.Value = System.DBNull.Value;
                if (cols[20].Length > 0)
                    maxpublishdata.Value = long.Parse(cols[20]);
                else
                    maxpublishdata.Value = System.DBNull.Value;
                if (cols[21].Length > 0)
                    maxpublishrows.Value = int.Parse(cols[21]);
                else
                    maxpublishrows.Value = System.DBNull.Value;
                if (cols[22].Length > 0)
                    maxpublishdataperday.Value = long.Parse(cols[22]);
                else
                    maxpublishdataperday.Value = System.DBNull.Value;
                if (cols[23].Length > 0)
                    maxpublishrowsperday.Value = int.Parse(cols[23]);
                else
                    maxpublishrowsperday.Value = System.DBNull.Value;
                if (cols[24].Length > 0)
                    maxpublishtime.Value = int.Parse(cols[24]);
                else
                    maxpublishtime.Value = System.DBNull.Value;
                defaultactive.Value = conn.getBooleanValue(bool.Parse(cols[25]));
                singleuse.Value = conn.getBooleanValue(bool.Parse(cols[26]));
                if ((cols.Length > 27) && cols[27].Length > 0)
                    maxscriptstatements.Value = long.Parse(cols[27]);
                else
                    maxscriptstatements.Value = System.DBNull.Value;
                if ((cols.Length > 28) && cols[28].Length > 0)
                    maxinputrows.Value = int.Parse(cols[28]);
                else
                    maxinputrows.Value = System.DBNull.Value;
                if ((cols.Length > 29) && cols[29].Length > 0)
                    maxoutputrows.Value = int.Parse(cols[29]);
                else
                    maxoutputrows.Value = System.DBNull.Value;
                if ((cols.Length > 30) && cols[30].Length > 0)
                    scriptquerytimeout.Value = int.Parse(cols[30]);
                else
                    scriptquerytimeout.Value = System.DBNull.Value;
                if ((cols.Length > 31) && cols[31].Length > 0)
                    maxdeliveredreportsperday.Value = int.Parse(cols[31]);
                else
                    maxdeliveredreportsperday.Value = System.DBNull.Value;
                cmd.ExecuteNonQuery();
            }
            reader.Close();
            cmd.CommandText = "SELECT PRODUCT_ID FROM " + schema + "." + PRODUCTS_TABLE + " WHERE " + conn.getReservedName("DEFAULT") + "=1";
            cmd.Parameters.Clear();
            DefaultProductID = Int32.Parse(cmd.ExecuteScalar().ToString());
            cmd.Dispose();
            return true;
        }

        public static List<Product> getProducts(QueryConnection conn, string schema, Guid userID)
        {
            return getProducts(conn, schema, userID, false);
        }

        public static List<Product> getProducts(QueryConnection conn, string schema, Guid userID, bool ignoreEndDate)
        {
            List<Product> plist = new List<Product>();
            QueryCommand cmd = conn.CreateCommand();
            cmd.CommandText = "SELECT A.PRODUCT_ID,Name,Description," + conn.getReservedName("Default") + ",Type,AccountType,Category,PricingDetails,PricePerMonth,HasPromotion,PromotionCode,PromotionPrice,PromotionEndDate,PromotionDurationDays,RowLimit,DataLimit,ShareLimit,ShowAsOption,MaxAllowableData,MaxAllowableRows,MaxPublishData,MaxPublishRows,MaxPublishDataPerDay,MaxPublishRowsPerDay,MaxPublishTime,DefaultActive,SingleUse,MaxScriptStatements,MaxInputRows,MaxOutputRows,ScriptQueryTimeout,MaxDeliveredReportsPerDay,B.Quantity,B.Active,B.EndDate FROM "
                + schema + "." + PRODUCTS_TABLE + " A INNER JOIN " + schema + "." + USER_PRODUCT_TABLE +
                " B ON A.PRODUCT_ID=B.PRODUCT_ID WHERE B.USER_ID=?";
            if (!ignoreEndDate)
                cmd.CommandText += " AND (B.EndDate IS NULL OR B.EndDate > " + conn.getCurrentTimestamp() + ")";
            cmd.Parameters.Add("USER_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(userID);
            QueryReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                Product p = new Product();
                p.ProductID = reader.GetInt32(0);
                p.Name = reader.GetString(1);
                p.Description = reader.IsDBNull(2) ? null : reader.GetString(2);
                p.Default = reader.GetBoolean(3);
                p.Type = reader.GetInt32(4);
                p.AccountType = reader.GetInt32(5);
                p.Category = reader.IsDBNull(6) ? null : reader.GetString(6);
                p.PricingDetails = reader.IsDBNull(7) ? null : reader.GetString(7);
                p.PricePerMonth = reader.IsDBNull(8) ? 0 : reader.GetDecimal(8);
                p.HasPromotion = reader.GetBoolean(9);
                p.PromotionCode = reader.IsDBNull(10) ? null : reader.GetString(10);
                p.PromotionPrice = reader.IsDBNull(11) ? 0 : reader.GetDecimal(11);
                p.PromotionEndDate = reader.IsDBNull(12) ? DateTime.Now : reader.GetDateTime(12);
                p.PromotionDurationDays = reader.IsDBNull(13) ? 0 : reader.GetInt32(13);
                p.RowLimit = reader.IsDBNull(14) ? 0 : reader.GetInt32(14);
                p.DataLimit = reader.IsDBNull(15) ? 0 : reader.GetInt64(15);
                p.ShareLimit = reader.IsDBNull(16) ? 0 : reader.GetInt32(16);
                p.ShowAsOption = reader.GetBoolean(17);
                if (!reader.IsDBNull(18))
                    p.MaxAllowableData = reader.GetInt64(18);
                if (!reader.IsDBNull(19))
                    p.MaxAllowableRows = reader.GetInt32(19);
                if (!reader.IsDBNull(20))
                    p.MaxPublishData = reader.GetInt64(20);
                if (!reader.IsDBNull(21))
                    p.MaxPublishRows = reader.GetInt32(21);
                if (!reader.IsDBNull(22))
                    p.MaxPublishDataPerDay = reader.GetInt64(22);
                if (!reader.IsDBNull(23))
                    p.MaxPublishRowsPerDay = reader.GetInt32(23);
                if (!reader.IsDBNull(24))
                    p.MaxPublishTime = reader.GetInt32(24);
                p.DefaultActive = reader.GetBoolean(25);
                p.SingleUse = reader.GetBoolean(26);
                if (!reader.IsDBNull(27))
                    p.MaxScriptStatements = reader.GetInt64(27);
                if (!reader.IsDBNull(28))
                    p.MaxInputRows = reader.GetInt32(28);
                if (!reader.IsDBNull(29))
                    p.MaxOutputRows = reader.GetInt32(29);
                if (!reader.IsDBNull(30))
                    p.ScriptQueryTimeout = reader.GetInt32(30);
                if (!reader.IsDBNull(31))
                    p.MaxDeliveredReportsPerDay = reader.GetInt32(31);
                p.Quantity = reader.GetInt32(32);
                p.Active = reader.GetBoolean(33);
                if (!reader.IsDBNull(34))
                    p.EndDate = reader.GetDateTime(34);
                // Only add products which are active
                if (p.Active)
                {
                    plist.Add(p);
                }
            }
            reader.Close();
            cmd.Dispose();
            return (plist);
        }

        public static List<Product> getProducts(QueryConnection conn, string schema)
        {
            return getProducts(conn, schema, -1);
        }

        public static List<Product> getProducts(QueryConnection conn, string schema, int accountType)
        {
            List<Product> plist = new List<Product>();
            QueryCommand cmd = conn.CreateCommand();
            cmd.CommandText = "SELECT PRODUCT_ID,Name,Description," + conn.getReservedName("DEFAULT") + ",Type,AccountType,Category,PricingDetails,PricePerMonth,HasPromotion,PromotionCode,PromotionPrice,PromotionEndDate,PromotionDurationDays,RowLimit,DataLimit,ShareLimit,ShowAsOption,MaxAllowableData,MaxAllowableRows,MaxPublishData,MaxPublishRows,MaxPublishDataPerDay,MaxPublishRowsPerDay,MaxPublishTime,MaxScriptStatements,MaxInputRows,MaxOutputRows,ScriptQueryTimeout,MaxDeliveredReportsPerDay,DefaultActive,SingleUse FROM "
                + schema + "." + PRODUCTS_TABLE;
            if (accountType >= 0)
            {
                cmd.CommandText = cmd.CommandText + " WHERE AccountType = ?";
                cmd.Parameters.Add("@AccountType", OdbcType.Int).Value = accountType;
            }

            QueryReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                Product p = new Product();
                p.ProductID = reader.GetInt32(0);
                p.Name = reader.GetString(1);
                p.Description = reader.GetString(2);
                p.Default = reader.GetBoolean(3);
                p.Type = reader.GetInt32(4);
                p.AccountType = reader.GetInt32(5);
                p.Category = reader.GetString(6);
                p.PricingDetails = reader.GetString(7);
                p.PricePerMonth = reader.IsDBNull(8) ? 0 : reader.GetDecimal(8);
                p.HasPromotion = reader.GetBoolean(9);
                p.PromotionCode = reader.GetString(10);
                p.PromotionPrice = reader.IsDBNull(11) ? 0 : reader.GetDecimal(11);
                if (!reader.IsDBNull(12))
                    p.PromotionEndDate = reader.GetDateTime(12);
                p.PromotionDurationDays = reader.IsDBNull(13) ? 0 : reader.GetInt32(13);
                p.RowLimit = reader.IsDBNull(14) ? 0 : reader.GetInt32(14);
                p.DataLimit = reader.IsDBNull(15) ? 0 : reader.GetInt64(15);
                p.ShareLimit = reader.IsDBNull(16) ? 0 : reader.GetInt32(16);
                p.ShowAsOption = reader.GetBoolean(17);
                if (!reader.IsDBNull(18))
                    p.MaxAllowableData = reader.GetInt64(18);
                if (!reader.IsDBNull(19))
                    p.MaxAllowableRows = reader.GetInt32(19);
                if (!reader.IsDBNull(20))
                    p.MaxPublishData = reader.GetInt64(20);
                if (!reader.IsDBNull(21))
                    p.MaxPublishRows = reader.GetInt32(21);
                if (!reader.IsDBNull(22))
                    p.MaxPublishDataPerDay = reader.GetInt64(22);
                if (!reader.IsDBNull(23))
                    p.MaxPublishRowsPerDay = reader.GetInt32(23);
                if (!reader.IsDBNull(24))
                    p.MaxPublishTime = reader.GetInt32(24);
                if (!reader.IsDBNull(25))
                    p.MaxScriptStatements = reader.GetInt64(25);
                if (!reader.IsDBNull(26))
                    p.MaxInputRows = reader.GetInt32(26);
                if (!reader.IsDBNull(27))
                    p.MaxOutputRows = reader.GetInt32(27);
                if (!reader.IsDBNull(28))
                    p.ScriptQueryTimeout = reader.GetInt32(28);
                if (!reader.IsDBNull(29))
                    p.MaxDeliveredReportsPerDay = reader.GetInt32(29);
                p.DefaultActive = reader.GetBoolean(30);
                p.SingleUse = reader.GetBoolean(31);
                if (p.PromotionEndDate == DateTime.MinValue && p.PromotionDurationDays > 0)
                {
                    p.PromotionEndDate = DateTime.Now.AddDays(p.PromotionDurationDays);
                }
                plist.Add(p);
            }
            reader.Close();
            cmd.Dispose();
            return (plist);
        }

        public static bool hasBeenUsed(QueryConnection conn, string schema, int productID)
        {
            List<Product> plist = new List<Product>();
            QueryCommand cmd = null;
            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = "SELECT COUNT(*) FROM " + schema + "." + USER_PRODUCT_TABLE +
                    " WHERE PRODUCT_ID=? AND EndDate IS NULL";
                cmd.Parameters.Add("PRODUCT_ID", OdbcType.Int).Value = productID;
                Int32 result = Int32.Parse(cmd.ExecuteScalar().ToString());
                return (result > 0);
            }
            finally
            {
                if (cmd != null)
                    cmd.Dispose();
            }
        }

        public static void setProducts(QueryConnection conn, string schema, Guid userID, List<Product> existingProducts, List<Product> newProducts)
        {
            QueryCommand cmd = conn.CreateCommand();
            cmd.CommandText = "UPDATE "
                + schema + "." + USER_PRODUCT_TABLE + " SET EndDate=" + conn.getCurrentTimestamp() + ", Active=0 WHERE USER_ID=? AND PRODUCT_ID=?";
            cmd.Parameters.Add("USER_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(userID);
            DbParameter prod = cmd.Parameters.Add("PRODUCT_ID", OdbcType.Int);
            List<Product> samelist = new List<Product>();
            foreach (Product p in existingProducts)
            {
                // See if exists or changed
                Product product = null;
                foreach (Product np in newProducts)
                {
                    if (np.ProductID == p.ProductID)
                    {
                        product = np;
                        break;
                    }
                    else if (p.Type == Product.TYPE_EDITION && np.Type == Product.TYPE_EDITION)
                    {
                        product = p;
                        break;
                    }
                }
                if (product != null)
                {
                    if (product.Quantity != p.Quantity || p.Type == Product.TYPE_EDITION)
                    {
                        prod.Value = product.ProductID;
                        cmd.ExecuteNonQuery();
                    }
                    else
                        samelist.Add(product);
                }
                else
                {
                    // Product has been removed so delete
                    prod.Value = p.ProductID;
                    cmd.ExecuteNonQuery();
                }
            }
            // Insert command
            cmd.CommandText = "INSERT INTO " + schema + "." + USER_PRODUCT_TABLE +
                " (USER_ID,PRODUCT_ID,StartDate,Quantity,Active) VALUES (?,?,?,?,?)";
            cmd.Parameters.Add("StartDate", OdbcType.DateTime).Value = DateTime.Now;
            DbParameter quantity = cmd.Parameters.Add("Quantity", OdbcType.Int);
            DbParameter active = cmd.Parameters.Add("Active", conn.getODBCBooleanType());
            foreach (Product np in newProducts)
            {
                if (samelist.Contains(np))
                    continue;
                prod.Value = np.ProductID;
                quantity.Value = np.Quantity;
                active.Value = conn.getBooleanValue(np.Active);
                cmd.ExecuteNonQuery();
            }
            cmd.Dispose();
        }

        public static void disableProducts(QueryConnection conn, string schema, Guid userID, List<Product> plist)
        {
            QueryCommand cmd = null;
            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = "UPDATE "
                    + schema + "." + USER_PRODUCT_TABLE + " SET EndDate=" + conn.getCurrentTimestamp() + " WHERE USER_ID=? AND PRODUCT_ID=?";
                cmd.Parameters.Add("USER_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(userID);
                DbParameter prod = cmd.Parameters.Add("PRODUCT_ID", OdbcType.Int);
                foreach (Product p in plist)
                {
                    prod.Value = p.ProductID;
                    cmd.ExecuteNonQuery();
                }
            }
            finally
            {
                if (cmd != null)
                    cmd.Dispose();
            }
        }

        public static void enableProducts(QueryConnection conn, string schema, Guid userID, List<Product> plist)
        {
            QueryCommand cmd = null;
            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = "UPDATE "
                    + schema + "." + USER_PRODUCT_TABLE + " SET EndDate=NULL WHERE USER_ID=? AND PRODUCT_ID=?";
                cmd.Parameters.Add("USER_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(userID);
                DbParameter prod = cmd.Parameters.Add("PRODUCT_ID", OdbcType.Int);
                foreach (Product p in plist)
                {
                    prod.Value = p.ProductID;
                    cmd.ExecuteNonQuery();
                }
            }
            finally
            {
                if (cmd != null)
                    cmd.Dispose();
            }
        }

        public static void addTxnCommandHistory(QueryConnection conn, bool isIB, string schema, string tableName, string loadGroup, object[] rowObj)
        {
            DateTime TM = DateTime.MinValue;
            string COMMAND_TYPE = null, STEP = null, SUBSTEP = null, PROCESSINGGROUP = null, ITERATION = null, MESSAGE = null;
            int STATUS = -1, NUMROWS = -1, NUMERRORS = -1, NUMWARNINGS = -1, DURATION = -1;
            for (int i = 0; i < rowObj.Length; i++)
            {
                switch (i)
                {
                    case 0:
                        string[] formats = { "yyyy'-'MM'-'dd HH':'mm':'ss'.'fff" };
                        if (!DateTime.TryParseExact(rowObj[i].ToString(), formats, null, System.Globalization.DateTimeStyles.None, out TM))
                            throw new Exception("Cannot parse date field, unable to insert records in " + schema + "." + tableName + " table");
                        break;
                    case 1:
                        COMMAND_TYPE = rowObj[i].ToString();
                        break;
                    case 2:
                        STEP = rowObj[i].ToString();
                        break;
                    case 3:
                        if (rowObj[i] == null || ((string)rowObj[i]) == "")
                            SUBSTEP = loadGroup; //make sure to use loadgroup for generateschema substep
                        else
                            SUBSTEP = rowObj[i].ToString();
                        break;
                    case 4:
                        if (rowObj[i] != null && rowObj[i].ToString().StartsWith("\"") && rowObj[i].ToString().EndsWith("\""))
                            rowObj[i] = rowObj[i].ToString().Substring(1, rowObj[i].ToString().Length - 2);
                        ITERATION = rowObj[i].ToString();
                        break;
                    case 5:
                        if (rowObj[i] != null && rowObj[i].ToString().StartsWith("\"") && rowObj[i].ToString().EndsWith("\""))
                            rowObj[i] = rowObj[i].ToString().Substring(1, rowObj[i].ToString().Length - 2);
                        STATUS = int.Parse(rowObj[i].ToString());
                        break;
                    case 6:
                        if (rowObj[i] != null && rowObj[i].ToString().StartsWith("\"") && rowObj[i].ToString().EndsWith("\""))
                            rowObj[i] = rowObj[i].ToString().Substring(1, rowObj[i].ToString().Length - 2);
                        NUMROWS = rowObj[i] == null ? -1 : int.Parse(rowObj[i].ToString());
                        break;
                    case 7:
                        if (rowObj[i] != null && rowObj[i].ToString().StartsWith("\"") && rowObj[i].ToString().EndsWith("\""))
                            rowObj[i] = rowObj[i].ToString().Substring(1, rowObj[i].ToString().Length - 2);
                        NUMERRORS = rowObj[i] == null ? -1 : int.Parse(rowObj[i].ToString());
                        break;
                    case 8:
                        if (rowObj[i] != null && rowObj[i].ToString().StartsWith("\"") && rowObj[i].ToString().EndsWith("\""))
                            rowObj[i] = rowObj[i].ToString().Substring(1, rowObj[i].ToString().Length - 2);
                        NUMWARNINGS = rowObj[i] == null ? -1 : int.Parse(rowObj[i].ToString());
                        break;
                    case 9:
                        if (rowObj[i] != null && rowObj[i].ToString().StartsWith("\"") && rowObj[i].ToString().EndsWith("\""))
                            rowObj[i] = rowObj[i].ToString().Substring(1, rowObj[i].ToString().Length - 2);
                        DURATION = rowObj[i] == null ? -1 : int.Parse(rowObj[i].ToString());
                        break;
                    case 10:
                        MESSAGE = rowObj[i] == null ? null : rowObj[i].ToString();
                        break;
                    case 11:
                        PROCESSINGGROUP = rowObj[i] == null ? null : rowObj[i].ToString();
                        break;
                }
            }
            QueryCommand cmd = conn.CreateCommand();
            string insertSQL = null;
            if (isIB)
            {
                insertSQL = "INSERT INTO " + schema + "." + tableName +
                     " (TM,COMMAND_TYPE,STEP,SUBSTEP,ITERATION,STATUS,NUMROWS,NUMERRORS,NUMWARNINGS,DURATION,MESSAGE" +
                     (rowObj.Length == 11 ? "" : ",PROCESSINGGROUP") +
                     ") VALUES ('" + TM.ToString("yyyy'-'MM'-'dd HH':'mm':'ss'.'fff") + "','" + COMMAND_TYPE + "','" + STEP + "','" + SUBSTEP + "','" + ITERATION + "'," + STATUS +
                     (NUMROWS != -1 ? "," + NUMROWS : ",NULL") +
                     (NUMERRORS != -1 ? "," + NUMERRORS : ",NULL") +
                     (NUMWARNINGS != -1 ? "," + NUMWARNINGS : ",NULL") +
                     (DURATION != -1 ? "," + DURATION : ",NULL") +
                     (MESSAGE != null ? ",'" + MESSAGE + "'" : ",NULL") +
                     (rowObj.Length == 11 ? "" : (PROCESSINGGROUP != null ? ",'" + PROCESSINGGROUP + "'" : ",NULL")) +
                     ")";
            }
            else
            {
                insertSQL = "INSERT INTO " + schema + "." + tableName +
                    " (TM,COMMAND_TYPE,STEP,SUBSTEP,ITERATION,STATUS,NUMROWS,NUMERRORS,NUMWARNINGS,DURATION,MESSAGE" +
                    (rowObj.Length == 11 ? "" : ",PROCESSINGGROUP") +
                    ") VALUES ('" + TM.ToString("yyyy'-'MM'-'dd HH':'mm':'ss'.'fff") + "','" + COMMAND_TYPE + "','" + STEP + "','" + SUBSTEP + "','" + ITERATION + "'," + STATUS +
                    (NUMROWS != -1 ? "," + NUMROWS : ",NULL") +
                    (NUMERRORS != -1 ? "," + NUMERRORS : ",NULL") +
                    (NUMWARNINGS != -1 ? "," + NUMWARNINGS : ",NULL") +
                    (DURATION != -1 ? "," + DURATION : ",NULL") +
                    (MESSAGE != null ? ",'" + MESSAGE + "'" : ",NULL") +
                    (rowObj.Length == 11 ? "" : (PROCESSINGGROUP != null ? ",'" + PROCESSINGGROUP + "'" : ",NULL")) +
                    ")";
            }
            cmd.CommandText = insertSQL;
            cmd.ExecuteNonQuery();
            cmd.Dispose();
        }

        public static void addPublish(QueryConnection conn, string schema, Guid spaceID, int loadNumber,
            int numRows, long dataSize, DateTime loadDate, string tableName, string loadGroup, DateTime startPublishDate)
        {
            addPublish(conn, schema, spaceID, loadNumber, numRows, dataSize, loadDate, DateTime.Now, tableName, loadGroup, startPublishDate);
        }

        public static void addPublish(QueryConnection conn, string schema, Guid spaceID, int loadNumber,
            int numRows, long dataSize, DateTime loadDate, DateTime publishDate, string tableName, string loadGroup, DateTime startPublishDate)
        {
            QueryCommand cmd = conn.CreateCommand();
            cmd.CommandText = "INSERT INTO " + schema + "." + PUBLISH_TABLE +
                " (SPACE_ID,LoadNumber,PublishDate,LoadDate,NumRows,DataSize,TableName,LoadGroup, StartPublishDate) VALUES (?,?,?,?,?,?,?,?,?)";
            cmd.Parameters.Add("SpaceID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(spaceID);
            cmd.Parameters.Add("LoadNumber", OdbcType.Int).Value = loadNumber;
            if (publishDate == DateTime.MinValue || publishDate == null)
            {
                publishDate = loadDate;
            }
            cmd.Parameters.Add("PublishDate", OdbcType.DateTime).Value = truncateDateTime(publishDate);
            cmd.Parameters.Add("LoadDate", OdbcType.DateTime).Value = truncateDateTime(loadDate);
            cmd.Parameters.Add("NumRows", OdbcType.Int).Value = numRows;
            cmd.Parameters.Add("DataSize", conn.getODBCBigIntType()).Value = dataSize;
            cmd.Parameters.Add("TableName", OdbcType.VarChar).Value = tableName;
            if (loadGroup == null || loadGroup.Length == 0)
                loadGroup = Util.LOAD_GROUP_NAME;
            cmd.Parameters.Add("LoadGroup", OdbcType.VarChar).Value = loadGroup;
            if (startPublishDate == DateTime.MinValue || startPublishDate == null)
            {
                startPublishDate = publishDate;
            }
            cmd.Parameters.Add("StartPublishDate", OdbcType.DateTime).Value = truncateDateTime(startPublishDate);
            cmd.ExecuteNonQuery();
            cmd.Dispose();
        }

        public static List<Publish> getPublishDetail(QueryConnection conn, string schema, Guid spaceID)
        {
            QueryCommand cmd = conn.CreateCommand();
            cmd.CommandText = "SELECT TableName,LoadNumber,PublishDate,LoadDate,LoadGroup,NumRows,DataSize,StartPublishDate FROM " +
                schema + "." + PUBLISH_TABLE + " WHERE SPACE_ID=?";
            cmd.Parameters.Add("SPACE_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(spaceID);
            QueryReader reader = cmd.ExecuteReader();
            List<Publish> plist = new List<Publish>();
            while (reader.Read())
            {
                Publish p = new Publish();
                p.SpaceID = spaceID;
                p.TableName = reader.GetString(0);
                p.LoadNumber = reader.GetInt32(1);
                if (!reader.IsDBNull(2))
                    p.PublishDate = reader.GetDateTime(2);
                if (!reader.IsDBNull(3))
                    p.LoadDate = reader.GetDateTime(3);
                p.LoadGroup = reader.IsDBNull(4) ? Util.LOAD_GROUP_NAME : reader.GetString(4);
                if (!reader.IsDBNull(5))
                    p.NumRows = reader.GetInt32(5);
                if (!reader.IsDBNull(6))
                    p.DataSize = reader.GetInt64(6);
                if (!reader.IsDBNull(7))
                    p.StartPublishDate = reader.GetDateTime(7);
                plist.Add(p);
            }
            cmd.Dispose();
            return (plist);
        }

        public static List<Publish> getPublishes(QueryConnection conn, string schema, Guid spaceID)
        {
            QueryCommand cmd = conn.CreateCommand();
            cmd.CommandText = "SELECT LoadNumber,PublishDate,LoadDate,LoadGroup,SUM(NumRows),SUM(DataSize), StartPublishDate FROM " +
                schema + "." + PUBLISH_TABLE + " WHERE SPACE_ID=? GROUP BY LoadNumber,PublishDate,LoadDate,LoadGroup, StartPublishDate ORDER BY PublishDate";
            cmd.Parameters.Add("SPACE_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(spaceID);
            QueryReader reader = cmd.ExecuteReader();
            List<Publish> plist = new List<Publish>();
            while (reader.Read())
            {
                Publish p = new Publish();
                p.SpaceID = spaceID;
                p.LoadNumber = reader.GetInt32(0);
                if (!reader.IsDBNull(1))
                    p.PublishDate = reader.GetDateTime(1);
                if (!reader.IsDBNull(2))
                    p.LoadDate = reader.GetDateTime(2);
                p.LoadGroup = reader.IsDBNull(3) ? Util.LOAD_GROUP_NAME : reader.GetString(3);
                if (!reader.IsDBNull(4))
                    p.NumRows = reader.GetInt32(4);
                if (!reader.IsDBNull(5))
                    p.DataSize = reader.GetInt64(5);
                if (!reader.IsDBNull(6))
                {
                    p.StartPublishDate = reader.GetDateTime(6);
                }
                plist.Add(p);
            }
            cmd.Dispose();
            return (plist);
        }

        public static void setPublishesID(QueryConnection conn, string schema, Guid curSpaceID, Guid newSpaceID)
        {
            QueryCommand cmd = null;
            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = "UPDATE " + schema + "." + PUBLISH_TABLE + " SET SPACE_ID=? WHERE SPACE_ID=?";
                cmd.Parameters.Add("SPACE_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(newSpaceID);
                cmd.Parameters.Add("SPACE_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(curSpaceID);
                cmd.ExecuteNonQuery();
            }
            finally
            {
                if (cmd != null)
                    cmd.Dispose();
            }
            return;
        }

        public static List<Publish> getTablePublishes(QueryConnection conn, string schema, Guid spaceID, string tableName)
        {
            QueryReader reader = null;
            QueryCommand cmd = null;
            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = "SELECT LoadNumber,PublishDate,LoadDate,LoadGroup,SUM(NumRows),SUM(DataSize) FROM " +
                    schema + "." + PUBLISH_TABLE + " WHERE SPACE_ID=? AND TableName=? GROUP BY LoadNumber,PublishDate,LoadDate,LoadGroup ORDER BY PublishDate";
                cmd.Parameters.Add("SPACE_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(spaceID);
                cmd.Parameters.Add("TableName", OdbcType.VarChar).Value = tableName;
                reader = cmd.ExecuteReader();
                List<Publish> plist = new List<Publish>();
                while (reader.Read())
                {
                    Publish p = new Publish();
                    p.SpaceID = spaceID;
                    p.LoadNumber = reader.GetInt32(0);
                    p.PublishDate = reader.GetDateTime(1);
                    p.LoadDate = reader.GetDateTime(2);
                    p.LoadGroup = reader.GetString(3);
                    p.NumRows = reader.GetInt32(4);
                    p.DataSize = reader.GetInt64(5);
                    plist.Add(p);
                }
                return (plist);
            }
            finally
            {
                if (reader != null)
                    reader.Close();
                if (cmd != null)
                    cmd.Dispose();
            }
        }

        public static void addPolicyLoadID(QueryConnection conn, Space sp, string tname, string policyName, int loadid)
        {
            QueryCommand cmd = null;
            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = "INSERT INTO " + sp.Schema + "." + tname + " (Policy,LOADID) VALUES (?,?)";
                cmd.Parameters.Add("Policy", OdbcType.VarChar).Value = policyName;
                cmd.Parameters.Add("LoadID", OdbcType.Int).Value = loadid;
                cmd.ExecuteNonQuery();
            }
            finally
            {
                if (cmd != null)
                    cmd.Dispose();
            }
        }

        public static Dictionary<string, int> getCurrentLoadNumbers(QueryConnection conn, string schema, Guid spaceID)
        {
            List<Publish> plist = Database.getPublishes(conn, schema, spaceID);
            Dictionary<string, int> result = new Dictionary<string, int>();
            foreach (Publish p in plist)
            {
                if (p.LoadGroup != null)
                {
                    string[] groups = p.LoadGroup.Split(new char[] { ',' });
                    foreach (string s in groups)
                    {
                        if (!result.ContainsKey(s))
                        {
                            result.Add(s, p.LoadNumber);
                        }
                        else
                        {
                            int current = result[s];
                            if (current < p.LoadNumber)
                                result[s] = p.LoadNumber;
                        }
                    }
                }
            }
            return result;
        }

        public static void removePublishes(QueryConnection conn, string schema, Guid spaceID, int loadNumber)
        {
            QueryCommand cmd = null;
            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = "DELETE FROM " + schema + "." + PUBLISH_TABLE +
                    " WHERE SPACE_ID=?";
                if (loadNumber >= 0)
                    cmd.CommandText += " AND LoadNumber=?";
                cmd.Parameters.Add("SpaceID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(spaceID);
                if (loadNumber >= 0)
                    cmd.Parameters.Add("LoadNumber", OdbcType.Int).Value = loadNumber;
                cmd.ExecuteNonQuery();
            }
            finally
            {
                if (cmd != null)
                    cmd.Dispose();
            }
        }

        public static List<DatabaseIndex> getIndices(QueryConnection conn, string schema)
        {
            return getIndices(conn, schema, "%");
        }

        public static List<DatabaseIndex> getIndices(QueryConnection conn, string schema, string prefix)
        {
            if (!conn.supportsIndexes())
            {
                return new List<DatabaseIndex>();
            }

            QueryReader reader = null;
            QueryCommand cmd = null;
            try
            {
                cmd = conn.CreateCommand();
                bool isOracle = conn.isDBTypeOracle();
                string escapeStr = Util.getEscapeStringForDB(conn, prefix);
                if (isOracle)
                {
                    cmd.CommandText = "SELECT INDEX_NAME, COLUMN_POSITION AS COLUMNNUMBER, 0, COLUMN_NAME, TABLE_NAME, INDEX_OWNER FROM ALL_IND_COLUMNS WHERE INDEX_OWNER = ? AND TABLE_NAME LIKE ? " + escapeStr;
                    cmd.Parameters.Add("INDEX_OWNER", OdbcType.VarChar).Value = schema;
                    cmd.Parameters.Add("TABLE_NAME", OdbcType.VarChar).Value = prefix;
                }
                else
                {
                    cmd.CommandText = "select a.name AS INDEX_NAME,b.index_column_id AS ColumnNumber,b.is_included_column AS Included," +
                    "c.name AS COLUMN_NAME,d.name AS TABLE_NAME,e.name AS SCHEMA_NAME from " +
                    "sys.indexes a inner join sys.index_columns b on a.index_id=b.index_id and a.object_id=b.object_id " +
                    "inner join sys.columns c on b.object_id=c.object_id and b.column_id=c.column_id " +
                    "inner join sys.tables d on a.object_id=d.object_id " +
                    "inner join sys.schemas e on d.schema_id=e.schema_id " +
                    "where e.name=? and d.name LIKE ? " + escapeStr + " and a.type=2 order by a.index_id,b.index_column_id";
                    cmd.Parameters.Add("SCHEMA", OdbcType.VarChar).Value = schema;
                    cmd.Parameters.Add("TABLE", OdbcType.VarChar).Value = prefix;
                }
                reader = cmd.ExecuteReader();
                Dictionary<string, DatabaseIndex> indexes = new Dictionary<string, DatabaseIndex>();
                while (reader.Read())
                {
                    string indexName = reader.GetString(0);
                    int colNum = reader.GetInt32(1);
                    bool included = reader.GetBoolean(2);
                    string colName = reader.GetString(3);
                    string tableName = reader.GetString(4);
                    string schemaName = reader.GetString(5);
                    DatabaseIndex di = null;
                    if (indexes.ContainsKey(indexName + "$" + tableName))
                        di = indexes[indexName + "$" + tableName];
                    else
                    {
                        di = new DatabaseIndex();
                        di.Name = indexName;
                        di.SchemaName = schemaName;
                        di.TableName = tableName;
                        di.Columns = new string[0];
                        di.IncludedColumns = new string[0];
                        indexes.Add(indexName + "$" + tableName, di);
                    }
                    if (included)
                    {
                        List<string> nlist = new List<string>(di.IncludedColumns);
                        nlist.Add(colName);
                        di.IncludedColumns = nlist.ToArray();
                    }
                    else
                    {
                        List<string> nlist = new List<string>(di.Columns);
                        nlist.Add(colName);
                        di.Columns = nlist.ToArray();
                    }
                }
                List<DatabaseIndex> ilist = new List<DatabaseIndex>(indexes.Values);
                return (ilist);
            }
            finally
            {
                if (reader != null)
                    reader.Close();
                if (cmd != null)
                    cmd.Dispose();
            }
        }

        public static List<string> getTables(QueryConnection conn, string schema)
        {
            return getTables(conn, schema, "%");
        }

        public static List<string> getTables(QueryConnection conn, string schema, string prefix)
        {
            QueryCommand cmd = null;
            QueryReader reader = null;
            string getTablesForSchemaQuery = "";
            string escapeStr = Util.getEscapeStringForDB(conn, prefix);
            if (conn.isDBTypeOracle())
            {
                schema = schema.ToUpper();
                getTablesForSchemaQuery = "SELECT TABLE_NAME FROM ALL_TABLES WHERE OWNER=? AND TABLE_NAME LIKE ? " + escapeStr;
            }
            else if (conn.isDBTypeSAPHana())
            {
                getTablesForSchemaQuery = "SELECT TABLE_NAME FROM SYS.TABLES WHERE SCHEMA_NAME=? AND TABLE_NAME LIKE ? " + escapeStr;
            }
            else if (conn.isDBTypeSQLServer())
            {
                getTablesForSchemaQuery = "select o.name, s.name from sys.objects o join sys.schemas s on o.schema_id = s.schema_id where o.type in ('S', 'U', 'V') and s.name=? and o.name like ? " + escapeStr;
            }
            else
            {
                getTablesForSchemaQuery = "SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA=? AND TABLE_NAME LIKE ? " + escapeStr;
            }
            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandTimeout = GET_TABLES_TIMEOUT;
                cmd.CommandText = getTablesForSchemaQuery;
                if (conn.isDBTypeSAPHana())
                {
                    schema = schema.ToUpper();
                    prefix = prefix.ToUpper();
                }
                else if (conn.isDBTypeParAccel())
                {
                    schema = schema.ToLower();
                    prefix = prefix.ToUpper();
                }
                cmd.Parameters.Add("SCHEMA", OdbcType.VarChar).Value = schema;
                cmd.Parameters.Add("TABLE", OdbcType.VarChar).Value = prefix;

                reader = cmd.ExecuteReader();
                List<string> tlist = new List<string>();
                while (reader.Read())
                {
                    tlist.Add(reader.GetString(0));
                }
                return (tlist);
            }
            finally
            {
                if (reader != null)
                    reader.Close();
                if (cmd != null)
                    cmd.Dispose();
            }
        }

        public static List<string> getAggregateTables(QueryConnection connection, Space sp)
        {
            List<string> tlist = new List<string>();

            string aggregateFileName = sp.Directory + "\\" + "listaggregatesphysical.cmd";
            StringBuilder commands = new StringBuilder();
            commands.Append("repository " + sp.Directory + "\\repository.xml\n");
            commands.Append("listaggregatesphysical");

            File.WriteAllText(aggregateFileName, commands.ToString());

            string enginecmd = Util.getEngineCommand(sp);

            //Unload tables
            ProcessStartInfo psi = new ProcessStartInfo(enginecmd);
            psi.Arguments = sp.Directory + "\\listaggregatesphysical.cmd " + sp.Directory;
            psi.WindowStyle = ProcessWindowStyle.Hidden;
            psi.RedirectStandardOutput = true;
            psi.CreateNoWindow = true;
            psi.UseShellExecute = false;
            psi.RedirectStandardInput = false;
            psi.RedirectStandardError = true;
            psi.WorkingDirectory = (new FileInfo(enginecmd)).Directory.FullName;
            Process proc = Process.Start(psi);
            proc.WaitForExit();
            string line = null;
            while ((line = proc.StandardError.ReadLine()) != null)
            {
                Global.systemLog.Warn(line);
            }
            while ((line = proc.StandardOutput.ReadLine()) != null)
            {
                if (line != null && line.Length > 0 && tableExists(connection, sp.Schema, line))
                {
                    tlist.Add(line);
                }
            }
            if (proc.HasExited)
            {
                Global.systemLog.Info("List Aggregates Physical (" + sp.ID + ") Exited with code: " + proc.ExitCode);
            }
            else
            {
                Global.systemLog.Info("List Aggregates Physical (" + sp.ID + ") - Running");
                proc.WaitForExit();
            }
            proc.Close();

            return (tlist);
        }

        public static int copyTable(QueryConnection conn, string tableName, string sourceSchema, string targetSchema,bool isInfoBrightConnection)
        {
            QueryCommand cmd = null;
            try
            {
                cmd = conn.CreateCommand();
                if (conn.Driver.StartsWith("myodbc"))
                {
                    //For IB Retain all the source table properties including collation
                    if (isInfoBrightConnection)
                    {
                        cmd.CommandText = "CREATE TABLE " + targetSchema + "." + tableName + "  LIKE " + sourceSchema + "." + tableName;
                        cmd.ExecuteNonQuery();
                        if (cmd != null)
                        {
                            cmd.Dispose();
                            cmd = null;
                            cmd = conn.CreateCommand();
                        }
                        cmd.CommandText = "INSERT INTO " + targetSchema + "." + tableName + "  SELECT * FROM " + sourceSchema + "." + tableName;
                    }
                    else
                    {
                        cmd.CommandText = "CREATE TABLE " + targetSchema + "." + tableName +
                            " SELECT * FROM " + sourceSchema + "." + tableName;
                    }
                }
                else if (conn.isDBTypeOracle())
                {
                    cmd.CommandText = "CREATE TABLE " + targetSchema + "." + tableName + " AS SELECT * FROM " + sourceSchema + "." + tableName;
                }
                else
                {
                    cmd.CommandText = "SELECT * INTO " + targetSchema + "." + tableName +
                        " FROM " + sourceSchema + "." + tableName;
                }
                cmd.CommandTimeout = COPY_TABLE_TIMEOUT;
                int rows = cmd.ExecuteNonQuery();
                return (rows);
            }
            finally
            {
                if (cmd != null)
                    cmd.Dispose();
            }
        }

        public static bool isValidPartnerCode(QueryConnection conn, string schema, string code)
        {
            QueryCommand cmd = null;
            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = "SELECT Count(*) FROM " +
                    schema + "." + PARTNER_CODE_TABLE + " WHERE Code=?";
                cmd.Parameters.Add("Code", OdbcType.VarChar).Value = code;
                int result = Int32.Parse(cmd.ExecuteScalar().ToString());
                return result > 0;
            }
            finally
            {
                if (cmd != null)
                    cmd.Dispose();
            }
        }

        public static int getCurrentVersion(QueryConnection conn, string schema)
        {
            QueryCommand cmd = null;
            try
            {
                bool isOracle = conn.isDBTypeOracle();
                cmd = conn.CreateCommand();
                cmd.CommandText = "SELECT CurrentVersion FROM " +
                    schema + "." + VERSION_TABLE_NAME;
                object o = cmd.ExecuteScalar();
                if (o == null)
                    return (-1);
                else
                {
                    return isOracle ? Int32.Parse(o.ToString()) : ((int)o);
                }
            }
            catch (Exception)
            {
                return -1;
            }
            finally
            {
                if (cmd != null)
                    cmd.Dispose();
            }
        }

        public static void updateCurrentVersion(QueryConnection conn, string schema, int newVersion)
        {
            QueryCommand cmd = null;
            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = "DELETE FROM  "
                    + schema + "." + VERSION_TABLE_NAME;
                cmd.ExecuteNonQuery();
                cmd.CommandText = "INSERT INTO  "
                    + schema + "." + VERSION_TABLE_NAME + " (CurrentVersion) VALUES (?)";
                cmd.Parameters.Add("CurrentVersion", OdbcType.Int).Value = newVersion;
                cmd.ExecuteNonQuery();
            }
            finally
            {
                if (cmd != null)
                    cmd.Dispose();
            }
        }

        public static int ALTER_ADD = 0;
        public static int ALTER_DROP = 1;
        public static int ALTER_COLUMN = 2;

        /*
        * Alter a table by adding or removing columns
        */
        public static bool alterTable(QueryConnection conn, string schema, String tableName, List<string> columnNames, List<string> columnTypes, List<int> actions)
        {
            if (conn == null)
            {
                return (false);
            }
            bool isOracle = conn.isDBTypeOracle();
            bool altered = false;
            bool exists = tableExists(conn, schema, tableName);
            if (exists)
            {
                for (int i = 0; i < columnNames.Count; i++)
                {
                    StringBuilder sb = new StringBuilder();
                    sb.Append("ALTER TABLE " + schema + "." + tableName);
                    if (actions[i] == ALTER_ADD)
                    {
                        sb.Append(" ADD " + columnNames[i] + " " + columnTypes[i]);
                    }
                    else if (actions[i] == ALTER_DROP)
                    {
                        if (isOracle)
                        {
                            sb.Append(" DROP COLUMN " + columnNames[i]);
                        }
                        else
                        {
                            sb.Append(" DROP " + columnNames[i]);
                        }
                    }
                    else if (actions[i] == ALTER_COLUMN)
                    {
                        if (isOracle)
                        {
                            sb.Append(" MODIFY( " + columnNames[i] + " " + columnTypes[i] + ")");
                        }
                        else
                        {
                            sb.Append(" ALTER COLUMN " + columnNames[i] + " " + columnTypes[i]);
                        }
                    }
                    QueryCommand cmd = conn.CreateCommand();
                    try
                    {
                        cmd.CommandText = sb.ToString();
                        Global.systemLog.Debug(cmd.CommandText);
                        cmd.ExecuteNonQuery();
                        altered = true;
                    }
                    catch (Exception ex)
                    {
                        Global.systemLog.Error("Unable to alter table[" + tableName + "]: " + ex.ToString());
                        altered = false;
                    }
                    finally
                    {
                        cmd.Dispose();
                    }
                }
                return altered;
            }
            else
            {
                Global.systemLog.Info("Table " + tableName + " does not exist - cannot alter");
                return false;
            }
        }

        public static void updateTaskSchedule(QueryConnection conn, string schema, Guid spaceID, Guid ID, string module, DateTime nextDate, string parameters)
        {
            updateTaskSchedule(conn, schema, spaceID, ID, module, nextDate, parameters, null);
        }

        public static void clearWorkingTasks(QueryConnection conn, string schema, Guid spaceID, Guid ID, string module, DateTime nextTime)
        {
            QueryCommand cmd = null;
            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = "UPDATE  "
                    + schema + "." + SCHEDULED_LOAD_TABLE + " SET WorkTime=NULL, WorkingServer=NULL " + (nextTime != DateTime.MinValue ? ", NextDate = ? " : "") + " WHERE SPACE_ID=? AND Module=?" + (ID != Guid.Empty ? " AND OBJ_ID=?" : "");
                if (nextTime != DateTime.MinValue)
                {
                    DateTime nextDateTime = nextTime;
                    if (module == ScheduledReport.MODULE_NAME || module == SalesforceLoader.SOURCE_GROUP_NAME)
                    {
                        nextDateTime = Util.getCustomizedSchedulerTime(nextTime);
                    }

                    cmd.Parameters.Add("NextDate", OdbcType.DateTime).Value = nextDateTime;
                }
                cmd.Parameters.Add("SPACE_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(spaceID);
                cmd.Parameters.Add("Module", OdbcType.VarChar).Value = module;
                DbParameter obj = cmd.Parameters.Add("ObjectID", conn.getODBCUniqueIdentiferType());
                if (ID != null && ID != Guid.Empty)
                    obj.Value = conn.getUniqueIdentifer(ID);
                else
                    obj.Value = System.DBNull.Value;
                int result = cmd.ExecuteNonQuery();
                if (result > 0)
                {
                    if (ID != null && ID != Guid.Empty)
                    {
                        Global.systemLog.Info(result + " tasks cleared for module " + module + ", space " + spaceID + ", and object " + ID + " from " + SCHEDULED_LOAD_TABLE);
                    }
                    else
                    {
                        Global.systemLog.Info(result + " tasks cleared for module " + module + " and space " + spaceID + " from " + SCHEDULED_LOAD_TABLE);
                    }
                }
            }
            finally
            {
                if (cmd != null)
                    cmd.Dispose();
            }
        }

        public static void clearDeadTasks(QueryConnection conn)
        {
            QueryReader resultReader = null;
            QueryCommand cmd = null;
            try
            {
                cmd = conn.CreateCommand();
                // get the spaces which are running for more than maxPublishTime
                cmd.CommandText = "select load_schedule.space_id, directory, workTime from load_schedule " +
                    "inner join spaces on load_schedule.space_id=spaces.id " +
                    "inner join user_space_inxn on spaces.id=user_space_inxn.space_id " +
                    "inner join user_product_inxn on user_space_inxn.user_id=user_product_inxn.user_id " +
                    "inner join products on user_product_inxn.product_id=products.product_id " +
                    "where worktime is not null and Owner=1 " +
                    "group by load_schedule.space_id, directory, workTime " +
                    "having DATEDIFF(MINUTE,worktime,GetDATE())>max(MaxPublishTime) ";

                resultReader = cmd.ExecuteReader();
                if (resultReader.HasRows)
                {
                    Dictionary<Guid, string> spacesDirectoryMap = new Dictionary<Guid, string>();
                    while (resultReader.Read())
                    {
                        if (!resultReader.IsDBNull(0) && !resultReader.IsDBNull(1))
                        {
                            Guid spaceID = resultReader.GetGuid(0);
                            string spaceDir = resultReader.GetString(1);
                            if (!spacesDirectoryMap.ContainsKey(spaceID))
                            {
                                spacesDirectoryMap.Add(spaceID, spaceDir);
                                Global.systemLog.Debug("clearDeadTasks: Load Schedule reset : Load Running/Hung for more than MaxPublishTime for " + spaceID);
                            }
                        }
                    }

                    if (spacesDirectoryMap != null && spacesDirectoryMap.Count > 0)
                    {
                        Util.clearSFDCExtractLockFiles(spacesDirectoryMap);
                        Util.clearSFDCKillLockFiles(spacesDirectoryMap);
                    }
                }

                cmd.CommandText = "update load_schedule set worktime=null,workingserver=null,nextdate=DateAdd(Hour,6,A.NextDate) " +
                    "from load_schedule A Inner Join " +
                    "(select load_schedule.space_id,obj_id,module,worktime from load_schedule " +
                    "inner join spaces on load_schedule.space_id=spaces.id " +
                    "inner join user_space_inxn on spaces.id=user_space_inxn.space_id " +
                    "inner join user_product_inxn on user_space_inxn.user_id=user_product_inxn.user_id " +
                    "inner join products on user_product_inxn.product_id=products.product_id " +
                    "where worktime is not null and Owner=1 " +
                    "group by load_schedule.space_id,obj_id,module,worktime " +
                    "having DATEDIFF(MINUTE,worktime,GetDATE())>max(MaxPublishTime)) B on A.space_id=b.space_id and a.module=b.module and A.worktime=b.worktime";
                int result = cmd.ExecuteNonQuery();
                if (result > 0)
                {
                    Global.systemLog.Info(result + " dead tasks cleaned up from " + SCHEDULED_LOAD_TABLE);
                }
            }
            finally
            {
                if (resultReader != null)
                    resultReader.Close();
                if (cmd != null)
                    cmd.Dispose();
            }
        }

        public static void updateTaskSchedule(QueryConnection conn, string schema, Guid spaceID, Guid ID, string module, DateTime nextDate, string parameters, string completedStatus)
        {
            QueryCommand cmd = null;
            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = "DELETE FROM  "
                    + schema + "." + SCHEDULED_LOAD_TABLE + " WHERE SPACE_ID=? AND Module=?" + (ID != Guid.Empty ? " AND OBJ_ID=?" : "");
                cmd.Parameters.Add("SPACE_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(spaceID);
                cmd.Parameters.Add("Module", OdbcType.VarChar).Value = module;
                DbParameter obj = cmd.Parameters.Add("ObjectID", conn.getODBCUniqueIdentiferType());
                if (ID != null && ID != Guid.Empty)
                    obj.Value = conn.getUniqueIdentifer(ID);
                else
                    obj.Value = System.DBNull.Value;
                cmd.ExecuteNonQuery();
                cmd.CommandText = "INSERT INTO  "
                    + schema + "." + SCHEDULED_LOAD_TABLE + " (SPACE_ID, Module, OBJ_ID, NextDate, Parameters, Status) VALUES (?,?,?,?,?,?)";
                DateTime nextDateTime = nextDate;
                if (module == ScheduledReport.MODULE_NAME || module == SalesforceLoader.SOURCE_GROUP_NAME)
                {
                    nextDateTime = Util.getCustomizedSchedulerTime(nextDate);
                }

                cmd.Parameters.Add("NextDate", OdbcType.DateTime).Value = nextDateTime;
                DbParameter param = cmd.Parameters.Add("Parameters", OdbcType.VarChar);
                if (parameters != null)
                    param.Value = parameters;
                else
                    param.Value = System.DBNull.Value;

                DbParameter statusParam = cmd.Parameters.Add("Status", OdbcType.VarChar);
                if (completedStatus != null)
                {
                    statusParam.Value = completedStatus;
                }
                else
                {
                    statusParam.Value = System.DBNull.Value;
                }

                cmd.ExecuteNonQuery();
            }
            finally
            {
                if (cmd != null)
                    cmd.Dispose();
            }
        }

        /// <summary>
        /// Get tasks running currently for a specific server
        /// </summary>
        /// <param name="conn"></param>
        /// <param name="schema"></param>
        /// <param name="workingServer"></param>
        /// <returns></returns>
        public static Dictionary<Guid, string> getRunningTasks(QueryConnection conn, string schema, string workingServer)
        {
            Dictionary<Guid, string> spacesDirectoryMap = new Dictionary<Guid, string>();

            QueryCommand cmd = null;
            QueryReader resultReader = null;
            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = "SELECT A.SPACE_ID, B.Directory FROM" +
                    SINGLE_SPACE + getTableName(schema, SCHEDULED_LOAD_TABLE) + SINGLE_SPACE + "A" +
                    SINGLE_SPACE + "INNER JOIN" + SINGLE_SPACE + getTableName(schema, SPACE_TABLE_NAME) + SINGLE_SPACE + "B" +
                    SINGLE_SPACE + "ON A.SPACE_ID = B.ID" +
                    SINGLE_SPACE + "WHERE B.Deleted IS NULL AND A.WorkTime IS NOT NULL AND A.WorkingServer LIKE ? ";
                cmd.Parameters.Add("WorkingServer", OdbcType.VarChar).Value = workingServer;
                resultReader = cmd.ExecuteReader();
                if (resultReader.HasRows)
                {
                    while (resultReader.Read())
                    {
                        if (!resultReader.IsDBNull(0) && !resultReader.IsDBNull(1))
                        {
                            Guid spaceID = resultReader.GetGuid(0);
                            string spaceDir = resultReader.GetString(1);
                            if (!spacesDirectoryMap.ContainsKey(spaceID))
                            {
                                spacesDirectoryMap.Add(spaceID, spaceDir);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("getDeadTasksSpaces : Error in retrieving dead tasks information ", ex);
            }
            finally
            {
                try
                {
                    if (resultReader != null)
                    {
                        resultReader.Close();
                    }
                    if (cmd != null)
                    {
                        cmd.Dispose();
                    }
                }
                catch (Exception ex2)
                {
                    Global.systemLog.Warn("getDeadTasksSpaces : Problem disposing reader/cmd references", ex2);
                }
            }

            return spacesDirectoryMap;
        }

        public static List<ScheduledItem> getTaskScheduleLoadStatus(Guid spaceId, string module)
        {
            QueryConnection conn = null;
            QueryCommand cmd = null;
            QueryReader reader = null;
            List<ScheduledItem> scheduledItemList = null;
            try
            {
                conn = ConnectionPool.getConnection();
                cmd = conn.CreateCommand();
                cmd.CommandText = "SELECT SPACE_ID, Module, NextDate, WorkTime, WorkingServer, Parameters, OBJ_ID, Status FROM  " +
                    mainSchema + "." + Database.SCHEDULED_LOAD_TABLE +
                    " WHERE SPACE_ID=? AND module=? ";
                cmd.Parameters.Add("Space_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(spaceId);
                cmd.Parameters.Add("module", OdbcType.VarChar).Value = module;

                reader = cmd.ExecuteReader();
                bool first = true;
                while (reader.Read())
                {
                    if (first)
                    {
                        scheduledItemList = new List<ScheduledItem>();
                        first = false;
                    }

                    ScheduledItem scheduledItem = new ScheduledItem();
                    scheduledItem.SpaceID = reader.GetGuid(0);
                    if (!reader.IsDBNull(1))
                    {
                        scheduledItem.Module = reader.GetString(1);
                    }

                    if (!reader.IsDBNull(2))
                    {
                        scheduledItem.NextDate = reader.GetDateTime(2);
                    }

                    if (!reader.IsDBNull(3))
                    {
                        scheduledItem.WorkTime = reader.GetDateTime(3);
                    }

                    if (!reader.IsDBNull(4))
                    {
                        scheduledItem.WorkingServer = reader.GetString(4);
                    }

                    if (!reader.IsDBNull(5))
                    {
                        scheduledItem.Parameters = reader.GetString(5);
                    }

                    if (!reader.IsDBNull(6))
                    {
                        scheduledItem.ID = reader.GetGuid(6);
                    }

                    if (!reader.IsDBNull(7))
                    {
                        scheduledItem.Status = reader.GetString(7);
                    }
                    scheduledItemList.Add(scheduledItem);
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Exception while querying " + Database.SCHEDULED_LOAD_TABLE + " " + ex.Message, ex);
            }
            finally
            {
                if (reader != null)
                    reader.Close();
                if (cmd != null)
                    cmd.Dispose();
                ConnectionPool.releaseConnection(conn);
            }
            return scheduledItemList;
        }

        public static void removeTaskSchedule(QueryConnection conn, string schema, Guid spaceID, Guid ID, string module)
        {
            QueryCommand cmd = null;
            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = "DELETE FROM  "
                    + schema + "." + SCHEDULED_LOAD_TABLE + " WHERE SPACE_ID=? AND Module=?" + (ID != Guid.Empty ? " AND OBJ_ID=?" : "");
                cmd.Parameters.Add("SPACE_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(spaceID);
                cmd.Parameters.Add("Module", OdbcType.VarChar).Value = module;
                DbParameter obj = cmd.Parameters.Add("ObjectID", conn.getODBCUniqueIdentiferType());
                if (ID != null && ID != Guid.Empty)
                    obj.Value = conn.getUniqueIdentifer(ID);
                else
                    obj.Value = System.DBNull.Value;
                int result = cmd.ExecuteNonQuery();
                if (result > 0)
                {
                    if (ID != null && ID != Guid.Empty)
                    {
                        Global.systemLog.Info(result + " tasks removed for module " + module + ", and object " + ID + " from " + SCHEDULED_LOAD_TABLE);
                    }
                    else
                    {
                        Global.systemLog.Info(result + "tasks removed for module " + module + " from " + SCHEDULED_LOAD_TABLE);
                    }
                }
            }
            finally
            {
                if (cmd != null)
                    cmd.Dispose();
            }
        }

        public static Guid addTaskSchedule(QueryConnection conn, string schema, Guid spaceID, string module, string parameters)
        {
            QueryCommand cmd = null;
            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = "INSERT INTO " + schema + "." + SCHEDULED_LOAD_TABLE + " "
                                + "(SPACE_ID, OBJ_ID, Module, NextDate, WorkTime, WorkingServer, Parameters) "
                                + "Values(?, ?, ?, ?, ?, ?, ?)";
                cmd.Parameters.Add("@SPACE_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(spaceID);
                Guid taskId = Guid.NewGuid();
                cmd.Parameters.Add("@OBJ_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(taskId);
                cmd.Parameters.Add("@Module", OdbcType.VarChar).Value = module;
                DateTime customizedDateTime = DateTime.Now;
                if (module == ScheduledReport.MODULE_NAME || module == SalesforceLoader.SOURCE_GROUP_NAME)
                {
                    customizedDateTime = Util.getCustomizedSchedulerTime(customizedDateTime);
                }
                cmd.Parameters.Add("@NextDate", OdbcType.DateTime).Value = customizedDateTime;
                cmd.Parameters.AddWithValue("@WorkTime", System.DBNull.Value);
                cmd.Parameters.AddWithValue("@WorkingServer", System.DBNull.Value);
                cmd.Parameters.Add("@Parameters", OdbcType.VarChar).Value = parameters;
                int result = cmd.ExecuteNonQuery();
                if (result > 0)
                {
                    Global.systemLog.Info("Task " + taskId + " for module " + module + " added to " + SCHEDULED_LOAD_TABLE);
                }
                return taskId;
            }
            finally
            {
                if (cmd != null)
                    cmd.Dispose();
            }
        }

        /*
         * called on server restart to clean up scheduled tasks that were in flight during server shutdown
         */
        public static void reapTasksInBadState(QueryConnection conn, string schema, string workingServer)
        {
            Dictionary<Guid, string> spacesDirectoryMap = Database.getRunningTasks(conn, schema, workingServer);
            if (spacesDirectoryMap != null && spacesDirectoryMap.Count > 0)
            {
                Util.clearSFDCExtractLockFiles(spacesDirectoryMap);
                Util.clearSFDCKillLockFiles(spacesDirectoryMap);
            }

            QueryCommand cmd = null;
            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = "UPDATE " + schema + "." + SCHEDULED_LOAD_TABLE + "  SET WorkingServer=NULL, WorkTime=NULL WHERE WorkingServer LIKE ? AND WorkTime IS NOT NULL";
                cmd.Parameters.Add("WorkingServer", OdbcType.VarChar).Value = workingServer;
                int result = cmd.ExecuteNonQuery();
                if (result > 0)
                {
                    Global.systemLog.Info(result + " tasks cleared on server start for " + workingServer + " from " + SCHEDULED_LOAD_TABLE);
                }
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }
        }

        public static ScheduledItem getNextTask(QueryConnection conn, string schema, string workingServer)
        {
            QueryCommand cmd = conn.CreateCommand();
            /* 
             * To reserve for work processes, set the work time. Take all processes that haven't been worked in an hour. 
             * This ensures that the first server to get the tasks will get it - preventing conflicts. You tag it, you keep it
             */
            DateTime now = DateTime.Now;
            cmd.CommandText = "UPDATE TOP (1) " + schema + "." + SCHEDULED_LOAD_TABLE + " SET WorkTime=?,WorkingServer=? " +
                " WHERE (WorkTime IS NULL OR DATEDIFF(mi,WorkTime,?)>90) ";

            // This where clause is to make sure that for Sforce only one entry from a space is picked. If sforce load schedule 
            // is being worked on, do not pick that space. This is to prevent 2 extractions running at the same time 
            string limitSpaceClause = " AND SPACE_ID NOT IN ( SELECT SPACE_ID FROM " + getTableName(schema, SCHEDULED_LOAD_TABLE) +
                " WHERE WorkTime is not null AND MODULE='" + SalesforceLoader.SOURCE_GROUP_NAME + "' )";

            cmd.CommandText = cmd.CommandText + " " + limitSpaceClause;

            cmd.Parameters.Add("Now", OdbcType.DateTime).Value = truncateDateTime(now);
            DbParameter server = cmd.Parameters.Add("WorkingServer", OdbcType.VarChar);
            if (workingServer != null)
                server.Value = workingServer;
            cmd.Parameters.Add("Now2", OdbcType.DateTime).Value = truncateDateTime(now);

            string lowerLimitDateRestrictionClause = " NextDate > ?";
            string upperLimitDateRestrictionClause = " NextDate<? ";
            //string workTimeDateRestriction = "(WorkTime IS NULL OR DATEDIFF(mi,WorkTime,?)>90) ";

            string OrConditionSForceModuleCondition = "(Module = '" + SalesforceLoader.SOURCE_GROUP_NAME + "' " +
               " AND " + lowerLimitDateRestrictionClause +
                " AND " + upperLimitDateRestrictionClause +
                " )";

            string OrConditionReportModuleCondition = "(Module= '" + ScheduledReport.MODULE_NAME + "' " +
                " AND " + lowerLimitDateRestrictionClause +
                " AND " + upperLimitDateRestrictionClause +
                " )";

            if (Util.isTaskSchedulerEnabledForReport() && Util.isTaskSchedulerEnabledForSForce())
            {
                cmd.CommandText = cmd.CommandText + " AND ( " + OrConditionSForceModuleCondition + " OR " + OrConditionReportModuleCondition + " ) ";
                cmd.Parameters.Add("Now3", OdbcType.DateTime).Value = truncateDateTime(Util.getLowerLimitOfCustomerScheduledTime());
                cmd.Parameters.Add("Now4", OdbcType.DateTime).Value = truncateDateTime(Util.getCustomizedSchedulerTime(now));
                cmd.Parameters.Add("Now5", OdbcType.DateTime).Value = truncateDateTime(Util.getLowerLimitOfCustomerScheduledTime());
                cmd.Parameters.Add("Now6", OdbcType.DateTime).Value = truncateDateTime(Util.getCustomizedSchedulerTime(now));
            }
            else if (Util.isTaskSchedulerEnabledForReport() && !Util.isTaskSchedulerEnabledForSForce())
            {
                cmd.CommandText = cmd.CommandText + " AND ( " + OrConditionReportModuleCondition + " ) ";
                cmd.Parameters.Add("Now5", OdbcType.DateTime).Value = truncateDateTime(Util.getLowerLimitOfCustomerScheduledTime());
                cmd.Parameters.Add("Now6", OdbcType.DateTime).Value = truncateDateTime(Util.getCustomizedSchedulerTime(now));
            }
            else if (!Util.isTaskSchedulerEnabledForReport() && Util.isTaskSchedulerEnabledForSForce())
            {
                cmd.CommandText = cmd.CommandText + " AND ( " + OrConditionSForceModuleCondition + " ) ";
                cmd.Parameters.Add("Now3", OdbcType.DateTime).Value = truncateDateTime(Util.getLowerLimitOfCustomerScheduledTime());
                cmd.Parameters.Add("Now4", OdbcType.DateTime).Value = truncateDateTime(Util.getCustomizedSchedulerTime(now));
            }
            else
            {
                // Task scheduler is not running. What is the point of picking up tasks
                Global.systemLog.Error("This should never be called because Task Scheduler is Off");
                return null;
            }

            int result = -1;
            try
            {
                result = cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Error on getNextTask:" + ex.Message);
            }
            if (result > 0)
            {
                Global.systemLog.Info(result + " tasks updated and reserving to work on from: " + workingServer + ", at " + now);
            }

            cmd.CommandText = "SELECT SPACE_ID,OBJ_ID,Module,Parameters,NextDate FROM  " + schema + "." + SCHEDULED_LOAD_TABLE + " WHERE WorkTime=? AND WorkingServer=?";
            QueryReader reader = cmd.ExecuteReader();
            ScheduledItem si = null;
            if (reader.Read())
            {
                si = new ScheduledItem();
                si.SpaceID = reader.GetGuid(0);
                if (!reader.IsDBNull(1))
                    si.ID = reader.GetGuid(1);
                si.Module = reader.GetString(2);
                if (!reader.IsDBNull(3))
                    si.Parameters = reader.GetString(3);
                if (!reader.IsDBNull(4))
                    si.NextDate = reader.GetDateTime(4);
            }
            reader.Close();
            cmd.Dispose();
            if (si != null)
                Global.systemLog.Info("Returning task for spaceId: " + si.SpaceID);
            return (si);
        }

        // only used to update object ids -- needed for special case 
        public static void updateScheduleTaskObjectID(QueryConnection conn, string schema, string module, Guid oldObjectId, Guid newObjectId)
        {
            QueryCommand cmd = null;
            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = "UPDATE" + SINGLE_SPACE + getTableName(schema, SCHEDULED_LOAD_TABLE) +
                    SINGLE_SPACE + "SET OBJ_ID = '" + newObjectId + "'" +
                    SINGLE_SPACE + "WHERE OBJ_ID = '" + oldObjectId + "'" +
                    SINGLE_SPACE + (oldObjectId == Guid.Empty ? "OR OBJ_ID is null" : "") +
                    SINGLE_SPACE + " AND MODULE = '" + module + "'";
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Error in updating schedule task object ids from " + oldObjectId + " to " + newObjectId, ex);
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }
        }

        public static void saveImage(QueryConnection conn, string schema, Guid id, byte[] data, string type)
        {
            QueryCommand cmd = null;
            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = "INSERT INTO " + schema + "." + IMAGE_TABLE + " (IMAGE_ID,Image,Type) VALUES (?,?,?)";
                cmd.Parameters.Add("id", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(id);
                cmd.Parameters.Add("image", OdbcType.VarBinary).Value = data;
                cmd.Parameters.Add("type", OdbcType.VarChar).Value = type;
                cmd.ExecuteNonQuery();
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }
        }

        public static StoredImage getImage(QueryConnection conn, string schema, Guid id)
        {
            StoredImage si = null;
            QueryCommand cmd = null;
            QueryReader reader = null;
            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = "SELECT Image,Type FROM " + schema + "." + IMAGE_TABLE + " WHERE IMAGE_ID=?";
                cmd.Parameters.Add("id", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(id);
                reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    si = new StoredImage();
                    si.data = (byte[])reader.GetValue(0);
                    si.type = reader.GetString(1);
                }
                return si;
            }
            finally
            {
                if (reader != null)
                {
                    reader.Close();
                }
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }
        }

        public static void deleteImage(QueryConnection conn, string schema, Guid id)
        {
            QueryCommand cmd = null;
            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = "DELETE FROM " + schema + "." + IMAGE_TABLE + " WHERE IMAGE_ID=?";
                cmd.Parameters.Add("id", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(id);
                cmd.ExecuteNonQuery();
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }
        }

        public static void addProxyRegistration(QueryConnection conn, string schema, string server, int port, string id)
        {
            QueryCommand cmd = null;
            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = "INSERT INTO " + schema + "." + PROXY_TABLE +
                    " (Server,Port,ID,RegistrationDate) VALUES (?,?,?,?)";
                cmd.Parameters.Add("Server", OdbcType.VarChar).Value = server;
                cmd.Parameters.Add("Port", OdbcType.Int).Value = port;
                cmd.Parameters.Add("ID", OdbcType.VarChar).Value = id;
                cmd.Parameters.Add("RegistrationDate", OdbcType.DateTime).Value = truncateDateTime(DateTime.Now);
                cmd.ExecuteNonQuery();
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }
        }

        public static void updateProxyRegistration(QueryConnection conn, string schema, string server, int port, string id)
        {
            QueryCommand cmd = null;
            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = "UPDATE " + schema + "." + PROXY_TABLE + " SET Server=?, Port=?, RegistrationDate = ? WHERE ID=?";
                cmd.Parameters.Add("Server", OdbcType.VarChar).Value = server;
                cmd.Parameters.Add("Port", OdbcType.Int).Value = port;
                cmd.Parameters.Add("RegistrationDate", OdbcType.DateTime).Value = truncateDateTime(DateTime.Now);
                cmd.Parameters.Add("ID", OdbcType.VarChar).Value = id;
                cmd.ExecuteNonQuery();
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }
        }

        public static ProxyRegistration getProxyRegistration(QueryConnection conn, string schema, string id)
        {
            QueryCommand cmd = null;
            QueryReader reader = null;
            ProxyRegistration pr = null;
            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = "SELECT Server,Port,RegistrationDate FROM " + schema + "." + PROXY_TABLE + " WHERE ID=?";
                cmd.Parameters.Add("ID", OdbcType.VarChar).Value = id;
                reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    pr = new ProxyRegistration();
                    pr.ServerName = reader.GetString(0);
                    pr.Port = reader.GetInt32(1);
                    pr.RegistrationDate = reader.GetDateTime(2);
                }
            }
            finally
            {
                if (reader != null)
                {
                    reader.Close();
                }
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }
            return pr;
        }

        public static void addScheduledReport(QueryConnection conn, string schema, ScheduledReport sr)
        {
            QueryCommand cmd = null;
            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = "INSERT INTO " + schema + "." + SCHEDULED_REPORTS_TABLE +
                    " (SPACE_ID,SCHEDULE_ID,USER_ID,ReportPath,ToReportPath,TriggerReportPath,Interval,DayOfWeek,DayOfMonth,Hour,Minute,Type,Subject,Body,List) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                cmd.Parameters.Add("SPACE_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(new Guid(sr.SpaceID));
                cmd.Parameters.Add("SCHEDULE_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(new Guid(sr.ID));
                cmd.Parameters.Add("USER_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(new Guid(sr.User));
                cmd.Parameters.Add("ReportPath", OdbcType.VarChar).Value = sr.ReportPath;
                DbParameter toReportPath = cmd.Parameters.Add("ToReportPath", OdbcType.VarChar);
                if (sr.ToReportPath != null)
                    toReportPath.Value = sr.ToReportPath;
                else
                    toReportPath.Value = System.DBNull.Value;
                DbParameter triggerReportPath = cmd.Parameters.Add("TriggerReportPath", OdbcType.VarChar);
                if (sr.TriggerReportPath != null)
                    triggerReportPath.Value = sr.TriggerReportPath;
                else
                    triggerReportPath.Value = System.DBNull.Value;
                cmd.Parameters.Add("Interval", OdbcType.Int).Value = sr.Interval;
                cmd.Parameters.Add("DayOfWeek", OdbcType.Int).Value = sr.DayOfWeek;
                cmd.Parameters.Add("DayOfMonth", OdbcType.Int).Value = sr.DayOfMonth;
                cmd.Parameters.Add("Hour", OdbcType.Int).Value = sr.Hour;
                cmd.Parameters.Add("Minute", OdbcType.Int).Value = sr.Minute;
                cmd.Parameters.Add("Type", OdbcType.VarChar).Value = sr.Type;
                DbParameter subject = cmd.Parameters.Add("Subject", OdbcType.VarChar);
                if (sr.Subject != null)
                    subject.Value = sr.Subject;
                else
                    subject.Value = System.DBNull.Value;
                DbParameter body = cmd.Parameters.Add("Body", OdbcType.VarChar);
                if (sr.EmailBody != null)
                    body.Value = sr.EmailBody;
                else
                    body.Value = System.DBNull.Value;
                DbParameter list = cmd.Parameters.Add("List", OdbcType.VarChar);
                if (sr.ToList != null)
                {
                    StringBuilder sb = new StringBuilder();
                    bool first = true;
                    foreach (string s in sr.ToList)
                    {
                        if (first)
                            first = false;
                        else
                            sb.Append(';');
                        sb.Append(s);
                    }
                    list.Value = sb.ToString();
                }
                else
                    list.Value = System.DBNull.Value;
                cmd.ExecuteNonQuery();
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }
        }

        public static void deleteScheduledReport(QueryConnection conn, string schema, Guid id)
        {
            QueryCommand cmd = null;
            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = "DELETE FROM " + schema + "." + SCHEDULED_REPORTS_TABLE + " WHERE SCHEDULE_ID=?";
                cmd.Parameters.Add("ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(id);
                cmd.ExecuteNonQuery();
                cmd.CommandText = "DELETE FROM " + schema + "." + SCHEDULED_LOAD_TABLE + " WHERE OBJ_ID=?";
                cmd.ExecuteNonQuery();
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }
        }

        private static ScheduledReport getScheduledReport(QueryReader reader)
        {
            ScheduledReport sr = new ScheduledReport();
            sr.SpaceID = reader.GetGuid(0).ToString();
            sr.ID = reader.GetGuid(1).ToString();
            sr.User = reader.GetGuid(2).ToString();
            if (!reader.IsDBNull(3))
                sr.ReportPath = reader.GetString(3);
            if (!reader.IsDBNull(4))
                sr.ToReportPath = reader.GetString(4);
            if (!reader.IsDBNull(5))
                sr.TriggerReportPath = reader.GetString(5);
            sr.Interval = reader.GetInt32(6);
            sr.DayOfWeek = reader.GetInt32(7);
            sr.DayOfMonth = reader.GetInt32(8);
            sr.Hour = reader.GetInt32(9);
            sr.Minute = reader.GetInt32(10);
            sr.Type = reader.GetString(11);
            if (!reader.IsDBNull(12))
                sr.Subject = reader.GetString(12);
            if (!reader.IsDBNull(13))
                sr.EmailBody = reader.GetString(13);
            if (!reader.IsDBNull(14))
                sr.ToList = reader.GetString(14).Split(new char[] { ';' });
            return sr;
        }

        public static ScheduledReport getScheduledReport(QueryConnection conn, string schema, Guid id)
        {
            ScheduledReport sr = null;
            QueryCommand cmd = null;
            QueryReader reader = null;
            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = "SELECT SPACE_ID,SCHEDULE_ID,USER_ID,ReportPath,ToReportPath,TriggerReportPath,Interval,DayOfWeek,DayOfMonth,Hour,Minute,Type,Subject,Body,List FROM " + schema + "." + SCHEDULED_REPORTS_TABLE + " WHERE SCHEDULE_ID=?";
                cmd.Parameters.Add("ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(id);
                reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    sr = getScheduledReport(reader);
                }
            }
            finally
            {
                if (reader != null)
                {
                    reader.Close();
                }
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }
            return sr;
        }

        public static List<ScheduledReport> getScheduledReports(QueryConnection conn, string schema, Guid spaceID)
        {
            List<ScheduledReport> reportList = new List<ScheduledReport>();
            QueryCommand cmd = null;
            QueryReader reader = null;
            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = "SELECT SPACE_ID,SCHEDULE_ID,USER_ID,ReportPath,ToReportPath,TriggerReportPath,Interval,DayOfWeek,DayOfMonth,Hour,Minute,Type,Subject,Body,List FROM " + schema + "." + SCHEDULED_REPORTS_TABLE + " WHERE SPACE_ID=?";
                cmd.Parameters.Add("ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(spaceID);
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    reportList.Add(getScheduledReport(reader));
                }
            }
            finally
            {
                if (reader != null)
                {
                    reader.Close();
                }
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }
            return reportList;
        }

        public static void logDelivery(QueryConnection conn, string schema, Guid spaceID, DateTime date, string path, int numDeliveries)
        {
            QueryCommand cmd = null;
            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = "INSERT INTO " + schema + "." + DELIVERY_LOG +
                    " (SPACE_ID,ReportPath,NumDeliveries,DeliveryDate) VALUES (?,?,?,?)";
                cmd.Parameters.Add("SpaceID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(spaceID);
                cmd.Parameters.Add("ReportPath", OdbcType.VarChar).Value = path;
                cmd.Parameters.Add("NumDeliveries", OdbcType.Int).Value = numDeliveries;
                cmd.Parameters.Add("DeliveryDate", OdbcType.DateTime).Value = date;
                cmd.ExecuteNonQuery();
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }
        }

        public static int getDeliveriesLast24Hours(QueryConnection conn, string schema, Guid spaceID)
        {
            object result = null;
            QueryCommand cmd = null;
            try
            {
                cmd = conn.CreateCommand();
                bool isOracle = conn.isDBTypeOracle();
                cmd.CommandText = "SELECT SUM(NumDeliveries) FROM " + schema + "." + DELIVERY_LOG + " WHERE SPACE_ID=? AND " + (isOracle ? conn.getCurrentTimestamp() + "- DeliveryDate" : "DateDiff(Day,[DeliveryDate]," + conn.getCurrentTimestamp() + ")") + "<=1";
                cmd.Parameters.Add("ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(spaceID);
                result = cmd.ExecuteScalar();
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }
            return (result == null || result == System.DBNull.Value) ? 0 : Int32.Parse(result.ToString());
        }


        public static Object getLoadDate(QueryConnection conn, string schema, Guid spaceID)
        {
            QueryCommand cmd = null;
            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = "SELECT max(LoadDate) FROM " + schema + "." + PUBLISH_TABLE + " WHERE SPACE_ID=?";
                cmd.Parameters.Add("SPACE_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(spaceID);
                Object anObject = cmd.ExecuteScalar();
                if (anObject == null || anObject == System.DBNull.Value)
                {
                    return null;
                }
                return anObject;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

        }

        /**
         * set the last login date and reset the failed password attempt counter
         */
        public static void updateLastLogin(string username)
        {
            QueryConnection conn = null;
            QueryCommand cmd = null;
            try
            {
                conn = ConnectionPool.getConnection();
                cmd = conn.CreateCommand();
                bool isOracle = conn.isDBTypeOracle();
                string FailedPasswordAttemptCount = isOracle ? "FailedPwdAttemptCount" : "FailedPasswordAttemptCount";

                cmd.CommandText = "UPDATE " + mainSchema + "." + USER_TABLE_NAME + " SET LastLoginDate = ?, " + FailedPasswordAttemptCount + " = ? WHERE Username = ?";
                DateTime dtnow = DateTime.UtcNow;
                dtnow = dtnow.AddMilliseconds(-dtnow.Millisecond);
                cmd.Parameters.Add("@LastLoginDate", OdbcType.DateTime).Value = dtnow; // change to UTC so we are consistent
                cmd.Parameters.Add("@Count", OdbcType.Int).Value = 0;
                cmd.Parameters.Add("@Username", OdbcType.NVarChar, 255).Value = username.ToLower();
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Error in updating last login date", ex);
            }
            finally
            {
                if (cmd != null)
                    cmd.Dispose();
                ConnectionPool.releaseConnection(conn);
            }
        }

        public static DateTime getLastLogin(string username)
        {
            // if this is the first time user is logging in, login time used is UtcNow
            DateTime loginDate = DateTime.Now;
            QueryConnection conn = null;
            QueryCommand cmd = null;
            try
            {
                conn = ConnectionPool.getConnection();
                cmd = conn.CreateCommand();
                cmd.CommandText = "SELECT LastLoginDate FROM " + mainSchema + "." + USER_TABLE_NAME + " WHERE Username='" + username.ToLower() + "'";
                object result = cmd.ExecuteScalar();
                if (result != null && result != System.DBNull.Value)
                {
                    loginDate = (DateTime)result;
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Error in getting last login date", ex);
            }
            finally
            {
                if (cmd != null)
                    cmd.Dispose();
                ConnectionPool.releaseConnection(conn);
            }
            return loginDate;

        }

        public static void addAllowedUsernameMasks(QueryConnection conn, string schema, Guid userID, String mask)
        {
            QueryCommand cmd = null;
            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = "INSERT INTO " + schema + "." + ALLOWED_USER_DOMAINS_TABLE + " (USER_ID,UsernameMask) VALUES (?,?)";
                cmd.Parameters.Add("USER_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(userID);
                cmd.Parameters.Add("UsernameMask", OdbcType.VarChar).Value = mask;
                cmd.ExecuteNonQuery();
            }
            finally
            {
                if (cmd != null)
                    cmd.Dispose();
            }
        }

        public static Dictionary<string, string> getSpaceEmailProperties(QueryConnection conn, string schema, Guid spaceID)
        {
            QueryReader reader = null;
            QueryCommand cmd = null;
            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = "SELECT ReportFrom, ReportSubject FROM " + getTableName(schema, SPACE_TABLE_NAME) + " WHERE ID=?";
                cmd.Parameters.Add("ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(spaceID);
                reader = cmd.ExecuteReader();
                Dictionary<string, string> result = new Dictionary<string, string>();
                while (reader.Read())
                {
                    if (!reader.IsDBNull(0))
                    {
                        result.Add(Util.KEY_REPORT_EMAIL_FROM, reader.GetString(0));
                    }

                    if (!reader.IsDBNull(1))
                    {
                        result.Add(Util.KEY_REPORT_EMAIL_SUBJECT, reader.GetString(1));
                    }
                }
                return result;
            }
            finally
            {
                if (reader != null)
                    reader.Close();
                if (cmd != null)
                    cmd.Dispose();
            }
        }

        public static List<string> getAllowedUsernameMasks(QueryConnection conn, string schema, Guid userID)
        {
            QueryReader reader = null;
            QueryCommand cmd = null;
            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = "SELECT UsernameMask FROM " + schema + "." + ALLOWED_USER_DOMAINS_TABLE + " WHERE USER_ID=?";
                cmd.Parameters.Add("ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(userID);
                reader = cmd.ExecuteReader();
                List<string> result = new List<string>();
                while (reader.Read())
                {
                    result.Add(reader.GetString(0));
                }
                return result;
            }
            finally
            {
                if (reader != null)
                    reader.Close();
                if (cmd != null)
                    cmd.Dispose();
            }
        }

        /// <summary>
        /// Updates the user table with the MANAGED_ACCOUNT_ID. Basically, createdUser
        /// gets MANAGED_ACCOUNT_ID from the ADMIN_ACCOUNT_ID of createdByUser.
        /// -- passing password here since we need to rehash with the account specific hashing policy
        /// </summary>
        /// <param name="conn"></param>
        /// <param name="schema"></param>
        /// <param name="userID"></param>
        /// <param name="createdByUser"></param>
        public static void logCreatedUser(QueryConnection conn, string schema, Guid userID, Guid createdByUser, string pwd)
        {
            User managedUser = Database.getUserById(conn, schema, userID);
            User adminUser = Database.getUserById(conn, schema, createdByUser);
            updateCreatedUserParams(conn, schema, managedUser.ID, managedUser.Username, adminUser.AdminAccountId, pwd);
        }

        public static void updateCreatedUserParams(QueryConnection conn, string schema, Guid managedUserID,
            string managedUsername, Guid adminAccountID, string pwd)
        {
            QueryCommand cmd = null;
            string hashAlgorithm = null;

            // get the account_id of the createdByUser
            try
            {
                PasswordPolicy policy = Database.getPasswordPolicy(conn, Database.mainSchema, adminAccountID);
                if (policy != null)
                    hashAlgorithm = policy.hashAlgorithm;
                string hash = Password.HashPassword(pwd, managedUsername, hashAlgorithm);

                cmd = conn.CreateCommand();
                cmd.CommandText = "UPDATE" + SINGLE_SPACE + getTableName(schema, USER_TABLE_NAME) + SINGLE_SPACE +
                    "SET MANAGED_ACCOUNT_ID = ?, PASSWORD = ? WHERE PKID = ?";
                cmd.Parameters.Add("@MANAGED_ACCOUNT_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(adminAccountID);
                cmd.Parameters.Add("@Password", OdbcType.VarChar, 1024).Value = hash;
                cmd.Parameters.Add("@PKID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(managedUserID);
                cmd.ExecuteNonQuery();
            }
            finally
            {
                if (cmd != null)
                    cmd.Dispose();
            }
        }

        public static bool canUserManageUser(QueryConnection conn, string schema, Guid createdByUserId, Guid createdUserId)
        {
            if (createdByUserId == createdUserId)
                return true;

            QueryCommand cmd = null;
            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = "SELECT COUNT(*) FROM" + SINGLE_SPACE + getTableName(schema, USER_TABLE_NAME) + SINGLE_SPACE +
                    "A INNER JOIN" + SINGLE_SPACE + getTableName(schema, USER_TABLE_NAME) + SINGLE_SPACE +
                    "B ON A.ADMIN_ACCOUNT_ID = B.MANAGED_ACCOUNT_ID" + SINGLE_SPACE +
                    "WHERE A.PKID = ? AND B.PKID = ?";
                cmd.Parameters.Add("@PKID1", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(createdByUserId);
                cmd.Parameters.Add("@PKID2", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(createdUserId);
                int result = Int32.Parse(cmd.ExecuteScalar().ToString());
                return result > 0;
            }
            finally
            {
                if (cmd != null)
                    cmd.Dispose();
            }
        }

        public static List<string> getManagedUsernames(QueryConnection conn, string schema, Guid createdByUserId)
        {
            QueryReader reader = null;
            QueryCommand cmd = null;
            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = "SELECT B.Username FROM" + SINGLE_SPACE + getTableName(schema, USER_TABLE_NAME) + SINGLE_SPACE +
                    "A INNER JOIN" + SINGLE_SPACE + getTableName(schema, USER_TABLE_NAME) + SINGLE_SPACE +
                    "B ON A.ADMIN_ACCOUNT_ID = B.MANAGED_ACCOUNT_ID" + SINGLE_SPACE +
                    "WHERE A.PKID = ? AND B.ADMIN_ACCOUNT_ID is NULL";
                cmd.Parameters.Add("@PKID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(createdByUserId);
                /*
                cmd.CommandText = "SELECT B.[Username] FROM " + schema + "." + CREATED_USERS_TABLE +
                                                  " A INNER JOIN " + schema + "." + USER_TABLE_NAME + " B ON A.[USER_ID] = B.[PKID] WHERE A.[CreatedByUser]=?";
                cmd.Parameters.Add("CreatedByUser", OdbcType.UniqueIdentifier).Value = createdByUserId;
                 */
                reader = cmd.ExecuteReader();
                List<string> userNames = new List<string>();
                while (reader.Read())
                {
                    userNames.Add(reader.GetString(0));
                }
                return userNames;
            }
            finally
            {
                if (reader != null)
                    reader.Close();
                if (cmd != null)
                    cmd.Dispose();
            }
        }

        public static List<string> getProxyUsernames(QueryConnection conn, string schema, Guid userId)
        {
            QueryReader reader = null;
            QueryCommand cmd = null;
            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = "SELECT B.Username, A.ExpirationDate FROM " + schema + "." + PROXY_USERS +
                                                  " A INNER JOIN " + schema + "." + USER_TABLE_NAME + " B ON A.PROXY_USER_ID = B.PKID WHERE A.USER_ID=?";
                cmd.Parameters.Add("USER_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(userId);
                reader = cmd.ExecuteReader();
                List<string> userNames = new List<string>();
                while (reader.Read())
                {
                    if (reader.IsDBNull(1))
                        userNames.Add(reader.GetString(0) + ": no expiration");
                    else
                        userNames.Add(reader.GetString(0) + ": " + reader.GetDateTime(1).ToString("yyyy-MM-dd"));
                }
                return userNames;
            }
            finally
            {
                if (reader != null)
                    reader.Close();
                if (cmd != null)
                    cmd.Dispose();
            }
        }

        /* ------------------------------------------------------------------------------
           ---                                                                        ---
           --- User Preferences                                                       ---
           ---                                                                        ---
           ------------------------------------------------------------------------------ */

        /// <summary>
        /// Reads any default user preferences from the Preferences.config file and
        /// adds or updates them to the PREFERENCES table.
        /// </summary>
        /// <param name="conn">the database connection</param>
        /// <param name="schema">the schema name</param>
        /// <param name="path">the relative path to the Acorn file/program directory</param>
        /// <returns>true if loaded successfully, false otherwise</returns>
        public static bool loadPreferencesTable(QueryConnection conn, string schema, string path)
        {
            // Get preferences file
            string filename = path + "\\" + PREFERENCES_FILENAME;
            if (!File.Exists(filename))
            {
                Global.systemLog.Error("No default preferences file: " + filename);
                return true;
            }
            Global.systemLog.Info("Loading default preferences from '" + filename + "'");

            // Read in parameters for each space type
            StreamReader reader = new StreamReader(filename);
            string line = null;
            int i = 0;
            while ((line = reader.ReadLine()) != null)
            {
                if (line.StartsWith("//") || line.Equals(""))
                    continue;
                string[] cols = line.Split(new char[] { '\t' });
                int type = int.Parse(cols[0]);
                UserPreference.Type preferenceType;
                switch (type)
                {
                    case 0:
                        preferenceType = UserPreference.Type.Global;
                        break;
                    case 1:
                        preferenceType = UserPreference.Type.User;
                        break;
                    case 2:
                        preferenceType = UserPreference.Type.UserSpace;
                        break;
                    default:
                        Global.systemLog.Error("Invalid preference type: " + type + " encountered.");
                        return false;
                }
                Guid userID = new Guid(cols[1]);
                Guid spaceID = new Guid(cols[2]);
                string key = cols[3];
                string value = cols[4];
                UserPreference userPreference = new UserPreference(preferenceType, key, value);
                Global.systemLog.Info("Loading preference: " + preferenceType + ": Key: " + key + "; Value: " + value);
                updateUserPreferences(conn, schema, userID, spaceID, userPreference);
                i++;
            }
            reader.Close();
            Global.systemLog.Info("Completed loading " + i + " default user preferences.");
            return true;
        }

        /// <summary>
        /// Adds or updates one or more user preferences at the specified level.
        /// </summary>
        /// <param name="conn">the database connection</param>
        /// <param name="schema">the schema name</param>
        /// <param name="userID">The Guid representation of the user ID. Can be Guid.EMPTY.</param>
        /// <param name="spaceID">The Guid representation of the space ID Can be Guid.EMPTY.</param>
        /// <param name="userPreference">The key/value pair to be added or updated and the level
        /// at which it should be added/updated. All keys must be non-null, non-empty.</param>
        public static void updateUserPreferences(QueryConnection conn,
                                                 string schema,
                                                 Guid userID,
                                                 Guid spaceID,
                                                 UserPreference userPreference)
        {
            if (schema == null)
            {
                schema = "dbo";
            }

            // Check if preference already exists
            QueryCommand queryCmd = conn.CreateCommand();
            //queryCmd.CommandText = generatePreferencesSelect(schema, userPreference.PreferenceType, true);
            queryCmd.CommandText = "SELECT PreferenceKey, PreferenceValue FROM " + schema + "." + PREFERENCES_TABLE
                + " WHERE PreferenceType=? AND USER_ID=? AND SPACE_ID=? AND PreferenceKey=?";
            queryCmd.Parameters.Add("@PreferenceType", OdbcType.Int).Value = (int)(object)userPreference.PreferenceType;
            queryCmd.Parameters.Add("@USER_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(getUserGuid(userID, userPreference));
            queryCmd.Parameters.Add("@SPACE_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(getSpaceGuid(spaceID, userPreference));
            queryCmd.Parameters.Add("@PreferenceKey", OdbcType.VarChar).Value = userPreference.Key;
            // Global.systemLog.Debug("Checking for key with: " + queryCmd.CommandText);
            object result = queryCmd.ExecuteScalar();
            queryCmd.Dispose();

            // Add or update
            if (result == null)
            {
                // Doesn't exist.  Add it.
                QueryCommand insertCmd = conn.CreateCommand();
                insertCmd.CommandText = "INSERT INTO " + schema + "." + PREFERENCES_TABLE
                    + " (PreferenceType, USER_ID, SPACE_ID, PreferenceKey, PreferenceValue) VALUES (?,?,?,?,?)";
                insertCmd.Parameters.Add("@PreferenceType", OdbcType.Int).Value = (int)(object)userPreference.PreferenceType;
                insertCmd.Parameters.Add("@USER_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(getUserGuid(userID, userPreference));
                insertCmd.Parameters.Add("@SPACE_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(getSpaceGuid(spaceID, userPreference));
                insertCmd.Parameters.Add("@PreferenceKey", OdbcType.VarChar).Value = userPreference.Key;
                insertCmd.Parameters.Add("@PreferenceValue", OdbcType.VarChar).Value = userPreference.Value;
                // Global.systemLog.Debug("Adding preference with: " + insertCmd.CommandText);
                insertCmd.ExecuteNonQuery();
                insertCmd.Dispose();
            }
            else
            {
                // Does exist. Update it.
                QueryCommand updateCmd = conn.CreateCommand();
                updateCmd.CommandText = "UPDATE " + schema + "." + PREFERENCES_TABLE
                    + " SET PreferenceValue =? WHERE PreferenceType =? AND USER_ID =? AND SPACE_ID =? AND PreferenceKey =?";
                updateCmd.Parameters.Add("@PreferenceValue", OdbcType.VarChar).Value = userPreference.Value;
                updateCmd.Parameters.Add("@PreferenceType", OdbcType.Int).Value = (int)(object)userPreference.PreferenceType;
                updateCmd.Parameters.Add("@USER_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(getUserGuid(userID, userPreference));
                updateCmd.Parameters.Add("@SPACE_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(getSpaceGuid(spaceID, userPreference));
                updateCmd.Parameters.Add("@PreferenceKey", OdbcType.VarChar).Value = userPreference.Key;
                // Global.systemLog.Debug("Updating preference with: " + updateCmd.CommandText);
                updateCmd.ExecuteNonQuery();
                updateCmd.Dispose();
            }
        }

        public static int deleteLastLoadHistory(QueryConnection conn, string schema, Guid spaceID, string loadgroup)
        {
            QueryCommand cmd = null;
            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = "DELETE FROM " + schema + "." + LAST_LOAD_HISTORY
                    + " WHERE SPACE_ID=? AND LOADGROUP=?";
                cmd.Parameters.Add("@SPACE_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(spaceID);
                cmd.Parameters.Add("@LOADGROUP", OdbcType.VarChar).Value = loadgroup;
                int rows = cmd.ExecuteNonQuery();
                return rows;
            }
            finally
            {
                if (cmd != null)
                    cmd.Dispose();
            }
        }

        public static int addLastLoadHistory(QueryConnection conn, string schema, Guid spaceID, string loadgroup, int loadNumber, DateTime loadDate, string processingGroups)
        {
            QueryCommand cmd = null;
            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = "INSERT INTO " + schema + "." + LAST_LOAD_HISTORY
                    + " (TM, SPACE_ID, LOADGROUP, LOADNUMBER, LOADDATE, PROCESSINGGROUPS)"
                    + " VALUES(?,?,?,?,?,?)";
                DateTime dtnow = DateTime.Now;
                dtnow = dtnow.AddMilliseconds(-dtnow.Millisecond);
                cmd.Parameters.Add("TM", OdbcType.DateTime).Value = dtnow;
                cmd.Parameters.Add("SPACE_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(spaceID);
                cmd.Parameters.Add("LOADGROUP", OdbcType.VarChar).Value = loadgroup;
                cmd.Parameters.Add("LOADNUMBER", OdbcType.Int).Value = loadNumber;
                loadDate = loadDate.AddMilliseconds(-loadDate.Millisecond);
                cmd.Parameters.Add("LOADDATE", OdbcType.DateTime).Value = loadDate;
                if (processingGroups == null)
                {
                    cmd.Parameters.Add("PROCESSINGGROUPS", OdbcType.VarChar).Value = System.DBNull.Value;
                }
                else
                {
                    cmd.Parameters.Add("PROCESSINGGROUPS", OdbcType.VarChar).Value = processingGroups;
                }
                int rows = cmd.ExecuteNonQuery();
                return rows;
            }
            finally
            {
                if (cmd != null)
                    cmd.Dispose();
            }
        }

        public static LastLoadHistory getLastLoadHistory(QueryConnection conn, string schema, Guid spaceID, string loadgroup, int loadNumber)
        {
            QueryCommand cmd = conn.CreateCommand();
            cmd.CommandText = "SELECT TM, SPACE_ID, LOADGROUP, LOADNUMBER, LOADDATE, PROCESSINGGROUPS FROM " + schema + "." + LAST_LOAD_HISTORY
                + " WHERE SPACE_ID=? AND LOADGROUP=? AND LOADNUMBER=?";
            cmd.Parameters.Add("@SPACE_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(spaceID);
            cmd.Parameters.Add("@LOADGROUP", OdbcType.VarChar).Value = loadgroup;
            cmd.Parameters.Add("@LOADNUMBER", OdbcType.Int).Value = loadNumber;
            QueryReader reader = cmd.ExecuteReader();
            LastLoadHistory flh = null;
            if (reader.Read())
            {
                flh = new LastLoadHistory();
                flh.TM = reader.GetDateTime(0);
                flh.SPACE_ID = reader.GetGuid(1);
                flh.LOADGROUP = reader.GetString(2);
                flh.LOADNUMBER = reader.GetInt16(3);
                flh.LOADDATE = reader.GetDateTime(4);
                flh.PROCESSINGGROUPS = reader.IsDBNull(5) ? null : reader.GetString(5);
            }
            cmd.Dispose();
            return flh;
        }

        /// <summary>
        /// Only for testing.
        /// </summary>
        /// <param name="conn">the database connection</param>
        /// <param name="schema">the schema name</param>
        public static void deleteUserPreferences(QueryConnection conn, string schema)
        {
            QueryCommand cmd = null;
            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = "DELETE FROM " + schema + "." + PREFERENCES_TABLE;
                cmd.ExecuteNonQuery();
            }
            finally
            {
                if (cmd != null)
                    cmd.Dispose();
            }
        }

        /// <summary>Gets all global preferences.</summary>
        public static IDictionary<string, string> getUserPreferences(QueryConnection conn,
                                                                     string schema)
        {
            QueryCommand cmd = conn.CreateCommand();
            cmd.CommandText = generatePreferencesSelect(schema, UserPreference.Type.Global, false);
            cmd.Parameters.Add("@PreferenceType", conn.getODBCUniqueIdentiferType()).Value = (int)(object)UserPreference.Type.Global;
            QueryReader reader = cmd.ExecuteReader();
            IDictionary<string, string> keyValuePairs = new Dictionary<string, string>();
            while (reader.Read())
            {
                keyValuePairs.Add(reader.GetString(0), reader.GetString(1));
            }
            reader.Close();
            cmd.Dispose();
            return keyValuePairs;
        }

        /// <summary>Gets a subset of global preferences.</summary>
        public static IDictionary<string, string> getUserPreferences(QueryConnection conn,
                                                                     string schema,
                                                                     List<string> preferenceKeys)
        {
            QueryCommand cmd = conn.CreateCommand();
            cmd.CommandText = generatePreferencesSelect(schema, preferenceKeys);
            cmd.Parameters.Add("@PreferenceType", OdbcType.Int).Value = (int)(object)UserPreference.Type.Global;
            cmd.Parameters.Add("@USER_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(Guid.Empty);
            cmd.Parameters.Add("@SPACE_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(Guid.Empty);
            foreach (string preferenceKey in preferenceKeys)
            {
                cmd.Parameters.Add("@PreferenceKey", OdbcType.VarChar).Value = preferenceKey;
            }
            QueryReader reader = cmd.ExecuteReader();
            IDictionary<string, string> keyValuePairs = new Dictionary<string, string>();
            while (reader.Read())
            {
                keyValuePairs.Add(reader.GetString(0), reader.GetString(1));
            }
            reader.Close();
            cmd.Dispose();
            return addMissingPreferenceKeys(keyValuePairs, preferenceKeys);
        }

        /// <summary>Gets all preferences for a user.</summary>
        public static IDictionary<string, string> getUserPreferences(QueryConnection conn,
                                                                     string schema,
                                                                     Guid userID)
        {
            QueryCommand cmd = conn.CreateCommand();
            cmd.CommandText = generatePreferencesSelect(schema, UserPreference.Type.User, false);
            cmd.Parameters.Add("@PreferenceType", conn.getODBCUniqueIdentiferType()).Value = (int)(object)UserPreference.Type.User;
            cmd.Parameters.Add("@USER_ID", conn.getODBCUniqueIdentiferType()).Value = userID;
            QueryReader reader = cmd.ExecuteReader();
            IDictionary<string, string> keyValuePairs = new Dictionary<string, string>();
            while (reader.Read())
            {
                keyValuePairs.Add(reader.GetString(0), reader.GetString(1));
            }
            reader.Close();
            cmd.Dispose();
            return keyValuePairs;
        }

        /// <summary>Gets a subset of preferences for a user.</summary>
        public static IDictionary<string, string> getUserPreferences(QueryConnection conn,
                                                                     string schema,
                                                                     Guid userID,
                                                                     List<string> preferenceKeys)
        {
            QueryCommand cmd = conn.CreateCommand();
            cmd.CommandText = generatePreferencesSelect(schema, preferenceKeys);
            cmd.Parameters.Add("@PreferenceType", OdbcType.Int).Value = (int)(object)UserPreference.Type.User;
            cmd.Parameters.Add("@USER_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(userID);
            cmd.Parameters.Add("@SPACE_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(Guid.Empty);
            foreach (string preferenceKey in preferenceKeys)
            {
                cmd.Parameters.Add("@PreferenceKey", OdbcType.VarChar).Value = preferenceKey;
            }
            QueryReader reader = cmd.ExecuteReader();
            IDictionary<string, string> keyValuePairs = new Dictionary<string, string>();
            while (reader.Read())
            {
                keyValuePairs.Add(reader.GetString(0), reader.GetString(1));
            }
            reader.Close();
            cmd.Dispose();
            return addMissingPreferenceKeys(keyValuePairs, preferenceKeys);
        }

        /// <summary>Gets all preferences for a user and space combination.</summary>
        public static IDictionary<string, string> getUserPreferences(QueryConnection conn,
                                                                     string schema,
                                                                     Guid userID,
                                                                     Guid spaceID)
        {
            QueryCommand cmd = conn.CreateCommand();
            cmd.CommandText = generatePreferencesSelect(schema, UserPreference.Type.UserSpace, false);
            cmd.Parameters.Add("@PreferenceType", conn.getODBCUniqueIdentiferType()).Value = (int)(object)UserPreference.Type.User;
            cmd.Parameters.Add("@USER_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(userID);
            cmd.Parameters.Add("@SPACE_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(spaceID);
            QueryReader reader = cmd.ExecuteReader();
            IDictionary<string, string> keyValuePairs = new Dictionary<string, string>();
            while (reader.Read())
            {
                keyValuePairs.Add(reader.GetString(0), reader.GetString(1));
            }
            reader.Close();
            cmd.Dispose();
            return keyValuePairs;
        }

        /// <summary>Gets a subset of preferences for a user and space combination.</summary>
        public static IDictionary<string, string> getUserPreferences(QueryConnection conn,
                                                                     string schema,
                                                                     Guid userID,
                                                                     Guid spaceID,
                                                                     List<string> preferenceKeys)
        {
            if (schema == null)
            {
                schema = "dbo";
            }
            QueryCommand cmd = conn.CreateCommand();
            cmd.CommandText = generatePreferencesSelect(schema, preferenceKeys);
            cmd.Parameters.Add("@PreferenceType", OdbcType.Int).Value = (int)(object)UserPreference.Type.UserSpace;
            cmd.Parameters.Add("@USER_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(userID);
            cmd.Parameters.Add("@SPACE_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(spaceID);
            foreach (string preferenceKey in preferenceKeys)
            {
                cmd.Parameters.Add("@PreferenceKey", OdbcType.VarChar).Value = preferenceKey;
            }
            QueryReader reader = cmd.ExecuteReader();
            IDictionary<string, string> keyValuePairs = new Dictionary<string, string>();
            while (reader.Read())
            {
                keyValuePairs.Add(reader.GetString(0), reader.GetString(1));
            }
            reader.Close();
            cmd.Dispose();
            return addMissingPreferenceKeys(keyValuePairs, preferenceKeys);
        }

        /// <summary>
        /// Gets user preferences. Handles three cases and two subcases:
        ///   'Global' type is specified, retrieve all global preferences.
        ///   'User' type is specified, 'userID' must be entered.
        ///   'UserSpace' type is specified, 'userID' and 'spaceID' must be entered.
        ///   And, in the case of 'User' and 'UserSpace' types:
        ///       If 'preferenceKeys' is not null, retrieve set of preferences for user
        ///       If 'preferenceKeys' is null, retrieve all preferences for user
        /// </summary>
        public static IDictionary<string, string> getUserPreferences(QueryConnection conn,
                                                                     string schema,
                                                                     UserPreference.Type type,
                                                                     Guid userID,
                                                                     Guid spaceID,
                                                                     List<string> preferenceKeys)
        {
            QueryCommand cmd = conn.CreateCommand();
            cmd.CommandText = generatePreferencesSelect(schema, type, true);
            cmd.Parameters.Add("@PreferenceType", conn.getODBCUniqueIdentiferType()).Value = (int)(object)type;
            cmd.Parameters.Add("@USER_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(userID);
            cmd.Parameters.Add("@SPACE_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(spaceID);
            QueryReader reader = cmd.ExecuteReader();
            IDictionary<string, string> keyValuePairs = new Dictionary<string, string>();
            while (reader.Read())
            {
                keyValuePairs.Add(reader.GetString(0), reader.GetString(1));
            }
            reader.Close();
            cmd.Dispose();
            return addMissingPreferenceKeys(keyValuePairs, preferenceKeys);
        }

        private static string generatePreferencesSelect(string schema, UserPreference.Type type, bool keysSpecified)
        {
            string selectString = null;
            switch (type)
            {
                case UserPreference.Type.Global:
                    selectString = "SELECT PreferenceKey, PreferenceValue FROM " + schema + "." + PREFERENCES_TABLE + " WHERE PreferenceType=?";
                    break;
                case UserPreference.Type.User:
                    selectString = "SELECT PreferenceKey, PreferenceValue FROM " + schema + "." + PREFERENCES_TABLE + " WHERE PreferenceType=? AND USER_ID=?";
                    break;
                case UserPreference.Type.UserSpace:
                    selectString = "SELECT PreferenceKey, PreferenceValue FROM " + schema + "." + PREFERENCES_TABLE + " WHERE PreferenceType=? AND USER_ID=? AND SPACE_ID=?";
                    break;
                default:
                    Global.systemLog.Error("Received invalid UserPreference.Type: " + type);
                    break;
            }
            // Are user preference keys specified?
            if (keysSpecified)
            {
                // Include in the select statement
                selectString = selectString + " AND PreferenceKey=?";
            }
            return selectString;
        }

        private static string generatePreferencesSelect(string schema, List<string> preferenceKeys)
        {
            if (schema == null || schema.Equals(""))
            {
                schema = "dbo";
            }
            // Initial incantation
            string selectStmt = "SELECT PreferenceKey, PreferenceValue FROM " + schema + "." + PREFERENCES_TABLE
                + " WHERE PreferenceType=? AND USER_ID=? AND SPACE_ID=? AND PreferenceKey IN (";
            // Add correct number of params
            for (int i = 0; i < preferenceKeys.Count; i++)
            {
                selectStmt = selectStmt + "?,";
            }
            // Chop off the last comma and add a right paren
            selectStmt = selectStmt.Substring(0, selectStmt.Length - 1);
            selectStmt = selectStmt + ")";

            return selectStmt;
        }

        private static IDictionary<string, string> addMissingPreferenceKeys(IDictionary<string, string> keyValuePairs,
                                                                            List<string> preferenceKeys)
        {
            // Add any missing key/value pairs in with 'null' value
            foreach (string preferenceKey in preferenceKeys)
            {
                if (!keyValuePairs.ContainsKey(preferenceKey))
                {
                    keyValuePairs.Add(preferenceKey, null);
                }
            }
            return keyValuePairs;
        }

        private static Guid getUserGuid(Guid userID, UserPreference preference)
        {
            if (preference.PreferenceType.Equals(UserPreference.Type.Global))
            {
                return Guid.Empty;
            }
            return userID;
        }

        private static Guid getSpaceGuid(Guid spaceID, UserPreference preference)
        {
            if (preference.PreferenceType.Equals(UserPreference.Type.UserSpace))
            {
                return spaceID;
            }
            return Guid.Empty;
        }

        /*
         * return the space size
         * - calls an operations stored procedure that returns value in MB
         */
        public static long getSpaceSize(QueryConnection conn, string schema, Space sp)
        {
            QueryReader reader = null;
            QueryCommand cmd = null;
            try
            {
                cmd = conn.CreateCommand();

                cmd.CommandText = "{call sp_BirstGetSpaceSize (?)}";
                cmd.Parameters.Add("@SpaceID", OdbcType.VarChar).Value = sp.ID.ToString();
                reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    return reader.GetInt32(0);
                }
            }
            finally
            {
                if (reader != null)
                    reader.Close();
                if (cmd != null)
                    cmd.Dispose();
            }
            return 0;
        }

        // Upload token values
        public static UploadToken getUploadToken(QueryConnection conn, string schema, Guid tokenID)
        {
            UploadToken uploadToken = null;
            QueryCommand cmd = conn.CreateCommand();
            bool isOracle = conn.isDBTypeOracle();
            // update table with used before getting the value
            cmd.CommandText = "UPDATE " + schema + "." + UPLOAD_TOKENS_TABLE + " SET Used=1 WHERE TOKEN_ID=? AND Used=0";
            cmd.Parameters.Add("TOKEN_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(tokenID);
            int cnt = cmd.ExecuteNonQuery();
            if (cnt != 1)
            {
                Global.systemLog.Warn("Request to reuse Upload token or an invalid token " + tokenID);
                cmd.Dispose();
                return null;
            }

            cmd.CommandText = "SELECT USER_ID,SPACE_ID,Parameters,End_Of_Life,TYPE,HOSTADDR FROM " + schema + "." + UPLOAD_TOKENS_TABLE + " WHERE TOKEN_ID=?";
            QueryReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                uploadToken = new UploadToken();
                uploadToken.tokenID = tokenID;
                uploadToken.userID = reader.GetGuid(0);
                uploadToken.spaceID = reader.IsDBNull(1) ? Guid.Empty : reader.GetGuid(1);
                uploadToken.parameters = reader.IsDBNull(2) ? null : reader.GetString(2);
                uploadToken.endOfLife = reader.GetDateTime(3);
                uploadToken.type = reader.IsDBNull(4) ? null : reader.GetString(4);
                uploadToken.hostaddr = reader.IsDBNull(5) ? null : reader.GetString(5);
            }
            cmd.Dispose();
            return uploadToken;
        }

        // called on server start
        public static void reapUploadTokens(QueryConnection conn, string schema)
        {
            QueryCommand cmd = null;
            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = "DELETE FROM " + schema + "." + UPLOAD_TOKENS_TABLE + " WHERE End_Of_Life < ? OR  Used=1";
                DateTime dtnow = DateTime.Now;
                dtnow = dtnow.AddMilliseconds(-dtnow.Millisecond);
                cmd.Parameters.Add("End_Of_Life", OdbcType.DateTime).Value = dtnow;
                int res = cmd.ExecuteNonQuery();
                Global.systemLog.Info(res + " out of date / used Upload tokens removed from " + UPLOAD_TOKENS_TABLE);
            }
            finally
            {
                if (cmd != null)
                    cmd.Dispose();
            }
            return;
        }

        public static void createUploadToken(QueryConnection conn, string schema, UploadToken uploadToken)
        {
            QueryCommand cmd = conn.CreateCommand();
            cmd.CommandText = "INSERT INTO " + schema + "." + UPLOAD_TOKENS_TABLE + " (TOKEN_ID,USER_ID,SPACE_ID,End_Of_Life,Parameters,TYPE,HOSTADDR,Used) VALUES (?,?,?,?,?,?,?,0)";
            cmd.Parameters.Add("@TOKEN_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(uploadToken.tokenID);
            cmd.Parameters.Add("@USER_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(uploadToken.userID);
            cmd.Parameters.Add("@SPACE_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(uploadToken.spaceID);
            uploadToken.endOfLife = uploadToken.endOfLife.AddMilliseconds(-uploadToken.endOfLife.Millisecond);
            cmd.Parameters.Add("@End_Of_Life", OdbcType.DateTime).Value = uploadToken.endOfLife != DateTime.MinValue ? uploadToken.endOfLife : DateTime.Now.AddSeconds(30);
            DbParameter svars = cmd.Parameters.Add("@Parameters", OdbcType.VarChar);
            if (uploadToken.parameters != null && uploadToken.parameters.Length > 0)
                svars.Value = uploadToken.parameters;
            else
                svars.Value = System.DBNull.Value;
            DbParameter type = cmd.Parameters.Add("@TYPE", OdbcType.VarChar);
            if (uploadToken.type != null)
                type.Value = uploadToken.type;
            else
                type.Value = System.DBNull.Value;
            DbParameter hostaddr = cmd.Parameters.Add("@HOSTADDR", OdbcType.VarChar);
            if (uploadToken.hostaddr != null)
                hostaddr.Value = uploadToken.hostaddr;
            else
                hostaddr.Value = System.DBNull.Value;
            cmd.ExecuteNonQuery();
            cmd.Dispose();
        }


        // SSO token management operations
        public static SSOToken getSSOToken(QueryConnection conn, string schema, Guid tokenID)
        {
            SSOToken ssoToken = null;
            QueryReader reader = null;
            QueryCommand cmd = null;
            try
            {
                cmd = conn.CreateCommand();
                // update table with used before getting the value
                cmd.CommandText = "UPDATE " + schema + "." + SSO_TOKENS_TABLE + " SET Used=1 WHERE TOKEN_ID=? AND Used=0";
                cmd.Parameters.Add("TOKEN_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(tokenID);
                int cnt = cmd.ExecuteNonQuery();
                if (cnt != 1)
                {
                    Global.systemLog.Warn("Request to reuse SSO token or an invalid token " + tokenID);
                    return null;
                }

                cmd.CommandText = "SELECT USER_ID,SPACE_ID,SessionVariables,End_Of_Life,REPADMIN,TYPE,HOSTADDR,TIMEOUT,LOGOUTPAGE FROM " + schema + "." + SSO_TOKENS_TABLE + " WHERE TOKEN_ID=?";
                reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    ssoToken = new SSOToken();
                    ssoToken.tokenID = tokenID;
                    ssoToken.userID = reader.GetGuid(0);
                    ssoToken.spaceID = reader.IsDBNull(1) ? Guid.Empty : reader.GetGuid(1);
                    ssoToken.sessionVariables = reader.IsDBNull(2) ? null : reader.GetString(2);
                    ssoToken.endOfLife = reader.GetDateTime(3);
                    ssoToken.repAdmin = reader.IsDBNull(4) ? false : reader.GetBoolean(4);
                    ssoToken.type = reader.IsDBNull(5) ? null : reader.GetString(5);
                    ssoToken.hostaddr = reader.IsDBNull(6) ? null : reader.GetString(6);
                    ssoToken.timeout = !reader.IsDBNull(7) ? reader.GetInt32(7) : -1;
                    ssoToken.logoutPage = !reader.IsDBNull(8) ? reader.GetString(8) : null;
                }
                return ssoToken;
            }
            finally
            {
                if (reader != null)
                    reader.Close();
                if (cmd != null)
                    cmd.Dispose();
            }
        }

        public static void createSSOToken(QueryConnection conn, string schema, SSOToken ssoToken)
        {
            QueryCommand cmd = null;
            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = "INSERT INTO " + schema + "." + SSO_TOKENS_TABLE + " (TOKEN_ID,USER_ID,SPACE_ID,End_Of_Life,SessionVariables,REPADMIN,TYPE,HOSTADDR,Used,TIMEOUT,LOGOUTPAGE) VALUES (?,?,?,?,?,?,?,?,0,?,?)";
                cmd.Parameters.Add("@TOKEN_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(ssoToken.tokenID);
                cmd.Parameters.Add("@USER_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(ssoToken.userID);
                cmd.Parameters.Add("@SPACE_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(ssoToken.spaceID);
                ssoToken.endOfLife = ssoToken.endOfLife.AddMilliseconds(-ssoToken.endOfLife.Millisecond);
                cmd.Parameters.Add("@End_Of_Life", OdbcType.DateTime).Value = ssoToken.endOfLife != DateTime.MinValue ? ssoToken.endOfLife : DateTime.Now.AddSeconds(30);
                DbParameter svars = cmd.Parameters.Add("@SessionVariables", OdbcType.NVarChar);
                if (ssoToken.sessionVariables != null)
                    svars.Value = ssoToken.sessionVariables;
                else
                    svars.Value = System.DBNull.Value;
                cmd.Parameters.Add("@REPADMIN", OdbcType.Bit).Value = ssoToken.repAdmin;
                DbParameter type = cmd.Parameters.Add("@TYPE", OdbcType.VarChar);
                if (ssoToken.type != null)
                    type.Value = ssoToken.type;
                else
                    type.Value = System.DBNull.Value;
                DbParameter hostaddr = cmd.Parameters.Add("@HOSTADDR", OdbcType.VarChar);
                if (ssoToken.hostaddr != null)
                    hostaddr.Value = ssoToken.hostaddr;
                else
                    hostaddr.Value = System.DBNull.Value;

                cmd.Parameters.Add("@TIMEOUT", OdbcType.Int).Value = ssoToken.timeout <= 0 ? -1 : ssoToken.timeout;
                DbParameter logoutPageParam = cmd.Parameters.Add("@LOGOUTPAGE", OdbcType.VarChar);
                if (string.IsNullOrWhiteSpace(ssoToken.logoutPage))
                {
                    logoutPageParam.Value = System.DBNull.Value;
                }
                else
                {
                    logoutPageParam.Value = ssoToken.logoutPage.Trim();
                }
                cmd.ExecuteNonQuery();
            }
            finally
            {
                if (cmd != null)
                    cmd.Dispose();
            }
        }

        // called on server start
        public static void reapSSOTokens(QueryConnection conn, string schema)
        {
            QueryCommand cmd = null;
            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = "DELETE FROM " + schema + "." + SSO_TOKENS_TABLE + " WHERE End_Of_Life < ? OR  Used=1";
                DateTime dtnow = DateTime.Now;
                dtnow = dtnow.AddMilliseconds(-dtnow.Millisecond);
                cmd.Parameters.Add("End_Of_Life", OdbcType.DateTime).Value = dtnow;
                int res = cmd.ExecuteNonQuery();
                Global.systemLog.Info(res + " out of date / used SSO tokens removed from " + SSO_TOKENS_TABLE);
            }
            finally
            {
                if (cmd != null)
                    cmd.Dispose();
            }
            return;
        }

        public static PasswordResetToken getPasswordResetToken(QueryConnection conn, string schema, Guid tokenID)
        {
            PasswordResetToken passwordResetToken = null;
            QueryCommand cmd = conn.CreateCommand();

            // update table with used before getting the value
            cmd.CommandText = "UPDATE " + schema + "." + PASSWORD_RESET_REQUEST_TABLE + " SET USED=1 WHERE TOKEN_ID=? AND USED=0";
            cmd.Parameters.Add("TOKEN_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(tokenID);
            int cnt = cmd.ExecuteNonQuery();
            if (cnt != 1)
            {
                Global.systemLog.Warn("Request to reuse password reset token or an invalid token " + tokenID);
                cmd.Dispose();
                return null;
            }

            cmd.CommandText = "SELECT USER_ID,END_OF_LIFE FROM " + schema + "." + PASSWORD_RESET_REQUEST_TABLE + " WHERE TOKEN_ID=?";
            QueryReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                passwordResetToken = new PasswordResetToken();
                passwordResetToken.tokenID = tokenID;
                passwordResetToken.userID = reader.GetGuid(0);
                passwordResetToken.endOfLife = reader.GetDateTime(1);
            }
            cmd.Dispose();
            return passwordResetToken;
        }

        public static void createPasswordResetToken(QueryConnection conn, string schema, PasswordResetToken token)
        {
            QueryCommand cmd = null;
            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = "INSERT INTO " + schema + "." + PASSWORD_RESET_REQUEST_TABLE
                    + " (TOKEN_ID,USER_ID,END_OF_LIFE,USED) VALUES (?,?,?,0)";
                cmd.Parameters.Add("@TOKEN_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(token.tokenID);
                cmd.Parameters.Add("@USER_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(token.userID);
                cmd.Parameters.Add("@END_OF_LIFE", OdbcType.DateTime).Value = token.endOfLife != DateTime.MinValue ? token.endOfLife : DateTime.Now.AddHours(1);
                cmd.ExecuteNonQuery();
            }
            finally
            {
                if (cmd != null)
                    cmd.Dispose();
            }
        }

        // called on server start
        public static void reapPasswordResetTokens(QueryConnection conn, string schema)
        {
            QueryCommand cmd = null;
            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = "DELETE FROM " + schema + "." + PASSWORD_RESET_REQUEST_TABLE + " WHERE END_OF_LIFE < ? OR USED=1";
                DateTime dtnow = DateTime.Now;
                dtnow = dtnow.AddMilliseconds(-dtnow.Millisecond);
                cmd.Parameters.Add("END_OF_LIFE", OdbcType.DateTime).Value = dtnow;
                int res = cmd.ExecuteNonQuery();
                Global.systemLog.Info(res + " out of date / used password reset tokens removed from " + PASSWORD_RESET_REQUEST_TABLE);
            }
            finally
            {
                if (cmd != null)
                    cmd.Dispose();
            }
            return;
        }

        // GROUP MANAGEMENT DATABASE OPERATIONS

        private static string SINGLE_SPACE = " ";
        public static string getTableName(string schema, string tableName)
        {
            if (schema == null || tableName == null)
            {
                throw new Exception("Either provided schema or tableName is null : schema =" + schema + " : tableName = " + tableName);
            }

            return schema + "." + tableName;
        }


        public static bool loadSpaceOperationsTable(QueryConnection conn, string schema, string path)
        {
            QueryCommand cmd = null;
            Dictionary<int, string> existingSpaceOps = new Dictionary<int, string>();
            QueryReader result = null;
            StreamReader reader = null;
            // Get a list of existing ACLs
            int count = 0;
            while (conn.State != ConnectionState.Open)
            {
                Thread.Sleep(1000);
                count++;
                if (count > 20)
                {
                    Global.systemLog.Error("Loading the Space Operations table failed, the connection is closed");
                    return false;
                }
            }
            try
            {
                cmd = conn.CreateCommand();
                if (tableExists(conn, schema, SPACE_OPS_TABLE))
                {
                    cmd.CommandText = "SELECT ID,Name FROM" + SINGLE_SPACE +
                        getTableName(schema, SPACE_OPS_TABLE);
                    result = cmd.ExecuteReader();
                    while (result.Read())
                    {
                        if (result.IsDBNull(0) && result.IsDBNull(1))
                        {
                            continue;
                        }

                        existingSpaceOps.Add(result.GetInt32(0), result.GetString(1));
                    }
                    result.Close();
                }
                else
                {
                    createTables(conn, schema);
                }

                string filename = path + "\\" + SPACE_OPS_FILENAME;
                if (!File.Exists(filename))
                {
                    Global.systemLog.Error("No Space Operations file: " + filename);
                    return false;
                }
                Global.systemLog.Info("Loading SpaceOps.config from '" + filename + "'");

                // Read from file and load/update the table
                reader = new StreamReader(filename);
                string line = null;
                int i = 0;
                while ((line = reader.ReadLine()) != null)
                {
                    if (line.StartsWith("//") || line.Equals(""))
                        continue;
                    string[] cols = line.Split(new char[] { '\t' });
                    int id = int.Parse(cols[0]);
                    string name = cols[1];
                    if (existingSpaceOps.ContainsKey(id))
                    {
                        if (existingSpaceOps[id] == name)
                        {
                            // No need to update on anything
                            continue;
                        }
                    }

                    cmd.CommandText = "INSERT INTO" + SINGLE_SPACE + getTableName(schema, SPACE_OPS_TABLE) +
                        "(ID,Name,CreatedDate,ModifiedDate)" + SINGLE_SPACE +
                        "Values (?,?," + conn.getCurrentTimestamp() + "," + conn.getCurrentTimestamp() + ")";
                    cmd.Parameters.Clear();
                    cmd.Parameters.Add("@ID", OdbcType.Int).Value = id;
                    cmd.Parameters.Add("@Name", OdbcType.VarChar).Value = name;
                    cmd.ExecuteNonQuery();
                    i++;
                }
                reader.Close();
                cmd.Dispose();
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Exception while loading table : " + SPACE_OPS_TABLE, ex);
                try
                {
                    if (result != null && !result.IsClosed)
                    {
                        result.Close();
                    }
                    if (reader != null)
                    {
                        reader.Close();
                    }

                    if (cmd != null)
                    {
                        cmd.Dispose();
                    }
                }
                catch (Exception ex2)
                {
                    Global.systemLog.Warn("Exception while trying to close the file and database reader. System should take care of it now", ex2);
                }
                return false;
            }

            return true;
        }

        public static Dictionary<int, string> getSpaceOperations(QueryConnection conn, string schema)
        {
            Dictionary<int, string> globalSpaceOperations = new Dictionary<int, string>();
            QueryCommand cmd = null;
            QueryReader result = null;
            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = "SELECT ID,Name FROM" + SINGLE_SPACE + getTableName(schema, SPACE_OPS_TABLE);
                result = cmd.ExecuteReader();
                while (result.Read())
                {
                    if (result.IsDBNull(0) || result.IsDBNull(1))
                    {
                        continue;
                    }
                    int id = result.GetInt32(0);
                    string name = result.GetString(1);
                    globalSpaceOperations.Add(id, name);
                }
            }
            finally
            {
                if (result != null)
                {
                    result.Close();
                }
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

            return globalSpaceOperations;
        }

        /// <summary>
        /// load/update global ACL table from the config file
        /// </summary>
        /// <param name="conn"></param>
        /// <param name="schema"></param>
        public static bool loadACLTable(QueryConnection conn, string schema, string path)
        {
            QueryCommand cmd = null;
            Dictionary<int, string> existingACLs = new Dictionary<int, string>();
            QueryReader result = null;
            StreamReader reader = null;
            // Get a list of existing ACLs
            int count = 0;
            while (conn.State != ConnectionState.Open)
            {
                Thread.Sleep(1000);
                count++;
                if (count > 20)
                {
                    Global.systemLog.Error("Loading the ACLs table failed, the connection is closed");
                    return false;
                }
            }
            try
            {
                cmd = conn.CreateCommand();
                if (tableExists(conn, schema, ACL_INFO_TABLE))
                {
                    cmd.CommandText = "SELECT ID,Name FROM" + SINGLE_SPACE +
                        getTableName(schema, ACL_INFO_TABLE);
                    result = cmd.ExecuteReader();
                    while (result.Read())
                    {
                        if (result.IsDBNull(0) && result.IsDBNull(1))
                        {
                            continue;
                        }

                        existingACLs.Add(result.GetInt32(0), result.GetString(1));
                    }
                    result.Close();
                }
                else
                {
                    createTables(conn, schema);
                }

                string filename = path + "\\" + ACL_FILENAME;
                if (!File.Exists(filename))
                {
                    Global.systemLog.Error("No Acls file: " + filename);
                    return false;
                }
                Global.systemLog.Info("Loading Acls.config from '" + filename + "'");

                // Read from file and load/update the table
                reader = new StreamReader(filename);
                string line = null;
                int i = 0;
                while ((line = reader.ReadLine()) != null)
                {
                    if (line.StartsWith("//") || line.Equals(""))
                        continue;
                    string[] cols = line.Split(new char[] { '\t' });
                    int id = int.Parse(cols[0]);
                    string name = cols[1];
                    if (existingACLs.ContainsKey(id))
                    {
                        if (existingACLs[id] == name)
                        {
                            // No need to update on anything
                            continue;
                        }
                    }

                    cmd.CommandText = "INSERT INTO" + SINGLE_SPACE + getTableName(schema, ACL_INFO_TABLE) +
                        "(ID,Name,CreatedDate,ModifiedDate)" + SINGLE_SPACE +
                        "Values (?,?," + conn.getCurrentTimestamp() + "," + conn.getCurrentTimestamp() + ")";
                    cmd.Parameters.Clear();
                    cmd.Parameters.Add("@ID", OdbcType.Int).Value = id;
                    cmd.Parameters.Add("@Name", OdbcType.VarChar).Value = name;
                    cmd.ExecuteNonQuery();
                    i++;
                }
                reader.Close();
                cmd.Dispose();
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Exception while loading table : " + ACL_INFO_TABLE, ex);
                try
                {
                    if (result != null && !result.IsClosed)
                    {
                        result.Close();
                    }
                    if (reader != null)
                    {
                        reader.Close();
                    }

                    if (cmd != null)
                    {
                        cmd.Dispose();
                    }
                }
                catch (Exception ex2)
                {
                    Global.systemLog.Warn("Exception while trying to close the file and database reader. System should take care of it now", ex2);
                }
                return false;
            }

            return true;
        }

        public static Dictionary<int, string> getACLs(QueryConnection conn, string schema)
        {
            Dictionary<int, string> globalACLs = new Dictionary<int, string>();
            QueryCommand cmd = null;
            QueryReader result = null;
            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = "SELECT ID,Name FROM" + SINGLE_SPACE + getTableName(schema, ACL_INFO_TABLE);
                result = cmd.ExecuteReader();
                while (result.Read())
                {
                    if (result.IsDBNull(0) || result.IsDBNull(1))
                    {
                        continue;
                    }

                    int id = result.GetInt32(0);
                    string name = result.GetString(1);
                    globalACLs.Add(id, name);
                }
            }
            finally
            {
                if (result != null)
                    result.Close();
                if (cmd != null)
                    cmd.Dispose();
            }

            return globalACLs;
        }

        public static string getAddGroupToSpaceQuery(QueryConnection conn, string schema)
        {
            string command = "INSERT INTO" + SINGLE_SPACE +
                getTableName(schema, SPACE_GROUP_TABLE) + SINGLE_SPACE +
                "(SPACE_ID,GROUP_ID,GroupName,InternalGroup,CreatedDate,ModifiedDate)" + SINGLE_SPACE +
                "Values (?,?,?,?," + conn.getCurrentTimestamp() + "," + conn.getCurrentTimestamp() + ")";

            return command;
        }

        /// <summary>
        /// This returns a light list of groups (only group id and group name are populated).
        /// Do not rely for group users
        /// </summary>
        /// <param name="conn"></param>
        /// <param name="schema"></param>
        /// <param name="packageID"></param>
        /// <param name="sp"></param>
        /// <returns></returns>
        public static List<SpaceGroup> getPackageGroups(QueryConnection conn, string schema, Guid packageID, Space sp)
        {
            return getPackageGroups(conn, schema, packageID, sp.ID);
        }

        public static List<SpaceGroup> getPackageGroups(QueryConnection conn, string schema, Guid packageID, Guid spaceID)
        {
            List<SpaceGroup> spaceGroups = new List<SpaceGroup>();
            Dictionary<Guid, SpaceGroup> spaceGroupDictonary = new Dictionary<Guid, SpaceGroup>();
            QueryCommand cmd = null;
            QueryReader result = null;
            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = "SELECT A.GROUP_ID,A.GroupName, A.InternalGroup FROM" +
                    SINGLE_SPACE + getTableName(schema, SPACE_GROUP_TABLE) + SINGLE_SPACE + "A INNER JOIN " +
                    SINGLE_SPACE + getTableName(schema, PACKAGE_GROUP_INXN) + SINGLE_SPACE +
                    "B ON A.GROUP_ID = B.GROUP_ID AND A.SPACE_ID = B.SPACE_ID" + SINGLE_SPACE +
                    "WHERE  B.PACKAGE_ID = ? AND B.SPACE_ID=?" + SINGLE_SPACE +
                    "ORDER BY A.CreatedDate asc";
                cmd.Parameters.Add("@PACKAGE_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(packageID);
                cmd.Parameters.Add("@SPACE_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(spaceID);
                result = cmd.ExecuteReader();
                while (result.Read())
                {
                    bool anyNull = false;
                    for (int i = 0; i < 3; i++)
                    {
                        if (result.IsDBNull(i))
                        {
                            Global.systemLog.Warn("Null value for " + result.GetName(i));
                            anyNull = true;
                            break;
                        }
                    }
                    if (anyNull)
                    {
                        continue;
                    }

                    Guid gpID = result.GetGuid(0);
                    if (!spaceGroupDictonary.ContainsKey(gpID))
                    {
                        SpaceGroup spGroup = new SpaceGroup();
                        spGroup.ID = gpID;
                        spGroup.Name = result.GetString(1);
                        spGroup.InternalGroup = result.GetBoolean(2);
                        spaceGroupDictonary.Add(gpID, spGroup);
                    }
                }
            }
            finally
            {
                if (result != null)
                    result.Close();
                if (cmd != null)
                    cmd.Dispose();
            }

            foreach (SpaceGroup spg in spaceGroupDictonary.Values)
            {
                spaceGroups.Add(spg);
            }
            return spaceGroups;

        }

        private static string updatePackageUsageQuery(QueryConnection conn, string schema)
        {
            string sql = "UPDATE  " + getTableName(schema, PACKAGE_USAGE_INXN) +
                " SET Available=?, ModifiedBy=?, ModifiedDate=" + conn.getCurrentTimestamp() + " Where PACKAGE_ID=? AND CHILD_SPACE_ID=? AND PARENT_SPACE_ID=?";
            return sql;
        }

        /// <summary>
        /// updates the availability flag in the package usage table
        /// </summary>
        /// <param name="conn"></param>
        /// <param name="schema"></param>
        /// <param name="user"></param>
        /// <param name="packageSpace">Creator space of the package</param> 
        /// <param name="packageID"></param>
        /// <param name="childSpace">Consumer space of the package</param>
        /// <param name="available"></param>
        public static void updatePackageUsage(QueryConnection conn, string schema, User user, Space packageSpace, Guid packageID, Guid childSpaceID, bool available)
        {
            QueryCommand cmd = null;
            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = updatePackageUsageQuery(conn, schema);
                cmd.Parameters.Add("@Available", conn.getODBCBooleanType()).Value = conn.getBooleanValue(available);
                cmd.Parameters.Add("@ModifiedBy", OdbcType.VarChar).Value = user.Username;
                cmd.Parameters.Add("@PACKAGE_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(packageID);
                cmd.Parameters.Add("@CHILD_SPACE_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(childSpaceID);
                cmd.Parameters.Add("@PARENT_SPACE_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(packageSpace.ID);
                int rowAffected = cmd.ExecuteNonQuery();
            }
            finally
            {

                if (cmd != null)
                {
                    try
                    {
                        cmd.Dispose();
                    }
                    catch (Exception ex2)
                    {
                        Global.systemLog.Warn("Unable to dispose cmd object ", ex2);
                    }
                }
            }

        }


        private static string getPackageUsageQueryForChildSpace(string schema)
        {
            string sql = "SELECT A.PACKAGE_ID, A.CHILD_SPACE_ID, B.Name, D.Username, A.Available, A.PARENT_SPACE_ID FROM " +
                    getTableName(schema, PACKAGE_USAGE_INXN) + SINGLE_SPACE + "A " +
                    "INNER JOIN " + getTableName(schema, SPACE_TABLE_NAME) + " B ON A.CHILD_SPACE_ID = B.ID " +
                    " INNER JOIN " + getTableName(schema, USER_SPACE_INXN) + " C ON C.SPACE_ID = A.CHILD_SPACE_ID AND C.Owner=1" +
                    " INNER JOIN " + getTableName(schema, USER_TABLE_NAME) + " D ON C.USER_ID = D.PKID " +
                    " WHERE A.CHILD_SPACE_ID = ? GROUP BY A.PACKAGE_ID, A.CHILD_SPACE_ID, B.NAME, D.Username, A.Available, A.PARENT_SPACE_ID";

            return sql;
        }

        public static List<PackageUsage> getPackageUsageByChildSpace(QueryConnection conn, string schema, Guid childSpaceID)
        {
            List<PackageUsage> response = null;
            QueryCommand cmd = null;
            QueryReader result = null;
            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = getPackageUsageQueryForChildSpace(schema);
                cmd.Parameters.Add("@CHILD_SPACE_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(childSpaceID);

                result = cmd.ExecuteReader();
                response = new List<PackageUsage>();
                while (result.Read())
                {
                    bool anyNull = false;
                    for (int i = 0; i < 5; i++)
                    {
                        if (result.IsDBNull(i))
                        {
                            Global.systemLog.Warn("Null value for " + result.GetName(i));
                            anyNull = true;
                            break;
                        }
                    }
                    if (anyNull)
                    {
                        continue;
                    }

                    PackageUsage usage = new PackageUsage();
                    usage.PackageID = result.GetGuid(0).ToString();
                    usage.ChildSpaceID = result.GetGuid(1).ToString();
                    usage.SpaceName = result.GetString(2);
                    usage.SpaceOwner = result.GetString(3);
                    usage.Available = result.GetBoolean(4);
                    usage.PackageSpaceID = !result.IsDBNull(5) ? result.GetGuid(5).ToString() : null;
                    response.Add(usage);
                }
            }
            finally
            {
                if (result != null)
                    result.Close();
                if (cmd != null)
                    cmd.Dispose();
            }

            return response;
        }

        private static string getPackageUsageQuery(string schema)
        {
            string sql = "SELECT A.PACKAGE_ID, A.CHILD_SPACE_ID, B.Name, D.Username, A.Available, A.PARENT_SPACE_ID FROM " +
                    getTableName(schema, PACKAGE_USAGE_INXN) + SINGLE_SPACE + "A " +
                    "INNER JOIN " + getTableName(schema, SPACE_TABLE_NAME) + " B ON A.CHILD_SPACE_ID = B.ID AND B.Deleted IS NULL" +
                    " INNER JOIN " + getTableName(schema, USER_SPACE_INXN) + " C ON C.SPACE_ID = A.CHILD_SPACE_ID AND C.Owner=1" +
                    " INNER JOIN " + getTableName(schema, USER_TABLE_NAME) + " D ON C.USER_ID = D.PKID " +
                    " WHERE A.PACKAGE_ID = ? AND A.PARENT_SPACE_ID = ? GROUP BY A.PACKAGE_ID, A.CHILD_SPACE_ID, B.NAME, D.Username, A.Available, A.PARENT_SPACE_ID";

            return sql;
        }

        public static List<PackageUsage> getPackageUsage(QueryConnection conn, string schema, Guid packageID, Space packageSpace)
        {
            List<PackageUsage> response = null;
            QueryCommand cmd = null;
            QueryReader result = null;
            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = getPackageUsageQuery(schema);
                cmd.Parameters.Add("@PACKAGE_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(packageID);
                cmd.Parameters.Add("@PARENT_SPACE_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(packageSpace.ID);
                result = cmd.ExecuteReader();
                response = new List<PackageUsage>();
                while (result.Read())
                {
                    bool anyNull = false;
                    for (int i = 0; i < 5; i++)
                    {
                        if (result.IsDBNull(i))
                        {
                            Global.systemLog.Warn("Null value for " + result.GetName(i));
                            anyNull = true;
                            break;
                        }
                    }
                    if (anyNull)
                    {
                        continue;
                    }

                    PackageUsage usage = new PackageUsage();
                    usage.PackageID = result.GetGuid(0).ToString();
                    usage.ChildSpaceID = result.GetGuid(1).ToString();
                    usage.SpaceName = result.GetString(2);
                    usage.SpaceOwner = result.GetString(3);
                    usage.Available = result.GetBoolean(4);
                    usage.PackageSpaceID = !result.IsDBNull(5) ? result.GetGuid(5).ToString() : null;
                    response.Add(usage);
                }
            }
            finally
            {
                if (result != null)
                    result.Close();
                if (cmd != null)
                    cmd.Dispose();
            }

            return response;
        }

        private static string getAddUsageToPackageQuery(QueryConnection conn, string schema)
        {
            string command = "INSERT INTO" + SINGLE_SPACE +
                getTableName(schema, PACKAGE_USAGE_INXN) + SINGLE_SPACE +
                "(PACKAGE_ID, CHILD_SPACE_ID, CreatedDate, CreatedBy, Available, PARENT_SPACE_ID)" + SINGLE_SPACE +
                "Values (?,?," + conn.getCurrentTimestamp() + ",?,?,?)";

            return command;
        }

        public static void addUsageToPackage(QueryConnection conn, string schema, User user, Space packageSpace, Guid packageID, Space childSpace)
        {
            addUsageToPackage(conn, schema, user, packageSpace.ID, packageID, childSpace.ID);
        }

        public static void addUsageToPackage(QueryConnection conn, string schema, User user, Guid packageSpaceID, Guid packageID, Guid childSpaceID)
        {
            QueryCommand cmd = null;
            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = getAddUsageToPackageQuery(conn, schema);
                cmd.Parameters.Add("@PACKAGE_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(packageID);
                cmd.Parameters.Add("@CHILD_SPACE_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(childSpaceID);
                cmd.Parameters.Add("@CreatedBy", OdbcType.VarChar).Value = user.Username;
                cmd.Parameters.Add("@Available", conn.getODBCBooleanType()).Value = conn.getBooleanValue(true);
                cmd.Parameters.Add("@PARENT_SPACE_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(packageSpaceID);
                int rowAffected = cmd.ExecuteNonQuery();
            }
            finally
            {
                if (cmd != null)
                {
                    try
                    {
                        cmd.Dispose();
                    }
                    catch (Exception ex2)
                    {
                        Global.systemLog.Warn("Unable to disposing cmd object ", ex2);
                    }
                }
            }

        }

        private static string removeUsageFromPackageQuery(string schema)
        {
            string command = "DELETE FROM" + SINGLE_SPACE +
                getTableName(schema, PACKAGE_USAGE_INXN) + SINGLE_SPACE +
                "WHERE PACKAGE_ID=? and CHILD_SPACE_ID=? AND PARENT_SPACE_ID=?";

            return command;
        }

        public static void removeUsageFromPackage(QueryConnection conn, string schema, User user, Space packageSpace, Guid packageID, Space childSpace)
        {
            QueryCommand cmd = null;
            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = removeUsageFromPackageQuery(schema);
                cmd.Parameters.Add("@PACKAGE_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(packageID);
                cmd.Parameters.Add("@CHILD_SPACE_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(childSpace.ID);
                cmd.Parameters.Add("@PARENT_SPACE_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(packageSpace.ID);
                int rowAffected = cmd.ExecuteNonQuery();
            }
            finally
            {

                if (cmd != null)
                {
                    try
                    {
                        cmd.Dispose();
                    }
                    catch (Exception ex2)
                    {
                        Global.systemLog.Warn("Unable to dispose cmd object ", ex2);
                    }
                }
            }
        }

        private static string removeAllPackageUsagesForChildSpaceQuery(string schema)
        {
            string command = "DELETE FROM" + SINGLE_SPACE +
                getTableName(schema, PACKAGE_USAGE_INXN) + SINGLE_SPACE + " WHERE CHILD_SPACE_ID=?";

            return command;
        }

        public static void removeAllPackageUsagesForChildSpace(QueryConnection conn, string schema, Guid childSpaceID)
        {
            QueryCommand cmd = null;
            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = removeAllPackageUsagesForChildSpaceQuery(schema);
                cmd.Parameters.Add("@CHILD_SPACE_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(childSpaceID);
                int rowAffected = cmd.ExecuteNonQuery();
                Global.systemLog.Info(rowAffected + " packages deleted from " + PACKAGE_USAGE_INXN + " for child space " + childSpaceID);
            }
            finally
            {
                if (cmd != null)
                {
                    try
                    {
                        cmd.Dispose();
                    }
                    catch (Exception ex2)
                    {
                        Global.systemLog.Warn("Unable to dispose cmd object ", ex2);
                    }
                }
            }
        }

        private static string getAddSpaceGroupToPackageQuery(QueryConnection conn, string schema)
        {
            string command = "INSERT INTO" + SINGLE_SPACE +
                getTableName(schema, PACKAGE_GROUP_INXN) + SINGLE_SPACE +
                "(PACKAGE_ID,SPACE_ID,GROUP_ID,CreatedDate,CreatedBy)" + SINGLE_SPACE +
                "Values (?,?,?," + conn.getCurrentTimestamp() + ",?)";

            return command;
        }

        public static void addGroupsToPackage(QueryConnection conn, string schema, User user, Guid packageID, Space sp, string[] groupIDs)
        {
            QueryCommand cmd = null;
            OdbcTransaction transaction = null;
            try
            {
                cmd = conn.CreateCommand();
                transaction = conn.BeginTransaction();
                cmd.Transaction = transaction;
                cmd.CommandText = getAddSpaceGroupToPackageQuery(conn, schema);

                foreach (string groupId in groupIDs)
                {
                    Guid groupGuid = new Guid(groupId);
                    cmd.Parameters.Clear();
                    cmd.Parameters.Add("@PACKAGE_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(packageID);
                    cmd.Parameters.Add("@SPACE_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(sp.ID);
                    cmd.Parameters.Add("@GROUP_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(groupGuid);
                    cmd.Parameters.Add("@CreatedBy", OdbcType.VarChar).Value = user.Username;
                    int rowAffected = cmd.ExecuteNonQuery();
                }
                transaction.Commit();
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Exception in addGroupsToPackage : Rolling back the transaction ", ex);
                if (transaction != null)
                {
                    try
                    {
                        transaction.Rollback();
                    }
                    catch (Exception ex2)
                    {
                        Global.systemLog.Warn("Unable to rollback transaction ", ex2);
                    }
                }

                throw ex;
            }
            finally
            {

                if (cmd != null)
                {
                    try
                    {
                        cmd.Dispose();
                    }
                    catch (Exception ex2)
                    {
                        Global.systemLog.Warn("Unable to disposing cmd object ", ex2);
                    }
                }
            }

        }

        private static string getRemoveAllPackageGroupsForSpace()
        {
            string command = "DELETE FROM" + SINGLE_SPACE +
                   getTableName(mainSchema, PACKAGE_GROUP_INXN) + SINGLE_SPACE +
                   "WHERE SPACE_ID=?";

            return command;
        }

        public static void removeAllPackageGroupsFromSpace(QueryConnection conn, string schema, Guid spID)
        {

            QueryCommand cmd = null;
            DateTime dateTime = DateTime.Now;
            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = getRemoveAllPackageGroupsForSpace();
                cmd.Parameters.Clear();
                cmd.Parameters.Add("@SPACE_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(spID);
                int rowAffected = cmd.ExecuteNonQuery();
            }
            finally
            {
                if (cmd != null)
                {
                    try
                    {
                        cmd.Dispose();
                    }
                    catch (Exception ex2)
                    {
                        Global.systemLog.Warn("Unable to disposing cmd object ", ex2);
                    }
                }
            }
        }

        private static string getRemoveAllGroupsForPackageQuery()
        {
            string command = "DELETE FROM" + SINGLE_SPACE +
                   getTableName(mainSchema, PACKAGE_GROUP_INXN) + SINGLE_SPACE +
                   "WHERE PACKAGE_ID=? AND SPACE_ID=?";

            return command;
        }

        public static void removeAllGroupsFromPackage(QueryConnection conn, string schema, Guid packageID, Space sp)
        {

            QueryCommand cmd = null;
            DateTime dateTime = DateTime.Now;
            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = getRemoveAllGroupsForPackageQuery();
                cmd.Parameters.Clear();
                cmd.Parameters.Add("@PACKAGE_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(packageID);
                cmd.Parameters.Add("@SPACE_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(sp.ID);
                int rowAffected = cmd.ExecuteNonQuery();
            }
            finally
            {
                if (cmd != null)
                {
                    try
                    {
                        cmd.Dispose();
                    }
                    catch (Exception ex2)
                    {
                        Global.systemLog.Warn("Unable to disposing cmd object ", ex2);
                    }
                }
            }
        }

        private static string getRemoveSpaceGroupFromPackageQuery()
        {
            string command = "DELETE FROM" + SINGLE_SPACE +
                getTableName(mainSchema, PACKAGE_GROUP_INXN) + SINGLE_SPACE +
                "WHERE PACKAGE_ID=? AND SPACE_ID=? AND GROUP_ID=?";

            return command;
        }

        public static void removeGroupsFromPackage(QueryConnection conn, string schema, Guid packageID, Space sp, string[] groupIDs)
        {

            QueryCommand cmd = null;
            OdbcTransaction transaction = null;
            DateTime dateTime = DateTime.Now;
            try
            {
                cmd = conn.CreateCommand();
                transaction = conn.BeginTransaction();
                cmd.Transaction = transaction;

                cmd.CommandText = getRemoveSpaceGroupFromPackageQuery();
                foreach (string groupId in groupIDs)
                {
                    Guid groupGuid = new Guid(groupId);
                    cmd.Parameters.Clear();
                    cmd.Parameters.Add("@PACKAGE_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(packageID);
                    cmd.Parameters.Add("@SPACE_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(sp.ID);
                    cmd.Parameters.Add("@GROUP_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(groupGuid);
                    int rowAffected = cmd.ExecuteNonQuery();
                }

                transaction.Commit();
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Exception in removeGroupsFromPackag : Rolling back the transaction ", ex);
                if (transaction != null)
                {
                    try
                    {
                        transaction.Rollback();
                    }
                    catch (Exception ex2)
                    {
                        Global.systemLog.Warn("Unable to rollback transaction ", ex2);
                    }
                }

                throw ex;
            }
            finally
            {

                if (cmd != null)
                {
                    try
                    {
                        cmd.Dispose();
                    }
                    catch (Exception ex2)
                    {
                        Global.systemLog.Warn("Unable to disposing cmd object ", ex2);
                    }
                }
            }
        }

        private static void transactUpdatePackageUsageTableWithSpaceId(QueryConnection conn, QueryCommand cmd, string schema, Guid spaceID1, Guid spaceID2)
        {
            cmd.CommandText = "UPDATE " + getTableName(schema, PACKAGE_USAGE_INXN) + " SET CHILD_SPACE_ID=? WHERE CHILD_SPACE_ID=?";
            cmd.Parameters.Clear();
            cmd.Parameters.Add("@CHILD_SPACE_ID1", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(spaceID2);
            cmd.Parameters.Add("@CHILD_SPACE_ID2", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(spaceID1);
            cmd.ExecuteNonQuery();
        }

        /// <summary>
        /// Swaps the space ids in the package_usage_inxn table so that 
        /// when swap is done, all the imported packages are correctly shown in the usages tab of the parent packages tab
        /// </summary>
        /// <param name="conn"></param>
        /// <param name="schema"></param>
        /// <param name="spaceID1"></param>
        /// <param name="spaceID2"></param>
        public static void swapPackageUsages(QueryConnection conn, string schema, Guid spaceID1, Guid spaceID2)
        {
            QueryCommand cmd = null;
            OdbcTransaction transaction = null;
            try
            {
                cmd = conn.CreateCommand();
                transaction = conn.BeginTransaction();
                cmd.Transaction = transaction;
                Guid tmpGuid = Guid.NewGuid();
                transactUpdatePackageUsageTableWithSpaceId(conn, cmd, schema, spaceID1, tmpGuid);
                transactUpdatePackageUsageTableWithSpaceId(conn, cmd, schema, spaceID2, spaceID1);
                transactUpdatePackageUsageTableWithSpaceId(conn, cmd, schema, tmpGuid, spaceID2);
                transaction.Commit();
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Exception in swapPackageUsage : Rolling back the transaction ", ex);
                if (transaction != null)
                {
                    try
                    {
                        transaction.Rollback();
                    }
                    catch (Exception ex2)
                    {
                        Global.systemLog.Warn("Unable to rollback transaction ", ex2);
                    }
                }

                throw ex;
            }
            finally
            {

                if (cmd != null)
                {
                    try
                    {
                        cmd.Dispose();
                    }
                    catch (Exception ex2)
                    {
                        Global.systemLog.Warn("Unable to disposing cmd object ", ex2);
                    }
                }
            }
        }

        public static Guid addGroupToSpace(QueryConnection conn, string schema, Space sp, string groupName, bool internalGroup)
        {
            return addGroupToSpace(conn, schema, sp, groupName, internalGroup, true);
        }

        public static Guid addGroupToSpace(QueryConnection conn, string schema, Space sp, string groupName, bool internalGroup, bool updateMappingModifiedDate)
        {
            bool added = false;
            QueryCommand cmd = null;
            Guid groupID = Guid.NewGuid();
            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = getAddGroupToSpaceQuery(conn, schema);

                cmd.Parameters.Add("@SPACE_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(sp.ID);
                cmd.Parameters.Add("@GROUP_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(groupID);
                cmd.Parameters.Add("@GroupName", OdbcType.NVarChar).Value = groupName;
                cmd.Parameters.Add("@InternalGroup", conn.getODBCBooleanType()).Value = conn.getBooleanValue(internalGroup);
                int rowAffected = cmd.ExecuteNonQuery();
                if (rowAffected > 0)
                {
                    added = true;
                }
                else
                {
                    Global.systemLog.Error("Unable to add the group " + groupName + " to the space " + sp.ID.ToString());
                    added = false;
                }
            }
            finally
            {
                if (cmd != null)
                    cmd.Dispose();
            }

            if (!added)
            {
                // if there was a problem adding, return empty guid
                groupID = Guid.Empty;
            }
            else
            {
                Database.updateMappingModifiedDate(conn, schema, sp);
            }
            return groupID;
        }

        public static void updateSpaceGroupInfo(QueryConnection conn, string schema, Space sp, SpaceGroup group)
        {
            QueryCommand cmd = null;
            bool updated = false;
            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = "UPDATE " + getTableName(schema, SPACE_GROUP_TABLE) + SINGLE_SPACE +
                    "SET GroupName=?, InternalGroup=?, ModifiedDate=" + conn.getCurrentTimestamp() + SINGLE_SPACE +
                    "WHERE SPACE_ID = ? AND GROUP_ID = ?";

                cmd.Parameters.Add("@GroupName", OdbcType.NVarChar).Value = group.Name;
                cmd.Parameters.Add("@InternalGroup", conn.getODBCBooleanType()).Value = conn.getBooleanValue(group.InternalGroup);
                cmd.Parameters.Add("@SPACE_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(sp.ID);
                cmd.Parameters.Add("@GROUP_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(group.ID);
                int rowAffected = cmd.ExecuteNonQuery();
                if (rowAffected > 0)
                {
                    updated = true;
                }
                else
                {
                    Global.systemLog.Warn("Unable to update the group " + group.Name + " to the space " + sp.ID.ToString());
                    updated = false;
                }
            }
            finally
            {
                if (cmd != null)
                    cmd.Dispose();
            }

            if (updated)
            {
                Database.updateMappingModifiedDate(conn, schema, sp);
            }
        }

        private static void transactRemoveUnmatchedPackageGroups(QueryConnection conn, QueryCommand cmd, Dictionary<Guid, List<Guid>> spaceIDToPackageIds)
        {
            cmd.CommandText = getRemoveAllGroupsForPackageQuery();
            foreach (KeyValuePair<Guid, List<Guid>> kvPair in spaceIDToPackageIds)
            {
                Guid spaceID = kvPair.Key;
                List<Guid> packageIDs = kvPair.Value;
                if (packageIDs != null && packageIDs.Count > 0)
                {
                    foreach (Guid packageID in packageIDs)
                    {
                        cmd.Parameters.Clear();
                        cmd.Parameters.Add("@PACKAGE_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(packageID);
                        cmd.Parameters.Add("@SPACE_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(spaceID);
                        int rowAffected = cmd.ExecuteNonQuery();
                    }
                }
            }
        }

        private static void transactUpdatePackageIDForGroups(QueryConnection conn, QueryCommand cmd, Guid spaceID, Dictionary<Guid, Guid> oldToNewPackageIds)
        {
            cmd.CommandText = getUpdatePackageIDForGroupQuery();
            foreach (KeyValuePair<Guid, Guid> kvPair in oldToNewPackageIds)
            {
                Guid oldPackageID = kvPair.Key;
                Guid newPackageID = kvPair.Value;
                cmd.Parameters.Clear();
                cmd.Parameters.Add("@PACKAGE_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(newPackageID);
                cmd.Parameters.Add("@PACKAGE_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(oldPackageID);
                cmd.Parameters.Add("@SPACE_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(spaceID);
                int rowAffected = cmd.ExecuteNonQuery();
            }
        }

        private static string getUpdatePackageIDForGroupQuery()
        {
            string command = "UPDATE " + getTableName(mainSchema, PACKAGE_GROUP_INXN) + SINGLE_SPACE +
                " Set PACKAGE_ID=?" +
                " WHERE PACKAGE_ID=? AND SPACE_ID=?";

            return command;
        }

        public static void updatePackageGroupsInfoOnSwap(QueryConnection conn, string schema, Guid prodSpaceID, Guid devSpaceID,
            Dictionary<Guid, List<Guid>> spaceToUnmatchedPackages, Dictionary<Guid, Guid> devToProdAlignPackagsIds)
        {
            QueryCommand cmd = null;
            OdbcTransaction transaction = null;
            DateTime dateTime = DateTime.Now;
            try
            {
                cmd = conn.CreateCommand();
                transaction = conn.BeginTransaction();
                cmd.Transaction = transaction;
                transactRemoveUnmatchedPackageGroups(conn, cmd, spaceToUnmatchedPackages);
                transactUpdatePackageIDForGroups(conn, cmd, devSpaceID, devToProdAlignPackagsIds);
                transaction.Commit();
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Exception in removeGroupsFromPackag : Rolling back the transaction ", ex);
                if (transaction != null)
                {
                    try
                    {
                        transaction.Rollback();
                    }
                    catch (Exception ex2)
                    {
                        Global.systemLog.Warn("Unable to rollback transaction ", ex2);
                    }
                }

                throw ex;
            }
            finally
            {

                if (cmd != null)
                {
                    try
                    {
                        cmd.Dispose();
                    }
                    catch (Exception ex2)
                    {
                        Global.systemLog.Warn("Unable to disposing cmd object ", ex2);
                    }
                }
            }
        }

        public static bool removeGroupFromSpace(QueryConnection conn, string schema, Space sp, SpaceGroup group)
        {
            return removeGroupFromSpace(conn, schema, sp, group, false);
        }

        public static bool removeGroupFromSpace(QueryConnection conn, string schema, Space sp, SpaceGroup group, bool deleteInternal)
        {
            return removeGroupFromSpace(conn, schema, sp, group, deleteInternal, true);
        }

        public static bool removeGroupFromSpace(QueryConnection conn, string schema, Space sp, SpaceGroup group, bool deleteInternal, bool updateMappingModifiedDate)
        {
            if (group.InternalGroup && !deleteInternal)
            {
                Global.systemLog.Warn("Cannot remove internal group with name : " + group.Name + " and id : " + group.ID);
                return false;
            }

            bool removed = false;
            QueryCommand cmd = null;
            OdbcTransaction transaction = null;
            try
            {
                cmd = conn.CreateCommand();
                transaction = conn.BeginTransaction();
                cmd.Transaction = transaction;

                // DELETE GROUP ACLS 
                cmd.CommandText = "DELETE FROM" + SINGLE_SPACE + getTableName(schema, GROUP_ACL_TABLE) + SINGLE_SPACE +
                    "WHERE GROUP_ID = ?";
                cmd.Parameters.Add("@GROUP_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(group.ID);
                int numGroupAClsDeleted = cmd.ExecuteNonQuery();

                // DELETE GROUP_USERS
                cmd.Parameters.Clear();

                cmd.CommandText = "DELETE FROM" + SINGLE_SPACE + getTableName(schema, GROUP_USER_TABLE) + SINGLE_SPACE +
                    "WHERE GROUP_ID = ?";
                cmd.Parameters.Add("@GROUP_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(group.ID);
                int numGroupUsersDeleted = cmd.ExecuteNonQuery();

                // NOW DELETE SPACE FROM GROUP
                cmd.Parameters.Clear();
                cmd.CommandText = "DELETE FROM" + SINGLE_SPACE +
                    getTableName(schema, SPACE_GROUP_TABLE) + SINGLE_SPACE +
                    "WHERE SPACE_ID = ? AND GROUP_ID = ?";

                cmd.Parameters.Add("@SPACE_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(sp.ID);
                cmd.Parameters.Add("@GROUP_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(group.ID);
                int rowAffected = cmd.ExecuteNonQuery();
                if (rowAffected > 0)
                {
                    removed = true;
                    Global.systemLog.Debug("Deleted Group " + group.Name + " with id " + group.ID + " from space " + sp.ID + "  , " + numGroupAClsDeleted + " ACLS , " + numGroupUsersDeleted + " Users");
                }
                else
                {
                    Global.systemLog.Warn("Unable to remove the group with id : " + group.ID + " and name : " + group.Name + " from the space " + sp.ID.ToString());
                    removed = false;
                }
                transaction.Commit();
                cmd.Dispose();
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Exception during removing Group to Space " + sp.ID, ex);
                try
                {
                    if (transaction != null)
                    {
                        transaction.Rollback();
                    }
                    if (cmd != null)
                    {
                        cmd.Dispose();
                    }
                }
                catch (Exception)
                {
                    //Global.systemLog.Warn("Cannot close result/cmd object. Let system handle", ex2);
                }

                throw ex;
            }

            if (updateMappingModifiedDate)
                Database.updateMappingModifiedDate(conn, schema, sp);
            return removed;
        }


        private static bool transactRemoveGroupFromSpace(QueryConnection conn, QueryCommand cmd, string schema, Space sp, SpaceGroup group)
        {
            // DELETE GROUP ACLS
            cmd.CommandText = "DELETE FROM" + SINGLE_SPACE + getTableName(schema, GROUP_ACL_TABLE) + SINGLE_SPACE +
                "WHERE GROUP_ID = ?";
            cmd.Parameters.Add("@GROUP_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(group.ID);
            int numGroupAClsDeleted = cmd.ExecuteNonQuery();

            // DELETE GROUP_USERS
            cmd.Parameters.Clear();

            cmd.CommandText = "DELETE FROM" + SINGLE_SPACE + getTableName(schema, GROUP_USER_TABLE) + SINGLE_SPACE +
                "WHERE GROUP_ID = ?";
            cmd.Parameters.Add("@GROUP_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(group.ID);
            int numGroupUsersDeleted = cmd.ExecuteNonQuery();

            // NOW DELETE SPACE FROM GROUP
            cmd.Parameters.Clear();
            cmd.CommandText = "DELETE FROM" + SINGLE_SPACE +
                getTableName(schema, SPACE_GROUP_TABLE) + SINGLE_SPACE +
                "WHERE SPACE_ID = ? AND GROUP_ID = ?";

            cmd.Parameters.Add("@SPACE_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(sp.ID);
            cmd.Parameters.Add("@GROUP_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(group.ID);
            int rowAffected = cmd.ExecuteNonQuery();

            return (rowAffected > 0);
        }

        public static void removeAllSpaceGroups(QueryConnection conn, string schema, Space sp, List<SpaceGroup> spaceGroupList)
        {
            if (spaceGroupList == null || (spaceGroupList != null && spaceGroupList.Count == 0))
            {
                Global.systemLog.Warn("No spaceGroups to delete for " + sp.ID);
                return;
            }

            QueryCommand cmd = null;
            OdbcTransaction transaction = null;

            try
            {
                cmd = conn.CreateCommand();
                transaction = conn.BeginTransaction();
                cmd.Transaction = transaction;
                foreach (SpaceGroup spg in spaceGroupList)
                {
                    cmd.CommandText = "";
                    cmd.Parameters.Clear();
                    Database.transactRemoveGroupFromSpace(conn, cmd, schema, sp, spg);
                }
                transaction.Commit();
                cmd.Dispose();
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Exception during removingall  group from Space " + sp.ID + ex);
                try
                {
                    if (transaction != null)
                    {
                        transaction.Rollback();
                    }
                    if (cmd != null)
                    {
                        cmd.Dispose();
                    }
                }
                catch (Exception)
                {
                    //Global.systemLog.Warn("Cannot close result/cmd object. Let system handle", ex2);
                }

                throw ex;
            }

            Database.updateMappingModifiedDate(conn, schema, sp);
        }


        public static void updateUsersForSpaceGroup(QueryConnection conn, string schema, Space sp, SpaceGroup oldGroup, List<string> userNames)
        {
            // Get info from the database
            List<User> usersToAdd = new List<User>();
            List<User> usersToRemove = new List<User>(oldGroup.GroupUsers);

            foreach (User u in oldGroup.GroupUsers)
            {
                if (!userNames.Contains(u.Username))
                {
                    usersToRemove.Add(u);
                }
                else
                {
                    // if already present, remove it from the add list
                    usersToAdd.Remove(u);
                }
            }


            foreach (string uName in userNames)
            {
                User currentUser = UserAndGroupUtils.getUserFromGroup(oldGroup, uName);
                if (currentUser == null)
                {
                    currentUser = Database.getUser(conn, schema, uName, false);
                    usersToAdd.Add(currentUser);
                }
                else
                {
                    // if it is already present. Remove it from the remove list
                    usersToRemove.Remove(currentUser);
                }
            }

            QueryCommand cmd = null;
            OdbcTransaction transaction = null;
            int deletedRows = 0;
            int addedUsers = 0;
            try
            {
                cmd = conn.CreateCommand();
                transaction = conn.BeginTransaction();
                cmd.Transaction = transaction;


                cmd.Parameters.Add("@Group_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(oldGroup.ID);


                if (usersToRemove != null && usersToRemove.Count > 0)
                {
                    cmd.CommandText = "DELETE FROM" + SINGLE_SPACE + getTableName(schema, GROUP_USER_TABLE) +
                    SINGLE_SPACE + "WHERE GROUP_ID = ? AND USER_ID = ?";

                    foreach (User deleteGroupUser in usersToRemove)
                    {
                        cmd.Parameters.Clear();
                        cmd.Parameters.Add("@GROUP_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(oldGroup.ID);
                        cmd.Parameters.Add("@USER_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(deleteGroupUser.ID);
                        deletedRows = deletedRows + cmd.ExecuteNonQuery();
                    }
                }

                // Add new users now
                cmd.Parameters.Clear();

                if (usersToAdd != null && usersToAdd.Count > 0)
                {
                    cmd.CommandText = "INSERT INTO" + SINGLE_SPACE + getTableName(schema, GROUP_USER_TABLE) +
                     "(GROUP_ID,USER_ID,CreatedDate,ModifiedDate) " +
                     "Values(?,?," + conn.getCurrentTimestamp() + "," + conn.getCurrentTimestamp() + ")";
                    foreach (User addGroupUser in usersToAdd)
                    {
                        cmd.Parameters.Clear();
                        cmd.Parameters.Add("@GROUP_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(oldGroup.ID);
                        cmd.Parameters.Add("@USER_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(addGroupUser.ID);
                        addedUsers = addedUsers + cmd.ExecuteNonQuery();
                    }
                }
                Global.systemLog.Debug("Removed " + deletedRows + " users. Added " + addedUsers + " for group : " + oldGroup.ID + " : name : " + oldGroup.Name);
                transaction.Commit();
                cmd.Dispose();
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Exception while updating group details :" + oldGroup.ID + "  id: " + oldGroup.Name + " ", ex);
                try
                {
                    if (transaction != null)
                    {
                        transaction.Rollback();
                    }
                }
                catch (Exception ex2)
                {
                    Global.systemLog.Warn("Unable to rollback transaction" + ex2);
                }

                try
                {
                    if (cmd != null)
                    {
                        cmd.Dispose();
                    }
                }
                catch (Exception)
                {
                    //Global.systemLog.Warn("Unable to close resources. Let System handle it" + ex3);
                }

                throw ex;
            }

            Database.updateMappingModifiedDate(conn, schema, sp);
        }


        public static string getAddUserToGroupQuery(QueryConnection conn, string schema)
        {
            string command = "INSERT INTO" + SINGLE_SPACE +
                getTableName(schema, GROUP_USER_TABLE) + SINGLE_SPACE +
                "(GROUP_ID,USER_ID,CreatedDate,ModifiedDate)" + SINGLE_SPACE +
                "Values (?,?," + conn.getCurrentTimestamp() + "," + conn.getCurrentTimestamp() + ")";

            return command;
        }

        public static bool addUserToGroup(QueryConnection conn, string schema, Space sp, SpaceGroup group, User u)
        {
            return addUserToGroup(conn, schema, sp, group.ID, u.ID, true);
        }

        public static bool addUserToGroup(QueryConnection conn, string schema, Space sp, Guid groupID, Guid userID, bool updateMappingModifiedDate)
        {
            bool added = false;
            QueryCommand cmd = null;
            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = getAddUserToGroupQuery(conn, schema);

                cmd.Parameters.Add("@GROUP_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(groupID);
                cmd.Parameters.Add("@USER_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(userID);
                int rowAffected = cmd.ExecuteNonQuery();
                if (rowAffected > 0)
                {
                    added = true;
                }
                else
                {
                    Global.systemLog.Error("Unable to add the group " + groupID + " to the space " + userID);
                    added = false;
                }
            }
            finally
            {
                if (cmd != null)
                    cmd.Dispose();
            }

            if (updateMappingModifiedDate)
                Database.updateMappingModifiedDate(conn, schema, sp);
            return added;
        }

        public static bool removeUserFromGroup(QueryConnection conn, string schema, Space sp, Guid groupID, Guid userID, bool updateMappingModifiedDate)
        {
            bool removed = false;
            QueryCommand cmd = null;
            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = "DELETE FROM" + SINGLE_SPACE +
                    getTableName(schema, GROUP_USER_TABLE) + SINGLE_SPACE +
                    "WHERE GROUP_ID = ? and USER_ID= ?";

                cmd.Parameters.Add("@GROUP_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(groupID);
                cmd.Parameters.Add("@USER_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(userID);

                int rowAffected = cmd.ExecuteNonQuery();
                if (rowAffected > 0)
                {
                    Global.systemLog.Debug("Deleted the user " + userID + " from the group " + groupID);
                }
                else
                {
                    Global.systemLog.Warn("Unable to delete the user " + userID + " from the group " + groupID);
                }
                removed = true;
            }
            finally
            {
                if (cmd != null)
                    cmd.Dispose();
            }

            if (updateMappingModifiedDate)
                Database.updateMappingModifiedDate(conn, schema, sp);
            return removed;
        }



        public static List<int> getGroupACLs(QueryConnection conn, string schema, Space sp, string groupName)
        {
            QueryCommand cmd = null;
            List<int> groupACLs = new List<int>();
            QueryReader result = null;
            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = "SELECT A.GROUP_ID,B.ACL_ID FROM" + SINGLE_SPACE +
                    getTableName(schema, SPACE_GROUP_TABLE) + "A INNER JOIN " + SINGLE_SPACE +
                    getTableName(schema, GROUP_ACL_TABLE) + "B ON A.GROUP_ID = B.GROUP_ID AND A.SPACE_ID = ? AND A.GroupName = ?";
                cmd.Parameters.Add("@SPACE_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(sp.ID);
                cmd.Parameters.Add("@GroupName", OdbcType.NVarChar).Value = groupName;
                result = cmd.ExecuteReader();
                while (result.Read())
                {
                    if (!result.IsDBNull(1))
                    {
                        groupACLs.Add(result.GetInt32(1));
                    }
                }
            }
            finally
            {
                if (result != null)
                    result.Close();
                if (cmd != null)
                    cmd.Dispose();
            }

            return groupACLs;
        }

        public static List<int> getACLsForUser(QueryConnection conn, string schema, User u)
        {
            QueryCommand cmd = null;
            List<int> userACLs = new List<int>();
            QueryReader result = null;
            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = "SELECT DISTINCT A.ID FROM" + SINGLE_SPACE +
                    getTableName(schema, ACL_INFO_TABLE) + " A INNER JOIN " + SINGLE_SPACE +
                    getTableName(schema, GROUP_ACL_TABLE) + " B ON A.ID = B.ACL_ID " +
                    " INNER JOIN " + getTableName(schema, GROUP_USER_TABLE) + " C ON C.GROUP_ID = B.GROUP_ID " +
                    " WHERE C.USER_ID = ?";
                cmd.Parameters.Add("@USER_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(u.ID);
                result = cmd.ExecuteReader();
                while (result.Read())
                {
                    if (!result.IsDBNull(0))
                    {
                        userACLs.Add(result.GetInt32(0));
                    }
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Exception while retreiving Acls for user", ex);
            }
            finally
            {
                try
                {
                    if (result != null)
                    {
                        result.Close();
                    }
                    if (cmd != null)
                    {
                        cmd.Dispose();
                    }
                }
                catch (Exception)
                {
                    //Global.systemLog.Warn("Cannot close cmd/result object. Let system handle", ex2);
                }
            }
            return userACLs;
        }

        public static List<int> getGroupACLs(QueryConnection conn, string schema, Guid groupID)
        {
            QueryCommand cmd = null;
            List<int> groupACLs = new List<int>();
            QueryReader result = null;
            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = "SELECT GROUP_ID,ACL_ID FROM" + SINGLE_SPACE +
                    getTableName(schema, GROUP_ACL_TABLE) + SINGLE_SPACE +
                    "WHERE GROUP_ID = ?";
                cmd.Parameters.Add("@GROUP_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(groupID);
                result = cmd.ExecuteReader();
                while (result.Read())
                {
                    if (!result.IsDBNull(1))
                    {
                        groupACLs.Add(result.GetInt32(1));
                    }
                }
            }
            finally
            {
                if (result != null)
                    result.Close();
                if (cmd != null)
                    cmd.Dispose();
            }
            return groupACLs;
        }

        public static bool removeAllACLsFromGroup(QueryConnection conn, string schema, Space sp, Guid groupID)
        {
            bool removed = false;
            QueryCommand cmd = null;
            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = "DELETE FROM" + SINGLE_SPACE +
                    getTableName(schema, GROUP_ACL_TABLE) +
                    SINGLE_SPACE + "WHERE GROUP_ID = ? ";
                cmd.Parameters.Add("@GROUP_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(groupID);
                cmd.ExecuteNonQuery();
                removed = true;
            }
            finally
            {
                if (cmd != null)
                    cmd.Dispose();
            }

            Database.updateMappingModifiedDate(conn, schema, sp);
            return removed;
        }

        public static bool removeACLFromGroup(QueryConnection conn, string schema, Space sp, Guid groupID, int ACLID)
        {
            return removeACLFromGroup(conn, schema, sp, groupID, ACLID, true);
        }

        public static bool removeACLFromGroup(QueryConnection conn, string schema, Space sp, Guid groupID, int ACLID, bool updateMappingModifiedDate)
        {
            bool removed = false;
            QueryCommand cmd = null;
            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = "DELETE FROM" + SINGLE_SPACE +
                    getTableName(schema, GROUP_ACL_TABLE) +
                    SINGLE_SPACE + "WHERE GROUP_ID = ?  AND ACL_ID = ?";

                cmd.Parameters.Add("@GROUP_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(groupID);
                cmd.Parameters.Add("@ACL_ID", OdbcType.Int).Value = ACLID;
                cmd.ExecuteNonQuery();
                removed = true;
                cmd.Dispose();
            }
            finally
            {
                if (cmd != null)
                    cmd.Dispose();
            }

            if (updateMappingModifiedDate)
                Database.updateMappingModifiedDate(conn, schema, sp);
            return removed;
        }

        public static string getAddACLToGroupQuery(QueryConnection conn, string schema)
        {
            string command = "INSERT INTO" + SINGLE_SPACE +
                getTableName(schema, GROUP_ACL_TABLE) + SINGLE_SPACE +
                "(GROUP_ID,ACL_ID,CreatedDate,ModifiedDate)" + SINGLE_SPACE +
                "Values (?,?," + conn.getCurrentTimestamp() + "," + conn.getCurrentTimestamp() + ")";

            return command;
        }

        public static bool addACLToGroup(QueryConnection conn, string schema, Space sp, Guid groupID, int ACLID)
        {
            return addACLToGroup(conn, schema, sp, groupID, ACLID, true);
        }

        public static bool addACLToGroup(QueryConnection conn, string schema, Space sp, Guid groupID, int ACLID, bool updateMappingModifiedDate)
        {
            bool added = false;
            QueryCommand cmd = null;
            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = getAddACLToGroupQuery(conn, schema);

                cmd.Parameters.Add("@GROUP_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(groupID);
                cmd.Parameters.Add("@ACL_ID", OdbcType.Int).Value = ACLID;
                int rowAffected = cmd.ExecuteNonQuery();
                if (rowAffected > 0)
                {
                    added = true;
                }
                else
                {
                    Global.systemLog.Error("Unable to add the acl " + ACLID + " to the group " + groupID);
                    added = false;
                }
                cmd.Dispose();
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Exception during adding Group to Space " + ex);
                try
                {
                    if (cmd != null)
                    {
                        cmd.Dispose();
                    }
                }
                catch (Exception)
                {
                    //Global.systemLog.Warn("Cannot close cmd object. Let system handle", ex2);
                }

                throw ex;
            }

            Database.updateMappingModifiedDate(conn, schema, sp);
            return added;
        }

        public static SpaceGroup getGroupInfo(QueryConnection conn, string schema, Space sp, string groupName)
        {
            SpaceGroup spg = null;
            QueryCommand cmd = null;
            QueryReader result = null;
            try
            {

                // GET GROUP_IDS FIRST 
                cmd = conn.CreateCommand();
                cmd.CommandText = "SELECT A.GROUP_ID,A.GroupName, A.InternalGroup, B.USER_ID, C.Username, A.CreatedDate FROM" +
                    SINGLE_SPACE + getTableName(schema, SPACE_GROUP_TABLE) + SINGLE_SPACE + "A LEFT OUTER JOIN" +
                    SINGLE_SPACE + getTableName(schema, GROUP_USER_TABLE) + SINGLE_SPACE +
                    "B ON A.GROUP_ID = B.GROUP_ID" + SINGLE_SPACE + "LEFT OUTER JOIN" +
                    SINGLE_SPACE + getTableName(schema, USER_TABLE_NAME) + SINGLE_SPACE +
                    "C ON B.USER_ID = C.PKID" + SINGLE_SPACE +
                    "WHERE A.SPACE_ID =? AND A.GroupName = ? " + SINGLE_SPACE +
                    "GROUP BY A.GROUP_ID,A.GroupName, A.InternalGroup, B.USER_ID,C.Username, A.CreatedDate" + SINGLE_SPACE +
                    "ORDER BY A.CreatedDate asc";
                cmd.Parameters.Add("@SPACE_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(sp.ID);
                cmd.Parameters.Add("@GroupName", OdbcType.NVarChar).Value = groupName;
                result = cmd.ExecuteReader();
                while (result.Read())
                {
                    bool anyNull = false;
                    // user ids could be null if group does not have any members.
                    for (int i = 0; i < 3; i++)
                    {
                        if (result.IsDBNull(i))
                        {
                            Global.systemLog.Warn("Null value for " + result.GetName(i));
                            anyNull = true;
                            break;
                        }
                    }
                    if (anyNull)
                    {
                        continue;
                    }

                    Guid userID = Guid.Empty;
                    string userName = null;
                    if (!result.IsDBNull(3))
                    {
                        userID = result.GetGuid(3);
                        userName = result.GetString(4);
                    }
                    if (spg == null)
                    {
                        spg = new SpaceGroup();
                        spg.ID = result.GetGuid(0);
                        spg.Name = result.GetString(1);
                        spg.InternalGroup = result.GetBoolean(2);
                        spg.GroupUsers = new List<User>();
                        if (userID != Guid.Empty)
                        {
                            User u = new User();
                            u.ID = userID;
                            u.Username = userName;
                            spg.GroupUsers.Add(u);
                        }
                        spg.ACLIds = new List<int>();
                    }
                    else
                    {
                        if (userID != Guid.Empty)
                        {
                            User u = new User();
                            u.ID = userID;
                            u.Username = userName;
                            spg.GroupUsers.Add(u);
                        }
                    }
                }
                result.Close();
                cmd.Dispose();

                // get acls 
                if (spg != null)
                {
                    spg.ACLIds = Database.getGroupACLs(conn, schema, spg.ID);
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Exception while retreivng spaceGroup details for group  " + groupName, ex);
                try
                {
                    if (result != null)
                    {
                        result.Close();
                    }

                    if (cmd != null)
                    {
                        cmd.Dispose();
                    }

                }
                catch (Exception)
                {
                    //Global.systemLog.Warn("Cannot close cmd/reader object. Let system handle", ex2);
                }

                throw ex;
            }

            return spg;

        }

        public static List<SpaceGroup> getSpaceGroups(QueryConnection conn, string schema, Space sp)
        {
            List<SpaceGroup> spaceGroups = new List<SpaceGroup>();
            Dictionary<Guid, SpaceGroup> spaceGroupDictonary = new Dictionary<Guid, SpaceGroup>();
            QueryCommand cmd = null;
            QueryReader result = null;
            try
            {
                // FULL JOIN Since Usernames could be null
                cmd = conn.CreateCommand();
                cmd.CommandText = "SELECT A.GROUP_ID,A.GroupName, A.InternalGroup, B.USER_ID, C.Username,A.CreatedDate FROM" +
                    SINGLE_SPACE + getTableName(schema, SPACE_GROUP_TABLE) + SINGLE_SPACE + "A LEFT OUTER JOIN" +
                    SINGLE_SPACE + getTableName(schema, GROUP_USER_TABLE) + SINGLE_SPACE +
                    "B ON A.GROUP_ID = B.GROUP_ID" + SINGLE_SPACE + "LEFT OUTER JOIN" +
                    SINGLE_SPACE + getTableName(schema, USER_TABLE_NAME) + SINGLE_SPACE +
                    "C ON B.USER_ID = C.PKID" + SINGLE_SPACE +
                    "WHERE  A.SPACE_ID =? GROUP BY A.GROUP_ID,A.GroupName, A.InternalGroup, B.USER_ID, C.Username, A.CreatedDate" + SINGLE_SPACE +
                    "ORDER BY A.CreatedDate asc";
                cmd.Parameters.Add("@SPACE_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(sp.ID);
                result = cmd.ExecuteReader();
                while (result.Read())
                {
                    bool anyNull = false;
                    // User id could be null. Since a group might not have any user members.
                    for (int i = 0; i < 3; i++)
                    {
                        if (result.IsDBNull(i))
                        {
                            Global.systemLog.Warn("Null value for " + result.GetName(i));
                            anyNull = true;
                            break;
                        }
                    }
                    if (anyNull)
                    {
                        continue;
                    }

                    Guid gpID = result.GetGuid(0);
                    Guid userId = Guid.Empty;
                    string userName = null;
                    if (!result.IsDBNull(3))
                    {
                        userId = result.GetGuid(3);
                    }

                    if (!result.IsDBNull(4))
                    {
                        userName = result.GetString(4);
                    }


                    if (!spaceGroupDictonary.ContainsKey(gpID))
                    {
                        SpaceGroup spGroup = new SpaceGroup();
                        spGroup.ID = gpID;
                        spGroup.Name = result.GetString(1);
                        spGroup.InternalGroup = result.GetBoolean(2);
                        spGroup.GroupUsers = new List<User>();
                        if (userId != Guid.Empty && userName != null && userName.Length > 0)
                        {
                            User u = new User();
                            u.ID = userId;
                            u.Username = userName;
                            spGroup.GroupUsers.Add(u);
                        }
                        spaceGroupDictonary.Add(gpID, spGroup);
                    }
                    else
                    {
                        SpaceGroup existingSpaceGroup = spaceGroupDictonary[gpID];
                        List<User> groupUsers = existingSpaceGroup.GroupUsers;
                        if (userId != Guid.Empty && userName != null && userName.Length > 0)
                        {
                            User u = new User();
                            u.ID = userId;
                            u.Username = userName;
                            groupUsers.Add(u);
                        }
                    }
                }
            }
            finally
            {
                if (result != null)
                    result.Close();
                if (cmd != null)
                    cmd.Dispose();
            }

            // GET MORE INFO ABOUT GROUP - acls
            // XXX could fold this into the query above, but this is cleaner for now
            int count = spaceGroupDictonary.Keys.Count;
            if (count > 0)
            {

                try
                {
                    // build the query and set the parameters
                    StringBuilder sb = new StringBuilder("SELECT GROUP_ID, ACL_ID FROM ");
                    sb.Append(getTableName(schema, GROUP_ACL_TABLE));
                    sb.Append(" WHERE GROUP_ID IN (");
                    sb.Append("SELECT DISTINCT A.GROUP_ID FROM ");
                    sb.Append(SINGLE_SPACE + getTableName(schema, SPACE_GROUP_TABLE) + SINGLE_SPACE + "A ");
                    sb.Append("WHERE  A.SPACE_ID =? ");
                    sb.Append(SINGLE_SPACE + " AND A.GROUP_ID IS NOT NULL AND A.GroupName IS NOT NULL AND  A.InternalGroup IS NOT NULL) ");

                    cmd = conn.CreateCommand();

                    cmd.Parameters.Add("@SPACE_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(sp.ID);

                    cmd.CommandText = sb.ToString();
                    result = cmd.ExecuteReader();
                    while (result.Read())
                    {
                        bool anyNull = false;
                        for (int i = 0; i < 2; i++)
                        {
                            if (result.IsDBNull(i))
                            {
                                Global.systemLog.Warn("Null value for " + result.GetName(i));
                                anyNull = true;
                                break;
                            }
                        }
                        if (anyNull)
                        {
                            continue;
                        }

                        Guid groupID = result.GetGuid(0);
                        int aclId = result.GetInt32(1);
                        if (!spaceGroupDictonary.ContainsKey(groupID))
                            continue;
                        SpaceGroup gp = spaceGroupDictonary[groupID];
                        if (gp.ACLIds == null)
                        {
                            gp.ACLIds = new List<int>();
                        }
                        gp.ACLIds.Add(aclId);
                    }
                    result.Close();
                }
                finally
                {
                    if (result != null)
                        result.Close();
                    if (cmd != null)
                        cmd.Dispose();
                }
            }
            foreach (SpaceGroup spg in spaceGroupDictonary.Values)
            {
                spaceGroups.Add(spg);
            }
            return spaceGroups;
        }

        // Had to club all the mappings in one method because we would want to roll back the mapping transaction if 
        // something goes wrong. Send an email to ops regarding the same.

        public static void mapAndCreateUserGroupMappings(QueryConnection conn, string schema, Space sp, Performance_Optimizer_Administration.MainAdminForm maf)
        {
            List<Performance_Optimizer_Administration.Group> groupList = maf.usersAndGroups.groupsList;
            List<Performance_Optimizer_Administration.ACLItem> groupaccessList = new List<Performance_Optimizer_Administration.ACLItem>(maf.usersAndGroups.accessList);

            QueryCommand cmd = null;
            OdbcTransaction transaction = null;
            try
            {
                cmd = conn.CreateCommand();
                transaction = conn.BeginTransaction();
                cmd.Transaction = transaction;
                Dictionary<string, string[][]> mappingResults = new Dictionary<string, string[][]>();

                foreach (Performance_Optimizer_Administration.Group group in groupList)
                {
                    // Add group to the space

                    List<string> addedGroupACLs = new List<string>();
                    List<string> addedGroupUsers = new List<string>();

                    cmd.CommandText = "";
                    cmd.Parameters.Clear();

                    Guid groupID = Guid.NewGuid();

                    cmd.CommandText = getAddGroupToSpaceQuery(conn, schema);
                    cmd.Parameters.Add("@SPACE_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(sp.ID);
                    cmd.Parameters.Add("@GROUP_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(groupID);
                    cmd.Parameters.Add("@GroupName", OdbcType.NVarChar).Value = group.Name;
                    cmd.Parameters.Add("@InternalGroup", conn.getODBCBooleanType()).Value = conn.getBooleanValue(group.InternalGroup);

                    int groupAdded = cmd.ExecuteNonQuery();
                    if (!(groupAdded > 0))
                    {
                        Global.systemLog.Warn("Unable to create group " + group.Name);
                        continue;
                    }

                    // Add ACLs to the group
                    cmd.CommandText = "";
                    cmd.Parameters.Clear();
                    foreach (Performance_Optimizer_Administration.ACLItem groupACL in groupaccessList)
                    {
                        cmd.CommandText = "";
                        cmd.Parameters.Clear();
                        if (groupACL.Group != group.Name)
                        {
                            continue;
                        }

                        if (!groupACL.Access)
                        {
                            // We are skipping any false flags since we are only storing true tag in the database.
                            //Global.systemLog.Debug("ACLItem is " + groupACL.Access + " for acl " + groupACL.Tag + " and group " + group.Name);
                            continue;
                        }

                        int aclID = ACLDetails.getACLID(groupACL.Tag);
                        //Global.systemLog.Debug("ACL tag is " + groupACL.Tag);
                        //Global.systemLog.Debug("ACL id is " + aclID);

                        if (aclID <= 0)
                        {
                            Global.systemLog.Warn("Unable to get the acl id for tag " + groupACL.Tag + " Skipping the population of " +
                                group.Name + " for space " + sp.ID + " : " + sp.Name);
                            continue;
                        }

                        cmd.CommandText = getAddACLToGroupQuery(conn, schema);
                        cmd.Parameters.Add("@GROUP_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(groupID);
                        cmd.Parameters.Add("@ACL_ID", OdbcType.Int).Value = aclID;
                        cmd.ExecuteNonQuery();
                        addedGroupACLs.Add(groupACL.Tag);
                    }

                    cmd.CommandText = "";
                    cmd.Parameters.Clear();

                    // Add user to the group
                    if (group.Usernames != null && group.Usernames.Length > 0)
                    {
                        List<string> groupUserNames = new List<string>(group.Usernames);
                        foreach (string userName in groupUserNames)
                        {
                            // Making sure users are not duplicated
                            if (addedGroupUsers.Contains(userName))
                            {
                                continue;
                            }
                            cmd.CommandText = "";
                            cmd.Parameters.Clear();
                            User groupUser = Database.getUserByUsername(userName);
                            if (groupUser == null)
                            {
                                Global.systemLog.Debug("Userid not found for user = " + userName + " . Skipping group-user mapping for  group = " + group.Name + " for space " + sp.ID);
                                continue;
                            }

                            cmd.CommandText = getAddUserToGroupQuery(conn, schema);
                            cmd.Parameters.Add("@GROUP_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(groupID);
                            cmd.Parameters.Add("@USER_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(groupUser.ID);
                            cmd.ExecuteNonQuery();
                            addedGroupUsers.Add(userName);
                        }
                    }

                    string[][] addedGroupInfo = new string[2][];
                    addedGroupInfo[0] = addedGroupUsers.ToArray();
                    addedGroupInfo[1] = addedGroupACLs.ToArray();

                    mappingResults.Add(group.Name, addedGroupInfo);
                }

                transaction.Commit();

                foreach (KeyValuePair<string, string[][]> kvPair in mappingResults)
                {
                    string gName = kvPair.Key;
                    string[][] results = kvPair.Value;
                    string displayUnames = "";
                    string displayAcls = "";
                    foreach (string displayUname in results[0])
                    {
                        displayUnames = displayUnames + displayUname + " ,";
                    }

                    foreach (string displayAcl in results[1])
                    {
                        displayAcls = displayAcls + displayAcl + " ,";
                    }

                    if (displayUnames.Length > 0)
                    {
                        displayUnames = displayUnames.Substring(0, displayUnames.Length - 1);
                    }
                    if (displayAcls.Length > 0)
                    {
                        displayAcls = displayAcls.Substring(0, displayAcls.Length - 1);
                    }
                    Global.systemLog.Debug("Group Mapping added : groupName " + gName + "; usersAdded = " + displayUnames + "; ACLs added = " + displayAcls);
                }
            }
            catch (Exception ex)
            {
                // Exception raised while adding space-group-user mapping. Rollback the entire transaction and send email to 
                Global.systemLog.Error("Error while creating Space-Group-UserBack mapping. Rolling back the transaction ", ex);

                try
                {
                    if (transaction != null)
                    {
                        transaction.Rollback();
                    }
                }
                catch (Exception ex2)
                {
                    Global.systemLog.Error("Error while rolling transaction for user-group-mapping", ex2);
                }
            }
            finally
            {
                if (cmd != null)
                    cmd.Dispose();
            }

            // Spit out a UsersAndGroups.xml file
            sp.SpaceGroups = Database.getSpaceGroups(conn, schema, sp);
            Database.updateMappingModifiedDate(conn, schema, sp);
            UserAndGroupUtils.saveUserAndGroupToFile(sp);
        }

        // Get user by email (used only in one place in Database.cs)
        private static User getUserByUsername(string userName)
        {
            QueryConnection conn = null;
            try
            {
                conn = ConnectionPool.getConnection();
                String schema = Util.getMainSchema();
                User u = Database.getUser(conn, schema, userName, Guid.Empty, null, true, false);
                return u;
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
        }

        private static void transactUpdateSpaceGroupTableWithSpaceId(QueryConnection conn, QueryCommand cmd, string schema, Guid oldSpaceId, Guid newSpaceId)
        {
            cmd.CommandText = "UPDATE" + SINGLE_SPACE + getTableName(schema, SPACE_GROUP_TABLE) + SINGLE_SPACE +
                "SET SPACE_ID = ? WHERE SPACE_ID = ?";

            cmd.Parameters.Add("@SPACE_ID1", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(oldSpaceId);
            cmd.Parameters.Add("@SPACE_ID2", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(newSpaceId);
            cmd.ExecuteNonQuery();
        }

        /// <summary>
        /// This is used to update the space id on the space_group_inxn table with the new one.
        /// This might be useful for activites like interchanging spaceIds.
        /// </summary>
        /// <param name="conn"></param>
        /// <param name="schema"></param>
        /// <param name="oldSpaceId"></param>
        /// <param name="newSpaceId"></param>
        public static void swapSpaceIdsInSpaceGroupTable(QueryConnection conn, string schema, Guid oldSpaceId, Guid newSpaceId)
        {
            QueryCommand cmd = null;
            OdbcTransaction transaction = null;
            Guid tempGuid = new Guid();
            try
            {
                cmd = conn.CreateCommand();
                transaction = conn.BeginTransaction();
                cmd.Transaction = transaction;

                transactUpdateSpaceGroupTableWithSpaceId(conn, cmd, schema, oldSpaceId, tempGuid);
                transactUpdateSpaceGroupTableWithSpaceId(conn, cmd, schema, tempGuid, newSpaceId);

                transaction.Commit();
            }
            catch (Exception ex)
            {
                try
                {
                    if (transaction != null)
                    {
                        transaction.Rollback();
                    }
                }
                catch (Exception)
                {
                    // Nothing to do.
                }
                throw ex;
            }
            finally
            {
                if (cmd != null)
                    cmd.Dispose();
            }
        }


        // Update space operations in the spaces table
        public static void updateSpaceAvailability(QueryConnection conn, string schema, Space sp, int spaceOpsId)
        {
            QueryCommand cmd = null;
            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = "UPDATE" + SINGLE_SPACE + getTableName(schema, Database.SPACE_TABLE_NAME) + SINGLE_SPACE +
                    "SET Available = ? WHERE ID = ?";
                cmd.Parameters.Add("@Available", OdbcType.Int).Value = spaceOpsId;
                cmd.Parameters.Add("@ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(sp.ID);
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Error while updating space op id for space " + sp.ID, ex);
            }
            finally
            {
                if (cmd != null)
                    cmd.Dispose();
            }
        }

        // Update space operations in the spaces table
        public static bool updateSpaceLoadNumber(QueryConnection conn, string schema, Space sp, int loadNumber)
        {
            QueryCommand cmd = null;
            int result = -1;
            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = "UPDATE" + SINGLE_SPACE + getTableName(schema, Database.SPACE_TABLE_NAME) + SINGLE_SPACE +
                    "SET LoadNumber = ? WHERE ID = ?";
                cmd.Parameters.Add("@LoadNumber", OdbcType.Int).Value = loadNumber;
                cmd.Parameters.Add("@ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(sp.ID);
                result = cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Error while updating space load number for space " + sp.ID, ex);
            }
            finally
            {
                if (cmd != null)
                    cmd.Dispose();
            }
            return result == 1;
        }

        public static int getSpaceAvailabilityStatus(QueryConnection conn, string schema, Space sp)
        {
            return getSpaceAvailabilityStatus(conn, schema, sp.ID);
        }

        public static int getSpaceAvailabilityStatus(QueryConnection conn, string schema, Guid spaceID)
        {
            QueryCommand cmd = null;
            int currentOp = 0;
            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = "SELECT Available FROM" + SINGLE_SPACE + getTableName(schema, Database.SPACE_TABLE_NAME) + SINGLE_SPACE +
                    "WHERE Deleted IS NULL AND ID = ?";
                cmd.Parameters.Add("@ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(spaceID);
                Object obj = cmd.ExecuteScalar();
                if (obj != null && obj != System.DBNull.Value)
                {
                    currentOp = Int32.Parse(obj.ToString());
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Error while getting space available id for space " + spaceID, ex);
            }
            finally
            {
                if (cmd != null)
                    cmd.Dispose();
            }

            return currentOp;
        }


        public static int getSpaceSFDCVersionID(QueryConnection conn, string schema, Space sp)
        {
            return getSpaceSFDCVersionID(conn, schema, sp.ID);
        }

        public static int getSpaceSFDCVersionID(QueryConnection conn, string schema, Guid spaceID)
        {
            QueryCommand cmd = null;
            int currentSfdcVersionID = 0;
            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = "SELECT SFDCVersion FROM" + SINGLE_SPACE + getTableName(schema, Database.SPACE_TABLE_NAME) + SINGLE_SPACE +
                    "WHERE Deleted IS NULL AND ID = ?";
                cmd.Parameters.Add("@ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(spaceID);
                Object obj = cmd.ExecuteScalar();
                if (obj != null && obj != System.DBNull.Value)
                {
                    currentSfdcVersionID = Int32.Parse(obj.ToString());
                }
                cmd.Dispose();
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Error while getting space available id for space " + spaceID, ex);
                try
                {
                    if (cmd != null)
                    {
                        cmd.Dispose();
                    }
                }
                catch (Exception)
                {
                    // Nothing to do.
                }
            }

            return currentSfdcVersionID;
        }

        // Update mapping modified date in the spaces table
        public static void updateMappingModifiedDate(QueryConnection conn, string schema, Space sp)
        {
            QueryCommand cmd = null;
            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = "UPDATE" + SINGLE_SPACE + getTableName(schema, Database.SPACE_TABLE_NAME) + SINGLE_SPACE +
                    "SET MappingModifiedDate = " + conn.getCurrentTimestamp() + " WHERE ID = ?";
                cmd.Parameters.Add("@ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(sp.ID);
                cmd.ExecuteNonQuery();
                cmd.Dispose();
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Error while updating MappingModifiedDate for space " + sp.ID, ex);
                try
                {
                    if (cmd != null)
                    {
                        cmd.Dispose();
                    }
                }
                catch (Exception)
                {
                    // Nothing to do.
                }
            }
        }

        public static DateTime getMappingModifiedDate(QueryConnection conn, string schema, Space sp)
        {
            QueryCommand cmd = null;
            DateTime dateTime = DateTime.MinValue;
            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = "SELECT MappingModifiedDate FROM" + SINGLE_SPACE + getTableName(schema, Database.SPACE_TABLE_NAME) + SINGLE_SPACE +
                    "WHERE ID = ?";
                cmd.Parameters.Add("@ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(sp.ID);
                Object obj = cmd.ExecuteScalar();
                if (obj != null && obj != System.DBNull.Value)
                {
                    dateTime = (DateTime)obj;
                }
                cmd.Dispose();
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Error while getting MappingModifiedDate for space " + sp.ID, ex);
                try
                {
                    if (cmd != null)
                    {
                        cmd.Dispose();
                    }
                }
                catch (Exception)
                {
                    // Nothing to do.
                }
            }

            return dateTime;
        }

        public static int getCurrentLoginsForAccount(QueryConnection conn, string schema, User u)
        {

            int result = -1;
            Guid adminAccountID = u.AdminAccountId;
            Guid managedAccountID = u.ManagedAccountId;

            if (adminAccountID != null && adminAccountID.ToString() == Guid.Empty.ToString())
            {
                return result;
            }

            if (managedAccountID == null || (managedAccountID != null && managedAccountID.ToString() == Guid.Empty.ToString()))
            {
                // If there is no way to find Account id, we will log warning and do not show any warning
                Global.systemLog.Warn("No account seemed to be setup for the user " + u.ID + " : " + u.Username);
                return result;
            }

            // get the account id first and see if there are any users for that account
            QueryCommand cmd = null;
            // Account ID to use when need to find other users logged in
            Guid accountID = u.ManagedAccountId;

            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = "SELECT COUNT(*) FROM" + SINGLE_SPACE + getTableName(schema, CURRENT_LOGINS_TABLE) + SINGLE_SPACE +
                    "A INNER JOIN" + SINGLE_SPACE + getTableName(schema, USER_TABLE_NAME) + SINGLE_SPACE +
                    "B ON A.USER_ID = B.PKID AND B.MANAGED_ACCOUNT_ID = ? WHERE A.Logout_Time IS NULL";

                cmd.Parameters.Add("@MANAGED_ACCOUNT_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(accountID);
                result = cmd.ExecuteNonQuery();
                cmd.Dispose();
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Error while looking for concurrent users for account " + accountID, ex);
                try
                {
                    if (cmd != null)
                    {
                        cmd.Dispose();
                    }
                }
                catch (Exception)
                {
                    // Nothing to do
                }
            }

            return result;
        }

        public static void addCurrentLogin(QueryConnection conn, string schema, Guid userID, string userIP,
            DateTime loginTime, string sessionID, string serverName)
        {
            QueryCommand cmd = null;

            if (sessionID == null)
            {
                Global.systemLog.Warn("No session Id to store. Skipping the current Login insert for " + userID + " : " + " : " + userIP);
                return;
            }

            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = "INSERT INTO" + SINGLE_SPACE + getTableName(schema, CURRENT_LOGINS_TABLE) + SINGLE_SPACE +
                    "(USER_ID, Remote_IP, Login_Time, Session_ID, Host_Server) VALUES (?,?,?,?,?)";

                cmd.Parameters.Add("@USER_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(userID);
                cmd.Parameters.Add("@Remote_IP", OdbcType.VarChar).Value = userIP;
                loginTime = loginTime.AddMilliseconds(-loginTime.Millisecond);
                cmd.Parameters.Add("@Login_time", OdbcType.DateTime).Value = loginTime;
                // no population of logout time, letting default to null
                cmd.Parameters.Add("@Session_ID", OdbcType.VarChar).Value = sessionID;
                cmd.Parameters.Add("@Host_Server", OdbcType.VarChar).Value = serverName;
                cmd.ExecuteNonQuery();
                cmd.Dispose();
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Skipping the addition to " + CURRENT_LOGINS_TABLE + " for user " + userID + " : " + " : " + userIP, ex);
                try
                {
                    if (cmd != null)
                    {
                        cmd.Dispose();
                    }
                }
                catch (Exception)
                {
                    // Nothing to do
                }
            }
        }

        /*
         * do not delete, just set logout_time, for tracking purposes
         */
        public static void removeCurrentLogin(QueryConnection conn, string schema, Guid userID, string sessionID)
        {
            QueryCommand cmd = null;

            if (sessionID == null)
            {
                Global.systemLog.Warn("No session Id to store. Skipping the current logout update for " + userID);
                return;
            }

            try
            {
                cmd = conn.CreateCommand();
                /*
                cmd.CommandText = "DELETE" + SINGLE_SPACE + getTableName(schema, CURRENT_LOGINS_TABLE) + SINGLE_SPACE +
                    "WHERE [USER_ID] = ? AND [Session_ID] = ?";
                */
                cmd.CommandText = "UPDATE" + SINGLE_SPACE + getTableName(schema, CURRENT_LOGINS_TABLE) + SINGLE_SPACE +
                    "SET Logout_Time=? WHERE USER_ID = ? AND Session_ID = ?";
                cmd.Parameters.Add("@Logout_Time", OdbcType.DateTime).Value = truncateDateTime(DateTime.Now);
                cmd.Parameters.Add("@USER_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(userID);
                cmd.Parameters.Add("@Session_ID", OdbcType.VarChar).Value = sessionID;
                cmd.ExecuteNonQuery();
                cmd.Dispose();
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Skipping the logout update from " + CURRENT_LOGINS_TABLE + " for user " + userID + " : " + " : " + sessionID, ex);
                try
                {
                    if (cmd != null)
                    {
                        cmd.Dispose();
                    }
                }
                catch (Exception)
                {
                    // Nothing to do
                }
            }
        }

        public static void removeCurrentLoginsForServer(QueryConnection conn, string schema, string serverName)
        {
            QueryCommand cmd = null;

            try
            {
                cmd = conn.CreateCommand();
                /*
                cmd.CommandText = "DELETE" + SINGLE_SPACE + getTableName(schema, CURRENT_LOGINS_TABLE) + SINGLE_SPACE +
                    "WHERE [Host_Server] = ?";
                */
                cmd.CommandText = "UPDATE" + SINGLE_SPACE + getTableName(schema, CURRENT_LOGINS_TABLE) + SINGLE_SPACE +
                    "SET Logout_Time =? WHERE Host_Server = ? AND Logout_Time IS NULL";
                DateTime dtnow = DateTime.Now;
                dtnow = dtnow.AddMilliseconds(-dtnow.Millisecond);
                cmd.Parameters.Add("@Logout_Time", OdbcType.DateTime).Value = dtnow;
                cmd.Parameters.Add("@Host_Server", OdbcType.VarChar).Value = serverName;
                int result = (int)cmd.ExecuteNonQuery();
                cmd.Dispose();
                Global.systemLog.Info("Updated " + result + " " + CURRENT_LOGINS_TABLE + " records for " + serverName);
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Error while deleting login records from " + CURRENT_LOGINS_TABLE + " for server " + serverName, ex);
                try
                {
                    if (cmd != null)
                    {
                        cmd.Dispose();
                    }
                }
                catch (Exception)
                {
                    // Nothing to do
                }
            }
        }

        public static AccountDetails getAccountDetails(QueryConnection conn, string schema, Guid accountID, User u)
        {
            if (accountID == null || (accountID != null && accountID.ToString() == Guid.Empty.ToString()))
            {
                return null;
            }

            AccountDetails actDetails = null;
            QueryCommand cmd = null;
            QueryReader reader = null;
            string Locale = "";
            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = "SELECT Name, MaxConcurrentLogins, MaxUsers, ExpirationDate, IsEnterprise, Disabled FROM" +
                    SINGLE_SPACE + getTableName(schema, ACCOUNTS_TABLE) + SINGLE_SPACE +
                    "WHERE ACCOUNT_ID = ?";

                cmd.Parameters.Add("@ACCOUNT_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(accountID);
                reader = cmd.ExecuteReader();
                if (!reader.HasRows)
                {
                    return null;
                }

                reader.Read();

                actDetails = new AccountDetails();
                actDetails.ID = accountID;
                if (!reader.IsDBNull(0))
                {
                    actDetails.Name = reader.GetString(0);
                }

                if (!reader.IsDBNull(1))
                {
                    actDetails.MaxConcurrentLogins = reader.GetInt32(1);
                }

                if (!reader.IsDBNull(2))
                {
                    actDetails.MaxUsers = reader.GetInt32(2);
                }

                if (!reader.IsDBNull(3))
                {
                    actDetails.ExpirationDate = reader.GetDateTime(3);
                }

                if (!reader.IsDBNull(4))
                {
                    actDetails.IsEnterprise = reader.GetBoolean(4);
                }

                if (!reader.IsDBNull(5))
                {
                    actDetails.Disabled = reader.GetBoolean(5);
                }

                Locale = FlexModule.GetUserLocale(u);
                actDetails.OverrideParameters = getOverrideParametersWithLocale(conn, schema, accountID, Locale);
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Error while getting account details for accountId " + accountID, ex);
            }
            finally
            {
                try
                {
                    if (reader != null)
                        reader.Close();
                    if (cmd != null)
                        cmd.Dispose();
                }
                catch (Exception)
                {
                    //Global.systemLog.Warn("Cannot close cmd/result object. Let system handle", ex2);
                }
            }
            return actDetails;
        }


        public static List<UserSummary> getManagedUsers(QueryConnection conn, string schema, User u, string searchString, int limitUserResults)
        {
            return getManagedUsers(conn, schema, u, searchString, Guid.Empty, limitUserResults);
        }

        public static List<UserSummary> getManagedUsers(QueryConnection conn, string schema, User u, string searchString,
            Guid spaceID, int limitUserResults)
        {
            if (u.AdminAccountId == null || u.AdminAccountId == Guid.Empty)
            {
                Global.systemLog.Warn("No Associated Account id found . Cannot get ManageUsers for User  : " + u.ID + " : " + u.Username);
            }
            List<UserSummary> userSummaryList = null;
            QueryCommand cmd = null;
            QueryReader reader = null;
            // Map of userid to enable flag.
            // Basically, we go over all the product and end date and depending on the expiration date we set the flag
            // By ORing over all products            
            Dictionary<string, UserSummary> userSummaryDict = new Dictionary<string, UserSummary>();
            try
            {
                cmd = conn.CreateCommand();
                String command = "SELECT A.PKID, A.ADMIN_ACCOUNT_ID, A.Username, A.Email, A.FirstName, A.LastName, A.IsLockedOut, A.LastLoginDate, A.Disabled, A.ReleaseType, A.HTMLInterface, A.ExternalDBEnabled, A.DenyAddSpace FROM " +
                    getTableName(schema, USER_TABLE_NAME) + " A ";
                if (spaceID != null && spaceID != Guid.Empty)
                {
                    command = command +
                        "INNER JOIN " + SINGLE_SPACE + getTableName(schema, USER_SPACE_INXN) + SINGLE_SPACE +
                        "C ON C.USER_ID = A.PKID AND C.SPACE_ID = ?" + SINGLE_SPACE;
                    cmd.Parameters.Add("@SPACE_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(spaceID);
                }

                command = command + "WHERE A.MANAGED_ACCOUNT_ID = ?";
                cmd.Parameters.Add("@MANAGED_ACCOUNT_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(u.AdminAccountId);
                if (searchString != null && searchString.Length > 0)
                {
                    command = command + SINGLE_SPACE + "AND A.Username LIKE ?";
                    cmd.Parameters.Add("@Username", OdbcType.NVarChar).Value = "%" + searchString + "%";
                }

                command = command + SINGLE_SPACE + "GROUP BY  A.PKID, ADMIN_ACCOUNT_ID, A.Username, A.Email, A.FirstName, A.LastName, " +
                    "A.IsLockedOut, A.LastLoginDate, A.Disabled, A.ReleaseType, A.HTMLInterface, A.ExternalDBEnabled, A.DenyAddSpace ORDER BY A.PKID";

                cmd.CommandText = command;
                reader = cmd.ExecuteReader();
                // since its order by, we iterate till we reach our limit results and then quit
                while (reader.Read())
                {
                    if (reader.IsDBNull(0) || reader.IsDBNull(2) || reader.IsDBNull(3))
                    {
                        continue;
                    }

                    // Check if we have already reached the limit before creating a new entry
                    if (limitUserResults > 0 && userSummaryDict != null && userSummaryDict.Count == limitUserResults)
                    {
                        Global.systemLog.Warn(limitUserResults + " Limit reached for number of users returned");
                        break;
                    }
                    UserSummary uSummary = new UserSummary();
                    uSummary.UserID = reader.GetGuid(0).ToString();
                    if (!reader.IsDBNull(1))
                    {
                        Guid adminAccountID = reader.GetGuid(1);
                        uSummary.AccountAdmin = u.AdminAccountId.Equals(adminAccountID);
                    }
                    uSummary.Username = reader.GetString(2);
                    uSummary.Email = reader.GetString(3);
                    if (!reader.IsDBNull(4))
                    {
                        uSummary.FirstName = reader.GetString(4);
                    }
                    if (!reader.IsDBNull(5))
                    {
                        uSummary.LastName = reader.GetString(5);
                    }
                    uSummary.Locked = reader.GetBoolean(6);
                    if (!reader.IsDBNull(7))
                    {
                        uSummary.LastLoginDate = reader.GetDateTime(7).ToString();
                    }
                    if (!reader.IsDBNull(8))
                    {
                        uSummary.Enabled = !reader.GetBoolean(8);
                    }
                    else
                    {
                        uSummary.Enabled = true;
                    }
                    if (!reader.IsDBNull(9))
                    {
                        uSummary.ReleaseType = reader.GetInt32(9);
                    }
                    else
                    {
                        uSummary.ReleaseType = -1;
                    }

                    if (!reader.IsDBNull(10))
                    {
                        uSummary.HtmlUser = reader.GetBoolean(10);
                    }
                    else
                    {
                        uSummary.HtmlUser = false;
                    }

                    if (!reader.IsDBNull(11))
                    {
                        uSummary.ExternalDBEnabled = reader.GetBoolean(11);
                    }
                    else
                    {
                        uSummary.ExternalDBEnabled = false;
                    }
                    if (!reader.IsDBNull(12))
                    {
                        uSummary.DenyAddSpace = reader.GetBoolean(12);
                    }
                    else
                    {
                        uSummary.DenyAddSpace = false;
                    }

                    userSummaryDict.Add(uSummary.UserID, uSummary);
                }

                if (userSummaryDict != null && userSummaryDict.Count > 0)
                {
                    userSummaryList = new List<UserSummary>();
                    Dictionary<string, UserSummary>.ValueCollection valueCollection = userSummaryDict.Values;
                    foreach (UserSummary val in valueCollection)
                    {
                        userSummaryList.Add(val);
                    }
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Exception while getting Managed Users for " + u.ID + " : " + u.Username, ex);
                throw ex;
            }
            finally
            {
                try
                {
                    if (reader != null)
                    {
                        reader.Close();
                    }
                    if (cmd != null)
                    {
                        cmd.Dispose();
                    }
                }
                catch (Exception)
                {
                    //
                }
            }

            return userSummaryList;
        }

        /// <summary>
        /// Returns the number of active/inactive account users
        /// int[0] returns the num active users
        /// int[1] returns the num inactive users
        /// </summary>
        /// <param name="conn"></param>
        /// <param name="schema"></param>
        /// <param name="accountId"></param>
        /// <returns></returns>
        public static int[] getNumAccountUsers(QueryConnection conn, string schema, Guid accountId)
        {
            int[] numUsers = new int[] { 0, 0 };
            QueryReader reader = null;
            QueryCommand cmd = null;
            Dictionary<string, bool> userIdToActiveFlagDict = new Dictionary<string, bool>();
            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = "SELECT A.PKID, A.Disabled FROM" + SINGLE_SPACE + getTableName(schema, USER_TABLE_NAME) +
                    " A WHERE A.MANAGED_ACCOUNT_ID = ?" +
                    " GROUP BY A.PKID, A.Disabled";

                cmd.Parameters.Add("@MANAGED_ACCOUNT_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(accountId);
                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    if (reader.IsDBNull(0))
                    {
                        continue;
                    }

                    string userId = reader.GetGuid(0).ToString();
                    bool disabled = (reader.IsDBNull(1) ? false : reader.GetBoolean(1));

                    if (disabled)
                        numUsers[1]++;
                    else
                        numUsers[0]++;
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Exception while getting num Users for accountId" + accountId, ex);
                throw ex;
            }
            finally
            {
                if (reader != null)
                {
                    reader.Close();
                }
                if (cmd != null)
                    cmd.Dispose();
            }
            return numUsers;
        }

        /// <summary>
        /// Update password failed counts. Basically reset all of them to unlock user
        /// </summary>
        /// <param name="conn"></param>
        /// <param name="schema"></param>
        /// <param name="userId"></param>

        public static void unlockUser(QueryConnection conn, string schema, Guid userId)
        {
            QueryCommand cmd = null;
            try
            {
                bool isOracle = conn.isDBTypeOracle();
                cmd = conn.CreateCommand();
                string FailedPasswordAttemptCount = isOracle ? "FailedPwdAttemptCount" : "FailedPasswordAttemptCount";
                string FailedPasswordAttemptWindowStart = isOracle ? "FailedPwdAttemptWindowStart" : "FailedPasswordAttemptWindowStart";
                string FailedPasswordAnswerAttemptCount = isOracle ? "FailedPwdAnsAttemptCount" : "FailedPasswordAnswerAttemptCount";
                string FailedPasswordAnswerAttemptWindowStart = isOracle ? "FailedPwdAnsAttemptWindowStart" : "FailedPasswordAnswerAttemptWindowStart";
                cmd.CommandText = "UPDATE " + SINGLE_SPACE + getTableName(schema, USER_TABLE_NAME) + SINGLE_SPACE +
                    "SET IsLockedOut = ?, LastLockedOutDate = ?," + SINGLE_SPACE +
                    FailedPasswordAttemptCount + "= ?, " + FailedPasswordAttemptWindowStart + "= ?," + SINGLE_SPACE +
                    FailedPasswordAnswerAttemptCount + " = ?, " + FailedPasswordAnswerAttemptWindowStart + "= ? " + SINGLE_SPACE +
                    "WHERE PKID = ?";
                cmd.Parameters.Add("@IsLockedOut", conn.getODBCBooleanType()).Value = conn.getBooleanValue(false);
                cmd.Parameters.Add("@LastLockedOutDate", OdbcType.DateTime).Value = System.DBNull.Value;
                cmd.Parameters.Add("@" + FailedPasswordAttemptCount, OdbcType.Int).Value = 0;
                cmd.Parameters.Add("@" + FailedPasswordAttemptWindowStart, OdbcType.DateTime).Value = System.DBNull.Value;
                cmd.Parameters.Add("@" + FailedPasswordAnswerAttemptCount, OdbcType.Int).Value = 0;
                cmd.Parameters.Add("@" + FailedPasswordAnswerAttemptWindowStart, OdbcType.DateTime).Value = System.DBNull.Value;
                cmd.Parameters.Add("@PKID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(userId);
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Exception while updating user status for " + userId, ex);
                throw ex;
            }
            finally
            {
                try
                {
                    if (cmd != null)
                    {
                        cmd.Dispose();
                    }
                }
                catch (Exception)
                {
                }
            }
        }

        public static void updateUserProductStatus(QueryConnection conn, string schema, User user, bool activeFlag)
        {
            QueryCommand cmd = null;
            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = "UPDATE " + SINGLE_SPACE + getTableName(schema, USER_PRODUCT_TABLE) + SINGLE_SPACE +
                    "SET Active = ? " + SINGLE_SPACE +
                    "WHERE USER_ID = ?";
                cmd.Parameters.Add("@Active", conn.getODBCBooleanType()).Value = conn.getBooleanValue(activeFlag);
                cmd.Parameters.Add("@USER_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(user.ID);
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Exception while updating user status for " + user.Username, ex);
                throw ex;
            }
            finally
            {
                try
                {
                    if (cmd != null)
                    {
                        cmd.Dispose();
                    }
                }
                catch (Exception)
                {
                }
            }

        }

        /**
         * create a new ACCOUNT object
         */
        public static Guid createAccount(QueryConnection conn, string schema, string name)
        {
            Guid accountId = Guid.NewGuid();
            QueryCommand cmd = null;
            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = "INSERT INTO " + getTableName(schema, ACCOUNTS_TABLE) +
                            " (ACCOUNT_ID, Name, CreatedDate, ModifiedDate)" +
                            "Values (?,?," + conn.getCurrentTimestamp() + "," + conn.getCurrentTimestamp() + ")";
                cmd.Parameters.Add("@ACCOUNT_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(accountId);
                cmd.Parameters.Add("@Name", OdbcType.VarChar).Value = name;
                int result = cmd.ExecuteNonQuery();
                return accountId;
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Error while creating accout " + name);
                throw ex;
            }
            finally
            {
                try
                {
                    if (cmd != null)
                        cmd.Dispose();
                }
                catch (Exception)
                {
                    //
                }
            }
        }

        public static void updateAccountAdminStatus(QueryConnection conn, string schema, bool enable, Guid adminAccountId, Guid userId)
        {
            updateAccountAdminStatus(conn, schema, enable, adminAccountId, new string[] { userId.ToString() });
        }

        /// <summary>
        /// Make the given users as account admins. 
        /// The concept of account admin was introduced 4.3 for performing
        /// user management activites.
        /// </summary>
        /// <param name="conn"></param>
        /// <param name="schema"></param>
        /// <param name="userId"></param>
        public static void updateAccountAdminStatus(QueryConnection conn, string schema, bool enable, Guid adminAccountId, string[] usersIds)
        {
            QueryCommand cmd = null;
            try
            {

                cmd = conn.CreateCommand();
                cmd.CommandText = "UPDATE " + SINGLE_SPACE + getTableName(schema, USER_TABLE_NAME) + SINGLE_SPACE +
                    "SET ADMIN_ACCOUNT_ID = ?, MANAGED_ACCOUNT_ID = ?" + SINGLE_SPACE +
                    "WHERE PKID = ?";

                foreach (string userId in usersIds)
                {
                    Guid pkid = new Guid(userId);

                    cmd.Parameters.Clear();
                    if (enable)
                    {
                        cmd.Parameters.Add("@ADMIN_ACCOUNT_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(adminAccountId);
                    }
                    else
                    {
                        cmd.Parameters.Add("@ADMIN_ACCOUNT_ID", conn.getODBCUniqueIdentiferType()).Value = System.DBNull.Value;
                    }

                    cmd.Parameters.Add("@MANAGED_ACCOUNT_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(adminAccountId);
                    cmd.Parameters.Add("@PKID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(pkid);
                    cmd.ExecuteNonQuery();
                    Global.systemLog.Debug("User Administration : user " + userId + " account admin status " + (enable ? "enabled" : "disabled"));
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Exception while updating account admin priveleges for account" + adminAccountId, ex);
                throw ex;
            }
            finally
            {
                try
                {
                    if (cmd != null)
                    {
                        cmd.Dispose();
                    }
                }
                catch (Exception)
                {
                    // Let system release resources
                }
            }
        }

        // return a list of spaceIDs to audit
        public static List<Space> getSpacesToAudit(QueryConnection conn, String schema, Guid accountId)
        {
            QueryCommand cmd = null;
            QueryReader reader = null;

            List<Space> spaceIds = new List<Space>();

            // only users/spaces associated with an account can get usage tracking data
            if (accountId == null || accountId == Guid.Empty)
                return spaceIds;

            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = "select distinct c.space_id, d.name from "
                    + getTableName(schema, ACCOUNTS_TABLE) + " a, "
                    + getTableName(schema, USER_TABLE_NAME) + " b, "
                    + getTableName(schema, USER_SPACE_INXN) + " c, "
                    + getTableName(schema, SPACE_TABLE_NAME) + " d "
                    + " where a.account_id = ? and a.account_id = b.managed_account_id and b.pkid = c.user_id and c.owner = 1 and c.space_id = d.id and d.Deleted IS NULL and d.UsageTracking = 1";
                cmd.Parameters.Add("@account_id", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(accountId);
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    Space sp = new Space();
                    sp.ID = reader.GetGuid(0);
                    sp.Name = reader.GetString(1);
                    spaceIds.Add(sp);
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Exception while searching for spaces in an account", ex);
                throw ex;
            }
            finally
            {
                try
                {
                    if (reader != null)
                        reader.Close();
                    if (cmd != null)
                        cmd.Dispose();
                }
                catch (Exception)
                {
                    // Let system release resources
                }
            }
            return spaceIds;
        }

        // query the audit db, joining with USERS (for userID) and SPACES (for spacename), filtering on spaceIds and fromdate
        public static QueryReader getAuditRecords(QueryConnection conn, string mainSchema, string auditTableName, Guid accountId, DateTime fromDate)
        {
            QueryCommand cmd = null;
            QueryReader reader = null;

            // only users/spaces associated with an account can get usage tracking data
            if (accountId == null)
                return null;

            try
            {
                List<Space> spaces = getSpacesToAudit(conn, mainSchema, accountId);
                if (spaces == null || spaces.Count == 0)
                    return null;

                cmd = conn.CreateCommand();
                cmd.CommandText = "SELECT A.TM, B.PKID, A.USERNAME, A.SPACEID, C.NAME, A.MODE, A.NAME, A.DASHBOARD, A.DASHBOARDGUID, A.SESSIONID, A.ELAPSEDTIME "
                    + "FROM "
                    + auditTableName + " A, "
                    + getTableName(mainSchema, USER_TABLE_NAME) + " B, "
                    + getTableName(mainSchema, SPACE_TABLE_NAME) + " C "
                    + "WHERE C.Deleted IS NULL AND C.UsageTracking = 1 AND A.USERNAME=B.USERNAME AND A.SPACEID=" + (conn.isDBTypeSQLServer() ? "CONVERT(" + conn.getVarcharType(64, false) + ", " : "") + "C.ID" + (conn.isDBTypeSQLServer() ? ")" : "") + " ";
                if (fromDate != null)
                {
                    cmd.CommandText += "AND A.TM >= ? ";
                }

                StringBuilder sb = new StringBuilder();
                sb.Append("AND C.ID IN (");
                sb.Append("SELECT DISTINCT D.SPACE_ID FROM ");
                sb.Append(getTableName(mainSchema, ACCOUNTS_TABLE) + " A, ");
                sb.Append(getTableName(mainSchema, USER_SPACE_INXN) + " D ");
                sb.Append("WHERE A.ACCOUNT_ID = ? AND A.ACCOUNT_ID = B.MANAGED_ACCOUNT_ID AND B.PKID = D.USER_ID AND D.OWNER = 1 AND D.SPACE_ID = C.ID AND C.DELETED IS NULL AND C.USAGETRACKING = 1");                    
                sb.Append(')');
                cmd.CommandText += sb.ToString();

                cmd.CommandText += " ORDER BY A.TM ASC";
                if (fromDate != null)
                    cmd.Parameters.Add("@fromDate", OdbcType.DateTime).Value = fromDate;
                cmd.Parameters.Add("@account_id", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(accountId);
                reader = cmd.ExecuteReader();
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Exception generating usage data", ex);
                throw ex;
            }
            return reader;
        }

        public static int getServerStatus(QueryConnection conn, string mainSchema, string hostaddr, ref string message)
        {
            int res = 0;
            QueryCommand cmd = null;
            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = "SELECT STATUSFLAG,MESSAGE FROM " + mainSchema + "." + SERVER_STATUS_TABLE + " WHERE HOSTADDR=?";
                cmd.Parameters.Add("HOSTADDR", OdbcType.VarChar).Value = hostaddr;
                QueryReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    res = reader.GetInt32(0);
                    message = reader.IsDBNull(1) ? null : reader.GetString(1);
                    break;
                }
            }
            finally
            {
                if (cmd != null)
                    cmd.Dispose();
            }
            return res;
        }

        public static Guid getUserIdForOpenId(QueryConnection conn, string schema, string openId)
        {
            QueryCommand cmd = null;
            QueryReader reader = null;

            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = "SELECT USERID FROM "
                    + getTableName(schema, OPENID_USER_TABLE)
                    + " WHERE OPENID=?";
                cmd.Parameters.Add("@OPENID", OdbcType.VarChar).Value = openId;

                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    return reader.GetGuid(0);
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Exception while looking up OpenId - " + openId, ex);
                throw ex;
            }
            finally
            {
                try
                {
                    if (reader != null)
                        reader.Close();
                    if (cmd != null)
                        cmd.Dispose();
                }
                catch (Exception)
                {
                    // Let system release resources
                }
            }
            return Guid.Empty;
        }

        public static bool isOpenIDUsed(QueryConnection conn, string schema, string openID)
        {
            QueryCommand cmd = null;
            QueryReader reader = null;
            List<string> openIDs = new List<string>();
            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = "SELECT USERID FROM "
                    + getTableName(schema, OPENID_USER_TABLE)
                    + " WHERE OPENID=?";
                cmd.Parameters.Add("@OPENID", OdbcType.VarChar).Value = openID;

                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Exception while looking for OpenID " + openID, ex);
                throw ex;
            }
            finally
            {
                try
                {
                    if (reader != null)
                        reader.Close();
                    if (cmd != null)
                        cmd.Dispose();
                }
                catch (Exception)
                {
                    // Let system release resources
                }
            }
            return false;
        }

        public static bool addOpenIDForUserId(QueryConnection conn, string schema, Guid userId, string openID)
        {
            QueryCommand cmd = null;

            if (isOpenIDUsed(conn, schema, openID))
                return false;

            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = "INSERT INTO "
                    + getTableName(schema, OPENID_USER_TABLE)
                    + " (USERID, OPENID) VALUES (?,?)";
                cmd.Parameters.Add("USERID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(userId);
                cmd.Parameters.Add("OPENID", OdbcType.VarChar).Value = openID;
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Exception while adding OpenID for User - " + openID + "/" + userId.ToString(), ex);
                throw ex;
            }
            finally
            {
                try
                {
                    if (cmd != null)
                        cmd.Dispose();
                }
                catch (Exception)
                {
                    // Let system release resources
                }
            }
            return true;
        }

        public static bool addSamlIdpInfo(QueryConnection conn, string schema, IdpMetaData idpInfo)
        {
            QueryCommand cmd = null;

            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = "INSERT INTO "
                    + getTableName(schema, SAML_IDP_TABLE)
                    + " (ID, IDPNAME, IDPISSUERID, CERTIFICATE, TIMEOUT, LOGOUTPAGE, ERRORPAGE, ACTIVE, IDPURL, SIGNIDPREQUEST, IDPBINDING, SPINITIATED) " +
                    " VALUES (?,?,?,?,?,?,?,?,?,?,?,?)";
                Guid id = Guid.NewGuid();
                cmd.Parameters.Add("@ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(id);
                cmd.Parameters.Add("@IDPNAME", OdbcType.VarChar).Value = idpInfo.idpName;
                cmd.Parameters.Add("@IDPISSUERID", OdbcType.VarChar).Value = idpInfo.idpIssuerID;
                cmd.Parameters.Add("@CERTIFICATE", OdbcType.VarChar).Value = idpInfo.cert;
                cmd.Parameters.Add("@TIMEOUT", OdbcType.Int).Value = idpInfo.timeout <= 0 ? -1 : idpInfo.timeout;
                DbParameter logoutPageParam = cmd.Parameters.Add("LOGOUTPAGE", OdbcType.VarChar);
                if (string.IsNullOrWhiteSpace(idpInfo.logoutPage))
                {
                    logoutPageParam.Value = System.DBNull.Value;
                }
                else
                {
                    logoutPageParam.Value = idpInfo.logoutPage.Trim();
                }

                DbParameter errorPageParam = cmd.Parameters.Add("ERRORPAGE", OdbcType.VarChar);
                if (string.IsNullOrWhiteSpace(idpInfo.errorPage))
                {
                    errorPageParam.Value = System.DBNull.Value;
                }
                else
                {
                    errorPageParam.Value = idpInfo.errorPage.Trim();
                }
                cmd.Parameters.Add("@ACTIVE", conn.getODBCBooleanType()).Value = conn.getBooleanValue(idpInfo.active);
                DbParameter idpUrlParam = cmd.Parameters.Add("@IDPURL", OdbcType.VarChar);
                if (string.IsNullOrWhiteSpace(idpInfo.idpURL))
                {
                    idpUrlParam.Value = System.DBNull.Value;
                }
                else
                {
                    idpUrlParam.Value = idpInfo.idpURL.Trim();
                }
                cmd.Parameters.Add("@SIGNIDPREQUEST", conn.getODBCBooleanType()).Value = conn.getBooleanValue(idpInfo.signIdpRequest);
                DbParameter idpBindingParam = cmd.Parameters.Add("@IDPBINDING", OdbcType.VarChar);
                if (string.IsNullOrWhiteSpace(idpInfo.idpBinding))
                {
                    idpBindingParam.Value = System.DBNull.Value;
                }
                else
                {
                    idpBindingParam.Value = idpInfo.idpBinding.Trim();
                }

                cmd.Parameters.Add("@SPINITIATED", conn.getODBCBooleanType()).Value = conn.getBooleanValue(idpInfo.spInitiated);

                cmd.ExecuteNonQuery();
                idpInfo.ID = id;
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Exception while adding idp info for idp - " + idpInfo.idpName, ex);
                throw ex;
            }
            finally
            {
                try
                {
                    if (cmd != null)
                        cmd.Dispose();
                }
                catch (Exception)
                {
                    // Let system release resources
                }
            }
            return true;
        }

        public static bool updateSamlIdpInfo(QueryConnection conn, string schema, Guid id, IdpMetaData idpInfo)
        {
            QueryCommand cmd = null;

            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = "UPDATE "
                    + getTableName(schema, SAML_IDP_TABLE)
                    + " SET IDPNAME=?, IDPISSUERID=?, CERTIFICATE=?, TIMEOUT=?, LOGOUTPAGE=?, " +
                    "  ERRORPAGE=?, ACTIVE=?, IDPURL=?, SIGNIDPREQUEST=?, IDPBINDING=?, SPINITIATED=? " +
                    " WHERE ID=?";
                cmd.Parameters.Add("@IDPNAME", OdbcType.VarChar).Value = idpInfo.idpName;
                cmd.Parameters.Add("@IDPISSUERID", OdbcType.VarChar).Value = idpInfo.idpIssuerID;
                cmd.Parameters.Add("@CERTIFICATE", OdbcType.VarChar).Value = idpInfo.cert;
                cmd.Parameters.Add("@TIMEOUT", OdbcType.Int).Value = idpInfo.timeout <= 0 ? -1 : idpInfo.timeout;
                DbParameter logoutPageParam = cmd.Parameters.Add("LOGOUTPAGE", OdbcType.VarChar);
                if (string.IsNullOrWhiteSpace(idpInfo.logoutPage))
                {
                    logoutPageParam.Value = System.DBNull.Value;
                }
                else
                {
                    logoutPageParam.Value = idpInfo.logoutPage.Trim();
                }

                DbParameter errorPageParam = cmd.Parameters.Add("ERRORPAGE", OdbcType.VarChar);
                if (string.IsNullOrWhiteSpace(idpInfo.errorPage))
                {
                    errorPageParam.Value = System.DBNull.Value;
                }
                else
                {
                    errorPageParam.Value = idpInfo.errorPage.Trim();
                }
                cmd.Parameters.Add("@ACTIVE", conn.getODBCBooleanType()).Value = conn.getBooleanValue(idpInfo.active);
                DbParameter idpUrlParam = cmd.Parameters.Add("IDPURL", OdbcType.VarChar);
                if (string.IsNullOrWhiteSpace(idpInfo.idpURL))
                {
                    idpUrlParam.Value = System.DBNull.Value;
                }
                else
                {
                    idpUrlParam.Value = idpInfo.idpURL.Trim();
                }
                cmd.Parameters.Add("@SIGNIDPREQUEST", conn.getODBCBooleanType()).Value = conn.getBooleanValue(idpInfo.signIdpRequest);
                DbParameter idpBindingParam = cmd.Parameters.Add("IDPBINDING", OdbcType.VarChar);
                if (string.IsNullOrWhiteSpace(idpInfo.idpBinding))
                {
                    idpBindingParam.Value = System.DBNull.Value;
                }
                else
                {
                    idpBindingParam.Value = idpInfo.idpBinding.Trim();
                }
                cmd.Parameters.Add("@SPINITIATED", conn.getODBCBooleanType()).Value = conn.getBooleanValue(idpInfo.spInitiated);
                cmd.Parameters.Add("@ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(id);

                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Exception while updating idp info for idp - " + idpInfo.idpName, ex);
                throw ex;
            }
            finally
            {
                try
                {
                    if (cmd != null)
                        cmd.Dispose();
                }
                catch (Exception)
                {
                    // Let system release resources
                }
            }
            return true;
        }

        public static void removeOpenIDForUserId(QueryConnection conn, string schema, Guid userId, string openID)
        {
            QueryCommand cmd = null;

            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = "DELETE FROM "
                    + getTableName(schema, OPENID_USER_TABLE)
                    + " WHERE OPENID=? AND USERID=?";
                cmd.Parameters.Add("@OPENID", OdbcType.VarChar).Value = openID;
                cmd.Parameters.Add("@USERID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(userId);
                int res = cmd.ExecuteNonQuery();
                if (res == 0)
                {
                    throw new Exception("OpenID does not exist");
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Exception while removing OpenID for User - " + openID + "/" + userId.ToString(), ex);
                throw ex;
            }
            finally
            {
                try
                {
                    if (cmd != null)
                        cmd.Dispose();
                }
                catch (Exception)
                {
                    // Let system release resources
                }
            }
            return;
        }

        public static List<string> getOpenIDsForUserId(QueryConnection conn, string schema, Guid userId)
        {
            QueryCommand cmd = null;
            QueryReader reader = null;
            List<string> openIDs = new List<string>();

            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = "SELECT OPENID FROM "
                    + getTableName(schema, OPENID_USER_TABLE)
                    + " WHERE USERID=?";
                cmd.Parameters.Add("@USERID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(userId);

                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    openIDs.Add(reader.GetString(0));
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Exception while getting OpenIDs for user " + userId.ToString(), ex);
                throw ex;
            }
            finally
            {
                try
                {
                    if (reader != null)
                        reader.Close();
                    if (cmd != null)
                        cmd.Dispose();
                }
                catch (Exception)
                {
                    // Let system release resources
                }
            }
            return openIDs;
        }

        public static SubDomainInfo getSubDomainInfo(QueryConnection conn, string schema, string subDomain)
        {
            QueryCommand cmd = null;
            QueryReader reader = null;

            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = "SELECT ID, SAMLIDPNAME, AVAILABLE FROM "
                    + getTableName(schema, SUB_DOMAIN_REGISTER_TABLE)
                    + " WHERE DOMAIN=?";
                cmd.Parameters.Add("DOMAIN", OdbcType.VarChar).Value = subDomain;
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    SubDomainInfo subDomainInfo = new SubDomainInfo();
                    subDomainInfo.SubDomain = subDomain;
                    subDomainInfo.ID = reader.GetGuid(0);
                    subDomainInfo.SamlIdpName = !reader.IsDBNull(1) ? reader.GetString(1) : null;
                    subDomainInfo.Active = reader.IsDBNull(2) ? false : reader.GetBoolean(2);
                    return subDomainInfo;
                }
                return null;
            }
            finally
            {
                try
                {
                    if (reader != null)
                        reader.Close();
                    if (cmd != null)
                        cmd.Dispose();
                }
                catch (Exception)
                {
                    // Let system release resources
                }
            }
        }

        public static bool isValidAccount(QueryConnection conn, string schema, Guid accountId)
        {
            QueryCommand cmd = null;
            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = "SELECT COUNT(*) FROM " + getTableName(schema, ACCOUNTS_TABLE)
                    + " WHERE ACCOUNT_ID=?";
                cmd.Parameters.Add("@ACCOUNTID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(accountId);
                object obj = cmd.ExecuteScalar();
                int value = -1;
                if (obj != null)
                {
                    if (obj is int)
                    {
                        value = (int)obj;
                    }
                    else
                    {
                        Int32.TryParse(value.ToString(), out value);
                    }
                    
                }
                return value > 0;
            }
            finally
            {
                closeResources(null, cmd, true);
            }
        }

        public static void addSamlAccountMapping(QueryConnection conn, string schema, IdpMetaData idpInfo, HashSet<Guid> accountIds)
        {
            QueryCommand cmd = null;
            OdbcTransaction transaction = null;
            try
            {
                if (accountIds != null && accountIds.Count > 0)
                {
                    transaction = conn.BeginTransaction();
                    cmd = conn.CreateCommand();
                    cmd.Transaction = transaction;
                    cmd.CommandText = "INSERT INTO " + getTableName(schema, SAML_ACCOUNT_MAPPING) 
                        + "(ID, SAMLID, ACCOUNTID, CreatedDate)"
                        + " VALUES(?,?,?, " + conn.getCurrentTimestamp() + ")";
                    foreach (Guid accountId in accountIds)
                    {
                        cmd.Parameters.Clear();
                        cmd.Parameters.Add("@ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(Guid.NewGuid());
                        cmd.Parameters.Add("@SAMLID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(idpInfo.ID);
                        cmd.Parameters.Add("@ACCOUNTID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(accountId);
                        cmd.ExecuteScalar();
                    }

                    transaction.Commit();
                }
            }
            catch (Exception ex)
            {
                if (transaction != null)
                {
                    try
                    {
                        transaction.Rollback();
                    }
                    catch (Exception ex2)
                    {
                        Global.systemLog.Warn("Unable to rollback transaction ", ex2);
                    }
                }
                throw ex;
            }
            finally
            {
                closeResources(null, cmd, true);
            }
        }

        public static List<Guid> getSamlAccountMappings(QueryConnection conn, string schema, Guid idpID)
        {
            QueryCommand cmd = null;
            QueryReader reader = null;
            List<Guid> accountIds = null;
            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = "SELECT ACCOUNTID FROM " + getTableName(schema, SAML_ACCOUNT_MAPPING)
                    + " WHERE SAMLID=?";
                cmd.Parameters.Add("SAMLID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(idpID);
                reader = cmd.ExecuteReader();
                if (reader != null && reader.HasRows)
                {
                    accountIds = new List<Guid>();
                    while (reader.Read())
                    {
                        accountIds.Add(reader.GetGuid(0));
                    }
                }
                return accountIds;
            }
            finally
            {
                closeResources(reader, cmd, true);                
            }
        }

        private static void closeResources(QueryReader reader, QueryCommand cmd, bool logErrorAndContinue)
        {
            try
            {
                if (reader != null)
                {
                    reader.Close();
                }
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Warn(" Error while cleaning db resources ", ex);
                if (!logErrorAndContinue)
                {
                    throw ex;
                }
            }
        }

        public static List<IdpMetaData> getSamlInfoByIssuerID(QueryConnection conn, string schema, String idpIssuerID)
        {
            QueryCommand cmd = null;
            QueryReader reader = null;
            List<IdpMetaData> info = new List<IdpMetaData>();

            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = "SELECT ID, IDPNAME, CERTIFICATE, TIMEOUT, LOGOUTPAGE, ERRORPAGE, " +
                    " ACTIVE, IDPURL, SIGNIDPREQUEST, IDPBINDING, SPINITIATED, ACCOUNTID FROM "
                    + getTableName(schema, SAML_IDP_TABLE)
                    + " WHERE IDPISSUERID=?";
                cmd.Parameters.Add("IDPISSUERID", OdbcType.VarChar).Value = idpIssuerID;
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    IdpMetaData idpmd = new IdpMetaData();
                    idpmd.ID = reader.GetGuid(0);
                    idpmd.idpIssuerID = idpIssuerID;
                    idpmd.idpName = reader.GetString(1);
                    idpmd.cert = !reader.IsDBNull(2) ? reader.GetString(2) : null;
                    idpmd.timeout = !reader.IsDBNull(3) ? reader.GetInt32(3) : -1;
                    idpmd.logoutPage = !reader.IsDBNull(4) ? reader.GetString(4) : null;
                    idpmd.errorPage = !reader.IsDBNull(5) ? reader.GetString(5) : null;
                    idpmd.active = !reader.IsDBNull(6) ? reader.GetBoolean(6) : false;
                    idpmd.idpURL = !reader.IsDBNull(7) ? reader.GetString(7) : null;
                    idpmd.signIdpRequest = !reader.IsDBNull(8) ? reader.GetBoolean(8) : false;
                    idpmd.idpBinding = !reader.IsDBNull(9) ? reader.GetString(9) : null;
                    idpmd.spInitiated = !reader.IsDBNull(10) ? reader.GetBoolean(10) : false;
                    idpmd.accountID = !reader.IsDBNull(11) ? reader.GetGuid(11) : Guid.Empty;
                    info.Add(idpmd);
                }
            }
            finally
            {
                try
                {
                    if (reader != null)
                        reader.Close();
                    if (cmd != null)
                        cmd.Dispose();
                }
                catch (Exception)
                {
                    // Let system release resources
                }
            }
            return info;
        }

        public static List<IdpMetaData> getSamlInfoByIdpName(QueryConnection conn, string schema, String idpName)
        {
            QueryCommand cmd = null;
            QueryReader reader = null;
            List<IdpMetaData> info = new List<IdpMetaData>();

            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = "SELECT ID, IDPISSUERID, CERTIFICATE, TIMEOUT, LOGOUTPAGE, ERRORPAGE, ACTIVE, " +
                    " IDPURL, SIGNIDPREQUEST, IDPBINDING, SPINITIATED, ACCOUNTID FROM "
                    + getTableName(schema, SAML_IDP_TABLE)
                    + " WHERE IDPNAME=?";
                cmd.Parameters.Add("IDPNAME", OdbcType.VarChar).Value = idpName;
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    IdpMetaData idpmd = new IdpMetaData();
                    idpmd.ID = reader.GetGuid(0);
                    idpmd.idpName = idpName;
                    idpmd.idpIssuerID = reader.GetString(1);
                    idpmd.cert = !reader.IsDBNull(2) ? reader.GetString(2) : null;
                    idpmd.timeout = !reader.IsDBNull(3) ? reader.GetInt32(3) : -1;
                    idpmd.logoutPage = !reader.IsDBNull(4) ? reader.GetString(4) : null;
                    idpmd.errorPage = !reader.IsDBNull(5) ? reader.GetString(5) : null;
                    idpmd.active = !reader.IsDBNull(6) ? reader.GetBoolean(6) : false;
                    idpmd.idpURL = !reader.IsDBNull(7) ? reader.GetString(7) : null;
                    idpmd.signIdpRequest = !reader.IsDBNull(8) ? reader.GetBoolean(8) : false;
                    idpmd.idpBinding = !reader.IsDBNull(9) ? reader.GetString(9) : null;
                    idpmd.spInitiated = !reader.IsDBNull(10) ? reader.GetBoolean(10) : false;
                    idpmd.accountID = !reader.IsDBNull(11) ? reader.GetGuid(11) : Guid.Empty;
                    info.Add(idpmd);
                }
            }
            finally
            {
                try
                {
                    if (reader != null)
                        reader.Close();
                    if (cmd != null)
                        cmd.Dispose();
                }
                catch (Exception)
                {
                    // Let system release resources
                }
            }
            return info;
        }



        public static List<Message> getMessages(QueryConnection conn, string schema, Guid accountId)
        {
            QueryCommand cmd = null;
            QueryReader reader = null;

            List<Message> messages = new List<Message>();

            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = "SELECT ENTRY_TIME, MESSAGE FROM "
                    + getTableName(schema, SYSTEM_MESSAGES_TABLE)
                    + " WHERE EXPIRATION_TIME > " + conn.getCurrentTimestamp()
                    + " AND (ACCOUNT_ID=? OR ACCOUNT_ID IS NULL) ORDER BY ENTRY_TIME ASC";
                cmd.Parameters.Add("@ACCOUNT_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(accountId);

                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    Message msg = new Message();
                    msg.Date = reader.GetDateTime(0);
                    msg.Text = reader.GetString(1);
                    messages.Add(msg);
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Exception while retrieving system messages", ex);
                throw ex;
            }
            finally
            {
                try
                {
                    if (reader != null)
                        reader.Close();
                    if (cmd != null)
                        cmd.Dispose();
                }
                catch (Exception)
                {
                    // Let system release resources
                }
            }
            return messages;
        }

        public static LogInfo getServerLogInfo(QueryConnection conn, string schema, int id)
        {
            QueryCommand cmd = null;
            QueryReader reader = null;
            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = "SELECT ID, NAME, DESCRIPTION, LOGDIR, LOGPREFIX FROM " + getTableName(schema, APP_LOGS_INFO_TABLE) 
                    +  " WHERE ID=?";
                cmd.Parameters.Add("@ID", OdbcType.Int).Value= id;
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    LogInfo entry = new LogInfo();
                    entry.ID = reader.GetInt32(0);
                    entry.Name = !reader.IsDBNull(1) ? reader.GetString(1) : null;
                    entry.Description = !reader.IsDBNull(2) ? reader.GetString(2) : null;
                    entry.LogDir = !reader.IsDBNull(3) ? reader.GetString(3) : null;
                    entry.LogPrefix = !reader.IsDBNull(4) ? reader.GetString(4) : null;
                    return entry;
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Exception while retrieving log info from db for id : " + id, ex);                
            }
            finally
            {
                try
                {
                    if (reader != null)
                        reader.Close();
                    if (cmd != null)
                        cmd.Dispose();
                }
                catch (Exception)
                {
                    // Let system release resources
                }
            }
            return null;
        }

        public static List<LogInfo> getServerLogsInfo(QueryConnection conn, string schema)
        {
            QueryCommand cmd = null;
            QueryReader reader = null;

            List<LogInfo> info = new List<LogInfo>();

            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = "SELECT ID, NAME, DESCRIPTION, LOGDIR, LOGPREFIX FROM " + getTableName(schema, APP_LOGS_INFO_TABLE);

                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    LogInfo entry = new LogInfo();
                    entry.ID = reader.GetInt32(0);
                    entry.Name = !reader.IsDBNull(1) ? reader.GetString(1) : null;
                    entry.Description = !reader.IsDBNull(2) ? reader.GetString(2) : null;
                    entry.LogDir = !reader.IsDBNull(3) ? reader.GetString(3) : null;
                    entry.LogPrefix = !reader.IsDBNull(4) ? reader.GetString(4) : null;                    
                    info.Add(entry);
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Exception while retrieving logs info from db ", ex);                
            }
            finally
            {
                try
                {
                    if (reader != null)
                        reader.Close();
                    if (cmd != null)
                        cmd.Dispose();
                }
                catch (Exception)
                {
                    // Let system release resources
                }
            }
            return info;
        }

        public static List<ServerInfo> getServerInfo(QueryConnection conn, string schema)
        {
            QueryCommand cmd = null;
            QueryReader reader = null;

            List<ServerInfo> info = new List<ServerInfo>();

            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = "SELECT SERVERID, DESCRIPTION, ACORNLOG, SMIWEBLOG, SCHEDULERWEBLOG, CONNECTORWEBLOG FROM " + getTableName(schema, SERVER_INFO_TABLE);

                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    ServerInfo entry = new ServerInfo();
                    entry.serverID = reader.GetString(0);
                    entry.description = !reader.IsDBNull(1) ? reader.GetString(1) : null;
                    entry.acornLog = !reader.IsDBNull(2) ? reader.GetString(2) : null;
                    entry.smiwebLog = !reader.IsDBNull(3) ? reader.GetString(3) : null;
                    entry.schedulerwebLog = !reader.IsDBNull(4) ? reader.GetString(4) : null;
                    entry.connectorwebLog = !reader.IsDBNull(5) ? reader.GetString(5) : null;
                    info.Add(entry);
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Exception while retrieving system messages", ex);
                throw ex;
            }
            finally
            {
                try
                {
                    if (reader != null)
                        reader.Close();
                    if (cmd != null)
                        cmd.Dispose();
                }
                catch (Exception)
                {
                    // Let system release resources
                }
            }
            return info;
        }

        public static PasswordPolicy getPasswordPolicy(QueryConnection conn, string schema, Guid accountID)
        {
            QueryCommand cmd = null;
            QueryReader reader = null;
            PasswordPolicy policy = new PasswordPolicy();
            try
            {
                cmd = conn.CreateCommand();
                if (accountID != Guid.Empty)
                {
                    cmd.CommandText = "SELECT DESCRIPTION, REGULAREXPRESSION, MINLENGTH, MAXLENGTH, CONTAINSMIXEDCASE, CONTAINSSPECIAL, CONTAINSNUMERIC, DOESNOTCONTAINUSERNAME, CHANGEONFIRSTUSE, HISTORYLENGTH, EXPIRATIONDAYS, HASHALGORITHM, MAXFAILEDLOGINATTEMPTS, FAILEDLOGINWINDOW FROM " + getTableName(schema, PASSWORD_POLICY_TABLE) + " WHERE ACCOUNTID=?";
                    cmd.Parameters.Add("@ACCOUNTID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(accountID);
                    reader = cmd.ExecuteReader();
                }

                if (reader == null || !reader.HasRows)
                {
                    if (reader != null)
                        reader.Close();
                    cmd.Parameters.Clear();
                    cmd.CommandText = "SELECT DESCRIPTION, REGULAREXPRESSION, MINLENGTH, MAXLENGTH, CONTAINSMIXEDCASE, CONTAINSSPECIAL, CONTAINSNUMERIC, DOESNOTCONTAINUSERNAME, CHANGEONFIRSTUSE, HISTORYLENGTH, EXPIRATIONDAYS, HASHALGORITHM, MAXFAILEDLOGINATTEMPTS, FAILEDLOGINWINDOW FROM " + getTableName(schema, PASSWORD_POLICY_TABLE) + " WHERE ACCOUNTID IS NULL";
                    reader = cmd.ExecuteReader();
                }
                while (reader.Read())
                {
                    policy.description = reader.IsDBNull(0) ? null : reader.GetString(0);
                    policy.regularExpression = reader.IsDBNull(1) ? null : reader.GetString(1);
                    policy.minLength = reader.IsDBNull(2) ? 0 : reader.GetInt32(2);
                    policy.maxLength = reader.IsDBNull(3) ? 0 : reader.GetInt32(3);
                    policy.containsMixedCase = reader.IsDBNull(4) ? false : reader.GetBoolean(4);
                    policy.containsSpecial = reader.IsDBNull(5) ? false : reader.GetBoolean(5);
                    policy.containsNumeric = reader.IsDBNull(6) ? false : reader.GetBoolean(6);
                    policy.doesnotContainUsername = reader.IsDBNull(7) ? false : reader.GetBoolean(7);
                    policy.changeOnFirstUse = reader.IsDBNull(8) ? false : reader.GetBoolean(8);
                    policy.historyLength = reader.IsDBNull(9) ? 0 : reader.GetInt32(9);
                    policy.expirationDays = reader.IsDBNull(10) ? 0 : reader.GetInt32(10);
                    policy.hashAlgorithm = reader.IsDBNull(11) ? PasswordPolicy.HASHALGORITHM : reader.GetString(11);
                    policy.maxFailedLoginAttempts = reader.IsDBNull(12) ? PasswordPolicy.MAXFAILEDLOGINATTEMPTS : reader.GetInt32(12);
                    policy.failedLoginWindow = reader.IsDBNull(13) ? PasswordPolicy.FAILEDLOGINWINDOW : reader.GetInt32(13);
                    return policy;
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Exception while retrieving password policy", ex);
                throw ex;
            }
            finally
            {
                try
                {
                    if (reader != null)
                        reader.Close();
                    if (cmd != null)
                        cmd.Dispose();
                }
                catch (Exception)
                {
                    // Let system release resources
                }
            }
            return null;
        }

        public static void setPasswordPolicy(QueryConnection conn, string schema, Guid accountID, PasswordPolicy policy)
        {
            QueryCommand cmd = null;

            try
            {
                // delete old one, insert new once
                cmd = conn.CreateCommand();
                if (accountID != Guid.Empty)
                {
                    cmd.CommandText = "DELETE FROM " + getTableName(schema, PASSWORD_POLICY_TABLE) + " WHERE ACCOUNTID=?";
                    cmd.Parameters.Add("@ACCOUNTID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(accountID);
                }
                else
                {
                    cmd.CommandText = "DELETE FROM " + getTableName(schema, PASSWORD_POLICY_TABLE) + " WHERE ACCOUNTID IS NULL";
                }
                cmd.ExecuteNonQuery();

                cmd.Parameters.Clear();
                cmd.CommandText = "INSERT INTO "
                    + getTableName(schema, PASSWORD_POLICY_TABLE)
                    + " (POLICYID, ACCOUNTID, DESCRIPTION, REGULAREXPRESSION, MINLENGTH, MAXLENGTH, CONTAINSMIXEDCASE, CONTAINSSPECIAL, CONTAINSNUMERIC, DOESNOTCONTAINUSERNAME, CHANGEONFIRSTUSE, HISTORYLENGTH, EXPIRATIONDAYS, HASHALGORITHM, MAXFAILEDLOGINATTEMPTS, FAILEDLOGINWINDOW)"
                    + " VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                cmd.Parameters.Add("@POLICYID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(Guid.NewGuid());
                DbParameter acctparam = cmd.Parameters.Add("@ACCOUNTID", conn.getODBCUniqueIdentiferType());
                if (accountID == Guid.Empty)
                    acctparam.Value = System.DBNull.Value;
                else
                    acctparam.Value = conn.getUniqueIdentifer(accountID);
                DbParameter descparam = cmd.Parameters.Add("@DESCRIPTION", OdbcType.VarChar);
                if (policy.description == null)
                    descparam.Value = System.DBNull.Value;
                else
                    descparam.Value = policy.description;
                DbParameter regexparam = cmd.Parameters.Add("@REGULAREXPRESSION", OdbcType.VarChar);
                if (policy.regularExpression == null)
                    regexparam.Value = System.DBNull.Value;
                else
                    regexparam.Value = policy.regularExpression;

                cmd.Parameters.Add("@MINLENGTH", OdbcType.Int).Value = policy.minLength;
                cmd.Parameters.Add("@MAXLENGTH", OdbcType.Int).Value = policy.maxLength;
                cmd.Parameters.Add("@CONTAINSMIXEDCASE", conn.getODBCBooleanType()).Value = conn.getBooleanValue(policy.containsMixedCase);
                cmd.Parameters.Add("@CONTAINSSPECIAL", conn.getODBCBooleanType()).Value = conn.getBooleanValue(policy.containsSpecial);
                cmd.Parameters.Add("@CONTAINSNUMERIC", conn.getODBCBooleanType()).Value = conn.getBooleanValue(policy.containsNumeric);
                cmd.Parameters.Add("@DOESNOTCONTAINUSERNAME", conn.getODBCBooleanType()).Value = conn.getBooleanValue(policy.doesnotContainUsername);
                cmd.Parameters.Add("@CHANGEONFIRSTUSE", conn.getODBCBooleanType()).Value = conn.getBooleanValue(policy.changeOnFirstUse);
                cmd.Parameters.Add("@HISTORYLENGTH", OdbcType.Int).Value = policy.historyLength;
                cmd.Parameters.Add("@EXPIRATIONDAYS", OdbcType.Int).Value = policy.expirationDays;
                DbParameter hashAlg = cmd.Parameters.Add("@HASHALGORITHM", OdbcType.VarChar);
                if (policy.hashAlgorithm == null)
                    hashAlg.Value = System.DBNull.Value;
                else
                    hashAlg.Value = policy.hashAlgorithm;
                cmd.Parameters.Add("@MAXFAILEDLOGINATTEMPTS", OdbcType.Int).Value = policy.maxFailedLoginAttempts;
                cmd.Parameters.Add("@FAILEDLOGINWINDOW", OdbcType.Int).Value = policy.failedLoginWindow;
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Exception while setting the password policy for account " + accountID, ex);
                throw ex;
            }
            finally
            {
                try
                {
                    if (cmd != null)
                        cmd.Dispose();
                }
                catch (Exception)
                {
                    // Let system release resources
                }
            }
            return;
        }

        public static List<string> getPasswordHistory(QueryConnection conn, string schema, Guid userID)
        {
            QueryCommand cmd = null;
            QueryReader reader = null;
            List<string> history = new List<string>();
            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = "SELECT HASHEDPASSWORD FROM " + getTableName(schema, PASSWORD_HISTORY_TABLE) + " WHERE USERID=? ORDER BY CHANGEDATE DESC";
                cmd.Parameters.Add("@USERID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(userID);
                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    history.Add(reader.GetString(0));
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Exception while retrieving password history for user - " + userID, ex);
                throw ex;
            }
            finally
            {
                try
                {
                    if (reader != null)
                        reader.Close();
                    if (cmd != null)
                        cmd.Dispose();
                }
                catch (Exception)
                {
                    // Let system release resources
                }
            }
            return history;
        }

        public static void addPasswordToHistory(QueryConnection conn, string schema, Guid userID, string hash)
        {
            QueryCommand cmd = null;

            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = "INSERT INTO "
                     + getTableName(schema, PASSWORD_HISTORY_TABLE)
                     + " (HISTORYID, USERID, HASHEDPASSWORD, CHANGEDATE)"
                     + " VALUES (?,?,?,?)";
                cmd.Parameters.Add("@HISTORYID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(Guid.NewGuid());
                cmd.Parameters.Add("@USERID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(userID);
                cmd.Parameters.Add("@HASHEDPASSWORD", OdbcType.VarChar).Value = hash;
                cmd.Parameters.Add("@CHANGEDATE", OdbcType.DateTime).Value = truncateDateTime(DateTime.Now);
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Exception while adding a password to the history for user " + userID, ex);
                throw ex;
            }
            finally
            {
                try
                {
                    if (cmd != null)
                        cmd.Dispose();
                }
                catch (Exception)
                {
                    // Let system release resources
                }
            }
            return;
        }

        public static List<ReleaseInfo> getReleasesInfo(QueryConnection conn, String schema)
        {
            QueryCommand cmd = null;
            QueryReader result = null;
            Dictionary<int, ReleaseInfo> releaseInfoMapping = new Dictionary<int, ReleaseInfo>();
            List<ReleaseInfo> releaseInfoList = null;
            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = "SELECT ID, Name, URL, BCRegExp, Enabled FROM " + SINGLE_SPACE + getTableName(schema, RELEASE_INFO_TABLE);
                result = cmd.ExecuteReader();
                while (result.Read())
                {
                    if (result.IsDBNull(0) || result.IsDBNull(1) || result.IsDBNull(2))
                        continue;

                    ReleaseInfo releaseInfo = new ReleaseInfo();
                    int id = result.GetInt32(0);
                    string name = result.GetString(1);
                    string redirectUrl = result.GetString(2);
                    string regexp = result.IsDBNull(3) ? null : result.GetString(3);
                    bool enabled = result.IsDBNull(4) ? true : result.GetBoolean(4);

                    if (!enabled)
                        continue;

                    if (releaseInfoMapping.ContainsKey(id))
                        continue;

                    releaseInfo.ID = id;
                    releaseInfo.Name = name;
                    releaseInfo.RedirectUrl = redirectUrl;
                    releaseInfo.BCRegExp = regexp;

                    releaseInfoMapping.Add(id, releaseInfo);
                }

                if (releaseInfoMapping != null && releaseInfoMapping.Count > 0)
                {
                    releaseInfoList = new List<ReleaseInfo>();
                    foreach (ReleaseInfo rInfo in releaseInfoMapping.Values)
                    {
                        releaseInfoList.Add(rInfo);
                    }
                }

                return releaseInfoList;
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Unable to retrieve release information ", ex);
                throw ex;
            }
            finally
            {
                try
                {
                    if (result != null)
                    {
                        result.Close();
                    }
                    if (cmd != null)
                    {
                        cmd.Dispose();
                    }
                }
                catch (Exception)
                {
                    //Global.systemLog.Warn("Cannot close cmd object. Let system handle", ex2);
                }
            }
        }

        public static void updateUserReleaseType(QueryConnection conn, String schema, Guid userID, int releaseType)
        {
            QueryCommand cmd = null;
            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = "UPDATE" + SINGLE_SPACE + getTableName(schema, Database.USER_TABLE_NAME) + SINGLE_SPACE +
                    "SET ReleaseType = ? WHERE PKID = ?";
                DbParameter param = cmd.Parameters.Add("@ReleaseType", OdbcType.Int);
                if (releaseType <= 0)
                    param.Value = System.DBNull.Value;
                else
                    param.Value = releaseType;
                cmd.Parameters.Add("@PKID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(userID);
                cmd.ExecuteNonQuery();
            }
            finally
            {
                if (cmd != null)
                    cmd.Dispose();
            }
        }

        public static void updateRenderType(QueryConnection conn, String schema, Guid userID, bool html)
        {
            QueryCommand cmd = null;
            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = "UPDATE" + SINGLE_SPACE + getTableName(schema, Database.USER_TABLE_NAME) + SINGLE_SPACE +
                    "SET HTMLInterface = ? WHERE PKID = ?";
                cmd.Parameters.Add("@HTMLInterface", conn.getODBCBooleanType()).Value = conn.getBooleanValue(html);
                cmd.Parameters.Add("@PKID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(userID);
                cmd.ExecuteNonQuery();
            }
            finally
            {
                if (cmd != null)
                    cmd.Dispose();
            }
        }

        public static void updateExternalDBFlag(QueryConnection conn, String schema, Guid userID, bool enabledExternalDB)
        {
            QueryCommand cmd = null;
            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = "UPDATE" + SINGLE_SPACE + getTableName(schema, Database.USER_TABLE_NAME) + SINGLE_SPACE +
                    "SET ExternalDBEnabled = ? WHERE PKID = ?";
                cmd.Parameters.Add("@ExternalDBEnabled", conn.getODBCBooleanType()).Value = conn.getBooleanValue(enabledExternalDB);
                cmd.Parameters.Add("@PKID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(userID);
                cmd.ExecuteNonQuery();
            }
            finally
            {
                if (cmd != null)
                    cmd.Dispose();
            }
        }

        public static void updateDenyCreateSpace(QueryConnection conn, String schema, Guid userID, bool denyCreateSpace)
        {
            QueryCommand cmd = null;
            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = "UPDATE" + SINGLE_SPACE + getTableName(schema, Database.USER_TABLE_NAME) + SINGLE_SPACE +
                    "SET DenyAddSpace = ? WHERE PKID = ?";
                cmd.Parameters.Add("@DenyAddSpace", conn.getODBCBooleanType()).Value = conn.getBooleanValue(denyCreateSpace);
                cmd.Parameters.Add("@PKID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(userID);
                cmd.ExecuteNonQuery();
            }
            finally
            {
                if (cmd != null)
                    cmd.Dispose();
            }
        }


        public static ReleaseInfo getReleaseInfoForUser(QueryConnection conn, String schema, User u)
        {
            int releaseType = u.ReleaseType;
            ReleaseInfo releaseInfo = getReleaseInfoByType(conn, schema, releaseType);
            if (releaseInfo != null && releaseInfo.RedirectUrl != null)
            {
                if (u.ManagedAccountId != Guid.Empty)
                {
                    AccountDetails accoutDetails = getAccountDetails(conn, schema, u.ManagedAccountId, u);
                    string updatedUrl = Util.getDomainReplacedUrl(accoutDetails, u, releaseInfo.RedirectUrl);
                    if (!string.IsNullOrWhiteSpace(updatedUrl))
                    {
                        releaseInfo.RedirectUrl = updatedUrl;
                    }
                }
            }
            return releaseInfo;
        }

        public static ReleaseInfo getReleaseInfoByType(QueryConnection conn, String schema, int releaseType)
        {
            QueryCommand cmd = null;
            QueryReader result = null;
            ReleaseInfo releaseInfo = null;
            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = "SELECT ID, Name, URL, BCRegExp, Enabled FROM" + SINGLE_SPACE + getTableName(schema, RELEASE_INFO_TABLE)
                    + " WHERE ID = ?";
                cmd.Parameters.Add("@ID", OdbcType.Int).Value = releaseType;
                result = cmd.ExecuteReader();
                if (result.HasRows)
                {
                    result.Read();
                    if (!result.IsDBNull(0) && !result.IsDBNull(1) && !result.IsDBNull(2))
                    {
                        int id = result.GetInt32(0);
                        string name = result.GetString(1);
                        string redirectURL = result.GetString(2);
                        string regexp = result.IsDBNull(3) ? null : result.GetString(3);
                        bool enabled = result.IsDBNull(4) ? true : result.GetBoolean(4);
                        if (!enabled)
                            return null;
                        releaseInfo = new ReleaseInfo();
                        releaseInfo.ID = id;
                        releaseInfo.Name = name;
                        releaseInfo.RedirectUrl = redirectURL;
                        releaseInfo.BCRegExp = regexp;
                    }
                }
                return releaseInfo;
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Unable to retrieve release info for type " + releaseType, ex);
                throw ex;
            }
            finally
            {
                try
                {
                    if (result != null)
                    {
                        result.Close();
                    }
                    if (cmd != null)
                    {
                        cmd.Dispose();
                    }
                }
                catch (Exception)
                {
                    //Global.systemLog.Warn("Cannot close cmd object. Let system handle", ex2);
                }
            }
        }

        public static ReleaseInfo getReleaseInfoByName(QueryConnection conn, String schema, string release)
        {
            QueryCommand cmd = null;
            QueryReader result = null;
            ReleaseInfo releaseInfo = null;
            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = "SELECT ID, Name, URL, BCRegExp, Enabled FROM" + SINGLE_SPACE + getTableName(schema, RELEASE_INFO_TABLE)
                    + " WHERE Name = ?";
                cmd.Parameters.Add("@Name", OdbcType.VarChar).Value = release;
                result = cmd.ExecuteReader();
                if (result.HasRows)
                {
                    result.Read();
                    if (!result.IsDBNull(0) && !result.IsDBNull(1) && !result.IsDBNull(2))
                    {
                        int id = result.GetInt32(0);
                        string name = result.GetString(1);
                        string redirectURL = result.GetString(2);
                        string regexp = result.IsDBNull(3) ? null : result.GetString(3);
                        bool enabled = result.IsDBNull(4) ? true : result.GetBoolean(4);
                        if (!enabled)
                            return null;
                        releaseInfo = new ReleaseInfo();
                        releaseInfo.ID = id;
                        releaseInfo.Name = name;
                        releaseInfo.RedirectUrl = redirectURL;
                        releaseInfo.BCRegExp = regexp;
                    }
                }
                return releaseInfo;
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Unable to retrieve release info for " + release, ex);
                throw ex;
            }
            finally
            {
                try
                {
                    if (result != null)
                    {
                        result.Close();
                    }
                    if (cmd != null)
                    {
                        cmd.Dispose();
                    }
                }
                catch (Exception)
                {
                    //Global.systemLog.Warn("Cannot close cmd object. Let system handle", ex2);
                }
            }
        }

        public static PerformanceEngineVersion getPerformanceEngineVersionForSpace(QueryConnection conn, string schema, Space sp)
        {
            return getPerformanceEngineVersionForSpace(conn, schema, sp.ProcessVersionID);
        }
        public static PerformanceEngineVersion getPerformanceEngineVersionForSpaceFromName(QueryConnection conn, string schema, string pName)
        {
            QueryCommand cmd = null;
            QueryReader result = null;
            PerformanceEngineVersion peVersion = null;
            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = "SELECT ID, Name, Release, EngineCommand, DeleteCommand, Visible, SupportsRetryLoad, IsProcessingGroupAware, SupportSFDCExtract FROM" + SINGLE_SPACE + getTableName(schema, PROCESS_VERSIONS_TABLE)
                    + " WHERE Name = ?";
                cmd.Parameters.Add("@Name", OdbcType.VarChar).Value = pName;
                result = cmd.ExecuteReader();
                if (result.HasRows)
                {
                    result.Read();
                    if (!result.IsDBNull(0) && !result.IsDBNull(1) && !result.IsDBNull(2)
                        && !result.IsDBNull(3) && !result.IsDBNull(4) && !result.IsDBNull(5))
                    {
                        peVersion = new PerformanceEngineVersion();
                        int id = result.GetInt32(0);
                        string name = result.GetString(1);
                        string release = result.GetString(2);
                        string engineCommand = result.GetString(3);
                        string deleteCommand = result.GetString(4);
                        bool visible = result.GetBoolean(5);
                        bool supportsRetryLoad = result.IsDBNull(6) ? false : result.GetBoolean(6);
                        bool isPgAware = result.IsDBNull(7) ? false : result.GetBoolean(7);
                        bool supportsSFDCExtract = result.IsDBNull(8) ? false : result.GetBoolean(8);

                        peVersion.ID = id;
                        peVersion.Name = name;
                        peVersion.Release = release;
                        peVersion.EngineCommand = engineCommand;
                        peVersion.DeleteCommand = deleteCommand;
                        peVersion.Visible = visible;
                        peVersion.SupportsRetryLoad = supportsRetryLoad;
                        peVersion.IsProcessingGroupAware = isPgAware;
                        peVersion.SupportsSFDCExtract = supportsSFDCExtract;
                    }
                }
                return peVersion;
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Unable to retrieve process information for process type " + pName, ex);
                throw ex;
            }
            finally
            {
                try
                {
                    if (result != null)
                    {
                        result.Close();
                    }
                    if (cmd != null)
                    {
                        cmd.Dispose();
                    }
                }
                catch (Exception)
                {
                    //Global.systemLog.Warn("Cannot close cmd object. Let system handle", ex2);
                }
            }
        }

        public static PerformanceEngineVersion getPerformanceEngineVersionForSpace(QueryConnection conn, string schema, int processVersion)
        {
            QueryCommand cmd = null;
            QueryReader result = null;
            PerformanceEngineVersion peVersion = null;
            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = "SELECT ID, Name, Release, EngineCommand, DeleteCommand, Visible, SupportsRetryLoad, IsProcessingGroupAware, SupportSFDCExtract FROM" + SINGLE_SPACE + getTableName(schema, PROCESS_VERSIONS_TABLE)
                    + " WHERE ID = ?";
                cmd.Parameters.Add("@ID", OdbcType.Int).Value = processVersion;
                result = cmd.ExecuteReader();
                if (result.HasRows)
                {
                    result.Read();
                    if (!result.IsDBNull(0) && !result.IsDBNull(1) && !result.IsDBNull(2)
                        && !result.IsDBNull(3) && !result.IsDBNull(4) && !result.IsDBNull(5))
                    {
                        peVersion = new PerformanceEngineVersion();
                        int id = result.GetInt32(0);
                        string name = result.GetString(1);
                        string release = result.GetString(2);
                        string engineCommand = result.GetString(3);
                        string deleteCommand = result.GetString(4);
                        bool visible = result.GetBoolean(5);
                        bool supportsRetryLoad = result.IsDBNull(6) ? false : result.GetBoolean(6);
                        bool isPgAware = result.IsDBNull(7) ? false : result.GetBoolean(7);
                        bool supportsSFDCExtract = result.IsDBNull(8) ? false : result.GetBoolean(8);

                        peVersion.ID = id;
                        peVersion.Name = name;
                        peVersion.Release = release;
                        peVersion.EngineCommand = engineCommand;
                        peVersion.DeleteCommand = deleteCommand;
                        peVersion.Visible = visible;
                        peVersion.SupportsRetryLoad = supportsRetryLoad;
                        peVersion.IsProcessingGroupAware = isPgAware;
                        peVersion.SupportsSFDCExtract = supportsSFDCExtract;
                    }
                }
                return peVersion;
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Unable to retrieve process information for process type " + processVersion, ex);
                throw ex;
            }
            finally
            {
                try
                {
                    if (result != null)
                    {
                        result.Close();
                    }
                    if (cmd != null)
                    {
                        cmd.Dispose();
                    }
                }
                catch (Exception)
                {
                    //Global.systemLog.Warn("Cannot close cmd object. Let system handle", ex2);
                }
            }
        }

        public static List<PerformanceEngineVersion> getPerformanceEngineVersions(QueryConnection conn, String schema)
        {
            QueryCommand cmd = null;
            QueryReader result = null;
            Dictionary<int, PerformanceEngineVersion> performanceEngineVersionMapping = new Dictionary<int, PerformanceEngineVersion>();
            List<PerformanceEngineVersion> performanceEngineVersionList = null;
            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = "SELECT ID, Name, Release, EngineCommand, DeleteCommand, Visible, SupportsRetryLoad, IsProcessingGroupAware, SupportSFDCExtract FROM" + SINGLE_SPACE + getTableName(schema, PROCESS_VERSIONS_TABLE);
                result = cmd.ExecuteReader();
                while (result.Read())
                {
                    if (result.IsDBNull(0) || result.IsDBNull(1) || result.IsDBNull(2)
                        || result.IsDBNull(3) || result.IsDBNull(4) || result.IsDBNull(5))
                    {
                        continue;
                    }

                    PerformanceEngineVersion performanceEngineVersion = new PerformanceEngineVersion();
                    int id = result.GetInt32(0);
                    string name = result.GetString(1);
                    string release = result.GetString(2);
                    string engineCommand = result.GetString(3);
                    string deleteCommand = result.GetString(4);
                    bool visible = result.GetBoolean(5);
                    bool supportsRetryLoad = result.IsDBNull(6) ? false : result.GetBoolean(6);
                    bool isPgAware = result.IsDBNull(7) ? false : result.GetBoolean(7);
                    bool supportsSFDCExtract = result.IsDBNull(8) ? false : result.GetBoolean(8);

                    if (performanceEngineVersionMapping.ContainsKey(id))
                    {
                        continue;
                    }
                    performanceEngineVersion.ID = id;
                    performanceEngineVersion.Name = name;
                    performanceEngineVersion.Release = release;
                    performanceEngineVersion.EngineCommand = engineCommand;
                    performanceEngineVersion.DeleteCommand = deleteCommand;
                    performanceEngineVersion.Visible = visible;
                    performanceEngineVersion.SupportsRetryLoad = supportsRetryLoad;
                    performanceEngineVersion.IsProcessingGroupAware = isPgAware;
                    performanceEngineVersion.SupportsSFDCExtract = supportsSFDCExtract;

                    performanceEngineVersionMapping.Add(id, performanceEngineVersion);
                }

                if (performanceEngineVersionMapping != null && performanceEngineVersionMapping.Count > 0)
                {
                    performanceEngineVersionList = new List<PerformanceEngineVersion>();
                    foreach (PerformanceEngineVersion peVersion in performanceEngineVersionMapping.Values)
                    {
                        performanceEngineVersionList.Add(peVersion);
                    }
                }

                return performanceEngineVersionList;
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Unable to retrieve release information ", ex);
                throw ex;
            }
            finally
            {
                try
                {
                    if (result != null)
                    {
                        result.Close();
                    }
                    if (cmd != null)
                    {
                        cmd.Dispose();
                    }
                }
                catch (Exception)
                {
                    //Global.systemLog.Warn("Cannot close cmd object. Let system handle", ex2);
                }
            }
        }

        public static List<RunningProcessInfo> getRunningProcessInfo(QueryConnection conn, string schema, string server, Guid spaceID)
        {
            QueryCommand cmd = null;
            QueryReader result = null;
            try
            {
                cmd = conn.CreateCommand();

                cmd.CommandText = "SELECT SERVER_IP, SERVER_PORT, TYPE, Source, PID, Info FROM" + SINGLE_SPACE + getTableName(schema, RUNNING_PROCESSES_TABLE) +
                    " WHERE SPACE_ID=?";
                cmd.Parameters.Add("SPACE_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(spaceID);
                if (server != null && server.Trim().Length > 0)
                {
                    cmd.CommandText = cmd.CommandText + " AND SERVER_IP=?";
                    cmd.Parameters.Add("SERVER_IP", OdbcType.VarChar).Value = server;
                }
                result = cmd.ExecuteReader();
                List<RunningProcessInfo> response = new List<RunningProcessInfo>();
                while (result.Read())
                {
                    if (result.IsDBNull(0) || result.IsDBNull(1))
                    {
                        continue;
                    }

                    RunningProcessInfo runningProcessInfo = new RunningProcessInfo();
                    runningProcessInfo.server = result.GetString(0);
                    runningProcessInfo.port = result.GetString(1);
                    if (!result.IsDBNull(2))
                    {
                        runningProcessInfo.type = result.GetString(2);
                    }

                    if (!result.IsDBNull(3))
                    {
                        runningProcessInfo.source = result.GetString(3);
                    }

                    if (!result.IsDBNull(4))
                    {
                        runningProcessInfo.pids = result.GetString(4);
                    }
                    if (!result.IsDBNull(5))
                    {
                        runningProcessInfo.info = result.GetString(5);
                    }
                    response.Add(runningProcessInfo);
                }
                return response;
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Unable to retrieve processing server url ", ex);
                return null;
            }
            finally
            {
                try
                {
                    if (result != null)
                    {
                        result.Close();
                    }
                    if (cmd != null)
                    {
                        cmd.Dispose();
                    }
                }
                catch (Exception)
                {
                    //Global.systemLog.Warn("Cannot close cmd object. Let system handle", ex2);
                }
            }
        }


        public static string getProcessingServerURL(string schema, Guid space_ID)
        {
            QueryConnection conn = null;
            QueryCommand cmd = null;
            QueryReader result = null;
            try
            {
                conn = ConnectionPool.getConnection();
                cmd = conn.CreateCommand();

                cmd.CommandText = "SELECT SERVER_IP, SERVER_PORT FROM" + SINGLE_SPACE + getTableName(schema, RUNNING_PROCESSES_TABLE) + " WHERE SPACE_ID=?";
                cmd.Parameters.Add("SPACE_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(space_ID);
                result = cmd.ExecuteReader();
                string serverip = null;
                string port = null;
                while (result.Read())
                {
                    if (result.IsDBNull(0) || result.IsDBNull(1))
                    {
                        continue;
                    }

                    serverip = result.GetString(0);
                    port = result.GetString(1);
                    break;
                }
                if (serverip == null)
                    return null;
                return serverip + ":" + port;
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Unable to retrieve processing server url ", ex);
                return null;
            }
            finally
            {
                try
                {
                    if (result != null)
                    {
                        result.Close();
                    }
                    if (cmd != null)
                    {
                        cmd.Dispose();
                    }
                }
                catch (Exception)
                {
                    //Global.systemLog.Warn("Cannot close cmd object. Let system handle", ex2);
                }
                ConnectionPool.releaseConnection(conn);
            }
        }

        public static void removeProcessingServerForSpace(Guid space_ID, string type)
        {
            QueryConnection conn = null;
            QueryCommand cmd = null;
            try
            {
                conn = ConnectionPool.getConnection();
                cmd = conn.CreateCommand();

                cmd.CommandText = "DELETE FROM" + SINGLE_SPACE + getTableName(mainSchema, RUNNING_PROCESSES_TABLE) + " WHERE SPACE_ID=?";
                cmd.Parameters.Add("SPACE_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(space_ID);
                if (type != null)
                {
                    cmd.CommandText = cmd.CommandText + " AND TYPE=?";
                    cmd.Parameters.Add("TYPE", OdbcType.VarChar).Value = type;
                }
                cmd.ExecuteNonQuery();
                Global.systemLog.Info("Removing processing server information from admin db : space " + space_ID + " : type : " + type);
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Unable to remove processing server information for space:" + space_ID, ex);
            }
            finally
            {
                try
                {
                    if (cmd != null)
                    {
                        cmd.Dispose();
                    }
                }
                catch (Exception)
                {
                    //Global.systemLog.Warn("Cannot close cmd object. Let system handle", ex2);
                }
                ConnectionPool.releaseConnection(conn);
            }
        }

        public static void removeProcessingServer(string schema, string serverIP, string serverPort)
        {
            QueryConnection conn = null;
            QueryCommand cmd = null;
            try
            {
                conn = ConnectionPool.getConnection();
                cmd = conn.CreateCommand();
                cmd.CommandText = "DELETE FROM" + SINGLE_SPACE + getTableName(schema, RUNNING_PROCESSES_TABLE) + " WHERE SERVER_IP =? AND SERVER_PORT =?";
                cmd.Parameters.Add("SERVER_IP", OdbcType.VarChar).Value = serverIP;
                cmd.Parameters.Add("SERVER_PORT", OdbcType.VarChar).Value = serverPort;
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Unable to remove processing server information:" + serverIP, ex);
            }
            finally
            {
                try
                {
                    if (cmd != null)
                    {
                        cmd.Dispose();
                    }
                }
                catch (Exception)
                {
                    //Global.systemLog.Warn("Cannot close cmd object. Let system handle", ex2);
                }
                ConnectionPool.releaseConnection(conn);
            }
        }

        public static bool addDurationColumnToTxnCommandHistory(Space sp)
        {
            QueryConnection c = null;
            try
            {
                string connString = sp.getFullConnectString();
                c = ConnectionPool.getConnection(connString, false);
                bool durationExists = false;
                DataTable dt = null;
                if (Database.isDBTypeIB(sp.ConnectString))
                    dt = c.GetSchema("Columns", new string[] { sp.Schema, null, "TXN_COMMAND_HISTORY" });
                else
                    dt = c.GetSchema("Columns", new string[] { null, sp.Schema, "TXN_COMMAND_HISTORY" });
                foreach (DataRow dr in dt.Rows)
                {
                    if (dr["COLUMN_NAME"].Equals("DURATION"))
                    {
                        durationExists = true;
                        break;
                    }
                }
                if (durationExists)
                    return true;
                List<string> colNames = new List<string>();
                List<string> colTypes = new List<string>();
                List<int> actions = new List<int>();
                colNames.Add("DURATION");
                colTypes.Add("BIGINT");
                actions.Add(Database.ALTER_ADD);
                Global.systemLog.Info("Adding duration column for space id : " + sp.ID + " : name : " + sp.Name);
                Database.alterTable(c, sp.Schema, "TXN_COMMAND_HISTORY", colNames, colTypes, actions);
                return true;
            }
            catch (Exception ex)
            {
                Global.systemLog.Error(ex);
            }
            finally
            {
                ConnectionPool.releaseConnection(c);
            }
            return false;
        }

        public static bool addProcessingGroupToTxnCommandHistory(Space sp)
        {
            QueryConnection c = null;
            try
            {
                string connString = sp.getFullConnectString();
                c = ConnectionPool.getConnection(connString, false);
                bool processingGroupExists = false;
                DataTable dt = null;
                if (Database.isDBTypeIB(sp.ConnectString))
                    dt = c.GetSchema("Columns", new string[] { sp.Schema, null, "TXN_COMMAND_HISTORY" });
                else
                    dt = c.GetSchema("Columns", new string[] { null, sp.Schema, "TXN_COMMAND_HISTORY" });
                foreach (DataRow dr in dt.Rows)
                {
                    if (dr["COLUMN_NAME"].Equals("PROCESSINGGROUP"))
                    {
                        processingGroupExists = true;
                        break;
                    }
                }
                if (processingGroupExists)
                    return true;
                List<string> colNames = new List<string>();
                List<string> colTypes = new List<string>();
                List<int> actions = new List<int>();
                colNames.Add("PROCESSINGGROUP");
                colTypes.Add("VARCHAR (1024)");
                actions.Add(Database.ALTER_ADD);
                Global.systemLog.Info("Adding ProcessingGroup column for space id : " + sp.ID + " : name : " + sp.Name);
                Database.alterTable(c, sp.Schema, "TXN_COMMAND_HISTORY", colNames, colTypes, actions);
                return true;
            }
            catch (Exception ex)
            {
                Global.systemLog.Error(ex);
            }
            finally
            {
                ConnectionPool.releaseConnection(c);
            }
            return false;
        }

        public static void addProcessingServer(string schema, Guid space_ID, string type, string serverip, string port, string source, string pids, string info)
        {
            QueryConnection conn = null;
            QueryCommand cmd = null;
            try
            {
                conn = ConnectionPool.getConnection();
                cmd = conn.CreateCommand();
                cmd.CommandText = "INSERT INTO " + schema + "." + RUNNING_PROCESSES_TABLE +
                    " (SPACE_ID,TYPE,SERVER_IP, SERVER_PORT, Source, PID, Info, CreatedDate) VALUES (?,?,?,?,?,?,?," + conn.getCurrentTimestamp() + ")";
                cmd.Parameters.Add("SPACE_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(space_ID);
                cmd.Parameters.Add("TYPE", OdbcType.VarChar).Value = type;
                cmd.Parameters.Add("SERVER_IP", OdbcType.VarChar).Value = serverip;
                cmd.Parameters.Add("SERVER_PORT", OdbcType.VarChar).Value = port;
                DbParameter dbparam1 = cmd.Parameters.Add("Source", OdbcType.VarChar);
                if (source == null)
                {
                    dbparam1.Value = System.DBNull.Value;
                }
                else
                {
                    dbparam1.Value = source;
                }
                cmd.Parameters.Add("PID", OdbcType.VarChar).Value = pids.ToString();
                DbParameter dbparam2 = cmd.Parameters.Add("Info", OdbcType.VarChar);
                if (info == null)
                {
                    dbparam2.Value = System.DBNull.Value;
                }
                else
                {
                    dbparam2.Value = info;
                }
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Unable to add server information ", ex);
            }
            finally
            {
                try
                {
                    if (cmd != null)
                    {
                        cmd.Dispose();
                    }
                }
                catch (Exception)
                {
                    //Global.systemLog.Warn("Cannot close cmd object. Let system handle", ex2);
                }
                ConnectionPool.releaseConnection(conn);
            }
        }

        public static bool isDBTypeXMLA(Connection conn)
        {
            return conn.Type == Database.MICROSOFT_ANALYSIS_SERVICES || conn.Type == Database.HYPERIAN_ESSBASE || conn.Type == Database.SAP_BW || conn.Type == Database.PENTAHO_MONDRIAN;
        }

        public static bool isDBTypeIB(string connectString)
        {
            if (connectString == null)
                return false;
            return connectString.StartsWith("jdbc:mysql://");
        }

        public static bool isDBTypeParAccel(string connectString)
        {
            if (connectString == null)
                return false;
            return connectString.StartsWith("jdbc:paraccel://");
        }

        public static bool isOraTableSpaceExists(QueryConnection conn, string tablespacename)
        {
            if (!conn.isDBTypeOracle())
                return false;
            else
            {
                QueryCommand cmd = conn.CreateCommand();
                cmd.CommandText = "SELECT * FROM DBA_TABLESPACES WHERE TABLESPACE_NAME=?";
                cmd.Parameters.Add("TABLESPACE_NAME", OdbcType.VarChar).Value = tablespacename;
                QueryReader reader = null;
                try
                {
                    reader = cmd.ExecuteReader();
                    return reader.Read();
                }
                finally
                {
                    try
                    {
                        if (cmd != null)
                        {
                            cmd.Dispose();
                        }
                        if (reader != null)
                        {
                            reader.Close();
                        }
                    }
                    catch (Exception)
                    {
                    }
                }
            }
        }

        public static string[][] getStatus(QueryConnection conn)
        {
            List<string[]> result = new List<string[]>();
            QueryReader reader = null;
            QueryCommand cmd = null;
            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = "SHOW SYSTEM STATUS";
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    result.Add(new string[] { reader.GetString(0), reader.GetString(1) });
                }
            }
            finally
            {
                if (reader != null)
                    reader.Close();
                if (cmd != null)
                    cmd.Dispose();
            }
            return result.ToArray();
        }

        public static object[] getTableSizes(QueryConnection conn, string schema)
        {
            List<Object[]> result = new List<Object[]>();
            QueryReader reader = null;
            QueryCommand cmd = null;
            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = "SELECT TABLE_NAME,OBJECTSIZE(TABLE_SCHEMA+'.'+TABLE_NAME) 'Size' from INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA='" + schema + "'";
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    result.Add(new object[] { reader.GetString(0), reader.GetInt64(1) });
                }
            }
            finally
            {
                if (reader != null)
                    reader.Close();
                if (cmd != null)
                    cmd.Dispose();
            }
            return result.ToArray();
        }

        public static object[] showTable(QueryConnection conn, string schema, string table)
        {
            List<Object[]> result = new List<Object[]>();
            QueryReader reader = null;
            QueryCommand cmd = null;
            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = "SHOW TABLE " + schema + "." + table;
                reader = cmd.ExecuteReader();
                int nameCol = 0;
                int typeCol = 0;
                int widthCol = 0;
                int diskCol = 0;
                int sizeCol = 0;
                for (int i = 0; i < reader.FieldCount; i++)
                {
                    if (reader.GetName(i) == "Column")
                        nameCol = i;
                    else if (reader.GetName(i) == "DataType")
                        typeCol = i;
                    else if (reader.GetName(i) == "Width")
                        widthCol = i;
                    else if (reader.GetName(i) == "Disk")
                        diskCol = i;
                    else if (reader.GetName(i) == "Size")
                        sizeCol = i;
                }
                while (reader.Read())
                {
                    result.Add(new object[] { reader.GetString(nameCol), reader.GetString(typeCol), reader.GetInt32(widthCol), reader.GetBoolean(diskCol), reader.GetInt64(sizeCol) });
                }
            }
            finally
            {
                if (reader != null)
                    reader.Close();
                if (cmd != null)
                    cmd.Dispose();
            }
            return result.ToArray();
        }

        public static string alterColumn(QueryConnection conn, string schema, string table, string column, string dataType, int width, bool diskBased)
        {
            List<Object[]> result = new List<Object[]>();
            QueryCommand cmd = null;
            try
            {
                cmd = conn.CreateCommand();
                if (dataType.ToUpper() == "VARCHAR")
                {
                    dataType = dataType + "(" + width + ")";
                }
                cmd.CommandText = "ALTER TABLE " + schema + "." + table + " ALTER COLUMN " + column + " " + dataType + " " + (diskBased ? "DISK COMPRESSED" : "MEMORY UNCOMPRESSED");

                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {

                if (cmd != null)
                    cmd.Dispose();
            }
            return null;
        }

        public static bool supportsTop(string driver)
        {
            driver = driver.ToLower();
            if (driver == "com.microsoft.sqlserver.jdbc.sqlserverdriver" || driver == "memdb.jdbcclient.dbdriver"
                || driver == "com.microsoft.jdbc.sqlserver.sqlserverdriver" || driver == "com.sybase.jdbc4.jdbc.sybdriver"
                || driver == "com.teradata.jdbc.teradriver")
                return true;
            return false;
        }

        internal static bool loadLocaleTable(QueryConnection conn, string schema, string path)
        {
            QueryCommand cmd = null;
            HashSet<string> llist = new HashSet<string>();
            // Get a list of existing Locales
            int count = 0;
            while (conn.State != ConnectionState.Open)
            {
                Thread.Sleep(1000);
                count++;
                if (count > 20)
                {
                    Global.systemLog.Error("Loading the locale table failed, the connection is closed");
                    return false;
                }
            }
            if (tableExists(conn, schema, LOCALE_TABLE))
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = "SELECT LOCALEID FROM " + schema + "." + LOCALE_TABLE;
                QueryReader preader = cmd.ExecuteReader();
                while (preader.Read())
                {
                    llist.Add(preader.GetString(0));
                }
                preader.Close();
            }
            else
            {
                createTables(conn, schema);
                cmd = conn.CreateCommand();
            }

            string filename = path + "\\" + LOCALE_FILENAME;
            if (!File.Exists(filename))
            {
                Global.systemLog.Error("No locale file: " + filename);
                return false;
            }
            Global.systemLog.Info("Loading locale.config from '" + filename + "'");

            // Read in parameters for each space type
            StreamReader reader = new StreamReader(filename);
            string line = null;
            cmd.CommandText = "INSERT INTO " + schema + "." + LOCALE_TABLE +
                " (LocaleID, Description) VALUES (?,?)";
            DbParameter locale = cmd.Parameters.Add("LocaleID", OdbcType.VarChar);
            DbParameter desc = cmd.Parameters.Add("Description", OdbcType.NVarChar);
            while ((line = reader.ReadLine()) != null)
            {
                if (line.StartsWith("//") || line.Equals(""))
                    continue;
                string[] cols = line.Split(new char[] { '\t' });
                string localeid = cols[0].Trim(new char[] { ' ', '"' });
                if (llist.Contains(localeid))
                    continue;
                locale.Value = localeid;
                desc.Value = cols[1].Trim(new char[] { ' ', '"' });
                cmd.ExecuteNonQuery();
            }
            reader.Close();
            cmd.Dispose();
            return true;
        }

        public static List<Language> listLanguages(QueryConnection conn, string schema)
        {
            int count = 0;
            while (conn.State != ConnectionState.Open)
            {
                Thread.Sleep(1000);
                count++;
                if (count > 20)
                {
                    Global.systemLog.Error("Reading the locale table failed, the connection is closed");
                    return null;
                }
            }

            List<Language> ret = new List<Language>();
            QueryCommand cmd = conn.CreateCommand();
            cmd.CommandText = "SELECT LOCALEID, Description FROM " + schema + "." + LOCALE_TABLE + " ORDER BY Description";
            QueryReader preader = cmd.ExecuteReader();
            while (preader.Read())
            {
                Language lang = new Language(preader.GetString(0), preader.GetString(1));
                ret.Add(lang);
            }
            preader.Close();

            return ret;
        }

        public static string getLanguageDescription(QueryConnection conn, string schema, string localeId)
        {
            int count = 0;
            while (conn.State != ConnectionState.Open)
            {
                Thread.Sleep(1000);
                count++;
                if (count > 20)
                {
                    Global.systemLog.Error("Reading the locale table failed, the connection is closed");
                    return null;
                }
            }

            if (localeId == null)
                localeId = "en-US";

            QueryCommand cmd = conn.CreateCommand();
            cmd.CommandText = "SELECT Description FROM " + schema + "." + LOCALE_TABLE + " WHERE LocaleID = '" + localeId + "'";
            String description = null;
            QueryReader preader = cmd.ExecuteReader();
            if (preader.Read())
            {
                description = preader.GetString(0);
            }
            preader.Close();

            return description;
        }

        public static string getDefaultCollationName(QueryConnection conn, Space sp)
        {
            string defaultCollation = null;
            if (sp.ConnectString.StartsWith("jdbc:sqlserver://"))
            {
                QueryCommand cmd = conn.CreateCommand();
                cmd.CommandText = "SELECT SERVERPROPERTY('Collation') AS ProductCollation";

                QueryReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    defaultCollation = reader.IsDBNull(0) ? null : reader.GetString(0);
                }
            }
            return defaultCollation;
        }

        public static List<string> getDatabaseCollations(QueryConnection conn, string schema, Space sp)
        {
            List<string> collationList = new List<string>();
            if (sp.ConnectString.StartsWith("jdbc:sqlserver://"))
            {
                QueryCommand cmd = conn.CreateCommand();
                cmd.CommandText = "SELECT NAME FROM FN_HELPCOLLATIONS()";

                QueryReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    String collationSeq = reader.IsDBNull(0) ? null : reader.GetString(0);
                    collationList.Add(collationSeq);
                }
            }
            return collationList;
        }

        internal static string getResetEndDateUserQuery(string schema)
        {
            string sql = "UPDATE " + getTableName(schema, USER_PRODUCT_TABLE) +
                " SET EndDate=? " +
                " WHERE USER_ID=?";
            return sql;
        }

        internal static void resetEndDate(QueryConnection conn, string schema, User user, int endDurationDays)
        {
            QueryCommand cmd = null;
            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = getResetEndDateUserQuery(schema);
                DateTime endDate = DateTime.Now.AddDays(endDurationDays);
                cmd.Parameters.Add("@Enddate", OdbcType.DateTime).Value = truncateDateTime(endDate);
                cmd.Parameters.Add("@USER_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(user.ID);
                cmd.ExecuteNonQuery();
            }
            finally
            {
                if (cmd != null)
                    cmd.Dispose();
            }
        }

        public static void initializeRepository(Space sp)
        {
            string repositorycmdfilename = sp.Directory + "\\" + "repository.cmd";
            StringBuilder commands = new StringBuilder();
            commands.Append("repository " + sp.Directory + "\\repository.xml\n");

            File.WriteAllText(repositorycmdfilename, commands.ToString());

            string enginecmd = Util.getEngineCommand(sp);

            //Unload tables
            ProcessStartInfo psi = new ProcessStartInfo(enginecmd);
            psi.Arguments = sp.Directory + "\\repository.cmd " + sp.Directory;
            psi.WindowStyle = ProcessWindowStyle.Hidden;
            psi.RedirectStandardOutput = true;
            psi.CreateNoWindow = true;
            psi.UseShellExecute = false;
            psi.RedirectStandardInput = false;
            psi.RedirectStandardError = true;
            psi.WorkingDirectory = (new FileInfo(enginecmd)).Directory.FullName;
            Process proc = Process.Start(psi);
            proc.WaitForExit();
            string line = null;
            while ((line = proc.StandardError.ReadLine()) != null)
            {
                Global.systemLog.Warn(line);
            }
            while ((line = proc.StandardOutput.ReadLine()) != null)
            {
                Global.systemLog.Info(line);
            }
            if (proc.HasExited)
            {
                Global.systemLog.Info("Repository (" + sp.ID + ") Exited with code: " + proc.ExitCode);
            }
            else
            {
                Global.systemLog.Info("Repository (" + sp.ID + ") - Running");
                proc.WaitForExit();
            }
            proc.Close();
        }

        public static Dictionary<string, string> getOverrideParametersWithLocale(QueryConnection conn, string schema, Guid accountID, string Locale)
        {
            Dictionary<string, string> overrideParametersWithoutLocale = new Dictionary<string, string>();
            Dictionary<string, string> overrideParametersWithLocale = new Dictionary<string, string>();
            try
            {
                {
                    QueryCommand cmd = conn.CreateCommand();
                    QueryReader reader = null;
                    cmd.CommandText = "SELECT PARAMETER_NAME, PARAMETER_VALUE FROM " + schema + "." + ACCOUNT_OVERRIDE_PARAMETERS_TABLE;
                    cmd.CommandText += " WHERE ACCOUNT_ID=?";
                    cmd.Parameters.Add("ACCOUNT_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(Guid.Empty);
                    reader = cmd.ExecuteReader();
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            string name = null;
                            string value = null;

                            if (!reader.IsDBNull(0))
                                name = reader.GetString(0);
                            else
                                name = string.Empty;

                            if (!reader.IsDBNull(1))
                                value = reader.GetString(01);
                            else
                                value = string.Empty;
                            if (name != null && value != null)
                            {
                                if (name.StartsWith(OverrideParameterNames.SUPPORT_URL))
                                {
                                    if (name.StartsWith(OverrideParameterNames.SUPPORT_URL) && Locale != null && name.EndsWith(Locale))
                                    {
                                        if (!overrideParametersWithLocale.ContainsKey(OverrideParameterNames.SUPPORT_URL))
                                        {
                                            overrideParametersWithLocale.Add(OverrideParameterNames.SUPPORT_URL, value);
                                        }
                                    }
                                    else if (name.Equals(OverrideParameterNames.SUPPORT_URL))
                                    {
                                        if (!overrideParametersWithoutLocale.ContainsKey(OverrideParameterNames.SUPPORT_URL))
                                        {
                                            overrideParametersWithoutLocale.Add(OverrideParameterNames.SUPPORT_URL, value);
                                        }
                                    }
                                }

                                if (name.StartsWith(OverrideParameterNames.HELP_URL))
                                {
                                    if (name.StartsWith(OverrideParameterNames.HELP_URL) && Locale != null && name.EndsWith(Locale))
                                    {
                                        if (!overrideParametersWithLocale.ContainsKey(OverrideParameterNames.HELP_URL))
                                        {
                                            overrideParametersWithLocale.Add(OverrideParameterNames.HELP_URL, value);
                                        }
                                    }

                                    else if (name.Equals(OverrideParameterNames.HELP_URL))
                                    {
                                        if (!overrideParametersWithoutLocale.ContainsKey(OverrideParameterNames.HELP_URL))
                                        {
                                            overrideParametersWithoutLocale.Add(OverrideParameterNames.HELP_URL, value);
                                        }
                                    }
                                }

                                if (name.StartsWith(OverrideParameterNames.TERMS_OF_SERVICE_URL))
                                {
                                    if (name.StartsWith(OverrideParameterNames.TERMS_OF_SERVICE_URL) && Locale != null && name.EndsWith(Locale))
                                    {
                                        if (!overrideParametersWithLocale.ContainsKey(OverrideParameterNames.TERMS_OF_SERVICE_URL))
                                        {
                                            overrideParametersWithLocale.Add(OverrideParameterNames.TERMS_OF_SERVICE_URL, value);
                                        }
                                    }

                                    else if (name.Equals(OverrideParameterNames.TERMS_OF_SERVICE_URL))
                                    {
                                        if (!overrideParametersWithoutLocale.ContainsKey(OverrideParameterNames.TERMS_OF_SERVICE_URL))
                                        {
                                            overrideParametersWithoutLocale.Add(OverrideParameterNames.TERMS_OF_SERVICE_URL, value);
                                        }
                                    }
                                }

                                if (name.StartsWith(OverrideParameterNames.PRIVACY_POLICY_URL))
                                {
                                    if (name.StartsWith(OverrideParameterNames.PRIVACY_POLICY_URL) && Locale != null && name.EndsWith(Locale))
                                    {
                                        if (!overrideParametersWithLocale.ContainsKey(OverrideParameterNames.PRIVACY_POLICY_URL))
                                        {
                                            overrideParametersWithLocale.Add(OverrideParameterNames.PRIVACY_POLICY_URL, value);
                                        }
                                    }
                                    else if (name.Equals(OverrideParameterNames.PRIVACY_POLICY_URL))
                                    {
                                        if (!overrideParametersWithoutLocale.ContainsKey(OverrideParameterNames.PRIVACY_POLICY_URL))
                                        {
                                            overrideParametersWithoutLocale.Add(OverrideParameterNames.PRIVACY_POLICY_URL, value);
                                        }
                                    }
                                }

                                if (name.StartsWith(OverrideParameterNames.CONTACT_US_URL))
                                {
                                    if (name.StartsWith(OverrideParameterNames.CONTACT_US_URL) && Locale != null && name.EndsWith(Locale))
                                    {
                                        if (!overrideParametersWithLocale.ContainsKey(OverrideParameterNames.CONTACT_US_URL))
                                        {
                                            overrideParametersWithLocale.Add(OverrideParameterNames.CONTACT_US_URL, value);
                                        }
                                    }
                                    else if (name.Equals(OverrideParameterNames.CONTACT_US_URL))
                                    {
                                        if (!overrideParametersWithoutLocale.ContainsKey(OverrideParameterNames.CONTACT_US_URL))
                                        {
                                            overrideParametersWithoutLocale.Add(OverrideParameterNames.CONTACT_US_URL, value);
                                        }
                                    }
                                }

                                if (name.StartsWith(OverrideParameterNames.COPYRIGHT_TEXT))
                                {
                                    if (name.StartsWith(OverrideParameterNames.COPYRIGHT_TEXT) && Locale != null && name.EndsWith(Locale))
                                    {
                                        if (!overrideParametersWithLocale.ContainsKey(OverrideParameterNames.COPYRIGHT_TEXT))
                                        {
                                            overrideParametersWithLocale.Add(OverrideParameterNames.COPYRIGHT_TEXT, value);
                                        }
                                    }
                                    else if (name.Equals(OverrideParameterNames.COPYRIGHT_TEXT))
                                    {
                                        if (!overrideParametersWithoutLocale.ContainsKey(OverrideParameterNames.COPYRIGHT_TEXT))
                                        {
                                            overrideParametersWithoutLocale.Add(OverrideParameterNames.COPYRIGHT_TEXT, value);
                                        }
                                    }
                                }
                                if (name.StartsWith(OverrideParameterNames.PAGE_TITLE))
                                {
                                    if (name.StartsWith(OverrideParameterNames.PAGE_TITLE) && Locale != null && name.EndsWith(Locale))
                                    {
                                        if (!overrideParametersWithLocale.ContainsKey(OverrideParameterNames.PAGE_TITLE))
                                        {
                                            overrideParametersWithLocale.Add(OverrideParameterNames.PAGE_TITLE, value);
                                        }
                                    }
                                    else if (name.Equals(OverrideParameterNames.PAGE_TITLE))
                                    {
                                        if (!overrideParametersWithoutLocale.ContainsKey(OverrideParameterNames.PAGE_TITLE))
                                        {
                                            overrideParametersWithoutLocale.Add(OverrideParameterNames.PAGE_TITLE, value);
                                        }
                                    }
                                }
                                if (name.StartsWith(OverrideParameterNames.ERROR_URL))
                                {
                                    if (name.StartsWith(OverrideParameterNames.ERROR_URL) && Locale != null && name.EndsWith(Locale))
                                    {
                                        if (!overrideParametersWithLocale.ContainsKey(OverrideParameterNames.ERROR_URL))
                                        {
                                            overrideParametersWithLocale.Add(OverrideParameterNames.ERROR_URL, value);
                                        }
                                    }
                                    else if (name.Equals(OverrideParameterNames.ERROR_URL))
                                    {
                                        if (!overrideParametersWithoutLocale.ContainsKey(OverrideParameterNames.ERROR_URL))
                                        {
                                            overrideParametersWithoutLocale.Add(OverrideParameterNames.ERROR_URL, value);
                                        }
                                    }
                                }
                                if (name.StartsWith(OverrideParameterNames.LOGOUT_URL))
                                {
                                    if (name.StartsWith(OverrideParameterNames.LOGOUT_URL) && Locale != null && name.EndsWith(Locale))
                                    {
                                        if (!overrideParametersWithLocale.ContainsKey(OverrideParameterNames.LOGOUT_URL))
                                        {
                                            overrideParametersWithLocale.Add(OverrideParameterNames.LOGOUT_URL, value);
                                        }
                                    }
                                    else if (name.Equals(OverrideParameterNames.LOGOUT_URL))
                                    {
                                        if (!overrideParametersWithoutLocale.ContainsKey(OverrideParameterNames.LOGOUT_URL))
                                        {
                                            overrideParametersWithoutLocale.Add(OverrideParameterNames.LOGOUT_URL, value);
                                        }
                                    }
                                }

                            }
                        }
                    }
                    try
                    {
                        if (reader != null)
                        {
                            reader.Close();
                        }
                        if (cmd != null)
                        {
                            cmd.Dispose();
                        }
                    }
                    catch (Exception)
                    {
                        //Global.systemLog.Warn("Cannot close cmd/result object. Let system handle", ex2);
                    }
                }
                if (accountID != Guid.Empty && accountID != null)
                {
                    QueryCommand cmd = conn.CreateCommand();
                    QueryReader reader = null;
                    cmd.CommandText =
                    "SELECT B.PARAMETER_NAME, B.PARAMETER_VALUE FROM " + schema + "." + ACCOUNTS_TABLE +
                    " A INNER JOIN " + schema + "." + ACCOUNT_OVERRIDE_PARAMETERS_TABLE + " B ON B.ACCOUNT_ID=A.ACCOUNT_ID";
                    cmd.CommandText += " WHERE A.ACCOUNT_ID=?";
                    cmd.Parameters.Add("ACCOUNT_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(accountID);
                    reader = cmd.ExecuteReader();
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            string name = null;
                            string value = null;

                            if (!reader.IsDBNull(0))
                                name = reader.GetString(0);
                            else
                                name = string.Empty;

                            if (!reader.IsDBNull(1))
                                value = reader.GetString(01);
                            else
                                value = string.Empty;

                            if (name != null && value != null)
                            {
                                if (name.StartsWith(OverrideParameterNames.SUPPORT_URL))
                                {
                                    if (name.StartsWith(OverrideParameterNames.SUPPORT_URL) && Locale != null && name.EndsWith(Locale))
                                    {
                                        if (overrideParametersWithLocale.ContainsKey(OverrideParameterNames.SUPPORT_URL))
                                        {
                                            overrideParametersWithLocale[OverrideParameterNames.SUPPORT_URL] = value;
                                        }
                                        else
                                        {
                                            overrideParametersWithLocale.Add(OverrideParameterNames.SUPPORT_URL, value);
                                        }
                                    }

                                    else if (name.Equals(OverrideParameterNames.SUPPORT_URL))
                                    {
                                        if (overrideParametersWithoutLocale.ContainsKey(OverrideParameterNames.SUPPORT_URL))
                                        {
                                            overrideParametersWithoutLocale[OverrideParameterNames.SUPPORT_URL] = value;
                                        }
                                        else
                                        {
                                            overrideParametersWithoutLocale.Add(OverrideParameterNames.SUPPORT_URL, value);
                                        }
                                    }
                                }

                                if (name.StartsWith(OverrideParameterNames.HELP_URL))
                                {
                                    if (name.StartsWith(OverrideParameterNames.HELP_URL) && Locale != null && name.EndsWith(Locale))
                                    {
                                        if (overrideParametersWithLocale.ContainsKey(OverrideParameterNames.HELP_URL))
                                        {
                                            overrideParametersWithLocale[OverrideParameterNames.HELP_URL] = value;
                                        }
                                        else
                                        {
                                            overrideParametersWithLocale.Add(OverrideParameterNames.HELP_URL, value);
                                        }
                                    }

                                    else if (name.Equals(OverrideParameterNames.HELP_URL))
                                    {
                                        if (overrideParametersWithoutLocale.ContainsKey(OverrideParameterNames.HELP_URL))
                                        {
                                            overrideParametersWithoutLocale[OverrideParameterNames.HELP_URL] = value;
                                        }
                                        else
                                        {
                                            overrideParametersWithoutLocale.Add(OverrideParameterNames.HELP_URL, value);
                                        }
                                    }
                                }

                                if (name.StartsWith(OverrideParameterNames.TERMS_OF_SERVICE_URL))
                                {
                                    if (name.StartsWith(OverrideParameterNames.TERMS_OF_SERVICE_URL) && Locale != null && name.EndsWith(Locale))
                                    {
                                        if (overrideParametersWithLocale.ContainsKey(OverrideParameterNames.TERMS_OF_SERVICE_URL))
                                        {
                                            overrideParametersWithLocale[OverrideParameterNames.TERMS_OF_SERVICE_URL] = value;
                                        }
                                        else
                                        {
                                            overrideParametersWithLocale.Add(OverrideParameterNames.TERMS_OF_SERVICE_URL, value);
                                        }
                                    }

                                    else if (name.Equals(OverrideParameterNames.TERMS_OF_SERVICE_URL))
                                    {
                                        if (overrideParametersWithoutLocale.ContainsKey(OverrideParameterNames.TERMS_OF_SERVICE_URL))
                                        {
                                            overrideParametersWithoutLocale[OverrideParameterNames.TERMS_OF_SERVICE_URL] = value;
                                        }
                                        else
                                        {
                                            overrideParametersWithoutLocale.Add(OverrideParameterNames.TERMS_OF_SERVICE_URL, value);
                                        }
                                    }
                                }

                                if (name.StartsWith(OverrideParameterNames.PRIVACY_POLICY_URL))
                                {
                                    if (name.StartsWith(OverrideParameterNames.PRIVACY_POLICY_URL) && Locale != null && name.EndsWith(Locale))
                                    {
                                        if (overrideParametersWithLocale.ContainsKey(OverrideParameterNames.PRIVACY_POLICY_URL))
                                        {
                                            overrideParametersWithLocale[OverrideParameterNames.PRIVACY_POLICY_URL] = value;
                                        }
                                        else
                                        {
                                            overrideParametersWithLocale.Add(OverrideParameterNames.PRIVACY_POLICY_URL, value);
                                        }
                                    }
                                    else if (name.Equals(OverrideParameterNames.PRIVACY_POLICY_URL))
                                    {
                                        if (overrideParametersWithoutLocale.ContainsKey(OverrideParameterNames.PRIVACY_POLICY_URL))
                                        {
                                            overrideParametersWithoutLocale[OverrideParameterNames.PRIVACY_POLICY_URL] = value;
                                        }
                                        else
                                        {
                                            overrideParametersWithoutLocale.Add(OverrideParameterNames.PRIVACY_POLICY_URL, value);
                                        }
                                    }
                                }

                                if (name.StartsWith(OverrideParameterNames.CONTACT_US_URL))
                                {
                                    if (name.StartsWith(OverrideParameterNames.CONTACT_US_URL) && Locale != null && name.EndsWith(Locale))
                                    {
                                        if (overrideParametersWithLocale.ContainsKey(OverrideParameterNames.CONTACT_US_URL))
                                        {
                                            overrideParametersWithLocale[OverrideParameterNames.CONTACT_US_URL] = value;
                                        }
                                        else
                                        {
                                            overrideParametersWithLocale.Add(OverrideParameterNames.CONTACT_US_URL, value);
                                        }
                                    }
                                    else if (name.Equals(OverrideParameterNames.CONTACT_US_URL))
                                    {
                                        if (overrideParametersWithoutLocale.ContainsKey(OverrideParameterNames.CONTACT_US_URL))
                                        {
                                            overrideParametersWithoutLocale[OverrideParameterNames.CONTACT_US_URL] = value;
                                        }
                                        else
                                        {
                                            overrideParametersWithoutLocale.Add(OverrideParameterNames.CONTACT_US_URL, value);
                                        }
                                    }
                                }
                                if (name.StartsWith(OverrideParameterNames.COPYRIGHT_TEXT))
                                {
                                    if (name.StartsWith(OverrideParameterNames.COPYRIGHT_TEXT) && Locale != null && name.EndsWith(Locale))
                                    {
                                        if (overrideParametersWithLocale.ContainsKey(OverrideParameterNames.COPYRIGHT_TEXT))
                                        {
                                            overrideParametersWithLocale[OverrideParameterNames.COPYRIGHT_TEXT] = value;
                                        }
                                        else
                                        {
                                            overrideParametersWithLocale.Add(OverrideParameterNames.COPYRIGHT_TEXT, value);
                                        }
                                    }
                                    else if (name.Equals(OverrideParameterNames.COPYRIGHT_TEXT))
                                    {
                                        if (overrideParametersWithoutLocale.ContainsKey(OverrideParameterNames.COPYRIGHT_TEXT))
                                        {
                                            overrideParametersWithoutLocale[OverrideParameterNames.COPYRIGHT_TEXT] = value;
                                        }
                                        else
                                        {
                                            overrideParametersWithoutLocale.Add(OverrideParameterNames.COPYRIGHT_TEXT, value);
                                        }
                                    }
                                }
                                if (name.StartsWith(OverrideParameterNames.PAGE_TITLE))
                                {
                                    if (name.StartsWith(OverrideParameterNames.PAGE_TITLE) && Locale != null && name.EndsWith(Locale))
                                    {
                                        if (overrideParametersWithLocale.ContainsKey(OverrideParameterNames.PAGE_TITLE))
                                        {
                                            overrideParametersWithLocale[OverrideParameterNames.PAGE_TITLE] = value;
                                        }
                                        else
                                        {
                                            overrideParametersWithLocale.Add(OverrideParameterNames.PAGE_TITLE, value);
                                        }
                                    }
                                    else if (name.Equals(OverrideParameterNames.PAGE_TITLE))
                                    {
                                        if (overrideParametersWithoutLocale.ContainsKey(OverrideParameterNames.PAGE_TITLE))
                                        {
                                            overrideParametersWithoutLocale[OverrideParameterNames.PAGE_TITLE] = value;
                                        }
                                        else
                                        {
                                            overrideParametersWithoutLocale.Add(OverrideParameterNames.PAGE_TITLE, value);
                                        }
                                    }
                                }
                                if (name.StartsWith(OverrideParameterNames.ERROR_URL))
                                {
                                    if (name.StartsWith(OverrideParameterNames.ERROR_URL) && Locale != null && name.EndsWith(Locale))
                                    {
                                        if (overrideParametersWithLocale.ContainsKey(OverrideParameterNames.ERROR_URL))
                                        {
                                            overrideParametersWithLocale[OverrideParameterNames.ERROR_URL] = value;
                                        }
                                        else
                                        {
                                            overrideParametersWithLocale.Add(OverrideParameterNames.ERROR_URL, value);
                                        }
                                    }
                                    else if (name.Equals(OverrideParameterNames.ERROR_URL))
                                    {
                                        if (overrideParametersWithoutLocale.ContainsKey(OverrideParameterNames.ERROR_URL))
                                        {
                                            overrideParametersWithoutLocale[OverrideParameterNames.ERROR_URL] = value;
                                        }
                                        else
                                        {
                                            overrideParametersWithoutLocale.Add(OverrideParameterNames.ERROR_URL, value);
                                        }
                                    }
                                }
                                if (name.StartsWith(OverrideParameterNames.LOGOUT_URL))
                                {
                                    if (name.StartsWith(OverrideParameterNames.LOGOUT_URL) && Locale != null && name.EndsWith(Locale))
                                    {
                                        if (overrideParametersWithLocale.ContainsKey(OverrideParameterNames.LOGOUT_URL))
                                        {
                                            overrideParametersWithLocale[OverrideParameterNames.LOGOUT_URL] = value;
                                        }
                                        else
                                        {
                                            overrideParametersWithLocale.Add(OverrideParameterNames.LOGOUT_URL, value);
                                        }
                                    }
                                    else if (name.Equals(OverrideParameterNames.LOGOUT_URL))
                                    {
                                        if (overrideParametersWithoutLocale.ContainsKey(OverrideParameterNames.LOGOUT_URL))
                                        {
                                            overrideParametersWithoutLocale[OverrideParameterNames.LOGOUT_URL] = value;
                                        }
                                        else
                                        {
                                            overrideParametersWithoutLocale.Add(OverrideParameterNames.LOGOUT_URL, value);
                                        }
                                    }
                                }
                            }
                        }

                        try
                        {
                            if (reader != null)
                            {
                                reader.Close();
                            }
                            if (cmd != null)
                            {
                                cmd.Dispose();
                            }
                        }
                        catch (Exception)
                        {
                            //Global.systemLog.Warn("Cannot close cmd/result object. Let system handle", ex2);
                        }
                    }
                }


                foreach (var key in overrideParametersWithoutLocale.Keys)
                {
                    string value = overrideParametersWithoutLocale[key];
                    if (!overrideParametersWithLocale.ContainsKey(key))
                        overrideParametersWithLocale.Add(key, value);
                }

                // get all the overrides the old way
                Dictionary<string, string> allOverrides = getOverrideParameters(conn, schema, accountID);
                if (allOverrides != null)
                {
                    foreach (KeyValuePair<string, string> kvPair in allOverrides)
                    {
                        string key = kvPair.Key;
                        if (key != null && !overrideParametersWithLocale.ContainsKey(key))
                        {
                            overrideParametersWithLocale.Add(key, kvPair.Value);
                        }
                    }
                }
            }

            catch (Exception ex)
            {
                Global.systemLog.Error("Exception thrown while getting override parameters", ex);
            }
            return overrideParametersWithLocale;
        }
        public static bool accountExists(QueryConnection conn, string schema, string accountName)
        {
            QueryReader reader = null;
            QueryCommand cmd = null;
            try
            {
                cmd = conn.CreateCommand();

                cmd.CommandText = "SELECT Name FROM" + SINGLE_SPACE + getTableName(schema, ACCOUNTS_TABLE) + SINGLE_SPACE + "WHERE Name = ?";


                cmd.Parameters.Add("Name", OdbcType.VarChar).Value = accountName;
                reader = cmd.ExecuteReader();
                bool result = false;
                while (reader.Read())
                {
                    result = true;
                    break;
                }
                return result;
            }
            finally
            {
                if (reader != null)
                    reader.Close();
                if (cmd != null)
                    cmd.Dispose();
            }
        }

        public static void updateAdminUserParams(QueryConnection conn, string schema, Guid managedUserID)
        {
            QueryCommand cmd = null;           

            // get the account_id of the createdByUser
            try
            {              
                cmd = conn.CreateCommand();
                cmd.CommandText = "UPDATE" + SINGLE_SPACE + getTableName(schema, USER_TABLE_NAME) + SINGLE_SPACE +
                    "SET ADMIN_ACCOUNT_ID = ? WHERE MANAGED_ACCOUNT_ID = ?";
                cmd.Parameters.Add("@ADMIN_ACCOUNT_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(managedUserID);
                cmd.Parameters.Add("@MANAGED_ACCOUNT_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(managedUserID);   
                cmd.ExecuteNonQuery();
            }
            finally
            {
                if (cmd != null)
                    cmd.Dispose();
            }
        }

        public static Guid getAccountID(QueryConnection conn, string schema, string accountName)
        {
            QueryCommand cmd = null;
            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = "SELECT ACCOUNT_ID FROM " + getTableName(schema, ACCOUNTS_TABLE) + " WHERE Name=?";
                cmd.Parameters.Add("Name", OdbcType.VarChar).Value = accountName;
                Object anObject = cmd.ExecuteScalar();
                if (anObject != null || anObject != System.DBNull.Value)
                {
                    return (Guid)anObject;
                }
                return (Guid)anObject;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

        }
        public static Guid getAdminAccountID(QueryConnection conn, string schema, Guid trialAdminAccountID)
        {
            QueryCommand cmd = null;
            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = "SELECT ADMIN_ACCOUNT_ID FROM " + getTableName(schema, ACCOUNTS_TABLE) + " WHERE MANAGED_ACCOUNT_ID=?";
                cmd.Parameters.Add("MANAGED_ACCOUNT_ID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(trialAdminAccountID);
                cmd.Parameters.Add("PKID", OdbcType.VarChar).Value = trialAdminAccountID;
                Object anObject = cmd.ExecuteScalar();
                if (anObject != null || anObject != System.DBNull.Value)
                {
                    return (Guid)anObject;
                }
                return (Guid)anObject;
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }

        }

        public static Guid getSpaceIDForAccount(QueryConnection conn, string schema, Guid userID)
        {
            QueryCommand cmd = null;
            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = "SELECT SPACE_ID FROM " + schema + "." + USER_TABLE_NAME + " A" +
                    " INNER JOIN " + schema + "." + USER_TABLE_NAME + " B ON A.MANAGED_ACCOUNT_ID = B.ADMIN_ACCOUNT_ID" +
                    " INNER JOIN " + schema + "." + USER_SPACE_INXN + " C ON B.PKID = C.USER_ID" +
                    " WHERE A.PKID=?";
                cmd.Parameters.Add("PKID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(userID);
                Object anObject = cmd.ExecuteScalar();
                if (anObject != null && anObject != System.DBNull.Value)
                {
                    return (Guid)anObject;
                }
                else
                {
                    return Guid.Empty;
                }
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }
        }

        public static Guid getSpaceForUser(QueryConnection conn, string schema, Guid userID)
        {
            QueryCommand cmd = null;
            try
            {
                cmd = conn.CreateCommand();
                cmd.CommandText = "SELECT SPACE_ID FROM " + schema + "." + USER_TABLE_NAME +
                    " A INNER JOIN " + schema + "." + USER_SPACE_INXN + " B ON A.PKID=B.USER_ID" +                  
                    " WHERE A.PKID=?";
                cmd.Parameters.Add("PKID", conn.getODBCUniqueIdentiferType()).Value = conn.getUniqueIdentifer(userID);
                Object anObject = cmd.ExecuteScalar();
                if (anObject != null && anObject != System.DBNull.Value)
                {
                    return (Guid)anObject;
                }
                else
                {
                    return Guid.Empty;
                }                
            }
            finally
            {
                if (cmd != null)
                {
                    cmd.Dispose();
                }
            }
        }

    }


    [Serializable]
    public class Language
    {
        public string code;
        public string description;

        public Language()
        {
        }

        public Language(string code, string description)
        {
            this.code = code;
            this.description = description;
        }
    }
}
