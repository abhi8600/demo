﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Acorn.Exceptions
{
    public class UserManagementNotAuthorizedException : BirstException
    {
        public UserManagementNotAuthorizedException()
            : base()
        {
        }

        public UserManagementNotAuthorizedException(string msg)
            : base(msg)
        {
        }

        public UserManagementNotAuthorizedException(string msg, Exception inner)
            : base(msg, inner)
        {
        }

        public override int getErrorType()
        {
            return BirstException.ERROR_UNAUTHORIZED_USER_MANAGEMENT;
        }

        public override string getErrorMessage()
        {
            return MSG_USER_MANAGEMENT_NOT_AUTHORIZED;
        }
    }
}
