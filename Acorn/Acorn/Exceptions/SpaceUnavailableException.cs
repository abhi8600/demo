﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Acorn.Utils;

namespace Acorn.Exceptions
{
    public class SpaceUnavailableException : BirstException
    {
        int code = BirstException.ERROR_SPACE_UNAVAILABLE;
        string errorMsg = BirstException.MSG_SPACE_UNAVAILABLE;

        public SpaceUnavailableException(int operationCode) : base()
        {
            populateErrorCode(operationCode);
        }

        public SpaceUnavailableException(int operationCode, string msg)
            : base(msg)
        {
            populateErrorCode(operationCode);
            errorMsg = msg;
        }

        public SpaceUnavailableException(int operationCode, string msg, Exception inner)
            : base(msg, inner)
        {
            populateErrorCode(operationCode);
            errorMsg = msg;
        }

        public override int getErrorType()
        {
            return code;
        }

        public override string getErrorMessage()
        {
            return errorMsg;
        }

        private void populateErrorCode(int opcode)
        {
            switch (opcode)
            {
                case (SpaceOpsUtils.OP_COPY_FROM):
                    code = SpaceUnavailableException.ERROR_SPACE_UNAVAILABLE_COPY_FROM;
                    break;
                case (SpaceOpsUtils.OP_COPY_TO_NEW):
                    code = SpaceUnavailableException.ERROR_SPACE_UNAVAILABLE_COPY_TO_NEW;
                    break;
                case (SpaceOpsUtils.OP_COPY_TO_EXISTING):
                    code = SpaceUnavailableException.ERROR_SPACE_UNAVAILABLE_COPY_TO_EXISTING;
                    break;
                case (SpaceOpsUtils.OP_SWAP):
                    code = SpaceUnavailableException.ERROR_SPACE_UNAVAILABLE_SWAP;
                    break;
                case (SpaceOpsUtils.OP_DELETE_DATA):
                    code = SpaceUnavailableException.ERROR_SPACE_UNAVAILABLE_DELETE_DATA;
                    break;
                case (SpaceOpsUtils.OP_DELETE_SPACE):
                    code = SpaceUnavailableException.ERROR_SPACE_UNAVAILABLE_DELETE_SPACE;
                    break;
                default:
                    code = SpaceUnavailableException.ERROR_SPACE_UNAVAILABLE;
                    break;
            }
        }
    }
}
