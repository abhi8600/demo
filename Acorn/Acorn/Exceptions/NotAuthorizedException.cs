﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Acorn.Exceptions
{
    public class NotAuthorizedException : BirstException
    {
        public NotAuthorizedException()
            : base("Acorn.Exceptions.NotAuthorizedException: ") // need to clean this up, need to override Message XXX
        {
        }

        public NotAuthorizedException(string msg)
            : base("Acorn.Exceptions.NotAuthorizedException: " + msg)
        {
        }

        public NotAuthorizedException(string msg, Exception inner)
            : base("Acorn.Exceptions.NotAuthorizedException: " + msg, inner)
        {
        }

        public override int getErrorType()
        {
            return BirstException.ERROR_NOT_AUTHORIZED;
        }

        public override string getErrorMessage()
        {
            return BirstException.MSG_NOT_AUTHORIZED;
        }
    }
}
