﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Acorn.Exceptions
{
    public class SpaceNotFoundException : BirstException
    {
         public SpaceNotFoundException() : base()
        {
        }

        public SpaceNotFoundException(string msg)
            : base(msg)
        {
        }

        public SpaceNotFoundException(string msg, Exception inner)
            : base(msg, inner)
        {
        }

        public override int getErrorType()
        {
            return BirstException.ERROR_SPACE_NOT_FOUND;
        }

        public override string getErrorMessage()
        {
            return BirstException.MSG_SPACE_NOT_FOUND;
        }
    }
}
