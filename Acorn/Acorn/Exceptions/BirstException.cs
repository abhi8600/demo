﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Acorn.Exceptions
{
    public class BirstException: Exception
    {
        
        // Starting all the error codes with 11 so that it does not clash with error codes used before.
        // Will remove the other error codes later on once application uses this class only to report
        // error codes
               

        //NOTE CAUTION:  Used for redirection to on the client side
        public static int ERROR_REDIRECT_LOGIN                      = -118;

        public static int ERROR_GENERAL                             = -119;
        public static int ERROR_SPACE_NOT_FOUND                     = -120;
        public static int ERROR_UNAUTHORIZED_USER_MANAGEMENT        = -121;

        public static int ERROR_SPACE_UNAVAILABLE                   = -122;
        public static int ERROR_SPACE_UNAVAILABLE_COPY_TO_NEW       = -123;
        public static int ERROR_SPACE_UNAVAILABLE_COPY_TO_EXISTING  = -124;
        public static int ERROR_SPACE_UNAVAILABLE_COPY_FROM         = -125;
        public static int ERROR_SPACE_UNAVAILABLE_SWAP              = -126;
        public static int ERROR_SPACE_UNAVAILABLE_DELETE_DATA       = -127;
        public static int ERROR_SPACE_UNAVAILABLE_DELETE_SPACE      = -128;

        public static int ERROR_PACKAGE_NAME_EXISTS = -529;
        public static int ERROR_PACKAGE_NAME_INVALID = -530;
        public static int ERROR_PACKAGE_IMPORT_COLLISION_NAME_STAGING_TABLE = -531;
        public static int ERROR_PACKAGE_IMPORT_COLLISION_NAME_DIMENSION_TABLE = -532;
        public static int ERROR_PACKAGE_IMPORT_COLLISION_NAME_MEASURE_TABLE = -533;
        public static int ERROR_PACKAGE_IMPORT_COLLISION_NAME_VARIABLE = -534;
        public static int ERROR_PACKAGE_IMPORT_COLLISION_NAME_HIERARCHY = -535;
        public static int ERROR_PACKAGE_NO_OBJECTS_FOUND = -536;
        public static int ERROR_PACKAGE_NOT_FOUND = -537;
        public static int ERROR_PACKAGE_SOURCE_IMPORT_SAME_SPACE = -538;
        public static int ERROR_IMPORT_PACKAGE_ALREADY_EXISTS = -539;  

        // SFDC
        public static int ERROR_SFDC_CREDENTIALS_LOGIN = -540;
        public static int ERROR_SFDC_CRENDETIALS_EXPIRED = -541;
        public static int ERROR_SFDC_EXTRACT_LOAD_IN_PROGRESS = -542;
        public static int ERROR_SFDC_EXTRACT_ANOTHER_IN_PROGRESS = -543;

        // Connector
        public static int ERROR_CONNECTOR_CREDENTIALS_LOGIN = -540;
        public static int ERROR_CONNECTOR_CRENDETIALS_EXPIRED = -541;
        public static int ERROR_CONNECTOR_EXTRACT_LOAD_IN_PROGRESS = -542;
        public static int ERROR_CONNECTOR_EXTRACT_ANOTHER_IN_PROGRESS = -543;

        // DB Schema
        public static int ERROR_DB_SCHEMA_NOT_FOUND = -551;
        public static int ERROR_DB_SCHEMA_IN_USE = -552;

        public static int ERROR_NOT_AUTHORIZED = -600;
        public static int ERROR_NOT_ALLOWED = -601;

        // Need to move all the error messages to client side
        public static string MSG_GENERAL_ERROR = "Internal Error";
        public static string MSG_SPACE_NOT_FOUND = "Unable to find space details";
        public static string MSG_USER_MANAGEMENT_NOT_AUTHORIZED = "User not authorized for User Management Activities";
        public static string MSG_SPACE_UNAVAILABLE = "Space Unavailable for the operation";
        public static string MSG_NOT_AUTHORIZED = "User is not authorized for this operation";
        public static string MSG_NOT_ALLOWED = "User tried to perform an operation that is not allowed";
        

        public BirstException()
            : base("")
        {
        }

        public BirstException(string message)
            : base(message)
        {            
        }

        public BirstException(string message, Exception inner)
            : base(message, inner)
        {

        }

        public virtual int getErrorType()
        {
            return ERROR_GENERAL;
        }

        public virtual string getErrorMessage()
        {
            return null;
        }

    }
}
