﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Acorn.Exceptions
{
    public class SchedulerException:BirstException
    {

        public SchedulerException() : base()
        {
        }

        public SchedulerException(string msg)
            : base(msg)
        {
        }

        public SchedulerException(string msg, Exception inner)
            : base(msg, inner)
        {
        }

        public override int getErrorType()
        {
            return BirstException.ERROR_GENERAL;
        }

        public override string getErrorMessage()
        {
            return BirstException.MSG_GENERAL_ERROR;
        }
    }
}