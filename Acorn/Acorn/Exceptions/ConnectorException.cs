﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Acorn.Exceptions
{
    public class ConnectorException : BirstException
    {
        int code = ERROR_GENERAL;
        string errorMsg = MSG_GENERAL_ERROR;

        public ConnectorException()
            : base()
        {
        }

        public ConnectorException(int errorCode)
            : base()
        {
            populateError(errorCode, null);
        }

        public ConnectorException(int errorCode, string msg)
            : base(msg)
        {
            populateError(errorCode, msg);
            errorMsg = msg;
        }

        public ConnectorException(int errrorCode, string msg, Exception inner)
            : base(msg, inner)
        {
            populateError(errrorCode, msg);
            errorMsg = msg;
        }

        public override int getErrorType()
        {
            return code;
        }

        public override string getErrorMessage()
        {
            return errorMsg;
        }

        private void populateError(int errorCode, string errorMessage)
        {
            code = errorCode;
            if (errorMessage != null)
            {
                errorMsg = errorMessage;
            }
        }
    }
}