﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Acorn.Exceptions
{
    public class NotAllowedException : BirstException
    {
        public NotAllowedException()
            : base("Acorn.Exceptions.NotAllowedException: ")
        {
        }

        public NotAllowedException(string msg)
            : base("Acorn.Exceptions.NotAllowedException: " + msg)
        {
        }

        public NotAllowedException(string msg, Exception inner)
            : base("Acorn.Exceptions.NotAllowedException: " + msg, inner)
        {
        }

        public override int getErrorType()
        {
            return BirstException.ERROR_NOT_ALLOWED;
        }

        public override string getErrorMessage()
        {
            return BirstException.MSG_NOT_ALLOWED;
        }
    }
}
