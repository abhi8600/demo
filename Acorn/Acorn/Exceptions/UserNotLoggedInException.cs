﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Acorn.Exceptions
{
    public class UserNotLoggedInException : BirstException
    {
        public UserNotLoggedInException() : base("Acorn.Exceptions.UserNotLoggedInException: ")
        {

        }

        public UserNotLoggedInException(string msg)
            : base("Acorn.Exceptions.UserNotLoggedInException: " + msg)
        {

        }

        public UserNotLoggedInException(string msg, Exception inner)
            : base("Acorn.Exceptions.UserNotLoggedInException: " + msg, inner)
        {

        }
                
        public override int getErrorType()
        {
            return BirstException.ERROR_REDIRECT_LOGIN;
        }
    }
}
