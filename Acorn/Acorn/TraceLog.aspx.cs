﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;

namespace Acorn
{
    public partial class TraceLog : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Util.needToChangePassword(Response, Session);
            Util.setUpHeaders(Response);;

            User u = Util.validateAuth(Session, Response);
            Space sp = Util.validateSpace(Session, Response, Request);

            if (u.isFreeTrialUser)
            {
                Response.Write("Not Supported for the Free Trial");
                return;
            }

            if (!DefaultUserPage.isAdhocPermitted(Session, u)) // only those with designer access should see this
            {
                Response.StatusCode = 400;
                return;
            }

            processLogFile(sp.Directory + "/logs/trace.log");
        }

        private void processLogFile(string filename)
        {
            FileInfo fi = new FileInfo(filename);
            if (fi.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=birsttracelog.txt");
                Response.ContentType = "text/plain";
                Response.WriteFile(filename);
                Response.Flush();
                Response.End();
            }
            else
            {
                Response.Clear();
                Response.ContentType = "text/plain";
                Response.Write("NO DATA");
                Response.Flush();
                Response.End();
            }
        }
    }
}