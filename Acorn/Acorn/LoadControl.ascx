﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LoadControl.ascx.cs"
    Inherits="Acorn.LoadControl" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Panel ID="ErrorPanel" runat="server" Visible="false" ForeColor="Red">
    <asp:Label ID="ErrorLabel" runat="server">Failed
to process data, please verify your data format matches that defined. Please contact
Birst technical support for more help if needed.</asp:Label>
</asp:Panel>
<asp:Panel ID="ScriptFailurePanel" runat="server" Visible="false">
    <div style="height: 5px">
        &nbsp;</div>
    <div style="color: Red">
        Failed to process data due to script failure.</div>
    <table cellspacing="3">
        <tr>
            <td>
                Source:
            </td>
            <td>
                <asp:Label ID="ScriptErrorSourceLabel" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                Description:
            </td>
            <td>
                <asp:Label ID="ScriptErrorMessage" runat="server"></asp:Label>
            </td>
        </tr>
    </table>
    <div style="height: 5px">
        &nbsp;</div>
</asp:Panel>
<asp:Panel ID="ButtonPanel" runat="server" Width="100%" Visible="false">
    <table width="100%" cellspacing="0" cellpadding="0">
        <tr>
            <td align="center" style="width: 50px; padding: 5px">
                <asp:ImageButton ID="UpdateCredentialsButton" runat="server" ImageUrl="~/Images/SFDC_Process_data.png"
                    ToolTip="Process last set of data retrieved from Salesforce.com" OnClick="loadButton_Click" />
            </td>
            <td>
                <asp:LinkButton ID="processButton" runat="server" ToolTip="Process Data" OnClick="loadButton_Click"
                    CssClass="defaultlinkstyle">Process last set of data retrieved from Salesforce.com</asp:LinkButton>
            </td>
        </tr>
    </table>
</asp:Panel>
<asp:Panel ID="LoadPanel" runat="server" Width="100%">
    <asp:Panel CssClass="wizard" runat="server" ID="WizardPanel">
        <table cellpadding="5" cellspacing="3">
            <asp:PlaceHolder ID="LoadOptionsPlaceholder" runat="server">
                <tr>
                    <td colspan="2" style="text-align: center; padding: 10px">
                        Publishing Timeline
                    </td>
                </tr>
                <tr>
                    <td>
                        Current Load Number:
                    </td>
                    <td>
                        <asp:Label ID="LoadNumLabel" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        Date to use for processing:
                    </td>
                    <td>
                        <asp:TextBox ID="LoadDateBox" runat="server"></asp:TextBox><asp:ImageButton ID="CalImageButton"
                            ImageUrl="~/Images/Calendar.png" runat="server" />
                        <cc1:CalendarExtender ID="LoadDateBox_CalendarExtender" runat="server" Enabled="True"
                            TargetControlID="LoadDateBox" PopupButtonID="CalImageButton" Format="MMMM d, yyyy">
                        </cc1:CalendarExtender>
                    </td>
                </tr>
                <asp:PlaceHolder ID="OverviewDashboardOption" runat="server" Visible="true">
                    <tr>
                        <td>
                            <asp:Label ID="CreateOverviewLabel" runat="server" Visible="true">Create Overview QuickDashboards<sup><font style="font-size:6pt">TM</font></sup>:</asp:Label>
                            <asp:Label ID="RecreateOverviewLabel" runat="server" Visible="false">Re-create Overview QuickDashboards<sup><font style="font-size:6pt">TM</font></sup>:</asp:Label>
                        </td>
                        <td>
                            <asp:CheckBox ID="OverviewDashboardBox" runat="server" Checked="true" />
                        </td>
                    </tr>
                </asp:PlaceHolder>
            </asp:PlaceHolder>
            <asp:Panel ID="LastPublishPanel" runat="server" Visible="false">
                <tr>
                    <td colspan="2" style="font-weight: bold">
                        Recent processing history
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:GridView ID="PublishHistory" runat="server" AutoGenerateColumns="False" CellPadding="0"
                            ForeColor="#333333" GridLines="None" Width="600px">
                            <RowStyle BackColor="White" ForeColor="#333333" CssClass="listviewcell" />
                            <Columns>
                                <asp:BoundField DataField="LoadNumber" HeaderText="Number" HeaderStyle-Width="10%"
                                    ItemStyle-HorizontalAlign="Center"></asp:BoundField>
                                <asp:BoundField DataField="PublishDate" HeaderText="Date Used" HeaderStyle-Width="20%"
                                    DataFormatString="{0:MM/dd/yyyy}" ItemStyle-HorizontalAlign="Center"></asp:BoundField>
                                <asp:BoundField DataField="PublishStartTime" HeaderText="Start Time" HeaderStyle-Width="25%"
                                    DataFormatString="{0:g}" ItemStyle-HorizontalAlign="Center"></asp:BoundField>
                                <asp:BoundField DataField="PublishEndTime" HeaderText="Completion Time" HeaderStyle-Width="25%"
                                    DataFormatString="{0:g}" ItemStyle-HorizontalAlign="Center"></asp:BoundField>
                                <asp:BoundField DataField="Result" HeaderText="Result" HeaderStyle-Width="20%" ItemStyle-HorizontalAlign="Center">
                                </asp:BoundField>
                                <asp:HyperLinkField Text="detail" DataNavigateUrlFields="LoadNumber,DateTime,LoadGroup,Back"
                                    DataNavigateUrlFormatString="NewPublishLog.aspx?load={0}&date={1:MM-dd-yyyy}&group={2}&back={3}" />
                            </Columns>
                            <HeaderStyle Font-Bold="True" ForeColor="Black" CssClass="listviewheader" />
                            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                        </asp:GridView>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <a href="ViewData.aspx">View Processed Data</a>
                    </td>
                </tr>
            </asp:Panel>
        </table>
    </asp:Panel>
    <asp:PlaceHolder ID="LoadNavigationPlaceholder" runat="server">
        <table width="100%">
            <tr>
                <td align="center" valign="middle" style="height: 40px">
                    <asp:ImageButton ID="ReturnButton" runat="server" ToolTip="Done" PostBackUrl="~/FlexModule.aspx?birst.module=home"
                        ImageUrl="~/Images/Done.png" />
                    <asp:ImageButton ID="loadButton" runat="server" ToolTip="Process Data" OnClick="loadButton_Click"
                        ImageUrl="~/Images/Process.png" />
                </td>
            </tr>
        </table>
    </asp:PlaceHolder>
</asp:Panel>
<asp:Panel ID="LoadingPanel" runat="server" Width="100%" Visible="false">
    <asp:UpdatePanel ID="LoadUpdate" runat="server">
        <ContentTemplate>
            <table width="100%">
                <tr style="height: 300px; vertical-align: middle">
                    <td colspan="3" style="text-align: center; font-size: 12pt; font-weight: bold; width: 100%"
                        align="center">
                        <img alt="Loading Data" src="Images/wait.gif" /><br />
                        <br />
                        Currently Processing Data<br />
                        <div style="font-size: 9pt; font-weight: normal">
                            <asp:Label ID="PhaseLabel" runat="server"></asp:Label></div>
                        <div>
                            <asp:HyperLink ID="DetailLink" runat="server" Font-Size="9pt" Visible="false">Progress Detail</asp:HyperLink></div>
                    </td>
                </tr>
            </table>
            <asp:Timer ID="LoadTimer" runat="server" Interval="5000" OnTick="LoadTimer_Tick">
            </asp:Timer>
            <asp:Panel ID="LogoutPanel" runat="server" CssClass="invisible">
                <iframe src='<%= this.frameSource %>' style="display: none"></iframe>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Panel>
<asp:Panel ID="AggRulePanel" runat="server" Width="100%" Visible="false">
    <div style="height: 15px">
        &nbsp;</div>
    <div>
        You have elected to have Birst create a set of QuickDashboards for you. These dashboards
        provide a quick way to start exploring your data. Birst will take the numeric columns
        you select below and build dashboards showing breakouts of those columns by related
        attributes. Please choose below how you would like Birst to aggregate the values
        in these columns. You may also choose to accept Birst’s default selection of metrics
        and press Continue
    </div>
    <div style="height: 15px">
        &nbsp;</div>
    <asp:ListView ID="MeasureList" runat="server">
        <LayoutTemplate>
            <table id="Table1" runat="server" runat="server" cellspacing="0">
                <tr>
                    <td class="listviewborderheader">
                        Column Name
                    </td>
                    <td align="center" width="17%" class="listviewborderheader">
                        Show Sum of Column
                    </td>
                    <td align="center" width="17%" class="listviewborderheader">
                        Show Average of Column
                    </td>
                    <td align="center" width="17%" class="listviewborderheader">
                        Show Count of Items in Column
                    </td>
                    <td align="center" width="17%" class="listviewborderheader">
                        Do Not Show This Column
                    </td>
                </tr>
                <tr runat="server" id="itemPlaceholder">
                </tr>
            </table>
        </LayoutTemplate>
        <ItemTemplate>
            <tr id="Tr1" runat="server">
                <td style="border: 1px solid #555555">
                    <%# Eval("Name") %>
                </td>
                <td align="center" style="border: 1px solid #555555">
                    <asp:RadioButton ID="SumButton" runat="server" GroupName='<%# Eval("Name") %>' Enabled='<%# Eval("SumVisible") %>'
                        Checked='<%# Eval("SumChecked") %>' />
                </td>
                <td align="center" style="border: 1px solid #555555">
                    <asp:RadioButton ID="AvgButton" runat="server" GroupName='<%# Eval("Name") %>' Enabled='<%# Eval("AvgVisible") %>'
                        Checked='<%# Eval("AvgChecked") %>' />
                </td>
                <td align="center" style="border: 1px solid #555555">
                    <asp:RadioButton ID="CountButton" runat="server" GroupName='<%# Eval("Name") %>'
                        Enabled='<%# Eval("CountVisible") %>' Checked='<%# Eval("CountChecked") %>' />
                </td>
                <td align="center" style="border: 1px solid #555555">
                    <asp:RadioButton ID="IgnoreButton" runat="server" GroupName='<%# Eval("Name") %>' />
                </td>
            </tr>
        </ItemTemplate>
    </asp:ListView>
    <div style="height: 10px">
        &nbsp;</div>
    <asp:ImageButton ID="ContinuePublishButton" runat="server" OnClick="ContinuePublish"
        ImageUrl="~/Images/Continue.png"></asp:ImageButton>
</asp:Panel>
<asp:Panel ID="InvalidPanel" runat="server" Width="100%" Visible="false">
    <table width="100%">
        <tr valign="middle" style="height: 300px">
            <td style="text-align: center; font-size: 12pt; font-weight: bold" align="center">
                <asp:Label ID="UnableToGetStatus" runat="server" Visible="false">Cannot retrieve current status. Please try again.</asp:Label><br />
                <asp:Label ID="ConcurrentProcessError" runat="server" Visible="false">Cannot publish space, it is being published currently.</asp:Label><br />
                <asp:Label ID="SpaceUnavailableError" runat="server" Visible="false"></asp:Label><br />
                <asp:Label ID="InvalidLabel" runat="server">Unable to process data. Return to Manage Data to setup for processing</asp:Label><br />
                <asp:Label ID="RowLimitLabel" runat="server" Visible="false">
                    Unable to process data. The number of rows added would exceed the limit set for your account. 
                    To process more data, please upgrade your account.</asp:Label>
                <asp:Label ID="DataLimitLabel" runat="server" Visible="false">
                    Unable to process data. The amount of data to be processed would exceed the limit set for your account. 
                    To process more data, please upgrade your account.</asp:Label>
                <asp:Label ID="RowPublishLimitLabel" runat="server" Visible="false">
                    Unable to process data. The number of rows added would exceed the amount that you can process at any one time. 
                    To process more data, please contact Birst to upgrade your account.</asp:Label>
                <asp:Label ID="DataPublishLimitLabel" runat="server" Visible="false">
                    Unable to process data. The amount of data to be processed would exceed the amount that you can process at any one time.
                    To process more data, please contact Birst to upgrade your account.</asp:Label>
                <br />
                <asp:HyperLink ID="InvalidLink" runat="server">Click To Continue</asp:HyperLink>
            </td>
        </tr>
    </table>
</asp:Panel>
