﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;
using System.Data.Odbc;
using Performance_Optimizer_Administration;
using System.Collections.Generic;
using log4net;
using System.Threading;
using Acorn.Utils;
using ICSharpCode.SharpZipLib.Zip;
using Acorn.SAMLSSO;
using Acorn.DBConnection;

namespace Acorn
{
    public partial class Repository : System.Web.UI.Page
    {
        public string frameSource = null;
        public string [] dcconfigs = null;
        Dictionary<string, Connection> connectionMap = null;
        Dictionary<string, Connection> localConnectionMap = null;
        private static string mainSchema = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["MainSchema"];

        void Page_Init(object sender, EventArgs e)
        {
            ViewStateUserKey = Session.SessionID;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            User u = Util.validateAuth(Session, Response);
            if (!u.RepositoryAdmin)
            {
                Global.systemLog.Warn("redirecting to home page due to not being a repository admin (Repository.aspx)");
                Response.Redirect(Util.getHomePageURL(Session, Request));
            }
            Space sp = Util.validateSpace(Session, Response, Request);
            Global.systemLog.Info("Repository.aspx page accessed for space " + sp.Name);

            // show user and space information
            SpaceLabel.Text = HttpUtility.HtmlEncode(sp.Name);
            SpaceID.Text = HttpUtility.HtmlEncode(sp.ID.ToString());
            Schema.Text = HttpUtility.HtmlEncode(sp.Schema);
            DirectoryID.Text = HttpUtility.HtmlEncode(sp.Directory);

            // show webserver information
            WebServerLabel.Text = HttpUtility.HtmlEncode(System.Environment.MachineName + " / " + System.Web.Hosting.HostingEnvironment.ApplicationHost.GetSiteName() + " / " + Global.siteID);
            CurrentUser.Text = HttpUtility.HtmlEncode(System.Security.Principal.WindowsIdentity.GetCurrent().Name);
            Version.Text = HttpUtility.HtmlEncode(Util.getAppVersion());

            if (u.OperationsFlag)
            {
                EncryptPanel.Visible = true;
            }

            DataTable dtp = new DataTable();
            dtp.Columns.Add("Product Id");
            dtp.Columns.Add("Account Type");
            dtp.Columns.Add("Product Name");
            dtp.Columns.Add("Quantity");
            dtp.Columns.Add("End Date");
            List<Product> productList = (List<Product>)Session["products"];
            foreach (Product product in productList)
            {
                DataRow dr0 = dtp.NewRow();
                dr0[0] = product.ProductID;
                string type = "Other";
                if (product.Type == Product.TYPE_EDITION)
                    type = "Edition";
                else if (product.Type == Product.TYPE_STORAGE)
                    type = "Storage";
                else if (product.Type == Product.TYPE_INVITATION)
                    type = "Invitation";
                else if (product.Type == Product.TYPE_PROMOTION)
                    type = "Promotion";
                else if (product.Type == Product.TYPE_WEBDAV)
                    type = "Birst Connect";
                else if (product.Type == Product.TYPE_OTHER)
                    type = "Other";
                else if (product.Type == Product.TYPE_REALTIME)
                    type = "Live Access";
                else if (product.Type == Product.TYPE_WEBSERVICE)
                    type = "Web Service";
                else if (product.Type == Product.TYPE_MOBILE)
                    type = "Mobile";
                else if (product.Type == Product.TYPE_SAP_CONNECTOR)
                    type = "SAP Connector";
                else if (product.Type == Product.TYPE_LOCAL_ETL_LOADER)
                    type = "Local ETL Loader";
                dr0[1] = type;
                dr0[2] = product.Name;
                dr0[3] = product.Quantity;
                string enddate = null;
                if (product.EndDate == null || product.EndDate.Year == 1)
                {
                    enddate = "Never";
                }
                else
                {
                    enddate = product.EndDate.ToShortDateString();
                }
                dr0[4] = enddate;
                dtp.Rows.Add(dr0);
            }
            Products.DataSource = dtp;
            Products.DataBind();

            DataTable dtdf = new DataTable();
            dtdf.Columns.Add("Data File");
            dtdf.Columns.Add("Size");
            dtdf.Columns.Add("Date");
            DirectoryInfo di = new DirectoryInfo(sp.Directory + "\\data");
            if (di.Exists)
            {
                FileInfo[] files = di.GetFiles("*.*");
                foreach (FileInfo finfo in files)
                {
                    DataRow dr0 = dtdf.NewRow();
                    dr0[0] = finfo.Name;
                    dr0[1] = finfo.Length;
                    dr0[2] = finfo.LastWriteTime;
                    dtdf.Rows.Add(dr0);
                }
            }
            DataFiles.DataSource = dtdf;
            DataFiles.DataBind();

            DataTable dtl = new DataTable();
            dtl.Columns.Add("Limit");
            dtl.Columns.Add("Value");

            DataRow dr = dtl.NewRow();
            dr[0] = "Max Input Rows (per run)";
            dr[1] = Util.formatCount(u.MaxInputRows);
            dtl.Rows.Add(dr);
            dr = dtl.NewRow();
            dr[0] = "Max Output Rows (per run)";
            dr[1] = Util.formatCount(u.MaxOutputRows);
            dtl.Rows.Add(dr);
            dr = dtl.NewRow();
            dr[0] = "Max Script Statements (per run)";
            dr[1] = Util.formatCount(u.MaxScriptStatements);
            dtl.Rows.Add(dr);
            dr = dtl.NewRow();
            dr[0] = "Script Query Timeout (seconds)";
            dr[1] = u.ScriptQueryTimeout;
            dtl.Rows.Add(dr);
            dr = dtl.NewRow();
            dr[0] = "Max Delivered Reports Per Day (per space)";
            dr[1] = Util.formatCount(u.MaxDeliveredReportsPerDay);
            dtl.Rows.Add(dr);

            Limits.DataSource = dtl;
            Limits.DataBind();

            DataTable dtv = new DataTable();
            dtv.Columns.Add("Version");
            DataRow drv = dtv.NewRow();
            drv[0] = Assembly.Load("Acorn").FullName;
            dtv.Rows.Add(drv);
            drv = dtv.NewRow();
            drv[0] = Assembly.Load("Birst Administration").FullName;
            dtv.Rows.Add(drv);
            drv = dtv.NewRow();
            drv[0] = Assembly.Load("LogicalExpression").FullName;
            dtv.Rows.Add(drv);
            AssemblyVersions.DataSource = dtv;
            AssemblyVersions.DataBind();

            string path = sp.Directory + "\\repository_dev.xml";
            FileInfo fi = new FileInfo(path);
            if (fi.Exists)
            {
                fi.Refresh();
                DateLabel.Text = HttpUtility.HtmlEncode(fi.LastWriteTime.ToShortDateString() + " " + fi.LastWriteTime.ToShortTimeString());
                DateLabel.Visible = true;
            }
            else
                DateLabel.Visible = false;
            Util.logoutSMIWebSession(Session, sp, Request);

            DataTable dt = new DataTable();
            dt.Columns.Add("Server");
            dt.Columns.Add("Description");
            dt.Columns.Add("Acorn Log");
            dt.Columns.Add("SMIWeb Log");
            dt.Columns.Add("Scheduler Log");
            dt.Columns.Add("Connector Log");

            DBConnection.QueryConnection infoConn = null;
            try
            {
                infoConn = ConnectionPool.getConnection();
                List<ServerInfo> info = Database.getServerInfo(infoConn, mainSchema);
                foreach (ServerInfo entry in info)
                {
                    DataRow dr2 = dt.NewRow();
                    dr2[0] = entry.serverID;
                    dr2[1] = entry.description;
                    dr2[2] = "download log";
                    dr2[3] = "download log";
                    dr2[4] = "download log";
                    dr2[5] = "download log";
                    dt.Rows.Add(dr2);
                }
                ServerView.DataSource = dt;
                ServerView.DataBind();

                DataTable dt2 = new DataTable();
                dt2.Columns.Add("ID");
                dt2.Columns.Add("Name");
                dt2.Columns.Add("Description");
                dt2.Columns.Add("Log");

                List<LogInfo> logsInfo = Database.getServerLogsInfo(infoConn, mainSchema);
                if (logsInfo != null && logsInfo.Count > 0)
                {
                    foreach (LogInfo logInfo in logsInfo)
                    {
                        DataRow row = dt2.NewRow();
                        row[0] = logInfo.ID;
                        row[1] = logInfo.Name;
                        row[2] = logInfo.Description;
                        row[3] = "download log";                        
                        dt2.Rows.Add(row);
                    }
                }
                VersionIndpendentServerView.DataSource = dt2;
                VersionIndpendentServerView.DataBind();
            }
            finally
            {
                ConnectionPool.releaseConnection(infoConn);
            }

            SetGetLoadLogsLinks();
            SetLiveAccessLinks();
            SetConnectorConfigDownloadLinks();
            SetConnectorConfigUploadLinks();

            string temp = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["EnableSeedQueries"];
            if (temp != null && temp.Equals("true"))
            {
                SeedQueriesPanel.Visible = true;
            }

            connectionMap = new Dictionary<string, Connection>();
            localConnectionMap = new Dictionary<string, Connection>();
            MainAdminForm maf = (MainAdminForm)Session["MAF"];
            Connection[] connections = Util.getConnections(sp, maf);
            foreach (Connection conn in connections)
            {
                connectionMap.Add(conn.VisibleName, conn);
                if (conn.VisibleName == Database.DEFAULT_CONNECTION || conn.LocalETL)
                {
                    localConnectionMap.Add(conn.VisibleName, conn);
                }
            }

            if (localConnectionMap.Count > 1)//contains local etl connections
            {
                SetLocalLoadLogsLinks();
                SetDeleteLocalPublishLinks();
            }
            SetLiveAccessActivityLogsLinks();
            if (!IsPostBack)
            {
                QueryConnection.DataSource = connectionMap.Keys.ToArray();
                QueryConnection.DataBind();
            }

            if (maf.disableImpliedGrainsProcessing)
            {
                ImpliedGrainsProcessing.Visible = false;
            }
        }

        protected void SetLiveAccessActivityLogsLinks()
        {
            Space sp = (Space)Session["space"];
            if (sp == null)
                return;
            string path = sp.Directory + "\\" + Util.DCCONFIG_DIR;
            if (Directory.Exists(path))
            {
                string[] dcfiles = Directory.GetFiles(path, "*dcconfig.xml");
                dcconfigs = new string[dcfiles.Length];
                for (int i = 0; i < dcfiles.Length; i++)
                {
                    List<Connection> rConnections = BirstConnectUtil.getRealtimeConnectionList(sp, new string[] { dcfiles[i] });
                    if (rConnections.Count <= 0)
                    {
                        continue;
                    }
                    dcconfigs[i] = new FileInfo(dcfiles[i]).Name;
                    LinkButton activityLogLink = new LinkButton();
                    string configName = dcconfigs[i].IndexOf("_dcconfig.xml") > 0 ? dcconfigs[i].Substring(0, dcconfigs[i].IndexOf("_dcconfig.xml")) : "Default";
                    activityLogLink.Text = HttpUtility.HtmlEncode("Get Live Access Activity Logs (" + configName + " configuration)");
                    activityLogLink.ID = HttpUtility.HtmlEncode("activitylog." + dcconfigs[i]);
                    activityLogLink.Click += new EventHandler(getLiveAccessActivityLog);
                    Literal lbLiteral = new Literal();
                    lbLiteral.Text = "<br/>";
                    GetActivityLogPanel.Controls.Add(activityLogLink);
                    GetActivityLogPanel.Controls.Add(lbLiteral);
                }
            }
        }

        protected void getLiveAccessActivityLog(object sender, EventArgs e)
        {
            Space sp = (Space)Session["space"];
            if (sp == null)
                return;
            string dcfile = ((LinkButton)sender).ID.Substring("activitylog.".Length);
            string path = sp.Directory + "\\" + Util.DCCONFIG_DIR + "\\" + dcfile;
            FileInfo fi = new FileInfo(path);
            if (fi.Exists)
            {
                long bytesToRead = 262144;
                if (BytesRead.Text != null)
                {
                    Int64.TryParse(BytesRead.Text, out bytesToRead);
                }
                List<Connection> rConnections = BirstConnectUtil.getRealtimeConnectionList(sp, new string[] {fi.FullName});
                if (rConnections.Count > 0)
                {
                    string connName = null;
                    foreach (Connection conn in rConnections)
                    {
                        bool isActiveConn = LiveAccessUtil.isActiveConnection(sp, conn.Name);
                        if (isActiveConn)
                        {
                            connName = conn.Name;
                            break;
                        }
                    }
                    if (connName == null)
                        return;
                    string filename = "activity." + sp.Name + ".log";
                    if (LiveAccessUtil.getLogFileFromLiveAccessConnection(sp, connName, bytesToRead, LiveAccessUtil.LIVEACCESS_ACTIVITY_LOG, 0, filename) &&
                            File.Exists(sp.Directory + "\\logs\\temp_" + connName + "\\" + filename))
                    {
                        processLogFile(sp.Directory + "\\logs\\temp_" + connName + "\\" + filename);
                    }
                }
            }
        }

        protected void SetLocalLoadLogsLinks()
        {
            foreach(string conn in localConnectionMap.Keys)
            {
                if (conn == Database.DEFAULT_CONNECTION)
                    continue;
                LinkButton localLoadLogLink = new LinkButton();
                localLoadLogLink.Text = HttpUtility.HtmlEncode("Get Last Local Load Log (" + conn + ")");
                localLoadLogLink.ID = HttpUtility.HtmlEncode(localConnectionMap[conn].Name + "_load");
                localLoadLogLink.Click += new EventHandler(getLocalLoadLog);
                Literal lbLiteral = new Literal();
                lbLiteral.Text = "<br/>";
                GetLocalLoadLogPanel.Controls.Add(localLoadLogLink);
                GetLocalLoadLogPanel.Controls.Add(lbLiteral);

                LinkButton localTraceLogLink = new LinkButton();
                localTraceLogLink.Text = HttpUtility.HtmlEncode("Get Last ETL Trace Log (PRINT statement output) (" + conn + ")");
                localTraceLogLink.ID = HttpUtility.HtmlEncode(localConnectionMap[conn].Name + "_trace");
                localTraceLogLink.Click += new EventHandler(getLocalTraceLog);
                lbLiteral = new Literal();
                lbLiteral.Text = "<br/>";
                GetLocalTraceLogPanel.Controls.Add(localTraceLogLink);
                GetLocalTraceLogPanel.Controls.Add(lbLiteral);
            }            
        }

        protected void getLocalLoadLog(object sender, EventArgs e)
        {
            Space sp = (Space)Session["space"];
            if (sp == null)
                return;
            // determine how much of the file to read
            long bytesToRead = 262144;
            if (BytesRead.Text != null)
            {
                Int64.TryParse(BytesRead.Text, out bytesToRead);
            }
            string connName = ((LinkButton)sender).ID.ToString().Substring(0, ((LinkButton)sender).ID.ToString().LastIndexOf('_'));
            string connVisibleName = connName;
            foreach (Connection conn in connectionMap.Values)
            {
                if (conn.Name.Equals(connName))
                {
                    connVisibleName = conn.VisibleName;
                    break;
                }
            }
            string filename = "smiengine." + connVisibleName + ".log";
            string logs = Path.Combine(sp.Directory + "logs");
            string path = Path.Combine(logs, "temp_" + connName, filename);
            Util.validateFileLocation(logs, path);
            if (LiveAccessUtil.getLogFileFromLiveAccessConnection(sp, connName, bytesToRead, LiveAccessUtil.LIVEACCESS_LAST_LOAD_LOG_FILE, 0, filename) && File.Exists(path))
            {
                processLogFile(path);
            }
        }

        protected void getLocalTraceLog(object sender, EventArgs e)
        {
            Space sp = (Space)Session["space"];
            if (sp == null)
                return;
            // determine how much of the file to read
            long bytesToRead = 262144;
            if (BytesRead.Text != null)
            {
                Int64.TryParse(BytesRead.Text, out bytesToRead);
            }
            string connName = ((LinkButton)sender).ID.ToString().Substring(0, ((LinkButton)sender).ID.ToString().LastIndexOf('_'));
            string connVisibleName = connName;
            foreach (Connection conn in connectionMap.Values)
            {
                if (conn.Name.Equals(connName))
                {
                    connVisibleName = conn.VisibleName;
                    break;
                }
            }
            string filename = "trace." + connVisibleName + ".log";
            string logs = Path.Combine(sp.Directory + "logs");
            string path = Path.Combine(logs, "temp_" + connName, filename);
            Util.validateFileLocation(logs, path);
            if (LiveAccessUtil.getLogFileFromLiveAccessConnection(sp, connName, bytesToRead, LiveAccessUtil.LIVEACCESS_TRACE_LOG_FILE, 0, filename) && File.Exists(path))
            {
                processLogFile(path);
            }            
        }

        protected void SetGetLoadLogsLinks()
        {
            Space sp = Util.getSessionSpace(Session);
            if (sp == null)
                return;
            MainAdminForm maf = Util.getSessionMAF(Session);
            string[] partitionConnections = maf.getPartitionConnectionNames("Default Connection");
            if (maf.IsPartitioned && partitionConnections != null && partitionConnections.Length > 0)
            {
                //<asp:LinkButton ID="GetLog" runat="server" OnClick="getLog">Get Last Load Log</asp:LinkButton>
                foreach (string partitionConn in partitionConnections)
                {
                    LinkButton logLink = new LinkButton();
                    logLink.ID = partitionConn;
                    logLink.Text = "Get Last Load Log for PartitionConnection - " + partitionConn;
                    logLink.Click += new EventHandler(getPartitionLog);
                    Literal lbLiteral = new Literal();
                    lbLiteral.Text = "<br/>";
                    GetLogPanel.Controls.Add(logLink);
                    GetLogPanel.Controls.Add(lbLiteral);
                }
                //get main log
                LinkButton dcLogLink = new LinkButton();
                dcLogLink.ID = "Default Connection";
                dcLogLink.Text = "Get Last Load Log for Default Connection";
                dcLogLink.Click += new EventHandler(getPartitionLog);
                Literal dcLbLiteral = new Literal();
                dcLbLiteral.Text = "<br/>";
                GetLogPanel.Controls.Add(dcLogLink);
                GetLogPanel.Controls.Add(dcLbLiteral);
            }
            else
            {
                LinkButton logLink = new LinkButton();
                logLink.ID = "GetLog";
                logLink.Text = "Get Last Load Log";
                logLink.Click += new EventHandler(getLog);
                Literal lbLiteral = new Literal();
                lbLiteral.Text = "<br/>";
                GetLogPanel.Controls.Add(logLink);
                GetLogPanel.Controls.Add(lbLiteral);
            }
        }

        protected void SetConnectorConfigDownloadLinks()
        {
            Space sp = (Space)Session["space"];
            if (sp == null)
                return;
            string path = sp.Directory + "\\" + Util.CONNECTOR_CONFIG_DIR;
            if (Directory.Exists(path))
            {
                Dictionary<string, string> connectorToSettings = null;
                if (Utils.ConnectorUtils.UseNewConnectorFramework())
                {
                    connectorToSettings = Utils.ConnectorUtils.getConnectorSettingsFiles(sp);                    
                }
                else
                {
                    connectorToSettings = Utils.ConnectorUtils.connectorNameToSettingsFileNames;
                }
                foreach (string connectorName in connectorToSettings.Keys)
                {
                    string configFileName = connectorToSettings[connectorName];
                    if (File.Exists(path + "\\" + configFileName))
                    {
                        LinkButton connectorConfigLink = new LinkButton();
                        connectorConfigLink.Text = "Download " + HttpUtility.HtmlEncode(connectorName) + " connector configuration";
                        connectorConfigLink.ID = configFileName + "_Downloader";
                        connectorConfigLink.Click += new EventHandler(DownloadConnectorConfigFile);
                        Literal lbLiteral = new Literal();
                        lbLiteral.Text = "<br/>";
                        ConnectorConfigsDownloadPanel.Controls.Add(connectorConfigLink);
                        ConnectorConfigsDownloadPanel.Controls.Add(lbLiteral);
                    }
                }
            }
        }

        protected void DownloadConnectorConfigFile(object sender, EventArgs e)
        {
            Space sp = (Space)Session["space"];
            if (sp == null)
                return;
            string path = sp.Directory + "\\" + Util.CONNECTOR_CONFIG_DIR;
            if (!Directory.Exists(path))
                return;
            string configFileName = ((LinkButton)sender).ID;
            configFileName = configFileName.Substring(0, configFileName.Length - "_Downloader".Length);
            if (!File.Exists(path + "\\" + configFileName))
                return;
            downloadFile(path, configFileName);
        }

        protected void SetConnectorConfigUploadLinks()
        {
            Space sp = (Space)Session["space"];
            if (sp == null)
                return;
            string path = sp.Directory + "\\" + Util.CONNECTOR_CONFIG_DIR;
            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);
            Dictionary<string, string> connectorToSettings = null;
            if (Utils.ConnectorUtils.UseNewConnectorFramework())
            {
                connectorToSettings = Utils.ConnectorUtils.getConnectorSettingsFiles(sp);
            }
            else
            {
                connectorToSettings = Utils.ConnectorUtils.connectorNameToSettingsFileNames;
            }
            foreach (string connectorName in connectorToSettings.Keys)
            {
                string configFileName = connectorToSettings[connectorName];
                FileUpload fUpload = new FileUpload();
                fUpload.Width = Unit.Pixel(300);
                fUpload.ID = configFileName + "_FileUploader";
                Button uploadConfigButton = new Button();
                uploadConfigButton.Text = "Upload " + configFileName + " (" + connectorName + " connector config)";
                uploadConfigButton.ID = configFileName + "_Uploader";
                uploadConfigButton.Click += new EventHandler(UploadConnectorConfigFile);
                Literal lbLiteral = new Literal();
                lbLiteral.Text = "<div style=\"height: 15px\">&nbsp;</div>";
                ConnectorConfigsUploadPanel.Controls.Add(lbLiteral);
                ConnectorConfigsUploadPanel.Controls.Add(fUpload);
                lbLiteral = new Literal();
                lbLiteral.Text = "&nbsp;&nbsp;";
                ConnectorConfigsUploadPanel.Controls.Add(lbLiteral);
                ConnectorConfigsUploadPanel.Controls.Add(uploadConfigButton);                                
            }            
        }

        protected void UploadConnectorConfigFile(object sender, EventArgs e)
        {
            string configFileName = ((Button)sender).ID;
            configFileName = configFileName.Substring(0, configFileName.Length - "_Uploader".Length);
            FileUpload fUpload = (FileUpload)(((Button)sender).Parent.FindControl(configFileName + "_FileUploader"));
            Space sp = (Space)Session["space"];
            if (sp == null)
                return;
            string path = sp.Directory + "\\" + Util.CONNECTOR_CONFIG_DIR;
            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);
            uploadFile(configFileName, path, fUpload);
        }

        protected void SetLiveAccessLinks()
        {
            Space sp = (Space)Session["space"];
            if (sp == null)
                return;
            string path = sp.Directory + "\\" + Util.DCCONFIG_DIR;
            if (Directory.Exists(path))
            {
                string [] dcfiles = Directory.GetFiles(path, "*dcconfig.xml");
                dcconfigs = new string[dcfiles.Length];
                for (int i=0; i < dcfiles.Length; i++)
                {
                    dcconfigs[i] = new FileInfo(dcfiles[i]).Name;
                    LinkButton dcconfigLink = new LinkButton();
                    string configName = dcconfigs[i].IndexOf("_dcconfig.xml") > 0 ? dcconfigs[i].Substring(0, dcconfigs[i].IndexOf("_dcconfig.xml")) : "Default"; 
                    dcconfigLink.Text = "Download " + HttpUtility.HtmlEncode(configName) + " configuration (Live Access Settings)";
                    dcconfigLink.ID = dcconfigs[i];
                    dcconfigLink.Click += new EventHandler(getLiveAccessConfig);
                    Literal lbLiteral = new Literal();
                    lbLiteral.Text = "<br/>";
                    DCconfigPanel.Controls.Add(dcconfigLink);
                    DCconfigPanel.Controls.Add(lbLiteral);            
                }
            }
        }

        protected void getLiveAccessConfig(object sender, EventArgs e)
        {
            Space sp = (Space)Session["space"];
            if (sp == null)
                return;
            string path = sp.Directory + "\\" + Util.DCCONFIG_DIR + "\\" + ((LinkButton)sender).ID;
            FileInfo fi = new FileInfo(path);
            if (fi.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + Util.removeCRLF(fi.Name));
                Response.AddHeader("Content-Length", fi.Length.ToString());
                Response.ContentType = "text/xml";
                Response.TransmitFile(fi.FullName);
                Response.Flush();
                Response.End();
                User u = (User)Session["user"];
                Global.systemLog.Warn(fi.Name + " downloaded by user " + (u == null ? "Unknown User" : u.Username) + " for space " + sp.Name);
            }
        }

        private void downloadFile(string name)
        {
            Space sp = (Space)Session["space"];
            if (sp == null)
                return;
            downloadFile(sp.Directory, name);
        }

        private void downloadFile(string directory, string name)
        {
            Space sp = (Space)Session["space"];
            if (sp == null)
                return;
            string path = Path.Combine(directory, name);
            Util.validateFileLocation(directory, path);
            FileInfo fi = new FileInfo(path);
            if (fi.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + Util.removeCRLF(fi.Name));
                Response.AddHeader("Content-Length", fi.Length.ToString());
                Response.ContentType = "text/xml";
                Response.TransmitFile(fi.FullName);
                Response.Flush();
                Response.End();
                User u = (User)Session["user"];
                Global.systemLog.Warn(name + " downloaded by user " + (u == null ? "Unknown User" : u.Username) + " for space " + sp.Name);
            }
        }

        protected void getSamlEncryptCertificate(object sender, EventArgs e)
        {
            FileInfo fi = new FileInfo(Server.MapPath("~/SAMLSSO/BirstSamlPublic.cert"));
            if (fi.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + Util.removeCRLF(fi.Name));
                Response.AddHeader("Content-Length", fi.Length.ToString());
                Response.ContentType = "text/xml";
                Response.TransmitFile(fi.FullName);
                Response.Flush();
                Response.End();
                User u = Util.getSessionUser(Session);
                Space sp = Util.getSessionSpace(Session);
                Global.systemLog.Warn("SAML public certificate downloaded by user " + (u == null ? "Unknown User" : u.Username) + " while logged into space " + sp.Name);
            }
        }

        protected void getRepository(object sender, EventArgs e)
        {
            downloadFile("repository_dev.xml");
        }

        protected void getRepositoryProd(object sender, EventArgs e)
        {
            downloadFile("repository.xml");
        }

        protected void getUsersAndGroups(object sender, EventArgs e)
        {
            downloadFile("UsersAndGroups.xml");
        }

        protected void getSForceConfig(object sender, EventArgs e)
        {
            downloadFile("sforce.xml");
        }

        protected void getCustomGeoMaps(object sender, EventArgs e)
        {
            downloadFile("CustomGeoMaps.xml");
        }

        protected void getPageSequence(object sender, EventArgs e)
        {
            downloadFile("pageSequence.txt");
        }

        protected void getSpaceSettings(object sender, EventArgs e)
        {
            downloadFile("spacesettings.xml");
        }

        protected void getPackagesXmlFile(object sender, EventArgs e)
        {
            downloadFile("packages.xml");
        }

        protected void getDrillMaps(object sender, EventArgs e)
        {
            downloadFile("DrillMaps.xml");
        }

        protected void getDashboardStyleSettings(object sender, EventArgs e)
        {
            downloadFile("DashboardStyleSettings.xml");
        }

        protected void getSavedExpressions(object sender, EventArgs e)
        {
            downloadFile("SavedExpressions.xml");
        }

        protected void getCustomSubjectAreas(object sender, EventArgs e)
        {
            Space sp = (Space)Session["space"];
            if (sp == null)
                return;
            string path = Path.Combine(sp.Directory, "custom-subject-areas");
            DirectoryInfo di = new DirectoryInfo(path);
            if (di.Exists)
            {
                FileInfo[] csaFiles = di.GetFiles("*.xml");
                DownloadZipToBrowser(csaFiles, "custom-subject-areas.zip");
                User u = (User)Session["user"];
                Global.systemLog.Warn("Custom subject areas downloaded by user " + (u == null ? "Unknown User" : u.Username) + " for space " + sp.Name);
            }
        }

        protected void getKmlFiles(object sender, EventArgs e)
        {
            Space sp = (Space)Session["space"];
            if (sp == null)
                return;
            string path = Path.Combine(sp.Directory, "catalog", "Kml");
            DirectoryInfo di = new DirectoryInfo(path);
            if (di.Exists)
            {
                FileInfo[] csaFiles = di.GetFiles("*.kml");
                DownloadZipToBrowser(csaFiles, "kml.zip");
                User u = (User)Session["user"];
                Global.systemLog.Warn("Kml files downloaded by user " + (u == null ? "Unknown User" : u.Username) + " for space " + sp.Name);
            }
        }

        protected void getSFDCMappingFiles(object sender, EventArgs e)
        {
            Space sp = (Space)Session["space"];
            if (sp == null)
                return;
            string path = Path.Combine(sp.Directory, "mapping");
            DirectoryInfo di = new DirectoryInfo(path);
            if (di.Exists)
            {
                FileInfo[] csaFiles = di.GetFiles("*.txt");
                DownloadZipToBrowser(csaFiles, "mapping.zip");
                User u = (User)Session["user"];
                Global.systemLog.Warn("SFDC mapping files downloaded by user " + (u == null ? "Unknown User" : u.Username) + " for space " + sp.Name);
            }
        }

        private void DownloadZipToBrowser(FileInfo[] files, string downloadName)
        {
            Response.Clear();
            Response.ContentType = "application/octet-stream";
            Response.AddHeader("content-disposition", "attachment; filename=\"" + Util.removeCRLF(downloadName) + "\"");

            byte[] buffer = new byte[4096];

            ZipOutputStream zipOutputStream = new ZipOutputStream(Response.OutputStream);
            zipOutputStream.SetLevel(4); //0-9, 9 being the highest level of compression

            foreach (FileInfo fi in files)
            {
                Stream fs = fi.OpenRead();
                ZipEntry entry = new ZipEntry(ZipEntry.CleanName(fi.Name));
                entry.Size = fs.Length;
                // Setting the Size provides WinXP built-in extractor compatibility,
                //  but if not available, you can set zipOutputStream.UseZip64 = UseZip64.Off instead.

                zipOutputStream.PutNextEntry(entry);

                int count = fs.Read(buffer, 0, buffer.Length);
                while (count > 0)
                {
                    zipOutputStream.Write(buffer, 0, count);
                    count = fs.Read(buffer, 0, buffer.Length);
                    if (!Response.IsClientConnected)
                    {
                        break;
                    }
                    Response.Flush();
                }
                fs.Close();
            }
            zipOutputStream.Close();

            Response.Flush();
            Response.End();
        }

        protected void getSFDCQueryInfo(object sender, EventArgs e)
        {
            Space sp = (Space)Session["space"];
            if (sp == null)
                return;
            string path = SalesforceLoader.getSFDCQueryLogFilePath(sp);
            FileInfo fi = new FileInfo(path);
            if (fi.Exists)
            {
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + Util.removeCRLF(fi.Name));
                Response.AddHeader("Content-Length", fi.Length.ToString());
                Response.ContentType = "text/plain";
                Response.TransmitFile(fi.FullName);
                Response.Flush();
                Response.End();
                User u = (User)Session["user"];
                Global.systemLog.Warn(path + " downloaded by user " + (u == null ? "Unknown User" : u.Username) + " for space " + sp.Name);
            }
        }

        protected void getProcessingGroupsAndSources(object sender, EventArgs e)
        {
            Space sp = (Space)Session["space"];
            if (sp == null)
                return;
            string baseFile = "sources" + DateTime.Now.ToFileTimeUtc() + ".txt";
            string tempFile = Path.Combine(sp.Directory, baseFile);
            TextWriter writer = null;
            try
            {
                if (File.Exists(tempFile))
                {
                    File.Delete(tempFile);
                }
                writer = new StreamWriter(tempFile);

                MainAdminForm maf = Util.getSessionMAF(Session);
                if (maf == null)
                    return;
                List<StagingTable> tbls = maf.stagingTableMod.getStagingTables();
                if (tbls == null)
                    return;
                foreach (StagingTable tbl in tbls)
                {
                    writer.Write(tbl.getDisplayName().Trim());
                    string groups = null;
                    if (tbl.SubGroups != null && tbl.SubGroups.Length > 0)
                    {
                        for (int i = 0; i < tbl.SubGroups.Length; i++)
                        {
                            if (groups != null)
                                groups += ',';
                            groups += tbl.SubGroups[i];
                        }
                        writer.Write('\t' + groups);
                    }
                    writer.WriteLine();
                }
                writer.Close();
                downloadFile(baseFile);
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Unable to write " + baseFile, ex);
                try
                {
                    if (writer != null)
                    {
                        writer.Close();
                    }
                }
                catch (Exception)
                {
                    Global.systemLog.Warn("Unable to close writer.");
                }
                throw ex;
            }
            finally
            {
                File.Delete(tempFile);
            }
        }

        protected void getExtractionLog(object sender, EventArgs e)
        {
            Space sp = Util.getSessionSpace(Session);
            if (sp != null)
            {
                processLogFiles(sp.Directory + "\\logs", "smiengine.*.0.log");
            }
        }

        protected void getSchedulerAcornOpLog(object sender, EventArgs e)
        {
            Space sp = Util.getSessionSpace(Session);
            if (sp != null)
            {
                processLogFiles(sp.Directory + "\\slogs", "processLog.*.log");
            }
        }

        protected void getPartitionLog(object sender, EventArgs e)
        {
            Space sp = Util.getSessionSpace(Session);
            if (sp == null)
                return;
            string connectionName = ((LinkButton)sender).ID;
            string searchPattern = null;
            string directory = sp.Directory + "\\logs";
            string[] files = null;
            if (connectionName.Equals("Default Connection"))
            {
                searchPattern = "smiengine.*.*.log";
                files = Directory.GetFiles(directory, searchPattern);
                if (files != null && files.Length > 0)
                {
                    List<string> fileList = files.ToList();
                    MainAdminForm maf = Util.getSessionMAF(Session);
                    string[] partitionConnections = maf.getPartitionConnectionNames("Default Connection");
                    foreach (string partitionConn in partitionConnections)
                    {
                        string[] partitionFiles = Directory.GetFiles(directory, "smiengine.*." + partitionConn + ".*.log");
                        if (partitionFiles != null && partitionFiles.Length > 0)
                        {
                            foreach (string pfName in partitionFiles)
                            {
                                fileList.Remove(pfName);
                            }
                        }
                    }
                    files = fileList.ToArray();
                }
            }
            else
            {
                searchPattern = "smiengine.*." + connectionName + ".*.log";
                files = Directory.GetFiles(directory, searchPattern);
            }
            if (files == null)
                return;
            string filename = getNewestFile(files);
            if (filename == null)
                return;
            processLogFile(filename);
        }

        private string getNewestFile(string[] files)
        {
            DateTime newestDate = DateTime.MinValue;
            string filename = null;
            foreach (string fname in files)
            {
                DateTime dt = File.GetLastWriteTime(fname);
                if (newestDate == DateTime.MinValue || dt > newestDate)
                {
                    newestDate = dt;
                    filename = fname;
                }
            }
            return filename;
        }

        protected void getLog(object sender, EventArgs e)
        {
            Space sp = (Space)Session["space"];
            if (sp == null)
                return;
            processLogFiles(sp.Directory + "\\logs", "smiengine.*");
        }

        protected void getTraceLog(object sender, EventArgs e)
        {
            Space sp = (Space)Session["space"];
            if (sp == null)
                return;
            processLogFile(sp.Directory + "\\logs\\trace.log");
        }

        protected void getIndependentLogCommand(object sender, GridViewCommandEventArgs e)
        {
            int index = int.Parse((string)e.CommandArgument);            
            QueryConnection conn = null;
            LogInfo logInfo = null;
            try
            {
                conn = ConnectionPool.getConnection();
                string id = VersionIndpendentServerView.DataKeys[index].Value.ToString();
                int result = -1;
                if (int.TryParse(id, out result))
                {
                    logInfo = Database.getServerLogInfo(conn, mainSchema, result);
                }
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }

            if (logInfo != null)
            {
                string prefix = logInfo.LogPrefix;
                if (!prefix.EndsWith("*"))
                {
                    prefix = prefix + "*";
                }

                getWebLog(logInfo.LogDir, prefix);
            }
        }

        protected void getLogCommand(object sender, GridViewCommandEventArgs e)
        {
            int index = int.Parse((string)e.CommandArgument);
            string command = (string) e.CommandName;

            DBConnection.QueryConnection infoConn = null;
            try
            {
                infoConn = ConnectionPool.getConnection();
                List<ServerInfo> info = Database.getServerInfo(infoConn, mainSchema);
                if (command == "acorn")
                    getWebLog(info[index].acornLog, "birst.*");
                else if (command == "smiweb")
                    getWebLog(info[index].smiwebLog, "smi.*");
                else if (command == "scheduler")
                    getWebLog(info[index].schedulerwebLog, "scheduler.*");
                else if (command == "connector")
                    getWebLog(info[index].connectorwebLog, "Connector.*");
            }
            finally
            {
                ConnectionPool.releaseConnection(infoConn);
            }
        }

        // get the smiweb log
        protected void getWebLog(string webLogDir, string prefix)
        {
            if (webLogDir == null)
                return;
            processLogFiles(webLogDir, prefix);
        }

        // find the most recent log file in a directory
        private void processLogFiles(string directory, string prefix)
        {
            ErrorLog.Visible = false;
            if (!Directory.Exists(directory))
                return;
            string[] files = Directory.GetFiles(directory, prefix);
            if (files == null)
                return;
            string filename = getNewestFile(files);
            if (filename == null)
                return;
            processLogFile(filename);
        }

        private void processLogFile(string filename)
        {
            FileInfo fi = new FileInfo(filename);
            if (fi.Exists)
            {
                FileStream fs = null;
                try
                {
                    fs = fi.Open(FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                }
                catch (Exception ex)
                {
                    ErrorLog.Visible = true;
                    ErrorLog.Text = "Error: " + HttpUtility.HtmlEncode(ex.Message);
                    if (fs != null)
                        fs.Close();
                    return;
                }

                // determine how much of the file to read
                long bytesToRead = 262144;
                if (BytesRead.Text != null)
                {
                    Int64.TryParse(BytesRead.Text, out bytesToRead);
                }
                if (bytesToRead <= 0 || bytesToRead > fs.Length)
                    bytesToRead = fs.Length;
             
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + Util.removeCRLF(fi.Name));
                Response.AddHeader("Content-Length", bytesToRead.ToString());
                Response.ContentType = "text/plain";
                long offset = fs.Length - bytesToRead;
                int bytesRead = 0;
                byte[] buffer = new byte[1 << 16]; // 64kb
                fs.Seek(offset, SeekOrigin.Begin); // seek to the beginning or just above the last 'lines' lines
                while ((bytesRead = fs.Read(buffer, 0, buffer.Length)) != 0)
                {
                    Response.OutputStream.Write(buffer, 0, bytesRead);
                    Response.Flush();
                }
                fs.Close();
                Response.Flush();
                Response.End();
            }
        }

        protected void UploadButton_Click(object sender, EventArgs e)
        {
            if (RepositoryUpload.HasFile)
            {
                Space sp = (Space)Session["space"];
                if (sp == null)
                    return;
                string path = Path.Combine(sp.Directory, "repository_dev.xml");
                RepositoryUpload.SaveAs(path);
                Util.FileCopy(path, Path.Combine(sp.Directory, "repository.xml"), true);
                
                MainAdminForm oldmaf = (MainAdminForm)Session["MAF"];
                HashSet<string> degenerateLevels = null;
                //get Degenerate field value from old repository if it's set to true
                if (oldmaf != null && Status.hasLoadedData(sp, null))
                {
                    degenerateLevels = new HashSet<string>();
                    foreach (Hierarchy h in oldmaf.hmodule.getHierarchies())
                    {
                        if (h.Children != null)
                        {
                            foreach (Level l in h.Children)
                            {
                                if (l.Degenerate)
                                {
                                    degenerateLevels.Add(h.Name + "~" + l.Name);
                                }
                            }
                        }
                    }
                }

                Util.setSpace(Session, sp, null, false);
                MainAdminForm maf = (MainAdminForm)Session["MAF"];
                maf.hybrid = true; // to let us know that the repository was potentially modified outside of Birst
                bool saveApplication = false;

                //following if condition preserves old value of Degenerate field in new repository
                if (degenerateLevels != null)
                {
                    foreach (Hierarchy h in maf.hmodule.getLatestHierarchies())
                    {
                        if (h.Children != null)
                        {
                            foreach (Level l in h.Children)
                            {
                                string hlevelName = h.Name + "~" + l.Name;
                                if (degenerateLevels.Contains(hlevelName))
                                {
                                    l.Degenerate = true;
                                    saveApplication = true;
                                }
                                else if (l.Degenerate && !degenerateLevels.Contains(hlevelName))
                                {
                                    l.Degenerate = false;
                                    saveApplication = true;
                                }
                            }
                        }
                    }
                }
                if (maf.builderVersion >= 25)
                {
                    //Set last modified date for all staging tables to current so that next time data is processed, all staging tables would be reloaded.
                    List<StagingTable> stList = maf.stagingTableMod.getStagingTables();
                    if ((stList != null) && (stList.Count > 0))
                    {
                        foreach(StagingTable st in stList)
                        {
                            st.LastModifiedDate = DateTime.UtcNow;
                        }
                        saveApplication = true;
                    }
                }
                DatabaseConnection defaultConnection = maf.connection.findConnection("Default Connection");
                if (defaultConnection != null && defaultConnection.IsPartitioned)
                {
                    string[] partitionConnectionNames = defaultConnection.PartitionConnectionNames;
                    if (partitionConnectionNames != null && partitionConnectionNames.Length > 0)
                    {
                        foreach (string partitionConnectionName in partitionConnectionNames)
                        {
                            DatabaseConnection dc = maf.connection.findConnection(partitionConnectionName);
                            if (dc != null && dc.Schema != sp.Schema)
                            {
                                dc.Schema = sp.Schema;
                                saveApplication = true;
                            }
                        }
                    }
                }
                if ((maf.connection.connectionList.Count > 0 && maf.connection.connectionList[0].Schema != sp.Schema) || saveApplication)
                {                    
                    Util.saveApplication(maf, sp, Session, null);
                }
                User u = (User)Session["user"];
                Global.systemLog.Warn("Repository uploaded by user " + (u == null ? "Unknown User" : u.Username) + " for space " + sp.Name);
            }
        }

        private void uploadFile(string name, FileUpload uploader)
        {
            Space sp = (Space)Session["space"];
            if (sp == null)
                return;
            uploadFile(name, sp.Directory, uploader);
        }

        private void uploadFile(string name, string directoryName, FileUpload uploader)
        {
            if (uploader.HasFile)
            {
                Space sp = (Space)Session["space"];
                if (sp == null)
                    return;
                string path = Path.Combine(directoryName, name);
                Util.validateFileLocation(directoryName, path);
                uploader.SaveAs(path);

                User u = (User)Session["user"];
                Global.systemLog.Warn(name + " uploaded by user " + (u == null ? "Unknown User" : u.Username) + " for space " + sp.Name);
            }
        }

        // name, directory
        private void uploadZipFile(string name, FileUpload uploader)
        {
            if (uploader.HasFile)
            {
                Space sp = (Space)Session["space"];
                if (sp == null)
                    return;
                string path = Path.Combine(sp.Directory, name);
                Util.validateFileLocation(sp.Directory, path);
                string file = Path.Combine(path, "upload-" + DateTime.UtcNow.ToFileTimeUtc().ToString() + ".zip");
                Util.validateFileLocation(path, file);
                uploader.SaveAs(file);

                // retrieve the meta data information
                Dictionary<string, ZipEntry> zipEntriesByName = new Dictionary<string, ZipEntry>();
                ZipFile zf = null;
                try
                {
                    zf = new ZipFile(file);
                    foreach (ZipEntry zen in zf)
                    {
                        zipEntriesByName.Add(zen.Name, zen);
                    }
                }
                catch (Exception ex)
                {
                    Global.systemLog.Warn("Failed to process the zip file catalog: " + ex.Message);
                }
                finally
                {
                    if (zf != null)
                        zf.Close();
                }

                FileStream fin = new FileStream(file, FileMode.Open);
                ZipInputStream zs = new ZipInputStream(fin);
                ZipEntry ze = null;
                while ((ze = zs.GetNextEntry()) != null)
                {
                    // Don't include directories - load all into the directory
                    if (ze.IsDirectory)
                        continue;
                    if (ze.Name.StartsWith("__MACOSX/")) // ignore MAC resource fork files in Zips
                        continue;
                    string zfname = ze.Name;
                    long zesize = ze.Size;
                    if (zesize == -1) // need to get the size from the cached meta data (streaming zipfiles have the size at the end)
                    {
                        if (zipEntriesByName.ContainsKey(ze.Name))
                        {
                            zesize = zipEntriesByName[ze.Name].Size;
                        }
                    }
                    FileStream fs = null;
                    string zfilename = Path.Combine(path, zfname);
                    Util.validateFileLocation(path, zfilename);
                    try
                    {
                        fs = new FileStream(zfilename, FileMode.Create, FileAccess.Write);
                        int size = 2048;
                        long total = 0;
                        byte[] data = new byte[2048];
                        while (true)
                        {
                            size = zs.Read(data, 0, data.Length);
                            total += size;
                            // sanity check, make sure that the file is not bigger than the zip entry size
                            if (zesize != -1 && total > zesize)
                            {
                                Global.systemLog.Warn("Writing file larger than the size in the zip entry, aborting: current size = " + total + ", zip entry size = " + zesize);
                                throw new Exception("Writing file larger than the size in the zip entry, aborting: current size = " + total + ", zip entry size = " + zesize);
                            }
                            if (size > 0)
                            {
                                fs.Write(data, 0, size);
                            }
                            else
                            {
                                break;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Global.systemLog.Debug(ex.Message);
                    }
                    finally
                    {
                        if (fs != null)
                            fs.Close();
                    }
                }
                File.Delete(file);
                User u = (User)Session["user"];
                Global.systemLog.Warn(name + " uploaded by user " + (u == null ? "Unknown User" : u.Username) + " for space " + sp.Name);
            }
        }

        protected void UploadPageSequenceButton_Click(object sender, EventArgs e)
        {
            uploadFile("pageSequence.txt", PageSequenceUpload);
        }

        protected void UploadCustomGeoMapsButton_Click(object sender, EventArgs e)
        {
            uploadFile("CustomGeoMaps.xml", CustomGeoMapsUpload);
        }

        protected void UploadDrillMapsButton_Click(object sender, EventArgs e)
        {
            uploadFile("DrillMaps.xml", DrillMapsUpload);
        }

        protected void UploadSforceButton_Click(object sender, EventArgs e)
        {
            uploadFile("sforce.xml", SforceUpload);
        }

        protected void UploadDashboardStyleSettingsButton_Click(object sender, EventArgs e)
        {
            uploadFile("DashboardStyleSettings.xml", DashboardStyleSettingsUpload);
        }

        protected void UploadSpaceSettingsButton_Click(object sender, EventArgs e)
        {
            uploadFile("spacesettings.xml", SpaceSettingsUpload);
        }

        protected void UploadCustomSubjectAreasButton_Click(object sender, EventArgs e)
        {
            uploadZipFile("custom-subject-areas", CustomSubjectAreasUpload);
        }

        protected void UploadKMLFilesButton_Click(object sender, EventArgs e)
        {
            uploadZipFile(Path.Combine("catalog", "Kml"), KMLFilesUpload);
        }

        protected void ListTables(object sender, EventArgs e)
        {
            Space sp = (Space)Session["space"];
            if (sp == null)
                return;
            MainAdminForm maf = (MainAdminForm)Session["MAF"];
            if (maf == null)
                return;
            Acorn.DBConnection.QueryConnection conn = null;
            List<string> tables = null;
            string queryConn = QueryConnection.SelectedValue;
            if (connectionMap.ContainsKey(queryConn) && connectionMap[queryConn].Realtime == false)
            {
                try
                {
                    Connection dconn = connectionMap[queryConn];
                    string schema = dconn.Schema;
                    if (queryConn.Equals(Database.DEFAULT_CONNECTION))
                    {
                        conn = Util.getConnectionWithVariedRetries(sp.getFullConnectString(), sp.DatabaseType);
                    }
                    else
                    {
                        string password = Performance_Optimizer_Administration.MainAdminForm.decrypt(dconn.Password, Performance_Optimizer_Administration.MainAdminForm.SecretPassword);
                        conn = ConnectionPool.getConnection(sp.getFullConnectString(dconn.ConnectString, dconn.UserName, password), false);
                    }
                    tables = Database.getTables(conn, schema);
                }
                catch (Exception ex)
                {
                    ErrorLabel.Visible = true;
                    ErrorLabel.Text = "Error: " + HttpUtility.HtmlEncode(ex.Message);
                    return;
                }
                finally
                {
                    ConnectionPool.releaseConnection(conn);
                }
            }
            else
            {
                Connection qconn = connectionMap[queryConn];
                string qschema = (qconn != null && qconn.Realtime && (!qconn.LocalETL)) ? null : sp.Schema;
                object[][] result = LiveAccessUtil.getSchemaTables(Session, null, maf, sp, connectionMap[queryConn].Name, qschema);
                if (result == null)
                    return;
                tables = new List<string>();
                foreach (object[] row in result)
                {
                    tables.Add((string)row[2]);
                }
            }
            DataTable dt = new DataTable("Tables");
            dt.Columns.Add("TABLE_NAME");
            if (tables != null)
            {
                foreach (string tname in tables)
                {
                    DataRow dr = dt.NewRow();
                    dr[0] = tname;
                    dt.Rows.Add(dr);
                }
            }
            ErrorLabel.Visible = false;
            ResultGrid.Visible = true;
            ResultGrid.Columns.Clear();
            ButtonField bf = new ButtonField();
            bf.DataTextField = "TABLE_NAME";
            bf.DataTextFormatString = "Details";
            bf.CommandName = "SCHEMA";
            bf.ButtonType = ButtonType.Link;
            ResultGrid.Columns.Add(bf);
            ResultGrid.DataSource = dt;
            ResultGrid.DataBind();            
        }

        protected void ResultGrid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                Space sp = (Space)Session["space"];
                if (sp == null)
                    return;
                MainAdminForm maf = (MainAdminForm)Session["MAF"];
                if (maf == null)
                    return;
                int index = int.Parse((string)e.CommandArgument);
                string tableName = ResultGrid.Rows[index].Cells[1].Text;
                string queryConn = QueryConnection.SelectedValue;
                if (connectionMap.ContainsKey(queryConn) && connectionMap[queryConn].Realtime == false)
                {
                    Acorn.DBConnection.QueryConnection conn = null;
                    try
                    {
                        Connection dconn = connectionMap[queryConn];
                        if (queryConn.Equals(Database.DEFAULT_CONNECTION))
                        {
                            conn = Util.getConnectionWithVariedRetries(sp.getFullConnectString(), sp.DatabaseType);
                        }
                        else
                        {
                            string password = Performance_Optimizer_Administration.MainAdminForm.decrypt(dconn.Password, Performance_Optimizer_Administration.MainAdminForm.SecretPassword);
                            conn = ConnectionPool.getConnection(sp.getFullConnectString(dconn.ConnectString, dconn.UserName, password), false);
                        }
                        if (tableName != null)
                        {
                            DataTable dt = null;
                            if (Database.isDBTypeIB(dconn.ConnectString))
                            {
                                // hack - GetSchema for some IB tables can cause a fault that crashes IIS
                                // dt = conn.GetSchema("Columns", new string[] { sp.Schema, null, tableName });
                                DBConnection.QueryCommand cmd = conn.CreateCommand();
                                cmd.CommandText = "select * from " + dconn.Schema + "." + tableName + " LIMIT 1"; ;
                                DBConnection.QueryReader reader = cmd.ExecuteReader();
                                dt = reader.GetSchemaTable();
                            }
                            else
                            {
                                string schema = dconn.Schema;
                                if (conn.isDBTypeOracle())
                                {
                                    schema = schema == null ? null : schema.ToUpper();
                                    tableName = tableName == null ? null : tableName.ToUpper();
                                }
                                else if (conn.isDBTypeParAccel())
                                {
                                    schema = schema == null ? null : schema.ToLower();
                                    tableName = tableName == null ? null : tableName.ToLower();
                                }
                                dt = conn.GetSchema("Columns", new string[] { null, schema, tableName });
                            }
                            ResultGrid.Visible = true;
                            ResultGrid.Columns.Clear();
                            ResultGrid.DataSource = dt;
                            ResultGrid.DataBind();
                        }
                    }
                    finally
                    {
                        ConnectionPool.releaseConnection(conn);
                    }
                }
                else
                {
                    Connection qconn = connectionMap[queryConn];
                    string qschema = (qconn != null && qconn.Realtime && (!qconn.LocalETL)) ? null : sp.Schema;
                    object[][] result = LiveAccessUtil.getTableSchema(Session, null, maf, sp, connectionMap[queryConn].Name, new string[] { tableName }, qschema);
                    DataTable dt = new DataTable();
                    dt.Columns.Add("TABLE_CAT");
                    dt.Columns.Add("TABLE_SCHEM");
                    dt.Columns.Add("TABLE_NAME");
                    dt.Columns.Add("COLUMN_NAME");
                    dt.Columns.Add("DATA_TYPE");
                    dt.Columns.Add("TYPE_NAME");
                    dt.Columns.Add("COLUMN_SIZE");
                    foreach (object[] row in result)
                    {
                        DataRow dr = dt.NewRow();
                        dr["TABLE_CAT"] = row[0];
                        dr["TABLE_SCHEM"] = row[1];
                        dr["TABLE_NAME"] = row[2];
                        dr["COLUMN_NAME"] = row[3];
                        dr["DATA_TYPE"] = row[4];
                        dr["TYPE_NAME"] = row[5];
                        dr["COLUMN_SIZE"] = row[6];
                        dt.Rows.Add(dr);
                    }
                    ResultGrid.Visible = true;
                    ResultGrid.Columns.Clear();
                    ResultGrid.DataSource = dt;
                    ResultGrid.DataBind();
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Exception in executing row command", ex);
            }
        }

        protected void buildApplication(object sender, EventArgs e)
        {
            Space sp = (Space)Session["space"];
            if (sp == null)
                return;
            MainAdminForm maf = (MainAdminForm)Session["MAF"];
            if (maf == null)
                return;
            User u = (User)Session["user"];
            if (u == null)
                return;
            Acorn.DBConnection.QueryConnection conn = null;
            Dictionary<string, int> map = null;
            try
            {
                conn = ConnectionPool.getConnection();
                map = Database.getCurrentLoadNumbers(conn, mainSchema, sp.ID);
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
            Util.buildApplication(maf, mainSchema, DateTime.MinValue, sp.LoadNumber, map, sp, Session);
            Util.saveApplication(maf, sp, Session, u);
        }

        protected void removeDependencies(object sender, EventArgs e)
        {
            Space sp = (Space)Session["space"];
            if (sp == null)
                return;
            MainAdminForm maf = (MainAdminForm)Session["MAF"];
            if (maf == null)
                return;
            User u = (User)Session["user"];
            if (u == null)
                return;
            bool changed = false;
            bool isInUberTransaction = maf.isInUberTransaction();
            maf.setInUberTransaction(true);

            if (maf.dependencies != null && maf.dependencies.Count > 0)
            {
                maf.dependencies.Clear();
                changed = true;
            }

            List<Hierarchy> hlist = maf.hmodule.getHierarchies();
            foreach (Hierarchy h in hlist)
            {
                if (!h.ProcessedDependencies)
                {
                    h.ProcessedDependencies = true;
                    changed = true;
                    maf.hmodule.updateHierarchy(h);
                }
            }

            if (changed)
            {
                Util.saveApplication(maf, sp, Session, u);
            }

            maf.setInUberTransaction(isInUberTransaction);
        }

        protected void disableImpliedGrainsProcessing(object sender, EventArgs e)
        {
            Space sp = (Space)Session["space"];
            if (sp == null)
                return;
            MainAdminForm maf = (MainAdminForm)Session["MAF"];
            if (maf == null)
                return;
            User u = (User)Session["user"];
            if (u == null)
                return;
            bool isInUberTransaction = maf.isInUberTransaction();
            maf.setInUberTransaction(true);
            maf.disableImpliedGrainsProcessing = true;
            Util.saveApplication(maf, sp, Session, u);
            maf.setInUberTransaction(isInUberTransaction);
        }

        private User user = null;
        private Space space = null;
        private MainAdminForm maf = null;

        protected void SeedCacheForDashboardQueries(object sender, EventArgs e)
        {
            Space sp = (Space)Session["space"];
            if (sp == null)
                return;
            User u = (User)Session["user"];
            if (u == null)
                return;
            MainAdminForm maf = (MainAdminForm)Session["MAF"];
            if (maf == null)
                return;
            user = u;
            space = sp;
            Thread t = new Thread(new ThreadStart(runSeedCacheOperation));
            t.IsBackground = true;
            t.Start();            
        }

        private void runSeedCacheOperation()
        {
            try
            {
                // Default to global if user-level pref does not exist
                IDictionary<string, string> localePref = ManagePreferences.getPreference(null, user.ID, "Locale");
                // Only query for the user-level preference
                IDictionary<string, string> timezonePref = ManagePreferences.getUserPreference(null, user.ID, "TimeZone");
                string timezone = null;
                if (timezonePref.ContainsKey("TimeZone"))
                {
                    timezone = timezonePref["TimeZone"];
                }
                string localprotocol = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["LocalProtocol"];
                SpaceConfig sc = Util.getSpaceConfiguration(space.Type);
                TrustedService.TrustedService ts = new TrustedService.TrustedService();
                ts.Url = localprotocol + sc.LocalURL + "/SMIWeb/services/TrustedService";
                ts.Timeout = Timeout.Infinite;
                string[] importedSpacesInfoArray = Util.getImportedPackageSpacesInfoArray(space, maf);
                ts.seedCache(space.Directory, space.ID.ToString(), user.Username, localePref["Locale"], timezone, true, true, false, true, importedSpacesInfoArray);
            }
            catch (Exception e)
            {
                Global.systemLog.Warn("Exception in runSeedCacheOperation : ", e);
            }
            user = null;
            space = null;
        }

        protected void RunQuery(object sender, EventArgs e)
        {
            Space sp = (Space)Session["space"];
            if (sp == null || QueryBox.Text.Length == 0)
                return;
            ResultGrid.Columns.Clear();
            try
            {
                string query = QueryBox.Text.ToLower().Trim();
                string queryConn = QueryConnection.SelectedValue;
                Connection conn = connectionMap[queryConn];
                bool isXMLA = Database.isDBTypeXMLA(conn);
                if (query.StartsWith("select") || query.StartsWith("show") || (isXMLA && query.StartsWith("(")))
                {
                    setResultGridDataSource(sp);
                    ResultGrid.Visible = true;
                    ResultLabel.Visible = false;
                    ErrorLabel.Visible = false;
                }
                else 
                {
                    ErrorLabel.Visible = true;
                    ErrorLabel.Text = "Only SELECT is allowed - contact Birst Operations if you need modifications to the database";
                    ResultGrid.Visible = false;
                    ResultLabel.Visible = false;
                    /* security hole - only operations should be able to update the database
                    QueryConnection conn = ConnectionPool.getConnection(sp.getFullConnectString());
                    QueryCommand cmd = conn.CreateCommand();
                    cmd.CommandText = QueryBox.Text;
                    cmd.CommandTimeout = 30 * 60;
                    int result = cmd.ExecuteNonQuery();
                    ResultGrid.Visible = false;
                    ResultLabel.Visible = true;
                    ErrorLabel.Visible = false;
                    ResultLabel.Text = result + " rows affected";
                    cmd.Dispose();
                    ConnectionPool.releaseConnection(conn);
                    */
                }
            }
            catch (Exception ex)
            {
                ErrorLabel.Visible = true;
                ErrorLabel.Text = HttpUtility.HtmlEncode(ex.Message);
                ResultGrid.Visible = false;
                ResultLabel.Visible = false;
            }
        }

        protected void SqlDataSource1_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
        {
        }

        private void setResultGridDataSource(Space sp)
        {
            string queryConn = QueryConnection.SelectedValue;
            if (connectionMap.ContainsKey(queryConn) && connectionMap[queryConn].Realtime == false)
            {
                DBConnection.QueryConnection conn = null;
                try
                {
                    Connection dconn = connectionMap[queryConn];
                    if (queryConn.Equals(Database.DEFAULT_CONNECTION))
                    {
                        conn = Util.getConnectionWithVariedRetries(sp.getFullConnectString(), sp.DatabaseType);
                    }
                    else
                    {
                        string password = Performance_Optimizer_Administration.MainAdminForm.decrypt(dconn.Password, Performance_Optimizer_Administration.MainAdminForm.SecretPassword);
                        conn = ConnectionPool.getConnection(sp.getFullConnectString(dconn.ConnectString, dconn.UserName, password), false);
                    }
                    DBConnection.QueryCommand cmd = conn.CreateCommand();
                    cmd.CommandTimeout = Int32.Parse(TimeOutBoxID.Text);

                    int count = Int32.Parse(ResultSetSizeBoxID.Text);
                    string query = QueryBox.Text;
                    if (conn.isDBTypeParAccel()) // paraccel and redshift do not have cursored fetch, need to limit the result set via SQL
                        query = "SELECT BIRST.* FROM (" + query + ") AS BIRST LIMIT " + count;
                    cmd.CommandText = query;
                    Global.systemLog.Info("Repository.aspx query:   " + cmd.CommandText);
                    Global.systemLog.Info("Repository.aspx timeout: " + cmd.CommandTimeout + " seconds");
                    Global.systemLog.Info("Repository.aspx query row count limit: " + count);

                    DBConnection.QueryReader reader = cmd.ExecuteReader();

                    DataTable dt = new DataTable();
                    int colIndex = -1;
                    for (int i = 0; i < reader.FieldCount; i++)
                    {
                        colIndex++;
                        string column = reader.GetName(i);
                        if (dt.Columns.Contains(column))
                            column = column + "-" + colIndex;
                        dt.Columns.Add(column);
                    }
                    while (reader.Read() && count-- > 0)
                    {
                        DataRow dr = dt.NewRow();
                        for (int i = 0; i < reader.FieldCount; i++)
                        {
                            dr[i] = reader.IsDBNull(i) ? null : reader.GetValue(i).ToString();
                        }
                        dt.Rows.Add(dr);
                    }
                    ResultGrid.DataSource = dt;
                    ResultGrid.DataBind();
                }
                finally
                {
                   ConnectionPool.releaseConnection(conn);
                }
            }
            else
            {
                object[][] result = LiveAccessUtil.executeLiveAccessQuery(Session, null, sp, connectionMap[queryConn].Name, QueryBox.Text, true);
                if (result != null && result.Length > 0 && result[0] != null && result[0].Length > 0)
                {
                    DataTable dt = new DataTable();
                    int colIndex = -1;
                    foreach (object header in result[0])
                    {
                        colIndex++;
                        string column = (string)header;
                        if (dt.Columns.Contains(column))
                            column = column + "-" + colIndex;
                        dt.Columns.Add(column);
                    }
                    for (int i = 1; i < result.Length; i++)
                    {
                        DataRow dr = dt.NewRow();
                        for (int j = 0; j < result[i].Length; j++)
                        {
                            dr[j] = (string)result[i][j];
                        }
                        dt.Rows.Add(dr);
                    }
                    ResultGrid.DataSource = dt;
                    ResultGrid.DataBind();
                }
            }
        }

        protected void ClearSFDCLoader(object sender, EventArgs e)
        {
            Space sp = Util.getSessionSpace(Session);
            if (sp == null)
            {
                Global.systemLog.Warn("Session returned null space to clear SFDC loader");
                return;
            }
            User u = Util.getSessionUser(Session);
            if (u == null)
            {
                Global.systemLog.Warn("Session returned null user to clear SFDC loader");
                return;
            }

            try
            {
                SalesforceLoader.clearLoader(sp.ID);
                Util.deleteSFDCExtractFile(sp.Directory);
                ConnectorUtils.deleteAllConnectorSpecificExtractFiles(Session, sp, u);
                Global.systemLog.Info("SFDC loader cleared from SalesforceLoader map for space and extract lock file deleted for " + sp.ID);
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Error while clearing SFDC loader ", ex);
            }
        }

        protected void InvalidateDashboardCache(object sender, EventArgs e)
        {
            Space sp = Util.getSessionSpace(Session);
            if (sp == null)
            {
                Global.systemLog.Warn("Session returned null space to invalidate dashboard cache");
                return;
            }
            sp.invalidateDashboardCache();
        }

        protected void EncryptValue(object sender, EventArgs e)
        {
            string cleartext = CryptValue.Text;
            string encryptedtext = Performance_Optimizer_Administration.MainAdminForm.encrypt(cleartext, Performance_Optimizer_Administration.MainAdminForm.SecretPassword);
            CryptLabel.Text = HttpUtility.HtmlEncode(encryptedtext);
        }

        protected void DecryptValue(object sender, EventArgs e)
        {
            string encryptedtext = CryptValue.Text;
            string cleartext = Performance_Optimizer_Administration.MainAdminForm.decrypt(encryptedtext, Performance_Optimizer_Administration.MainAdminForm.SecretPassword);
            CryptLabel.Text = HttpUtility.HtmlEncode(cleartext);
        }

        private void SetDeleteLocalPublishLinks()
        {
            foreach (string conn in localConnectionMap.Keys)
            {
                if (conn == Database.DEFAULT_CONNECTION)
                    continue;
                LinkButton deleteLocalPublishLink = new LinkButton();
                deleteLocalPublishLink.Text = HttpUtility.HtmlEncode("Delete publish.lock file (" + conn + ")");
                deleteLocalPublishLink.ID = HttpUtility.HtmlEncode(localConnectionMap[conn].Name + "_delete");
                deleteLocalPublishLink.Click += new EventHandler(deleteLocalPublishLock);
                Literal lbLiteral = new Literal();
                lbLiteral.Text = "<br/>";
                GetDeleteLocalPublishPanel.Controls.Add(deleteLocalPublishLink);
                GetDeleteLocalPublishPanel.Controls.Add(lbLiteral);
            }
        }

        private void deleteLocalPublishLock(object sender, EventArgs e)
        {
            Space sp = (Space)Session["space"];
            if (sp == null)
                return;
            string connName = ((LinkButton)sender).ID.ToString().Substring(0, ((LinkButton)sender).ID.ToString().LastIndexOf('_'));
            bool isSuccess = LiveAccessUtil.sendCommandToLiveAccess(sp, connName, "deletepublishlock");
            if (!isSuccess)
            {
                Global.systemLog.Debug("Not able to delete publish.lock file for space " + sp.ID.ToString() + " using " + connName + " connection");
            }
        }

        protected void deletePublishLock(object sender, EventArgs e)
        {
            Space sp = (Space)Session["space"];
            if (sp == null)
                return;
            MainAdminForm maf = (MainAdminForm)Session["MAF"];
            if (maf == null)
                return;
            User u = (User)Session["user"];
            if (u == null)
                return;
            string path = Path.Combine(sp.Directory, "publish.lock");
            FileInfo fi = new FileInfo(path);
            if (fi.Exists)
            {
                try
                {
                    fi.Delete();
                }
                catch (Exception ex)
                {
                    Global.systemLog.Error("Error while deleting publish.lock file for space " + sp.ID.ToString(), ex);
                }
            }
            else
            {
                Global.systemLog.Info("Not able to delete publish.lock file. File not exists for space " + sp.ID.ToString() + ". File not exists at " + path);
            }
            Util.deleteLoadLockFile(sp);
        }

        protected void AddIDP_Click(object sender, EventArgs e)
        {
            resetSamlErrorLabel();
            QueryConnection conn = null;
            try
            {
                conn = ConnectionPool.getConnection();
                string idpNameVal = !string.IsNullOrWhiteSpace(IdpNameValue.Text) ? IdpNameValue.Text.Trim() : null;
                string idpIssuerVal = !string.IsNullOrWhiteSpace(IdpIssuerIDValue.Text) ? IdpIssuerIDValue.Text.Trim() : null;
                string certValue = !string.IsNullOrWhiteSpace(CertificateValue.Text) ? CertificateValue.Text : null;
                if (idpNameVal == null || idpNameVal == null || certValue == null)
                {
                    setSamlErrorLabel("Please supply the required fields");
                    return;
                }

                List<IdpMetaData> idpInfoList = Database.getSamlInfoByIdpName(conn, Util.mainSchema, idpNameVal);
                if (idpInfoList != null && idpInfoList.Count > 0)
                {
                    setSamlErrorLabel("IdpName already exists. Please supply a different idpName");
                    return;
                }

                HashSet<Guid> accountIds = new HashSet<Guid>();
                if (!validateInputSamlAccounts(conn, mainSchema, out accountIds))
                {
                    return;
                }

                IdpMetaData idpInfo = new IdpMetaData();
                idpInfo.idpName = idpNameVal;
                idpInfo.idpIssuerID = idpIssuerVal;
                idpInfo.cert = certValue;
                int timeout = -1;
                int.TryParse(TimeoutValue.Text, out timeout);
                idpInfo.timeout = timeout;
                idpInfo.active = ActiveValue.Checked;
                idpInfo.signIdpRequest = SignIdpValue.Checked;
                string logoutPage = LogoutPageValue.Text;
                if (!string.IsNullOrWhiteSpace(logoutPage))
                {
                    if (!Util.isValidHttpUrl(logoutPage))
                    {
                        setSamlErrorLabel("Invalid login page url");
                        return;
                    }
                    idpInfo.logoutPage = logoutPage.Trim();
                }

                string errorPage = ErrorPageValue.Text;
                if (!string.IsNullOrWhiteSpace(errorPage))
                {
                    if (!Util.isValidHttpUrl(errorPage))
                    {
                        setSamlErrorLabel("Invalid error page url");
                        return;
                    }
                    idpInfo.errorPage = errorPage.Trim();
                }
                idpInfo.idpBinding = "binding=urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST";
                string idpUrl = IdpUrlValue.Text;
                if (!string.IsNullOrWhiteSpace(idpUrl))
                {
                    if (!Util.isValidHttpUrl(idpUrl))
                    {
                        setSamlErrorLabel("Invalid idp page url");
                        return;
                    }
                    idpInfo.idpURL = idpUrl.Trim();
                }
                idpInfo.spInitiated = SPInitValue.Checked;
                Database.addSamlIdpInfo(conn, mainSchema, idpInfo);
                Database.addSamlAccountMapping(conn, mainSchema, idpInfo, accountIds);
                resetSamlInputs();
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Exception while adding idp information : " + ex.Message, ex);
                string msg = "Error while adding idp information. Please look into logs for more details";
                setSamlErrorLabel(msg);
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
        }

        private bool validateInputSamlAccounts(QueryConnection conn, string mainSchema, out HashSet<Guid> accountIds)
        {
            accountIds = new HashSet<Guid>();
            if (SamlAccountIdValues.Text != null && SamlAccountIdValues.Text.Trim().Length > 0)
            {
                string[] acctIds = SamlAccountIdValues.Text.Split(',');
                foreach (string accountId in acctIds)
                {
                    Guid parsedAccountId = Guid.Empty;
                    if (!Guid.TryParse(accountId, out parsedAccountId))
                    {
                        setSamlErrorLabel("Invalid AccountID entered");
                        return false;
                    }
                    if (!Database.isValidAccount(conn, mainSchema, parsedAccountId))
                    {
                        setSamlErrorLabel("Invalid accountId provided. Please supply a valid accountName");
                        return false;
                    }
                    accountIds.Add(parsedAccountId);
                }
            }
            return true;
        }
        private void updateSamlAccounts(QueryConnection conn, string schema, IdpMetaData idpInfo, HashSet<Guid> accountIdValues)
        {   
            if (accountIdValues != null && accountIdValues.Count > 0)
            {
                List<Guid> existingAccounts = Database.getSamlAccountMappings(conn, schema, idpInfo.ID);
                HashSet<Guid> toAddAccountIds = new HashSet<Guid>(accountIdValues);
                if (existingAccounts != null && existingAccounts.Count > 0)
                {
                    foreach (Guid accountId in accountIdValues)
                    {
                        if(existingAccounts.Contains(accountId))
                        {
                            toAddAccountIds.Remove(accountId);
                        }
                    }
                }

                Database.addSamlAccountMapping(conn, schema, idpInfo, toAddAccountIds);
            }
        }

        protected void UpdateIDP_Click(object sender, EventArgs e)
        {
            resetSamlErrorLabel();
            QueryConnection conn = null;
            try
            {
                conn = ConnectionPool.getConnection();
                string idpNameVal = !string.IsNullOrWhiteSpace(IdpNameValue.Text) ? IdpNameValue.Text.Trim() : null;
                string idpIssuerVal = !string.IsNullOrWhiteSpace(IdpIssuerIDValue.Text) ? IdpIssuerIDValue.Text.Trim() : null;
                string certValue = !string.IsNullOrWhiteSpace(CertificateValue.Text) ? CertificateValue.Text : null;
                if (idpNameVal == null || idpNameVal == null)
                {
                    setSamlErrorLabel("Please supply the required fields");
                    return;
                }

                List<IdpMetaData> idpInfoList = Database.getSamlInfoByIdpName(conn, Util.mainSchema, idpNameVal);
                if (idpInfoList != null && idpInfoList.Count == 0)
                {
                    setSamlErrorLabel("IdpName does not exist. Please supply a different idpName");
                    return;
                }

                HashSet<Guid> accountIds = new HashSet<Guid>();
                if (!validateInputSamlAccounts(conn, mainSchema, out accountIds))
                {
                    return;
                }

                IdpMetaData existingIdpInfo = idpInfoList[0];
                // there should only be one with the name
                IdpMetaData idpInfo = idpInfoList[0];
                idpInfo.idpName = idpNameVal;
                idpInfo.idpIssuerID = idpIssuerVal;
                idpInfo.cert = string.IsNullOrWhiteSpace(certValue) ? existingIdpInfo.cert : certValue;
                int timeout = -1;
                int.TryParse(TimeoutValue.Text, out timeout);
                idpInfo.timeout = timeout;
                idpInfo.active = ActiveValue.Checked;
                idpInfo.signIdpRequest = SignIdpValue.Checked;
                string logoutPage = LogoutPageValue.Text;
                if (string.IsNullOrWhiteSpace(logoutPage))
                {
                    logoutPage = existingIdpInfo.logoutPage;
                }
                else if (logoutPage.Trim().ToLower() == "null")
                {
                    logoutPage = null;
                }
                else if (!Util.isValidHttpUrl(logoutPage))
                {
                    setSamlErrorLabel("Invalid logout page url");
                    return;
                }

                idpInfo.logoutPage = logoutPage;

                string errorPage = ErrorPageValue.Text;
                if (string.IsNullOrWhiteSpace(errorPage))
                {
                    errorPage = existingIdpInfo.errorPage;
                }
                else if (errorPage.Trim().ToLower() == "null")
                {
                    errorPage = null;
                }
                else if (!Util.isValidHttpUrl(errorPage))
                {
                    setSamlErrorLabel("Invalid error page url");
                    return;
                }
                idpInfo.errorPage = errorPage;

                idpInfo.idpBinding = "binding=urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST";
                string idpURL = IdpUrlValue.Text;
                if (string.IsNullOrWhiteSpace(idpURL))
                {
                    idpURL = existingIdpInfo.idpURL;
                }
                else if (idpURL.Trim().ToLower() == "null")
                {
                    idpURL = null;
                }
                else if (!Util.isValidHttpUrl(idpURL))
                {
                    setSamlErrorLabel("Invalid idp url");
                    return;
                }
                idpInfo.idpURL = idpURL;
                idpInfo.spInitiated = SPInitValue.Checked;
                Database.updateSamlIdpInfo(conn, Util.mainSchema, idpInfo.ID, idpInfo);
                updateSamlAccounts(conn, mainSchema, idpInfo, accountIds);
                resetSamlInputs();
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Exception while updating idp information : " + ex.Message, ex);
                string msg = "Error while updating idp information. Please look into logs for more details";
                setSamlErrorLabel(msg);
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
        }

        private void resetSamlInputs()
        {
            IdpNameValue.Text = null;
            IdpIssuerIDValue.Text = null;
            CertificateValue.Text = null;
            TimeoutValue.Text = null;
            ActiveValue.Checked = false;
            LogoutPageValue.Text = null;
            ErrorPageValue.Text = null;
            IdpUrlValue.Text = null;
            SignIdpValue.Text = null;
            SPInitValue.Checked = false;
            SamlAccountIdValues.Text = null;

        }

        private void setSamlErrorLabel(string errorMsg)
        {
            SamlActionError.Visible = true;
            SamlActionError.Text = errorMsg;
        }

        private void resetSamlErrorLabel()
        {
            SamlActionError.Visible = false;
        }
    }
}
