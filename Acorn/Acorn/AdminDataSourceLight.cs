﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Acorn
{
    public class DataSourceLight
    {
        public string Name;
        public string DisplayName;
        public int Status;
        public string StatusMessage;
        public bool Enabled;
        public DateTime LastUploadDate;
        public string[] LoadGroups;
        public bool UseBirstLocalForSource;
    }
}