﻿//
// Copyright (C) 2009-2011 Birst, Inc. All rights reserved.
// BIRST PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
//

using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Web.SessionState;
using System.IO;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Net.Security;
using System.Collections.Specialized;

namespace Acorn
{
    public partial class SSOTest : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack)
                return;

            if (Request["internal"] != null && Request["internal"] == "true")
                designerOptionsPanel.Visible = true;

            // see if all of the parameters have been set
            if (Request["baseUrl"] != null)
            {
                baseUrl.Text = Request["baseUrl"];
            }
            if (Request["username"] != null)
            {
                username.Text = Request["username"];
            }
            if (Request["spacename"] != null)
            {
                spacename.Text = Request["spacename"];
            }
            if (Request["BirstSpaceId"] != null)
            {
                BirstSpaceId.Text = Request["BirstSpaceId"];
            }
            if (Request["ssopassword"] != null)
            {
                ssopassword.Text = Request["ssopassword"];
            }
            if (Request["sessionVars"] != null)
            {
                sessionVars.Text = Request["sessionVars"];
            }
            if (Request["module"] != null)
            {
                module.Text = Request["module"];
            }
            if (Request["dashboardname"] != null)
            {
                dashboardname.Text = Request["dashboardname"];
            }
            if (Request["dashboardName2"] != null)
            {
                dashboardName2.Text = Request["dashboardName2"];
            }
            if (Request["pageName"] != null)
            {
                pageName.Text = Request["pageName"];
            }
            if (Request["reportName"] != null)
            {
                reportName.Text = Request["reportName"];
            }
            if (Request["extraParameters"] != null)
            {
                extraParameters.Text = Request["extraParameters"];
            }
            if (Request["embedded"] != null)
            {
                embedded.Checked = Boolean.Parse(Request["embedded"]);
            }
            if (Request["hideDashboardNavigation"] != null)
            {
                hideDashboardNavigation.Checked = Boolean.Parse(Request["hideDashboardNavigation"]);
            }
            if (Request["hideNav"] != null)
            {
                hideDashboardNavigation.Checked = Boolean.Parse(Request["hideNav"]);
            }
            if (Request["hideDashboardPrompts"] != null)
            {
                hideDashboardPrompts.Checked = Boolean.Parse(Request["hideDashboardPrompts"]);
            }
            if (Request["submit"] != null)
            {
                SSOTest_Submit(sender, e);
            }

            if (Request["logPageInfo"] != null)
            {
                logPageInfo(Request);
            }
            Copyright.Text = Util.getCopyright();
            Version.Text = Util.getAppVersion();
        }

        // process the request from the sample web form
        protected void SSOTest_Submit(object sender, EventArgs e)
        {
            Global.systemLog.Debug("baseUrl: " + baseUrl.Text);
            Global.systemLog.Debug("username: " + username.Text + ", spaceid: " + BirstSpaceId.Text + ", spacename: " + spacename.Text + ", ssopassword: " + ssopassword.Text);
            Global.systemLog.Debug("module: " + module.SelectedValue);
            // get the security token
            string birstURL = null;
            string tokenPostData = null;
            string token = getSecurityToken(baseUrl.Text + "/TokenGenerator.aspx", username.Text, BirstSpaceId.Text, spacename.Text, ssopassword.Text, sessionVars.Text, TestUsed.Checked, out birstURL, out tokenPostData);
            if (token == null || token.Length == 0)
            {
                Response.Status = "400 Bad Request";
                return;
            }

            string redirectUrl = birstURL + "/SSO.aspx?BirstSSOToken=" + token;
            Global.systemLog.Debug("Redirecting to " + redirectUrl);
            string moduleValue = module.SelectedValue.ToLower();
            if (moduleValue == "adhoc")
                moduleValue = "designer";
            if (moduleValue == "dashboard" || moduleValue == "designer" || moduleValue == "customupload" || moduleValue == "export")
            {
                redirectUrl = redirectUrl + "&birst.module=" + moduleValue;
            } 
            if (dashboardname.Text != null && dashboardname.Text.Length > 0)
                redirectUrl = redirectUrl + "&birst.name=" + HttpUtility.UrlEncode(dashboardname.Text);
            if (dashboardName2.Text != null && dashboardName2.Text.Length > 0)
                redirectUrl = redirectUrl + "&birst.dashboard=" + HttpUtility.UrlEncode(dashboardName2.Text);
            if (pageName.Text != null && pageName.Text.Length > 0)
                redirectUrl = redirectUrl + "&birst.page=" + HttpUtility.UrlEncode(pageName.Text);
            if (reportName.Text != null && reportName.Text.Length > 0)
                redirectUrl = redirectUrl + "&birst.reportName=" + HttpUtility.UrlEncode(reportName.Text);
            if (designerOptions.Text != null && designerOptions.Text.Length > 0)
                redirectUrl = redirectUrl + "&birst.designerOptions=" + designerOptions.Text;
            if (HelpURLID.Text != null && HelpURLID.Text.Length > 0)
                redirectUrl = redirectUrl + "&birst.helpURL=" + HttpUtility.UrlEncode(HelpURLID.Text);
            else if (HelpURLIncludeEmptyID.Checked)
                redirectUrl = redirectUrl + "&birst.helpURL=";
            string exportType = exportTypeList.SelectedValue.ToLower();
            if (exportType != null && exportType.Length > 0)
                redirectUrl = redirectUrl + "&birst.exportType=" + exportType;
            if (imageZoom.Text != null && imageZoom.Text.Length > 0)
                redirectUrl = redirectUrl + "&birst.exportZoom=" + imageZoom.Text;
            redirectUrl = redirectUrl + "&birst.embedded=" + embedded.Checked.ToString().ToLower();
            redirectUrl = redirectUrl + "&birst.hideDashboardNavigation=" + hideDashboardNavigation.Checked.ToString().ToLower();
            redirectUrl = redirectUrl + "&birst.hideDashboardPrompts=" + hideDashboardPrompts.Checked.ToString().ToLower();
            redirectUrl = redirectUrl + "&birst.openPageForEdit=" + openForEdit.Checked.ToString().ToLower();
            if (viewMode.SelectedValue != null && viewMode.SelectedValue.Length > 0)
                redirectUrl = redirectUrl + "&birst.viewMode=" + viewMode.SelectedValue.ToLower();
            if (wmode.SelectedValue != null && wmode.SelectedValue.Length > 0)
                redirectUrl = redirectUrl + "&birst.wmode=" + wmode.SelectedValue.ToLower();
            if (filterLayout != null && filterLayout.SelectedValue != null && filterLayout.SelectedValue.Length > 0)
                redirectUrl = redirectUrl + "&birst.filterLayout=" + filterLayout.SelectedValue.ToLower();
            // redirectUrl = redirectUrl + "&birst.openDashboardInEdit=true"; // XXX
            redirectUrl = redirectUrl + "&" + extraParameters.Text;
            Global.systemLog.Debug(redirectUrl);
            int sleep = 0;
            if (Sleep.Text != null && Sleep.Text.Length > 0)
                sleep = Int32.Parse(Sleep.Text);
            if (sleep > 0)
                System.Threading.Thread.Sleep(sleep);
            if (embedded.Checked)
            {
                int width = -1;
                int height = -1;
                if (Width.Text != null && Width.Text.Length > 0)
                    Int32.TryParse(Width.Text, out width);
                if (Height.Text != null && Height.Text.Length > 0)
                    Int32.TryParse(Height.Text, out height);
                if (moduleValue == "export" && imageTag.Checked)
                {
                    if (width > 0 && height > 0)
                        Response.Write("<html><body><pre>Token Request (must be sent via POST): " + tokenPostData + "</pre><pre>Redirect URL: " + redirectUrl + "</pre><img border=1 src=\"" + redirectUrl + "\" width=" + width + " height=" + height + "></body></html>");
                    else
                        Response.Write("<html><body><pre>Token Request (must be sent via POST): " + tokenPostData + "</pre><pre>Redirect URL: " + redirectUrl + "</pre><img border=1 src=\"" + redirectUrl + "\"></body></html>");
                }
                else
                {
                    if (width > 0 && height > 0)
                        Response.Write("<html><body><pre>Token Request (must be sent via POST): " + tokenPostData + "</pre><pre>Redirect URL: " + redirectUrl + "</pre><iframe border=1 src=\"" + redirectUrl + "\" width=" + width + " height=" + height + "></iframe></body></html>");
                    else
                        Response.Write("<html><body><pre>Token Request (must be sent via POST): " + tokenPostData + "</pre><pre>Redirect URL: " + redirectUrl + "</pre><iframe border=1 src=\"" + redirectUrl + "\"></iframe></body></html>");
                }
            }
            else
            {
                Response.Redirect(redirectUrl);
            }
        }
 
        //
        // POST a request to the security token service for a opaque token for the user and space
        //
        private static string getSecurityToken(string url, string username, string spaceid, string spacename, string ssopassword, string sessionVars, bool testUsed, out string birstURL, out string tokenPostData)
        {
            Global.systemLog.Debug("tokenUrl: " + url);
            WebRequest wrq = WebRequest.Create(url);
            wrq.Method = "POST";    // for security reasons this request must be a POST
            wrq.ContentType = "application/x-www-form-urlencoded; charset=utf-8";  // make it look like a form submission

            // create the post data and write it to the stream
            string postData = "username=" + username + "&ssopassword=" + ssopassword;
            if (spaceid != null && spaceid.Length > 0)
            {
                postData += "&BirstSpaceId=" + spaceid;
            }
            else
            {
                postData += "&spacename=" + spacename;
            }
            if (sessionVars != null && sessionVars.Length > 0)
            {
                postData = postData + "&sessionVars=" + sessionVars;
            }
            if (testUsed)
            {
                postData = postData + "&testUsed=" + testUsed;
            }
            tokenPostData = url + "?" + postData;
            Global.systemLog.Debug("postData: " + postData);
            byte[] byteArray = Encoding.UTF8.GetBytes(postData);
            wrq.ContentLength = byteArray.Length;
            Stream dataStream = wrq.GetRequestStream();
            dataStream.Write(byteArray, 0, byteArray.Length);
            dataStream.Close();

            WebResponse wresp = null;
            try
            {
                wresp = wrq.GetResponse();
            }
            catch (Exception ex)
            {
                Global.systemLog.Debug(ex);
                birstURL = null;
                return null;
            }
            // get the redirect url
            birstURL = wresp.Headers["BirstURL"];

            // read the response - the opaque token
            StreamReader sr = new StreamReader(wresp.GetResponseStream(), System.Text.Encoding.ASCII);
            string result = sr.ReadToEnd();
            sr.Close();
            wresp.Close();
            return result;
        }

        public static void logPageInfo(HttpRequest Request)
        {
            Global.systemLog.Info("Page Request Request.Url - " + Request.Url);
            Global.systemLog.Info("Page Request Request.UrlHostAddress - " + Request.UserHostAddress);
            Global.systemLog.Info("Page Request Request.UserHostName - " + Request.UserHostName);
            Global.systemLog.Info("Page Request Request.RawUrl - " + Request.RawUrl);
            Global.systemLog.Info("Page Request Request.Url.Host - " + Request.Url.Host);
            Global.systemLog.Info("Page Request Request.UrlRefererrer - " + Request.UrlReferrer);
            NameValueCollection headers = Request.Headers;
            if (headers != null)
            {
                foreach (String key in headers.Keys)
                    Global.systemLog.Info("Header: " + key + "=" + headers[key]);
            }
        }
    }
}
