﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Performance_Optimizer_Administration;
using System.Web.SessionState;
using System.IO;
using System.IO.Compression;
using System.Text;
using Acorn.Utils;
using System.Data;
using System.Xml.Serialization;
using System.Xml;
using System.Web.Services.Protocols;
using Amazon.Runtime;

namespace Acorn
{
    public class ManageSources
    {
        private static int NUM_LINES_TO_SCAN = 50000;
        private static bool VALIDATE_GRAIN_MERGE = false;
        private static string awsAccessKeyId = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["AwsAccessKeyId"];
        private static string awsSecretKey = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["AwsSecretKey"];
        private static string connectorsDemoMode = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["ConnectorsDemoMode"];

        public static DataSourceLight[] getSourcesList(HttpSessionState session)
        {
            MainAdminForm maf = (MainAdminForm)session["MAF"];
            if (maf == null)
                return null;
            Space sp = (Space)session["space"];
            if (sp == null)
                return null;
            User u = (User)session["user"];
            if (u == null)
                return null;

            List<DataSourceLight> sourceList = new List<DataSourceLight>();
            List<SourceFile> sflist = maf.sourceFileMod.getSourceFiles();
            List<StagingTable> allStagingTables = maf.stagingTableMod.getAllStagingTables();

            foreach (StagingTable sst in allStagingTables)
            {
                SourceFile ssf = null;
                foreach (SourceFile sf in sflist)
                {
                    if (sst.SourceFile.ToLower() == sf.FileName.ToLower())
                    {
                        ssf = sf;
                        break;
                    }
                }
                if (!sst.LiveAccess && ssf == null)
                    continue;
                DataSourceLight adsl = getDataSourceLight(maf, sp, ssf, sst, false);
                if (adsl != null)
                    sourceList.Add(adsl);
            }
            /*
             * Process imported staging tables
             */
            List<ImportedRepositoryItem> ilist = Util.getImportedTables(maf, session, null);
            foreach (ImportedRepositoryItem it in ilist)
            {
                if (it.st == null || it.hide)
                    continue;
                DataSourceLight ads = getDataSourceLight(it.ispace.maf, it.ispace.sp, it.sf, it.st, true);
                if (ads != null)
                    sourceList.Add(ads);
            }

            return sourceList.ToArray();
        }

        public static DataSourceLight getDataSourceLight(MainAdminForm maf, Space sp, SourceFile sf, StagingTable st, bool imported)
        {
            DataSourceLight adsl = new DataSourceLight();
            if (sf != null)
                adsl.guid = sf.Guid.ToString();
            adsl.ImportedTable = imported;
            adsl.LiveAccessTable = st.LiveAccess;
            adsl.Cardinality = st.Cardinality;
            adsl.UnCached = st.UnCached;
            adsl.Status = AdminDataSource.STATUS_OK;
            if (st != null)
            {
                adsl.Enabled = !st.Disabled;
                Publishability pub = SpaceAdminService.checkPublishability(maf, sp, st);
                if (!pub.publishable && !st.Disabled)
                {
                    adsl.Status = AdminDataSource.STATUS_ERROR;
                    adsl.StatusMessage = pub.message;
                }
                else if (pub.warning)
                {
                    adsl.Status = AdminDataSource.STATUS_WARNING;
                    adsl.StatusMessage = pub.message;
                }
            }
            else
                return null;

            adsl.Name = st.Name;
            adsl.DisplayName = st.getDisplayName();
            if (sf != null)
                adsl.LastUploadDate = sf.LastUploadDate;
            adsl.LoadGroups = st.LoadGroups;
            adsl.SubGroups = st.SubGroups;
            adsl.UseBirstLocalForSource = st.UseBirstLocalForSource;
            adsl.Levels = st.Levels;
            adsl.HierarchyName = st.HierarchyName;
            if (st.Script != null && st.Script.InputQuery != null)
            {
                adsl.Script = st.Script;
            }
            else
            {
                adsl.Script = null;
            }
            return adsl;
        }

        public static AdminRepository GetSources(HttpSessionState session)
        {
            MainAdminForm maf = (MainAdminForm)session["MAF"];
            if (maf == null)
                return null;
            Space sp = (Space)session["space"];
            if (sp == null)
                return null;
            User u = (User)session["user"];
            if (u == null)
                return null;
            List<string> sstringlist = new List<string>();

            /*
             * Process staging tables within the current repository
             */
            foreach (StagingTable sst in maf.stagingTableMod.getAllStagingTables())
            {
                SourceFile ssf = null;
                if (!sst.LiveAccess)
                {
                    foreach (SourceFile sf in maf.sourceFileMod.getSourceFiles())
                    {
                        if (sst.SourceFile.ToLower() == sf.FileName.ToLower())
                        {
                            ssf = sf;
                            break;
                        }
                    }
                }
                AdminDataSource ads = getAdminDataSource(maf, maf, sp, sst, ssf, false);
                if (ads != null)
                {
                    sstringlist.Add(ads.getSerializedString());
                }
            }
            /*
             * Process imported staging tables
             */
            List<ImportedRepositoryItem> ilist = Util.getImportedTables(maf, session, null);
            foreach (ImportedRepositoryItem it in ilist)
            {
                if (it.st == null || it.hide)
                    continue;
                AdminDataSource ads = getAdminDataSource(maf, it.ispace.maf, it.ispace.sp, it.st, it.sf, true);
                if (ads != null)
                {
                    sstringlist.Add(ads.getSerializedString());
                }
            }

            AdminRepository ar = new AdminRepository();
            bool showNewConnectorsUI = false;
            bool.TryParse(connectorsDemoMode, out showNewConnectorsUI);
            if (showNewConnectorsUI)
            {
                ar.ShowNewConnectorsUI = true;
            }
            else
            {
                ar.ShowNewConnectorsUI = false;
            }
            //Below two variables are used in sourceAdmin to enable 'reprocess only modified data soure' feature
            ar.BuilderVersion = maf.builderVersion;            
            ar.EngineVersion = (maf.builderVersion >= 26) ? Util.getProcessingEngineVersion(sp) : 0;            
            ar.ArrayOfSourceStrings = sstringlist.ToArray();
            List<Hierarchy> hlist = new List<Hierarchy>(maf.hmodule.getLatestHierarchies());
            if (maf.Imports != null && maf.Imports != null)
            {
                foreach (ImportedRepositoryItem it in ilist)
                {
                    if (it.dt == null)
                        continue;
                    Hierarchy h = it.ispace.maf.hmodule.getDimensionHierarchy(it.dt.DimensionName);
                    if (h == null)
                    {
                        continue;
                    }
                    bool found = false;
                    foreach(Hierarchy sh in hlist)
                    {
                        if (sh.Name == h.Name)
                        {
                            found = true;
                            break;
                        }
                    }
                    if (!found)
                    {
                        h.Imported = true;
                        hlist.Add(h);
                    }
                }
            }
            ar.Hierarchies = hlist.ToArray();
            ar.TimeDimensionName = maf.timeDefinition == null ? null : maf.timeDefinition.Name;
            ar.RepositoryPath = sp.Directory + "\\repository_dev.xml";
            ar.Published = Status.hasLoadedData(sp, null);
            ar.queryLanguageVersion = sp.QueryLanguageVersion;
            ar.SupportsFullOuterJoin = true;
            foreach (DatabaseConnection dc in maf.connection.connectionList)
            {
                if (dc.Name == "Default Connection")
                {
                    string dname = dc.Driver.ToLower();
                    if (dname == "com.mysql.jdbc.driver" || dname == "memdb.jdbcclient.dbdriver")
                    {
                        ar.SupportsFullOuterJoin = false;
                    }
                }
            }

            ar.HasAdvancedAdmin = u.UserACLs != null && u.UserACLs.Contains(ACLDetails.getACLID(ACLDetails.ACL_ADVANCED_ADMIN));
            ar.Imports = maf.Imports;
            Dictionary<string, object> loadGroups = new Dictionary<string, object>();
            if (maf.aggModule.getAggregateList() != null)
            {
                foreach (Aggregate ag in maf.aggModule.getAggregateList())
                {
                    if (ag.SubGroups == null)
                        continue;
                    foreach (string lgname in ag.SubGroups)
                        if (lgname != Util.LOAD_GROUP_NAME && !loadGroups.ContainsKey(lgname))
                            loadGroups.Add(lgname, null);
                }
            }
            ar.LoadGroups = loadGroups.Keys.ToArray<string>();
            if (maf.RServer != null && maf.RServer.URL != null)
            {
                ar.RServerURL = maf.RServer.URL + (maf.RServer.Port > 0 ? ":" + maf.RServer.Port : "");
            }
            return ar;
        }

        public static AdminDataSource getAdminDataSource(MainAdminForm mainmaf, MainAdminForm maf, Space sp, StagingTable st, SourceFile sf, bool imported)
        {
            AdminDataSource ads = new AdminDataSource();
            ads.Status = AdminDataSource.STATUS_OK;
            if (st != null)
            {
                ads.Enabled = !st.Disabled;
                Publishability pub = SpaceAdminService.checkPublishability(maf, sp, st);
                if (!pub.publishable && !st.Disabled)
                {
                    ads.Status = AdminDataSource.STATUS_ERROR;
                    ads.StatusMessage = pub.message;
                }
                else if (pub.warning)
                {
                    ads.Status = AdminDataSource.STATUS_WARNING;
                    ads.StatusMessage = pub.message;
                }
            }
            else
                return null;
            ads.Name = st.Name;
            ads.SourceKey = null;
            ads.ImportedTable = imported;
            ads.DisplayName = ApplicationBuilder.getDisplayName(st);
            if (st.SourceKey != null)
                ads.SourceKey = st.SourceKey;
            ads.ReadOnly = st.WebReadOnly;
            ads.Levels = st.Levels;
            ads.StagingColumns = st.Columns;
            ads.Transactional = st.Transactional;
            ads.TruncateOnLoad = st.TruncateOnLoad;
            ads.InheritTables = st.InheritTables;

            bool allowBirstLocalForSource = false;
            string liveAccessLoadGroup = null;
            Dictionary<string, LocalETLConfig> leConfigs = BirstConnectUtil.getLocalETLConfigsForSpace(sp);
            if (st.LoadGroups != null && leConfigs.Count > 0)
            {
                foreach (string lg in st.LoadGroups)
                {
                    if (lg.StartsWith(LiveAccessUtil.LIVE_ACCESS_LOAD_GROUP_PREFIX) && leConfigs.Keys.Contains(lg))
                    {
                        liveAccessLoadGroup = lg;
                        break;
                    }
                }
                allowBirstLocalForSource = (liveAccessLoadGroup == null ||
                    (st.UseBirstLocalForSource && st.PreviousLoadGroup != null));
            }
            ads.AllowBirstLocalForSource = allowBirstLocalForSource;
            ads.UseBirstLocalForSource = st.UseBirstLocalForSource;
            ads.DiscoveryTable = st.DiscoveryTable;
            if (liveAccessLoadGroup != null)
                ads.LocalConnForSource = leConfigs[liveAccessLoadGroup].getLocalETLConnection();

            ads.SourceFileColumnPreventUpdate = new bool[st.Columns.Length];
            ads.SourceFileColumnFormat = new string[st.Columns.Length];
            ads.CustomTimeColumn = st.CustomTimeColumn;
            if (st.CustomTimePrefix != null && (st.CustomTimePrefix.Length == 0 || st.CustomTimePrefix == "null")) // deal with bad data, fix it up here
                st.CustomTimePrefix = null;
            ads.CustomTimePrefix = st.CustomTimePrefix;
            ads.CustomTimeShifts = st.CustomTimeShifts;
            ads.SubGroups = st.SubGroups;
            ads.LoadGroups = st.LoadGroups;
            ads.SourceGroups = st.SourceGroups;
            ads.Snapshots = st.Snapshots;
            ads.SnapshotDeleteKeys = st.SnapshotDeleteKeys;
            ads.IncrementalSnapshotFact = st.IncrementalSnapshotFact;
            ads.OverrideLevelKeys = cleanupOverrideLevelKeys(st.OverrideLevelKeys);
            ads.NewColumnTarget = st.NewColumnTarget;
            if (st.Script != null && st.Script.InputQuery != null)
            {
                ads.Script = st.Script;
            }
            else
            {
                ads.Script = null;
            }
            ads.ForeignKeys = st.ForeignKeys;
            ads.FactForeignKeys = st.FactForeignKeys;
            ads.ParentForeignKeySource = st.ParentForeignKeySource;
            ads.HierarchyName = st.HierarchyName;
            ads.LevelName = st.LevelName;
            ads.ExcludeFromModel = st.ExcludeFromModel;
            ads.LiveAccess = st.LiveAccess;
            ads.TableSource = st.TableSource;
            ads.RServerPath = st.RServerPath;
            ads.RExpression = st.RExpression;
            ads.RServerSettings = st.RServerSettings;
            if (sf != null)
            {
                ads.LastUploadDate = sf.LastUploadDate;
                ads.NumColumns = sf.Columns.Length;
                ads.NumRows = sf.NumRows;
                ads.FileSize = sf.FileLength;
                ads.FileExists = File.Exists(sp.Directory + "\\data\\" + sf.FileName);
                if (sf.InputTimeZone != null && !sf.InputTimeZone.Equals("") && sf.InputTimeZone != "null")
                {
                    ads.InputTimeZone = TimeZoneUtils.getTimeZoneDisplayName(sf.InputTimeZone);
                }
                else
                    ads.InputTimeZone = Performance_Optimizer_Administration.Util.SOURCE_TIMEZONE_DEFAULT;
                ads.Encoding = sf.Encoding;
                ads.Separator = sf.Separator;
                ads.ReplaceWithNull = sf.BadColumnValueAction == SourceFile.REPLACE_WITH_NULL;
                ads.SkipRecord = sf.BadColumnValueAction == SourceFile.SKIP_RECORD;
                ads.SourceFileColumnTypes = new string[st.Columns.Length];
                // Set the original imported data type
                if (sf != null && sf.ImportColumns != null)
                    for (int i = 0; i < st.Columns.Length; i++)
                    {
                        if (st.Columns[i].SourceFileColumn != null)
                        {
                            foreach (SourceFile.SourceColumn sfc in sf.ImportColumns)
                            {
                                if (sfc.Name == st.Columns[i].SourceFileColumn)
                                {
                                    ads.SourceFileColumnTypes[i] = sfc.DataType;
                                    break;
                                }
                            }
                        }
                    }
                ads.LockFormat = sf.LockFormat;
                ads.SchemaLock = sf.SchemaLock;
                if (sf.SchemaLock) // set manual sub options only if option 3 is selected
                {
                    ads.AllowAddColumns = sf.AllowAddColumns;
                    ads.AllowNullBindingRemovedColumns = sf.AllowNullBindingRemovedColumns;
                    ads.AllowUpcast = sf.AllowUpcast;
                    ads.AllowVarcharExpansion = sf.AllowVarcharExpansion;
                    ads.AllowValueTruncationOnLoad = sf.AllowValueTruncationOnLoad;
                    ads.FailUploadOnVarcharExpansion = sf.FailUploadOnVarcharExpansion;
                }
                ads.CustomUpload = sf.CustomUpload;
                ads.OnlyQuoteAfterSeparator = sf.OnlyQuoteAfterSeparator;
                if (sf.Columns != null)
                {
                    for (int i = 0; i < st.Columns.Length; i++)
                    {
                        if (st.Columns[i].SourceFileColumn != null && !st.Columns[i].DataType.StartsWith("Date ID:"))
                        {
                            foreach (SourceFile.SourceColumn sfc in sf.Columns)
                            {
                                if (sfc.Name != null && sfc.Name.Equals(st.Columns[i].SourceFileColumn))
                                {
                                    ads.SourceFileColumnPreventUpdate[i] = sfc.PreventUpdate;
                                    ads.SourceFileColumnFormat[i] = sfc.Format;
                                    break;
                                }
                            }
                        }
                    }
                }
                if (ads.SourceKey == null && sf.KeyIndices != null)
                {
                    string[] sk = new string[sf.KeyIndices.Length];
                    bool valid = true;
                    for (int i = 0; i < sf.KeyIndices.Length; i++)
                    {
                        string cname = null;
                        int maxIndex = sf.Columns.Length - 1;
                        if (sf.KeyIndices[i] > maxIndex)
                        {
                            Global.systemLog.Warn("Key indices for source file : " + sf.FileName +
                                " is incorrectly set to " + sf.KeyIndices[i] + " while max index could be " + maxIndex);
                            continue;
                        }
                        foreach (StagingColumn sc in st.Columns)
                        {
                            if (sc.SourceFileColumn == sf.Columns[sf.KeyIndices[i]].Name)
                            {
                                cname = sc.Name;
                                break;
                            }
                        }
                        if (cname == null)
                        {
                            valid = false;
                            break;
                        }
                        sk[i] = cname;
                    }
                    if (valid)
                        ads.SourceKey = sk;
                }
            }
            if (imported)
            {
                foreach (PackageImport pi in mainmaf.Imports)
                {
                    ImportedItem ii = pi.getImportedItem(st.Name, false);
                    if (ii != null)
                    {
                        ads.Locations = SourceLocation.getFromLocations(ii.Locations);
                        ads.addForeignKeySources(ii.ForeignKeys);
                    }
                    if (ads.Locations != null)
                        break;
                }
            }
            else
                ads.Locations = SourceLocation.getFromLocations(st.Locations);
            return ads;
        }

        public static bool isSpaceInIndependentMode(Space sp)
        {
            bool independentMode = false;
            string spaceDirectory = sp.Directory;
            SalesforceSettings settings = SalesforceSettings.getSalesforceSettings(spaceDirectory + "//sforce.xml");
            if (settings != null && settings.objects != null && settings.objects.Length > 0)
            {
                // the space is sforce configured if it has certain objects
                independentMode = settings.automatic;
            }
            return independentMode;
        }

        public static AdminDataSource getSourceDetails(HttpSessionState session, string stagingTableName, string sourceFileName )
        {
            MainAdminForm maf = (MainAdminForm)session["MAF"];
            if (maf == null)
                return null;
            Space sp = (Space)session["space"];
            if (sp == null)
                return null;
                   
            AdminDataSource ads = null;
            StagingTable st = null;
            foreach (StagingTable sst in maf.stagingTableMod.getAllStagingTables())
            {
                if(sst.Name.ToLower().Equals(stagingTableName.ToLower()))
                {
                    // making sure the source file name also matches
                    string fname = sst.SourceFile;
                    int extindex = fname.LastIndexOf('.');
                    if (extindex >= 0)
                        fname = fname.Substring(0, extindex);
                    if (fname.ToLower().Equals(sourceFileName.ToLower()))
                    {
                        st = sst;
                        break;
                    }
                }
            }
            
            // just update the staging column
            // Rest all the details are populated as part of AdminRepository
            if (st != null)
            {
                ads = new AdminDataSource();
                ads.Name = st.Name;
                ads.Levels = st.Levels;
                ads.StagingColumns = st.Columns;
            }
            return ads;
        }

        [Serializable]
        public class SourceData
        {
            public StagingTable stagingTable;
            public SourceFile sourceFile;
            public string localConnForSource; //use visible name
            public bool imported;

            public SourceData() { }

            public SourceData(StagingTable st, SourceFile sf, string localConnName, bool importedSource)
            {
                stagingTable = st;
                sourceFile = sf;
                localConnForSource = localConnName;
                imported = importedSource;
            }
        }

        public static SourceData getSourceData(HttpSessionState session, string stagingTableName, string sourceFileName)
        {
            MainAdminForm maf = (MainAdminForm)session["MAF"];
            if (maf == null)
                return null;
            Space sp = (Space)session["space"];
            if (sp == null)
                return null;
            User u = (User)session["user"];
            if (u == null)
                return null;
            return getSourceData(maf, sp, u, stagingTableName, sourceFileName, session);
        }

        public static SourceData getSourceData(MainAdminForm maf, Space sp, User u, string stagingTableName, string sourceFileName, HttpSessionState session) {
            bool importedSource = false;
            StagingTable st = maf.stagingTableMod.findTable(stagingTableName);
            SourceFile sf = null;
            if (st != null)
            {
                if (!st.LiveAccess)
                {
                    List<SourceFile> sflist = maf.sourceFileMod.getSourceFiles();
                    foreach (SourceFile ssf in sflist)
                    {
                        string fname = ssf.FileName;
                        int extindex = fname.LastIndexOf('.');
                        if (extindex >= 0)
                            fname = fname.Substring(0, extindex);
                        if (fname.ToLower().Equals(sourceFileName.ToLower()))
                        {
                            sf = ssf;
                            break;
                        }
                    }
                }
                if (st.InheritTables != null)
                {
                    foreach (InheritTable it in st.InheritTables)
                    {
                        if (it.Prefix == null)
                            continue;
                        it.Prefix = it.Prefix.Replace(" ", "&nbsp;");
                    }
                }
            }
            else
            {
                // Get table info if imported
                List<ImportedRepositoryItem> ilist = Util.getImportedTables(maf, session, stagingTableName);
                {
                    if (ilist != null && ilist.Count > 0 && ilist[0].st != null)
                    {
                        st = ilist[0].st;
                        importedSource = true;
                        if (!st.LiveAccess)
                            sf = ilist[0].sf;
                    }
                }
            }

            string localConnNameForSource = null; //visible name 
            if (st != null)
            {
                Dictionary<string, LocalETLConfig> leConfigs = BirstConnectUtil.getLocalETLConfigsForSpace(sp);
                if (st.LoadGroups != null && leConfigs.Count > 0)
                {
                    foreach (string lg in st.LoadGroups)
                    {
                        if (lg.StartsWith(LiveAccessUtil.LIVE_ACCESS_LOAD_GROUP_PREFIX) && leConfigs.Keys.Contains(lg))
                        {
                            LocalETLConfig leconfig = leConfigs[lg];
                            localConnNameForSource = leconfig.getLocalETLConnection();
                            break;
                        }
                    }
                }
            }
            return new SourceData(st, sf, localConnNameForSource, importedSource);
        }

        private static void setScriptLoadGroupsIfNeeded(Space sp, MainAdminForm maf, User user, StagingTable modifiedSt, ScriptDefinition currentScript)
        {            
            if (modifiedSt != null && modifiedSt.Script != null)
            {                
                ScriptDefinition modifiedScript = modifiedSt.Script;
                if(modifiedScript.InputQuery != null && modifiedScript.InputQuery.Length > 0 
                    && modifiedScript.Script != null && modifiedScript.Script.Length > 0)
                {
                    bool updateLoadGroup = false;
                    if (currentScript == null || currentScript.Script == null || currentScript.InputQuery == null)
                    {
                        updateLoadGroup = true;
                    }
                    else if (modifiedScript.Script != currentScript.Script || modifiedScript.InputQuery != currentScript.InputQuery)
                    {
                        updateLoadGroup = true;
                    }
                    
                    if (updateLoadGroup)
                    {
                        // To ensure that it is not called on every save since call to SMIWeb can be little expensive
                        setLoadGroupsForScriptedSource(sp, maf, modifiedSt, user);
                    }
                }
            }
        }

        private static void setLoadGroupsForScriptedSource(Space sp, MainAdminForm maf, StagingTable st, User user)
        {
            try
            {
                if (st != null && st.Script != null && st.Script.InputQuery != null && st.Script.Script != null
                    && st.Script.InputQuery.Length > 0 && st.Script.Script.Length > 0 )
                {
                    ScriptDefinition script = st.Script;
                    SpaceConfig sc = Util.getSpaceConfiguration(sp.Type);
                    string localprotocol = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["LocalProtocol"];
                    TrustedService.TrustedService ts = new TrustedService.TrustedService();
                    // 5 Minute timeout
                    ts.Timeout = 5 * 60 * 1000;
                    ts.Url = localprotocol + sc.LocalURL + "/SMIWeb/services/TrustedService";
                    script.Output = ManageSources.getOutputString(st, maf);
                    string[] importedSpacesInfoArray = Util.getImportedPackageSpacesInfoArray(sp, maf);
                    TrustedService.ValidateResult vr = ts.validateScript((user == null ? null : user.Username), sp.ID.ToString(), sp.Directory, script.InputQuery, script.Output, script.Script, importedSpacesInfoArray);
                    if (vr.loadGroups != null && vr.loadGroups.Length > 0)
                    {
                        string[] lgs = vr.loadGroups.Split(',');
                        if (Array.IndexOf<string>(st.LoadGroups, lgs[0]) < 0)
                        {
                            st.LoadGroups = new string[] { lgs[0] };
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Error in setting load group " + ex.Message, ex);
            }
        }

        internal static void saveSourceDataAndHierarchies(HttpSessionState session, SourceData sourceData, Hierarchy[] hierarchiesThatChanged)
        {
            MainAdminForm maf = (MainAdminForm)session["MAF"];
            if (maf == null)
                return;
            Space sp = (Space)session["space"];
            if (sp == null)
                return;
            User u = (User)session["user"];
            if (u == null)
                return;

            saveSourceDataAndHierarchies(sourceData, hierarchiesThatChanged, sp, u, maf, session, Util.getSessionUser(session));
        }

        public static void saveSourceDataAndHierarchies(SourceData sourceData, Hierarchy[] hierarchiesThatChanged, Space sp, User u, MainAdminForm maf, HttpSessionState session, User sessionUser)
        {
            bool rebuild = false;
            bool didSecurityFiltersChange = false;
            if (sourceData.imported)
            {
                // make sure it is prevented not only from UI but from server also
                return;
            }
            if (!sourceData.stagingTable.LiveAccess)
                maf.sourceFileMod.setRepositoryDirectory(sp.Directory);
            maf.stagingTableMod.setRepositoryDirectory(sp.Directory);
            try
            {
                if (!sourceData.stagingTable.LiveAccess)
                    maf.sourceFileMod.isNewOrUpToDate(sourceData.sourceFile);
                maf.stagingTableMod.isNewOrUpToDate(sourceData.stagingTable);
                // current staging table
                StagingTable currentStagingTable = maf.stagingTableMod.findTable(sourceData.stagingTable.Name);
                if (currentStagingTable != null && currentStagingTable.DiscoveryTable && !sourceData.stagingTable.DiscoveryTable)
                {
                    String targettedVizSourceDimName = ApplicationBuilder.getDisplayName(currentStagingTable);
                    Hierarchy vizHierarchy = null;

                    if (hierarchiesThatChanged != null && hierarchiesThatChanged.Length > 0)
                    {
                        foreach (Hierarchy hierarchy in hierarchiesThatChanged)
                        {
                            // Never do anything with imported hiearrchy
                            if (hierarchy.Imported)
                            {
                                continue;
                            }
                            if (hierarchy.DimensionName == targettedVizSourceDimName && hierarchy.Name == targettedVizSourceDimName)
                            {
                                List<Level> levelList = new List<Level>();
                                hierarchy.getChildLevels(levelList);
                                if (hierarchy.AutoGenerated && levelList != null && levelList.Count == 1 && levelList[0].Name == targettedVizSourceDimName)
                                {
                                    vizHierarchy = hierarchy;
                                    vizHierarchy.AutoGenerated = false;
                                    break;
                                }
                            }
                        }
                    }

                    if (vizHierarchy == null)
                    {
                        // source changed from viz to non-viz
                        // flip the Autogenerated flag on hierarchy for viz source so that it doesn't get deleted while building                       
                        Hierarchy currentHierarchy =  maf.hmodule.getHierarchy(targettedVizSourceDimName);
                        if (currentHierarchy != null)
                        {
                            List<Level> currentLevelList = new List<Level>();
                            currentHierarchy.getChildLevels(currentLevelList);
                            if (currentHierarchy.AutoGenerated && currentLevelList != null && currentLevelList.Count == 1 && currentLevelList[0].Name == targettedVizSourceDimName)
                            {
                                vizHierarchy = currentHierarchy;
                                vizHierarchy.AutoGenerated = false;
                                if (hierarchiesThatChanged == null)
                                {
                                    hierarchiesThatChanged = new Hierarchy[] { vizHierarchy };
                                }
                                else
                                {
                                    List<Hierarchy> hierarchiesList = new List<Hierarchy>(hierarchiesThatChanged);
                                    hierarchiesList.Add(vizHierarchy);
                                    hierarchiesThatChanged = hierarchiesList.ToArray();
                                }
                            }
                        }
                    }
                }

                rebuild = isRebuildRequired(sp, currentStagingTable, sourceData); // also resets the load groups if required as well as time shifts
                didSecurityFiltersChange = securityFiltersChanged(currentStagingTable, sourceData);

                HierarchyList hierarchyList = null;
                if (hierarchiesThatChanged != null && hierarchiesThatChanged.Length > 0)
                {
                    hierarchyList = new HierarchyList(maf.getLogger());
                    hierarchyList.variableList = maf.hmodule.getHierarchies();
                    hierarchyList.updateRepositoryDirectory(sp.Directory, maf);

                    foreach (Hierarchy h in hierarchiesThatChanged)
                    {
                        if (h.Imported)
                        {
                            continue;
                        }
                        hierarchyList.isNewOrUpToDate(h, "Hierarchy", maf);
                    }
                }

                StagingTable st = sourceData.stagingTable;
                st.OverrideLevelKeys = cleanupOverrideLevelKeys(st.OverrideLevelKeys);
                updateCustomMeasures(maf, currentStagingTable, st.Levels, !st.Disabled);
                List<StagingColumn> columnsToRemove = new List<StagingColumn>();
                foreach (StagingColumn sc in st.Columns)
                {
                    if (sc.NaturalKey && sc.GenerateTimeDimension && sc.DataType.StartsWith("Date ID:") &&
                        sc.TargetTypes.Length == 1 && sc.TargetTypes[0] == maf.timeDefinition.Name)
                    {
                        string rootName = sc.Name.Substring(0, sc.Name.Length - ApplicationBuilder.DayIDName.Length - 1);
                        foreach (StagingColumn sc2 in st.Columns)
                        {
                            if ((sc2.DataType == "DateTime" || sc2.DataType == "Date") &&
                                (sc2.Name == rootName || st.Name.Substring(3) + " " + sc2.Name == rootName))
                            {
                                if (sc2.GenerateTimeDimension == false)
                                    columnsToRemove.Add(sc);
                                break;
                            }
                        }
                    }
                }

                if (columnsToRemove.Count > 0) {
                    List<StagingColumn> newColumns = new List<StagingColumn>(st.Columns);
                    List<SourceFile.SourceColumn> newSourceColumns = new List<SourceFile.SourceColumn>(sourceData.sourceFile.Columns);
                    foreach (StagingColumn sc in columnsToRemove)
                    {
                        newColumns.Remove(sc);
                        if (!st.LiveAccess)
                        {
                            foreach (SourceFile.SourceColumn sf in sourceData.sourceFile.Columns)
                            {
                                if (sf.Name == sc.Name)
                                {
                                    newSourceColumns.Remove(sf);
                                    break;
                                }
                            }
                        }
                    }

                    st.Columns = newColumns.ToArray();
                    if (!st.LiveAccess)
                        sourceData.sourceFile.Columns = newSourceColumns.ToArray();
                }
                if (!st.LiveAccess)
                    sourceData.sourceFile.lockThis(SpaceConfigurationFile<SourceFile>.getLockFileName(sp.Directory));

                if (hierarchyList != null)
                {
                    foreach (Hierarchy h in hierarchiesThatChanged)
                    {
                        if (h.Imported)
                        {
                            continue;
                        }
                        h.useLock(st);
                        hierarchyList.updateItemWithoutLock(h, u.Username, "Hieararchy", maf);
                    }
                }

                if (!st.LiveAccess)
                {
                    maf.sourceFileMod.updateSourceFileWithoutLock(sourceData.sourceFile, u.Username);
                    sourceData.sourceFile.useLock(st);
                }
                string liveAccessLoadGroup = null;
                if (sourceData.localConnForSource != null && sourceData.localConnForSource.Trim().Length > 0)
                {
                    Dictionary<string, LocalETLConfig> leConfigs = BirstConnectUtil.getLocalETLConfigsForSpace(sp);
                    if (leConfigs != null)
                    {
                        foreach (LocalETLConfig lconfig in leConfigs.Values)
                        {
                            if (lconfig.getLocalETLConnection().Equals(sourceData.localConnForSource))
                            {
                                liveAccessLoadGroup = lconfig.getLocalETLLoadGroup();
                                break;
                            }
                        }
                    }
                    if (st.PreviousLoadGroup == null)
                    {
                        st.PreviousLoadGroup = st.LoadGroups.Contains(SalesforceLoader.LOAD_GROUP_NAME) ? SalesforceLoader.LOAD_GROUP_NAME : Util.LOAD_GROUP_NAME;
                    }
                    string[] lgroups = new string[st.LoadGroups.Length];
                    for (int i = 0; i < st.LoadGroups.Length; i++)
                    {
                        if (st.LoadGroups[i].StartsWith(LiveAccessUtil.LIVE_ACCESS_LOAD_GROUP_PREFIX) || st.LoadGroups[i].Equals(st.PreviousLoadGroup))
                            lgroups[i] = liveAccessLoadGroup;
                        else
                            lgroups[i] = st.LoadGroups[i];
                    }
                    st.LoadGroups = lgroups;
                    Util.addLoadGroup(maf, liveAccessLoadGroup);
                    Util.addScriptGroup(maf, liveAccessLoadGroup);
                }
                else if (!st.UseBirstLocalForSource && st.PreviousLoadGroup != null)
                {
                    //reset previous loadgroup (ACORN/SFORCE) for source
                    string[] lgroups = new string[st.LoadGroups.Length];
                    for (int i = 0; i < st.LoadGroups.Length; i++)
                    {
                        if (st.LoadGroups[i].StartsWith(LiveAccessUtil.LIVE_ACCESS_LOAD_GROUP_PREFIX))
                            lgroups[i] = st.PreviousLoadGroup;
                        else
                            lgroups[i] = st.LoadGroups[i];
                    }
                    st.LoadGroups = lgroups;
                    st.PreviousLoadGroup = null;
                }
                else
                {
                    setScriptLoadGroupsIfNeeded(sp, maf, sessionUser, st, currentStagingTable.Script);
                }
                st.LiveAccess = sourceData.stagingTable.LiveAccess;
                st.Cardinality = sourceData.stagingTable.Cardinality;
                if (sourceData.stagingTable.LiveAccess)
                {
                    if (currentStagingTable.SourceFile != sourceData.stagingTable.SourceFile)
                    {
                        // Logical table name has been changed, so update targeting
                        foreach (StagingColumn sc in st.Columns)
                        {
                            if (sc.TargetTypes == null)
                                continue;
                            for (int i = 0; i < sc.TargetTypes.Length; i++)
                            {
                                if (sc.TargetTypes[i] == currentStagingTable.SourceFile)
                                    sc.TargetTypes[i] = sourceData.stagingTable.SourceFile;
                            }
                        }
                        st.HierarchyName = sourceData.stagingTable.SourceFile;
                        st.LevelName = sourceData.stagingTable.SourceFile;
                    }
                    if (sourceData.stagingTable.TableSource != null)
                    {
                        didSecurityFiltersChange = true;
                        st.TableSource = sourceData.stagingTable.TableSource;
                    }
                }

                if (st.InheritTables != null)
                {
                    foreach (InheritTable it in st.InheritTables)
                    {
                        it.Prefix = it.Prefix.Replace("&nbsp;", " ");
                    }
                }
                // this will save and relase the lock on the staging table
                Util.updateStagingTable(maf, st, null, u.Username, false);

                // get last saved version and set grain keys
                st = maf.stagingTableMod.get(st.Name);
                bool setDefaultTargets = false;
                if (maf.builderVersion >= 20)
                    setDefaultTargets = false;
                else if (maf.builderVersion < 11)
                    setDefaultTargets = true;
                else if (maf.builderVersion >= 11 && sp.LoadNumber == 0)
                    setDefaultTargets = true;
                st = setGrainKeys(maf, st, setDefaultTargets);
                
                if (session != null)
                {
                    session["AdminDirty"] = true;
                    if (rebuild || didSecurityFiltersChange)
                    {
                        session["RequiresRebuild"] = true;
                    }

                    if (rebuild)
                    {
                        maf.RequiresPublish |= !st.LiveAccess;
                        Util.updateRequiresPublishFlag(maf, maf.RequiresPublish);
                    }
                }
                if (st.RServerPath != null && st.RServerPath.Trim().Length == 0)
                    st.RServerPath = null;
                if (st.RExpression != null && st.RExpression.Trim().Length == 0)
                    st.RExpression = null;
            }
            catch (Exception e)
            {
                string message = e.Message;
                if (message == null || message.Length == 0)
                    message = "Error saving source data";
                throw new SoapException(message, SoapException.ServerFaultCode, e);
            }
            finally
            {
                // now release lock on source data
                if (!sourceData.stagingTable.LiveAccess)
                    sourceData.sourceFile.unlockThis();
                sourceData.stagingTable.unlockThis();
            }
        }

        private static bool securityFiltersChanged(StagingTable currentStagingTable, SourceData sourceData)
        {   
            if (currentStagingTable != null && sourceData.stagingTable != null)
            {
                StagingColumn[] currentStagingColumns = currentStagingTable.Columns;
                StagingColumn[] suppliedStagingColumns = sourceData.stagingTable.Columns;
                if (currentStagingColumns != null && currentStagingColumns.Length > 0
                    && suppliedStagingColumns != null && suppliedStagingColumns.Length > 0)
                {
                    foreach (StagingColumn sc in suppliedStagingColumns)
                    {
                        StagingColumn ssc = null;
                        foreach (StagingColumn foundsc in currentStagingColumns)
                        {
                            if (foundsc.Name == sc.Name)
                            {
                                ssc = foundsc;
                                break;
                            }
                        }

                        if (ssc != null)
                        {
                            if (sc.SecFilter != null && sc.SecFilter.SessionVariable == null)
                            {
                                sc.SecFilter = null;
                            }
                            if ((ssc.SecFilter != null && !(ssc.SecFilter.Equals(sc.SecFilter)))
                                || (ssc.SecFilter == null && sc.SecFilter != null))
                            {
                                return true;
                            }     
                        }
                    }
                }
            }
            return false;
        }
                
        private static bool isRebuildRequired(Space sp, StagingTable currentStagingTable, SourceData sourceData)
        {
            bool rebuild = false;
            if (currentStagingTable == null && sourceData.stagingTable != null && sourceData.stagingTable.Script != null && sourceData.stagingTable.Script.InputQuery != null)
            {
                return true;
            }

            if (sourceData.stagingTable.LiveAccess)
                return true;

            string customTimeColumn = null;
            if (sourceData.stagingTable != null)
            {
                customTimeColumn = sourceData.stagingTable.CustomTimeColumn;
                if (customTimeColumn != null && customTimeColumn.Length > 0)
                {
                    rebuild = rebuild || currentStagingTable.CustomTimeColumn == null || currentStagingTable.CustomTimeColumn != customTimeColumn;
                    CustomTimeShift[] timeShifts = sourceData.stagingTable.CustomTimeShifts;
                    if (!rebuild && timeShifts != null)
                    {
                        if (currentStagingTable.CustomTimeShifts == null)
                            rebuild = true;
                        else if (timeShifts.Length != currentStagingTable.CustomTimeShifts.Length)
                            rebuild = true;
                        else
                        {
                            for (int i = 0; i < currentStagingTable.CustomTimeShifts.Length; i++)
                            {
                                if (currentStagingTable.CustomTimeShifts[i].BaseKey != timeShifts[i].BaseKey ||
                                    currentStagingTable.CustomTimeShifts[i].RelativeKey != timeShifts[i].RelativeKey ||
                                    !Performance_Optimizer_Administration.Repository.EqualsArray(currentStagingTable.CustomTimeShifts[i].Columns, timeShifts[i].Columns))
                                {
                                    rebuild = true;
                                    break;
                                }
                            }
                        }
                    }
                    if (timeShifts != null)
                    {
                        foreach (CustomTimeShift cts in timeShifts)
                        {
                            List<string> colNames = getDependentColumns(sp, sourceData.stagingTable, sourceData.sourceFile, cts.BaseKey);
                            if (colNames != null)
                                cts.Columns = colNames.ToArray();
                            else
                                cts.Columns = null;
                        }
                    }
                }
                else
                {
                    if (currentStagingTable != null && currentStagingTable.CustomTimeColumn != null && currentStagingTable.CustomTimeColumn.Length > 0)
                        rebuild = true;
                }
            }
            return rebuild;
        }

        public static StagingTable getStagingTable(HttpSessionState session, string stagingTableName, string sourceFileName)
        {
            MainAdminForm maf = (MainAdminForm)session["MAF"];
            if (maf == null)
                return null;
            Space sp = (Space)session["space"];
            if (sp == null)
                return null;

            StagingTable st = null;
            foreach (StagingTable sst in maf.stagingTableMod.getAllStagingTables())
            {
                if (sst.Name.ToLower().Equals(stagingTableName.ToLower()))
                {
                    // making sure the source file name also matches
                    string fname = sst.SourceFile;
                    int extindex = fname.LastIndexOf('.');
                    if (extindex >= 0)
                        fname = fname.Substring(0, extindex);
                    if (fname.ToLower().Equals(sourceFileName.ToLower()))
                    {
                        st = sst;
                        break;
                    }
                }
            }
            return st;
        }
        private static Publishability checkPublishability(MainAdminForm maf, Space sp, StagingTable st)
        {
            Publishability pub = new Publishability();
            if (st.Disabled)
                return (pub);
            /*
             * Allowing untargeted tables is OK now with ETL services
            if (st.Levels == null || st.Levels.Length <= 1)
            {
                pub.publishable = false;
                pub.message = "No grain has been defined";
                return (pub);
            }
             */
            int countCols = 0;
            int countKeys = 0;
            pub.publishable = true;
            foreach (StagingColumn sc in st.Columns)
            {
                if (sc.NaturalKey)
                    countKeys++;
                if (sc.TargetTypes != null && sc.TargetTypes.Length > 0)
                    countCols++;
            }
            /*
            if (countKeys == 0)
            {
                pub.publishable = false;
                pub.message = "No keys have been defined";
                return (pub);
            }
             */
            if (countCols == 0)
            {
                pub.publishable = false;
                pub.message = "No columns have been defined";
                return pub;
            }
            SourceFile sf = getSourceFile(maf, st);

            bool exists = (sf != null);
            if (exists)
            {
                if (sf.S3Bucket != null)
                    exists = AWSUtils.s3ObjectExists(new BasicAWSCredentials(awsAccessKeyId, awsSecretKey), sf);
                else
                    exists = File.Exists(Path.Combine(sp.Directory, "data", sf.FileName));
            }
            if (!exists && (st.Script == null || st.Script.InputQuery == null))
            {
                pub.message = "Source not present";
                if (st.WebOptional)
                    pub.warning = true;
                else
                    pub.publishable = false;
            }
            return pub;
        }

        private static SourceFile getSourceFile(MainAdminForm maf, StagingTable st)
        {
            SourceFile sf = null;
            foreach (SourceFile ssf in maf.sourceFileMod.getSourceFiles())
            {
                if (st.SourceFile.ToLower() == ssf.FileName.ToLower())
                {
                    sf = ssf;
                    break;
                }
            }
            return (sf);
        }

        public static void saveStagingColumn(HttpSessionState session, StagingTable st, string tableName, StagingColumn sc)
        {
            MainAdminForm maf = (MainAdminForm)session["MAF"];
            if (maf == null)
                return;

            saveStagingColumn(session, st, tableName, sc, maf);
        }

        public static void saveStagingColumn(HttpSessionState session, StagingTable st, string tableName, StagingColumn sc, MainAdminForm maf)
        {
            if (st == null)
                st = maf.stagingTableMod.findTable(tableName);
            bool found = false;
            StagingColumn ssc = null;
            if (st != null)
            {
                foreach (StagingColumn foundsc in st.Columns)
                {
                    if (foundsc.Name == sc.Name)
                    {
                        ssc = foundsc;
                        found = true;
                        break;
                    }
                }
            }
            SourceFile.SourceColumn sfc = null;

            if (!found)
            {
                ssc = new StagingColumn();
                ssc.Name = sc.Name;
                List<StagingColumn> newList = null;
                if (st == null || st.Columns == null)
                    newList = new List<StagingColumn>();
                else
                    newList = new List<StagingColumn>(st.Columns);
                newList.Add(ssc);
                st.Columns = newList.ToArray();
                if ((sc.Name != ApplicationBuilder.DayIDName) &&
                    !sc.DataType.StartsWith("Date ID:"))
                {
                    sfc = new SourceFile.SourceColumn();
                    sfc.Name = sc.SourceFileColumn == null ? sc.Name : sc.SourceFileColumn;
                    ssc.SourceFileColumn = sfc.Name;
                    SourceFile sf = null;
                    if (st != null)
                        sf = maf.sourceFileMod.getSourceFile(st.SourceFile);
                    if (sf != null)
                    {
                        List<SourceFile.SourceColumn> newsfList = new List<SourceFile.SourceColumn>(sf.Columns);
                        newsfList.Add(sfc);
                        sf.Columns = newsfList.ToArray();
                    }
                }
            }
            else if (!st.LiveAccess && !ssc.DataType.StartsWith("Date ID:")) //do not try to find sourcefilecolumn for autogenerated stagingcolumns
            {
                SourceFile sf = maf.sourceFileMod.getSourceFile(st.SourceFile);
                foreach (SourceFile.SourceColumn ssfc in sf.Columns)
                {
                    if (ssfc.Name == ssc.SourceFileColumn)
                    {
                        sfc = ssfc;
                        break;
                    }
                }
            }
            if (ssc != null)
            {
                ssc.DataType = sc.DataType;
                ssc.ExcludeFromChecksum = sc.ExcludeFromChecksum;
                ssc.GenerateTimeDimension = sc.GenerateTimeDimension;
                ssc.Index = sc.Index;
                ssc.NaturalKey = sc.NaturalKey;
                ssc.TargetAggregations = sc.TargetAggregations;
                List<string> ttypes = new List<string>();
                if (sc.TargetTypes != null)
                {
                    foreach (string s in sc.TargetTypes)
                        if (s != null)
                            ttypes.Add(s);
                }                
                ssc.TargetTypes = ttypes.ToArray();
                if (sc.Transformations != null)
                    ssc.Transformations = sc.Transformations;
                ssc.UnknownValue = (sc.UnknownValue != null && sc.UnknownValue.Length > 0) ? sc.UnknownValue : null;
                ssc.Width = sc.Width;
                if (sc.SecFilter != null && sc.SecFilter.SessionVariable == null)
                {
                    sc.SecFilter = null;
                }
                if(((ssc.SecFilter != null  && !(ssc.SecFilter.Equals(sc.SecFilter)))  
                    || ssc.SecFilter == null && sc.SecFilter != null) && session != null)
                {
                    session["AdminDirty"] = true;
                    session["RequiresRebuild"] = true;                 
                }                
                ssc.SecFilter = sc.SecFilter;
                ssc.AnalyzeMeasure = sc.AnalyzeMeasure;
                if (sfc != null)
                {
                    sfc.Name = sc.SourceFileColumn;
                    sfc.DataType = sc.DataType;
                    sfc.Width = sc.Width;
                }
                if ((sc.Name != ApplicationBuilder.DayIDName) &&
                    !sc.DataType.StartsWith("Date ID:"))
	                st = setKey(session, maf, st, ssc);

                // Prune any added date ID columns that may have been deleted
                List<StagingColumn> newList = new List<StagingColumn>();
                bool pruned = false;
                foreach (StagingColumn dsc in st.Columns)
                {
                    found = false;
                    // Find columns that were automatically added
                    if (dsc.NaturalKey && dsc.GenerateTimeDimension && dsc.DataType == "Date ID: Day" &&
                        dsc.TargetTypes.Length == 1 && dsc.TargetTypes[0] == maf.timeDefinition.Name)
                    {
                        // Now find if they should still be there
                        string rootName = dsc.Name.Substring(0, dsc.Name.Length - ApplicationBuilder.DayIDName.Length - 1);
                        foreach (StagingColumn sc2 in st.Columns)
                        {
                            if (/* sc2.GenerateTimeDimension && */ (sc2.DataType == "DateTime" || sc2.DataType == "Date") && (sc2.Name == rootName || st.Name.Substring(3) + " " + sc2.Name == rootName))
                            {
                                found = true;
                                break;
                            }
                        }
                    }
                    if (!found)
                        newList.Add(dsc);
                    else
                        pruned = true;
                }
                if (pruned)
                    st.Columns = newList.ToArray();
                setOutputString(st, maf);
                if (session != null)
                    session["AdminDirty"] = true;
                maf.stagingTableMod.setupNodes(maf.stagingTableMod.rootNode);
                maf.sourceFileMod.setupNodes(maf.sourceFileMod.rootNode);
            }
        }

        public static void removeStagingColumn(HttpSessionState session, string tableName, string name)
        {
            MainAdminForm maf = (MainAdminForm)session["MAF"];
            if (maf == null)
                return;
            StagingTable st = maf.stagingTableMod.findTable(tableName);
            StagingColumn ssc = null;
            foreach (StagingColumn foundsc in st.Columns)
            {
                if (foundsc.Name == name)
                {
                    ssc = foundsc;
                    break;
                }
            }

            if (ssc != null)
            {
                bool script = st.Script != null && st.Script.InputQuery != null && st.Script.InputQuery.Length > 0;
                if (!script)
                {
                    Space sp = (Space)session["space"];
                    if (sp == null)
                        return;
                    FileInfo fi = new FileInfo(sp.Directory + "\\data\\" + st.SourceFile);
                    if (fi.Exists)
                    {
                        // If changing the structure, remove the source file
                        fi.Delete();
                    }
                }
                // Remove source file column as well
                SourceFile sf = maf.sourceFileMod.getSourceFile(st.SourceFile);
                if (sf != null)
                {
                    SourceFile.SourceColumn sfc = null;
                    int i = 0;
                    foreach (SourceFile.SourceColumn ssfc in sf.Columns)
                    {
                        if (ssfc.Name == ssc.SourceFileColumn)
                        {
                            sfc = ssfc;
                            break;
                        }
                        i++;
                    }
                    if (sfc != null)
                    {
                        List<SourceFile.SourceColumn> sfcols = new List<SourceFile.SourceColumn>(sf.Columns);
                        sfcols.RemoveAt(i);
                        sf.Columns = sfcols.ToArray();
                        if (sf.ImportColumns != null)
                        {
                            List<SourceFile.SourceColumn> sfimpcols = new List<SourceFile.SourceColumn>(sf.ImportColumns);
                            sfimpcols.RemoveAt(i);
                            sf.ImportColumns = sfimpcols.ToArray();
                        }
                    }
                }
                // Remove any dependent columns as well
                List<StagingColumn> sclist = new List<StagingColumn>();                 
                foreach (StagingColumn sc in st.Columns)
                {
                    if (sc == ssc)
                        continue;
                    if ((ssc.DataType == "Date" || ssc.DataType == "DateTime") && sc.Name.Equals(ssc.Name + " Day ID"))
                    {
                        continue;
                    }
                    sclist.Add(sc);
                }
                st.Columns = sclist.ToArray();
                session["AdminDirty"] = true;
            }
        }

        private static StagingTable setKey(HttpSessionState session, MainAdminForm maf, StagingTable st, StagingColumn sc)
        {
            sc.NaturalKey = false;
            if (st.Levels != null)
            {
                foreach (string[] level in st.Levels)
                {
                    if (maf.timeDefinition != null && level[0] == maf.timeDefinition.Name)
                        continue;
                    //if (maf.getDimensionList().Contains(level[0]))
                    if(maf.hmodule != null)
                    {
                        Hierarchy h = maf.hmodule.getDimensionHierarchy(level[0]);
                        if (h != null)
                        {
                            if (h.findLevelWithKeyColumn(sc.Name) != null)
                            {
                                sc.NaturalKey = true;
                                if (session != null)
                                    session["AdminDirty"] = true;
                                maf.stagingTableMod.save(st);
                                break;
                            }
                        }
                    }
                }
            }
            return maf.stagingTableMod.get(st.Name);
        }

        public static void renameStagingColumn(HttpSessionState session, string tableName, string oldName, string newName)
        {
            MainAdminForm maf = (MainAdminForm)session["MAF"];
            if (maf == null)
                return;
            StagingTable st = maf.stagingTableMod.findTable(tableName);
            foreach (StagingColumn foundsc in st.Columns)
            {
                if (foundsc.Name == oldName)
                {
                    foundsc.Name = newName;
                    if (oldName == foundsc.SourceFileColumn && !st.LiveAccess)
                    {
                        SourceFile sf = maf.sourceFileMod.getSourceFile(st.SourceFile);
                        if (sf != null)
                        {
                            foreach (SourceFile.SourceColumn sfc in sf.Columns)
                            {
                                if (sfc.Name == foundsc.SourceFileColumn)
                                {
                                    sfc.Name = newName;
                                    foundsc.SourceFileColumn = newName;
                                    break;
                                }
                            }
                        }
                        foreach(StagingColumn ssc in st.Columns)
                        {
                            if (ssc == foundsc)
                                continue;
                            if (ssc.SourceFileColumn == oldName)
                                ssc.SourceFileColumn = newName;
                        }
                    }
                    setKey(session, maf, st, foundsc);
                    session["AdminDirty"] = true;
                    break;
                }
            }
        }

        private static Hierarchy getHierarchy(MainAdminForm maf, string hname)
        {
            return maf.hmodule.getLatestHierarchy(hname);
        }

        public static bool saveHierarchy(HttpSessionState session, Hierarchy h, string oldName)
        {
            MainAdminForm maf = (MainAdminForm)session["MAF"];
            if (maf == null)
                return false;
            if (oldName != null && oldName.Length == 0)
                oldName = null;
            User u = (User)session["user"];
            if (h != null && h.Imported)
            {
                // reject any changes to imported hierarchy
                // takes care of any client UI issue
                return true;
            }
            Hierarchy curh = getHierarchy(maf, oldName != null ? oldName : h.Name);
            if (curh != null)
            {
                if (curh.Guid != null && curh.Guid != Guid.Empty && h.Guid != null && h.Guid != Guid.Empty)
                {
                    maf.hmodule.replaceHierarchy(h);
                }
                else
                {
                    // if there are no guids, remove the old hierarchy and add the new one.  The new one will now have a guid.
                    maf.hmodule.replaceHierarchy(curh, h);
                }
                h.Hidden = curh.Hidden;
                h.AutoGenerated = false;
                h.ExclusionFilter = curh.ExclusionFilter;
                h.GeneratedTime = curh.GeneratedTime;
                h.NoMeasureFilter = curh.NoMeasureFilter;
                h.ProcessedDependencies = curh.ProcessedDependencies;
                if (h.SourceGroups == null)
                    h.SourceGroups = curh.SourceGroups;
                List<Level> llist = new List<Level>();
                h.getChildLevels(llist);
                foreach (Level l in llist)
                {
                    Level curl = curh.findLevel(l.Name);
                    if (curl != null)
                    {
                        l.Cardinality = curl.Cardinality;
                        l.Degenerate = curl.Degenerate;
                        l.GenerateDimensionTable = curl.GenerateDimensionTable;
                        l.GenerateInheritedCurrentDimTable = curl.GenerateInheritedCurrentDimTable;
                        l.Locked = curl.Locked;
                        l.SCDType = curl.SCDType;
                    }
                    if (l.Children.Length == 0)
                        h.DimensionKeyLevel = l.Name;
                }
                if (oldName != null)
                {
                    /*
                        * Update grains
                        */
                    foreach (StagingTable st in maf.stagingTableMod.getAllStagingTables())
                    {

                        if (st.HierarchyName == oldName)
                        {
                            st.HierarchyName = h.Name;
                        }
                        foreach (string[] level in st.Levels)
                        {
                            if (level[0] == oldName)
                                level[0] = h.DimensionName;
                        }
                        foreach (StagingColumn sc in st.Columns)
                        {
                            if (sc.TargetTypes != null)
                                for (int i = 0; i < sc.TargetTypes.Length; i++)
                                    if (sc.TargetTypes[i] == oldName)
                                        sc.TargetTypes[i] = h.DimensionName;
                        }
                    }
                }
                // Remove references to any old levels that may have been deleted
                List<string> lnames = new List<string>();
                h.getChildLevelNames(lnames);
                foreach (StagingTable st in maf.stagingTableMod.getAllStagingTables())
                {
                    List<string[]> newlist = new List<string[]>();
                    foreach (string[] level in st.Levels)
                    {
                        bool valid = true;
                        if (level[0] == h.DimensionName && !lnames.Contains(level[1]))
                            valid = false;
                        if (valid)
                            newlist.Add(level);
                    }
                    if (st.Levels.Length != newlist.Count)
                    {
                        st.Levels = newlist.ToArray();
                        maf.stagingTableMod.updateStagingTable(st, u == null ? "" : u.Username);
                    }
                }
                session["AdminDirty"] = true;
            }
            else
            {
                maf.hmodule.addHierarchy(h);
                session["AdminDirty"] = true;
            }
            return oldName != null && oldName != h.Name;
        }

        private static bool validateLevels(MainAdminForm maf, string[][] levels)
        {
            foreach (StagingTable st in maf.stagingTableMod.getStagingTables())
            {
                if (st.SourceGroups != null && st.matchesLevels(levels))
                    return false;
            }
            return true;
        }

        public static void removeHierarchy(HttpSessionState session, string hname)
        {
            MainAdminForm maf = (MainAdminForm)session["MAF"];
            if (maf == null)
                return;
            Hierarchy curh = getHierarchy(maf, hname);
            if (curh != null && !curh.Imported)
            {
                if (VALIDATE_GRAIN_MERGE)
                {
                    // Make sure we can delete the hierarchy cleanly - if not ignore
                    foreach (StagingTable st in maf.stagingTableMod.getAllStagingTables())
                    {
                        bool valid = true;
                        foreach (string[] level in st.Levels)
                        {
                            if (level[0] == curh.Name)
                            {
                                string[][] newlevels = new string[st.Levels.Length - 1][];
                                int count = 0;
                                foreach (string[] level2 in st.Levels)
                                {
                                    if (level2[0] != curh.Name)
                                        newlevels[count++] = new string[2] { level2[0], level2[1] };
                                }
                                if (!validateLevels(maf, newlevels))
                                {
                                    valid = false;
                                }
                                break;
                            }
                        }
                        if (st.HierarchyName == curh.Name)
                        {
                            st.HierarchyName = null;
                            st.LevelName = null;
                        }
                        if (!valid)
                            return;
                    }
                }
                /*
                 * Update grains
                 */
                foreach (StagingTable st in maf.stagingTableMod.getAllStagingTables())
                {
                    foreach (string[] level in st.Levels)
                    {
                        if (level[0] == curh.DimensionName)
                        {
                            string[][] newlevels = new string[st.Levels.Length - 1][];
                            int count = 0;
                            foreach (string[] level2 in st.Levels)
                            {
                                if (level2[0] != curh.DimensionName)
                                    newlevels[count++] = new string[2] { level2[0], level2[1] };
                            }
                            st.Levels = newlevels;
                            break;
                        }
                    }
                    foreach (StagingColumn sc in st.Columns)
                    {
                        if (sc.TargetTypes != null)
                            for (int i = 0; i < sc.TargetTypes.Length; i++)
                                if (sc.TargetTypes[i] == curh.DimensionName)
                                {
                                    int count = 0;
                                    string[] ttypes = new string[sc.TargetTypes.Length - 1];
                                    for (int j = 0; j < sc.TargetTypes.Length; j++)
                                        if (sc.TargetTypes[j] != curh.DimensionName)
                                            ttypes[count++] = sc.TargetTypes[j];
                                    sc.TargetTypes = ttypes;
                                    break;
                                }
                    }
                }
                // Delete the dimension
                maf.hmodule.removeHierarchy(curh);
                maf.deleteDimension(curh.DimensionName);
                session["AdminDirty"] = true;
            }
        }

        public static bool saveLevel(HttpSessionState session, string hierarchyName, Level l, string oldLevelName, bool rebuildApplication)
        {
            MainAdminForm maf = (MainAdminForm)session["MAF"];
            if (maf == null)
                return false;
            Hierarchy curh = getHierarchy(maf, hierarchyName);
            if (oldLevelName != null && oldLevelName.Length == 0)
                oldLevelName = null;
            if (curh != null)
            {
                Level curl = curh.findLevel(oldLevelName != null ? oldLevelName : l.Name);
                if (curl != null)
                {
                    curl.Name = l.Name;
                    curl.Cardinality = l.Cardinality;
                    curl.ColumnNames = l.ColumnNames;
                    curl.HiddenColumns = l.HiddenColumns;
                    curl.Keys = l.Keys;
                    /*
                     * If SCDType is toggled, we should not copy the changes to repository.xml otherwise the newly added SCD Type II columns
                     * will start appearing in subject area and dragging them to report will fail.
                     */ 
                    if (curl.SCDType != l.SCDType)
                    {
                        Util.updateRequiresPublishFlag(maf, true);
                    }
                    curl.SCDType = l.SCDType;
                    curl.GenerateInheritedCurrentDimTable = l.GenerateInheritedCurrentDimTable;
                    curl.ForeignKeys = l.ForeignKeys;
                    /*
                     * Need (in the future) to verify that the primary keys in all foreign key sources are actually in this dimension. If they are not
                     * these foreign key references will need to be cleaned. This is because in DataFlow we don't have any targeting information. We can only
                     * see if a column is in a source that has this dimension in its grain. If the column is untargeted or targeted to a different dimension, then this
                     * foriegn key reference is invalid. (Ideally DataFlow would prevent this, but for performance we don't load targeting info)
                     */
                    if (l.Aliases != null && l.Aliases.Length == 0)
                        l.Aliases = null;
                    curl.Aliases = l.Aliases;
                    /*
                     * Make sure columns are only in one level at a time
                     */
                    List<Level> llist = new List<Level>();
                    curh.getChildLevels(llist);
                    foreach (string cname in l.ColumnNames)
                    {
                        foreach (Level sl in llist)
                        {
                            if (sl == curl)
                                continue;
                            int index = Array.IndexOf<string>(sl.ColumnNames, cname);
                            if (index >= 0)
                            {
                                List<string> newlist = new List<string>(sl.ColumnNames);
                                List<bool> newhidden = new List<bool>(sl.HiddenColumns);
                                newlist.RemoveAt(index);
                                newhidden.RemoveAt(index);
                                sl.ColumnNames = newlist.ToArray();
                                sl.HiddenColumns = newhidden.ToArray();
                            }
                        }
                    }
                    session["AdminDirty"] = true;
                    session["RequiresRebuild"] = true;
                }

                // Update dimension key level if necessary
                if (oldLevelName != null && curh.DimensionKeyLevel == oldLevelName)
                    curh.DimensionKeyLevel = l.Name;

                // save now - setGrainKeys may also modify hierarchy and try to save
                maf.hmodule.updateHierarchy(curh);

                // Update grains
                foreach (StagingTable st in maf.stagingTableMod.getAllStagingTables())
                {
                    if (oldLevelName != null && oldLevelName.Trim().Length > 0 &&
                        st.HierarchyName == curh.Name && st.LevelName == oldLevelName)
                    {
                        st.LevelName = l.Name;
                    }
                    bool setgkeys = false; ;
                    foreach (string[] level in st.Levels)
                    {
                        if (level[0] == curh.DimensionName && level[1] == (oldLevelName != null ? oldLevelName : l.Name))
                        {
                            level[1] = l.Name;
                            setgkeys = true;
                        }
                    }
                    if (setgkeys)
                        setGrainKeys(maf, st);
                }
            }
            if (rebuildApplication)
            {
                session["RequiresRebuild"] = true;
            }

            return oldLevelName != null && oldLevelName != l.Name;
        }

        private static StagingTable setGrainKeys(MainAdminForm maf, StagingTable st)
        {
            return setGrainKeys(maf, st, true);
        }
        
        /*
         * Set all natural keys in a staging table based on its grain. Go through each level and
         * make sure all columns that are natural keys in the grain levels or any parent levels 
         * are set.
         */
        private static StagingTable setGrainKeys(MainAdminForm maf, StagingTable st, bool setDefaultTargets)
        {
            foreach (StagingColumn sc in st.Columns)
            {
                if (sc.DataType.StartsWith("Date ID:"))
                    continue;
                if (sc.TargetTypes == null)
                    continue;
                if (maf.timeDefinition != null && Array.IndexOf<string>(sc.TargetTypes, maf.timeDefinition.Name) >= 0)
                    continue;
                if (setDefaultTargets)
                    sc.NaturalKey = false;
            }
            foreach (string[] level in st.Levels)
            {
                if (maf.timeDefinition != null && maf.timeDefinition.Name == level[0])
                    continue;
                Hierarchy h = maf.hmodule.getDimensionHierarchy(level[0]);
                if (h != null)
                {
                    Level l = h.findLevel(level[1]);
                    if (l != null)
                    {
                        l.GenerateDimensionTable = true;
                        maf.hmodule.updateHierarchy(h);
                        List<Level> ancestors = h.getParents(l.Name);
                        ancestors.Add(l);
                        foreach (Level al in ancestors)
                        {
                            foreach (LevelKey lk in al.Keys)
                            {
                                if (lk.SurrogateKey)
                                    continue;
                                foreach (string s in lk.ColumnNames)
                                {
                                    foreach (StagingColumn sc in st.Columns)
                                    {
                                        if (sc.Name == s)
                                        {
                                            if (setDefaultTargets)
                                            {
                                                // Set the target dimension if necessary
                                                if (sc.TargetTypes == null || Array.IndexOf<string>(sc.TargetTypes, h.DimensionName) < 0)
                                                {
                                                    if (sc.TargetTypes == null || sc.TargetTypes.Length == 0)
                                                    {
                                                        sc.TargetTypes = new string[] { h.DimensionName };
                                                    }
                                                    else if (sc.TargetTypes.Length == 1 && sc.TargetTypes[0] == "Measure")
                                                    {
                                                        sc.TargetTypes = new string[] { "Measure", h.DimensionName };
                                                    }
                                                    else
                                                    {
                                                        /*
                                                         * If this column is targeted to a dimension not in the 
                                                         * grain, then reassign
                                                         */
                                                        bool found = false;
                                                        foreach (string[] sl in st.Levels)
                                                        {
                                                            int pos = Array.IndexOf<string>(sc.TargetTypes, sl[0]);
                                                            if (pos >= 0)
                                                            {
                                                                found = true;
                                                                /*
                                                                 * If it is targeted to a different dimension in the grain than than this one and it is a level key, then we must retarget
                                                                 */
                                                                if (sc.TargetTypes[pos] != h.DimensionName && !h.ProcessedDependencies && h.ModelWizard)
                                                                {
                                                                    sc.TargetTypes[pos] = h.DimensionName;
                                                                }
                                                                break;
                                                            }
                                                        }
                                                        if (!found)
                                                        {
                                                            if (sc.TargetTypes.Length >= 2)
                                                                sc.TargetTypes = new string[] { "Measure", h.DimensionName };
                                                            else
                                                                sc.TargetTypes = new string[] { h.DimensionName };
                                                        }
                                                    }
                                                }
                                                sc.NaturalKey = true;
                                                break;
                                            }
                                            else if ((sc.TargetTypes != null) && (Array.IndexOf<string>(sc.TargetTypes, h.DimensionName) >= 0))
                                            {
                                                sc.NaturalKey = true;
                                                break;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            maf.stagingTableMod.save(st);
            return maf.stagingTableMod.get(st.Name);
        }

        public static void saveDataSource(HttpSessionState session, string name, string displayName, string[][] levels, bool enabled, bool transactional, bool truncateonload, bool lockformat, bool schemalock,
            bool AllowAddColumns, bool AllowNullBindingRemovedColumns, bool AllowUpcast, bool AllowVarcharExpansion, bool AllowValueTruncationOnLoad, bool FailUploadOnVarcharExpansion, ScriptDefinition script, 
            string[] loadGroups, string sourceGroups, string customTimeColumn, string customTimePrefix, CustomTimeShift[] timeShifts, string[] subGroups, SnapshotPolicy snapshots, string newColumnTarget,
            string dataSourceTimeZone, string[] sourceKey, string foreignKeys, string factForeignKeys, string parentForeignKeySource, string hierarchyName, string levelName, bool excludeFromModel, bool[] SFColPreventUpdate,
            bool customUpload, bool incrementalSnapshotFact, string[] snapshotDeleteKeys, OverrideLevelKey[] OverrideLevelKeys, bool onlyQuoteAfterSeparator, bool useBirstLocalForSource, string localConnForSource,
            SourceLocation[] locations, string encoding, string separator, bool replaceWithNull, bool skipRecord, string[] SFColFormats, bool discoveryTable, bool liveAccess, string tableSource, string[] inheritTables,
            bool unCached, string rServerPath, string rExpression, string rSettings)
        {
            saveDataSourceAndColumns(session, name, displayName, levels, enabled, transactional, truncateonload, lockformat, schemalock, AllowAddColumns, AllowNullBindingRemovedColumns, AllowUpcast, AllowVarcharExpansion, 
            AllowValueTruncationOnLoad, FailUploadOnVarcharExpansion, script, loadGroups, sourceGroups, customTimeColumn, customTimePrefix, timeShifts, subGroups, null, snapshots, newColumnTarget, dataSourceTimeZone, sourceKey,
                foreignKeys, factForeignKeys, parentForeignKeySource, hierarchyName, levelName, excludeFromModel, SFColPreventUpdate, customUpload, incrementalSnapshotFact, snapshotDeleteKeys, OverrideLevelKeys, onlyQuoteAfterSeparator,
                useBirstLocalForSource, localConnForSource, locations, encoding, separator, replaceWithNull, skipRecord, SFColFormats, discoveryTable, liveAccess, tableSource, inheritTables, unCached, rServerPath, rExpression, rSettings);
        }

        public static void saveDataSourceAndColumns(HttpSessionState session, string name, string displayName, string[][] levels, bool enabled, bool transactional, bool truncateonload, bool lockformat, bool schemalock,
            bool AllowAddColumns, bool AllowNullBindingRemovedColumns, bool AllowUpcast, bool AllowVarcharExpansion, bool AllowValueTruncationOnLoad, bool FailUploadOnVarcharExpansion, ScriptDefinition script, string[] loadGroups, string sourceGroups,
            string customTimeColumn, string customTimePrefix, CustomTimeShift[] timeShifts, string[] subGroups, StagingColumn[] colList, SnapshotPolicy snapshots, string newColumnTarget, string datasourceTimeZone,
            string[] sourceKey, string foreignKeys, string factForeignKeys, string parentForeignKeySource, string hierarchyName, string levelName, bool excludeFromModel, bool[] SFColPreventUpdate, bool customUpload,
            bool incrementalSnapshotFact, string[] snapshotDeleteKeys, OverrideLevelKey[] OverrideLevelKeys, bool onlyQuoteAfterSeparator, bool useBirstLocalForSource, string localConnForSource,
            SourceLocation[] locations, string encoding, string separator, bool replaceWithNull, bool skipRecord, string[] SFColFormats, bool discoveryTable, bool liveAccess, string tableSource, string[] inheritTables,
            bool unCached, string rServerPath, string rExpression, string rSettings)
        {
            MainAdminForm maf = (MainAdminForm)session["MAF"];
            if (maf == null)
                return;
            User u = (User)session["user"];
            if (u == null)
                return;
            Space sp = Util.getSessionSpace(session);

            saveDataSourceAndColumns(session, name, displayName, levels, enabled, transactional, truncateonload, lockformat, schemalock, AllowAddColumns,
                AllowNullBindingRemovedColumns, AllowUpcast, AllowVarcharExpansion, AllowValueTruncationOnLoad, FailUploadOnVarcharExpansion, script, loadGroups, sourceGroups,
                customTimeColumn, customTimePrefix, timeShifts, subGroups, colList, snapshots, newColumnTarget, datasourceTimeZone, sourceKey, foreignKeys,
                factForeignKeys, parentForeignKeySource, hierarchyName, levelName, excludeFromModel, SFColPreventUpdate, customUpload, incrementalSnapshotFact,
                snapshotDeleteKeys, OverrideLevelKeys, onlyQuoteAfterSeparator, useBirstLocalForSource, localConnForSource, locations, encoding, separator, replaceWithNull, skipRecord,
                SFColFormats, discoveryTable, liveAccess, tableSource, inheritTables, unCached, rServerPath, rExpression, rSettings, maf, u, sp);
        }



        public static void saveDataSourceAndColumns(HttpSessionState session, string name, string displayName, string[][] levels, bool enabled, bool transactional, bool truncateonload, bool lockformat, bool schemalock,
            bool AllowAddColumns, bool AllowNullBindingRemovedColumns, bool AllowUpcast, bool AllowVarcharExpansion, bool AllowValueTruncationOnLoad, bool FailUploadOnVarcharExpansion, ScriptDefinition script, string[] loadGroups, string sourceGroups,
            string customTimeColumn, string customTimePrefix, CustomTimeShift[] timeShifts, string[] subGroups, StagingColumn[] colList, SnapshotPolicy snapshots, string newColumnTarget, string datasourceTimeZone,
            string[] sourceKey, string foreignKeys, string factForeignKeys, string parentForeignKeySource, string hierarchyName, string levelName, bool excludeFromModel, bool[] SFColPreventUpdate, bool customUpload,
            bool incrementalSnapshotFact, string[] snapshotDeleteKeys, OverrideLevelKey[] OverrideLevelKeys, bool onlyQuoteAfterSeparator, bool useBirstLocalForSource, string localConnForSource,
            SourceLocation[] locations, string encoding, string separator, bool replaceWithNull, bool skipRecord, string[] SFColFormats, bool discoveryTable, bool liveAccess, string tableSource, string[] inheritTables,
            bool unCached, string rServerPath, string rExpression, string rSettings, MainAdminForm maf, User u, Space sp)
        {
            StagingTable st = null;
            SourceFile sf = null;
            foreach (StagingTable sst in maf.stagingTableMod.getAllStagingTables())
            {
                if (sst.Name == name)
                {
                    st = sst;
                    break;
                }

                if (sst.Name.Equals(name, StringComparison.CurrentCultureIgnoreCase))
                    throw new DuplicateNameException("Can not create a new source with that name " + name + ". " + sst.Name + " already exists.");
            }
            bool rebuild = false;
            bool noAuto = false;
            ForeignKey[] fkeys = null;
            ForeignKey[] ffkeys = null;
            if (foreignKeys != null)
                fkeys = ForeignKey.getArrayFromString(foreignKeys);
            if (factForeignKeys != null)
                ffkeys = ForeignKey.getArrayFromString(factForeignKeys);
            bool foreignKeysChanged = false;
            bool imported = false;
            if (st == null)
            {   
                List<ImportedRepositoryItem> itlist = Util.getImportedTables(maf, session, name);
                if (itlist.Count > 0)
                {
                    ImportedRepositoryItem it = itlist[0];
                    if (locations != null && locations.Length > 0 && it.st != null)
                    {
                        ImportedItem ii = it.ispace.pi.getImportedItem(it.st.Name, true);
                        imported = true;
                        /*
                         * When saving foreign key sources, should only save the ones to non-inherited sources
                         */
                        if (fkeys != null)
                        {
                            for (int i = 0; i < fkeys.Length; i++)
                                ii.addForeignKey(fkeys[i]);
                        }
                        else
                        {
                            ii.ForeignKeys = null;
                        }
                        ii.Locations = SourceLocation.getFromSourceLocations(locations);
                        it.ispace.pi.updatePackageImport(maf);
                        if (session != null)
                            session["AdminDirty"] = true;
                    }
                }
            }
            if (!imported && st == null && ((script != null && script.InputQuery != null) || liveAccess))
            {
                /*
                 * Add a new script-based source file
                 */
                sf = new SourceFile();
                sf.Columns = new SourceFile.SourceColumn[0];
                if (liveAccess)
                    sf.FileName = displayName;
                else
                    sf.FileName = displayName + ".txt";
                sf.Separator = "|";
                sf.Quote = "\"";
                sf.Encoding = "UTF-8";
                sf.HasHeaders = true;

                // for a new source file, set the input time zone to defalt
                TimeZoneUtils.setDataSourceTimeZone(sf, "Default");
                st = maf.stagingTableMod.addStagingTable(sf, false, liveAccess);
                if (!liveAccess)
                {
                    if (st != null)
                        maf.sourceFileMod.addSourceFile(sf);
                    else
                        return;
                }
                else
                    // Don't need a source file for a live access table
                    sf = null;
                if (loadGroups != null)
                    st.LoadGroups = loadGroups;
                else
                    st.LoadGroups = new string[] { Util.LOAD_GROUP_NAME };
                if (sourceGroups != null)
                    st.SourceGroups = sourceGroups;
                rebuild = true;
            }
            if (st != null)
            {
                st.LiveAccess = liveAccess;
                updateCustomMeasures(maf, st, levels, enabled);
                st.Levels = levels;
                bool exists = false;
                // Make sure that the time level is added to the staging table
                foreach (string[] level in st.Levels)
                {
                    if (level[0] == maf.timeDefinition.Name)
                    {
                        level[1] = ApplicationBuilder.DayLevelName;
                        exists = true;
                        break;
                    }
                }
                if (!exists)
                {
                    List<string[]> llist = new List<string[]>(st.Levels);
                    llist.Add(new string[] { maf.timeDefinition.Name, ApplicationBuilder.DayLevelName });
                    st.Levels = llist.ToArray();
                }

                if (snapshots == null || snapshots.Type == 0)
                    st.Snapshots = null;
                else
                    st.Snapshots = snapshots;
                st.IncrementalSnapshotFact = incrementalSnapshotFact;
                st.SnapshotDeleteKeys = snapshotDeleteKeys;
                st.OverrideLevelKeys = cleanupOverrideLevelKeys(OverrideLevelKeys);
                st.NewColumnTarget = newColumnTarget;
                st.Disabled = !enabled;
                st.Transactional = transactional;
                st.TruncateOnLoad = truncateonload;
                st.Script = script;
                if (subGroups != null && subGroups.Length > 0)
                    st.SubGroups = subGroups;
                else
                    st.SubGroups = null;
                setOutputString(st, maf);
                string liveAccessLoadGroup = null;
                if (localConnForSource != null && localConnForSource.Trim().Length > 0)
                {
                    Dictionary<string, LocalETLConfig> leConfigs = BirstConnectUtil.getLocalETLConfigsForSpace(sp);
                    if (leConfigs != null)
                    {
                        foreach (LocalETLConfig lconfig in leConfigs.Values)
                        {
                            if (lconfig.getLocalETLConnection().Equals(localConnForSource))
                            {
                                liveAccessLoadGroup = lconfig.getLocalETLLoadGroup();
                                break;
                            }
                        }
                    }
                }
                st.UseBirstLocalForSource = useBirstLocalForSource;
                if (useBirstLocalForSource && liveAccessLoadGroup != null)
                {
                    if (st.PreviousLoadGroup == null)
                    {
                        st.PreviousLoadGroup = st.LoadGroups.Contains(SalesforceLoader.LOAD_GROUP_NAME) ? SalesforceLoader.LOAD_GROUP_NAME : Util.LOAD_GROUP_NAME;
                    }
                    string[] lgroups = new string[st.LoadGroups.Length];
                    for (int i = 0; i < st.LoadGroups.Length; i++)
                    {
                        if (st.LoadGroups[i].StartsWith(LiveAccessUtil.LIVE_ACCESS_LOAD_GROUP_PREFIX) || st.LoadGroups[i].Equals(st.PreviousLoadGroup))
                            lgroups[i] = liveAccessLoadGroup;
                        else
                            lgroups[i] = st.LoadGroups[i];
                    }
                    st.LoadGroups = lgroups;
                    Util.addLoadGroup(maf, liveAccessLoadGroup);
                    Util.addScriptGroup(maf, liveAccessLoadGroup);
                    rebuild = true;
                }
                else if (!useBirstLocalForSource && st.PreviousLoadGroup != null)
                {
                    //reset previous loadgroup (ACORN/SFORCE) for source
                    string[] lgroups = new string[st.LoadGroups.Length];
                    for (int i = 0; i < st.LoadGroups.Length; i++)
                    {
                        if (st.LoadGroups[i].StartsWith(LiveAccessUtil.LIVE_ACCESS_LOAD_GROUP_PREFIX))
                            lgroups[i] = st.PreviousLoadGroup;
                        else
                            lgroups[i] = st.LoadGroups[i];
                    }
                    st.LoadGroups = lgroups;
                    st.PreviousLoadGroup = null;
                    rebuild = true;
                }
                if (sf == null && !liveAccess)
                    sf = maf.sourceFileMod.getSourceFile(st.SourceFile);
                if (sf != null && (script == null || script.InputQuery == null))
                {
                    sf.LockFormat = false;
                    sf.SchemaLock = false;
                    if (lockformat)
                        sf.LockFormat = lockformat;
                    else if (schemalock)
                    {
                        sf.SchemaLock = schemalock;
                        sf.AllowAddColumns = AllowAddColumns;
                        sf.AllowNullBindingRemovedColumns = AllowNullBindingRemovedColumns;
                        sf.AllowUpcast = AllowUpcast;
                        sf.AllowVarcharExpansion = AllowVarcharExpansion;
                        sf.AllowValueTruncationOnLoad = AllowValueTruncationOnLoad;
                        sf.FailUploadOnVarcharExpansion = FailUploadOnVarcharExpansion;
                    }
                    if (!schemalock)
                    {
                        sf.AllowAddColumns = sf.AllowNullBindingRemovedColumns = sf.AllowUpcast = sf.AllowVarcharExpansion = sf.AllowValueTruncationOnLoad = sf.FailUploadOnVarcharExpansion = false;
                    }
                }
                if (sf != null)
                {
                    sf.CustomUpload = customUpload;
                    sf.OnlyQuoteAfterSeparator = onlyQuoteAfterSeparator;
                    TimeZoneUtils.setDataSourceTimeZone(sf, datasourceTimeZone);
                    if (encoding != null && encoding.Length > 0)
                        sf.Encoding = encoding;
                    if (separator != null && separator.Length > 0)
                        sf.Separator = separator;
                    sf.BadColumnValueAction = skipRecord ? SourceFile.SKIP_RECORD : SourceFile.REPLACE_WITH_NULL;
                }
                if (customTimeColumn != null && customTimeColumn.Length > 0)
                {
                    rebuild = rebuild || st.CustomTimeColumn == null || st.CustomTimeColumn != customTimeColumn;
                    if (!rebuild && timeShifts != null)
                    {
                        if (st.CustomTimeShifts == null)
                            rebuild = true;
                        else if (timeShifts.Length != st.CustomTimeShifts.Length)
                            rebuild = true;
                        else
                        {
                            for (int i = 0; i < st.CustomTimeShifts.Length; i++)
                            {
                                if (st.CustomTimeShifts[i].BaseKey != timeShifts[i].BaseKey ||
                                    st.CustomTimeShifts[i].RelativeKey != timeShifts[i].RelativeKey ||
                                    !Performance_Optimizer_Administration.Repository.EqualsArray(st.CustomTimeShifts[i].Columns, timeShifts[i].Columns))
                                {
                                    rebuild = true;
                                    break;
                                }
                            }
                        }
                    }
                    if (timeShifts != null && sf != null)
                    {
                        foreach (CustomTimeShift cts in timeShifts)
                        {
                            List<string> colNames = getDependentColumns(sp, st, sf, cts.BaseKey);
                            if (colNames != null)
                                cts.Columns = colNames.ToArray();
                            else
                                cts.Columns = null;
                        }
                    }
                }
                else
                {
                    if (st.CustomTimeColumn != null && st.CustomTimeColumn.Length > 0)
                        rebuild = true;
                }
                st.CustomTimeColumn = customTimeColumn;
                if (customTimePrefix != null && (customTimePrefix.Length == 0 || customTimePrefix == "null")) // deal with bad data, fix it up here
                    customTimePrefix = null;
                st.CustomTimePrefix = customTimePrefix;
                st.CustomTimeShifts = timeShifts;

                if (sourceKey != null && sourceKey.Length > 0)
                {
                    List<int> keyindices = new List<int>();
                    bool found = true;
                    // Populate source file indices to match
                    if (sf != null)
                    {
                        foreach (string kname in sourceKey)
                        {
                            StagingColumn sc = null;
                            foreach (StagingColumn ssc in st.Columns)
                            {
                                if (ssc.Name == kname)
                                {
                                    sc = ssc;
                                    break;
                                }
                            }
                            if (sc == null)
                            {
                                found = false;
                                break;
                            }
                            bool foundIndex = false;
                            for (int i = 0; i < sf.Columns.Length; i++)
                            {
                                if (sf.Columns[i].Name == sc.SourceFileColumn)
                                {
                                    foundIndex = true;
                                    keyindices.Add(i);
                                    break;
                                }
                            }
                            if (!foundIndex)
                            {
                                found = false;
                                break;
                            }
                        }
                    }
                    if (found)
                    {
                        st.SourceKey = sourceKey;
                        if (sf != null)
                            sf.KeyIndices = keyindices.ToArray();
                    }
                }
                else
                    st.SourceKey = null;
                foreignKeysChanged = areKeysDifferent(fkeys, st.ForeignKeys);
                if (fkeys != null && fkeys.Length > 0)
                    st.ForeignKeys = fkeys;
                else
                    st.ForeignKeys = null;
                foreignKeysChanged = foreignKeysChanged || areKeysDifferent(ffkeys, st.FactForeignKeys);
                if (ffkeys != null && ffkeys.Length > 0)
                    st.FactForeignKeys = ffkeys;
                else
                    st.FactForeignKeys = null;
                if (parentForeignKeySource != null && parentForeignKeySource.Length > 0)
                    st.ParentForeignKeySource = parentForeignKeySource;
                else
                    st.ParentForeignKeySource = null;
                if (hierarchyName != null && hierarchyName.Length > 0)
                {
                    if (st.HierarchyName !=null && st.HierarchyName.Length > 0 && st.HierarchyName != hierarchyName)
                    {
                        // Remove all old targeting (otherwise it will just set back to the old hierarchy)
                        List<string[]> newgrains = new List<string[]>();
                        foreach (string[] level in st.Levels)
                        {
                            if (level[0] != st.HierarchyName)
                                newgrains.Add(level);
                        }
                        st.Levels = newgrains.ToArray();
                        foreach (StagingColumn sc in st.Columns)
                        {
                            if (sc.TargetTypes == null)
                                continue;
                            List<string> newtargets = new List<string>();
                            foreach (string s in sc.TargetTypes)
                            {
                                if (s != st.HierarchyName)
                                    newtargets.Add(s);
                            }
                            if (newtargets.Count != sc.TargetTypes.Length)
                                sc.TargetTypes = newtargets.ToArray();
                        }
                    }
                    st.HierarchyName = hierarchyName;
                }
                else
                    st.HierarchyName = null;
                if (levelName != null && levelName.Length > 0)
                    st.LevelName = levelName;
                else
                    st.LevelName = null;
                bool tsset = false;
                if (inheritTables != null && inheritTables.Length > 0)
                {
                    st.InheritTables = new InheritTable[inheritTables.Length];
                    for (int i = 0; i < inheritTables.Length; i++)
                    {
                        st.InheritTables[i] = new InheritTable();
                        st.InheritTables[i].getFromSerializedString(inheritTables[i]);
                        
                        if (st.InheritTables[i].Type == InheritTable.MEASURE_TYPE)
                        {
                            // Lookup measure table name and grain
                            int index = maf.findMeasureTableIndex(st.InheritTables[i].Name);
                            if (index < 0)
                                continue;
                            MeasureTable mt = maf.measureTablesList[index];
                            if (mt == null)
                                continue;
                            st.InheritTables[i].Name = mt.TableName;
                            st.TableSource = TableSource.fromSerializedString(tableSource);
                            st.TableSource.Tables[0].PhysicalName = mt.TableSource.Tables[0].PhysicalName;
                            st.TableSource.Tables[0].Schema = mt.TableSource.Tables[0].Schema;
                            tsset = true;
                        }
                        if (st.InheritTables[i].Type == InheritTable.DIMENSION_TYPE)
                        {
                            if (tableSource != null && tableSource.Trim().Length > 0)
                            {
                                st.TableSource = TableSource.fromSerializedString(tableSource);
                                fixUpPhysicalNameForInheritTableSource(maf, st.InheritTables[i], st.TableSource);
                                tsset = true;
                            }
                        }
                    }
                }
                else
                    st.InheritTables = null;
                st.ExcludeFromModel = excludeFromModel;
                st.DiscoveryTable = discoveryTable;
                st.RServerPath = rServerPath == null || rServerPath.Trim().Length == 0 ? null : rServerPath;
                st.RExpression = rExpression == null || rExpression.Trim().Length == 0 ? null : rExpression;
                st.RServerSettings = rSettings == null || rSettings.Trim().Length == 0 ? null : rSettings;
                bool setDefaultTargets = false;
                if (maf.builderVersion >= 20)
                    setDefaultTargets = false;
                else if (maf.builderVersion < 11)
                    setDefaultTargets = true;
                else if (maf.builderVersion >= 11 && sp.LoadNumber == 0)
                    setDefaultTargets = true;
                st = setGrainKeys(maf, st, setDefaultTargets);
                if (session != null)
                    session["AdminDirty"] = true;
                if (colList != null)
                {
                    foreach (StagingColumn ssc in colList)
                    {
                        saveStagingColumn(session, st, st.Name, ssc, maf);
                    }
                }
                if (!st.LiveAccess)
                {
                    for (int i = 0; i < st.Columns.Length; i++)
                    {
                        if (st.Columns[i].SourceFileColumn != null && !st.Columns[i].DataType.StartsWith("Date ID:"))
                        {
                            foreach (SourceFile.SourceColumn sfc in sf.Columns)
                            {
                                if (sfc.Name != null && sfc.Name.Equals(st.Columns[i].SourceFileColumn))
                                {
                                    if (SFColPreventUpdate != null && SFColPreventUpdate.Length > i)
                                    {
                                        sfc.PreventUpdate = SFColPreventUpdate[i];
                                    }
                                    if (SFColFormats != null && SFColFormats.Length > i)
                                    {
                                        sfc.Format = SFColFormats[i];
                                    }
                                    break;
                                }
                            }
                        }
                    }
                }
                if (locations != null && locations.Length > 0)
                {
                    Location[] newlocations = SourceLocation.getFromSourceLocations(locations);
                    if (st.DiscoveryTable && st.Locations != null && newlocations != null)
                    {
                        Location allOld = null;
                        Location allNew = null;
                        foreach(Location l in st.Locations)
                        {
                            if (l.LoadGroupName == "All")
                            {
                                allOld = l;
                                break;
                            }
                        }
                        foreach(Location l in newlocations)
                        {
                            if (l.LoadGroupName == "All")
                            {
                                allNew = l;
                                break;
                            }
                        }
                        if (st.Autoplace && allOld != null && allNew != null && (allOld.x != allNew.x || allOld.y != allNew.y))
                        {
                            noAuto = true;
                            st.Autoplace = false;
                        }
                    }
                    st.Locations = newlocations;
                }
                else
                {
                    st.Locations = null;
                }
                if (liveAccess)
                {
                    if (!tsset)
                    {
                        if (tableSource != null && tableSource.Trim().Length > 0)
                        {
                            st.TableSource = TableSource.fromSerializedString(tableSource);                            
                        }
                        else
                        {
                            st.TableSource = null;
                        }
                    }
                    st.UnCached = unCached;
                }
                rebuild |= liveAccess;
            }
            if (rebuild)
            {
                if (session != null)
                    session["RequiresRebuild"] = true;
                maf.RequiresPublish |= !st.LiveAccess;
                Util.updateRequiresPublishFlag(maf, maf.RequiresPublish);
            }
            if (st != null)
                Util.updateStagingTable(maf, st, sf, u.Username, false);

            if (foreignKeysChanged && session != null)
            {
                session["RequiresRebuild"] = true;
            }

            if (noAuto)
            {
                foreach (StagingTable ast in maf.stagingTableMod.getStagingTables())
                {
                    if (ast.DiscoveryTable && ast.Autoplace)
                    {
                        ast.Autoplace = false;
                        Util.updateStagingTable(maf, ast, maf.sourceFileMod.getSourceFile(ast.SourceFile), u.Username, false);
                    }
                }
            }
            if (session != null)
                Util.saveDirty(session);
        }


        public static void fixUpPhysicalNameForInheritTableSource(MainAdminForm maf, InheritTable it, TableSource tableSource)
        {
            if (maf != null && it != null && tableSource != null && tableSource.Tables != null)
            {
                if (it.Type == InheritTable.DIMENSION_TYPE)
                {
                    foreach (TableSource.TableDefinition td in tableSource.Tables)
                    {
                        if (ApplicationBuilder.ContainsNonASCIICharacters(td.PhysicalName))
                        {
                            string newName = td.PhysicalName;
                            if (newName.StartsWith("DW_DM_"))
                            {
                                newName = newName.Substring(6);
                                ApplicationBuilder ab = new ApplicationBuilder(maf);
                                newName = ApplicationBuilder.getPhysicalTableName(ab.getDbType(), newName, null, ab.getMaxPhysicalTableNameLength(), maf.builderVersion);
                                td.PhysicalName = newName;                                                                
                            }
                        }
                    }
                }
            }
        }

        // cleanup 'bad' level key arrays due to the UI passing back a data structure w/o any columns
        //       <OverrideLevelKeys>
        //          <OverrideLevelKey>
        //              <Dimension>Employees</Dimension>
        //              <Level>Employees</Level>
        //              <KeyColumns />
        //          </OverrideLevelKey>
        //      </OverrideLevelKeys
        public static OverrideLevelKey[] cleanupOverrideLevelKeys(OverrideLevelKey[] keys)
        {
            if (keys == null)
                return null;
            if (keys.Length == 0)
                return null;
            List<OverrideLevelKey> keyList = new List<OverrideLevelKey>();
            foreach (OverrideLevelKey key in keys)
            {
                if (key.Dimension != null && key.Level != null && key.KeyColumns != null && key.KeyColumns.Length > 0)
                    keyList.Add(key);
            }
            if (keyList.Count == 0)
                return null;
            return keyList.ToArray();
        }

        private static bool areKeysDifferent(ForeignKey[] fkeys, ForeignKey[] foreignKeys)
        {
            if((fkeys == null && foreignKeys != null && foreignKeys.Length > 0) || (fkeys != null && fkeys.Length > 0 && foreignKeys == null))
            {
                return true;
            }

            if (fkeys != null && foreignKeys != null)
            {
                if (fkeys.Length != foreignKeys.Length)
                {
                    return true;
                }

                foreach (ForeignKey fkey in fkeys)
                {
                    bool found = false;
                    foreach (ForeignKey foreignKey in foreignKeys)
                    {
                        if (foreignKey.Source == fkey.Source && foreignKey.Type == fkey.Type && foreignKey.Condition == fkey.Condition)
                        {
                            found = true;
                            break;
                        }
                    }
                    if (!found)
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        // Bug 8066
        // Find all the custom measures with matching grain of the current staging table.
        // Update it with updated Grain.
        // LIMITATION: Make sure that there is no other data source with the old grain. If there
        // are other sources (apart from the current one), do not do anything
        // Before the levels are udpated on the staging table, we compare the exisiting levels with the 
        // new ones and if they are changed, find all the affected custom measures and udpates the same
        // (if there are no other data sources with matching grain)

        private static void updateCustomMeasures(MainAdminForm maf, StagingTable st, string[][] updatedLevels, bool enabled)
        {
            if (maf == null || st == null)
            {
                return;
            }

            // if the data source is selected as Ignore, no need to continue
            if (!enabled)
            {
                return;
            }
            // if there are no CustomMeasures, no need to proceed
            LogicalExpression[] logicalExpressionList = maf.getLogicalExpressions();
            if (logicalExpressionList == null || logicalExpressionList.Length == 0)
            {
                return;
            }

            try
            {
                // find if the grain of the data source has changed
                bool sameGrain = CustomCalculations.matchGrain(st.Levels, updatedLevels);
                if (!sameGrain)
                {
                    bool matchingGrain = false;
                    // see if any other data source has the matching grain
                    foreach (StagingTable stOther in maf.stagingTableMod.getStagingTables())
                    {
                        if (stOther.Name == st.Name)
                        {
                            continue;
                        }

                        if (CustomCalculations.matchGrain(st.Levels, stOther.Levels))
                        {
                            // if there is any other data source with the matching grain, do not
                            // attempt to modify Custom Measures - bug 8066
                            matchingGrain = true;
                            break;
                        }
                    }

                    // if there is no other matching grain found, proceed and update the custom mesures if there are any
                    if (!matchingGrain)
                    {
                        foreach (LogicalExpression le in logicalExpressionList)
                        {
                            if (le.Type == LogicalExpression.TYPE_MEASURE)
                            {
                                // get the Custom measure with the matching grain with the old levels
                                // and update it with the new levels.
                                if (CustomCalculations.matchGrain(st.Levels, le.Levels))
                                {
                                    le.Levels = updatedLevels;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                // let the user continue atleast. Log the exception message
                Global.systemLog.Error("Exception while updating Custom Measures", ex);
            }
        }

        private static List<string> getDependentColumns(Space sp, StagingTable st, SourceFile sf, string cname)
        {
            Encoding encoding = SourceFiles.getEncoding(sf);
            StreamReader reader = null;
            int keyColumn = -1;
            for (int i = 0; i < sf.Columns.Length; i++)
            {
                if (sf.Columns[i].Name == cname)
                {
                    keyColumn = i;
                    break;
                }
            }
            if (keyColumn < 0)
                return null;
            List<string> results = new List<string>();
            try
            {
                string path = Path.Combine(sp.Directory, "data");
                string fname = Path.Combine(path, sf.FileName);
                Util.validateFileLocation(path, fname);
                if (encoding == null)
                    reader = new StreamReader(fname);
                else
                    reader = new StreamReader(fname, encoding);
                string line = null;
                Dictionary<string, string[]> map = new Dictionary<string, string[]>();
                List<int> colList = new List<int>();
                for (int i = 0; i < sf.Columns.Length; i++)
                    colList.Add(i);
                int count = 0;
                while ((line = SourceFiles.readLine(reader, sf.Separator, sf.IgnoreBackslash, sf.Quote, sf.ForceNumColumns ? sf.Columns.Length - 1 : 0, false, sf.DoNotTreatCarriageReturnAsNewline)) != null &&
                    count < NUM_LINES_TO_SCAN)
                {
                    bool skip = false;
                    if (sf.IgnoredRows > 0 && count < sf.IgnoredRows)
                        skip = true;
                    count++;
                    if (skip)
                        continue;
                    if (sf.HasHeaders && count == 1)
                        continue;
                    string[] cols = Performance_Optimizer_Administration.Util.splitString(line, sf.Separator[0]);
                    if (cols.Length <= keyColumn)
                        continue;
                    if (!map.ContainsKey(cols[keyColumn]))
                    {
                        map.Add(cols[keyColumn], cols);
                    }
                    else
                    {
                        string[] mappedCols = map[cols[keyColumn]];
                        List<int> toRemove = new List<int>();
                        foreach (int i in colList)
                        {
                            if (mappedCols.Length <= i || cols.Length <= i)
                                continue;
                            if (mappedCols[i] != cols[i])
                                toRemove.Add(i);
                        }
                        foreach (int i in toRemove)
                            colList.Remove(i);
                    }
                }
                reader.Close();
                foreach (int i in colList)
                {
                    for (int j = 0; j < st.Columns.Length; j++)
                    {
                        if (st.Columns[j].SourceFileColumn != null && st.Columns[j].SourceFileColumn == sf.Columns[i].Name)
                        {
                            results.Add(st.Columns[j].Name);
                            break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error(ex);
            }
            finally
            {
                if (reader != null)
                    reader.Close();
            }
            return results;
        }

        public static void setOutputString(StagingTable st, MainAdminForm maf)
        {
            if (st == null || st.Script == null || st.Script.Script == null || st.Script.InputQuery == null || st.Script.InputQuery.Length == 0)
                return;
            string outputString = getOutputString(st, maf);
            st.Script.Output = outputString;
        }

        public static string getOutputString(StagingTable st, MainAdminForm maf)
        {            
            StringBuilder sb = new StringBuilder();
            int index = st.SourceFile.LastIndexOf('.');
            string s = st.SourceFile;
            if (index >= 0)
                s = s.Substring(0, index);
            sb.Append('[' + s + "] COLUMNS ");
            bool first = true;
            foreach (StagingColumn sc in st.Columns)
            {
                if (sc.DataType == "Integer" && sc.Name == ApplicationBuilder.DayIDName && sc.TargetTypes != null &&
                    sc.TargetTypes.Length == 1 && maf.timeDefinition != null && sc.TargetTypes[0] == maf.timeDefinition.Name)
                    continue;
                if (sc.Transformations != null && sc.Transformations.Length > 0)
                    continue;
                if (sc.SourceFileColumn == null || sc.SourceFileColumn.Length == 0)
                    continue;
                if (sc.GenerateTimeDimension && sc.DataType.StartsWith("Date ID:"))
                    continue;
                if (first)
                    first = false;
                else
                    sb.Append(',');
                sb.Append('[' + sc.Name + "] ");
                if (sc.DataType == "Varchar")
                    sb.Append("VARCHAR(" + sc.Width + ")");
                else
                    sb.Append(sc.DataType.ToUpper());
            }
            return sb.ToString();
        }

        public static void removeDataSource(HttpSessionState session, string name)
        {
            MainAdminForm maf = (MainAdminForm)session["MAF"];
            if (maf == null)
                return;
            foreach (StagingTable st in maf.stagingTableMod.getAllStagingTables())
            {
                if (st.Name == name)
                {
                    SourceFile sf = maf.sourceFileMod.getSourceFile(st.SourceFile);
                    if (sf != null)
                        maf.sourceFileMod.removeSourceFile(sf.FileName);
                    maf.stagingTableMod.removeStagingTable(st.Name);
                    session["AdminDirty"] = true;
                    if (st.LiveAccess)
                    {   
                        session["RequiresRebuild"] = true;
                    }
                    Util.saveDirty(session);
                    break;
                }
            }
        }

        public static string[][] getRawData(HttpSessionState session, string sourceName, int startRow, int endRow)
        {
            MainAdminForm maf = (MainAdminForm)session["MAF"];
            if (maf == null)
                return null;
            Space sp = (Space)session["space"];
            if (sp == null)
                return null;
            bool isScriptSourceAndGzip = false;
            SourceFile sf = null;
            foreach (StagingTable st in maf.stagingTableMod.getAllStagingTables())
            {
                if (st.Name == sourceName)
                {
                    sf = maf.sourceFileMod.getSourceFile(st.SourceFile);
                    if (st.Script != null && (sp.DatabaseType == Database.REDSHIFT || sp.DatabaseType == Database.PARACCEL))
                    {
                        isScriptSourceAndGzip = true;
                    }
                    break;
                }
            }
            if (sf == null)
                return null;
            StreamReader reader = null;
            Encoding encoding = null;
            if (sf.Encoding == null)
                encoding = null;
            else if (sf.Encoding == "UTF-8")
                encoding = Encoding.UTF8;
            else if (sf.Encoding == "UTF-16" || sf.Encoding == "Unicode")
                encoding = Encoding.Unicode;
            else if (sf.Encoding == "UTF-16LE")
                encoding = Encoding.Unicode;
            else if (sf.Encoding == "UTF-16BE")
                encoding = Encoding.BigEndianUnicode;
            string path = Path.Combine(sp.Directory, "data");
            string fname = Path.Combine(path, sf.FileName);
            if (isScriptSourceAndGzip && sp.DatabaseType == Database.REDSHIFT)
            {
                fname = fname + ".1";                
            }
            Util.validateFileLocation(path, fname);
            List<string[]> result = new List<string[]>();
            try
            {
                GZipStream instream = null;
                if (isScriptSourceAndGzip)
                {
                    instream = new GZipStream(File.OpenRead(fname), CompressionMode.Decompress);
                }
                if (isScriptSourceAndGzip && instream != null)
                {
                    if (encoding == null)
                    {
                        reader = new StreamReader(instream);
                    }
                    else
                    {
                        reader = new StreamReader(instream, encoding);
                    }
                }
                else
                {
                    if (encoding == null)
                    {
                        reader = new StreamReader(fname);
                    }
                    else
                    {
                        reader = new StreamReader(fname, encoding);
                    }
                }
                for (int i = 0; i < endRow; i++)
                {
                    string line = SourceFiles.readLine(reader, sf.Separator, sf.IgnoreBackslash, sf.Quote, sf.ForceNumColumns ? sf.Columns.Length - 1 : 0, sf.OnlyQuoteAfterSeparator, sf.DoNotTreatCarriageReturnAsNewline);
                    if (line == null)
                        break;
                    char sep = '|';
                    if (sf.Separator != null && sf.Separator.Length > 0)
                        sep = sf.Separator[0];
                    string[] cols = Performance_Optimizer_Administration.Util.splitString(line, sep);
                    result.Add(cols);
                }
                reader.Close();
            }
            catch (System.IO.FileNotFoundException)
            {
                // file does not exist, the script probably has not been run
                return null;
            }
            catch (Exception e)
            {
                Global.systemLog.Error(e);
            }
            return result.ToArray();
        }

        public static string[][] getColumnsAtLowerGrains(HttpSessionState session, string sourceName, bool automaticMode)
        {
            MainAdminForm maf = (MainAdminForm)session["MAF"];
            if (maf == null)
                return null;
            StagingTable st = null;
            foreach (StagingTable sst in maf.stagingTableMod.getAllStagingTables())
            {
                if (sst.Name == sourceName)
                {
                    st = sst;
                    break;
                }
            }
            if (st == null)
                return null;
            List<StagingTable> stlist = new List<StagingTable>();
            List<string[]> resultList = new List<string[]>();
            foreach (StagingTable sst in maf.stagingTableMod.getAllStagingTables())
            {
                if (sst == st)
                    continue;
                string sname = sst.SourceFile;
                int index = sname.LastIndexOf('.');
                if (index >= 0)
                    sname = sname.Substring(0, index);
                if (isTargeted(maf, sst) && ApplicationBuilder.isLowerGrain(maf, getGrainList(st), getGrainList(sst), maf.dependencies, automaticMode) != null)
                {
                    foreach (StagingColumn sc in sst.Columns)
                    {
                        if (sc.TargetTypes != null && Array.IndexOf<string>(sc.TargetTypes, "Measure") >= 0)
                            resultList.Add(new string[] {sc.Name, sname });
                    }
                }
            }
            return resultList.ToArray();
        }

        public static bool isTargeted(MainAdminForm maf, StagingTable st)
        {
            if (maf.timeDefinition != null)
            {
                if (st.Levels == null || st.Levels.Length == 0)
                    return false;
                if (st.Levels.Length == 1 && st.Levels[0][0] == maf.timeDefinition.Name)
                    return false;
                return true;
            }
            else
            {
                return !(st.Levels == null || st.Levels.Length == 0);
            }
        }

        private static List<MeasureTableGrain> getGrainList(StagingTable st)
        {
            List<MeasureTableGrain> mtglist = new List<MeasureTableGrain>();
            foreach (string[] level in st.Levels)
            {
                MeasureTableGrain mtg = new MeasureTableGrain();
                mtg.DimensionName = level[0];
                mtg.DimensionLevel = level[1];
                mtglist.Add(mtg);
            }
            return mtglist;
        }

        public static void emptyDataSource(HttpSessionState session, string name)
        {
            MainAdminForm maf = (MainAdminForm)session["MAF"];
            Space sp = null;
            if (maf == null)
                return;
            foreach (StagingTable st in maf.stagingTableMod.getAllStagingTables())
            {
                if (st.Name == name)
                {
                    SourceFile sf = maf.sourceFileMod.getSourceFile(st.SourceFile);
                    if (sf != null)
                    {
                        sp = (Space)session["space"];
                        if (sp == null) return;
                        string path = Path.Combine(sp.Directory, "data");
                        string file = Path.Combine(path, st.SourceFile);
                        Util.validateFileLocation(path, file);
                        FileInfo fi = new FileInfo(file);
                        if (fi.Exists)
                        {
                            // remove the source file and recreate it
                            fi.Delete();
                            StreamWriter fs = fi.CreateText();
                            fs.Close();
                            break;
                        }
                    }
                }
            }
        }

        public class LastProcessedResults
        {
            public List<Hierarchy> hierarchies;
            public List<StagingTable> stagingTables;
        }

        private static LastProcessedResults getLastProcessedResults(MainAdminForm maf, Space sp)
        {
            LastProcessedResults lpr = new LastProcessedResults();
            DirectoryInfo di = new DirectoryInfo(sp.Directory);
            FileInfo[] flist = di.GetFiles("*.save", SearchOption.TopDirectoryOnly);
            FileInfo fi = null;
            // Find the most recent published repository - cannot make changes to hierarchies that have been processed
            foreach (FileInfo sfi in flist)
            {
                if (sfi.Name.EndsWith(sp.LoadNumber + ".save"))
                    continue;
                if (fi == null)
                    fi = sfi;
                else if (sfi.LastWriteTime > fi.LastWriteTime)
                    fi = sfi;
            }
            if (fi == null)
                return null;
            lpr.hierarchies = new List<Hierarchy>();
            string file = (new StreamReader(fi.OpenRead())).ReadToEnd();
            int index1 = file.IndexOf("<Hierarchy>");
            if (index1 < 0)
                return null;
            XmlSerializer serializer = new XmlSerializer(typeof(Hierarchy));
            try
            {
                while (index1 < file.Length && index1 >= 0)
                {
                    int index2 = file.IndexOf("</Hierarchy>", index1);
                    if (index2 < 0)
                        return null;
                    index2 += 12;
                    string deserialize = file.Substring(index1, index2 - index1);
                    XmlReader xreader = new XmlTextReader(new StringReader(deserialize));
                    Hierarchy h = (Hierarchy)serializer.Deserialize(xreader);
                    if (h != null)
                        lpr.hierarchies.Add(h);
                    index1 = file.IndexOf("<Hierarchy>", index2);
                }
            }
            catch (System.InvalidOperationException ex)
            {
                Global.systemLog.Error(ex);
                return null;
            }
            lpr.stagingTables = new List<StagingTable>();
            index1 = file.IndexOf("<StagingTable>");
            if (index1 < 0)
                return null;
            serializer = new XmlSerializer(typeof(StagingTable));
            try
            {
                while (index1 < file.Length && index1 >= 0)
                {
                    int index2 = file.IndexOf("</StagingTable>", index1);
                    if (index2 < 0)
                        return null;
                    index2 += 15;
                    string deserialize = file.Substring(index1, index2 - index1);
                    XmlReader xreader = new XmlTextReader(new StringReader(deserialize));
                    StagingTable st = (StagingTable)serializer.Deserialize(xreader);
                    if (st != null)
                    {
                        bool wasTargeted = st.Levels != null && st.Levels.Length > 1;
                        if (wasTargeted && st.Levels.Length == 1 && st.Levels[0][0] == "Time")
                            wasTargeted = false;
                        // Don't freeze data modeling if the table wan't targeted to the warehouse before
                        if (wasTargeted)
                            lpr.stagingTables.Add(st);
                    }
                    index1 = file.IndexOf("<StagingTable>", index2);
                }
            }
            catch (System.InvalidOperationException ex)
            {
                Global.systemLog.Error(ex);
                return null;
            }
            return lpr;
        }

        public static void updateLogicalModel(HttpSessionState session)
        {
            MainAdminForm maf = (MainAdminForm)session["MAF"];
            if (maf == null)
                return;
            Space sp = (Space)session["space"];
            if (sp == null)
                return;
            LastProcessedResults lpr = getLastProcessedResults(maf, sp);

            // Clear out any previously generated hierarchies
            List<Hierarchy> hlist = new List<Hierarchy>();
            foreach (Hierarchy h in maf.hmodule.getLatestHierarchies())
            {
                if (h.ModelWizard)
                {
                    bool found = false;
                    // Only delete hierarchies that haven't previously been processed
                    if (lpr != null)
                    {
                        foreach (Hierarchy oh in lpr.hierarchies)
                        {
                            if (oh.Name == h.Name)
                            {
                                found = true;
                            }
                        }
                    }
                    if (!found)
                        hlist.Add(h);
                }
            }
            foreach (Hierarchy h in hlist)
                ManageSources.deleteHierarchy(maf, h, session);
            List<string> oldStagingTableNames = new List<string>();
            if (lpr != null)
                foreach (StagingTable ost in lpr.stagingTables)
                {
                    if (ost.ExcludeFromModel || ost.Disabled)
                        continue;
                    oldStagingTableNames.Add(ost.Name);
                }
            ApplicationBuilder ab = new ApplicationBuilder(maf);
            List<Hierarchy[]> matches = new List<Hierarchy[]>();
            // Create a new hierarchy for each and record dependency matches
            foreach (StagingTable st in maf.stagingTableMod.getAllStagingTables())
            {
                if (st.ExcludeFromModel || st.Disabled)
                    continue;
                if (st.SourceKey == null || st.SourceKey.Length == 0)
                    continue;
                SourceFile sf = maf.sourceFileMod.getSourceFile(st.SourceFile);
                if (sf == null)
                    continue;
                Hierarchy h = null;
                if (!oldStagingTableNames.Contains(st.Name))
                    h = ApplicationUploader.setupNewStagingTable(maf, st, sf, null, st.HierarchyName, st.LevelName, true);
                // If previously processed staging tables are present, add any foreign keys to grains
                if (lpr != null && lpr.stagingTables.Count > 0 && !oldStagingTableNames.Contains(st.Name))
                {
                    foreach (StagingTable ost in lpr.stagingTables)
                    {
                        if (ost.ForeignKeys == null)
                            continue;
                        StagingTable newost = null;
                        foreach (StagingTable sst in maf.stagingTableMod.getAllStagingTables())
                        {
                            if (sst.ExcludeFromModel || sst.Disabled)
                                continue;
                            if (sst.Name == ost.Name)
                            {
                                newost = sst;
                                break;
                            }
                        }
                        if (newost == null)
                            continue;
                        foreach (ForeignKey fk in newost.ForeignKeys)
                        {
                            string s = fk.Source;
                            if (s == st.Name)
                            {
                                List<string[]> llist = new List<string[]>(ost.Levels);
                                bool found = false;
                                foreach (string[] level in llist)
                                {
                                    if (level[0] == h.DimensionName)
                                    {
                                        found = true;
                                    }
                                }
                                if (!found)
                                {
                                    llist.Add(new string[] { h.DimensionName, h.DimensionKeyLevel });
                                    newost.Levels = llist.ToArray();
                                    // Add it to lower-level grains too
                                    addToLowerLevelGrains(maf, ab, newost, h);
                                }
                            }
                        }
                    }
                }
            }
            // Now fold hierarchies and create dependencies
            foreach (StagingTable st in maf.stagingTableMod.getAllStagingTables())
            {
                if (st.ExcludeFromModel)
                    continue;
                if (st.SourceKey == null || st.SourceKey.Length == 0)
                    continue;
                SourceFile sf = maf.sourceFileMod.getSourceFile(st.SourceFile);
                if (sf == null)
                    continue;
                if (oldStagingTableNames.Contains(st.Name))
                    continue;
                if (st.ForeignKeys != null)
                {
                    foreach (ForeignKey fk in st.ForeignKeys)
                    {
                        string fkname = fk.Source;
                        StagingTable fkst = maf.stagingTableMod.findTable(fkname);
                        if (fkst == null)
                            continue;
                        Hierarchy fkh = getHierarchy(maf, fkst.HierarchyName);
                        Hierarchy h = getHierarchy(maf, st.HierarchyName);
                        if (h == null || fkh == null || h.ModelWizard == false || fkh.ModelWizard == false)
                            continue;
                        bool found = false;
                        // Only match hierarchies that haven't previously been processed
                        if (lpr != null)
                        {
                            foreach (Hierarchy oh in lpr.hierarchies)
                            {
                                if (oh.Name == h.Name)
                                {
                                    found = true;
                                }
                            }
                        }
                        if (!found )
                            matches.Add(new Hierarchy[] { h, fkh });
                    }
                }
            }
            // Add sources with no primary key
            foreach (StagingTable st in maf.stagingTableMod.getAllStagingTables())
            {
                if (st.SourceKey != null && st.SourceKey.Length > 0)
                    continue;
                if (st.ExcludeFromModel)
                    continue;
                SourceFile sf = maf.sourceFileMod.getSourceFile(st.SourceFile);
                if (sf == null)
                    continue;
                if (oldStagingTableNames.Contains(st.Name))
                    continue;
                if (st.ForeignKeys != null)
                {
                    List<string[]> newlist = new List<string[]>();
                    if (maf.timeDefinition != null)
                        foreach (string[] level in st.Levels)
                        {
                            if (level[0] == maf.timeDefinition.Name)
                            {
                                newlist.Add(level);
                                break;
                            }
                        }
                    foreach (ForeignKey fk in st.ForeignKeys)
                    {
                        string fkname = fk.Source;
                        StagingTable fkst = maf.stagingTableMod.findTable(fkname);
                        if (fkst == null)
                            continue;
                        Hierarchy fkh = getHierarchy(maf, fkst.HierarchyName);
                        if (fkh == null)
                            continue;
                        newlist.Add(new string[] { fkh.DimensionName, fkh.DimensionKeyLevel });
                    }
                    st.Levels = newlist.ToArray();
                }
            }
            try
            {
                ApplicationLoader.foldHierarchies(maf, sp, null, matches, lpr != null ? lpr.hierarchies : null, true);
                // After folding do a rebuild to make sure that all grains are set (including ones that are implied)
                Util.reBuildIfRequired(session, maf, sp, false);
                // For each source, make sure that the natural key flag is set for grain level keys
                foreach (StagingTable st in maf.stagingTableMod.getAllStagingTables())
                {
                    if (st.ForeignKeys == null || st.ForeignKeys.Length == 0)
                        continue;
                    if (st.ExcludeFromModel)
                        continue;
                    setGrainKeys(maf, st);
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error(ex);
            }
            session["RequiresRebuild"] = true;
            session["AdminDirty"] = true;
            Util.saveDirty(session, true);
        }

        private static void addToLowerLevelGrains(MainAdminForm maf, ApplicationBuilder ab, StagingTable st, Hierarchy h)
        {
            List<StagingTable> processed = new List<StagingTable>();
            processed.Add(st);
            MeasureGrain curGrain = ab.getStagingTableGrain(maf, st);
            // Remove the grain to be added (because we want to see if other grains are lower BEFORE this grain is added)
            List<MeasureTableGrain> glist = new List<MeasureTableGrain>();
            foreach (MeasureTableGrain mtg in curGrain.measureTableGrains)
            {
                if (mtg.DimensionName == h.DimensionName)
                    continue;
                glist.Add(mtg);
            }
            curGrain.measureTableGrains = glist.ToArray();
            foreach (StagingTable sst in maf.stagingTableMod.getStagingTables())
            {
                if (sst.Disabled || sst.ExcludeFromModel || processed.Contains(sst))
                    continue;
                MeasureGrain grain = ab.getStagingTableGrain(maf, sst);
                List<MeasureTableGrain[]> matches = ApplicationBuilder.isLowerGrain(maf, curGrain.measureTableGrains, grain.measureTableGrains, maf.dependencies, false);
                if (matches != null)
                {
                    List<string[]> llist = new List<string[]>(sst.Levels);
                    bool found = false;
                    foreach (string[] level in llist)
                    {
                        if (level[0] == h.DimensionName)
                        {
                            found = true;
                        }
                    }
                    if (!found)
                    {
                        llist.Add(new string[] { h.DimensionName, h.DimensionKeyLevel });
                        sst.Levels = llist.ToArray();
                        // Add it to lower-level grains too
                        addToLowerLevelGrains(maf, ab, sst, h);
                    }
                }
            }
        }

        public static void deleteHierarchy(MainAdminForm maf, Hierarchy h, HttpSessionState session)
        {
            string oldname = h.Name;
            /*
             * Update grains
             */
            foreach (StagingTable st in maf.stagingTableMod.getAllStagingTables())
            {
                if (st.HierarchyName == oldname)
                    st.HierarchyName = st.getDisplayName();
                foreach (string[] level in st.Levels)
                {
                    if (level[0] == oldname)
                    {
                        string[][] newlevels = new string[st.Levels.Length - 1][];
                        int count = 0;
                        foreach (string[] level2 in st.Levels)
                        {
                            if (level2[0] != oldname)
                                newlevels[count++] = new string[2] { level2[0], level2[1] };
                        }
                        if (validateLevels(maf, newlevels))
                            st.Levels = newlevels;
                        break;
                    }
                }
                foreach (StagingColumn sc in st.Columns)
                {
                    if (sc.TargetTypes != null)
                        for (int i = 0; i < sc.TargetTypes.Length; i++)
                            if (sc.TargetTypes[i] == oldname)
                            {
                                int count = 0;
                                string[] ttypes = new string[sc.TargetTypes.Length - 1];
                                for (int j = 0; j < sc.TargetTypes.Length; j++)
                                    if (sc.TargetTypes[j] != oldname)
                                        ttypes[count++] = sc.TargetTypes[j];
                                sc.TargetTypes = ttypes;
                                break;
                            }
                }
                if (session != null)
                {
                    DataTable dt = (DataTable)session["stagingdatatable"];
                    if (dt != null)
                    {
                        foreach (DataRow dr in dt.Rows)
                        {
                            object o = dr["Hierarchy"];
                            if (o != System.DBNull.Value && (string)o == oldname)
                            {
                                dr["Hierarchy"] = System.DBNull.Value;
                                dr["Level"] = System.DBNull.Value;
                            }
                        }
                    }
                }
            }
            // Delete the dimension
            maf.hmodule.removeHierarchy(h);
            maf.deleteDimension(oldname);
        }


        internal static Hierarchy[] getHierarchies(HttpSessionState Session)
        {
            List<Hierarchy> response = new List<Hierarchy>();
            MainAdminForm maf = (MainAdminForm)Session["MAF"];
            if (maf != null)
            {
                response.AddRange(maf.hmodule.getLatestHierarchies());
                List<ImportedRepositoryItem> importedItemsList = Util.getImportedTables(maf, Session, null);
                if(importedItemsList != null && importedItemsList.Count > 0)
                {
                    List<Hierarchy> importedHierarchies = new List<Hierarchy>();
                    foreach (ImportedRepositoryItem it in importedItemsList)
                    {
                        if (it.dt == null || it.hide)
                            continue;
                        Hierarchy h = it.ispace.maf.hmodule.getDimensionHierarchy(it.dt.DimensionName);
                        bool found = false;
                        foreach (Hierarchy sh in importedHierarchies)
                        {
                            if (sh.Name == h.Name)
                            {
                                found = true;
                                break;
                            }
                        }
                        if (!found)
                        {
                            h.Imported = true;
                            importedHierarchies.Add(h);
                        }
                    }

                    if (importedHierarchies.Count > 0)
                    {
                        response.AddRange(importedHierarchies);
                    }
                }
            }

            return response.ToArray();
        }
    }
}
