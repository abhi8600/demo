﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using Performance_Optimizer_Administration;
using System.Text;
using System.Web.SessionState;
using Acorn.Utils;

namespace Acorn
{
    public class CustomCalculations
    {
        public static Dictionary<string, string[][]> getGrains(HttpSessionState session)
        {
            MainAdminForm maf = (MainAdminForm)session["MAF"];
            if (maf == null)
                return null;
            Dictionary<string, string[][]> uniqueGrains = getGrains(maf);
            session["grainmap"] = uniqueGrains;
            return (uniqueGrains);
        }

        public static Dictionary<string, string[][]>  getGrains(MainAdminForm maf)
        {
            Dictionary<string, string[][]> uniqueGrains = new Dictionary<string, string[][]>();
            foreach (StagingTable st in maf.stagingTableMod.getStagingTables())
            {
                bool found = false;
                foreach (string[][] grain in uniqueGrains.Values)
                {
                    if (matchGrain(st.Levels, grain))
                    {
                        found = true;
                        break;
                    }
                }
                if (!found)
                {
                    string grainname = getGrainString(st.Levels);
                    uniqueGrains.Add(grainname, st.Levels);
                }
            }
            return (uniqueGrains);
        }

        public static List<string> getDiscoverySources(HttpSessionState session)
        {
            MainAdminForm maf = (MainAdminForm)session["MAF"];
            if (maf == null)
                return null;
            List<string> result = new List<string>();
            foreach (StagingTable st in maf.stagingTableMod.getStagingTables())
            {
                if (st.DiscoveryTable)
                {
                    result.Add(ApplicationBuilder.getDisplayName(st));
                }
            }
            return result;
        }

        public static bool matchGrain(string[][] grain1, string[][] grain2)
        {
            if (grain1.Length != grain2.Length)
                return (false);
            foreach (string[] level in grain1)
            {
                bool found = false;
                foreach (string[] level2 in grain2)
                {
                    if (level[0] == level2[0] && level[1] == level2[1])
                    {
                        found = true;
                        break;
                    }
                }
                if (!found)
                    return (false);
            }
            return (true);
        }

        public static string getGrainString(string[][] grain)
        {
            StringBuilder sb = new StringBuilder();
            foreach (string[] level in grain)
            {
                if (sb.Length > 0)
                    sb.Append(',');
                sb.Append(level[0] + "." + level[1]);
            }
            return (sb.ToString());
        }

        public static List<string> getLevels(HttpSessionState session)
        {
            MainAdminForm maf = (MainAdminForm)session["MAF"];
            if (maf == null)
                return null;

            Dictionary<string, string[]> levelMap = new Dictionary<string, string[]>();
            List<string> result = getLevels(maf, out levelMap);
            session["levelMap"] = levelMap;
            return result;
        }

        public static List<string>  getLevels(MainAdminForm maf, out Dictionary<string, string[]> levelMap)
        {
           levelMap = new Dictionary<string, string[]>();
            List<string> result = new List<string>();
            foreach (Hierarchy h in maf.hmodule.getHierarchies())
            {
                //if (maf.timeDefinition != null && maf.timeDefinition.Name == h.DimensionName)
                //    continue;
                List<Level> llist = new List<Level>();
                h.getChildLevels(llist);
                foreach (Level l in llist)
                {
                    if (l.GenerateDimensionTable || l.AutomaticallyCreatedAsDegenerate)
                    {
                        string name = h.DimensionName + "." + l.Name;
                        try
                        {
                            levelMap.Add(name, new string[] { h.DimensionName, l.Name });
                            result.Add(name);
                        }
                        catch (Exception)
                        {
                        }
                    }
                }
            }

            return result;
        }

        public static void setAttributeColumnMap(HttpSessionState session, string dim, string lname)
        {
            MainAdminForm maf = (MainAdminForm)session["MAF"];
            if (maf == null)
                return;
            // Get columns and type maps
            Dictionary<string, string> columnMap = new Dictionary<string, string>();
            Dictionary<string, ExpressionParser.LogicalExpression.DataType> typeMap =
                new Dictionary<string, ExpressionParser.LogicalExpression.DataType>();
            Hierarchy h = maf.hmodule.getHierarchy(dim);
            Level lvl = null;
            if(h != null)
            {
                lvl = h.findLevel(lname);
            }
            DimensionTable dtAtLvl = null;
            if (lvl != null)
            {
                foreach (DimensionTable dt in maf.dimensionTablesList)
                {
                    if (dt.DimensionName == dim && dt.Level == lvl.Name)
                    {
                        if (dt.InheritTable != null && dt.InheritTable.Length > 0 && dt.InheritPrefix != null && dt.InheritPrefix.Length > 0)
                            continue;
                        dtAtLvl = dt;
                        if (lvl.AutomaticallyCreatedAsDegenerate)
                        {
                            addToColumnAndTypeMaps(maf, dtAtLvl, columnMap, typeMap);
                        }
                        else
                        {
                            foreach (StagingTable st in maf.stagingTableMod.getStagingTables())
                            {
                                bool found = false;
                                if (st.Levels == null)
                                    continue;
                                foreach (string[] level in st.Levels)
                                {
                                    if (level[0] == dim)
                                    {
                                        found = true;
                                        break;
                                    }
                                }
                                if (!found)
                                    continue;
                                foreach (StagingColumn sc in st.Columns)
                                {
                                    if (sc.TargetTypes == null)
                                        continue;
                                    if (Array.IndexOf<string>(sc.TargetTypes, dim) >= 0)
                                    {
                                        string name = sc.getLogicalColumnName();
                                        foreach (DimensionColumn dc in dtAtLvl.DimensionColumns)
                                        {
                                            if (dc.ColumnName.Equals(name))
                                            {
                                                if (!columnMap.ContainsKey(name))
                                                {
                                                    columnMap.Add(name, name);
                                                    typeMap.Add(name, new ExpressionParser.LogicalExpression.DataType(sc.DataType, sc.Width));
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (maf.timeDefinition != null && maf.timeDefinition.Name == dim && maf.dimensionTablesList != null)
            {
                foreach (DimensionTable dt in maf.dimensionTablesList)
                {
                    if ((dt.InheritTable == null || dt.InheritTable.Length == 0) && 
                        dt.DimensionName != null && dt.Level != null && dt.DimensionName == dim && dt.Level == lname)
                    {
                        addToColumnAndTypeMaps(maf, dt, columnMap, typeMap);
                    }
                }
            }
            session["columnMap"] = columnMap;
            session["typeMap"] = typeMap;
        }

        public static void addToColumnAndTypeMaps(MainAdminForm maf, DimensionTable dt, Dictionary<string, string> columnMap, Dictionary<string, ExpressionParser.LogicalExpression.DataType> typeMap)
        {
            DataRowView[] drv = maf.dimensionColumnMappingsTablesView.FindRows(new object[] { dt.TableName });
            foreach (DataRowView dr in drv)
            {
                try
                {
                    string colName = (string)dr["ColumnName"];
                    int width = 20;
                    DataRow dcr = maf.dimensionColumnNamesView.FindRows(new string[] { dt.DimensionName, colName })[0].Row;
                    if ((bool)dcr["Key"])
                        continue;
                    string s = (string)dcr["ColWidth"];
                    if (s.Length > 0)
                        width = int.Parse(s);
                    columnMap.Add(colName, colName);
                    typeMap.Add(colName, new ExpressionParser.LogicalExpression.DataType((string)dcr["DataType"], width));
                }
                catch (Exception)
                {
                }
            }
        }
        public static void setMetricColumnMap(HttpSessionState session, string grain, bool automaticMode)
        {
            MainAdminForm maf = (MainAdminForm)session["MAF"];
            if (maf == null)
                return;
            // Get columns and type maps
            Dictionary<string, string> columnMap = new Dictionary<string, string>();
            Dictionary<string, ExpressionParser.LogicalExpression.DataType> typeMap =
                new Dictionary<string, ExpressionParser.LogicalExpression.DataType>();
            foreach (StagingTable st in maf.stagingTableMod.getStagingTables())
            {
                if (grain == CustomCalculations.getGrainString(st.Levels))
                {
                    MeasureGrain mg = ApplicationBuilder.getMeasureGrain(st, maf);
                    foreach (StagingColumn sc in st.Columns)
                    {
                        if (sc.TargetTypes != null && Array.IndexOf<string>(sc.TargetTypes, "Measure") >= 0)
                        {
                            string name = sc.getLogicalColumnName();
                            if (!columnMap.ContainsKey(name))
                            {
                                columnMap.Add(name, name);
                                typeMap.Add(name, new ExpressionParser.LogicalExpression.DataType(sc.DataType, sc.Width));
                            }
                        }
                    }
                    if (Util.GENERATE_INHERITED_MEASURE_TABLES)
                    {
                        // Get columns that result from combining facts
                        // First, get all staging tables at a higher grain
                        foreach (StagingTable st2 in maf.stagingTableMod.getStagingTables())
                        {
                            if (st == st2)
                                continue;
                            MeasureGrain mg2 = ApplicationBuilder.getMeasureGrain(st2, maf);
                            if (ManageSources.isTargeted(maf, st2) && ApplicationBuilder.isLowerGrain(maf, mg2.measureTableGrains, mg.measureTableGrains, maf.dependencies, automaticMode) != null)
                            {
                                foreach (StagingColumn sc in st2.Columns)
                                {
                                    if (sc.TargetTypes != null && Array.IndexOf<string>(sc.TargetTypes, "Measure") >= 0)
                                    {
                                        string name = sc.getLogicalColumnName();
                                        if (!columnMap.ContainsKey(name))
                                        {
                                            columnMap.Add(name, name);
                                            typeMap.Add(name, new ExpressionParser.LogicalExpression.DataType(sc.DataType, sc.Width));
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    /*
                     * Generate discovery source metrics
                     */
                    if (ApplicationBuilder.getDisplayName(st) == grain)
                    {
                        foreach (StagingColumn sc in st.Columns)
                        {
                            if (sc.TargetTypes != null && Array.IndexOf<string>(sc.TargetTypes, "Measure") >= 0)
                            {
                                string name = sc.Name;
                                if (!columnMap.ContainsKey(name))
                                {
                                    columnMap.Add(name, name);
                                    typeMap.Add(name, new ExpressionParser.LogicalExpression.DataType(sc.DataType, sc.Width));
                                }
                            }
                        }
                        if (st.ForeignKeys != null)
                        {
                            // Traverse up a one-to-many join
                            foreach (ForeignKey fk in st.ForeignKeys)
                            {
                                StagingTable fkst = maf.stagingTableMod.findTable(fk.Source);
                                if (fkst == null)
                                    continue;
                                foreach (StagingColumn sc in fkst.Columns)
                                {
                                    if (sc.TargetTypes != null && Array.IndexOf<string>(sc.TargetTypes, "Measure") >= 0)
                                    {
                                        string name = sc.getLogicalColumnName();
                                        if (!columnMap.ContainsKey(name))
                                        {
                                            columnMap.Add(name, name);
                                            typeMap.Add(name, new ExpressionParser.LogicalExpression.DataType(sc.DataType, sc.Width));
                                        }
                                    }
                                }
                            }
                        }
                        
                    }

                }
            }
            session["columnMap"] = columnMap;
            session["typeMap"] = typeMap;
        }

        public static List<CustomFormulasOutput> getImportedLogicalExpressions(HttpSessionState session)
        {
            MainAdminForm maf = Util.getSessionMAF(session);
            if (maf == null)
                return null;
            Space sp = Util.getSessionSpace(session);
            if (sp == null)
                return null;
            maf.setRepositoryDirectory(sp.Directory);
            List<CustomFormulasOutput> response = new List<CustomFormulasOutput>();
            Dictionary<ImportedSpace, List<LogicalExpression>> importedCustomFormulasMap = PackageUtils.getImportedCustomFormulas(maf, session, true);
            if (importedCustomFormulasMap != null && importedCustomFormulasMap.Count > 0)
            {
                foreach (KeyValuePair<ImportedSpace, List<LogicalExpression>> kvPair in importedCustomFormulasMap)
                {
                    ImportedSpace importedSpace = kvPair.Key;
                    List<LogicalExpression> importedLogicalExpressions = kvPair.Value;
                    MainAdminForm importedMaf = importedSpace.maf;
                    if(importedMaf != null && importedLogicalExpressions != null && importedLogicalExpressions.Count > 0)
                    {
                        Dictionary<string, string[]> levelMap = new Dictionary<string, string[]>();
                        getLevels(importedMaf, out levelMap);
                        List<CustomFormulasOutput> cFormulas = getLogicalExpressions(importedMaf, importedLogicalExpressions.ToArray(), getGrains(importedMaf), levelMap);
                        if (cFormulas != null && cFormulas.Count > 0)
                        {
                            foreach (CustomFormulasOutput cFormula in cFormulas)
                            {
                                cFormula.Imported = true;
                                response.Add(cFormula);    
                            }                            
                        }
                    }                    
                }
            }
            return response;
        }

        public static List<CustomFormulasOutput> getLogicalExpressions(HttpSessionState session)
        {   
            MainAdminForm maf = (MainAdminForm)session["MAF"];
            if (maf == null)
                return null;
            Space sp = (Space)session["space"];
            if (sp == null)
                return null;

            try
            {
                maf.setRepositoryDirectory(sp.Directory);
                List<CustomFormulasOutput> response = new List<CustomFormulasOutput>();
                LogicalExpression[] exlist = maf.getLogicalExpressions();
                if (exlist == null)
                    return null;
                // update the session["grainmap"] and session["levelMap"]
                getGrains(session);
                getLevels(session);
                Dictionary<string, string[][]> grains = (Dictionary<string, string[][]>)session["grainmap"];
                Dictionary<string, string[]> levelMap = (Dictionary<string, string[]>)session["levelMap"];
                return getLogicalExpressions(maf, exlist, grains, levelMap);
            }
            catch (Exception ex)
            {
                Global.systemLog.Info("Exception thrown while getting logicalExpression : " + ex);
                return null;
            }
        }

        public static List<CustomFormulasOutput> getLogicalExpressions(MainAdminForm maf, LogicalExpression[] exlist, 
            Dictionary<string, string[][]> grains, Dictionary<string, string[]> levelMap)
        {
            List<CustomFormulasOutput> response = new List<CustomFormulasOutput>();
            try
            {  
                foreach (LogicalExpression le in exlist)
                {
                    CustomFormulasOutput output = new CustomFormulasOutput();
                    output.LogicalExpression = le;
                    output.Valid = true;
                    response.Add(output);

                    if (le.Levels == null || le.Levels.Length == 0)
                    {
                        output.Valid = false;
                        continue;
                    }
                    if (le.Type == LogicalExpression.TYPE_MEASURE)
                    {   
                        if (grains == null || grains.Count == 0)
                        {
                            continue;
                        }
                        if (le.Levels.Length == 1 && le.Levels[0].Length == 1)
                        {
                            // Discovery source
                            output.Sources = new string[] { le.Levels[0][0] };
                        }
                        else
                        {
                            string grain = getGrainString(le.Levels);
                            if (grain == null || !grains.ContainsKey(grain))
                            {
                                output.Valid = false;
                            }
                            List<string> sourceList = new List<string>();
                            foreach (StagingTable st in maf.stagingTableMod.getStagingTables())
                            {
                                if (st.Levels.Length != le.Levels.Length)
                                    continue;
                                bool match = true;
                                foreach (string[] level in st.Levels)
                                {
                                    bool found = false;
                                    foreach (string[] lelevel in le.Levels)
                                    {
                                        if (lelevel[0] == level[0] && lelevel[1] == level[1])
                                        {
                                            found = true;
                                            break;
                                        }
                                    }
                                    if (!found)
                                    {
                                        match = false;
                                        break;
                                    }
                                }
                                if (match)
                                {
                                    sourceList.Add(ApplicationBuilder.getDisplayName(st));
                                }
                            }
                            output.Sources = sourceList.ToArray();
                        }
                    }
                    else if (le.Type == LogicalExpression.TYPE_DIMENSION)
                    {                        
                        if (levelMap == null || levelMap.Count == 0)
                        {
                            continue;
                        }
                        string level = le.Levels[0][0] + "." + le.Levels[0][1];
                        if (!levelMap.ContainsKey(level))
                        {
                            output.Valid = false;
                        }
                    }
                }
                return response;
            }
            catch (Exception ex)
            {
                Global.systemLog.Info("Exception thrown while getting logicalExpression : " + ex);
                return null;
            }
        }


        private static bool sameGrainOrLevel(LogicalExpression le, LogicalExpression sle, bool useGrain)
        {
            if (sle.Name == le.Name && sle.Type == le.Type)
            {
                // For measures, can have a measure of same name at different grains
                if (le.Type == LogicalExpression.TYPE_MEASURE && useGrain)
                {
                    if (le.Levels.Length == sle.Levels.Length)
                    {
                        bool same = true;
                        foreach (string[] level in le.Levels)
                        {
                            bool found = false;
                            foreach (string[] level2 in sle.Levels)
                            {
                                if (level[0] == level2[0] && level[1] == level2[1])
                                {
                                    found = true;
                                    break;
                                }
                            }
                            if (!found)
                            {
                                same = false;
                                break;
                            }
                        }
                        if (same)
                            return true;
                    }
                    return false;
                }
                else if (le.Type == LogicalExpression.TYPE_DIMENSION)
                {
                    if (le.Levels[0][0] == sle.Levels[0][0] && le.Levels[0][1] == sle.Levels[0][1])
                        return true;
                }
            }
            return false;
        }

        private static void setGrainOrLevel(LogicalExpression le, string grainorlevel, HttpSessionState session)
        {
            if (grainorlevel != null)
            {
                if (le.Type == LogicalExpression.TYPE_MEASURE)
                {
                    Dictionary<string, string[][]> grains = (Dictionary<string, string[][]>)session["grainmap"];
                    if (grains != null && grains.ContainsKey(grainorlevel))
                        le.Levels = grains[grainorlevel];
                    else 
                    {
                        // Discovery source                         
                        le.Levels = new string[1][];
                        le.Levels[0] = new string[] { grainorlevel };
                    }
                }
                else if (le.Type == LogicalExpression.TYPE_DIMENSION)
                {
                    Dictionary<string, string[]> levelMap = (Dictionary<string, string[]>)session["levelMap"];
                    if (levelMap != null && levelMap.ContainsKey(grainorlevel))
                    {
                        le.Levels = new string[1][];
                        le.Levels[0] = levelMap[grainorlevel];
                    }
                }
            }
        }

        private static void updateExpression(HttpSessionState session, LogicalExpression le, string grainorlevel, string label)
        {
            MainAdminForm maf = (MainAdminForm)session["MAF"];
            if (maf == null)
                return;
            Space sp = (Space)session["space"];
            if (sp == null)
                return;
            User u = (User)session["user"];

            maf.setRepositoryDirectory(sp.Directory);

            maf.updateLogicalExpression(le, u == null ? "" : u.Username, label);
            session["RequiresRebuild"] = true;
            session["AdminDirty"] = true;
        }

        public static string updateLogicalExpression(HttpSessionState session, LogicalExpression le, string grainorlevel, string label)
        {
            MainAdminForm maf = (MainAdminForm)session["MAF"];
            if (maf == null)
                return "Invalid session";

            // validate name
            if (!Util.isNameValid(le.Name))
            {
                return "Name " + le.Name + " is invalid.";
            }

            setGrainOrLevel(le, grainorlevel, session);

            // validate that this is not a duplicate logical expression
            LogicalExpression[] expressions = maf.getLogicalExpressions();
            if (expressions != null && le.Guid == null) // le.Guid == null means that is is a CREATE rather than a MODIFY
            {
                foreach (LogicalExpression existingLE in expressions)
                {
                    if (sameGrainOrLevel(le, existingLE, le.Type == LogicalExpression.TYPE_MEASURE))
                    {
                        if (le.Type == LogicalExpression.TYPE_MEASURE)
                            return "Custom Measure with the same name and grain already exists";
                        else
                            return "Custom Attribute with the same name and level already exists";
                    }
                }
            }

            // validate expression
            Dictionary<string, string> columnMap = (Dictionary<string, string>)session["columnMap"];
            Dictionary<string, ExpressionParser.LogicalExpression.DataType> typeMap =
                (Dictionary<string, ExpressionParser.LogicalExpression.DataType>)session["typeMap"];
            ExpressionParser.LogicalExpression lep = new ExpressionParser.LogicalExpression(le.Expression, columnMap, typeMap, ApplicationBuilder.getSqlGenIntType(ApplicationBuilder.getSQLGenType(maf)), (maf.builderVersion >= 9));
            if (lep.HasError)
            {
                return lep.Error;
            }

            // Make sure metric name is unique
            bool dupe = false;
            string[] targetTypes;
            foreach (StagingTable st in maf.stagingTableMod.getStagingTables())
            {
                foreach (StagingColumn sc in st.Columns)
                {
                    targetTypes = sc.TargetTypes;
                    if (sc.Name == le.Name && targetTypes != null && Array.IndexOf<string>(targetTypes, "Measure") >= 0)
                    {
                        dupe = true;
                        break;
                    }
                }
            }
            if (dupe)
            {
                return "Metric name is not unique, please choose another";
            }

			try
			{
				updateExpression(session, le, grainorlevel, label);
			}
			catch (Exception e)
			{
				return e.Message;
			}
            return null;

        }

        public static void deleteLogicalExpression(HttpSessionState session, LogicalExpression le, string grainorlevel)
        {
            MainAdminForm maf = (MainAdminForm)session["MAF"];
            if (maf == null)
                return;
            Space sp = (Space)session["space"];
            if (sp == null)
                return;
            try
            {
                setGrainOrLevel(le, grainorlevel, session);
            }
            catch (Exception ex)
            {
                Global.systemLog.Warn("Exception thrown when finding levels from grain or level map for : "  + grainorlevel + " : " + ex);
            }
            maf.deleteLogicalExpression(le);
            // bugfix 6897
            session["RequiresRebuild"] = true;
            session["AdminDirty"] = true;
        }

        private static void save(MainAdminForm maf, HttpSessionState session)
        {
            Space sp = (Space)session["space"];
            User u = (User)session["user"];
            string schema = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["MainSchema"];
            Util.buildApplication(maf, schema, DateTime.MinValue, -1, null, sp, session);
            Util.saveApplication(maf, sp, session, u);
        }

        public static VirtualColumn[] getBucketedMeasures(HttpSessionState session)
        {
            MainAdminForm maf = (MainAdminForm)session["MAF"];
            if (maf == null)
                return null;
            List<VirtualColumn> vlist = new List<VirtualColumn>();
            foreach (VirtualColumn v in maf.getVirtualColumnList())
            {
                //set measureprefix if it is not set
                if (v.MeasurePrefix == null || v.MeasurePrefix.Length == 0)
                {
                    HashSet<string> matchingPrefixes = new HashSet<string>();
                    foreach (string prefix in maf.aggAvailableMeasures.Keys)
                    {
                        if (v.Measure.StartsWith(prefix) && v.Measure != prefix)
                            matchingPrefixes.Add(prefix);
                    }
                    int longestLength = 0;
                    string chosenPrefix = null;
                    foreach (string prefix in matchingPrefixes)
                    {
                        if (prefix.Length > longestLength)
                        {
                            longestLength = prefix.Length;
                            chosenPrefix = prefix;
                        }
                    }
                    if (longestLength == 0)
                        v.MeasurePrefix = "<blank>";
                    else
                        v.MeasurePrefix = chosenPrefix;
                }
                vlist.Add(v);
            }
            return vlist.ToArray();
        }

        public static String updateBucketedMeasure(HttpSessionState session, VirtualColumn vc)
        {
            MainAdminForm maf = (MainAdminForm)session["MAF"];
            if (maf == null)
                return null;

            // validate name
            if (!Util.isNameValid(vc.Name))
            {
                return "Name " + vc.Name + " is invalid.";
            }

            try
			{
				User u = (User)session["user"];
				maf.updateVirtualColumn(vc, u != null ? u.Username : null);
				session["AdminDirty"] = true;
			}
			catch (Exception e)
			{
				return e.Message;
			}
			return null;
        }

        public static void deleteBucketedMeasure(HttpSessionState session, VirtualColumn vc)
        {
            MainAdminForm maf = (MainAdminForm)session["MAF"];
            if (maf == null)
                return;
            User u = (User)session["user"];
            maf.deleteVirtualColumn(vc);
            session["AdminDirty"] = true;
        }
        public static AggregateOutput[] getAggregates(HttpSessionState session)
        {
            MainAdminForm maf = (MainAdminForm)session["MAF"];
            if (maf == null)
                return null;
            Space sp = (Space)session["space"];

            maf.aggModule.setRepositoryDirectory(sp.Directory);
            List<AggregateOutput> response = new List<AggregateOutput>();
            Aggregate[] aglist = maf.aggModule.getAggregateList();
            if (aglist == null)
                return null;
            foreach (Aggregate ag in aglist)
            {
                AggregateOutput output = new AggregateOutput();
                output.Aggregate = ag;
                output.Valid = true;
                response.Add(output);
            }

            // get imported aggregates
            List<AggregateOutput> importedAggs = PackageUtils.getImportedAggregates(maf, session);
            if (importedAggs != null && importedAggs.Count > 0)
            {
                response.AddRange(importedAggs);
            }
            try
            {
                return response.ToArray();
            }
            catch (Exception ex)
            {
                Global.systemLog.Info("Exception thrown while getting logicalExpression : " + ex);
                return null;
            }
        }

        public static string updateAggregate(HttpSessionState session, Aggregate ag)
        {
            MainAdminForm maf = (MainAdminForm)session["MAF"];
            if (maf == null)
                return "Invalid session";

            try
            {
                User u = (User)session["user"];
                Space sp = (Space)session["space"];

                maf.aggModule.setRepositoryDirectory(sp.Directory);
                maf.aggModule.updateAggregate(ag, u.Username);

                session["AdminDirty"] = true;
            }
            catch (Exception e)
            {
                return e.Message;
            }
            return null;
        }

        public static void deleteAggregate(HttpSessionState session, Aggregate ag)
        {
            MainAdminForm maf = (MainAdminForm)session["MAF"];
            if (maf == null)
                return;
            User u = (User)session["user"];
            Space sp = (Space)session["space"];

            maf.aggModule.setRepositoryDirectory(sp.Directory);
            maf.aggModule.deleteAggregate(ag);

            // bugfix 6897
            session["AdminDirty"] = true;
        }
    }
}
