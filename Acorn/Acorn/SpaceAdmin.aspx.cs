﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.Odbc;
using Performance_Optimizer_Administration;
using System.Net.Mail;
using System.Text;
using System.IO;
using System.Collections.Generic;
using System.Web.SessionState;
using System.Threading;
using Acorn.Background;

namespace Acorn
{
    public partial class SpaceAdmin : System.Web.UI.Page
    {

        void Page_Init(object sender, EventArgs e)
        {
            ViewStateUserKey = Session.SessionID;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            Util.needToChangePassword(Response, Session);
            Util.setUpHeaders(Response);

            User u = Util.validateAuth(Session, Response);
            MainAdminForm maf = (MainAdminForm)Session["MAF"];
            Space sp = Util.validateSpace(Session, Response, Request);
            if (!Util.isSpaceAdmin(u, sp, Session))
            {
                Global.systemLog.Debug("redirecting to home page due to not being the space admin (admin)");
                Response.Redirect(Util.getHomePageURL(Session, Request));
            }
            Util.setBreadCrumbPage(sp, u, Master);
            string getattachment = Request.QueryString["attachment"];
            string getdatafile = Request.QueryString["datafile"];
            string getmetadata = Request.QueryString["metasearch"];
            string getmetadataPath = Request.QueryString["mpath"];
            string getmetadataFilenameFilter = Request.QueryString["fname"];
            string getmetadataDimValue = Request.QueryString["dvalue"];
            string getmetadataExprValue = Request.QueryString["exprvalue"];
            string getmetadataLabelValue = Request.QueryString["labelvalue"];
            string getfile = null;
            if (getattachment != null)
            {
                getfile = sp.Directory + "\\attachments\\" + getattachment;
            }
            else if (getdatafile != null)
            {
                if (!Directory.Exists(sp.Directory+"\\data"))
                    Directory.CreateDirectory(sp.Directory+"\\data");
                if (!Directory.Exists(sp.Directory+"\\data\\export"))
                    Directory.CreateDirectory(sp.Directory+"\\data\\export");
                getfile = sp.Directory + "\\data\\export\\" + getdatafile;
            }
            else if (getmetadata != null)
            {
                if (!Directory.Exists(sp.Directory + "\\data"))
                    Directory.CreateDirectory(sp.Directory + "\\data");
                if (!Directory.Exists(sp.Directory + "\\data\\export"))
                    Directory.CreateDirectory(sp.Directory + "\\data\\export");
                getfile = sp.Directory + "\\data\\export\\msearch.txt";
                SearchCatalog.searchCatalog(Session, getmetadataPath, getmetadataFilenameFilter, getmetadataDimValue, getmetadataExprValue, getmetadataLabelValue, getfile);
            }
            if (getfile != null)
            {
                FileInfo fi = new FileInfo(getfile);
                try
                {
                    if (fi.Exists)
                    {
                        Response.Clear();
                        // change from fi.Name to file. + fi.Extension due to some browsers trimming off everything after the first space
                        Response.AddHeader("Content-Disposition", "attachment; filename=file" + Util.removeCRLF(fi.Extension));
                        Response.AddHeader("Content-Length", fi.Length.ToString());
                        Response.ContentType = "application/octet-stream";
                        Response.TransmitFile(fi.FullName);
                        Response.Flush();
                        Response.End();
                    }
                }
                catch (System.Threading.ThreadAbortException tae)
                {
                    Global.systemLog.Debug(tae.Message);
                }
                catch (Exception ex)
                {
                    Global.systemLog.Error("Problem in sending the requested file " + getfile, ex);
                    // File may still be open being written to
                }
               // return;
            }  
        }        
    }
}
