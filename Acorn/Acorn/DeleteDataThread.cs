﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Web.SessionState;
using System.Data.Odbc;
using System.Diagnostics;
using System.IO;
using Performance_Optimizer_Administration;
using System.Collections.Generic;
using Acorn.Background;
using System.Text;
using Acorn.Utils;
using System.Xml;
using Acorn.DBConnection;
using System.Threading;

namespace Acorn
{
    public class DeleteDataThread: BackgroundModule
    {
        private Space sp;
        private bool deleteAll;
        private string cmd;
        HttpSessionState session;
        string mainSchema;
        bool restoreRepository;
        int loadNumber;
        MainAdminForm maf;
        User user;
        private Dictionary<BackgroundModuleObserver, object> observers;
        private bool running = true;
        private Exception error;

        public DeleteDataThread(Space sp, MainAdminForm maf, bool deleteAll, string cmd, HttpSessionState session, string mainSchema, bool restoreRepository, int loadNumber, User user)
        {
            this.sp = sp;
            this.maf = maf;
            this.deleteAll = deleteAll;
            this.cmd = cmd;
            this.session = session;
            this.mainSchema = mainSchema;
            this.restoreRepository = restoreRepository;
            this.loadNumber = loadNumber;
            this.user = user;
            observers = new Dictionary<BackgroundModuleObserver, object>();
			maf.setRepositoryDirectory(sp.Directory);
            this.error = null;
        }

        public void addObserver(BackgroundModuleObserver bmo, object o)
        {
            observers.Add(bmo, o);
        }

        public void run()
        {
            SpaceOperationStatus spaceOperation = null;
            try
            {                
                spaceOperation = new SpaceOperationStatus(sp, user);
                spaceOperation.logBeginStatus(SpaceOpsUtils.OP_DELETE_DATA);
                string userName = user.Username;
                log4net.ThreadContext.Properties["user"] = userName;
                Global.systemLog.Info("Beginning delete for space: " + sp.ID + " by user: " + userName);
                if (deleteAll)
                {
                    QueryConnection conn = null;
                    try
                    {
                        conn = ConnectionPool.getConnection();
                        Database.removePublishes(conn, mainSchema, sp.ID, -1);
                    }
                    finally
                    {
                        ConnectionPool.releaseConnection(conn);
                    }
                    lock (sp)
                    {
                        sp.LoadNumber = 0;
                    }
                    Global.systemLog.Info("Removed publish history for space: " + sp.ID);
                }
                else
                {
                    lock (sp)
                    {
                        if (loadNumber <= Util.MAX_LOAD_NUMBER && loadNumber > 0)
                        {
                            sp.LoadNumber = loadNumber - 1;
                        }
                        if (restoreRepository && sp.LoadNumber > 0)
                        {
                            DeleteDataService.restoreSavedRepository(session, sp, user, sp.LoadNumber);
                        }
                    }
                }
                if (deleteAll || loadNumber == 1)
                {
                    DeleteDataService.deletePublishedDataAndLogs(sp);
                }
                else
                {
                    string[] partitionConnectionNames = maf.getPartitionConnectionNames("Default Connection");
                    if (maf.IsPartitioned && partitionConnectionNames != null && partitionConnectionNames.Length > 0)
                    {
                        ProcessStartInfo[] psiArray = null;
                        Process[] processes = null;
                        Thread[] processMonitors = null;
                        ProcessStartInfo psi = null;
                        psiArray = new ProcessStartInfo[partitionConnectionNames.Length + 1];
                        int count = 0;
                        string enginecmd = Util.getEngineCommand(sp);
                        foreach (string partitionConn in partitionConnectionNames)
                        {
                            // Create the ProcessInfo object for each Partition Connection
                            StringBuilder partitionDeleteCommands = getPartitionDeleteDataCommands(partitionConn);
                            File.WriteAllText(sp.Directory + "\\delete_" + partitionConn + ".cmd", partitionDeleteCommands.ToString());
                            psi = new ProcessStartInfo(enginecmd);
                            psi.Arguments = DeleteDataService.getDeleteCmdArguments(sp, loadNumber, partitionConn);
                            psi.WindowStyle = ProcessWindowStyle.Hidden;
                            psi.RedirectStandardOutput = true;
                            psi.CreateNoWindow = true;
                            psi.UseShellExecute = false;
                            psi.RedirectStandardInput = false;
                            psi.RedirectStandardError = true;
                            psi.WorkingDirectory = (new FileInfo(cmd)).Directory.FullName;
                            psiArray[count++] = psi;
                        }
                        // Create the ProcessInfo object for Default Connection (uses delete_data.bat)
                        psi = new ProcessStartInfo(cmd);
                        psi.Arguments = DeleteDataService.getDeleteCmdArguments(sp, loadNumber);// sp.Directory + "\\repository_dev.xml" + " " + curLoad + " " + sp.Directory;
                        psi.WindowStyle = ProcessWindowStyle.Hidden;
                        psi.RedirectStandardOutput = true;
                        psi.CreateNoWindow = true;
                        psi.UseShellExecute = false;
                        psi.RedirectStandardInput = false;
                        psi.RedirectStandardError = true;
                        psi.WorkingDirectory = (new FileInfo(cmd)).Directory.FullName;
                        psiArray[count++] = psi;

                        // Start the processes
                        processes = new Process[psiArray.Length];
                        processMonitors = new Thread[psiArray.Length];
                        for (int i = 0; i < psiArray.Length; i++)
                        {
                            Global.systemLog.Debug("Starting: " + psiArray[i].FileName + " " + psiArray[i].Arguments);
                            processes[i] = Process.Start(psiArray[i]);
                            ProcessMonitor pm = new ProcessMonitor();
                            pm.proc = processes[i];
                            pm.processThreadID = "LoadDeleteProcess-" + i;
                            Thread t = new Thread(pm.monitorProcess);
                            processMonitors[i] = t;
                            t.Start();
                        }

                        // Monitor the processes
                        for (int i = 0; i < processMonitors.Length; i++)
                        {
                            processMonitors[i].Join();
                        }

                        int exitCode = 0;
                        for (int i = 0; i < processes.Length; i++)
                        {
                            Global.systemLog.Info("[LoadDeleteProcess-" + i + "] Deleting space (" + sp.ID.ToString() + ") - Exited with code: " + exitCode);
                            if (processes[i].ExitCode != 0)
                            {
                                exitCode = processes[i].ExitCode;
                            }
                        }
                        for (int i = 0; i < processes.Length; i++)
                        {
                            processes[i].Close();
                        }
                    }
                    else
                    {
                        // Create the ProcessInfo object
                        ProcessStartInfo psi = new ProcessStartInfo(cmd);
                        int curLoad = loadNumber;
                        psi.Arguments = DeleteDataService.getDeleteCmdArguments(sp, loadNumber);// sp.Directory + "\\repository_dev.xml" + " " + curLoad + " " + sp.Directory;
                        psi.WindowStyle = ProcessWindowStyle.Hidden;
                        psi.RedirectStandardOutput = true;
                        psi.CreateNoWindow = true;
                        psi.UseShellExecute = false;
                        psi.RedirectStandardInput = false;
                        psi.RedirectStandardError = true;
                        psi.WorkingDirectory = (new FileInfo(cmd)).Directory.FullName;

                        Global.systemLog.Info("Starting delete process for space: " + sp.ID + ":" + cmd + " " + psi.Arguments);
                        // Start the process
                        Process tproc = Process.Start(psi);
                        string line = null;
                        while ((line = tproc.StandardError.ReadLine()) != null)
                        {
                            Global.systemLog.Warn(line);
                        }
                        while ((line = tproc.StandardOutput.ReadLine()) != null)
                        {
                            Global.systemLog.Info(line);
                        }
                        if (tproc.HasExited)
                        {
                            Global.systemLog.Info("Deleting data (" + sp.ID + ") - Exited with code: " + tproc.ExitCode);
                        }
                        else
                        {
                            Global.systemLog.Info("Deleting data (" + sp.ID + ") - Running");
                            tproc.WaitForExit();
                        }
                        tproc.Close();
                    }
                    Global.systemLog.Info("Finished delete process for space: " + sp.ID);                    
                    DeleteDataService.deleteBirstLocal(sp, maf, loadNumber);
                }

                DeleteDataService.cleanPublishTableAndConsolidatedData(session, sp, user, maf, loadNumber);
                Util.removeSessionDeleteLastLoadNumber(session);                
            }
            catch (Exception de)
            {
                Global.systemLog.Error("Delete failed for space: " + sp.ID, de);
                error = de;
            }
            finally
            {   
                if (spaceOperation != null)
                {
                    spaceOperation.logEndStatus(SpaceOpsUtils.OP_DELETE_DATA);
                }                
            }
        }

        private class ProcessMonitor
        {
            public Process proc;
            public string processThreadID;
            public void monitorProcess()
            {
                string line = null;
                while ((line = proc.StandardError.ReadLine()) != null)
                {
                    Global.systemLog.Warn("[" + processThreadID + "]" + line);
                }
                while ((line = proc.StandardOutput.ReadLine()) != null)
                {
                    Global.systemLog.Info("[" + processThreadID + "]" + line);
                }
                if (!proc.HasExited)
                {
                    proc.WaitForExit();
                }
            }
        }

        private StringBuilder getPartitionDeleteDataCommands(string partitionConnection)
        {
            StringBuilder commands = new StringBuilder();
            commands.Append("setpartitionconnection \"" + partitionConnection + "\"\n");
            commands.Append("repository \"" + sp.Directory + "\\repository_dev.xml\"\n");
            commands.Append("resetetlrun " + loadNumber + "\n");
            commands.Append("deletedata " + loadNumber + "\n");
            return commands;
        }
        
        public void kill()
        {
            foreach (KeyValuePair<BackgroundModuleObserver, object> kvp in observers)
            {
                kvp.Key.killed(kvp.Value);
            }
        }

        public void finish()
        {
            running = false;
            foreach (KeyValuePair<BackgroundModuleObserver, object> kvp in observers)
            {
                kvp.Key.finished(kvp.Value);
            }
        }

        #region BackgroundModule Members


		public Status.StatusResult getStatus()
        {
            Status.StatusCode code = running ? Status.StatusCode.DeleteData : Status.StatusCode.Complete;
            if (!running && error != null)
            {
                   code = Status.StatusCode.Failed;
            }
			Status.StatusResult sr = new Status.StatusResult(code);
            return sr;
        }

        #endregion
    }
}
