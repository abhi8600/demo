﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Acorn.TrustedService;
using Performance_Optimizer_Administration;
using System.Web.SessionState;
using System.IO;
using Acorn.Utils;

namespace Acorn
{
    public class ExecuteScripts
    {
        public static int MAX_ROWS_FOR_EXECUTE_SCRIPT = 10000;
        public static int MAX_ROWS_FOR_EXECUTE_SCRIPT_FOR_BIRST_LOCAL = 10000;

        public static WizardResult scriptWizard(HttpSessionState session, string name, ScriptDefinition script)
        {
            WizardResult wr = new WizardResult();
            if (script.InputQuery == null || script.InputQuery.Trim().Length == 0)
                return wr;
            MainAdminForm maf = (MainAdminForm)session["MAF"];
            if (maf == null)
            {
                wr.errorCode = -2;
                wr.errorMessage = "Invalid session";
                return wr;
            }
            Space sp = (Space)session["space"];
            if (sp == null)
            {
                wr.errorCode = -2;
                wr.errorMessage = "Invalid session";
                return wr;
            }
            StagingTable st = null;
            foreach (StagingTable sst in maf.stagingTableMod.getAllStagingTables())
            {
                if (sst.Name == name)
                {
                    st = sst;
                    break;
                }
            }
            if (st != null)
            {
                SpaceConfig sc = Util.getSpaceConfiguration(sp.Type);
                string localprotocol = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["LocalProtocol"];
                TrustedService.TrustedService ts = new TrustedService.TrustedService();
                // 5 Minute timeout
                ts.Timeout = 5 * 60 * 1000;
                ts.Url = localprotocol + sc.LocalURL + "/SMIWeb/services/TrustedService";
                User user = (User)session["user"];
                ManageSources.setOutputString(st, maf);
                script.Output = st.Script.Output;
                string[] importedSpacesInfoArray = Util.getImportedPackageSpacesInfoArray(sp, maf);
                wr = ts.scriptWizard((user == null ? null : user.Username), sp.ID.ToString(), sp.Directory, script.InputQuery, script.Output, script.Script, importedSpacesInfoArray);
            }
            else
            {
                wr.errorCode = -2;
                wr.errorMessage = "Unable to find data source";
                return wr;
            }
            return wr;
        }

        public static WizardResult formatScript(HttpSessionState session, string name, ScriptDefinition script)
        {
            WizardResult wr = new WizardResult();
            if (script.InputQuery == null || script.InputQuery.Trim().Length == 0)
                return wr;
            MainAdminForm maf = (MainAdminForm)session["MAF"];
            if (maf == null)
            {
                wr.errorCode = -2;
                wr.errorMessage = "Invalid session";
                return wr;
            }
            Space sp = (Space)session["space"];
            if (sp == null)
            {
                wr.errorCode = -2;
                wr.errorMessage = "Invalid session";
                return wr;
            }
            StagingTable st = null;
            foreach (StagingTable sst in maf.stagingTableMod.getAllStagingTables())
            {
                if (sst.Name == name)
                {
                    st = sst;
                    break;
                }
            }
            if (st != null)
            {
                SpaceConfig sc = Util.getSpaceConfiguration(sp.Type);
                string localprotocol = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["LocalProtocol"];
                TrustedService.TrustedService ts = new TrustedService.TrustedService();
                // 5 Minute timeout
                ts.Timeout = 5 * 60 * 1000;
                ts.Url = localprotocol + sc.LocalURL + "/SMIWeb/services/TrustedService";
                User user = (User)session["user"];
                script.Output = ManageSources.getOutputString(st, maf);
                string[] importedSpacesInfoArray = Util.getImportedPackageSpacesInfoArray(sp, maf);
                wr = ts.formatScript((user == null ? null : user.Username), sp.ID.ToString(), sp.Directory, script.InputQuery, script.Output, script.Script, importedSpacesInfoArray);
            }
            else
            {
                wr.errorCode = -2;
                wr.errorMessage = "Unable to find data source";
                return wr;
            }
            return wr;
        }

        public static ValidateResult validateScript(HttpSessionState session, string name, ScriptDefinition script, string part)
        {
            ValidateResult vr = new ValidateResult();
            MainAdminForm maf = (MainAdminForm)session["MAF"];
            if (maf == null)
            {
                vr.errorCode = -2;
                vr.errorMessage = "Invalid session";
                return vr;
            }
            Space sp = (Space)session["space"];
            if (sp == null)
            {
                vr.errorCode = -2;
                vr.errorMessage = "Invalid session";
                return vr;
            }
            StagingTable st = null;
            foreach (StagingTable sst in maf.stagingTableMod.getAllStagingTables())
            {
                if (sst.Name == name)
                {
                    st = sst;
                    break;
                }
            }
            if (st != null)
            {
                vr.success = true;
                if (st.Script != null && st.Script.InputQuery != null && st.Script.Script != null &&
                    st.Script.InputQuery == script.InputQuery && st.Script.Script == script.Script && part != "ALL")
                    return vr;
                try
                {
                    
                    // Make sure that user and group information is updated.
                    UserAndGroupUtils.updatedUserGroupMappingFile(sp);

                    SpaceConfig sc = Util.getSpaceConfiguration(sp.Type);
                    string localprotocol = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["LocalProtocol"];
                    TrustedService.TrustedService ts = new TrustedService.TrustedService();
                    // 5 Minute timeout
                    ts.Timeout = 5 * 60 * 1000;
                    ts.Url = localprotocol + sc.LocalURL + "/SMIWeb/services/TrustedService";
                    script.Output = ManageSources.getOutputString(st, maf);
                    User user = (User)session["user"];
                    string[] importedSpacesInfoArray = Util.getImportedPackageSpacesInfoArray(sp, maf);
                    vr = ts.validateScript((user == null ? null : user.Username), sp.ID.ToString(), sp.Directory, script.InputQuery, script.Output, script.Script, importedSpacesInfoArray);
                    // moving the saving of vr.loadGroups into actual saveSourceData(..) operation                    
                }
                catch (System.Exception e)
                {
                    vr.errorCode = -2;
                    vr.errorMessage = e.Message;
                    return vr;
                }
            }
            else
            {
                vr.errorCode = -2;
                vr.errorMessage = "Unable to find data source";
                return vr;
            }
            return vr;
        }

        public static ExecuteResult executeScript(HttpSessionState session, string name, ScriptDefinition scriptDefinition)
        {
            MainAdminForm maf = (MainAdminForm)session["MAF"];
            ExecuteResult er = new ExecuteResult();
            if (maf == null)
            {
                er.errorCode = -2;
                er.errorMessage = "Invalid session";
                return er;
            }
            Space sp = (Space)session["space"];
            if (sp == null)
            {
                er.errorCode = -2;
                er.errorMessage = "Invalid session";
                return er;
            }
            StagingTable st = null;
            foreach (StagingTable sst in maf.stagingTableMod.getAllStagingTables())
            {
                if (sst.Name == name)
                {
                    st = sst;
                    break;
                }
            }
            if (st == null)
            {
                er.errorCode = -2;
                er.errorMessage = "Unable to find data source";
                return er;
            }
            Util.saveDirty(session);
            try
            {
                // Make sure that user and group information is updated.
                UserAndGroupUtils.updatedUserGroupMappingFile(sp);

                string liveAccessLoadGroup = null;
                foreach (string s in st.LoadGroups)
                {
                    if (s.StartsWith(LiveAccessUtil.LIVE_ACCESS_LOAD_GROUP_PREFIX))
                    {
                        liveAccessLoadGroup = s;
                        break;
                    }
                }
                SourceFile sf = maf.sourceFileMod.getSourceFile(st.SourceFile);
                string scriptInputQuery = scriptDefinition.InputQuery;
                string scriptOutput = ManageSources.getOutputString(st, maf);
                string script = scriptDefinition.Script;                    
                if (liveAccessLoadGroup == null)
                {
                    User user = (User)session["user"];
                    SpaceConfig sc = Util.getSpaceConfiguration(sp.Type);
                    string localprotocol = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["LocalProtocol"];
                    TrustedService.TrustedService ts = new TrustedService.TrustedService();
                    // 5 Minute timeout
                    ts.Timeout = 5 * 60 * 1000;
                    ts.Url = localprotocol + sc.LocalURL + "/SMIWeb/services/TrustedService";
                    string[] importedSpacesInfoArray = Util.getImportedPackageSpacesInfoArray(sp, maf);
                    string awsAccessKeyId = sp.awsAccessKeyId;
                    string awsSecretKey = sp.awsSecretKey;
                    er = ts.executeScript((user == null ? null : user.Username), sp.ID.ToString(), sp.LoadNumber, true, sp.Directory, sp.DatabaseLoadDir + "\\data", st.LoadGroups == null || st.LoadGroups.Length == 0 ? Util.LOAD_GROUP_NAME : st.LoadGroups[0],
                        scriptInputQuery, scriptOutput, script, MAX_ROWS_FOR_EXECUTE_SCRIPT, true, awsAccessKeyId, awsSecretKey, importedSpacesInfoArray);
                }
                else
                {
                    //execute script on local connection
                    Dictionary<string, LocalETLConfig> localETLConfigs = BirstConnectUtil.getLocalETLConfigsForSpace(sp);
                    LocalETLConfig leConfig = null;
                    if (localETLConfigs != null && localETLConfigs.ContainsKey(liveAccessLoadGroup))
                    {
                        leConfig = localETLConfigs[liveAccessLoadGroup];
                    }
                    if (leConfig == null)
                    {
                        throw new Exception("Could not find local ETL configuration for load group: " + liveAccessLoadGroup);
                    }
                    string liveAccessConnectionName = leConfig.getLocalETLConnection();
                    DatabaseConnection liveAccessDc = null;
                    foreach (DatabaseConnection dc in maf.connection.connectionList)
                    {
                        if (dc.Name != null && dc.Name == liveAccessConnectionName)
                        {
                            liveAccessDc = dc;
                            break;
                        }
                    }
                    if (liveAccessDc == null)
                    {
                        throw new Exception("Could not find live access connection with name: " + liveAccessConnectionName);
                    }
                    System.Text.StringBuilder overrideProperties = BirstConnectUtil.getLiveAccessOverrideProperties(sp, maf, leConfig.getApplicationDirectory(), leConfig.getLocalETLConnection(), false);
                    File.WriteAllText(sp.Directory + "\\customer." + leConfig.getLocalETLLoadGroup() + ".properties", overrideProperties.ToString());
                    er = LiveAccessUtil.executeScriptOnLiveAccessConnection(sp, liveAccessConnectionName, sp.LoadNumber, scriptInputQuery,
                            scriptOutput, script, MAX_ROWS_FOR_EXECUTE_SCRIPT_FOR_BIRST_LOCAL);
                    if (er != null && er.success && er.errorCode == 0)
                    {
                        //get sourcefile generated in local application directory
                        LiveAccessUtil.getSourceFileFromLiveAccessConnection(sp, liveAccessConnectionName, sf.FileName);
                    }
                }

                if (sf != null && File.Exists(sp.Directory + "\\data\\" + sf.FileName))
                {
                    sf.LastUploadDate = File.GetLastWriteTime(sp.Directory + "\\data\\" + sf.FileName);
                }
            }
            catch (System.Exception e)
            {
                Global.systemLog.Error("Exception occured during script execution ", e);
                er.errorCode = -2;
                er.errorMessage = e.Message;
            }
            return er;
        }

        public static string getScriptString(ScriptDefinition sd)
        {
            String script = "INPUT: " + sd.InputQuery + "\nOUTPUT: " + sd.Output + "\nBEGIN " + sd.Script + " END";
            return script;
        }
    }
}
