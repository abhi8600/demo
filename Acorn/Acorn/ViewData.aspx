﻿<%@ Page Language="C#" MasterPageFile="~/WebAdmin.Master" AutoEventWireup="true"
    CodeBehind="ViewData.aspx.cs" Inherits="Acorn.ViewData" Title="Birst" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="ContentID" ContentPlaceHolderID="mainPlaceholder" runat="server">
    <div style="background-image: url(Images/nback.gif); height: 15px">
        &nbsp;
    </div>
    <div class="pagepadding">
        <asp:Panel ID="ShowTables" runat="server">
            <div style="font-size: 9pt">
                <a href="AddSalesforce.aspx">Back</a></div>
            <div style="height: 10px">
                &nbsp;</div>
            <div class="pageheader">
                Processed Hierarchy Data
            </div>
            <div style="height: 10px">
                &nbsp;</div>
            <div style="border: 1px solid #CCCCCC; width: 900px;">
                <asp:GridView ID="HierarchyTablesView" runat="server" AutoGenerateColumns="False"
                    OnRowCommand="LevelCommand" BorderWidth="0px" Width="100%">
                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" CssClass="listviewcell" />
                    <Columns>
                        <asp:ButtonField DataTextField="Level" HeaderText="Hierarchy Level" HeaderStyle-Width="400px"
                            HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" CommandName="LevelClick">
                        </asp:ButtonField>
                        <asp:ButtonField Text="[Profile]" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                            CommandName="LevelProfile"></asp:ButtonField>
                    </Columns>
                    <HeaderStyle Font-Bold="True" ForeColor="Black" CssClass="listviewheader" />
                    <AlternatingRowStyle BackColor="White" ForeColor="#555555" />
                    <EmptyDataTemplate>
                        <div style="padding-left: 10px">
                            No levels processed</div>
                    </EmptyDataTemplate>
                </asp:GridView>
            </div>
            <div style="height: 20px">
                &nbsp;
            </div>
            <div class="pageheader">
                Processed Measure Data
            </div>
            <div style="height: 10px">
                &nbsp;</div>
            <div style="border: 1px solid #CCCCCC; width: 900px;">
                <asp:GridView ID="MetricTablesView" runat="server" AutoGenerateColumns="False" BorderWidth="0px"
                    Width="100%" OnRowCommand="MeasureCommand">
                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" CssClass="listviewcell" />
                    <Columns>
                        <asp:ButtonField DataTextField="Grain" HeaderText="Measure Grain" HeaderStyle-Width="400px"
                            HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" CommandName="MeasureTableClick">
                        </asp:ButtonField>
                        <asp:ButtonField Text="[Profile]" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                            CommandName="MeasureTableProfile"></asp:ButtonField>
                    </Columns>
                    <HeaderStyle Font-Bold="True" ForeColor="Black" CssClass="listviewheader" />
                    <AlternatingRowStyle BackColor="White" ForeColor="#555555" />
                    <EmptyDataTemplate>
                        <div style="padding-left: 10px">
                            No measures processed</div>
                    </EmptyDataTemplate>
                </asp:GridView>
            </div>
            <div style="height: 20px">
                &nbsp;
            </div>
            <div class="pageheader">
                Staged Data
            </div>
            <div style="height: 10px">
                &nbsp;</div>
            <div style="border: 1px solid #CCCCCC; width: 900px;">
                <asp:GridView ID="StagingView" runat="server" AutoGenerateColumns="False" BorderWidth="0px"
                    Width="100%" OnRowCommand="StagingCommand">
                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" CssClass="listviewcell" />
                    <Columns>
                        <asp:ButtonField DataTextField="DataSource" HeaderText="Data Source" HeaderStyle-Width="400px"
                            HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" CommandName="DataSourceClick">
                        </asp:ButtonField>
                        <asp:ButtonField Text="[Profile]" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                            CommandName="DataSourceProfile"></asp:ButtonField>
                    </Columns>
                    <HeaderStyle Font-Bold="True" ForeColor="Black" CssClass="listviewheader" />
                    <AlternatingRowStyle BackColor="White" ForeColor="#555555" />
                    <EmptyDataTemplate>
                        <div style="padding-left: 10px">
                            No measures processed</div>
                    </EmptyDataTemplate>
                </asp:GridView>
            </div>
        </asp:Panel>
        <asp:Panel ID="ShowData" runat="server" Visible="false">
            <div class="pageheader">
                Data:&nbsp;<asp:Label ID="NameLabel" runat="server"></asp:Label>
            </div>
            <span style="font-size: 9pt"><a href="ViewData.aspx">Back</a></span>
            <div style="height: 10px">
                &nbsp;</div>
            <asp:PlaceHolder ID="LoadIDPlaceHolder" runat="server" Visible="false">Load Number:&nbsp;<asp:DropDownList
                ID="LoadList" runat="server" AutoPostBack="true" OnSelectedIndexChanged="LoadListChanged">
            </asp:DropDownList>
             <div style="height: 5px">
                &nbsp;</div>
           </asp:PlaceHolder>
            <asp:GridView ID="DataView" runat="server" AutoGenerateColumns="False" BorderColor="#555555"
                BorderStyle="Solid" BorderWidth="1px" Font-Size="9pt" AllowSorting="true" OnSorting="DataSort">
                <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                <HeaderStyle Font-Bold="True" ForeColor="Black" CssClass="listviewborderheader" Wrap="false" />
                <AlternatingRowStyle BackColor="White" ForeColor="#555555" />
                <EmptyDataTemplate>
                    <div style="padding-left: 10px">
                        No rows</div>
                </EmptyDataTemplate>
            </asp:GridView>
        </asp:Panel>
    </div>
</asp:Content>
