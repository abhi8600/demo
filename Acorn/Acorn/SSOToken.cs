﻿using System;

namespace Acorn
{
    public class SSOToken
    {
        public Guid tokenID;
        public Guid userID;
        public string hostaddr;
        public Guid spaceID;
        public DateTime endOfLife;
        public string sessionVariables;
        public bool repAdmin;
        public string type; // SSO, AppExchange, Login, Delegated Authentication
        public int timeout; // SAML specific timeout
        public string logoutPage; // SAML configured logouturl
    }
}