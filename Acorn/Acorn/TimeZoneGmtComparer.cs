﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace Acorn
{
    public class TimeZoneGmtComparer : IComparer<TimeZoneInfo>
    {
        public int Compare(TimeZoneInfo a, TimeZoneInfo b)
        {
            TimeZoneInfo tz1 = (TimeZoneInfo)a;
            TimeZoneInfo tz2 = (TimeZoneInfo)b;
            if (tz1.BaseUtcOffset > tz2.BaseUtcOffset)
            {
                return 1;
            }
            if (tz1.BaseUtcOffset < tz2.BaseUtcOffset)
            {
                return -1;
            }
            return 0;
        }
    }
}
