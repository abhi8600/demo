﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Odbc;
using Acorn.DBConnection;

namespace Acorn
{
    public class ACLDetails
    {
        
        public static string ACL_DASHBOARD = "Dashboard";
        public static string ACL_NEW_DASHBOARD = "NewDashboard";
        public static string ACL_EDIT_DASHBOARD = "EditDashboard";
        public static string ACL_ADHOC = "Adhoc";
        public static string ACL_ENABLE_DOWNLOAD = "EnableDownload";
        public static string ACL_ADVANCED_FILTER = "AdvancedFilter";
        public static string ACL_MAINTAIN_CATALOG = "MaintainCatalog";
        public static string ACL_PREMIUM_ADHOC = "PremiumAdHoc";
        public static string ACL_DASHBOARD_DELETE = "DashboardDelete";
        public static string ACL_MANAGE = "Manage";
        public static string ACL_HOME = "Home";
        public static string ACL_ACCOUNT = "Account";
        public static string ACL_CREATE_NEW_SPACE = "CreateNewSpace";
        public static string ACL_BROWSE_TEMPLATE = "BrowseTemplate";
        public static string ACL_CUSTOM_UPLOAD_ALLOWED = "CustomUploadAllowed";
        public static string ACL_PROMPT_GROUP_ALLOWED = "PromptGroupAllowed";      
        public static string ACL_DEMO_MODE = "DemoMode";
        public static string ACL_ADVANCED_ADMIN = "AdvancedAdmin";
        public static string ACL_SELF_SCHEDULE_ALLOWED = "SelfScheduleAllowed";
        public static string ACL_MODIFY_SAVED_EXPRESSIONS_ALLOWED = "ModifySavedExpressionsAllowed";
        public static string DISABLE_DESIGNER_NEW_AND_OPEN_OPERATIONS = "DisableDesignerNewAndOpenOperations";
		public static string MAKE_SHARED_FOLDER_THE_ROOT_FOR_FILE_OPERATIONS = "MakeSharedFolderTheRootForFileOperations";
        public static string DISABLE_PIVOT_CONTROL_IN_DASHLETS = "DisablePivotControlInDashlets";
        public static string DISABLE_LAYOUT_IN_DESIGNER = "DisableLayoutInDesigner";
        public static string ACL_EDIT_REPORT_CATALOG = "EditReportCatalog";
        // This is only used for Trial Users. Some shared spaces shown to the trial user only needed to have Visualizer access
        // and not even Dashboards. Because Dashboards are by default available for all shared spaces (no ACL controls it), we 
        // have somewhat of negative ACL only for Trial User. This ACL means that ONLY visualizer will be shown and nothing else
        public static string ACL_VIZUALIZER_ONLY = "EnableVisualizer";
        public static string ACL_VISUALIZER_REGULAR = "Visualizer";

        // container for ACLs
        public static Dictionary<int, string> ACLDictionary = new Dictionary<int, string>();

        private static string mainSchema = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["MainSchema"];

        public static string getACLTag(int id)
        {
            string tag = null;
            Dictionary<int, string> acls = getGlobalACLs();
            if (acls != null && acls.Count > 0 && acls.ContainsKey(id))
            {
                tag = acls[id];
            }
            return tag;
        }

        

        public static int getACLID(string name)
        {
            int aclID = -1;
            Dictionary<int, string> acls = getGlobalACLs();
            if (acls != null && acls.Count > 0)
            {
                foreach (KeyValuePair<int, string> kvPair in acls)
                {
                    int key = kvPair.Key;
                    string value = kvPair.Value;
                    if (name == value)
                    {
                        aclID = key;
                        break;
                    }
                }
            }
            return aclID;
        }

        public static Dictionary<int, string> getGlobalACLs()
        {
            if (ACLDictionary == null || (ACLDictionary != null && ACLDictionary.Count == 0))
            {
                QueryConnection conn = null;
                try
                {
                    // get it from database and populate it
                    conn = ConnectionPool.getConnection();
                    ACLDictionary = Database.getACLs(conn, mainSchema);
                }
                catch (Exception ex)
                {
                    Global.systemLog.Error("Error while populating ACL tags", ex);
                }
                finally
                {
                    ConnectionPool.releaseConnection(conn);
                }
            }

            return ACLDictionary;
        }

    }


}
