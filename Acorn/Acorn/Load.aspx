<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Load.aspx.cs" Inherits="Acorn.Load"
    MasterPageFile="~/WebAdmin.Master" %>

<%@ Register TagPrefix="lc" TagName="LoadControl" Src="~/LoadControl.ascx" %>
<asp:Content ID="mainContent" ContentPlaceHolderID="mainPlaceholder" runat="server">
    <div style="background-image: url(Images/nback.gif); height: 15px">
        &nbsp;
    </div>
    <div class="pagepadding">
        <div class="pageheader">
            Process Data
        </div>
        <div style="padding-right: 10px">
        <lc:LoadControl id="Loader" runat="server" CreateDashboards="true"></lc:LoadControl>
        </div>
    </div>
</asp:Content>
