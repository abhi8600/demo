﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Performance_Optimizer_Administration;
using System.Collections.Generic;
using System.Text;

namespace Acorn
{
    public class AdminDataSource
    {
        public static int STATUS_OK = 0;
        public static int STATUS_WARNING = 1;
        public static int STATUS_ERROR = 2;

        public string Name;
        public string DisplayName;
        public int Status;
        public string StatusMessage;
        public bool Enabled;
        public DateTime LastUploadDate;
        public int NumColumns;
        public int NumRows;
        public long FileSize;
        public bool FileExists;
        public bool ReadOnly;
        public bool Transactional;
        public bool TruncateOnLoad;
        public bool LockFormat;
        public bool SchemaLock;
        public bool AllowAddColumns;
        public bool AllowNullBindingRemovedColumns;
        public bool AllowUpcast;
        public bool AllowVarcharExpansion;
        public bool AllowValueTruncationOnLoad;
        public bool FailUploadOnVarcharExpansion;
        public bool CustomUpload;
        public string[][] Levels;
        public StagingColumn[] StagingColumns;
        public string[] SourceFileColumnTypes;
        public bool[] SourceFileColumnPreventUpdate;
        public string[] SourceFileColumnFormat;
        public ScriptDefinition Script;
        public string CustomTimeColumn;
        public string CustomTimePrefix;
        public CustomTimeShift[] CustomTimeShifts;
        public string[] SubGroups;
        public string SourceGroups;
        public string[] LoadGroups;
        public SnapshotPolicy Snapshots;
        public bool IncrementalSnapshotFact;
        public string[] SnapshotDeleteKeys;
        public OverrideLevelKey[] OverrideLevelKeys;
        public string NewColumnTarget;
        public string InputTimeZone;
        public string[] SourceKey;
        public ForeignKey[] ForeignKeys;
        public ForeignKey[] FactForeignKeys;
        public string ParentForeignKeySource;
        public string HierarchyName;
        public string LevelName;
        public bool ExcludeFromModel;
        public bool OnlyQuoteAfterSeparator;
        public bool AllowBirstLocalForSource;
        public bool UseBirstLocalForSource;
        public string LocalConnForSource;
        public SourceLocation[] Locations;
        public string Encoding;
        public string Separator;
        public bool ReplaceWithNull; //(we do this for bad integers, floats etc.)
        public bool SkipRecord; //(we do this for incorrect date/datetime parsing)
        public bool DiscoveryTable;
        public bool ImportedTable;
        public bool LiveAccess;
        public bool UnCached;
        public TableSource TableSource;
        public InheritTable[] InheritTables;
        public string RServerPath;
        public string RExpression;
        public string RServerSettings;

        public void addForeignKeySources(ForeignKey[] keys)
        {
            if (ForeignKeys == null)
            {
                ForeignKeys = keys;
            }
            else if (keys != null)
            {
                List<ForeignKey> newfksources = new List<ForeignKey>(ForeignKeys);
                newfksources.AddRange(keys);
                ForeignKeys = newfksources.ToArray();
            }
        }

        public string getSerializedString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("name=" + Name);
            sb.Append("|0dname=" + DisplayName);
            sb.Append("|0status=" + Status);
            if (StatusMessage != null)
                sb.Append("|0statusm=" + StatusMessage);
            if (Enabled)
                sb.Append("|0enabled=" + Enabled);
            sb.Append("|0lastupload=" + LastUploadDate.ToString("MM/dd/yyyy"));
            sb.Append("|0numcols=" + NumColumns);
            sb.Append("|0numrows=" + NumRows);
            sb.Append("|0fsize=" + FileSize);
            if (FileExists)
                sb.Append("|0fexists=true");
            if (ReadOnly)
                sb.Append("|0ro=true");
            if (Transactional)
                sb.Append("|0trans=true");
            if (TruncateOnLoad)
                sb.Append("|0trunc=true");
            if (LockFormat)
                sb.Append("|0lockf=true");
            if (SchemaLock)
                sb.Append("|0slock=true");
            if (AllowAddColumns)
                sb.Append("|0allowaddc=true");
            if (AllowNullBindingRemovedColumns)
                sb.Append("|0allownullbinding=true");
            if (AllowUpcast)
                sb.Append("|0allowup=true");
            if (AllowVarcharExpansion)
                sb.Append("|0allowvce=true");
            if (AllowValueTruncationOnLoad)
                sb.Append("|0allowvaltrunc=true");
            if (FailUploadOnVarcharExpansion)
                sb.Append("|0failuponvce=true");
            if (CustomUpload)
                sb.Append("|0customup=true");
            if (Levels != null)
                sb.Append("|0levels=" + Util.getSerializedString(Levels));
            if (StagingColumns != null)
                sb.Append("|0cols=" + getSerializedColumns());
            if (SourceFileColumnTypes != null)
                sb.Append("|0sfctypes=" + Util.getSerializedString(SourceFileColumnTypes));
            if (SourceFileColumnPreventUpdate != null)
                sb.Append("|0sfcprevupdates=" + Util.getSerializedString(SourceFileColumnPreventUpdate));
            if (SourceFileColumnFormat != null)
                sb.Append("|0sfcformat=" + Util.getSerializedString(SourceFileColumnFormat));
            if (Script != null)
                sb.Append("|0sd=" + Script.getSerializedString());
            if (CustomTimeColumn != null)
                sb.Append("|0ctc=" + CustomTimeColumn);
            if (CustomTimePrefix != null)
                sb.Append("|0ctp=" + CustomTimePrefix);
            if (CustomTimeShifts != null)
                sb.Append("|0cts=" + getSerializedShifts());
            if (SubGroups != null)
                sb.Append("|0subgroups=" + Util.getSerializedString(SubGroups));
            if (SourceGroups != null)
                sb.Append("|0sourcegroups=" + SourceGroups);
            if (LoadGroups != null)
                sb.Append("|0loadgroups=" + Util.getSerializedString(LoadGroups));
            if (Snapshots != null)
                sb.Append("|0snapshots=" + Snapshots.getSerializedString());
            if (IncrementalSnapshotFact)
                sb.Append("|0isf=true");
            if (SnapshotDeleteKeys != null)
                sb.Append("|0sdkeys="+Util.getSerializedString(SnapshotDeleteKeys));
            if (OverrideLevelKeys != null)
                sb.Append("|0olkeys=" + getSerializedLevelKeyOverrides());
            if (NewColumnTarget != null)
                sb.Append("|0nct=" + NewColumnTarget);
            if (InputTimeZone != null)
                sb.Append("|0itz=" + InputTimeZone);
            if (SourceKey != null)
                sb.Append("|0skey=" + Util.getSerializedString(SourceKey));
            if (ForeignKeys != null)
                sb.Append("|0fkeys=" + getSerializedForeignKeys(ForeignKeys));
            if (FactForeignKeys != null)
                sb.Append("|0factfkeys=" + getSerializedForeignKeys(FactForeignKeys));
            if (ParentForeignKeySource != null)
                sb.Append("|0pfks=" + ParentForeignKeySource);
            if (HierarchyName != null)
                sb.Append("|0hn=" + HierarchyName);
            if (LevelName != null)
                sb.Append("|0ln=" + LevelName);
            if (ExcludeFromModel)
                sb.Append("|0efm=true");
            if (OnlyQuoteAfterSeparator)
                sb.Append("|0oqas=true");
            if (AllowBirstLocalForSource)
                sb.Append("|0ablfs=true");
            if (UseBirstLocalForSource)
                sb.Append("|0ublfs=true");
            if (LocalConnForSource != null)
                sb.Append("|0lcfs=" + LocalConnForSource);
            if (Locations != null)
                sb.Append("|0locations=" + getSourceLocations());
            if (Encoding != null)
                sb.Append("|0enc=" + Encoding);
            if (Separator != null)
                sb.Append("|0sep=" + Separator);
            if (ReplaceWithNull)
                sb.Append("|0rwn=true");
            if (SkipRecord)
                sb.Append("|0sr=true");
            if (DiscoveryTable)
                sb.Append("|0dt=true");
            if (ImportedTable)
                sb.Append("|0it=true");
            if (LiveAccess)
                sb.Append("|0la=true");
            if (UnCached)
                sb.Append("|0uc=true");
            if (TableSource != null)
                sb.Append("|0ts=" + TableSource.toSerializedString());
            if (InheritTables != null)
                sb.Append("|0itables=" + getInheritedTables());
            if (RServerPath != null)
                sb.Append("|0rspath=" + RServerPath);
            if (RExpression != null)
                sb.Append("|0rexp=" + RExpression);
            if (RServerSettings != null)
                sb.Append("|0rset=" + RServerSettings);
            return sb.ToString();
        }

        private string getSerializedColumns()
        {
            StringBuilder sb = new StringBuilder();
            foreach (StagingColumn sc in StagingColumns)
            {
                if (sb.Length > 0)
                    sb.Append("|5");
                sb.Append(sc.getSerializedString());
            }
            return sb.ToString();
        }

        private string getSerializedShifts()
        {
            StringBuilder sb = new StringBuilder();
            foreach (CustomTimeShift cts in CustomTimeShifts)
            {
                if (sb.Length > 0)
                    sb.Append("|5");
                sb.Append(cts.getSerializedString());
            }
            return sb.ToString();
        }

        private string getSerializedLevelKeyOverrides()
        {
            StringBuilder sb = new StringBuilder();
            foreach (OverrideLevelKey olk in OverrideLevelKeys)
            {
                if (sb.Length > 0)
                    sb.Append("|5");
                sb.Append(olk.getSerializedString());
            }
            return sb.ToString();
        }

        private string getSerializedForeignKeys(ForeignKey[] fkeys)
        {
            StringBuilder sb = new StringBuilder();
            foreach (ForeignKey fk in fkeys)
            {
                if (sb.Length > 0)
                    sb.Append("|5");
                sb.Append(fk.toSerializedString());
            }
            return sb.ToString();
        }

        private string getSourceLocations()
        {
            StringBuilder sb = new StringBuilder();
            foreach (SourceLocation sl in Locations)
            {
                if (sb.Length > 0)
                    sb.Append("|5");
                sb.Append(sl.toSerializedString());
            }
            return sb.ToString();
        }

        private string getInheritedTables()
        {
            StringBuilder sb = new StringBuilder();
            foreach (InheritTable it in InheritTables)
            {
                if (sb.Length > 0)
                    sb.Append("|5");
                sb.Append(it.toSerializedString());
            }
            return sb.ToString();
        }
    }
}
