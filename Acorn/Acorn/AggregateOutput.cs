﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Performance_Optimizer_Administration;

namespace Acorn
{
    public class AggregateOutput
    {
        public Aggregate Aggregate;
        public bool Valid;
        public bool Imported;

        public AggregateOutput(){}
        public AggregateOutput(Aggregate aggregate, bool valid, bool imported)
        {
            this.Aggregate = aggregate;
            this.Valid = valid;
            this.Imported = imported;
        }
    }
}
