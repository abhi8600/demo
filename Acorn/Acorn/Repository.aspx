﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Repository.aspx.cs" Inherits="Acorn.Repository" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Repository Administration</title>
    <script type="text/javascript" src="KeepAliveSMIWeb.js"></script>
</head>
<body style="font-family: Arial">
    <form id="mainform" runat="server">
    <div>
        <span style="font-weight: bold; font-size: larger">Space:&nbsp;
            <asp:Label ID="SpaceLabel" runat="server" Text=""></asp:Label></span>
    </div>
    <div>    
        <span style="font-size: 9pt">ID:&nbsp;
            <asp:Label ID="SpaceID" runat="server" Text=""></asp:Label></span>
    </div>            
    <div>            
        <span style="font-size: 9pt">Schema:&nbsp;
            <asp:Label ID="Schema" runat="server" Text=""></asp:Label></span>
    </div>
    <div>            
        <span style="font-size: 9pt">Directory:&nbsp;
            <asp:Label ID="DirectoryID" runat="server" Text=""></asp:Label></span>
    </div>
    <div style="height: 10px">
        &nbsp;</div>    
    <div>
        <span style="font-size: 9pt">Web Server:&nbsp;
            <asp:Label ID="WebServerLabel" runat="server" Text=""></asp:Label></span>
    </div> 
    <div>
        <span style="font-size: 9pt">Current User:&nbsp;
            <asp:Label ID="CurrentUser" runat="server" Text=""></asp:Label></span>
    </div>       
    <div>
        <span style="font-size: 9pt">Birst Version:&nbsp;
            <asp:Label ID="Version" runat="server" Text=""></asp:Label></span>
    </div>     
    <div style="height: 30px">
        &nbsp;</div>  
    <div>
            <asp:GridView ID="Products" runat="server" AutoGenerateColumns="true" CellPadding="5"
                ShowHeader="true" GridLines="None">
            </asp:GridView>  
    </div>
       <div style="height: 30px">
        &nbsp;</div>                    
    <span style="font-weight: bold">Repository</span>
    <div style="font-size: smaller">
        Last Updated:
        <asp:Label ID="DateLabel" runat="server" Text=""></asp:Label>
    </div>
    <div style="padding: 10px; border: 1px solid gray">
        <div>
            <asp:LinkButton ID="GetRepository" runat="server" OnClick="getRepository">Download Repository (dev)</asp:LinkButton>
        </div>
        <div>
            <asp:LinkButton ID="GetRepositoryProd" runat="server" OnClick="getRepositoryProd">Download Repository (prod)</asp:LinkButton>
        </div> 
        <asp:Panel ID="DCconfigPanel" runat="server"/>
        <div>
            <asp:LinkButton ID="GetSForceConfig" runat="server" OnClick="getSForceConfig">Download sforce.xml (SFDC Settings)</asp:LinkButton>
        </div> 
        <asp:Panel ID="ConnectorConfigsDownloadPanel" runat="server" />
        <div>
            <asp:LinkButton ID="GetUsersAndGroups" runat="server" OnClick="getUsersAndGroups">Download UsersAndGroups.xml</asp:LinkButton>
        </div>
        <div>
            <asp:LinkButton ID="GetDrillMaps" runat="server" OnClick="getDrillMaps">Download DrillMaps.xml</asp:LinkButton>
        </div>
        <div>
            <asp:LinkButton ID="GetCustomSubjectAreas" runat="server" OnClick="getCustomSubjectAreas">Download the Custom Subject area files</asp:LinkButton>
        </div>
        <div>
            <asp:LinkButton ID="GetKmlFiles" runat="server" OnClick="getKmlFiles">Download the Kml files</asp:LinkButton>
        </div>
        <div>
            <asp:LinkButton ID="GetSFDCMappingFiles" runat="server" OnClick="getSFDCMappingFiles">Download the SFDC mapping files</asp:LinkButton>
        </div>
        <div>
            <asp:LinkButton ID="GetSavedExpressions" runat="server" OnClick="getSavedExpressions">Download SavedExpressions.xml</asp:LinkButton>
        </div> 
        <div>
            <asp:LinkButton ID="GetSpaceSettings" runat="server" OnClick="getSpaceSettings">Download spacesettings.xml (foreground/background color)</asp:LinkButton>
        </div> 
        <div>
            <asp:LinkButton ID="GetDashboardStyleSettings" runat="server" OnClick="getDashboardStyleSettings">Download DashboardStyleSettings.xml</asp:LinkButton>
        </div>
        <div>
            <asp:LinkButton ID="GetCustomGeoMaps" runat="server" OnClick="getCustomGeoMaps">Download CustomGeoMaps.xml</asp:LinkButton>
        </div>  
        <div>
            <asp:LinkButton ID="GetPageSequence" runat="server" OnClick="getPageSequence">Download pageSequence.txt (for Kiosk mode)</asp:LinkButton>
        </div>                                                                      
        <div>
            <asp:LinkButton ID="GetSFDCQueryInfo" runat="server" OnClick="getSFDCQueryInfo">Download Last Run SFDC Query Info</asp:LinkButton>
        </div>
        <div>
            <asp:LinkButton ID="GetPackagesXml" runat="server" OnClick="getPackagesXmlFile">Download Packages.xml</asp:LinkButton>
        </div>
        <div>
            <asp:LinkButton ID="GetProcessingGroupsAndSources" runat="server" OnClick="getProcessingGroupsAndSources">Download processing groups and sources</asp:LinkButton>
        </div>
        <div style="height: 15px">
            &nbsp;</div>
        <div>
            <asp:FileUpload ID="RepositoryUpload" runat="server" Width="300px" />&nbsp;
            <asp:Button ID="UploadButton" runat="server" Text="Upload Repository" OnClick="UploadButton_Click"/>
        </div>
        <div style="height: 15px">
            &nbsp;</div>
        <div>
            <asp:FileUpload ID="DrillMapsUpload" runat="server" Width="300px" />&nbsp;
            <asp:Button ID="UploadDrillMapsButton" runat="server" Text="Upload DrillMaps.xml" OnClick="UploadDrillMapsButton_Click"/>
        </div>
        <div style="height: 15px">
            &nbsp;</div>
        <div>
            <asp:FileUpload ID="SforceUpload" runat="server" Width="300px" />&nbsp;
            <asp:Button ID="UploadSforceButton" runat="server" Text="Upload sforce.xml (SFDC settings)" OnClick="UploadSforceButton_Click"/>
        </div>
        <asp:Panel ID="ConnectorConfigsUploadPanel" runat="server" />
        <div style="height: 15px">
            &nbsp;</div>
        <div>
            <asp:FileUpload ID="SpaceSettingsUpload" runat="server" Width="300px" />&nbsp;
            <asp:Button ID="UploadSpaceSettings" runat="server" Text="Upload spacesettings.xml (foreground/background color)" OnClick="UploadSpaceSettingsButton_Click"/>
        </div>
        <div style="height: 15px">
            &nbsp;</div>
        <div>
            <asp:FileUpload ID="DashboardStyleSettingsUpload" runat="server" Width="300px" />&nbsp;
            <asp:Button ID="UploadDashboardStyleSettings" runat="server" Text="Upload DashboardStyleSettings.xml" OnClick="UploadDashboardStyleSettingsButton_Click"/>
        </div>
        <div style="height: 15px">
            &nbsp;</div>
        <div>
            <asp:FileUpload ID="CustomGeoMapsUpload" runat="server" Width="300px" />&nbsp;
            <asp:Button ID="UploadCustomGeoMapsButton" runat="server" Text="Upload CustomGeoMaps.xml" OnClick="UploadCustomGeoMapsButton_Click"/>
        </div>
        <div style="height: 15px">
            &nbsp;</div>
        <div>
            <asp:FileUpload ID="PageSequenceUpload" runat="server" Width="300px" />&nbsp;
            <asp:Button ID="PageSequenceButton" runat="server" Text="Upload pageSequence.txt (for Kiosk mode)" OnClick="UploadPageSequenceButton_Click"/>
        </div>
        <div style="height: 15px">
            &nbsp;</div>
        <div>
            <asp:FileUpload ID="CustomSubjectAreasUpload" runat="server" Width="300px" />&nbsp;
            <asp:Button ID="UploadCustomSubjectAreasButton" runat="server" Text="Upload CustomSubjectAreas Zip File" OnClick="UploadCustomSubjectAreasButton_Click"/>
        </div>
        <div style="height: 15px">
            &nbsp;</div>
        <div>
            <asp:FileUpload ID="KMLFilesUpload" runat="server" Width="300px" />&nbsp;
            <asp:Button ID="UploadKMLFilesButton" runat="server" Text="Upload KML Zip File" OnClick="UploadKMLFilesButton_Click"/>
        </div>
        <div style="height: 15px">
            &nbsp;</div>
        <div>
            <asp:LinkButton ID="RemoveDependencies" runat="server" OnClick="removeDependencies">Remove Dependencies From Repository</asp:LinkButton>
            <br />
            <asp:LinkButton ID="ImpliedGrainsProcessing" runat="server" 
                OnClick="disableImpliedGrainsProcessing" 
                ToolTip="Click this link if you would like Birst to stop carrying keys down levels and processing implied grains">Disable Implied Grains Processing</asp:LinkButton>
            <br />
            <asp:LinkButton ID="BuildApplication" runat="server" OnClick="buildApplication">Rebuild Application</asp:LinkButton>
        </div>
        &nbsp;</div>
    <span style="font-weight: bold">Manage Files</span>
    <div style="padding: 10px; border: 1px solid gray">
        <div>
            <asp:LinkButton ID="DeletePublishLock" runat="server" OnClick="deletePublishLock">Delete publish.lock file</asp:LinkButton>
        </div>
        <asp:Panel ID="GetDeleteLocalPublishPanel" runat="server"/>
    </div>
    <div style="height: 30px">
        &nbsp;</div>
    <span style="font-weight: bold">Logs</span>
    <div style="padding: 10px; border: 1px solid gray">
        <div>
            <asp:Panel ID="GetLogPanel" runat="server"/>            
        </div>
        <asp:Panel ID="GetLocalLoadLogPanel" runat="server"/>
        <div>
            <asp:LinkButton ID="GetTraceLog" runat="server" OnClick="getTraceLog">Get Last ETL Trace Log (PRINT statement output)</asp:LinkButton>
        </div>
        <asp:Panel ID="GetLocalTraceLogPanel" runat="server"/>
        <asp:Panel ID="GetActivityLogPanel" runat="server"/>              
        <div>
            <asp:LinkButton ID="SFDCExtractionLog" runat="server" OnClick="getExtractionLog">Connectors extraction log (scheduled extraction via external scheduler)</asp:LinkButton>
        </div>
        <div>
            <asp:LinkButton ID="SchedulerAcornOperationLog" runat="server" OnClick="getSchedulerAcornOpLog">Scheduler Acorn Operation log (external scheduler - space specific log containing Acorn Admin and Builder operations)</asp:LinkButton>
        </div>
        <div>
            <asp:Label ID="ErrorLog" runat="server" Text="" Visible="false"></asp:Label>
        </div>
    <div style="height: 30px">
        &nbsp;</div>
        <div>
            <asp:Label ID="Label2" Text="Servers" runat="server"></asp:Label>
            <asp:GridView ID="ServerView" runat="server" AutoGenerateColumns="false" OnRowCommand="getLogCommand" ShowHeader="true" GridLines="Both">
                <Columns>
                    <asp:BoundField HeaderText="Server" DataField="Server"/>
                    <asp:BoundField HeaderText="Description" DataField="Description" />
                    <asp:ButtonField HeaderText="Acorn Log" DataTextField="Acorn Log" CommandName="acorn" />
                    <asp:ButtonField HeaderText="SMIWeb Log" DataTextField="SMIWeb Log" CommandName="smiweb" />
                    <asp:ButtonField HeaderText="Scheduler Log" DataTextField="Scheduler Log" CommandName="scheduler" />
                    <asp:ButtonField HeaderText="Connector Log" DataTextField="Connector Log" CommandName="connector" />
                </Columns>
            </asp:GridView>
            <div style="height: 30px">
            &nbsp;</div>
            <asp:GridView ID="VersionIndpendentServerView" runat="server" AutoGenerateColumns="false" 
              DataKeyNames="ID" OnRowCommand="getIndependentLogCommand" ShowHeader="true" GridLines="Both">
                <Columns>
                    <asp:BoundField HeaderText="Server" DataField="Name"/>
                    <asp:BoundField HeaderText="Description" DataField="Description" />
                    <asp:ButtonField HeaderText="Log" DataTextField="Log" />                    
                </Columns>
            </asp:GridView>
        </div>
    <div style="height: 30px">
        &nbsp;</div>
        <div>
            Bytes to read (<=0 for the entire log):&nbsp;<asp:TextBox ID="BytesRead" runat="server" Text="512000"></asp:TextBox>
        </div>
    </div>
    <asp:Panel ID="SeedQueriesPanel" runat="server" Visible="false">
    <div style="height: 30px">
        &nbsp;</div>
    <span style="font-weight: bold">Cache</span>
    <div style="padding: 10px; border: 1px solid gray">
        <div>
            <asp:Button ID="SeedDashboardQueries" runat="server" OnClick="SeedCacheForDashboardQueries" Text="Seed Cache for Dashboard Queries (Beta)" />
        </div>
    </div>
    </asp:Panel>
    <div style="height: 30px">
        &nbsp;</div>
    <span style="font-weight: bold">Query</span>
    <div style="padding: 10px; border: 1px solid gray">
        <table>
            <tr>
                <td>
                    <asp:Label ID="QueryConnectionLabel" runat="server" Text="Query Connection" />
                    <asp:DropDownList ID="QueryConnection" runat="server" AutoPostBack="true" /><br />
                </td>                
            </tr>
            <tr>
                <td>
                    <asp:TextBox ID="QueryBox" runat="server" Height="200px" TextMode="MultiLine" Width="950px"
                        Font-Size="9pt"></asp:TextBox>
                </td>
                <td valign="top">
                    <asp:Button ID="RunQueryButton" runat="server" Text="Run Query" OnClick="RunQuery" /><br />
                    <asp:Label Text="Query Timeout: " runat="server" /><asp:TextBox ID="TimeOutBoxID" Text="10" runat="server" /><br />
                    <asp:Label Text="Maximum Number of Rows to Display: " runat="server" /><asp:TextBox ID="ResultSetSizeBoxID" Text="10" runat="server" /><br />
                    <asp:Button ID="ListTablesButton" runat="server" Text="List Tables" OnClick="ListTables" />
                </td>
            </tr>
        </table>
        <asp:GridView ID="ResultGrid" runat="server" Visible="false" 
            AllowPaging="False" Font-Size="8pt" OnRowCommand="ResultGrid_RowCommand" 
            ShowHeaderWhenEmpty="True" />
        <asp:Label ID="ResultLabel" runat="server" Text="" Visible="false"></asp:Label>
        <asp:Label ID="ErrorLabel" runat="server" Text="" Visible="false"></asp:Label>
    </div>
    <iframe src='<%= this.frameSource %>' style="display: none"></iframe>
       <div style="height: 30px">
        &nbsp;</div> 
    <div>
            <asp:GridView ID="Limits" runat="server" AutoGenerateColumns="true" CellPadding="5"
                ShowHeader="true" GridLines="None">
            </asp:GridView>  
    </div>
       <div style="height: 30px">
        &nbsp;</div>   
        <div> 
            <asp:GridView ID="DataFiles" runat="server" AutoGenerateColumns="true" CellPadding="5"
                ShowHeader="true" GridLines="None">
            </asp:GridView>  
        </div>
       <div style="height: 30px">
        &nbsp;</div>       
        <asp:Button ID="InvalidateDashboardCacheID" runat="server" Text="Invalidate Dashboard Cache" OnClick="InvalidateDashboardCache" /><br />
       <div style="height: 30px">
        &nbsp;</div>       
        <asp:Button ID="SFDCClearLoaderID" runat="server" Text="Connector Clear Status" OnClick="ClearSFDCLoader" /><br />
        <asp:Label ID="Label1" runat="server" Font-Size="Small" Text = "(CAUTION : If connector extraction seems to hang and no processing or extraction seems to be going on, only then use this to clear connector status)" />
     <div style="height: 30px">
        &nbsp;</div>     
    <div>
            <asp:GridView ID="AssemblyVersions" runat="server" AutoGenerateColumns="true"
                ShowHeader="false" GridLines="None">
            </asp:GridView>  
    </div>   
    <asp:Panel ID="EncryptPanel" runat="server" Visible="false"> 
        <div>
            <asp:TextBox ID="CryptValue"  Columns="100" runat="server" Text="" />
            <asp:Label ID="CryptLabel"  runat="server" Text="" />
            <br />
            <asp:Button ID="EncryptButton" runat="server" Text="Encrypt" OnClick="EncryptValue" />
            <asp:Button ID="DecryptButton" runat="server" Text="Decrypt" OnClick="DecryptValue" />
        </div>
    </asp:Panel>
    <br /><br />
    <asp:Panel ID="Panel1" runat="server">
        <span style="font-weight: bold">SAML Configuration Paramters</span>
        <div style="padding: 10px; border: 1px solid gray">
           <asp:Label runat="server" ID="SamlActionError" Visible="false" ForeColor="Red"></asp:Label>
            <table>
                <tr>
                    <td>
                        <asp:Label runat="server" Width="125px" Text="IDP Name" ID="IdpName"></asp:Label>
                        <asp:TextBox runat="server" ID="IdpNameValue" Width="150px"></asp:TextBox>
                        &nbsp;&nbsp;&nbsp;&nbsp;
                    </td>
                    <td>
                        <asp:Label runat="server" Width="125px" Text="IDP IssuerID" ID="IdpIssuerID"></asp:Label>
                        <asp:TextBox runat="server" ID="IdpIssuerIDValue" Width="200px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:Label runat="server" Width="125px" Text="Certificate" ID="Certificate"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">    
                        <asp:TextBox runat="server" TextMode="MultiLine"  ID="CertificateValue" 
                            Width="100%" Height="200px"></asp:TextBox>
                        &nbsp;&nbsp;&nbsp;&nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label runat="server" Width="125px" Text="Timeout (Minutes)" ID="LogTimeout"></asp:Label>
                        <asp:TextBox runat="server" ID="TimeoutValue" Width="50px"></asp:TextBox>
                        &nbsp;&nbsp;&nbsp;&nbsp;
                    </td>
                    <td>
                        <asp:Label runat="server" Width="125px" Text="Active" ID="Active"></asp:Label>
                        <asp:CheckBox runat="server" ID="ActiveValue" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label runat="server" Width="125px" Text="Logout Page" ID="LogoutPage"></asp:Label>
                        <asp:TextBox runat="server" ID="LogoutPageValue" Width="150px"></asp:TextBox>
                        &nbsp;&nbsp;&nbsp;&nbsp;
                    </td>
                    <td>
                        <asp:Label runat="server" Width="125px" Text="Error Page URL" ID="IdpErrorPage"></asp:Label>
                        <asp:TextBox runat="server" ID="ErrorPageValue" Width="200px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label runat="server" Width="125px" Text="IDP URL" ID="Label3"></asp:Label>
                        <asp:TextBox runat="server" ID="IdpUrlValue" Width="150px"></asp:TextBox>
                        &nbsp;&nbsp;&nbsp;&nbsp;
                    </td>
                    <td>
                        <asp:Label runat="server" Width="125px" Text="Sign IDP Request" ID="SignIdpRequest"></asp:Label>
                        <asp:CheckBox runat="server" ID="SignIdpValue" />
                    </td>
               </tr>
               <tr>
                    <td>
                        <asp:Label runat="server" Width="125px" Text="SPInitiated" ID="SpInitiatedRedirect"></asp:Label>
                        <asp:CheckBox runat="server" ID="SPInitValue" />
                    </td>
                    <td>
                        <asp:Label runat="server" Width="125px" Text="AccountIDs" ID="SamlAccount"></asp:Label>
                        <asp:TextBox runat="server" ID="SamlAccountIdValues" Width="200px"></asp:TextBox>
                    </td>
               </tr>
               <tr align="right">
                   <td colspan="2">
                        <asp:Button runat="server" Width="100px" ID="AddIDP" Text="Add" OnClick="AddIDP_Click" />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:Button runat="server" Width="100px" ID="UpdateIDP" Text="Update" OnClick="UpdateIDP_Click" />
                   </td>
               </tr>
         </table>
        </div>
    </asp:Panel>
    </form>
</body>
</html>
