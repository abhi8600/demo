﻿<%@ Page Language="C#" AutoEventWireup="True" CodeBehind="Login.aspx.cs" Inherits="Acorn.Login" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Birst</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="description" content="Birst" />
    <link rel="stylesheet" type="text/css" href="global.css" />
    <!--[if gt IE 6]>
<link rel="stylesheet" type="text/css" href="common/css/ie7only.css" />
<![endif]-->
    <script type="text/javascript">
        if (top.frames.length != 0)
            top.location = self.document.location;
    </script>
    <link type="text/css" rel="stylesheet" href="openid-selector/css/openid.css" />
    <script type="text/javascript" src="openid-selector/js/jquery-1.2.6.min.js"></script>
    <script type="text/javascript" src="openid-selector/js/openid-jquery.js"></script>
    <script type="text/javascript" src="openid-selector/js/openid-en.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            openid.init('openid_identifier');
        });  
    </script>
</head>
<body>
    <noscript>
        <p class="nonjsAlert">
            NOTE: This web application requires Javascript and cookies. You can enable both
            via your browser's preference settings.</p>
    </noscript>
    <div id="container">
        <div class="top">
        </div>
        <div class="wrapper">
            <div class="margins">
                <div id="header">
                    <div class="logo">
                        <asp:HyperLink ID="WebsiteLink" runat="server" NavigateUrl="http://www.birst.com"
                            ImageUrl="images/logo-birst.png" Target="_blank" />
                    </div>
                </div>
                <div class="cleaner">
                </div>
            </div>
                  <div>
                  <table align="center" >
                  <tr>
                  <td>
                    <div class="headline">
                    <!--
                        <h2>
                            <asp:Label ID="HeaderTextLabel" runat="server" Text="Birst Agile Business Analytics" />
                        </h2>
                        -->
                    </div>
                        <asp:Label ID="SuccessfulLabel" runat="server" Visible="false" ForeColor="Red" Text="Registration Successful" />                        
                        <asp:Label ID="OpenIDLabel" runat="server" Visible="false" ForeColor="Red" />
                        <asp:Label ID="RegistrationKeyLabel" runat="server" Visible="false" ForeColor="Red"
                            Text="Birst server failed to initialize, please contact customer service" />
                    <div id="content">
                    <form id="Form1" runat="server" autocomplete="off">
                        <div id="loginWrap">
                            <div class="login">
                                <div class="loginFill">
                                    <asp:Login ID="LoginForm2" runat="server" OnAuthenticate="LoginForm2_Authenticate"                                        
                                        DisplayRememberMe="False" OnInit="LoginForm2_Init" UserNameLabelText="User Name (email):"
                                        OnLoggedIn="LoginForm2_LoggedIn">
                                        <LayoutTemplate>
                                            <asp:Literal ID="FailureText" runat="server"></asp:Literal>
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td width="20%">
                                                        <asp:Label ID="UsernameLabel" runat="server" Text="Username:"  Font-Bold="true" />
                                                    </td>
                                                    <td colspan="2">
                                                        <asp:TextBox ID="UserName" MaxLength="255" runat="server"/>
                                                        <asp:RequiredFieldValidator ID="UserNameRequired" runat="server" ControlToValidate="UserName"
                                                            ErrorMessage="User Name is required." ToolTip="User Name is required." ValidationGroup="LoginForm">*</asp:RequiredFieldValidator>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="PasswordLabel" runat="server" Text="Password:" Font-Bold="true"/>
                                                    </td>
                                                    <td colspan="2">
                                                        <asp:TextBox ID="Password" MaxLength="64" runat="server" TextMode="Password" autocomplete="off"/>
                                                        <asp:RequiredFieldValidator ID="PasswordRequired" runat="server" ControlToValidate="Password"
                                                            ErrorMessage="Password is required." ToolTip="Password is required." ValidationGroup="LoginForm">*</asp:RequiredFieldValidator>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        &nbsp;
                                                    </td>
                                                    <td width="41%">
                                                        <asp:Button ID="LoginButton" runat="server" CssClass="btn"
                                                            CommandName="Login" Text="Login" ValidationGroup="LoginForm" AlternateText="Secure Login" />
                                                    </td>
                                                    <td width="39%">
                                                        <asp:LinkButton ID="recoverPasswordLink" runat="server" Font-Size="10px" PostBackUrl="~/RecoverPassword.aspx"
                                                            Text="Forgot Your Password?" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </LayoutTemplate>
                                        <InstructionTextStyle Font-Italic="True" ForeColor="Black" />
                                        <LabelStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                    </asp:Login>
                                </div>
                            </div>
                            <div class="cleaner">
                                <br />
                            </div>
                        </div>
                        <div class="cleaner">
                        </div>
                    </form>
        <div style="height: 20px">
            &nbsp;</div>
                    <asp:PlaceHolder ID="OpenIDPlaceHolder" runat="server">
                        <form id="openid_form" name="action" action="Login.aspx?birst.OpenID=true" method="post">
                        <div>
                            <fieldset style="width: 440px">
                                <legend>Login using OpenID</legend>
                                <div id="openid_choice">
                                    <p>
                                        Please select an OpenID provider below.<br />
                                        Note: Your Birst username must be associated with the OpenID provider account.</p>
                                    <div id="openid_btns">
                                    </div>
                                </div>
                                <div id="openid_input_area">
                                    <input id="openid_identifier" name="openid_identifier" type="text" value="http://" />
                                    <input id="openid_submit" type="submit" value="Sign-In" />
                                </div>
                            </fieldset>
                        </div>
                        </form>
                    </asp:PlaceHolder>
                    <asp:PlaceHolder ID="CertificationPlaceHolder" runat="server">
                        <table border="0" align="center" cellpadding="2" cellspacing="0">
                            <tr>
                                <td align="center" valign="top" width="126">
                                    <script type="text/javascript" src="https://seal.verisign.com/getseal?host_name=app.birst.com&size=M&use_flash=NO&use_transparent=NO&lang=en"></script>
                                    <br />
                                    <a href="http://www.verisign.com/ssl-certificate/" target="_blank" style="color: #000000;
                                        text-decoration: none; font: bold 7px verdana,sans-serif; letter-spacing: .5px;
                                        text-align: center; margin: 0px; padding: 0px;">ABOUT SSL CERTIFICATES</a>
                                </td>
                                <td align="center" valign="top" width="122">
                                    <a href="https://www.securitymetrics.com/site_certificate.adp?s=app.birst.com&i=190634"
                                        target="_blank">
                                        <img src="https://www.securitymetrics.com/images/sm_tested3.gif" alt="SecurityMetrics Certified"
                                            border="0" /></a>
                                </td>
                            </tr>
                        </table>
                    </asp:PlaceHolder>
                </div>
                </div>
            <div class="cleaner">
            </div>
        <div style="height: 20px">
            &nbsp;</div>
        <div id="loginWrap" class="footerstyle">
            <table width="100%" cellpadding="0" cellspacing="0" style="padding-top: 10px">
                <tr>
                    <td style="color: #555555; text-align: left">
                        <asp:Literal ID="Copyright" runat="server" 
                            meta:resourcekey="CopyrightResource1"></asp:Literal>
                        &nbsp;
                        <asp:Literal ID="Version" runat="server" meta:resourcekey="VersionResource1"></asp:Literal>
                        &nbsp;
                    </td>
                    <asp:PlaceHolder ID="TermsOfServicePlaceHolder" runat="server">
                        <td style="width: 100px; text-align:center ">
                            <asp:HyperLink ID="TermsOfServiceLink" runat='server' NavigateUrl="http://www.birst.com/terms-of-service" Target="_blank" ForeColor="#006FAA" Font-Overline="false" Text="Terms of Service" />
                        </td>
                    </asp:PlaceHolder>
                    <asp:PlaceHolder ID="PrivacyPolicyPlaceHolder" runat="server">
                        <td style="width: 80px; text-align: right">
                            <asp:HyperLink ID="PrivacyPolicyLink" runat='server' NavigateUrl="http://www.birst.com/privacy-policy" Target="_blank" ForeColor="#006FAA" Font-Overline="false" Text="Privacy Policy" />
                        </td>
                    </asp:PlaceHolder>
                    <asp:PlaceHolder ID="ContactUsPlaceHolder" Visible="false" runat="server">
                        <td style="width: 100px; text-align: center">
                            <asp:HyperLink ID="ContactUsLink" runat='server' NavigateUrl="http://www.birst.com/company/contact" Target="_blank" ForeColor="#006FAA" Font-Overline="false" Text="Contact Us" />
                        </td>
                    </asp:PlaceHolder>
                </tr>
            </table>
        </div>
        </div>
    </div>
</body>
</html>
