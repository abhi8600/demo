﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Web.SessionState;
using Performance_Optimizer_Administration;
using System.Collections.Generic;
using System.Data.Odbc;
using System.Text;
using Acorn.Utils;
using Acorn.DBConnection;

namespace Acorn
{
    public class ViewProcessed
    {
        private const int NUM_RECORDS_TO_SHOW = 100;

        public static ViewProcessedResults getDataTables(HttpSessionState session)
        {
            MainAdminForm maf = (MainAdminForm)session["MAF"];
            if (maf == null)
                return null;
            Space sp = (Space)session["space"];
            QueryConnection conn = null;
            List<string[]> results = new List<string[]>();
            object[] sizes = null;
            ViewProcessedResults vpr = new ViewProcessedResults();
            try
            {
                conn = Util.getConnectionWithVariedRetries(sp.getFullConnectString(), sp.DatabaseType);
                if (sp.DatabaseDriver == "memdb.jdbcclient.DBDriver")
                {
                    vpr.Status = Database.getStatus(conn);
                    sizes = Database.getTableSizes(conn, sp.Schema);
                }
                foreach (Hierarchy h in maf.hmodule.getHierarchies())
                {
                    if (maf.timeDefinition != null && h.DimensionName == maf.timeDefinition.Name)
                        continue;
                    List<Level> llist = new List<Level>();
                    h.getChildLevels(llist);
                    foreach (Level l in llist)
                    {
                        if (!l.GenerateDimensionTable)
                            continue;
                        string name = h.DimensionName + "." + l.Name;
                        long size = 0;
                        if (sizes != null)
                        {
                            // Get table size
                            DimensionTable dt = getDimensionTableFromName(maf, name);
                            if (dt != null)
                            {
                                foreach (object[] sizerow in sizes)
                                {
                                    if (dt.TableSource != null && dt.TableSource.Tables != null && dt.TableSource.Tables.Length == 1 &&
                                        (string)sizerow[0] == dt.TableSource.Tables[0].PhysicalName)
                                    {                               
                                        size = (long)sizerow[1];
                                        break;
                                    }
                                }
                            }
                        }
                        results.Add(new string[] { "level", name, size.ToString() });
                    }
                }
                Dictionary<string, string[][]> grains = CustomCalculations.getGrains(session);
                foreach (string s in grains.Keys)
                {
                    long size = 0;
                    if (sizes != null)
                    {
                        // Get table size
                        MeasureTable mt = getMeasureTableFromName(session, sp, maf, s);
                        if (mt != null)
                        {
                            foreach (object[] sizerow in sizes)
                            {
                                if (mt.TableSource != null && mt.TableSource.Tables != null && mt.TableSource.Tables.Length == 1 &&
                                    (string)sizerow[0] == mt.TableSource.Tables[0].PhysicalName)
                                {
                                    size = (long)sizerow[1];
                                    break;
                                }
                            }
                        }
                    }
                    results.Add(new string[] { "grain", s, size.ToString() });
                }
                List<string> tableList = null;
                Dictionary<string, LocalETLConfig> leConfigs = BirstConnectUtil.getLocalETLConfigsForSpace(sp);
                foreach (StagingTable sst in maf.stagingTableMod.getStagingTables())
                {
                    DatabaseConnection dc = null;
                    foreach (string lg in sst.LoadGroups)
                    {
                        if (lg.StartsWith(LiveAccessUtil.LIVE_ACCESS_LOAD_GROUP_PREFIX) && leConfigs.ContainsKey(lg))
                        {
                            dc = maf.connection.findConnection(leConfigs[lg].getLocalETLConnection());
                            break;
                        }
                    }
                    long size = 0;
                    if (sizes != null)
                    {
                        // Get table size
                        foreach (object[] sizerow in sizes)
                        {
                            if ((string)sizerow[0] == sst.Name)
                            {
                                size = (long)sizerow[1];
                                break;
                            }
                        }
                    }
                    if (dc != null && dc.Realtime)
                    {
                        bool tableExists = LiveAccessUtil.tableExistsOnLiveAccess(sp, dc.Name, sp.Schema, sst.Name);
                        results.Add(new string[] { "staging", sst.Name.Substring(3), tableExists ? Boolean.TrueString : Boolean.FalseString, size.ToString(), getLogicalNameNullCheck(sst.LogicalName, sst.Name) });
                    }
                    else
                    {
                        bool tableExists = Database.tableExists(conn, sp.Schema, sst.Name, ref tableList);
                        results.Add(new string[] { "staging", sst.Name.Substring(3), tableExists ? Boolean.TrueString : Boolean.FalseString, size.ToString() , getLogicalNameNullCheck(sst.LogicalName, sst.Name)});
                    }
                }
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
            vpr.Results = results.ToArray();
            return vpr;
        }

        private static String getLogicalNameNullCheck(String logicalName, String name)
        {
            if (string.IsNullOrEmpty(logicalName) == true)
                return name.Substring(3);
            else if(logicalName != null && logicalName.StartsWith("ST_"))
                return logicalName.Substring(3);
            else
                return logicalName;            
        }

        private static DimensionTable getDimensionTableFromName(MainAdminForm maf, string name)
        {
            int index = name.IndexOf('.');
            if (index < 0)
                return null;
            string dimension = name.Substring(0, index);
            string level = name.Substring(index + 1);
            DimensionTable founddt = null;
            foreach (DimensionTable dt in maf.dimensionTablesList)
            {
                if (dt.DimensionName == dimension && dt.Level == level && (dt.InheritTable == null || dt.InheritTable.Length == 0))
                {
                    founddt = dt;
                    break;
                }
            }
            return founddt;
        }

        public static ViewProcessedResults getLevelData(HttpSessionState session, string name, bool profile, string query)
        {
            MainAdminForm maf = (MainAdminForm)session["MAF"];
            if (maf == null)
                return null;
            DimensionTable founddt = getDimensionTableFromName(maf, name);
            if (founddt != null)
            {
                if (profile)
                    return getProfileResults(session, founddt, maf, 0);
                else if (query != null && query.Length > 0)
                    return getQueryResults(session, founddt, query, name);
                else
                    return getTableResults(session, founddt, null, SortDirection.Ascending);
            }
            return null;
        }

        private static MeasureTable getMeasureTableFromName(HttpSessionState session, Space sp, MainAdminForm maf, string name)
        {
            MeasureTable foundmt = getMeasureTableFromName(session, maf, name, null);
            if (foundmt == null)
            {
                //check if measuretable exists on any local etl connection
                Dictionary<string, LocalETLConfig> leConfigs = BirstConnectUtil.getLocalETLConfigsForSpace(sp);
                if (leConfigs.Count > 0)
                {
                    foreach (LocalETLConfig leConfig in leConfigs.Values)
                    {
                        foundmt = getMeasureTableFromName(session, maf, name, leConfig.getLocalETLConnection());
                        if (foundmt != null)
                            break;
                    }
                }
            }
            return foundmt;
        }

        private static MeasureTable getMeasureTableFromName(HttpSessionState session, MainAdminForm maf, string name, string localETLConnection)
        {
            int index = name.IndexOf('.');
            if (index < 0)
                return null;
            Dictionary<string, string[][]> grains = CustomCalculations.getGrains(session);
            string[][] grain = null;
            if (!grains.ContainsKey(name))
                return null;
            grain = grains[name];

            MeasureTable foundmt = null;
            MeasureGrain mg = new MeasureGrain();
            mg.connection = localETLConnection;
            mg.measureTableGrains = new MeasureTableGrain[grain.Length];
            for (int i = 0; i < grain.Length; i++)
            {
                mg.measureTableGrains[i] = new MeasureTableGrain();
                mg.measureTableGrains[i].DimensionName = grain[i][0];
                mg.measureTableGrains[i].DimensionLevel = grain[i][1];
            }
            ApplicationBuilder ab = new ApplicationBuilder(maf);
            string mtname = ab.getLogicalMeasureTableName(null, mg);
            foreach (MeasureTable mt in maf.measureTablesList)
            {
                if (mt.TableName == mtname)
                {
                    foundmt = mt;
                    break;
                }
            }
            return foundmt;
        }

        public static ViewProcessedResults getMeasureTableData(HttpSessionState session, string name, bool profile, string query)
        {
            MainAdminForm maf = (MainAdminForm)session["MAF"];
            if (maf == null)
                return null;
            Space sp = Util.getSessionSpace(session);
            MeasureTable foundmt = getMeasureTableFromName(session, sp, maf, name);
            if (foundmt != null)
            {
                if (profile)
                    return getProfileResults(session, foundmt, maf, 0);
                else if (query != null && query.Length > 0)
                    return getQueryResults(session, foundmt, query, name);
                else
                    return getTableResults(session, foundmt, null, SortDirection.Ascending);
            }
            return null;
        }

        public static StagingTable getStagingTableFromName(MainAdminForm maf, string name)
        {
            List<StagingTable> stlist = maf.stagingTableMod.getStagingTables();
            StagingTable st = null;
            foreach (StagingTable sst in stlist)
            {
                if (sst.Name == "ST_" + name)
                {
                    st = sst;
                    break;
                }
            }
            return st;
        }

        public static ViewProcessedResults getStagingTableData(HttpSessionState session, string name, bool profile, string query)
        {
            MainAdminForm maf = (MainAdminForm)session["MAF"];
            if (maf == null)
                return null;
            StagingTable st = getStagingTableFromName(maf, name);
            if (st != null)
            {
                if (profile)
                    return getProfileResults(session, st, maf, 0);
                else if (query != null && query.Length > 0)
                    return getQueryResults(session, st, query, name);
                else
                    return getTableResults(session, st, null, SortDirection.Ascending);
            }
            return null;
        }

        private static ViewProcessedResults getTableResults(HttpSessionState session, object o, string sortExpression, SortDirection sd)
        {
            QueryConnection conn = null;
            ViewProcessedResults vpr = new ViewProcessedResults();
            try
            {
                MainAdminForm maf = (MainAdminForm)session["MAF"];
                if (maf == null)
                    return null;
                Space sp = (Space)session["space"];
                conn = Util.getConnectionWithVariedRetries(sp.getFullConnectString(), sp.DatabaseType);
                string pname = null;
                string tname = null;
                bool dimension = typeof(DimensionTable).Equals(o.GetType());
                bool measure = typeof(MeasureTable).Equals(o.GetType());
                bool staging = typeof(StagingTable).Equals(o.GetType());
                DatabaseConnection dc = null;
                if (dimension)
                {
                    pname = ((DimensionTable)o).TableSource.Tables[0].PhysicalName;
                    tname = ((DimensionTable)o).TableName;
                    dc = maf.connection.findConnection(((DimensionTable)o).TableSource.Connection);
                }
                else if (measure)
                {
                    pname = ((MeasureTable)o).TableSource.Tables[0].PhysicalName;
                    tname = ((MeasureTable)o).TableName;
                    dc = maf.connection.findConnection(((MeasureTable)o).TableSource.Connection);
                }
                else if (staging)
                {
                    pname = ((StagingTable)o).Name;              
                    tname = ((StagingTable)o).Name.Substring(3);
                    Dictionary<string, LocalETLConfig> leConfigs = BirstConnectUtil.getLocalETLConfigsForSpace(sp);
                    foreach (string lg in ((StagingTable)o).LoadGroups)
                    {
                        if (lg.StartsWith(LiveAccessUtil.LIVE_ACCESS_LOAD_GROUP_PREFIX) && leConfigs.ContainsKey(lg))
                        {
                            dc = maf.connection.findConnection(leConfigs[lg].getLocalETLConnection());
                            break;
                        }
                    }
                }
                string sortclause = "";
                if (sortExpression != null)
                    sortclause = " ORDER BY " + getPhysicalName(o, sortExpression) + (sd == SortDirection.Ascending ? " ASC" : " DESC");
                string query = null;
                if (sp.DatabaseType == Database.INFOBRIGHT || sp.DatabaseType == Database.MEMDB || sp.DatabaseType == Database.PARACCEL || sp.DatabaseType == Database.REDSHIFT)
                    query = "SELECT * FROM " + sp.Schema + "." + pname + sortclause + " LIMIT " + NUM_RECORDS_TO_SHOW;
                else if (sp.DatabaseType == Database.ORACLE)
                    query = "SELECT * FROM (SELECT * FROM " + sp.Schema + "." + pname + sortclause + ") WHERE ROWNUM <= " + NUM_RECORDS_TO_SHOW;
                else
                    query = "SELECT TOP " + NUM_RECORDS_TO_SHOW + " * FROM " + sp.Schema + "." + pname + sortclause;
                if (dc != null && dc.Realtime)
                {
                    Object[][] res = LiveAccessUtil.executeLiveAccessQuery(session, null, sp, dc.Name, query, true);
                    vpr.Results = new string[res.Length][];
                    List<int> cols = new List<int>();
                    for (int i = 0; i < res[0].Length; i++)
                    {
                        if (dimension && res[0][i].ToString().EndsWith("_CKSUM$") && res[0][i].ToString().StartsWith("ST_"))
                            continue;
                        else if (staging && res[0][i].ToString().EndsWith("_CKSUM$"))
                            continue;
                        cols.Add(i);
                    }
                    for (int i = 0; i < res.Length; i++)
                    {
                        vpr.Results[i] = new string[cols.Count];
                        for (int j = 0; j < cols.Count; j++)
                        {
                            if (i == 0)
                                vpr.Results[i][j] = getLogicalName(o, res[i][cols[j]].ToString(), maf);
                            else
                                vpr.Results[i][j] = res[i][cols[j]].ToString();
                        }
                    }
                }
                else
                {
                    QueryDataAdapter adapt = new QueryDataAdapter(query, conn);
                    DataTable dt = new DataTable(tname);
                    adapt.Fill(dt);
                    List<string[]> results = new List<string[]>();
                    List<int> cols = new List<int>();
                    List<string> headers = new List<string>();
                    for (int i = 0; i < dt.Columns.Count; i++)
                    {
                        if (dimension && dt.Columns[i].ColumnName.EndsWith("_CKSUM$") && dt.Columns[i].ColumnName.StartsWith("ST_"))
                            continue;
                        else if (staging && dt.Columns[i].ColumnName.EndsWith("_CKSUM$"))
                            continue;
                        headers.Add(getLogicalName(o, dt.Columns[i].ColumnName, maf));
                        cols.Add(i);
                    }
                    results.Add(headers.ToArray());
                    foreach (DataRow dr in dt.Rows)
                    {
                        string[] row = new string[cols.Count];
                        for (int i = 0; i < cols.Count; i++)
                            row[i] = dr[cols[i]].ToString();
                        results.Add(row);
                    }
                    vpr.Results = results.ToArray();
                }
            }
            catch (Exception)
            {
                vpr.ErrorMessage = "Unable to retrieve results of query";
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
            return vpr;
        }

        public static string getTableQueryString(HttpSessionState session, string name, string type)
        {
            MainAdminForm maf = (MainAdminForm)session["MAF"];
            if (maf == null)
                return null;
            StringBuilder sb = new StringBuilder("Select ");
            if (type == "level")
            {
                DimensionTable dt = getDimensionTableFromName(maf, name);
                if (dt != null)
                {
                    DataRowView[] drv = maf.dimensionColumnMappingsTablesView.FindRows(new object[] { dt.TableName });
                    bool first = true;
                    foreach (DataRowView dr in drv)
                    {
                        if ((bool)dr["AutoGenerated"] && (bool)dr["Qualify"])
                        {
                            string cname = (string)dr["ColumnName"];
                            if (first)
                                first = false;
                            else
                                sb.Append(',');
                            sb.Append("[" + cname + "]");
                        }
                    }
                    sb.Append(" FROM [" + name + "]");
                }
            }
            else if (type == "grain")
            {
                Space sp = Util.getSessionSpace(session);
                MeasureTable mt = getMeasureTableFromName(session, sp, maf, name);
                if (mt != null)
                {
                    bool first = true;
                    DataRowView[] drv = maf.measureColumnMappingsTablesView.FindRows(new object[] { mt.TableName });
                    foreach (DataRowView dr in drv)
                    {
                        if ((bool)dr["AutoGenerated"] && (bool)dr["Qualify"])
                        {
                            string cname = (string)dr["ColumnName"];
                            if (first)
                                first = false;
                            else
                                sb.Append(',');
                            sb.Append("[" + cname + "]");
                        }
                    }
                    sb.Append(" FROM [" + name + "]");
                }
            }
            else if (type == "staging")
            {
                StagingTable st = getStagingTableFromName(maf, name);               
                if (st != null)
                {
                    bool first = true;
                    foreach (StagingColumn sc in st.Columns)
                    {
                        if (sc.SourceFileColumn == null || sc.SourceFileColumn.Length == 0)
                            continue;
                        string cname = sc.Name;
                        if (first)
                            first = false;
                        else
                            sb.Append(',');
                        sb.Append("[" + cname + "]");
                    }
                    sb.Append(" FROM [" + name + "]");
                }
            }
            return sb.ToString();
        }

        private static ViewProcessedResults getQueryResults(HttpSessionState session, object o, string query, string tname)
        {
            QueryConnection conn = null;
            ViewProcessedResults vpr = new ViewProcessedResults();
            try
            {
                MainAdminForm maf = (MainAdminForm)session["MAF"];
                if (maf == null)
                    return null;
                Space sp = (Space)session["space"];
                string pname = null;
                bool dimension = typeof(DimensionTable).Equals(o.GetType());
                bool measure = typeof(MeasureTable).Equals(o.GetType());
                bool staging = typeof(StagingTable).Equals(o.GetType());
                Dictionary<string, string> columnMap = new Dictionary<string, string>();
                Dictionary<string, ExpressionParser.LogicalExpression.DataType> typeMap = new Dictionary<string, ExpressionParser.LogicalExpression.DataType>();
                DatabaseConnection dc = null;
                List<string> cnames = new List<string>();
                if (dimension)
                {
                    DimensionTable dim = (DimensionTable)o;
                    pname = dim.TableSource.Tables[0].PhysicalName;
                    DataRowView[] drv = maf.dimensionColumnMappingsTablesView.FindRows(new object[] { dim.TableName });
                    foreach (DataRowView dr in drv)
                    {
                        if ((bool)dr["AutoGenerated"] && (bool)dr["Qualify"])
                        {
                            string cname = (string)dr["ColumnName"];
                            string cpname = (string)dr["PhysicalName"];
                            DataRowView[] ldrv = maf.dimensionColumnNamesView.FindRows(new object[] { dim.DimensionName, (string)dr["ColumnName"] });
                            if (ldrv.Length > 0)
                            {
                                columnMap.Add(cname, cpname);
                                typeMap.Add(cname, new ExpressionParser.LogicalExpression.DataType((string)ldrv[0]["DataType"], 0));
                                cnames.Add(cname);
                            }
                        }
                    }
                    dc = maf.connection.findConnection(dim.TableSource.Connection);
                    columnMap.Add("LOAD_ID", "LOAD_ID");
                    typeMap.Add("LOAD_ID", new ExpressionParser.LogicalExpression.DataType("Integer", 0));
                    cnames.Add("LOAD_ID");
                }
                else if (measure)
                {
                    MeasureTable mt = ((MeasureTable)o);
                    pname = mt.TableSource.Tables[0].PhysicalName;
                    DataRowView[] drv = maf.measureColumnMappingsTablesView.FindRows(new object[] { mt.TableName });
                    foreach (DataRowView dr in drv)
                    {
                        if ((bool)dr["AutoGenerated"] && (bool)dr["Qualify"])
                        {
                            string cname = (string)dr["ColumnName"];
                            string cpname = (string)dr["PhysicalName"];
                            DataRowView[] mdrv = maf.measureColumnNamesView.FindRows(new object[] { (string)dr["ColumnName"] });
                            if (mdrv.Length > 0)
                            {
                                columnMap.Add(cname, cpname);
                                typeMap.Add(cname, new ExpressionParser.LogicalExpression.DataType((string)mdrv[0]["DataType"], 0));
                                cnames.Add(cname);
                            }
                        }
                    }
                    columnMap.Add("LOAD_ID", "LOAD_ID");
                    typeMap.Add("LOAD_ID",new ExpressionParser.LogicalExpression.DataType("Integer", 0));
                    cnames.Add("LOAD_ID");
                    dc = maf.connection.findConnection(mt.TableSource.Connection);
                }
                else if (staging)
                {
                    pname = ((StagingTable)o).Name;
                    tname = ((StagingTable)o).Name.Substring(3);
                    StagingTable st = (StagingTable)o;
                    Dictionary<string, LocalETLConfig> leConfigs = BirstConnectUtil.getLocalETLConfigsForSpace(sp);
                    foreach (string lg in st.LoadGroups)
                    {
                        if (lg.StartsWith(LiveAccessUtil.LIVE_ACCESS_LOAD_GROUP_PREFIX) && leConfigs.ContainsKey(lg))
                        {
                            dc = maf.connection.findConnection(leConfigs[lg].getLocalETLConnection());
                            break;
                        }
                    }
                    foreach (StagingColumn sc in st.Columns)
                    {
                        if (sc.SourceFileColumn == null || sc.SourceFileColumn.Length == 0)
                            continue;
                        string cname = sc.Name;
                        string cpname = Performance_Optimizer_Administration.Util.generatePhysicalName(sc.Name, maf.builderVersion) + '$';
                        columnMap.Add(cname, cpname);
                        typeMap.Add(cname, new ExpressionParser.LogicalExpression.DataType(sc.DataType, 0));
                        cnames.Add(cname);
                    }
                }
                int sqlGenType = sp.getSQLGenType();
                ExpressionParser.LogicalExpression le = new ExpressionParser.LogicalExpression(query, tname, sp.Schema + "." + pname, columnMap, typeMap,
                    100, sqlGenType, (maf.builderVersion >= 9));
                if (le.HasError)
                {
                    vpr.ErrorMessage = le.Error;
                    return vpr;
                }

                Global.systemLog.Info(le.GeneratedCode);
                DataTable dt = new DataTable(tname);
                if (dc != null && dc.Realtime)
                {
                    Object[][] res = LiveAccessUtil.executeLiveAccessQuery(session, null, sp, dc.Name, le.GeneratedCode, false);
                    vpr.Results = new string[res.Length + 1][];
                    vpr.Results[0] = new string[cnames.Count];
                    for (int j = 0; j < cnames.Count; j++)
                    {
                        vpr.Results[0][j] = cnames[j];
                    }
                    for (int i = 1; i <= res.Length; i++)
                    {
                        vpr.Results[i] = new string[res[i - 1].Length];
                        for (int j = 0; j < res[i - 1].Length; j++)
                        {
                            vpr.Results[i][j] = res[i - 1][j].ToString();
                        }
                    }
                }
                else
                {
                    conn = Util.getConnectionWithVariedRetries(sp.getFullConnectString(), sp.DatabaseType);
                    QueryDataAdapter adapt = new QueryDataAdapter(le.GeneratedCode, conn);
                    adapt.Fill(dt);
                    List<string[]> results = new List<string[]>();
                    List<int> cols = new List<int>();
                    List<string> headers = new List<string>();
                    for (int i = 0; i < dt.Columns.Count; i++)
                    {
                        if (dimension && dt.Columns[i].ColumnName.EndsWith("_CKSUM$") && dt.Columns[i].ColumnName.StartsWith("ST_"))
                            continue;
                        else if (staging && dt.Columns[i].ColumnName.EndsWith("_CKSUM$"))
                            continue;
                        headers.Add(le.ProjectionListColumns[i]);
                        cols.Add(i);
                    }
                    results.Add(headers.ToArray());
                    foreach (DataRow dr in dt.Rows)
                    {
                        string[] row = new string[cols.Count];
                        for (int i = 0; i < cols.Count; i++)
                            row[i] = dr[cols[i]].ToString();
                        results.Add(row);
                    }
                    vpr.Results = results.ToArray();
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Warn(ex, ex);
                vpr.ErrorMessage = "Unable to retrieve results of query";
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
            return vpr;
        }

        private static string getLogicalName(Object o, string physicalName, MainAdminForm maf)
        {
            if (typeof(DimensionTable).Equals(o.GetType()))
            {
                foreach (DimensionColumn dc in ((DimensionTable)o).DimensionColumns)
                {
                    if (dc.Qualify && dc.PhysicalName == physicalName)
                        return dc.ColumnName;
                }
            }
            else if (typeof(MeasureTable).Equals(o.GetType()))
            {
                foreach (MeasureColumn mc in ((MeasureTable)o).MeasureColumns)
                {
                    if (mc.Qualify && mc.PhysicalName == physicalName)
                        return mc.ColumnName;
                }
            }
            else if (typeof(StagingTable).Equals(o.GetType()))
            {
                foreach (StagingColumn sc in ((StagingTable)o).Columns)
                {
                    if (Performance_Optimizer_Administration.Util.generatePhysicalName(sc.Name, maf.builderVersion) + '$' == physicalName)
                        return sc.Name;
                }
            }
            return physicalName;
        }

        private static string getPhysicalName(Object o, string logicalName)
        {
            if (typeof(DimensionTable).Equals(o.GetType()))
            {
                foreach (DimensionColumn dc in ((DimensionTable)o).DimensionColumns)
                {
                    if (dc.Qualify && dc.ColumnName == logicalName)
                        return dc.PhysicalName;
                }
            }
            else if (typeof(MeasureTable).Equals(o.GetType()))
            {
                foreach (MeasureColumn mc in ((MeasureTable)o).MeasureColumns)
                {
                    if (mc.Qualify && mc.ColumnName == logicalName)
                        return mc.PhysicalName;
                }
            }
            else if (typeof(StagingTable).Equals(o.GetType()))
            {
                foreach (StagingColumn sc in ((StagingTable)o).Columns)
                {
                    if (sc.Name == logicalName)
                        return Performance_Optimizer_Administration.Util.generatePhysicalName(sc.Name) + "$";
                }
            }
            return logicalName;
        }

        private static ViewProcessedResults getProfileResults(HttpSessionState session, object o, MainAdminForm maf, int loadNumber)
        {
            QueryConnection conn = null;
            ViewProcessedResults vpr = new ViewProcessedResults();
            try
            {
                Space sp = (Space)session["space"];
                string pname = null;
                string tname = null;
                bool dimension = typeof(DimensionTable).Equals(o.GetType());
                bool measure = typeof(MeasureTable).Equals(o.GetType());
                bool staging = typeof(StagingTable).Equals(o.GetType());
                StringBuilder columns = new StringBuilder();
                List<string> headers = new List<string>();
                headers.Add("Column");
                headers.Add("Count");
                headers.Add("Count Distinct");
                headers.Add("Sum");
                headers.Add("Average");
                headers.Add("Min");
                headers.Add("Max");
                headers.Add("Null Count");

                DatabaseConnection dc = null;
                List<string> colNames = new List<string>();
                if (dimension)
                {
                    pname = ((DimensionTable)o).TableSource.Tables[0].PhysicalName;
                    tname = ((DimensionTable)o).TableName;
                    dc = maf.connection.findConnection(((DimensionTable)o).TableSource.Connection);
                    DataRowView[] drv = maf.dimensionColumnMappingsTablesView.FindRows(new object[] { tname });
                    foreach (DataRowView dr in drv)
                    {
                        if ((bool)dr["AutoGenerated"] && (bool)dr["Qualify"])
                        {
                            if (columns.Length > 0)
                                columns.Append(',');
                            string cname = Performance_Optimizer_Administration.Util.generatePhysicalName((string)dr["ColumnName"], maf.builderVersion);
                            string clname = (string)dr["ColumnName"];
                            string cpname = (string)dr["PhysicalName"];
                            columns.Append("COUNT(" + cpname + ")");
                            columns.Append(",COUNT(DISTINCT " + cpname + ")");
                            DataRowView[] drv2 = maf.dimensionColumnNamesView.FindRows(new object[] { ((DimensionTable)o).DimensionName, (string)dr["ColumnName"] });
                            string dtype = null;
                            if (drv2.Length > 0)
                            {
                                dtype = (string)drv2[0]["DataType"];
                            }
                            if (dtype == "Integer" || dtype == "Number" || dtype == "Float")
                            {
                                if (sp.DatabaseType == "Infobright")
                                {
                                    columns.Append(",SUM(" + cpname + ")");
                                    columns.Append(",AVG(" + cpname + ")");
                                }
                                else
                                {
                                    columns.Append(",SUM(CAST(" + cpname + " AS FLOAT))");
                                    columns.Append(",AVG(CAST(" + cpname + " AS FLOAT))");
                                }
                            }
                            else
                            {
                                columns.Append(",NULL,NULL");
                            }
                            columns.Append(",MIN(" + cpname + ")");
                            columns.Append(",MAX(" + cpname + ")");
                            columns.Append(",SUM(CASE WHEN " + cpname + " IS NULL THEN 1 ELSE 0 END)");
                            colNames.Add(clname);
                        }
                    }
                }
                else if (measure)
                {
                    pname = ((MeasureTable)o).TableSource.Tables[0].PhysicalName;
                    tname = ((MeasureTable)o).TableName;
                    dc = maf.connection.findConnection(((MeasureTable)o).TableSource.Connection);
                    DataRowView[] drv = maf.measureColumnMappingsTablesView.FindRows(new object[] { tname });
                    foreach (DataRowView dr in drv)
                    {
                        if ((bool)dr["AutoGenerated"] && (bool)dr["Qualify"])
                        {
                            if (columns.Length > 0)
                                columns.Append(',');
                            string cname = Performance_Optimizer_Administration.Util.generatePhysicalName((string)dr["ColumnName"], maf.builderVersion);
                            string clname = (string)dr["ColumnName"];
                            string cpname = (string)dr["PhysicalName"];
                            columns.Append("COUNT(" + cpname + ")");
                            columns.Append(",COUNT(DISTINCT " + cpname + ")");
                            DataRowView[] drv2 = maf.measureColumnNamesView.FindRows(new object[] { (string)dr["ColumnName"] });
                            string dtype = null;
                            if (drv2.Length > 0)
                            {
                                dtype = (string)drv2[0]["DataType"];
                            }
                            if (dtype == "Integer" || dtype == "Number" || dtype == "Float")
                            {
                                if (sp.DatabaseType == "Infobright")
                                {
                                    columns.Append(",SUM(" + cpname + ")");
                                    columns.Append(",AVG(" + cpname + ")");
                                }
                                else
                                {
                                    columns.Append(",SUM(CAST(" + cpname + " AS FLOAT))");
                                    columns.Append(",AVG(CAST(" + cpname + " AS FLOAT))");
                                }
                            }
                            else
                            {
                                columns.Append(",NULL,NULL");
                            }
                            columns.Append(",MIN(" + cpname + ")");
                            columns.Append(",MAX(" + cpname + ")");
                            columns.Append(",SUM(CASE WHEN " + cpname + " IS NULL THEN 1 ELSE 0 END)");
                            colNames.Add(clname);
                        }
                    }
                }
                else if (staging)
                {
                    pname = ((StagingTable)o).Name;
                    tname = ((StagingTable)o).Name.Substring(3);
                    StagingTable st = (StagingTable)o;
                    Dictionary<string, LocalETLConfig> leConfigs = BirstConnectUtil.getLocalETLConfigsForSpace(sp);
                    foreach (string lg in st.LoadGroups)
                    {
                        if (lg.StartsWith(LiveAccessUtil.LIVE_ACCESS_LOAD_GROUP_PREFIX) && leConfigs.ContainsKey(lg))
                        {
                            dc = maf.connection.findConnection(leConfigs[lg].getLocalETLConnection());
                            break;
                        }
                    }
                    foreach (StagingColumn sc in st.Columns)
                    {
                        if (sc.SourceFileColumn == null)
                            continue;
                        if (columns.Length > 0)
                            columns.Append(',');
                        string cname = sc.Name;
                        string cpname = Performance_Optimizer_Administration.Util.generatePhysicalName(sc.Name, maf.builderVersion) + '$';
                        columns.Append("COUNT(" + cpname + ")");
                        columns.Append(",COUNT(DISTINCT " + cpname + ")");
                        string dtype = sc.DataType;
                        if (dtype == "Integer" || dtype == "Float" || dtype == "Number")
                        {
                            if (sp.DatabaseType == "Infobright")
                            {
                                columns.Append(",SUM(" + cpname + ")");
                                columns.Append(",AVG(" + cpname + ")");
                            }
                            else
                            {
                                columns.Append(",SUM(CAST(" + cpname + " AS FLOAT))");
                                columns.Append(",AVG(CAST(" + cpname + " AS FLOAT))");
                            }
                        }
                        else
                        {
                            columns.Append(",NULL,NULL");
                        }
                        columns.Append(",MIN(" + cpname + ")");
                        columns.Append(",MAX(" + cpname + ")");
                        columns.Append(",SUM(CASE WHEN " + cpname + " IS NULL THEN 1 ELSE 0 END)");
                        colNames.Add(cname);
                    }
                }
                string query = null;
                if (Database.supportsTop(sp.DatabaseDriver))
                    query = "SELECT " + columns + " FROM (SELECT TOP 50000 * FROM " + sp.Schema + "." + pname +
                        (loadNumber > 0 ? " WHERE LOAD_ID=" + loadNumber : ") T");
                else
                    query = "SELECT " + columns + " FROM (SELECT * FROM " + sp.Schema + "." + pname +
                    (loadNumber > 0 ? " WHERE LOAD_ID=" + loadNumber : " LIMIT 50000) T");
                Global.systemLog.Info(query);
                List<string[]> results = new List<string[]>();
                int NUM_AGGS = 7;
                if (dc != null && dc.Realtime)
                {
                    results.Add(headers.ToArray());
                    Object[][] res = LiveAccessUtil.executeLiveAccessQuery(session, null, sp, dc.Name, query, true);
                    if (res != null && res.Length > 1)
                    {
                        for (int r = 1; r < res.Length; r++)
                        {
                            for (int i = 0; i < colNames.Count; i++)
                            {
                                string[] dr = new string[NUM_AGGS + 1];
                                dr[0] = colNames[i];
                                for (int j = 0; j < 7; j++)
                                {
                                    if (res[r][(i * NUM_AGGS) + j] != null)
                                        dr[j + 1] = (string)res[r][(i * NUM_AGGS) + j];
                                }
                                results.Add(dr);
                            }
                        }
                    }
                    vpr.Results = results.ToArray();
                }
                else
                {
                    conn = Util.getConnectionWithVariedRetries(sp.getFullConnectString(), sp.DatabaseType);
                    QueryCommand cmd = conn.CreateCommand();
                    cmd.CommandText = query;
                    QueryReader reader = cmd.ExecuteReader();
                    results.Add(headers.ToArray());
                    if (reader.Read())
                    {
                        for (int i = 0; i < colNames.Count; i++)
                        {
                            string[] dr = new string[NUM_AGGS + 1];
                            dr[0] = colNames[i];
                            for (int j = 0; j < 7; j++)
                            {
                                if (!reader.IsDBNull((i * NUM_AGGS) + j))
                                    dr[j + 1] = reader.GetValue((i * NUM_AGGS) + j).ToString();
                            }
                            results.Add(dr);
                        }
                    }
                    reader.Close();
                    vpr.Results = results.ToArray();
                }

            }
            catch (Exception ex)
            {
                Global.systemLog.Warn(ex, ex);
                vpr.ErrorMessage = "Unable to retrieve results of query";
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
            return vpr;
        }

        public static ViewProcessedResults getColumnProperties(HttpSessionState session, string type, string tableName)
        {
            MainAdminForm maf = (MainAdminForm)session["MAF"];
            if (maf == null)
                return null;
            Space sp = (Space)session["space"];
            QueryConnection conn = null;
            List<string[]> results = new List<string[]>();
            ViewProcessedResults vpr = new ViewProcessedResults();
            try
            {
                conn = Util.getConnectionWithVariedRetries(sp.getFullConnectString(), sp.DatabaseType);
                string pname = null;
                if (type == "level")
                {
                    DimensionTable dt = getDimensionTableFromName(maf, tableName);
                    if (dt != null)
                    {
                        if (dt.TableSource != null && dt.TableSource.Tables != null && dt.TableSource.Tables.Length == 1)
                            pname = dt.TableSource.Tables[0].PhysicalName;
                    }
                }
                else if (type == "grain")
                {
                    MeasureTable mt = getMeasureTableFromName(session, sp, maf, tableName);
                    if (mt != null)
                    {
                        if (mt.TableSource != null && mt.TableSource.Tables != null && mt.TableSource.Tables.Length == 1)
                            pname = mt.TableSource.Tables[0].PhysicalName;
                    }
                }
                else if (type == "staging")
                {
                    StagingTable st = getStagingTableFromName(maf, tableName);
                    if (st != null)
                    {
                        pname = st.Name;
                    }
                }
                object[] result = null;
                if (pname != null)
                    result = Database.showTable(conn, sp.Schema, pname);
                // Get table size
                foreach (object[] row in result)
                {
                    results.Add(new string[] { row[0].ToString(), row[1].ToString(), ((int)row[2]).ToString(), ((bool)row[3]).ToString(), ((long)row[4]).ToString() });
                }
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
            vpr.Results = results.ToArray();
            return vpr;
        }

        public static ViewProcessedResults convertColumn(HttpSessionState session, string type, string tableName, string colName, string dataType, int width, bool disk)
        {
            MainAdminForm maf = (MainAdminForm)session["MAF"];
            if (maf == null)
                return null;
            Space sp = (Space)session["space"];
            QueryConnection conn = null;
            //List<string[]> results = new List<string[]>();
            ViewProcessedResults vpr = new ViewProcessedResults();
            vpr.ErrorMessage = null;
            try
            {
                conn = Util.getConnectionWithVariedRetries(sp.getFullConnectString(), sp.DatabaseType);
                string pname = null;
                if (type == "level")
                {
                    DimensionTable dt = getDimensionTableFromName(maf, tableName);
                    if (dt != null)
                    {
                        if (dt.TableSource != null && dt.TableSource.Tables != null && dt.TableSource.Tables.Length == 1)
                            pname = dt.TableSource.Tables[0].PhysicalName;
                    }
                }
                else if (type == "grain")
                {
                    MeasureTable mt = getMeasureTableFromName(session, sp, maf, tableName);
                    if (mt != null)
                    {
                        if (mt.TableSource != null && mt.TableSource.Tables != null && mt.TableSource.Tables.Length == 1)
                            pname = mt.TableSource.Tables[0].PhysicalName;
                    }
                }
                else if (type == "staging")
                {
                    StagingTable st = getStagingTableFromName(maf, tableName);
                    if (st != null)
                    {
                        pname = st.Name;
                    }
                }
                if (pname != null)
                {
                    string result = Database.alterColumn(conn, sp.Schema, pname, colName, dataType, width, disk);
                    if (result != "OK")
                        vpr.ErrorMessage = result;
                }
            }
            finally
            {                
                ConnectionPool.releaseConnection(conn);
            }
            return vpr; 
        }
    }
}