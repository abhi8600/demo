﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace Acorn
{
    public class XmlUtils
    {
        public static string CDATA_OPEN = "<![CDATA[";
        public static string CDATA_CLOSE = "]]>";

        public static string trimCData(string xmlString)
        {
            if (xmlString.StartsWith(CDATA_OPEN))
            {
                xmlString = xmlString.Substring(CDATA_OPEN.Length);
            }
            if (xmlString.EndsWith(CDATA_CLOSE))
            {
                xmlString = xmlString.Substring(0, xmlString.Length - CDATA_CLOSE.Length);
            }
            return xmlString;
        }
   }
}