﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Performance_Optimizer_Administration;
using System.Web.SessionState;
using System.Data.Odbc;
using System.Data;
using Acorn.Utils;
using Acorn.DBConnection;

namespace Acorn
{
    public class ProcessLoad
    {
        private static string mainSchema = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["MainSchema"];
        public static string createtimecmd = System.Web.Configuration.WebConfigurationManager.AppSettings["CreateTimeCommand"];
        private static string contentDir = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["ContentDirectory"];

        public static ProcessOptions getProcessInitData(HttpSessionState session, bool autoMode)
        {

            ProcessOptions processOptions = new ProcessOptions();
            ErrorOutput errorOutput = new ErrorOutput();
            processOptions.ErrorOutput = errorOutput;

            if (!canProceed(session))
            {
                errorOutput.ErrorType = ResponseMessages.ERROR_TYPE_OTHER;
                errorOutput.ErrorMessage = "Unable to get space details.";
                return processOptions;
            }
            
           
            Space sp = (Space)session["space"];
            MainAdminForm maf = (MainAdminForm)session["MAF"];
            bool allowCreateDashboards = !sp.DiscoveryMode;

            processOptions.CreateDashboards = canCreateDashboards(session, allowCreateDashboards);
            processOptions.RecreateDashboards = !canCreateDashboards(session, allowCreateDashboards) && allowCreateDashboards;

            string loadGroup = getLoadGroup();
            int loadNumber = getLoadNumber(sp, loadGroup);
            // Get the updated status. Takes care of the use case :
            // Where schedule processing is going on (or via BirstConnect) and the status in the session needs to be updated to display the            
            bool getStatus = session["status"] == null ? true : false;
            if (!getStatus)
            {
                Status.StatusResult status = (Status.StatusResult)session["status"];
                // this is to refresh the status in case user went from acorn to sfdc status and status gets reset
                if (!Status.isRunningCode(status.code) || status.code == Status.StatusCode.LoadingAnotherLoadGroup)
                {
                    // if the status is of anotherLoadingGroup, make sure it not the present one
                    getStatus = true;
                }
            }
            
            if (getStatus)
            {
                session["status"] = new Status.StatusResult(Status.StatusCode.None);
                session["status"] = Status.getLoadStatus(session, sp, loadNumber + 1, loadGroup);
            }

            bool fetchOldWay = true;
            Status.StatusResult updatedResultFromExternalScheduler = null;
            if (Util.useExternalSchedulerForProcess(sp, maf))
            {
                updatedResultFromExternalScheduler = setRunningStatus2(session, maf, sp, processOptions, loadNumber, loadGroup, false);
                fetchOldWay = updatedResultFromExternalScheduler != null && Status.isRunningCode(updatedResultFromExternalScheduler.code) ? false : true;
            }

            // a hack to make sure that if the user changes performance engines from 5.1 to pre 5.1 , we don't loose the 
            // status check logic. Also manual processing is bypassed
            if(fetchOldWay)
            {
                // if status is retrieved from both the external scheduler as well as the old, then resolve the use case
                // where status from scheduler returns failed while old way does not. It can happen in number of cases but 
                // a false alarm is sent if the scheduled process had failed while the next manual one is successful. 
                // Resolve that by ignoring the status with the old timestamp
                if (Status.isFailedStatusObsolete(sp, loadGroup, updatedResultFromExternalScheduler))
                {
                    processOptions.ErrorOutput = null;
                }
                setRunningStatus(session, maf, sp, mainSchema, processOptions, loadGroup);
            }
            setWarningMessageForScriptGroup(processOptions, sp, session);
            processOptions.LoadNumberProcessed = loadNumber.ToString();
            return processOptions;
        }

        private static void processFailedStatus2(HttpSessionState session, MainAdminForm maf, Space sp,
            Status.StatusResult status, int loadNumber, string loadGroup, ErrorOutput errorOutput)
        {
            Status.StatusStep ss = null;
            if (status.substeps != null)
            {
                foreach (Status.StatusStep step in status.substeps)
                {
                    if (step.result == Status.SCRIPTFAILURE)
                    {
                        ss = step;
                        break;
                    }
                }
            }
            
            if (ss == null)
            {
                // check for process not started
                if (Status.checkForLoadStartedFailureEntry(session, sp, loadNumber + 1))
                {
                    errorOutput.ErrorType = ResponseMessages.ERROR_TYPE_GENERAL;
                    errorOutput.ErrorMessage = ResponseMessages.ERROR_MSG_PROCESS_NOT_STARTED;
                }
                else
                {
                    if (status.message != null && status.message.Length > 0)
                    {
                        errorOutput.ErrorType = ResponseMessages.ERROR_TYPE_GENERAL;
                        errorOutput.ErrorMessage = status.message;
                    }
                    else
                    {
                        errorOutput.ErrorType = ResponseMessages.ERROR_TYPE_GENERAL;
                        errorOutput.ErrorMessage = ResponseMessages.ERROR_MSG_GENERAL;
                    }
                }
            }
            else
            {
                string errorSource = "";
                foreach (StagingTable st in maf.stagingTableMod.getAllStagingTables())
                {
                    if (ss.substep == loadGroup + ": " + st.Name)
                    {
                        string s = st.SourceFile;
                        int index = s.LastIndexOf('.');
                        if (index >= 0)
                            s = s.Substring(0, index);
                        errorSource = s;
                        break;
                    }
                }

                errorOutput.ErrorType = ResponseMessages.ERROR_TYPE_SCRIPT;
                errorOutput.ErrorMessageSource = errorSource;
                errorOutput.ErrorMessage = ss.message;
            }

            configureRetryLoadOptions(sp, loadGroup, loadNumber, errorOutput);
        }

        private static Status.StatusResult setRunningStatus2(HttpSessionState session, MainAdminForm maf, Space sp, ProcessOptions processOptions, int loadNumber, string loadGroup, bool fetchFromTxn)
        {           

            ErrorOutput errorOutput = processOptions.ErrorOutput;
            if (errorOutput == null)
            {
                errorOutput = new ErrorOutput();
                processOptions.ErrorOutput = errorOutput;
            }

            Status.StatusResult status = Status.getStatusUsingExternalScheduler(sp, loadNumber, loadGroup, fetchFromTxn);
            
            if (status != null && status.code == Status.StatusCode.Failed)
            {
                // need to separate out the function since it could be called later during Running Status updated check
                processFailedStatus2(session, maf, sp, status, loadNumber, loadGroup, errorOutput);
            }
            
            // Get whether anything is still running
            if (status != null && (status.code == Status.StatusCode.Ready) || (status.code == Status.StatusCode.Complete)) 
            {
                if (session["autopublish"] != null)
                {
                    session.Remove("autopublish");
                    if (maf.quickDashboards != null && maf.quickDashboards.Count > 0)
                        processOptions.RedirectModule = "Dashboards";
                    else
                        processOptions.RedirectModule = "Adhoc";
                }
                processOptions.CurrentState = ResponseMessages.STATUS_DONE;
                processOptions.LoadNumberProcessed = loadNumber.ToString();
            }

            if (status != null && Status.isRunningCode(status.code))
            {
                processOptions.CurrentState = ResponseMessages.STATUS_PROCESSING;
            }
            else
            {
                processOptions.CurrentState = ResponseMessages.STATUS_AVAIABLE;
            }

            /*
            if (session != null)
            {
                session["status"] = status;
            }
             */
            return status;
        }
        
        public static PublishHistoryResult getPublishHistory(HttpSessionState session )
        {
            PublishHistoryResult publishResult = new PublishHistoryResult();
            ErrorOutput errorOutput = new ErrorOutput();
            publishResult.Error = errorOutput;

            if (!canProceed(session))
            {
                errorOutput.ErrorType = ResponseMessages.ERROR_TYPE_OTHER;
                errorOutput.ErrorMessage = "Unable to get space details.";
                return publishResult;
            }
            
            Space sp = (Space)session["space"];
            User u = Util.getSessionUser(session);
            QueryConnection conn = null;
            try
            {
                conn = ConnectionPool.getConnection();
                MainAdminForm maf = (MainAdminForm)session["MAF"];
                HashSet<string> loadGroups = Util.getLoadGroups(maf);
                DataTable consolidatedDt = new DataTable();
                consolidatedDt.Columns.Add("DateTime", typeof(DateTime));
                consolidatedDt.Columns.Add("LoadNumber", typeof(int));
                consolidatedDt.Columns.Add("Step");
                consolidatedDt.Columns.Add("Substep");
                consolidatedDt.Columns.Add("ResultCode", typeof(int));
                consolidatedDt.Columns.Add("Result");
                consolidatedDt.Columns.Add("PublishDate", typeof(DateTime));
                consolidatedDt.Columns.Add("LoadGroup");
                consolidatedDt.Columns.Add("PublishStartTime", typeof(DateTime));
                consolidatedDt.Columns.Add("PublishEndTime", typeof(DateTime));
                consolidatedDt.Columns.Add("ProcessingGroups");
                loadGroups.Add(Util.LOAD_GROUP_NAME);

                foreach (string lg in loadGroups)
                {                    
                    if (lg == SalesforceLoader.LOAD_GROUP_NAME)
                    {
                        continue;
                    }
                    DataTable dt = publishHistory(session, sp, conn, lg);
                    if (dt != null)
                    {
                        foreach (DataRow dr in dt.Rows)
                        {
                            DataRow newdr = consolidatedDt.NewRow();
                            newdr["DateTime"] = dr["DateTime"];
                            newdr["LoadNumber"] = dr["LoadNumber"];
                            newdr["Step"] = dr["Step"];
                            newdr["Substep"] = dr["Substep"];
                            newdr["ResultCode"] = dr["ResultCode"];
                            newdr["Result"] = dr["Result"];
                            newdr["PublishDate"] = dr["PublishDate"];
                            newdr["LoadGroup"] = dr["LoadGroup"];
                            if(dt.Columns.Contains("PublishStartTime"))
                            {
                                newdr["PublishStartTime"] = dr["PublishStartTime"];
                            }
                            if(dt.Columns.Contains("PublishEndTime"))
                            {
                                newdr["PublishEndTime"] = dr["PublishEndTime"];
                            }
                            if (dt.Columns.Contains("ProcessingGroups"))
                            {
                                newdr["ProcessingGroups"] = dr["ProcessingGroups"];
                            }
                            consolidatedDt.Rows.Add(newdr);
                        }
                    }
                }
                setPublishResultObject(consolidatedDt, publishResult, sp, u);
                return publishResult;
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
        }

        private static void setPublishResultObject(DataTable dt, PublishHistoryResult publishResult, Space sp, User u)
        {
                        
            List<PublishHistory> result = new List<PublishHistory>();
            
            if (dt.Rows.Count > 0)
            {
               
                foreach (DataRow dr in dt.Rows)
                {
                    PublishHistory publishRow = new PublishHistory();    

                    foreach (DataColumn column in dt.Columns)
                    {
                        if (dr[column] == null)
                        {
                            continue;
                        }
                            
                        if (column.ColumnName == "DateTime")
                        {
                            string date = dr[column].ToString();
                            publishRow.DateTime = date.Split(' ')[0];
                        }
                        if (column.ColumnName == "PublishDate")
                        {
                            string date = dr[column].ToString();
                            publishRow.PublishLoadDate = date.Split(' ')[0];
                        }
                        if (column.ColumnName == "LoadNumber")
                        {
                            publishRow.LoadNumber = dr[column].ToString();
                        }
                        if (column.ColumnName == "Step")
                        {
                            publishRow.Step = dr[column].ToString();
                        }
                        if (column.ColumnName == "Substep")
                        {
                            publishRow.Substep = dr[column].ToString();
                        }
                        if (column.ColumnName == "ResultCode")
                        {
                            publishRow.ResultCode = dr[column].ToString();
                        }
                        if (column.ColumnName == "Result")
                        {
                            publishRow.Result = dr[column].ToString();
                        }
                        if (column.ColumnName == "LoadGroup")
                        {
                            publishRow.LoadGroup = dr[column].ToString();
                        }

                        if (column.ColumnName == "PublishStartTime")
                        {
                            DateTime publishStartTime = (DateTime)dr[column];
                            if (publishStartTime != DateTime.MinValue)
                            {
                                //publishRow.PublishStartTime = LocaleUtils.getLocaleAdjustedDateTimeString(u, sp, publishStartTime);
                                publishRow.PublishStartTime = publishStartTime.ToString("g");
                            }
                        }
                        if (column.ColumnName == "PublishEndTime")
                        {                           
                            DateTime publishEndTime = (DateTime)dr[column];
                            if (publishEndTime != DateTime.MinValue)
                            {
                                //publishRow.PublishEndTime = LocaleUtils.getLocaleAdjustedDateTimeString(u, sp, publishEndTime);
                                publishRow.PublishEndTime = publishEndTime.ToString("g");
                            }
                        }
                        if (column.ColumnName == "ProcessingGroups")
                        {
                            publishRow.ProcessingGroups = dr[column].ToString();
                        }
                    }

                    result.Add(publishRow);
                }                
            }
            publishResult.PublishHistoryOutput = result.ToArray();
        }

        public static GenericResponse startPublishing(HttpSessionState session,HttpRequest request,
            string loadDateString, String loadGroup, string[] subGroups, bool retryFailedLoad, bool independentMode, bool reprocessmodified) 
        {
            GenericResponse publishProcess = new GenericResponse();            
            ErrorOutput errorOutput = new ErrorOutput();
            publishProcess.Error = errorOutput;
            
            if(!canProceed(session))
            {                
                errorOutput.ErrorType = ResponseMessages.ERROR_TYPE_OTHER;
                errorOutput.ErrorMessage = "Unable to get space details.";              
                return publishProcess;
            }

            ApplicationLoader al = initializePublishing(session, request, loadDateString, loadGroup, subGroups, retryFailedLoad, independentMode, errorOutput, reprocessmodified);
            Space sp = Util.getSessionSpace(session);
            if (al != null && ApplicationLoader.isSpaceBeingPublished(sp))
            {
                errorOutput.ErrorType = ResponseMessages.ERROR_TYPE_PUBLISH;
                errorOutput.ErrorMessage = "Cannot publish space, it is being published currently.";
                return publishProcess;
            }
            if (isSpaceBusyInOtherOps(sp, publishProcess))
            {
                return publishProcess;
            }


            if(publishProcess.Error != null && publishProcess.Error.ErrorMessage != null)
            {
                return publishProcess;
            }
            
            Status.StatusResult sr = new Status.StatusResult(Status.StatusCode.Started);//new Status.StatusResult(Status.StatusCode.GenerateSchema);
            session["status"] = sr;
            al.retryFailedLoad = retryFailedLoad;
            if (Util.useExternalSchedulerForManualProcessing(sp, Util.getSessionMAF(session)))
            {
                // We are not going to start the load.
                // This will just clean out the txn_command_history table so that no false status are given during deletelast/failed use cases
                al.load(false, session, false, false, false, reprocessmodified);
                SchedulerUtils.schedulePublishJob(sp, Util.getSessionUser(session), false, al.getLoadNumber(), al.getLoadGroup(), al.getLoadDate(), al.SubGroups, al.retryFailedLoad, false);                
            }
            else
            {
                al.load(false, session, false, true, true, reprocessmodified);
            }
            sr = (Status.StatusResult) session["status"];
            if (sr.statusCode == Status.StatusCode.Failed.ToString() && sr.message != null)
                publishProcess.Error.ErrorMessage = sr.message;
            return publishProcess;
        }

        public static DashboardConfiguration autoPublish(HttpSessionState session, HttpRequest request)
        {
            DashboardConfiguration response = new DashboardConfiguration();
            ErrorOutput error = new ErrorOutput();
            response.ErrorOutput = error;
            
            Space sp = (Space)session["space"];
            if (sp == null)
            {
                error.ErrorType = ResponseMessages.ERROR_TYPE_OTHER;
                error.ErrorMessage = "Unable to get space details. Please try again";
                return response;
            }

            if (sp.Automatic && sp.LoadNumber == 0)
            {
                DateTime loadDate = DateTime.Now;
                session["autopublish"] = true;
                Status.StatusResult sr = new Status.StatusResult(Status.StatusCode.BuildingDashboards);
                session["status"] = sr;
                response = configureDashboardOptions(session, request, DateTime.Now.ToString("d"), Util.LOAD_GROUP_NAME, null, false, false);
                if (response.ErrorOutput != null && response.ErrorOutput.ErrorMessage != null)
                {
                    session.Remove("autopublish");
                    session["status"] = new Status.StatusResult(Status.StatusCode.Ready);
                }
            }
            else
            {
                error.ErrorType = ResponseMessages.ERROR_TYPE_GENERAL;
                error.ErrorMessage = "Unable to auto publish.";

            }

            return response;
        }

        public static DashboardConfiguration configureDashboardOptions(HttpSessionState session, HttpRequest request, string loadDateString, String loadGroup, string[] subGroups,
            bool retryFailedLoad, bool independentMode)
        {

            DashboardConfiguration dashboardConfiguration = new DashboardConfiguration();        
            ErrorOutput errorOutput = new ErrorOutput();
            dashboardConfiguration.ErrorOutput = errorOutput;            
 
            if(!canProceed(session))
            {
                errorOutput.ErrorType = ResponseMessages.ERROR_TYPE_OTHER;
                errorOutput.ErrorMessage = "Unable to get space details. Please try again";
                return dashboardConfiguration;
            }

            ApplicationLoader al = initializePublishing(session, request, loadDateString, loadGroup, subGroups, retryFailedLoad, independentMode, errorOutput, false);
            al.retryFailedLoad = retryFailedLoad;
            if(errorOutput != null &&  errorOutput.ErrorMessage != null )
            {
                return dashboardConfiguration;
            }

            MainAdminForm maf = Util.getSessionMAF(session);
            Space sp = Util.getSessionSpace(session);
            session["loader"] = al;
            BuildDashboards bd = null;
            al.BuildDashboards = bd;
            ScanBuffer scanBuff = null;
            Acorn.BuildDashboards.EvaluatedMeasure[] emlist = null;
            if (session["scanbuff"] == null)
            {
                bd = new BuildDashboards(maf, sp.Directory + "\\data");
                scanBuff = new ScanBuffer();
                al.ScanBuff = scanBuff;
                emlist = bd.getMeasures(sp.Directory + "\\data", scanBuff);
            }
            else
            {
                scanBuff = (ScanBuffer)session["scanbuff"];
                bd = (BuildDashboards)session["builddashboards"];
                emlist = bd.getMeasures(sp.Directory + "\\data", scanBuff);
                session.Remove("scanbuff");
                session.Remove("builddashboards");
            }
            if (emlist.Length > 0)
            {
                al.BuildDashboards = bd;
                DataTable dt = new DataTable();
                dt.Columns.Add("Name");
                dt.Columns.Add("SumVisible", typeof(bool));
                dt.Columns.Add("SumChecked", typeof(bool));
                dt.Columns.Add("AvgVisible", typeof(bool));
                dt.Columns.Add("AvgChecked", typeof(bool));
                dt.Columns.Add("CountVisible", typeof(bool));
                dt.Columns.Add("CountChecked", typeof(bool));
                foreach (BuildDashboards.EvaluatedMeasure em in emlist)
                {
                    DataRow dr = dt.NewRow();
                    dr[0] = em.sc.Name;
                    dr[1] = Array.IndexOf<string>(em.sc.TargetAggregations, "SUM") >= 0;
                    dr[2] = (bool)dr[1];
                    dr[5] = Array.IndexOf<string>(em.sc.TargetAggregations, "COUNT") >= 0;
                    dr[6] = (bool)dr[5] && !((bool)dr[2]);
                    dr[3] = Array.IndexOf<string>(em.sc.TargetAggregations, "AVG") >= 0;
                    dr[4] = (bool)dr[3] && !((bool)dr[2] || (bool)dr[6]);
                    dt.Rows.Add(dr);
                }

                 setDashboardResultObject(dt, dashboardConfiguration);
            }
            
            return dashboardConfiguration;
        }

        private static void setDashboardResultObject(DataTable dt, DashboardConfiguration dashboardConfiguration)
        {            
            List<DashboardOption> result = new List<DashboardOption>();
            if (dt.Rows.Count > 0)
            {                                
                foreach (DataRow dr in dt.Rows)
                {
                    DashboardOption dashboardOption = new DashboardOption();

                    foreach (DataColumn column in dt.Columns)
                    {
                        if (column.ColumnName == "Name")
                            dashboardOption.Name = dr[column].ToString();
                        if (column.ColumnName == "SumVisible")
                            dashboardOption.SumVisible = (bool)dr[column];
                        if (column.ColumnName == "SumChecked")
                            dashboardOption.SumChecked = (bool)dr[column];
                        if (column.ColumnName == "AvgVisible")
                            dashboardOption.AvgVisible = (bool)dr[column];
                        if (column.ColumnName == "AvgChecked")
                            dashboardOption.AvgChecked = (bool)dr[column];
                        if (column.ColumnName == "CountVisible")
                            dashboardOption.CountVisible = (bool)dr[column];
                        if (column.ColumnName == "CountChecked")
                            dashboardOption.CountChecked = (bool)dr[column];
                    }

                    result.Add(dashboardOption);
                }
            }
            dashboardConfiguration.DashboardOptions = result.ToArray();            
        }
               
        public static GenericResponse publishWithDashboard(HttpSessionState session, string[] measureOptions) {
            // the signature should contain the selected data
            // output needs to be an array of measures for dashboard creation

            GenericResponse publishProcess = new GenericResponse();            
            ErrorOutput errorOutput = new ErrorOutput();
            publishProcess.Error = errorOutput;
           
            if(!canProceed(session))
            {
                errorOutput.ErrorType = ResponseMessages.ERROR_TYPE_OTHER;
                errorOutput.ErrorMessage = "Unable to get space details. Please try again";                                
                return publishProcess;
            }        

            ApplicationLoader al = (ApplicationLoader)session["loader"];
            session.Remove("loader");
            if (al == null)
            {
                errorOutput.ErrorType = ResponseMessages.ERROR_TYPE_GENERAL;
                errorOutput.ErrorMessage = "Unable to process set of data";
                publishProcess.Error = errorOutput;
                return publishProcess;
            }
            Space sp = Util.getSessionSpace(session);
            if (ApplicationLoader.isSpaceBeingPublished(sp))
            {
                errorOutput.ErrorType = ResponseMessages.ERROR_TYPE_PUBLISH;
                errorOutput.ErrorMessage = "Cannot publish space, it is being published currently.";
                return publishProcess;
            }

            if (isSpaceBusyInOtherOps(sp, publishProcess))
            {
                return publishProcess;
            }

            List<string> measureList = measureOptions.ToList();
            BuildDashboards bd = al.BuildDashboards;
            Acorn.BuildDashboards.EvaluatedMeasure[] emlist = bd.getMeasures(sp.Directory + "\\data", al.ScanBuff);
            int count = 0;
            foreach (string option in measureList)
            {
                string rule = null;
                if ( option.Equals("SumButton"))
                {
                    rule = "SUM";
                }
                else if (option.Equals("AvgButton"))
                {
                    rule = "AVG";
                }
                else if (option.Equals("CountButton"))
                {    
                    rule = "COUNT";
                }
                if (rule != null)
                {
                    emlist[count].aggregationRule = rule;
                }
                else
                {
                    emlist[count].ignore = true;
                }
                count++;
            }
            
            Status.StatusResult sr = new Status.StatusResult(Status.StatusCode.BuildingDashboards);
            session["status"] = sr;
            // we still initiliaze appLoader so that we could get errors around LoadAuthorization. T
            // This is useful error message at the time when the user kicks off the load.
            if (Util.useExternalSchedulerForManualProcessing(sp, Util.getSessionMAF(session)))
            {
                // This ensures that the txn_command_history table gets cleaned out in case of "deleteLast/Failed" scneario
                // Also dashboards are build for the live access connection
                al.load(true, session, false, false, false, false);
                SchedulerUtils.schedulePublishJob(sp, Util.getSessionUser(session), true, al.getLoadNumber(), al.getLoadGroup(), al.getLoadDate(), al.SubGroups, al.retryFailedLoad, false);
            }
            else
            {
                al.load(true, session);
            }
            
            return publishProcess;
        }

        private static bool isSpaceBusyInOtherOps(Space sp, GenericResponse response)
        {
            bool isBusySpace = false;
            int spaceOpID = SpaceOpsUtils.getSpaceAvailability(sp);
            if (!SpaceOpsUtils.isSpaceAvailable(spaceOpID))
            {
                if(response.Error != null)
                    response.Error = new ErrorOutput();
                response.Error.ErrorType = ResponseMessages.ERROR_TYPE_GENERAL;
                response.Error.ErrorMessage = "Cannot publish space. " + SpaceOpsUtils.getAvailabilityMessage(null, spaceOpID);
                isBusySpace = true;
            }
            return isBusySpace;
        }

        public static ProcessOptions getProgressStatus2(HttpSessionState session)
        {
            ProcessOptions processOptions = new ProcessOptions();
            ErrorOutput errorOutput = new ErrorOutput();
            processOptions.ErrorOutput = errorOutput;

            if (!canProceed(session))
            {
                errorOutput.ErrorType = ResponseMessages.ERROR_TYPE_OTHER;
                errorOutput.ErrorMessage = "Unable to get space details.";
                return processOptions;
            }

            Space sp = Util.getSessionSpace(session);
            MainAdminForm maf = Util.getSessionMAF(session);

            string loadGroup = Util.LOAD_GROUP_NAME;
            int loadNumber = getLoadNumber(sp, loadGroup);
            Status.StatusResult statusResult = setRunningStatus2(session, maf, sp, processOptions, loadNumber, loadGroup, true);
            
            if (processOptions.ErrorOutput != null && processOptions.ErrorOutput.ErrorMessage == null)
            {
                List<string> result = new List<string>();
                if (statusResult.code == Status.StatusCode.LoadWarehouse)
                {
                    processOptions.DetailLink = String.Format("{0}&{1:MM-dd-yyyy}&{2}", loadNumber + 1, DateTime.Now, loadGroup);
                }
                bool includeDashboards = (statusResult.code == Status.StatusCode.BuildingDashboards ? true : false);
                int percentComplete = Status.getPercentComplete(statusResult.code, includeDashboards);
                processOptions.progressPercentage = percentComplete.ToString();
                string progressText = Status.getPhaseLabel(statusResult, includeDashboards);
                processOptions.progressText = progressText;
                processOptions.StopProcessing = Status.getStopProcessing(statusResult.code);
            }

            setWarningMessageForScriptGroup(processOptions, sp, session);
            return processOptions;
        }
        


        public static ProcessOptions getProgressStatus(HttpSessionState session)
        {
            ProcessOptions processOptions = new ProcessOptions();
            ErrorOutput errorOutput = new ErrorOutput();
            processOptions.ErrorOutput = errorOutput;

            if (!canProceed(session))
            {
                errorOutput.ErrorType = ResponseMessages.ERROR_TYPE_OTHER;
                errorOutput.ErrorMessage = "Unable to get space details.";
                return processOptions;
            }            

            Space sp = (Space)session["space"];
            MainAdminForm maf = (MainAdminForm)session["MAF"];

            string loadGroup = Util.LOAD_GROUP_NAME;

            setRunningStatus(session, maf, sp, mainSchema, processOptions, loadGroup);

            if (processOptions.ErrorOutput != null && processOptions.ErrorOutput.ErrorMessage == null)
            {
                List<string> result = new List<string>();
                Status.StatusResult statusResult = (Status.StatusResult)session["status"];
                if (statusResult.code == Status.StatusCode.LoadWarehouse)
                {
                    processOptions.DetailLink = String.Format("{0}&{1:MM-dd-yyyy}&{2}", sp.LoadNumber + 1, DateTime.Now, loadGroup);                    
                }
                bool includeDashboards = (statusResult.code == Status.StatusCode.BuildingDashboards ? true : false);
                int percentComplete = Status.getPercentComplete(statusResult.code, includeDashboards);
                processOptions.progressPercentage = percentComplete.ToString();
                string progressText = Status.getPhaseLabel(statusResult, includeDashboards);
                processOptions.progressText = progressText;
                processOptions.StopProcessing = Status.getStopProcessing(statusResult.code);
            }

            setWarningMessageForScriptGroup(processOptions, sp, session);
            return processOptions;
        }

        private static ApplicationLoader initializePublishing(HttpSessionState session, HttpRequest request, string loadDateString, String loadGroup, string[] subGroups,
            bool retryFailedLoad, bool independentMode, ErrorOutput errorOutput, bool reprocessmodified)
        {
            MainAdminForm maf = (MainAdminForm)session["MAF"];
            Space sp = (Space)session["space"];
            int loadNumber = getLoadNumber(sp, loadGroup); ;
            /*
            if(loadGroup != Util.LOAD_GROUP_NAME)
            {
                loadNumber = getLoadNumber(sp, loadGroup);
            }                
            */
            bool canpublish = SpaceAdminService.checkSpacePublishability(maf, sp);
            if (!canpublish)
            {                
                errorOutput.ErrorType = ResponseMessages.ERROR_TYPE_PUBLISH;
                errorOutput.ErrorMessage = ResponseMessages.ERROR_MSG_PUBLISH;                                        
                return null;
            }
            DateTime loadDate = DateTime.Now;
            if (!independentMode && !retryFailedLoad)
            {
                try
                {
                    if (!Performance_Optimizer_Administration.Util.tryDateTimeParse(loadDateString, out loadDate))
                    {
                        Global.systemLog.Error("LoadDate could not be parsed : " + loadDateString);
                        throw new FormatException("LoadDate could not be parsed : " + loadDateString);
                    }
                }
                catch (FormatException)
                {                    
                    errorOutput.ErrorType = ResponseMessages.ERROR_TYPE_GENERAL;
                    errorOutput.ErrorMessage = "Date Format not in Expected Format";                    
                    return null;
                }
            }

            ApplicationLoader al = startProcess(session, request, sp, loadDate, loadNumber, loadGroup, subGroups, errorOutput, reprocessmodified);
            if (al == null)
            {
                if(errorOutput.ErrorMessage == null)
                {
                    errorOutput.ErrorType = ResponseMessages.ERROR_TYPE_GENERAL;
                    errorOutput.ErrorMessage = "Unable to process data";                                        
                }      
                
                return null;
            }

            return al;
        }

        public static int getLoadNumber(Space sp, string loadGroup)
        {
            int loadNumber = 0;
            QueryConnection conn = null;
            Dictionary<string, int> curLoadNumbers = null;
            try
            {
                conn = ConnectionPool.getConnection();

                if (loadGroup == SalesforceLoader.LOAD_GROUP_NAME)
                {
                    curLoadNumbers = Database.getCurrentLoadNumbers(conn, mainSchema, sp.ID);
                    loadNumber = curLoadNumbers.ContainsKey(SalesforceLoader.LOAD_GROUP_NAME) ? curLoadNumbers[SalesforceLoader.LOAD_GROUP_NAME] : SalesforceLoader.BASE_SFORCE_LOAD_NUMBER;
                }
                else
                {
                    // always gets the updated load number from db
                    Space updatedSpaceInfo = Database.getSpace(conn, mainSchema, sp.ID);
                    loadNumber = updatedSpaceInfo.LoadNumber; 
                }
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }

            return loadNumber;
        }
       
        private static bool canProceed(HttpSessionState session)
        {
            bool proceed = false;
            Space sp = (Space)session["space"];
            MainAdminForm maf = (MainAdminForm)session["MAF"];
            if (sp != null && maf != null)
            {
                proceed = true;
            }

            return proceed;
        }

        private static bool canCreateDashboards(HttpSessionState session, bool allowCreateDashboards)
        {
            bool create = false;
            MainAdminForm maf = (MainAdminForm)session["MAF"];
            bool setCreateDashboards = maf.quickDashboards == null || maf.quickDashboards.Count == 0;
            if (!setCreateDashboards)
            {
                bool found = false;
                foreach (QuickDashboard qd in maf.quickDashboards)
                {
                    if (qd.DashboardName == BuildDashboards.OverviewDashboardName)
                    {
                        found = true;
                        break;
                    }
                }
                setCreateDashboards = !found;
            }
            create = setCreateDashboards && allowCreateDashboards;

            return create;
        }

        private static string getLoadGroup()
        {
            return Util.LOAD_GROUP_NAME;
        }

        // update the running status of the load under processing
        private static void setRunningStatus(HttpSessionState session, MainAdminForm maf,
            Space sp, string mainSchema, ProcessOptions processOptions, string loadGroup)
        {
            
            ErrorOutput errorOutput = processOptions.ErrorOutput;
            if (errorOutput == null)
            {
                errorOutput = new ErrorOutput();
                processOptions.ErrorOutput = errorOutput;
            }
            int loadNumber = getLoadNumber(sp, loadGroup);
            Status.StatusResult status = (Status.StatusResult)session["status"];
            if (status != null && status.code == Status.StatusCode.Failed)
            {
                // need to separate out the function since it could be called later during Running Status updated check
                processFailedStatus(session, maf, sp, status, loadNumber, loadGroup, errorOutput);
            }
            
            // Get whether anything is still running
            if (Status.isRunningCode(status.code))
            {
                // For delete all data or with delete data with load number = 1, return
                // wait for it(session status) to get updated by the deleteDataThread

                if (status.code == Status.StatusCode.DeleteData && sp.LoadNumber == 0)
                {
                    processOptions.CurrentState = ResponseMessages.STATUS_PROCESSING;
                    processOptions.LoadNumberProcessed = sp.LoadNumber.ToString();
                    return;
                }

                if (status.code == Status.StatusCode.DeleteData && Util.getSessionDeleteLastLoadNumber(session) > 0)
                {
                    // this is the load number of the last successful run
                    loadNumber = Util.getSessionDeleteLastLoadNumber(session) - 1;
                }

                Status.StatusResult sr = Status.getLoadStatus(session, sp, loadNumber + 1, loadGroup);
                if (sr.code == Status.StatusCode.Complete)
                {
                    QueryConnection conn = null;
                    try
                    {
                        conn = ConnectionPool.getConnection();
                        ApplicationLoader.updateLoadSuccess(maf, sp, mainSchema, conn, sr, loadNumber, loadGroup);
                    }
                    catch (Exception ex)
                    {
                        Global.systemLog.Error(ex);
                        try
                        {
                            ApplicationLoader.updateLoadSuccess(maf, sp, mainSchema, conn, sr, loadNumber, loadGroup);

                        }
                        catch (Exception ex2)
                        {
                            Global.systemLog.Error(ex2);
                        }
                    }
                    finally
                    {
                        ConnectionPool.releaseConnection(conn);
                    }
                    session["status"] = sr;
                    if (session["autopublish"] != null)
                    {
                        session.Remove("autopublish");
                        if (maf.quickDashboards != null && maf.quickDashboards.Count > 0)
                            processOptions.RedirectModule = "Dashboards";
                        else
                           processOptions.RedirectModule = "Adhoc";
                    }
                    Util.setDesignerAndDashboardViewablePermissions(session);
                }
                else if (sr.code == Status.StatusCode.Failed)
                {
                    Global.systemLog.Error("Load failed, space: " + sp.ToString());
                    session["status"] = sr;
                    processFailedStatus(session, maf, sp, sr, loadNumber, loadGroup, errorOutput);
                }
                else if (status.code == Status.StatusCode.BuildingDashboards && sr.code == Status.StatusCode.Ready)
                {
                    sr = status;
                }
                //setProgressLabel(sr);
                if (sr.code != Status.StatusCode.None)
                {
                    status = sr;
                }
            }
            else
            {
                if (status.code == Status.StatusCode.Complete)
                {
                    QueryConnection conn = null;
                    try
                    {
                        conn = ConnectionPool.getConnection();
                        ApplicationLoader.updateLoadSuccess(maf, sp, mainSchema, conn, status, loadNumber, loadGroup);
                    }
                    finally
                    {
                        ConnectionPool.releaseConnection(conn);
                    }
                }
                if (status.code == Status.StatusCode.Ready || status.code == Status.StatusCode.Complete)
                {
                    processOptions.CurrentState = ResponseMessages.STATUS_DONE;   
                    processOptions.LoadNumberProcessed = loadNumber.ToString();
                    
                    if (session["autopublish"] != null)
                    {
                        session.Remove("autopublish");
                        if (maf.quickDashboards != null && maf.quickDashboards.Count > 0)
                            processOptions.RedirectModule = "Dashboards";
                        else
                            processOptions.RedirectModule = "Adhoc";
                    }
                    return;
                }
            }
            // set the current state based on the updated status of running process
            if(Status.isRunningCode(status.code))
            {
                processOptions.CurrentState = ResponseMessages.STATUS_PROCESSING;
            }
            else if (status.code == Status.StatusCode.Ready || status.code == Status.StatusCode.Complete)
            {
                processOptions.CurrentState = ResponseMessages.STATUS_DONE;
            }
            else 
            {   
                // this is for status code None and for Failed. For Failed, errorOutput Type and messages are set to trigger
                // the output on the client side
                processOptions.CurrentState = ResponseMessages.STATUS_AVAIABLE;
            }
            
            processOptions.LoadNumberProcessed = loadNumber.ToString();            
        }

        private static void setWarningMessageForScriptGroup(ProcessOptions processOptions, Space sp, HttpSessionState session)
        {
            if (processOptions != null && sp != null && session != null)
            {
                if (processOptions.CurrentState == ResponseMessages.STATUS_DONE || processOptions.CurrentState == ResponseMessages.STATUS_AVAIABLE)
                {
                    String warning = null;
                    int scriptGroupStatus = Status.getScriptGroupStatus(sp, sp.LoadNumber, session);
                    if (scriptGroupStatus == Status.FAILED)
                    {
                        warning = "Script(s) Execution Failed. Please contact Birst Technical Support.";
                    }
                    if (processOptions.ErrorOutput == null)
                    {
                        processOptions.ErrorOutput = new ErrorOutput();
                    }
                    processOptions.ErrorOutput.WarningMessage = warning;
                }
            }
        }

        private static void processFailedStatus(HttpSessionState session, MainAdminForm maf, Space sp,
            Status.StatusResult status, int loadNumber, string loadGroup, ErrorOutput errorOutput)
        {
            Status.StatusStep ss = null;
            if (status.substeps != null)
            {
                foreach (Status.StatusStep step in status.substeps)
                {
                    if (step.result == Status.SCRIPTFAILURE)
                    {
                        ss = step;
                        break;
                    }
                }
            }
            if (ss == null || status.loadid != loadNumber + 1)
            {
                // Maybe status is from a different load group
                session.Remove("status");
                Status.StatusResult sr = Status.getLoadStatus(session, sp, loadNumber + 1, loadGroup);
                session["status"] = status;
                if (sr.substeps != null)
                    foreach (Status.StatusStep sss in sr.substeps)
                    {
                        if (sss.result == Status.SCRIPTFAILURE)
                        {
                            ss = sss;
                            break;
                        }
                    }
            }
            if (ss == null)
            {
                // check for process not started
                if (Status.checkForLoadStartedFailureEntry(session, sp, loadNumber + 1))
                {
                    errorOutput.ErrorType = ResponseMessages.ERROR_TYPE_GENERAL;
                    errorOutput.ErrorMessage = ResponseMessages.ERROR_MSG_PROCESS_NOT_STARTED;
                }
                else
                {
                    if (status.message != null && status.message.Length > 0)
                    {
                        errorOutput.ErrorType = ResponseMessages.ERROR_TYPE_GENERAL;
                        errorOutput.ErrorMessage = status.message;
                    }
                    else
                    {
                        errorOutput.ErrorType = ResponseMessages.ERROR_TYPE_GENERAL;
                        errorOutput.ErrorMessage = ResponseMessages.ERROR_MSG_GENERAL;
                    }
                }
            }
            else
            {
                string errorSource = "";
                foreach (StagingTable st in maf.stagingTableMod.getAllStagingTables())
                {
                    if (ss.substep == loadGroup + ": " + st.Name)
                    {
                        string s = st.SourceFile;
                        int index = s.LastIndexOf('.');
                        if (index >= 0)
                            s = s.Substring(0, index);
                        errorSource = s;
                        break;
                    }
                }

                errorOutput.ErrorType = ResponseMessages.ERROR_TYPE_SCRIPT;
                errorOutput.ErrorMessageSource = errorSource;
                errorOutput.ErrorMessage = ss.message;
            }
            
            configureRetryLoadOptions(sp, loadGroup, loadNumber, errorOutput);            
        }

        private static void configureRetryLoadOptions(Space sp, string loadGroup, int loadNumber, ErrorOutput errorOutput)
        {
            errorOutput.canRetryLoad = false;
            //check if the failed load can be retried
            if (!sp.AllowRetryFailedLoad)
            {
                Global.systemLog.Debug("RetryFailedLoad is not enabled for Space " + sp.ID.ToString());
            }
            else
            {
                PerformanceEngineVersion peVersion = Util.getPerformanceEngineVersion(sp, loadGroup);
                if (peVersion == null)
                {
                    Global.systemLog.Warn("Cannot figure out performance engine version for Space " + sp.ID.ToString() + " for loadgroup " + loadGroup);
                }
                else if (!peVersion.SupportsRetryLoad)
                {
                    Global.systemLog.Debug("Performance engine version for Space " + sp.ID.ToString() + " for loadgroup " + loadGroup + " does not support retry failed load");
                }
                else
                {
                    QueryConnection conn = null;
                    try
                    {
                        conn = ConnectionPool.getConnection();
                        LastLoadHistory llh = Database.getLastLoadHistory(conn, mainSchema, sp.ID, loadGroup, loadNumber + 1);
                        if (llh == null)
                        {
                            Global.systemLog.Error("Could not find failed load history for space " + sp.ID.ToString() + " loadgroup " + loadGroup + " for loadNumber " + (loadNumber + 1) + ", cannot retry failed load");
                        }
                        else
                        {
                            errorOutput.canRetryLoad = true;
                            errorOutput.retryLoadDate = llh.LOADDATE.ToString("g");
                            errorOutput.retryProcessingGroups = llh.PROCESSINGGROUPS;
                        }
                    }
                    finally
                    {
                        ConnectionPool.releaseConnection(conn);
                    }
                }
            }
        }

        private static DataTable publishHistory(HttpSessionState session, Space sp, QueryConnection conn, string loadGroup)
        {
            int loadNumber = getLoadNumber(sp, loadGroup); //sp.LoadNumber;
            // Fetch Publish History from TXN_COMMAND_HISTORY
            // to be compared later with the one from Publish table for getting "PublishDate"
            DataTable dt = Status.getPublishHistory(session, sp, loadNumber + 1, loadGroup, false, true, 5);
            DataTable txnHistory = Status.getPublishStartHistory(session, sp, loadNumber + 1, loadGroup);

            if (dt.Rows.Count > 0)
            {
                dt.Columns.Add("PublishStartTime", typeof(DateTime));
                dt.Columns.Add("PublishEndTime", typeof(DateTime));
                List<Publish> publishes = Database.getPublishes(conn, mainSchema, sp.ID);
                foreach (DataRow dr in dt.Rows)
                {
                    //Get the LoadDate stored in the Publish Table for a particular iteration number.
                    
                    int ln = (int)dr["LoadNumber"];
                    bool found = false;
                    foreach (Publish p in publishes)
                    {
                        if (p.LoadNumber == ln)
                        {
                            dr["PublishDate"] = p.LoadDate;
                            if (p.StartPublishDate == DateTime.MinValue)
                            {
                                // see if you can get it from txn_command_history                       
                                dr["PublishStartTime"] = getStartDateFromTxnCommandHistory(txnHistory, ln);
                            }
                            else
                            {
                                dr["PublishStartTime"] = p.StartPublishDate;
                            }
                            dr["PublishEndTime"] = p.PublishDate;
                            found = true;
                            break;
                        }
                    }
                    // If the LoadDate for the particular load number is not found, use the TM date from
                    // txn_command_history - e.g. if the load failed.
                    if (!found)
                    {
                        dr["PublishDate"] = dr["DateTime"];
                        // see if you can get it from txn_command_history                       
                        dr["PublishStartTime"] = getStartDateFromTxnCommandHistory(txnHistory, ln);
                        DateTime publishEndTime = Status.getPublishEndTime(sp, ln, loadGroup);
                        if (publishEndTime != DateTime.MinValue)
                        {
                            dr["PublishEndTime"] = publishEndTime;
                        }
                        else
                        {
                            dr["PublishEndTime"] = dr["DateTime"];
                        }
                    }
                }                
                return dt;
            }
            else
                return null;
            
        }

        public static DateTime getStartDateFromTxnCommandHistory(DataTable txnHistory, int loadNumber )
        {
            DateTime dateTime = DateTime.MinValue;

            if (txnHistory != null && txnHistory.Rows.Count > 0)
            {
                foreach (DataRow dr in txnHistory.Rows)
                {
                    int ln = (int)dr["LoadNumber"];
                    if (ln == loadNumber)
                    {
                        dateTime = (DateTime)dr["DateTime"];
                        break;
                    }
                }
            }
            return dateTime;
        }

        private static string[][] getArray(DataTable dt)
        {
            List<string[]> result = new List<string[]>();
            
            if(dt.Rows.Count >0){

                List<string> headerRow = new List<string>();
                foreach (DataColumn column in dt.Columns)
                {
                    headerRow.Add(column.ColumnName);                 
                }
                result.Add(headerRow.ToArray());
                foreach (DataRow dr in dt.Rows)
                {
                    List<string> row = new List<string>();

                    foreach(DataColumn column in dt.Columns)
                    {                       
                       row.Add(dr[column].ToString());                         
                    }

                    result.Add(row.ToArray());
                }
            }            
            return result.ToArray();
        }

        private static ApplicationLoader startProcess(HttpSessionState session, HttpRequest request, Space sp, DateTime loadDate, int loadNumber,
            string loadGroup, string[] subgroups, ErrorOutput errorOutput, bool reprocessmodified)
        {
            Util.logoutSMIWebSession(session, sp, request);
            
            //if ((OverviewDashboardBox.Checked ? sr.code != Status.StatusCode.BuildingDashboards : sr.code != Status.StatusCode.GenerateSchema) && Status.isRunningCode(sr.code))
            //    return null;
            // Make sure the application is fully built
            MainAdminForm maf = (MainAdminForm)session["MAF"];
            if (maf == null)
                return null;
            string enginecmd = Util.getEngineCommand(sp);
            log4net.ThreadContext.Properties["user"] = ((User)session["user"]).Username;
            using (log4net.ThreadContext.Stacks["itemid"].Push(sp.ID.ToString()))
            {
                Global.userEventLog.Info("PUBLISH");
            }
            int newLoadNumber = reprocessmodified ? loadNumber : loadNumber + 1;
            ApplicationLoader al = new ApplicationLoader(maf, sp, loadDate, mainSchema, enginecmd, createtimecmd, contentDir,
                null, (User)session["user"], loadGroup, newLoadNumber);
            if (subgroups != null)
                al.SubGroups = subgroups;
            Acorn.ApplicationLoader.LoadAuthorization la = al.getLoadAuthorization(loadGroup);
            if (la == ApplicationLoader.LoadAuthorization.RowLimitExceeded || la == ApplicationLoader.LoadAuthorization.RowPublishLimitExceeded)
            {
                if (la == ApplicationLoader.LoadAuthorization.RowLimitExceeded)
                {
                    errorOutput.ErrorType = ResponseMessages.ERROR_TYPE_ROW_LIMIT;
                    errorOutput.ErrorMessage = ResponseMessages.ERROR_MSG_ROW_LIMIT;
                }                    
                else
                {
                    errorOutput.ErrorType = ResponseMessages.ERROR_TYPE_ROW_PUBLISH_LIMIT;
                    errorOutput.ErrorMessage = ResponseMessages.ERROR_MSG_ROW_PUBLISH_LIMIT;
                }
                         
                return null;
            }
            else if (la == ApplicationLoader.LoadAuthorization.DataLimitExceeded || la == ApplicationLoader.LoadAuthorization.DataPublishLimitExceeded)
            {
                if (la == ApplicationLoader.LoadAuthorization.DataLimitExceeded)
                {
                    errorOutput.ErrorType = ResponseMessages.ERROR_TYPE_DATA_LIMIT;
                    errorOutput.ErrorMessage = ResponseMessages.ERROR_MSG_DATA_LIMIT;                   
                }                    
                else
                {
                    errorOutput.ErrorType = ResponseMessages.ERROR_TYPE_DATA_PUBLISH_LIMIT;
                    errorOutput.ErrorMessage = ResponseMessages.ERROR_MSG_DATA_PUBLISH_LIMIT;
                }
                          
                return null;
            }
            return al;
        }
        
        public string frameSource = null;

        private static void clearLevel(Level l)
        {
            l.GenerateDimensionTable = false;
            if (l.Keys[0].SurrogateKey)
            {
                List<LevelKey> lklist = new List<LevelKey>();
                foreach (LevelKey lk in l.Keys)
                    if (!lk.SurrogateKey)
                        lklist.Add(lk);
                l.Keys = lklist.ToArray();
            }
        }

        private static List<StagingColumn> getStagingColumns(MainAdminForm maf, string dimName)
        {
            List<StagingColumn> sclist = new List<StagingColumn>();
            foreach (StagingTable st in maf.stagingTableMod.getAllStagingTables())
            {
                bool match = false;
                foreach (string[] level in st.Levels)
                {
                    if (level[0] == dimName)
                    {
                        match = true;
                        break;
                    }
                }
                if (match)
                {
                    foreach (StagingColumn sc in st.Columns)
                        if (Array.IndexOf<string>(sc.TargetTypes, dimName) >= 0)
                            sclist.Add(sc);
                }
            }
            return (sclist);
        } 
      
    }
}
