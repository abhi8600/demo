﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;
using System.Xml.Serialization;

namespace Acorn
{
    public class SpaceSettings
    {
        public long HeaderBackgroundColor = -1;
        private byte[] logoImage;
        private string logoImageType;
        public long HeaderForegroundColor = -1;
        [XmlIgnore]
        public DateTime lastModifiedDate;

        public byte[] LogoImage
        {
            get
            {
                return this.logoImage;
            }
            set
            {
                this.logoImage = value;
            }
        }

        public string LogoImageType
        {
            get
            {
                return this.logoImageType;
            }
            set
            {
                this.logoImageType = value;
            }
        }
    }
}
