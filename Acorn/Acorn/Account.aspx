﻿<%@ Page Language="C#" AutoEventWireup="True" CodeBehind="Account.aspx.cs" Inherits="Acorn.Account"
    MasterPageFile="~/WebAdmin.Master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="ContentID" ContentPlaceHolderID="mainPlaceholder" runat="server">
    <div style="background-image: url(Images/nback.gif); height: 15px">
        &nbsp;
    </div>
    <div class="pagepadding">
        <div class="pageheader">
            <asp:Label ID= "UserSettingsLabel" runat="server" Text="<%$ Resources:UserSettingsLabels, UserSettingsLabel %>" />
        </div>
        <div style="padding-right: 10px">
            <div class="wizard">
                <asp:MultiView ID="MultiView" runat="server" ActiveViewIndex="0">
                    <asp:View ID="AccountDetailView" runat="server">
                        <table width="80%"">
                            <tr class="listviewheader">
                                <td colspan="2" style="padding-left: 3px; font-weight: bold">
                                     <asp:Label ID= "FeaturesLabel" runat="server" Text="<%$ Resources:UserSettingsLabels, FeaturesLabel %>" />
                                </td>
                            </tr>
                            <asp:PlaceHolder ID="OtherProductsPanel" runat="server" Visible="false">
                                <tr>
                                    <td style="width: 175px; padding-left: 10px">
                                          <asp:Label ID= "ProductsLabel" runat="server" Text="<%$ Resources:UserSettingsLabels, ProductsLabel %>" />
                                    </td>
                                    <td style="font-weight: normal">
                                        <asp:Label ID="OptionsLabel" runat="server"></asp:Label>
                                    </td>
                                </tr>
                            </asp:PlaceHolder>
                        </table>
                        <div style="height: 10px">
                            &nbsp;</div>
                        <asp:DetailsView ID="AccountDetails" runat="server" Width="100%" CellPadding="3"
                            CellSpacing="0" AutoGenerateRows="False" BorderWidth="0" BorderStyle="None" GridLines="None"
                            AutoGenerateEditButton="False" OnModeChanging="AccountDetails_ModeChanging" OnItemUpdating="AccountDetails_ItemUpdating">
                            <FieldHeaderStyle VerticalAlign="Top" Width="200px" />
                            <HeaderTemplate>
                                <table style="width: 80%" cellpadding="0" cellspacing="0">
                                    <tr class="listviewheader">
                                        <td>
                                           <asp:Label ID= "SettingsLabel" runat="server" Text="<%$ Resources:UserSettingsLabels, SettingsLabel %>" />
                                        </td>
                                        <td align="right" valign="middle" style="padding-right: 5px; padding-top: 2px">
                                            <asp:ImageButton ID="EditButton" runat="server" ToolTip="<%$ Resources:UserSettingsLabels, EditSettingsLabel %>" ImageUrl="~/Images/Edit.png"
                                                CommandName="Edit" Visible='<%# AccountDetails.CurrentMode != DetailsViewMode.Edit %>' />
                                            <asp:ImageButton ID="UpdateButton" runat="server" ToolTip="<%$ Resources:UserSettingsLabels, SaveChangesLabel %>" ImageUrl="~/Images/Save_Changes.png"
                                                CommandName="Update" Visible='<%# AccountDetails.CurrentMode == DetailsViewMode.Edit %>' />
                                        </td>
                                    </tr>
                                </table>
                            </HeaderTemplate>
                            <Fields>
                                <asp:BoundField DataField="Username" HeaderText="<%$ Resources:UserSettingsLabels, UserNameLabel %>" ReadOnly="true" />
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <asp:Label ID= "EmailLabel" runat="server" Text="<%$ Resources:UserSettingsLabels, EmailLabel %>" />
                                      </HeaderTemplate>
                                    <ItemTemplate>
                                        <%# Eval("Email") %></ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox ID="EmailTextBox" runat="server" MaxLength="64" Text='<%# Eval("Email") %>'></asp:TextBox>
                                        <asp:RegularExpressionValidator id="EmailValidator" runat="server" 
                                            ControlToValidate="EmailTextBox" 
                                            ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" 
                                            ErrorMessage="<%$ Resources:UserSettingsLabels, InvalidEmailLabel %>">
                                        </asp:RegularExpressionValidator>
                                        <asp:RequiredFieldValidator ID="EmailRequired" runat="server" ControlToValidate="EmailTextBox"
                                             ErrorMessage="<%$ Resources:UserSettingsLabels, EmailRequiredLabel %>" ToolTip="<%$ Resources:UserSettingsLabels, EmailRequiredLabel %>">* Required</asp:RequiredFieldValidator>
                                    </EditItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <asp:Label ID= "DefaultSpaceLabel" runat="server" Text="<%$ Resources:UserSettingsLabels, DefaultSpaceLabel %>" />
                                     </HeaderTemplate>
                                    <ItemTemplate>
                                        <%# Eval("DefaultSpace") %></ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:DropDownList ID="SpaceDropDown" runat="server" Text='<%# Eval("DefaultSpace") %>'>
                                            <asp:ListItem></asp:ListItem>
                                        </asp:DropDownList>
                                    </EditItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <asp:Label ID= "DefaultToDashboardsLabel" runat="server" Text="<%$ Resources:UserSettingsLabels, DefaultToDashboardsLabel %>" />
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:CheckBox runat="server" ID="DashboardBox" Checked='<%# Eval("DefaultDashboards") %>'
                                            Enabled="false" />
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:CheckBox runat="server" ID="DashboardBox" Checked='<%# Eval("DefaultDashboards") %>'
                                            Enabled="true" />
                                    </EditItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                      <asp:Label ID= "LanguageLabel" runat="server" Text="<%$ Resources:UserSettingsLabels, LanguageLabel %>" />
                                    </HeaderTemplate>
                                    <ItemTemplate><asp:TextBox ID="Language"  BorderStyle="None"  ReadOnly="true" runat="server" Width="360px"></asp:TextBox></ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:DropDownList ID="LanguageBox" runat="server"/>
                                    </EditItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField Visible="false">
                                    <HeaderTemplate>Time Zone:</HeaderTemplate>
                                    <ItemTemplate><asp:TextBox ID="TimeZone"  BorderStyle="None"  ReadOnly="true" runat="server"  Width="360px"></asp:TextBox></ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:DropDownList ID="TimeZoneBox" runat="server"/>
                                    </EditItemTemplate>
                                </asp:TemplateField>
                            </Fields>
                        </asp:DetailsView>
                        <div style="height: 10px">
                            &nbsp;
                        </div>
                        <asp:LinkButton ID="ChangePWD" runat="server" Text="<%$ Resources:UserSettingsLabels,ChangePasswordLabel %>" PostBackUrl="~/ChangePassword.aspx" />
                        <div style="height: 10px">
                            &nbsp;
                        </div>
                        <div> 
                                <table style="width: 80%" cellpadding="0" cellspacing="0">
                                    <tr class="listviewheader">
                                        <td>
                                             <asp:Label ID= "OpenID" runat="server" Text="<%$ Resources:UserSettingsLabels,OpenIDLabel %>"></asp:Label> <a href="http://www.openid.net" target="_blank">www.openid.net</a>)                                         
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                </table>
                            <asp:Label ID="OpenIDLabel" runat="server" Visible="false" ForeColor="Red" />
                            <asp:GridView ID="OpenIDs" runat="server" GridLines="None" ShowHeader="false" CellPadding="5" OnRowCommand="OpenIds_RowCommand"  OnRowDeleting="OpenIds_OnRowDeleting" ShowFooter="true">
                                <Columns>
                                    <asp:TemplateField HeaderText="OpenID">
                                        <ItemTemplate>
                                            <%# Eval("OpenID") %>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:TextBox ID="AssociationID" Width="400px" runat="server" MaxLength="256" Text='<%# Bind("OpenID") %>' />
                                            <asp:RegularExpressionValidator id="OpenIDValidator" runat="server" 
                                                ControlToValidate="AssociationID" 
                                                ValidationExpression="^http(s?)\:\/\/[0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*(:(0-9)*)*(\/?)([a-zA-Z0-9\-\.\?\,\'\/\\\+&amp;%\$#_=]*)?$" 
                                                ErrorMessage= "<%$ Resources:UserSettingsLabels,InvalidOpenIDLabel %>">                                               
                                            </asp:RegularExpressionValidator>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Commands">
                                        <ItemTemplate>
                                            <asp:Button runat="server" ID="Delete" Text="<%$ Resources:UserSettingsLabels,DeleteLabel %>" CommandName="Delete" CommandArgument="<%# ((GridViewRow) Container).RowIndex %>"/>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Button runat="server" ID="Insert" Text="<%$ Resources:UserSettingsLabels,AddLabel %>" CommandName="InsertNew" />
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <EmptyDataTemplate>
                                    <asp:TextBox runat="server" Width="400px" MaxLength="256" ID="EmptyAssociationID" />
                                    <asp:RegularExpressionValidator id="OpenIDValidator2" runat="server" 
                                        ControlToValidate="EmptyAssociationID" 
                                        ValidationExpression="^http(s?)\:\/\/[0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*(:(0-9)*)*(\/?)([a-zA-Z0-9\-\.\?\,\'\/\\\+&amp;%\$#_=]*)?$" 
                                        ErrorMessage="<%$ Resources:UserSettingsLabels,InvalidOpenIDLabel %>"   >
                                    </asp:RegularExpressionValidator>
                                    <asp:Button runat="server" ID="NewOpenIdButton" CommandName="InsertEmpty" Text="<%$ Resources:UserSettingsLabels,AddLabel %>"/>
                                </EmptyDataTemplate>
                            </asp:GridView>  
                        </div>
                       <div style="height: 10px">
                            &nbsp;
                        </div>
                                <table style="width: 80%" cellpadding="0" cellspacing="0">
                                    <tr class="listviewheader">
                                        <td>
                                           <asp:Label ID= "OtherLabel" runat="server" Text="<%$ Resources:UserSettingsLabels, OtherLabel %>" />
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                </table>
                        <table>
                            <asp:PlaceHolder ID="SkinPlaceHolder" runat="server" Visible="false">
                                <tr valign="middle">
                                    <td>
                                        <asp:LinkButton ID="ChangeSkin" runat="server" Text="<%$ Resources:UserSettingsLabels, ModifyLookAndFeelLabel %>" PostBackUrl="~/Skin.aspx" />
                                    </td>
                                </tr>
                            </asp:PlaceHolder>                           
                        </table>
                    </asp:View>
                </asp:MultiView>
            </div>
        </div>
    </div>
    <div style="height: 50px">
        &nbsp;</div>
</asp:Content>
