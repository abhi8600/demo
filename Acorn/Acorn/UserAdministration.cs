﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.SessionState;
using System.Data.Odbc;
using Acorn.Exceptions;
using Acorn.Utils;
using System.Security.Cryptography;
using System.Text;
using System.IO;
using Acorn.DBConnection;
using Performance_Optimizer_Administration;
using System.Xml;
using System.Xml.Linq;
using Amazon.Runtime;

namespace Acorn
{
    public class UserAdministration
    {        
        public static int FAILED_NUM_USERS_TO_SHOW = 10;

        public static List<UserSummary> getUsersToManage(HttpSessionState session, string userSearchString, string spaceID)
        {

            User u = Util.getSessionUser(session);
            if(u == null)
            {
                Global.systemLog.Warn("Session User unavailable");
                throw new UserNotLoggedInException();
            }
            
            // Check if the user is authorized to perform user management operations
            if (!UserManagementUtils.isUserAuthorized(u))
            {
                Global.systemLog.Warn("User unauthorized for user management activities : " + u.ID + " : " + u.Username);
                throw new UserManagementNotAuthorizedException();
            }

            return getUsersToManage(u, userSearchString, spaceID);
        }

        public static List<UserSummary> getUsersToManage(User u, string userSearchString, string spaceID)
        {

            QueryConnection conn = null;
            List<UserSummary> response = null;            
            try
            {
                conn = ConnectionPool.getConnection();
                string schema = Util.getMainSchema();

                if (spaceID == null)
                {
                    response = Database.getManagedUsers(conn, schema, u, userSearchString, UserManagementUtils.LIMIT_USER_RESULT);
                }
                else 
                {
                    response = Database.getManagedUsers(conn, schema, u, userSearchString, new Guid(spaceID), UserManagementUtils.LIMIT_USER_RESULT);  
                }                
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("UserManagement user : " + u.Username + " : Exception during getUsertoManage for user " + u.Username, ex);
                throw ex;
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }

            return response;
        }

        public static List<SpaceSummary> getSpacesToManage(HttpSessionState session)
        {
            User u = Util.getSessionUser(session);
            if (u == null)
            {
                Global.systemLog.Warn("Session User unavailable");
                throw new UserNotLoggedInException();
            }

            // Check if the user is authorized to perform user management operations
            if (!UserManagementUtils.isUserAuthorized(u))
            {
                Global.systemLog.Warn("User unauthorized for user management activities : " + u.ID + " : " + u.Username);
                throw new UserManagementNotAuthorizedException();
            }

            return getSpacesToManage(u);
        }

        public static List<SpaceSummary> getSpacesToManage(User u)
        {
            List<SpaceSummary> response = null;
            Dictionary<string, SpaceSummary> spaceSummaryDict = null;
            QueryConnection conn = null;
            try
            {
                conn = ConnectionPool.getConnection();
                string schema = Util.getMainSchema();
                List<SpaceMembership> allSpaces = Database.getSpaces(conn, schema, u, true);
                if (allSpaces != null && allSpaces.Count > 0)
                {
                    response = new List<SpaceSummary>();
                    spaceSummaryDict = new Dictionary<string, SpaceSummary>();
                    foreach (SpaceMembership sm in allSpaces)
                    {
                        // only add spaces for which the current user is Admin
                        if (sm.Administrator && !spaceSummaryDict.ContainsKey(sm.Space.ID.ToString()))
                        {
                            SpaceSummary spaceSummary = new SpaceSummary();
                            spaceSummary.Owner = sm.Owner;
                            spaceSummary.OwnerUsername = u.Username;
                            spaceSummary.SpaceID = sm.Space.ID.ToString();
                            spaceSummary.SpaceName = sm.Space.Name;
                            response.Add(spaceSummary);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("UserManagement user : " + u.Username + " : Exception during getSpacesToManage for user " + u.Username, ex);
                throw ex;
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }

            return response;
        }

        public static AccountSummary getAccountSummary(HttpSessionState session)
        {
            User u = Util.getSessionUser(session);
            if (u == null)
            {
                Global.systemLog.Error("Session User unavailable");
                throw new UserNotLoggedInException();
            }

            // Check if the user is authorized to perform user management operations
            if (!UserManagementUtils.isUserAuthorized(u))
            {
                Global.systemLog.Warn("User unauthorized for user management activities : " + u.ID + " : " + u.Username);
                throw new UserManagementNotAuthorizedException();
            }

            return getAccountSummary(u);
        }

        public static AccountSummary getAccountSummary(User u)
        {
            AccountSummary response = new AccountSummary();
            QueryConnection conn = null;
            string schema = Util.getMainSchema();
            try
            {
                conn = ConnectionPool.getConnection();
                AccountDetails actDetails = Database.getAccountDetails(conn, schema, u.AdminAccountId, u);
                if (actDetails != null)
                {
                    response.AccountID = actDetails.ID.ToString();
                    response.Name = actDetails.Name;
                    DateTime dateTime = actDetails.ExpirationDate;
                    if (dateTime != DateTime.MinValue)
                    {
                        response.ExpirationDate = dateTime.ToString();
                    }
                    response.MaxAllowedUsers = actDetails.MaxUsers;
                    response.MaxConcurrentUsers = actDetails.MaxConcurrentLogins;
                }
                else
                {
                    throw new UserManagementNotAuthorizedException();
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("UserManagement user : " + u.Username + " : Exception during getAccountSummary for user " + u.Username, ex);
                throw ex;
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
            return response;
        }

        public static int[] getNumberUsers(HttpSessionState session)
        {
            User u = Util.getSessionUser(session);
            if (u == null)
            {
                Global.systemLog.Warn("Session User unavailable");
                throw new UserNotLoggedInException();
            }

            // Check if the user is authorized to perform user management operations
            if (!UserManagementUtils.isUserAuthorized(u))
            {
                Global.systemLog.Warn("User unauthorized for user management activities : " + u.ID + " : " + u.Username);
                throw new UserManagementNotAuthorizedException();
            }

            return getNumberUsers(u);
        }

        /// <summary>
        /// Returns the num account users
        /// int[0] num Active Users
        /// int[1] num Disabled Users
        /// </summary>
        /// <param name="u"></param>
        /// <returns></returns>

        public static int[] getNumberUsers(User u)
        {
            int[] numUsers = new int[]{-1,-1};

            if (u.AdminAccountId == null || u.AdminAccountId.Equals(Guid.Empty))
            {
                return numUsers;
            }

            QueryConnection conn = null;
            string schema = Util.getMainSchema();
            try
            {
                conn = ConnectionPool.getConnection();
                numUsers = Database.getNumAccountUsers(conn, schema, u.AdminAccountId);                
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Exception during getting num of Active Users for user " + u.Username);
                throw ex;
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }

            return numUsers;
        }

        public static void addUserToSpace(HttpSessionState session, string[] toAddUsersIds, string spaceID, bool admin)
        {
            User u = Util.getSessionUser(session);
            if (u == null)
            {
                Global.systemLog.Warn("Session User unavailable");
                throw new UserNotLoggedInException();
            }

            // Check if the user is authorized to perform user management operations
            if (!UserManagementUtils.isUserAuthorized(u))
            {
                Global.systemLog.Warn("User unauthorized for user management activities : " + u.ID + " : " + u.Username);
                throw new UserManagementNotAuthorizedException();
            }

            addUserToSpace(u, toAddUsersIds, spaceID, admin);
        }

        public static void addUserToSpace(User u, string[] toAddUsersIds, string spaceID, bool admin)
        {
            QueryConnection conn = null;
            List<string> successfulOperationUsernames = new List<string>();
            String schema = Util.getMainSchema();
            try
            {
                conn = ConnectionPool.getConnection();                
                Guid spId = new Guid(spaceID);
                Space toAddSpace = Database.getSpace(conn, schema, spId);

                foreach (string userId in toAddUsersIds)
                {
                    User toAddUser = Database.getUser(conn, schema, null, new Guid(userId), null, false);
                    if (toAddUser == null)
                        continue;
                    List<SpaceMembership> mlist = Database.getUsers(conn, schema, toAddSpace, false);
                    bool found = false;
                    foreach (SpaceMembership sm in mlist)
                    {
                        if (sm.User == null)
                            continue;
                        if (sm.User.ID != null && toAddUser.ID == sm.User.ID)
                        {
                            Global.systemLog.Info("Not adding " + toAddUser.ID + " (" + toAddUser.Username + ") to space (" + toAddSpace.ID + ") because they are already a member of the space");
                            found = true;
                            break;
                        }
                    }
                    if (!found)
                    {
                        Database.addSpaceMembership(conn, schema, toAddUser.ID, toAddSpace.ID, admin, false);
                        // Check if space-group-user mapping is done in the db. If not, populate required tables
                        Util.checkAndCreateUserGroupDbMappings(toAddSpace);
                        SpaceGroup spg = Database.getGroupInfo(conn, schema, toAddSpace, Util.USER_GROUP_NAME);
                        if (!UserAndGroupUtils.isUserGroupMember(spg, toAddUser.ID))
                        {
                            Database.addUserToGroup(conn, schema, toAddSpace, spg.ID, toAddUser.ID, true);
                        }
                        successfulOperationUsernames.Add(userId);
                    }
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("UserManagement user : " + u.Username + " : Exception while adding users to space ", ex);
                if (successfulOperationUsernames.Count == 0)
                {
                    throw new BirstException("Operation failed for All users : ");
                }
                else if (successfulOperationUsernames.Count < toAddUsersIds.Length)
                {

                    string failedOperationUsernames = "";
                    List<string> failedOperationUserIds = new List<string>();
                    // get the users not done yet
                    foreach (string toModifyUserId in toAddUsersIds)
                    {
                        if (!successfulOperationUsernames.Contains(toModifyUserId))
                        {
                            failedOperationUserIds.Add(toModifyUserId);
                        }
                    }

                    // You don't want to fill the screen with a large number of users
                    if (failedOperationUserIds.Count < UserAdministration.FAILED_NUM_USERS_TO_SHOW)
                    {
                        bool retrieveFailedUsers = false;
                        try
                        {
                            foreach (string failedUserId in failedOperationUserIds)
                            {
                                User failedUser = Database.getUser(conn, schema, null, new Guid(failedUserId), null, false);
                                failedOperationUsernames = failedOperationUsernames + failedUser.Username + " ;";
                            }

                            retrieveFailedUsers = true;
                        }
                        catch (Exception ex2)
                        {
                            // If error while retrieving user details..Nothing to do
                            Global.systemLog.Warn("Exception while retreiving failed user details ", ex2);
                        }
                        // If all the failed user info is retrieved, we want to display it on UI
                        if (retrieveFailedUsers)
                        {
                            throw new BirstException("Error Encountered. Unable to Unlock the following users : " + failedOperationUsernames);
                        }                       
                    }
                }
                throw ex;
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
        }

        public static void setHtmlUser(HttpSessionState session, string[] toAddUsersIds, bool html)
        {
            User u = Util.getSessionUser(session);
            if (u == null)
            {
                Global.systemLog.Warn("Session User unavailable");
                throw new UserNotLoggedInException();
            }

            // Check if the user is authorized to perform user management operations
            if (!UserManagementUtils.isUserAuthorized(u))
            {
                Global.systemLog.Warn("User unauthorized for user management activities : " + u.ID + " : " + u.Username);
                throw new UserManagementNotAuthorizedException();
            }

            setRenderType(u, toAddUsersIds, html);
        }
        public static void setDenyCreateSpace(HttpSessionState session, string[] toAddUsersIds, bool deny)
        {
            User u = Util.getSessionUser(session);
            if (u == null)
            {
                Global.systemLog.Warn("Session User unavailable");
                throw new UserNotLoggedInException();
            }

            // Check if the user is authorized to perform user management operations
            if (!UserManagementUtils.isUserAuthorized(u))
            {
                Global.systemLog.Warn("User unauthorized for user management activities : " + u.ID + " : " + u.Username);
                throw new UserManagementNotAuthorizedException();
            }

            setDenyAddSpace(u, toAddUsersIds, deny);
        }
        public static void setRenderType(User u, string[] toModifyUserIds, bool html)
        {
            QueryConnection conn = null;
            List<string> successfulOperationUsernames = new List<string>();
            String schema = Util.getMainSchema();
            try
            {
                conn = ConnectionPool.getConnection();

                if (toModifyUserIds != null && toModifyUserIds.Length > 0)
                {
                    conn = ConnectionPool.getConnection();
                    foreach (String userId in toModifyUserIds)
                    {
                        User user = Database.getUserById(conn, schema, new Guid(userId));
                        if (user != null)
                        {
                            Database.updateRenderType(conn, schema, user.ID, html);
                            successfulOperationUsernames.Add(userId);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("UserManagement user : " + u.Username + " : Exception while setting render type ", ex);
                if (successfulOperationUsernames.Count == 0)
                {
                    throw new BirstException("Operation failed for All users : ");
                }
                else if (successfulOperationUsernames.Count < toModifyUserIds.Length)
                {

                    string failedOperationUsernames = "";
                    List<string> failedOperationUserIds = new List<string>();
                    // get the users not done yet
                    foreach (string toModifyUserId in toModifyUserIds)
                    {
                        if (!successfulOperationUsernames.Contains(toModifyUserId))
                        {
                            failedOperationUserIds.Add(toModifyUserId);
                        }
                    }

                    // You don't want to fill the screen with a large number of users
                    if (failedOperationUserIds.Count < UserAdministration.FAILED_NUM_USERS_TO_SHOW)
                    {
                        bool retrieveFailedUsers = false;
                        try
                        {
                            foreach (string failedUserId in failedOperationUserIds)
                            {
                                User failedUser = Database.getUser(conn, schema, null, new Guid(failedUserId), null, false);
                                failedOperationUsernames = failedOperationUsernames + failedUser.Username + " ;";
                            }

                            retrieveFailedUsers = true;
                        }
                        catch (Exception ex2)
                        {
                            // If error while retrieving user details..Nothing to do
                            Global.systemLog.Warn("Exception while retreiving failed user details ", ex2);
                        }
                        // If all the failed user info is retrieved, we want to display it on UI
                        if (retrieveFailedUsers)
                        {
                            throw new BirstException("Error Encountered. Unable to Unlock the following users : " + failedOperationUsernames);
                        }
                    }
                }
                throw ex;
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
        }

        public static void setDenyAddSpace(User u, string[] toModifyUserIds, bool deny)
        {
            QueryConnection conn = null;
            List<string> successfulOperationUsernames = new List<string>();
            String schema = Util.getMainSchema();
            try
            {
                conn = ConnectionPool.getConnection();

                if (toModifyUserIds != null && toModifyUserIds.Length > 0)
                {
                    conn = ConnectionPool.getConnection();
                    foreach (String userId in toModifyUserIds)
                    {
                        User user = Database.getUserById(conn, schema, new Guid(userId));
                        if (user != null)
                        {
                            Database.updateDenyCreateSpace(conn, schema, user.ID, deny);
                            successfulOperationUsernames.Add(userId);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("UserManagement user : " + u.Username + " : Exception while setting deny add space ", ex);
                if (successfulOperationUsernames.Count == 0)
                {
                    throw new BirstException("Operation failed for All users : ");
                }
                else if (successfulOperationUsernames.Count < toModifyUserIds.Length)
                {

                    string failedOperationUsernames = "";
                    List<string> failedOperationUserIds = new List<string>();
                    // get the users not done yet
                    foreach (string toModifyUserId in toModifyUserIds)
                    {
                        if (!successfulOperationUsernames.Contains(toModifyUserId))
                        {
                            failedOperationUserIds.Add(toModifyUserId);
                        }
                    }

                    // You don't want to fill the screen with a large number of users
                    if (failedOperationUserIds.Count < UserAdministration.FAILED_NUM_USERS_TO_SHOW)
                    {
                        bool retrieveFailedUsers = false;
                        try
                        {
                            foreach (string failedUserId in failedOperationUserIds)
                            {
                                User failedUser = Database.getUser(conn, schema, null, new Guid(failedUserId), null, false);
                                failedOperationUsernames = failedOperationUsernames + failedUser.Username + " ;";
                            }

                            retrieveFailedUsers = true;
                        }
                        catch (Exception ex2)
                        {
                            // If error while retrieving user details..Nothing to do
                            Global.systemLog.Warn("Exception while retreiving failed user details ", ex2);
                        }
                        // If all the failed user info is retrieved, we want to display it on UI
                        if (retrieveFailedUsers)
                        {
                            throw new BirstException("Error Encountered. Unable to Unlock the following users : " + failedOperationUsernames);
                        }
                    }
                }
                throw ex;
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
        }

        public static void updatePassword(HttpSessionState session, string[] toModifyUsersIds, HttpServerUtility server, HttpRequest request)
        {
            User u = Util.getSessionUser(session);
            if (u == null)
            {
                Global.systemLog.Warn("Session User unavailable");
                throw new UserNotLoggedInException();
            }

            // Check if the user is authorized to perform user management operations
            if (!UserManagementUtils.isUserAuthorized(u))
            {
                Global.systemLog.Warn("User unauthorized for user management activities : " + u.ID + " : " + u.Username);
                throw new UserManagementNotAuthorizedException();
            }

            updatePassword(u, toModifyUsersIds, server, request);
        }

        public static void updatePassword(User u, string[] toModifyUsersIds, HttpServerUtility server, HttpRequest request)
        {
            QueryConnection conn = null;
            List<string> successfulOperationUsernames = new List<string>();
            string schema = Util.getMainSchema();
            try
            {
                // generate a new password for the user
                conn = ConnectionPool.getConnection();                
                string REALM = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["AuthenticationRealm"];
                int minRequiredPasswordLength = System.Web.Security.Membership.MinRequiredPasswordLength;
                int minRequiredNonAlphanumericCharacters = System.Web.Security.Membership.MinRequiredNonAlphanumericCharacters;
                string password = System.Web.Security.Membership.GeneratePassword(minRequiredPasswordLength, minRequiredNonAlphanumericCharacters);
                MD5CryptoServiceProvider md5Hasher = new MD5CryptoServiceProvider();

                foreach (string userId in toModifyUsersIds)
                {
                    //User user = Database.getUser(conn, schema, username, false);
                    User user = Database.getUser(conn, schema, null, new Guid(userId), null, false);
                    byte[] HA1 = md5Hasher.ComputeHash(user.Username.ToLower() + ":" + REALM + ":" + password);
                    string encodedPassword = Util.getHexString(HA1);
                    bool res = Database.updateUserPassword(conn, schema, user.Username, encodedPassword);
                    if (!res)
                    {
                        Global.systemLog.Error("UserManagement user : " + u.Username + " : resetpassword operation : Database operation failed on update password for " + user.Username);                        
                    }
                    else
                    {
                        successfulOperationUsernames.Add(userId);
                        Command.sendMailAfterPasswordReset(user, server, request, password);
                    }
                }                
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("UserManagement user : " + u.Username + " : Exception during reset password for user " + u.Username, ex);
                if (successfulOperationUsernames.Count == 0)
                {
                    throw new BirstException("Operation failed for All users : ");
                }
                else if(successfulOperationUsernames.Count < toModifyUsersIds.Length)
                {
                    string failedOperationUsernames = "";
                    List<string> failedOperationUserIds = new List<string>();
                    // get the users not done yet
                    foreach (string toModifyUserId in toModifyUsersIds)
                    {
                        if (!successfulOperationUsernames.Contains(toModifyUserId))
                        {
                            failedOperationUserIds.Add(toModifyUserId);
                        }
                    }
                    
                    // You don't want to fill the screen with a large number of users
                    if (failedOperationUserIds.Count < UserAdministration.FAILED_NUM_USERS_TO_SHOW)
                    {
                        bool retrieveFailedUsers = false;
                        try
                        {
                            foreach (string failedUserId in failedOperationUserIds)
                            {
                                User failedUser = Database.getUser(conn, schema, null, new Guid(failedUserId), null, false);
                                failedOperationUsernames = failedOperationUsernames + failedUser.Username + " ;";
                            }

                            retrieveFailedUsers = true;
                        }
                        catch (Exception ex2)
                        {
                            // If error while retrieving user details..Nothing to do
                            Global.systemLog.Warn("Exception while retreiving failed user details ", ex2);
                        }
                        // If all the failed user info is retrieved, we want to display it on UI
                        if (retrieveFailedUsers)
                        {
                            throw new BirstException("Error Encountered. Password Not reset for the following users : " + failedOperationUsernames);
                        }
                    }
                }

                throw ex;                
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
        }

        

        public static void updateAccountAdminPriveleges(HttpSessionState session, string[] toModifyUsersIds, bool isAdmin)
        {
            User u = Util.getSessionUser(session);
            if (u == null)
            {
                Global.systemLog.Warn("Session User unavailable");
                throw new UserNotLoggedInException();
            }

            // Check if the user is authorized to perform user management operations
            if (!UserManagementUtils.isUserAuthorized(u))
            {
                Global.systemLog.Warn("User unauthorized for user management activities : " + u.ID + " : " + u.Username);
                throw new UserManagementNotAuthorizedException();
            }

            updateAccountAdminPriveleges(u, toModifyUsersIds, isAdmin);
        }

        public static void updateAccountAdminPriveleges(User u, string[] toModifyUsersIds, bool isAdmin)
        {
            QueryConnection conn = null;            
            string schema = Util.getMainSchema();
            try
            {
                conn = ConnectionPool.getConnection();
                Database.updateAccountAdminStatus(conn, schema, isAdmin, u.AdminAccountId, toModifyUsersIds);                
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("UserManagement user : " + u.Username + " : Exception during updating Account Admin priveleges operation", ex);                
                throw ex;
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            } 
        }

        public static void unlockUsers(HttpSessionState session, string[] toModifyUsersIds)
        {
            User u = Util.getSessionUser(session);
            if (u == null)
            {
                Global.systemLog.Warn("Session User unavailable");
                throw new UserNotLoggedInException();
            }

            // Check if the user is authorized to perform user management operations
            if (!UserManagementUtils.isUserAuthorized(u))
            {
                Global.systemLog.Warn("User unauthorized for user management activities : " + u.ID + " : " + u.Username);
                throw new UserManagementNotAuthorizedException();
            }

            unlockUsers(u, toModifyUsersIds);
        }


        public static void updateReleasesForUsers(HttpSessionState session, string[] toModifyUserIds, int releaseType)
        {
            User u = Util.getSessionUser(session);
            if (u == null)
            {
                Global.systemLog.Warn("Session User unavailable");
                throw new UserNotLoggedInException();
            }

            // Check if the user is authorized to perform user management operations
            if (!UserManagementUtils.isUserAuthorized(u))
            {
                Global.systemLog.Warn("User unauthorized for user management activities : " + u.ID + " : " + u.Username);
                throw new UserManagementNotAuthorizedException();
            }

            updateReleasesForUsers(u, toModifyUserIds, releaseType);
        }

        public static void updateReleasesForUsers(User u, string[] toModifyUserIds, int releaseType)
        {
            QueryConnection conn = null;
            string schema = Util.getMainSchema();
            List<string> successfulOperationUsernames = new List<string>();
            try
            {
                if (toModifyUserIds != null && toModifyUserIds.Length > 0)
                {
                    conn = ConnectionPool.getConnection();
                    foreach (String userId in toModifyUserIds)
                    {
                        User user = Database.getUserById(conn, schema, new Guid(userId));
                        if (user != null)
                        {
                            Database.updateUserReleaseType(conn, schema, user.ID, releaseType);
                            successfulOperationUsernames.Add(userId);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("UserManagement : Exception during updating users release info operation ", ex);                
                if (successfulOperationUsernames.Count == 0)
                {
                    throw new BirstException("Unlock Users Operation failed for All users : ");
                }
                else if (successfulOperationUsernames.Count < toModifyUserIds.Length)
                {
                    string failedOperationUsernames = "";
                    List<string> failedOperationUserIds = new List<string>();
                    // get the users not done yet
                    foreach (string toModifyUserId in toModifyUserIds)
                    {
                        if (!successfulOperationUsernames.Contains(toModifyUserId))
                        {
                            failedOperationUserIds.Add(toModifyUserId);
                        }
                    }

                    // You don't want to fill the screen with a large number of users
                    if (failedOperationUserIds.Count < UserAdministration.FAILED_NUM_USERS_TO_SHOW)
                    {
                        bool retrieveFailedUsers = false;
                        try
                        {
                            foreach (string failedUserId in failedOperationUserIds)
                            {
                                User failedUser = Database.getUser(conn, schema, null, new Guid(failedUserId), null, false);
                                failedOperationUsernames = failedOperationUsernames + failedUser.Username + " ;";
                            }

                            retrieveFailedUsers = true;
                        }
                        catch (Exception ex2)
                        {
                            // If error while retrieving user details..Nothing to do
                            Global.systemLog.Warn("Exception while retreiving failed user details ", ex2);
                        }
                        // If all the failed user info is retrieved, we want to display it on UI
                        if (retrieveFailedUsers)
                        {
                            throw new BirstException("Error Encountered. Unable to release the following users : " + failedOperationUsernames);
                        }
                    }
                }

                throw ex;
            }
            finally
            {
                try
                {
                    ConnectionPool.releaseConnection(conn);
                }
                catch (Exception ex2)
                {
                    Global.systemLog.Warn("updateRelaseInfo : Error in releasing connections ", ex2);
                }
            }
        }

        public static void unlockUsers(User u, string[] toModifyUsersIds)
        {
            QueryConnection conn = null;
            List<string> successfulOperationUsernames = new List<string>();
            string schema = Util.getMainSchema();
            try
            {
                conn = ConnectionPool.getConnection();                
                foreach (string userId in toModifyUsersIds)
                {
                    User user = Database.getUser(conn, schema, null, new Guid(userId), null, false);
                    if (user != null)
                    {
                        CommandHelper.unlockUser(u, user.Username);
                        successfulOperationUsernames.Add(userId);
                    }
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("UserManagement user : " + u.Username + " : Exception during unlock users operation", ex);
                if (successfulOperationUsernames.Count == 0)
                {
                    throw new BirstException("Unlock Users Operation failed for All users : ");
                }
                else if (successfulOperationUsernames.Count < toModifyUsersIds.Length)
                {
                    string failedOperationUsernames = "";
                    List<string> failedOperationUserIds = new List<string>();
                    // get the users not done yet
                    foreach (string toModifyUserId in toModifyUsersIds)
                    {
                        if (!successfulOperationUsernames.Contains(toModifyUserId))
                        {
                            failedOperationUserIds.Add(toModifyUserId);
                        }
                    }                    

                    // You don't want to fill the screen with a large number of users
                    if (failedOperationUserIds.Count < UserAdministration.FAILED_NUM_USERS_TO_SHOW)
                    {
                        bool retrieveFailedUsers = false;
                        try
                        {
                            foreach (string failedUserId in failedOperationUserIds)
                            {
                                User failedUser = Database.getUser(conn, schema, null, new Guid(failedUserId), null, false);
                                failedOperationUsernames = failedOperationUsernames + failedUser.Username + " ;";
                            }

                            retrieveFailedUsers = true;
                        }
                        catch (Exception ex2)
                        {
                            // If error while retrieving user details..Nothing to do
                            Global.systemLog.Warn("Exception while retreiving failed user details ", ex2);
                        }
                        // If all the failed user info is retrieved, we want to display it on UI
                        if (retrieveFailedUsers)
                        {
                            throw new BirstException("Error Encountered. Unable to Unlock the following users : " + failedOperationUsernames);
                        }
                    }

                }

                throw ex;
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
        }


        public static void setExternalDBOption(HttpSessionState session, string[] toModifyUsersIds, bool enableExternalDB)
        {
            User u = Util.getSessionUser(session);
            if (u == null)
            {
                Global.systemLog.Warn("Session User unavailable");
                throw new UserNotLoggedInException();
            }

            // Check if the user is authorized to perform user management operations
            if (!UserManagementUtils.isUserAuthorized(u))
            {
                Global.systemLog.Warn("User unauthorized for user management activities : " + u.ID + " : " + u.Username);
                throw new UserManagementNotAuthorizedException();
            }

            setExternalDBOption(u, toModifyUsersIds, enableExternalDB);
        }

        public static void setExternalDBOption(User u, string[] toModifyUserIds, bool enableExternalDB)
        {
            QueryConnection conn = null;
            List<string> successfulOperationUsernames = new List<string>();
            String schema = Util.getMainSchema();
            try
            {
                conn = ConnectionPool.getConnection();

                if (toModifyUserIds != null && toModifyUserIds.Length > 0)
                {
                    conn = ConnectionPool.getConnection();
                    foreach (String userId in toModifyUserIds)
                    {
                        User user = Database.getUserById(conn, schema, new Guid(userId));
                        if (user != null)
                        {
                            Database.updateExternalDBFlag(conn, schema, user.ID, enableExternalDB);
                            successfulOperationUsernames.Add(userId);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("UserManagement user : " + u.Username + " : Exception while setting external db flag ", ex);
                if (successfulOperationUsernames.Count == 0)
                {
                    throw new BirstException("Operation failed for All users : ");
                }
                else if (successfulOperationUsernames.Count < toModifyUserIds.Length)
                {

                    string failedOperationUsernames = "";
                    List<string> failedOperationUserIds = new List<string>();
                    // get the users not done yet
                    foreach (string toModifyUserId in toModifyUserIds)
                    {
                        if (!successfulOperationUsernames.Contains(toModifyUserId))
                        {
                            failedOperationUserIds.Add(toModifyUserId);
                        }
                    }

                    // You don't want to fill the screen with a large number of users
                    if (failedOperationUserIds.Count < UserAdministration.FAILED_NUM_USERS_TO_SHOW)
                    {
                        bool retrieveFailedUsers = false;
                        try
                        {
                            foreach (string failedUserId in failedOperationUserIds)
                            {
                                User failedUser = Database.getUser(conn, schema, null, new Guid(failedUserId), null, false);
                                failedOperationUsernames = failedOperationUsernames + failedUser.Username + " ;";
                            }

                            retrieveFailedUsers = true;
                        }
                        catch (Exception ex2)
                        {
                            // If error while retrieving user details..Nothing to do
                            Global.systemLog.Warn("Exception while retreiving failed user details ", ex2);
                        }
                        // If all the failed user info is retrieved, we want to display it on UI
                        if (retrieveFailedUsers)
                        {
                            throw new BirstException("Error Encountered. Unable to modify warehouse option for the following users : " + failedOperationUsernames);
                        }
                    }
                }
                throw ex;
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
        }


        public static void updateUsersStatus(HttpSessionState session, string[] toModifyUsersIds, bool enable)
        {
            User u = Util.getSessionUser(session);
            if (u == null)
            {
                Global.systemLog.Warn("Session User unavailable");
                throw new UserNotLoggedInException();
            }

            // Check if the user is authorized to perform user management operations
            if (!UserManagementUtils.isUserAuthorized(u))
            {
                Global.systemLog.Warn("User unauthorized for user management activities : " + u.ID + " : " + u.Username);
                throw new UserManagementNotAuthorizedException();
            }

            updateUsersStatus(u, toModifyUsersIds, enable);
        }

        public static void updateUsersStatus(User u, string[] toModifyUsersIds, bool enable)
        {
            QueryConnection conn = null;
            string schema = Util.getMainSchema();
            List<string> successfulOperationUsernames = new List<string>();

            try
            {
                conn = ConnectionPool.getConnection();
                foreach (string userId in toModifyUsersIds)
                {
                    User toUpdateUser = Database.getUser(conn, schema, null, new Guid(userId), null, false);
                    CommandHelper.enableUser(u, toUpdateUser.Username, enable);
                    successfulOperationUsernames.Add(userId);
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("UserManagement user : " + u.Username + " : Exception during update user status ", ex);
                if (successfulOperationUsernames.Count == 0)
                {
                    throw new BirstException("Unlock Users Operation failed for All users : ");
                }
                else if (successfulOperationUsernames.Count < toModifyUsersIds.Length)
                {
                    string failedOperationUsernames = "";
                    List<string> failedOperationUserIds = new List<string>();                    
                    // get the users not done yet
                    foreach (string toModifyUserId in toModifyUsersIds)
                    {
                        if (!successfulOperationUsernames.Contains(toModifyUserId))
                        {
                            failedOperationUserIds.Add(toModifyUserId);                            
                        }
                    }

                    // You don't want to fill the screen with a large number of users
                    if (failedOperationUserIds.Count < UserAdministration.FAILED_NUM_USERS_TO_SHOW)
                    {
                        bool retrieveFailedUsers = false;
                        try
                        {
                            foreach (string failedUserId in failedOperationUserIds)
                            {
                                User failedUser = Database.getUser(conn, schema, null, new Guid(failedUserId), null, false);
                                failedOperationUsernames = failedOperationUsernames + failedUser.Username + " ;";
                            }

                            retrieveFailedUsers = true;
                        }
                        catch (Exception ex2)
                        {
                            // If error while retrieving user details..Nothing to do
                            Global.systemLog.Warn("Exception while retreiving failed user details ", ex2);
                        }
                        // If all the failed user info is retrieved, we want to display it on UI
                        if (retrieveFailedUsers)
                        {
                            throw new BirstException("Unable to update the following users : " + failedOperationUsernames);
                        }
                    }
                }
                throw ex;
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
        }

        public static List<string> processCreateUserFile(HttpSessionState session, HttpServerUtility server, HttpRequest request, string clientFileName)
        {
            User u = Util.getSessionUser(session);
            if (u == null)
            {
                Global.systemLog.Warn("Session User unavailable");
                throw new UserNotLoggedInException();
            }

            // Check if the user is authorized to perform user management operations
            if (!UserManagementUtils.isUserAuthorized(u))
            {
                Global.systemLog.Warn("User unauthorized for user management activities : " + u.ID + " : " + u.Username);
                throw new UserManagementNotAuthorizedException();
            }

            return processCreateUserFile(u, clientFileName, session, server, request);
        }

        public static List<string> processCreateUserFile(User u, string clientFileName, HttpSessionState session, HttpServerUtility server, HttpRequest request)
        {   
            
            string commandFileName = UserManagementUtils.getTempFileName(u.ID, clientFileName);
            FileInfo fi = new FileInfo(commandFileName);
            if(!fi.Exists)
            {
                Global.systemLog.Warn("Unable to find the createUser file " + clientFileName);
                throw (new BirstException("Processing File Error"));
            }
            
            List<string> log = new List<string>();
            Command cmd = new Command(null, u, log, request, server, session, null);

            StreamReader reader = null;
            try
            {
                reader = new StreamReader(commandFileName);
                string line = null;
                while ((line = reader.ReadLine()) != null)
                {
                    List<string> tokens = Command.gettokens(line);
                    if (checkCommandFormat(tokens, log))
                    {
                        cmd.processCommand(line);
                    }
                }
            }
            finally
            {
                if (reader != null)
                    reader.Close();
                fi.Delete();
            }
            return log;
        }

        private static bool checkCommandFormat(List<string> tokens, List<string> log)
        {
            bool allowed = false;
            if (tokens[0].ToLower() == "createuser")
            {


                allowed = true;
            }
            else
            {
                log.Add("Invalid Command");
            }
            
            return allowed;
        }

        public static List<string> getCreateUserAllowedDomains(HttpSessionState session)
        {
            User u = Util.getSessionUser(session);
            if (u == null)
            {
                Global.systemLog.Warn("Session User unavailable");
                throw new UserNotLoggedInException();
            }

            // Check if the user is authorized to perform user management operations
            if (!UserManagementUtils.isUserAuthorized(u))
            {
                Global.systemLog.Warn("User unauthorized for user management activities : " + u.ID + " : " + u.Username);
                throw new UserManagementNotAuthorizedException();
            }

            return getCreateUserAllowedDomains(u);
        }

        public static List<string> getCreateUserAllowedDomains(User u)
        {
            List<string> masks = new List<string>();
            QueryConnection conn = null;
            string schema = Util.getMainSchema();
            try
            {
                conn = ConnectionPool.getConnection();
                masks = Database.getAllowedUsernameMasks(conn, schema, u.ID);
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Exception while getting CreateUser format masks for user " + u.Username);
                throw ex;
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }

            return masks;
        }

        public static List<SpaceTypeDetail> getAllAdminRedShiftClusters(User u)
        {
            // Check if the user is authorized to perform user management operations
            if (!UserManagementUtils.isUserAuthorized(u))
            {
                Global.systemLog.Warn("User unauthorized for user management activities : " + u.ID + " : " + u.Username);
                throw new UserManagementNotAuthorizedException();
            }
            return getAllRedShiftClusters(u.AdminAccountId, u);
        }

        public static List<SpaceTypeDetail> getAllAvailableRedShiftClusters(User user)
        {            
            return getAllRedShiftClusters(user.ManagedAccountId, user);
        }

        /// <summary>
        /// Returns all the red shift cluster instances which are allowed for the user        
        /// </summary>
        /// <param name="user"></param>
        /// <param name="filterOnUser"></param>
        /// <returns></returns>
        public static List<SpaceTypeDetail> getAllRedShiftClusters(Guid accountId, User u)
        {
            List<SpaceTypeDetail> response = null;
            if (accountId != Guid.Empty)
            {
                // retrieve the space types from account override table
                // then use the comma separated values to get details from space config table
                AccountDetails accountDetails = null;
                QueryConnection conn = null;
                string schema = Util.getMainSchema();
                try
                {
                    conn = ConnectionPool.getConnection();
                    accountDetails = Database.getAccountDetails(conn, schema, accountId, u);
                    if (accountDetails != null)
                    {
                        List<SpaceConfig> allSpaceConfigs = getAllAccountSpaceConfigs(conn, schema, accountDetails, Database.REDSHIFT);
                        if (allSpaceConfigs != null && allSpaceConfigs.Count > 0)
                        {
                            response = new List<SpaceTypeDetail>();
                            foreach (SpaceConfig sc in allSpaceConfigs)
                            {
                                SpaceTypeDetail sTypeDetail = new SpaceTypeDetail();
                                sTypeDetail.Type = sc.Type;
                                sTypeDetail.Name = sc.Name; 
                                sTypeDetail.Description = sc.Description;
                                sTypeDetail.DatabaseConnectString = sc.DatabaseConnectString;
                                sTypeDetail.DatabaseType = sc.DatabaseType;
                                sTypeDetail.AdminUser = sc.AdminUser;
                                sTypeDetail.AdminPwd = sc.AdminPwd;
                                sTypeDetail.RegionID = sc.regionId;
                                response.Add(sTypeDetail);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Global.systemLog.Error("Exception while retrieving red shift cluster details for account " + accountId, ex);
                    throw ex;
                }
                finally
                {
                    ConnectionPool.releaseConnection(conn);
                }
            }
            return response;
        }

        private static List<SpaceConfig> getAllAccountSpaceConfigs(QueryConnection conn, string schema, AccountDetails accountDetails, 
            string filterDatabaseType)
        {
            if (accountDetails != null)
            {
                List<int> spaceTypes = accountDetails.getSpaceTypesList();
                if (spaceTypes != null && spaceTypes.Count > 0)
                {
                    List<SpaceConfig> response = new List<SpaceConfig>();
                    foreach (int sType in spaceTypes)
                    {
                        SpaceConfig sc = Database.getSpaceConfig(conn, schema, sType);
                        if (sc == null)
                        {
                            Global.systemLog.Info("Skipping - Cannot find details for space type " + sType);
                            continue;
                        }

                        if (filterDatabaseType != null && filterDatabaseType != sc.DatabaseType)
                        {
                            continue;
                        }
                        response.Add(sc);
                    }
                    return response;
                }
            }
            return null;
        }

        private static void validateAccountUniqueClusterName(string name, List<SpaceConfig> spaceConfigs)
        {
            if (name != null && name.Trim().Length > 0 && spaceConfigs != null && spaceConfigs.Count > 0)
            {
                foreach (SpaceConfig sc in spaceConfigs)
                {
                    if(sc.Name != null && name.Trim().Equals(sc.Name, StringComparison.OrdinalIgnoreCase))
                    {
                        throw new BirstException("Name is already used by other configuration");
                    }
                }
            }
        }

        internal static void validateAccountDetails(Guid accountID, AccountDetails accountDetails)
        {
            if (accountDetails == null)
            {
                Global.systemLog.Error("No account information found for accountID " + accountID);
                throw new BirstException("Unable to retrieve account details");
            }
        }

        public static void addRedShiftCluster(User user, string name, string description, int regionId, string jdbcUrl, 
            string dbUser, string dbPwd, out int timeCreationStatus)
        {            
            validateUserManagementAuthorization(user);
            QueryConnection conn = null;            
            try
            {
                conn = ConnectionPool.getConnection();
                string schema = Util.getMainSchema();
                int maxTypeID = Database.getMaxSpaceConfigTypeID(conn, schema);
                maxTypeID = maxTypeID < Database.REDSHIFT_BASE_SPACE_CONFIG ? Database.REDSHIFT_BASE_SPACE_CONFIG : maxTypeID + 1;

                // format the override paramter for "SpaceTypes"
                Guid accountID = user.AdminAccountId;                
                AccountDetails accountDetails = Database.getAccountDetails(conn, schema, accountID, user);
                validateAccountDetails(accountID, accountDetails);
                List<SpaceConfig> accountSpaceConfigs = getAllAccountSpaceConfigs(conn, schema, accountDetails, Database.REDSHIFT);
                validateAccountUniqueClusterName(name, accountSpaceConfigs);
                List<int> spaceTypes = accountDetails.getSpaceTypesList();
                if(spaceTypes == null)
                {
                    spaceTypes = new List<int>();
                }
                spaceTypes.Add(maxTypeID);
                StringBuilder sb = new StringBuilder();
                foreach (int i in spaceTypes)
                {
                    sb.Append(i);
                    sb.Append(",");
                }
                string value = sb.ToString().TrimEnd(',');

                SpaceConfig sc = new SpaceConfig();
                sc.DatabaseType = Database.REDSHIFT;
                sc.DatabaseDriver = Database.REDSHIFT_JDBC_DRIVER_NAME;
                sc.Type = maxTypeID;
                sc.Name = name;
                sc.Description = description;
                sc.DatabaseConnectString = jdbcUrl;                
                sc.AdminUser = dbUser;
                sc.AdminPwd = dbPwd;
                setURLsFromGlobalProcessEngine(sc);
                setGlobalDefaultsOnRedshiftClusters(sc);
                if (regionId > 0)
                {
                    AWSRegion aRegion = Database.getRedshiftRegionDetails(conn, schema, regionId);
                    if (aRegion != null)
                    {
                        sc.regionId = regionId;
                        sc.DatabaseLoadDir = aRegion.UploadBucket;
                    }
                }

                Database.createSpaceConfig(conn, schema, sc);
                Database.setOverrideParameter(conn, schema, user.AdminAccountId, AccountDetails.OVERRIDE_PARAM_SPACETYPES, value);
                timeCreationStatus = Status.COMPLETE;
                try
                {
                    Status.StatusStep endStep = Status.checkTimeDimensionStatus(sc);
                    if (endStep != null)
                    {
                        if (endStep.result == Status.NONE)
                        {
                            // meaning time dimension do not exist
                            startTimeCreation(user, sc.Type, false);
                            timeCreationStatus = Status.STARTED;
                        }
                        else
                        {
                            timeCreationStatus = endStep.result;
                        }
                    }
                }
                catch (Exception ex2)
                {
                    // if everything else succeeded except creating time dimension, then properly display the message to user
                    timeCreationStatus = -1;
                    Global.systemLog.Error("Error while creating time dimension tables", ex2);
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Exception while adding redsfhift cluster for user " + user.Username, ex);
                throw ex;
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
        }

        // This is to ensure that there are non-null valid values for 
        // url and localurl columns for newly added space config. 
        // These are effecively same for all space configs but needed a reference for valid values
        private static void setURLsFromGlobalProcessEngine(SpaceConfig sc)
        {
            SpaceConfig referenceSpaceConfig = Util.getSpaceConfiguration(Space.NEW_SPACE_TYPE);
            sc.URL = referenceSpaceConfig.URL;
            sc.LocalURL = referenceSpaceConfig.LocalURL;
        }

        public static void modifyRedShiftCluster(User user, int type, string name, string description, int regionId, string jdbcUrl,
            string dbUser, string dbPwd)
        {
            validateUserManagementAuthorization(user);
            QueryConnection conn = null;
            OdbcTransaction transaction = null;
            try
            {
                conn = ConnectionPool.getConnection();
                string schema = Util.getMainSchema();                
                SpaceConfig sc = Database.getSpaceConfig(conn, schema, type);
                if (sc == null || sc.DatabaseType != Database.REDSHIFT)
                {
                    Global.systemLog.Error("Cannot find red shift cluster spaceConfig details for type " + type);
                    throw new BirstException("Cannot find the redshift cluster details");
                }
                AccountDetails accountDetails = Database.getAccountDetails(conn, schema, user.AdminAccountId, user);               
                validateAccountDetails(user.AdminAccountId, accountDetails);
                List<SpaceConfig> allRedShiftAccountClusters = getAllAccountSpaceConfigs(conn, schema, accountDetails, Database.REDSHIFT);
                List<SpaceConfig> updatedRedShiftAccountClusters = new List<SpaceConfig>(allRedShiftAccountClusters);
                foreach (SpaceConfig toDelete in allRedShiftAccountClusters)
                {
                    if (toDelete.Type == type)
                        updatedRedShiftAccountClusters.Remove(toDelete);
                }

                validateAccountUniqueClusterName(name, updatedRedShiftAccountClusters);
                setGlobalDefaultsOnRedshiftClusters(sc);
                // If only non-essentials stuff like name, description changes, no need to update spaces table
                bool needToUpdateSpaces = false;
                if(jdbcUrl != sc.DatabaseConnectString || dbUser != sc.AdminUser || dbPwd != sc.AdminPwd                     
                    || (regionId > 0 && sc.regionId > 0 && regionId != sc.regionId))
                {
                    needToUpdateSpaces = true;
                }
                
                
                sc.Name = name;
                sc.Description = description;
                sc.DatabaseConnectString = jdbcUrl;                
                sc.AdminUser = dbUser;
                sc.AdminPwd = dbPwd;
                // region id changes occur for releases where regions are exposed
                // for older created regions, regions won't be allowed to alter
                if (regionId > 0 && sc.regionId > 0)
                {
                    AWSRegion aRegion = Database.getRedshiftRegionDetails(conn, schema, regionId);
                    if (aRegion != null)
                    {
                        sc.regionId = regionId;
                        sc.DatabaseLoadDir = aRegion.UploadBucket;
                    }
                }

                List<SpaceMembership> toUpdateSpaces = needToUpdateSpaces ? Database.getSpaceTypesUsage(conn, schema, sc, user.AdminAccountId) : null;
                string errorMessage = validateSpaceBusyOperation(toUpdateSpaces);
                if (errorMessage != null && errorMessage.Trim().Length > 0)
                {
                    throw new BirstException("Cannot modify cluster details." + errorMessage);
                }

                transaction = conn.BeginTransaction();
                Database.updateRedshiftClusterSpaceConfig(conn, schema, sc, transaction);                
                if (toUpdateSpaces != null && toUpdateSpaces.Count > 0)
                {
                    foreach (SpaceMembership sm in toUpdateSpaces)
                    {
                        Database.updateSpaceConfigInSpacesTable(conn, schema, sm.Space.ID, sc, transaction);
                    }
                }
                transaction.Commit();
            }
            catch (Exception ex)
            {               
                Global.systemLog.Error("Exception while modifying redsfhift cluster for user " + user.Username, ex);
                if (transaction != null)
                {
                    transaction.Rollback();
                }
                throw ex;
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
        }

        
        internal static string validateSpaceBusyOperation(List<SpaceMembership> smList)
        {
            if (smList != null && smList.Count > 0)
            {
                foreach (SpaceMembership sm in smList)
                {                    
                    int spOpID = SpaceOpsUtils.getSpaceAvailability(sm.Space.ID);
                    string spaceName = sm.Space.Name;
                    if (!SpaceOpsUtils.isSpaceAvailable(spOpID))
                    {
                        SpaceOpsUtils.getAvailabilityMessage(spaceName, spOpID); 
                    }

                    // check for the processing status for from space
                    if (ApplicationLoader.isSpaceBeingPublished(sm.Space))
                    {
                        return  spaceName + " is being published";
                    }
                }
            }
            return null;
        }

        internal static void setGlobalDefaultsOnRedshiftClusters(SpaceConfig sc)
        {
            if (sc.DatabaseType == Database.REDSHIFT)
            {
                sc.awsAccessKeyId = Util.getUploadAWSAccessKey();
                sc.awsSecretKey = Util.getUploadAWSSecretKey();
                sc.DatabaseLoadDir = Util.getUploadS3BucketName(sc.DatabaseConnectString);
            }
        }


        public static List<AWSRegion> getAvailableRegionsForRedshift()
        {
            List<AWSRegion> activeRegions = null;
            QueryConnection conn = null;            
            try
            {
                conn = ConnectionPool.getConnection();
                List<AWSRegion> allRegions = Database.getRedshiftAvailableRegions(conn, Util.getMainSchema());
                if (allRegions != null && allRegions.Count > 0)
                {
                    activeRegions = new List<AWSRegion>();
                    foreach (AWSRegion aRegion in allRegions)
                    {
                        if (!aRegion.Active)
                        {
                            continue;
                        }
                        activeRegions.Add(aRegion);
                    }
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Exception while retrieving supported region details for redshift " , ex);
                throw ex;
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
            return activeRegions;
        }

        public static List<SpaceSummary> getSpaceTypesUsage(User user, int spaceType)
        {
            validateUserManagementAuthorization(user);
            QueryConnection conn = null;
            string schema = Util.getMainSchema();
            try
            {
                conn = ConnectionPool.getConnection();
                SpaceConfig sc = Database.getSpaceConfig(conn, schema, spaceType);
                if (sc != null)
                {
                    List<SpaceMembership> typeUsageList = Database.getSpaceTypesUsage(conn, schema, sc, user.ManagedAccountId);
                    if (typeUsageList != null && typeUsageList.Count > 0)
                    {
                        List<SpaceSummary> response = new List<SpaceSummary>();
                        foreach (SpaceMembership sm in typeUsageList)
                        {
                            SpaceSummary spSummary = new SpaceSummary();
                            spSummary.SpaceID = sm.Space.ID.ToString();
                            spSummary.SpaceName = sm.Space.Name;
                            spSummary.Owner = sm.Owner;
                            spSummary.OwnerUsername = sm.OwnerUsername;
                            response.Add(spSummary);
                        }
                        return response;
                    }
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Exception while retrieving space type usages for spaceType " + spaceType, ex);
                throw ex;
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
            return null;
        }

        public static void deleteRedShiftCluster(HttpSessionState session, User user, int type)
        {
            validateUserManagementAuthorization(user);
            QueryConnection conn = null;
            try
            {
                conn = ConnectionPool.getConnection();
                string schema = Util.getMainSchema();
                SpaceConfig sc = Database.getSpaceConfig(conn, schema, type);
                if (sc == null || sc.DatabaseType != Database.REDSHIFT)
                {
                    Global.systemLog.Error("Cannot find red shift cluster spaceConfig details for type " + type);
                    throw new BirstException("Cannot find the redshift cluster details");
                }

                // format the override paramter for "SpaceTypes"
                Guid accountID = user.AdminAccountId;
                AccountDetails accountDetails = Database.getAccountDetails(conn, schema, accountID, user);
                validateAccountDetails(accountID, accountDetails);
                List<SpaceMembership> toUpdateSpaces = Database.getSpaceTypesUsage(conn, schema, sc, user.AdminAccountId);
                string errorMessage = validateSpaceBusyOperation(toUpdateSpaces);
                if (errorMessage != null && errorMessage.Trim().Length > 0)
                {
                    throw new BirstException("Cannot delete cluster details." + errorMessage);
                }
                List<int> spaceTypes = null;
                if (accountDetails != null)
                {
                    spaceTypes = accountDetails.getSpaceTypesList();
                    if (spaceTypes != null && spaceTypes.Count > 0)
                    {
                        StringBuilder sb = new StringBuilder();
                        foreach (int i in spaceTypes)
                        {
                            if (i == sc.Type)
                                continue;
                            sb.Append(i);
                            sb.Append(",");
                        }
                        string value = sb.ToString().TrimEnd(',');
                        Database.setOverrideParameter(conn, schema, accountID, AccountDetails.OVERRIDE_PARAM_SPACETYPES, value);
                    }
                }

                // delete all the used spaces
                if (toUpdateSpaces != null && toUpdateSpaces.Count > 0)
                {
                    foreach (SpaceMembership sm in toUpdateSpaces)
                    {
                        DeleteDataService.deleteSpace(sm.Space, user, session);
                    }
                }
                Database.removeSpaceConfig(conn, schema, type);
                
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Exception while removing redsfhift cluster for user " + user.Username, ex);
                throw ex;
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
        }

        public static void validateUserManagementAuthorization(User u)
        {
            // Check if the user is authorized to perform user management operations
            if (!UserManagementUtils.isUserAuthorized(u))
            {
                Global.systemLog.Warn("User unauthorized for user management activities : " + u.ID + " : " + u.Username);
                throw new UserManagementNotAuthorizedException();
            }
        }

        internal static Status.StatusStep validateTimeDimensions(int type)
        {
            SpaceConfig sc = Util.getSpaceConfigurationNoException(type);           
            if ((sc == null || sc.DatabaseType != Database.REDSHIFT) && (sc == null || sc.DatabaseType != Database.HANA))
            {                            
                Global.systemLog.Error("Cannot find red shift cluster/sap hana spaceConfig details for type " + type);
                throw new BirstException("Cannot find the redshift cluster/sap hana details");
            }

            return Status.checkTimeDimensionStatus(sc);
        }

        internal static void startTimeCreation(User user, int type, bool dropIfExists)
        {
            SpaceConfig sc = Util.getSpaceConfigurationNoException(type);           
            if ((sc == null || sc.DatabaseType != Database.REDSHIFT) && (sc == null || sc.DatabaseType != Database.HANA))
            {
                Global.systemLog.Error("Cannot find red shift cluster/sap hana spaceConfig details for type " + type);
                throw new BirstException("Cannot find the redshift cluster/sap hana details");
            }

            // see if there is a time creation already in progress
            Status.StatusStep endStep = validateTimeDimensions(type);
            if (endStep != null && endStep.result == Status.RUNNING)
            {
                Global.systemLog.Error("Time Dimensions are already being created");
                string userMessage = "Time Dimensions are already being created";
                throw new BirstException(userMessage);
            }

            if (endStep != null && !dropIfExists
                && endStep.result == Status.COMPLETE)
            {
                // meaning time dimension already exists, no need to forcefully recreate them
                return;
            }
            

            QueryConnection conn = null;
            try
            {
                conn = ConnectionPool.getConnection();
                List<SpaceMembership> spaceUsages = Database.getSpaceTypesUsage(conn, Util.getMainSchema(), sc, user.AdminAccountId);
                string spaceBusyMsg = validateSpaceBusyOperation(spaceUsages);
                if (spaceBusyMsg != null && spaceBusyMsg.Trim().Length > 0)
                {
                    throw new BirstException("Cannot create time dimension tables." + spaceBusyMsg);
                }
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }

            string timeRepositoryPath = ApplicationLoader.setupRedshiftTimeRepository(sc, ProcessData.contentDir);
            TrustedService.TrustedService ts = Util.getSMIWebTrustedService(sc.LocalURL);
            XmlNode[] obj = null;
            if (sc.DatabaseType == Database.REDSHIFT)
            {
                string folderName = Util.getS3BucketName(sc);
                string awsAccessKey = Util.getAWSAccessKey();
                string awsSecretKey = Util.getAWSSecretKey();
                // Get AWS temporary session credentials
                AWSCredentials s3credentials = Acorn.Utils.AWSUtils.getTemporaryCredentials(awsAccessKey, awsSecretKey, folderName);

                obj = (XmlNode[])ts.startTimeCreation(user.Username, timeRepositoryPath, sc.DatabaseType, folderName,
                    s3credentials.GetCredentials().AccessKey, s3credentials.GetCredentials().ClearSecretKey, s3credentials.GetCredentials().Token);
                // obj is OMElement, need to parse 
            }
            else
            {
                string folderName = Util.getTimeDimensionFolder(sc);
                obj = (XmlNode[])ts.startTimeCreation(user.Username, timeRepositoryPath, sc.DatabaseType, folderName,
                    null, null, null);
                // obj is OMElement, need to parse 
            }
            WebServiceResponseXml wsResponseXml = WebServiceResponseXml.getWebServiceResponseXml(obj[0]);
            if (!wsResponseXml.isSuccesssful())
            {
                string errorCode = wsResponseXml.getErrorCode();
                string errorMessage = wsResponseXml.getErrorMessage();
                Global.systemLog.Error("Error from SMIWeb while calling start time creation - " +
                    "errorCode - " + wsResponseXml.getErrorCode() +
                    "errorMessage - " + wsResponseXml.getErrorMessage());
                string userMessage = "Internal Error : Cannot create time dimension tables. Please try again";
                throw new BirstException(userMessage);
            }
        }
    }

}
