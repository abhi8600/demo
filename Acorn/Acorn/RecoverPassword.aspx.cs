﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Net.Mail;
using System.Data.Odbc;
using Acorn.DBConnection;

namespace Acorn
{
    public partial class RecoverPassword : System.Web.UI.Page
    {
        private static string mainSchema = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["MainSchema"];
        private User user = null;
        bool passwordValid = false;
        private static string useRecaptcha = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["UseRecaptcha"];
        private static string RecaptchaPublicKey = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["RecaptchaPublicKey"];
        private static string RecaptchaPrivateKey = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["RecaptchaPrivateKey"];
        private static string termsOfServiceURL = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["TermsOfServiceLink"];
        private static string privacyPolicyURL = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["PrivacyPolicyLink"];
        private static string contactUsURL = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["ContactUsLink"];
        private static string copyRight = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["copyRight"];

        protected void Page_Load(object sender, EventArgs e)
        {
            Dictionary<string, string> overrideParameters = new Dictionary<string, string>();
            Util.setUpHeaders(Response, false);
            string Locale = "en_US";
            overrideParameters = Util.getLocalizedOverridenParams(Locale, null);

            if (overrideParameters != null && overrideParameters.Count > 0)
            {
                if (overrideParameters.ContainsKey(OverrideParameterNames.TERMS_OF_SERVICE_URL))
                {
                    if (overrideParameters[OverrideParameterNames.TERMS_OF_SERVICE_URL] != "")
                    {
                        TermsOfServiceLink.NavigateUrl = overrideParameters[OverrideParameterNames.TERMS_OF_SERVICE_URL];
                    }
                    else
                    {
                        TermsOfServicePlaceHolder.Visible = false;
                    }
                }
                else
                {
                    TermsOfServiceLink.NavigateUrl = termsOfServiceURL;
                }

                if (overrideParameters.ContainsKey(OverrideParameterNames.PRIVACY_POLICY_URL))
                {
                    if (overrideParameters[OverrideParameterNames.PRIVACY_POLICY_URL] != "")
                    {
                        PrivacyPolicyLink.NavigateUrl = overrideParameters[OverrideParameterNames.PRIVACY_POLICY_URL];
                    }
                    else
                    {
                        PrivacyPolicyPlaceHolder.Visible = false;
                    }
                }
                else
                {
                    PrivacyPolicyLink.NavigateUrl = privacyPolicyURL;
                }

                if (overrideParameters.ContainsKey(OverrideParameterNames.CONTACT_US_URL))
                {
                    if (overrideParameters[OverrideParameterNames.CONTACT_US_URL] != "")
                    {
                        ContactUsLink.NavigateUrl = overrideParameters[OverrideParameterNames.CONTACT_US_URL];
                    }

                    else
                    {
                        ContactUsPlaceHolder.Visible = false;
                    }
                }
                else
                {
                    ContactUsLink.NavigateUrl = contactUsURL;
                }

                if (overrideParameters.ContainsKey(OverrideParameterNames.COPYRIGHT_TEXT))
                {
                    if (overrideParameters[OverrideParameterNames.COPYRIGHT_TEXT] != "")
                    {
                        copyRight = overrideParameters[OverrideParameterNames.COPYRIGHT_TEXT];
                        Copyright.Text = HttpUtility.HtmlEncode(copyRight);

                    }
                    else
                    {
                        Copyright.Text = "";
                    }

                }
                else
                {
                    Copyright.Text = HttpUtility.HtmlEncode(copyRight);
                }

                if (overrideParameters.ContainsKey(OverrideParameterNames.PAGE_TITLE))
                {
                    if (overrideParameters[OverrideParameterNames.PAGE_TITLE] != "")
                    {
                        WebAdminMasterHeadTag.Title = overrideParameters[OverrideParameterNames.PAGE_TITLE];
                    }
                    else
                    {
                        WebAdminMasterHeadTag.Title = "";
                    }
                }
                else
                {
                    WebAdminMasterHeadTag.Title = "Birst";
                }
            }
            else
            {
                string copyRight = Util.getCopyright();

                if (termsOfServiceURL != null)
                    TermsOfServiceLink.NavigateUrl = termsOfServiceURL;
                else
                    TermsOfServicePlaceHolder.Visible = false;
                if (privacyPolicyURL != null)
                    PrivacyPolicyLink.NavigateUrl = privacyPolicyURL;
                else
                    PrivacyPolicyPlaceHolder.Visible = false;
                if (contactUsURL != null)
                    ContactUsLink.NavigateUrl = contactUsURL;
                else
                    ContactUsPlaceHolder.Visible = false;

                WebAdminMasterHeadTag.Title = "Birst";

                Copyright.Text = HttpUtility.HtmlEncode(copyRight);
            }

            Version.Text = HttpUtility.HtmlEncode(Util.getAppVersion());

            Recaptcha.RecaptchaControl control = (Recaptcha.RecaptchaControl)RecoverPasswordControl.UserNameTemplateContainer.FindControl("recaptcha");

            if (useRecaptcha == null || useRecaptcha.ToLower().Equals("true"))
            {
                if (Global.connectionIsViaHTTPS)
                {
                    if (control != null)
                        control.OverrideSecureMode = true;
                }
                control.PublicKey = RecaptchaPublicKey;
                control.PrivateKey = RecaptchaPrivateKey;
            }
            else
            {
                control.Enabled = false;
                control.Visible = false;
            }

            MultiView.ActiveViewIndex = 0;
            string recordId = Request["token"];
            if (recordId != null && recordId.Length != 0)
            {
                MultiView.ActiveViewIndex = 1;
                if (!IsPostBack && !validateToken(recordId))
                {
                    MultiView.ActiveViewIndex = 3;
                }
            }
        }

        protected void RecoverPasswordControl_SendingMail(object sender, MailMessageEventArgs e)
        {
            PasswordResetToken token = new PasswordResetToken();
            token.tokenID = Guid.NewGuid();
            token.userID = user.ID;
            token.endOfLife = DateTime.Now.AddHours(1);

            QueryConnection conn = null;
            try
            {
                conn = ConnectionPool.getConnection();
                Database.createPasswordResetToken(conn, mainSchema, token);
                e.Message.ReplyToList.Add(new MailAddress((string)System.Web.Configuration.WebConfigurationManager.AppSettings["BounceAddress"]));
                e.Message.From = new MailAddress((string)System.Web.Configuration.WebConfigurationManager.AppSettings["BounceAddress"]);
                e.Message.Body = e.Message.Body.Replace("${LINK}", Util.getRequestBaseURL(Request) + "/RecoverPassword.aspx?token=" + token.tokenID.ToString());
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
        }

        protected void RecoverPasswordControl_SendMailError(object sender, SendMailErrorEventArgs e)
        {
            FailureText.Text = "Error sending email.  Please try again later.  If the problem persists, please contact support.";
        }

        protected void RecoverPasswordControl_VerifyingUser(object sender, LoginCancelEventArgs e)
        {
            FailLabel.Visible = false;
            Page.Validate();
            if (!Page.IsValid)
            {
                if (useRecaptcha == null || useRecaptcha.ToLower().Equals("true"))
                {
                    FailLabel.Text = "Failed to enter the correct CAPTCHA words, please try again.";
                    FailLabel.Visible = true;
                    e.Cancel = true;
                    return;
                }
            }
            MembershipUser mu = Membership.GetUser(RecoverPasswordControl.UserName, false);
            if (mu == null)
            {
                FailLabel.Text = "User does not exist.  Please enter a valid username.";
                FailLabel.Visible = true;
                e.Cancel = true;
                return;
            }
            if (mu.IsLockedOut)
            {
                FailLabel.Text = "User has been locked out.  Please contact support.";
                FailLabel.Visible = true;
                e.Cancel = true;
                return;
            }
            QueryConnection conn = null;
            try
            {
                conn = ConnectionPool.getConnection();
                user = Database.getUser(conn, mainSchema, RecoverPasswordControl.UserName, false);
                if (user == null)
                {
                    FailLabel.Text = "User has expired.  Please contact support.";
                    FailLabel.Visible = true;
                    e.Cancel = true;
                    return;
                }
                AccountDetails details = Database.getAccountDetails(conn, mainSchema, user.ManagedAccountId, user);
                if (details != null && details.isRADIUS())
                {
                    FailLabel.Text = "User is not allowed reset their password.  Please contact support.";
                    FailLabel.Visible = true;
                    e.Cancel = true;
                    return;
                }
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
        }

        protected void PasswordValidator_Validate(object source, ServerValidateEventArgs args)
        {
            string username = (string)Session["username"];
            if (username == null)
                return;

            PasswordPolicy policy = (PasswordPolicy)Session["PasswordPolicy"];
            args.IsValid = false;

            if (policy != null)
            {
                List<string> history = (List<string>)Session["PasswordHistory"];
                args.IsValid = policy.isValid(args.Value, username, history);
            }
            else
                args.IsValid = true; // no password policy
            passwordValid = args.IsValid;
        }

        protected void ChangePWDButton_Click(object sender, EventArgs e)
        {
            string username = (string)Session["username"];
            if (username == null || !passwordValid)
                return;
            MembershipUser mu = Membership.GetUser(username, false);
            string tempPassword = mu.ResetPassword();
            mu.ChangePassword(tempPassword, NewPassword.Text);
            MultiView.ActiveViewIndex = 2;
            LoginLink.NavigateUrl = Util.getLoginPageURL(Session);
            Session.Remove("username");
            Session.Remove("PasswordPolicy");
            Session.Remove("PasswordHistory");
            Session.Abandon();
        }

        protected void ChangePassword_ChangedPassword(object sender, EventArgs e)
        {
        }

        protected bool validateToken(string recordId)
        {
            Guid tokenID = Guid.Empty;

            try
            {
                tokenID = new Guid(recordId);
            }
            catch (Exception)
            {
                return false;
            }

            QueryConnection conn = null;
            try
            {
                conn = ConnectionPool.getConnection();
                PasswordResetToken token = Database.getPasswordResetToken(conn, mainSchema, tokenID);
                if (token == null)
                {
                    Global.systemLog.Warn("Tried to use an invalid password reset token: " + recordId);
                    return false;
                }
                if (DateTime.Now > token.endOfLife)
                {
                    Global.systemLog.Warn("Tried to use an expired password reset token: " + token.endOfLife);
                    return false;
                }
                user = Database.getUserById(conn, mainSchema, token.userID);
                if (user == null)
                {
                    Global.systemLog.Warn("Tried to reset the password of an inactive/missing user: " + token.userID);
                    return false;
                }

                MembershipUser mu = Membership.GetUser(user.Username, false);
                if (mu == null)
                {
                    return false;
                }

                FormsAuthentication.SetAuthCookie(user.Username, false);

                PasswordPolicy policy = Database.getPasswordPolicy(conn, mainSchema, user.ManagedAccountId);
                if (policy != null)
                {
                    PasswordValidator.ErrorMessage = HttpUtility.HtmlEncode(policy.description + "; Common passwords, repeated characters, and sequences (lexigraphic, numeric, and keyboard) are not allowed");
                    PasswordValidator.ToolTip = HttpUtility.HtmlEncode(policy.description + ";\nCommon passwords, repeated characters, and sequences (lexigraphic, numeric, and keyboard) are not allowed");
                    Session["PasswordPolicy"] = policy;
                    List<string> history = Database.getPasswordHistory(conn, mainSchema, user.ID);
                    Session["PasswordHistory"] = history;
                }
                Session["username"] = user.Username;
                return true;
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
        }
    }
}
