﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.Odbc;
using System.Collections.Generic;
using Acorn.DBConnection;

namespace Acorn
{
    public class Product
    {
        public static int TYPE_EDITION = 0;
        public static int TYPE_STORAGE = 1;
        public static int TYPE_INVITATION = 2;
        public static int TYPE_PROMOTION = 3;
        public static int TYPE_WEBDAV = 4;
        public static int TYPE_OTHER = 5;
        public static int TYPE_REALTIME = 6;
        public static int TYPE_WEBSERVICE = 7;
        public static int TYPE_MOBILE = 8;
        public static int TYPE_SAP_CONNECTOR = 9;
        public static int TYPE_LOCAL_ETL_LOADER = 10;
        public static int TYPE_BING_MAPS = 11;

        public int ProductID;
        public string Name;
        public string Description;
        public bool Default;
        public int Type;
        public int AccountType;
        public string Category;
        public string PricingDetails;
        public decimal PricePerMonth;
        public bool HasPromotion;
        public string PromotionCode;
        public decimal PromotionPrice;
        public DateTime PromotionEndDate;
        public int PromotionDurationDays;
        public int RowLimit;
        public long DataLimit;
        public int ShareLimit;
        public long MaxAllowableData;
        public int MaxAllowableRows;
        public long MaxPublishData;
        public int MaxPublishRows;
        public long MaxPublishDataPerDay;
        public int MaxPublishRowsPerDay;
        public int MaxPublishTime;
        public bool ShowAsOption;
        public bool DefaultActive;
        public bool SingleUse;
        public long MaxScriptStatements;
        public int MaxInputRows;
        public int MaxOutputRows;
        public int ScriptQueryTimeout;
        public int MaxDeliveredReportsPerDay;
        // Only set when getting a list of products for a user
        public int Quantity;
        public bool Active;
        public DateTime EndDate;

        public Product clone()
        {
            Product p = new Product();
            p.ProductID = ProductID;
            p.Name = Name;
            p.Description = Description;
            p.Default = Default;
            p.Type = Type;
            p.AccountType = AccountType;
            p.Category = Category;
            p.PricingDetails = PricingDetails;
            p.PricePerMonth = PricePerMonth;
            p.HasPromotion = HasPromotion;
            p.PromotionCode = PromotionCode;
            p.PromotionPrice = PromotionPrice;
            p.PromotionEndDate = PromotionEndDate;
            p.PromotionDurationDays = PromotionDurationDays;
            p.RowLimit = RowLimit;
            p.DataLimit = DataLimit;
            p.ShareLimit = ShareLimit;
            p.ShowAsOption = ShowAsOption;
            p.MaxAllowableData = MaxAllowableData;
            p.MaxAllowableRows = MaxAllowableRows;
            p.MaxPublishData = MaxPublishData;
            p.MaxPublishRows = MaxPublishRows;
            p.MaxPublishDataPerDay = MaxPublishDataPerDay;
            p.MaxPublishRowsPerDay = MaxPublishRowsPerDay;
            p.MaxPublishTime = MaxPublishTime;
            p.DefaultActive = DefaultActive;
            p.SingleUse = SingleUse;
            p.MaxScriptStatements = MaxScriptStatements;
            p.MaxInputRows = MaxInputRows;
            p.MaxOutputRows = MaxOutputRows;
            p.ScriptQueryTimeout = ScriptQueryTimeout;
            p.MaxDeliveredReportsPerDay = MaxDeliveredReportsPerDay;
            p.Quantity = Quantity;
            p.Default = Default;
            p.EndDate = EndDate;
            return (p);
        }

        public static List<Product> getAllProducts(QueryConnection conn, string schema, System.Web.SessionState.HttpSessionState session)
        {
            List<Product> allProducts = (List<Product>)session["allproducts"];
            if (allProducts == null)
            {
                allProducts = Database.getProducts(conn, schema);
                session["allproducts"] = allProducts;
            }
            return (allProducts);
        }
    }
}
