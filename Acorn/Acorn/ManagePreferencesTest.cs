﻿namespace Acorn
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data.Odbc;
    using System.Web;
    using System.Windows.Forms;
    using NUnit.Framework;
    using Acorn.DBConnection;

    [TestFixture]
    public class ManagePreferencesTest
    {
        public const string TEST_CONNECT_STRING =
            "Driver={SQL Native Client};Server=localhost;Database=BirstAdmin;Uid=smi;Pwd=smi";

        public const string TEST_USER_ID = "2A439193-CCD2-4A36-8FED-27D9B9ECB8FA";
        public const string TEST_SPACE_ID = "95629ED0-8DE7-40C6-9B31-E0E94EE4A4B4";

        public static string[] TEST_KEYS = new string[]{ "TestKey1", "TestKey2", "TestKey3" };
        public static string[,] TEST_VALUES = new string[,]{
            { "TestValue1-Global", "TestValue1-User", "TestValue1-UserSpace" },
            { "TestValue2-Global", "TestValue2-User", "TestValue2-UserSpace" },
            { "TestValue3-Global", "TestValue3-User", "TestValue3-UserSpace" },
        };

        private QueryConnection conn;
        private string mainSchema;
        private Guid userID;
        private Guid spaceID;

        [SetUp]
        public void setUp()
        {
            mainSchema = "dbo";
            this.conn = new QueryConnection();
            this.conn.ConnectionString = TEST_CONNECT_STRING;
            System.Console.WriteLine("Using connection string: " + this.conn.ConnectionString);
            try
            {
                this.conn.Open();
                System.Console.WriteLine("Dropping and creating the test preferences table.");
                List<string> schemaTable = null;
                // 'true' ==> drop table first
                Database.createUserPreferenceTable(this.conn, mainSchema, ref schemaTable, true);
            }
            catch (Exception ex)
            {
                System.Console.WriteLine("Failed to connect to data source" + ex);
            }

            // Set up user and space IDs
            userID = new Guid(TEST_USER_ID);
            System.Console.WriteLine("'userID' Guid is: " + userID);
            spaceID = new Guid(TEST_SPACE_ID);
            System.Console.WriteLine("'spaceID' Guid is: " + spaceID);
        }

        [TearDown]
        public void tearDown()
        {
            this.conn.Close();
        }

        [Test]
        public void TestAddPreferences()
        {
            AddTestDataToTable();
        }

        [Test]
        public void TestUserSpacePreference()
        {
            System.Console.WriteLine("********************* TestUserSpacePreference *********************");
            // Set up test data
            AddTestDataToTable();

            // Retrieve results
            IDictionary<string, string> keyValuePairs
                = ManagePreferences.getPreference(TEST_CONNECT_STRING, userID, spaceID, TEST_KEYS[2]);

            // Check results
            Assert.AreEqual(1, keyValuePairs.Count);
            Assert.IsTrue(keyValuePairs.ContainsKey(TEST_KEYS[2]));
            Assert.AreEqual(TEST_VALUES[2, 2], keyValuePairs[TEST_KEYS[2]]);
        }

        [Test]
        public void TestUserSpacePreferences()
        {
            System.Console.WriteLine("********************* TestUserSpacePreferences *********************");
            // Set up test data
            AddTestDataToTable();

            // Retrieve results
            List<string> preferenceKeys = new List<string>();
            preferenceKeys.Add(TEST_KEYS[2]);
            IDictionary<string, string> keyValuePairs
                = ManagePreferences.getPreferences(TEST_CONNECT_STRING, userID, spaceID, preferenceKeys);

            // Check results
            Assert.AreEqual(1, keyValuePairs.Count);
            Assert.IsTrue(keyValuePairs.ContainsKey(TEST_KEYS[2]));
            Assert.AreEqual(TEST_VALUES[2, 2], keyValuePairs[TEST_KEYS[2]]);
        }

        [Test]
        public void TestUserPreference()
        {
            System.Console.WriteLine("********************* TestUserPreference *********************");

            // Set up test data
            AddTestDataToTable();

            // Retrieve results
            IDictionary<string, string> keyValuePairs
                = ManagePreferences.getPreference(TEST_CONNECT_STRING, userID, TEST_KEYS[1]);

            // Check results
            Assert.AreEqual(1, keyValuePairs.Count);
            Assert.IsTrue(keyValuePairs.ContainsKey(TEST_KEYS[1]));
            Assert.AreEqual(TEST_VALUES[1, 1], keyValuePairs[TEST_KEYS[1]]);
        }

        [Test]
        public void TestUserPreferences()
        {
            System.Console.WriteLine("********************* TestUserPreferences *********************");

            // Set up test data
            AddTestDataToTable();

            // Retrieve results
            List<string> preferenceKeys = new List<string>();
            preferenceKeys.Add(TEST_KEYS[1]);
            IDictionary<string, string> keyValuePairs
                = ManagePreferences.getPreferences(TEST_CONNECT_STRING, userID, preferenceKeys);

            // Check results
            Assert.AreEqual(1, keyValuePairs.Count);
            Assert.IsTrue(keyValuePairs.ContainsKey(TEST_KEYS[1]));
            Assert.AreEqual(TEST_VALUES[1, 1], keyValuePairs[TEST_KEYS[1]]);
        }

        [Test]
        public void TestGlobalPreferences()
        {
            System.Console.WriteLine("********************* TestGlobalPreferences *********************");

            // Set up test data
            AddTestDataToTable();

            // Retrieve results
            List<string> preferenceKeys = new List<string>();
            preferenceKeys.Add(TEST_KEYS[0]);
            IDictionary<string, string> keyValuePairs 
                = ManagePreferences.getGlobalPreferences(TEST_CONNECT_STRING, preferenceKeys);

            // Check results
            Assert.AreEqual(1, keyValuePairs.Count);
            Assert.IsTrue(keyValuePairs.ContainsKey(TEST_KEYS[0]));
            Assert.AreEqual(TEST_VALUES[0, 0], keyValuePairs[TEST_KEYS[0]]);
        }

        private void AddTestDataToTable()
        {
            List<UserPreference> userPreferences = new List<UserPreference>();
            for (int i = 0; i < TEST_KEYS.Length; i++)
            {
                for (int j = 0; j <= i; j++)
                {
                    UserPreference userPreference = new UserPreference();
                    // Set up user-space preference
                    switch (j)
                    {
                        case 0:
                            userPreference.PreferenceType = UserPreference.Type.Global;
                            break;
                        case 1:
                            userPreference.PreferenceType = UserPreference.Type.User;
                            break;
                        case 2:
                            userPreference.PreferenceType = UserPreference.Type.UserSpace;
                            break;
                    }
                    userPreference.Key = TEST_KEYS[i];
                    userPreference.Value = TEST_VALUES[i, j];
                    System.Console.WriteLine(userPreference.toString());
                    userPreferences.Add(userPreference);
                }
            }

            // Add preferences to table
            ManagePreferences.updateUserPreferences(TEST_CONNECT_STRING, userID, spaceID, userPreferences);
        }
    }
}
