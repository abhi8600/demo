﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Acorn.TrustedService;
using System.IO;
using Performance_Optimizer_Administration;
using System.Data;

namespace Acorn
{
    public class RServerUtil
    {
        public static RServerResult TestRServerConnection(Space sp)
        {
            SpaceConfig sc = Util.getSpaceConfiguration(sp.Type);
            string localprotocol = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["LocalProtocol"];
            // 5 Minute timeout
            TrustedService.TrustedService ts = new TrustedService.TrustedService();
            ts.Timeout = 5 * 60 * 1000;
            ts.Url = localprotocol + sc.LocalURL + "/SMIWeb/services/TrustedService";
            RServerResult rsr = null; ;
            if (!File.Exists(sp.Directory + "\\repository_dev.xml"))
            {
                return rsr;
            }
            return ts.testRServerConnection(sp.Directory);
        }

        public static RServerResult UploadRServerFile(Space sp, User u, MainAdminForm maf, string path, string rexpression)
        {
            SpaceConfig sc = Util.getSpaceConfiguration(sp.Type);
            string localprotocol = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["LocalProtocol"];
            // 5 Minute timeout
            TrustedService.TrustedService ts = new TrustedService.TrustedService();
            ts.Timeout = 5 * 60 * 1000;
            ts.Url = localprotocol + sc.LocalURL + "/SMIWeb/services/TrustedService";
            RServerResult rsr = null; ;
            if (!File.Exists(sp.Directory + "\\repository_dev.xml"))
            {
                return rsr;
            }

            try
            {
                rsr = ts.getRServerFile(sp.Directory, path, rexpression, u.Username, sp.ID.ToString());
                if (!rsr.success)
                    return rsr;
                int index1 = path.LastIndexOf('/');
                int index2 = path.LastIndexOf('\\');
                string fname = path;
                if (index1 >= 0 && index1 > index2)
                    fname = path.Substring(index1 + 1);
                else if (index2 >= 0 && index2 > index1)
                    fname = path.Substring(index2 + 1);
                if (fname.Trim().Length == 0)
                    return rsr;
                fname += "rserver";
                if (File.Exists(sp.Directory + "\\data\\" + fname))
                {
                    if (maf == null)
                    {
                        rsr.errorCode = 1;
                        rsr.errorMessage = "Invalid session";
                        return rsr;
                    }
                    ApplicationUploader uploader =
                         new ApplicationUploader(maf, sp, fname, true, false, false, null, false, null, u,
                             null, 0, 0, false, null, null, false);
                    uploader.RServerPath = path;
                    DataTable errorTable = uploader.uploadFile(false, true);
                    if (errorTable.Rows.Count > 0)
                        rsr.errorMessage = "";
                    foreach (DataRow dr in errorTable.Rows)
                    {
                        string errorType = null;
                        if (dr["Severity"] != null)
                        {
                            errorType = (string)dr["Severity"];
                        }
                        rsr.errorMessage += (rsr.errorMessage.Length > 0 ? "; " : "") + dr["Error"];
                        if (errorType != null && errorType == ApplicationUploader.ERROR_STR)
                        {
                            Global.systemLog.Error("Space : " + sp.Name + " Source : " + dr["Source"] + " Severity : " + dr["Severity"] + " Message : " + dr["Error"]);
                        }
                        else
                        {
                            Global.systemLog.Warn("Space : " + sp.Name + " Source : " + dr["Source"] + " Severity : " + dr["Severity"] + " Message : " + dr["Error"]);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Global.systemLog.Warn("Could not validate repository metadata ", e);
            }
            return rsr;
        }
    }
}