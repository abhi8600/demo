<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TrialUserSignup.aspx.cs"
    Inherits="Acorn.TrialUserSignup" %>

<%@ Register TagPrefix="recaptcha" Namespace="Recaptcha" Assembly="Recaptcha" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Birst</title>
    <link type="text/css" rel="stylesheet" href="global.css" />
    <link rel="shortcut icon" href="Images/favicon.ico" type="image/x-icon" />
    <link rel="icon" href="Images/favicon.ico" type="image/x-icon" />
    <script type="text/javascript" language="javascript">
<!--
        if (top.frames.length != 0)
            top.location = self.document.location;
// -->
    </script>
    <asp:Literal ID="HeaderColorStyle" runat="server"><style type="text/css">
                                                          #headerstyle
                                                          {
                                                              background-color: #E7E1D8;
                                                          }
                                                      </style></asp:Literal>
    <style type="text/css">
        #logostyle
        {
        	width: 20%;   
            vertical-align: middle;
            background-repeat: no-repeat;
            padding: 10px 10px;         	
            background-image: url(<asp:Literal ID="LogoLiteral" runat="server" >/Logo.aspx</asp:Literal>);
        }
       
    </style>
</head>
<body>
        <form name="createuserform" id="createuserform" method="post" action="TrialUserSignup.aspx" onsubmit="_gaq.push(['_linkByPost', this]); return WebForm_OnSubmit();" runat="server">
        <asp:ScriptManager ID="AjaxScriptManager" ScriptMode="Release" runat="server" />
        <script type="text/javascript">
            function showEPopup() {
                var gid = $get('<%= this.ErrorPanel.ClientID %>');
                gid.className = '';
                var modalPopup = $find('ErrorPopup');
                modalPopup.show();
            }
            function hideEPopup() {
                var gid = $get('<%= this.ErrorPanel.ClientID %>');
                gid.className = 'invisible';
                var modalPopup = $find('ErrorPopup');
                modalPopup.hide();
            }
        </script>
        <div id="headerstyle" runat="server" style="height:44px;">
          <table id="headerTable">
            <tr>
                <td id="logostyle" style="height:44px; width:130px">
                </td>
            </tr>
          </table>
        </div>
        <asp:Label Width="80%" ID="FailLabel" runat="server" Visible="false" Style="color: Red;
            padding: 5px" /><br />

        <table cellpadding="0" style="width: 75%; padding-left: 10px; padding-right: 10px">
            <tr valign="top" style="height: 100%">
                <td>
                    <asp:CreateUserWizard ID="CreateUserWizard" runat="server" Width="100%" DisableCreatedUser="True"
                        LoginCreatedUser="False" MembershipProvider="UserProvider" OnCreatedUser="CreateUserWizard_CreatedUser"
                        OnSendingMail="CreateUserWizard_SendingMail" OnSendMailError="CreateUserWizard_SendMailError"
                        OnCreateUserError="CreateUserWizard_CreateUserError" OnCreatingUser="CreateUserWizard_CreatingUser"
                        CreateUserButtonType="Image" CreateUserButtonImageUrl="Images/Sign_Up_Now.png"
                        ErrorMessageStyle-ForeColor="Red" MailDefinition-Subject="Please confirm your Birst subscription">
                        <StepStyle BorderWidth="0px" />
                        <WizardSteps>
                            <asp:CreateUserWizardStep ID="CreateUserWizardStep" runat="server" Title="Sign Up">
                                <ContentTemplate>
                                    <asp:Panel ID="ErrorPanel" runat="server" Visible="false" ForeColor="Red" Height="30px"
                                        HorizontalAlign="Center">
                                        <asp:Literal runat="server" EnableViewState="false" ID="ErrorMessage"></asp:Literal>
                                    </asp:Panel>
                                    <div class="pageheader">
                                        Create your Birst Express edition account
                                    </div>
                                    <table border="0" style="font-weight: normal" class="wizard">
                                        <tr height="20px" valign="top">
                                            <td colspan="4">
                                                <span style="font-size: 9pt"><font style="color: blue">*</font>&nbsp;Required information</span>
                                                <asp:ValidationSummary ValidationGroup="CreateUserWizard" runat="server" ShowMessageBox="False"
                                                    DisplayMode="SingleParagraph" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                            </td>
                                            <td>
                                                <asp:Label ID="EmailLabel" runat="server" AssociatedControlID="Email">Username (e-mail address):</asp:Label>
                                            </td>
                                            <td>
                                                <font color="blue">*</font>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="Email" runat="server" MaxLength="64" Width="300px"></asp:TextBox>
                                                <asp:RegularExpressionValidator ID="EmailValid" runat="server" ControlToValidate="Email"
                                                    ValidationGroup="CreateUserWizard" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                                    ErrorMessage="A valid E-mail address is required" ToolTip="A valid E-mail address is required"
                                                    Display="Dynamic">*</asp:RegularExpressionValidator><asp:RequiredFieldValidator ID="EmailRequired"
                                                        runat="server" ControlToValidate="Email" ValidationGroup="CreateUserWizard" ErrorMessage="A valid E-mail address is required."
                                                        ToolTip="A valid E-mail address is required." Display="Dynamic">*</asp:RequiredFieldValidator>
                                                <asp:TextBox ID="UserName" runat="server" Visible="false"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                            </td>
                                            <td>
                                                <asp:Label ID="PasswordLabel" runat="server" AssociatedControlID="Password">Password:</asp:Label>
                                            </td>
                                            <td>
                                                <font color="blue">*</font>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="Password" runat="server" TextMode="Password" MaxLength="128"  Width="300px" autocomplete="off"></asp:TextBox>
                                                <asp:CustomValidator ID="PasswordValidator" ControlToValidate="Password" runat="server"
                                                            OnServerValidate="PasswordValidator_Validate" EnableClientScript="False" ValidationGroup="ChangePassword"></asp:CustomValidator>
                                                <asp:RequiredFieldValidator ID="NewPasswordRequired" runat="server" ControlToValidate="Password"
                                                            ErrorMessage="New Password is required." ToolTip="New Password is required."
                                                            ValidationGroup="ChangePassword">*</asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                            </td>
                                            <td>
                                                <asp:Label ID="ConfirmPasswordLabel" runat="server" AssociatedControlID="ConfirmPassword">Confirm Password:</asp:Label>
                                            </td>
                                            <td>
                                                <font color="blue">*</font>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="ConfirmPassword" runat="server" TextMode="Password" MaxLength="128" Width="300px"
                                                    autocomplete="off"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="ConfirmPasswordRequired" runat="server" ControlToValidate="ConfirmPassword"
                                                    ValidationGroup="CreateUserWizard" ErrorMessage="Confirm Password is required."
                                                    ToolTip="Confirm Password is required.">*</asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" colspan="4">
                                                <asp:CompareValidator ID="PasswordCompare" runat="server" ControlToCompare="Password"
                                                    ValidationGroup="CreateUserWizard" ControlToValidate="ConfirmPassword" Display="Dynamic"
                                                    ErrorMessage="The Password and Confirmation Password must match."></asp:CompareValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4">
                                                <recaptcha:RecaptchaControl ID="recaptcha" runat="server" Theme="red" PublicKey="6LdYg8MSAAAAAF9YXsjza98mKYakOk80mDu_P7Pa"
                                                    PrivateKey="6LdYg8MSAAAAAP9MSCt7IUCYNo0YnuegnpIXGXnf" />
                                            </td>
                                        </tr>
                                        <tr>
                                        <td colspan="4">
                                        <br />
                                        </td>
                                        </tr>
                                        <tr>
                                        <td colspan="4">
            <div style="width: 100%; padding-left: 10px; padding-right: 10px">
            <asp:Label ID="Label1" runat="server" Text="The Express edition of Birst allows you to:"/>
            <asp:BulletedList ID="BulletedList1" runat="server">
                <asp:ListItem Text="Create new Birst spaces and upload up to 1 GB of data" />
                <asp:ListItem Text="Extract and load data from Salesforce.com" />
                <asp:ListItem Text="Visualize your data, create reports and interactive dashboards" />
                <asp:ListItem Text="Invite up to 5 other people to share insights in Birst" />
            </asp:BulletedList>
            <asp:Label ID="Label2" runat="server" Text="If you are interested in learning about Birst�s premium editions, please contact Birst at " />
            <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="http://www.birst.com/contact.shtml" Text="sales@birst.com" Target="_blank" />
            </div>
            <br />
            </td>
                                        </tr>
                                        <tr height="30px" valign="middle">
                                            <td colspan="4">
                                                <script language="javascript">
                                                    function ValidateChecked(oSrc, args) {
                                                        args.IsValid = document.getElementById('<%=CreateUserWizardStep.ContentTemplateContainer.FindControl("ServiceBox").ClientID%>').checked;
                                                        if (!args.IsValid) showEPopup()
                                                    }
                                                </script>
                                                <font style="font-weight: bold">
                                                    <asp:CheckBox ID="ServiceBox" runat="server" />&nbsp;I accept the <a target="_blank" href="http://www.birst.com/terms-of-service">
                                                        Birst Terms of Service</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a  target="_blank" href="http://www.birst.com/privacy-policy">Privacy Policy</a>
                                                </font>
                                                <asp:CustomValidator ID="ServiceValidator" runat="server" ValidationGroup="CreateUserWizard"
                                                    ClientValidationFunction="ValidateChecked"></asp:CustomValidator></div>
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                                <CustomNavigationTemplate>
                                    <table border="0" cellspacing="5" style="width: 100%; height: 100%;">
                                        <tr align="center">
                                            <td align="center" colspan="0">
                                                <div style="height: 10px">
                                                    &nbsp;</div>
                                                <asp:ImageButton ID="StepNextButton" OnClientClick="_gaq.push(['_trackPageview', '/funnel/trial/continue']);"
                                                    runat="server" ToolTip="Create User" CommandName="MoveNext" ImageUrl="Images/Sign_Up_Now.png"
                                                    ValidationGroup="CreateUserWizard" />
                                            </td>
                                        </tr>
                                    </table>
                                </CustomNavigationTemplate>
                            </asp:CreateUserWizardStep>
                            <asp:CompleteWizardStep ID="CompleteWizardStep" runat="server" Title="Complete Registration">
                                <ContentTemplate>
                                    <div class="pageheader">
                                        Signup Information Submitted
                                    </div>
                                    <asp:PlaceHolder ID="NormalConfirmation" runat="server" Visible="true">
                                        <table border="0" class="wizard">
                                            <tr height="50px" valign="middle">
                                                <td style="font-weight: normal" align="center">
                                                    An email has been sent to you with instructions to complete signup. The email is
                                                    from
                                                    <asp:Label ID="FromEmailLabel" runat="server"></asp:Label>. Make sure this email
                                                    address is in your safe sender�s list.
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:PlaceHolder>
                                    <div style="height: 15px">
                                        &nbsp;</div>
                                    <div style="width: 100%; text-align: center">
                                        <asp:ImageButton ID="ContinueButton" runat="server" ImageUrl="~/Images/Continue.png"
                                            PostBackUrl="~/Login.aspx" ToolTip="Continue" CommandName="Continue" /></div>
                                </ContentTemplate>
                            </asp:CompleteWizardStep>
                        </WizardSteps>
                        <MailDefinition Subject="Confirm Your Signup Request" BodyFileName="~/Thank You for Registering Birst Express.htm"
                            IsBodyHtml="True">
                        </MailDefinition>
                    </asp:CreateUserWizard>
                </td>
            </tr>
        </table>
        <asp:Panel ID="ErrorPanel" runat="server" Width="400px" CssClass="invisible">
            <table width="100%" style="background-color: White" cellpadding="0" cellspacing="0">
                <tr style="height: 30px; background-image: url(Images/WarningGradient.gif)" class="warningheader">
                    <td align="center">
                        Please Complete Your Sign Up Form
                    </td>
                </tr>
                <tr style="height: 35px; vertical-align: middle">
                    <td align="center">
                        You must acknowledge the Terms of Service to Sign Up
                    </td>
                </tr>
                <tr style="height: 35px; vertical-align: middle">
                    <td align="center">
                        <asp:ImageButton runat="server" ImageUrl="~/Images/Close.png" />
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <asp:LinkButton ID="ErrorButton" runat="server" CssClass="invisible"></asp:LinkButton>
        <cc1:ModalPopupExtender ID="ErrorPanelExtender" runat="server" BackgroundCssClass="modalbackground"
            DropShadow="False" Enabled="True" PopupControlID="ErrorPanel" TargetControlID="ErrorButton"
            BehaviorID="ErrorPopup">
        </cc1:ModalPopupExtender>
        </form>
    </div>
    <asp:PlaceHolder ID="TrackingPanel" runat="server" Visible="false">
    </asp:PlaceHolder>
</body>
</html>
