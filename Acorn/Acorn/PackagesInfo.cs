﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Acorn
{
    public class PackagesInfo : GenericResponse
    {
        public PackageInfoLite[] PackagesLiteList;
    }

    public class PackageInfoLite
    {
        public string ID;
        public string Name;
        public string SpaceID;
        public string SpaceName;
        public string Description;
        public bool ImportedPackage;
        public int Status;
        public string CreatedBy;
        public string ModifiedBy;
        public DateTime? CreatedDate;
        public DateTime? ModifiedDate;
    }

    public class PackageSpaceProperties
    {
        public Guid PackageID;
        public string PackageName;
        public Guid SpaceID;
        public string SpaceName;
        public string SpaceDirectory;
    }
}