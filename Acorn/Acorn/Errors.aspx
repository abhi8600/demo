﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Errors.aspx.cs" Inherits="Acorn.Errors" MasterPageFile="~/WebAdmin.Master" %>

<asp:Content ID="ContentID" ContentPlaceHolderID="mainPlaceholder" runat="server">
        <div style="height: 30px">
            &nbsp;
        </div>
        <div style="text-align: center">
            We're sorry, an error has occurred. Please try again. If this persists, please contact Support.
        </div>
</asp:Content>
