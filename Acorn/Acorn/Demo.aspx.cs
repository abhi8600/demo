﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.Odbc;
using Acorn.DBConnection;
using System.Collections.Generic;
using System.Collections.Specialized;
using Acorn.Utils;

namespace Acorn
{
    public partial class Demo : System.Web.UI.Page
    {
        private static string mainSchema = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["MainSchema"];
        public static string DEMO_USER_NAME = "BirstDemoUser@birst.com";
        public static string BIRST_HOME = "http://www.birst.com";

        protected void Page_Load(object sender, EventArgs e)
        {
            // clear out the state
            // SSO.logout(Session, Request, Response);
            Util.endSession(Session);
            Util.endSMIWebSession(Session, Request);
            SchedulerUtils.endSchedulerSession(Session, Request);
            Util.setLogging(null, null);
            Session.Clear();

            string space_id = Util.getBirstParam(Request, "birst.spaceId", "space");
            if (space_id == null)
                Response.Redirect(BIRST_HOME);

            QueryConnection conn = null;
            Space sp = null;
            try
            {
                conn = ConnectionPool.getConnection();
                sp = Database.getSpace(conn, mainSchema, new Guid(space_id));
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
            if (sp == null || !sp.Demo)
                Response.Redirect(BIRST_HOME);

            Session["space"] = sp;
            // build mapping table entries from pre 4.3 repository if needed
            Util.checkAndCreateUserGroupDbMappings(sp);

            Session.Remove("showDesigner");
            string showDesigner = Util.getBirstParam(Request, "birst.showDesigner", "showDesigner");
            if (showDesigner != null && showDesigner.ToLower() == "true")
            {
                Session["showDesigner"] = "true";
            }
            
            // Adding FormsAuth cookie .ASPXFORMSAUTH
            FormsAuthentication.SetAuthCookie(DEMO_USER_NAME, false);
            // Redirect to dashboard module since sp has the flag demo turned to true which is checked at both
            // Adhoc and Dashboard pages

            NameValueCollection arguments = new NameValueCollection();
            arguments.Add(Request.Form);
            arguments.Add(Request.QueryString);
            Util.fixupArguments(arguments);
            Session["requiresLogout"] = true;
            Acorn.DefaultUserPage.redirectToDashboard(Session, Response, Server, arguments, Request, sp);
        }
    }
}
