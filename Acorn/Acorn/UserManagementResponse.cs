﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Acorn
{
    public class UserManagementResponse 
    {       
    }

    public class UsersDetails : GenericResponse
    {
        public UserSummary[] Users;
    }

    public class UserSummary
    {
        public string UserID;
        public string Username;
        public string Email;
        public string FirstName;
        public string LastName;
        public bool Enabled;
        public bool Locked;
        public bool AccountAdmin;
        public string LastLoginDate;
        public bool OperationFlag;
        public int ReleaseType;
        public bool ProxyIsOperation;
        public bool RepositoryAdmin;
        public bool FreeTrial;
        public bool HtmlUser;
        public bool ExternalDBEnabled;
        public bool DiscoveryFreeTrial;
        public bool DenyAddSpace;
    }

    public class SpacesToManage : GenericResponse
    {
        public SpaceSummary[] Spaces;
    }

    public class AccountSummary : GenericResponse
    {
        public string AccountID;
        public string Name;
        public string ExpirationDate;
        // This includes all the users which are enabled
        public int ActiveUsers;
        // This includes all the users which are enabled and disabled
        public int TotalAssignedUsers;
        // This includes the maximum number of users that can be assigned
        public int MaxAllowedUsers;
        public int MaxConcurrentUsers;       
    }
}
