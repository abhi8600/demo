﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NUnit.Framework;
using Acorn.Exceptions;

namespace Acorn.tests
{
    public class BirstExceptionTest
    {

        [SetUp]
        public void setUp()
        {
        }

        [TearDown]
        public void tearDown()
        {

        }

        [Test]
        public void testBirstException()
        {
            GenericResponse response = null;

            response = new GenericResponse();
            SpaceNotFoundException spEx = new SpaceNotFoundException();            
            response.setException(spEx);
            Console.WriteLine("Expected " + spEx.getErrorType());
            ErrorOutput actualError = response.Error;
            Assert.NotNull(actualError.ErrorType);
            Console.WriteLine("Actual " + actualError.ErrorType);
            Assert.True(actualError.ErrorType == BirstException.ERROR_SPACE_NOT_FOUND);
            Assert.True(actualError.ErrorMessage == BirstException.MSG_SPACE_NOT_FOUND);

            
            
            // 2 flavors, one when nothing is supplied and no errormessage is supplied in the exception
            // It should not give some system default message. 
            // To fix BirstException is called with call to super call as base(""). Otherwise a non-user friendly 
            // message is displayed

            response = new GenericResponse();            
            UserNotLoggedInException uEx1 = new UserNotLoggedInException();
            response.setException(uEx1);
            actualError = response.Error;
            Console.WriteLine("Actual errortype " +  actualError.ErrorType);
            Assert.NotNull(actualError.ErrorType);            
            Assert.True(actualError.ErrorType == BirstException.ERROR_REDIRECT_LOGIN);
            Console.WriteLine("Actual " + actualError.ErrorMessage);
            Assert.True(actualError.ErrorMessage == null );

            response = new GenericResponse();
            string exceptionMsg = "Msg Test";
            UserNotLoggedInException uEx2 = new UserNotLoggedInException(exceptionMsg);
            response.setException(uEx2);
            actualError = response.Error;
            Console.WriteLine("Actual errortype " + actualError.ErrorType);
            Assert.NotNull(actualError.ErrorType);
            Assert.True(actualError.ErrorType == BirstException.ERROR_REDIRECT_LOGIN);
            Console.WriteLine("Actual " + actualError.ErrorMessage);
            Assert.True(actualError.ErrorMessage == exceptionMsg);

            response = new GenericResponse();
            UserManagementNotAuthorizedException umEx = new UserManagementNotAuthorizedException();
            response.setException(umEx);
            actualError = response.Error;
            Assert.NotNull(actualError.ErrorType);
            Assert.True(actualError.ErrorType == BirstException.ERROR_UNAUTHORIZED_USER_MANAGEMENT);            
            Assert.True(actualError.ErrorMessage == BirstException.MSG_USER_MANAGEMENT_NOT_AUTHORIZED);
            
        }
                
        [Test]
        public void testRandomStuff()
        {
            try
            {
                Console.WriteLine("In try");
                throw new Exception();
            }
            catch (Exception)
            {
                Console.WriteLine("In catch before throw");
                throw new BirstException();
                // Console.WriteLine("In catch after throw");
            }
            finally
            {
                Console.WriteLine("In finally after throw");
            }
        }
    }    
}
