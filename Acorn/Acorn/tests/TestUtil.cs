﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using Performance_Optimizer_Administration;
using System.Data.Odbc;
using Acorn.DBConnection;

namespace Acorn.tests
{
    public class TestUtil
    {        
        // FOR NOW CHANGE FOR YOUR LOCAL MACHINES. 
        // Need a test config file which has all these parameters.
        public const string TEST_CONNECT_STRING =
            "Driver={SQL Native Client};Server=localhost;Database=AcornAdmin;Uid=smi;Pwd=smi";
        public const string schema = "dbo";
        public const string SPACES_DIR = @"C:\SMI\Data";
        public const string SPACES_CONFIG_DIR = @"C:\Workspace\Acorn_Head_4_0_0\Acorn";

        public static  Space getTestSpace()
        {
            return getTestSpace(null, null);
        }

        public static Space getTestSpace(string name, string testDirectory)
        {
            string spaceName = name != null ? name : "SpaceTestName";
            testDirectory = testDirectory != null ? testDirectory : Path.GetTempPath();
            Space space = new Space();
            space.Name = spaceName;
            space.ID = Guid.NewGuid();
            space.Directory = testDirectory + Path.DirectorySeparatorChar.ToString() + space.ID.ToString();
            if (!Directory.Exists(space.Directory))
            {
                Directory.CreateDirectory(space.Directory);
            }      

            return space;
        }

        public static MainAdminForm getTestMAF(Space space)
        {
            Space sp = space != null ? space : getTestSpace();
            bool newRepository = false;            
            MainAdminForm maf = Util.loadRepository(sp, out newRepository);
            return maf;
        }

        public static User createNewTestUser(string username, List<string> productIds)
        {
            QueryConnection conn = ConnectionPool.getConnection();
            string schema = Util.getMainSchema();
            if (username == null)
            {
                username = "nobodyTestUser@birst.com";
            }
            string email = username;
            // string password = "test";
            string firstname = "TestUserFirstName";
            string lastname = "TestUserLastName";
            string title = "yoMaan";
            string company = "huh";
            string phone = "111-222-3333";
            string city = "Far Far Away";
            string state = "Remotizona";
            string zip = "12345";
            string country = "GoodLuckWithThat";
            bool stepByStep = false;
            int contactStatus = User.ALLOW_CONTACT;
            string department = "NeverMind";
            string industry = "OOLaLa";
            string orgsize = "2";
            string likelyUsage = "Always";
            if (productIds == null || productIds.Count == 0)
            {
                string products = "3,9,10,11";
                productIds = new List<string>(products.Split(','));                
            }
            
            username = username.ToLower();
            QueryCommand cmd = new QueryCommand("INSERT INTO Users " +
                  " (PKID, Username, Password, Email, PasswordQuestion, " +
                  " PasswordAnswer, IsApproved," +
                  " Comment, CreationDate, LastPasswordChangedDate, LastActivityDate," +
                  " IsLockedOut, LastLockedOutDate," +
                  " FailedPasswordAttemptCount, FailedPasswordAttemptWindowStart, " +
                  " FailedPasswordAnswerAttemptCount, FailedPasswordAnswerAttemptWindowStart)" +
                  " Values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", conn);

            cmd.Parameters.Add("@PKID", OdbcType.UniqueIdentifier).Value = Guid.NewGuid();
            cmd.Parameters.Add("@Username", OdbcType.NVarChar, 255).Value = username;
            cmd.Parameters.Add("@Password", OdbcType.VarChar, 1024).Value = "test";
            cmd.Parameters.Add("@Email", OdbcType.VarChar, 128).Value = email;
            cmd.Parameters.Add("@PasswordQuestion", OdbcType.VarChar, 255).Value = "Test";
            cmd.Parameters.Add("@PasswordAnswer", OdbcType.VarChar, 255).Value = "Test";
            cmd.Parameters.Add("@IsApproved", OdbcType.Bit).Value = true;
            cmd.Parameters.Add("@Comment", OdbcType.VarChar, 255).Value = "";
            cmd.Parameters.Add("@CreationDate", OdbcType.DateTime).Value = DateTime.Now;
            cmd.Parameters.Add("@LastPasswordChangedDate", OdbcType.DateTime).Value = DateTime.Now;
            cmd.Parameters.Add("@LastActivityDate", OdbcType.DateTime).Value = DateTime.Now;
            cmd.Parameters.Add("@IsLockedOut", OdbcType.Bit).Value = false;
            cmd.Parameters.Add("@LastLockedOutDate", OdbcType.DateTime).Value = DateTime.Now;
            cmd.Parameters.Add("@FailedPasswordAttemptCount", OdbcType.Int).Value = 0;
            cmd.Parameters.Add("@FailedPasswordAttemptWindowStart", OdbcType.DateTime).Value = DateTime.Now;
            cmd.Parameters.Add("@FailedPasswordAnswerAttemptCount", OdbcType.Int).Value = 0;
            cmd.Parameters.Add("@FailedPasswordAnswerAttemptWindowStart", OdbcType.DateTime).Value = DateTime.Now;
            cmd.ExecuteNonQuery();
            
            Database.setupNewUser(conn, schema, username, firstname, lastname, email, title,
                company, phone, city, state, zip, country, stepByStep, contactStatus, department,
                industry, orgsize, likelyUsage, productIds, DateTime.MinValue);
            User newUser = Database.getUser(conn, schema, username);
            
            return newUser;
        }

        public static Space createNewTestSpace(User u, string spaceName, bool automatic)
        {

            // Util.loadSpaceParameters(TestUtil.SPACES_CONFIG_DIR);
            QueryConnection conn = ConnectionPool.getConnection();
            string schema = Util.getMainSchema();
            Space existingSpace = Database.getSpace(conn, spaceName, u, true, schema);
            if (existingSpace != null)
            {
                return existingSpace;
            }

            string spaceComments = "All Ok";            
            int queryLanguageVersion = 1;
            Space sp = new Space(u, Space.NEW_SPACE_TYPE, Space.NEW_SPACE_PROCESSVERSION_ID, Space.NEW_SPACE_SFDCVERSION_ID, TestUtil.SPACES_DIR, null);
            if (spaceName == null)
            {
                spaceName = "TestSpace";
            }
            sp.Name = spaceName.Trim();
            sp.Comments = spaceComments.Trim();
            sp.Automatic = automatic;

            SpaceConfig sc = Util.getSpaceConfiguration(sp.Type);
            sp.ConnectString = u.DatabaseConnectString != null ? u.DatabaseConnectString : sc.DatabaseConnectString;
            sp.DatabaseType = u.DatabaseType != null ? u.DatabaseType : sc.DatabaseType;
            sp.DatabaseDriver = u.DatabaseDriver != null ? u.DatabaseDriver : sc.DatabaseDriver;
            sp.setDatabaseLoadDir(u.DatabaseLoadDir != null ? u.DatabaseLoadDir : sc.DatabaseLoadDir);
            sp.AdminUser = u.AdminUser != null ? u.AdminUser : sc.AdminUser;
            sp.AdminPwd = u.AdminPwd != null ? u.AdminPwd : sc.AdminPwd;
            sp.QueryConnectString = u.QueryDatabaseConnectString != null ? u.QueryDatabaseConnectString : sc.QueryDatabaseConnectString;
            sp.QueryDatabaseType = u.QueryDatabaseType != null ? u.QueryDatabaseType : sc.QueryDatabaseType;
            sp.QueryDatabaseDriver = u.QueryDatabaseDriver != null ? u.QueryDatabaseDriver : sc.QueryDatabaseDriver;
            sp.QueryUser = u.QueryUser != null ? u.QueryUser : sc.QueryUser;
            sp.QueryPwd = u.QueryPwd != null ? u.QueryPwd : sc.QueryPwd;
            sp.QueryConnectionName = u.QueryConnectionName != null ? u.QueryConnectionName : sc.QueryConnectionName;
            sp.Active = true;
            sp.QueryLanguageVersion = queryLanguageVersion;
            sp.MaxQueryRows = Database.MAX_QUERY_ROWS;
            sp.MaxQueryTimeout = Database.MAX_QUERY_TIMEOUT;
            sp.UsageTracking = false;

            Database.createSpace(conn, schema, u, sp);
            Space createdSpace = Database.getSpace(conn, schema, sp.ID);
            Util.setSpace(null, sp, u);
            ConnectionPool.releaseConnection(conn);

            return createdSpace;
        }
    }
}
