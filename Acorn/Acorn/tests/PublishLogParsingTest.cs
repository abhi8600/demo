﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NUnit.Framework;
using System.Text.RegularExpressions;
using System.Web.SessionState;
using System.IO;
using Performance_Optimizer_Administration;

namespace Acorn.tests
{
    public class PublishLogParsingTest
    {
        private string thisTestDir = Path.GetTempPath() + "PublishLogParsingTest";
        [SetUp]
        public void setUp()
        {            
            if (!Directory.Exists(thisTestDir))
            {
                Directory.CreateDirectory(thisTestDir);
            }
            Util.TEST_FLAG = true;
        }

        [TearDown]
        public void tearDown()
        {
            Util.TEST_FLAG = false;
            // clean up logs directorty in the temp folder if it exists
            if(Directory.Exists(thisTestDir))
            {
                Directory.Delete(thisTestDir, true);
            }

        }

        [Test]
        public void parseDateStringFromLogLine1()
        {            
            string expectedMatch = "2009-11-25 23:12:12";
            string logStringToParse = expectedMatch + ",671-0800 [Pool Worker - 9] INFO  -  Starting: ResetETLRun 1 ";
            string startMarker = "Starting: ResetETLRun 1";
            int indexStartMarker = logStringToParse.IndexOf(startMarker);

            Match match = Util.getMatchForTimeInSmiEngineLog(logStringToParse, indexStartMarker);
            Console.WriteLine("Match found = " + match.Value);            
            
            string actualMatch = match.Value != null ? (string)match.Value : null;
            Assert.True(actualMatch != null, "No Matches returned");
            Assert.AreEqual(expectedMatch, actualMatch, "Returned Match is invalid");            
        }

        [Test]
        public void parseDateStringFromLogLine2()
        {            
            string expectedMatch1 = "2009-11-25 23:12:12";
            string expectedMatch2 = "2009-11-25 13:12:12";
            string logStringToParse1 = expectedMatch1 + ",671-0800 [Pool Worker - 9] INFO  -  Starting: ResetETLRun 1 ";
            string logStringToParse2 = expectedMatch2 + ",671-0800 [Pool Worker - 9] INFO  -  Starting: ResetETLRun 1 ";
            string startMarker = "Starting: ResetETLRun 1";
            int indexStartMarker1 = logStringToParse1.IndexOf(startMarker);
            int indexStartMarker2 = logStringToParse2.IndexOf(startMarker);

            Match match1 = Util.getMatchForTimeInSmiEngineLog(logStringToParse1, indexStartMarker1);
            
            string actualMatch1 = match1.Value != null ? (string)match1.Value : null;
            Assert.True(actualMatch1 != null, "No Matches returned");
            Assert.AreEqual(expectedMatch1, actualMatch1, "Returned Match is invalid");

            Match match2 = Util.getMatchForTimeInSmiEngineLog(logStringToParse2, indexStartMarker2);            
            string actualMatch2 = match2.Value != null ? (string)match2.Value : null;
            Assert.True(actualMatch2 != null, "No Matches returned");
            Assert.AreEqual(expectedMatch2, actualMatch2, "Returned Match is invalid");

            DateTime dt1;
            DateTime dt2;
            try
            {

                DateTime.TryParse(actualMatch1, out dt1);
                Console.WriteLine("DateTime " + dt1);
                DateTime.TryParse(actualMatch2, out dt2);
                Console.WriteLine("DateTime " + dt2);
                Assert.AreEqual(1, dt1.CompareTo(dt2));
            }
            catch (Exception ex)
            {                
                Assert.Fail("Exception while parsing times from smi engine logs" + ex);                
            }
        }

       
        [Test]
        public void parseSingleLogFileForWarning()
        {
            string dateString = "2010-04-01";
            string timeString = "11:12:14";
            string warnStagingTableSource = "Categories";
            int loadNumber = 1;

            MockHttpSession mockSession = MockHttpSession.getMockSession();
            Util.mockSession = mockSession;

            Space space = TestUtil.getTestSpace("SpaceTest", thisTestDir);
            mockSession["space"] = space;

            String fileName = createSMIEngineTestFileName(space.Directory, dateString, null);
            Console.WriteLine("Filename = " + fileName);

            MainAdminForm maf = TestUtil.getTestMAF(space);
            mockSession["MAF"] = maf;
            
            string expectedOutputWarning = "Line processing problem: 9, column: CategoryID - [10.2.3.123] - does not fit Varchar(8) - truncating";
            StreamWriter streamWriter = new StreamWriter(fileName);
            streamWriter.WriteLine( dateString + " " + timeString + "671-0800 [Pool Worker - 9] INFO  -  Starting: ResetETLRun " + loadNumber + " ");
            streamWriter.WriteLine(dateString + " " + timeString + ",702-0700 [Process - com.successmetricsinc.warehouse.SourceFile@20807c - 18] WARN" +
                "  - [DATAWARNING:Categories.txt] " + expectedOutputWarning);
            streamWriter.Flush();
            streamWriter.Close();           
            
            
            SourceFile sf = new SourceFile();
            sf.FileName = warnStagingTableSource + ".txt";
            sf.Columns = new SourceFile.SourceColumn[0];
            StagingTable st = null;// maf.stagingTableMod.addStagingTable(sf, false);
            if (st == null)
            {
                Assert.Fail("Error while adding test source file");
            }
            maf.sourceFileMod.addSourceFile(sf);
            
            GenericResponse response = PublishLogDetailsService.getFileDetails(null, warnStagingTableSource, dateString, true, loadNumber);
            foreach(string str in response.other)
            {
                Console.WriteLine("Returned response = " + str);
            }

            Assert.AreEqual(expectedOutputWarning, response.other[0], "Expected warning not parsed");
           
        }

        [Test]
        public void parseSingleLogFileWithDuplicateLoadNumbers()
        {
            string dateString = "2010-04-01";
            string timeString = "11:12:14";            
            
            string warnStagingTableSource = "Categories";
            int loadNumber = 1;
            MockHttpSession mockSession = MockHttpSession.getMockSession();
            Util.mockSession = mockSession;

            Space space = TestUtil.getTestSpace("SpaceTest", thisTestDir);
            mockSession["space"] = space;

            String fileName = createSMIEngineTestFileName(space.Directory, dateString, null);
            Console.WriteLine("Filename = " + fileName);

            MainAdminForm maf = TestUtil.getTestMAF(space);
            mockSession["MAF"] = maf;

            SourceFile sf = new SourceFile();
            sf.FileName = warnStagingTableSource + ".txt";
            sf.Columns = new SourceFile.SourceColumn[0];
            StagingTable st = null;// maf.stagingTableMod.addStagingTable(sf, false);
            if (st == null)
            {
                Assert.Fail("Error while adding test source file");
            }
            maf.sourceFileMod.addSourceFile(sf);

            string outputWarningFirstIteration = "Line processing problem: 9, column: CategoryID - [10.2.3.123] - does not fit Varchar(8) - truncating";
            string outputWarningSecondIteration = "Line processing problem: 9, column: CategoryID - [10.2.3.123] - does not fit Varchar(9) - truncating";
            StreamWriter streamWriter = new StreamWriter(fileName);
            timeString = incrementTime(timeString);
            streamWriter.WriteLine(dateString + " " + timeString + ",671-0800 [Pool Worker - 9] INFO  -  Starting: ResetETLRun " + loadNumber + " ");
            timeString = incrementTime(timeString);
            streamWriter.WriteLine(dateString + " " + timeString + ",702-0700 [Process - com.successmetricsinc.warehouse.SourceFile@20807c - 18] WARN" +
                "  - [DATAWARNING:Categories.txt] " + outputWarningFirstIteration);
            timeString = incrementTime(timeString);
            streamWriter.WriteLine(dateString + " " + timeString + ",671-0800 [Pool Worker - 9] INFO  -  Starting: ResetETLRun " + loadNumber + " ");
            timeString = incrementTime(timeString);
            streamWriter.WriteLine(dateString + " " + timeString + ",702-0700 [Process - com.successmetricsinc.warehouse.SourceFile@20807c - 18] WARN" +
                "  - [DATAWARNING:Categories.txt] " + outputWarningSecondIteration);
            streamWriter.Flush();
            streamWriter.Close();
            

            
            GenericResponse response = PublishLogDetailsService.getFileDetails(null, warnStagingTableSource, dateString, true, loadNumber);
            foreach (string str in response.other)
            {
                Console.WriteLine("Returned response = " + str);
            }

            Assert.AreEqual(outputWarningSecondIteration, response.other[0], "Expected warning not parsed");

        }

        [Test]
        public void parseSingleLogFileWithDifferentLoadNumbers()
        {
            string dateString = "2010-04-01";
            string timeString = "11:12:14";            
            string warnStagingTableSource = "Categories";
            int loadNumber1 = 1;
            int loadNumber2 = 2;
            int loadNumber3 = 3;
            MockHttpSession mockSession = MockHttpSession.getMockSession();
            Util.mockSession = mockSession;

            Space space = TestUtil.getTestSpace("SpaceTest", thisTestDir);
            mockSession["space"] = space;

            String fileName = createSMIEngineTestFileName(space.Directory, dateString, null);
            Console.WriteLine("Filename = " + fileName);

            MainAdminForm maf = TestUtil.getTestMAF(space);
            mockSession["MAF"] = maf;

            SourceFile sf = new SourceFile();
            sf.FileName = warnStagingTableSource + ".txt";
            sf.Columns = new SourceFile.SourceColumn[0];
            StagingTable st = null;// maf.stagingTableMod.addStagingTable(sf, false);
            if (st == null)
            {
                Assert.Fail("Error while adding test source file");
            }
            maf.sourceFileMod.addSourceFile(sf);

            string loadNumber1OutputWarningFirstIteration = "Line processing problem: 9, column: CategoryID - [10.2.3.123] - does not fit Varchar(8) - truncating";
            string loadNumber1OutputWarningSecondIteration = "Line processing problem: 9, column: CategoryID - [10.2.3.123] - does not fit Varchar(9) - truncating";
           
            string loadNumber2OutputWarningFirstIteration = "Line processing problem: 9, column: CategoryID - [10.2.3.123] - does not fit Varchar(10) - truncating";
            string loadNumber2OutputWarningSecondIteration = "Line processing problem: 9, column: CategoryID - [10.2.3.123] - does not fit Varchar(11) - truncating";

            string loadNumber3OutputWarningFirstIteration = "Line processing problem: 9, column: CategoryID - [10.2.3.123] - does not fit Varchar(12) - truncating";
            string loadNumber3OutputWarningSecondIteration = "Line processing problem: 9, column: CategoryID - [10.2.3.123] - does not fit Varchar(13) - truncating";


            StreamWriter streamWriter = new StreamWriter(fileName);
            // loads first iterations
            timeString = incrementTime(timeString);
            streamWriter.WriteLine(dateString + " " + incrementTime(timeString) + ",671-0800 [Pool Worker - 9] INFO  -  Starting: ResetETLRun " + loadNumber1 + " ");
            timeString = incrementTime(timeString);
            streamWriter.WriteLine(dateString + " " + timeString + ",702-0700 [Process - com.successmetricsinc.warehouse.SourceFile@20807c - 18] WARN" +
                "  - [DATAWARNING:Categories.txt] " + loadNumber1OutputWarningFirstIteration);

            timeString = incrementTime(timeString);
            streamWriter.WriteLine(dateString + " " + incrementTime(timeString) + ",671-0800 [Pool Worker - 9] INFO  -  Starting: ResetETLRun " + loadNumber2 + " ");
            timeString = incrementTime(timeString);
            streamWriter.WriteLine(dateString + " " + timeString + ",702-0700 [Process - com.successmetricsinc.warehouse.SourceFile@20807c - 18] WARN" +
                "  - [DATAWARNING:Categories.txt] " + loadNumber2OutputWarningFirstIteration);

            timeString = incrementTime(timeString);
            streamWriter.WriteLine(dateString + " " + incrementTime(timeString) + ",671-0800 [Pool Worker - 9] INFO  -  Starting: ResetETLRun " + loadNumber3 + " ");
            timeString = incrementTime(timeString);
            streamWriter.WriteLine(dateString + " " + timeString + ",702-0700 [Process - com.successmetricsinc.warehouse.SourceFile@20807c - 18] WARN" +
                "  - [DATAWARNING:Categories.txt] " + loadNumber3OutputWarningFirstIteration);

            // load second iterations

            timeString = incrementTime(timeString);
            streamWriter.WriteLine(dateString + " " + timeString + ",671-0800 [Pool Worker - 9] INFO  -  Starting: ResetETLRun " + loadNumber1 + " ");
            timeString = incrementTime(timeString);
            streamWriter.WriteLine(dateString + " " + timeString + ",702-0700 [Process - com.successmetricsinc.warehouse.SourceFile@20807c - 18] WARN" +
                "  - [DATAWARNING:Categories.txt] " + loadNumber1OutputWarningSecondIteration);

            timeString = incrementTime(timeString);
            streamWriter.WriteLine(dateString + " " + timeString + ",671-0800 [Pool Worker - 9] INFO  -  Starting: ResetETLRun " + loadNumber2 + " ");
            timeString = incrementTime(timeString);
            streamWriter.WriteLine(dateString + " " + timeString + ",702-0700 [Process - com.successmetricsinc.warehouse.SourceFile@20807c - 18] WARN" +
                "  - [DATAWARNING:Categories.txt] " + loadNumber2OutputWarningSecondIteration);

            timeString = incrementTime(timeString);
            streamWriter.WriteLine(dateString + " " + timeString + ",671-0800 [Pool Worker - 9] INFO  -  Starting: ResetETLRun " + loadNumber3 + " ");
            timeString = incrementTime(timeString);
            streamWriter.WriteLine(dateString + " " + timeString + ",702-0700 [Process - com.successmetricsinc.warehouse.SourceFile@20807c - 18] WARN" +
                "  - [DATAWARNING:Categories.txt] " + loadNumber3OutputWarningSecondIteration);

            streamWriter.Flush();
            streamWriter.Close();

            // Lets get the updated details for load number 2

            GenericResponse response = PublishLogDetailsService.getFileDetails(null, warnStagingTableSource, dateString, true, loadNumber2);
            foreach (string str in response.other)
            {
                Console.WriteLine("Returned response = " + str);
            }

            Assert.AreEqual(loadNumber2OutputWarningSecondIteration, response.other[0], "Expected warning not parsed");

        }

        [Test]
        public void parseMultipleLogFilesWithDifferentLoadNumbers()
        {
            string dateString = "2010-04-01";
            string timeString1 = "11:12:14";
            
            string warnStagingTableSource = "Categories";
            //int loadNumber1 = 1;
            int loadNumber2 = 2;
            //int loadNumber3 = 3;
            MockHttpSession mockSession = MockHttpSession.getMockSession();
            Util.mockSession = mockSession;

            Space space = TestUtil.getTestSpace("SpaceTest", thisTestDir);
            mockSession["space"] = space;
                       

            MainAdminForm maf = TestUtil.getTestMAF(space);
            mockSession["MAF"] = maf;

            SourceFile sf = new SourceFile();
            sf.FileName = warnStagingTableSource + ".txt";
            sf.Columns = new SourceFile.SourceColumn[0];
            StagingTable st = null;// maf.stagingTableMod.addStagingTable(sf, false);
            if (st == null)
            {
                Assert.Fail("Error while adding test source file");
            }
            maf.sourceFileMod.addSourceFile(sf);

            // SMI engine files from server 1
            

            String fileName1 = createSMIEngineTestFileName(space.Directory, dateString, "Server1");
            Console.WriteLine("Filename = " + fileName1);
            string recentLogLoadNumber2Server1 = writeToSmiEngineTestFile(fileName1, timeString1, dateString, "Server1", 2);

            // SMI engine files from server 2            
            string timeString2 = "13:12:14";

            String fileName2 = createSMIEngineTestFileName(space.Directory, dateString, "Server2");
            Console.WriteLine("Filename = " + fileName2);
            string recentLogLoadNumber2Server2 = writeToSmiEngineTestFile(fileName1, timeString2, dateString, "Server2", 2);

            // Lets get the updated details for load number 2 from server 2

            GenericResponse response = PublishLogDetailsService.getFileDetails(null, warnStagingTableSource, dateString, true, loadNumber2);
            foreach (string str in response.other)
            {
                Console.WriteLine("Returned response = " + str);
            }

            Assert.AreEqual(recentLogLoadNumber2Server2, response.other[0], "Expected warning not parsed");

        }

        // get the recent line for the load number
        private string writeToSmiEngineTestFile(string fileName, string timeString, string dateString, string serverName, int loadNumber)
        {
            int loadNumber1 = 1;
            int loadNumber2 = 2;
            int loadNumber3 = 3;

            string loadNumber1OutputWarningFirstIteration = "Line processing problem: 9, column: CategoryID - [10.2.3.123] - does not fit Varchar(8) - truncating" + "_" + serverName;
            string loadNumber1OutputWarningSecondIteration = "Line processing problem: 9, column: CategoryID - [10.2.3.123] - does not fit Varchar(9) - truncating" + "_" + serverName;

            string loadNumber2OutputWarningFirstIteration = "Line processing problem: 9, column: CategoryID - [10.2.3.123] - does not fit Varchar(10) - truncating" + "_" + serverName;
            string loadNumber2OutputWarningSecondIteration = "Line processing problem: 9, column: CategoryID - [10.2.3.123] - does not fit Varchar(11) - truncating" + "_" + serverName;

            string loadNumber3OutputWarningFirstIteration = "Line processing problem: 9, column: CategoryID - [10.2.3.123] - does not fit Varchar(12) - truncating" + "_" + serverName;
            string loadNumber3OutputWarningSecondIteration = "Line processing problem: 9, column: CategoryID - [10.2.3.123] - does not fit Varchar(13) - truncating" + "_" + serverName;



            StreamWriter streamWriter = new StreamWriter(fileName);
            // loads first iterations
            timeString = incrementTime(timeString);
            streamWriter.WriteLine(dateString + " " + incrementTime(timeString) + ",671-0800 [Pool Worker - 9] INFO  -  Starting: ResetETLRun " + loadNumber1 + " ");
            timeString = incrementTime(timeString);
            streamWriter.WriteLine(dateString + " " + timeString + ",702-0700 [Process - com.successmetricsinc.warehouse.SourceFile@20807c - 18] WARN" +
                "  - [DATAWARNING:Categories.txt] " + loadNumber1OutputWarningFirstIteration);

            timeString = incrementTime(timeString);
            streamWriter.WriteLine(dateString + " " + incrementTime(timeString) + ",671-0800 [Pool Worker - 9] INFO  -  Starting: ResetETLRun " + loadNumber2 + " ");
            timeString = incrementTime(timeString);
            streamWriter.WriteLine(dateString + " " + timeString + ",702-0700 [Process - com.successmetricsinc.warehouse.SourceFile@20807c - 18] WARN" +
                "  - [DATAWARNING:Categories.txt] " + loadNumber2OutputWarningFirstIteration);

            timeString = incrementTime(timeString);
            streamWriter.WriteLine(dateString + " " + incrementTime(timeString) + ",671-0800 [Pool Worker - 9] INFO  -  Starting: ResetETLRun " + loadNumber3 + " ");
            timeString = incrementTime(timeString);
            streamWriter.WriteLine(dateString + " " + timeString + ",702-0700 [Process - com.successmetricsinc.warehouse.SourceFile@20807c - 18] WARN" +
                "  - [DATAWARNING:Categories.txt] " + loadNumber3OutputWarningFirstIteration);

            // load second iterations

            timeString = incrementTime(timeString);
            streamWriter.WriteLine(dateString + " " + timeString + ",671-0800 [Pool Worker - 9] INFO  -  Starting: ResetETLRun " + loadNumber1 + " ");
            timeString = incrementTime(timeString);
            streamWriter.WriteLine(dateString + " " + timeString + ",702-0700 [Process - com.successmetricsinc.warehouse.SourceFile@20807c - 18] WARN" +
                "  - [DATAWARNING:Categories.txt] " + loadNumber1OutputWarningSecondIteration);

            timeString = incrementTime(timeString);
            streamWriter.WriteLine(dateString + " " + timeString + ",671-0800 [Pool Worker - 9] INFO  -  Starting: ResetETLRun " + loadNumber2 + " ");
            timeString = incrementTime(timeString);
            streamWriter.WriteLine(dateString + " " + timeString + ",702-0700 [Process - com.successmetricsinc.warehouse.SourceFile@20807c - 18] WARN" +
                "  - [DATAWARNING:Categories.txt] " + loadNumber2OutputWarningSecondIteration);

            timeString = incrementTime(timeString);
            streamWriter.WriteLine(dateString + " " + timeString + ",671-0800 [Pool Worker - 9] INFO  -  Starting: ResetETLRun " + loadNumber3 + " ");
            timeString = incrementTime(timeString);
            streamWriter.WriteLine(dateString + " " + timeString + ",702-0700 [Process - com.successmetricsinc.warehouse.SourceFile@20807c - 18] WARN" +
                "  - [DATAWARNING:Categories.txt] " + loadNumber3OutputWarningSecondIteration);

            streamWriter.Flush();
            streamWriter.Close();

            string expectedOutput = null;
            expectedOutput = loadNumber == 1 ? loadNumber1OutputWarningSecondIteration : expectedOutput;
            expectedOutput = loadNumber == 2 ? loadNumber2OutputWarningSecondIteration : expectedOutput;
            expectedOutput = loadNumber == 3 ? loadNumber3OutputWarningSecondIteration : expectedOutput;

            return expectedOutput;

        }

        public string createSMIEngineTestFileName(String stringDir, string dateStringYYYYMMDD, string serverName)
        {
            Console.WriteLine("Server name " + serverName);

            string smiEngineLogName = "smiengine." + (serverName != null ? serverName : "SFOTEST") + "." + dateStringYYYYMMDD + ".log";
            Console.WriteLine("filename =  " + smiEngineLogName);

            if (!Directory.Exists(stringDir))
            {
                Directory.CreateDirectory(stringDir);
            }

            string spaceLogsDir = stringDir + Path.DirectorySeparatorChar + "logs";
            if (!Directory.Exists(spaceLogsDir))
            {
                Directory.CreateDirectory(spaceLogsDir);
            }

            String fullFileName = spaceLogsDir + Path.DirectorySeparatorChar.ToString() + smiEngineLogName;
            FileStream file = File.Create(fullFileName);
            file.Close();
            return fullFileName;
        }

        // timestamp is of the form hh:mm:ss
        private string incrementTime(string timeStamp)
        {
            string updatedTimeStamp = null;

            string[] timePars = timeStamp.Split(':');
            int hours = int.Parse(timePars[0]);
            int minutes = int.Parse(timePars[1]);
            int seconds = int.Parse(timePars[2]);

            if (seconds != 59)
            {
                seconds++;
            }
            else if(minutes != 59)
            {
                minutes++;
            }
            else if(hours != 59)
            {
                hours++;
            }

            updatedTimeStamp = hours + ":" + minutes + ":" + seconds;

            return updatedTimeStamp;
        }
    }


    
}
