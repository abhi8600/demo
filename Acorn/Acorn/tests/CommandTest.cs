﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NUnit.Framework;
using System.Data.Odbc;
using Acorn.DBConnection;

namespace Acorn.tests
{
    public class CommandTest
    {
        [SetUp]
        public void setup()
        {
            Util.TEST_FLAG = true;
        }

        [TearDown]
        public void tearDown()
        {
            Util.TEST_FLAG = false;
        }


        [Test]
        public void testDidCreateUser()
        {
            // right now just relying on test space ids on my local m/c
            // need to adduser, create user as part of setup.. TO DO.
            Guid createdByUserId = new Guid("92605E7A-7F0C-4F60-B6B0-27A05B31162C");
            Guid createdUserId = new Guid("5B6A9BAD-B53E-49F7-A716-0471A17F0670");

            QueryConnection conn = ConnectionPool.getConnection();
            string schema = Util.getMainSchema();

            Assert.True(Database.canUserManageUser(conn, schema, createdByUserId, createdUserId));

            // flip it and see if it still works
            Assert.False(Database.canUserManageUser(conn, schema, createdUserId, createdByUserId));
        }

        [Test]
        public void testGetCreatedUsers()
        {
            // right now just relying on test space ids on my local m/c
            // need to adduser, create user as part of setup.. TO DO.
            Guid createdByUserId = new Guid("92605E7A-7F0C-4F60-B6B0-27A05B31162C");
            string[] expectedUserNames = new string[]{
                "nobody21@birst.com", "nobody22@birst.com", 
                "nobody23@birst.com", "nobody24@birst.com",
                "nobody25@birst.com"};

            List<string> expectedUserNamesList = new List<string>(expectedUserNames);
            QueryConnection conn = ConnectionPool.getConnection();
            string schema = Util.getMainSchema();
            List<string> actualUserNamesList = Database.getManagedUsernames(conn, schema, createdByUserId);
            Assert.NotNull(actualUserNamesList);
            Assert.True(actualUserNamesList.Count == expectedUserNamesList.Count);

            Console.Write("Acutual ------\n");
            foreach (string actual in actualUserNamesList)
            {
                Console.WriteLine(actual);
            }

            Console.WriteLine("\n Expected --");
            foreach (string expected in expectedUserNamesList)
            {
                Console.WriteLine(expected);
                if (!actualUserNamesList.Contains(expected))
                {
                    Assert.Fail(expected + " User not found in createdUserNames "); 
                }                
            }





        }

    }
}
