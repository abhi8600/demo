﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Odbc;
using log4net;
using log4net.Config;
using NUnit.Framework;
using Acorn.Utils;
using Acorn.DBConnection;

namespace Acorn.tests
{
    public class UserManagementTest
    {
        private QueryConnection conn = null;
        private string schema = null;
        private static readonly ILog systemLog = LogManager.GetLogger(typeof(UserGroupTest));

        // keep all the usernames lowercase
        string[] manageUserNames = new string[]{
                "nobodytestmanageuser1@birst.com", "nobodytestmanageuser2@birst.com", 
                "nobodytestmanageuser3@birst.com", "nobodytestmanageuser4@birst.com",
                };

        string[] manageUserIds = null;

        string adminUserName = "nobodytestadminuser@birst.com";

        [SetUp]
        public void setUp()
        {            
            //Global.systemLog = systemLog;
            BasicConfigurator.Configure();
            Util.TEST_FLAG = true;
            conn = ConnectionPool.getConnection();
            ACLDetails.ACLDictionary = Database.getACLs(conn, TestUtil.schema);
            schema = Util.getMainSchema();

            configureUsers();
        }

        [TearDown]
        public void tearDown()
        {
            if (conn != null)
            {
                ConnectionPool.releaseConnection(conn);
            }
            Util.TEST_FLAG = false;            
        }


        private void configureUsers()
        {
            
            string manageUser1Name = manageUserNames[0].ToLower();
            string manageUser2Name = manageUserNames[1].ToLower();
            string manageUser3Name = manageUserNames[2].ToLower();
            string manageUser4Name = manageUserNames[3].ToLower(); ;
            

            User adminUser = Database.getUser(conn, schema, adminUserName, false);
            User manageUser1 = Database.getUser(conn, schema, manageUser1Name, false);
            User manageUser2 = Database.getUser(conn, schema, manageUser2Name, false);
            User manageUser3 = Database.getUser(conn, schema, manageUser3Name, false);
            User manageUser4 = Database.getUser(conn, schema, manageUser4Name, false);

            if (adminUser == null)
            {
                adminUser = TestUtil.createNewTestUser(adminUserName, null);
            }

            if (manageUser1 == null)
            {
                manageUser1 = TestUtil.createNewTestUser(manageUser1Name, null);
            }
            if (manageUser2 == null)
            {
                manageUser2 = TestUtil.createNewTestUser(manageUser2Name, null);
            }
            if (manageUser3 == null)
            {
                manageUser3 = TestUtil.createNewTestUser(manageUser3Name, null);
            }
            if (manageUser4 == null)
            {
                manageUser4 = TestUtil.createNewTestUser(manageUser4Name, null);
            }

            if (adminUser.AdminAccountId == null || adminUser.AdminAccountId == Guid.Empty)
            {
                adminUser.AdminAccountId = Guid.NewGuid();
                adminUser.ManagedAccountId = adminUser.AdminAccountId;
                Database.updateUser(conn, schema, adminUser);
            }

            if (manageUser1.ManagedAccountId == null || manageUser1.ManagedAccountId.ToString() != adminUser.AdminAccountId.ToString())
            {
                manageUser1.ManagedAccountId = adminUser.AdminAccountId;
                Database.updateUser(conn, schema, manageUser1);
            }


            if (manageUser2.ManagedAccountId == null || manageUser2.ManagedAccountId.ToString() != adminUser.AdminAccountId.ToString())
            {
                manageUser2.ManagedAccountId = adminUser.AdminAccountId;
                Database.updateUser(conn, schema, manageUser2);
            }

            if (manageUser3.ManagedAccountId == null || manageUser3.ManagedAccountId.ToString() != adminUser.AdminAccountId.ToString())
            {
                manageUser3.ManagedAccountId = adminUser.AdminAccountId;
                Database.updateUser(conn, schema, manageUser3);
            }

            if (manageUser4.ManagedAccountId == null || manageUser4.ManagedAccountId.ToString() != adminUser.AdminAccountId.ToString())
            {
                manageUser4.ManagedAccountId = adminUser.AdminAccountId;
                Database.updateUser(conn, schema, manageUser4);
            }

            manageUserIds = new string[manageUserNames.Length];
            manageUserIds[0] = manageUser1.ID.ToString();
            manageUserIds[1] = manageUser2.ID.ToString();
            manageUserIds[2] = manageUser3.ID.ToString();
            manageUserIds[3] = manageUser4.ID.ToString();
        }

        [Test]
        public void testGetManagedUsers()
        {
            try
            {                  

                User adminUser = Database.getUser(conn, schema, adminUserName, false);
                List<string> expectedUserNames = new List<string>();
                foreach (string str in manageUserNames)
                {
                    expectedUserNames.Add(str.ToLower());
                }
                List<UserSummary> response = Database.getManagedUsers(conn, schema, adminUser, null, UserManagementUtils.LIMIT_USER_RESULT);

                Assert.NotNull(response, "Response cannot be null");

                Console.WriteLine("Number of records = " + response.Count);
                Assert.True(response.Count == expectedUserNames.Count);
                foreach (UserSummary usummary in response)
                {
                    Console.WriteLine("User name " + usummary.Username);
                    if (expectedUserNames.Contains(usummary.Username))
                    {
                        expectedUserNames.Remove(usummary.Username);
                    }
                    else
                    {
                        Assert.Fail("Username not found " + usummary.Username);
                    }
                }

                // if there is any remaining one left in expectedUsers, flag it as failed
                if (expectedUserNames.Count > 0)
                {
                    string notPresent = "";
                    foreach (string name in expectedUserNames)
                    {
                        notPresent = notPresent + " ; " + name;
                    }
                    Assert.Fail("Following users were not returned " + notPresent);
                }
            }
            catch (Exception ex)
            {
                systemLog.Error("Exception : ", ex);
                Assert.Fail("Exception Encountered");
            }
        }

        [Test]
        public void testGetSpacesToManage()
        {
            try
            {
                User adminUser = Database.getUser(conn, schema, adminUserName, false);
                string adminSpace1Name = adminUserName + "Space1";
                string adminSpace2Name = adminUserName + "Space2";
                TestUtil.createNewTestSpace(adminUser, adminSpace1Name, true);
                TestUtil.createNewTestSpace(adminUser, adminSpace2Name, true);
                List<SpaceSummary> response = UserAdministration.getSpacesToManage(adminUser);

                Assert.NotNull(response, "Response cannot be null");

                Console.WriteLine("Number of records = " + response.Count);
                Assert.True(response.Count == 2);
                List<string> expectedSpaceNames = new List<string>();
                expectedSpaceNames.Add(adminSpace1Name);
                expectedSpaceNames.Add(adminSpace2Name);

                foreach (SpaceSummary spSummary in response)
                {
                    Console.WriteLine("User name " + spSummary.SpaceName);
                    if (expectedSpaceNames.Contains(spSummary.SpaceName))
                    {
                        expectedSpaceNames.Remove(spSummary.SpaceName);
                    }
                    else
                    {
                        Assert.Fail("Username not found " + spSummary.SpaceName);
                    }
                }

                // if there is any remaining one left in expectedSpaces, flag it as failed
                if (expectedSpaceNames.Count > 0)
                {
                    string notPresent = "";
                    foreach (string name in expectedSpaceNames)
                    {
                        notPresent = notPresent + " ; " + name;
                    }
                    Assert.Fail("Following spaces were not returned " + notPresent);
                }
            }
            catch (Exception ex)
            {
                systemLog.Error("Exception : ", ex);
                Assert.Fail("Exception Encountered");
            }
        }

        [Test]
        public void testaddUserToSpace()
        {
            User adminUser = Database.getUser(conn, schema, adminUserName, false);
            string adminSpace1Name = adminUserName + "Space1";
            Space adminUserSpace = TestUtil.createNewTestSpace(adminUser, adminSpace1Name, true);

            User manageUser1 = Database.getUser(conn, schema, manageUserNames[0], false); ;
            User manageUser2 = Database.getUser(conn, schema, manageUserNames[1], false); ;

            string[] toAddUsers = new string[] { manageUser1.ID.ToString(), manageUser2.ID.ToString() };

            UserAdministration.addUserToSpace(adminUser, toAddUsers, adminUserSpace.ID.ToString(), false);

            List<SpaceMembership> smList1 = Database.getSpaces(conn, schema, manageUser1, true);
            Assert.NotNull(smList1);
            bool found1 = false;
            foreach (SpaceMembership sm in smList1)
            {
                if (sm.Space.Name == adminUserSpace.Name)
                {
                    found1 = true;
                    Assert.True(sm.Administrator == false);
                }
            }
            Assert.True(found1, "Unable to find added space for user " + manageUser1.Username);           

            List<SpaceMembership> smList2 = Database.getSpaces(conn, schema, manageUser2, true);
            Assert.NotNull(smList2);
            bool found2 = false;
            foreach (SpaceMembership sm in smList1)
            {
                if (sm.Space.Name == adminUserSpace.Name)
                {
                    found2 = true;
                    Assert.True(sm.Administrator == false);
                    Assert.True(sm.Adhoc == true);
                    Assert.True(sm.Dashboards == true);
                }
            }

            Assert.True(found2, "Unable to find added space for user " + manageUser1.Username);           

        }

        [Test]
        public void testSetupTearDown()
        {
            configureUsers();
            configureUsers();
            configureUsers();
        }
        
        [Test]
        public void testUpdateUsersStatus()
        {
            try
            {
                User adminUser = Database.getUser(conn, schema, adminUserName, false); ;
                UserAdministration.updateUsersStatus(adminUser, manageUserIds, false);

                List<UserSummary> manageUserList = UserAdministration.getUsersToManage(adminUser, null, null);
                // This is to test if updating status in the user_product_table still allows us to retrieve users

                Assert.NotNull(manageUserList);
                Assert.True(manageUserList.Count > 0);

                foreach (string manageUserName in manageUserNames)
                {
                    systemLog.Info("User = " + manageUserName);
                    User manageUser = Database.getUser(conn, schema, manageUserName, false); ;
                    Assert.NotNull(manageUser);
                    List<Product> userProducts = Database.getProducts(conn, schema, manageUser.ID);
                    if (userProducts != null && userProducts.Count > 0)
                    {
                        Assert.Fail("Expected No active active products for user " + manageUser.Username + " but found " + userProducts.Count);
                    }
                }

                UserAdministration.updateUsersStatus(adminUser, manageUserIds, true);
                foreach (string manageUserName in manageUserNames)
                {
                    User manageUser = Database.getUser(conn, schema, manageUserName, false);
                    List<Product> userProducts = Database.getProducts(conn, schema, manageUser.ID);
                    if (userProducts == null || userProducts.Count == 0)
                    {
                        Assert.Fail("No active product found for user " + manageUser.Username);
                    }
                }
            }
            catch (Exception ex)
            {
                systemLog.Error("Exception : ", ex);
                Assert.Fail("Exception Encountered");
            }
        }
    }
}
