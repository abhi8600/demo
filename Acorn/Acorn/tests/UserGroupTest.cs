﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NUnit.Framework;
using Performance_Optimizer_Administration;
using System.IO;
using System.Data.Odbc;
using Acorn.Utils;
using log4net;
using log4net.Config;
using System.Xml.Serialization;
using System.Xml;
using Acorn.DBConnection;

namespace Acorn.tests
{
    public class UserGroupTest
    {

        private static string testDir = @"C:\Guri\Test\testFiles";
       // private static string fileName = testDir + @"\repository_dev.xml";
        private static string spId = "ab26d60e-1e91-4fd6-93de-2e197283534c";
        private static readonly ILog systemLog = LogManager.GetLogger(typeof(UserGroupTest));
        [SetUp]
        public void setUp()
        {
            Util.TEST_FLAG = true;            
            BasicConfigurator.Configure();
            QueryConnection conn = ConnectionPool.getConnection(TestUtil.TEST_CONNECT_STRING);
            ACLDetails.ACLDictionary = Database.getACLs(conn, TestUtil.schema);
            ConnectionPool.releaseConnection(conn);
            deleteTestSpaceGroups();            
        }


        [TearDown]
        public void tearDown()
        {
            Util.TEST_FLAG = false;
            //deleteTestSpaceGroups();
        }

        // Checking mapping Api at the database level
        [Test]
        public void createMappingInDbTest1()
        {
            Space sp = getSpaceFromDb();
            deleteMappingFile(sp);
            MainAdminForm maf = new MainAdminForm();
            string fileName = getTestRepFile(sp);
            maf.loadRepositoryFromFile(fileName);
            QueryConnection conn = ConnectionPool.getConnection(TestUtil.TEST_CONNECT_STRING);
            Database.mapAndCreateUserGroupMappings(conn, TestUtil.schema, sp, maf);            
            compareMappingResults(sp);
            ConnectionPool.releaseConnection(conn);
        }

        private string getTestRepFile(Space sp)
        {
            return sp.Directory + "\\" + Util.REPOSITORY_DEV;
        }

        // Checking the wrapper API at Util level
        [Test]
        public void createMappingInDbTest2()
        {            
            Space sp = getSpaceFromDb();
            deleteMappingFile(sp);
            if (sp.SpaceGroups != null && sp.SpaceGroups.Count > 0)
            {
                Assert.Fail("Space should have not space groups");
            }

            MainAdminForm maf = new MainAdminForm();
            string fileName = getTestRepFile(sp);
            maf.loadRepositoryFromFile(fileName);
            Util.checkAndCreateUserGroupDbMappings(sp, maf);            
            compareMappingResults(sp);
        }


        // Checking mappingModified Date at the space level
        // datetime values are rounded to increments of .000, .003, or .007 seconds, during insertion into sqlserver.
        
        [Test]
        public void testMappingDate()
        {            
            Space sp = getSpaceFromDb();
            //deleteMappingFile(sp);
            // write a dummy mapping file
            UserAndGroupUtils.saveUserAndGroupToFile(sp);
            QueryConnection conn = ConnectionPool.getConnection();            
            Database.updateMappingModifiedDate(conn, TestUtil.schema, sp);
            // Read it back again to get the current value from db
            DateTime updatedDbValue = Database.getMappingModifiedDate(conn, TestUtil.schema, sp);            
            UserAndGroupUtils.updatedUserGroupMappingFile(sp);            

            // Read the value from the mapping file
            UserAndGroupTransient uAg = UserAndGroupUtils.readFromUserGroupMappingFile(sp, null);
            Assert.True(updatedDbValue.ToString() == uAg.LastEditTime, "Dates do not match in db and file");            
            
            ConnectionPool.releaseConnection(conn);
        }        

        private void compareMappingResults(Space sp)
        {
            // Expected group,acls, users
            MainAdminForm maf = new MainAdminForm();
            string fileName = getTestRepFile(sp);
            maf.loadRepositoryFromFile(fileName);

            // Actual group, acls, users
            TextReader reader = new StreamReader(testDir + "\\" + UserAndGroupUtils.USER_AND_GROUP_FILENAME);
            Console.WriteLine(testDir + "\\" + UserAndGroupUtils.USER_AND_GROUP_FILENAME);
            XmlReader xreader = new XmlTextReader(reader);
            XmlSerializer serializer = new XmlSerializer(typeof(UserAndGroupTransient));
            UserAndGroupTransient uAg = (UserAndGroupTransient)serializer.Deserialize(xreader);

            foreach (Performance_Optimizer_Administration.Group expectedGroup in maf.usersAndGroups.groupsList)
            {
                if (expectedGroup.Name == "Administrators" && expectedGroup.InternalGroup)
                {
                    continue;
                }

                // check for the name
                bool foundGroupName = false;
                bool foundUserName = false;
                string unameNotFound = null;
                foreach (Performance_Optimizer_Administration.Group actualGroup in uAg.Groups)
                {
                    if (expectedGroup.Name == actualGroup.Name && expectedGroup.InternalGroup == actualGroup.InternalGroup)
                    {
                        foundGroupName = true;
                        foundUserName = false;

                        if (expectedGroup.Usernames == null)
                        {
                            Console.WriteLine("Groups with no users " + expectedGroup.Name);
                            Assert.True(actualGroup.Usernames == null || actualGroup.Usernames.Length == 0);
                            continue;
                        }

                        foreach (string expectedUsername in expectedGroup.Usernames)
                        {                 
                            if (actualGroup.Usernames.Contains(expectedUsername))
                            {
                                foundUserName = true;
                            }
                            else
                            {
                                foundUserName = false;
                                unameNotFound = expectedUsername;
                                break;
                            }
                        }

                        break;
                    }
                }

                if (!foundGroupName)
                {
                    Assert.Fail("Unable to find group " + expectedGroup.Name);
                }

                if (!foundUserName && expectedGroup.Usernames != null && expectedGroup.Usernames.Length > 0)
                {
                    Assert.Fail("Unable to find user = " + unameNotFound + " group name = " + expectedGroup.Name);
                }

                // check for acls
                foreach (Performance_Optimizer_Administration.ACLItem expectedACL in maf.usersAndGroups.accessList)
                {
                    if (expectedACL.Group != expectedGroup.Name)
                    {
                        continue;
                    }

                    // only check for ACLs with true access. We do not store false flag in database 
                    if (!expectedACL.Access)
                    {
                        continue;
                    }
                    bool foundACL = false;
                    foreach (Performance_Optimizer_Administration.ACLItem actualACL in uAg.ACL)
                    {
                        if (actualACL.Group == expectedGroup.Name && actualACL.Group == expectedACL.Group && actualACL.Access == expectedACL.Access)
                        {
                            foundACL = true;
                            break;
                        }
                    }

                    if (!foundACL)
                    {
                        Assert.Fail("Unable to find acl " + expectedACL.Tag + " for group " + expectedGroup.Name);
                    }
                }
                
            }

            // compare USERS node
            foreach (Performance_Optimizer_Administration.User expectedUser in maf.usersAndGroups.usersList)
            {
                if (expectedUser.Username == "Administrator")
                {
                    continue;
                }
                bool foundUserInUserNode = false;
                foreach (Performance_Optimizer_Administration.User actualUser in uAg.Users)
                {
                    if (actualUser.Username == expectedUser.Username)
                    {
                        foundUserInUserNode = true;
                        break;
                    }
                }

                if (!foundUserInUserNode)
                {
                    Assert.Fail("Unable to find username in Usernode " + expectedUser.Username);
                }
            }
            xreader.Close();
            reader.Close();
        }

        private Space getSpaceFromDb()
        {
            Guid spaceId = new Guid(spId);
            QueryConnection conn = null;
            try
            {
                conn = ConnectionPool.getConnection(TestUtil.TEST_CONNECT_STRING);
                Space sp = Database.getSpace(conn, TestUtil.schema, spaceId);
                sp.Directory = UserGroupTest.testDir;
                sp.SpaceGroups = Database.getSpaceGroups(conn, TestUtil.schema, sp);
                return sp;
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
        }

        public void deleteTestSpaceGroups()
        {
            Space sp = getSpaceFromDb();
            QueryConnection conn = ConnectionPool.getConnection(TestUtil.TEST_CONNECT_STRING);
            foreach (SpaceGroup spg in sp.SpaceGroups)
            {
                Console.WriteLine("Deleted Group " + spg.ID + " result " + Database.removeGroupFromSpace(conn, TestUtil.schema, sp, spg, true));
            }
            ConnectionPool.releaseConnection(conn);
        }   

        private void deleteMappingFile(Space sp)
        {
            string mappingFileName = sp.Directory + "\\" + UserAndGroupUtils.USER_AND_GROUP_FILENAME;
            FileInfo fi = new FileInfo(mappingFileName);
            if (fi.Exists)
            {
                File.Delete(mappingFileName);
            }
        }
        /*
        public void populateTestRepository1()
        {
            try
            {
                Guid spaceID = new Guid(spId);
                QueryConnection conn = ConnectionPool.getConnection(TestUtil.TEST_CONNECT_STRING);

                ACLDetails.ACLDictionary = Database.getACLs(conn, TestUtil.schema);

                Space sp = Database.getSpace(conn, TestUtil.schema, spaceID);

                MainAdminForm maf = new MainAdminForm();
                maf.loadRepository(new StreamReader(new FileStream(fileName, FileMode.Open, FileAccess.Read)));

                List<Group> groupList = maf.usersAndGroups.groupsList;
                List<ACLItem> groupaccessList = new List<ACLItem>(maf.usersAndGroups.accessList);
                foreach (Group group in groupList)
                {   
                    // Add group to the space
                    Guid groupID = Database.addGroupToSpace(conn, TestUtil.schema, sp.ID, group.Name, group.InternalGroup);

                    // Add ACLS for the group
                    foreach (ACLItem groupACL in groupaccessList)
                    {
                        if (groupACL.Group != group.Name)
                        {
                            Console.WriteLine("ACL Group : " + groupACL.Group);
                            Console.WriteLine("Skipped Present Group : " + group.Name); 
                            continue;
                        }

                        Console.WriteLine("ACL Group : " + groupACL.Group);
                        Console.WriteLine("Present Group : " + group.Name);
                        if (!groupACL.Access)
                        {
                            Console.WriteLine("ACLItem is " + groupACL.Access + " for acl " + groupACL.Tag + " and group " + group.Name);
                            continue;
                        }
                        int aclID = ACLDetails.getACLID(groupACL.Tag);
                        if(aclID <= 0)
                        {
                            Console.WriteLine("Unable to get the acl id for tag " + groupACL.Tag + " Skipping the population of " + 
                                group.Name + " for space " + sp.ID + " : " + sp.Name);
                            continue;
                        }

                        Database.addACLToGroup(conn, TestUtil.schema, groupID, aclID);                        
                    }                    

                    // Add user to the group
                    if (group.Usernames != null && group.Usernames.Length > 0)
                    {
                        List<string> groupUserNames = new List<string>(group.Usernames);
                        foreach (string userName in groupUserNames)
                        {
                            User groupUser = Database.getUserByEmail(conn, TestUtil.schema, userName);
                            if (groupUser == null)
                            {
                                Console.WriteLine("Userid not found for user = " + userName);
                                continue;
                            }
                            Database.addUserToGroup(conn, TestUtil.schema, groupID, groupUser.ID);
                        }
                    }
                }
                ConnectionPool.releaseConnection(conn);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Test exception " + ex.StackTrace);
                Assert.Fail("Test Exception", ex.Message);
            }
        }
         */

    }
}
