﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Acorn.sforce;
using System.Text;
using System.IO;
using Performance_Optimizer_Administration;
using System.Collections.Generic;
using System.Threading;
using System.Web.Services.Protocols;
using System.Data.Odbc;
using System.Net;
using Acorn.Background;
using System.Web.SessionState;
using Acorn.Utils;
using Acorn.DBConnection;
using System.Xml;

namespace Acorn
{
    public partial class AddSalesforce : LocalizedPage
    {
        private static bool ALLOW_SCHEDULED_IN_INTEGRATED = true;
        private static string mainSchema = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["MainSchema"];

        private User u;
        private Space sp;
        private MainAdminForm maf;

        void Page_Init(object sender, EventArgs e)
        {
            ViewStateUserKey = Session.SessionID;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            Util.needToChangePassword(Response, Session);

            maf = (MainAdminForm)Session["maf"];
            if (maf == null)
                Response.Redirect(Util.getHomePageURL(Session, Request));

            u = Util.validateAuth(Session, Response);
            sp = Util.validateSpace(Session, Response, Request);
            if (!Util.isSpaceAdmin(u, sp, Session))
            {
                Global.systemLog.Debug("redirecting to home page due to not being the space admin (admin)");
                Response.Redirect(Util.getHomePageURL(Session, Request));
            }
            Util.setUpdatedHeader(Session, sp, u, Master);

            Loader.Master = Master;
            Loader.Back = "AddSalesforce.aspx";
            Status.StatusResult status = (Status.StatusResult)Session["status"];
            bool reset = false;

            resetLastScheduleError();
            resetGeneralError();

            if (status != null && status.code == Status.StatusCode.DeleteData)
            {
                SalesForceMultiView.ActiveViewIndex = SalesForceMultiView.Views.IndexOf(DeletingData);                
                return;
            }
            else if (SalesForceMultiView.ActiveViewIndex == SalesForceMultiView.Views.IndexOf(DeletingData))
            {
                reset = true;
                QueryConnection conn = null;
                try
                {
                    conn = ConnectionPool.getConnection();
                    // at this point , loader should also be configured for the right load group and load number
                    // This decision point is reached if the earlier deleting data has been completed.
                    setLoaderForSalesForce();
                    Loader.setPublishHistory(sp, conn);
                }
                finally
                {
                    ConnectionPool.releaseConnection(conn);
                }
            }
            else if(SalesForceMultiView.ActiveViewIndex == SalesForceMultiView.Views.IndexOf(ScheduleStep))
            {
                // Let the event handler take care of switching to next view
                return;
            }
            else
            {
                
                SalesforceLoader sl = SalesforceLoader.getLoader(sp.ID);
                // Salesforce loader will be not null if the object is stored in the current server. 
                // What if there is a scheduled extraction picked up by another server. In that case, we 
                // need to check the task schedule table to make sure it is not being worked on. If it.
                // Then we show the page
                /*
                bool isExtractAlreadyRunning = false;
                if (sl == null)
                {
                    //bool isExtractAlreadyRunning = Util.isSFDCExtractRunning(sp) || (status != null && status.code == Status.StatusCode.SFDCExtract) ? true : false;
                }
                */
                bool isExtractAlreadyRunning = Util.isSFDCExtractRunning(sp);

                if (isExtractAlreadyRunning)
                {
                    SalesForceMultiView.ActiveViewIndex = SalesForceMultiView.Views.IndexOf(StatusStep);
                    return;
                }
            }
            if (!IsPostBack || reset)
            {
                setDebugTimes();
                Session.Remove("salesforcesettings");
                Session.Remove("sfbinding");
                Session.Remove("sforceloader");
                Session.Remove("sfautomatic");
                Session.Remove("ScheduleChanged");
                Session.Remove("ScheduleChangedNewScheduler");
                SalesforceSettings settings = getSettings();
                CredentialsNextButton.CommandName = "";
                if (settings.HasObjects())
                {
                    setViewSettings(settings, sp);
                    if (!Util.useExternalSchedulerExtract(sp))
                    {

                        // In addition, get status from the LOAD_SCHEDULE for the last scheduled run
                        // If there was an error during loading in the scheduled run, a generic 
                        // message is thrown to the user. 
                        //The specific exception that are caught are SoapException and WebException
                        // which are not UI user friendly messages e.g. The underlying connectionw as closed unexpectedly..what is user supposed to do ? 

                        List<ScheduledItem> scheduleItemList = Database.getTaskScheduleLoadStatus(sp.ID, SalesforceLoader.LOAD_GROUP_NAME);
                        if (scheduleItemList != null && scheduleItemList.Count > 0)
                        {
                            foreach (ScheduledItem sItem in scheduleItemList)
                            {
                                string lastRunCompletedStatus = sItem.Status;
                                if (lastRunCompletedStatus != null && lastRunCompletedStatus != SalesforceLoader.SALESFORCE_LOADING_SUCCESSFUL && lastRunCompletedStatus != SalesforceLoader.SALESFORCE_LOADING_SKIPPED)
                                {
                                    setLastScheduleError("Last Schedule Run : Unable to retrieve fully Salesforce Data. Please try again");
                                    break;
                                }
                            }
                        }
                    }
                    else
                    {
                        setLastScheduleError(status != null && status.message != null ? status.message : null);
                    }

                    if (!settings.automatic)
                        Loader.Visible = false;
                }
                else
                    SalesForceMultiView.ActiveViewIndex = SalesForceMultiView.Views.IndexOf(Authenticate);
            }
            else
            {
                SalesforceSettings settings = getSettings();
                if (status != null)
                    SettingsPlaceholder.Visible = !Status.isRunningCode(status.code) && !Request.Form["__EVENTTARGET"].Contains("processButton");
                int index = SalesForceMultiView.ActiveViewIndex;
                if (settings.HasObjects())
                {
                    setViewSettings(settings, sp);
                    if (!settings.automatic)
                        Loader.Visible = false;
                }
                if (SalesForceMultiView.ActiveViewIndex != index && SalesForceMultiView.ActiveViewIndex == SalesForceMultiView.Views.IndexOf(ViewSettings)
                    && index == SalesForceMultiView.Views.IndexOf(StatusStep))
                    Response.Redirect("~/AddSalesforce.aspx");

            }
        }

        protected void DeleteDataStatus_Tick(object sender, EventArgs e)
        {
            // Only use this, if delete data was kicked off using external scheduler
            // Keeping the old logic intact
            /*
            Space sp = Util.getSessionSpace(Session);
            if (sp != null && Util.useExternalSchedulerForProcess(sp))
            {
                Status.StatusResult statusResult = Status.getStatusUsingExternalScheduler(sp, Loader.LoadNumber, Loader.LoadGroup, false);
                if (statusResult != null && Status.isRunningCode(statusResult.code))
                {
                    Session["status"] = statusResult;
                }
            }
             */
        }

        private void setViewSettings(SalesforceSettings settings, Space sp)
        {
            SalesForceMultiView.ActiveViewIndex = SalesForceMultiView.Views.IndexOf(ViewSettings);
            bindObjects(settings);
            int maxLoad = Status.getMaxSuccessfulLoadID(sp, SalesforceLoader.LOAD_GROUP_NAME);
            if (settings.Automatic)
            {
                DeleteLastButton.Enabled = maxLoad > 0;
                DeleteLastLink.Enabled = maxLoad > 0;
                ProcessPostExtract.Enabled = true;
            }
            else
            {
                // If integrated mode, don't allow for separate delete
                DeleteLastButton.Visible = false;
                DeleteLastLink.Visible = false;
                ProcessPostExtract.Visible = false;
            }
            Loader.Visible = true;
        }

        private SalesforceSettings getSettings()
        {
            SalesforceSettings settings = (SalesforceSettings)Session["salesforcesettings"];
            if (settings != null)
                return settings;
            settings = SalesforceSettings.getSalesforceSettings(sp.Directory + "//sforce.xml");
            if (settings.scheduledSFDCJob == null)
            {
                // create a dummy sfdc job for later scheduling if it doesn't exist
                settings.scheduledSFDCJob = new SalesforceSettings.ScheduleSFDCJob();
            }
            Session["salesforcesettings"] = settings;
            return settings;
        }

        private void bindObjects(SalesforceSettings settings)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("Name");
            dt.Columns.Add("Date");
            if (settings.HasObjects())
            {
                foreach (SalesforceSettings.SalesforceObjectConnection soc in settings.Objects)
                {
                    DataRow dr = dt.NewRow();
                    dr[0] = soc.name;
                    dr[1] = soc.lastUpdatedDate;
                    dt.Rows.Add(dr);
                }
            }
            CurrentObjectsView.DataSource = dt;
            CurrentObjectsView.DataBind();
        }

        private SforceService getBinding()
        {
            SforceService service = (SforceService)Session["sfbinding"];
            if (service != null)
                return service;
            // Create service object
            SalesforceSettings settings = getSettings();
            SforceService binding = new SforceService();
            if (settings.sitetype == SalesforceSettings.SiteType.Sandbox)
                binding.Url = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["SFDC.Sandbox.URL"];
            if (!Util.isValidSFDCUrl(binding.Url))
            {
                Global.systemLog.Warn("Bad SFDC binding URL prior to authentication: " + binding.Url);
                return null;
            }
            binding.Timeout = 30 * 60 * 1000;
            String clientID = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["SFDCClientID"];
            if (clientID != null && clientID.Length > 0)
            {
                CallOptions co = new CallOptions();
                co.client = clientID;
                binding.CallOptionsValue = co;
            }
            // Invoke the login call and save results in LoginResult
            LoginResult lr = null;
            try
            {
                lr = binding.login(settings.Username, settings.Password);
            }
            catch (SoapException ex)
            {
                LoginError.Text = ex.Message;
                LoginError.Visible = true;
                return null;
            }
            catch (WebException ex)
            {
                LoginError.Text = ex.Message;
                LoginError.Visible = true;
                return null;
            }
            if (!lr.passwordExpired)
            {
                /*
                 * Can't login using zip until after authentication - errors not caught correctly if zip is on
                 */
                binding = new SforceServiceGzip(true, true);
                if (settings.sitetype == SalesforceSettings.SiteType.Sandbox)
                    binding.Url = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["SFDC.Sandbox.URL"];
                if (!Util.isValidSFDCUrl(binding.Url))
                {
                    Global.systemLog.Warn("Bad SFDC binding URL prior to gzip authentication: " + binding.Url);
                    return null;
                }
                binding.Timeout = 30 * 60 * 1000;
                if (clientID != null && clientID.Length > 0)
                {
                    CallOptions co = new CallOptions();
                    co.client = clientID;
                    binding.CallOptionsValue = co;
                }
                try
                {
                    lr = binding.login(settings.Username, settings.Password);
                }
                catch (SoapException ex)
                {
                    LoginError.Text = ex.Message;
                    LoginError.Visible = true;
                    return null;
                }
                catch (WebException ex)
                {
                    LoginError.Text = ex.Message;
                    LoginError.Visible = true;
                    return null;
                }
                // Reset the SOAP endpoint to the returned server URL
                binding.Url = lr.serverUrl;
                if (!Util.isValidSFDCUrl(binding.Url))
                {
                    Global.systemLog.Warn("Bad SFDC binding URL after authentication: " + binding.Url);
                    return null;
                }
                // Create a new session header object
                // Add the session ID returned from the login
                binding.SessionHeaderValue = new SessionHeader();
                binding.SessionHeaderValue.sessionId = lr.sessionId;
                GetUserInfoResult userInfo = lr.userInfo;
                Session["sfbinding"] = binding;
            }
            else
            {
                LoginError.Text = "Your password has expired";
                LoginError.Visible = true;
                return null;
            }
            return binding;
        }

        private List<string> saveSelections(MainAdminForm maf, Space sp, SforceService service, SalesforceSettings settings)
        {
            List<string> objects = new List<string>();
            for (int i = 0; i < SforceObjects.Items.Count; i++)
            {
                ListViewDataItem lvdi = SforceObjects.Items[i];
                CheckBox cb = (CheckBox)lvdi.FindControl("SelectedBox");
                if (cb != null && cb.Checked)
                {
                    DataRow dr = ((DataTable)Session["sfobjects"]).Rows[i];
                    string oname = (string)dr[0];
                    objects.Add(oname);
                    settings.getObjectConnection(oname);
                }
            }
            if (settings.Objects != null)
                foreach (SalesforceSettings.SalesforceObjectConnection soc in settings.Objects)
                {
                    if (soc.query != null && soc.query.Length > 0)
                    {
                        objects.Add(soc.name);
                    }
                }
            settings.updateNonqueryObjects(objects);
            return objects;
        }

        protected void SelectNext(object sender, EventArgs e)
        {
            SalesforceSettings settings = getSettings();
            SforceService service = getBinding();
            if (service == null)
                return;
            List<string> objects = saveSelections(maf, sp, service, settings);
            SalesforceLoader sl = new SalesforceLoader(objects, maf, sp, (User)Session["user"], service, settings, false, Session, true);
            Session["sforceloader"] = sl;
            AutomaticButton.Checked = sl.Settings.automatic;
            ManualButton.Checked = u.isFreeTrialUser;
            SalesforceSettings.ScheduleSFDCJob sfdcJob = null;
            try
            {
                sfdcJob = retrieveAndUpdateSchedulesFromExternalScheduler();
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Exception while retreiving schedules for salesforce", ex);
                setGeneralError("Error during retreiving schedules. Please contact Birst technical support for help.", null);
                return;
            }

            configureScheduleButtons();
            disableAllOldScheduleButtons();
            configureScheduleButtonsNewScheduler();

            if (u.isFreeTrialUser || (!ManualButton.Checked && !ScheduledButton.Checked && !ScheduledButtonNewScheduler.Checked))
            {
                // if both Manual and Schedule are false, then start with Manual
                ManualButton.Checked = true;
            }

            applySchedulerVersionConfiguration(sfdcJob);
            if (u.isFreeTrialUser)
            {

            }
            else if (Status.getMaxSuccessfulLoadID(sp, SalesforceLoader.LOAD_GROUP_NAME) > 0)
            {
                ScheduleNext(sender, e);
            }
            else
                SalesForceMultiView.ActiveViewIndex = SalesForceMultiView.Views.IndexOf(ProcessModeStep);
        }

        private void disableAllOldScheduleButtons()
        {
            ScheduledButton.Enabled = false;
            ScheduleCustomButton.Enabled = false;
            ScheduleDailyButton.Enabled = false;
            ScheduleMonthlyButton.Enabled = false;
            ScheduleAddID.Enabled = false;
            ScheduleDeleteID.Enabled = false;
            ScheduleDayID.Enabled = false;
            ScheduleHourID.Enabled = false;
            ScheduleMinuteID.Enabled = false;
            ScheduleAMPMID.Enabled = false;
        }

        private void configureScheduleButtons()
        {
            ScheduledButton.Checked = false;
            ScheduleCustomButton.Checked = false;
            ScheduleDailyButton.Checked = false;
            ScheduleMonthlyButton.Checked = false;

            List<SalesforceSettings.ScheduleInfo> details = getScheduleDetails();
            if (details.Count > 0)
            {
                bool isDailyEnabled = false;
                bool isCustomEnabled = false;
                bool isMonthlyEnabled = false;
                foreach (SalesforceSettings.ScheduleInfo si in details)
                {   
                    if (si.type == SalesforceSettings.UpdateSchedule.Daily && si.enable)
                    {
                        isDailyEnabled = true;
                    }
                    if (si.type == SalesforceSettings.UpdateSchedule.Weekly && si.enable)
                    {
                        isCustomEnabled = true;
                    }
                    if (si.type == SalesforceSettings.UpdateSchedule.Monthly && si.enable)
                    {
                        isMonthlyEnabled = true;
                    }
                    if (isDailyEnabled && isCustomEnabled && isMonthlyEnabled)
                    {
                        // The two checkboxes are arleady enabled. Nothing else to check. Just break
                        break;
                    }
                }

                if (isDailyEnabled || isCustomEnabled || isMonthlyEnabled)
                {
                    ScheduledButton.Checked = true;
                }
                ScheduleDailyButton.Checked = isDailyEnabled;
                ScheduleCustomButton.Checked = isCustomEnabled;
                ScheduleMonthlyButton.Checked = isMonthlyEnabled;
            }
        }

        private void configureScheduleButtonsNewScheduler()
        {
            ScheduledButtonNewScheduler.Checked = false;
            ScheduleCustomButtonNewScheduler.Checked = false;
            ScheduleDailyButtonNewScheduler.Checked = false;
            ScheduleMonthlyButtonNewScheduler.Checked = false;

            List<SalesforceSettings.ScheduleInfoExternalScheduler> details = getScheduleDetailsNewScheduler();
            if (details.Count > 0)
            {
                bool isDailyEnabledExternalScheduler = false;
                bool isCustomEnabledExternalScheduler = false;
                bool isMonthlyEnabledExternalScheduler = false;
                foreach (SalesforceSettings.ScheduleInfoExternalScheduler si in details)
                {  

                    if (si.type == SalesforceSettings.UpdateSchedule.Daily && si.enable)
                    {
                        isDailyEnabledExternalScheduler = true;
                    }
                    if (si.type == SalesforceSettings.UpdateSchedule.Weekly && si.enable)
                    {
                        isCustomEnabledExternalScheduler = true;
                    }
                    if (si.type == SalesforceSettings.UpdateSchedule.Monthly && si.enable)
                    {
                        isMonthlyEnabledExternalScheduler = true;
                    }
                    if (isDailyEnabledExternalScheduler && isCustomEnabledExternalScheduler && isMonthlyEnabledExternalScheduler)
                    {
                        // The two checkboxes are arleady enabled. Nothing else to check. Just break
                        break;
                    }
                }

                if (isDailyEnabledExternalScheduler || isCustomEnabledExternalScheduler || isMonthlyEnabledExternalScheduler)
                {
                    ScheduledButtonNewScheduler.Checked = true;
                }
                ScheduleDailyButtonNewScheduler.Checked = isDailyEnabledExternalScheduler;
                ScheduleCustomButtonNewScheduler.Checked = isCustomEnabledExternalScheduler;
                ScheduleMonthlyButtonNewScheduler.Checked = isMonthlyEnabledExternalScheduler;
            }
        }

        protected void ProcessPrev(object sender, EventArgs e)
        {
            SalesForceMultiView.ActiveViewIndex = SalesForceMultiView.Views.IndexOf(SelectObjects);
        }

        private void processDataUpdate(SalesforceLoader sl)
        {
            DataTable errors = new DataTable();
            errors.Columns.Add("Source");
            errors.Columns.Add("Severity");
            errors.Columns.Add("Error");
            errors.Columns.Add("Object");
            sl.Errors = errors;
            Space sp = sl.getSpace();
            if (Util.isLoadLockFilePresent(sp))
            {
                Global.systemLog.Error("Load Lock file is present for space : " + (sp != null ? sp.ID.ToString() : "" ));
                setGeneralError("Unable to start extract. Loading already in progress.", null);
                return;
            }
            else
            {
                resetGeneralError();
            }
               
            Session["sferrors"] = errors;
            if (Util.useExternalSchedulerForManualExtract(sl.getSpace()))
            {
                try
                {
                    SchedulerUtils.runNowScheduleExtract(sl.getSpace().ID, sl.getUser().ID, sl.getProcessAfterExtraction());
                }
                catch (Exception ex)
                {
                    Global.systemLog.Error("Exception while starting run now extract for salesforce", ex);
                    setGeneralError("Error starting extraction. Please contact Birst technical support for help.",  null);
                    return;
                }
            }
            else
            {
                String connectorName = "sfdc";
                if (ConnectorUtils.UseNewConnectorFramework())
                {
                    User u = Util.getSessionUser(Session);
                    connectorName = getSalesForceConnectorString(this.sp, u);
                }
                ConnectorUtils.startExtractWithoutScheduler(Session, sl.getUser(), sl.getSpace(), null, connectorName, SchedulerUtils.getStartExtractOptionsXml(sl.getProcessAfterExtraction()).ToString(), null);
                //BackgroundProcess.startModule(new BackgroundTask(sl, ApplicationLoader.MAX_TIMEOUT * 60 * 1000));
            }
            
            SalesForceMultiView.ActiveViewIndex = SalesForceMultiView.Views.IndexOf(StatusStep);
            StatusLabel.Text = "";
            StatusPrefix.Visible = false;
        }

        public static string getSalesForceConnectorString(Space sp, User u)
        {
            string connConnectionString = "";
            TrustedService.TrustedService ts = getTrustedServiceInstance(sp);
            XmlNode[] obj = (XmlNode[])ts.getConnectorsList(u.Username, sp.ID.ToString());
            WebServiceResponseXml response = WebServiceResponseXml.getWebServiceResponseXml(obj[0]);
            XElement[] connectorsList = WebServiceResponseXml.getElements(response.getRoot(), "Connector");

            Dictionary<string, string> connectorToConnectionString = new Dictionary<string, string>();
            string currentAPIVersionInConfig = ConnectorUtils.findConnectorAPIVersionInConfig("Salesforce", sp);
            for (int i = 0; i < connectorsList.Length; i++)
            {
                string connName = WebServiceResponseXml.getFirstElementValue(connectorsList[i], "ConnectorName");
                if (string.Equals(connName,"salesforce",StringComparison.OrdinalIgnoreCase))
                {
                    connConnectionString = WebServiceResponseXml.getFirstElementValue(connectorsList[i], "ConnectorConnectionString");
                    bool isDefault = bool.Parse(WebServiceResponseXml.getFirstElementValue(connectorsList[i], "IsDefaultAPIVersion"));
                    if (currentAPIVersionInConfig != null && currentAPIVersionInConfig.Equals(connConnectionString))
                        return connConnectionString;
                    else if (currentAPIVersionInConfig == null && isDefault)
                        return connConnectionString;
                }                
            }
            return connConnectionString;
        }

        private static string localprotocol = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["LocalProtocol"];

        private static TrustedService.TrustedService getTrustedServiceInstance(Space sp)
        {
            SpaceConfig sc = Util.getSpaceConfiguration(sp.Type);
            TrustedService.TrustedService ts = new TrustedService.TrustedService();
            ts.Url = localprotocol + sc.LocalURL + "/SMIWeb/services/TrustedService";
            return ts;
        }

        protected void ScheduleNext(object sender, EventArgs e)
        {
            if (u.isFreeTrialUser || (!ALLOW_SCHEDULED_IN_INTEGRATED && IntegratedButton.Checked))
            {
                ProcessingNext(sender, e);
            }
            else
            {
                SalesforceLoader sl = (SalesforceLoader)Session["sforceloader"];
                if (sl == null)
                    Response.Redirect(Util.getHomePageURL(Session, Request));
                NextDateBox.Text = sl.Settings.startDateNewScheduler.ToShortDateString();
                //NextDateBox.Text = getEarlierNextDate().ToShortDateString();
                List<SalesforceSettings.ScheduleInfo> details = getScheduleDetails();
                populateScheduleListControl(details);
                populateScheduleListControlNewScheduler(getScheduleDetailsNewScheduler());
                SalesForceMultiView.ActiveViewIndex = SalesForceMultiView.Views.IndexOf(ScheduleStep);                
            }
        }

        private DateTime getEarlierNextDate()
        {
            
            SalesforceSettings settings = getSettings();                                    
            List<SalesforceSettings.ScheduleInfo> scheduleDetails = getScheduleDetails();            
            List<DateTime> allScheduleDateTimes = new List<DateTime>();
            foreach (SalesforceSettings.ScheduleInfo scheduleInfo in scheduleDetails)
            {               
                // Add it to an array
                allScheduleDateTimes.Add(scheduleInfo.nextScheduleTime);
            }

            // find the earliest time, i.e. the earliest of all the schedules (greater than now)
            DateTime updatedDate = allScheduleDateTimes.Count > 0 ? allScheduleDateTimes[0] : DateTime.Now;
            foreach (DateTime tmpDate in allScheduleDateTimes)
            {
                if (tmpDate < DateTime.Now)
                {
                    // do not entertain time less than now
                    continue;
                }

                if (tmpDate < updatedDate)
                {
                    updatedDate = tmpDate;
                }
            }

            return updatedDate;
        }

        private void populateScheduleListControl(List<SalesforceSettings.ScheduleInfo> details)
        {
            // Populate list items                
            ScheduleList.Items.Clear();
            if (details != null && details.Count > 0)
            {                
                foreach (SalesforceSettings.ScheduleInfo si in details)
                {
                    if (si.type == SalesforceSettings.UpdateSchedule.Weekly)
                    {
                        string minute = si.Minute == 0 ? "00" : si.Minute.ToString();
                        string text = si.Day + "s" + " at " + si.Hour + ":" + minute + " " + si.AmPm;
                        string value = si.id;
                        ListItem scheduleListItem = new ListItem(text, value);
                        ScheduleList.Items.Add(scheduleListItem);                        
                    }
                }
            }            
        }

        private void setSelectedItem(string scheduleInfoId)
        {
            SalesforceSettings settings = getSettings();
            SalesforceSettings.ScheduleInfo si = settings.getScheduleInfo(scheduleInfoId);
            if (si != null)
            {                
                foreach (ListItem li in ScheduleDayID.Items)
                {                    
                    if (li.Text == si.Day)
                    {
                        ScheduleDayID.SelectedIndex = ScheduleDayID.Items.IndexOf(li);
                        break;
                    }                    
                }

                foreach (ListItem li in ScheduleHourID.Items)
                {                    
                    if (li.Text == si.Hour.ToString())
                    {
                        ScheduleHourID.SelectedIndex = ScheduleHourID.Items.IndexOf(li);
                        break;
                    }

                }

                foreach (ListItem li in ScheduleMinuteID.Items)
                {                    
                    if (si.Minute == 0)
                    {
                        ScheduleMinuteID.SelectedIndex = 0;
                        break;
                    }

                    if(li.Text == si.Minute.ToString())
                    {
                        ScheduleMinuteID.SelectedIndex = ScheduleMinuteID.Items.IndexOf(li);
                        break;
                    }
                }

                foreach (ListItem li in ScheduleAMPMID.Items)
                {
                    if (li.Text == si.AmPm)
                    {
                        ScheduleAMPMID.SelectedIndex = ScheduleAMPMID.Items.IndexOf(li);
                        break;
                    }
                }
            }

        }

        protected void ProcessingNext(object sender, EventArgs e)
        {
            SalesforceLoader sl = (SalesforceLoader)Session["sforceloader"];
            if (sl == null)
                Response.Redirect(Util.getHomePageURL(Session, Request));
            SalesForceMultiView.ActiveViewIndex = SalesForceMultiView.Views.IndexOf(StatusStep);
            sl.Settings.Automatic = !u.isFreeTrialUser && AutomaticButton.Checked;

            // check if schedule has changed, if not no point in continuing
            if (Session["ScheduleChanged"] != null)
            {
                Session.Remove("ScheduleChanged");
                saveScheduleSettings();
                updateAllScheduleTimes();
            }
            else if (scheduleStartDateChangedNewScheduler() || Session["ScheduleChangedNewScheduler"] != null)
            {
                Session.Remove("ScheduleChangedNewScheduler");
                saveScheduleSettingsNewScheduler();
                updateAllScheduleTimesNewScheduler();
            }
            else
            {   
                // just save the settings. This is to ensure that any objects changed are 
                // saved before external scheduler extraction is called
                getSettings().saveSettings(sp.Directory);
            }

            processDataUpdate(sl);
        }

        private void setDebugTimes()
        {
            if (Global.DebugDataMode)
            {
                ScheduleMinuteIDNewScheduler.Items.Clear();
                int duration = 5;
                for (int i = 0; i < 60; i = i + duration)
                {
                    string text = i < 10 ? "0" + i.ToString() : i.ToString();
                    ListItem li = new ListItem(text);
                    ScheduleMinuteIDNewScheduler.Items.Add(li);
                }

                ScheduleMinuteIDNewScheduler.DataBind();
            }
        }

        private void deactiveOldSchedules()
        {
            SalesforceSettings settings = getSettings();
            List<SalesforceSettings.ScheduleInfo> oldSchedulesInfoList = getScheduleDetails();
            if (oldSchedulesInfoList != null && oldSchedulesInfoList.Count > 0)
            {
                foreach (SalesforceSettings.ScheduleInfo scheduleInfo in oldSchedulesInfoList)
                {
                    scheduleInfo.enable = false;
                }
                settings.scheduleDetails = oldSchedulesInfoList.ToArray();
                updateAllScheduleTimes();
            }
            
        }

        private bool scheduleStartDateChanged()
        {
            bool changed = false;

            SalesforceSettings settings = getSettings();
            DateTime exisiting = settings.nextDate;
            DateTime givenDate = DateTime.Parse(NextDateBox.Text);
            // compare the date only 
            if (exisiting.Date != givenDate.Date)
            {
                changed = true;
                settings.nextDate = givenDate;
            }
            return changed;
        }

        private bool scheduleStartDateChangedNewScheduler()
        {
            bool changed = false;

            SalesforceSettings settings = getSettings();
            DateTime exisiting = settings.startDateNewScheduler;
            DateTime givenDate = DateTime.Parse(NextDateBox.Text);
            // compare the date only 
            if (exisiting.Date != givenDate.Date)
            {
                changed = true;
                settings.startDateNewScheduler = givenDate;
            }
            return changed;
        }

        private void updateAllScheduleTimes()
        {
            SalesforceSettings settings = getSettings();
            List<SalesforceSettings.ScheduleInfo> details = getScheduleDetails();
            DateTime nextDate = DateTime.Parse(NextDateBox.Text);            
            if (details != null)
            {
                foreach (SalesforceSettings.ScheduleInfo si in details)
                {
                    DateTime scheduleTime = nextDate;
                    if (nextDate < DateTime.Now)
                    {
                        si.nextScheduleTime = SalesforceLoader.getNextDate(si, scheduleTime);
                        continue;
                    }

                    
                    if (si.type == SalesforceSettings.UpdateSchedule.Weekly)
                    {
                        int hour = si.Hour;
                        if (si.AmPm == SalesforceSettings.AMTime && hour == 12)
                        {
                            hour = 0;
                        }
                        else if (hour < 12 && si.AmPm == SalesforceSettings.PMTime)
                        {
                            hour = hour + 12;
                        }

                        // check the current scheduled time                         
                        if (scheduleTime.DayOfWeek.ToString() != si.Day)
                        {
                            for (int i = 0; i < 7; i++)
                            {
                               scheduleTime =  scheduleTime.AddDays(1);
                                if (scheduleTime.DayOfWeek.ToString() == si.Day)
                                {
                                    break;
                                }
                            }
                        }

                        si.nextScheduleTime = new DateTime(scheduleTime.Year, scheduleTime.Month, scheduleTime.Day, hour, si.Minute, 0);
                    }
                    
                    if(si.type == SalesforceSettings.UpdateSchedule.Daily || si.type == SalesforceSettings.UpdateSchedule.Monthly)
                    {
                        si.nextScheduleTime = nextDate;
                    }
                }

                settings.scheduleDetails = details.ToArray();
            }

            settings.saveSettings(sp, true);
        }

        protected void StatusTick(object sender, EventArgs e)
        {           
            SalesforceLoader sl = SalesforceLoader.getLoader(sp.ID);
            if (sl == null)
            {
                DataTable et = (DataTable)Session["sferrors"];
                bool hasError = false;
                if (et != null && et.Rows.Count > 0)
                {
                    IssueView.DataSource = et;
                    IssueView.DataBind();
                    hasError = true;
                }
                SalesforceSettings settings = getSettings();
                if (hasError || (settings != null && settings.automatic))
                {
                    if (settings.HasObjects())
                        setViewSettings(settings, sp);
                    SalesForceMultiView.ActiveViewIndex = SalesForceMultiView.Views.IndexOf(ViewSettings);
                    Response.Redirect("~/AddSalesforce.aspx");
                }
                else
                {
                    Response.Redirect("~/FlexModule.aspx?birst.module=admin");
                }
                return;
            }
            if (sl.CurrentObject != null)
            {
                StatusPrefix.Visible = true;
                StatusLabel.Text = HttpUtility.HtmlEncode(sl.CurrentObject + " (" + sl.NumRecords + ")");
            }
        }

        private bool getsfobjects()
        {
            SforceService service = getBinding();
            if (service == null)
                return false;
            DataTable dt = new DataTable();
            dt.Columns.Add("Name");
            dt.Columns.Add("Selected", typeof(bool));
            sforce.DescribeGlobalResult dgr = null;
            try
            {
                dgr = service.describeGlobal();
                if (dgr == null)
                {
                    Global.systemLog.Debug("service.describeGlobal() returned null");
                }
                else
                {
                    Global.systemLog.Debug("Encoding: " + dgr.encoding);
                }
            }
            catch (Exception)
            {
                return false;
            }
            SalesforceSettings settings = getSettings();
            DescribeGlobalSObjectResult[] dgos = dgr.sobjects;
            if (dgos == null)
            {
                Global.systemLog.Debug("dgr.sobjects returned null");
            }
            foreach (DescribeGlobalSObjectResult dgsor in dgos)
            {
                DataRow dr = dt.NewRow();
                dr[0] = dgsor.name;
                dr[1] = false;
                if (settings.HasObjects())
                {
                    foreach (SalesforceSettings.SalesforceObjectConnection soc in settings.Objects)
                    {
                        if (soc.name == dgsor.name && (soc.query == null || soc.query.Length == 0))
                        {
                            dr[1] = true;
                            break;
                        }
                    }
                }
                dt.Rows.Add(dr);
            }
            // Add any sources previously mapped
            if (settings.objects != null)
                foreach (SalesforceSettings.SalesforceObjectConnection soc in settings.Objects)
                {
                    bool found = false;
                    foreach (DataRow dr in dt.Rows)
                    {
                        if ((string)dr[0] == soc.name)
                        {
                            found = true;
                        }
                    }
                    if (!found)
                    {
                        DataRow dr = dt.NewRow();
                        dr[0] = soc.name;
                        dr[1] = true;
                        dt.Rows.Add(dr);
                    }
                }
            SforceObjects.DataSource = dt;
            SforceObjects.DataBind();
            Session["sfobjects"] = dt;
            getsfqueries(settings);
            return true;
        }

        private void getsfqueries(SalesforceSettings settings)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("Name");
            dt.Columns.Add("Query");
            if (settings.Objects != null)
                foreach (SalesforceSettings.SalesforceObjectConnection soc in settings.Objects)
                {
                    if (soc.query != null && soc.query.Length > 0)
                    {
                        DataRow dr = dt.NewRow();
                        dr[0] = soc.name;
                        dr[1] = soc.query;
                        dt.Rows.Add(dr);
                    }
                }
            QueriesView.DataSource = dt;
            QueriesView.DataBind();
            if (dt.Rows.Count > 0)
                AddQuery.Visible = true;
        }

        protected void CredentialsNext(object sender, EventArgs e)
        {
            SalesforceSettings settings = getSettings();
            settings.SaveAuthentication = SaveLoginBox.Checked;
            settings.Username = UsernameBox.Text;
            settings.Password = PasswordBox.Text;
            string urlType = URLs.SelectedValue;
            settings.sitetype = (urlType == "Sandbox") ? SalesforceSettings.SiteType.Sandbox : SalesforceSettings.SiteType.Production;

            Session["salesforcesettings"] = settings;
            Session.Remove("sfbinding");

            if (!getsfobjects())
            {
                SalesForceMultiView.ActiveViewIndex = SalesForceMultiView.Views.IndexOf(Authenticate);
                return;
            }
            settings.saveSettings(sp.Directory);
            if (((ImageButton)sender).CommandName == "Status")
            {
                setViewSettings(settings, sp);
            }
            else
                SalesForceMultiView.ActiveViewIndex = SalesForceMultiView.Views.IndexOf(SelectObjects);
        }

        protected void SettingsNext(object sender, EventArgs e)
        {
            SalesforceSettings settings = getSettings();
            if (settings.SaveAuthentication)
            {
                SalesForceMultiView.ActiveViewIndex = SalesForceMultiView.Views.IndexOf(SelectObjects);
                getsfobjects();
            }
            else
                SalesForceMultiView.ActiveViewIndex = SalesForceMultiView.Views.IndexOf(Authenticate);
        }

        protected void ChangeCredentialsClick(object sender, EventArgs e)
        {
            CredentialsNextButton.CommandName = "Status";
            SalesForceMultiView.ActiveViewIndex = SalesForceMultiView.Views.IndexOf(Authenticate);
        }

        protected void UpdateDataClick(object sender, EventArgs e)
        {
            SforceService service = getBinding();
            if (service == null)
                return;
            List<string> objects = new List<string>();
            SalesforceSettings settings = getSettings();
            for (int i = 0; i < settings.Objects.Length; i++)
                objects.Add(settings.Objects[i].name);
            SalesforceLoader sl = new SalesforceLoader(objects, maf, sp, (User)Session["user"], service, settings, ProcessPostExtract.Checked, Session, true);
            Session["sforceloader"] = sl;
            processDataUpdate(sl);
        }

        protected void SettingsLoad(object sender, EventArgs e)
        {            
            SalesforceSettings settings = getSettings();
            if (settings.Objects == null || settings.Objects.Length == 0)
                Loader.Visible = false;
            setLoaderForSalesForce();
        }

        private void setLoaderForSalesForce()
        {           
            Loader.LoadGroup = SalesforceLoader.LOAD_GROUP_NAME;
            QueryConnection conn = null;
            try
            {
                conn = ConnectionPool.getConnection();
                Dictionary<string, int> curLoadNumbers = Database.getCurrentLoadNumbers(conn, mainSchema, sp.ID);
                Loader.LoadNumber = curLoadNumbers.ContainsKey(SalesforceLoader.LOAD_GROUP_NAME) ? curLoadNumbers[SalesforceLoader.LOAD_GROUP_NAME] : SalesforceLoader.BASE_SFORCE_LOAD_NUMBER;
                if (!IsPostBack && Session != null)
                {
                    // Get the updated status. Takes care of the use case :
                    // Where schedule extraction/processing is going and the status in the session needs to be updated to display the 
                    // LoadControl running status
                    bool getStatus = Session["status"] == null ? true : false;
                    if (!getStatus)
                    {
                        Status.StatusResult status = (Status.StatusResult)Session["status"];
                        if (!Status.isRunningCode(status.code) || status.code == Status.StatusCode.LoadingAnotherLoadGroup)
                        {
                            getStatus = true;
                        }
                    }

                    if (getStatus)
                    {   
                        bool fetchOldWay = true;
                        Status.StatusResult updatedResult = new Status.StatusResult(Status.StatusCode.None);
                        if (Util.useExternalSchedulerExtract(sp))
                        {
                            try
                            {
                                updatedResult = Status.getStatusUsingExternalScheduler(sp, Loader.LoadNumber + 1, Loader.LoadGroup, false);
                                fetchOldWay = updatedResult != null && Status.isRunningCode(updatedResult.code) ? false : true;
                            }catch(Exception ex)
                            {
                                Global.systemLog.Error("Error while getting status using externalScheduler", ex);
                            }
                        }

                        if(fetchOldWay)
                        {
                            updatedResult = Status.getLoadStatus(Session, sp, Loader.LoadNumber + 1, Loader.LoadGroup);
                        }

                        Session["status"] = updatedResult;
                    }
                }

            }
            catch (Exceptions.SchedulerException sEx)
            {
                Global.systemLog.Error("Exception while connecting to scheduler", sEx);
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
        }

        protected void AddQueryClick(object sender, EventArgs e)
        {
            QueryNameBox.Text = "";
            QueryBox.Text = "";
            LabelMappingBox.Text = "";
            MainAdminForm maf = (MainAdminForm)Session["maf"];
            if (maf == null)
                Response.Redirect(Util.getHomePageURL(Session, Request));
            Space sp = (Space)Session["space"];
            if (sp == null)
                Response.Redirect(Util.getHomePageURL(Session, Request));
            SalesforceSettings settings = getSettings();
            SforceService service = getBinding();
            if (service == null)
                return;
            saveSelections(maf, sp, service, settings);
            SelectObjectsViews.SetActiveView(EditQueryView);
            SalesForceMultiView.ActiveViewIndex = SalesForceMultiView.Views.IndexOf(SelectObjects);
        }

        protected void EditQueryClick(object sender, GridViewEditEventArgs e)
        {            
            SalesforceSettings settings = getSettings();
            if (settings.Objects != null)
            {
                int count = 0;
                foreach (SalesforceSettings.SalesforceObjectConnection soc in settings.Objects)
                {
                    if (soc.query != null && soc.query.Length > 0)
                    {
                        if (e.NewEditIndex == count)
                        {
                            SelectObjectsViews.SetActiveView(EditQueryView);
                            QueryNameBox.Text = soc.name;
                            QueryBox.Text = soc.query;
                            LabelMappingBox.Text = getObjectLabelMapping(Util.getSessionSpace(Session), soc.name);
                            break;
                        }
                        count++;
                    }
                }
            }
            getsfobjects();
            SalesForceMultiView.ActiveViewIndex = SalesForceMultiView.Views.IndexOf(SelectObjects);
        }

        private static string getObjectLabelMapping(Space sp, string objectName)
        {
            string mapping = "";
            try
            {
                string mappingFileName = SalesforceLoader.getMappingFileName(sp, objectName);
                if (File.Exists(mappingFileName))
                {
                    mapping = File.ReadAllText(mappingFileName, Encoding.UTF8);
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Error while reading mapping information for " + objectName, ex);
            }   
            
            return mapping;
        }

        private static void saveObjectLabelMapping(Space sp, string objectName, string mappingInfo)
        {
            try
            {
                string mappingFileName = SalesforceLoader.getMappingFileName(sp, objectName);
                File.WriteAllText(mappingFileName, mappingInfo);
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Error while writing mapping information for " + objectName, ex);
            }
        }

       

        protected void DeleteQueryClick(object sender, GridViewDeleteEventArgs e)
        {
            SalesforceSettings settings = getSettings();
            if (settings.Objects != null)
            {
                int count = 0;
                SalesforceSettings.SalesforceObjectConnection fsoc = null;
                foreach (SalesforceSettings.SalesforceObjectConnection soc in settings.Objects)
                {
                    if (soc.query != null && soc.query.Length > 0)
                    {
                        if (e.RowIndex == count)
                        {
                            fsoc = soc;
                            break;
                        }
                        count++;
                    }
                }
                if (fsoc != null)
                {
                    List<SalesforceSettings.SalesforceObjectConnection> olist = new List<SalesforceSettings.SalesforceObjectConnection>(settings.Objects);
                    olist.Remove(fsoc);
                    settings.objects = olist.ToArray();
                    getsfobjects();
                }
            }
            SalesForceMultiView.ActiveViewIndex = SalesForceMultiView.Views.IndexOf(SelectObjects);
        }

        protected void UpdateQueryClick(object sender, EventArgs e)
        {
            SelectObjectsViews.SetActiveView(SelectObjectsView);
            SalesforceSettings settings = getSettings();
            SalesforceSettings.SalesforceObjectConnection soc = settings.getObjectConnection(QueryNameBox.Text);
            soc.query = QueryBox.Text;
            saveObjectLabelMapping(Util.getSessionSpace(Session), soc.name, LabelMappingBox.Text);
            getsfobjects();
            SalesForceMultiView.ActiveViewIndex = SalesForceMultiView.Views.IndexOf(SelectObjects);
        }

        protected void GetQueryForObjectClick(object sender, EventArgs e)
        {
            SforceService service = getBinding();
            if (service == null)
                return;
            sforce.DescribeSObjectResult dresult = null;
            bool noObject = false;
            try
            {
                dresult = service.describeSObject(QueryNameBox.Text);
            }
            catch (SoapException se)
            {
                if (se.Message.StartsWith("INVALID_TYPE:"))
                {
                    Global.systemLog.Warn("Space: " + sp.Name + " - invalid Type Object : " + QueryNameBox.Text, se);
                }
                else
                {
                    Global.systemLog.Error("Space: " + sp.Name + " - SoapException in fetching object description for : " + QueryNameBox.Text, se);
                }
                noObject = true;
            }
            catch (WebException we)
            {
                Global.systemLog.Error("Space: " + sp.Name + " - WebException in fetching object description for : " + QueryNameBox.Text, we);
                noObject = true;
            }
            StringBuilder query = new StringBuilder();
            StringBuilder labelMapping = new StringBuilder();
            if (!noObject)
            {
                query.Append("SELECT ");
                bool first = true;
                foreach (Field f in dresult.fields)
                {
                    if (first)
                        first = false;
                    else
                    {
                        query.Append(',');
                        labelMapping.Append(",");
                    }
                    query.Append(f.name);
                    labelMapping.Append(getMappingForField(f));
                }
                query.Append(" FROM " + QueryNameBox.Text);
                for (int i = 0; i < dresult.fields.Length; i++)
                {
                    Field f = dresult.fields[i];
                }
            }
            QueryBox.Text = query.ToString();
            LabelMappingBox.Text = labelMapping.ToString();
            SelectObjectsViews.SetActiveView(EditQueryView);
            SalesForceMultiView.ActiveViewIndex = SalesForceMultiView.Views.IndexOf(SelectObjects);
        }

        private static string getMappingForField(Field field)
        {
            string labelMap = field.name + "=" + field.label;
            if(field.type == fieldType.id)
            {
                labelMap = labelMap + "=true";
            }

            return labelMap;
        }

        protected void CancelQueryUpdateClick(object sender, EventArgs e)
        {
            SelectObjectsViews.SetActiveView(SelectObjectsView);
            SalesForceMultiView.ActiveViewIndex = SalesForceMultiView.Views.IndexOf(SelectObjects);
        }

        protected void DeleteLastClick(object sender, EventArgs e)
        {
            string cmd = Util.getDeleteDataCommand(sp);
            List<Guid> statusTableCache = (List<Guid>)Session["statustablecache"];
            if (statusTableCache != null)
            {
                statusTableCache.Remove(sp.ID);
            }
            int maxLoad = Status.getMaxSuccessfulLoadID(sp, SalesforceLoader.LOAD_GROUP_NAME);
            DeleteDataThread ddt = new DeleteDataThread(sp, (MainAdminForm)Session["MAF"], false, cmd, Session, mainSchema, false, maxLoad, u);

            int spOpID = SpaceOpsUtils.getSpaceAvailability(sp);
            if (!SpaceOpsUtils.isSpaceAvailable(spOpID))
            {
                DeleteLastError.Text = "Error while starting delete operation. " + SpaceOpsUtils.getAvailabilityMessage(null, spOpID);
                DeleteLastError.Visible = true;

                Global.systemLog.Warn("DeleteData failed since space is already in use." + SpaceOpsUtils.getAvailabilityMessage(sp.Name, spOpID));
                return;
            }
            
            // Before starting background process, make sure you udpate txn_command_history because of weird bug issues around DeleteLast
            // e.g. bug 8767
            // bugfix 8767, Remove the earlier entry for the load number and put in the start of DeleteData                    
            bool updatedTxnStatus = Status.updateTxnStatusWithNextOperation(sp, maxLoad, Status.OP_DELETE_DATA, false);
            if (!updatedTxnStatus)
            {
                DeleteLastError.Text = "Error while starting delete operation. Please try again";
                DeleteLastError.Visible = true;

                Global.systemLog.Warn("DeleteData failed during update of Status table");
                return;
            }

            if (Util.useExternalSchedulerForDeletion(sp, Util.getSessionMAF(Session)))
            {
                try
                {
                    SchedulerUtils.scheduleDeleteJob(sp.ID, u.ID, false, false, maxLoad, SalesforceLoader.LOAD_GROUP_NAME);
                }
                catch (Exception ex)
                {
                    Global.systemLog.Error("Exception while scheduling delete job for salesforce", ex);
                    setGeneralError("Error starting Delete operation. Please contact Birst technical support for help.", null);
                    return;
                }
            }
            else
            {
                BackgroundProcess.startModule(new BackgroundTask(ddt, ApplicationLoader.MAX_TIMEOUT * 60 * 1000));
            }
            Session["status"] = new Status.StatusResult(Status.StatusCode.DeleteData);
            SalesForceMultiView.ActiveViewIndex = SalesForceMultiView.Views.IndexOf(DeletingData);
        }

        protected void SaveSelectionsClick(object sender, EventArgs e)
        {
            SalesforceSettings settings = getSettings();
            SforceService service = getBinding();
            if (service == null)
                return;
            saveSelections(maf, sp, service, settings);
            settings.saveSettings(sp, false);
            SalesForceMultiView.ActiveViewIndex = SalesForceMultiView.Views.IndexOf(SelectObjects);
            getsfobjects();
        }

        protected void CancelSelectionsClick(object sender, EventArgs e)
        {
            Response.Redirect("~/AddSalesforce.aspx");
        }

        protected void CancelSFDCLoad(object sender, EventArgs e)
        {
            SalesforceLoader sl = SalesforceLoader.getLoader(sp.ID);
            if (sl != null)
                sl.kill();
            // Put a kill file
            Util.createIfNotExistsSFDCKillLockFile(sp);
        }

        private void resetLastScheduleError()
        {
            LastScheduledRunError.Text = "";
            LastScheduledRunError.Visible = false;
        }

        private void setLastScheduleError(string message)
        {
            if (message != null && message != null && message.Trim().Length > 0)
            {
                LastScheduledRunError.Text = message;
                LastScheduledRunError.Visible = true;
            }
            else
            {
                resetLastScheduleError();
            }
            
        }

        protected void ScheduleList_SelectedIndexChanged(object sender, EventArgs e)
        {
            SalesforceSettings settings = getSettings();
            if (ScheduleList.SelectedIndex >= 0)
            {
                string scheduleId = ScheduleList.SelectedValue;
                setSelectedItem(scheduleId);
            }            
        }

        private List<SalesforceSettings.ScheduleInfo> getScheduleDetails()
        {
            SalesforceSettings settings = getSettings();
            List<SalesforceSettings.ScheduleInfo> scheduleDetails = null;
            if (settings.scheduleDetails != null)
            {
                scheduleDetails = new List<SalesforceSettings.ScheduleInfo>(settings.scheduleDetails);
            }
            else
            {
                scheduleDetails = new List<SalesforceSettings.ScheduleInfo>();
            }

            return scheduleDetails;
        }

        protected void ScheduleAddID_Click(object sender, EventArgs e)
        {
            markScheduleChange();
            SalesforceSettings settings = getSettings();
            SalesforceSettings.ScheduleInfo si = new SalesforceSettings.ScheduleInfo();
            si.id = Guid.NewGuid().ToString(); ;
            si.type = SalesforceSettings.UpdateSchedule.Weekly;
            si.Day = ScheduleDayID.SelectedValue;
            si.Hour = int.Parse(ScheduleHourID.SelectedValue);
            si.Minute = int.Parse(ScheduleMinuteID.SelectedValue);
            si.AmPm = ScheduleAMPMID.SelectedValue;
            si.enable = ScheduleCustomButton.Checked;
            List<SalesforceSettings.ScheduleInfo> details = getScheduleDetails();
            details.Add(si);
            settings.scheduleDetails = details.ToArray();
            populateScheduleListControl(details);
        }

        protected void ScheduleDeleteID_Click(object sender, EventArgs e)
        {
            markScheduleChange();
            SalesforceSettings settings = getSettings();
            if (ScheduleList.SelectedIndex >= 0)
            {
                string scheduleId = ScheduleList.SelectedValue;
                List<SalesforceSettings.ScheduleInfo> existingScheduleDetails = getScheduleDetails();
                if (existingScheduleDetails != null && existingScheduleDetails.Count > 0)                
                {
                    List<SalesforceSettings.ScheduleInfo> updatedList = new List<SalesforceSettings.ScheduleInfo>(existingScheduleDetails);                    
                    foreach (SalesforceSettings.ScheduleInfo si in existingScheduleDetails)
                    {
                        if (si.id == scheduleId)
                        {
                            updatedList.Remove(si);
                            // remove any load schedule from db
                            QueryConnection conn = null;
                            string schema = Util.getMainSchema();
                            try
                            {
                                conn = ConnectionPool.getConnection();
                                Database.removeTaskSchedule(conn, schema, sp.ID, new Guid(si.id), SalesforceLoader.SOURCE_GROUP_NAME);
                            }
                            catch (Exception)
                            {
                                Global.systemLog.Warn("Error while removing task schedule entry " + si.id);
                            }
                            finally
                            {
                                ConnectionPool.releaseConnection(conn);
                            }
                            break;
                        }                        
                    }

                    settings.scheduleDetails = updatedList.ToArray();
                    populateScheduleListControl(updatedList);
                }                
            }
        }

        protected void ScheduleCancel_Click(object sender, ImageClickEventArgs e)
        {
            removeTempCopyNewSchedules();
            SalesforceLoader.clearLoader(sp.ID);
            Response.Redirect("~/AddSalesforce.aspx");
        }

        protected void ScheduleDone_Click(object sender, ImageClickEventArgs e)
        {
            string action = "";
            try
            {
                SalesforceSettings settings = getSettings();
                settings.Automatic = !u.isFreeTrialUser && AutomaticButton.Checked;
                // check for migration first
                if (MigrateCheckBoxID.Checked)
                {
                    action = "Migration";
                    SchedulerUtils.copySFDCSchedules(sp, Util.getSessionUser(Session), settings.scheduledSFDCJob);
                    Util.updateSpaceSFDCVersion(sp, Util.getDefaultSFDCVersion(), true);
                }
                else if (Session["ScheduleChanged"] != null)
                {   
                    Session.Remove("ScheduleChanged");
                    saveScheduleSettings();
                    updateAllScheduleTimes();
                }
                else if (scheduleStartDateChangedNewScheduler() || Session["ScheduleChangedNewScheduler"] != null)
                {
                    bool scheduleChangedNewSchedulerFlag = Session["ScheduleChangedNewScheduler"] != null ? true : false;
                    Session.Remove("ScheduleChangedNewScheduler");
                    if (sp.SFDCVersionID <= 0)
                    {
                        Util.updateSpaceSFDCVersion(sp, Util.getDefaultSFDCVersion(), false);
                    }
                    if (Util.useExternalSchedulerExtract(sp))
                    {
                        bool canSchedulesBeModified = true;
                        // check if the schedules are from the current logged in version
                        if (settings.scheduledSFDCJob != null && settings.scheduledSFDCJob.schedulerVersion != null
                            && !settings.scheduledSFDCJob.schedulerVersion.primaryScheduler)
                        {
                            canSchedulesBeModified = false;
                        }

                        if (canSchedulesBeModified)
                        {
                            int sfdcExtractionVersionId = Util.getDefaultSFDCVersion();
                            if (sp.SFDCVersionID != sfdcExtractionVersionId && scheduleChangedNewSchedulerFlag)
                            {
                                Util.updateSpaceSFDCVersion(sp, sfdcExtractionVersionId, true);
                            }

                            action = "SchedulesUpdate";
                            List<SalesforceSettings.ScheduleInfoExternalScheduler> updatedList = getTempCopyOfScheduleDetailsNewScheduler();
                            settings.scheduledSFDCJob.schedulerDetailsNewScheduler = updatedList.ToArray();
                            saveScheduleSettingsNewScheduler();
                            updateAllScheduleTimesNewScheduler();
                            deactiveOldSchedules();
                        }
                    }
                }
                else
                {
                    // just save settings and write to sforce.xml without the need to update schedule times in the load_schedule table                

                    settings.saveSettings(sp.Directory);
                }

                removeTempCopyNewSchedules();
                SalesforceLoader.clearLoader(sp.ID);
            }
            catch(Exception ex)
            {
                Global.systemLog.Error("Error during done click event handler ", ex);
                if (action == "Migration")
                {
                    setGeneralError("Error during migrating schedules. Please contact Birst technical support for help.", null);
                }
                else if (action == "SchedulesUpdate")
                {
                    setGeneralError("Error during updating schedules. Please contact Birst technical support for help.", null);
                }

                return;
            }
            Response.Redirect("~/AddSalesforce.aspx");
        }

        private void resetGeneralError()
        {
            GeneralErrorLabel.Visible = false;
            GeneralErrorLabel.Text = "";
            ContinueLink.NavigateUrl = "~/AddSalesforece.aspx";
        }

        private void setGeneralError(string msg, string continueLinkUrl)
        {
            GeneralErrorLabel.Visible = true;
            GeneralErrorLabel.Text = msg;
            ContinueLink.NavigateUrl = continueLinkUrl == null ? "~/AddSalesforce.aspx" : continueLinkUrl;
            SalesForceMultiView.ActiveViewIndex = SalesForceMultiView.Views.IndexOf(ErrorView);
        }

        private void saveScheduleSettings()
        {

            SalesforceSettings settings = getSettings();
            List<SalesforceSettings.ScheduleInfo> scheduleDetails = getScheduleDetails();            

            SalesforceSettings.ScheduleInfo dailyScheduleInfo = null;
            SalesforceSettings.ScheduleInfo monthlySchduleInfo = null;
            SalesforceSettings.ScheduleInfo manualScheduleInfo = null;
                        
            // update all the custom schedule -- enable or disable them
            if (scheduleDetails != null && scheduleDetails.Count > 0)
            {
                foreach (SalesforceSettings.ScheduleInfo si in scheduleDetails)
                {
                    if (si.type == SalesforceSettings.UpdateSchedule.Weekly)
                    {
                        si.enable = ScheduleCustomButton.Checked && ScheduledButton.Checked;
                    }

                    if (si.type == SalesforceSettings.UpdateSchedule.Daily)
                    {                        
                        si.enable = ScheduleDailyButton.Checked && ScheduledButton.Checked;
                        dailyScheduleInfo = si;
                    }

                    if (si.type == SalesforceSettings.UpdateSchedule.Monthly)
                    {                        
                        si.enable = ScheduleMonthlyButton.Checked && ScheduledButton.Checked;
                        monthlySchduleInfo = si;
                    }

                    if (si.type == SalesforceSettings.UpdateSchedule.Manual)
                    {
                        manualScheduleInfo = si;
                    } 
                }
            }

            if (dailyScheduleInfo == null && ScheduledButton.Checked && ScheduleDailyButton.Checked)
            {
                SalesforceSettings.ScheduleInfo dailyInfo = new SalesforceSettings.ScheduleInfo();
                dailyInfo.type = SalesforceSettings.UpdateSchedule.Daily;
                dailyInfo.id = Guid.NewGuid().ToString();
                dailyInfo.enable = true;
                scheduleDetails.Add(dailyInfo);
            }

            if (monthlySchduleInfo == null && ScheduledButton.Checked && ScheduleMonthlyButton.Checked)
            {
                SalesforceSettings.ScheduleInfo monthlyInfo = new SalesforceSettings.ScheduleInfo();
                monthlyInfo.type = SalesforceSettings.UpdateSchedule.Monthly;
                monthlyInfo.id = Guid.NewGuid().ToString();
                monthlyInfo.enable = true;
                scheduleDetails.Add(monthlyInfo);
            }
                        
            if (manualScheduleInfo != null && !ManualButton.Checked)
            {
                // Remove if it not requried
                scheduleDetails.Remove(manualScheduleInfo);
            }
            else if (manualScheduleInfo == null && ManualButton.Checked)
            {
                // Add manual if needed
                SalesforceSettings.ScheduleInfo si = new SalesforceSettings.ScheduleInfo();
                si.type = SalesforceSettings.UpdateSchedule.Manual;
                si.id = Guid.NewGuid().ToString();
                si.enable = true;
                scheduleDetails.Add(si);
            }

            settings.scheduleDetails = scheduleDetails.ToArray();
        }

        protected void ManualButton_CheckedChanged(object sender, EventArgs e)
        {
            markScheduleChangeNewScheduler();
        }

        protected void ScheduledButton_CheckedChanged(object sender, EventArgs e)
        {
            markScheduleChange();
        }

        protected void ScheduleDailyButton_CheckedChanged(object sender, EventArgs e)
        {
            markScheduleChange();
        }

        protected void ScheduleMonthlyButton_CheckedChanged(object sender, EventArgs e)
        {
            markScheduleChange();
        }

        protected void ScheduleCustomButton_CheckedChanged(object sender, EventArgs e)
        {
            markScheduleChange();
        }

        private void markScheduleChange()
        {
            Session["ScheduleChanged"] = true;
        }


        protected void ScheduledButtonNewScheduler_CheckedChanged(object sender, EventArgs e)
        {
            markScheduleChangeNewScheduler();
        }

        protected void ScheduleDailyButtonNewScheduler_CheckedChanged(object sender, EventArgs e)
        {
            markScheduleChangeNewScheduler();
        }

        protected void ScheduleMonthlyButtonNewScheduler_CheckedChanged(object sender, EventArgs e)
        {
            markScheduleChangeNewScheduler();
        }

        protected void ScheduleCustomButtonNewScheduler_CheckedChanged(object sender, EventArgs e)
        {
            markScheduleChangeNewScheduler();
        }

        protected void ScheduleAddIDNewScheduler_Click(object sender, EventArgs e)
        {
            markScheduleChangeNewScheduler();
            SalesforceSettings settings = getSettings();
            SalesforceSettings.ScheduleInfoExternalScheduler si = new SalesforceSettings.ScheduleInfoExternalScheduler();
            si.id = Guid.NewGuid().ToString(); ;
            si.type = SalesforceSettings.UpdateSchedule.Weekly;
            si.Day = ScheduleDayIDNewScheduler.SelectedValue;
            si.Hour = int.Parse(ScheduleHourIDNewScheduler.SelectedValue);
            si.Minute = int.Parse(ScheduleMinuteIDNewScheduler.SelectedValue);
            si.AmPm = ScheduleAMPMIDNewScheduler.SelectedValue;
            si.enable = ScheduleCustomButtonNewScheduler.Checked;
            List<SalesforceSettings.ScheduleInfoExternalScheduler> details = getTempCopyOfScheduleDetailsNewScheduler(); //getScheduleDetailsNewScheduler();
            details.Add(si);
            //settings.scheduledSFDCJob.schedulerDetailsNewScheduler = details.ToArray();
            populateScheduleListControlNewScheduler(details);
        }

        private List<SalesforceSettings.ScheduleInfoExternalScheduler> getTempCopyOfScheduleDetailsNewScheduler()
        {
            if (Session["TempCopySchedules"] == null)
            {
                List<SalesforceSettings.ScheduleInfoExternalScheduler> tmpCopy = new List<SalesforceSettings.ScheduleInfoExternalScheduler>(getScheduleDetailsNewScheduler());
                Session["TempCopySchedules"] = tmpCopy;
            }

            return (List<SalesforceSettings.ScheduleInfoExternalScheduler>)Session["TempCopySchedules"];
        }

        private void removeTempCopyNewSchedules()
        {
            Session.Remove("TempCopySchedules");
        }

        private List<SalesforceSettings.ScheduleInfoExternalScheduler> getScheduleDetailsNewScheduler()
        {
            SalesforceSettings settings = getSettings();
            List<SalesforceSettings.ScheduleInfoExternalScheduler> newScheduleDetails = null;
            if (settings.scheduledSFDCJob.schedulerDetailsNewScheduler != null)
            {
                newScheduleDetails = new List<SalesforceSettings.ScheduleInfoExternalScheduler>(settings.scheduledSFDCJob.schedulerDetailsNewScheduler);
            }
            else
            {
                newScheduleDetails = new List<SalesforceSettings.ScheduleInfoExternalScheduler>();
            }

            return newScheduleDetails;
        }

        protected void ScheduleDeleteIDNewScheduler_Click(object sender, EventArgs e)
        {
            markScheduleChangeNewScheduler();
            SalesforceSettings settings = getSettings();
            if (ScheduleListNewScheduler.SelectedIndex >= 0)
            {
                string scheduleId = ScheduleListNewScheduler.SelectedValue;
                List<SalesforceSettings.ScheduleInfoExternalScheduler> existingScheduleDetails = getTempCopyOfScheduleDetailsNewScheduler();//getScheduleDetailsNewScheduler();
                if (existingScheduleDetails != null && existingScheduleDetails.Count > 0)
                {
                    SalesforceSettings.ScheduleInfoExternalScheduler toDelete = null;
                    foreach (SalesforceSettings.ScheduleInfoExternalScheduler si in existingScheduleDetails)
                    {
                        if (si.id != null && si.id.ToString() == scheduleId)
                        {   
                            toDelete = si;
                            break;
                        }
                    }

                    //settings.scheduledSFDCJob.schedulerDetailsNewScheduler = updatedList.ToArray();
                    if (toDelete != null)
                    {
                        existingScheduleDetails.Remove(toDelete);
                    }
                    populateScheduleListControlNewScheduler(existingScheduleDetails);
                }
            }
        }

        private void markScheduleChangeNewScheduler()
        {
            Session["ScheduleChangedNewScheduler"] = true;
        }

        private void saveScheduleSettingsNewScheduler()
        {

            SalesforceSettings settings = getSettings();
            List<SalesforceSettings.ScheduleInfoExternalScheduler> scheduleDetailsNewScheduler = getScheduleDetailsNewScheduler();
            List<SalesforceSettings.ScheduleInfoExternalScheduler> updatedSchedulerDetailsNewScheduler = new List<SalesforceSettings.ScheduleInfoExternalScheduler>();
            SalesforceSettings.ScheduleInfoExternalScheduler dailyScheduleInfoNewScheduler = null;
            SalesforceSettings.ScheduleInfoExternalScheduler monthlySchduleInfoNewScheduler = null;
            SalesforceSettings.ScheduleInfoExternalScheduler manualScheduleInfoNewScheduler = null;

            // update all the custom schedule -- enable or disable them
            if (scheduleDetailsNewScheduler != null && scheduleDetailsNewScheduler.Count > 0)
            {
                foreach (SalesforceSettings.ScheduleInfoExternalScheduler si in scheduleDetailsNewScheduler)
                {
                    if (si.type == SalesforceSettings.UpdateSchedule.Weekly)
                    {
                        si.enable = ScheduleCustomButtonNewScheduler.Checked && ScheduledButtonNewScheduler.Checked;
                        updatedSchedulerDetailsNewScheduler.Add(si);
                    }

                    if (si.type == SalesforceSettings.UpdateSchedule.Daily && ScheduleDailyButtonNewScheduler.Checked && ScheduledButtonNewScheduler.Checked)
                    {
                        dailyScheduleInfoNewScheduler = si;
                        updatedSchedulerDetailsNewScheduler.Add(si);
                    }

                    if (si.type == SalesforceSettings.UpdateSchedule.Monthly && ScheduleMonthlyButtonNewScheduler.Checked && ScheduledButtonNewScheduler.Checked)
                    {
                       monthlySchduleInfoNewScheduler = si;
                       updatedSchedulerDetailsNewScheduler.Add(si);
                    }

                    if (si.type == SalesforceSettings.UpdateSchedule.Manual)
                    {
                        manualScheduleInfoNewScheduler = si;
                    }
                }
            }

            if (dailyScheduleInfoNewScheduler == null && ScheduledButtonNewScheduler.Checked && ScheduleDailyButtonNewScheduler.Checked)
            {
                SalesforceSettings.ScheduleInfoExternalScheduler dailyInfo = new SalesforceSettings.ScheduleInfoExternalScheduler();
                dailyInfo.type = SalesforceSettings.UpdateSchedule.Daily;
                dailyInfo.id = Guid.NewGuid().ToString();
                dailyInfo.enable = true;
                updatedSchedulerDetailsNewScheduler.Add(dailyInfo);
            }

            if (monthlySchduleInfoNewScheduler == null && ScheduledButtonNewScheduler.Checked && ScheduleMonthlyButtonNewScheduler.Checked)
            {
                SalesforceSettings.ScheduleInfoExternalScheduler monthlyInfo = new SalesforceSettings.ScheduleInfoExternalScheduler();
                monthlyInfo.type = SalesforceSettings.UpdateSchedule.Monthly;
                monthlyInfo.id = Guid.NewGuid().ToString();
                monthlyInfo.enable = true;
                updatedSchedulerDetailsNewScheduler.Add(monthlyInfo);
            }
            settings.scheduledSFDCJob.schedulerDetailsNewScheduler = updatedSchedulerDetailsNewScheduler.ToArray();
        }

        private void populateScheduleListControlNewScheduler(List<SalesforceSettings.ScheduleInfoExternalScheduler> newScheduleDetails)
        {
            // Populate list items                
            ScheduleListNewScheduler.Items.Clear();
            if (newScheduleDetails != null && newScheduleDetails.Count > 0)
            {
                foreach (SalesforceSettings.ScheduleInfoExternalScheduler si in newScheduleDetails)
                {
                    if (si.type == SalesforceSettings.UpdateSchedule.Weekly)
                    {
                        string minute = si.Minute == 0 ? "00" : si.Minute.ToString();
                        string text = si.Day + "s" + " at " + si.Hour + ":" + minute + " " + si.AmPm;
                        string value = si.id;
                        ListItem scheduleListItem = new ListItem(text, value);
                        ScheduleListNewScheduler.Items.Add(scheduleListItem);
                    }
                }
            }
        }

        private void updateAllScheduleTimesNewScheduler()
        {
            SalesforceSettings settings = getSettings();
            List<SalesforceSettings.ScheduleInfoExternalScheduler> detailsNewScheduler = getScheduleDetailsNewScheduler();
            DateTime nextDate = DateTime.Parse(NextDateBox.Text);
            if (detailsNewScheduler != null)
            {
                foreach (SalesforceSettings.ScheduleInfoExternalScheduler si in detailsNewScheduler)
                {   
                    si.startTime = nextDate < DateTime.Now ? DateTime.Now : nextDate;
                }

                settings.scheduledSFDCJob.schedulerDetailsNewScheduler = detailsNewScheduler.ToArray();
            }
            settings.saveSettings(sp.Directory);
            settings.updateScheduleInExternalScheduler(sp, u);
        }

        public SalesforceSettings.ScheduleSFDCJob retrieveAndUpdateSchedulesFromExternalScheduler()
        {
            SalesforceSettings.ScheduleSFDCJob sfdcJob = null;
            if (Util.useExternalSchedulerExtract(sp))
            {
                sfdcJob = SchedulerUtils.getSFDCJob(sp, u);
                SalesforceSettings settings = getSettings();
                if (sfdcJob != null)
                {
                    settings.scheduledSFDCJob = sfdcJob;
                }
                else
                {
                    settings.scheduledSFDCJob = new SalesforceSettings.ScheduleSFDCJob();
                }
            }

            return sfdcJob;
        }

        public void applySchedulerVersionConfiguration(SalesforceSettings.ScheduleSFDCJob sfdcJob)
        {
            if (sfdcJob != null && sfdcJob.schedulerVersion != null 
                && sfdcJob.schedulerVersion.schedulerDisplayName != null && sfdcJob.schedulerVersion.schedulerDisplayName.Trim().Length > 0)
            {
                SchedulerVersion sVersion = sfdcJob.schedulerVersion;
                SchedulerVersionNameID.Text = "(" + sVersion.schedulerDisplayName + ")";
                SchedulerVersionNameID.ToolTip = "Schedules are currently configured in the release version " + sVersion.schedulerDisplayName + " ";
                if (sVersion.primaryScheduler)
                {
                    // If the scheduled sfdc job is on the same release of scheduler as the current Acorn release, nothing to do
                    return;
                }
                else
                {
                    ScheduleAddIDNewScheduler.Enabled = false;
                    ScheduleDeleteIDNewScheduler.Enabled = false;
                    ManualButton.Enabled = false;
                    ScheduleCustomButtonNewScheduler.Enabled = false;
                    ScheduleDailyButtonNewScheduler.Enabled = false;
                    ScheduleMonthlyButtonNewScheduler.Enabled = false;
                    string toolTipMessage = null;
                    MigrateSchedulesID.Visible = sVersion.enableMigration;
                    if (sVersion.enableMigration)
                    {                        
                        toolTipMessage = "Salesforce schedule setup was created using release version " + sVersion.schedulerDisplayName + " of Birst." +
                            " Please move all the schedules from " + sVersion.schedulerDisplayName + " to " + sVersion.primarySchedulerName;
                        MigrateLabelID.Text = "Move all the schedules from " + sVersion.schedulerDisplayName + " to " + sVersion.primarySchedulerName;
                    }
                    else
                    {
                        toolTipMessage = "Salesforce schedule setup was created using release version " + sVersion.schedulerDisplayName + " of Birst." +
                        " In order to modify this schedule, log in to Birst using " + sVersion.schedulerDisplayName + " or a newer release version. ";
                    }
                    
                    ScheduleAddIDNewScheduler.ToolTip = toolTipMessage;
                    ScheduleDeleteIDNewScheduler.ToolTip = toolTipMessage;
                    ManualButton.ToolTip = toolTipMessage;
                    ScheduleCustomButtonNewScheduler.ToolTip = toolTipMessage;
                    ScheduleDailyButtonNewScheduler.ToolTip = toolTipMessage;
                    ScheduleMonthlyButtonNewScheduler.ToolTip = toolTipMessage;
                }                
            }
        }

        protected void ScheduleListNewScheduler_SelectedIndexChanged(object sender, EventArgs e)
        {
            SalesforceSettings settings = getSettings();
            if (ScheduleListNewScheduler.SelectedIndex >= 0)
            {
                string scheduleId = ScheduleListNewScheduler.SelectedValue;
                setSelectedItemNewScheduler(scheduleId);
            }
        }

        private void setSelectedItemNewScheduler(string scheduleInfoId)
        {
            //SalesforceSettings settings = getSettings();
            SalesforceSettings.ScheduleInfoExternalScheduler si = SalesforceSettings.getScheduleInfoNewScheduler(getTempCopyOfScheduleDetailsNewScheduler(), scheduleInfoId);
            if (si != null)
            {
                foreach (ListItem li in ScheduleDayIDNewScheduler.Items)
                {
                    if (li.Text == si.Day)
                    {
                        ScheduleDayIDNewScheduler.SelectedIndex = ScheduleDayIDNewScheduler.Items.IndexOf(li);
                        break;
                    }
                }

                foreach (ListItem li in ScheduleHourIDNewScheduler.Items)
                {
                    if (li.Text == si.Hour.ToString())
                    {
                        ScheduleHourIDNewScheduler.SelectedIndex = ScheduleHourIDNewScheduler.Items.IndexOf(li);
                        break;
                    }

                }

                foreach (ListItem li in ScheduleMinuteIDNewScheduler.Items)
                {
                    if (si.Minute == 0)
                    {
                        ScheduleMinuteIDNewScheduler.SelectedIndex = 0;
                        break;
                    }

                    if (li.Text == si.Minute.ToString())
                    {
                        ScheduleMinuteIDNewScheduler.SelectedIndex = ScheduleMinuteIDNewScheduler.Items.IndexOf(li);
                        break;
                    }
                }

                foreach (ListItem li in ScheduleAMPMIDNewScheduler.Items)
                {
                    if (li.Text == si.AmPm)
                    {
                        ScheduleAMPMIDNewScheduler.SelectedIndex = ScheduleAMPMIDNewScheduler.Items.IndexOf(li);
                        break;
                    }
                }
            }

        }
    }
}
