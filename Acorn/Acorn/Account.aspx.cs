﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Data;
using System.Data.Odbc;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Acorn.Utils;
using Acorn.DBConnection;

namespace Acorn
{
    public partial class Account : LocalizedPage
    {
        private static string mainSchema = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["MainSchema"];

        private IDictionary<string, string> preferences;

        void Page_Init(object sender, EventArgs e)
        {
            ViewStateUserKey = Session.SessionID;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            Util.setUpHeaders(Response, false);
            User u = Util.validateAuth(Session, Response);
            if (u.isFreeTrialUser)
            {
                Global.systemLog.Warn("redirecting to home page due to being a free trial user (Account.aspx)");
                Response.Redirect(Util.getHomePageURL(Session, Request));
            }

            if (Session["ssoAuthentication"] != null) // SSO should not be allowed to see the accounts page
            {
                // if this is an embedded sso session, don't let them go outside of the module they were originally sent to
                string ssoModule = (string)Session["birst.module"];
                if (ssoModule != null)
                {
                    Response.Redirect(Util.getLoginPageURL(Session));
                    return;
                }
                Response.Redirect(Util.getHomePageURL(Session, Request));
                return;
            }
            Util.setUpdatedHeader(Session, null, u, Master);
            SkinPlaceHolder.Visible = u.CanSkin;
            OpenIDLabel.Visible = false;

            if (IsPostBack)
                return;
            bindData(u, false);
        }

        public static string md5Hash(string password)
        {
            byte[] textBytes = null;
            if (password.Any(c => c > 255))
            {
                textBytes = System.Text.Encoding.UTF8.GetBytes(password);
            }
            else
            {
                textBytes = System.Text.Encoding.Default.GetBytes(password);
            }
            try
            {
                System.Security.Cryptography.MD5CryptoServiceProvider cryptHandler;
                cryptHandler = new System.Security.Cryptography.MD5CryptoServiceProvider();
                byte[] hash = cryptHandler.ComputeHash(textBytes);
                string ret = "";
                foreach (byte a in hash)
                {
                    if (a < 16)
                        ret += "0" + a.ToString("x");
                    else
                        ret += a.ToString("x");
                }
                return ret;
            }
            catch
            {
                return null;
            }
        }

        private void bindData(User u, bool edit)
        {
            DataTable dt = u.getDataSet();
            AccountDetails.DataSource = dt;
            if (edit)
                dt.Rows[0]["DefaultSpace"] = "";
            AccountDetails.DataBind();
            QueryConnection conn = null;
            try
            {
                conn = ConnectionPool.getConnection();
                AccountDetails details = Database.getAccountDetails(conn, mainSchema, u.ManagedAccountId, u);
                if (details != null && details.isRADIUS())
                {
                    ChangePWD.Enabled = false;
                }

                List<Product> plist = Database.getProducts(conn, mainSchema, u.ID);
                StringBuilder sb = new StringBuilder();
                foreach (Product p in plist)
                {
                    if (p.Type != Product.TYPE_EDITION && p.Description != null && p.Description.Length > 0)
                    {
                        if (sb.Length > 0)
                            sb.Append(", ");
                        sb.Append(p.Description);
                        if (p.Type == Product.TYPE_STORAGE && p.Quantity > 1)
                            sb.Append(" (" + p.Quantity + ")");
                    }
                }
                if (sb.Length > 0)
                {
                    OptionsLabel.Text = HttpUtility.HtmlEncode(sb.ToString());
                    OtherProductsPanel.Visible = true;
                }

                // fix up DefaultSpaceId
                if (u.DefaultSpace != null && (u.DefaultSpaceId == null || u.DefaultSpaceId == Guid.Empty))
                {
                    List<SpaceMembership> mlist = Database.getSpaces(conn, mainSchema, u, true);
                    foreach (SpaceMembership sm in mlist)
                    {
                        if (sm.Space.Name == u.DefaultSpace)
                        {
                            u.DefaultSpaceId = sm.Space.ID;
                            dt.Rows[0]["DefaultSpace"] = u.DefaultSpace + '/' + u.DefaultSpaceId;
                            break;
                        }
                    }
                }

                bindOpenIds(conn, mainSchema, u.ID);

                // Get any existing preferences
                List<string> preferenceKeys = new List<string>();
                preferenceKeys.Add("TimeZone");
                preferenceKeys.Add("Locale");
                preferences = ManagePreferences.getUserPreferences(null, u.ID, preferenceKeys);
                if (edit)
                {
                    // Gets a sorted list of all locales and loads control with values
                    List<Language> localeNames = Database.listLanguages(conn, mainSchema);
                    DropDownList languagesDropDown = (DropDownList)AccountDetails.FindControl("LanguageBox");
                    for (int i = 0; i < localeNames.Count; i++ )
                    {
                        ListItem item = new ListItem(localeNames[i].description, localeNames[i].code);
                        languagesDropDown.Items.Add(item);
                    }
                    // Set the currently selected locale, if any
                    string localeName = "en-US";
                    if (preferences["Locale"] != null)
                    {
                        localeName = preferences["Locale"];
                    }
                    for (int i = 0; i < localeNames.Count; i++)
                    {
                        if (localeName.Equals(localeNames[i].code, StringComparison.OrdinalIgnoreCase))
                        {
                            languagesDropDown.SelectedIndex = i;
                            break;
                        }
                    }

                    languagesDropDown.Enabled = true;
                    TextBox languageTemp = (TextBox)AccountDetails.FindControl("Language");
                    if (languageTemp != null)
                    {
                        languageTemp.Enabled = false;
                    }

                    // Gets a list of time zones and loads control
                    DropDownList timezoneDropDown = (DropDownList)AccountDetails.FindControl("TimeZoneBox");
                    timezoneDropDown = populateTimeZoneDropDown(timezoneDropDown);
                    // Set the currently selected timezone, if any
                    if (preferences["TimeZone"] != null)
                    {
                        int timeZoneIndex = TimeZoneUtils.getTimeZoneIndexById(preferences["TimeZone"]);
                        // Need to +1 over the 'Server Local Time' which is not in the baseline list of TZs
                        timezoneDropDown.SelectedIndex = timeZoneIndex + 1;
                    }
                    else
                    {
                        timezoneDropDown.SelectedIndex = -1;
                    }
                    timezoneDropDown.Enabled = true;
                    TextBox timezoneTemp = (TextBox)AccountDetails.FindControl("TimeZone");
                    if (timezoneTemp != null)
                    {
                        timezoneTemp.Enabled = false;
                    }
                }
                else
                {
                    // Format text display of locale
                    ((TextBox)AccountDetails.FindControl("Language")).Text = Database.getLanguageDescription(conn, mainSchema, preferences["Locale"]);
                    ((TextBox)AccountDetails.FindControl("Language")).Enabled = true;
                    DropDownList languagesTemp = (DropDownList)AccountDetails.FindControl("LanguageBox");
                    if (languagesTemp != null)
                    {
                        languagesTemp.Enabled = false;
                    }
                    // Format text display of timezone
                    if (preferences["TimeZone"] != null)
                    {
                        string timeZoneDisplayName = TimeZoneUtils.getTimeZoneDisplayName(preferences["TimeZone"]);
                        ((TextBox)AccountDetails.FindControl("TimeZone")).Text = timeZoneDisplayName;
                        ((TextBox)AccountDetails.FindControl("TimeZone")).Enabled = true;
                    }
                    DropDownList timezoneTemp = (DropDownList)AccountDetails.FindControl("TimeZoneBox");
                    if (timezoneTemp != null)
                    {
                        timezoneTemp.Enabled = false;
                    }
                }
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
        }

        private void bindOpenIds(QueryConnection conn, string mainSchema, Guid userId)
        {
            DataTable openIDdataTable = new DataTable();
            openIDdataTable.Columns.Add("OpenID");

            List<string> openids = Database.getOpenIDsForUserId(conn, mainSchema, userId);
            foreach (string openid in openids)
            {
                DataRow dr0 = openIDdataTable.NewRow();
                dr0[0] = openid;
                openIDdataTable.Rows.Add(dr0);
            }
            OpenIDs.DataSource = openIDdataTable;
            OpenIDs.DataBind();
        }

        protected void OpenIds_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "InsertNew")
            {
                TextBox box = OpenIDs.FooterRow.FindControl("AssociationID") as TextBox;
                string openId = box.Text;
                if (openId == null || openId.Length == 0)
                {
                    OpenIDLabel.Text = String.Format(Resources.UserSettingsLabels.OpenIDUrlValidLabel);                  
                    OpenIDLabel.Visible = true;
                    return;
                }
                QueryConnection conn = null;
                try
                {
                    User u = (User)Session["user"];
                    conn = ConnectionPool.getConnection();
                    if (!Database.addOpenIDForUserId(conn, mainSchema, u.ID, openId))
                    {
                        OpenIDLabel.Text = String.Format(Resources.UserSettingsLabels.OpenIDAlreadyUseLabel);                       
                        OpenIDLabel.Visible = true;
                        return;
                    }
                    bindOpenIds(conn, mainSchema, u.ID);
                }
                finally
                {
                    ConnectionPool.releaseConnection(conn);
                }
            }
            else if (e.CommandName == "InsertEmpty")
            {
                GridViewRow row = (GridViewRow)((e.CommandSource as Button).NamingContainer);
                string openId = (row.FindControl("EmptyAssociationID") as TextBox).Text;
                if (openId == null || openId.Length == 0)
                {
                    OpenIDLabel.Text = String.Format(Resources.UserSettingsLabels.OpenIDValidUrlLabel);  
                    OpenIDLabel.Visible = true;
                    return;
                }
                QueryConnection conn = null;
                try
                {
                    User u = (User)Session["user"];
                    conn = ConnectionPool.getConnection();
                    if (!Database.addOpenIDForUserId(conn, mainSchema, u.ID, openId))
                    {
                        OpenIDLabel.Text = String.Format(Resources.UserSettingsLabels.OpenIDAlreadyUseLabel);   
                        OpenIDLabel.Visible = true;
                        return;
                    }
                    bindOpenIds(conn, mainSchema, u.ID);
                }
                finally
                {
                    ConnectionPool.releaseConnection(conn);
                }
            }
            else if (e.CommandName == "Delete")
            {
                int index = Convert.ToInt32(e.CommandArgument);
                GridViewRow row = OpenIDs.Rows[index];
                string openId = row.Cells[2].Text;
                QueryConnection conn = null;
                try
                {
                    User u = (User)Session["user"];
                    conn = ConnectionPool.getConnection();
                    Database.removeOpenIDForUserId(conn, mainSchema, u.ID, openId);
                    bindOpenIds(conn, mainSchema, u.ID);
                }
                finally
                {
                    ConnectionPool.releaseConnection(conn);
                }
            }
        }

        protected void OpenIds_OnRowDeleting(object sender, Object e)
        {
        }

        protected void AccountDetails_ModeChanging(object sender, DetailsViewModeEventArgs e)
        {
            User u = (User)Session["user"];
            AccountDetails.ChangeMode(e.NewMode);
            bindData(u, true);
            if (e.NewMode == DetailsViewMode.Edit)
            {
                QueryConnection conn = null;
                try
                {
                    conn = ConnectionPool.getConnection();
                    DropDownList ddl = (DropDownList)AccountDetails.FindControl("SpaceDropDown");
                    List<SpaceMembership> mlist = Database.getSpaces(conn, mainSchema, u, true);
                    ddl.Items.Clear();
                    ddl.Items.Add("");
                    foreach (SpaceMembership sm in mlist)
                    {
                        ddl.Items.Add(sm.Space.Name + '/' + sm.Space.ID);
                    }
                    ddl.Text = u.DefaultSpace + '/' + u.DefaultSpaceId;
                }
                finally
                {
                    ConnectionPool.releaseConnection(conn);
                }
            }
        }

        protected void AccountDetails_ItemUpdating(object sender, DetailsViewUpdateEventArgs e)
        {
            User u = (User)Session["user"];
            u.Email = ((TextBox)AccountDetails.FindControl("EmailTextBox")).Text;
            string temp = ((DropDownList)AccountDetails.FindControl("SpaceDropDown")).Text;
            if (temp.Length == 0)
            {
                u.DefaultSpace = null;
                u.DefaultSpaceId = Guid.Empty;
            }
            else
            {
                // split on last '/'
                int pos = temp.LastIndexOf('/');
                u.DefaultSpace = temp.Substring(0, pos);
                u.DefaultSpaceId = new Guid(temp.Substring(pos + 1));
            }
            u.DefaultDashboards = ((CheckBox)AccountDetails.FindControl("DashboardBox")).Checked;
            QueryConnection conn = null;
            try
            {
                conn = ConnectionPool.getConnection();
                Database.updateUser(conn, mainSchema, u);
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }

            // Update preference fields
            DropDownList languageDropDown = (DropDownList)AccountDetails.FindControl("LanguageBox");
            string localeCode = languageDropDown.SelectedValue;
            UserPreference localePreference 
                = new UserPreference(UserPreference.Type.User, "Locale", localeCode);
            DropDownList timezoneDropDown = (DropDownList)AccountDetails.FindControl("TimeZoneBox");
            string timezoneName = timezoneDropDown.Text;
            UserPreference timezonePreference 
                = new UserPreference(UserPreference.Type.User, "TimeZone", timezoneName);
            List<UserPreference> preferences 
                = new List<UserPreference>(new UserPreference[]{localePreference});
            ManagePreferences.updateUserPreferences(null, u.ID, Guid.Empty, preferences);            
            Util.markSMIWebSessionToLogout(Session);
            Util.removeSessionObject(Session, "Locale");
            AccountDetails.ChangeMode(DetailsViewMode.ReadOnly);
            bindData(u, false);
        }

        private int getLanguageIndex(string languageCode)
        {
            DropDownList languageDropDown = (DropDownList)AccountDetails.FindControl("LanguageBox");
            int i = 0;
            foreach (ListItem item in languageDropDown.Items)
            {
                if (item.Text.Equals(languageCode))
                {
                    return i;
                }
                i++;
            }
            return i;
        }

        private DropDownList populateTimeZoneDropDown(DropDownList dropDownList)
        {
            List<TimeZoneInfo> sysTZ = TimeZoneUtils.getSystemTimeZones();
            string displayName = Performance_Optimizer_Administration.Util.DISPLAY_TIMEZONE_DEFAULT;
            TimeZoneInfo defaultTZ = TimeZoneInfo.CreateCustomTimeZone(displayName, new TimeSpan(), displayName, displayName);
            sysTZ.Insert(0, defaultTZ);
            dropDownList.Items.Clear();
            foreach (TimeZoneInfo zone in sysTZ)
            {
                dropDownList.Items.Add(new ListItem(zone.DisplayName, zone.Id));
            }
            return dropDownList;
        }
    }
}
