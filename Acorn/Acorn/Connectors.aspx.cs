﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace Acorn
{
    public partial class Connectors : LocalizedPage
    {
        void Page_Init(object sender, EventArgs e)
        {
            ViewStateUserKey = Session.SessionID;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            Util.needToChangePassword(Response, Session);
            Util.setUpHeaders(Response);

            User u = Util.validateAuth(Session, Response);
            Space sp = Util.validateSpace(Session, Response, Request);

            if (u.isFreeTrialUser)
            {
                Global.systemLog.Warn("redirecting to home page due to being a free trial user (Connectors.aspx)");
                Response.Redirect(Util.getHomePageURL(Session, Request));
            }
            if (!Util.isSpaceAdmin(u, sp, Session))
            {
                Global.systemLog.Warn("redirecting to home page due to not being the space admin (Connectors.aspx)");
                Response.Redirect(Util.getHomePageURL(Session, Request));
            }

            GoogleAnalyticsButton.Enabled = !u.isFreeTrialUser;
            GoogleAnalyticsLink.Enabled = !u.isFreeTrialUser;

            Util.setUpdatedHeader(Session, sp, u, Master);
        }
    }
}
