﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Odbc;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Web.SessionState;
using Acorn.DBConnection;

namespace Acorn
{
    public class ManagePreferences
    {
        private static string mainSchema = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["MainSchema"];

        private static UserPreference.Type[] USER_PRECEDENCE = {
            UserPreference.Type.User,
            UserPreference.Type.Global,
        };
        private static UserPreference.Type[] USER_SPACE_PRECEDENCE = {
            UserPreference.Type.UserSpace,
            UserPreference.Type.User,
            UserPreference.Type.Global,
        };

        public const String PREFERNCE_OPTOUT_VISUALIZER_BANNER = "OptOutVisualizerBanner";
        public const String PREFERNCE_VISUALIZER_HELP = "VisualizerHelp";

        /// <summary>
        /// Updates the set of key/value preference pairs at the specified level.
        /// </summary>
        /// <param name="connectionString">Allows a unit test to override the connection string normally retrieved from 
        /// the Web.config file. Pass 'null' if the "DatabaseConnection" value from Web.config is what is desired.
        /// </param>
        /// <param name="type">'int' value of the type of user preference to add. See UserPreference.Type for values.</param>
        /// <param name="userID">The Guid representation of the user ID. Can be Guid.EMPTY.</param>
        /// <param name="spaceID">The Guid representation of the space ID Can be Guid.EMPTY.</param>
        /// <param name="keyValuePairs">The key/value pairs to be added or updated. All keys must be non-null, non-empty.</param>
        public static void updateUserPreferences(string connectionString,
                                                 Guid userID,
                                                 Guid spaceID,
                                                 List<UserPreference> userPreferences)
        {
            QueryConnection conn = getDatabaseConnection(connectionString);

            foreach (UserPreference userPreference in userPreferences)
            {
                // Check that inputs are valid
                if (invalidPreferenceInputs(userID, spaceID, userPreference))
                {
                    Global.systemLog.Warn("Cannot process 'updateUserPreferences()'. Invalid input.");
                    ConnectionPool.releaseConnection(conn);            
                    return;
                }
                Database.updateUserPreferences(conn, mainSchema, userID, spaceID, userPreference);
            }
            ConnectionPool.releaseConnection(conn);            
        }

        /// <summary>
        /// Gets a specified subset of global preferences. Only searches the global level of preferences.
        /// </summary>
        /// <param name="connectionString">Allows a unit test to override the connection string normally retrieved from 
        /// the Web.config file. Pass 'null' if the "DatabaseConnection" value from Web.config is what is desired.
        /// </param>
        /// <param name="preferenceKeys">is the set of global keys to retrieve.</param>
        /// <returns>a dictionary of key/value pairs. If the 'key' was not found, the corresponding 'value' will be null.</returns>
        public static IDictionary<string, string> getGlobalPreferences(string connectionString, List<string> preferenceKeys)
        {
            IDictionary<string, string> globalPreferences = null;
            if (requestingAllPreferences(preferenceKeys))
            {
                globalPreferences = getAllGlobalPreferences(connectionString);
            }
            else
            {
                QueryConnection conn = getDatabaseConnection(connectionString);
                globalPreferences = Database.getUserPreferences(conn, mainSchema, preferenceKeys);
                ConnectionPool.releaseConnection(conn);
            }

            return globalPreferences;
        }

        /// <summary>
        /// Gets all global preferences. Only searches the global level of preferences.
        /// </summary>
        /// <param name="connectionString">Allows a unit test to override the connection string normally retrieved from 
        /// the Web.config file. Pass 'null' if the "DatabaseConnection" value from Web.config is what is desired.
        /// </param>
        /// <returns>a dictionary of all global key/value preference pairs.</returns>
        public static IDictionary<string, string> getAllGlobalPreferences(string connectionString)
        {
            QueryConnection conn = null;
            try
            {
                conn = getDatabaseConnection(connectionString);

                IDictionary<string, string> allGlobalPreferences = Database.getUserPreferences(conn, mainSchema);

                // No need to filter globals
                return allGlobalPreferences;
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
        }

        public static bool isUserPreferenceEnabled(User u, string preferenceKey, bool defaultVal)
        {
            IDictionary<string,string> values = getUserPreference(null, u.ID, preferenceKey);
            if (values != null && values.ContainsKey(preferenceKey))
            {
                string value = values[preferenceKey];
                bool result = false;
                if(bool.TryParse(value, out result))
                {
                    return result;
                }
            }
            return defaultVal;
        }

        /// <summary>
        /// Get a user-level preference for a user. Only preferences at the user level are searched.
        /// If there is no preference for the specified user, then a 'null' value is returned.
        /// </summary>
        /// <param name="connectionString">Allows a unit test to override the connection string normally retrieved from 
        /// the Web.config file. Pass 'null' if the "DatabaseConnection" value from Web.config is what is desired.
        /// </param>
        /// <param name="userID">is the Guid identifier for a user</param>
        /// <param name="preferenceKey">is the key of the user-level preference to retrieve.</param>
        /// <returns>a dictionary of the specified set of key/value preference pairs for the specified 'userID'.
        /// If there is no corresponding preference, then 'null' is returned for the key's 'value'.</returns>
        public static IDictionary<string, string> getUserPreference(string connectionString,
                                                                    Guid userID,
                                                                    string preferenceKey)
        {
            List<string> preferenceKeys = new List<string>();
            preferenceKeys.Add(preferenceKey);
            return getUserPreferences(connectionString, userID, preferenceKeys);
        }

        /// <summary>
        /// Get a subset of preferences for a user. Only preferences at the user level are searched.
        /// If there is no preference for the specified user, then a 'null' value is returned.
        /// </summary>
        /// <param name="connectionString">Allows a unit test to override the connection string normally retrieved from 
        /// the Web.config file. Pass 'null' if the "DatabaseConnection" value from Web.config is what is desired.
        /// </param>
        /// <param name="userID">is the Guid identifier for a user</param>
        /// <param name="preferenceKeys">is the list of preference keys to retrieve.</param>
        /// <returns>a dictionary of the specified set of key/value preference pairs for the specified 'userID'.
        /// If there is no corresponding preference, then 'null' is returned for the key's 'value'.</returns>
        public static IDictionary<string, string> getUserPreferences(string connectionString,
                                                                     Guid userID,
                                                                     List<string> preferenceKeys)
        {
            QueryConnection conn = null;
            try
            {
                conn = getDatabaseConnection(connectionString);
                // Retrieve the user-level preferences
                IDictionary<string, string> preferences = Database.getUserPreferences(conn, mainSchema, userID, preferenceKeys);
                return preferences;
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
        }

        /// <summary>
        /// Get all preferences for a user.
        /// </summary>
        /// <param name="connectionString">Allows a unit test to override the connection string normally retrieved from 
        /// the Web.config file. Pass 'null' if the "DatabaseConnection" value from Web.config is what is desired.
        /// </param>
        /// <param name="userID">is the Guid identifier for a user</param>
        /// <returns>a dictionary of all key/value preference pairs for the specified 'userID'.</returns>
        public static IDictionary<string, string> getAllUserPreferences(string connectionString, Guid userID)
        {
            QueryConnection conn = null;
            try
            {
                conn = getDatabaseConnection(connectionString);
                IDictionary<string, string> preferences = Database.getUserPreferences(conn, mainSchema, userID);
                return preferences;
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
        }

        /// <summary>
        /// Get a subset of preferences for a user and space combination.  Doesn't search
        /// user or global level preferences.  Only returns preferences from the user-space
        /// level.
        /// </summary>
        /// <param name="connectionString">Allows a unit test to override the connection string normally retrieved from 
        /// the Web.config file. Pass 'null' if the "DatabaseConnection" value from Web.config is what is desired.
        /// </param>
        /// <param name="userID">is the Guid identifier for a user</param>
        /// <param name="spaceID">is the Guid identifier for a user</param>
        /// <param name="preferenceKeys">is the list of preference keys to retrieve.</param>
        /// <returns>a dictionary of all key/value preference pairs for the specified 'userID' and 'spaceID'.</returns>
        public static IDictionary<string, string> getUserSpacePreferences(string connectionString,
                                                                          Guid userID,
                                                                          Guid spaceID,
                                                                          List<string> preferenceKeys)
        {
            // Are they requesting to retrieve "ALL" preferences?
            if (requestingAllPreferences(preferenceKeys))
            {
                return getAllUserSpacePreferences(connectionString, userID, spaceID);
            }

            QueryConnection conn = null;
            try
            {
                conn = getDatabaseConnection(connectionString);
                // UserSpace preferences, then User preferences, then globals
                IDictionary<string, string> preferences = Database.getUserPreferences(conn, mainSchema, userID, spaceID, preferenceKeys);
                return preferences;
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
        }

        /// <summary>
        /// Get all preferences for a user and space combination.
        /// </summary>
        /// <param name="connectionString">Allows a unit test to override the connection string normally retrieved from 
        /// the Web.config file. Pass 'null' if the "DatabaseConnection" value from Web.config is what is desired.
        /// </param>
        /// <param name="userID">is the Guid identifier for a user</param>
        /// <param name="spaceID">is the Guid identifier for a user</param>
        /// <returns>a dictionary of all key/value preference pairs for the specified 'userID' and 'spaceID'.</returns>
        public static IDictionary<string, string> getAllUserSpacePreferences(string connectionString,
                                                                             Guid userID,
                                                                             Guid spaceID)
        {
            QueryConnection conn = getDatabaseConnection(connectionString);

            IDictionary<string, string>[] allPreferences = new Dictionary<string, string>[3];
            // UserSpace preferences, then User preferences, then globals
            allPreferences[0] = Database.getUserPreferences(conn, mainSchema, userID, spaceID);
            allPreferences[1] = Database.getUserPreferences(conn, mainSchema, userID);
            allPreferences[2] = Database.getUserPreferences(conn, mainSchema);
            ConnectionPool.releaseConnection(conn);

            // Filter out preferences using precedence of UserSpace over User over Global
            IDictionary<string, string> filteredPreferences = mergePreferences(allPreferences);

            return filteredPreferences;
        }

        /// <summary>
        /// Get a user-level preference. For the specified preference, this method searches 
        /// for the user-level preference first, then looks for global-level preference.  If 
        /// neither exists, then it returns 'null' for that preference.
        /// </summary>
        /// <param name="connectionString">Allows a unit test to override the connection string normally retrieved from 
        /// the Web.config file. Pass 'null' if the "DatabaseConnection" value from Web.config is what is desired.
        /// </param>
        /// <param name="userID">is the Guid identifier for a user</param>
        /// <param name="preferenceKeys">is the list of preference keys to retrieve.</param>
        /// <returns>a dictionary of the specified set of key/value preference pairs for the specified 'userID'.
        /// If there is no corresponding preference, then 'null' is returned for the key's 'value'.</returns>
        public static IDictionary<string, string> getPreference(string connectionString,
                                                                Guid userID,
                                                                string preferenceKey)
        {
            List<string> preferenceKeys = new List<string>();
            preferenceKeys.Add(preferenceKey);
            return getPreferences(connectionString, userID, preferenceKeys);
        }

        /// <summary>
        /// Get a subset of preferences for a user. For each specified preference, this method searches 
        /// for the user-level preferences first, then looks for global-level preferences.  If neither
        /// exists, then it returns 'null' for that preference.
        /// </summary>
        /// <param name="connectionString">Allows a unit test to override the connection string normally retrieved from 
        /// the Web.config file. Pass 'null' if the "DatabaseConnection" value from Web.config is what is desired.
        /// </param>
        /// <param name="userID">is the Guid identifier for a user</param>
        /// <param name="preferenceKeys">is the list of preference keys to retrieve.</param>
        /// <returns>a dictionary of the specified set of key/value preference pairs for the specified 'userID'.
        /// If there is no corresponding preference, then 'null' is returned for the key's 'value'.</returns>
        public static IDictionary<string, string> getPreferences(string connectionString,
                                                                 Guid userID,
                                                                 List<string> preferenceKeys)
        {
            if (requestingAllPreferences(preferenceKeys))
            {
                return getAllUserPreferences(connectionString, userID);
            }
            QueryConnection conn = getDatabaseConnection(connectionString);

            IDictionary<string, string>[] allPreferences = new Dictionary<string, string>[2];
            // Retrieve the user, then global-level preferences
            allPreferences[0] = Database.getUserPreferences(conn, mainSchema, userID, preferenceKeys);
            allPreferences[1] = Database.getUserPreferences(conn, mainSchema, preferenceKeys);
            ConnectionPool.releaseConnection(conn);
            
            // Filter out preferences using precedence of UserSpace over User over Global
            IDictionary<string, string> filteredPreferences = filterPreferences(preferenceKeys, allPreferences);

            return filteredPreferences;
        }

        /// <summary>
        /// Get a preference for a user and space combination.  If the preference does not exist at the
        /// user/space level, then the user-level preference is queried.  If that does not exist, then 
        /// the global-level preference with that key is queried.  A IDictionary<string, string> with 
        /// the requested key is always returned, but the value may be 'null' if it is not found at 
        /// any of the three levels.
        /// </summary>
        /// <param name="connectionString">Allows a unit test to override the connection string normally retrieved from 
        /// the Web.config file. Pass 'null' if the "DatabaseConnection" value from Web.config is what is desired.
        /// </param>
        /// <param name="userID">is the Guid identifier for a user</param>
        /// <param name="spaceID">is the Guid identifier for a user</param>
        /// <param name="preferenceKey">is the key of the preference to retrieve.</param>
        /// <returns>a dictionary of all key/value preference pairs for the specified 'userID' and 'spaceID'.</returns>
        public static IDictionary<string, string> getPreference(string connectionString,
                                                                Guid userID,
                                                                Guid spaceID,
                                                                string preferenceKey)
        {
            List<string> preferenceKeys = new List<string>();
            preferenceKeys.Add(preferenceKey);
            return getPreferences(connectionString, userID, spaceID, preferenceKeys);
        }

        /// <summary>
        /// Get a subset of preferences for a user and space combination.
        /// </summary>
        /// <param name="connectionString">Allows a unit test to override the connection string normally retrieved from 
        /// the Web.config file. Pass 'null' if the "DatabaseConnection" value from Web.config is what is desired.
        /// </param>
        /// <param name="userID">is the Guid identifier for a user</param>
        /// <param name="spaceID">is the Guid identifier for a user</param>
        /// <param name="preferenceKeys">is the list of preference keys to retrieve.</param>
        /// <returns>a dictionary of all key/value preference pairs for the specified 'userID' and 'spaceID'.</returns>
        public static IDictionary<string, string> getPreferences(string connectionString,
                                                                 Guid userID,
                                                                 Guid spaceID,
                                                                 List<string> preferenceKeys)
        {
            // Are they requesting to retrieve "ALL" preferences?
            if (requestingAllPreferences(preferenceKeys))
            {
                return getAllUserSpacePreferences(connectionString, userID, spaceID);
            }

            QueryConnection conn = getDatabaseConnection(connectionString);

            IDictionary<string, string>[] allPreferences = new Dictionary<string, string>[3];
            // UserSpace preferences, then User preferences, then globals
            allPreferences[0] = Database.getUserPreferences(conn, mainSchema, userID, spaceID, preferenceKeys);
            allPreferences[1] = Database.getUserPreferences(conn, mainSchema, userID, preferenceKeys);
            allPreferences[2] = Database.getUserPreferences(conn, mainSchema, preferenceKeys);
            ConnectionPool.releaseConnection(conn);

            // Filter out preferences using precedence of UserSpace over User over Global
            IDictionary<string, string> filteredPreferences = filterPreferences(preferenceKeys, allPreferences);

            return filteredPreferences;
        }

        /* ----------------------------------------------------------------------------------------
         * ----------------------------------------------------------------------------------------
         * ---  Helper methods                                                                  ---
         * ----------------------------------------------------------------------------------------
         * ---------------------------------------------------------------------------------------- */


        /// <summary>
        /// Used in the cases where a subset of the preferences are requested, in other words, when 
        /// the caller specifies a set of preferences to be retrieved.  In this case, only the 
        /// preferences requested are retrieved.  So, no attempt to merge these preferences with
        /// other preferences is attempted.
        /// 'preferences[0]' is always the most specific set of preferences (e.g., user-space) and
        /// the last (e.g., 'preferences[2]') is the least specific (e.g., global).  The 'preferences'
        /// are processed in reverse sequence; i.e., the least specific is queried to see if it contains
        /// any of the requested preferences, then more specific 'preferences' occurrence until the 
        /// most specific 'preferences' is processed.  In this way the most specific preferences are
        /// always returned to the requester.
        /// </summary>
        /// <param name="preferenceKeys">Is the list of keys that are being requested.</param>
        /// <param name="preferences">
        /// Is the array of key/value pairs that are returned from the global, user and user-space 
        /// queries, in that order.
        /// </param>
        /// <returns>
        /// A set of key/value pairs that contain the most specific (i.e., user-space more specific than 
        /// user more specific than global) user preference for each preference that was requested.  If 
        /// the preference did not exist, then the value of that key/value pair is null.
        /// </returns>
        private static IDictionary<string, string> filterPreferences(List<string> preferenceKeys, 
                                                                     IDictionary<string, string>[] preferences)
        {
            IDictionary<string, string> keyValuePairs = new Dictionary<string, string>();
            foreach (string preferenceKey in preferenceKeys)
            {
                bool found = false;
                foreach (IDictionary<string, string> levelResults in preferences)
                {
                    if (levelResults.ContainsKey(preferenceKey) && levelResults[preferenceKey] != null)
                    {
                        keyValuePairs.Add(preferenceKey, levelResults[preferenceKey]);
                        found = true;
                        break;
                    }
                }
                // Add with null value if not found
                if (!found)
                {
                    keyValuePairs.Add(preferenceKey, null);
                }

            }
            return keyValuePairs;
        }

        /// <summary>
        /// This method takes all three -- user-space, user & global -- preference key/value pairs and
        /// merges them to give the requester all the preferences with the most specific value for each 
        /// preference.
        /// The method merges by starting with the global preferences and adding or replacing preferences
        /// from the user preferences key/value pairs and then from the user-space key/value pairs.
        /// </summary>
        /// <param name="preferences">
        /// Is the array of key/value pairs that are returned from the
        /// global, user and user-space queries, in that order.
        /// </param>
        /// <returns>
        /// A set of all key/value pairs that contain the most specific (i.e., user-space more
        /// specific than user more specific than global) user preference for each preference.
        /// </returns>
        private static IDictionary<string, string> mergePreferences(IDictionary<string, string>[] preferences)
        {
            int lastEntry = preferences.Length - 1;
            IDictionary<string, string> mergedPreferences = preferences[lastEntry];

            // Start with user, then process user-space last
            for (int i = lastEntry - 1; i >= 0; i--)
            {
                ICollection<string> currentKeys = mergedPreferences.Keys;
                foreach (string key in currentKeys)
                {
                    if (mergedPreferences.ContainsKey(key))
                    {
                        // Do an update of an existing value
                        mergedPreferences[key] = preferences[i][key];
                    }
                    else
                    {
                        // Otherwise add a new key/value pair
                        mergedPreferences.Add(key, preferences[i][key]);
                    }
                }
            }

            return mergedPreferences;
        }

        /// <summary>
        /// Examines the list of preference keys and determines if a request for "ALL" 
        /// preferences has been made.
        /// </summary>
        /// <param name="preferenceKeys">is the list of preference keys to retrieve.</param>
        /// <returns>True if requester wants all preferences returned; false otherwise.</returns>
        private static bool requestingAllPreferences(List<string> preferenceKeys)
        {
            // Are they requesting to retrieve "ALL" preferences?
            bool requestingAll = false;
            foreach (String key in preferenceKeys) 
            {
                if (key.ToUpper().Equals("ALL"))
                {
                    requestingAll = true;
                    break;
                }
            }
            return requestingAll;
        }

        /// <summary>
        /// Checks all the inputs for validity.
        /// </summary>
        /// <param name="type">The type of preference to be added.</param>
        /// <param name="userID">The Guid representation of the user id.</param>
        /// <param name="spaceID">The Guid representation of the space id.</param>
        /// <param name="key">The key of the preference key/value pair being updated.</param>
        /// <returns></returns>
        private static bool invalidPreferenceInputs(Guid userID,
                                                    Guid spaceID,
                                                    UserPreference userPreference)
        {
            bool isInvalid = false;
            // 'key' cannot be null or empty
            if (userPreference.Key == null || userPreference.Key.Length == 0)
            {
                Global.systemLog.Warn("Key should always be non-null/non-empty when updating preferences.");
                isInvalid = true;
            }
            if (userPreference.PreferenceType.Equals(UserPreference.Type.User))
            {
                if (userID == null || userID.Equals(Guid.Empty))
                {
                    Global.systemLog.Warn("User ID cannot be empty for 'User'-level preference.");
                    isInvalid = true;
                }
            }
            if (userPreference.PreferenceType.Equals(UserPreference.Type.UserSpace))
            {
                if (userID == null || userID.Equals(Guid.Empty))
                {
                    Global.systemLog.Warn("User ID cannot be empty for 'User'-level preference.");
                    isInvalid = true;
                }
                if (spaceID == null || spaceID.Equals(Guid.Empty))
                {
                    Global.systemLog.Warn("Space ID cannot be empty for 'UserSpace'-level preference.");
                    isInvalid = true;
                }
            }
            return isInvalid;
        }

        private static QueryConnection getDatabaseConnection(string connectionString)
        {
            QueryConnection connection;
            if (connectionString == null)
            {
                connection = ConnectionPool.getConnection();
            }
            else
            {
                // Mainly useful for unit testing when needing an overrider for the connection string
                connection = ConnectionPool.getConnection(connectionString);
            }
            return connection;
        }
    }
}
