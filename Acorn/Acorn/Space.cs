﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Collections.Generic;
using Acorn.DBConnection;

namespace Acorn
{
    public class Space
    {
        public static int NEW_SPACE_TYPE = 0;
        public static int NEW_SPACE_PROCESSVERSION_ID = 1;
        public static int NEW_SPACE_SFDCVERSION_ID = -1;

        public const int SPACE_MODE_AUTOMATIC = 1;
        public const int SPACE_MODE_ADVANCED = 2;
        public const int SPACE_MODE_DISCOVERY = 3; 

        private static string SPACE_SETTINGS = "spacesettings.xml";

        public Guid ID;
        public string Name;
        public string Comments;
        public string Directory;
        public string Schema;
        public bool Automatic;
        public int LoadNumber;
        public string SourceTemplateName;
        public int Type;
        public string ConnectString;
        public string DatabaseType;
        public string DatabaseDriver;
        public string DatabaseLoadDir;
        public string AdminUser;
        public string AdminPwd;
        public bool Active;
        public bool Folded;
        public bool TestMode;
        public bool Locked;
        public bool Demo;
        public bool SSO;
        public string SSOPassword;
        public SpaceSettings Settings;
        // Query database settings
        public string QueryConnectString;
        public string QueryDatabaseType;
        public string QueryDatabaseDriver;
        public string QueryUser;
        public string QueryPwd;
        public string QueryConnectionName;

        public int MaxQueryRows;
        public int MaxQueryTimeout;

        public int QueryLanguageVersion;

        public bool UsageTracking;

        public int ProcessVersionID;

        public int AppVersion;

        // controls whether dynamic group query needs to be executed for accessing
        // adhoc level permissions
        public bool UseDynamicGroups;

        public bool AllowRetryFailedLoad;
        public string ScheduleFrom;
        public string ScheduleSubject;

        // point to the performance engine version for sfdc extraction.
        // Since extraction is moved to performance engine, to avoid flexibility
        // we are disjoining between PROCESS engine and EXTRACT engine. This property relates to EXTRACT engine
        public int SFDCVersionID;
        // This is granular control on the space. True means use the old way. Null or false means 
        public bool DisallowExternalScheduler;

        // time stamp when group/user/acl association is changed for a space
       // public DateTime MappingModifiedDate;

        // number of parallel threads for sfdc extraction through external scheduler
        // default is set at the scheduler server level
        public int SFDCNumThreads;
        public bool DiscoveryMode;

        public bool MapNullsToZero;

        public string awsAccessKeyId;
        public string awsSecretKey;

        public string dbTimeZone;

        // Container for all the groups
        public List<SpaceGroup> SpaceGroups;
        
        public Space(User u, int spaceType, int processId, int sfdcId, string contentDirectory, string schemaName)
        {
            SpaceConfig sc = Util.getSpaceConfiguration(spaceType);

            ID = Guid.NewGuid();
            Directory = Path.Combine(contentDirectory, ID.ToString());
            Type = spaceType;
            ProcessVersionID = processId;
            SFDCVersionID = sfdcId;
            string schemaname = "";
            string dbType = u.DatabaseType != null ? u.DatabaseType : sc.DatabaseType;
            if (dbType == null || dbType.Trim().Length == 0)
            {
                dbType = sc.DatabaseType;
            }
            if (schemaName != null && schemaName.Length > 0)
            {
                if (dbType != null && dbType.Equals(Database.ORACLE))
                {
                    schemaName = schemaName.ToUpper();
                }
                schemaname = schemaName;
            }
            else
            {
                if (dbType != null && dbType.Equals(Database.ORACLE))
                {
                    schemaname = "S_" + Performance_Optimizer_Administration.Util.generatePhysicalName(DateTime.Now.ToString("yyyyMMddHHmmssffffffzz"));
                    schemaname = schemaname.ToUpper();
                }
                else
                {
                    schemaname = "S_" + Performance_Optimizer_Administration.Util.generatePhysicalName(ID.ToString());
                }
            }
            Schema = schemaname;

            awsAccessKeyId = sc.awsAccessKeyId;
            awsSecretKey = sc.awsSecretKey;            
        }

        public Space()
        {
        }

        public void setDatabaseLoadDir(string databaseLoadDirectory)
        {
            DatabaseLoadDir = Path.Combine(databaseLoadDirectory, ID.ToString());
        }

        public string getFullConnectString()
        {
            return getFullConnectString(ConnectString, AdminUser, AdminPwd);
        }

        public string getFullConnectString(string ConnectString, string AdminUser, string AdminPwd)
        {
            return getFullConnectString(ConnectString, AdminUser, AdminPwd, DatabaseType);   
        }

        public static string getFullConnectString(string ConnectString, string AdminUser, string AdminPwd, string DatabaseType)
        {
            string connectStr = "";
            string temp = ConnectString.ToLower();
            if (temp.StartsWith("jdbc:sqlserver://"))
            {
                if (DatabaseType == "MS SQL Server 2008 R2" || DatabaseType == "MS SQL Server 2012" || DatabaseType == "MS SQL Server 2014")
                    connectStr = "Server=" + ConnectString.Substring(17).Replace("databaseName", "Database") + ";Uid=" + AdminUser + ";Pwd=" + AdminPwd + ";Driver={SQL Server Native Client 10.0}";
                else
                    connectStr = "Server=" + ConnectString.Substring(17).Replace("databaseName", "Database") + ";Uid=" + AdminUser + ";Pwd=" + AdminPwd + ";Driver={SQL Native Client}";
                connectStr = connectStr.Replace(':', ',');
               
            }
            else if (temp.StartsWith("jdbc:mysql://"))
            {
                string server = ConnectString.Substring(13).Replace("/", "");
                int portindex = server.IndexOf(':');
                string port = "";
                if (portindex > 0)
                {
                    port = "port=" + server.Substring(portindex + 1) + ";";
                    server = server.Substring(0, portindex);
                }
                connectStr = "Server=" + server + ";Uid=" + AdminUser + ";Password=" + AdminPwd + ";Driver={MySQL ODBC 5.1 Driver};" + port;
            }
            else if (temp.StartsWith("jdbc:oracle:thin:@"))
            {
                // jdbc:oracle:thin:@host:port:sid
                // jdbc:oracle:thin:@//host:port/service
                int atPos = ConnectString.IndexOf('@');
                if (atPos != 17)
                    throw new Exception("Bad oracle connection string: " + ConnectString);
                string dbq = ConnectString.Substring(atPos + 1);
                string host = null;
                if (dbq.StartsWith("//"))
                {
                    // new form
                    //  jdbc:oracle:thin:@//host:port/service
                    //  Driver={Oracle in OraClient_11g_home1}SERVER=host;DBQ=host:port/service;Uid=...;Pwd=...;
                    dbq = dbq.Substring(2);
                    int pos = dbq.IndexOf('/');
                    if (pos < 0)
                        throw new Exception("Bad Oracle connection string: " + ConnectString);
                    host = dbq.Substring(0, pos);
                    string service = dbq.Substring(pos + 1);
                    dbq = host + '/' + service; // DBQ should have port, host should not
                    int pos2 = host.IndexOf(':');
                    if (pos2 > 0)
                        host = host.Substring(0, pos2);
                }
                else
                {
                    // old form
                    //  jdbc:oracle:thin:@host:port:sid
                    //  Driver={Oracle in OraClient_11g_home1}SERVER=host;DBQ=host:port/sid;Uid=...;Pwd=...; /<=== can't get this to connect
                    int pos = dbq.LastIndexOf(':');
                    if (pos < 0)
                        throw new Exception("Bad Oracle connection string: " + ConnectString);
                    host = dbq.Substring(0, pos);
                    string sid = dbq.Substring(pos + 1);
                    dbq = host + '/' + sid;

                    int pos2 = host.IndexOf(':');
                    if (pos2 > 0)
                        host = host.Substring(0, pos2);
                }
                connectStr = "Driver={" + QueryConnection.oracledrivername + "};SERVER=" + host + ";DBQ=" + dbq + ";Uid=" + AdminUser + ";Pwd=" + AdminPwd;
            }
            else if (temp.StartsWith("jdbc:paraccel://"))
            {
                string server = ConnectString.Substring("jdbc:paraccel://".Length);
                string database = "";
                if (server.IndexOf("/") >= 0)
                {
                    database = server.Substring(server.IndexOf("/") + 1);
                    if (database.IndexOf("?") >= 0)
                    {
                        database = database.Substring(0, database.IndexOf("?"));
                    }
                    server = server.Substring(0, server.IndexOf("/"));
                }
                int portindex = server.IndexOf(':');
                string port = "";
                if (portindex > 0)
                {
                    port = server.Substring(portindex + 1);
                    server = server.Substring(0, portindex);
                }
                connectStr = "HostName=" + server + ";PortNumber=" + port + ";Database=" + database + ";UID=" + AdminUser + ";Password=" + AdminPwd + ";Driver={ParAccel};";
            }
            else if (temp.StartsWith("jdbc:sap://"))
            {
                string server = ConnectString.Substring("jdbc:sap://".Length);
                connectStr = "ServerNode=" + server + ";UID=" + AdminUser + ";Pwd=" + AdminPwd + ";Driver={HDBODBC32};"; // HDBODBC
            }
            else if (temp.StartsWith("jdbc:postgresql://"))
            {
                string server = ConnectString.Substring("jdbc:postgresql://".Length);
                string database = "";
                if (server.IndexOf("/") >= 0)
                {
                    database = server.Substring(server.IndexOf("/") + 1);
                    if (database.IndexOf("?") >= 0)
                    {
                        database = database.Substring(0, database.IndexOf("?"));
                    }
                    server = server.Substring(0, server.IndexOf("/"));
                }
                int portindex = server.IndexOf(':');
                string port = "";
                if (portindex > 0)
                {
                    port = server.Substring(portindex + 1);
                    server = server.Substring(0, portindex);
                }
                string postgresDriverName = Util.getPostgresDriverName();
                if (postgresDriverName == null || postgresDriverName.Trim().Length == 0)
                {
                    postgresDriverName = "PostgreSQL Unicode";
                }

                string[] ver = Util.getPostGresDriverVersionSubset();
                if ((int.Parse(ver[0]) == 9 && int.Parse(ver[1]) > 1) || (int.Parse(ver[0]) >= 10 )) //for Postgre ODBC versions starting from 09.02.xxx
                {
                    connectStr = "Server=" + server + ";Port=" + port + ";Database=" + database + ";UID=" + AdminUser + ";Password={" + AdminPwd + "};Driver={" + postgresDriverName + "};";
                }
                else if ((int.Parse(ver[0]) == 9 && int.Parse(ver[1]) <= 1) || (int.Parse(ver[0]) < 9))    //for Postgre ODBC versions older than 09.02.xxx
                {
                    connectStr = "Server=" + server + ";Port=" + port + ";Database=" + database + ";UID=" + AdminUser + ";Password=" + AdminPwd + ";Driver={" + postgresDriverName + "};";
                }                
            }
            else
            {
                return ConnectString;
            }
            return (connectStr);
        }

        public string getQueryFullConnectString()
        {
            string connectStr = "";
            if (QueryDatabaseType == "Infobright")
            {
                string server = QueryConnectString.Substring(13).Replace("/", "");
                int portindex = server.IndexOf(':');
                string port = "";
                if (portindex > 0)
                {
                    port = "port=" + server.Substring(portindex + 1) + ";";
                    server = server.Substring(0, portindex);
                }
                connectStr = "Server=" + server + ";Uid=" + QueryUser + ";Password=" + QueryPwd + ";Driver={MySQL ODBC 5.1 Driver};" + port;
            }
            return (connectStr);
        }

        public string getSessionKey()
        {
            return (Schema);
        }

        public Space clone()
        {
            Space sp = new Space();
            sp.AdminPwd = AdminPwd;
            sp.AdminUser = AdminUser;
            sp.Automatic = Automatic;
            sp.Comments = Comments;
            sp.ConnectString = ConnectString;
            sp.DatabaseDriver = DatabaseDriver;
            sp.DatabaseType = DatabaseType;
            sp.DatabaseLoadDir = DatabaseLoadDir;
            sp.Directory = Directory;
            sp.ID = Guid.NewGuid();
            sp.Name = Name;
            sp.LoadNumber = LoadNumber;
            sp.Schema = Schema;
            sp.SourceTemplateName = SourceTemplateName;
            sp.Type = Type;
            sp.Active = Active;
            sp.Folded = Folded;
            sp.TestMode = TestMode;
            sp.Locked = Locked;
            sp.Demo = Demo;
            sp.SSO = SSO;
            sp.SSOPassword = SSOPassword;
            sp.Settings = Settings;
            sp.QueryConnectionName = QueryConnectionName;
            sp.QueryConnectString = QueryConnectString;
            sp.QueryDatabaseDriver = QueryDatabaseDriver;
            sp.QueryDatabaseType = QueryDatabaseType;
            sp.QueryPwd = QueryPwd;
            sp.QueryUser = QueryUser;

            sp.MaxQueryRows = MaxQueryRows;
            sp.MaxQueryTimeout = MaxQueryTimeout;
            sp.QueryLanguageVersion = QueryLanguageVersion;
            sp.SpaceGroups = SpaceGroups;

            sp.UsageTracking = UsageTracking;
            sp.AppVersion = AppVersion;
            sp.UseDynamicGroups = UseDynamicGroups;

            sp.AllowRetryFailedLoad = AllowRetryFailedLoad;
            sp.SFDCVersionID = SFDCVersionID;
            sp.DisallowExternalScheduler = DisallowExternalScheduler;
            sp.SFDCNumThreads = SFDCNumThreads;
            sp.DiscoveryMode = DiscoveryMode;
            sp.MapNullsToZero = MapNullsToZero;
            sp.awsAccessKeyId = awsAccessKeyId;
            sp.awsSecretKey = awsSecretKey;
            sp.ScheduleFrom = ScheduleFrom;
            sp.ScheduleSubject = ScheduleSubject;
            sp.dbTimeZone = dbTimeZone;
            return (sp);
        }

        public void getSettings()
        {
            Settings = loadSpaceSettings();
        }

        private SpaceSettings loadSpaceSettings()
        {
            string fileName = Path.Combine(Directory, SPACE_SETTINGS);
            SpaceSettings spSettings = null;
            if (File.Exists(fileName))
            {
                XmlReader xreader = new XmlTextReader(new StreamReader(fileName));
                XmlSerializer serializer = new XmlSerializer(typeof(SpaceSettings));
                try
                {
                    spSettings = (SpaceSettings)serializer.Deserialize(xreader);
                    spSettings.lastModifiedDate = File.GetLastWriteTime(fileName);
                }
                catch (System.InvalidOperationException ex)
                {
                    Global.systemLog.Warn(ex);
                    return null;
                }
                finally
                {
                    xreader.Close();
                }
            }

            return spSettings;
        }

        public void saveSettings()
        {
            if (Settings != null)
            {
                string fileName = Path.Combine(Directory, SPACE_SETTINGS);
                StreamWriter writer = new StreamWriter(fileName);
                XmlSerializer serializer = new XmlSerializer(typeof(SpaceSettings));
                serializer.Serialize(writer, Settings);
                writer.Close();
                Settings.lastModifiedDate = File.GetLastWriteTime(fileName);
            }
        }

        public void clearSettings()
        {
            string fileName = Path.Combine(Directory, SPACE_SETTINGS);
            if (File.Exists(fileName))
                File.Delete(fileName);
            Settings = null;
        }

        public void invalidateDataCache()
        {
            setRepostioryLastWriteTime(Util.REPOSITORY_DEV);
            setRepostioryLastWriteTime(Util.REPOSITORY_PROD);
        }

        private void setRepostioryLastWriteTime(string repository)
        {   
            string fileName = Path.Combine(Directory, repository);
            if (!File.Exists(fileName))
            {
                return;
            }
            bool retry = false;
            int maxRetryCount = 3;
            int count = 0;
            do
            {
                try
                {
                    retry = false;
                    File.SetLastWriteTime(fileName, DateTime.Now);                    
                }
                catch (Exception ex)
                {   
                    if (count < maxRetryCount)
                    {
                        retry = true;
                        System.Threading.Thread.Sleep(2000);
                    }
                    else
                    {
                        Global.systemLog.Warn("Cannot set modified time on after retries : " + repository, ex);
                    }
                }
                
            } while (retry && count++ < maxRetryCount);
        }


        public void invalidateDashboardCache()
        {
            string fileName = Path.Combine(Directory, "catalog", "DashboardMongoCacheRebuild");
            FileStream fs = null;
            try
            {
                fs = File.Create(fileName);
            }
            catch (Exception ex)
            {
                Global.systemLog.Debug("could not create " + fileName, ex);
            }
            finally
            {
                if (fs != null)
                {
                    fs.Close();
                }
            }
        }

        public int getSQLGenType()
        {
            int sqlGenType = ExpressionParser.LogicalExpression.SQL_GEN_DEFAULT;
            if (DatabaseType == Database.INFOBRIGHT)
            {
                sqlGenType = ExpressionParser.LogicalExpression.SQL_GEN_MYSQL;
            }
            else if (DatabaseType == Database.ORACLE)
            {
                sqlGenType = ExpressionParser.LogicalExpression.SQL_GEN_ORACLE;
            }
            else if (DatabaseType == Database.PARACCEL)
            {
                sqlGenType = ExpressionParser.LogicalExpression.SQL_GEN_PARACCEL;
            }
            else if (DatabaseType == Database.MEMDB)
            {
                sqlGenType = ExpressionParser.LogicalExpression.SQL_GEN_BIRST;
            }
            return sqlGenType;
        }

        public void fillSpaceAttributes(SpaceConfig sc, User u, bool useUserOverrides)
        {
            ConnectString = useUserOverrides && u.DatabaseConnectString != null ? u.DatabaseConnectString : sc.DatabaseConnectString;
            DatabaseType = useUserOverrides && u.DatabaseType != null ? u.DatabaseType : sc.DatabaseType;
            DatabaseDriver = useUserOverrides &&  u.DatabaseDriver != null ? u.DatabaseDriver : sc.DatabaseDriver;
            setDatabaseLoadDir(useUserOverrides && u.DatabaseLoadDir != null ? u.DatabaseLoadDir : sc.DatabaseLoadDir);
            AdminUser = useUserOverrides && u.AdminUser != null ? u.AdminUser : sc.AdminUser;
            AdminPwd = useUserOverrides && u.AdminPwd != null ? u.AdminPwd : sc.AdminPwd;
            QueryConnectString = useUserOverrides && u.QueryDatabaseConnectString != null ? u.QueryDatabaseConnectString : sc.QueryDatabaseConnectString;
            QueryDatabaseType = useUserOverrides && u.QueryDatabaseType != null ? u.QueryDatabaseType : sc.QueryDatabaseType;
            QueryDatabaseDriver = useUserOverrides && u.QueryDatabaseDriver != null ? u.QueryDatabaseDriver : sc.QueryDatabaseDriver;
            QueryUser = useUserOverrides && u.QueryUser != null ? u.QueryUser : sc.QueryUser;
            QueryPwd = useUserOverrides && u.QueryPwd != null ? u.QueryPwd : sc.QueryPwd;
            QueryConnectionName = useUserOverrides && u.QueryConnectionName != null ? u.QueryConnectionName : sc.QueryConnectionName;
            Active = true;
            MaxQueryRows = Database.MAX_QUERY_ROWS;
            MaxQueryTimeout = Database.MAX_QUERY_TIMEOUT;
            UseDynamicGroups = true; // Default value of new space is true
            awsAccessKeyId = sc.awsAccessKeyId;
            awsSecretKey = sc.awsSecretKey;
            dbTimeZone = sc.dbTimeZone;
        }

        /// <summary>
        /// Refresh the logo image if needed. Compares the modified timestamp of the settings file with 
        /// the inmemory object
        /// </summary>
        internal void refreshLogoImage()
        {
             string fileName = Path.Combine(Directory, SPACE_SETTINGS);
             if (File.Exists(fileName) && (Settings == null || (File.GetLastWriteTime(fileName) > Settings.lastModifiedDate)))
             {
                 SpaceSettings spSettings = loadSpaceSettings();
                 if (spSettings != null && spSettings.LogoImage != null)
                 {
                     if (Settings == null)
                     {
                         Settings = new SpaceSettings();
                     }
                     Settings.LogoImage = spSettings.LogoImage;
                     Settings.LogoImageType = spSettings.LogoImageType;
                 }
             }
        }
    }
}
