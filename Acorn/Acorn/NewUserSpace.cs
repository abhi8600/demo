﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace Acorn
{
    public class NewUserSpace
    {
        public Guid ID;
        public string description;
        public bool valid;
        public int[] accountTypes;
        public int type;
        public int spaceSetUp;
        public int[] acls; // for spaces which are shared
    }
}
