﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Xml;
using Newtonsoft.Json;


using System.Collections;
using Acorn.sforce;
using System.Text;

using Performance_Optimizer_Administration;

using System.Threading;
using System.Web.Services.Protocols;
using System.Data.Odbc;

namespace Acorn
{
    public class GoogleAnalyticsUtil
    {
        public static List<GoogleAnalyticsProfile> getProfiles(string username, string password)
        {
            HttpWebRequest wr = (HttpWebRequest)WebRequest.Create("https://www.google.com/accounts/ClientLogin");
            wr.Method = "POST";
            wr.ContentType = "application/x-www-form-urlencoded; charset=utf-8";
            StreamWriter writer = new StreamWriter(wr.GetRequestStream());
            writer.WriteLine("accountType=GOOGLE&Email=" + username + "&Passwd=" + password + "&service=analytics&source=Birst");
            writer.Close();

            HttpWebResponse resp = (HttpWebResponse)wr.GetResponse();
            StreamReader reader = new StreamReader(resp.GetResponseStream());
            string line = null;
            Dictionary<string, string> parms = new Dictionary<string, string>();
            while ((line = reader.ReadLine()) != null)
            {
                string[] vals = line.Split(new char[] { '=' });
                if (vals.Length == 2)
                    parms.Add(vals[0], vals[1]);
            }
            string token = parms["Auth"];
            resp.Close();
            wr = (HttpWebRequest)WebRequest.Create("https://www.googleapis.com/analytics/v3/management/accounts/~all/webproperties/~all/profiles");
            wr.Method = "GET";
            wr.ContentLength = 0;
            wr.ContentType = "application/x-www-form-urlencoded; charset=utf-8";
            wr.Headers["Authorization"] = "GoogleLogin auth=" + token;
            resp = (HttpWebResponse)wr.GetResponse();
            reader = new StreamReader(resp.GetResponseStream());

            string items = reader.ReadToEnd();
            List<GoogleAnalyticsProfile> itemList = new List<GoogleAnalyticsProfile>();

            XmlNode xmlNode = JsonConvert.DeserializeXmlNode(items, "items");
            string xml = xmlNode.InnerXml;

            XmlDocument xmlDocs = new XmlDocument();
            xmlDocs.XmlResolver = null;
            xmlDocs.LoadXml(xml);
            
            foreach (XmlNode cnode in xmlDocs.ChildNodes)
            {
                if (cnode.Name == "items")
                {
                    XmlNodeList node = cnode.ChildNodes;

                    foreach (XmlNode chnode in node)
                    {
                        string nodeName = chnode.Name;
                        string innerxml = chnode.InnerText;

                        if (nodeName.Equals("items")) 
                        {
                            XmlNodeList itemNodeList = chnode.ChildNodes;
                            GoogleAnalyticsProfile googleAnalyticsProfile = new GoogleAnalyticsProfile();
                            foreach (XmlNode itemNode in itemNodeList)
                            {
                                string itemNodeName = itemNode.Name;
                                string itemInnerXml = itemNode.InnerText;

                                if (itemNodeName.Equals("id"))
                                {
                                    googleAnalyticsProfile.profileId = itemInnerXml;
                                    googleAnalyticsProfile.tableID = "ga:" + itemInnerXml;
                                }
                                else if (itemNodeName.Equals("accountId"))
                                {
                                    googleAnalyticsProfile.accountId = itemInnerXml;
                                }
                                else if (itemNodeName.Equals("webPropertyId"))
                                {
                                    googleAnalyticsProfile.webPropertyId = itemInnerXml;
                                }
                                else if (itemNodeName.Equals("name"))
                                {
                                    googleAnalyticsProfile.accountName = itemInnerXml;
                                    googleAnalyticsProfile.title = "Google Analytics Profile " + itemInnerXml;
                                }
                            }

                            itemList.Add(googleAnalyticsProfile);
                        }

                    }
                }
            }
            resp.Close();
            return itemList;

        }
    }
}
