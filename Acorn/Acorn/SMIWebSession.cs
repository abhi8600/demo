﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Specialized;

namespace Acorn
{
    public class SMIWebSession : SMIWebSessionService.SMIWebSession
    {
        private NameValueCollection headers;

        public void setRequestHeaders(NameValueCollection headers)
        {
            this.headers = headers;
        }

        protected override System.Net.WebRequest GetWebRequest(Uri uri)
        {
            System.Net.WebRequest result = base.GetWebRequest(uri);
            if (headers != null)
            {
                result.Headers = new System.Net.WebHeaderCollection();
                result.Headers.Add(headers);
            }
            return result;
        }
    }
}
