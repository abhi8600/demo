﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.Odbc;
using System.IO;
using System.Net.Mail;
using System.Net.Sockets;
using System.Net;
using System.Text;
using Acorn.DBConnection;

namespace Acorn
{
    public partial class Health : System.Web.UI.Page
    {
        private static string mainSchema = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["MainSchema"];
        private static string directory = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["ContentDirectory"];
        private static int type = int.Parse(System.Web.Configuration.WebConfigurationManager.AppSettings["NewSpaceType"]);
        private static string localprotocol = (String)System.Web.Configuration.WebConfigurationManager.AppSettings["LocalProtocol"];
        private static string TOMCAT_HEALTH_SERVLET = "/SMIWeb/Health.jsp";

        protected void Page_Load(object sender, EventArgs e)
        {
            // don't cache
            Response.Clear();
            Util.setUpHeaders(Response);

            // validate where the request is coming from
            string hostaddr = Request.UserHostAddress;
            if (!Util.IsValidIP(hostaddr))
            {
                Global.systemLog.Warn("Health check request from unauthorized network - " + hostaddr);
                Response.StatusCode = 401;
                return;
            }

            QueryConnection conn = null;
            try
            {
                conn = ConnectionPool.getConnection();

                // check for server down request
                string message = null;
                string serverhost = Util.getRequestBaseHost(Request);
                int status = Database.getServerStatus(conn, mainSchema, serverhost, ref message);
                if (status < 0)
                {
                    Response.StatusCode = 403;
                }
                else
                {
                    // check the server health
                    bool res = check(conn, mainSchema);
                    if (res)
                        Response.StatusCode = 200;
                    else
                        Response.StatusCode = 500;
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Fatal("Health check request failed - " + ex.Message);
                Response.StatusCode = 500;
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
            return;
        }

        private bool check(QueryConnection conn, string mainSchema)
        {
            // check admin database
            try
            {
                int version = Database.getCurrentVersion(conn, mainSchema);
                if (version == -1)
                {
                    Global.systemLog.Fatal("Health check failed to access the database");
                    return false;
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Fatal("Health check failed to access the admin database - " + ex.Message);
                return false;
            }

            // check the content directory
            try
            {
                DirectoryInfo di = new DirectoryInfo(directory);
                if (!di.Exists)
                {
                    Global.systemLog.Fatal("Health check failed to access the content directory (does not exist) - " + directory);
                    return false;
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Fatal("Health check failed to access the content directory - " + directory + " - " + ex.Message);
                return false;
            }

            // check tomcat
            if (!checkTomcat())
                return false;

            return true;
        }

        private bool checkTomcat()
        {
            SpaceConfig sc = Util.getSpaceConfiguration(type);
            string url = localprotocol + sc.LocalURL + TOMCAT_HEALTH_SERVLET;
            HttpWebRequest myRequest = (HttpWebRequest)WebRequest.Create(url);
            myRequest.Method = "GET";
            WebResponse myResponse = null;
            StreamReader sr = null;
            try
            {
                myResponse = myRequest.GetResponse();
                sr = new StreamReader(myResponse.GetResponseStream(), System.Text.Encoding.UTF8);
                string result = sr.ReadToEnd();
                if (result != null && result.Equals("success"))
                    return true;
            }
            catch (Exception ex)
            {
                Global.systemLog.Fatal("Health check failed to access tomcat - " + url + " - " + ex.Message);
            }
            finally
            {
                if (sr != null)
                    sr.Close();
                if (myResponse != null)
                    myResponse.Close();
            }
            return false;
        }
    }
}
