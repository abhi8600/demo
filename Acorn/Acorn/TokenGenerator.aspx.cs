﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.Odbc;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using System.Collections.Specialized;
using Acorn.Utils;
using Acorn.DBConnection;

namespace Acorn
{
    public partial class TokenGenerator : System.Web.UI.Page
    {
        private static string mainSchema = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["MainSchema"];
        private static string tokenLifeTime = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["TokenLifeTime"];
        private static string uploadTokenLifeTime;

        private string getBirstParam(string newName, string oldName)
        {
            string val = Request.Params[newName];
            if (val == null || val.Length == 0)
                val = Request.Params[oldName];
            return val;
        }

        /*
         * handle SSO token requests
         */
        protected void Page_Load(object sender, EventArgs e)
        {
            Util.setUpHeaders(Response);
            bool spaceRequired = true;

            QueryConnection conn = null;
            try
            {
                string username = getBirstParam("birst.username", "username");
                if (Request.RequestType != "POST")
                {
                    // only POST requests are allowed since passwords can appear in this request
                    Response.StatusCode = 405;
                    Global.systemLog.Warn("TokenGenerator: non-POST request (" + username + "," + Request.UserHostAddress + ")");
                    return;
                }
                string tokenType = getBirstParam("birst.type", "type");
                string password = getBirstParam("birst.password", "password");
                string spacename = getBirstParam("birst.spacename", "spacename");
                string id = getBirstParam("birst.spaceId", "BirstSpaceId");
                string ssopassword = getBirstParam("birst.ssopassword", "ssopassword");
                string sessionVarsParam = getBirstParam("birst.sessionVars", "sessionVars");

                if (tokenType == null || tokenType.Length == 0 || tokenType.Equals("SSO")) // default type is SSO (backwards compatibility)
                {
                    if (username == null || (spacename == null && id == null) || ssopassword == null)
                    {
                        Response.StatusCode = 400;
                        Global.systemLog.Warn("TokenGenerator: missing birst.username/(birst.spacename|birst.spaceId)/birst.ssopassword (" + username + "," + spacename + "," + id + "," + Request.UserHostAddress + ")");
                        return;
                    }
                }
                else if (tokenType.Equals("LOGIN")) // mainly for iPad
                {
                    if (username == null || password == null)
                    {
                        Response.StatusCode = 400;
                        Global.systemLog.Warn("TokenGenerator: missing username/password (" + username + "," + Request.UserHostAddress + ")");
                        return;
                    }
                    spaceRequired = false;
                }
                else
                {
                    // only supported types via the POST are SSO and LOGIN
                    Response.StatusCode = 400;
                    Global.systemLog.Warn("TokenGenerator: bad birst.type (" + tokenType + "," + Request.UserHostAddress + ")");
                    return;
                }

                log4net.NDC.Clear();
                log4net.NDC.Push(username);

                conn = ConnectionPool.getConnection();

                User u = Database.getUser(conn, mainSchema, username);
                if (u == null)
                {
                    Response.StatusCode = 400;
                    Global.systemLog.Warn("TokenGenerator: user not in the database or products expired (" + username + "," + Request.UserHostAddress + ")");
                    return;
                }

                // get the appropriate type of security token based upon the request (Login or SSO)
                bool lockedout = false;
                string toEncrypt = null;
                if (spaceRequired)
                {
                    Guid spaceId = Guid.Empty;
                    if (id != null && id.Length > 0)
                    {
                        try
                        {
                            spaceId = new Guid(id);
                        }
                        catch (Exception)
                        {
                            Response.StatusCode = 400;
                            Global.systemLog.Warn("Security token request failed - bad space id - " + id);
                            return;
                        }
                    }
                    if (spaceId == Guid.Empty)
                    {
                        Space sp = Util.getSpaceByName(conn, mainSchema, u, spacename);
                        if (sp == null)
                        {
                            Response.StatusCode = 400;
                            Global.systemLog.Warn("Security token request failed - user does not have access to space " + spacename + " or the space does not exist");
                            return;
                        }
                        spaceId = sp.ID;
                    }
                    toEncrypt = getSSOSecurityToken(conn, mainSchema, u, ssopassword, Request.UserHostAddress, spaceId, sessionVarsParam, ref lockedout);
                }
                else
                {
                    toEncrypt = getLoginSecurityToken(conn, mainSchema, u, password, Request.UserHostAddress, false, false, Guid.Empty, sessionVarsParam, ref lockedout);
                }

                if (lockedout || toEncrypt == null)
                {
                    Global.systemLog.Debug("TokenGenerator: user locked out, disabled, account disabled, or invalid password - " + u.Username);
                    Response.StatusCode = 400;
                    return;
                }
                string redirectURL = null;
                string userRedirectedSite = Util.getUserRedirectedSite(Request, u);
                if (userRedirectedSite == null || userRedirectedSite.Length == 0)
                {
                    redirectURL = Util.getRequestBaseURL(Request);
                }
                else
                {
                    redirectURL = userRedirectedSite;
                }

                Response.AddHeader("BirstURL", Util.removeCRLF(redirectURL));
                string token = Util.encryptToken(toEncrypt);
                Global.systemLog.Debug("TokenGenerator returned " + token + " (" + toEncrypt + ")");
                Response.Write(token);
            }
            catch (Exception ex)
            {
                Response.Status = "500 Internal Error";
                Global.systemLog.Warn(ex, ex);
                return;
            }
            finally
            {
                log4net.NDC.Clear();
                ConnectionPool.releaseConnection(conn);
            }
        }

        /*
         * create a security token and store it in the database, return a unique identifier for the token
         */
        private static string createSSOToken(QueryConnection conn, string mainSchema, User u, Space sp, string sessionVarsParam, bool repAdmin, string type)
        {
            if (!Global.initialized)
                return "";

            // create a token and store the parameters in the database
            SSOToken ssoToken = new SSOToken();
            ssoToken.tokenID = Guid.NewGuid();
            ssoToken.userID = u.ID;
            ssoToken.spaceID = sp != null ? sp.ID : Guid.Empty;
            ssoToken.sessionVariables = sessionVarsParam;
            ssoToken.repAdmin = repAdmin;
            if (tokenLifeTime != null)
            {
                int delta = 30;
                Int32.TryParse(tokenLifeTime, out delta);
                ssoToken.endOfLife = DateTime.Now.AddSeconds(delta);
            }
            ssoToken.type = type;
            Database.createSSOToken(conn, mainSchema, ssoToken);
            return ssoToken.tokenID.ToString();
        }

        /*
         * get a security token for an SSO request
         */
        private static string getSSOSecurityToken(QueryConnection conn, string mainSchema, User u, string ssopassword, string hostaddr, Guid spaceId, string sessionVarsParam, ref bool lockedout)
        {
            lockedout = false;
            if (!isValid(conn, mainSchema, u, hostaddr, ref lockedout))
            {
                return null;
            }
            Space sp = Util.getSpaceById(conn, mainSchema, u, spaceId);
            if (sp == null)
            {
                Global.systemLog.Warn("SSO security token request failed - user does not have access to space " + spaceId + " or the space does not exist");
                return null;
            }

            if (!sp.SSO) // XXX do we need this flag?  non-null ssopassword is sufficient or should we require this for AppExchangeSSO
            {
                Global.systemLog.Warn("SSO security token request failed - space " + sp.ID + " has not been SSO enabled");
                return null;
            }
            if (sp.SSOPassword != ssopassword)
            {
                Global.systemLog.Warn("SSO security token request failed - incorrect ssopassword for space " + sp.ID);
                return null;
            }

            int spaceOpID = SpaceOpsUtils.getSpaceAvailability(sp.ID);
            if (!SpaceOpsUtils.isSpaceAvailable(spaceOpID) && !SpaceOpsUtils.isSpaceBeingCopiedFrom(spaceOpID))
            {   
                Global.systemLog.Warn("SSO security token request failed. Space is unavailable. space id : " + sp.ID + " : " + SpaceOpsUtils.getAvailabilityMessage(null, spaceOpID));
                return null;
            }

            return createSSOToken(conn, mainSchema, u, sp, sessionVarsParam, false, "SSO");
        }

        /*
         * get a security token for an AppExchange SSO request
         */
        public static string getAppExchangeSecurityToken(QueryConnection conn, string mainSchema, User u, string hostaddr, Guid id, string sessionVarsParam, ref bool lockedout)
        {
            lockedout = false;
            if (!isValid(conn, mainSchema, u, hostaddr, ref lockedout))
            {
                return null;
            }
            Space sp = Util.getSpaceById(conn, mainSchema, u, id);
            if (sp == null)
            {
                Global.systemLog.Warn("AppExchange security token request failed failed - user does not have access to space " + id + " or the space does not exist");
                return null;
            }
            return createSSOToken(conn, mainSchema, u, sp, sessionVarsParam, false, "SFDC SSO");
        }

        /*
         * This request is called through a webservice. A valid session should exist
         */ 
        public static string getUploadSecurityToken(QueryConnection conn, string mainSchema, User u, Space sp, string hostaddress, string type, string parameters)
        {   
            // create a token and store the parameters in the database
            UploadToken uploadToken = new UploadToken();
            uploadToken.tokenID = Guid.NewGuid();
            uploadToken.userID = u.ID;
            uploadToken.spaceID = sp != null ? sp.ID : Guid.Empty;
            uploadToken.parameters = parameters;
            uploadToken.hostaddr = hostaddress;
            uploadToken.endOfLife = getUploadTokenLifeTime();
            uploadToken.type = type;
            Database.createUploadToken(conn, mainSchema, uploadToken);
            return uploadToken.tokenID.ToString();            
        }

        private static DateTime getUploadTokenLifeTime()
        {
            if (uploadTokenLifeTime == null || uploadTokenLifeTime.Trim().Length == 0)
            {
                object obj = System.Web.Configuration.WebConfigurationManager.AppSettings["UploadTokenLifeTime"];
                if (obj != null)
                {
                    uploadTokenLifeTime = (string)obj;
                }
            }

            uploadTokenLifeTime = uploadTokenLifeTime == null || uploadTokenLifeTime.Trim().Length == 0 ? "30" : uploadTokenLifeTime;
            int delta = 30;
            Int32.TryParse(uploadTokenLifeTime, out delta);
            return DateTime.Now.AddSeconds(delta);
        }

        /*
         * get a security token for a 'login' request
         */
        public static string getLoginSecurityToken(QueryConnection conn, string mainSchema, User u, string password, string hostaddr, bool proxyuser, bool repAdmin, Guid spaceId, string sessionVarParams, ref bool lockedout)
        {
            lockedout = false;
            if (!isValid(conn, mainSchema, u, hostaddr, ref lockedout))
            {
                return null;
            }
            // validate username/password pair - skip for superuser and proxyuser(already validated)
            if (!proxyuser)
            {
                if (!Membership.ValidateUser(u.Username, password))
                {
                    Global.systemLog.Warn("Login security token request failed - user not authenticated");
                    return null;
                }
            }

            Space sp = null;
            if (spaceId != Guid.Empty)
            {
                sp = Util.getSpaceById(conn, mainSchema, u, spaceId);
                if (sp == null)
                {
                    Global.systemLog.Warn("SSO security token request failed - user does not have access to space " + spaceId + " or the space does not exist");
                    return null;
                }
            }
            return createSSOToken(conn, mainSchema, u, sp, sessionVarParams, repAdmin, "BROWSER");
        }

        /*
         * get a security token for an OpenID 'login' request
         */
        public static string getOpenIDLoginSecurityToken(QueryConnection conn, string mainSchema, User u, string hostaddr, ref bool lockedout)
        {
            lockedout = false;
            if (!isValid(conn, mainSchema, u, hostaddr, ref lockedout))
            {
                return null;
            }
            return createSSOToken(conn, mainSchema, u, null, null, false, "BROWSER (OPENID)");
        }

        /*
         * get a security token for an SFDC delegated authentication request
         */
        public static string getSFDCDelegatedAuthenticationSecurityToken(QueryConnection conn, string mainSchema, User u, string hostaddr, ref bool lockedout)
        {
            lockedout = false;
            if (!isValid(conn, mainSchema, u, hostaddr, ref lockedout))
            {
                return null;
            }
            return createSSOToken(conn, mainSchema, u, null, null, false, "BROWSER (DA)");
        }

        /*
         * get a security token for an Windows Integrated Authentication (NTLM) 'login' request
         */
        public static string getNTLMLoginSecurityToken(QueryConnection conn, string mainSchema, User u, string hostaddr, ref bool lockedout)
        {
            lockedout = false;
            if (!isValid(conn, mainSchema, u, hostaddr, ref lockedout))
            {
                return null;
            }
            return createSSOToken(conn, mainSchema, u, null, null, false, "BROWSER (NTLM)");
        }

        /*
         * get a security token for an OpenID 'sso' request
         */
        public static string getOpenIDSSOSecurityToken(QueryConnection conn, string mainSchema, User u, Guid id, string sessionVarsParam, string hostaddr, ref bool lockedout)
        {
            lockedout = false;
            if (!isValid(conn, mainSchema, u, hostaddr, ref lockedout))
            {
                return null;
            }
            Space sp = Util.getSpaceById(conn, mainSchema, u, id);
            if (sp == null)
            {
                Global.systemLog.Warn("OpenID security token request failed failed - user does not have access to space " + id + " or the space does not exist");
                return null;
            }

            return createSSOToken(conn, mainSchema, u, sp, sessionVarsParam, false, "SSO (OPENID)");
        }

        /*
         * has the user been locked out due to too many failed login attempts
         */
        private static bool isLockedOut(User u)
        {
            MembershipUser mu = Membership.GetUser(u.Username, false);
            if (mu != null && mu.IsLockedOut)
            {
                Global.systemLog.Warn("Security token request failed - user locked out");
                return true;
            }
            return false;
        }

        /*
         * encapsulation to make sure all paths do the same validations
         */
        public static bool isValid(QueryConnection conn, string mainSchema, User u, string hostaddr, ref bool lockedout)
        {
            DateTime lastLoginDate = Database.getLastLogin(u.Username);
            Util.checkIfUserInactiveAndDisable(conn, u, lastLoginDate);

            if (u.Disabled)
            {
                Global.systemLog.Warn("Security token request failed - user has been disabled (" + u.Username + ")");
                return false;
            }
            if (u.actDetails != null && u.actDetails.Disabled)
            {
                Global.systemLog.Warn("Security token request failed - account has been disabled (" + u.Username + ")");
            }

            if (isLockedOut(u))
            {
                lockedout = true;
                return false;
            }
            List<String> userAllowedIPs = Database.getAllowedIPs(conn, mainSchema, u.ManagedAccountId, u.ID);
            if (!Util.IsValidIP(hostaddr, userAllowedIPs))
            {
                Global.systemLog.Warn("Security token request failed - user using invalid ip adddress - " + hostaddr);
                return false;
            }

            if (!Util.doesUserHaveValidEdition(conn, mainSchema, u))
            {
                Global.systemLog.Warn("Security token request failed - user has expired");
                return false;
            }
            return true;
        }

        /*
         * build and return the correct SSO redirect URL
         */
        public static string getSSORedirectURL(string redirectURL, string token, HttpRequest request, NameValueCollection newqs)
        {
            NameValueCollection qs = fixUpQueryString(newqs);
            qs.Add("BirstSSOToken", token);
         //   qs.Add("birst.masterURL", Util.getRequestBaseURL(request));
            string url = redirectURL + "/SSO.aspx?" + Util.nameValuePairToUrlString(qs);
            return url;
        }

        /*
         * strip off query string parameters that we do not need to pass on
         */
        private static NameValueCollection fixUpQueryString(NameValueCollection items)
        {
            NameValueCollection newItems = new NameValueCollection();
            foreach (String key in items.AllKeys)
            {
                if (key == null || key == "BirstSSOToken" || key == "birst.masterURL" // items added by getSSORedirectURL
                    /* || key == "sessionid" || key == "serverurl" */ || key == "birst.useSFDCEmailForBirstUsername" // strip off SFDC parameters
                    || key == "birst.sessionVars" || key == "birst.spaceId" || key == "birst.ssopassword" || key == "birst.password" || key == "BirstSpaceName" || key == "BirstSpaceId" // strip off Birst parameters
                    || key == "birst.OpenID" || key == "birst.OpenIDURL" // Birst OpenId parameters
                    || key.StartsWith("dnoa.") || key.StartsWith("openid.")) // strip out OpenId parameters
                {
                    continue;
                }
                newItems.Add(key, items[key]);
            }
            return newItems;
        }
    }
}
