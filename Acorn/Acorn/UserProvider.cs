using System.Web.Security;
using System.Configuration.Provider;
using System.Collections.Specialized;
using System;
using System.Data;
using System.Data.Odbc;
using System.Configuration;
using System.Diagnostics;
using System.Web;
using System.Globalization;
using System.Security.Cryptography;
using System.Text;
using System.Web.Configuration;
using Acorn.DBConnection;
using System.Collections.Generic;
using System.Net.Radius;

namespace Acorn
{
    public sealed class UserProvider : MembershipProvider
    {
        //
        // Global connection string, generated password length, generic exception message, event log info.
        //
        private int newPasswordLength = 32;
        private string eventSource = "OdbcMembershipProvider";
        private string eventLog = "Application";
        private string exceptionMessage = "An exception occurred. Please check the Event Log.";
        private string connectionString;
        public QueryConnection connection; // XXX used by MoveSpace, do not remove
        private static string CurrentPasswordHashVersion = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["CurrentPasswordHashVersion"];

        //
        // Used when determining encryption key values.
        //
        private MachineKeySection machineKey;

        //
        // If false, exceptions are thrown to the caller. If true,
        // exceptions are written to the event log.
        //
        private bool pWriteExceptionsToEventLog;

        public bool WriteExceptionsToEventLog
        {
            get { return pWriteExceptionsToEventLog; }
            set { pWriteExceptionsToEventLog = value; }
        }

        //
        // System.Configuration.Provider.ProviderBase.Initialize Method
        //
        public override void Initialize(string name, NameValueCollection config)
        {
            //
            // Initialize values from web.config.
            //
            if (config == null)
                throw new ArgumentNullException("config");

            if (name == null || name.Length == 0)
                name = "UserProvider";

            if (String.IsNullOrEmpty(config["description"]))
            {
                config.Remove("description");
                config.Add("description", "ODBC User provider");
            }

            // Initialize the abstract base class.
            base.Initialize(name, config);

            pMaxInvalidPasswordAttempts = Convert.ToInt32(GetConfigValue(config["maxInvalidPasswordAttempts"], "5"));
            pPasswordAttemptWindow = Convert.ToInt32(GetConfigValue(config["passwordAttemptWindow"], "10"));
            pMinRequiredNonAlphanumericCharacters = Convert.ToInt32(GetConfigValue(config["minRequiredNonAlphanumericCharacters"], "1"));
            pMinRequiredPasswordLength = Convert.ToInt32(GetConfigValue(config["minRequiredPasswordLength"], "7"));
            pPasswordStrengthRegularExpression = Convert.ToString(GetConfigValue(config["passwordStrengthRegularExpression"], ""));
            pEnablePasswordReset = Convert.ToBoolean(GetConfigValue(config["enablePasswordReset"], "true"));
            pEnablePasswordRetrieval = Convert.ToBoolean(GetConfigValue(config["enablePasswordRetrieval"], "false"));
            pRequiresQuestionAndAnswer = Convert.ToBoolean(GetConfigValue(config["requiresQuestionAndAnswer"], "false"));
            pRequiresUniqueEmail = Convert.ToBoolean(GetConfigValue(config["requiresUniqueEmail"], "false"));
            pWriteExceptionsToEventLog = Convert.ToBoolean(GetConfigValue(config["writeExceptionsToEventLog"], "true"));

            string temp_format = config["passwordFormat"];
            if (temp_format == null)
            {
                temp_format = "Hashed";
            }

            switch (temp_format)
            {
                case "Hashed":
                    pPasswordFormat = MembershipPasswordFormat.Hashed;
                    break;
                case "Encrypted":
                    pPasswordFormat = MembershipPasswordFormat.Encrypted;
                    break;
                case "Clear":
                    pPasswordFormat = MembershipPasswordFormat.Clear;
                    break;
                default:
                    throw new ProviderException("Password format not supported.");
            }

            //
            // Initialize QueryConnection.
            //
            ConnectionStringSettings ConnectionStringSettings =
              ConfigurationManager.ConnectionStrings[config["connectionStringName"]];

            if (ConnectionStringSettings == null || ConnectionStringSettings.ConnectionString.Trim() == "")
            {
                throw new ProviderException("Connection string cannot be blank.");
            }

            connectionString = ConnectionStringSettings.ConnectionString;

            // Get encryption and decryption key information from the configuration.
            Configuration cfg =
              WebConfigurationManager.OpenWebConfiguration(System.Web.Hosting.HostingEnvironment.ApplicationVirtualPath);
            machineKey = (MachineKeySection)cfg.GetSection("system.web/machineKey");

            if (machineKey.ValidationKey.Contains("AutoGenerate"))
                if (PasswordFormat != MembershipPasswordFormat.Clear)
                    throw new ProviderException("Hashed or Encrypted passwords " +
                                                "are not supported with auto-generated keys.");
        }

        //
        // A helper function to retrieve config values from the configuration file.
        //
        private string GetConfigValue(string configValue, string defaultValue)
        {
            if (String.IsNullOrEmpty(configValue))
                return defaultValue;

            return configValue;
        }


        //
        // System.Web.Security.MembershipProvider properties -- most of this is not actually used anymore, need to fully cut the cord
        //
        private bool pEnablePasswordReset;
        private bool pEnablePasswordRetrieval;
        private bool pRequiresQuestionAndAnswer;
        private bool pRequiresUniqueEmail;
        private int pMaxInvalidPasswordAttempts;
        private int pPasswordAttemptWindow;
        private MembershipPasswordFormat pPasswordFormat;

        public override string ApplicationName
        {
            get { return "Birst"; }
            set { }
        }

        public override bool EnablePasswordReset
        {
            get { return pEnablePasswordReset; }
        }

        public override bool EnablePasswordRetrieval
        {
            get { return pEnablePasswordRetrieval; }
        }

        public override bool RequiresQuestionAndAnswer
        {
            get { return pRequiresQuestionAndAnswer; }
        }

        public override bool RequiresUniqueEmail
        {
            get { return pRequiresUniqueEmail; }
        }

        public override int MaxInvalidPasswordAttempts
        {
            get { return pMaxInvalidPasswordAttempts; }
        }

        public override int PasswordAttemptWindow
        {
            get { return pPasswordAttemptWindow; }
        }

        public override MembershipPasswordFormat PasswordFormat
        {
            get { return pPasswordFormat; }
        }

        private int pMinRequiredNonAlphanumericCharacters;

        public override int MinRequiredNonAlphanumericCharacters
        {
            get { return pMinRequiredNonAlphanumericCharacters; }
        }

        private int pMinRequiredPasswordLength;

        public override int MinRequiredPasswordLength
        {
            get { return pMinRequiredPasswordLength; }
        }

        private string pPasswordStrengthRegularExpression;

        public override string PasswordStrengthRegularExpression
        {
            get { return pPasswordStrengthRegularExpression; }
        }

        //
        // System.Web.Security.MembershipProvider methods.
        //

        //
        // MembershipProvider.ChangePassword
        //
        public override bool ChangePassword(string username, string oldPwd, string newPwd)
        {
            if (!ValidateUser(username, oldPwd))
            {
                Global.systemLog.Warn("Password change failed (bad old password) for user: " + username);
                return false;
            }

            ValidatePasswordEventArgs args =
              new ValidatePasswordEventArgs(username, newPwd, true);

            OnValidatingPassword(args);

            if (args.Cancel)
            {
                Global.systemLog.Warn("Password change failed (new password validation failure) for user: " + username);
                if (args.FailureInformation != null)
                    throw args.FailureInformation;
                else
                    throw new MembershipPasswordException("Change password canceled due to new password validation failure.");
            }
            QueryConnection conn = new QueryConnection(connectionString);
            QueryCommand cmd = new QueryCommand("UPDATE " + Database.getTableName(Database.mainSchema, Database.USER_TABLE_NAME) +
                    " SET Password = ?, LastPasswordChangedDate = ? " +
                    " WHERE Username = ?", conn);

            int rowsAffected = 0;
            try
            {
                conn.Open();
                string hashAlgorithm = null;
                User u = Database.getUser(conn, Database.mainSchema, username, false);
                if (u != null)
                {
                    PasswordPolicy policy = Database.getPasswordPolicy(conn, Database.mainSchema, u.ManagedAccountId);
                    if (policy != null)
                        hashAlgorithm = policy.hashAlgorithm;
                }
                string hash = Password.HashPassword(newPwd, username, hashAlgorithm);
                cmd.Parameters.Add("@Password", OdbcType.VarChar, 1024).Value = hash;
                cmd.Parameters.Add("@LastPasswordChangedDate", OdbcType.DateTime).Value = DateTime.Now;
                cmd.Parameters.Add("@Username", OdbcType.NVarChar, 255).Value = username.ToLower();

                rowsAffected = cmd.ExecuteNonQuery();
                if (rowsAffected > 0)
                {
                    Database.addPasswordToHistory(conn, Database.mainSchema, u.ID, hash);
                }
            }
            catch (OdbcException e)
            {
                if (WriteExceptionsToEventLog)
                {
                    WriteToEventLog(e, "ChangePassword");
                    throw new ProviderException(exceptionMessage);
                }
                else
                {
                    throw e;
                }
            }
            finally
            {
                conn.Close();
            }
            if (rowsAffected > 0)
            {
                Global.systemLog.Info("Password changed for user: " + username);
                return true;
            }
            Global.systemLog.Info("Password change failed (database updated zero records) for user: " + username);
            return false;
        }

        //
        // MembershipProvider.ChangePasswordQuestionAndAnswer
        //
        public override bool ChangePasswordQuestionAndAnswer(string username,
                      string password,
                      string newPwdQuestion,
                      string newPwdAnswer)
        {
            throw new ProviderException("ChangePasswordQuestionAndAnswer not supported");
        }

        public bool headless;

        //
        // MembershipProvider.CreateUser
        //
        public override MembershipUser CreateUser(string username,
                 string password,
                 string email,
                 string passwordQuestion,
                 string passwordAnswer,
                 bool isApproved,
                 object providerUserKey,
                 out MembershipCreateStatus status)
        {
            if (username == null || username == "")
                username = email;
            ValidatePasswordEventArgs args =
              new ValidatePasswordEventArgs(username, password, true);

            OnValidatingPassword(args);

            if (args.Cancel)
            {
                status = MembershipCreateStatus.InvalidPassword;
                return null;
            }

            if (RequiresUniqueEmail && GetUserNameByEmail(email) != "")
            {
                status = MembershipCreateStatus.DuplicateEmail;
                return null;
            }
            bool userExists = false;
            if (headless)
            {
                Object userResult = getUserObject(username, false);
                userExists = userResult == null ? false : (bool) userResult == true;
            }
            else
            {
                MembershipUser u = GetUser(username, false);
                userExists = u != null;
            }
            if (!userExists)
            {
                DateTime createDate = DateTime.Now;
                if (providerUserKey == null)
                {
                    providerUserKey = Guid.NewGuid();
                }
                else
                {
                    if (!(providerUserKey is Guid))
                    {
                        status = MembershipCreateStatus.InvalidProviderUserKey;
                        return null;
                    }
                }

                QueryConnection conn = connection != null ? connection : new QueryConnection(connectionString);
                bool isOracle = conn.isDBTypeOracle();
                username = username.ToLower();
                string FailedPasswordAttemptCount = isOracle ? "FailedPwdAttemptCount" : "FailedPasswordAttemptCount";
                string FailedPasswordAttemptWindowStart = isOracle ? "FailedPwdAttemptWindowStart" : "FailedPasswordAttemptWindowStart";
                string Comment = conn.getOracleReservedName("Comment");
                QueryCommand cmd = new QueryCommand("INSERT INTO " + Database.getTableName(Database.mainSchema, Database.USER_TABLE_NAME) +
                      " (PKID, Username, Password, Email, IsApproved," +
                       Comment + ", CreationDate, LastPasswordChangedDate, LastActivityDate," +
                      " IsLockedOut, LastLockedOutDate," +
                       FailedPasswordAttemptCount + ", " + FailedPasswordAttemptWindowStart + ")" +
                      " Values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", conn);

                string hash = Password.HashPassword(password, username, null);
                createDate = Database.truncateDateTime(createDate);

                cmd.Parameters.Add("@PKID", conn.getODBCUniqueIdentiferType()).Value = isOracle ? conn.getUniqueIdentifer((Guid)providerUserKey) : providerUserKey;
                cmd.Parameters.Add("@Username", OdbcType.NVarChar, 255).Value = username;
                cmd.Parameters.Add("@Password", OdbcType.VarChar, 1024).Value = hash;
                cmd.Parameters.Add("@Email", OdbcType.VarChar, 128).Value = email;
                cmd.Parameters.Add("@IsApproved", conn.getODBCBooleanType()).Value = conn.getBooleanValue(isApproved);
                cmd.Parameters.Add("@" + Comment, OdbcType.VarChar, 255).Value = "";
                cmd.Parameters.Add("@CreationDate", OdbcType.DateTime).Value = createDate;
                cmd.Parameters.Add("@LastPasswordChangedDate", OdbcType.DateTime).Value = createDate;
                cmd.Parameters.Add("@LastActivityDate", OdbcType.DateTime).Value = createDate;
                cmd.Parameters.Add("@IsLockedOut", conn.getODBCBooleanType()).Value = conn.getBooleanValue(false);
                cmd.Parameters.Add("@LastLockedOutDate", OdbcType.DateTime).Value = createDate;
                cmd.Parameters.Add("@" + FailedPasswordAttemptCount, OdbcType.Int).Value = 0;
                cmd.Parameters.Add("@" + FailedPasswordAttemptWindowStart, OdbcType.DateTime).Value = createDate;

                try
                {
                    if (conn.State != ConnectionState.Open)
                        conn.Open();

                    int recAdded = cmd.ExecuteNonQuery();
                    if (recAdded > 0)
                    {
                        status = MembershipCreateStatus.Success;
                        Database.addPasswordToHistory(conn, Database.mainSchema, (Guid)providerUserKey, hash);
                    }
                    else
                    {
                        status = MembershipCreateStatus.UserRejected;
                    }
                }
                catch (OdbcException e)
                {
                    if (WriteExceptionsToEventLog)
                    {
                        WriteToEventLog(e, "CreateUser");
                    }
                    status = MembershipCreateStatus.ProviderError;
                }
                finally
                {
                    conn.Close();
                }
                if (headless)
                    return null;
                else
                    return GetUser(username, false);
            }
            else
            {
                status = MembershipCreateStatus.DuplicateUserName;
            }

            return null;
        }

        //
        // MembershipProvider.DeleteUser
        //
        public override bool DeleteUser(string username, bool deleteAllRelatedData)
        {
            QueryConnection conn = new QueryConnection(connectionString);
            QueryCommand cmd = new QueryCommand("DELETE FROM " + Database.getTableName(Database.mainSchema, Database.USER_TABLE_NAME) +
                    " WHERE Username = ?", conn);

            cmd.Parameters.Add("@Username", OdbcType.NVarChar, 255).Value = username.ToLower();
            int rowsAffected = 0;
            try
            {
                conn.Open();
                rowsAffected = cmd.ExecuteNonQuery();
                if (deleteAllRelatedData)
                {
                    // Process commands to delete all data for the user in the database.
                }
            }
            catch (OdbcException e)
            {
                if (WriteExceptionsToEventLog)
                {
                    WriteToEventLog(e, "DeleteUser");

                    throw new ProviderException(exceptionMessage);
                }
                else
                {
                    throw e;
                }
            }
            finally
            {
                conn.Close();
            }

            if (rowsAffected > 0)
                return true;
            return false;
        }

        //
        // MembershipProvider.GetAllUsers
        //
        public override MembershipUserCollection GetAllUsers(int pageIndex, int pageSize, out int totalRecords)
        {
            QueryConnection conn = new QueryConnection(connectionString);
            bool isOracle = conn.isDBTypeOracle();
            string FailedPasswordAttemptWindowStart = isOracle ? "FailedPwdAttemptWindowStart" : "FailedPasswordAttemptWindowStart";

            QueryCommand cmd = new QueryCommand("SELECT Count(*) FROM " + Database.getTableName(Database.mainSchema, Database.USER_TABLE_NAME), conn);
            MembershipUserCollection users = new MembershipUserCollection();

            QueryReader reader = null;
            totalRecords = 0;
            try
            {
                conn.Open();
                totalRecords = (int)cmd.ExecuteScalar();
                if (totalRecords <= 0) { return users; }
                cmd.CommandText = "SELECT PKID, Username, Email, PasswordQuestion," +
                         " Comment, IsApproved, IsLockedOut, CreationDate, LastLoginDate," +
                         " LastActivityDate, LastPasswordChangedDate, LastLockedOutDate, " + FailedPasswordAttemptWindowStart + ", MANAGED_ACCOUNT_ID" +
                         " FROM " + Database.getTableName(Database.mainSchema, Database.USER_TABLE_NAME) +
                         " ORDER BY Username ASC";
                reader = cmd.ExecuteReader();

                int counter = 0;
                int startIndex = pageSize * pageIndex;
                int endIndex = startIndex + pageSize - 1;

                while (reader.Read())
                {
                    if (counter >= startIndex)
                    {
                        MembershipUser u = GetUserFromReader(conn, reader);
                        users.Add(u);
                    }
                    if (counter >= endIndex) { cmd.Cancel(); }
                    counter++;
                }
            }
            catch (OdbcException e)
            {
                if (WriteExceptionsToEventLog)
                {
                    WriteToEventLog(e, "GetAllUsers ");
                    throw new ProviderException(exceptionMessage);
                }
                else
                {
                    throw e;
                }
            }
            finally
            {
                if (reader != null) { reader.Close(); }
                conn.Close();
            }
            return users;
        }

        //
        // MembershipProvider.GetNumberOfUsersOnline
        //
        public override int GetNumberOfUsersOnline()
        {
            TimeSpan onlineSpan = new TimeSpan(0, System.Web.Security.Membership.UserIsOnlineTimeWindow, 0);
            DateTime compareTime = DateTime.Now.Subtract(onlineSpan);

            QueryConnection conn = new QueryConnection(connectionString);
            QueryCommand cmd = new QueryCommand("SELECT Count(*) FROM " + Database.getTableName(Database.mainSchema, Database.USER_TABLE_NAME) +
                    " WHERE LastActivityDate > ?", conn);
            cmd.Parameters.Add("@CompareDate", OdbcType.DateTime).Value = compareTime;

            int numOnline = 0;
            try
            {
                conn.Open();
                numOnline = (int)cmd.ExecuteScalar();
            }
            catch (OdbcException e)
            {
                if (WriteExceptionsToEventLog)
                {
                    WriteToEventLog(e, "GetNumberOfUsersOnline");
                    throw new ProviderException(exceptionMessage);
                }
                else
                {
                    throw e;
                }
            }
            finally
            {
                conn.Close();
            }
            return numOnline;
        }

        //
        // MembershipProvider.GetPassword
        //
        public override string GetPassword(string username, string answer)
        {
            throw new ProviderException("Password Retrieval Not Supported.");
        }

        public object getUserObject(string username, bool userIsOnline)
        {
            QueryConnection conn = connection != null ? connection : new QueryConnection(connectionString);
            bool isOracle = conn.isDBTypeOracle();
            string FailedPasswordAttemptWindowStart = isOracle ? "FailedPwdAttemptWindowStart" : "FailedPasswordAttemptWindowStart";

            QueryCommand cmd = new QueryCommand("SELECT PKID, Username, Email, PasswordQuestion, " +
                 conn.getReservedName("Comment") + ", IsApproved, IsLockedOut, CreationDate, LastLoginDate," +
                 " LastActivityDate, LastPasswordChangedDate, LastLockedOutDate, " + FailedPasswordAttemptWindowStart + ", MANAGED_ACCOUNT_ID" +
                 " FROM " + Database.getTableName(Database.mainSchema, Database.USER_TABLE_NAME) +" WHERE Username = ?", conn);

            cmd.Parameters.Add("@Username", OdbcType.NVarChar, 255).Value = username.ToLower();

            MembershipUser u = null;
            QueryReader reader = null;

            try
            {
                if (conn.State != ConnectionState.Open)
                    conn.Open();
                reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    reader.Read();
                    if (headless)
                    {
                        return true;
                    }
                    u = GetUserFromReader(conn, reader);
                    if (userIsOnline)
                    {
                        QueryCommand updateCmd = new QueryCommand("UPDATE " + Database.getTableName(Database.mainSchema, Database.USER_TABLE_NAME) + 
                                  " SET LastActivityDate = ? " +
                                  "WHERE Username = ?", conn);
                        updateCmd.Parameters.Add("@LastActivityDate", OdbcType.DateTime).Value = DateTime.Now;
                        updateCmd.Parameters.Add("@Username", OdbcType.NVarChar, 255).Value = username.ToLower();
                        updateCmd.ExecuteNonQuery();
                    }
                }
            }
            catch (OdbcException e)
            {
                if (WriteExceptionsToEventLog)
                {
                    WriteToEventLog(e, "GetUser(String, Boolean)");

                    throw new ProviderException(exceptionMessage);
                }
                else
                {
                    throw e;
                }
            }
            finally
            {
                if (reader != null) { reader.Close(); }
                conn.Close();
            }

            return u;
        }

        //
        // MembershipProvider.GetUser(string, bool)
        //
        public override MembershipUser GetUser(string username, bool userIsOnline)
        {
            return (MembershipUser)getUserObject(username, userIsOnline);
        }

        //
        // MembershipProvider.GetUser(object, bool)
        //
        public override MembershipUser GetUser(object providerUserKey, bool userIsOnline)
        {
            QueryConnection conn = new QueryConnection(connectionString);
            bool isOracle = conn.isDBTypeOracle();
            string FailedPasswordAttemptWindowStart = isOracle ? "FailedPwdAttemptWindowStart" : "FailedPasswordAttemptWindowStart";

            QueryCommand cmd = new QueryCommand("SELECT PKID, Username, Email, PasswordQuestion," +
                  " Comment, IsApproved, IsLockedOut, CreationDate, LastLoginDate," +
                  " LastActivityDate, LastPasswordChangedDate, LastLockedOutDate, " + FailedPasswordAttemptWindowStart + ", MANAGED_ACCOUNT_ID" +
                  " FROM " + Database.getTableName(Database.mainSchema, Database.USER_TABLE_NAME) + " WHERE PKID = ?", conn);
            cmd.Parameters.Add("@PKID", conn.getODBCUniqueIdentiferType()).Value = isOracle ? conn.getUniqueIdentifer((Guid)providerUserKey) : providerUserKey;
            MembershipUser u = null;
            QueryReader reader = null;
            try
            {
                conn.Open();
                reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    reader.Read();
                    u = GetUserFromReader(conn, reader);
                    if (userIsOnline)
                    {
                        QueryCommand updateCmd = new QueryCommand("UPDATE " + Database.getTableName(Database.mainSchema, Database.USER_TABLE_NAME) +
                                  " SET LastActivityDate = ? " +
                                  "WHERE PKID = ?", conn);
                        updateCmd.Parameters.Add("@LastActivityDate", OdbcType.DateTime).Value = DateTime.Now;
                        updateCmd.Parameters.Add("@PKID", conn.getODBCUniqueIdentiferType()).Value = isOracle ? conn.getUniqueIdentifer((Guid)providerUserKey) : providerUserKey;
                        updateCmd.ExecuteNonQuery();
                    }
                }
            }
            catch (OdbcException e)
            {
                if (WriteExceptionsToEventLog)
                {
                    WriteToEventLog(e, "GetUser(Object, Boolean)");

                    throw new ProviderException(exceptionMessage);
                }
                else
                {
                    throw e;
                }
            }
            finally
            {
                if (reader != null) { reader.Close(); }

                conn.Close();
            }

            return u;
        }


        //
        // GetUserFromReader
        //    A helper function that takes the current row from the QueryReader
        // and hydrates a MembershiUser from the values. Called by the 
        // MembershipUser.GetUser implementation.
        //

        private MembershipUser GetUserFromReader(QueryConnection conn, QueryReader reader)
        {
            bool isOracle = conn.isDBTypeOracle();
            object providerUserKey = reader.GetValue(0);
            if (isOracle && providerUserKey is string)
            {
                providerUserKey = new Guid((string)providerUserKey);
            }
            string username = reader.GetString(1);
            string email = reader.GetString(2);

            string passwordQuestion = "";
            if (reader.GetValue(3) != DBNull.Value)
                passwordQuestion = reader.GetString(3);

            string comment = "";
            if (reader.GetValue(4) != DBNull.Value)
                comment = reader.GetString(4);

            bool isApproved = reader.IsDBNull(5) ? false : reader.GetBoolean(5);
            bool isLockedOut = reader.IsDBNull(6) ? false : reader.GetBoolean(6);
            DateTime creationDate = reader.GetDateTime(7);

            DateTime lastLoginDate = new DateTime();
            if (reader.GetValue(8) != DBNull.Value)
                lastLoginDate = reader.GetDateTime(8);

            DateTime lastActivityDate = reader.IsDBNull(9) ? DateTime.MinValue : reader.GetDateTime(9);
            DateTime lastPasswordChangedDate = reader.IsDBNull(10) ? DateTime.MinValue : reader.GetDateTime(10);

            DateTime lastLockedOutDate = new DateTime();
            if (!reader.IsDBNull(11))
                lastLockedOutDate = reader.GetDateTime(11);

            DateTime windowStart = new DateTime();
            if (!reader.IsDBNull(12))
                windowStart = reader.GetDateTime(12);

            Guid accountId = Guid.Empty;
            if (!reader.IsDBNull(13))
                accountId = reader.GetGuid(13);

            QueryConnection conn2 = ConnectionPool.getConnection();
            try
            {
                // check for lockout windows passed
                PasswordPolicy policy = Database.getPasswordPolicy(conn2, Database.mainSchema, accountId);
                DateTime windowEnd = windowStart.AddMinutes(policy != null ? policy.failedLoginWindow : PasswordPolicy.FAILEDLOGINWINDOW);
                if (isLockedOut && DateTime.Now > windowEnd)
                {
                    UnlockUser(username);
                    isLockedOut = false;
                }
            }
            finally
            {
                ConnectionPool.releaseConnection(conn2);
            }

            MembershipUser u = new MembershipUser(this.Name,
                                                  username,
                                                  providerUserKey,
                                                  email,
                                                  passwordQuestion,
                                                  comment,
                                                  isApproved,
                                                  isLockedOut,
                                                  creationDate,
                                                  lastLoginDate,
                                                  lastActivityDate,
                                                  lastPasswordChangedDate,
                                                  lastLockedOutDate);

            return u;
        }


        //
        // MembershipProvider.UnlockUser
        //
        public override bool UnlockUser(string username)
        {
            QueryConnection conn = new QueryConnection(connectionString);
            bool isOracle = conn.isDBTypeOracle();
            string FailedPasswordAttemptCount = isOracle ? "FailedPwdAttemptCount" : "FailedPasswordAttemptCount";

            QueryCommand cmd = new QueryCommand("UPDATE " + Database.getTableName(Database.mainSchema, Database.USER_TABLE_NAME) +
                                              " SET IsLockedOut = 0, LastLockedOutDate = ?, " + FailedPasswordAttemptCount + " = 0" +
                                              " WHERE Username = ?", conn);

            cmd.Parameters.Add("@LastLockedOutDate", OdbcType.DateTime).Value = DateTime.Now;
            cmd.Parameters.Add("@Username", OdbcType.NVarChar, 255).Value = username.ToLower();

            int rowsAffected = 0;

            try
            {
                conn.Open();

                rowsAffected = cmd.ExecuteNonQuery();
            }
            catch (OdbcException e)
            {
                if (WriteExceptionsToEventLog)
                {
                    WriteToEventLog(e, "UnlockUser");
                    throw new ProviderException(exceptionMessage);
                }
                else
                {
                    throw e;
                }
            }
            finally
            {
                conn.Close();
            }

            if (rowsAffected > 0)
            {
                Global.systemLog.Warn("Unlocked user: " + username);
                return true;
            }
            Global.systemLog.Warn("Unlock user failed (database updated zero records): " + username);
            return false;
        }


        //
        // MembershipProvider.GetUserNameByEmail
        //

        public override string GetUserNameByEmail(string email)
        {
            QueryConnection conn = new QueryConnection(connectionString);
            QueryCommand cmd = new QueryCommand("SELECT Username" +
                  " FROM " + Database.getTableName(Database.mainSchema, Database.USER_TABLE_NAME) + " WHERE Email = ?", conn);

            cmd.Parameters.Add("@Email", OdbcType.VarChar, 128).Value = email;

            string username = "";

            try
            {
                conn.Open();
                username = (string)cmd.ExecuteScalar();
            }
            catch (OdbcException e)
            {
                if (WriteExceptionsToEventLog)
                {
                    WriteToEventLog(e, "GetUserNameByEmail");
                    throw new ProviderException(exceptionMessage);
                }
                else
                {
                    throw e;
                }
            }
            finally
            {
                conn.Close();
            }

            if (username == null)
                username = "";

            return username;
        }




        //
        // MembershipProvider.ResetPassword
        //

        public override string ResetPassword(string username, string answer)
        {
            string newPassword = System.Web.Security.Membership.GeneratePassword(newPasswordLength, MinRequiredNonAlphanumericCharacters); // dummy, temporary during reset operation

            ValidatePasswordEventArgs args = new ValidatePasswordEventArgs(username, newPassword, true);
            OnValidatingPassword(args);

            if (args.Cancel)
                if (args.FailureInformation != null)
                    throw args.FailureInformation;
                else
                    throw new MembershipPasswordException("Reset password canceled due to password validation failure.");

            QueryConnection conn = new QueryConnection(connectionString);
 
            int rowsAffected = 0;
            QueryReader reader = null;

            try
            {
                conn.Open();

                QueryCommand updateCmd = new QueryCommand("UPDATE " + Database.getTableName(Database.mainSchema, Database.USER_TABLE_NAME) +
                    " SET Password = ?" +
                    " WHERE Username = ? AND IsLockedOut = 0", conn);

                string hashAlgorithm = null;
                User u = Database.getUser(conn, Database.mainSchema, username);
                if (u != null)
                {
                    PasswordPolicy policy = Database.getPasswordPolicy(conn, Database.mainSchema, u.ManagedAccountId);
                    if (policy != null)
                        hashAlgorithm = policy.hashAlgorithm;
                }
                string pw = Password.HashPassword(newPassword, username, hashAlgorithm);
                updateCmd.Parameters.Add("@Password", OdbcType.VarChar, 1024).Value = pw;
                updateCmd.Parameters.Add("@Username", OdbcType.NVarChar, 255).Value = username.ToLower();

                rowsAffected = updateCmd.ExecuteNonQuery();
            }
            catch (OdbcException e)
            {
                if (WriteExceptionsToEventLog)
                {
                    WriteToEventLog(e, "ResetPassword");

                    throw new ProviderException(exceptionMessage);
                }
                else
                {
                    throw e;
                }
            }
            finally
            {
                if (reader != null) { reader.Close(); }
                conn.Close();
            }

            if (rowsAffected > 0)
            {
                return newPassword;
            }
            else
            {
                throw new MembershipPasswordException("User not found, or user is locked out. Password not Reset.");
            }
        }


        //
        // MembershipProvider.UpdateUser
        //
        public override void UpdateUser(MembershipUser user)
        {
            QueryConnection conn = new QueryConnection(connectionString);
            bool isOracle = conn.isDBTypeOracle();
            QueryCommand cmd = new QueryCommand("UPDATE " + Database.getTableName(Database.mainSchema, Database.USER_TABLE_NAME) +
                    " SET Email = ?, Comment = ?," +
                    " IsApproved = ?" +
                    " WHERE Username = ?", conn);

            cmd.Parameters.Add("@Email", OdbcType.VarChar, 128).Value = user.Email;
            cmd.Parameters.Add("@Comment", OdbcType.VarChar, 255).Value = user.Comment;
            cmd.Parameters.Add("@IsApproved", conn.getODBCBooleanType()).Value = conn.getBooleanValue(user.IsApproved);
            cmd.Parameters.Add("@Username", OdbcType.NVarChar, 255).Value = user.UserName.ToLower();

            try
            {
                conn.Open();

                cmd.ExecuteNonQuery();
            }
            catch (OdbcException e)
            {
                if (WriteExceptionsToEventLog)
                {
                    WriteToEventLog(e, "UpdateUser");

                    throw new ProviderException(exceptionMessage);
                }
                else
                {
                    throw e;
                }
            }
            finally
            {
                conn.Close();
            }
        }


        //
        // MembershipProvider.ValidateUser
        //
        public override bool ValidateUser(string username, string password)
        {
            bool isValid = false;

            QueryConnection conn = new QueryConnection(connectionString);
            bool isOracle = conn.isDBTypeOracle();           
            string FailedPasswordAttemptWindowStart = isOracle ? "FailedPwdAttemptWindowStart" : "FailedPasswordAttemptWindowStart";

            QueryCommand cmd = new QueryCommand("SELECT Password, IsApproved, IsLockedOut, " + FailedPasswordAttemptWindowStart + 
                ", MANAGED_ACCOUNT_ID FROM " + Database.getTableName(Database.mainSchema, Database.USER_TABLE_NAME) + " WHERE Username = ?", conn);

            cmd.Parameters.Add("@Username", OdbcType.NVarChar, 255).Value = username.ToLower();

            QueryReader reader = null;
            bool isApproved = false;
            string dbpassword = null;
            bool isLockedOut = false;
            DateTime windowStart = new DateTime();
            Guid accountId = Guid.Empty;

            try
            {
                conn.Open();
                reader = cmd.ExecuteReader(CommandBehavior.SingleRow);
                if (reader.HasRows)
                {
                    reader.Read();
                    dbpassword = reader.GetString(0);
                    if (!reader.IsDBNull(1))
                        isApproved = reader.GetBoolean(1);
                    if (!reader.IsDBNull(2))
                        isLockedOut = reader.GetBoolean(2);
                    if (!reader.IsDBNull(3))
                        windowStart = reader.GetDateTime(3);
                    if (!reader.IsDBNull(4))
                        accountId = reader.GetGuid(4);
                }
                else
                {
                    return false;
                }
            }
            catch (OdbcException e)
            {
                if (WriteExceptionsToEventLog)
                {
                    WriteToEventLog(e, "ValidateUser");
                    throw new ProviderException(exceptionMessage);
                }
                else
                {
                    throw e;
                }
            }
            finally
            {
                if (reader != null) { reader.Close(); }
            }

            try
            {
                // check for lockout windows passed
                PasswordPolicy policy = Database.getPasswordPolicy(conn, Database.mainSchema, accountId);
                DateTime windowEnd = windowStart.AddMinutes(policy != null ? policy.failedLoginWindow : PasswordPolicy.FAILEDLOGINWINDOW);
                if (isLockedOut && DateTime.Now > windowEnd)
                {
                    UnlockUser(username);
                    isLockedOut = false;
                }
                User u = Database.getUser(conn, Database.mainSchema, username, false);
                
                AccountDetails details = Database.getAccountDetails(conn, Database.mainSchema, accountId, u);              
                bool useRadius = false;
                bool checkpw = false;

                if (details != null && details.isRADIUS())
                {
                    string radiusHost = details.getRADIUSHost();
                    string radiusSecret = details.getRADIUSSharedSecret();
                    if (radiusHost != null && radiusSecret != null)
                    {
                        useRadius = true;
                        checkpw = validateRADIUSuser(radiusHost, radiusSecret, username, password);
                    }
                    else
                    {
                        Global.systemLog.Warn("AuthType set to RADIUS for account, but host/secret null; using default authentication - " + accountId);
                    }
                }

                if (!useRadius)
                {
                    checkpw = Password.VerifyPassword(password, dbpassword, username);
                }
                if (checkpw)
                {
                    if (isApproved)
                    {
                        isValid = true;
                    }
                }
                else
                {
                    UpdateFailureCount(username, "password");
                }
            }
            catch (OdbcException e)
            {
                if (WriteExceptionsToEventLog)
                {
                    WriteToEventLog(e, "ValidateUser");
                    throw new ProviderException(exceptionMessage);
                }
                else
                {
                    throw e;
                }
            }
            finally
            {   
                conn.Close();
            }
            return isValid;
        }

        /**
         * validate against a RADIUS server
         */
        private bool validateRADIUSuser(string host, string secret, string username, string password)
        {
            try
            {
                RadiusClient rc = new RadiusClient(host, secret);
                RadiusPacket authPacket = rc.Authenticate(username, password);
                RadiusPacket receivedPacket = rc.SendAndReceivePacket(authPacket);
                if (receivedPacket == null)
                {
                    Global.systemLog.Warn("Unable to connect to the RADIUS server - " + host);
                    return false;
                }

                switch (receivedPacket.Type)
                {
                    case RadiusPacketType.ACCESS_ACCEPT:
                        return true;
                    default:
                        return false;
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Warn(ex.Message, ex);
            }
            return false;
        }

        //
        // UpdateFailureCount
        //   A helper method that performs the checks and updates associated with
        // password failure tracking.
        //

        private void UpdateFailureCount(string username, string failureType)
        {
            QueryConnection conn = new QueryConnection(connectionString);
            bool isOracle = conn.isDBTypeOracle();
            string FailedPasswordAttemptCount = isOracle ? "FailedPwdAttemptCount" : "FailedPasswordAttemptCount";
            string FailedPasswordAttemptWindowStart = isOracle ? "FailedPwdAttemptWindowStart" : "FailedPasswordAttemptWindowStart";
            QueryCommand cmd = new QueryCommand("SELECT " + FailedPasswordAttemptCount + ", " +
                                                FailedPasswordAttemptWindowStart + 
                                              "  FROM " + Database.getTableName(Database.mainSchema, Database.USER_TABLE_NAME) + 
                                              "  WHERE Username = ?", conn);

            cmd.Parameters.Add("@Username", OdbcType.NVarChar, 255).Value = username.ToLower();

            QueryReader reader = null;
            DateTime windowStart = new DateTime();
            int failureCount = 0;

            try
            {
                conn.Open();

                PasswordPolicy policy = null;
                User u = Database.getUser(conn, Database.mainSchema, username, false);
                if (u != null)
                {
                    policy = Database.getPasswordPolicy(conn, Database.mainSchema, u.ManagedAccountId);
                }

                reader = cmd.ExecuteReader(CommandBehavior.SingleRow);

                if (reader.HasRows)
                {
                    reader.Read();
                    failureCount = reader.IsDBNull(0) ? 0 : reader.GetInt32(0);
                    windowStart = reader.IsDBNull(1) ? DateTime.Now : reader.GetDateTime(1);
                }

                reader.Close();

                DateTime windowEnd = windowStart.AddMinutes(policy != null ? policy.failedLoginWindow : PasswordPolicy.FAILEDLOGINWINDOW);

                if (failureCount == 0 || DateTime.Now > windowEnd)
                {
                    // First password failure or outside of PasswordAttemptWindow. 
                    // Start a new password failure count from 1 and a new window starting now.

                    cmd.CommandText = "UPDATE " + Database.getTableName(Database.mainSchema, Database.USER_TABLE_NAME) +
                                      "  SET " + FailedPasswordAttemptCount + " = ?, " +
                                         FailedPasswordAttemptWindowStart + " = ? " +
                                      "  WHERE Username = ?";

                    cmd.Parameters.Clear();

                    cmd.Parameters.Add("@Count", OdbcType.Int).Value = 1;
                    cmd.Parameters.Add("@WindowStart", OdbcType.DateTime).Value = Database.truncateDateTime(DateTime.Now);
                    cmd.Parameters.Add("@Username", OdbcType.NVarChar, 255).Value = username.ToLower();

                    if (cmd.ExecuteNonQuery() < 0)
                        throw new ProviderException("Unable to update failure count and window start.");
                }
                else
                {
                    if (++failureCount >= (policy != null ? policy.maxFailedLoginAttempts : PasswordPolicy.MAXFAILEDLOGINATTEMPTS))
                    {
                        // Password attempts have exceeded the failure threshold. Lock out the user.
                        cmd.CommandText = "UPDATE " + Database.getTableName(Database.mainSchema, Database.USER_TABLE_NAME) +
                                          "  SET IsLockedOut = ?, LastLockedOutDate = ?, " + FailedPasswordAttemptCount + " = ?" +
                                          "  WHERE Username = ?";
                        cmd.Parameters.Clear();
                        cmd.Parameters.Add("@IsLockedOut", conn.getODBCBooleanType()).Value = conn.getBooleanValue(true);
                        cmd.Parameters.Add("@LastLockedOutDate", OdbcType.DateTime).Value = Database.truncateDateTime(DateTime.Now);
                        cmd.Parameters.Add("@Count", OdbcType.Int).Value = failureCount;
                        cmd.Parameters.Add("@Username", OdbcType.NVarChar, 255).Value = username.ToLower();

                        if (cmd.ExecuteNonQuery() < 0)
                            throw new ProviderException("Unable to lock out user.");
                        Global.systemLog.Warn("Locking user due to too many failed login attempts: " + username);
                    }
                    else
                    {
                        // Password attempts have not exceeded the failure threshold. Update the failure counts. Leave the window the same.
                        cmd.CommandText = "UPDATE " + Database.getTableName(Database.mainSchema, Database.USER_TABLE_NAME) +
                                          "  SET " + FailedPasswordAttemptCount + " = ?" +
                                          "  WHERE Username = ?";
                        cmd.Parameters.Clear();
                        cmd.Parameters.Add("@Count", OdbcType.Int).Value = failureCount;
                        cmd.Parameters.Add("@Username", OdbcType.NVarChar, 255).Value = username.ToLower();

                        if (cmd.ExecuteNonQuery() < 0)
                            throw new ProviderException("Unable to update failure count.");
                    }
                }
            }
            catch (OdbcException e)
            {
                if (WriteExceptionsToEventLog)
                {
                    WriteToEventLog(e, "UpdateFailureCount");

                    throw new ProviderException(exceptionMessage);
                }
                else
                {
                    throw e;
                }
            }
            finally
            {
                if (reader != null) { reader.Close(); }
                conn.Close();
            }
        }

        //
        // MembershipProvider.FindUsersByName
        //
        public override MembershipUserCollection FindUsersByName(string usernameToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            QueryConnection conn = new QueryConnection(connectionString);
            bool isOracle = conn.isDBTypeOracle();
            string FailedPasswordAttemptWindowStart = isOracle ? "FailedPwdAttemptWindowStart" : "FailedPasswordAttemptWindowStart";

            QueryCommand cmd = new QueryCommand("SELECT Count(*) FROM " + Database.getTableName(Database.mainSchema, Database.USER_TABLE_NAME) +
                      "WHERE Username LIKE ?", conn);
            cmd.Parameters.Add("@UsernameSearch", OdbcType.NVarChar, 255).Value = usernameToMatch.ToLower();
            MembershipUserCollection users = new MembershipUserCollection();
            QueryReader reader = null;
            try
            {
                conn.Open();
                totalRecords = Int32.Parse(cmd.ExecuteScalar().ToString());
                if (totalRecords <= 0) { return users; }

                cmd.CommandText = "SELECT PKID, Username, Email, PasswordQuestion," +
                  " Comment, IsApproved, IsLockedOut, CreationDate, LastLoginDate," +
                  " LastActivityDate, LastPasswordChangedDate, LastLockedOutDate, " + FailedPasswordAttemptWindowStart + ", MANAGED_ACCOUNT_ID" +
                  " FROM " + Database.getTableName(Database.mainSchema, Database.USER_TABLE_NAME) +
                  " WHERE Username LIKE ? " +
                  " ORDER BY Username Asc";

                reader = cmd.ExecuteReader();
                int counter = 0;
                int startIndex = pageSize * pageIndex;
                int endIndex = startIndex + pageSize - 1;

                while (reader.Read())
                {
                    if (counter >= startIndex)
                    {
                        MembershipUser u = GetUserFromReader(conn, reader);
                        users.Add(u);
                    }
                    if (counter >= endIndex) { cmd.Cancel(); }
                    counter++;
                }
            }
            catch (OdbcException e)
            {
                if (WriteExceptionsToEventLog)
                {
                    WriteToEventLog(e, "FindUsersByName");

                    throw new ProviderException(exceptionMessage);
                }
                else
                {
                    throw e;
                }
            }
            finally
            {
                if (reader != null) { reader.Close(); }

                conn.Close();
            }

            return users;
        }

        //
        // MembershipProvider.FindUsersByEmail
        //
        public override MembershipUserCollection FindUsersByEmail(string emailToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            QueryConnection conn = new QueryConnection(connectionString);
            bool isOracle = conn.isDBTypeOracle();
            string FailedPasswordAttemptWindowStart = isOracle ? "FailedPwdAttemptWindowStart" : "FailedPasswordAttemptWindowStart";

            QueryCommand cmd = new QueryCommand("SELECT Count(*) FROM " + Database.getTableName(Database.mainSchema, Database.USER_TABLE_NAME) +
                                              "WHERE Email LIKE ?", conn);
            cmd.Parameters.Add("@EmailSearch", OdbcType.VarChar, 255).Value = emailToMatch;

            MembershipUserCollection users = new MembershipUserCollection();
            QueryReader reader = null;
            totalRecords = 0;
            try
            {
                conn.Open();
                totalRecords = (int)cmd.ExecuteScalar();
                if (totalRecords <= 0) { return users; }
                cmd.CommandText = "SELECT PKID, Username, Email, PasswordQuestion," +
                         " Comment, IsApproved, IsLockedOut, CreationDate, LastLoginDate," +
                         " LastActivityDate, LastPasswordChangedDate, LastLockedOutDate, " + FailedPasswordAttemptWindowStart + ", MANAGED_ACCOUNT_ID" +
                         " FROM " + Database.getTableName(Database.mainSchema, Database.USER_TABLE_NAME) +
                         " WHERE Email LIKE ?" +
                         " ORDER BY Username Asc";

                reader = cmd.ExecuteReader();
                int counter = 0;
                int startIndex = pageSize * pageIndex;
                int endIndex = startIndex + pageSize - 1;
                while (reader.Read())
                {
                    if (counter >= startIndex)
                    {
                        MembershipUser u = GetUserFromReader(conn, reader);
                        users.Add(u);
                    }

                    if (counter >= endIndex) { cmd.Cancel(); }

                    counter++;
                }
            }
            catch (OdbcException e)
            {
                if (WriteExceptionsToEventLog)
                {
                    WriteToEventLog(e, "FindUsersByEmail");

                    throw new ProviderException(exceptionMessage);
                }
                else
                {
                    throw e;
                }
            }
            finally
            {
                if (reader != null) { reader.Close(); }

                conn.Close();
            }

            return users;
        }

        //
        // WriteToEventLog
        //   A helper function that writes exception detail to the event log. Exceptions
        // are written to the event log as a security measure to avoid private database
        // details from being returned to the browser. If a method does not return a status
        // or boolean indicating the action succeeded or failed, a generic exception is also 
        // thrown by the caller.
        //
        private void WriteToEventLog(Exception e, string action)
        {
            EventLog log = new EventLog();
            log.Source = eventSource;
            log.Log = eventLog;

            string message = "An exception occurred communicating with the data source.\n\n";
            message += "Action: " + action + "\n\n";
            message += "Exception: " + e.ToString();

            log.WriteEntry(message);
        }
    }
}