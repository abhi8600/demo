﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.Odbc;
using Performance_Optimizer_Administration;
using System.Collections.Generic;
using System.Web.SessionState;
using Acorn.Utils;
using Acorn.DBConnection;

namespace Acorn
{
    public partial class LoadControl : System.Web.UI.UserControl
    {
        private static string mainSchema = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["MainSchema"];
        private static string createtimecmd = System.Web.Configuration.WebConfigurationManager.AppSettings["CreateTimeCommand"];
        private static string contentDir = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["ContentDirectory"];

        // For automatic compatibility with Advanced Mode the following both should be true
        public static bool GENERATE_ONLY_BOTTOM_LEVEL_TABLES =
            bool.Parse((string)System.Web.Configuration.WebConfigurationManager.AppSettings["GenerateOnlyBottomLevelTables"]);
        // No shared children
        private static bool GENERATE_SIMPLE_HIERARCHIES =
            bool.Parse((string)System.Web.Configuration.WebConfigurationManager.AppSettings["GenerateSimpleHierarchies"]);

        private MasterPage master;
        private bool allowCreateDashboards;
        private int loadNumber;
        private string loadGroup;
        private bool loadButtonOnly;
        protected string back = "Load.aspx";

        public MasterPage Master
        {
            set
            {
                this.master = value;
            }
        }

        public bool CreateDashboards
        {
            get
            {
                return allowCreateDashboards;
            }
            set
            {
                this.allowCreateDashboards = value;
            }
        }

        public int LoadNumber
        {
            get
            {
                return loadNumber;
            }
            set
            {
                loadNumber = value;
            }
        }

        public string LoadGroup
        {
            get
            {
                return loadGroup;
            }
            set
            {
                this.loadGroup = value;
            }
        }

        public bool LoadButtonOnly
        {
            get
            {
                return loadButtonOnly;
            }
            set
            {
                this.loadButtonOnly = value;
            }
        }

        public string Back
        {
            get
            {
                return back;
            }
            set
            {
                this.back = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            OverviewDashboardOption.Visible = allowCreateDashboards;
            if (IsPostBack)
            {
                ButtonPanel.Visible = false;
                if (LoadingPanel.Visible)
                {
                    Status.StatusResult status = (Status.StatusResult)Session["status"];
                    if (status != null && status.code == Status.StatusCode.LoadWarehouse)
                    {
                        DetailLink.NavigateUrl = String.Format("NewPublishLog.aspx?load={0}&date={1:MM-dd-yyyy}&group={2}&back={3}", loadNumber + 1, DateTime.Now, loadGroup, back);
                        DetailLink.Visible = true;
                    }
                }
                else if (LoadPanel.Visible && loadButtonOnly)
                {
                    ButtonPanel.Visible = true;
                    LoadOptionsPlaceholder.Visible = false;
                    LoadNavigationPlaceholder.Visible = false;
                    WizardPanel.CssClass = null;
                }
                return;
            }

            UnableToGetStatus.Visible = false;
            Space sp = (Space)Session["space"];
            if (Request.Params["Autopublish"] != null && sp.Automatic && sp.LoadNumber == 0)
            {
                DateTime loadDate = DateTime.Now;
                Session["autopublish"] = true;
                Status.StatusResult sr = new Status.StatusResult(Status.StatusCode.BuildingDashboards);
                Session["status"] = sr;
                if (!publish(sp, loadDate))
                {
                    Session.Remove("autopublish");
                    Session["status"] = new Status.StatusResult(Status.StatusCode.Ready);
                }
                return;
            }
            MainAdminForm maf = (MainAdminForm)Session["MAF"];
            bool setCreateDashboards = maf.quickDashboards == null || maf.quickDashboards.Count == 0;
            if (!setCreateDashboards)
            {
                bool found = false;
                foreach (QuickDashboard qd in maf.quickDashboards)
                {
                    if (qd.DashboardName == BuildDashboards.OverviewDashboardName)
                    {
                        found = true;
                        break;
                    }
                }
                setCreateDashboards = !found;
            }
            OverviewDashboardBox.Checked = setCreateDashboards && allowCreateDashboards;
            CreateOverviewLabel.Visible = setCreateDashboards;
            RecreateOverviewLabel.Visible = !CreateOverviewLabel.Visible && allowCreateDashboards;
            bool fetchOldWay = true;

            Status.StatusResult updatedResultFromExternalScheduler = null;
            if (Util.useExternalSchedulerForProcess(sp, maf))
            {
                updatedResultFromExternalScheduler = setRunning2(Session, maf, sp, mainSchema, false);
                fetchOldWay = updatedResultFromExternalScheduler != null && Status.isRunningCode(updatedResultFromExternalScheduler.code) ? false : true;
            }
            
            if(fetchOldWay)
            {   
                // if status is retrieved from both the external scheduler as well as the old, then resolve the use case
                // where status from scheduler returns failed while old way does not. It can happen in number of cases but 
                // a false alarm is sent if the scheduled process had failed while the next manual one is successful. 
                // Resolve that by ignoring the status with the old timestamp
                if (Status.isFailedStatusObsolete(sp, loadGroup, updatedResultFromExternalScheduler))
                {
                    ErrorPanel.Visible = false;
                    ScriptFailurePanel.Visible = false;
                }

                setRunning(maf, sp, mainSchema);
            }
            LoadNumLabel.Text = HttpUtility.HtmlEncode(loadNumber.ToString());
            LoadDateBox_CalendarExtender.SelectedDate = DateTime.Now;
            QueryConnection conn = null;
            try
            {
                conn = ConnectionPool.getConnection();
                setPublishHistory(sp, conn);
            }
            finally
            {
                ConnectionPool.releaseConnection(conn);
            }
            if (LoadPanel.Visible && loadButtonOnly)
            {
                LoadOptionsPlaceholder.Visible = false;
                LoadNavigationPlaceholder.Visible = false;
                WizardPanel.CssClass = null;
                ButtonPanel.Visible = true;
            }
        }

        public void setPublishHistory(Space sp, QueryConnection conn)
        {
            HttpSessionState session = getSession();
            DataTable dt = Status.getPublishHistory(session, sp, loadNumber + 1, loadGroup, false, true, 20);
            DataTable txnHistory = Status.getPublishStartHistory(session, sp, loadNumber + 1, loadGroup);

            if (dt.Rows.Count > 0)
            {
                LastPublishPanel.Visible = true;
                dt.Columns.Add("Back");
                dt.Columns.Add("PublishStartTime", typeof(DateTime));
                dt.Columns.Add("PublishEndTime", typeof(DateTime));
                List<Publish> publishes = Database.getPublishes(conn, mainSchema, sp.ID);
                foreach (DataRow dr in dt.Rows)
                {
                    dr["Back"] = back;
                    int ln = (int)dr["LoadNumber"];
                    bool found = false;
                    foreach (Publish p in publishes)
                    {
                        if (p.LoadNumber == ln)
                        {
                            dr["PublishDate"] = p.LoadDate;
                            if (p.StartPublishDate == DateTime.MinValue)
                            {
                                // see if you can get it from txn_command_history  
                                DateTime dateTime = ProcessLoad.getStartDateFromTxnCommandHistory(txnHistory, ln);
                                if (dateTime != DateTime.MinValue)
                                {
                                    dr["PublishStartTime"] = dateTime;
                                }
                            }
                            else
                            {
                                dr["PublishStartTime"] = p.StartPublishDate;
                            }
                            dr["PublishEndTime"] = p.PublishDate;
                            found = true;
                            break;
                        }
                    }
                    if (!found)
                    {
                        dr["PublishDate"] = dr["DateTime"];
                        // see if you can get it from txn_command_history                       
                        DateTime txnStartTime = ProcessLoad.getStartDateFromTxnCommandHistory(txnHistory, ln);
                        if (txnStartTime != DateTime.MinValue)
                        {
                            dr["PublishStartTime"] = txnStartTime;
                        }
                        DateTime publishEndTime = Status.getPublishEndTime(sp, ln, loadGroup);
                        if (publishEndTime != DateTime.MinValue)
                        {
                            dr["PublishEndTime"] = publishEndTime;
                        }
                        else
                        {
                            dr["PublishEndTime"] = dr["DateTime"];
                        }                        
                    }
                }
                PublishHistory.DataSource = dt;
                PublishHistory.DataBind();
            }
            else
                LastPublishPanel.Visible = false;
        }

        public string frameSource = null;

        protected void loadButton_Click(object sender, EventArgs e)
        {
            ErrorPanel.Visible = false;
            ScriptFailurePanel.Visible = false;
            ButtonPanel.Visible = false;
            MainAdminForm maf = (MainAdminForm)Session["MAF"];
            if (maf == null)
                return;
            Space sp = (Space)Session["space"];
            bool canpublish = SpaceAdminService.checkSpacePublishability(maf, sp);
            if (!canpublish)
            {
                LoadPanel.Visible = false;
                LoadingPanel.Visible = false;
                InvalidPanel.Visible = true;
                InvalidLink.NavigateUrl = "~/FlexModule.aspx?birst.module=admin";
                return;
            }
            DateTime loadDate = DateTime.Now;
            if (!loadButtonOnly)
            {
                try
                {
                    loadDate = DateTime.Parse(LoadDateBox.Text);
                }
                catch (FormatException)
                {
                    return;
                }
            }
            publish(sp, loadDate);
            ButtonPanel.Visible = false;
        }

        public ApplicationLoader startProcess(Space sp, DateTime loadDate)
        {
            try
            {
                Util.logoutSMIWebSession(Session, sp, Request);
                Status.StatusResult sr = null;
                bool fetchOldWay = true;
                MainAdminForm maf = (MainAdminForm)Session["MAF"];
                if (Util.useExternalSchedulerForProcess(sp, maf))
                {
                    sr = Status.getStatusUsingExternalScheduler(sp, loadNumber, loadGroup, false);
                    fetchOldWay = sr != null && Status.isRunningCode(sr.code) ? false : true;
                }
                
                if(fetchOldWay)
                {
                    sr = Status.getLoadStatus(Session, sp, loadNumber, loadGroup);
                }

                if ((OverviewDashboardBox.Checked ? sr.code != Status.StatusCode.BuildingDashboards : sr.code != Status.StatusCode.Started) && Status.isRunningCode(sr.code))
                    return null;
                // Make sure the application is fully built
                if (maf == null)
                    return null;
                string enginecmd = Util.getEngineCommand(sp);
                log4net.ThreadContext.Properties["user"] = ((User)Session["user"]).Username;
                using (log4net.ThreadContext.Stacks["itemid"].Push(sp.ID.ToString()))
                {
                    Global.userEventLog.Info("PUBLISH");
                }
                ApplicationLoader al = new ApplicationLoader(maf, sp, loadDate, mainSchema, enginecmd, createtimecmd, contentDir,
                    this, (User)Session["user"], loadGroup, loadNumber + 1);
                Acorn.ApplicationLoader.LoadAuthorization la = al.getLoadAuthorization(loadGroup);
                if (la == ApplicationLoader.LoadAuthorization.RowLimitExceeded || la == ApplicationLoader.LoadAuthorization.RowPublishLimitExceeded)
                {
                    if (la == ApplicationLoader.LoadAuthorization.RowLimitExceeded)
                        RowLimitLabel.Visible = true;
                    else
                        RowPublishLimitLabel.Visible = true;
                    InvalidLink.NavigateUrl = Util.getHomePageURL(Session, Request);
                    return null;
                }
                else if (la == ApplicationLoader.LoadAuthorization.DataLimitExceeded || la == ApplicationLoader.LoadAuthorization.DataPublishLimitExceeded)
                {
                    if (la == ApplicationLoader.LoadAuthorization.DataLimitExceeded)
                        DataLimitLabel.Visible = true;
                    else
                        DataPublishLimitLabel.Visible = true;
                    InvalidLink.NavigateUrl = Util.getHomePageURL(Session, Request);
                    return null;
                }
                return al;
            }
            catch (Exceptions.SchedulerException sEx)
            {
                Global.systemLog.Error("Exception while connecting to scheduler", sEx);
                return null;
            }
        }

        private bool publish(Space sp, DateTime loadDate)
        {
            int spaceOpID = SpaceOpsUtils.getSpaceAvailability(sp);
            bool spaceAvailable = SpaceOpsUtils.isSpaceAvailable(spaceOpID);
            ApplicationLoader al = startProcess(sp, loadDate);
            if (al == null || ApplicationLoader.isSpaceBeingPublished(sp) || !spaceAvailable)
            {
                LoadPanel.Visible = false;
                LoadingPanel.Visible = false;
                InvalidPanel.Visible = true;
                InvalidLabel.Visible = false;
                if (al != null)
                {
                    if (ApplicationLoader.isSpaceBeingPublished(sp))
                    {
                        // Separate Error for concurrent space processing                    
                        ConcurrentProcessError.Visible = true;
                    }
                    else if (!spaceAvailable)
                    {
                        SpaceUnavailableError.Visible = true;
                        SpaceUnavailableError.Text = HttpUtility.HtmlEncode("Cannot publish space, " + SpaceOpsUtils.getAvailabilityMessage(null, spaceOpID));
                    }

                    if(back != null && back.Length > 0)
                    {
                        InvalidLink.NavigateUrl = back;
                    }
                }
                return false;
            }        

            MainAdminForm maf = (MainAdminForm)Session["MAF"];
            if (OverviewDashboardBox.Checked)
            {
                AggRulePanel.Visible = true;
                LoadPanel.Visible = false;
                LoadingPanel.Visible = false;
                Session["loader"] = al;
                BuildDashboards bd = null;
                al.BuildDashboards = bd;
                ScanBuffer scanBuff = null;
                Acorn.BuildDashboards.EvaluatedMeasure[] emlist = null;
                if (Session["scanbuff"] == null)
                {
                    bd = new BuildDashboards(maf, sp.Directory + "\\data");
                    scanBuff = new ScanBuffer();
                    al.ScanBuff = scanBuff;
                    emlist = bd.getMeasures(sp.Directory + "\\data", scanBuff);
                }
                else
                {
                    scanBuff = (ScanBuffer)Session["scanbuff"];
                    bd = (BuildDashboards)Session["builddashboards"];
                    emlist = bd.getMeasures(sp.Directory + "\\data", scanBuff);
                    Session.Remove("scanbuff");
                    Session.Remove("builddashboards");
                }
                if (emlist.Length > 0)
                {
                    al.BuildDashboards = bd;
                    DataTable dt = new DataTable();
                    dt.Columns.Add("Name");
                    dt.Columns.Add("SumVisible", typeof(bool));
                    dt.Columns.Add("SumChecked", typeof(bool));
                    dt.Columns.Add("AvgVisible", typeof(bool));
                    dt.Columns.Add("AvgChecked", typeof(bool));
                    dt.Columns.Add("CountVisible", typeof(bool));
                    dt.Columns.Add("CountChecked", typeof(bool));
                    foreach (BuildDashboards.EvaluatedMeasure em in emlist)
                    {
                        DataRow dr = dt.NewRow();
                        dr[0] = em.sc.Name;
                        dr[1] = (em.sc.TargetAggregations != null) && (Array.IndexOf<string>(em.sc.TargetAggregations, "SUM") >= 0);
                        dr[2] = (bool)dr[1];
                        dr[5] = (em.sc.TargetAggregations != null) && (Array.IndexOf<string>(em.sc.TargetAggregations, "COUNT") >= 0);
                        dr[6] = (bool)dr[5] && !((bool)dr[2]);
                        dr[3] = (em.sc.TargetAggregations != null) && (Array.IndexOf<string>(em.sc.TargetAggregations, "AVG") >= 0);
                        dr[4] = (bool)dr[3] && !((bool)dr[2] || (bool)dr[6]);
                        dt.Rows.Add(dr);
                    }
                    MeasureList.DataSource = dt;
                    MeasureList.DataBind();
                    return true;
                }
            }
            Status.StatusResult sr = new Status.StatusResult(OverviewDashboardBox.Checked ? Status.StatusCode.BuildingDashboards : Status.StatusCode.Started);
            Session["status"] = sr;
            if (Util.useExternalSchedulerForManualProcessing(sp, maf))
            {
                al.load(OverviewDashboardBox.Checked, null, false, false, false, false);
                SchedulerUtils.schedulePublishJob(sp, Util.getSessionUser(Session), OverviewDashboardBox.Checked, al.getLoadNumber(), al.getLoadGroup(), al.getLoadDate(), al.SubGroups, false, false);
            }
            else
            {
                al.load(OverviewDashboardBox.Checked);
            }
            
            setProgressLabel(sr);
            LoadPanel.Visible = false;
            LoadingPanel.Visible = true;
            AggRulePanel.Visible = false;
            return true;
        }

        protected void ContinuePublish(object sender, EventArgs e)
        {
            ApplicationLoader al = (ApplicationLoader)Session["loader"];
            Session.Remove("loader");
            Space sp = Util.getSessionSpace(Session);
            if (al == null || ApplicationLoader.isSpaceBeingPublished(sp))
                return;
            if (OverviewDashboardBox.Checked)
            {
                BuildDashboards bd = al.BuildDashboards;
                Acorn.BuildDashboards.EvaluatedMeasure[] emlist = bd.getMeasures(sp.Directory + "\\data", al.ScanBuff);
                int count = 0;
                foreach (ListViewItem lvi in MeasureList.Items)
                {
                    string rule = null;
                    if (((RadioButton)lvi.FindControl("SumButton")).Checked)
                        rule = "SUM";
                    else if (((RadioButton)lvi.FindControl("AvgButton")).Checked)
                        rule = "AVG";
                    else if (((RadioButton)lvi.FindControl("CountButton")).Checked)
                        rule = "COUNT";
                    if (rule != null)
                        emlist[count].aggregationRule = rule;
                    else
                        emlist[count].ignore = true;
                    count++;
                }
            }
            Status.StatusResult sr = new Status.StatusResult(OverviewDashboardBox.Checked ? Status.StatusCode.BuildingDashboards : Status.StatusCode.Started);
            Session["status"] = sr;
            if (Util.useExternalSchedulerForManualProcessing(sp, Util.getSessionMAF(Session)))
            {
                al.load(OverviewDashboardBox.Checked, null, false, false, false, false);
                SchedulerUtils.schedulePublishJob(sp, Util.getSessionUser(Session), OverviewDashboardBox.Checked, al.getLoadNumber(), al.getLoadGroup(), al.getLoadDate(), al.SubGroups, al.retryFailedLoad, false);
            }
            {
                al.load(OverviewDashboardBox.Checked);
            }
            LoadPanel.Visible = false;
            LoadingPanel.Visible = true;
            AggRulePanel.Visible = false;
        }

        private static void clearLevel(Level l)
        {
            l.GenerateDimensionTable = false;
            if (l.Keys[0].SurrogateKey)
            {
                List<LevelKey> lklist = new List<LevelKey>();
                foreach (LevelKey lk in l.Keys)
                    if (!lk.SurrogateKey)
                        lklist.Add(lk);
                l.Keys = lklist.ToArray();
            }
        }

        private static List<StagingColumn> getStagingColumns(MainAdminForm maf, string dimName)
        {
            List<StagingColumn> sclist = new List<StagingColumn>();
            foreach (StagingTable st in maf.stagingTableMod.getAllStagingTables())
            {
                bool match = false;
                foreach (string[] level in st.Levels)
                {
                    if (level[0] == dimName)
                    {
                        match = true;
                        break;
                    }
                }
                if (match)
                {
                    foreach (StagingColumn sc in st.Columns)
                        if (Array.IndexOf<string>(sc.TargetTypes, dimName) >= 0)
                            sclist.Add(sc);
                }
            }
            return (sclist);
        }

        protected void LoadTimer_Tick(object sender, EventArgs e)
        {
            Space sp = Util.getSessionSpace(Session);
            MainAdminForm maf = Util.getSessionMAF(Session);
            String mainSchema = Util.getMainSchema();
            bool fetchOldWay = true;
            Status.StatusResult updatedResultFromExternalScheduler = null;
            if (Util.useExternalSchedulerForProcess(sp, maf))
            {
                updatedResultFromExternalScheduler = setRunning2(Session, maf, sp, mainSchema, true);
                fetchOldWay = updatedResultFromExternalScheduler != null && Status.isRunningCode(updatedResultFromExternalScheduler.code) ? false : true;
            }

            if(fetchOldWay)
            {
                if (Status.isFailedStatusObsolete(sp, loadGroup, updatedResultFromExternalScheduler))
                {
                    ErrorPanel.Visible = false;
                    ScriptFailurePanel.Visible = false;
                }

                setRunning(maf, sp, mainSchema);
            }
        }

        public HttpSessionState getSession()
        {
            return Session;
        }

        private void setProgressLabel(Status.StatusResult sr)
        {
            PhaseLabel.Text = HttpUtility.HtmlEncode(Status.getPhaseLabel(sr, OverviewDashboardBox.Checked));
        }

        public void setRunning(MainAdminForm maf, Space sp, string mainSchema)
        {
            Status.StatusResult status = (Status.StatusResult)Session["status"];
            // By this time status should be set by the parent control
            if (status == null)
            {
                return;
            }
            if (status != null && status.code == Status.StatusCode.Failed)
            {
                bool currentLoadGroupFailed = true;
                Status.StatusStep ss = null;
                if (status.substeps != null)
                    foreach (Status.StatusStep step in status.substeps)
                    {
                        if (step.result == Status.SCRIPTFAILURE)
                        {
                            ss = step;
                            break;
                        }
                    }
                if (ss == null || status.loadid != loadNumber + 1)
                {
                    // Maybe status is from a different load group
                    Session.Remove("status");
                    Status.StatusResult sr = Status.getLoadStatus(Session, sp, loadNumber + 1, loadGroup);
                    // Update the session object with the right loadGroup status.
                    // Recheck the status. If the status still says failed, then continue.
                    Session["status"] = sr;
                    if (sr != null & sr.code != Status.StatusCode.Failed)
                    {
                        // the failed status is from a different Load Group and not from the load group under consideration. 
                        // There is a use case when Acorn status is failed and user goes into Salesforce screen.
                        // To prevent the failed status of Acorn to show on Salesforce/other loadgroup screen, flag it.
                        currentLoadGroupFailed = false;
                    }

                    // continue if it is the right load group
                    if (currentLoadGroupFailed)
                    {
                        if (sr.substeps != null)
                            foreach (Status.StatusStep sss in sr.substeps)
                            {
                                if (sss.result == Status.SCRIPTFAILURE)
                                {
                                    ss = sss;
                                    break;
                                }
                            }
                    }
                }
                if (currentLoadGroupFailed)
                {
                    if (ss == null)
                    {
                        if (status.message != null && status.message.Length > 0)
                            ErrorLabel.Text = HttpUtility.HtmlEncode(status.message);
                        ErrorPanel.Visible = true;
                    }
                    else
                    {
                        ScriptFailurePanel.Visible = true;
                        ScriptErrorMessage.Text = HttpUtility.HtmlEncode(ss.message);
                        foreach (StagingTable st in maf.stagingTableMod.getAllStagingTables())
                        {
                            if (ss.substep == loadGroup + ": " + st.Name)
                            {
                                string s = st.SourceFile;
                                int index = s.LastIndexOf('.');
                                if (index >= 0)
                                    s = s.Substring(0, index);
                                ScriptErrorSourceLabel.Text = HttpUtility.HtmlEncode(s);
                                break;
                            }
                        }
                    }
                }
            }
            // Get whether anything is still running
            if (Status.isRunningCode(status.code))
            {
                Status.StatusResult sr = Status.getLoadStatus(Session, sp, loadNumber + 1, loadGroup);
                if (sr.code == Status.StatusCode.Complete)
                {
                    QueryConnection conn = null; ;
                    try
                    {
                        conn = ConnectionPool.getConnection();
                        ApplicationLoader.updateLoadSuccess(maf, sp, mainSchema, conn, sr, loadNumber, loadGroup);
                        setPublishHistory(sp, conn);
                    }
                    catch (Exception ex)
                    {
                        Global.systemLog.Error(ex);
                        try
                        {
                            ApplicationLoader.updateLoadSuccess(maf, sp, mainSchema, conn, sr, loadNumber, loadGroup);
                            setPublishHistory(sp, conn);
                        }
                        catch (Exception ex2)
                        {
                            Global.systemLog.Error(ex2);
                        }
                    }
                    finally
                    {
                        ConnectionPool.releaseConnection(conn);
                    }
                    Session["status"] = sr;
                    if (Session["autopublish"] != null)
                    {
                        Session.Remove("autopublish");
                        if (maf.quickDashboards != null && maf.quickDashboards.Count > 0)
                            DefaultUserPage.redirectToDashboard(Session, Response, Server, null, Request, sp);
                        else
                            DefaultUserPage.redirectToAdhoc(Session, Response, Server, null);
                    }
                }
                else if (sr.code == Status.StatusCode.Failed)
                {
                    Global.systemLog.Error("Load failed, space: " + sp.ToString());
                    Session["status"] = sr;
                }
                else if (status.code == Status.StatusCode.BuildingDashboards && sr.code == Status.StatusCode.Ready)
                {
                    sr = status;
                }
                setProgressLabel(sr);
                if (sr.code != Status.StatusCode.None)
                {
                    status = sr;
                }
            }
            else
            {
                if (status.code == Status.StatusCode.Complete)
                {
                    QueryConnection conn = null;
                    try
                    {
                        conn = ConnectionPool.getConnection();
                        ApplicationLoader.updateLoadSuccess(maf, sp, mainSchema, conn, status, loadNumber, loadGroup);
                        setPublishHistory(sp, conn);
                    }
                    finally
                    {
                        ConnectionPool.releaseConnection(conn);
                    }
                }
                if (status.code == Status.StatusCode.Ready || status.code == Status.StatusCode.Complete)
                {
                    LoadingPanel.Visible = false;
                    LoadPanel.Visible = !LoadingPanel.Visible;
                    InvalidPanel.Visible = false;
                    if (LoadPanel.Visible && loadButtonOnly)
                    {
                        ButtonPanel.Visible = true;
                        LoadOptionsPlaceholder.Visible = false;
                        LoadNavigationPlaceholder.Visible = false;
                    }
                    LoadNumLabel.Text = HttpUtility.HtmlEncode(sp.LoadNumber.ToString());
                    QueryConnection conn = null;
                    try
                    {
                        conn = ConnectionPool.getConnection();
                        setPublishHistory(sp, conn);
                    }
                    finally
                    {
                        ConnectionPool.releaseConnection(conn);
                    }
                    if (Session["autopublish"] != null)
                    {
                        Session.Remove("autopublish");
                        if (maf.quickDashboards != null && maf.quickDashboards.Count > 0)
                            DefaultUserPage.redirectToDashboard(Session, Response, Server, null, Request, sp);
                        else
                            DefaultUserPage.redirectToAdhoc(Session, Response, Server, null);
                    }
                    return;
                }
            }
            LoadingPanel.Visible = Status.isRunningCode(status.code);
            LoadPanel.Visible = !LoadingPanel.Visible;
            if (LoadPanel.Visible && loadButtonOnly)
            {
                ButtonPanel.Visible = true;
                LoadOptionsPlaceholder.Visible = false;
                LoadNavigationPlaceholder.Visible = false;
                WizardPanel.CssClass = null;
            }
            InvalidPanel.Visible = false;
            LoadNumLabel.Text = HttpUtility.HtmlEncode(sp.LoadNumber.ToString());
        }


        private void processFailedStatus2(HttpSessionState session, MainAdminForm maf, Space sp, Status.StatusResult status, int loadNumber, string loadGroup)
        {
            Status.StatusStep ss = null;
            if (status != null && status.code == Status.StatusCode.Failed)
            {
                if (status.substeps != null)
                    foreach (Status.StatusStep sss in status.substeps)
                    {
                        if (sss.result == Status.SCRIPTFAILURE)
                        {
                            ss = sss;
                            break;
                        }
                    }


                if (ss == null)
                {
                    if (status.message != null && status.message.Length > 0)
                        ErrorLabel.Text = HttpUtility.HtmlEncode(status.message);
                    ErrorPanel.Visible = true;
                }
                else
                {
                    ScriptFailurePanel.Visible = true;
                    ScriptErrorMessage.Text = HttpUtility.HtmlEncode(ss.message);
                    foreach (StagingTable st in maf.stagingTableMod.getAllStagingTables())
                    {
                        if (ss.substep == loadGroup + ": " + st.Name)
                        {
                            string s = st.SourceFile;
                            int index = s.LastIndexOf('.');
                            if (index >= 0)
                                s = s.Substring(0, index);
                            ScriptErrorSourceLabel.Text = HttpUtility.HtmlEncode(s);
                            break;
                        }
                    }
                }
            }
        }

        public Status.StatusResult setRunning2(HttpSessionState session, MainAdminForm maf, Space sp, string mainSchema, bool fetchTxnCommandHistoryDetails)
        {
            Status.StatusResult status = new Status.StatusResult(Status.StatusCode.None);
            try
            {
                status = Status.getStatusUsingExternalScheduler(sp, -1, loadGroup, fetchTxnCommandHistoryDetails);

                if (status != null && status.code == Status.StatusCode.Failed)
                {
                    processFailedStatus2(Session, maf, sp, status, loadNumber, loadGroup);
                }

                setProgressLabel(status);
                if (!Status.isRunningCode(status.code))
                {
                    QueryConnection conn = null;
                    try
                    {
                        conn = ConnectionPool.getConnection();
                        setPublishHistory(sp, conn);
                    }
                    finally
                    {
                        ConnectionPool.releaseConnection(conn);
                    }
                }

                LoadingPanel.Visible = Status.isRunningCode(status.code);
                LoadPanel.Visible = !LoadingPanel.Visible;
                if (LoadPanel.Visible && loadButtonOnly)
                {
                    ButtonPanel.Visible = true;
                    LoadOptionsPlaceholder.Visible = false;
                    LoadNavigationPlaceholder.Visible = false;
                    WizardPanel.CssClass = null;
                }
                InvalidPanel.Visible = false;
                LoadNumLabel.Text = HttpUtility.HtmlEncode(ProcessLoad.getLoadNumber(sp, loadGroup).ToString());
               
                /*
                if (session != null)
                {
                    session["status"] = status;
                }
                 * */
            }
            catch (Exceptions.SchedulerException ex)
            {
                Global.systemLog.Error("Exception while setting status ", ex);
                UnableToGetStatus.Visible = true;
                InvalidLink.NavigateUrl = back;
            }

            return status;
        }
    }
}