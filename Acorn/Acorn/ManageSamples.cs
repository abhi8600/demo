﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.SessionState;
using Performance_Optimizer_Administration;
using Acorn.TrustedService;

namespace Acorn
{
    public class ManageSamples
    {
        public static ManageSamplesResult getSampleMetadata(HttpSessionState session)
        {
            MainAdminForm maf = (MainAdminForm)session["MAF"];
            if (maf == null)
                return null;
            Space sp = (Space)session["space"];
            if (sp == null)
                return null;
            User u = (User)session["user"];
            if (u == null)
                return null;
            ManageSamplesResult msr = null;
            // Make sure that user and group information is updated.
            SpaceConfig sc = Util.getSpaceConfiguration(sp.Type);
            string localprotocol = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["LocalProtocol"];
            TrustedService.TrustedService ts = new Acorn.TrustedService.TrustedService();
            // 30 Minute timeout
            ts.Timeout = 30 * 60 * 1000;
            ts.Url = localprotocol + sc.LocalURL + "/SMIWeb/services/TrustedService";
            msr = ts.getSampleMetadata(sp.Directory, u.Username, sp.ID.ToString());
            if (!msr.successSpecified || !msr.success)
            {
                Global.systemLog.Info("TrustedService:ManageSamples failed: " + msr.errorMessage + " (" + msr.errorCode + ')');
            }
            return msr;
        }

        public static ManageSamplesResult createSample(HttpSessionState session, string dimension, string level, string source, double percent)
        {
            MainAdminForm maf = (MainAdminForm)session["MAF"];
            if (maf == null)
                return null;
            Space sp = (Space)session["space"];
            if (sp == null)
                return null;
            User u = (User)session["user"];
            if (u == null)
                return null;
            ManageSamplesResult msr = null;
            // Make sure that user and group information is updated.
            SpaceConfig sc = Util.getSpaceConfiguration(sp.Type);
            string localprotocol = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["LocalProtocol"];
            TrustedService.TrustedService ts = new Acorn.TrustedService.TrustedService();
            // 30 Minute timeout
            ts.Timeout = 30 * 60 * 1000;
            ts.Url = localprotocol + sc.LocalURL + "/SMIWeb/services/TrustedService";
            msr = ts.createSample(sp.Directory, u.Username, sp.ID.ToString(), dimension, level, source, percent, true);
            if (!msr.successSpecified || !msr.success)
            {
                Global.systemLog.Info("TrustedService:ManageSamples failed: " + msr.errorMessage + " (" + msr.errorCode + ')');
            }
            return msr;
        }

        public static ManageSamplesResult applySample(HttpSessionState session, string dimension, string level, string source)
        {
            MainAdminForm maf = (MainAdminForm)session["MAF"];
            if (maf == null)
                return null;
            Space sp = (Space)session["space"];
            if (sp == null)
                return null;
            User u = (User)session["user"];
            if (u == null)
                return null;
            ManageSamplesResult msr = null;
            // Make sure that user and group information is updated.
            SpaceConfig sc = Util.getSpaceConfiguration(sp.Type);
            string localprotocol = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["LocalProtocol"];
            TrustedService.TrustedService ts = new Acorn.TrustedService.TrustedService();
            // 30 Minute timeout
            ts.Timeout = 30 * 60 * 1000;
            ts.Url = localprotocol + sc.LocalURL + "/SMIWeb/services/TrustedService";
            msr = ts.applySample(sp.Directory, u.Username, sp.ID.ToString(), dimension, level, source);
            if (!msr.successSpecified || !msr.success)
            {
                Global.systemLog.Info("TrustedService:ManageSamples failed: " + msr.errorMessage + " (" + msr.errorCode + ')');
            }
            return msr;
        }

        public static ManageSamplesResult removeSample(HttpSessionState session, string dimension, string level, string source)
        {
            MainAdminForm maf = (MainAdminForm)session["MAF"];
            if (maf == null)
                return null;
            Space sp = (Space)session["space"];
            if (sp == null)
                return null;
            User u = (User)session["user"];
            if (u == null)
                return null;
            ManageSamplesResult msr = null;
            // Make sure that user and group information is updated.
            SpaceConfig sc = Util.getSpaceConfiguration(sp.Type);
            string localprotocol = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["LocalProtocol"];
            TrustedService.TrustedService ts = new Acorn.TrustedService.TrustedService();
            // 30 Minute timeout
            ts.Timeout = 30 * 60 * 1000;
            ts.Url = localprotocol + sc.LocalURL + "/SMIWeb/services/TrustedService";
            msr = ts.removeSample(sp.Directory, u.Username, sp.ID.ToString(), dimension, level, source);
            if (!msr.successSpecified || !msr.success)
            {
                Global.systemLog.Info("TrustedService:ManageSamples failed: " + msr.errorMessage + " (" + msr.errorCode + ')');
            }
            return msr;
        }

        public static ManageSamplesResult deleteSamples(HttpSessionState session)
        {
            MainAdminForm maf = (MainAdminForm)session["MAF"];
            if (maf == null)
                return null;
            Space sp = (Space)session["space"];
            if (sp == null)
                return null;
            User u = (User)session["user"];
            if (u == null)
                return null;
            ManageSamplesResult msr = null;
            // Make sure that user and group information is updated.
            SpaceConfig sc = Util.getSpaceConfiguration(sp.Type);
            string localprotocol = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["LocalProtocol"];
            TrustedService.TrustedService ts = new Acorn.TrustedService.TrustedService();
            // 30 Minute timeout
            ts.Timeout = 30 * 60 * 1000;
            ts.Url = localprotocol + sc.LocalURL + "/SMIWeb/services/TrustedService";
            msr = ts.deleteSamples(sp.Directory, u.Username, sp.ID.ToString());
            if (!msr.successSpecified || !msr.success)
            {
                Global.systemLog.Info("TrustedService:ManageSamples failed: " + msr.errorMessage + " (" + msr.errorCode + ')');
            }

            //Set last modified date for all staging tables to current so that next time data is processed, all staging tables would be reloaded.
            List<StagingTable> stList = maf.stagingTableMod.getStagingTables();
            if ((stList != null) && (stList.Count > 0))
            {
                foreach (StagingTable st in stList)
                {
                    st.LastModifiedDate = DateTime.UtcNow;
                }
            }
            Acorn.Util.saveApplication(maf, sp, session, null);

            return msr;
        }

        public static ManageSamplesResult deriveSample(HttpSessionState session, string sourceDimension, string sourceLevel, string targetDimension, string targetLevel, string source)
        {
            MainAdminForm maf = (MainAdminForm)session["MAF"];
            if (maf == null)
                return null;
            Space sp = (Space)session["space"];
            if (sp == null)
                return null;
            User u = (User)session["user"];
            if (u == null)
                return null;
            ManageSamplesResult msr = null;
            // Make sure that user and group information is updated.
            SpaceConfig sc = Util.getSpaceConfiguration(sp.Type);
            string localprotocol = (string)System.Web.Configuration.WebConfigurationManager.AppSettings["LocalProtocol"];
            TrustedService.TrustedService ts = new Acorn.TrustedService.TrustedService();
            // 30 Minute timeout
            ts.Timeout = 30 * 60 * 1000;
            ts.Url = localprotocol + sc.LocalURL + "/SMIWeb/services/TrustedService";
            msr = ts.deriveSample(sp.Directory, u.Username, sp.ID.ToString(), sourceDimension, sourceLevel, targetDimension, targetLevel, source);
            if (!msr.successSpecified || !msr.success)
            {
                Global.systemLog.Info("TrustedService:ManageSamples failed: " + msr.errorMessage + " (" + msr.errorCode + ')');
            }
            return msr;
        }

    }
}