﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Xml.Linq;
using System.Data.Odbc;
using System.Collections.Generic;
using System.Runtime.Remoting.Contexts;
using System.Xml.Serialization;
using System.Xml;
using Performance_Optimizer_Administration;

namespace Acorn
{
    /// <summary>
    /// Summary description for CommandWebService
    /// </summary>
    [WebService(Namespace = "http://www.birst.com/",
        Description = "A webservice which performs provisioning and execution for Birst")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class InterServerWebService : System.Web.Services.WebService
    {
        [WebMethod(Description = "Kill processing initiated by another web server.  Arguments are the spaceID and username.")]
        public string KillRunningProcess(string spaceID, string username)
        {
            string UserAddress = Context.Request.UserHostAddress;
            List<string> allowedIPs = Util.getTrustedInterServerIPs();
            if (!Util.IsValidIP(UserAddress, allowedIPs))
            {
	            Global.systemLog.Warn("KillRunningProcess: bad request IP: " + UserAddress);
                return "Unable to stop running process for space: " + spaceID;
            }
            Global.systemLog.Warn("KillRunningProcess: valid request IP: " + UserAddress);
            return Acorn.ProcessHandler.killRunningProcess(spaceID, username, false, true);
        }

        [WebMethod(Description = "Return the package details for a given child space.")]
        public string getPackagesDetails(string childSpaceDirectory)
        {   
            int errorCode = 0;
            string errorMessage = null;
            XElement packageDetailsElement = new XElement("PackageDetails"); ;
            try
            {
                string UserAddress = Context.Request.UserHostAddress;
                List<string> allowedIPs = Util.getTrustedInterServerIPs();
                if (!Util.IsValidIP(UserAddress, allowedIPs))
                {
                    Global.systemLog.Warn("getPackagesDetails: bad request IP: " + UserAddress);
                    errorCode = -1;
                    errorMessage = "bad request IP : " + UserAddress;
                }
                Global.systemLog.Debug("getPackagesDetails: valid request IP: " + UserAddress);
                Space sp = Util.getSpaceByDirectory(childSpaceDirectory);
                bool newRepository = false;
                MainAdminForm maf = Util.loadRepository(sp, out newRepository);
                List<PackageSpaceProperties> psProps = Util.getImportedPackageSpaces(sp, maf);
                if (psProps != null && psProps.Count > 0)
                {
                    foreach (PackageSpaceProperties pSpace in psProps)
                    {
                        XElement psElement = new XElement("PackageSpace",
                            new XElement("PackageID", pSpace.PackageID),
                            new XElement("Name", pSpace.PackageName),
                            new XElement("SpaceID", pSpace.SpaceID),
                            new XElement("SpaceName", pSpace.SpaceName),
                            new XElement("SpaceDirectory", pSpace.SpaceDirectory));
                        packageDetailsElement.Add(psElement);
                    }
                }
            }
            catch (Exception ex)
            {
                errorCode = -1;
                errorMessage = ex.Message;
            }

            XElement result =
                  new XElement("Result",
                          new XElement("Error",
                              new XElement("ErrorCode", errorCode),
                              new XElement("ErrorMessage", errorMessage)),
                              packageDetailsElement);
            return result.ToString();
        }
    }
}
