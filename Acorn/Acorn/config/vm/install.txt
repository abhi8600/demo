
Copyright (C) 2007-2011 Birst, Inc. All rights reserved.
BIRST PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.

In order to deploy the Birst VM you need to make the following modifications to the configuration files in .../Acorn/config/).

a) mailsettings.config
	- change BIRSTEMAILFROMHOST to the host that you want the email from the Birst servers to appear to come from
	- change BIRSTSMTPHOST to the host that is running your SMTP server
b) overrides.config
	- change BIRSTEMAILFROMHOST to the host that you want the email from the Birst servers to appear to come from
	- change BIRSTREPOSITORYDIRECTORY to the name of a UNC folder that can be accessed from the VM and from the database server on BIRSTDBHOST (see below)
c) spaces.config
    - change BIRSTREPOSITORYDIRECTORY to the name of a UNC folder that can be accessed from the VM and from the database server on BIRSTDBHOST (see below)
   
By default, all database connections are to SQL Server (using the SQL Native Client from C# and JDBC from Java) at the default port (1433).

There are 3 databases: 
	- BirstAdmin (administration information: list of users, list of spaces, permissions, etc.)
	- BirstAudit (audit logs)
	- BirstData (data warehouses, initially contains a demo 'space')
	
To change the above database connection information you will need to change one or more of the following configuration files:
	- connections.config (BirstAdmin connection information)
	- overrides.config (BirstAudit database name)
	- log4j.xml (BirstAudit connection information)
	- log4net.config (BirstAdmin connection information)
	- Spaces.config (BirstData connection information)
	
	Change BIRSTDBHOST
	       BIRSTDBUSER
	       BIRSTDBPASSWORD
	       
Attach the DBs

All Birst software, logs, and data outside of the database are located in d:\birst

	- data (meta data, SFDC configuration, load logs, uploaded data), d:\birst\data
    - logs (all servers), d:\birst\logs
    - software
    	- d:\birst\Acorn
    	- d:\birst\SMIWeb   
    	
Birst uses RECAPTCHA (http://www.google.com/recaptcha) to stop spammers from creating new accounts or resetting passwords.
The VM is configured to use the birst.com RECAPTCHA public and private keys. We recommend that you get your own keys and
use them (change them in RecoverPassword.aspx and TrialUserSignup.aspx).

The VM is running Windows Server 2003 and is set up with a single user account:
	username: birstuser
	password: birst@123
	
The Birst Web Server is listening on port 6105, so the URL is http://ipaddress:6105/Login.aspx

The initial configuration in the databases is:

- Birst accounts
		1 account
			Name of account: �account�
		1 ops user
			Username: �operations�
			Password: birst@123
			Email: operations@local.local
			Products: Admin Base, Birst Connect, Live Access, Web Services, Mobile and SAP Connector
			Repository admin flag set to true
		1 account admin
			Account admin of �account�
			Username: �accountadmin�
			Password: birst@123
			Email: accountadmin@local.local
			Products: Admin Base, Birst Connect, Live Access, Web Services, Mobile and SAP Connector
			Repository admin flag set to true
		1 end user
			Username: �user�
			Password: birst@123
			Email: user@local.local
			Products: User Base
			Member of account �default�

- Space types
		 �default�

- Releases
		Front end: 4.5.0
		Processing engine: 4.5.0

- Demo space
		Space owner: �accountadmin�
		Data: Grocery Sales
		Type: Advanced
		�user� has Designer and Dashboard access
