﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Acorn
{
    /// <summary>
    /// Handle BCrypt password hashing
    /// </summary>
    public class BCryptHash
    {
        public static string CreateHash(string password, int cost)
        {
            string salt = BCrypt.Net.BCrypt.GenerateSalt(cost);
            return BCrypt.Net.BCrypt.HashPassword(password, salt);
        }

        public static bool ValidatePassword(string password, string hash)
        {
            return BCrypt.Net.BCrypt.Verify(password, hash);
        }
    }
}