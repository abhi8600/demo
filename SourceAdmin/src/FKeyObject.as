package
{
	import flash.geom.Point;
	
	import generated.webservices.AdminDataSource;
	import generated.webservices.ImportedItem;
	import generated.webservices.Level;
	
	import mx.controls.Text;
	
	public class FKeyObject
	{
		public var fklabel: Text;
		public var pk: Point;
		public var fk: Point;
		public var pkads: AdminDataSource;
		public var fkads: AdminDataSource;
		public var level: Level;
		public var ii: ImportedItem;
		public var complex: Boolean;
		public var hasParent: Boolean;
		
		public function FKeyObject()
		{
		}
	}
}