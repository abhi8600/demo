package
{
	public class HistoryResult
	{
        public var tm: Number;
        public var step: String;
        public var substep: String;
        public var loadnumber: int;
        public var numrows: int;
        public var numerrors: int;
        public var numwarnings: int;
        public var duration: int;
        
		public function HistoryResult()
		{
		}
	}
}