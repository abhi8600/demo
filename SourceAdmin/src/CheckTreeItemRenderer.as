package
{
	import flash.events.MouseEvent;
	
	import generated.webservices.ArrayOfArrayOfString;
	import generated.webservices.ArrayOfString;
	import generated.webservices.Level;
	
	import mx.controls.CheckBox;
	import mx.controls.Tree;
	import mx.controls.treeClasses.TreeItemRenderer;
	import mx.controls.treeClasses.TreeListData;

	public class CheckTreeItemRenderer extends TreeItemRenderer
	{
		private var cb: CheckBox;
		
		public function CheckTreeItemRenderer(): void
		{
			super();
		}
		
		override protected function createChildren():void
		{
   			super.createChildren();
			cb = new CheckBox();
			cb.setStyle( "verticalAlign", "middle" );
			cb.addEventListener(MouseEvent.CLICK, checkboxClick);
			addChild(cb);
		}
		
		private function getCurrentLevel(gh: GrainHierarchies): Array
		{
       		var tld: TreeListData = TreeListData(super.listData);
       		var hx: XML;
       		while (hx == null || hx.parent() != null)
       		{
       			if (hx == null)
       				hx = XML(tld.item).parent();
       			else
       				hx = hx.parent();	
       		}
       		var hname: String = hx.@Name;
       		var lname: String = XML(tld.item).@Name;
       		return [hname, lname];
		}
   
   		private function checkboxClick(event: MouseEvent): void
   		{
       		var gh: GrainHierarchies = GrainHierarchies(this.document);
       		var curlevel: Array = getCurrentLevel(gh);
       		var newGrain: ArrayOfArrayOfString = new ArrayOfArrayOfString();
       		var updated: Boolean = false;
       		var toRemove: int = -1;
       		var count: int = 0;
       		for each(var level: ArrayOfString in gh.context.ads.Levels)
       		{
       			if (level[0] == curlevel[0])
       			{
       				if (cb.selected)
       				{
       					level[1] = curlevel[1];
       				} else
       				{
       					toRemove = count;
       				}
       				updated = true;
       			}
       			count++;
       		}
       		if (toRemove >= 0)
       			gh.context.ads.Levels.removeItemAt(toRemove);
       		if (!updated && cb.selected)
       		{
       			var newLevel: ArrayOfString = new ArrayOfString();
       			newLevel.addItem(curlevel[0]);
       			newLevel.addItem(curlevel[1]);
       			gh.context.ads.Levels.addItem(newLevel);
       		}
       		SourceAdmin.saveDataSource(gh.context);
       		Tree(this.owner).invalidateList();
   		}
   
		override protected function updateDisplayList(unscaledWidth:Number, 
            unscaledHeight:Number):void 
		{
            super.updateDisplayList(unscaledWidth, unscaledHeight);
            if(super.data && super.icon && cb)
            {
            	if (TreeListData(super.listData).depth == 1)
            	{
            		cb.visible = false;
            	} else
            	{
            		icon.visible = false;
            		cb.selected = false;
            		cb.visible = true;
		       		var gh: GrainHierarchies = GrainHierarchies(this.document);
            		var curlevel: Array = getCurrentLevel(gh);
            		for each(var level: ArrayOfString in gh.context.ads.Levels)
            		{
            			if (level[0] == curlevel[0] && level[1] == curlevel[1])
            				cb.selected = true;
            		}
            		cb.x = super.icon.x;
            		cb.y = 8;
            		super.icon.x = cb.x + cb.width+17;            	
            		super.label.x = super.icon.x;// + super.icon.width + 3;            		
            	}
            }
        }
	}
}