package
{
	public class DataFlowValues
	{
		public static var DATASOURCE_COLOR: uint = 0xC6D1F7;
		public static var DATASOURCE_BORDER: uint = 0xC0C1C4;
		public static var DATASOURCE_SCRIPT_COLOR: uint = 0xD7C1F4;
		public static var DATASOURCE_SCRIPT_BORDER: uint = 0xB3B0B7;
		public static var DATASOURCE_IMPORTED_COLOR: uint = 0xE9FFE6;
		public static var DATASOURCE_IMPORTED_BORDER: uint = 0xDCE0DC;
		public static var FACT_COLOR: uint = 0xF9D7DB;
		public static var FACT_BORDER: uint = 0xC6C1C2;
		public static var LEVEL_COLOR: uint = 0xC8EFBF;
		public static var LEVEL_BORDER: uint = 0xC3C9C3;
		public static var DIMENSION_COLOR: uint = 0xF7F7F7;
		public static var DIMENSION_BORDER: uint = 0xE5E5E5;
		public static var DISABLED_COLOR: uint = 0xF2F2F2;
		public static var DISABLED_BORDER: uint = 0xE5E5E5;
		public static var COMPLEX_JOIN_MARKER: uint = 0xAA0033;
	}
}