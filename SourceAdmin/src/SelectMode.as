package
{
	public final class SelectMode
	{
		public static const NONE: int = 0;
		public static const DIMENSION: int = 1;
		public static const DIMENSION_TABLE: int = 2;
		public static const MEASURES: int = 3;
		public static const MEASURE_TABLE: int = 4;
		public static const JOINS: int = 5;
		
		public function SelectMode(): void
		{
		}
	}
}