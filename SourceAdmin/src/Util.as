package
{
	import com.birst.flex.core.CoreApp;
	
	import flash.net.URLRequest;
	import flash.net.navigateToURL;
	
	import mx.core.Application;
	import mx.resources.ResourceManager;
	import mx.utils.StringUtil;
		
	public class Util
	{
		public static var DEFAULT_DB_CONNECTION: String = "Default Connection";
		public static var MEMAGGREGATES_DB_CONNECTION: String = "In-memory Aggregate";
		public static var multsp: RegExp = /[ ]{2,}/g;
		
		private static var initialized:Boolean = false;

		// for source/staging entities
		public static var newQueryLangBadChars:RegExp;
		public static var newQueryLangGoodChars:RegExp;
		public static var oldQueryLangBadChars:RegExp = /[^\w\.@\-_\$#%\*&:;\/<>,=+| ]/g;
		public static var oldQueryLangGoodChars:RegExp = /[\w\.@\-_\$#%\*&:;\/<>,=+| ]+/g;
		//String representation of supported chars to show in UI message
		public static var NEW_LANG_CHARS: String = "a-zA-Z0-9:$#%&!^@,;<>/- and <space>";
		public static var OLD_LANG_CHARS: String = "a-zA-Z0-9.@_$#%*&:;/<>,=+|- and <space>";
		
		// for attribute/measure entities (to allow () in names)
		public static var newQueryLangBadChars2:RegExp;
		public static var newQueryLangGoodChars2:RegExp;
		public static var oldQueryLangBadChars2:RegExp = /[^\w\.@\-_\$#%\*&:;\/()<>,=+| ]/g;
		public static var oldQueryLangGoodChars2:RegExp = /[\w\.@\-_\$#%\*&:;\/()<>,=+| ]+/g;
		//String representation of supported chars to show in UI message
		public static var NEW_LANG_CHARS_2: String = "a-zA-Z0-9:$#%&!^@,;<>/- and <space>";
		public static var OLD_LANG_CHARS_2: String = "a-zA-Z0-9.@_$#%*&:;/<>(),=+|- and <space>";
		
		// Different user modes
		public static const USER_MODE_DISCOVERY_DISABLED:int = 1;
		public static const USER_MODE_DISCOVERY_ONLY:int = 2;
		public static const USER_MODE_DISCOVERY_HYBRID:int = 3;
 

		public static const SPACE_MODE_AUTOMATIC:int = 1;
		public static const SPACE_MODE_ADVANCED:int = 2;
		public static const SPACE_MODE_DISCOVERY:int = 3; 
		
		{
			initialize();
		}
				
		public static function generateRangeForUnicodeCharacters(var1:Object, var2:Object):String
		{
   			return String.fromCharCode(var1) + "-" + String.fromCharCode(var2);
		}
		
		public static function initialize(): void
		{
			if (initialized) 
				return;
			newQueryLangBadChars =  new RegExp("[^\\w\\s:\\$#%\\-&!^@,;><\\/" + generateRangeForUnicodeCharacters(0x0080, 0xFFCF) + "]", "g");
			newQueryLangGoodChars =  new RegExp("[\\w\\s:\\$#%\\-&!^@,;><\\/" + generateRangeForUnicodeCharacters(0x0080, 0xFFCF) + "]*", "g");

			newQueryLangBadChars2 = new RegExp("[^\\w\\s:\\$#%\\-&!^@,;><\\/" + generateRangeForUnicodeCharacters(0x0080, 0xFFCF) + "]", "g");
			newQueryLangGoodChars2 = new RegExp("[\\w\\s:\\$#%\\-&!^@,;><\\/" + generateRangeForUnicodeCharacters(0x0080, 0xFFCF) + "]*", "g");
			initialized = true;
		}

 		public static function generatePhysicalName(colName: String): String
        {
            if (colName.length == 0)
                return colName;
            var c: String = colName.charAt(0);
            if (c >= '0' && c <= '9')
            {
                colName = 'N' + colName;
            }
            
			var re: RegExp = /\W\u0080-\uFFCF/g;
            var p1: String = colName.replace('$', 'D').replace('#', 'N');
            var p2: String = p1.replace(re, "_");

            return p2;
        }
        
        public static function replaceUnSupportedChars(str: String, queryLanguageVersion: int): String
        {
        	initialize();
        	var unsupportedChars: RegExp = Util.newQueryLangBadChars;
			if (queryLanguageVersion == 0)
				unsupportedChars = Util.oldQueryLangBadChars;
        	return str.replace(unsupportedChars, "_");
        }
        
        public static function removeMulSpaces(str: String): String
        {
        	return str.replace(multsp," ");
        }
        
        // two versions of these routines, one set for source/staging column fields, one for custom measure/attribute/buckets measures
        public static function getSupportedCharsForQueryLanguage(queryLanguageVersion: int): String
        {
        	if (queryLanguageVersion == 0)
				return OLD_LANG_CHARS;
			return NEW_LANG_CHARS;
        }
        
        public static function getFirstMatchingSupportedChars(str: String, queryLanguageVersion: int): String
        {
        	initialize();
        	
        	var supportedChars: RegExp = Util.newQueryLangGoodChars;
			if (queryLanguageVersion == 0)
				supportedChars = Util.oldQueryLangGoodChars;
			var a: Array = str.match(supportedChars);
			if (a.length > 0)
			{
				return a[0];
			}
			return "";
        }

        public static function getSupportedCharsForQueryLanguage2(queryLanguageVersion: int): String
        {
        	if (queryLanguageVersion == 0)
				return OLD_LANG_CHARS_2;
			return NEW_LANG_CHARS_2;
        }
        
        public static function getFirstMatchingSupportedChars2(str: String, queryLanguageVersion: int): String
        {
        	initialize();
        
        	var supportedChars: RegExp = Util.newQueryLangGoodChars2;
			if (queryLanguageVersion == 0)
				supportedChars = Util.oldQueryLangGoodChars2;
			var a: Array = str.match(supportedChars);
			if (a.length > 0)
			{
				return a[0];
			}
			return "";
        }
        
        public static function isUserModeDiscoveryOnly(userMode:int):Boolean
        {
        	return userMode == Util.USER_MODE_DISCOVERY_ONLY ? true : false;
        }
        
        public static function isUserModeHybrid(userMode:int):Boolean
        {
        	return userMode == Util.USER_MODE_DISCOVERY_HYBRID ? true : false;
        }
        
        public static function isUserModeDiscoveryDisabled(userMode:int):Boolean
        {
        	return userMode == Util.USER_MODE_DISCOVERY_DISABLED ? true : false;
        }
        
        public static function isSpaceModeAdvanced(spaceMode:int):Boolean
        {
        	return spaceMode == Util.SPACE_MODE_ADVANCED ? true : false;
        }
        
        public static function isSpaceModeAutomatic(spaceMode:int):Boolean
        {
        	return spaceMode == Util.SPACE_MODE_AUTOMATIC ? true : false;
        }
        
        public static function isSpaceModeDiscovery(spaceMode:int):Boolean
        {
        	return spaceMode == Util.SPACE_MODE_DISCOVERY ? true : false;
        }
        
		public static function gotoHelp(topic:String, siteurl:String=null, window:String=null):void{
			var app:CoreApp = CoreApp(Application.application);
			if (app != null)
				app.trackEvent("help", topic);
			var helpUrl:String = null;
			if (siteurl != null) {
				helpUrl = StringUtil.substitute(siteurl, topic);
			} else {
				if (app != null && app.hasOwnProperty("isFreeTrial") && app["isFreeTrial"] == true)
				{
					helpUrl = ResourceManager.getInstance().getString('sharable','SH_HELP_BXE_URL', [ topic ]);
				}
				else
				{
					helpUrl = ResourceManager.getInstance().getString('sharable','SH_HELP_FULL_URL', [ topic ]);				
				}	
			}
			var helpWindow:String = null;
			if (window != null) {
				helpWindow = window;
			} else {
				helpWindow = ResourceManager.getInstance().getString('sharable','SH_HELP_WINDOW');
			}
			navigateToURL(new URLRequest(helpUrl), helpWindow);
		}
		
		public static function trim(s: String): String
		{
			var start: int = -1;
			var end: int = 0;
			for(var i: int = 0; i < s.length; i++)
			{
				var isSpace: Boolean = s.charAt(i) == ' ';
				if (start < 0 && !isSpace)
					start = i;
				if (!isSpace)
					end = i;
			}
			if (start < 0)
				start = 0;
			return s.substring(start, end+1);
		}
		
		public static function htmlDecode(text:String):String{
			text = text.split("&amp;").join("&");
			text = text.split("&gt;").join(">");
			text = text.split("&lt;").join("<");
			text = text.split("&#37;").join("%");
			text = text.split("&quot;").join("\"");
			return text;						
		}
		
		public static function urlDecode(text:String):String{
			text = text.split("%23").join("#");
			text = text.split("%24").join("$");
			text = text.split("%26").join("&");
			text = text.split("%2B").join("+");
			text = text.split("%2C").join(",");
			text = text.split("%2F").join("/");
			text = text.split("%3A").join(":");
			text = text.split("%3B").join(";");
			text = text.split("%3D").join("=");
			text = text.split("%3F").join("?");
			text = text.split("%40").join("@");
			return text;	
		}
		
		public static function getQuotedName(connType: String, item: String): String
		{
			if (item == null || StringUtil.trim(item).length == 0)
				return item;
	        if (item.indexOf(" ") >= 0 || item.indexOf("\\") >= 0 || item.indexOf("/") >= 0 || tablesRequireDoubleQuotes(connType))
	        {				
				if (Util.isBDTypeMySQL(connType))
					return "`" + item + "`";
				else
					return "\"" + item + "\"";
	        }
	        return item;
		}
		
		public static function isBDTypeMySQL(connType: String): Boolean
		{
			return (connType != null && (connType.toLowerCase() == "mysql" || connType.toLowerCase() == "infobright" || connType.toLowerCase() == "odbcmysql"))	
		}
		
		public static function tablesRequireDoubleQuotes(connType: String):Boolean
		{
			return (connType != null && (connType.toLowerCase() == "postgresql" || connType.toLowerCase() =="SAP Hana"));
		}
	}
}