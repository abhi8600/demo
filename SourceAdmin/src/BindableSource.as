package
{
	import generated.webservices.AdminDataSource;
	
	public class BindableSource
	{
		[Bindable]
		public var ads: AdminDataSource;
		
		public function BindableSource(ads: AdminDataSource)
		{
			this.ads = ads;
		}
	}
}