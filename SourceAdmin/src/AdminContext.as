package
{
	import generated.webservices.AdminDataSource;
	import generated.webservices.AdminRepository;
	import generated.webservices.AdminService;
	
	public class AdminContext
	{
		public var ar: AdminRepository;
		public var ads: AdminDataSource;
		public var svc: AdminService;
		public var gh: GrainHierarchies;
		public var sa: SourceAdmin;
		
		public function AdminContext(): void
		{
		}
	}
}