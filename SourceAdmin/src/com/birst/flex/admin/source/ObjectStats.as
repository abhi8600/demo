package com.birst.flex.admin.source
{
	public class ObjectStats
	{
		public var name: String;
		public var type: String;
		public var numRows: int;
		public var loadTime: int;

		public function ObjectStats()
		{
		}
	}
}