package com.birst.flex.admin.source
{
	import com.birst.flex.util.ServiceUtil;
	
	import generated.webservices.AdminRepository;
	import generated.webservices.ArrayOfArrayOfString;
	
	public class BrowserUploadOptions
	{
		public var consolidate:Boolean;
		public var firstRows:Boolean;
		public var filterRows:Boolean;
		public var lockData:Boolean;
		public var ignoreQuote:Boolean;
		public var beginSkip:String;
		public var endSkip:String;
		public var encodingIndex:int;
		public var encodingValue:String;
		public var escape:ArrayOfArrayOfString;
		public var separator:String;
		public var automodel:Boolean;		
		public var quoteCharacter:String;
		public var ignorecarriagereturn:Boolean;
		public var forceNumColumns:Boolean;
		
		public static var DEFAULT_ENCODING_VALUE:String = "Detect";
		public static var DISPLAY_ENCODING_UTF_16_LE:String = "UTF-16(Little Endian)";
		public static var DISPLAY_ENCODING_UTF_16_BE:String = "UTF-16(Big Endian)";		
		public function BrowserUploadOptions()
		{
		}
		
		public function setDefaultValues(_ar:AdminRepository):void
		{			
			// need to make it false owing to bug
			consolidate = true;
			
			if(!Util.isSpaceModeAutomatic(ServiceUtil.navObj.SpaceMode))
			{
				consolidate = false;
			}
	 		firstRows = true;
	 		filterRows = true;
	 		lockData = false;
	 		ignoreQuote = false;
	 		beginSkip = "0";
	 		endSkip = "0";
	 		encodingIndex = 0;
	 		encodingValue = DEFAULT_ENCODING_VALUE; 
	 		escape = new ArrayOfArrayOfString();
	 		separator = "";
	 		automodel = true;
			quoteCharacter = "\"";
	 		ignorecarriagereturn = false;
	 		forceNumColumns = false;
		}
		
		public function getEncodingType():String
		{
			if(encodingValue == DEFAULT_ENCODING_VALUE)
			{
				return "";
			}
			else if(encodingValue == DISPLAY_ENCODING_UTF_16_LE)
			{
				return "UTF-16LE";
			}
			else if(encodingValue == DISPLAY_ENCODING_UTF_16_BE)
			{
				return "UTF-16BE";
			}
			else
			{
				return encodingValue;
			}
		}
	}
}