package com.birst.flex.Hierarchies
{
	import mx.controls.treeClasses.TreeItemRenderer;
	public class HierarchyListItemRenderer extends TreeItemRenderer
	{
		public function HierarchyListItemRenderer()
		{
			super();
		}

		override public function set data(value:Object):void 
		{
			super.data = value;
			
			if (value is XML) 
			{	
				var xml:XML = null;
				if (data.localName() == "Level")
				{	
	    	   		while (xml == null || xml.parent() != null)
	       			{
	       				if (xml == null)
	       					xml = XML(data).parent();
	       				else
		       				xml = xml.parent();	
	    	   		}
				} else if (data.localName() == "Hierarchy")
				{
					xml = XML(data);
				}
				
				var imported:String = xml != null ? xml.@Imported : null;
				if (imported != null && imported == "true") {
					setStyle("color", 0xAAB3B3);
					setStyle("fontStyle", "italic");					
				}
				else 
				{
					setStyle("color", 0x000000);
					setStyle("fontStyle", "normal");
				}
			}
			else 
			{
				setStyle("color", 0x000000);
				setStyle("fontStyle", "normal");
			}
		}
	}
}