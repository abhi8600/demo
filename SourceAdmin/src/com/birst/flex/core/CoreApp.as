package com.birst.flex.core
{
	import com.google.analytics.AnalyticsTracker;
	import com.google.analytics.GATracker;
	
	import flash.events.Event;
	import flash.events.IEventDispatcher;
	import flash.events.KeyboardEvent;
	import flash.ui.Keyboard;
	
	import flash.external.ExternalInterface;
	
	import mx.collections.ArrayCollection;
	import mx.containers.TitleWindow;
	import mx.controls.Alert;
	import mx.controls.ComboBox;
	import mx.core.Application;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.events.ListEvent;
	import mx.events.ResourceEvent;
	import mx.managers.PopUpManager;
	import mx.resources.IResourceManager;
	import mx.resources.ResourceManager;    
	
	[ResourceBundle('sharable')]
	public class CoreApp extends mx.core.Application
	{
		// Google Analytics Tracking variables (see trackEvent below)
	    private static var _tracker:AnalyticsTracker = null;
	    private var _GAAccount:String = null;
	    private var _GADebug:Boolean = false;
		private var _GASegment:String = null;

		[Bindable]
		public var mainBundle:String;
		
		protected static var EN_US:String = "en_US";
		
		protected var _name:String;
		protected var _locale:String;
		
		private var _localePopUp:TitleWindow;
		private var _loadingLocale:String;
		
		[Bindable]
		public var helpEnable:Boolean = false;
		
		[Bindable]
		public var xsrfToken:String = null;
		
		public function CoreApp(){
			addEventListener(mx.events.FlexEvent.INITIALIZE, onInitCore, false, 9999);
			addEventListener(mx.events.FlexEvent.APPLICATION_COMPLETE, createTracker, false); 
		}
		
		protected function onInitCore(event:FlexEvent):void{
			//Attempt to get the name of our application. The mx.core.Application.application.name 
			//is normally something like 'Dashboards0', 'Dashboard1', etc.
			//Strip out the last digit and use that substring
			var regex:RegExp = /(.*)\d+$/;
			if (name){
				var result:Object = regex.exec(name);
				if (result && result is Array){
					var rst:Array = result as Array;
					_name = (rst.length > 1) ? rst[1] : (rst.length > 0) ? rst[0] : "";
				}
			}
			
			// set the xsrf token
			xsrfToken = ExternalInterface.call("getCookie", "XSRF-TOKEN");
		
			//set our locale
			_locale = this.parameters.locale ? this.parameters.locale : EN_US;
			
			//Check if locale is different than English US
			loadNonUSResourceModule();
			
			//Test locale dialog
			setLocaleDialogOnKeyPress();
		}
		
		// from FlexAdhoc Util.as
		public static function getApplicationGoogleAccount():String		
		{
			return Application.application.parameters['gaAccount'];	
		}
		
		public function createTracker(event:FlexEvent):void{
			try
			{
				if (_tracker == null)
				{
					_GAAccount = getApplicationGoogleAccount();
					if (_GAAccount != null && _GAAccount.length > 0)
						_tracker = new GATracker(this, _GAAccount, "AS3", _GADebug);
				}
				trackEvent("entry");
			}
			catch (e:Error)
			{}
		}	
		
		// XXX label not tracked
		public function trackEvent(category:String, action:String = null, label:String = null):void {
			if (_tracker)
			{
				try
				{
					if (this.hasOwnProperty("isFreeTrial") && this["isFreeTrial"] == true && _GASegment == null)
					{
						_GASegment = "BXE"
						_tracker.setVar(_GASegment);
					}				
					_tracker.trackEvent(this.birstAppName, category, action);
				}
				catch (e:Error)
				{}
			}
		}
		
		protected function loadNonUSResourceModule():void{
			var locale:String = getLocale();
			if (locale != EN_US){
				ResourceManager.getInstance().addEventListener(Event.CHANGE, localizeAlert);
				loadResourceModuleByLocale(locale);
			}		
		}
		
		protected function getLocaleResourceBundleUrl(locale:String):String{
			if (locale && birstAppName){
				return '/swf/' + locale + "_" + birstAppName + ".swf";
			}
			
			return null;			
		}
		
		public function loadResourceModuleByLocale(locale:String):void{
			_loadingLocale = locale;	
			if (locale == EN_US){
				onCompleteLoadLocaleResourceModule(null);
				return;
			}
			loadResourceModule(getLocaleResourceBundleUrl(locale));
		}
		
		public function loadResourceModule(url:String):void{
			if(url){
				var dispatcher:IEventDispatcher = resourceManager.loadResourceModule(url);	
				dispatcher.addEventListener(ResourceEvent.COMPLETE, onCompleteLoadLocaleResourceModule);
				dispatcher.addEventListener(ResourceEvent.ERROR, onErrorLoadLocaleResourceModule); 	
			}
		}
		
		protected function onCompleteLoadLocaleResourceModule(event:ResourceEvent):void{
			resourceManager.localeChain = [ _loadingLocale, EN_US ];
			_locale = _loadingLocale;
			localizeAlert(null);
		}
		
		protected function onErrorLoadLocaleResourceModule(event:ResourceEvent):void{
			_loadingLocale = null;
		}
		
		public function setLocale(locale:String):void{
			if (resourceManager.getLocales().indexOf(locale) != -1){
				onCompleteLoadLocaleResourceModule(null);
			}else{
				loadResourceModuleByLocale(locale);
			}	
		}
		
		public function getLocale():String{
			return _locale;
		}
		
		public function get birstAppName():String{
			return (_name) ? _name : null;
		}
		
		override protected function resourcesChanged():void{
			dispatchEvent(new Event("localeChange"));
		}
		
		protected function localizeAlert(event:Event):void{
			var bundle:String = 'sharable';
			var mgr:IResourceManager = ResourceManager.getInstance();
			
			Alert.yesLabel = mgr.getString(bundle, 'AL_yes');
			Alert.noLabel = mgr.getString(bundle, 'AL_no');
			Alert.cancelLabel = mgr.getString(bundle, 'AL_cancel');
			Alert.okLabel = mgr.getString(bundle, 'AL_ok');
		}
		
		[Bindable("localeChange")]
		public function getLocString(key:String, params:Array = null, locale:String = null):String{
			return resourceManager.getString(mainBundle, key, params, locale);
		}
		
		//Locale test dialog
		private function setLocaleDialogOnKeyPress():void{
			addEventListener(flash.events.KeyboardEvent.KEY_DOWN, onKeyDownLocale);
		}	
		
		private function onKeyDownLocale(event:KeyboardEvent):void{
			if (event.keyCode == flash.ui.Keyboard.F8){
				if (!_localePopUp){
					_localePopUp = createLocaleDialog();	
				}
				PopUpManager.addPopUp(_localePopUp, this);
				PopUpManager.bringToFront(_localePopUp);
				PopUpManager.centerPopUp(_localePopUp);
			}
		}	
		
		private function createLocaleDialog():TitleWindow{
			var c:ComboBox = new ComboBox();
			c.addEventListener(ListEvent.CHANGE, function(event:ListEvent):void{
				loadResourceModuleByLocale(c.selectedItem.value);	
			});
			c.addEventListener(FlexEvent.CREATION_COMPLETE, function(event:FlexEvent):void{
				var locales:Array = ResourceManager.getInstance().localeChain;
				if (locales.length > 0)
				{
					var value:String = locales[0]
					var dp:Object = c.dataProvider;
					if (dp != null)
					{
						if (dp is ArrayCollection){
							var comboDataProvider:ArrayCollection = dp as ArrayCollection;
							for (var i:int = 0; i < comboDataProvider.length; i++)
							{
								var a:Object = comboDataProvider.getItemAt(i);
								if ((a.hasOwnProperty('value') && a.value.toString() == value) || (a.toString() == value))
								{
									c.selectedItem = a;									
								}
							}
						}					
					}
				}
			});
			c.dataProvider = [{label: 'English', value: 'en_US'},
							  {label: 'Spanish', value: 'es_ES'}];
							    		
			var w:TitleWindow = new TitleWindow;			
			w.title = "Change locale";			
			w.addChild(c);
			w.showCloseButton = true;
			w.addEventListener(CloseEvent.CLOSE, function(event:Event):void{
				PopUpManager.removePopUp(w);
			});
			return w;
		}
		
		public function isFreeTrialUser():Boolean
		{
			return this.hasOwnProperty("isFreeTrial") && this["isFreeTrial"] == true; 
		}				
		
		public function hasLocalConnect():Boolean
		{
			return this.hasOwnProperty("LocalConnect") && this["LocalConnect"] == true;
		}
		
		public function hasBirstConnect():Boolean
		{
			return this.hasOwnProperty("BirstConnect") && this["BirstConnect"] == true;
		}
	}
}