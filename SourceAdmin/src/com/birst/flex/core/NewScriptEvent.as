package com.birst.flex.core
{
	import flash.events.Event;

	public class NewScriptEvent extends Event
	{
		public static const NEW_SOURCE_ADDED:String = "New Source Added";
		
		private var _newScriptName:String = null;
		
		public function NewScriptEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			//TODO: implement function
			super(type, bubbles, cancelable);
		}
		
		public function set scriptName(n:String):void {
			_newScriptName = n;
		}
		
		public function get scriptName():String {
			return _newScriptName;
		}
		
	}
}