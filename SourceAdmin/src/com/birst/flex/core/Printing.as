package com.birst.flex.core
{
	import mx.core.UIComponent;
	import mx.core.Application;
	
	import flash.net.URLRequestHeader;
	
	import org.alivepdf.display.Display;
	import org.alivepdf.images.ResizeMode;
	import org.alivepdf.layout.Layout;
	import org.alivepdf.layout.Orientation;
	import org.alivepdf.layout.Size;
	import org.alivepdf.layout.Unit;
	import org.alivepdf.pdf.PDF;
	import org.alivepdf.saving.Download;
	import org.alivepdf.saving.Method;
	
	import com.birst.flex.util.ServiceUtil;
	
	import generated.webservices.AdminService;
	import generated.webservices.CreateSMIWebLoginResultEvent;
	
	public class Printing
	{
		private static var _url:String;
		
		public function Printing()
		{
		}

		/**
		 * Basic printing - landscape, single letter.
		 */
		public static function printSingleLetterLandscape(whatToPrint:UIComponent, pageName:String):void{
			var printPDF:PDF = new PDF( Orientation.LANDSCAPE, Unit.MM, Size.LETTER );
			printPDF.setDisplayMode( Display.FULL_PAGE, Layout.SINGLE_PAGE );
			printPDF.addPage();
			printPDF.addImage( whatToPrint, 0, 0, 0, 0, 'PNG', 100, 1, ResizeMode.FIT_TO_PAGE );
			
        	var filename:String = pageName ? pageName : 'page';
        	filename += ".pdf";
        	
        	var svc:AdminService  = ServiceUtil.getWebServiceClient();
            var doPrintHandler:Function = function(event:CreateSMIWebLoginResultEvent):void {
            			var extraHeader:URLRequestHeader = new URLRequestHeader ("X-XSRF-TOKEN", Application.application.xsrfToken);
                        printPDF.save( Method.REMOTE, getRenderUrl(), Download.INLINE, filename, "_blank", extraHeader);                 
                     };                              
            svc.addEventListener(CreateSMIWebLoginResultEvent.CreateSMIWebLogin_RESULT, doPrintHandler);
            svc.createSMIWebLogin(false); 		
        }
		
		/**
		 * The url to read and create the inline browser print image
		 */
		private static function getRenderUrl():String{
			if(_url){
				return _url;
			}
			
			_url = '/SMIWeb/CreatePDFServlet.jsp';
			var flashVarRootURL:String = getApplicationSMIWebURL();
        	if(flashVarRootURL != null && flashVarRootURL.length > 0){
        		_url = flashVarRootURL + _url;
        	}
			return _url;
		}
		
		// dev mode : check for any flash vars as smiweb url, use that instead
		public static function getApplicationSMIWebURL():String		
		{
			return Application.application.parameters['smiroot'];	
		}
	}
}