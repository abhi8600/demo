package com.birst.flex.diffrepository
{
	public class DiffObject
	{
		public function DiffObject(s: String)
		{
			var params: Array = s.split("|");
			label = params.length > 0 ? params[0] : null;
			baseObjectVersion = params.length > 1 ? params[1] : null;
			compareObjectVersion = params.length > 2 ? params[2] : null;
		}
		public var itemName: String;
		public var itemPath: String;
		public var itemType: String;
		public var label: String;
		public var baseObjectVersion: String;
		public var compareObjectVersion: String;
	}
}