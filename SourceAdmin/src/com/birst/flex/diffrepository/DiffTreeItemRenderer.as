package com.birst.flex.diffrepository
{
	import flash.events.MouseEvent;
	
	import mx.controls.CheckBox;
	import mx.controls.treeClasses.TreeItemRenderer;
	import mx.controls.treeClasses.TreeListData;

	public class DiffTreeItemRenderer extends TreeItemRenderer
	{
		private var cb: CheckBox;

		public function DiffTreeItemRenderer()
		{
			super();
		}
		
		override protected function createChildren():void
		{
   			super.createChildren();
			cb = new CheckBox();
			cb.addEventListener(MouseEvent.CLICK, checkboxClick);
			addChild(cb);
		}
		
   		private function checkboxClick(event: MouseEvent): void
   		{
   			var x: XML = XML(super.data);
   			var sel: String = x.@Selected;
   			sel = sel.toLowerCase();
   			var b: Boolean = sel == "true" ? true : false;
   			setIncludingChildren(x, !b);
			DiffRepository(super.document).showDiffData();
   		}
   		
   		private function setIncludingChildren(x: XML, b: Boolean): void
   		{
   			x.@Selected = b;
   			var children: XMLList = x.children();
   			if (children.length() > 0)
   			{
   				for(var i: int = 0; i < children.length(); i++)
   					setIncludingChildren(children[i], b); 
   			}
   		}
   		
		override protected function updateDisplayList(unscaledWidth:Number, unscaledHeight:Number):void 
		{
            super.updateDisplayList(unscaledWidth, unscaledHeight);
            if(super.data && cb)
            {
            	if (TreeListData(super.listData).depth == 1)
            	{
            		cb.visible = false;
            	} else
            	{
            		cb.visible = true;
            		cb.y = 9;
            		cb.x = ((TreeListData(super.listData).depth-2) * 18);
		   			var sel: String = XML(super.data).@Selected;
		   			sel = sel.toLowerCase();
		   			var b: Boolean = sel == "true" ? true : false;
		   			cb.selected = b;
            	}
            }
  		}
	}
}