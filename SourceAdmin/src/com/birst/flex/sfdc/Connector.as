package com.birst.flex.sfdc
{
	public class Connector
	{
		internal var ConnectorName:String;
		internal var ConnectorAPIVersion:String;
		internal var ConnectorConnectionString:String;
		internal var isDefaultVersion:Boolean;
		internal var isSelectedAPIVersion:Boolean;
		
		public function Connector(connName:String, connAPIVersion:String, connConnectionString:String, isDefault:Boolean, isSelectedVersion:Boolean = false)
		{
			ConnectorName= connName;
			ConnectorAPIVersion = connAPIVersion;
			ConnectorConnectionString = connConnectionString;
			isDefaultVersion = isDefault;
			isSelectedAPIVersion = isSelectedVersion;
		}

	}
}