package com.birst.flex.sfdc
{
	import mx.collections.ArrayCollection;
	
	public class CloudConnectorConfig
	{
		internal var ConfigVersion:int;
		internal var CloudConnections:ArrayCollection;
		internal var ExtractionGroups:ArrayCollection;
		internal var ExtractionObjects:ArrayCollection;
		internal var CreatedDate:String;
		internal var LastModifiedDate:String;
		internal var CreatedUsername:String;
		internal var LastModifiedUsername:String;
		
		public function CloudConnectorConfig()
		{
		}

		public static function parseConfig(_xml:XML):CloudConnectorConfig
		{
			if (_xml == null)
			{
				return null;
			}
			var config:CloudConnectorConfig = new CloudConnectorConfig();
			if (_xml.ConfigVersion)
			{
				config.ConfigVersion = parseInt(_xml.ConfigVersion);
			}
			config.setCloudConnections(_xml);
			config.setExtractionGroups(_xml);
			config.setExtractionObjects(_xml);				
			if (_xml.CreatedUsername)
			{
				config.CreatedUsername = _xml.CreatedUsername;  
			}
			if (_xml.CreatedDate)
			{
				config.CreatedDate = _xml.CreatedDate;  
			}
			if (_xml.LastModifiedUsername)
			{
				config.LastModifiedUsername = _xml.LastModifiedUsername;  
			}
			if (_xml.LastModifiedDate)
			{
				config.LastModifiedDate = _xml.LastModifiedDate;  
			}
			return config;
		}
		
		public function setCloudConnections(_xml:XML):void
		{
			this.CloudConnections = new ArrayCollection();
			if (_xml.CloudConnections)
			{
				var cldConnectionList:XMLList = _xml.CloudConnections.CloudConnection as XMLList;
				if (cldConnectionList != null && cldConnectionList.length() > 0)
				{
					for each(var cldConn:XML in cldConnectionList)
					{
						var cloudConnection:CloudConnection = CloudConnection.parse(cldConn);
						this.CloudConnections.addItem(cloudConnection);
					}
				}
			}			
		}
		
		public function setExtractionGroups(_xml:XML):void
		{
			this.ExtractionGroups = new ArrayCollection();
			if (_xml.ExtractionGroups)
			{
				var extGroupList:XMLList = _xml.ExtractionGroups.ExtractionGroup as XMLList;
				if (extGroupList != null && extGroupList.length() > 0)
				{
					for each(var extGroup:XML in extGroupList)
					{
						var extractionGroup:ExtractionGroup = ExtractionGroup.parse(extGroup);
						this.ExtractionGroups.addItem(extractionGroup);
					}
				}
			}			
		}
		
		public function setExtractionObjects(_xml:XML):void
		{
			this.ExtractionObjects = new ArrayCollection();
			if (_xml.ExtractionObjects)
			{
				var extObjList:XMLList = _xml.ExtractionObjects.ExtractionObjectWrapper as XMLList;
				if (extObjList != null && extObjList.length() > 0)
				{
					for each(var extObject:XML in extObjList)
					{
						var extractionObject:ExtractionObjectWrapper = ExtractionObjectWrapper.parse(extObject);
						this.ExtractionObjects.addItem(extractionObject);
					}
				}
			}
		}
		
		public function getSelectedObjectsForConnection(conn:CloudConnectionWrapper):ArrayCollection
		{
			var selectedObjects:ArrayCollection = new ArrayCollection();
			if (ExtractionObjects && ExtractionObjects.length > 0)
			{
				for each (var wrp:ExtractionObjectWrapper in ExtractionObjects)
				{
					if (wrp.CloudConnections != null && wrp.CloudConnections.length > 0)
					{
						for each (var cldConn:String in wrp.CloudConnections)
						{
							if (cldConn == conn.connection.GUID)
							{
								selectedObjects.addItem(wrp);
							}
						}
					}
				}
			}
			return selectedObjects;
		}
		
		public function getSelectedObjectsForConnectorType(connType:String):ArrayCollection
		{
			var selectedObjects:ArrayCollection = new ArrayCollection();
			var connectorsForConnectorType:ArrayCollection = new ArrayCollection();
			if (CloudConnections && CloudConnections.length > 0)
			{
				for each (var conn:CloudConnection in CloudConnections)
				{
					if (connType == "All" || conn.ConnectorType == connType)
					{
						connectorsForConnectorType.addItem(conn);
					}
				}
			}
			if (connectorsForConnectorType.length > 0 && ExtractionObjects && ExtractionObjects.length > 0)
			{
				for each (var wrp:ExtractionObjectWrapper in ExtractionObjects)
				{
					if (wrp.CloudConnections != null && wrp.CloudConnections.length > 0)
					{
						var found:Boolean = false;
						for each (var connId:String in wrp.CloudConnections)
						{
							for each (var cldConn:CloudConnection in connectorsForConnectorType)
							{
								if (connId == cldConn.GUID)
								{
									selectedObjects.addItem(wrp);
									found = true;
									break;
								}
							}
							if (found)
							{
								break;
							}
						}
					}
				}
			}
			return selectedObjects;
		}
		
		public function getCloudConnectionByGUID(guid:String):CloudConnection
		{
			if (CloudConnections != null)
			{
				for each (var conn:CloudConnection in CloudConnections)
				{
					if (conn.GUID == guid)
						return conn;
				}
			}
			return null;
		}
		
		public function getExtractionObject(name:String, connectorType:String, connectionNames:String):ExtractionObjectWrapper
		{
			var connections:ArrayCollection = new ArrayCollection();
			if (connectionNames != null)
			{
				connections = new ArrayCollection(connectionNames.split(','));
			}
			var connectionIDs:ArrayCollection = new ArrayCollection();
			if (CloudConnections != null)
			{
				for each(var cldConn:CloudConnection in CloudConnections)
				{
					if (cldConn.ConnectorType == connectorType && connections.contains(cldConn.Name))
					{
						connectionIDs.addItem(cldConn.GUID);
					}
				}
			}
			if (ExtractionObjects != null)
			{
				for each (var eobj:ExtractionObjectWrapper in ExtractionObjects)
				{
					if (eobj.Name == name && eobj.ConnectorType == connectorType)
					{
						if (eobj.CloudConnections != null)
						{
							var foundAllConnections:Boolean = true;
							for each (var connID:String in connectionIDs)
							{
								var found:Boolean = false;
								for each (var objConn:String in eobj.CloudConnections)
								{
									if (connID == objConn)
									{
										found = true;
										break;
									}
								}
								if (!found)
								{
									foundAllConnections = false;
									break;
								}
							}
							if (foundAllConnections)
							{
								return eobj;
							}							
						}
					}
				}
			}
			return null;
		}
		
	}	
}