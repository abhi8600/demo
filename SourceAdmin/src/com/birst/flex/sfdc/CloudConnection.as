package com.birst.flex.sfdc
{
	import mx.collections.ArrayCollection;
	
	public class CloudConnection
	{
		internal var Name:String;
		internal var GUID:String;
		internal var ConnectorType:String;
		internal var ConnectorAPIVersion:String;
		internal var ConnectionProperties:ArrayCollection;
		internal var StartExtractionTime:String;
		internal var EndExtractionTime:String;
		internal var CreatedDate:String;
		internal var LastModifiedDate:String;
		internal var CreatedUsername:String;
		internal var LastModifiedUsername:String;
		internal var MigratedConnection:String;
		internal var isModified:Boolean = false;
			
		public function CloudConnection()
		{
		}
		
		public static function parse(_xml:XML):CloudConnection
		{
			var conn:CloudConnection = new CloudConnection();
			conn.Name = _xml.Name;
			conn.GUID = _xml.GUID;
			conn.ConnectorType = _xml.ConnectorType;
			conn.ConnectorAPIVersion = _xml.ConnectorAPIVersion;
			var connProperties:XMLList = _xml.ConnectionProperties;
			if (connProperties && connProperties.length() > 0)
			{
				var connPropertiesList:XMLList = connProperties[0].ConnectionProperty;
				if (connPropertiesList && connPropertiesList.length() > 0)
				{
					conn.ConnectionProperties = new ArrayCollection();
					for each(var cp:XML in connPropertiesList)
					{
						var connProp:Object = new Object();
						connProp.Name = cp.Name && (cp.Name as XMLList).length() > 0 ? (cp.Name as XMLList)[0].toString() : null;
						connProp.Value = cp.Value && (cp.Value as XMLList).length() > 0 ? (cp.Value as XMLList)[0].toString() : null;
						connProp.IsRequired = cp.IsRequired && (cp.IsRequired as XMLList).length() > 0 ? (cp.IsRequired as XMLList)[0].toString() : "false";
						connProp.IsSecret = cp.IsSecret && (cp.IsSecret as XMLList).length() > 0 ? (cp.IsSecret as XMLList)[0].toString() : "false";
						connProp.IsEncrypted = cp.IsEncrypted && (cp.IsEncrypted as XMLList).length() > 0 ? (cp.IsEncrypted as XMLList)[0].toString() : "false";
						connProp.SaveToConfig = cp.SaveToConfig && (cp.SaveToConfig as XMLList).length() > 0 ? (cp.SaveToConfig as XMLList)[0].toString() : "false";
						connProp.DisplayIndex = cp.DisplayIndex && (cp.DisplayIndex as XMLList).length() > 0 ? (cp.DisplayIndex as XMLList)[0].toString() : "-1";
						connProp.DisplayLabel = cp.DisplayLabel && (cp.DisplayLabel as XMLList).length() > 0 ? (cp.DisplayLabel as XMLList)[0].toString() : null;
						conn.ConnectionProperties.addItem(connProp);
					}
				}
			}
			conn.StartExtractionTime = _xml.StartExtractionTime;
			conn.EndExtractionTime = _xml.EndExtractionTime;
			conn.CreatedDate = _xml.CreatedDate;
			conn.LastModifiedDate = _xml.LastModifiedDate;
			conn.CreatedUsername = _xml.CreatedUsername;
			conn.LastModifiedUsername = _xml.LastModifiedUsername;
			conn.MigratedConnection = _xml.MigratedConnection;
			return conn;
		}
		
		public function getConnectionCredentials():Array
		{
			var credentials:Array = new Array();
			if (ConnectionProperties != null)
			{
				for each (var cp:Object in ConnectionProperties)
				{
					credentials.push({name:cp.Name, value:cp.Value});
				}
			}
			return credentials
		}
	}
}