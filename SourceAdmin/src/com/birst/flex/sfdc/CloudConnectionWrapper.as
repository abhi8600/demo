package com.birst.flex.sfdc
{
	import mx.core.IUID;

	public class CloudConnectionWrapper implements IUID
	{
		private var _uid:String;
		private var _connection:CloudConnection;
		public var label:String;
		
		public function CloudConnectionWrapper()
		{
		}

		public function get uid():String
		{
			return _uid;
		}
		
		public function set uid(value:String):void
		{
			_uid = value;
		}
		
		public function get connection():CloudConnection
		{
			return _connection;
		}
		
		public function set connection(connection:CloudConnection):void
		{
			this._connection = connection;
		}
		
	}
}