package com.birst.flex.sfdc
{
	public class ExtractionGroup
	{
		internal var Name:String;
		internal var GUID:String;
		internal var CreatedDate:String;
		internal var LastModifiedDate:String;
		internal var CreatedUsername:String;
		internal var LastModifiedUsername:String;
		internal var isModified:Boolean = false;
		
		public function ExtractionGroup()
		{
		}
		
		public static function parse(_xml:XML):ExtractionGroup
		{
			var eGroup:ExtractionGroup = new ExtractionGroup();
			eGroup.Name = _xml.Name;
			eGroup.GUID = _xml.GUID;
			eGroup.CreatedDate = _xml.CreatedDate;
			eGroup.LastModifiedDate = _xml.LastModifiedDate;
			eGroup.CreatedUsername = _xml.CreatedUsername;
			eGroup.LastModifiedUsername = _xml.LastModifiedUsername;
			return eGroup;
		}

	}
}