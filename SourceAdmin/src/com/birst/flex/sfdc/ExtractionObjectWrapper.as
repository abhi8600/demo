package com.birst.flex.sfdc
{
	import mx.collections.ArrayCollection;
	
	public class ExtractionObjectWrapper
	{
		public var ConnectorType:String;
		public var CloudConnections:ArrayCollection;
		public var ExtractionGroups:ArrayCollection;
		public var Name:String;
		internal var ObjectName:String;
		internal var Type:String;
		internal var Query:String;
		internal var Mapping:String;
		internal var SelectionCriteria:String;
		internal var ExtractionGroupNames:String;
		internal var LastUpdatedDate:String;
		internal var UseLastModified:String;
		internal var LastSystemModStamp:String;		
		internal var ColumnNames:ArrayCollection;
		internal var ObjectId:String;
		internal var RecordType:String;
		internal var URL:String;
		internal var ProfileID:String;
		internal var Dimensions:String;
		internal var Metrics:String;
		internal var SegmentID:String;
		internal var Filters:String;
		internal var IncludeAllRecords:String;
		internal var StartDate:String;
		internal var EndDate:String;
		internal var isModified:Boolean = false;
		internal var CreatedUsername:String;
		internal var CreatedDate:String;
		internal var LastModifiedUsername:String;
		internal var LastModifiedDate:String;
		internal var SamplingLevel:String;
		public var sortedConnectionNames:String;
		 
		public function ExtractionObjectWrapper()
		{
		}
		
		public static function parse(_xml:XML):ExtractionObjectWrapper
		{
			var wrp:ExtractionObjectWrapper = new ExtractionObjectWrapper();
			wrp.ConnectorType = _xml.ConnectorType;
			if (_xml.CloudConnections && (_xml.CloudConnections as XMLList).length() > 0)
			{
				var cldConnections:XMLList = (_xml.CloudConnections as XMLList)[0].CloudConnection;
				if (cldConnections && cldConnections.length() > 0)
				{
					wrp.CloudConnections = new ArrayCollection();
					for each (var conn:XML in cldConnections)
					{
						wrp.CloudConnections.addItem(conn.toString());
					}
				}
			}
			if (_xml.ExtractionGroups && (_xml.ExtractionGroups as XMLList).length() > 0)
			{
				var extractGroups:XMLList = (_xml.ExtractionGroups as XMLList)[0].ExtractionGroup;
				if (extractGroups && extractGroups.length() > 0)
				{
					wrp.ExtractionGroups = new ArrayCollection();
					for each (var eg:XML in extractGroups)
					{
						wrp.ExtractionGroups.addItem(eg.toString());
					}
				}
			}
			if (_xml.ExtractionObject && (_xml.ExtractionObject as XMLList).length() > 0)
			{
				var extractionObject:XML = (_xml.ExtractionObject as XMLList)[0];
				wrp.Name = extractionObject.Name;
				wrp.ObjectName = extractionObject.ObjectName;
				wrp.Type = extractionObject.Type;
				wrp.Query = extractionObject.Query;
				wrp.Mapping = extractionObject.Mapping;
				wrp.SelectionCriteria = extractionObject.SelectionCriteria;
				wrp.ExtractionGroupNames = extractionObject.ExtractionGroupNames;
				wrp.LastUpdatedDate = extractionObject.LastUpdatedDate;
				wrp.UseLastModified = extractionObject.UseLastModified && (extractionObject.UseLastModified as XMLList).length() > 0 ? (extractionObject.UseLastModified as XMLList)[0].toString() : "false";
				wrp.LastSystemModStamp = extractionObject.LastSystemModStamp;
				if (extractionObject.ColumnNames && (extractionObject.ColumnNames as XMLList).length() > 0)
				{
					var cNames:XMLList = (extractionObject.ColumnNames as XMLList)[0].ColumnName;
					if (cNames && cNames.length() > 0)
					{
						wrp.ColumnNames = new ArrayCollection();
						for each (var cName:XML in cNames)
						{
							wrp.ColumnNames.addItem(cName.toString());
						}
					}
				}
				wrp.ObjectId = extractionObject.ObjectId;
				wrp.RecordType = extractionObject.RecordType;
				wrp.URL = extractionObject.URL;
				wrp.ProfileID = extractionObject.ProfileID;
				wrp.Dimensions = extractionObject.Dimensions;
				wrp.Metrics = extractionObject.Metrics;
				wrp.SegmentID = extractionObject.SegmentID;
				wrp.Filters = extractionObject.Filters;
				wrp.IncludeAllRecords = extractionObject.IncludeAllRecords && (extractionObject.IncludeAllRecords as XMLList).length() > 0 ? (extractionObject.IncludeAllRecords as XMLList)[0].toString() : "false";
				wrp.StartDate = extractionObject.StartDate;
				wrp.EndDate = extractionObject.EndDate;
				wrp.SamplingLevel=extractionObject.SamplingLevel;
			}
			wrp.CreatedDate = _xml.CreatedDate;
			wrp.LastModifiedDate = _xml.LastModifiedDate;
			wrp.CreatedUsername = _xml.CreatedUsername;
			wrp.LastModifiedUsername = _xml.LastModifiedUsername;
			return wrp;
		}
	}
}