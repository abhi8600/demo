package com.birst.flex.controls
 {
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.controls.CheckBox;
	import mx.events.FlexEvent;
	
	[Event(name="comboChecked", type="com.birst.flex.controls.ComboCheckEvent")]
	
	public class ComboCheckItemRenderer extends CheckBox {
		
		public function ComboCheckItemRenderer() {
			super();
			addEventListener(FlexEvent.CREATION_COMPLETE, onCreationComplete);
			addEventListener(MouseEvent.CLICK,onClick);
		}
		
		override protected function updateDisplayList(w:Number, h:Number):void
		{
			super.updateDisplayList(w, h);
			if (data != null ) {
				if (data.assigned==true) {
					selected=true;
					cck=ComboCheck(ComboCheckDropDownFactory(owner).owner);
					var index:int=cck.selectedItems.getItemIndex(data);
	        		if (index==-1) {
						cck.selectedItems.addItem(data);
	        		}
	   			}
	   			else {
	   				selected = false;
	   				var cck:ComboCheck=ComboCheck(ComboCheckDropDownFactory(owner).owner);
					index=cck.selectedItems.getItemIndex(data);
					if (index!=-1)
					{
						cck.selectedItems.removeItemAt(index);
					}	
	   			}
	   			if(data.toolTip != null)
	   			{
		   			this.toolTip = data.toolTip;
		   		}
   			}
		}

		private function onCreationComplete(event:Event):void {
			if (data != null && data.assigned==true) {
				selected=true;
				var cck:ComboCheck=ComboCheck(ComboCheckDropDownFactory(owner).owner);
				var index:int=cck.selectedItems.getItemIndex(data);
        		if (index==-1) {
					cck.selectedItems.addItem(data);
        		}	
			}
		}

        private function onClick(event:Event):void {
	        super.data.assigned=selected;
	        var myComboCheckEvent:ComboCheckEvent=new ComboCheckEvent(ComboCheckEvent.COMBO_CHECKED);
	        myComboCheckEvent.obj=data;
	        owner.dispatchEvent(myComboCheckEvent);
        }
	}
}