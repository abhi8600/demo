package com.birst.flex.controls
 {
	import flash.events.Event;
	
	import mx.collections.ArrayCollection;
	import mx.controls.ComboBox;
	import mx.core.ClassFactory;
	import mx.events.DropdownEvent;
	import mx.events.FlexEvent;
	
	[Event(name="addItem", type="flash.events.Event")]
	
    public class ComboCheck extends ComboBox {
    	private var _selectedItems:ArrayCollection;
    	private var _selectionBeginIndex:int=0;
    	private var _selectionEndIndex:int=0;
    	
    	[Bindable("change")]
    	[Bindable("valueCommit")]
    	[Bindable("collectionChange")]
    	
    	public function set selectedItems(value:ArrayCollection):void {
    		_selectedItems=value;
    	}
        
    	public function get selectedItems():ArrayCollection {
    		return _selectedItems;
    	}
    	
        public function ComboCheck() {
            super();
            addEventListener("comboChecked", onComboChecked);
            addEventListener(FlexEvent.CREATION_COMPLETE, onCreationComplete);
            var render:ClassFactory = new ClassFactory(ComboCheckItemRenderer);
		    itemRenderer=render;
		    var myDropDownFactory:ClassFactory = new ClassFactory(ComboCheckDropDownFactory);
        	super.dropdownFactory=myDropDownFactory;
        	selectedItems=new ArrayCollection();
        	this.addEventListener(DropdownEvent.OPEN, dropEvt);
        	this.addEventListener(DropdownEvent.CLOSE, dropEvt);
        }
                
        private function textFocusChanged(event:Event):void
        {
        	resetRenderer();
        }         
        
        private function resetRenderer():void
        {
        	for (var i:int=0; i < selectedItems.length; i++)  
        	{
        		found = isTextInputItem(selectedItems[i]);
        		if (!found)
        		{
        			selectedItems[i].assigned = false;
        		}
        	}
        	
        	var textLabel:String = textInput.text;
        	var textArr:Array = textInput.text.split(",");
        	var found:Boolean = false;
        	if (textArr.length > 0)
        	{
        		var item:Object = null;
	        	for (var text:String in textArr) 
	        	{
	        		item = getItem(textArr[text]);
	        		if (item != null && item.assigned == false)
	        		{
	        			item.assigned =true;
	        			selectedItems.addItem(item);
	        		}
	        	}
        	}
        	
        	var dp:ArrayCollection = this.dataProvider as ArrayCollection;
        	
        	if (dp != null && selectedItems.length > 0)
        	{
        		_selectionBeginIndex = textInput.selectionBeginIndex;
        		_selectionEndIndex = textInput.selectionEndIndex;
        		dp.refresh();
        	}
        	this.text = textLabel;
        }
        
        private function isTextInputItem(item:Object):Boolean
        {
        	var textArr:Array = textInput.text.split(",");
        	var found:Boolean = false;
        	if (textArr.length > 0)
        	{
	        	for (var text:String in textArr) 
	        	{
	        		if (textArr[text] ==  item.label)
	        		{
	        			found = true;
	        			break;		
	        		}
	        	}	        		        	
	        }
        	return found;
        }
        
        private function getItem(text:String):Object
        {
        	var item:Object = null;
        	var dp:ArrayCollection = this.dataProvider as ArrayCollection;
        	if (dp != null)
        	{
	        	for (var i:int=0; i < dp.length; i++)  
	        	{
	        		if (dp[i].label == text)
	        		{
	        			item = dp[i];
	        			break;
	        		}
	        	}
	        }
        	
        	return item;
        }
        
        private function dropEvt(event:DropdownEvent):void 
        {
            resetTextInputSelectedIndex();
        }
        
        private function resetTextInputSelectedIndex():void
        {
        	if (_selectionBeginIndex > 0 && _selectionEndIndex > 0)
        	{
        		textInput.selectionBeginIndex = _selectionBeginIndex;
				textInput.selectionEndIndex	= _selectionEndIndex;
        	}
        	else
        	{
        		textInput.selectionBeginIndex = text.length;
				textInput.selectionEndIndex	= text.length;
        	}
        }
        
        private function onCreationComplete(event:Event):void {
        	dropdown.addEventListener(FlexEvent.CREATION_COMPLETE, onDropDownComplete);
        	textInput.addEventListener(Event.CHANGE, textFocusChanged);
        	resetTextInputSelectedIndex();
        }
        
        private function onDropDownComplete(event:Event):void {
        	//trace ("dropdown complete!");
        }
        
        override protected function updateDisplayList(unscaledWidth:Number, unscaledHeight:Number):void
        {
              super.updateDisplayList(unscaledWidth, unscaledHeight);
              this.toolTip = this.text;
              resetTextInputSelectedIndex();
        }
        
        private function onComboChecked(event:ComboCheckEvent):void {
        	_selectionBeginIndex = 0;
        	_selectionEndIndex = 0;
        	var obj:Object=event.obj;
        	var index:int=selectedItems.getItemIndex(obj);
        	if (index==-1) {
        		selectedItems.addItem(obj);
        	} else {
        		var textArr:Array = textInput.text.split(",");
        		if (textArr.length > 0)
	        	{
	        		text  = '';
					var first:Boolean = true;
		        	for (var item:String in textArr) 
		        	{
		        		if (textArr[item] == selectedItems.getItemAt(index)[labelField])
		        		{
		        			continue;
		        		}	
		        		if (first)
		        		{
		        			first = false;
		        		}
		        		else
		        		{
		        			text += ',';
		        		}
		        		text += textArr[item];
		        	}
        		}
       			selectedItems.removeItemAt(index);
        	}
        	if (selectedItems.length>1) {
        		var size: int = selectedItems.length;
        		//text = '';
        		for (item in selectedItems) {
        			var len:int = text.length;
        			if (!isTextInputItem(selectedItems[item]))
        			{
            			text+=(len > 0 ? ',' : '') + selectedItems[item].label;
           			}
            	}
        	}
        	if (selectedItems.length==1) {
        		len = text.length;
    			if (!isTextInputItem(selectedItems[0]))
    			{
        			text+=(len > 0 ? ',' : '') + selectedItems.getItemAt(0)[labelField];
       			}
        	}
        	
        	this.toolTip = this.text;
        	
        	dispatchEvent(new Event("valueCommit"));
        	dispatchEvent(new Event("addItem"));
        }
    }    
}