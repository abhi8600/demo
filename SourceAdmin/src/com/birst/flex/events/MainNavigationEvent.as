package com.birst.flex.events
{
	import flash.events.Event;
	
	public class MainNavigationEvent extends Event
	{
		// Manage Space Tab and Link Bar Navigate Controls
		public static const TAB_MANAGE_SPACE:String = "Manage Space";
		public static const LINKBAR_MODIFY_PROPERTIES:String = "Modify Properties";
		public static const LINKBAR_CREATE_TEMPLATE:String = "Create a Template";
		public static const LINKBAR_EDIT_QUICK_DASHBOARDS:String = "Edit Quick Dashboards";
		public static const LINKBAR_DELETE_SPACE:String = "Delete Space";
		
		// Define Sources Tab and Link Bar Navigate Controls
		public static const TAB_DEFINE_SOURCES:String = "Define Sources";
		public static const LINKBAR_MANAGE_LOCAL_SOURCES:String = "Manage Local Sources";
		public static const LINKBAR_MANAGE_SOURCES:String = "Manage Sources";
		public static const LINKBAR_BROWSER_UPLOAD:String = "Upload Files";
		public static const LINKBAR_BIRST_CONNECT:String = "Birst Connect";
		public static const LINKBAR_APPLICATION_CONNECTORS:String = "Application Connectors";
		public static const LINKBAR_GOOGLE_ANALYTICS:String = "Google Analytics";
		public static const LINKBAR_SFDC_EXTRACT:String = "Extract from Salesforce.com";
		public static const LINKBAR_BXE_CONNECTOR:String = "Application Connectors";
		public static const LINKBAR_DATA_FLOW:String = "Data Flow";	
		
		public static const TAB_CUSTOMIZE_SPACE:String = "Customize Space";
		public static const LINKBAR_CUSTOM_MEASURES:String = "Custom Measures";
		public static const LINKBAR_CUSTOM_ATTRIBUTES:String = "Custom Attributes";
		public static const LINKBAR_BUCKETED_MEASURES:String = "Bucketed Measures";
		public static const LINKBAR_VARIABLES:String = "Variables";
		public static const LINKBAR_PACKAGES:String = "Packages";
		public static const LINKBAR_MANAGE_AGGREGATES:String = "Manage Aggregates";
		public static const LINKBAR_DRILL_MAPS:String = "Drill Maps";
		
		public static const TAB_PROCESS_DATA:String = "Process Data";
		public static const LINKBAR_PROCESS_DATA:String = "Process New Data";
		public static const LINKBAR_DELETE_DATA:String = "Delete Data";						
		public static const LINKBAR_VIEW_PROCESSED:String = "View Processed Data";
		public static const LINKBAR_SCHEDULE_PROCESS:String = "Schedule Data Processing";
		
		public static const TAB_MANAGE_ACCESS:String = "Manage Access";
		public static const SHARE_SPACE: String = "Share Space";
		public static const MANAGE_GROUPS:String = "Manage Groups";
		public static const CATALOG_ADMIN: String = "Manage Report Catalog";
		public static const SCHEDULE_REPORT: String = "Report Scheduler (old)";
		public static const SCHEDULE_REPORT_NEW:String = "Report Scheduler";
		public static const EXPORT_DATA: String = "Export Report Data";
		
		public var tabBarItem:String;
		public var linkBarItem:String
		public var additionalData:Object;
		// Event name
		public static const MAIN_NAV_CHANGED:String = "MainNavigationChanged";
		public function MainNavigationEvent(_type:String, _tabBarItem:String, _linkBarItem:String, _additionalData:Object=null)		
		{
			super(_type, false, false);
			
			this.tabBarItem = _tabBarItem;
			this.linkBarItem = _linkBarItem;
			this.additionalData = _additionalData;
		}
		
		override public function clone():Event
		{
			return new MainNavigationEvent(type, tabBarItem, linkBarItem, additionalData);	
		}
		
	}
}