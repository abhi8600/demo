package com.birst.flex.events
{
	import flash.events.Event;

	public class ProcessingSubGroupsModifiedEvent extends Event
	{
		public static const P_SUBGROUP_MODIFIED:String  = "Modified Processing SubGroup";		
		
		public var subGroupName:String
		// added or removed
		public var operation:Boolean;
		public function ProcessingSubGroupsModifiedEvent(_subGroupName:String, _operation:Boolean, type:String,bubbles:Boolean=false, cancelable:Boolean=false)
		{
			subGroupName = _subGroupName;
			operation = _operation; 
			super(type, bubbles, cancelable);
		}
		
		override public function clone():Event
		{
			return new ProcessingSubGroupsModifiedEvent(subGroupName, operation, type, bubbles, cancelable);	
		}
	}
}