package com.birst.flex.events
{
	import flash.events.Event;
	public class NavigateChangeEvent  extends Event
	{
		
		// wizard related navigation change event
		
		public static const WIZARD_BUTTON_CHANGE:String = "WizardButtonChange";
		public static const WIZARD_BUTTON_PREVIOUS:String = "PreviousMode";
		public static const WIZARD_BUTTON_NEXT:String = "NextMode";
		public static const WIZARD_BUTTON_CANCEL:String = "CancelMode";
		public static const WIZARD_BUTTON_DONE:String = "DoneMode";
		public var mode:String; 
		public var screenIndex:int;
		public function NavigateChangeEvent(_mode:String, _screenIndex:int=0)
		{
			super(WIZARD_BUTTON_CHANGE,false, false);
			this.mode = _mode;
			this.screenIndex = _screenIndex;
		}	
		
		override public function clone():Event
		{
			return new NavigateChangeEvent(mode, screenIndex);	
		}
	}
}