package com.birst.flex.events
{
	import flash.events.Event;
	public class RefreshEvent  extends Event
	{
		// Event name
		public static const REFRESH_NAV:String = "RefreshNav";
		public static const REFRESH_SOURCES:String = "RefreshSources";
		public static const REFRESH_ACCESS:String = "RefreshAccess";
		public static const REFRESH_PROCESS_DATA:String = "RefreshProcessData";
		// refresh any groups dependent screens like Catalog Management and Custom Subject Area
		public static const REFRESH_GROUP_DEPENDENT_SCREEN:String="RefreshGroupDependentScreen";
		
		// refresh a specific source
		public static const REFRESH_SINGLE_DATA_SOURCE:String = "RefreshSpecificSource";
		// refresh event used by cluster configuration screen on UserManagement
		public static const REFRESH_CLUSTER_CONFIGURATION:String = "RefreshClusterConfiguration";
		public var refreshParam:Object;
		
		public var refreshType:String; 		
		public function RefreshEvent(_refreshType:String, _refreshParam:Object=null)
		{		
			super(REFRESH_NAV, false, false);
			this.refreshType = _refreshType;			
			this.refreshParam = _refreshParam;
		}

		override public function clone():Event
		{
			return new RefreshEvent(refreshType);	
		}
	}
}