package com.birst.flex.events
{
	import flash.events.Event;

	public class SubGroupsModifiedEvent extends Event
	{
		public static const P_SUBGROUP_MODIFIED:String  = "Modified SubGroup";		
		
		public var subGroupName:String
		// added or removed
		public var operation:Boolean;
		public function SubGroupsModifiedEvent(_subGroupName:String, _operation:Boolean, type:String,bubbles:Boolean=false, cancelable:Boolean=false)
		{
			subGroupName = _subGroupName;
			operation = _operation; 
			super(type, bubbles, cancelable);
		}
		
		override public function clone():Event
		{
			return new SubGroupsModifiedEvent(subGroupName, operation, type, bubbles, cancelable);	
		}
	}
}