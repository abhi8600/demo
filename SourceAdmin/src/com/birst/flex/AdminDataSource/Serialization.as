package com.birst.flex.AdminDataSource
{
	import generated.webservices.AdminDataSource;
	import generated.webservices.ArrayOfArrayOfString;
	import generated.webservices.ArrayOfBoolean;
	import generated.webservices.ArrayOfCustomTimeShift;
	import generated.webservices.ArrayOfForeignKey;
	import generated.webservices.ArrayOfInheritTable;
	import generated.webservices.ArrayOfOverrideLevelKey;
	import generated.webservices.ArrayOfSourceLocation;
	import generated.webservices.ArrayOfStagingColumn;
	import generated.webservices.ArrayOfString;
	import generated.webservices.ArrayOfTableDefinition;
	import generated.webservices.ArrayOfTableSourceFilter;
	import generated.webservices.CustomTimeShift;
	import generated.webservices.ForeignKey;
	import generated.webservices.InheritTable;
	import generated.webservices.OverrideLevelKey;
	import generated.webservices.ScriptDefinition;
	import generated.webservices.SecurityFilter;
	import generated.webservices.SnapshotPolicy;
	import generated.webservices.SourceLocation;
	import generated.webservices.StagingColumn;
	import generated.webservices.TableDefinition;
	import generated.webservices.TableSource;
	import generated.webservices.TableSourceFilter;
	
	import mx.utils.StringUtil;
	
	public class Serialization
	{
		public function Serialization()
		{
		}

		public static function getAdminDataSource(s: String): AdminDataSource
		{
			var params: Array = s.split("|0");
			var ads: AdminDataSource = new AdminDataSource();
			for each (var s: String in params)
			{
				var i: int = s.indexOf("=");
				if (i < 0)
					continue;
				var name: String = s.substring(0, i);
				var val: String = s.substring(i+1);
				if (name == "name")
				{
					ads.Name = val;
				} else if (name == "dname")
				{
					ads.DisplayName = val;
				} else if (name == "status")
				{
					ads.Status = int(val);
				} else if (name == "statusm")
				{
					ads.StatusMessage = val;
				} else if (name == "enabled")
				{
					ads.Enabled = true;
				} else if (name == "lastupload")
				{
					var da: Array = val.split("/");
					ads.LastUploadDate = new Date(int(da[2]),int(da[0])-1,int(da[1]));
				} else if (name == "numcols")
				{
					ads.NumColumns = int(val);
				} else if (name == "numrows")
				{
					ads.NumRows = int(val);
				} else if (name == "fsize")
				{
					ads.FileSize = int(val);
				} else if (name == "fexists")
				{
					ads.FileExists = true;
				} else if (name == "ro")
				{
					ads.ReadOnly = true;
				} else if (name == "trans")
				{
					ads.Transactional = true;
				} else if (name == "trunc")
				{
					ads.TruncateOnLoad = true;
				} else if (name == "lockf")
				{
					ads.LockFormat = true;
				} else if (name == "slock")
				{
					ads.SchemaLock = true;
				} else if (name == "allowaddc")
				{
					ads.AllowAddColumns = true;
				} else if (name == "allownullbinding")
				{
					ads.AllowNullBindingRemovedColumns = true;
				} else if (name == "allowup")
				{
					ads.AllowUpcast = true;
				} else if (name == "allowvce")
				{
					ads.AllowVarcharExpansion = true;
				} else if (name == "allowvaltrunc")
				{
					ads.AllowValueTruncationOnLoad = true;
				} else if (name == "failuponvce")
				{
					ads.FailUploadOnVarcharExpansion = true;
				} else if (name == "customup")
				{
					ads.CustomUpload = true;
				} else if (name == "levels")
				{
					ads.Levels = getArrayOfArrayOfString(val);
				} else if (name == "cols")
				{
					ads.StagingColumns = getStagingColumns(val);
				} else if (name == "sfctypes")
				{
					ads.SourceFileColumnTypes = getArrayOfString(val);
				} else if (name == "sfcprevupdates")
				{
					ads.SourceFileColumnPreventUpdate = getArrayOfBoolean(val);
				} else if (name == "sfcformat")
				{
					ads.SourceFileColumnFormat = getArrayOfString(val);
				} else if (name == "sd")
				{
					ads.Script = getScript(val);
				} else if (name == "ctc")
				{
					ads.CustomTimeColumn = val;
				} else if (name == "ctp")
				{
					ads.CustomTimePrefix = val;
				} else if (name == "cts")
				{
					ads.CustomTimeShifts = getTimeShifts(val);
				} else if (name == "subgroups")
				{
					if(val != null && val.length > 0 && StringUtil.trim(val).length > 0)
					{
						ads.SubGroups = getArrayOfString(val);
					}
				} else if (name == "sourcegroups")
				{
					ads.SourceGroups = val;
				} else if (name == "loadgroups")
				{
					ads.LoadGroups = getArrayOfString(val);
				} else if (name == "snapshots")
				{
					ads.Snapshots = getSnapshotPolicy(val);
				} else if (name == "sfcformat")
				{
					ads.SourceFileColumnFormat = getArrayOfString(val);
				} else if (name == "isf")
				{
					ads.IncrementalSnapshotFact = getBoolean(val);
				} else if (name == "sdkeys")
				{
					ads.SnapshotDeleteKeys = getArrayOfString(val);
				} else if (name == "olkeys")
				{
					ads.OverrideLevelKeys = getLevelKeyOverrides(val);
				} else if (name == "nct")
				{
					ads.NewColumnTarget = val;
				} else if (name == "itz")
				{
					ads.InputTimeZone = val;
				} else if (name == "skey")
				{
					ads.SourceKey = getArrayOfString(val);
				} else if (name == "fkeys")
				{
					ads.ForeignKeys = getForeignKeys(val);
				} else if (name == "factfkeys")
				{
					ads.FactForeignKeys = getForeignKeys(val);
				} else if (name == "pfks")
				{
					ads.ParentForeignKeySource = val;
				} else if (name == "hn")
				{
					ads.HierarchyName = val;
				} else if (name == "ln")
				{
					ads.LevelName = val;
				} else if (name == "efm")
				{
					ads.ExcludeFromModel = true;
				} else if (name == "oqas")
				{
					ads.OnlyQuoteAfterSeparator = true;
				} else if (name == "ablfs")
				{
					ads.AllowBirstLocalForSource = true;
				} else if (name == "ublfs")
				{
					ads.UseBirstLocalForSource = true;
				} else if (name == "lcfs")
				{
					ads.LocalConnForSource = val;
				} else if (name == "locations")
				{
					ads.Locations = getLocations(val);
				} else if (name == "enc")
				{
					ads.Encoding = val;
				} else if (name == "sep")
				{
					ads.Separator = val;
				} else if (name == "rwn")
				{
					ads.ReplaceWithNull = true;
				} else if (name == "sr")
				{
					ads.SkipRecord = true;
				} else if (name == "dt")
				{
					ads.DiscoveryTable = true;
				} else if (name == "it")
				{
					ads.ImportedTable = true;
				} else if (name == "la")
				{
					ads.LiveAccess = true;
				} else if (name == "uc")
				{
					ads.UnCached = true;
				} else if (name == "ts")
				{
					ads.TableSource = getTableSource(val);
				} else if (name == "itables")
				{
					ads.InheritTables = getInheritedTables(val);
				} else if (name == "rspath")
				{
					ads.RServerPath = val;
				} else if (name == "rexp")
				{
					ads.RExpression = val;
				} 
			}
			return ads;
		}
		
		private static function getScript(s: String): ScriptDefinition
		{
        	var a: Array = s.split("|1");
			var sd: ScriptDefinition = new ScriptDefinition();
			sd.InputQuery = a[0];
			sd.Output = a[1];
			sd.Script = a[2];
			return sd;			
		}
		
        private static function getArrayOfBoolean(s: String): ArrayOfBoolean
        {
        	var aob: ArrayOfBoolean = new ArrayOfBoolean();
        	var a: Array = s.split("|1");
        	for each(var cs: String in a)
        	{
        		aob.addItem(Boolean(cs == "True" || cs == "true"));
        	}
        	return aob;
        }
        
        private static function getArrayOfString(s: String): ArrayOfString
        {
        	var aos: ArrayOfString = new ArrayOfString();
        	if (s == null)
        		return aos;
        	var a: Array = s.split("|1");
        	for each(var cs: String in a)
        	{
        		aos.addItem(cs);
        	}
        	return aos;
        }
        
        private static function getArrayOfArrayOfString(s: String): ArrayOfArrayOfString
        {
        	var aaos: ArrayOfArrayOfString = new ArrayOfArrayOfString();
        	var a: Array = s.split("|2");
        	for each(var cs: String in a)
        	{
        		aaos.addItem(getArrayOfString(cs));
        	}
        	return aaos;
        } 

        private static function getTimeShifts(s: String): ArrayOfCustomTimeShift
        {
        	var acts: ArrayOfCustomTimeShift = new ArrayOfCustomTimeShift();
        	var a: Array = s.split("|5");
        	for each(var cs: String in a)
        	{
        		acts.addItem(getArrayOfString(cs));
        	}
        	return acts;
        }
        
        private static function getLevelKeyOverrides(s: String): ArrayOfOverrideLevelKey
        {
        	var acts: ArrayOfOverrideLevelKey = new ArrayOfOverrideLevelKey();
        	var a: Array = s.split("|5");
        	for each(var cs: String in a)
        	{
        		var b: Array = cs.split("|2");
        		var olk: OverrideLevelKey = new OverrideLevelKey();
        		olk.Dimension = b[0];
        		olk.Level = b[1];
        		olk.KeyColumns = getArrayOfString(b[2]);
        		acts.addItem(olk);
        	}
        	return acts;
        }
        
        private static function getForeignKeys(s: String): ArrayOfForeignKey
        {
        	var afk: ArrayOfForeignKey = new ArrayOfForeignKey();
        	var a: Array = s.split("|5");
        	for each(var cs: String in a)
        	{
        		var b: Array = cs.split("|2");
        		if (b.length < 4)
        			continue;
        		var fk: ForeignKey = new ForeignKey();
        		fk.Source = b[0];
        		fk.Type = b[1];
        		fk.Invalid = getBoolean(b[2]);
        		fk.Redundant = getBoolean(b[3]);
        		if (b.length > 4)
        		{
        			fk.Condition = b[4];
        			fk.Condition = Util.trim(fk.Condition);
        			if (fk.Condition.length == 0)
        			{
        				fk.Condition = null;
        			}
        		}
        		afk.addItem(fk);
        	}
        	return afk;
        }

        private static function getLocations(s: String): ArrayOfSourceLocation
        {
        	var aosl: ArrayOfSourceLocation = new ArrayOfSourceLocation();
        	var a: Array = s.split("|5");
        	for each(var cs: String in a)
        	{
        		var b: Array = cs.split("|2");
        		var sl: SourceLocation = new SourceLocation();
        		sl.LoadGroupName = b[0];
        		sl.x = int(b[1]);
        		sl.y = int(b[2]);
        		aosl.addItem(sl);
        	}
        	return aosl;
        }

        private static function getInheritedTables(s: String): ArrayOfInheritTable
        {
        	var aoit: ArrayOfInheritTable = new ArrayOfInheritTable();
        	var a: Array = s.split("|5");
        	for each(var cs: String in a)
        	{
        		var b: Array = s.split("|");
        		var it: InheritTable = new InheritTable();
            	if (b.length != 3)
            		continue;
            	it.Type = int(b[0]);
            	it.Name = b[1];
            	it.Prefix = b[2];
        		aoit.addItem(it);
        	}
        	return aoit;
        }
        
        private static function getBoolean(s: String): Boolean
        {
        	if (s.toLowerCase() == "true")
        		return true;
        	return false;
        }

        private static function getCustomTimeShift(s: String): CustomTimeShift
        {
        	var cts: CustomTimeShift = new CustomTimeShift();
        	var a: Array = s.split("|2");
        	cts.BaseKey = a[0];
        	cts.RelativeKey = a[1];
        	cts.Prefix = a[2];
        	cts.Columns = getArrayOfString(a[3]);
        	return cts;
        }

        private static function getSnapshotPolicy(s: String): SnapshotPolicy
        {
        	var sp: SnapshotPolicy = new SnapshotPolicy();
        	var a: Array = s.split("|2");
        	sp.CurrentDay = getBoolean(a[0]);
        	sp.Type = int(a[1]);
        	sp.DaysOfWeek = int(a[2]);
        	sp.DayOfMonth = int(a[3]);
        	sp.firstDayOfMonth = getBoolean(a[4]);
        	sp.lastDayOfMonth = getBoolean(a[5]);
        	return sp;
        }
        
        private static function getSecurityFilter(s: String): SecurityFilter
        {
        	var sf: SecurityFilter = new SecurityFilter();
        	var a: Array = s.split("|4");
        	sf.Type = int(a[0]);
        	sf.SessionVariable = a[1];
        	sf.Enabled = getBoolean(a[2]);
        	if (a.length >= 4)
        		sf.FilterGroups = getArrayOfString(a[3]);
        	return sf;
        }
        
        private static function getStagingColumns(s: String): ArrayOfStagingColumn
        {
        	var aosc: ArrayOfStagingColumn = new ArrayOfStagingColumn();
        	var params: Array = s.split("|5");
			for each (var ps: String in params)
			{
				if (ps.length == 0)
					continue;
				var sc: StagingColumn = getStagingColumn(ps);
				aosc.addItem(sc);
			}
			return aosc;
        }
        
        private static function getStagingColumn(s: String): StagingColumn
        {
        	var sc: StagingColumn = new StagingColumn();
        	var params: Array = s.split("|3");
        	
			for each (var ps: String in params)
			{
				var i: int = ps.indexOf("=");
				var name: String = null;
				var val: String = null;
				if (i < 0)
				{
					name = ps;
				} else
				{
					name = ps.substring(0, i);
					val = ps.substring(i+1);	
				}
				if (name == "n")
				{
        			sc.Name = val;
				} else if (name == "d")
				{
					sc.DataType = val;
				} else if (name == "w")
				{
					sc.Width = int(val);
				} else if (name == "s")
				{
					sc.SourceFileColumn = val;
				} else if (name == "t")
				{
					sc.TargetTypes = getArrayOfString(val);
				} else if (name == "k")
				{
					sc.NaturalKey = true;
				} else if (name == "g")
				{
					sc.GenerateTimeDimension = true;
				} else if (name == "i")
				{
					sc.Index = true;
				} else if (name == "u")
				{
					sc.UnknownValue = val;
				} else if (name == "a")
				{
					sc.TargetAggregations = getArrayOfString(val);
				} else if (name == "e")
				{
					sc.ExcludeFromChecksum = true;
				} else if (name == "y")
				{
					sc.TableKey = true;
				} else if (name == "f")
				{
					sc.SecFilter = getSecurityFilter(val);
				} else if (name == "m")
				{
					sc.AnalyzeMeasure = true;
    			}
			}
			if (sc.DataType == null)
				sc.DataType = "";
        	return sc;
        }

        private static function getTableSource(s: String): TableSource
        {
        	var ts: TableSource = new TableSource();
        	var a: Array = s.split("|");
            if (String(a[0]).length > 0)
            {
                ts.Schema = a[0];
                var conindex: int = ts.Schema.indexOf("/");
                if (conindex >= 0)
                {
                    var c: String = ts.Schema;
                    ts.Schema = c.substring(0, conindex);
                    ts.Connection = c.substring(conindex + 1);
                }
            }
            ts.Tables = new ArrayOfTableDefinition();
            for (var i: int = 1; i < a.length; i++)
            {
            	var item: String = String(a[i]);
            	if (item.charAt(0) == 'D')
            	{
            		if (ts.Tables == null)
            			ts.Tables = new ArrayOfTableDefinition();
            		ts.Tables.addItem(getTableDefinition(item.substring(1)));
            	}
            	else if (item.charAt(0) == 'F')
            	{
            		if (ts.Filters == null)
            			ts.Filters = new ArrayOfTableSourceFilter();
            		ts.Filters.addItem(getTableSourceFilter(item.substring(1)));
            	}
            }
        	return ts;
        }
        
        private static function getTableSourceFilter(s: String): TableSourceFilter
        {
        	var tsf: TableSourceFilter = new TableSourceFilter();
            tsf.CurrentLoadFilter = (s.charAt(0) == '1');
            tsf.SecurityFilter = (s.charAt(1) == '1');
            var a: Array = s.substring(2).split(',');
            tsf.Filter = a[0];
            if (a.length > 1)
            {
                tsf.FilterGroups = new ArrayOfString();
                for (var i: int = 1; i < a.length; i++)
                {
                    tsf.FilterGroups.addItem(String(a[i]));
                }
            }
            return tsf;
        }
        
        private static function getTableDefinition(s: String): TableDefinition
        {
        	var td: TableDefinition = new TableDefinition();
        	var a: Array = s.split("\\");
        	td.Schema = a[0];
        	td.PhysicalName = String(a[1]);
        	td.JoinType = int(a[2]);
        	if (a.length > 3)
        		td.JoinClause = a[3];
        	return td;
        }
	}
}