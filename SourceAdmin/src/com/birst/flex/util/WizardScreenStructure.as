package com.birst.flex.util
{
	public class WizardScreenStructure
	{
		public var screenIndex:uint;
		public var cancel:Boolean;
		public var done:Boolean; 
		public var previous:Boolean;
		public var next:Boolean;
		public function WizardScreenStructure(_screenIndex:uint=0, _cancel:Boolean=true, _done:Boolean=true,
		_previous:Boolean=false, _next:Boolean=false)		
		{
			this.screenIndex = _screenIndex;
			this.cancel = _cancel;
			this.done = _done;
			this.previous = _previous;	
			this.next = _next;	
		}

	}
}