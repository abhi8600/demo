package com.birst.flex.util
{
	import com.birst.flex.core.Tracer;
	
	import generated.webservices.AdminService;
	import generated.webservices.ArrayOfPerformanceEngineVersion;
	import generated.webservices.ArrayOfReleaseInfo;
	import generated.webservices.GetPerformanceEngineVersionsResultEvent;
	import generated.webservices.GetReleaseInfoDetailsResultEvent;
	import generated.webservices.PerformaceEngineDetails;
	import generated.webservices.ReleaseDetails;
	
	public class StaticUtils
	{
		public static var releasesInfoArray:ArrayOfReleaseInfo;
		public static var performanceEngineVersionsArray:ArrayOfPerformanceEngineVersion;	
		private static var adminService:AdminService;
		public function StaticUtils()
		{
		}
		
		public static function getReleasesDetails():ArrayOfReleaseInfo
		{
			return releasesInfoArray;			
		}
		
		public static function setReleasesDetails():void
		{		
			// fill up the array
			adminService = ServiceUtil.getWebServiceClient();
			var releasesEventHandler:Function = function(e:GetReleaseInfoDetailsResultEvent):void
			{
				if(e != null && e.result != null)
				{
					var result:ReleaseDetails = e.result;
					if(ServiceUtil.isSuccess(result.Error))
					{
						releasesInfoArray = result.ReleasesInfo;
					}
				}			
			};
			adminService.addgetReleaseInfoDetailsEventListener(releasesEventHandler);
			adminService.getReleaseInfoDetails();
		}
		
		public static function getPerformanceEngineVersions():ArrayOfPerformanceEngineVersion
		{
			return performanceEngineVersionsArray;
		}
		
		public static function setPerformanceEngineVersions():void
		{
			// fill up the array
			adminService = ServiceUtil.getWebServiceClient();
			var peVersionsEventHandler:Function = function(e:GetPerformanceEngineVersionsResultEvent):void
			{
				if(e != null && e.result != null)
				{
					var result:PerformaceEngineDetails = e.result;
					if(ServiceUtil.isSuccess(result.Error))
					{
						performanceEngineVersionsArray = result.PerformanceEngineVersions;
					}
				}			
			};
			adminService.addgetPerformanceEngineVersionsEventListener(peVersionsEventHandler);
			adminService.getPerformanceEngineVersions();
		}
	}
}