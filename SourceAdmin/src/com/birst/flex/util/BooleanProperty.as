package com.birst.flex.util
{
	
	[Bindable]
	public class BooleanProperty
	{			
		public function BooleanProperty()
		{			
		}
				
		private var _boolProp:Boolean;
		
		public function get boolProp():Boolean{
			return _boolProp;
		} 
		
		public function set boolProp(value:Boolean):void{
			_boolProp = value;
		}
	}
}