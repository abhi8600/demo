package com.birst.flex.util
{
    import mx.containers.*;
    import mx.controls.Text;
    import mx.controls.ToolTip;
    import mx.core.*;

    public class TooltipHTML extends ToolTip
    {
        public function TooltipHTML()
        {  super(); }

        override protected function commitProperties():void{
            super.commitProperties();
            textField.htmlText = text;
        }
    }
}