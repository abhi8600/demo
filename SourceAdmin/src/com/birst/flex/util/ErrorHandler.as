package com.birst.flex.util
{
	// common error handler for web service errors
	
	import mx.rpc.soap.SOAPFault;
	import com.birst.flex.core.ConnectionErrorDialog;	
	import generated.webservices.AdminService;
	import com.birst.flex.core.*;
	import mx.events.CloseEvent;
	import mx.controls.Alert;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.soap.WebService;
	import mx.managers.PopUpManager;
	import flash.display.*;
	
	public class ErrorHandler
	{
		private var svc:AdminService;
		private var displayObject:DisplayObject;

		public function ErrorHandler(service:AdminService, dispO:DisplayObject)
		{
			svc = service;
			displayObject = dispO;
		}
		
		private function checkNull(str:Object):Object{
        	return (str) ? str : '';	
        }
		
        private var alertNoLabel:String;
        private var detailErrorMessage:String = null;
        
        public function getHandler():Function
        {
        	return webserviceFailed;
       	}
       	
		// Silently absorb error
		private function absorbError(faultMessage:String):Boolean{
			// When a user tries to rapdily move from one Admin to Designer
			// browser cancels the event and ends up throwing the error with Error #2032
			// e.g. faultCode:Server.Error.Request faultString:'HTTP request error' faultDetail:'Error: [IOErrorEvent type="ioError" bubbles=false cancelable=false eventPhase=2 text="Error #2032: .....
			if(faultMessage != null && faultMessage.indexOf('HTTP request error') >=0 && 
				faultMessage.indexOf('Error #2032') >=0){
					return true;
				} 
			return false;
		}
 
		private function webserviceFailed(event:FaultEvent):void {
        	svc.removeEventListener(FaultEvent.FAULT, getHandler());

			var faultString:String;
        	var details:Boolean = false;
        	var titleMessage:String = "Unexpected Error";
        	var errorMessage:String = "An unexpected error occurred. Click Details to get more information.";
        	
        	if (event.fault is SOAPFault)
        	{
                var fault:SOAPFault = event.fault as SOAPFault;
                var faultElement:XML = fault.element;
                faultString = faultElement.faultstring.toString();
                if (faultString != null && faultString.indexOf('Acorn.Exceptions.UserNotLoggedInException') >= 0)
                {
                	titleMessage = "Invalid Session";
                	errorMessage = "The session has timed out or the server was restarted.  Please log in again.";
                }
                else if (faultString != null && faultString.indexOf('Acorn.Exceptions.NotAuthorizedException') >= 0)
                {
                	titleMessage = "Not Authorized";
                	errorMessage = "You are not authorized for this operation.";
                }
                else if (faultString != null && faultString.indexOf('Acorn.Exceptions.NotAllowedException') >= 0)
                {
                	titleMessage = "Operation Not Allowed";
                	errorMessage = "You tried to perform an operation that is not allowed.";
                }                
                else
                {
                	detailErrorMessage = 'SOAP FAULT:\n' + checkNull(faultElement.toXMLString());
                	details = true;
                }
         	}
         	else
         	{
         		faultString = event.fault.message;
         	    if (faultString != null && faultString.indexOf('HTTP request error') >= 0)
                {
                	if(absorbError(faultString)){
                		return;
                	}
                	titleMessage = "Connection Error";
                	errorMessage = "Could not communicate with the server. The server may be unavailable or there may be a network problem. Please try again (you may need to log in again).";
                }                
                else
                {
                	detailErrorMessage = 'FAULT:\n' + checkNull(event.fault.message) + '\n\nCONTENT:\n' + checkNull(event.fault.content);
                	details = true;
                }
          	}

         	var flags:uint = Alert.OK;
          	if (details)
         		flags = Alert.NO | Alert.OK; // include the 'Details' button to pop up the details dialog (for support)         	
         	alertNoLabel = Alert.noLabel;
         	Alert.noLabel = "Detail";
         	Alert.okLabel = "OK";
         	
         	Alert.show(errorMessage, titleMessage, flags, null, faultAlertCloseFunc);
		}

        private function faultAlertCloseFunc(event:CloseEvent):void
        {
        	if(event.detail == Alert.NO)
        	{
        		var popup:ConnectionErrorDialog = ConnectionErrorDialog(PopUpManager.createPopUp(displayObject, ConnectionErrorDialog, true));
        		popup.setup(detailErrorMessage, svc, getHandler());
        		PopUpManager.centerPopUp(popup);
        	}
        	else
        		svc.addAdminServiceFaultEventListener(webserviceFailed);
        	Alert.noLabel = alertNoLabel;
        }
    }
}