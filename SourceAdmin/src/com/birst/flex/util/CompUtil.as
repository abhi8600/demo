package com.birst.flex.util
{
	import generated.webservices.AdminDataSource;
	import com.birst.flex.util.ServiceUtil;
	
	public class CompUtil
	{
		public function CompUtil()
		{
		}
		
		public static function updateAdminDataSource(ads:AdminDataSource , updatedAds:AdminDataSource):void
		{
			if(ads == null || (updatedAds != null && updatedAds.StagingColumns == null))
			{
				ServiceUtil.generalAlert("Unable to get Source Details");
				return;
			}
			
			ads.StagingColumns = updatedAds.StagingColumns;						
		}
		
		public static function isUpdateSourceRequired(ads:AdminDataSource):Boolean
		{
			var req:Boolean = true;
			if(ads != null && ads.StagingColumns != null && ads.StagingColumns.length > 0)
			{
				req = false;
			}
			
			return req;
		}
	}
}