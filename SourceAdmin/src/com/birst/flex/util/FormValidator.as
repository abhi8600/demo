package com.birst.flex.util
{	
	import mx.core.UIComponent;
	import flash.events.Event;
	import flash.display.DisplayObject;
	import flash.events.EventDispatcher;
	import flash.events.IEventDispatcher;
	
	import mx.events.ValidationResultEvent;
	import mx.validators.Validator;

	// very close to FormValidator.as in FlexAdhoc, only difference is the addition of the ui component to enable
	public class FormValidator
	{
		private var formIsValid:Boolean = false;
		private var focusedFormControl:DisplayObject;
		private var componentToEnable:UIComponent;
		private var validators:Array;

		public function FormValidator(vals:Array,component:UIComponent)
		{
			validators = vals;
			componentToEnable = component;
		}

		public function validateForm(event:Event):void
		{
			focusedFormControl = event.target as DisplayObject;
			formIsValid = true;
 
			for each (var validator:Validator in validators) {
				if (validator.source == null) {
					continue;
				}
				validate(validator);
			}
			if(componentToEnable != null)
			{
				componentToEnable.enabled = formIsValid;
			}
		}
			
		private function validate(validator:Validator):Boolean
		{
			var validatorSource:DisplayObject = validator.source as DisplayObject;
			var supressEvents:Boolean = validatorSource != focusedFormControl;
			var event:ValidationResultEvent = validator.validate(null, supressEvents)
			var currentControlIsValid:Boolean = event.type == ValidationResultEvent.VALID;
 
			formIsValid = formIsValid && currentControlIsValid;
			return currentControlIsValid;
		}
	}
}