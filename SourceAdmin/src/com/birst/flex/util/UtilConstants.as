package com.birst.flex.util
{
	public class UtilConstants
	{
		public function UtilConstants()
		{
		}
		
		
		public static const allSubGroups:String = "All";
		public static const OP_COPY_TO_NEW:int = 1;
        public static const OP_COPY_TO_EXISTING:int = 2;
        public static const OP_COPY_FROM:int = 3;
        public static const OP_SWAP:int = 4;
        public static const OP_DELETE_DATA:int = 5;
        public static const OP_DELETE_SPACE:int = 6;
        
        // package specific constants
        public static const PACKAGE_OBJECT_TYPE_STAGING_TABLE:int = 1;
        public static const PACKAGE_OBJECT_TYPE_DIMENSION_TABLE:int = 2;
        public static const PACKAGE_OBJECT_TYPE_MEASURE_TABLE:int = 3;
        public static const PACKAGE_OBJECT_TYPE_VARIABLE:int = 4;
        public static const PACKAGE_OBJECT_TYPE_DIMENSION:int = 5;
        public static const PACKAGE_OBJECT_TYPE_AGGREGATE:int = 8;
        public static const PACKAGE_OBJECT_TYPE_CUSTOM_SA:int = 9;

        // additional collision specific
        public static const PACKAGE_COLLISION_CIRCULAR_REFERENCE:int = 6;
        public static const PACKAGE_COLLISION_DB_MISMATCH:int = 7;
        
        
        public static const PACKAGE_OBJECT_DEPEDENT_TYPE_HIERARCHY:int = 101;
        public static const PACKAGE_OBJECT_DEPEDENT_TYPE_VARIABLE:int = 102;
        public static const PACKAGE_OBJECT_DEPEDENT_TYPE_CUSTOM_ATTRIBUTE:int = 103;
        public static const PACKAGE_OBJECT_DEPEDENT_TYPE_CUSTOM_MEASURE:int = 104;
        public static const PACKAGE_OBJECT_DEPEDENT_TYPE_BUCKETED_MEASURE:int = 105;
        
        public static const PACKAGE_STATUS_AVAILABLE:int = 1;
        public static const PACKAGE_STATUS_DELETED:int = 2;
        public static const PACKAGE_STATUS_ACCESS_REMOVED:int = 3;
        public static const PACKAGE_STATUS_UNAVAILABLE:int = 4;
        public static const PACKAGE_STATUS_COLLISION:int = 5;
        
        // Feature flag constants
        public static const VIZ_ACL_GA:String = "EnableVisualizerForAll";
	}
}