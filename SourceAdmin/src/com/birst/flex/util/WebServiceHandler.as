package com.birst.flex.util
{
	import flash.events.Event;
	
	import generated.webservices.AdminService;
	
	import mx.rpc.events.FaultEvent;
	
	public class WebServiceHandler
	{
		import mx.managers.CursorManager;
		private var svc:AdminService;
		public function WebServiceHandler()
		{
		}
		
		
		public function configure(_svc:AdminService):void
		{
			this.svc = _svc;			
			svc.addEventListener(FaultEvent.FAULT, faultHandler);
			svc.addAdminServiceFaultEventListener(faultHandler);					
		}
		
		public function setCursor():void
		{			
			CursorManager.setBusyCursor();
		}
		
		public function removeCursor():void
		{
			//Tracer.debug('removeCursor', this);
			CursorManager.removeBusyCursor();			
		}
		
		private function faultHandler(event:Event):void
		{
			//Tracer.debug('faultHandler', this);
			CursorManager.removeBusyCursor();
		}		
		
	}
}