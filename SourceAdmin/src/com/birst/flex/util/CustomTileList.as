package com.birst.flex.util
{
	import flash.display.Graphics;
	import flash.display.Sprite;
	
	import mx.controls.TileList;
	import mx.controls.listClasses.IListItemRenderer;

	public class CustomTileList extends TileList
	{
		public function CustomTileList()
		{
			super();
		}
		
		
		override protected function drawSelectionIndicator(
                                indicator:Sprite, x:Number, y:Number,
                                width:Number, height:Number, color:uint,
                                itemRenderer:IListItemRenderer):void
	    {
	        var g:Graphics = Sprite(indicator).graphics;
	        g.clear();
	        g.lineStyle(1,0xd4d4d4);
	        g.beginFill(color);
	        g.drawRect(0, 0, width, height);
	        g.endFill();
	        
	        indicator.x = x;
	        indicator.y = y;
	    }
	    	   
	    override protected function drawHighlightIndicator(
	                                indicator:Sprite, x:Number, y:Number,
	                                width:Number, height:Number, color:uint,
	                                itemRenderer:IListItemRenderer):void
	    {
	        var g:Graphics = Sprite(indicator).graphics;
	        g.clear();
	        g.lineStyle(1,0xd4d4d4);
	        g.beginFill(color);
	        g.drawRect(0, 0, width, height);
	        g.endFill();
	        
	        indicator.x = x;
	        indicator.y = y;
	    }
 }
}