package com.birst.flex.util
{
	import generated.webservices.AdminService;
	import generated.webservices.GetLoggedInSpaceDetailsResultEvent;
	import generated.webservices.SpaceDetails;
	
	public class ModulePermissions
	{
		
		
		public static var P_CATALOG = "Catalog";	
		private var _svc:AdminService;
		private var _module:String;
		public function ModulePermissions()
		{
		}		
		
	
		
		public function getPermissions(svc:AdminService, module:String):void
		{
			_svc = svc;
			getPermissions();
		}
		
		private function getPermissions():void
		{
			_svc.removeEventListener(GetLoggedInSpaceDetailsResultEvent.GetLoggedInSpaceDetails_RESULT, getLoggedInSpaceDetailsHandler);
			_svc.addgetLoggedInSpaceDetailsEventListener(getLoggedInSpaceDetailsHandler);
			_svc.getLoggedInSpaceDetails();
		}
		
		private function getLoggedInSpaceDetailsHandler(event:GetLoggedInSpaceDetailsResultEvent):Boolean
		{
			var isPermitted:Boolean = false;
			var response:SpaceDetails = event.result;
			if(response == null)
			{	
				return isPermitted;
			}
			
			if(!ServiceUtil.isSuccess(response.Error))
			{
				return isPermitted;
			}
			
			
			if(response.Administrator && response.AdminAccess != null)
			{
				if(_module == P_CATALOG && response.AdminAccess.ManageCatalog)
				{
					return isPermitted;
				}
			}
			
			return isPermitted;
		}
	}
}