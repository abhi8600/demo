package com.birst.flex.util
{
	import generated.webservices.ErrorOutput;
	import generated.webservices.GenericResponse;
	
	// Used for parsing the SMIWeb wrapped in Acorn GenericResponse tag
	public class WServiceResult
	{
		private var genericResponse:GenericResponse;
		private var wrappedResponse:XML;
		private const SUCCESS:Number = 0;
		
		public function WServiceResult(res:GenericResponse){
			this.genericResponse = res;
			wrappedResponse = getWrappedResponseXml();
		}
		
		public static function getWebserviceResult(res:GenericResponse):WServiceResult{
			return new WServiceResult(res);
		}
		
		private function getWrappedResponseXml():XML{
			if(genericResponse != null && genericResponse.other && genericResponse.other.length > 0){
				var str:String = genericResponse.other.getStringAt(0);
				if(str != null){
					return XML(str);
				}
			}
			return null;
		}
		
		public function isSuccessful():Boolean{
			if(genericResponse != null && !ServiceUtil.isSuccess(genericResponse.Error)){
				return false;
			}
			
			return isWrappedResponseSuccess();
		}
		
		private function isWrappedResponseSuccess():Boolean{
			return getWrappedErrorCode() == SUCCESS;
		}
		
		public function getWrappedErrorCode():Number {
			return wrappedResponse != null ? Number(wrappedResponse.ErrorCode) : SUCCESS;
		}
		
		public function getWrappedErrorMessage():String {
			return wrappedResponse != null ? wrappedResponse.ErrorMessage : null;
		}
		
		public function getResult():XML{
			return wrappedResponse != null ? wrappedResponse.Result[0] : null;
		}
		
		public function getCombinedError():ErrorOutput{
			var error:ErrorOutput = this.genericResponse.Error;
			if(error == null && !isWrappedResponseSuccess()){
				error = new ErrorOutput();
				error.ErrorMessage = getWrappedErrorMessage();
				error.ErrorType = getWrappedErrorCode();
			}
			return error;
		}
	}
}