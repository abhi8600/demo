package com.birst.flex.util
{
	import com.birst.flex.core.BirstAlertDialog;
	import com.birst.flex.core.CoreApp;
	
	import flash.net.URLRequest;
	import flash.net.navigateToURL;
	import flash.utils.getTimer;
	
	import generated.webservices.AdminService;
	import generated.webservices.ArrayOfString;
	import generated.webservices.ErrorOutput;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.core.Application;
	import mx.core.IFlexDisplayObject;
	import mx.core.UIComponent;
	import mx.managers.CursorManager;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;
	import mx.utils.URLUtil;
	
	[ResourceBundle("errors")]
	[ResourceBundle("navErrors")]
	[ResourceBundle("adminSharable")]
	
	public class ServiceUtil
	{
		public function ServiceUtil()
		{
		}
		
		public static var xDim:int = 200;
		public static var yDim:int = 150;
		public static var navDispatcher:NavigateDispatcher;
		public static var REDIRECT_ERROR_TYPE_1:Number = -8;
		public static var REDIRECT_ERROR_TYPE_2:Number = -118;
		public static var triggerDelayStartTime:uint = 0;
		public static var navObj:Navigation;
		private static var sourceProcessingTimeZones:ArrayCollection;
		private static var spaceProcessingTimeZones:ArrayCollection;
		
		public static var GLOBAL_TIMEZONE_DEFAULT:String = "Server Local Time";
        public static var SOURCE_TIMEZONE_DEFAULT:String = "Same As Processing Time Zone";
		private static var superUser:Boolean = false;
		
		private static var userManagementInstance:UserManagement;
		public static const BUNDLE_ADMIN_ERRORS:String = "navErrors"; 
		public static const BUNDLE_ADMIN_SHARABLE:String = "adminSharable";
		public static const BUNDLE_NAVIGATION:String = "navigation"; 
		
		public static function alertErrMessage(heading:String, error:ErrorOutput=null):void{
			var alert:Alert = Alert.show(formatErrMessage(heading, error), "Error");
			PopUpManager.centerPopUp(alert);
		}
				
		public static function formatErrMessage(heading:String, error:ErrorOutput):String{
			if(error == null || error.ErrorMessage == null)
			{
				return heading;
			}
			
			return heading + '\nMessage:\n' + error.ErrorMessage;
		}				
		
		public static function generalAlert(text:String, title:String="Error", xDimension:int=-1, yDimension:int=-1):void
		{
			var alert:Alert = Alert.show(text,title);			
			PopUpManager.centerPopUp(alert);
			var newXDim:int = xDimension < 0 ? xDim : xDimension;
			var newYDim:int = yDimension < 0 ? yDim : yDimension; 
			alert.move(newXDim, newYDim); 			
		}
		
		public static function localeGeneralAlert(keyText:String, keyTitle:String, bundle:String=null):void
		{
			if(bundle == null)
			{
				bundle = getAppMainBundle();
			}
			var txt:String = ResourceManager.getInstance().getString(bundle, keyText);
			var title:String = "Error";
			if(keyTitle == null)
			{
				title = ResourceManager.getInstance().getString("errors","AL_Title"); 
			}
			else
			{
				title = ResourceManager.getInstance().getString(bundle, keyTitle);
				 
			}			
			
			title = title != null ? title : "Message";				
			var alert:Alert = Alert.show(txt, title);						
			PopUpManager.centerPopUp(alert);
			alert.move(xDim, yDim);
		}
		
		public static function getLocString(key:String, bundle:String, params:Array=null):String
		{
			return ResourceManager.getInstance().getString(bundle, key, params);
		}
				
		public static function localeAlertErrMessage(headingLocKey:String, bundle:String, error:ErrorOutput=null, params:Array=null):void{
			var title:String = ResourceManager.getInstance().getString(bundle, 'AL_Title');
			var alert:Alert = Alert.show(localeFormatErrMessage(headingLocKey, bundle, error, params), title);
			PopUpManager.centerPopUp(alert);
		}
				
		public static function localeFormatErrMessage(headingLocKey:String, bundle:String, error:ErrorOutput=null,  params:Array=null):String{
			var heading:String = ResourceManager.getInstance().getString(bundle, headingLocKey, params);
			if(error == null)
			{
				return heading;
			}
			
			params = params == null ? [error.ErrorMessage] : params;
			
			var resourceError:String = ResourceManager.getInstance().getString(bundle,error.ErrorType.toString(), params);
			
			if(resourceError == null || resourceError.length == 0)
			{
				resourceError = error.ErrorMessage;
			}
			
			if(resourceError != null && resourceError.length > 0)
			{
				return heading + '\nMessage:\n' + resourceError;
			}
			
			return heading;			
		}
		
		
		public static function getBool(input:String):Boolean
		{
			var result:uint = 0; // corresponding to false boolean value
			
			if(input.toLowerCase() == "true")
			{
				result = 1;
			}
			
			return (new Boolean(result));
		}
		
		public static function isSuccess(errorOutput:ErrorOutput):Boolean
		{
			return (errorOutput == null || errorOutput.ErrorType == 0?true:false);	
		}
		
		public static function doRedirect(errorOutput:ErrorOutput):Boolean
		{
			return (errorOutput.ErrorType == REDIRECT_ERROR_TYPE_1 || errorOutput.ErrorType == REDIRECT_ERROR_TYPE_2 )? true:false;
		}
		
		public static function navigateToHome():void
		{
			navigateToURL(new URLRequest('FlexModule.aspx?birst.module=home'), '_self');
		}
		
		public static function navigateToSourceAdmin():void
		{
			navigateToURL(new URLRequest('FlexModule.aspx?birst.module=admin'), '_self');
		}
		
		public static function navigateToDashboard():void
		{
			navigateToURL(new URLRequest('FlexModule.aspx?birst.module=dashboard'), '_self');			
		}
		
		public static function navigateToDesigner():void
		{
			navigateToURL(new URLRequest('FlexModule.aspx?birst.module=designer'), '_self');			
		}
		
		public static function getNavDispatcher():NavigateDispatcher
		{
			return navDispatcher;
		}	
		
		public static function setBusyCursor():void
		{
			CursorManager.setBusyCursor();
		}
		
		public static function navigateToManageSource():void
		{
			if(navDispatcher != null)
			{
				navDispatcher.refresh();				
			}
		}	
	
		public static function alignPopUp(popUp:IFlexDisplayObject, xDimension:int=0, yDimension:int=0):void
		{
			var newX:Number = xDimension > 0 ? xDimension : xDim;
			var newY:Number = yDimension > 0 ? yDimension : yDim;			
			popUp.move(newX, newY);
		}
		
		public static function setDelay():void
		{
			triggerDelayStartTime = getTimer();
		}
		
		public static function isDelayRequired():Boolean
		{
			var delayRequired:Boolean = false;
			if(triggerDelayStartTime != 0)
			{
				var duration:int = getTimer() - triggerDelayStartTime;
				if(duration < 5000)
				{
					delayRequired = true;
				}
				
				// reset the delay time..no delay unless set explictly
				triggerDelayStartTime = 0;	
			}
			
			return delayRequired;
		}
		
		// false means return source Level
		public static function getProcessingTimeZones(spaceLevel:Boolean=false):ArrayCollection		
		{						
			if(spaceLevel)
			{
				return spaceProcessingTimeZones;
			}
			else
			{
				return sourceProcessingTimeZones;
			}
		}	
		
		// true means global level -- the difference is the way Default is shown
		public static function setProcessingTimeZones(arrayColl:ArrayOfString):void
		{
			spaceProcessingTimeZones = new ArrayCollection();
			sourceProcessingTimeZones = new ArrayCollection();
			for each(var str:String in arrayColl)
			{
				if(str == "Default")
				{
					spaceProcessingTimeZones.addItem(GLOBAL_TIMEZONE_DEFAULT);
					sourceProcessingTimeZones.addItem(SOURCE_TIMEZONE_DEFAULT);		
				}
				else
				{
					spaceProcessingTimeZones.addItem(str);
					sourceProcessingTimeZones.addItem(str);
				}			
			}			
		}
		
		public static function setSuperUser(isSuper:Boolean=false):void
		{			
			superUser = isSuper;
		}		
		
		public static function isSuperUser():Boolean
		{
			return superUser;	
		}
		
		public static function getAppMainBundle():String{
			if (Application.application is CoreApp){
				return CoreApp(Application.application).mainBundle;
			}
			
			return 'sharable';
		}
		
		public static function getBaseUrl():String 
		{
			var fullUrl:String = Application.application.url;
			return URLUtil.getProtocol(fullUrl) + "://" + URLUtil.getServerNameWithPort(fullUrl) + "/";
		}
		
		public static function getWebServiceClient():AdminService
		{
			var svc:AdminService = new AdminService(null, getBaseUrl() + "AdminService.asmx");
			var header:Object = new Object();
            header["X-XSRF-TOKEN"] = Application.application.xsrfToken;
            svc.getWebService().httpHeaders = header;
			return svc;
		}
		
		public static function setUserManagementInstance(instance:UserManagement):void
		{
			userManagementInstance = instance;		
		}
		
		public static function getUserManagementInstance(instance:UserManagement):UserManagement
		{
			return userManagementInstance;		
		}
		
		public static function isPropertyEnabled(props:ArrayOfString, property:String):Boolean
		{
			var propEnabled:Boolean = false;			
			if(props != null && props.length > 0)
			{
				for each(var prop:String in props)
				{
					var splitArray:Array = prop.split('=');
					if(splitArray != null && splitArray.length == 2)
					{
						var key:String = splitArray[0];
						var value:String = splitArray[1];
						if(key == property && value.toLowerCase() == "true")
						{
							propEnabled = true;
							break;
						}
					}
				}
			}
			return propEnabled;
		}
		
		public static function getPackageObjectType(type:int, baseObject:Boolean):String
		{
			if(baseObject)
			{
				if(type == UtilConstants.PACKAGE_OBJECT_TYPE_STAGING_TABLE) { return "Source";}
				if(type == UtilConstants.PACKAGE_OBJECT_TYPE_DIMENSION_TABLE) { return "Dimension Table";}
				if(type == UtilConstants.PACKAGE_OBJECT_TYPE_MEASURE_TABLE) { return "Measure Table";}
				if(type == UtilConstants.PACKAGE_OBJECT_TYPE_VARIABLE) { return "Variable";}
				if(type == UtilConstants.PACKAGE_OBJECT_TYPE_DIMENSION) { return "Hierarchy";}	
				if(type == UtilConstants.PACKAGE_OBJECT_TYPE_AGGREGATE) { return "Aggregate";}
				if(type == UtilConstants.PACKAGE_OBJECT_TYPE_CUSTOM_SA) { return "Custom Subject Area";}
			}
			else
			{
				if(type == UtilConstants.PACKAGE_OBJECT_DEPEDENT_TYPE_VARIABLE) { return "Variable";}
				if(type == UtilConstants.PACKAGE_OBJECT_DEPEDENT_TYPE_HIERARCHY) { return "Hierarchy";}
				if(type == UtilConstants.PACKAGE_OBJECT_DEPEDENT_TYPE_CUSTOM_ATTRIBUTE) { return "Custom Attribute";}
				if(type == UtilConstants.PACKAGE_OBJECT_DEPEDENT_TYPE_CUSTOM_MEASURE) { return "Custom Measure";}
				if(type == UtilConstants.PACKAGE_OBJECT_DEPEDENT_TYPE_BUCKETED_MEASURE) { return "Bucket Measure";}
			}
			
			return null;
		}
		
		        /**
         * Like the Flex Alert.show() but uses a styled TitleWindow.
         */
        public static function showBirstAlert(message:String, title:String, icon:Class, caller:UIComponent):void {
            // Pop up Birst-style alert
            var birstAlertDialog:BirstAlertDialog = getBirstAlertDialog(message, title, icon);            
            PopUpManager.addPopUp(birstAlertDialog, caller, true);
            PopUpManager.centerPopUp(birstAlertDialog);
            PopUpManager.bringToFront(birstAlertDialog);
        }
        
        public static function getBirstAlertDialog(message:String, title:String, icon:Class):BirstAlertDialog {
            // Birst-style alert
            var birstAlertDialog:BirstAlertDialog = new BirstAlertDialog();
            birstAlertDialog.title = title;
            birstAlertDialog.alertTitle = title;
            birstAlertDialog.message = message;
            birstAlertDialog.icon = icon;
            return birstAlertDialog;
        }
        
        public static function isFeatureFlagAllowed(featureId:String, defaultValue:Boolean):Boolean{
        	var params:ArrayOfString = navObj.AppConfigParameters;
        	if(params != null && params.length > 0){        		
	        	for each(var param:String in params){
	        		// param of the form featureId=value
	        		if(param.indexOf(featureId) == 0){
	        			var arr:Array = param.split('=');
	        			if(arr.length == 2){
	        				return arr[1] == "true";
	        			} 
	        		}	
	        	}
        	}
        	return defaultValue;
        } 
	}
}