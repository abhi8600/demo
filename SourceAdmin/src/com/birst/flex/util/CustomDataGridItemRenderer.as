package com.birst.flex.util
{
	import generated.webservices.VariableWrapper;
	
	import mx.controls.dataGridClasses.DataGridItemRenderer;

	public class CustomDataGridItemRenderer extends DataGridItemRenderer
	{
		public function CustomDataGridItemRenderer()
		{
			super();
		}
		
		override public function set data(value:Object):void {
			super.data = value;
			
			 if (value != null && value.hasOwnProperty("Imported") && value["Imported"] == true) {
				setStyle("color", 0xAAB3B3);
				setStyle("fontStyle", "italic");
			}
			else {
				setStyle("color", 0x000000);
				setStyle("fontStyle", "normal");
			} 
		}
	}
}