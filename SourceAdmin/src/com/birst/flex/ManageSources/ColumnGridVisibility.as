package com.birst.flex.ManageSources
{
	public class ColumnGridVisibility
	{
		private var FormatsVisible: Boolean = true;
		private var TypeVisible:Boolean = true;
		private var PreventUpdateVisible:Boolean = true;
		private var WidthVisible:Boolean = true;
		private var HierarchyVisible:Boolean = true;
		private var LevelVisible:Boolean = true;
		private var MeasureVisible:Boolean = true;
		private var AnalyzeMeasureVisible:Boolean = true;
		private var AnalyzeByDateVisible:Boolean = true;
		private var UnknownVisible:Boolean = true;
		private var SecurityVisible:Boolean = true;
		
		public function ColumnGridVisibility(formatsVisible:Boolean, typeVisible:Boolean, preventUpdateVisible:Boolean,
			widthVisible:Boolean, hierarchyVisible:Boolean, levelVisible:Boolean, measureVisible:Boolean,
			analyzeMeasureVisible:Boolean, analyzeByDateVisible:Boolean, unknownVisible:Boolean, securityVisible:Boolean){
				FormatsVisible = formatsVisible;
				TypeVisible = typeVisible;
				PreventUpdateVisible = preventUpdateVisible;
				WidthVisible = widthVisible;
				HierarchyVisible = hierarchyVisible;
				LevelVisible = levelVisible;
				MeasureVisible = measureVisible;
				AnalyzeMeasureVisible = analyzeMeasureVisible;
				AnalyzeByDateVisible = analyzeByDateVisible;
				UnknownVisible = unknownVisible;
				SecurityVisible = securityVisible; 
			}
			
		public function get formatsVisible():Boolean {
			return FormatsVisible;
		}
		public function set formatsVisible(b:Boolean):void {
			FormatsVisible = b;
		}
		
		public function get typeVisible():Boolean {
			return TypeVisible;
		}
		public function set typeVisible(b:Boolean):void {
			TypeVisible = b;
		}
		public function get preventUpdateVisible():Boolean {
			return PreventUpdateVisible
		}
		public function set preventUpdateVisible(b:Boolean):void {
			PreventUpdateVisible = b;
		}
		
		public function get widthVisible():Boolean {
			return WidthVisible;
		}
		public function set widthVisible(b:Boolean):void {
			WidthVisible = b;
		}
		public function get hierarchyVisible():Boolean {
			return HierarchyVisible;
		}
		public function set hierarchyVisible(b:Boolean):void {
			HierarchyVisible = b;
		}
		
		public function get levelVisible():Boolean {
			return LevelVisible;
		}
		public function set levelVisible(b:Boolean):void {
			LevelVisible = b;
		}

		public function get measureVisible():Boolean {
			return MeasureVisible;
		}
		public function set measureVisible(b:Boolean):void {
			MeasureVisible = b;
		}
		
		public function get analyzeMeasureVisible():Boolean {
			return AnalyzeMeasureVisible;
		}
		public function set analyzeMeasureVisible(b:Boolean):void {
			AnalyzeMeasureVisible = b;
		}
		public function get analyzeByDateVisible():Boolean {
			return AnalyzeByDateVisible;
		}
		public function set analyzeByDateVisible(b:Boolean):void {
			AnalyzeByDateVisible = b;
		}
		
		public function get unknownVisible():Boolean {
			return UnknownVisible;
		}
		public function set unknownVisible(b:Boolean):void {
			UnknownVisible = b;
		}
		
		public function get securityVisible():Boolean {
			return SecurityVisible;
		}
		public function set securityVisible(b:Boolean):void {
			SecurityVisible = b;
		}
	}
}