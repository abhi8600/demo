package com.birst.flex.ManageSources
{
	import generated.webservices.DataSourceLight;
	
	import mx.controls.treeClasses.TreeItemRenderer;

	public class ManageSourcesItemRenderer extends TreeItemRenderer
	{
		public function ManageSourcesItemRenderer()
		{
			super();
		}
		
		override public function set data(value:Object):void {
			super.data = value;
			
			if (value is LightDataSource) {
				var dsl:LightDataSource = value as LightDataSource;
				if(dsl.Imported){
					setStyle("color", 0xAAB3B3);
					setStyle("fontStyle", "italic");
				}
				else {
					if (dsl.Enabled) {
						setStyle("color", 0x000000);
						setStyle("fontStyle", "normal");
					}
					else {
						setStyle("color", 0xAAB3B3);
						setStyle("fontStyle", "italic");
					}
				}
			}
			else {
				setStyle("color", 0x000000);
				setStyle("fontStyle", "normal");
			}
		}
		
	}
}