package com.birst.flex.ManageSources
{
	import generated.webservices.ArrayOfArrayOfString;
	import generated.webservices.ArrayOfString;
	import generated.webservices.DataSourceLight;
	import generated.webservices.ScriptDefinition;
	
	import mx.core.IUID;

	public class LightDataSource implements IUID
	{
		private var _uid:String;
		private var _dsl:DataSourceLight;
		
		public function LightDataSource(dsl:DataSourceLight) {
			super();
			_dsl = dsl;
			this._uid = dsl.guid;
		}
		
		public function get uid():String
		{
			return _uid;
		}
		
		public function set uid(value:String):void
		{
			_uid = value;
		}
		
		public function get DisplayName():String { return _dsl.DisplayName; }
		public function get Enabled():Boolean { return _dsl.Enabled; }
		public function set Enabled(b:Boolean):void { _dsl.Enabled = b; }
		public function get guid():String { return _dsl.guid; }
		public function get HierarchyName():String { return _dsl.HierarchyName; }
		public function get LastUploadDate():Date { return _dsl.LastUploadDate; }
		public function get Levels():ArrayOfArrayOfString { return _dsl.Levels; }
		public function get LoadGroups():ArrayOfString { return _dsl.LoadGroups; }
		public function get Name():String { return _dsl.Name; }
		public function get Script():ScriptDefinition { return _dsl.Script; }
		public function get Status():Number { return _dsl.Status; }
		public function get StatusMessage():String { return _dsl.StatusMessage; }
		public function get SubGroups(): ArrayOfString { return _dsl.SubGroups; }
		public function get UseBirstLocalForSource():Boolean { return _dsl.UseBirstLocalForSource; }
		public function get	Imported():Boolean { return _dsl.ImportedTable; }
	}
}