package com.birst.flex.ManageSources
{
	import generated.webservices.AdminDataSource;
	import generated.webservices.AdminRepository;
	import generated.webservices.AdminService;
	import generated.webservices.ArrayOfDataSourceLight;
	import generated.webservices.ArrayOfHierarchy;
	import generated.webservices.Hierarchy;
	import generated.webservices.Level;
	import generated.webservices.LevelKey;
	import generated.webservices.StagingColumn;
	
	import mx.collections.ArrayCollection;
	import mx.collections.XMLListCollection;
	import mx.controls.Alert;
	import mx.utils.StringUtil;
	
		
	public class ManageSourceData
	{
		public static var MAX_COLUMN_NAME_WIDTH: int  = 80;
		private static var goodchars: RegExp = new RegExp("[\\w\\.@\\-_\\$#%\\*&:;\\/<>,=+| " + Util.generateRangeForUnicodeCharacters(0x0080, 0xFFCF) + "]+", "g");
		
		private var _sourceData: XML;
		private var _changed:Boolean;
		private var _hierarchiesToSave:ArrayCollection = new ArrayCollection();
		private var _sa:SourceAdmin;
		private var _svc:AdminService;
		private var _dsls:ArrayOfDataSourceLight;
		private var _reloadSources:Boolean;
		
		public function ManageSourceData()
		{
			_sourceData = null;
			_changed = false;
			_reloadSources = false;
		}
		
		public function get sourceData() : XML {
			return _sourceData;
		}
		
		public function set sourceData(sd:XML):void {
			_sourceData = sd;
			_hierarchiesToSave.removeAll();
		}
		
		public function set changed(c:Boolean):void {
			_changed = c;
		}
		
		public function get changed():Boolean {
			return _changed;
		}
		
		public function set reloadSources(c:Boolean):void {
			_reloadSources = c;
		}
		
		public function get reloadSources():Boolean {
			return _reloadSources;
		}
		
		public function get stagingTable():XML {
			return _sourceData ? _sourceData.stagingTable[0] : null;
		}
		
		public function get sourceFile():XML {
			return _sourceData ? _sourceData.sourceFile[0] : null;
		}

		public function addHierarchyToSave(h:Hierarchy):void {
			if (!_hierarchiesToSave.contains(h))
				_hierarchiesToSave.addItem(h);
		}
		
		public function get hierarchiesToSave():ArrayCollection {
			return _hierarchiesToSave;
		}
		
		public function get hierarchies():ArrayOfHierarchy {
			return ar.Hierarchies;
		}
		
		public function get ar():AdminRepository {
			return _sa.ar;
		}
		
		public function set ar(ar:AdminRepository):void {
			_sa.ar = ar;
		}
		
		public function set sa(sa:SourceAdmin):void {
			_sa = sa;
		}
		
		public function get sa():SourceAdmin {
			return _sa;
		}
		
		public function findMatchingSourceColumn(stagingColumn:XML):XML {
			if (sourceFile == null)
				return null;
			var scName:String = stagingColumn.Name;
			if (String(stagingColumn.SourceFileColumn) != "") {
				scName = stagingColumn.SourceFileColumn;
			}
			var sf:XML = null;
			for each (var x:XML in sourceFile.Columns.SourceColumn) {
				if (String(x.Name) == scName) {
					return x;
				}
			}
			return null;
		}
		
		public function renameStagingColumn(source: String, hierarchy: String, level: String, oldName: String, newName: String): String
		{
			var sc:XML;
			var scName:String;
			for each (sc in stagingTable.Columns.StagingColumn) {
				scName = sc.Name;
				if (scName == newName) {
					Alert.show("There is already a column named " + newName + " in this source.  Please choose another name.", "Warning", Alert.OK);
					return oldName;
				}
			}
			changed = true;
			var i:uint;
			for each (sc in stagingTable.Columns.StagingColumn) {
				scName = sc.Name;
				if (scName == oldName) {
					sc.Name = newName;
					break;
				}
			}
			
			var liveAccess:String = stagingTable.LiveAccess;
			if (!liveAccess || liveAccess == "false")
			{
				/*
				* In the case of live access the source file name is the physical name, the
				* sc name is the logical name.
				*/
				for each (var sf:XML in sourceFile.Columns.SourceColumn) {
					var sfName:String = sf.Name;
					if (sfName == oldName) {
						sf.Name = newName;
						break;
					}
				}
				for each (sc in stagingTable.Columns.StagingColumn) {
					var scColName:String = sc.SourceFileColumn;
					if (scColName == oldName) {
						sc.SourceFileColumn = newName;					
					}
				}
			}
//			svc.renameStagingColumn(source, oldName, newName);
			// Update any level keys or levels
			var lname: String = level;
			var h: Hierarchy = GrainHierarchies.getHierarchy(ar, hierarchy);
			// Update
			if (h != null)
			{
				var levelList: Array = new Array();
				for each(var cl: Level in h.Children)
					getChildLevels(cl, levelList);
				for each(var l: Level in levelList)
				{
					var lindex: int = l.ColumnNames.getItemIndex(oldName);
					var saveLevel: Boolean = false;
					if (lindex >= 0)
					{
						l.ColumnNames[lindex] = newName;
						saveLevel = true;
					}
					for each(var lk: LevelKey in l.Keys)
					{
						if (lk.SurrogateKey)
							continue;
						lindex = lk.ColumnNames.getItemIndex(oldName);
						if (lindex >= 0)
						{
							lk.ColumnNames[lindex] = newName;
							saveLevel = true;
						}
					}
					if (saveLevel) {
						addHierarchyToSave(h);
					}
				}
			}
			return newName;
		}
		
		private function getChildLevels(l: Level, list: Array): void
		{
			list.push(l);
			for each(var cl: Level in l.Children)
			{
				getChildLevels(cl, list);	
			}
		}
		
		public function renameSourceColumn(newName:String, curobj: Object):String {
			// Remove bad characters
			var s:String = Util.replaceUnSupportedChars(newName, ar.queryLanguageVersion);
			if (s.length > 0)
			{
				// Remove multiple spaces 
				s = Util.removeMulSpaces(s);
				if (s.length > MAX_COLUMN_NAME_WIDTH)
                    s = s.substring(0, MAX_COLUMN_NAME_WIDTH);
			}
			if (s.length > 0 && curobj.Name != s)
			{
				s = renameStagingColumn(stagingTable.Name[0], curobj.Hierarchy, curobj.Level, curobj.Name, s);
			}
			return s;
		}
		
		public function changeDataType(curobj:Object, dataType:String, curSourceColumn:XML, curStagingColumn:XML):Boolean {
			// Can't change the datatype of an Analyze By Date column
			var curDataType:String = null;
			if (curSourceColumn != null)
				curDataType = curSourceColumn.DataType;
			if ((curobj.AnalyzeByDate == false || supportsAnalyzeByDate(dataType))
				&& (curDataType == null || curDataType != dataType))
			{
				if (curSourceColumn != null)
					curSourceColumn.DataType = dataType;
				curStagingColumn.DataType = dataType;
				changed = true;
			}
			else if (curobj.AnalyzeByDate == true && !supportsAnalyzeByDate(dataType) && (curDataType == null || curDataType != dataType)) {
				return false;
			}
			return true;
		}
		
		public function changeFormula(value: String, curStagingColumn:XML):void 
		{
			var cursc:String = curStagingColumn.SourceFileColumn; 
			if (cursc != value) 
			{
				curStagingColumn.SourceFileColumn = value;
				changed = true;
			}
		}

		private function supportsAnalyzeByDate(dataType:String):Boolean {
			return dataType == "Date" || dataType == "DateTime";
		}
		
		public function changeWidth(curSourceColumn:XML, curStagingColumn:XML, w:int):void {
			var edit: Boolean = true;
			if (curSourceColumn != null)
				edit = edit && (w != int(String(curSourceColumn.Width))); 
			if (edit) {
				if (w >= 0) {
					if (curSourceColumn != null)
						curSourceColumn.Width = w;
					curStagingColumn.Width = w;
					changed = true;
				}
			}
		}
		
		public function changeHierarchyAndLevel(curStagingColumn:XML, curobj:Object, hname: String, lname:String ):void {
			var curscName:String = curStagingColumn.Name;
			curStagingColumn.TargetTypes = <TargetTypes/>;
			var ttList:XMLListCollection = new XMLListCollection(curStagingColumn.TargetTypes.string);
			if (curobj.Measure)
			{
				ttList.addItem(<string>Measure</string>);
			}
			if (hname != null && hname.length > 0)
			{
				var temp:String = "<string>"+hname+"</string>";
				ttList.addItem(XML(temp));
				var h: Hierarchy = GrainHierarchies.getHierarchy(ar, hname);
			}
			var oldh: Hierarchy = curobj.Hierarchy == null ? null : GrainHierarchies.getHierarchy(ar, curobj.Hierarchy);
			
			// Add new membership
			if (h != null)
			{
				if (lname != null && lname.length > 0)
				{
					var l: Level = GrainHierarchies.getHierarchyLevel(lname, h);
					if (l != null)
					{
						if (!l.ColumnNames.contains(curscName))
						{
							l.ColumnNames.addItem(curscName);
							l.HiddenColumns.addItem(false);
							addHierarchyToSave(h);
						}								
					} 
				}
			}	
			changed = true;
			var index:uint;
			
			// Clear level membership
			if (oldh != null)
			{
				var levelList: Array = new Array();
					for each(var cl: Level in oldh.Children)
						getChildLevels(cl, levelList);
				for each(var level: Level in levelList)
				{
					for(index = 0;index < level.ColumnNames.length; index++)
					{
						if (level.ColumnNames[index] == curscName)
							break;
					}
					if (index < level.ColumnNames.length && (oldh != h || lname == null || lname.length == 0 || level.Name != lname))
					{
						level.ColumnNames.removeItemAt(index);
						level.HiddenColumns.removeItemAt(index);
						addHierarchyToSave(oldh);
					}
				}
				if (oldh.SourceGroups != null && oldh.SourceGroups.length > 0)
				{
					// Remove from source groups if necessary from hierarchy
					var oldGroups: Array = oldh.SourceGroups.split(",");
					var newsgstring: String = "";
					for each(var sgs: String in oldGroups)
					{
						var found:Boolean = false;
						for each(var sgst: AdminDataSource in ar.Sources)
						{
							if (sgst.SourceGroups == null)
								continue;
							for each(var sgsc: StagingColumn in sgst.StagingColumns)
							{
								if (sgsc.TargetTypes != null && sgsc.TargetTypes.contains(oldh.DimensionName))
								{											
									var stgroups: Array = sgst.SourceGroups.split(",");
									if (stgroups.indexOf(sgs)>=0)
									{
										found = true;
										break;
									}
								}
							}
							if (found)
								break;
						}
						if (found)
							newsgstring += (newsgstring.length>0 ? "," : "") + sgs;
					}
					if (oldh.SourceGroups != newsgstring)
					{
						oldh.SourceGroups = newsgstring;
						addHierarchyToSave(oldh);
					}
				}
			}
		}
		
		public function changeAttributeField(isSelected:Boolean, curStagingColumn:XML, isDiscoverySource:Boolean, discoverySourceDimName:String ):void {
			if(!isDiscoverySource || discoverySourceDimName == null)
			{
				return;
			}
			
			var sfColumn:String = curStagingColumn.SourceFileColumn;
			if( sfColumn == null || sfColumn.length == 0)
			{
				return;
			}
			
			var targetTypes:XMLListCollection;
			changed = true;
			if(isSelected)
			{
				if (curStagingColumn.TargetTypes == null || curStagingColumn.TargetTypes.length() == 0)
					curStagingColumn.TargetTypes = XML(<TargetTypes/>);
				targetTypes = new XMLListCollection(curStagingColumn.TargetTypes.string);
				var containsAttribute:Boolean = false;
				for each (var item:XML in targetTypes) {
					if (item.toString() == discoverySourceDimName )
						containsAttribute = true;
				}
				if (! containsAttribute)
				{
					targetTypes.addItemAt(XML("<string>" + discoverySourceDimName + "</string>" ), 0);
				}
				
			} else
			{
				if (curStagingColumn.TargetTypes != null || curStagingColumn.TargetTypes.length() > 0)
				{
					targetTypes = new XMLListCollection(curStagingColumn.TargetTypes.string);
					for (var index:uint = 0; index < targetTypes.length; index++) {
						if (targetTypes[index].toString() == discoverySourceDimName) {
							targetTypes.removeItemAt(index);
							break;
						}						
					} 
				}
			}
		}
		
		public function changeMeasureField(isSelected:Boolean, curStagingColumn:XML):void {
			var targetTypes:XMLListCollection;
			changed = true;
			if(isSelected)
			{
				if (curStagingColumn.TargetTypes == null || curStagingColumn.TargetTypes.length() == 0)
					curStagingColumn.TargetTypes = XML(<TargetTypes/>);
				targetTypes = new XMLListCollection(curStagingColumn.TargetTypes.string);
				var containsMeasure:Boolean = false;
				for each (var item:XML in targetTypes) {
					if (item.toString() == "Measure")
						containsMeasure = true;
				}
				if (! containsMeasure)
				{
					targetTypes.addItemAt(XML(<string>Measure</string>), 0);
				}
				var dataType:String = curStagingColumn.DataType;
				curStagingColumn.TargetAggregations = XML(<TargetAggregations/>);
				var targetAggregations:XMLListCollection = new XMLListCollection(curStagingColumn.TargetAggregations.string);
				if (dataType == "Integer" || dataType == "Number" || dataType == "Float")
				{
					targetAggregations.addItem(<string>SUM</string>);
					targetAggregations.addItem(<string>AVG</string>);
					targetAggregations.addItem(<string>COUNT DISTINCT</string>);
					targetAggregations.addItem(<string>MIN</string>);
					targetAggregations.addItem(<string>MAX</string>);
				} else
				{
					targetAggregations.addItem(<string>COUNT</string>);
					targetAggregations.addItem(<string>COUNT DISTINCT</string>);
					targetAggregations.addItem(<string>MIN</string>);
					targetAggregations.addItem(<string>MAX</string>);
				}
			} else
			{
				if (curStagingColumn.TargetTypes != null || curStagingColumn.TargetTypes.length() > 0)
				{
					targetTypes = new XMLListCollection(curStagingColumn.TargetTypes.string);
					for (var index:uint = 0; index < targetTypes.length; index++) {
						if (targetTypes[index].toString() == "Measure") {
							targetTypes.removeItemAt(index);
							break;
						}
						
					} 
				}
				curStagingColumn.AnalyzeMeasure = false;
			}
		}
		
		public function changeAnalyzeByDate(isSet:Boolean, curStagingColumn:XML):void {
			var isGenerateTimeDimension:Boolean = false;
			if (curStagingColumn.GenerateTimeDimension.length() > 0) {
				if (curStagingColumn.GenerateTimeDimension[0].toString() == 'true')
					isGenerateTimeDimension = true;
			}
			if (isGenerateTimeDimension != isSet) { 
				curStagingColumn.GenerateTimeDimension = isSet;
				changed = true;
			}
		}
		
		public function changeAnalyzeByMeasure(isSet:Boolean, curStagingColumn:XML, curobj:Object):void {
			var isAnalyzeMeasure:Boolean = false;
			if (curStagingColumn.AnalyzeMeasure.length() > 0) {
				if (curStagingColumn.AnalyzeMeasure[0].toString() == 'true')
					isAnalyzeMeasure = true;
			}
			if (isAnalyzeMeasure != isSet) {
				curStagingColumn.AnalyzeMeasure = curobj.Measure && isSet;
				changed = true;
			}
		}
		
		public function changeUnknown(s:String, curStagingColumn:XML):String {
			s = StringUtil.trim(s);				
			
			// Remove bad characters
			var arr:Array = s.match(goodchars);
			s = arr.length > 0 ? arr[0] : "";
			s = s.length == 0 ? null : s;
			if ((s == null && curStagingColumn.UnknownValue.length() == 0) || curStagingColumn.UnknownValue != s) {
				if (s == null)
					curStagingColumn.UnknownValue = <UnknownValue/>;
				else
					curStagingColumn.UnknownValue = s;
				changed = true; 				
			}
			return s;
		}
		
		public function changePreventUpdate(isSet:Boolean, curSourceColumn:XML):void {
			if (curSourceColumn.PreventUpdate != isSet) {
				curSourceColumn.PreventUpdate = isSet;
				changed = true;
			}
		}
		
		public function changeFormat(format:String, curSourceColumn:XML):void {
			if (curSourceColumn != null && curSourceColumn.Format != format) {
				curSourceColumn.Format = format;
				changed = true;
			}
		}
		
		public function get adminService():AdminService {
			return _svc;
		}
		
		public function set adminService(service:AdminService):void {
			_svc = service;
		}
		
		public function get dsls():ArrayOfDataSourceLight {
			return _dsls;
		}
		
		public function set dsls(dsls:ArrayOfDataSourceLight):void {
			_dsls = dsls;
		}
		
		public function getSourceColumn(name:String):XML {
			for each (var x:XML in this.sourceFile.Columns.SourceColumn) {
				var scName:String = x.Name;
				if (scName == name)
					return x;
			}
			return null;
		}
		
		public function getStagingColumn(name:String):XML {
			for each (var x:XML in this.stagingTable.Columns.StagingColumn) {
				var scName:String = x.Name;
				if (scName == name)
					return x;
			}
			return null;
		}
	}
	
}