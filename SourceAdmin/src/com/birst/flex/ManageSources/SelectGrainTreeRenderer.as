package com.birst.flex.ManageSources
{
	import flash.events.MouseEvent;
	
	import generated.webservices.ArrayOfArrayOfString;
	
	import mx.collections.XMLListCollection;
	import mx.controls.CheckBox;
	import mx.controls.Tree;
	import mx.controls.treeClasses.TreeItemRenderer;
	import mx.controls.treeClasses.TreeListData;

	public class SelectGrainTreeRenderer extends TreeItemRenderer
	{
		private var cb: CheckBox;
		
		public function SelectGrainTreeRenderer(): void
		{
			super();
		}
		
		override protected function createChildren():void
		{
   			super.createChildren();
			cb = new CheckBox();
			cb.setStyle( "verticalAlign", "middle" );
			cb.addEventListener(MouseEvent.CLICK, checkboxClick);
			addChild(cb);
		}
		
		private function getCurrentLevel(): Array
		{
       		var tld: TreeListData = TreeListData(super.listData);
       		var hx: XML;
       		while (hx == null || hx.parent() != null)
       		{
       			if (hx == null)
       				hx = XML(tld.item).parent();
       			else
       				hx = hx.parent();	
       		}
       		var hname: String = hx.@Name;
       		var lname: String = XML(tld.item).@Name;
       		return [hname, lname];
		}
   
   		private function checkboxClick(event: MouseEvent): void
   		{
       		var sg: SelectGrain = SelectGrain(this.document);
       		var curlevel: Array = getCurrentLevel();
       		var newGrain: ArrayOfArrayOfString = new ArrayOfArrayOfString();
       		var updated: Boolean = false;
       		var toRemove: int = -1;
       		var count: int = 0;
    		for each(var level: XML in sg.msData.stagingTable.Levels.ArrayOfString)
    		{
    			var lName:String = level.string[0];
    			var lName2:String = level.string[1];
       			if (lName == curlevel[0])
       			{
       				if (cb.selected)
       				{
       					level.string[1] = curlevel[1];
       				} else
       				{
       					toRemove = count;
       				}
       				updated = true;
       			}
       			count++;
       		}
       		if (toRemove >= 0) {
       			var ls:XMLListCollection = new XMLListCollection(sg.msData.stagingTable.Levels.ArrayOfString);
       			ls.removeItemAt(toRemove);
//       			sg.msData.stagingTable.Levels = ls.source;
       		}
       		if (!updated && cb.selected)
       		{
       			var ls1:XMLListCollection = new XMLListCollection(sg.msData.stagingTable.Levels.ArrayOfString);
       			var newLevel: XML = <ArrayOfString/>;
       			var childText:String = "<string>" + curlevel[0] + "</string>";
       			var child:XML = new XML(childText);
       			newLevel.appendChild(child);
       			childText = "<string>" + curlevel[1] + "</string>";
       			child = new XML(childText);
       			newLevel.appendChild(child);
       			ls1.addItem(newLevel);
//       			sg.msData.stagingTable.Levels = ls1.source;
       		}
       		sg.msData.changed = true;
       		Tree(this.owner).invalidateList();
   		}
   
		override protected function updateDisplayList(unscaledWidth:Number, 
            unscaledHeight:Number):void 
		{
            super.updateDisplayList(unscaledWidth, unscaledHeight);
            if(super.data && super.icon && cb)
            {
            	if (TreeListData(super.listData).depth == 1)
            	{
            		cb.visible = false;
            	} else
            	{
            		var l:XML = XML(super.data);
            		var key:String = l.@Key;
            		icon.visible = false;
            		cb.selected = false;
            		cb.visible = true;
		       		var sg: SelectGrain = SelectGrain(this.document);
            		var curlevel: Array = getCurrentLevel();
            		if (sg.msData.stagingTable.Levels && sg.msData.stagingTable.Levels.ArrayOfString) {
	            		for each(var level: XML in sg.msData.stagingTable.Levels.ArrayOfString)
	            		{
	            			var lName:String = level.string[0];
	            			var lName2:String = level.string[1];
	            			if (lName == curlevel[0] && lName2 == curlevel[1])
	            				cb.selected = true;
	            		}
            		}
            		cb.x = super.icon.x;
            		cb.y = 8;
            		super.icon.x = cb.x + cb.width+17;            	
            		super.label.x = super.icon.x;// + super.icon.width + 3;            		
            		if (key == null || key.length == 0) {
            			cb.enabled = false;
            			super.label.textColor = 0xEEEEEE;
            			this.toolTip = "This level does not have a key.  Please set the level key.";
            		}
            		else {
            			cb.enabled = true;
            			super.label.textColor = 0;
            			this.toolTip = "";
            		}
            	}
            }
        }
	}
}