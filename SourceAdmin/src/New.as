package
{
	/*
	* Need to properly initialize classes or they won't serialize (Numbers in particular become NaN
	* and won't serialize)
	*/
	import generated.webservices.ArrayOfBoolean;
	import generated.webservices.ArrayOfLevelKey;
	import generated.webservices.ArrayOfString;
	import generated.webservices.ArrayOfLevel;
	import generated.webservices.ArrayOfTableDefinition;
	import generated.webservices.Level;
	import generated.webservices.LevelKey;
	import generated.webservices.TableSource;

	public class New
	{
		public function New(): void
		{
		}
		
		public static function NewLevelKey(): LevelKey
		{
			var lk: LevelKey = new LevelKey();
			lk.ColumnNames = new ArrayOfString();
			return lk;
		}
		
		public static function NewLevel(): Level
		{
			var l: Level = new Level();
			l.Cardinality = 1;
			l.SCDType = 1;
			l.Children = new ArrayOfLevel();
			l.SharedChildren = new ArrayOfString();
			l.ColumnNames = new ArrayOfString();
			l.HiddenColumns = new ArrayOfBoolean();
			l.Keys = new ArrayOfLevelKey();
			return l;
		}
		
		public static function NewTableSource(): TableSource
		{
			var ts: TableSource = new TableSource();
			ts.Tables = new ArrayOfTableDefinition();
			return ts;
		}
	}
}