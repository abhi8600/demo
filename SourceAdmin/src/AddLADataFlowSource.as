package
{
	import generated.webservices.AdminDataSource;
	import generated.webservices.ArrayOfArrayOfString;
	import generated.webservices.ArrayOfStagingColumn;
	import generated.webservices.ArrayOfString;
	import generated.webservices.ArrayOfTableDefinition;
	import generated.webservices.GetTableSchemaResultEvent;
	import generated.webservices.StagingColumn;
	import generated.webservices.TableDefinition;
	import generated.webservices.TableSource;
	
	import mx.collections.ArrayCollection;

	public class AddLADataFlowSource
	{
		private var event: GetTableSchemaResultEvent;
		private var df: DataFlow;
		
		public function AddLADataFlowSource(df: DataFlow, event: GetTableSchemaResultEvent)
		{
			this.df = df;
			this.event = event;
		}
		
		public static function getNewLADataSource(sourceName: String, schema: String): AdminDataSource
		{
			var ads: AdminDataSource = new AdminDataSource();
			ads.Name = sourceName;
			ads.DisplayName = sourceName;
			ads.StagingColumns = new ArrayOfStagingColumn();
			// for visualization space default it to mark it enabled
			ads.Enabled = true;
			ads.Separator = '|';
			ads.Encoding = 'UTF-8';
			ads.DiscoveryTable = true;
			ads.LiveAccess = true;
			ads.Levels = new ArrayOfArrayOfString();
			ads.TableSource = new TableSource();
			ads.TableSource.Tables = new ArrayOfTableDefinition();
			ads.TableSource.Schema = schema;
			return ads;
		}

		public function process(): void
		{
			var sources: ArrayCollection = new ArrayCollection();
			for each(var r: Object in event.result)
			{
				var row: generated.webservices.ArrayOfAnyType = generated.webservices.ArrayOfAnyType(r);
				var dbase: String = String(row.getItemAt(0));
				var schema: String = String(row.getItemAt(1));
				var tname: String = String(row.getItemAt(2));
				var cname: String = String(row.getItemAt(3));
				var dtype: String = String(row.getItemAt(5));
				var width: int = int(row.getItemAt(6));
				var ads: AdminDataSource = null;
				var sourceName: String = tname;
				if (df.importTablesType == 1 || df.importTablesType == 2)
				{
					sourceName = df.importTableName;
				}
				for each(var sads: AdminDataSource in sources)
				{
					if (sads.Name == sourceName)
					{
						ads = sads;
						break;	
					}
				}
				if (ads == null)
				{
					ads = getNewLADataSource(sourceName, schema);
					sources.addItem(ads);
				}
				var found:Boolean = false;
				for each(var ssc: StagingColumn in ads.StagingColumns)
				{
					if (ssc.Name == cname)
					{
						found = true;
						break;
					}
				}
				if (!found)
				{
					var sc: StagingColumn = new StagingColumn();
					sc.Name = cname;
					sc.DataType = RealtimeAdmin.getType(dtype);
					sc.Width = width;
					sc.TargetTypes = new ArrayOfString();
					if (sc.DataType == "Integer" || sc.DataType == "Number" || sc.DataType == "Float")
						sc.TargetTypes.addItem("Measure");
					else
						sc.TargetTypes.addItem(sourceName);
					sc.SourceFileColumn = tname+"."+Util.getQuotedName(df.importConnectionType, cname);
					ads.StagingColumns.addItem(sc);
					ads.TableSource.Schema = schema;
					ads.TableSource.Connection = df.importConnectionName;
					found = false;
					for each(var td: TableDefinition in ads.TableSource.Tables)
					{
						if (td.PhysicalName == tname)
						{
							found = true;
							break;								
						}
					}
					if (!found)
					{
						var newtd: TableDefinition = new TableDefinition();
						newtd.PhysicalName = tname;
						ads.TableSource.Tables.addItem(newtd);
					}
				}
			}
			for each(var savesource: AdminDataSource in sources)
			{
				df.nav.SA.ar.Sources.addItem(savesource);
				SourceAdmin.saveDataSourceAndColumns(df.nav.SA.context, savesource, savesource.StagingColumns);
			}
		}
	}
}