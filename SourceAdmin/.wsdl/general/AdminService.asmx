<?xml version="1.0" encoding="UTF-8"?><wsdl:definitions xmlns:wsdl="http://schemas.xmlsoap.org/wsdl/" xmlns:http="http://schemas.xmlsoap.org/wsdl/http/" xmlns:mime="http://schemas.xmlsoap.org/wsdl/mime/" xmlns:s="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/wsdl/soap/" xmlns:soap12="http://schemas.xmlsoap.org/wsdl/soap12/" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/" xmlns:tm="http://microsoft.com/wsdl/mime/textMatching/" xmlns:tns="http://www.birst.com/" targetNamespace="http://www.birst.com/">
  <wsdl:types>
    <s:schema elementFormDefault="qualified" targetNamespace="http://www.birst.com/">
      <s:element name="GetSources">
        <s:complexType/>
      </s:element>
      <s:element name="GetSourcesResponse">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="GetSourcesResult" type="tns:ArrayOfString"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:complexType name="ArrayOfString">
        <s:sequence>
          <s:element maxOccurs="unbounded" minOccurs="0" name="string" nillable="true" type="s:string"/>
        </s:sequence>
      </s:complexType>
    </s:schema>
  </wsdl:types>
  <wsdl:message name="GetSourcesSoapIn">
    <wsdl:part element="tns:GetSources" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="GetSourcesSoapOut">
    <wsdl:part element="tns:GetSourcesResponse" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:portType name="AdminServiceSoap">
    <wsdl:operation name="GetSources">
      <wsdl:input message="tns:GetSourcesSoapIn">
    </wsdl:input>
      <wsdl:output message="tns:GetSourcesSoapOut">
    </wsdl:output>
    </wsdl:operation>
  </wsdl:portType>
  <wsdl:binding name="AdminServiceSoap12" type="tns:AdminServiceSoap">
    <soap12:binding transport="http://schemas.xmlsoap.org/soap/http"/>
    <wsdl:operation name="GetSources">
      <soap12:operation soapAction="http://www.birst.com/GetSources" style="document"/>
      <wsdl:input>
        <soap12:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
  </wsdl:binding>
  <wsdl:binding name="AdminServiceSoap" type="tns:AdminServiceSoap">
    <soap:binding transport="http://schemas.xmlsoap.org/soap/http"/>
    <wsdl:operation name="GetSources">
      <soap:operation soapAction="http://www.birst.com/GetSources" style="document"/>
      <wsdl:input>
        <soap:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
  </wsdl:binding>
  <wsdl:service name="AdminService">
    <wsdl:port binding="tns:AdminServiceSoap12" name="AdminServiceSoap12">
      <soap12:address location="http://localhost:57747/AdminService.asmx"/>
    </wsdl:port>
    <wsdl:port binding="tns:AdminServiceSoap" name="AdminServiceSoap">
      <soap:address location="http://localhost:57747/AdminService.asmx"/>
    </wsdl:port>
  </wsdl:service>
</wsdl:definitions>