<?xml version="1.0" encoding="UTF-8"?><wsdl:definitions xmlns:wsdl="http://schemas.xmlsoap.org/wsdl/" xmlns:http="http://schemas.xmlsoap.org/wsdl/http/" xmlns:mime="http://schemas.xmlsoap.org/wsdl/mime/" xmlns:s="http://www.w3.org/2001/XMLSchema" xmlns:s1="http://trustedservice.WebServices.successmetricsinc.com/xsd" xmlns:soap="http://schemas.xmlsoap.org/wsdl/soap/" xmlns:soap12="http://schemas.xmlsoap.org/wsdl/soap12/" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/" xmlns:tm="http://microsoft.com/wsdl/mime/textMatching/" xmlns:tns="http://www.birst.com/" targetNamespace="http://www.birst.com/">
  <wsdl:types>
    <s:schema elementFormDefault="qualified" targetNamespace="http://www.birst.com/">
      <s:import namespace="http://trustedservice.WebServices.successmetricsinc.com/xsd"/>
      <s:element name="getCreateUserAllowedDomains">
        <s:complexType/>
      </s:element>
      <s:element name="getCreateUserAllowedDomainsResponse">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="getCreateUserAllowedDomainsResult" type="tns:GenericResponse"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:complexType name="GenericResponse">
        <s:sequence>
          <s:element maxOccurs="1" minOccurs="0" name="other" type="tns:ArrayOfString"/>
          <s:element maxOccurs="1" minOccurs="0" name="Error" type="tns:ErrorOutput"/>
        </s:sequence>
      </s:complexType>
      <s:complexType name="ArrayOfString">
        <s:sequence>
          <s:element maxOccurs="unbounded" minOccurs="0" name="string" nillable="true" type="s:string"/>
        </s:sequence>
      </s:complexType>
      <s:complexType name="ErrorOutput">
        <s:sequence>
          <s:element maxOccurs="1" minOccurs="1" name="ErrorType" type="s:int"/>
          <s:element maxOccurs="1" minOccurs="0" name="ErrorMessageSource" type="s:string"/>
          <s:element maxOccurs="1" minOccurs="0" name="ErrorMessage" type="s:string"/>
        </s:sequence>
      </s:complexType>
      <s:element name="createAccount">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="accountName" type="s:string"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="createAccountResponse">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="createAccountResult" type="tns:GenericResponse"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="getLoggedInUserDetails">
        <s:complexType/>
      </s:element>
      <s:element name="getLoggedInUserDetailsResponse">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="getLoggedInUserDetailsResult" type="tns:UsersDetails"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:complexType name="UsersDetails">
        <s:complexContent mixed="false">
          <s:extension base="tns:GenericResponse">
            <s:sequence>
              <s:element maxOccurs="1" minOccurs="0" name="Users" type="tns:ArrayOfUserSummary"/>
            </s:sequence>
          </s:extension>
        </s:complexContent>
      </s:complexType>
      <s:complexType name="ArrayOfUserSummary">
        <s:sequence>
          <s:element maxOccurs="unbounded" minOccurs="0" name="UserSummary" nillable="true" type="tns:UserSummary"/>
        </s:sequence>
      </s:complexType>
      <s:complexType name="UserSummary">
        <s:sequence>
          <s:element maxOccurs="1" minOccurs="0" name="UserID" type="s:string"/>
          <s:element maxOccurs="1" minOccurs="0" name="Username" type="s:string"/>
          <s:element maxOccurs="1" minOccurs="0" name="Email" type="s:string"/>
          <s:element maxOccurs="1" minOccurs="0" name="FirstName" type="s:string"/>
          <s:element maxOccurs="1" minOccurs="0" name="LastName" type="s:string"/>
          <s:element maxOccurs="1" minOccurs="1" name="Enabled" type="s:boolean"/>
          <s:element maxOccurs="1" minOccurs="1" name="Locked" type="s:boolean"/>
          <s:element maxOccurs="1" minOccurs="1" name="AccountAdmin" type="s:boolean"/>
          <s:element maxOccurs="1" minOccurs="0" name="LastLoginDate" type="s:string"/>
          <s:element maxOccurs="1" minOccurs="1" name="OperationFlag" type="s:boolean"/>
        </s:sequence>
      </s:complexType>
      <s:element name="publish">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="loadDate" type="s:string"/>
            <s:element maxOccurs="1" minOccurs="0" name="loadGroup" type="s:string"/>
            <s:element maxOccurs="1" minOccurs="0" name="subgroups" type="tns:ArrayOfString"/>
            <s:element maxOccurs="1" minOccurs="1" name="independentMode" type="s:boolean"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="publishResponse">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="publishResult" type="tns:GenericResponse"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="ConfigureDashboards">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="loadDate" type="s:string"/>
            <s:element maxOccurs="1" minOccurs="0" name="loadGroup" type="s:string"/>
            <s:element maxOccurs="1" minOccurs="0" name="subGroups" type="tns:ArrayOfString"/>
            <s:element maxOccurs="1" minOccurs="1" name="independentMode" type="s:boolean"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="ConfigureDashboardsResponse">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="ConfigureDashboardsResult" type="tns:DashboardConfiguration"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:complexType name="DashboardConfiguration">
        <s:sequence>
          <s:element maxOccurs="1" minOccurs="0" name="DashboardOptions" type="tns:ArrayOfDashboardOption"/>
          <s:element maxOccurs="1" minOccurs="0" name="ErrorOutput" type="tns:ErrorOutput"/>
        </s:sequence>
      </s:complexType>
      <s:complexType name="ArrayOfDashboardOption">
        <s:sequence>
          <s:element maxOccurs="unbounded" minOccurs="0" name="DashboardOption" nillable="true" type="tns:DashboardOption"/>
        </s:sequence>
      </s:complexType>
      <s:complexType name="DashboardOption">
        <s:sequence>
          <s:element maxOccurs="1" minOccurs="0" name="Name" type="s:string"/>
          <s:element maxOccurs="1" minOccurs="1" name="SumVisible" type="s:boolean"/>
          <s:element maxOccurs="1" minOccurs="1" name="SumChecked" type="s:boolean"/>
          <s:element maxOccurs="1" minOccurs="1" name="AvgVisible" type="s:boolean"/>
          <s:element maxOccurs="1" minOccurs="1" name="AvgChecked" type="s:boolean"/>
          <s:element maxOccurs="1" minOccurs="1" name="CountVisible" type="s:boolean"/>
          <s:element maxOccurs="1" minOccurs="1" name="CountChecked" type="s:boolean"/>
        </s:sequence>
      </s:complexType>
      <s:element name="PublishWithDashboards">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="measureOptions" type="tns:ArrayOfString"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="PublishWithDashboardsResponse">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="PublishWithDashboardsResult" type="tns:GenericResponse"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetPublishHistory">
        <s:complexType/>
      </s:element>
      <s:element name="GetPublishHistoryResponse">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="GetPublishHistoryResult" type="tns:PublishHistoryResult"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:complexType name="PublishHistoryResult">
        <s:sequence>
          <s:element maxOccurs="1" minOccurs="0" name="PublishHistoryOutput" type="tns:ArrayOfPublishHistory"/>
          <s:element maxOccurs="1" minOccurs="0" name="Error" type="tns:ErrorOutput"/>
        </s:sequence>
      </s:complexType>
      <s:complexType name="ArrayOfPublishHistory">
        <s:sequence>
          <s:element maxOccurs="unbounded" minOccurs="0" name="PublishHistory" nillable="true" type="tns:PublishHistory"/>
        </s:sequence>
      </s:complexType>
      <s:complexType name="PublishHistory">
        <s:sequence>
          <s:element maxOccurs="1" minOccurs="0" name="DateTime" type="s:string"/>
          <s:element maxOccurs="1" minOccurs="0" name="LoadNumber" type="s:string"/>
          <s:element maxOccurs="1" minOccurs="0" name="Step" type="s:string"/>
          <s:element maxOccurs="1" minOccurs="0" name="Substep" type="s:string"/>
          <s:element maxOccurs="1" minOccurs="0" name="ResultCode" type="s:string"/>
          <s:element maxOccurs="1" minOccurs="0" name="Result" type="s:string"/>
          <s:element maxOccurs="1" minOccurs="0" name="PublishLoadDate" type="s:string"/>
          <s:element maxOccurs="1" minOccurs="0" name="LoadGroup" type="s:string"/>
          <s:element maxOccurs="1" minOccurs="0" name="PublishStartTime" type="s:string"/>
          <s:element maxOccurs="1" minOccurs="0" name="PublishEndTime" type="s:string"/>
        </s:sequence>
      </s:complexType>
      <s:element name="CanDeleteLast">
        <s:complexType/>
      </s:element>
      <s:element name="CanDeleteLastResponse">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="CanDeleteLastResult" type="tns:DeleteDataOutput"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:complexType name="DeleteDataOutput">
        <s:sequence>
          <s:element maxOccurs="1" minOccurs="1" name="deleteLast" type="s:boolean"/>
          <s:element maxOccurs="1" minOccurs="1" name="spaceInUse" type="s:boolean"/>
          <s:element maxOccurs="1" minOccurs="1" name="isDeleted" type="s:boolean"/>
          <s:element maxOccurs="1" minOccurs="0" name="errorOutput" type="tns:ErrorOutput"/>
        </s:sequence>
      </s:complexType>
      <s:element name="DeleteLast">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="1" name="restore" type="s:boolean"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="DeleteLastResponse">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="DeleteLastResult" type="tns:DeleteDataOutput"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="DeleteAll">
        <s:complexType/>
      </s:element>
      <s:element name="DeleteAllResponse">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="DeleteAllResult" type="tns:DeleteDataOutput"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="DeleteSpace">
        <s:complexType/>
      </s:element>
      <s:element name="DeleteSpaceResponse">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="DeleteSpaceResult" type="tns:DeleteDataOutput"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="DeleteOverview">
        <s:complexType/>
      </s:element>
      <s:element name="DeleteOverviewResponse">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="DeleteOverviewResult" type="tns:DeleteDataOutput"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetTemplateCategories">
        <s:complexType/>
      </s:element>
      <s:element name="GetTemplateCategoriesResponse">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="GetTemplateCategoriesResult" type="tns:TemplateCategories"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:complexType name="TemplateCategories">
        <s:sequence>
          <s:element maxOccurs="1" minOccurs="0" name="Categories" type="tns:ArrayOfTemplateCategory"/>
          <s:element maxOccurs="1" minOccurs="0" name="Error" type="tns:ErrorOutput"/>
        </s:sequence>
      </s:complexType>
      <s:complexType name="ArrayOfTemplateCategory">
        <s:sequence>
          <s:element maxOccurs="unbounded" minOccurs="0" name="TemplateCategory" nillable="true" type="tns:TemplateCategory"/>
        </s:sequence>
      </s:complexType>
      <s:complexType name="TemplateCategory">
        <s:sequence>
          <s:element maxOccurs="1" minOccurs="0" name="CategoryName" type="s:string"/>
          <s:element maxOccurs="1" minOccurs="1" name="Count" type="s:int"/>
        </s:sequence>
      </s:complexType>
      <s:element name="GetTemplates">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="categoryName" type="s:string"/>
            <s:element maxOccurs="1" minOccurs="0" name="sortExpression" type="s:string"/>
            <s:element maxOccurs="1" minOccurs="1" name="rate" type="s:boolean"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetTemplatesResponse">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="GetTemplatesResult" type="tns:TemplateList"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:complexType name="TemplateList">
        <s:sequence>
          <s:element maxOccurs="1" minOccurs="0" name="TemplateDetails" type="tns:ArrayOfTemplateDetail"/>
          <s:element maxOccurs="1" minOccurs="0" name="Error" type="tns:ErrorOutput"/>
        </s:sequence>
      </s:complexType>
      <s:complexType name="ArrayOfTemplateDetail">
        <s:sequence>
          <s:element maxOccurs="unbounded" minOccurs="0" name="TemplateDetail" nillable="true" type="tns:TemplateDetail"/>
        </s:sequence>
      </s:complexType>
      <s:complexType name="TemplateDetail">
        <s:sequence>
          <s:element maxOccurs="1" minOccurs="0" name="TemplateID" type="s:string"/>
          <s:element maxOccurs="1" minOccurs="0" name="CreaterID" type="s:string"/>
          <s:element maxOccurs="1" minOccurs="0" name="Name" type="s:string"/>
          <s:element maxOccurs="1" minOccurs="0" name="Category" type="s:string"/>
          <s:element maxOccurs="1" minOccurs="0" name="Comments" type="s:string"/>
          <s:element maxOccurs="1" minOccurs="1" name="NumUses" type="s:int"/>
          <s:element maxOccurs="1" minOccurs="1" name="AvgRating" type="s:float"/>
          <s:element maxOccurs="1" minOccurs="1" name="UserRating" type="s:float"/>
          <s:element maxOccurs="1" minOccurs="1" name="CreateDate" type="s:dateTime"/>
          <s:element maxOccurs="1" minOccurs="1" name="Automatic" type="s:boolean"/>
          <s:element maxOccurs="1" minOccurs="1" name="Private" type="s:boolean"/>
          <s:element maxOccurs="1" minOccurs="0" name="Attachments" type="tns:ArrayOfString"/>
          <s:element maxOccurs="1" minOccurs="1" name="CanDelete" type="s:boolean"/>
        </s:sequence>
      </s:complexType>
      <s:element name="GetAttachmentList">
        <s:complexType/>
      </s:element>
      <s:element name="GetAttachmentListResponse">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="GetAttachmentListResult" type="tns:GenericResponse"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetAttachmentProcessOuput">
        <s:complexType/>
      </s:element>
      <s:element name="GetAttachmentProcessOuputResponse">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="GetAttachmentProcessOuputResult" type="tns:GenericResponse"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetCurrentSession">
        <s:complexType/>
      </s:element>
      <s:element name="GetCurrentSessionResponse">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="GetCurrentSessionResult" type="tns:GenericResponse"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="CreateNewTemplate">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="categoryName" type="s:string"/>
            <s:element maxOccurs="1" minOccurs="0" name="templateName" type="s:string"/>
            <s:element maxOccurs="1" minOccurs="0" name="templateComments" type="s:string"/>
            <s:element maxOccurs="1" minOccurs="1" name="isPrivate" type="s:boolean"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="CreateNewTemplateResponse">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="CreateNewTemplateResult" type="tns:GenericResponse"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="RemoveAttachment">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="fileName" type="s:string"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="RemoveAttachmentResponse">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="RemoveAttachmentResult" type="tns:GenericResponse"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GenerateSSOCredentials">
        <s:complexType/>
      </s:element>
      <s:element name="GenerateSSOCredentialsResponse">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="GenerateSSOCredentialsResult" type="tns:GenericResponse"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="ConvertSpace">
        <s:complexType/>
      </s:element>
      <s:element name="ConvertSpaceResponse">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="ConvertSpaceResult" type="tns:GenericResponse"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="ResetSpaceSettings">
        <s:complexType/>
      </s:element>
      <s:element name="ResetSpaceSettingsResponse">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="ResetSpaceSettingsResult" type="tns:GenericResponse"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetSpaceProperties">
        <s:complexType/>
      </s:element>
      <s:element name="GetSpacePropertiesResponse">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="GetSpacePropertiesResult" type="tns:SpaceProperties"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:complexType name="SpaceProperties">
        <s:sequence>
          <s:element maxOccurs="1" minOccurs="0" name="SpaceName" type="s:string"/>
          <s:element maxOccurs="1" minOccurs="0" name="SpaceComments" type="s:string"/>
          <s:element maxOccurs="1" minOccurs="0" name="SpaceType" type="s:string"/>
          <s:element maxOccurs="1" minOccurs="1" name="EnableConvert" type="s:boolean"/>
          <s:element maxOccurs="1" minOccurs="1" name="TestMode" type="s:boolean"/>
          <s:element maxOccurs="1" minOccurs="1" name="checkTestMode" type="s:boolean"/>
          <s:element maxOccurs="1" minOccurs="1" name="EnableSSO" type="s:boolean"/>
          <s:element maxOccurs="1" minOccurs="1" name="checkSSO" type="s:boolean"/>
          <s:element maxOccurs="1" minOccurs="1" name="LookAndFeel" type="s:boolean"/>
          <s:element maxOccurs="1" minOccurs="1" name="UploadData" type="s:boolean"/>
          <s:element maxOccurs="1" minOccurs="1" name="LoadNumber" type="s:int"/>
          <s:element maxOccurs="1" minOccurs="0" name="Error" type="tns:ErrorOutput"/>
          <s:element maxOccurs="1" minOccurs="0" name="AdminSpaces" type="tns:ArrayOfArrayOfString"/>
          <s:element maxOccurs="1" minOccurs="0" name="SpaceTimeZone" type="s:string"/>
          <s:element maxOccurs="1" minOccurs="1" name="LockProcessingTimeZone" type="s:boolean"/>
          <s:element maxOccurs="1" minOccurs="1" name="MaxQueryRows" type="s:int"/>
          <s:element maxOccurs="1" minOccurs="1" name="MaxQueryTimeout" type="s:int"/>
          <s:element maxOccurs="1" minOccurs="1" name="QueryLanguageVersion" type="s:int"/>
          <s:element maxOccurs="1" minOccurs="1" name="RepositoryAdmin" type="s:boolean"/>
        </s:sequence>
      </s:complexType>
      <s:complexType name="ArrayOfArrayOfString">
        <s:sequence>
          <s:element maxOccurs="unbounded" minOccurs="0" name="ArrayOfString" nillable="true" type="tns:ArrayOfString"/>
        </s:sequence>
      </s:complexType>
      <s:element name="ModifySpaceProperties">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="spaceName" type="s:string"/>
            <s:element maxOccurs="1" minOccurs="0" name="spaceComments" type="s:string"/>
            <s:element maxOccurs="1" minOccurs="1" name="testMode" type="s:boolean"/>
            <s:element maxOccurs="1" minOccurs="1" name="enableSSO" type="s:boolean"/>
            <s:element maxOccurs="1" minOccurs="0" name="color" type="s:string"/>
            <s:element maxOccurs="1" minOccurs="0" name="spaceProcessingTimeZone" type="s:string"/>
            <s:element maxOccurs="1" minOccurs="1" name="maxQueryRows" type="s:int"/>
            <s:element maxOccurs="1" minOccurs="1" name="maxQueryTime" type="s:int"/>
            <s:element maxOccurs="1" minOccurs="1" name="queryLanguageVersion" type="s:int"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="ModifySpacePropertiesResponse">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="ModifySpacePropertiesResult" type="tns:GenericResponse"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="ProcessUploadedFile">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="clientFileName" type="s:string"/>
            <s:element maxOccurs="1" minOccurs="0" name="beginSkip" type="s:string"/>
            <s:element maxOccurs="1" minOccurs="0" name="endSkip" type="s:string"/>
            <s:element maxOccurs="1" minOccurs="1" name="firstRowNames" type="s:boolean"/>
            <s:element maxOccurs="1" minOccurs="1" name="lockFormat" type="s:boolean"/>
            <s:element maxOccurs="1" minOccurs="1" name="ignoreInternalQuotes" type="s:boolean"/>
            <s:element maxOccurs="1" minOccurs="0" name="escape" type="s:string"/>
            <s:element maxOccurs="1" minOccurs="1" name="filterRows" type="s:boolean"/>
            <s:element maxOccurs="1" minOccurs="0" name="encoding" type="s:string"/>
            <s:element maxOccurs="1" minOccurs="1" name="consolidate" type="s:boolean"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="ProcessUploadedFileResponse">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="ProcessUploadedFileResult" type="tns:UploaderResponse"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:complexType name="UploaderResponse">
        <s:sequence>
          <s:element maxOccurs="1" minOccurs="0" name="Error" type="tns:ErrorOutput"/>
          <s:element maxOccurs="1" minOccurs="0" name="UploadErrors" type="tns:ArrayOfUploaderError"/>
          <s:element maxOccurs="1" minOccurs="0" name="ExcelErrors" type="tns:ArrayOfExcelWarningError"/>
        </s:sequence>
      </s:complexType>
      <s:complexType name="ArrayOfUploaderError">
        <s:sequence>
          <s:element maxOccurs="unbounded" minOccurs="0" name="UploaderError" nillable="true" type="tns:UploaderError"/>
        </s:sequence>
      </s:complexType>
      <s:complexType name="UploaderError">
        <s:sequence>
          <s:element maxOccurs="1" minOccurs="0" name="Source" type="s:string"/>
          <s:element maxOccurs="1" minOccurs="0" name="Severity" type="s:string"/>
          <s:element maxOccurs="1" minOccurs="0" name="Issue" type="s:string"/>
        </s:sequence>
      </s:complexType>
      <s:complexType name="ArrayOfExcelWarningError">
        <s:sequence>
          <s:element maxOccurs="unbounded" minOccurs="0" name="ExcelWarningError" nillable="true" type="tns:ExcelWarningError"/>
        </s:sequence>
      </s:complexType>
      <s:complexType name="ExcelWarningError">
        <s:sequence>
          <s:element maxOccurs="1" minOccurs="0" name="FileName" type="s:string"/>
          <s:element maxOccurs="1" minOccurs="0" name="Rows" type="tns:ArrayOfString"/>
          <s:element maxOccurs="1" minOccurs="0" name="Defective" type="tns:ArrayOfBoolean"/>
        </s:sequence>
      </s:complexType>
      <s:complexType name="ArrayOfBoolean">
        <s:sequence>
          <s:element maxOccurs="unbounded" minOccurs="0" name="boolean" type="s:boolean"/>
        </s:sequence>
      </s:complexType>
      <s:element name="ProcessNextStage">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="beginSkip" type="s:string"/>
            <s:element maxOccurs="1" minOccurs="0" name="endSkip" type="s:string"/>
            <s:element maxOccurs="1" minOccurs="1" name="firstRowNames" type="s:boolean"/>
            <s:element maxOccurs="1" minOccurs="1" name="lockFormat" type="s:boolean"/>
            <s:element maxOccurs="1" minOccurs="1" name="ignoreInternalQuotes" type="s:boolean"/>
            <s:element maxOccurs="1" minOccurs="0" name="escape" type="s:string"/>
            <s:element maxOccurs="1" minOccurs="1" name="filterRows" type="s:boolean"/>
            <s:element maxOccurs="1" minOccurs="0" name="encoding" type="s:string"/>
            <s:element maxOccurs="1" minOccurs="1" name="consolidate" type="s:boolean"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="ProcessNextStageResponse">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="ProcessNextStageResult" type="tns:UploaderResponse"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="CancelNextStage">
        <s:complexType/>
      </s:element>
      <s:element name="CancelNextStageResponse">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="CancelNextStageResult" type="tns:GenericResponse"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="AutoPublish">
        <s:complexType/>
      </s:element>
      <s:element name="AutoPublishResponse">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="AutoPublishResult" type="tns:DashboardConfiguration"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetAllSpaces">
        <s:complexType/>
      </s:element>
      <s:element name="GetAllSpacesResponse">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="GetAllSpacesResult" type="tns:UserSpaces"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:complexType name="UserSpaces">
        <s:sequence>
          <s:element maxOccurs="1" minOccurs="0" name="Error" type="tns:ErrorOutput"/>
          <s:element maxOccurs="1" minOccurs="0" name="UserName" type="s:string"/>
          <s:element maxOccurs="1" minOccurs="0" name="FirstName" type="s:string"/>
          <s:element maxOccurs="1" minOccurs="0" name="LastName" type="s:string"/>
          <s:element maxOccurs="1" minOccurs="0" name="LastLoginDate" type="s:string"/>
          <s:element maxOccurs="1" minOccurs="1" name="EnableCopy" type="s:boolean"/>
          <s:element maxOccurs="1" minOccurs="1" name="ShowFreeTrialPage" type="s:boolean"/>
          <s:element maxOccurs="1" minOccurs="1" name="canCreateNewSpace" type="s:boolean"/>
          <s:element maxOccurs="1" minOccurs="1" name="showTemplate" type="s:boolean"/>
          <s:element maxOccurs="1" minOccurs="0" name="SpacesList" type="tns:ArrayOfSpaceSummary"/>
          <s:element maxOccurs="1" minOccurs="1" name="Selected" type="s:int"/>
          <s:element maxOccurs="1" minOccurs="0" name="Invites" type="tns:ArrayOfInviteResponse"/>
          <s:element maxOccurs="1" minOccurs="1" name="EnableUserManagement" type="s:boolean"/>
        </s:sequence>
      </s:complexType>
      <s:complexType name="ArrayOfSpaceSummary">
        <s:sequence>
          <s:element maxOccurs="unbounded" minOccurs="0" name="SpaceSummary" nillable="true" type="tns:SpaceSummary"/>
        </s:sequence>
      </s:complexType>
      <s:complexType name="SpaceSummary">
        <s:sequence>
          <s:element maxOccurs="1" minOccurs="0" name="SpaceName" type="s:string"/>
          <s:element maxOccurs="1" minOccurs="0" name="SpaceID" type="s:string"/>
          <s:element maxOccurs="1" minOccurs="0" name="OwnerUsername" type="s:string"/>
          <s:element maxOccurs="1" minOccurs="1" name="Owner" type="s:boolean"/>
        </s:sequence>
      </s:complexType>
      <s:complexType name="ArrayOfInviteResponse">
        <s:sequence>
          <s:element maxOccurs="unbounded" minOccurs="0" name="InviteResponse" nillable="true" type="tns:InviteResponse"/>
        </s:sequence>
      </s:complexType>
      <s:complexType name="InviteResponse">
        <s:sequence>
          <s:element maxOccurs="1" minOccurs="0" name="SpaceID" type="s:string"/>
          <s:element maxOccurs="1" minOccurs="0" name="Email" type="s:string"/>
          <s:element maxOccurs="1" minOccurs="0" name="InviteDate" type="s:string"/>
          <s:element maxOccurs="1" minOccurs="0" name="SpaceName" type="s:string"/>
          <s:element maxOccurs="1" minOccurs="0" name="InviteUserEmail" type="s:string"/>
        </s:sequence>
      </s:complexType>
      <s:element name="GetSpaceDetails">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="spaceID" type="s:string"/>
            <s:element maxOccurs="1" minOccurs="0" name="spaceName" type="s:string"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetSpaceDetailsResponse">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="GetSpaceDetailsResult" type="tns:SpaceDetails"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:complexType name="SpaceDetails">
        <s:sequence>
          <s:element maxOccurs="1" minOccurs="0" name="Error" type="tns:ErrorOutput"/>
          <s:element maxOccurs="1" minOccurs="0" name="SpaceName" type="s:string"/>
          <s:element maxOccurs="1" minOccurs="0" name="SpaceID" type="s:string"/>
          <s:element maxOccurs="1" minOccurs="0" name="UserEmail" type="s:string"/>
          <s:element maxOccurs="1" minOccurs="1" name="Owner" type="s:boolean"/>
          <s:element maxOccurs="1" minOccurs="0" name="OwnerUsername" type="s:string"/>
          <s:element maxOccurs="1" minOccurs="1" name="Administrator" type="s:boolean"/>
          <s:element maxOccurs="1" minOccurs="1" name="Adhoc" type="s:boolean"/>
          <s:element maxOccurs="1" minOccurs="1" name="Dashboards" type="s:boolean"/>
          <s:element maxOccurs="1" minOccurs="1" name="QuickDashboard" type="s:boolean"/>
          <s:element maxOccurs="1" minOccurs="1" name="EnableAdhoc" type="s:boolean"/>
          <s:element maxOccurs="1" minOccurs="1" name="EnableDashboards" type="s:boolean"/>
          <s:element maxOccurs="1" minOccurs="1" name="EnableQuickDashBoards" type="s:boolean"/>
          <s:element maxOccurs="1" minOccurs="0" name="AdminAccess" type="tns:AdminModulesAccess"/>
          <s:element maxOccurs="1" minOccurs="0" name="LastUsed" type="s:string"/>
          <s:element maxOccurs="1" minOccurs="0" name="LastUpload" type="s:string"/>
          <s:element maxOccurs="1" minOccurs="0" name="SpaceSize" type="s:string"/>
          <s:element maxOccurs="1" minOccurs="0" name="SpaceComments" type="s:string"/>
          <s:element maxOccurs="1" minOccurs="1" name="SuperUser" type="s:boolean"/>
        </s:sequence>
      </s:complexType>
      <s:complexType name="AdminModulesAccess">
        <s:sequence>
          <s:element maxOccurs="1" minOccurs="1" name="ManageCatalog" type="s:boolean"/>
          <s:element maxOccurs="1" minOccurs="1" name="Publish" type="s:boolean"/>
          <s:element maxOccurs="1" minOccurs="1" name="UseTemplate" type="s:boolean"/>
          <s:element maxOccurs="1" minOccurs="1" name="ServicesAdmin" type="s:boolean"/>
          <s:element maxOccurs="1" minOccurs="1" name="CopySpace" type="s:boolean"/>
          <s:element maxOccurs="1" minOccurs="1" name="CustomFormula" type="s:boolean"/>
          <s:element maxOccurs="1" minOccurs="1" name="ShareSpace" type="s:boolean"/>
        </s:sequence>
      </s:complexType>
      <s:element name="GetLoggedInSpaceDetails">
        <s:complexType/>
      </s:element>
      <s:element name="GetLoggedInSpaceDetailsResponse">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="GetLoggedInSpaceDetailsResult" type="tns:SpaceDetails"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="RemoveMember">
        <s:complexType/>
      </s:element>
      <s:element name="RemoveMemberResponse">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="RemoveMemberResult" type="tns:GenericResponse"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="SendInvitationResponse">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="1" name="accept" type="s:boolean"/>
            <s:element maxOccurs="1" minOccurs="0" name="spaceID" type="s:string"/>
            <s:element maxOccurs="1" minOccurs="0" name="userEmail" type="s:string"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="SendInvitationResponseResponse">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="SendInvitationResponseResult" type="tns:GenericResponse"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="CreateSpaceCopy">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="spaceName" type="s:string"/>
            <s:element maxOccurs="1" minOccurs="0" name="copyComments" type="s:string"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="CreateSpaceCopyResponse">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="CreateSpaceCopyResult" type="tns:GenericResponse"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="CreateNewSpace">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="spaceName" type="s:string"/>
            <s:element maxOccurs="1" minOccurs="0" name="spaceComments" type="s:string"/>
            <s:element maxOccurs="1" minOccurs="1" name="automatic" type="s:boolean"/>
            <s:element maxOccurs="1" minOccurs="1" name="useTemplate" type="s:boolean"/>
            <s:element maxOccurs="1" minOccurs="1" name="queryLanguageVersion" type="s:int"/>
            <s:element maxOccurs="1" minOccurs="0" name="templateID" type="s:string"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="CreateNewSpaceResponse">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="CreateNewSpaceResult" type="tns:GenericResponse"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="DeleteTemplate">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="templateID" type="s:string"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="DeleteTemplateResponse">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="DeleteTemplateResult" type="tns:GenericResponse"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetPublishLogDetails">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="1" name="loadNumber" type="s:int"/>
            <s:element maxOccurs="1" minOccurs="0" name="loadGroup" type="s:string"/>
            <s:element maxOccurs="1" minOccurs="0" name="loadDate" type="s:string"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetPublishLogDetailsResponse">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="GetPublishLogDetailsResult" type="tns:PublishLogResult"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:complexType name="PublishLogResult">
        <s:sequence>
          <s:element maxOccurs="1" minOccurs="0" name="Summary" type="tns:ArrayOfPublishLogSummary"/>
          <s:element maxOccurs="1" minOccurs="0" name="DimensionResult" type="tns:ArrayOfDimensionTableResult"/>
          <s:element maxOccurs="1" minOccurs="0" name="MeasureResult" type="tns:ArrayOfMeasureTableResult"/>
          <s:element maxOccurs="1" minOccurs="0" name="Error" type="tns:ErrorOutput"/>
        </s:sequence>
      </s:complexType>
      <s:complexType name="ArrayOfPublishLogSummary">
        <s:sequence>
          <s:element maxOccurs="unbounded" minOccurs="0" name="PublishLogSummary" nillable="true" type="tns:PublishLogSummary"/>
        </s:sequence>
      </s:complexType>
      <s:complexType name="PublishLogSummary">
        <s:sequence>
          <s:element maxOccurs="1" minOccurs="0" name="DataSource" type="s:string"/>
          <s:element maxOccurs="1" minOccurs="0" name="Processed" type="s:string"/>
          <s:element maxOccurs="1" minOccurs="0" name="Errors" type="s:string"/>
          <s:element maxOccurs="1" minOccurs="0" name="Warnings" type="s:string"/>
        </s:sequence>
      </s:complexType>
      <s:complexType name="ArrayOfDimensionTableResult">
        <s:sequence>
          <s:element maxOccurs="unbounded" minOccurs="0" name="DimensionTableResult" nillable="true" type="tns:DimensionTableResult"/>
        </s:sequence>
      </s:complexType>
      <s:complexType name="DimensionTableResult">
        <s:sequence>
          <s:element maxOccurs="1" minOccurs="0" name="Dimension" type="s:string"/>
          <s:element maxOccurs="1" minOccurs="0" name="Level" type="s:string"/>
          <s:element maxOccurs="1" minOccurs="0" name="Type" type="s:string"/>
          <s:element maxOccurs="1" minOccurs="0" name="DataSource" type="s:string"/>
          <s:element maxOccurs="1" minOccurs="0" name="Rows" type="s:string"/>
          <s:element maxOccurs="1" minOccurs="0" name="Columns" type="s:string"/>
          <s:element maxOccurs="1" minOccurs="0" name="ID" type="s:string"/>
          <s:element maxOccurs="1" minOccurs="1" name="Load" type="s:int"/>
          <s:element maxOccurs="1" minOccurs="0" name="Group" type="s:string"/>
          <s:element maxOccurs="1" minOccurs="0" name="Date" type="s:string"/>
        </s:sequence>
      </s:complexType>
      <s:complexType name="ArrayOfMeasureTableResult">
        <s:sequence>
          <s:element maxOccurs="unbounded" minOccurs="0" name="MeasureTableResult" nillable="true" type="tns:MeasureTableResult"/>
        </s:sequence>
      </s:complexType>
      <s:complexType name="MeasureTableResult">
        <s:sequence>
          <s:element maxOccurs="1" minOccurs="0" name="Grain" type="s:string"/>
          <s:element maxOccurs="1" minOccurs="0" name="Type" type="s:string"/>
          <s:element maxOccurs="1" minOccurs="0" name="DataSource" type="s:string"/>
          <s:element maxOccurs="1" minOccurs="0" name="Rows" type="s:string"/>
          <s:element maxOccurs="1" minOccurs="0" name="Columns" type="s:string"/>
          <s:element maxOccurs="1" minOccurs="0" name="ID" type="s:string"/>
          <s:element maxOccurs="1" minOccurs="1" name="Load" type="s:int"/>
          <s:element maxOccurs="1" minOccurs="0" name="Group" type="s:string"/>
          <s:element maxOccurs="1" minOccurs="0" name="Date" type="s:string"/>
        </s:sequence>
      </s:complexType>
      <s:element name="GetFileDetails">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="fileName" type="s:string"/>
            <s:element maxOccurs="1" minOccurs="0" name="loadDate" type="s:string"/>
            <s:element maxOccurs="1" minOccurs="1" name="warnings" type="s:boolean"/>
            <s:element maxOccurs="1" minOccurs="1" name="loadNumber" type="s:int"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetFileDetailsResponse">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="GetFileDetailsResult" type="tns:GenericResponse"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetQueryDetails">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="queryid" type="s:string"/>
            <s:element maxOccurs="1" minOccurs="0" name="dimension" type="s:string"/>
            <s:element maxOccurs="1" minOccurs="0" name="level" type="s:string"/>
            <s:element maxOccurs="1" minOccurs="0" name="source" type="s:string"/>
            <s:element maxOccurs="1" minOccurs="0" name="grain" type="s:string"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetQueryDetailsResponse">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="GetQueryDetailsResult" type="tns:GenericResponse"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetDynamicGroups">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="selectedGroup" type="s:string"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetDynamicGroupsResponse">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="GetDynamicGroupsResult" type="tns:DynamicGroupsResult"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:complexType name="DynamicGroupsResult">
        <s:sequence>
          <s:element maxOccurs="1" minOccurs="0" name="Hierarchy" type="tns:ArrayOfString"/>
          <s:element maxOccurs="1" minOccurs="0" name="sourceColumns" type="tns:ArrayOfString"/>
          <s:element maxOccurs="1" minOccurs="0" name="selectedGroupSource" type="s:string"/>
          <s:element maxOccurs="1" minOccurs="0" name="selectedUserColumn" type="s:string"/>
          <s:element maxOccurs="1" minOccurs="0" name="selectedGroupColumn" type="s:string"/>
          <s:element maxOccurs="1" minOccurs="0" name="Error" type="tns:ErrorOutput"/>
        </s:sequence>
      </s:complexType>
      <s:element name="GetMAFGroups">
        <s:complexType/>
      </s:element>
      <s:element name="GetMAFGroupsResponse">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="GetMAFGroupsResult" type="tns:GenericResponse"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetGroupDetails">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="groupName" type="s:string"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetGroupDetailsResponse">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="GetGroupDetailsResult" type="tns:GroupDetails"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:complexType name="GroupDetails">
        <s:sequence>
          <s:element maxOccurs="1" minOccurs="0" name="MAFGroups" type="tns:ArrayOfString"/>
          <s:element maxOccurs="1" minOccurs="0" name="GroupName" type="s:string"/>
          <s:element maxOccurs="1" minOccurs="1" name="AdhocAccess" type="s:boolean"/>
          <s:element maxOccurs="1" minOccurs="1" name="DashboardAccess" type="s:boolean"/>
          <s:element maxOccurs="1" minOccurs="1" name="EnableDownloadAccess" type="s:boolean"/>
          <s:element maxOccurs="1" minOccurs="0" name="Members" type="tns:ArrayOfMemberDetails"/>
          <s:element maxOccurs="1" minOccurs="0" name="Error" type="tns:ErrorOutput"/>
        </s:sequence>
      </s:complexType>
      <s:complexType name="ArrayOfMemberDetails">
        <s:sequence>
          <s:element maxOccurs="unbounded" minOccurs="0" name="MemberDetails" nillable="true" type="tns:MemberDetails"/>
        </s:sequence>
      </s:complexType>
      <s:complexType name="MemberDetails">
        <s:sequence>
          <s:element maxOccurs="1" minOccurs="0" name="Username" type="s:string"/>
          <s:element maxOccurs="1" minOccurs="1" name="Member" type="s:boolean"/>
          <s:element maxOccurs="1" minOccurs="1" name="Admin" type="s:boolean"/>
        </s:sequence>
      </s:complexType>
      <s:element name="AddNewGroup">
        <s:complexType/>
      </s:element>
      <s:element name="AddNewGroupResponse">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="AddNewGroupResult" type="tns:GenericResponse"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="DeleteGroup">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="name" type="s:string"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="DeleteGroupResponse">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="DeleteGroupResult" type="tns:GenericResponse"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="SaveGroup">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="oldName" type="s:string"/>
            <s:element maxOccurs="1" minOccurs="0" name="newName" type="s:string"/>
            <s:element maxOccurs="1" minOccurs="0" name="userNames" type="tns:ArrayOfString"/>
            <s:element maxOccurs="1" minOccurs="0" name="isMember" type="tns:ArrayOfBoolean"/>
            <s:element maxOccurs="1" minOccurs="1" name="adhocAccess" type="s:boolean"/>
            <s:element maxOccurs="1" minOccurs="1" name="dashboardAccess" type="s:boolean"/>
            <s:element maxOccurs="1" minOccurs="1" name="enableDownloadAccess" type="s:boolean"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="SaveGroupResponse">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="SaveGroupResult" type="tns:GenericResponse"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="AssignDynamicGroups">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="groupHierarchy" type="s:string"/>
            <s:element maxOccurs="1" minOccurs="0" name="userColumn" type="s:string"/>
            <s:element maxOccurs="1" minOccurs="0" name="groupColumn" type="s:string"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="AssignDynamicGroupsResponse">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="AssignDynamicGroupsResult" type="tns:GenericResponse"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="CompleteGroupManagement">
        <s:complexType/>
      </s:element>
      <s:element name="CompleteGroupManagementResponse">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="CompleteGroupManagementResult" type="tns:GenericResponse"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetSpaceShareDetails">
        <s:complexType/>
      </s:element>
      <s:element name="GetSpaceShareDetailsResponse">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="GetSpaceShareDetailsResult" type="tns:ShareSpaceResult"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:complexType name="ShareSpaceResult">
        <s:sequence>
          <s:element maxOccurs="1" minOccurs="0" name="InvitedMembers" type="tns:ArrayOfInvite"/>
          <s:element maxOccurs="1" minOccurs="0" name="SpaceAccessMembers" type="tns:ArrayOfMemberDetails"/>
          <s:element maxOccurs="1" minOccurs="0" name="Error" type="tns:ErrorOutput"/>
        </s:sequence>
      </s:complexType>
      <s:complexType name="ArrayOfInvite">
        <s:sequence>
          <s:element maxOccurs="unbounded" minOccurs="0" name="Invite" nillable="true" type="tns:Invite"/>
        </s:sequence>
      </s:complexType>
      <s:complexType name="Invite">
        <s:sequence>
          <s:element maxOccurs="1" minOccurs="0" name="SpaceID" type="s:string"/>
          <s:element maxOccurs="1" minOccurs="0" name="Email" type="s:string"/>
          <s:element maxOccurs="1" minOccurs="1" name="InviteDate" type="s:dateTime"/>
          <s:element maxOccurs="1" minOccurs="0" name="SpaceName" type="s:string"/>
          <s:element maxOccurs="1" minOccurs="0" name="InviteUserEmail" type="s:string"/>
        </s:sequence>
      </s:complexType>
      <s:element name="SendInvite">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="emailId" type="s:string"/>
            <s:element maxOccurs="1" minOccurs="0" name="emailText" type="s:string"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="SendInviteResponse">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="SendInviteResult" type="tns:GenericResponse"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="DeleteInvite">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="emailId" type="s:string"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="DeleteInviteResponse">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="DeleteInviteResult" type="tns:GenericResponse"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="UpdateAdminAccess">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="accessMembers" type="tns:ArrayOfArrayOfString"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="UpdateAdminAccessResponse">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="UpdateAdminAccessResult" type="tns:GenericResponse"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="RemoveAccess">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="emailId" type="s:string"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="RemoveAccessResponse">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="RemoveAccessResult" type="tns:GenericResponse"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="createSMIWebLogin">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="1" name="fillSessionVars" type="s:boolean"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="createSMIWebLoginResponse">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="createSMIWebLoginResult" type="tns:GenericResponse"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="swapContentWithSpace">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="id" type="s:string"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="swapContentWithSpaceResponse">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="swapContentWithSpaceResult" type="tns:GenericResponse"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="getTimeZones">
        <s:complexType/>
      </s:element>
      <s:element name="getTimeZonesResponse">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="getTimeZonesResult" type="tns:GenericResponse"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="executeCommand">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="command" type="s:string"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="executeCommandResponse">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="executeCommandResult" type="s:string"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="updateLogicalModel">
        <s:complexType/>
      </s:element>
      <s:element name="updateLogicalModelResponse">
        <s:complexType/>
      </s:element>
      <s:element name="getAccountSummary">
        <s:complexType/>
      </s:element>
      <s:element name="getAccountSummaryResponse">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="getAccountSummaryResult" type="tns:AccountSummary"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:complexType name="AccountSummary">
        <s:complexContent mixed="false">
          <s:extension base="tns:GenericResponse">
            <s:sequence>
              <s:element maxOccurs="1" minOccurs="0" name="AccountID" type="s:string"/>
              <s:element maxOccurs="1" minOccurs="0" name="Name" type="s:string"/>
              <s:element maxOccurs="1" minOccurs="0" name="ExpirationDate" type="s:string"/>
              <s:element maxOccurs="1" minOccurs="1" name="ActiveUsers" type="s:int"/>
              <s:element maxOccurs="1" minOccurs="1" name="TotalAssignedUsers" type="s:int"/>
              <s:element maxOccurs="1" minOccurs="1" name="MaxAllowedUsers" type="s:int"/>
              <s:element maxOccurs="1" minOccurs="1" name="MaxConcurrentUsers" type="s:int"/>
            </s:sequence>
          </s:extension>
        </s:complexContent>
      </s:complexType>
      <s:element name="getNumAccountUsers">
        <s:complexType/>
      </s:element>
      <s:element name="getNumAccountUsersResponse">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="getNumAccountUsersResult" type="tns:GenericResponse"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="getUsersToManage">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="searchString" type="s:string"/>
            <s:element maxOccurs="1" minOccurs="0" name="spaceID" type="s:string"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="getUsersToManageResponse">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="getUsersToManageResult" type="tns:UsersDetails"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="getSpacesToManage">
        <s:complexType/>
      </s:element>
      <s:element name="getSpacesToManageResponse">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="getSpacesToManageResult" type="tns:SpacesToManage"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:complexType name="SpacesToManage">
        <s:complexContent mixed="false">
          <s:extension base="tns:GenericResponse">
            <s:sequence>
              <s:element maxOccurs="1" minOccurs="0" name="Spaces" type="tns:ArrayOfSpaceSummary"/>
            </s:sequence>
          </s:extension>
        </s:complexContent>
      </s:complexType>
      <s:element name="addUserToSpace">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="toAddUsersIds" type="tns:ArrayOfString"/>
            <s:element maxOccurs="1" minOccurs="0" name="spaceID" type="s:string"/>
            <s:element maxOccurs="1" minOccurs="1" name="admin" type="s:boolean"/>
            <s:element maxOccurs="1" minOccurs="1" name="adhoc" type="s:boolean"/>
            <s:element maxOccurs="1" minOccurs="1" name="dashboards" type="s:boolean"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="addUserToSpaceResponse">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="addUserToSpaceResult" type="tns:GenericResponse"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="updatePassword">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="usersIds" type="tns:ArrayOfString"/>
            <s:element maxOccurs="1" minOccurs="0" name="newPassword" type="s:string"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="updatePasswordResponse">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="updatePasswordResult" type="tns:GenericResponse"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="updateUserStatus">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="usersIds" type="tns:ArrayOfString"/>
            <s:element maxOccurs="1" minOccurs="1" name="enable" type="s:boolean"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="updateUserStatusResponse">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="updateUserStatusResult" type="tns:GenericResponse"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="unlockUsers">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="usersIds" type="tns:ArrayOfString"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="unlockUsersResponse">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="unlockUsersResult" type="tns:GenericResponse"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="updateAccountAdminPriveleges">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="usersIds" type="tns:ArrayOfString"/>
            <s:element maxOccurs="1" minOccurs="1" name="isAdmin" type="s:boolean"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="updateAccountAdminPrivelegesResponse">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="updateAccountAdminPrivelegesResult" type="tns:GenericResponse"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="processCreateUserFile">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="clientFileName" type="s:string"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="processCreateUserFileResponse">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="processCreateUserFileResult" type="tns:GenericResponse"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetSources">
        <s:complexType/>
      </s:element>
      <s:element name="GetSourcesResponse">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="GetSourcesResult" type="tns:AdminRepository"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:complexType name="AdminRepository">
        <s:sequence>
          <s:element maxOccurs="1" minOccurs="1" name="Automatic" type="s:boolean"/>
          <s:element maxOccurs="1" minOccurs="0" name="TimeDimensionName" type="s:string"/>
          <s:element maxOccurs="1" minOccurs="0" name="Hierarchies" type="tns:ArrayOfHierarchy"/>
          <s:element maxOccurs="1" minOccurs="0" name="Sources" type="tns:ArrayOfAdminDataSource"/>
          <s:element maxOccurs="1" minOccurs="0" name="RepositoryPath" type="s:string"/>
          <s:element maxOccurs="1" minOccurs="1" name="Published" type="s:boolean"/>
          <s:element maxOccurs="1" minOccurs="1" name="BirstConnect" type="s:boolean"/>
          <s:element maxOccurs="1" minOccurs="1" name="LocalConnect" type="s:boolean"/>
          <s:element maxOccurs="1" minOccurs="1" name="AllowUpload" type="s:boolean"/>
          <s:element maxOccurs="1" minOccurs="1" name="queryLanguageVersion" type="s:int"/>
        </s:sequence>
      </s:complexType>
      <s:complexType name="ArrayOfHierarchy">
        <s:sequence>
          <s:element maxOccurs="unbounded" minOccurs="0" name="Hierarchy" nillable="true" type="tns:Hierarchy"/>
        </s:sequence>
      </s:complexType>
      <s:complexType name="Hierarchy">
        <s:complexContent mixed="false">
          <s:extension base="tns:BaseObject">
            <s:sequence>
              <s:element maxOccurs="1" minOccurs="0" name="Name" type="s:string"/>
              <s:element maxOccurs="1" minOccurs="0" name="DimensionName" type="s:string"/>
              <s:element maxOccurs="1" minOccurs="0" name="DimensionKeyLevel" type="s:string"/>
              <s:element maxOccurs="1" minOccurs="0" name="ExclusionFilter" type="s:string"/>
              <s:element maxOccurs="1" minOccurs="0" name="NoMeasureFilter" type="s:string"/>
              <s:element maxOccurs="1" minOccurs="0" name="Children" type="tns:ArrayOfLevel"/>
              <s:element maxOccurs="1" minOccurs="0" name="SharedChildren" type="tns:ArrayOfString"/>
              <s:element maxOccurs="1" minOccurs="1" name="AutoGenerated" type="s:boolean"/>
              <s:element maxOccurs="1" minOccurs="1" name="GeneratedTime" type="s:boolean"/>
              <s:element maxOccurs="1" minOccurs="1" name="Hidden" type="s:boolean"/>
              <s:element maxOccurs="1" minOccurs="1" name="Locked" type="s:boolean"/>
              <s:element maxOccurs="1" minOccurs="1" name="ProcessedDependencies" type="s:boolean"/>
              <s:element default="false" maxOccurs="1" minOccurs="0" name="FilterForAllLoads" type="s:boolean"/>
              <s:element maxOccurs="1" minOccurs="0" name="SourceGroups" type="s:string"/>
              <s:element default="false" maxOccurs="1" minOccurs="0" name="ModelWizard" type="s:boolean"/>
            </s:sequence>
          </s:extension>
        </s:complexContent>
      </s:complexType>
      <s:complexType abstract="true" name="BaseObject"/>
      <s:complexType name="SnapshotPolicy">
        <s:complexContent mixed="false">
          <s:extension base="tns:BaseObject">
            <s:sequence>
              <s:element maxOccurs="1" minOccurs="1" name="CurrentDay" type="s:boolean"/>
              <s:element maxOccurs="1" minOccurs="1" name="Type" type="s:int"/>
              <s:element maxOccurs="1" minOccurs="1" name="DaysOfWeek" type="s:int"/>
              <s:element maxOccurs="1" minOccurs="1" name="DayOfMonth" type="s:int"/>
              <s:element maxOccurs="1" minOccurs="1" name="firstDayOfMonth" type="s:boolean"/>
              <s:element maxOccurs="1" minOccurs="1" name="lastDayOfMonth" type="s:boolean"/>
            </s:sequence>
          </s:extension>
        </s:complexContent>
      </s:complexType>
      <s:complexType name="SecurityFilter">
        <s:complexContent mixed="false">
          <s:extension base="tns:BaseObject">
            <s:sequence>
              <s:element maxOccurs="1" minOccurs="1" name="Type" type="s:int"/>
              <s:element maxOccurs="1" minOccurs="0" name="SessionVariable" type="s:string"/>
              <s:element maxOccurs="1" minOccurs="1" name="Enabled" type="s:boolean"/>
              <s:element maxOccurs="1" minOccurs="0" name="FilterGroups" type="tns:ArrayOfString"/>
            </s:sequence>
          </s:extension>
        </s:complexContent>
      </s:complexType>
      <s:complexType name="StagingAggregation">
        <s:complexContent mixed="false">
          <s:extension base="tns:BaseObject">
            <s:sequence>
              <s:element maxOccurs="1" minOccurs="0" name="Type" type="s:string"/>
              <s:element maxOccurs="1" minOccurs="0" name="Default" type="s:string"/>
              <s:element maxOccurs="1" minOccurs="0" name="ValueList" type="tns:ArrayOfString"/>
              <s:element maxOccurs="1" minOccurs="0" name="SetList" type="tns:ArrayOfArrayOfString"/>
            </s:sequence>
          </s:extension>
        </s:complexContent>
      </s:complexType>
      <s:complexType name="StagingColumnGrainInfo">
        <s:complexContent mixed="false">
          <s:extension base="tns:BaseObject">
            <s:sequence>
              <s:element maxOccurs="1" minOccurs="0" name="Dimension" type="s:string"/>
              <s:element maxOccurs="1" minOccurs="0" name="Level" type="s:string"/>
              <s:element maxOccurs="1" minOccurs="0" name="StagingFilterName" type="s:string"/>
              <s:element maxOccurs="1" minOccurs="0" name="NewName" type="s:string"/>
              <s:element maxOccurs="1" minOccurs="1" name="NewNameType" type="s:int"/>
              <s:element maxOccurs="1" minOccurs="0" name="Aggregation" type="tns:StagingAggregation"/>
            </s:sequence>
          </s:extension>
        </s:complexContent>
      </s:complexType>
      <s:complexType name="Transformation">
        <s:complexContent mixed="false">
          <s:extension base="tns:BaseObject">
            <s:sequence>
              <s:element maxOccurs="1" minOccurs="1" name="ExecuteAfter" type="s:int"/>
            </s:sequence>
          </s:extension>
        </s:complexContent>
      </s:complexType>
      <s:complexType name="Rank">
        <s:complexContent mixed="false">
          <s:extension base="tns:Transformation">
            <s:sequence>
              <s:element maxOccurs="1" minOccurs="1" name="Type" type="s:int"/>
              <s:element maxOccurs="1" minOccurs="1" name="NTileNum" type="s:int"/>
              <s:element maxOccurs="1" minOccurs="0" name="PartitionByColumns" type="tns:ArrayOfString"/>
              <s:element maxOccurs="1" minOccurs="0" name="OrderByColumns" type="tns:ArrayOfString"/>
              <s:element maxOccurs="1" minOccurs="0" name="OrderByAscending" type="tns:ArrayOfBoolean"/>
            </s:sequence>
          </s:extension>
        </s:complexContent>
      </s:complexType>
      <s:complexType name="TableLookup">
        <s:complexContent mixed="false">
          <s:extension base="tns:Transformation">
            <s:sequence>
              <s:element maxOccurs="1" minOccurs="0" name="Formula" type="s:string"/>
              <s:element maxOccurs="1" minOccurs="0" name="LookupTable" type="s:string"/>
              <s:element maxOccurs="1" minOccurs="0" name="JoinCondition" type="s:string"/>
            </s:sequence>
          </s:extension>
        </s:complexContent>
      </s:complexType>
      <s:complexType name="DateID">
        <s:complexContent mixed="false">
          <s:extension base="tns:Transformation">
            <s:sequence>
              <s:element maxOccurs="1" minOccurs="0" name="DateExpression" type="s:string"/>
              <s:element maxOccurs="1" minOccurs="1" name="DateType" type="s:int"/>
            </s:sequence>
          </s:extension>
        </s:complexContent>
      </s:complexType>
      <s:complexType name="Expression">
        <s:complexContent mixed="false">
          <s:extension base="tns:Transformation">
            <s:sequence>
              <s:element maxOccurs="1" minOccurs="0" name="Formula" type="s:string"/>
            </s:sequence>
          </s:extension>
        </s:complexContent>
      </s:complexType>
      <s:complexType name="StagingColumn">
        <s:complexContent mixed="false">
          <s:extension base="tns:BaseObject">
            <s:sequence>
              <s:element maxOccurs="1" minOccurs="0" name="Name" type="s:string"/>
              <s:element maxOccurs="1" minOccurs="0" name="DataType" type="s:string"/>
              <s:element maxOccurs="1" minOccurs="1" name="Width" type="s:int"/>
              <s:element maxOccurs="1" minOccurs="0" name="SourceFileColumn" type="s:string"/>
              <s:element maxOccurs="1" minOccurs="0" name="Transformations" type="tns:ArrayOfChoice1"/>
              <s:element maxOccurs="1" minOccurs="0" name="TargetTypes" type="tns:ArrayOfString"/>
              <s:element default="false" maxOccurs="1" minOccurs="0" name="NaturalKey" type="s:boolean"/>
              <s:element default="false" maxOccurs="1" minOccurs="0" name="GenerateTimeDimension" type="s:boolean"/>
              <s:element default="false" maxOccurs="1" minOccurs="0" name="Index" type="s:boolean"/>
              <s:element maxOccurs="1" minOccurs="0" name="GrainInfo" type="tns:ArrayOfStagingColumnGrainInfo"/>
              <s:element maxOccurs="1" minOccurs="0" name="UnknownValue" type="s:string"/>
              <s:element maxOccurs="1" minOccurs="0" name="TargetAggregations" type="tns:ArrayOfString"/>
              <s:element default="false" maxOccurs="1" minOccurs="0" name="ExcludeFromChecksum" type="s:boolean"/>
              <s:element default="false" maxOccurs="1" minOccurs="0" name="TableKey" type="s:boolean"/>
              <s:element maxOccurs="1" minOccurs="0" name="SecFilter" type="tns:SecurityFilter"/>
              <s:element default="false" maxOccurs="1" minOccurs="0" name="AnalyzeMeasure" type="s:boolean"/>
            </s:sequence>
          </s:extension>
        </s:complexContent>
      </s:complexType>
      <s:complexType name="ArrayOfChoice1">
        <s:choice maxOccurs="unbounded" minOccurs="0">
          <s:element maxOccurs="1" minOccurs="1" name="TableLookup" nillable="true" type="tns:TableLookup"/>
          <s:element maxOccurs="1" minOccurs="1" name="DateID" nillable="true" type="tns:DateID"/>
          <s:element maxOccurs="1" minOccurs="1" name="Rank" nillable="true" type="tns:Rank"/>
          <s:element maxOccurs="1" minOccurs="1" name="Expression" nillable="true" type="tns:Expression"/>
        </s:choice>
      </s:complexType>
      <s:complexType name="ArrayOfStagingColumnGrainInfo">
        <s:sequence>
          <s:element maxOccurs="unbounded" minOccurs="0" name="StagingColumnGrainInfo" nillable="true" type="tns:StagingColumnGrainInfo"/>
        </s:sequence>
      </s:complexType>
      <s:complexType name="LevelKey">
        <s:complexContent mixed="false">
          <s:extension base="tns:BaseObject">
            <s:sequence>
              <s:element maxOccurs="1" minOccurs="1" name="SurrogateKey" type="s:boolean"/>
              <s:element maxOccurs="1" minOccurs="0" name="ColumnNames" type="tns:ArrayOfString"/>
            </s:sequence>
          </s:extension>
        </s:complexContent>
      </s:complexType>
      <s:complexType name="Level">
        <s:complexContent mixed="false">
          <s:extension base="tns:BaseObject">
            <s:sequence>
              <s:element maxOccurs="1" minOccurs="0" name="Name" type="s:string"/>
              <s:element maxOccurs="1" minOccurs="1" name="Cardinality" type="s:int"/>
              <s:element maxOccurs="1" minOccurs="0" name="ColumnNames" type="tns:ArrayOfString"/>
              <s:element maxOccurs="1" minOccurs="0" name="HiddenColumns" type="tns:ArrayOfBoolean"/>
              <s:element maxOccurs="1" minOccurs="0" name="Children" type="tns:ArrayOfLevel"/>
              <s:element maxOccurs="1" minOccurs="0" name="Keys" type="tns:ArrayOfLevelKey"/>
              <s:element maxOccurs="1" minOccurs="0" name="SharedChildren" type="tns:ArrayOfString"/>
              <s:element maxOccurs="1" minOccurs="1" name="GenerateDimensionTable" type="s:boolean"/>
              <s:element maxOccurs="1" minOccurs="1" name="Degenerate" type="s:boolean"/>
              <s:element maxOccurs="1" minOccurs="1" name="SCDType" type="s:int"/>
              <s:element maxOccurs="1" minOccurs="1" name="GenerateInheritedCurrentDimTable" type="s:boolean"/>
              <s:element maxOccurs="1" minOccurs="1" name="Locked" type="s:boolean"/>
            </s:sequence>
          </s:extension>
        </s:complexContent>
      </s:complexType>
      <s:complexType name="ArrayOfLevel">
        <s:sequence>
          <s:element maxOccurs="unbounded" minOccurs="0" name="Level" nillable="true" type="tns:Level"/>
        </s:sequence>
      </s:complexType>
      <s:complexType name="ArrayOfLevelKey">
        <s:sequence>
          <s:element maxOccurs="unbounded" minOccurs="0" name="LevelKey" nillable="true" type="tns:LevelKey"/>
        </s:sequence>
      </s:complexType>
      <s:complexType name="ArrayOfAdminDataSource">
        <s:sequence>
          <s:element maxOccurs="unbounded" minOccurs="0" name="AdminDataSource" nillable="true" type="tns:AdminDataSource"/>
        </s:sequence>
      </s:complexType>
      <s:complexType name="AdminDataSource">
        <s:sequence>
          <s:element maxOccurs="1" minOccurs="0" name="Name" type="s:string"/>
          <s:element maxOccurs="1" minOccurs="0" name="DisplayName" type="s:string"/>
          <s:element maxOccurs="1" minOccurs="1" name="Status" type="s:int"/>
          <s:element maxOccurs="1" minOccurs="0" name="StatusMessage" type="s:string"/>
          <s:element maxOccurs="1" minOccurs="1" name="Enabled" type="s:boolean"/>
          <s:element maxOccurs="1" minOccurs="1" name="LastUploadDate" type="s:dateTime"/>
          <s:element maxOccurs="1" minOccurs="1" name="NumColumns" type="s:int"/>
          <s:element maxOccurs="1" minOccurs="1" name="NumRows" type="s:int"/>
          <s:element maxOccurs="1" minOccurs="1" name="FileSize" type="s:long"/>
          <s:element maxOccurs="1" minOccurs="1" name="FileExists" type="s:boolean"/>
          <s:element maxOccurs="1" minOccurs="1" name="ReadOnly" type="s:boolean"/>
          <s:element maxOccurs="1" minOccurs="1" name="Transactional" type="s:boolean"/>
          <s:element maxOccurs="1" minOccurs="1" name="TruncateOnLoad" type="s:boolean"/>
          <s:element maxOccurs="1" minOccurs="1" name="LockFormat" type="s:boolean"/>
          <s:element maxOccurs="1" minOccurs="1" name="SchemaLock" type="s:boolean"/>
          <s:element maxOccurs="1" minOccurs="1" name="AllowAddColumns" type="s:boolean"/>
          <s:element maxOccurs="1" minOccurs="1" name="AllowNullBindingRemovedColumns" type="s:boolean"/>
          <s:element maxOccurs="1" minOccurs="1" name="AllowUpcast" type="s:boolean"/>
          <s:element maxOccurs="1" minOccurs="1" name="AllowVarcharExpansion" type="s:boolean"/>
          <s:element maxOccurs="1" minOccurs="1" name="AllowValueTruncationOnLoad" type="s:boolean"/>
          <s:element maxOccurs="1" minOccurs="1" name="FailUploadOnVarcharExpansion" type="s:boolean"/>
          <s:element maxOccurs="1" minOccurs="1" name="CustomUpload" type="s:boolean"/>
          <s:element maxOccurs="1" minOccurs="0" name="Levels" type="tns:ArrayOfArrayOfString"/>
          <s:element maxOccurs="1" minOccurs="0" name="StagingColumns" type="tns:ArrayOfStagingColumn"/>
          <s:element maxOccurs="1" minOccurs="0" name="SourceFileColumnTypes" type="tns:ArrayOfString"/>
          <s:element maxOccurs="1" minOccurs="0" name="SourceFileColumnPreventUpdate" type="tns:ArrayOfBoolean"/>
          <s:element maxOccurs="1" minOccurs="0" name="Script" type="tns:ScriptDefinition"/>
          <s:element maxOccurs="1" minOccurs="0" name="CustomTimeColumn" type="s:string"/>
          <s:element maxOccurs="1" minOccurs="0" name="CustomTimePrefix" type="s:string"/>
          <s:element maxOccurs="1" minOccurs="0" name="CustomTimeShifts" type="tns:ArrayOfCustomTimeShift"/>
          <s:element maxOccurs="1" minOccurs="0" name="SubGroups" type="tns:ArrayOfString"/>
          <s:element maxOccurs="1" minOccurs="0" name="SourceGroups" type="s:string"/>
          <s:element maxOccurs="1" minOccurs="0" name="LoadGroups" type="tns:ArrayOfString"/>
          <s:element maxOccurs="1" minOccurs="0" name="Snapshots" type="tns:SnapshotPolicy"/>
          <s:element maxOccurs="1" minOccurs="1" name="IncrementalSnapshotFact" type="s:boolean"/>
          <s:element maxOccurs="1" minOccurs="0" name="SnapshotDeleteKeys" type="tns:ArrayOfString"/>
          <s:element maxOccurs="1" minOccurs="0" name="NewColumnTarget" type="s:string"/>
          <s:element maxOccurs="1" minOccurs="0" name="InputTimeZone" type="s:string"/>
          <s:element maxOccurs="1" minOccurs="0" name="SourceKey" type="tns:ArrayOfString"/>
          <s:element maxOccurs="1" minOccurs="0" name="ForeignKeySources" type="tns:ArrayOfString"/>
          <s:element maxOccurs="1" minOccurs="0" name="ParentForeignKeySource" type="s:string"/>
          <s:element maxOccurs="1" minOccurs="0" name="HierarchyName" type="s:string"/>
          <s:element maxOccurs="1" minOccurs="0" name="LevelName" type="s:string"/>
          <s:element maxOccurs="1" minOccurs="1" name="ExcludeFromModel" type="s:boolean"/>
        </s:sequence>
      </s:complexType>
      <s:complexType name="ArrayOfStagingColumn">
        <s:sequence>
          <s:element maxOccurs="unbounded" minOccurs="0" name="StagingColumn" nillable="true" type="tns:StagingColumn"/>
        </s:sequence>
      </s:complexType>
      <s:complexType name="ScriptDefinition">
        <s:sequence>
          <s:element maxOccurs="1" minOccurs="0" name="InputQuery" type="s:string"/>
          <s:element maxOccurs="1" minOccurs="0" name="Output" type="s:string"/>
          <s:element maxOccurs="1" minOccurs="0" name="Script" type="s:string"/>
        </s:sequence>
      </s:complexType>
      <s:complexType name="ArrayOfCustomTimeShift">
        <s:sequence>
          <s:element maxOccurs="unbounded" minOccurs="0" name="CustomTimeShift" nillable="true" type="tns:CustomTimeShift"/>
        </s:sequence>
      </s:complexType>
      <s:complexType name="CustomTimeShift">
        <s:sequence>
          <s:element maxOccurs="1" minOccurs="0" name="BaseKey" type="s:string"/>
          <s:element maxOccurs="1" minOccurs="0" name="RelativeKey" type="s:string"/>
          <s:element maxOccurs="1" minOccurs="0" name="Prefix" type="s:string"/>
          <s:element maxOccurs="1" minOccurs="0" name="Columns" type="tns:ArrayOfString"/>
        </s:sequence>
      </s:complexType>
      <s:element name="GetSourceDetails">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="stagingTableName" type="s:string"/>
            <s:element maxOccurs="1" minOccurs="0" name="sourceFileName" type="s:string"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetSourceDetailsResponse">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="GetSourceDetailsResult" type="tns:AdminDataSource"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="saveRepository">
        <s:complexType/>
      </s:element>
      <s:element name="saveRepositoryResponse">
        <s:complexType/>
      </s:element>
      <s:element name="saveStagingColumn">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="tableName" type="s:string"/>
            <s:element maxOccurs="1" minOccurs="0" name="sc" type="tns:StagingColumn"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="saveStagingColumnResponse">
        <s:complexType/>
      </s:element>
      <s:element name="removeStagingColumn">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="tableName" type="s:string"/>
            <s:element maxOccurs="1" minOccurs="0" name="name" type="s:string"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="removeStagingColumnResponse">
        <s:complexType/>
      </s:element>
      <s:element name="renameStagingColumn">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="tableName" type="s:string"/>
            <s:element maxOccurs="1" minOccurs="0" name="oldName" type="s:string"/>
            <s:element maxOccurs="1" minOccurs="0" name="newName" type="s:string"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="renameStagingColumnResponse">
        <s:complexType/>
      </s:element>
      <s:element name="saveHierarchy">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="h" type="tns:Hierarchy"/>
            <s:element maxOccurs="1" minOccurs="0" name="oldName" type="s:string"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="saveHierarchyResponse">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="1" name="saveHierarchyResult" type="s:boolean"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="removeHierarchy">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="hname" type="s:string"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="removeHierarchyResponse">
        <s:complexType/>
      </s:element>
      <s:element name="saveLevel">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="hierarchyName" type="s:string"/>
            <s:element maxOccurs="1" minOccurs="0" name="l" type="tns:Level"/>
            <s:element maxOccurs="1" minOccurs="0" name="oldLevelName" type="s:string"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="saveLevelResponse">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="1" name="saveLevelResult" type="s:boolean"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="saveDataSource">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="name" type="s:string"/>
            <s:element maxOccurs="1" minOccurs="0" name="displayName" type="s:string"/>
            <s:element maxOccurs="1" minOccurs="0" name="levels" type="tns:ArrayOfArrayOfString"/>
            <s:element maxOccurs="1" minOccurs="1" name="enabled" type="s:boolean"/>
            <s:element maxOccurs="1" minOccurs="1" name="transactional" type="s:boolean"/>
            <s:element maxOccurs="1" minOccurs="1" name="truncateonload" type="s:boolean"/>
            <s:element maxOccurs="1" minOccurs="1" name="lockformat" type="s:boolean"/>
            <s:element maxOccurs="1" minOccurs="1" name="schemalock" type="s:boolean"/>
            <s:element maxOccurs="1" minOccurs="1" name="AllowAddColumns" type="s:boolean"/>
            <s:element maxOccurs="1" minOccurs="1" name="AllowNullBindingRemovedColumns" type="s:boolean"/>
            <s:element maxOccurs="1" minOccurs="1" name="AllowUpcast" type="s:boolean"/>
            <s:element maxOccurs="1" minOccurs="1" name="AllowVarcharExpansion" type="s:boolean"/>
            <s:element maxOccurs="1" minOccurs="1" name="AllowValueTruncationOnLoad" type="s:boolean"/>
            <s:element maxOccurs="1" minOccurs="1" name="FailUploadOnVarcharExpansion" type="s:boolean"/>
            <s:element maxOccurs="1" minOccurs="0" name="script" type="tns:ScriptDefinition"/>
            <s:element maxOccurs="1" minOccurs="0" name="loadGroups" type="tns:ArrayOfString"/>
            <s:element maxOccurs="1" minOccurs="0" name="sourceGroups" type="s:string"/>
            <s:element maxOccurs="1" minOccurs="0" name="customTimeColumn" type="s:string"/>
            <s:element maxOccurs="1" minOccurs="0" name="customTimePrefix" type="s:string"/>
            <s:element maxOccurs="1" minOccurs="0" name="timeShifts" type="tns:ArrayOfCustomTimeShift"/>
            <s:element maxOccurs="1" minOccurs="0" name="subGroups" type="tns:ArrayOfString"/>
            <s:element maxOccurs="1" minOccurs="0" name="snapshots" type="tns:SnapshotPolicy"/>
            <s:element maxOccurs="1" minOccurs="0" name="newColumnTarget" type="s:string"/>
            <s:element maxOccurs="1" minOccurs="0" name="dataSourceTimeZone" type="s:string"/>
            <s:element maxOccurs="1" minOccurs="0" name="sourceKey" type="tns:ArrayOfString"/>
            <s:element maxOccurs="1" minOccurs="0" name="foreignKeySources" type="tns:ArrayOfString"/>
            <s:element maxOccurs="1" minOccurs="0" name="parentForeignKeySource" type="s:string"/>
            <s:element maxOccurs="1" minOccurs="0" name="hierarchyName" type="s:string"/>
            <s:element maxOccurs="1" minOccurs="0" name="levelName" type="s:string"/>
            <s:element maxOccurs="1" minOccurs="1" name="excludeFromModel" type="s:boolean"/>
            <s:element maxOccurs="1" minOccurs="0" name="SFColPreventUpdate" type="tns:ArrayOfBoolean"/>
            <s:element maxOccurs="1" minOccurs="1" name="customUpload" type="s:boolean"/>
            <s:element maxOccurs="1" minOccurs="1" name="incrementalSnapshotFact" type="s:boolean"/>
            <s:element maxOccurs="1" minOccurs="0" name="snapshotDeleteKeys" type="tns:ArrayOfString"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="saveDataSourceResponse">
        <s:complexType/>
      </s:element>
      <s:element name="saveDataSourceAndColumns">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="name" type="s:string"/>
            <s:element maxOccurs="1" minOccurs="0" name="displayName" type="s:string"/>
            <s:element maxOccurs="1" minOccurs="0" name="levels" type="tns:ArrayOfArrayOfString"/>
            <s:element maxOccurs="1" minOccurs="1" name="enabled" type="s:boolean"/>
            <s:element maxOccurs="1" minOccurs="1" name="transactional" type="s:boolean"/>
            <s:element maxOccurs="1" minOccurs="1" name="truncateonload" type="s:boolean"/>
            <s:element maxOccurs="1" minOccurs="1" name="lockformat" type="s:boolean"/>
            <s:element maxOccurs="1" minOccurs="1" name="schemalock" type="s:boolean"/>
            <s:element maxOccurs="1" minOccurs="1" name="AllowAddColumns" type="s:boolean"/>
            <s:element maxOccurs="1" minOccurs="1" name="AllowNullBindingRemovedColumns" type="s:boolean"/>
            <s:element maxOccurs="1" minOccurs="1" name="AllowUpcast" type="s:boolean"/>
            <s:element maxOccurs="1" minOccurs="1" name="AllowVarcharExpansion" type="s:boolean"/>
            <s:element maxOccurs="1" minOccurs="1" name="AllowValueTruncationOnLoad" type="s:boolean"/>
            <s:element maxOccurs="1" minOccurs="1" name="FailUploadOnVarcharExpansion" type="s:boolean"/>
            <s:element maxOccurs="1" minOccurs="0" name="script" type="tns:ScriptDefinition"/>
            <s:element maxOccurs="1" minOccurs="0" name="loadGroups" type="tns:ArrayOfString"/>
            <s:element maxOccurs="1" minOccurs="0" name="sourceGroups" type="s:string"/>
            <s:element maxOccurs="1" minOccurs="0" name="customTimeColumn" type="s:string"/>
            <s:element maxOccurs="1" minOccurs="0" name="customTimePrefix" type="s:string"/>
            <s:element maxOccurs="1" minOccurs="0" name="timeShifts" type="tns:ArrayOfCustomTimeShift"/>
            <s:element maxOccurs="1" minOccurs="0" name="subGroups" type="tns:ArrayOfString"/>
            <s:element maxOccurs="1" minOccurs="0" name="colList" type="tns:ArrayOfStagingColumn"/>
            <s:element maxOccurs="1" minOccurs="0" name="snapshots" type="tns:SnapshotPolicy"/>
            <s:element maxOccurs="1" minOccurs="0" name="newColumnTarget" type="s:string"/>
            <s:element maxOccurs="1" minOccurs="0" name="dataSourceTimeZone" type="s:string"/>
            <s:element maxOccurs="1" minOccurs="0" name="sourceKey" type="tns:ArrayOfString"/>
            <s:element maxOccurs="1" minOccurs="0" name="foreignKeySources" type="tns:ArrayOfString"/>
            <s:element maxOccurs="1" minOccurs="0" name="parentForeignKeySource" type="s:string"/>
            <s:element maxOccurs="1" minOccurs="0" name="hierarchyName" type="s:string"/>
            <s:element maxOccurs="1" minOccurs="0" name="levelName" type="s:string"/>
            <s:element maxOccurs="1" minOccurs="1" name="excludeFromModel" type="s:boolean"/>
            <s:element maxOccurs="1" minOccurs="0" name="SFColPreventUpdate" type="tns:ArrayOfBoolean"/>
            <s:element maxOccurs="1" minOccurs="1" name="customUpload" type="s:boolean"/>
            <s:element maxOccurs="1" minOccurs="1" name="incrementalSnapshotFact" type="s:boolean"/>
            <s:element maxOccurs="1" minOccurs="0" name="snapshotDeleteKeys" type="tns:ArrayOfString"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="saveDataSourceAndColumnsResponse">
        <s:complexType/>
      </s:element>
      <s:element name="saveAndValidateScript">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="name" type="s:string"/>
            <s:element maxOccurs="1" minOccurs="0" name="script" type="tns:ScriptDefinition"/>
            <s:element maxOccurs="1" minOccurs="0" name="part" type="s:string"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="saveAndValidateScriptResponse">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="saveAndValidateScriptResult" type="s1:ValidateResult"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="executeScript">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="name" type="s:string"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="executeScriptResponse">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="executeScriptResult" type="s1:ExecuteResult"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="removeDataSource">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="name" type="s:string"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="removeDataSourceResponse">
        <s:complexType/>
      </s:element>
      <s:element name="EmptyDataSource">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="name" type="s:string"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="EmptyDataSourceResponse">
        <s:complexType/>
      </s:element>
      <s:element name="getRawData">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="sourceName" type="s:string"/>
            <s:element maxOccurs="1" minOccurs="1" name="startRow" type="s:int"/>
            <s:element maxOccurs="1" minOccurs="1" name="endRow" type="s:int"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="getRawDataResponse">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="getRawDataResult" type="tns:ArrayOfArrayOfString"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetRepository">
        <s:complexType/>
      </s:element>
      <s:element name="GetRepositoryResponse">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="GetRepositoryResult" type="tns:RARepository"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:complexType name="RARepository">
        <s:sequence>
          <s:element maxOccurs="1" minOccurs="0" name="Dimensions" type="tns:ArrayOfRADimension"/>
          <s:element maxOccurs="1" minOccurs="0" name="Measures" type="tns:ArrayOfRAMeasureColumn"/>
          <s:element maxOccurs="1" minOccurs="0" name="MeasureTables" type="tns:ArrayOfRAMeasureTable"/>
          <s:element maxOccurs="1" minOccurs="0" name="Joins" type="tns:ArrayOfRAJoin"/>
          <s:element maxOccurs="1" minOccurs="0" name="Connections" type="tns:ArrayOfRAConnection"/>
          <s:element maxOccurs="1" minOccurs="0" name="Hierarchies" type="tns:ArrayOfHierarchy"/>
          <s:element maxOccurs="1" minOccurs="1" name="queryLanguageVersion" type="s:int"/>
        </s:sequence>
      </s:complexType>
      <s:complexType name="ArrayOfRADimension">
        <s:sequence>
          <s:element maxOccurs="unbounded" minOccurs="0" name="RADimension" nillable="true" type="tns:RADimension"/>
        </s:sequence>
      </s:complexType>
      <s:complexType name="RADimension">
        <s:sequence>
          <s:element maxOccurs="1" minOccurs="0" name="Name" type="s:string"/>
          <s:element maxOccurs="1" minOccurs="1" name="Locked" type="s:boolean"/>
          <s:element maxOccurs="1" minOccurs="0" name="Definitions" type="tns:ArrayOfRADimensionTable"/>
          <s:element maxOccurs="1" minOccurs="0" name="Columns" type="tns:ArrayOfRADimensionColumn"/>
        </s:sequence>
      </s:complexType>
      <s:complexType name="ArrayOfRADimensionTable">
        <s:sequence>
          <s:element maxOccurs="unbounded" minOccurs="0" name="RADimensionTable" nillable="true" type="tns:RADimensionTable"/>
        </s:sequence>
      </s:complexType>
      <s:complexType name="RADimensionTable">
        <s:sequence>
          <s:element maxOccurs="1" minOccurs="0" name="Name" type="s:string"/>
          <s:element maxOccurs="1" minOccurs="0" name="Level" type="s:string"/>
          <s:element maxOccurs="1" minOccurs="1" name="Type" type="s:int"/>
          <s:element maxOccurs="1" minOccurs="0" name="Source" type="tns:TableSource"/>
          <s:element maxOccurs="1" minOccurs="0" name="Query" type="s:string"/>
          <s:element maxOccurs="1" minOccurs="1" name="Cacheable" type="s:boolean"/>
          <s:element maxOccurs="1" minOccurs="0" name="Mappings" type="tns:ArrayOfRADimensionColumnMapping"/>
        </s:sequence>
      </s:complexType>
      <s:complexType name="TableSource">
        <s:complexContent mixed="false">
          <s:extension base="tns:BaseObject">
            <s:sequence>
              <s:element maxOccurs="1" minOccurs="0" name="Tables" type="tns:ArrayOfTableDefinition"/>
              <s:element maxOccurs="1" minOccurs="0" name="Connection" type="s:string"/>
              <s:element maxOccurs="1" minOccurs="0" name="Schema" type="s:string"/>
              <s:element maxOccurs="1" minOccurs="0" name="ContentFilters" type="tns:ArrayOfString"/>
              <s:element maxOccurs="1" minOccurs="0" name="Filters" type="tns:ArrayOfTableSourceFilter"/>
            </s:sequence>
          </s:extension>
        </s:complexContent>
      </s:complexType>
      <s:complexType name="ArrayOfTableDefinition">
        <s:sequence>
          <s:element maxOccurs="unbounded" minOccurs="0" name="TableDefinition" nillable="true" type="tns:TableDefinition"/>
        </s:sequence>
      </s:complexType>
      <s:complexType name="TableDefinition">
        <s:complexContent mixed="false">
          <s:extension base="tns:BaseObject">
            <s:sequence>
              <s:element maxOccurs="1" minOccurs="0" name="PhysicalName" type="s:string"/>
              <s:element default="0" maxOccurs="1" minOccurs="0" name="JoinType" type="s:int"/>
              <s:element maxOccurs="1" minOccurs="0" name="JoinClause" type="s:string"/>
            </s:sequence>
          </s:extension>
        </s:complexContent>
      </s:complexType>
      <s:complexType name="ArrayOfTableSourceFilter">
        <s:sequence>
          <s:element maxOccurs="unbounded" minOccurs="0" name="TableSourceFilter" nillable="true" type="tns:TableSourceFilter"/>
        </s:sequence>
      </s:complexType>
      <s:complexType name="TableSourceFilter">
        <s:complexContent mixed="false">
          <s:extension base="tns:BaseObject">
            <s:sequence>
              <s:element maxOccurs="1" minOccurs="0" name="Filter" type="s:string"/>
              <s:element default="false" maxOccurs="1" minOccurs="0" name="CurrentLoadFilter" type="s:boolean"/>
              <s:element default="false" maxOccurs="1" minOccurs="0" name="SecurityFilter" type="s:boolean"/>
              <s:element maxOccurs="1" minOccurs="0" name="FilterGroups" type="tns:ArrayOfString"/>
            </s:sequence>
          </s:extension>
        </s:complexContent>
      </s:complexType>
      <s:complexType name="ArrayOfRADimensionColumnMapping">
        <s:sequence>
          <s:element maxOccurs="unbounded" minOccurs="0" name="RADimensionColumnMapping" nillable="true" type="tns:RADimensionColumnMapping"/>
        </s:sequence>
      </s:complexType>
      <s:complexType name="RADimensionColumnMapping">
        <s:sequence>
          <s:element maxOccurs="1" minOccurs="0" name="ColumnName" type="s:string"/>
          <s:element maxOccurs="1" minOccurs="0" name="Formula" type="s:string"/>
        </s:sequence>
      </s:complexType>
      <s:complexType name="ArrayOfRADimensionColumn">
        <s:sequence>
          <s:element maxOccurs="unbounded" minOccurs="0" name="RADimensionColumn" nillable="true" type="tns:RADimensionColumn"/>
        </s:sequence>
      </s:complexType>
      <s:complexType name="RADimensionColumn">
        <s:sequence>
          <s:element maxOccurs="1" minOccurs="0" name="ColumnName" type="s:string"/>
          <s:element maxOccurs="1" minOccurs="0" name="DataType" type="s:string"/>
          <s:element maxOccurs="1" minOccurs="1" name="Width" type="s:int"/>
        </s:sequence>
      </s:complexType>
      <s:complexType name="ArrayOfRAMeasureColumn">
        <s:sequence>
          <s:element maxOccurs="unbounded" minOccurs="0" name="RAMeasureColumn" nillable="true" type="tns:RAMeasureColumn"/>
        </s:sequence>
      </s:complexType>
      <s:complexType name="RAMeasureColumn">
        <s:sequence>
          <s:element maxOccurs="1" minOccurs="0" name="ColumnName" type="s:string"/>
          <s:element maxOccurs="1" minOccurs="0" name="AggRule" type="s:string"/>
          <s:element maxOccurs="1" minOccurs="0" name="DataType" type="s:string"/>
          <s:element maxOccurs="1" minOccurs="1" name="Width" type="s:int"/>
        </s:sequence>
      </s:complexType>
      <s:complexType name="ArrayOfRAMeasureTable">
        <s:sequence>
          <s:element maxOccurs="unbounded" minOccurs="0" name="RAMeasureTable" nillable="true" type="tns:RAMeasureTable"/>
        </s:sequence>
      </s:complexType>
      <s:complexType name="RAMeasureTable">
        <s:sequence>
          <s:element maxOccurs="1" minOccurs="0" name="Name" type="s:string"/>
          <s:element maxOccurs="1" minOccurs="1" name="Type" type="s:int"/>
          <s:element maxOccurs="1" minOccurs="0" name="Source" type="tns:TableSource"/>
          <s:element maxOccurs="1" minOccurs="0" name="Query" type="s:string"/>
          <s:element maxOccurs="1" minOccurs="1" name="Cacheable" type="s:boolean"/>
          <s:element maxOccurs="1" minOccurs="1" name="Cardinality" type="s:double"/>
          <s:element maxOccurs="1" minOccurs="0" name="Mappings" type="tns:ArrayOfRAMeasureColumnMapping"/>
        </s:sequence>
      </s:complexType>
      <s:complexType name="ArrayOfRAMeasureColumnMapping">
        <s:sequence>
          <s:element maxOccurs="unbounded" minOccurs="0" name="RAMeasureColumnMapping" nillable="true" type="tns:RAMeasureColumnMapping"/>
        </s:sequence>
      </s:complexType>
      <s:complexType name="RAMeasureColumnMapping">
        <s:sequence>
          <s:element maxOccurs="1" minOccurs="0" name="ColumnName" type="s:string"/>
          <s:element maxOccurs="1" minOccurs="0" name="Formula" type="s:string"/>
        </s:sequence>
      </s:complexType>
      <s:complexType name="ArrayOfRAJoin">
        <s:sequence>
          <s:element maxOccurs="unbounded" minOccurs="0" name="RAJoin" nillable="true" type="tns:RAJoin"/>
        </s:sequence>
      </s:complexType>
      <s:complexType name="RAJoin">
        <s:sequence>
          <s:element maxOccurs="1" minOccurs="0" name="Table1" type="s:string"/>
          <s:element maxOccurs="1" minOccurs="0" name="Table2" type="s:string"/>
          <s:element maxOccurs="1" minOccurs="0" name="JoinCondition" type="s:string"/>
          <s:element maxOccurs="1" minOccurs="1" name="Redundant" type="s:boolean"/>
          <s:element maxOccurs="1" minOccurs="0" name="JoinType" type="s:string"/>
          <s:element maxOccurs="1" minOccurs="1" name="Invalid" type="s:boolean"/>
        </s:sequence>
      </s:complexType>
      <s:complexType name="ArrayOfRAConnection">
        <s:sequence>
          <s:element maxOccurs="unbounded" minOccurs="0" name="RAConnection" nillable="true" type="tns:RAConnection"/>
        </s:sequence>
      </s:complexType>
      <s:complexType name="RAConnection">
        <s:sequence>
          <s:element maxOccurs="1" minOccurs="0" name="Name" type="s:string"/>
          <s:element maxOccurs="1" minOccurs="0" name="Type" type="s:string"/>
        </s:sequence>
      </s:complexType>
      <s:element name="SetRepository">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="rap" type="tns:RARepository"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="SetRepositoryResponse">
        <s:complexType/>
      </s:element>
      <s:element name="getSchemaTables">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="connection" type="s:string"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="getSchemaTablesResponse">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="getSchemaTablesResult" type="tns:ArrayOfArrayOfAnyType"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:complexType name="ArrayOfArrayOfAnyType">
        <s:sequence>
          <s:element maxOccurs="unbounded" minOccurs="0" name="ArrayOfAnyType" nillable="true" type="tns:ArrayOfAnyType"/>
        </s:sequence>
      </s:complexType>
      <s:complexType name="ArrayOfAnyType">
        <s:sequence>
          <s:element maxOccurs="unbounded" minOccurs="0" name="anyType" nillable="true"/>
        </s:sequence>
      </s:complexType>
      <s:element name="getCubes">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="connection" type="s:string"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="getCubesResponse">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="getCubesResult" type="tns:ArrayOfArrayOfAnyType"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="getCube">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="connection" type="s:string"/>
            <s:element maxOccurs="1" minOccurs="0" name="name" type="s:string"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="getCubeResponse">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="getCubeResult" type="tns:ArrayOfArrayOfAnyType"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="getTableSchema">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="connection" type="s:string"/>
            <s:element maxOccurs="1" minOccurs="0" name="tables" type="tns:ArrayOfString"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="getTableSchemaResponse">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="getTableSchemaResult" type="tns:ArrayOfArrayOfAnyType"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="isConnected">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="connections" type="tns:ArrayOfString"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="isConnectedResponse">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="1" name="isConnectedResult" type="s:boolean"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="updateScheduledReport">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="id" type="s:string"/>
            <s:element maxOccurs="1" minOccurs="0" name="deliveryType" type="s:string"/>
            <s:element maxOccurs="1" minOccurs="0" name="reportPath" type="s:string"/>
            <s:element maxOccurs="1" minOccurs="0" name="triggerReportPath" type="s:string"/>
            <s:element maxOccurs="1" minOccurs="0" name="toReportPath" type="s:string"/>
            <s:element maxOccurs="1" minOccurs="0" name="toList" type="tns:ArrayOfString"/>
            <s:element maxOccurs="1" minOccurs="0" name="subject" type="s:string"/>
            <s:element maxOccurs="1" minOccurs="1" name="interval" type="s:int"/>
            <s:element maxOccurs="1" minOccurs="1" name="dayofweek" type="s:int"/>
            <s:element maxOccurs="1" minOccurs="1" name="dayofmonth" type="s:int"/>
            <s:element maxOccurs="1" minOccurs="1" name="hour" type="s:int"/>
            <s:element maxOccurs="1" minOccurs="1" name="minute" type="s:int"/>
            <s:element maxOccurs="1" minOccurs="0" name="emailbody" type="s:string"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="updateScheduledReportResponse">
        <s:complexType/>
      </s:element>
      <s:element name="runScheduledReport">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="reportId" type="s:string"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="runScheduledReportResponse">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="runScheduledReportResult" type="tns:GenericResponse"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="addScheduledReport">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="deliveryType" type="s:string"/>
            <s:element maxOccurs="1" minOccurs="0" name="reportPath" type="s:string"/>
            <s:element maxOccurs="1" minOccurs="0" name="triggerReportPath" type="s:string"/>
            <s:element maxOccurs="1" minOccurs="0" name="toReportPath" type="s:string"/>
            <s:element maxOccurs="1" minOccurs="0" name="toList" type="tns:ArrayOfString"/>
            <s:element maxOccurs="1" minOccurs="0" name="subject" type="s:string"/>
            <s:element maxOccurs="1" minOccurs="1" name="interval" type="s:int"/>
            <s:element maxOccurs="1" minOccurs="1" name="dayofweek" type="s:int"/>
            <s:element maxOccurs="1" minOccurs="1" name="dayofmonth" type="s:int"/>
            <s:element maxOccurs="1" minOccurs="1" name="hour" type="s:int"/>
            <s:element maxOccurs="1" minOccurs="1" name="minute" type="s:int"/>
            <s:element maxOccurs="1" minOccurs="0" name="emailbody" type="s:string"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="addScheduledReportResponse">
        <s:complexType/>
      </s:element>
      <s:element name="getScheduledReports">
        <s:complexType/>
      </s:element>
      <s:complexType name="ScheduledReport">
        <s:sequence>
          <s:element maxOccurs="1" minOccurs="0" name="ID" type="s:string"/>
          <s:element maxOccurs="1" minOccurs="0" name="SpaceID" type="s:string"/>
          <s:element maxOccurs="1" minOccurs="0" name="User" type="s:string"/>
          <s:element maxOccurs="1" minOccurs="0" name="ReportPath" type="s:string"/>
          <s:element maxOccurs="1" minOccurs="1" name="Interval" type="s:int"/>
          <s:element maxOccurs="1" minOccurs="1" name="DayOfWeek" type="s:int"/>
          <s:element maxOccurs="1" minOccurs="1" name="DayOfMonth" type="s:int"/>
          <s:element maxOccurs="1" minOccurs="1" name="Hour" type="s:int"/>
          <s:element maxOccurs="1" minOccurs="1" name="Minute" type="s:int"/>
          <s:element maxOccurs="1" minOccurs="0" name="Type" type="s:string"/>
          <s:element maxOccurs="1" minOccurs="0" name="Subject" type="s:string"/>
          <s:element maxOccurs="1" minOccurs="0" name="EmailBody" type="s:string"/>
          <s:element maxOccurs="1" minOccurs="0" name="TriggerReportPath" type="s:string"/>
          <s:element maxOccurs="1" minOccurs="0" name="ToList" type="tns:ArrayOfString"/>
          <s:element maxOccurs="1" minOccurs="0" name="ToReportPath" type="s:string"/>
        </s:sequence>
      </s:complexType>
      <s:complexType name="ArrayOfScheduledReport">
        <s:sequence>
          <s:element maxOccurs="unbounded" minOccurs="0" name="ScheduledReport" nillable="true" type="tns:ScheduledReport"/>
        </s:sequence>
      </s:complexType>
      <s:element name="getScheduledReportsResponse">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="getScheduledReportsResult" type="tns:ArrayOfScheduledReport"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="getScheduledReport">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="id" type="s:string"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="getScheduledReportResponse">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="getScheduledReportResult" type="tns:ScheduledReport"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="deleteScheduledReport">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="id" type="s:string"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="deleteScheduledReportResponse">
        <s:complexType/>
      </s:element>
      <s:element name="exportData">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="reportPath" type="s:string"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="exportDataResponse">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="exportDataResult" type="s1:ExecuteResult"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="getExportedDataFiles">
        <s:complexType/>
      </s:element>
      <s:element name="getExportedDataFilesResponse">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="getExportedDataFilesResult" type="tns:ArrayOfString"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="getReportList">
        <s:complexType/>
      </s:element>
      <s:element name="getReportListResponse">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="getReportListResult" type="tns:ArrayOfString"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="validateQuery">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="query" type="s:string"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="validateQueryResponse">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="validateQueryResult" type="tns:GenericResponse"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="getBirstConnectConfigs">
        <s:complexType/>
      </s:element>
      <s:complexType name="BirstConnectConfig">
        <s:sequence>
          <s:element maxOccurs="1" minOccurs="0" name="Name" type="s:string"/>
        </s:sequence>
      </s:complexType>
      <s:complexType name="ArrayOfBirstConnectConfig">
        <s:sequence>
          <s:element maxOccurs="unbounded" minOccurs="0" name="BirstConnectConfig" nillable="true" type="tns:BirstConnectConfig"/>
        </s:sequence>
      </s:complexType>
      <s:element name="getBirstConnectConfigsResponse">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="getBirstConnectConfigsResult" type="tns:ArrayOfBirstConnectConfig"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="deleteBirstConnectConfig">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="filename" type="s:string"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="deleteBirstConnectConfigResponse">
        <s:complexType/>
      </s:element>
      <s:element name="addBirstConnectConfig">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="filename" type="s:string"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="addBirstConnectConfigResponse">
        <s:complexType/>
      </s:element>
      <s:element name="renameBirstConnectConfig">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="oldname" type="s:string"/>
            <s:element maxOccurs="1" minOccurs="0" name="newname" type="s:string"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="renameBirstConnectConfigResponse">
        <s:complexType/>
      </s:element>
      <s:element name="getVariables">
        <s:complexType/>
      </s:element>
      <s:complexType name="ArrayOfVariable">
        <s:sequence>
          <s:element maxOccurs="unbounded" minOccurs="0" name="Variable" nillable="true" type="tns:Variable"/>
        </s:sequence>
      </s:complexType>
      <s:complexType name="Variable">
        <s:complexContent mixed="false">
          <s:extension base="tns:BaseObject">
            <s:sequence>
              <s:element maxOccurs="1" minOccurs="1" name="Type" type="tns:VariableType"/>
              <s:element maxOccurs="1" minOccurs="1" name="Session" type="s:boolean"/>
              <s:element maxOccurs="1" minOccurs="1" name="MultiValue" type="s:boolean"/>
              <s:element maxOccurs="1" minOccurs="1" name="Constant" type="s:boolean"/>
              <s:element maxOccurs="1" minOccurs="1" name="MultipleColumns" type="s:boolean"/>
              <s:element maxOccurs="1" minOccurs="1" name="LogicalQuery" type="s:boolean"/>
              <s:element maxOccurs="1" minOccurs="1" name="PhysicalQuery" type="s:boolean"/>
              <s:element maxOccurs="1" minOccurs="0" name="Name" type="s:string"/>
              <s:element maxOccurs="1" minOccurs="0" name="Query" type="s:string"/>
              <s:element maxOccurs="1" minOccurs="0" name="Connection" type="s:string"/>
              <s:element maxOccurs="1" minOccurs="0" name="DefaultIfInvalid" type="s:string"/>
              <s:element maxOccurs="1" minOccurs="1" name="Cacheable" type="s:boolean"/>
              <s:element maxOccurs="1" minOccurs="1" name="EvaluateOnDemand" type="s:boolean"/>
              <s:element default="false" maxOccurs="1" minOccurs="0" name="BirstCanModify" type="s:boolean"/>
            </s:sequence>
          </s:extension>
        </s:complexContent>
      </s:complexType>
      <s:simpleType name="VariableType">
        <s:restriction base="s:string">
          <s:enumeration value="Session"/>
          <s:enumeration value="Repository"/>
          <s:enumeration value="Warehouse"/>
        </s:restriction>
      </s:simpleType>
      <s:element name="getVariablesResponse">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="getVariablesResult" type="tns:ArrayOfVariable"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="updateVariable">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="v" type="tns:Variable"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="updateVariableResponse">
        <s:complexType/>
      </s:element>
      <s:element name="deleteVariable">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="name" type="s:string"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="deleteVariableResponse">
        <s:complexType/>
      </s:element>
      <s:element name="getLogicalExpressions">
        <s:complexType/>
      </s:element>
      <s:complexType name="CustomFormulasOutput">
        <s:sequence>
          <s:element maxOccurs="1" minOccurs="0" name="LogicalExpression" type="tns:LogicalExpression"/>
          <s:element maxOccurs="1" minOccurs="0" name="Sources" type="tns:ArrayOfString"/>
          <s:element maxOccurs="1" minOccurs="1" name="Valid" type="s:boolean"/>
        </s:sequence>
      </s:complexType>
      <s:complexType name="LogicalExpression">
        <s:sequence>
          <s:element maxOccurs="1" minOccurs="1" name="Type" type="s:int"/>
          <s:element maxOccurs="1" minOccurs="0" name="Name" type="s:string"/>
          <s:element maxOccurs="1" minOccurs="0" name="AggregationRule" type="s:string"/>
          <s:element maxOccurs="1" minOccurs="0" name="Levels" type="tns:ArrayOfArrayOfString"/>
          <s:element maxOccurs="1" minOccurs="0" name="Expression" type="s:string"/>
        </s:sequence>
      </s:complexType>
      <s:complexType name="ArrayOfCustomFormulasOutput">
        <s:sequence>
          <s:element maxOccurs="unbounded" minOccurs="0" name="CustomFormulasOutput" nillable="true" type="tns:CustomFormulasOutput"/>
        </s:sequence>
      </s:complexType>
      <s:element name="getLogicalExpressionsResponse">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="getLogicalExpressionsResult" type="tns:ArrayOfCustomFormulasOutput"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="getAggregates">
        <s:complexType/>
      </s:element>
      <s:complexType name="AggregateOutput">
        <s:sequence>
          <s:element maxOccurs="1" minOccurs="0" name="Aggregate" type="tns:Aggregate"/>
          <s:element maxOccurs="1" minOccurs="1" name="Valid" type="s:boolean"/>
        </s:sequence>
      </s:complexType>
      <s:complexType name="Aggregate">
        <s:complexContent mixed="false">
          <s:extension base="tns:BaseObject">
            <s:sequence>
              <s:element maxOccurs="1" minOccurs="0" name="Name" type="s:string"/>
              <s:element maxOccurs="1" minOccurs="1" name="Query" type="s:boolean"/>
              <s:element maxOccurs="1" minOccurs="0" name="Measures" type="tns:ArrayOfString"/>
              <s:element maxOccurs="1" minOccurs="0" name="Columns" type="tns:ArrayOfAggColumn"/>
              <s:element maxOccurs="1" minOccurs="0" name="Filters" type="tns:ArrayOfString"/>
              <s:element maxOccurs="1" minOccurs="0" name="LogicalQuery" type="s:string"/>
              <s:element maxOccurs="1" minOccurs="0" name="Transformations" type="tns:ArrayOfAggTransformation"/>
              <s:element maxOccurs="1" minOccurs="0" name="LoadGroups" type="tns:ArrayOfString"/>
              <s:element maxOccurs="1" minOccurs="1" name="OuterJoin" type="s:boolean"/>
              <s:element maxOccurs="1" minOccurs="1" name="Disabled" type="s:boolean"/>
              <s:element maxOccurs="1" minOccurs="0" name="Connection" type="s:string"/>
              <s:element maxOccurs="1" minOccurs="0" name="SubGroups" type="tns:ArrayOfString"/>
            </s:sequence>
          </s:extension>
        </s:complexContent>
      </s:complexType>
      <s:complexType name="ArrayOfAggColumn">
        <s:sequence>
          <s:element maxOccurs="unbounded" minOccurs="0" name="AggColumn" nillable="true" type="tns:AggColumn"/>
        </s:sequence>
      </s:complexType>
      <s:complexType name="AggColumn">
        <s:complexContent mixed="false">
          <s:extension base="tns:BaseObject">
            <s:sequence>
              <s:element maxOccurs="1" minOccurs="0" name="Dimension" type="s:string"/>
              <s:element maxOccurs="1" minOccurs="0" name="Column" type="s:string"/>
            </s:sequence>
          </s:extension>
        </s:complexContent>
      </s:complexType>
      <s:complexType name="ArrayOfAggTransformation">
        <s:sequence>
          <s:element maxOccurs="unbounded" minOccurs="0" name="AggTransformation" nillable="true" type="tns:AggTransformation"/>
        </s:sequence>
      </s:complexType>
      <s:complexType name="AggTransformation">
        <s:sequence>
          <s:element maxOccurs="1" minOccurs="0" name="Dimension" type="s:string"/>
          <s:element maxOccurs="1" minOccurs="0" name="ColumnName" type="s:string"/>
          <s:element maxOccurs="1" minOccurs="0" name="Transformations" type="tns:ArrayOfChoice2"/>
        </s:sequence>
      </s:complexType>
      <s:complexType name="ArrayOfChoice2">
        <s:choice maxOccurs="unbounded" minOccurs="0">
          <s:element maxOccurs="1" minOccurs="1" name="Procedure" nillable="true" type="tns:Procedure"/>
          <s:element maxOccurs="1" minOccurs="1" name="TableLookup" nillable="true" type="tns:TableLookup"/>
          <s:element maxOccurs="1" minOccurs="1" name="LogicalColumnDefinition" nillable="true" type="tns:LogicalColumnDefinition"/>
          <s:element maxOccurs="1" minOccurs="1" name="Rank" nillable="true" type="tns:Rank"/>
          <s:element maxOccurs="1" minOccurs="1" name="Expression" nillable="true" type="tns:Expression"/>
        </s:choice>
      </s:complexType>
      <s:complexType name="Procedure">
        <s:complexContent mixed="false">
          <s:extension base="tns:Transformation">
            <s:sequence>
              <s:element maxOccurs="1" minOccurs="0" name="Name" type="s:string"/>
              <s:element maxOccurs="1" minOccurs="0" name="Body" type="s:string"/>
            </s:sequence>
          </s:extension>
        </s:complexContent>
      </s:complexType>
      <s:complexType name="LogicalColumnDefinition">
        <s:complexContent mixed="false">
          <s:extension base="tns:Transformation">
            <s:sequence>
              <s:element maxOccurs="1" minOccurs="0" name="Formula" type="s:string"/>
            </s:sequence>
          </s:extension>
        </s:complexContent>
      </s:complexType>
      <s:complexType name="ArrayOfAggregateOutput">
        <s:sequence>
          <s:element maxOccurs="unbounded" minOccurs="0" name="AggregateOutput" nillable="true" type="tns:AggregateOutput"/>
        </s:sequence>
      </s:complexType>
      <s:element name="getAggregatesResponse">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="getAggregatesResult" type="tns:ArrayOfAggregateOutput"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="updateCustomMeasure">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="le" type="tns:LogicalExpression"/>
            <s:element maxOccurs="1" minOccurs="0" name="grain" type="s:string"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="updateCustomMeasureResponse">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="updateCustomMeasureResult" type="s:string"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="updateCustomAttribute">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="le" type="tns:LogicalExpression"/>
            <s:element maxOccurs="1" minOccurs="0" name="level" type="s:string"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="updateCustomAttributeResponse">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="updateCustomAttributeResult" type="s:string"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="deleteLogicalExpression">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="le" type="tns:LogicalExpression"/>
            <s:element maxOccurs="1" minOccurs="0" name="grainorlevel" type="s:string"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="deleteLogicalExpressionResponse">
        <s:complexType/>
      </s:element>
      <s:element name="getBucketedMeasures">
        <s:complexType/>
      </s:element>
      <s:complexType name="ArrayOfMeasureBucket">
        <s:sequence>
          <s:element maxOccurs="unbounded" minOccurs="0" name="MeasureBucket" nillable="true" type="tns:MeasureBucket"/>
        </s:sequence>
      </s:complexType>
      <s:complexType name="MeasureBucket">
        <s:complexContent mixed="false">
          <s:extension base="tns:BaseObject">
            <s:sequence>
              <s:element maxOccurs="1" minOccurs="0" name="Name" type="s:string"/>
              <s:element maxOccurs="1" minOccurs="0" name="Min" type="s:string"/>
              <s:element maxOccurs="1" minOccurs="0" name="Max" type="s:string"/>
            </s:sequence>
          </s:extension>
        </s:complexContent>
      </s:complexType>
      <s:complexType name="ArrayOfVirtualColumn">
        <s:sequence>
          <s:element maxOccurs="unbounded" minOccurs="0" name="VirtualColumn" nillable="true" type="tns:VirtualColumn"/>
        </s:sequence>
      </s:complexType>
      <s:complexType name="VirtualColumn">
        <s:complexContent mixed="false">
          <s:extension base="tns:BaseObject">
            <s:sequence>
              <s:element maxOccurs="1" minOccurs="0" name="Name" type="s:string"/>
              <s:element maxOccurs="1" minOccurs="0" name="TableName" type="s:string"/>
              <s:element maxOccurs="1" minOccurs="0" name="Measure" type="s:string"/>
              <s:element maxOccurs="1" minOccurs="0" name="Filters" type="tns:ArrayOfString"/>
              <s:element maxOccurs="1" minOccurs="1" name="Type" type="s:int"/>
              <s:element maxOccurs="1" minOccurs="0" name="PivotDimension" type="s:string"/>
              <s:element maxOccurs="1" minOccurs="0" name="PivotLevel" type="s:string"/>
              <s:element maxOccurs="1" minOccurs="1" name="PivotPercent" type="s:boolean"/>
              <s:element maxOccurs="1" minOccurs="0" name="Buckets" type="tns:ArrayOfMeasureBucket"/>
              <s:element maxOccurs="1" minOccurs="0" name="measureBucketDefaultName" type="s:string"/>
              <s:element maxOccurs="1" minOccurs="0" name="measureBucketNullName" type="s:string"/>
            </s:sequence>
          </s:extension>
        </s:complexContent>
      </s:complexType>
      <s:element name="getBucketedMeasuresResponse">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="getBucketedMeasuresResult" type="tns:ArrayOfVirtualColumn"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="updateBucketedMeasure">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="vc" type="tns:VirtualColumn"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="updateBucketedMeasureResponse">
        <s:complexType/>
      </s:element>
      <s:element name="deleteBucketedMeasure">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="name" type="s:string"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="deleteBucketedMeasureResponse">
        <s:complexType/>
      </s:element>
      <s:element name="getMeasureList">
        <s:complexType/>
      </s:element>
      <s:element name="getMeasureListResponse">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="getMeasureListResult" type="tns:ArrayOfString"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="getDataTables">
        <s:complexType/>
      </s:element>
      <s:complexType name="ViewProcessedResults">
        <s:sequence>
          <s:element maxOccurs="1" minOccurs="0" name="ErrorMessage" type="s:string"/>
          <s:element maxOccurs="1" minOccurs="0" name="Results" type="tns:ArrayOfArrayOfString"/>
        </s:sequence>
      </s:complexType>
      <s:element name="getDataTablesResponse">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="getDataTablesResult" type="tns:ViewProcessedResults"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="getLevelData">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="name" type="s:string"/>
            <s:element maxOccurs="1" minOccurs="1" name="profile" type="s:boolean"/>
            <s:element maxOccurs="1" minOccurs="0" name="query" type="s:string"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="getLevelDataResponse">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="getLevelDataResult" type="tns:ViewProcessedResults"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="getMeasureTableData">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="name" type="s:string"/>
            <s:element maxOccurs="1" minOccurs="1" name="profile" type="s:boolean"/>
            <s:element maxOccurs="1" minOccurs="0" name="query" type="s:string"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="getMeasureTableDataResponse">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="getMeasureTableDataResult" type="tns:ViewProcessedResults"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="getStagingTableData">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="name" type="s:string"/>
            <s:element maxOccurs="1" minOccurs="1" name="profile" type="s:boolean"/>
            <s:element maxOccurs="1" minOccurs="0" name="query" type="s:string"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="getStagingTableDataResponse">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="getStagingTableDataResult" type="tns:ViewProcessedResults"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="getTableQueryString">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="name" type="s:string"/>
            <s:element maxOccurs="1" minOccurs="0" name="type" type="s:string"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="getTableQueryStringResponse">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="getTableQueryStringResult" type="s:string"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="getGrainList">
        <s:complexType/>
      </s:element>
      <s:element name="getGrainListResponse">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="getGrainListResult" type="tns:ArrayOfString"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="getGrainColumnList">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="grain" type="s:string"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="getGrainColumnListResponse">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="getGrainColumnListResult" type="tns:ArrayOfString"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="getLevelList">
        <s:complexType/>
      </s:element>
      <s:element name="getLevelListResponse">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="getLevelListResult" type="tns:ArrayOfString"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="getLevelColumnList">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="levelname" type="s:string"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="getLevelColumnListResponse">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="getLevelColumnListResult" type="tns:ArrayOfString"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="getColumnsAtLowerGrains">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="sourceName" type="s:string"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="getColumnsAtLowerGrainsResponse">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="getColumnsAtLowerGrainsResult" type="tns:ArrayOfArrayOfString"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="ping">
        <s:complexType/>
      </s:element>
      <s:element name="pingResponse">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="1" name="pingResult" type="s:boolean"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetProcessInitData">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="1" name="automode" type="s:boolean"/>
            <s:element maxOccurs="1" minOccurs="0" name="loadGroup" type="s:string"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:complexType name="ProcessOptions">
        <s:sequence>
          <s:element maxOccurs="1" minOccurs="0" name="LoadNumberProcessed" type="s:string"/>
          <s:element maxOccurs="1" minOccurs="1" name="CreateDashboards" type="s:boolean"/>
          <s:element maxOccurs="1" minOccurs="1" name="RecreateDashboards" type="s:boolean"/>
          <s:element maxOccurs="1" minOccurs="0" name="ProcessDate" type="s:string"/>
          <s:element maxOccurs="1" minOccurs="0" name="CurrentState" type="s:string"/>
          <s:element maxOccurs="1" minOccurs="0" name="ErrorOutput" type="tns:ErrorOutput"/>
          <s:element maxOccurs="1" minOccurs="0" name="RedirectModule" type="s:string"/>
          <s:element maxOccurs="1" minOccurs="0" name="progressPercentage" type="s:string"/>
          <s:element maxOccurs="1" minOccurs="0" name="progressText" type="s:string"/>
          <s:element maxOccurs="1" minOccurs="0" name="DetailLink" type="s:string"/>
        </s:sequence>
      </s:complexType>
      <s:element name="GetProcessInitDataResponse">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="GetProcessInitDataResult" type="tns:ProcessOptions"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="GetProgress">
        <s:complexType/>
      </s:element>
      <s:element name="GetProgressResponse">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="GetProgressResult" type="tns:ProcessOptions"/>
          </s:sequence>
        </s:complexType>
      </s:element>
    </s:schema>
    <s:schema elementFormDefault="qualified" targetNamespace="http://trustedservice.WebServices.successmetricsinc.com/xsd">
      <s:complexType name="ValidateResult">
        <s:sequence>
          <s:element maxOccurs="1" minOccurs="0" name="errorCode" type="s:int"/>
          <s:element maxOccurs="1" minOccurs="1" name="errorMessage" nillable="true" type="s:string"/>
          <s:element maxOccurs="1" minOccurs="1" name="inputErrorMessage" nillable="true" type="s:string"/>
          <s:element maxOccurs="1" minOccurs="0" name="inputLine" type="s:int"/>
          <s:element maxOccurs="1" minOccurs="0" name="inputStart" type="s:int"/>
          <s:element maxOccurs="1" minOccurs="0" name="inputStop" type="s:int"/>
          <s:element maxOccurs="1" minOccurs="1" name="loadGroups" nillable="true" type="s:string"/>
          <s:element maxOccurs="1" minOccurs="1" name="outputErrorMessage" nillable="true" type="s:string"/>
          <s:element maxOccurs="1" minOccurs="0" name="outputLine" type="s:int"/>
          <s:element maxOccurs="1" minOccurs="0" name="outputStart" type="s:int"/>
          <s:element maxOccurs="1" minOccurs="0" name="outputStop" type="s:int"/>
          <s:element maxOccurs="1" minOccurs="1" name="scriptErrorMessage" nillable="true" type="s:string"/>
          <s:element maxOccurs="1" minOccurs="0" name="scriptLine" type="s:int"/>
          <s:element maxOccurs="1" minOccurs="0" name="scriptStart" type="s:int"/>
          <s:element maxOccurs="1" minOccurs="0" name="scriptStop" type="s:int"/>
          <s:element maxOccurs="1" minOccurs="0" name="success" type="s:boolean"/>
        </s:sequence>
      </s:complexType>
      <s:complexType name="ExecuteResult">
        <s:sequence>
          <s:element maxOccurs="1" minOccurs="0" name="errorCode" type="s:int"/>
          <s:element maxOccurs="1" minOccurs="1" name="errorMessage" nillable="true" type="s:string"/>
          <s:element maxOccurs="unbounded" minOccurs="0" name="groups" nillable="true" type="s:string"/>
          <s:element maxOccurs="1" minOccurs="0" name="numInputRows" type="s:long"/>
          <s:element maxOccurs="1" minOccurs="0" name="numOutputRows" type="s:long"/>
          <s:element maxOccurs="1" minOccurs="1" name="part" nillable="true" type="s:string"/>
          <s:element maxOccurs="1" minOccurs="0" name="success" type="s:boolean"/>
        </s:sequence>
      </s:complexType>
    </s:schema>
  </wsdl:types>
  <wsdl:message name="updateCustomAttributeSoapIn">
    <wsdl:part element="tns:updateCustomAttribute" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="CanDeleteLastSoapIn">
    <wsdl:part element="tns:CanDeleteLast" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="removeDataSourceSoapOut">
    <wsdl:part element="tns:removeDataSourceResponse" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="getScheduledReportsSoapIn">
    <wsdl:part element="tns:getScheduledReports" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="UpdateAdminAccessSoapIn">
    <wsdl:part element="tns:UpdateAdminAccess" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="exportDataSoapOut">
    <wsdl:part element="tns:exportDataResponse" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="unlockUsersSoapIn">
    <wsdl:part element="tns:unlockUsers" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="exportDataSoapIn">
    <wsdl:part element="tns:exportData" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="ConfigureDashboardsSoapIn">
    <wsdl:part element="tns:ConfigureDashboards" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="processCreateUserFileSoapOut">
    <wsdl:part element="tns:processCreateUserFileResponse" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="getLevelDataSoapOut">
    <wsdl:part element="tns:getLevelDataResponse" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="DeleteSpaceSoapIn">
    <wsdl:part element="tns:DeleteSpace" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="getColumnsAtLowerGrainsSoapIn">
    <wsdl:part element="tns:getColumnsAtLowerGrains" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="addScheduledReportSoapIn">
    <wsdl:part element="tns:addScheduledReport" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="getTableQueryStringSoapOut">
    <wsdl:part element="tns:getTableQueryStringResponse" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="getDataTablesSoapOut">
    <wsdl:part element="tns:getDataTablesResponse" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="GetSpaceDetailsSoapOut">
    <wsdl:part element="tns:GetSpaceDetailsResponse" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="deleteVariableSoapIn">
    <wsdl:part element="tns:deleteVariable" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="CreateNewTemplateSoapIn">
    <wsdl:part element="tns:CreateNewTemplate" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="addBirstConnectConfigSoapOut">
    <wsdl:part element="tns:addBirstConnectConfigResponse" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="getTableSchemaSoapIn">
    <wsdl:part element="tns:getTableSchema" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="saveDataSourceSoapOut">
    <wsdl:part element="tns:saveDataSourceResponse" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="GetAttachmentListSoapOut">
    <wsdl:part element="tns:GetAttachmentListResponse" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="AssignDynamicGroupsSoapOut">
    <wsdl:part element="tns:AssignDynamicGroupsResponse" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="ModifySpacePropertiesSoapOut">
    <wsdl:part element="tns:ModifySpacePropertiesResponse" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="saveHierarchySoapIn">
    <wsdl:part element="tns:saveHierarchy" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="addBirstConnectConfigSoapIn">
    <wsdl:part element="tns:addBirstConnectConfig" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="SetRepositorySoapOut">
    <wsdl:part element="tns:SetRepositoryResponse" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="getSpacesToManageSoapOut">
    <wsdl:part element="tns:getSpacesToManageResponse" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="GetProgressSoapIn">
    <wsdl:part element="tns:GetProgress" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="getScheduledReportSoapOut">
    <wsdl:part element="tns:getScheduledReportResponse" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="CancelNextStageSoapIn">
    <wsdl:part element="tns:CancelNextStage" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="processCreateUserFileSoapIn">
    <wsdl:part element="tns:processCreateUserFile" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="getGrainListSoapOut">
    <wsdl:part element="tns:getGrainListResponse" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="updateAccountAdminPrivelegesSoapIn">
    <wsdl:part element="tns:updateAccountAdminPriveleges" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="GetPublishLogDetailsSoapIn">
    <wsdl:part element="tns:GetPublishLogDetails" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="GetMAFGroupsSoapIn">
    <wsdl:part element="tns:GetMAFGroups" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="getAggregatesSoapIn">
    <wsdl:part element="tns:getAggregates" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="GetGroupDetailsSoapIn">
    <wsdl:part element="tns:GetGroupDetails" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="getMeasureTableDataSoapOut">
    <wsdl:part element="tns:getMeasureTableDataResponse" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="getBucketedMeasuresSoapIn">
    <wsdl:part element="tns:getBucketedMeasures" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="ProcessUploadedFileSoapIn">
    <wsdl:part element="tns:ProcessUploadedFile" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="CreateNewSpaceSoapIn">
    <wsdl:part element="tns:CreateNewSpace" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="executeCommandSoapIn">
    <wsdl:part element="tns:executeCommand" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="GetSpaceDetailsSoapIn">
    <wsdl:part element="tns:GetSpaceDetails" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="GetAttachmentListSoapIn">
    <wsdl:part element="tns:GetAttachmentList" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="deleteBucketedMeasureSoapIn">
    <wsdl:part element="tns:deleteBucketedMeasure" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="getLogicalExpressionsSoapIn">
    <wsdl:part element="tns:getLogicalExpressions" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="deleteVariableSoapOut">
    <wsdl:part element="tns:deleteVariableResponse" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="CreateNewTemplateSoapOut">
    <wsdl:part element="tns:CreateNewTemplateResponse" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="getLevelListSoapIn">
    <wsdl:part element="tns:getLevelList" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="CompleteGroupManagementSoapIn">
    <wsdl:part element="tns:CompleteGroupManagement" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="updateScheduledReportSoapOut">
    <wsdl:part element="tns:updateScheduledReportResponse" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="GetAllSpacesSoapIn">
    <wsdl:part element="tns:GetAllSpaces" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="saveLevelSoapOut">
    <wsdl:part element="tns:saveLevelResponse" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="saveDataSourceAndColumnsSoapOut">
    <wsdl:part element="tns:saveDataSourceAndColumnsResponse" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="renameStagingColumnSoapIn">
    <wsdl:part element="tns:renameStagingColumn" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="addUserToSpaceSoapOut">
    <wsdl:part element="tns:addUserToSpaceResponse" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="DeleteAllSoapIn">
    <wsdl:part element="tns:DeleteAll" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="ModifySpacePropertiesSoapIn">
    <wsdl:part element="tns:ModifySpaceProperties" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="deleteBirstConnectConfigSoapOut">
    <wsdl:part element="tns:deleteBirstConnectConfigResponse" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="GetGroupDetailsSoapOut">
    <wsdl:part element="tns:GetGroupDetailsResponse" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="removeStagingColumnSoapOut">
    <wsdl:part element="tns:removeStagingColumnResponse" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="getLevelColumnListSoapOut">
    <wsdl:part element="tns:getLevelColumnListResponse" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="SaveGroupSoapIn">
    <wsdl:part element="tns:SaveGroup" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="runScheduledReportSoapOut">
    <wsdl:part element="tns:runScheduledReportResponse" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="updateBucketedMeasureSoapOut">
    <wsdl:part element="tns:updateBucketedMeasureResponse" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="updateCustomMeasureSoapOut">
    <wsdl:part element="tns:updateCustomMeasureResponse" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="ConfigureDashboardsSoapOut">
    <wsdl:part element="tns:ConfigureDashboardsResponse" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="GetSourceDetailsSoapIn">
    <wsdl:part element="tns:GetSourceDetails" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="getCubeSoapOut">
    <wsdl:part element="tns:getCubeResponse" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="getTimeZonesSoapIn">
    <wsdl:part element="tns:getTimeZones" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="getExportedDataFilesSoapIn">
    <wsdl:part element="tns:getExportedDataFiles" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="validateQuerySoapOut">
    <wsdl:part element="tns:validateQueryResponse" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="updateCustomAttributeSoapOut">
    <wsdl:part element="tns:updateCustomAttributeResponse" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="getAccountSummarySoapIn">
    <wsdl:part element="tns:getAccountSummary" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="GetSpaceShareDetailsSoapOut">
    <wsdl:part element="tns:GetSpaceShareDetailsResponse" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="SaveGroupSoapOut">
    <wsdl:part element="tns:SaveGroupResponse" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="saveHierarchySoapOut">
    <wsdl:part element="tns:saveHierarchyResponse" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="EmptyDataSourceSoapOut">
    <wsdl:part element="tns:EmptyDataSourceResponse" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="ProcessNextStageSoapOut">
    <wsdl:part element="tns:ProcessNextStageResponse" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="getVariablesSoapIn">
    <wsdl:part element="tns:getVariables" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="publishSoapIn">
    <wsdl:part element="tns:publish" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="saveAndValidateScriptSoapOut">
    <wsdl:part element="tns:saveAndValidateScriptResponse" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="GetRepositorySoapIn">
    <wsdl:part element="tns:GetRepository" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="CreateSpaceCopySoapOut">
    <wsdl:part element="tns:CreateSpaceCopyResponse" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="getScheduledReportSoapIn">
    <wsdl:part element="tns:getScheduledReport" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="GenerateSSOCredentialsSoapOut">
    <wsdl:part element="tns:GenerateSSOCredentialsResponse" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="deleteBirstConnectConfigSoapIn">
    <wsdl:part element="tns:deleteBirstConnectConfig" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="deleteLogicalExpressionSoapIn">
    <wsdl:part element="tns:deleteLogicalExpression" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="DeleteOverviewSoapOut">
    <wsdl:part element="tns:DeleteOverviewResponse" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="removeDataSourceSoapIn">
    <wsdl:part element="tns:removeDataSource" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="AddNewGroupSoapOut">
    <wsdl:part element="tns:AddNewGroupResponse" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="swapContentWithSpaceSoapIn">
    <wsdl:part element="tns:swapContentWithSpace" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="updatePasswordSoapOut">
    <wsdl:part element="tns:updatePasswordResponse" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="GetSpaceShareDetailsSoapIn">
    <wsdl:part element="tns:GetSpaceShareDetails" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="getCubesSoapIn">
    <wsdl:part element="tns:getCubes" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="GetAllSpacesSoapOut">
    <wsdl:part element="tns:GetAllSpacesResponse" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="ProcessNextStageSoapIn">
    <wsdl:part element="tns:ProcessNextStage" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="GetCurrentSessionSoapOut">
    <wsdl:part element="tns:GetCurrentSessionResponse" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="getSchemaTablesSoapOut">
    <wsdl:part element="tns:getSchemaTablesResponse" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="AddNewGroupSoapIn">
    <wsdl:part element="tns:AddNewGroup" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="getExportedDataFilesSoapOut">
    <wsdl:part element="tns:getExportedDataFilesResponse" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="GetRepositorySoapOut">
    <wsdl:part element="tns:GetRepositoryResponse" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="DeleteTemplateSoapIn">
    <wsdl:part element="tns:DeleteTemplate" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="getRawDataSoapOut">
    <wsdl:part element="tns:getRawDataResponse" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="getRawDataSoapIn">
    <wsdl:part element="tns:getRawData" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="renameBirstConnectConfigSoapOut">
    <wsdl:part element="tns:renameBirstConnectConfigResponse" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="getUsersToManageSoapIn">
    <wsdl:part element="tns:getUsersToManage" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="GetFileDetailsSoapIn">
    <wsdl:part element="tns:GetFileDetails" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="createAccountSoapOut">
    <wsdl:part element="tns:createAccountResponse" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="addUserToSpaceSoapIn">
    <wsdl:part element="tns:addUserToSpace" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="getLevelColumnListSoapIn">
    <wsdl:part element="tns:getLevelColumnList" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="removeStagingColumnSoapIn">
    <wsdl:part element="tns:removeStagingColumn" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="GetProgressSoapOut">
    <wsdl:part element="tns:GetProgressResponse" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="GetPublishLogDetailsSoapOut">
    <wsdl:part element="tns:GetPublishLogDetailsResponse" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="CanDeleteLastSoapOut">
    <wsdl:part element="tns:CanDeleteLastResponse" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="updateUserStatusSoapIn">
    <wsdl:part element="tns:updateUserStatus" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="isConnectedSoapOut">
    <wsdl:part element="tns:isConnectedResponse" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="GetQueryDetailsSoapOut">
    <wsdl:part element="tns:GetQueryDetailsResponse" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="GetTemplateCategoriesSoapOut">
    <wsdl:part element="tns:GetTemplateCategoriesResponse" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="removeHierarchySoapOut">
    <wsdl:part element="tns:removeHierarchyResponse" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="saveLevelSoapIn">
    <wsdl:part element="tns:saveLevel" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="AutoPublishSoapIn">
    <wsdl:part element="tns:AutoPublish" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="GetSourcesSoapOut">
    <wsdl:part element="tns:GetSourcesResponse" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="GetSpacePropertiesSoapIn">
    <wsdl:part element="tns:GetSpaceProperties" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="saveAndValidateScriptSoapIn">
    <wsdl:part element="tns:saveAndValidateScript" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="updatePasswordSoapIn">
    <wsdl:part element="tns:updatePassword" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="updateUserStatusSoapOut">
    <wsdl:part element="tns:updateUserStatusResponse" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="RemoveAttachmentSoapIn">
    <wsdl:part element="tns:RemoveAttachment" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="GetFileDetailsSoapOut">
    <wsdl:part element="tns:GetFileDetailsResponse" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="deleteBucketedMeasureSoapOut">
    <wsdl:part element="tns:deleteBucketedMeasureResponse" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="updateBucketedMeasureSoapIn">
    <wsdl:part element="tns:updateBucketedMeasure" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="AutoPublishSoapOut">
    <wsdl:part element="tns:AutoPublishResponse" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="GetLoggedInSpaceDetailsSoapIn">
    <wsdl:part element="tns:GetLoggedInSpaceDetails" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="getLoggedInUserDetailsSoapOut">
    <wsdl:part element="tns:getLoggedInUserDetailsResponse" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="getCreateUserAllowedDomainsSoapIn">
    <wsdl:part element="tns:getCreateUserAllowedDomains" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="GetSpacePropertiesSoapOut">
    <wsdl:part element="tns:GetSpacePropertiesResponse" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="pingSoapIn">
    <wsdl:part element="tns:ping" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="GetDynamicGroupsSoapOut">
    <wsdl:part element="tns:GetDynamicGroupsResponse" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="renameBirstConnectConfigSoapIn">
    <wsdl:part element="tns:renameBirstConnectConfig" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="CreateSpaceCopySoapIn">
    <wsdl:part element="tns:CreateSpaceCopy" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="addScheduledReportSoapOut">
    <wsdl:part element="tns:addScheduledReportResponse" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="updateLogicalModelSoapIn">
    <wsdl:part element="tns:updateLogicalModel" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="GetProcessInitDataSoapIn">
    <wsdl:part element="tns:GetProcessInitData" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="publishSoapOut">
    <wsdl:part element="tns:publishResponse" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="saveStagingColumnSoapIn">
    <wsdl:part element="tns:saveStagingColumn" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="GetSourcesSoapIn">
    <wsdl:part element="tns:GetSources" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="deleteLogicalExpressionSoapOut">
    <wsdl:part element="tns:deleteLogicalExpressionResponse" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="GetTemplatesSoapIn">
    <wsdl:part element="tns:GetTemplates" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="getAggregatesSoapOut">
    <wsdl:part element="tns:getAggregatesResponse" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="GetPublishHistorySoapOut">
    <wsdl:part element="tns:GetPublishHistoryResponse" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="GenerateSSOCredentialsSoapIn">
    <wsdl:part element="tns:GenerateSSOCredentials" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="getDataTablesSoapIn">
    <wsdl:part element="tns:getDataTables" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="GetQueryDetailsSoapIn">
    <wsdl:part element="tns:GetQueryDetails" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="RemoveAccessSoapIn">
    <wsdl:part element="tns:RemoveAccess" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="getNumAccountUsersSoapOut">
    <wsdl:part element="tns:getNumAccountUsersResponse" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="getLoggedInUserDetailsSoapIn">
    <wsdl:part element="tns:getLoggedInUserDetails" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="renameStagingColumnSoapOut">
    <wsdl:part element="tns:renameStagingColumnResponse" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="GetDynamicGroupsSoapIn">
    <wsdl:part element="tns:GetDynamicGroups" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="GetPublishHistorySoapIn">
    <wsdl:part element="tns:GetPublishHistory" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="getStagingTableDataSoapOut">
    <wsdl:part element="tns:getStagingTableDataResponse" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="deleteScheduledReportSoapIn">
    <wsdl:part element="tns:deleteScheduledReport" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="getSpacesToManageSoapIn">
    <wsdl:part element="tns:getSpacesToManage" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="getScheduledReportsSoapOut">
    <wsdl:part element="tns:getScheduledReportsResponse" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="runScheduledReportSoapIn">
    <wsdl:part element="tns:runScheduledReport" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="ResetSpaceSettingsSoapOut">
    <wsdl:part element="tns:ResetSpaceSettingsResponse" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="CreateNewSpaceSoapOut">
    <wsdl:part element="tns:CreateNewSpaceResponse" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="RemoveMemberSoapOut">
    <wsdl:part element="tns:RemoveMemberResponse" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="EmptyDataSourceSoapIn">
    <wsdl:part element="tns:EmptyDataSource" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="DeleteTemplateSoapOut">
    <wsdl:part element="tns:DeleteTemplateResponse" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="unlockUsersSoapOut">
    <wsdl:part element="tns:unlockUsersResponse" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="getStagingTableDataSoapIn">
    <wsdl:part element="tns:getStagingTableData" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="DeleteLastSoapOut">
    <wsdl:part element="tns:DeleteLastResponse" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="GetTemplateCategoriesSoapIn">
    <wsdl:part element="tns:GetTemplateCategories" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="GetLoggedInSpaceDetailsSoapOut">
    <wsdl:part element="tns:GetLoggedInSpaceDetailsResponse" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="GetCurrentSessionSoapIn">
    <wsdl:part element="tns:GetCurrentSession" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="GetMAFGroupsSoapOut">
    <wsdl:part element="tns:GetMAFGroupsResponse" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="SendInviteSoapIn">
    <wsdl:part element="tns:SendInvite" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="GetProcessInitDataSoapOut">
    <wsdl:part element="tns:GetProcessInitDataResponse" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="updateScheduledReportSoapIn">
    <wsdl:part element="tns:updateScheduledReport" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="DeleteInviteSoapOut">
    <wsdl:part element="tns:DeleteInviteResponse" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="saveRepositorySoapOut">
    <wsdl:part element="tns:saveRepositoryResponse" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="RemoveMemberSoapIn">
    <wsdl:part element="tns:RemoveMember" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="UpdateAdminAccessSoapOut">
    <wsdl:part element="tns:UpdateAdminAccessResponse" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="executeCommandSoapOut">
    <wsdl:part element="tns:executeCommandResponse" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="swapContentWithSpaceSoapOut">
    <wsdl:part element="tns:swapContentWithSpaceResponse" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="getBucketedMeasuresSoapOut">
    <wsdl:part element="tns:getBucketedMeasuresResponse" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="saveStagingColumnSoapOut">
    <wsdl:part element="tns:saveStagingColumnResponse" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="RemoveAttachmentSoapOut">
    <wsdl:part element="tns:RemoveAttachmentResponse" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="getAccountSummarySoapOut">
    <wsdl:part element="tns:getAccountSummaryResponse" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="getCubeSoapIn">
    <wsdl:part element="tns:getCube" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="getTableQueryStringSoapIn">
    <wsdl:part element="tns:getTableQueryString" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="SendInviteSoapOut">
    <wsdl:part element="tns:SendInviteResponse" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="SendInvitationResponseSoapOut">
    <wsdl:part element="tns:SendInvitationResponseResponse" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="pingSoapOut">
    <wsdl:part element="tns:pingResponse" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="saveRepositorySoapIn">
    <wsdl:part element="tns:saveRepository" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="updateVariableSoapOut">
    <wsdl:part element="tns:updateVariableResponse" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="getLogicalExpressionsSoapOut">
    <wsdl:part element="tns:getLogicalExpressionsResponse" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="CompleteGroupManagementSoapOut">
    <wsdl:part element="tns:CompleteGroupManagementResponse" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="updateAccountAdminPrivelegesSoapOut">
    <wsdl:part element="tns:updateAccountAdminPrivelegesResponse" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="getSchemaTablesSoapIn">
    <wsdl:part element="tns:getSchemaTables" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="removeHierarchySoapIn">
    <wsdl:part element="tns:removeHierarchy" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="DeleteSpaceSoapOut">
    <wsdl:part element="tns:DeleteSpaceResponse" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="DeleteGroupSoapOut">
    <wsdl:part element="tns:DeleteGroupResponse" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="createSMIWebLoginSoapOut">
    <wsdl:part element="tns:createSMIWebLoginResponse" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="getGrainListSoapIn">
    <wsdl:part element="tns:getGrainList" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="ResetSpaceSettingsSoapIn">
    <wsdl:part element="tns:ResetSpaceSettings" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="ProcessUploadedFileSoapOut">
    <wsdl:part element="tns:ProcessUploadedFileResponse" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="getLevelDataSoapIn">
    <wsdl:part element="tns:getLevelData" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="SendInvitationResponseSoapIn">
    <wsdl:part element="tns:SendInvitationResponse" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="PublishWithDashboardsSoapOut">
    <wsdl:part element="tns:PublishWithDashboardsResponse" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="getGrainColumnListSoapIn">
    <wsdl:part element="tns:getGrainColumnList" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="GetSourceDetailsSoapOut">
    <wsdl:part element="tns:GetSourceDetailsResponse" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="deleteScheduledReportSoapOut">
    <wsdl:part element="tns:deleteScheduledReportResponse" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="getColumnsAtLowerGrainsSoapOut">
    <wsdl:part element="tns:getColumnsAtLowerGrainsResponse" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="getNumAccountUsersSoapIn">
    <wsdl:part element="tns:getNumAccountUsers" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="createAccountSoapIn">
    <wsdl:part element="tns:createAccount" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="CancelNextStageSoapOut">
    <wsdl:part element="tns:CancelNextStageResponse" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="validateQuerySoapIn">
    <wsdl:part element="tns:validateQuery" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="DeleteOverviewSoapIn">
    <wsdl:part element="tns:DeleteOverview" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="PublishWithDashboardsSoapIn">
    <wsdl:part element="tns:PublishWithDashboards" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="ConvertSpaceSoapOut">
    <wsdl:part element="tns:ConvertSpaceResponse" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="updateCustomMeasureSoapIn">
    <wsdl:part element="tns:updateCustomMeasure" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="GetTemplatesSoapOut">
    <wsdl:part element="tns:GetTemplatesResponse" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="getUsersToManageSoapOut">
    <wsdl:part element="tns:getUsersToManageResponse" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="getCreateUserAllowedDomainsSoapOut">
    <wsdl:part element="tns:getCreateUserAllowedDomainsResponse" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="DeleteLastSoapIn">
    <wsdl:part element="tns:DeleteLast" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="ConvertSpaceSoapIn">
    <wsdl:part element="tns:ConvertSpace" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="updateLogicalModelSoapOut">
    <wsdl:part element="tns:updateLogicalModelResponse" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="SetRepositorySoapIn">
    <wsdl:part element="tns:SetRepository" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="getMeasureListSoapOut">
    <wsdl:part element="tns:getMeasureListResponse" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="DeleteInviteSoapIn">
    <wsdl:part element="tns:DeleteInvite" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="isConnectedSoapIn">
    <wsdl:part element="tns:isConnected" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="saveDataSourceSoapIn">
    <wsdl:part element="tns:saveDataSource" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="getBirstConnectConfigsSoapIn">
    <wsdl:part element="tns:getBirstConnectConfigs" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="getMeasureTableDataSoapIn">
    <wsdl:part element="tns:getMeasureTableData" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="executeScriptSoapOut">
    <wsdl:part element="tns:executeScriptResponse" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="getReportListSoapOut">
    <wsdl:part element="tns:getReportListResponse" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="RemoveAccessSoapOut">
    <wsdl:part element="tns:RemoveAccessResponse" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="createSMIWebLoginSoapIn">
    <wsdl:part element="tns:createSMIWebLogin" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="saveDataSourceAndColumnsSoapIn">
    <wsdl:part element="tns:saveDataSourceAndColumns" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="getGrainColumnListSoapOut">
    <wsdl:part element="tns:getGrainColumnListResponse" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="AssignDynamicGroupsSoapIn">
    <wsdl:part element="tns:AssignDynamicGroups" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="updateVariableSoapIn">
    <wsdl:part element="tns:updateVariable" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="getMeasureListSoapIn">
    <wsdl:part element="tns:getMeasureList" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="executeScriptSoapIn">
    <wsdl:part element="tns:executeScript" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="getLevelListSoapOut">
    <wsdl:part element="tns:getLevelListResponse" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="GetAttachmentProcessOuputSoapIn">
    <wsdl:part element="tns:GetAttachmentProcessOuput" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="getTimeZonesSoapOut">
    <wsdl:part element="tns:getTimeZonesResponse" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="DeleteGroupSoapIn">
    <wsdl:part element="tns:DeleteGroup" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="getCubesSoapOut">
    <wsdl:part element="tns:getCubesResponse" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="getBirstConnectConfigsSoapOut">
    <wsdl:part element="tns:getBirstConnectConfigsResponse" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="getVariablesSoapOut">
    <wsdl:part element="tns:getVariablesResponse" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="DeleteAllSoapOut">
    <wsdl:part element="tns:DeleteAllResponse" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="GetAttachmentProcessOuputSoapOut">
    <wsdl:part element="tns:GetAttachmentProcessOuputResponse" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="getReportListSoapIn">
    <wsdl:part element="tns:getReportList" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="getTableSchemaSoapOut">
    <wsdl:part element="tns:getTableSchemaResponse" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:portType name="AdminServiceSoap">
    <wsdl:operation name="getCreateUserAllowedDomains">
      <wsdl:input message="tns:getCreateUserAllowedDomainsSoapIn">
    </wsdl:input>
      <wsdl:output message="tns:getCreateUserAllowedDomainsSoapOut">
    </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="createAccount">
      <wsdl:input message="tns:createAccountSoapIn">
    </wsdl:input>
      <wsdl:output message="tns:createAccountSoapOut">
    </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getLoggedInUserDetails">
      <wsdl:input message="tns:getLoggedInUserDetailsSoapIn">
    </wsdl:input>
      <wsdl:output message="tns:getLoggedInUserDetailsSoapOut">
    </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="publish">
      <wsdl:input message="tns:publishSoapIn">
    </wsdl:input>
      <wsdl:output message="tns:publishSoapOut">
    </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="ConfigureDashboards">
      <wsdl:input message="tns:ConfigureDashboardsSoapIn">
    </wsdl:input>
      <wsdl:output message="tns:ConfigureDashboardsSoapOut">
    </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="PublishWithDashboards">
      <wsdl:input message="tns:PublishWithDashboardsSoapIn">
    </wsdl:input>
      <wsdl:output message="tns:PublishWithDashboardsSoapOut">
    </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetPublishHistory">
      <wsdl:input message="tns:GetPublishHistorySoapIn">
    </wsdl:input>
      <wsdl:output message="tns:GetPublishHistorySoapOut">
    </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="CanDeleteLast">
      <wsdl:input message="tns:CanDeleteLastSoapIn">
    </wsdl:input>
      <wsdl:output message="tns:CanDeleteLastSoapOut">
    </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="DeleteLast">
      <wsdl:input message="tns:DeleteLastSoapIn">
    </wsdl:input>
      <wsdl:output message="tns:DeleteLastSoapOut">
    </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="DeleteAll">
      <wsdl:input message="tns:DeleteAllSoapIn">
    </wsdl:input>
      <wsdl:output message="tns:DeleteAllSoapOut">
    </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="DeleteSpace">
      <wsdl:input message="tns:DeleteSpaceSoapIn">
    </wsdl:input>
      <wsdl:output message="tns:DeleteSpaceSoapOut">
    </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="DeleteOverview">
      <wsdl:input message="tns:DeleteOverviewSoapIn">
    </wsdl:input>
      <wsdl:output message="tns:DeleteOverviewSoapOut">
    </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetTemplateCategories">
      <wsdl:input message="tns:GetTemplateCategoriesSoapIn">
    </wsdl:input>
      <wsdl:output message="tns:GetTemplateCategoriesSoapOut">
    </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetTemplates">
      <wsdl:input message="tns:GetTemplatesSoapIn">
    </wsdl:input>
      <wsdl:output message="tns:GetTemplatesSoapOut">
    </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetAttachmentList">
      <wsdl:input message="tns:GetAttachmentListSoapIn">
    </wsdl:input>
      <wsdl:output message="tns:GetAttachmentListSoapOut">
    </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetAttachmentProcessOuput">
      <wsdl:input message="tns:GetAttachmentProcessOuputSoapIn">
    </wsdl:input>
      <wsdl:output message="tns:GetAttachmentProcessOuputSoapOut">
    </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetCurrentSession">
      <wsdl:input message="tns:GetCurrentSessionSoapIn">
    </wsdl:input>
      <wsdl:output message="tns:GetCurrentSessionSoapOut">
    </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="CreateNewTemplate">
      <wsdl:input message="tns:CreateNewTemplateSoapIn">
    </wsdl:input>
      <wsdl:output message="tns:CreateNewTemplateSoapOut">
    </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="RemoveAttachment">
      <wsdl:input message="tns:RemoveAttachmentSoapIn">
    </wsdl:input>
      <wsdl:output message="tns:RemoveAttachmentSoapOut">
    </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GenerateSSOCredentials">
      <wsdl:input message="tns:GenerateSSOCredentialsSoapIn">
    </wsdl:input>
      <wsdl:output message="tns:GenerateSSOCredentialsSoapOut">
    </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="ConvertSpace">
      <wsdl:input message="tns:ConvertSpaceSoapIn">
    </wsdl:input>
      <wsdl:output message="tns:ConvertSpaceSoapOut">
    </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="ResetSpaceSettings">
      <wsdl:input message="tns:ResetSpaceSettingsSoapIn">
    </wsdl:input>
      <wsdl:output message="tns:ResetSpaceSettingsSoapOut">
    </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetSpaceProperties">
      <wsdl:input message="tns:GetSpacePropertiesSoapIn">
    </wsdl:input>
      <wsdl:output message="tns:GetSpacePropertiesSoapOut">
    </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="ModifySpaceProperties">
      <wsdl:input message="tns:ModifySpacePropertiesSoapIn">
    </wsdl:input>
      <wsdl:output message="tns:ModifySpacePropertiesSoapOut">
    </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="ProcessUploadedFile">
      <wsdl:input message="tns:ProcessUploadedFileSoapIn">
    </wsdl:input>
      <wsdl:output message="tns:ProcessUploadedFileSoapOut">
    </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="ProcessNextStage">
      <wsdl:input message="tns:ProcessNextStageSoapIn">
    </wsdl:input>
      <wsdl:output message="tns:ProcessNextStageSoapOut">
    </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="CancelNextStage">
      <wsdl:input message="tns:CancelNextStageSoapIn">
    </wsdl:input>
      <wsdl:output message="tns:CancelNextStageSoapOut">
    </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="AutoPublish">
      <wsdl:input message="tns:AutoPublishSoapIn">
    </wsdl:input>
      <wsdl:output message="tns:AutoPublishSoapOut">
    </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetAllSpaces">
      <wsdl:input message="tns:GetAllSpacesSoapIn">
    </wsdl:input>
      <wsdl:output message="tns:GetAllSpacesSoapOut">
    </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetSpaceDetails">
      <wsdl:input message="tns:GetSpaceDetailsSoapIn">
    </wsdl:input>
      <wsdl:output message="tns:GetSpaceDetailsSoapOut">
    </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetLoggedInSpaceDetails">
      <wsdl:input message="tns:GetLoggedInSpaceDetailsSoapIn">
    </wsdl:input>
      <wsdl:output message="tns:GetLoggedInSpaceDetailsSoapOut">
    </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="RemoveMember">
      <wsdl:input message="tns:RemoveMemberSoapIn">
    </wsdl:input>
      <wsdl:output message="tns:RemoveMemberSoapOut">
    </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="SendInvitationResponse">
      <wsdl:input message="tns:SendInvitationResponseSoapIn">
    </wsdl:input>
      <wsdl:output message="tns:SendInvitationResponseSoapOut">
    </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="CreateSpaceCopy">
      <wsdl:input message="tns:CreateSpaceCopySoapIn">
    </wsdl:input>
      <wsdl:output message="tns:CreateSpaceCopySoapOut">
    </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="CreateNewSpace">
      <wsdl:input message="tns:CreateNewSpaceSoapIn">
    </wsdl:input>
      <wsdl:output message="tns:CreateNewSpaceSoapOut">
    </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="DeleteTemplate">
      <wsdl:input message="tns:DeleteTemplateSoapIn">
    </wsdl:input>
      <wsdl:output message="tns:DeleteTemplateSoapOut">
    </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetPublishLogDetails">
      <wsdl:input message="tns:GetPublishLogDetailsSoapIn">
    </wsdl:input>
      <wsdl:output message="tns:GetPublishLogDetailsSoapOut">
    </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetFileDetails">
      <wsdl:input message="tns:GetFileDetailsSoapIn">
    </wsdl:input>
      <wsdl:output message="tns:GetFileDetailsSoapOut">
    </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetQueryDetails">
      <wsdl:input message="tns:GetQueryDetailsSoapIn">
    </wsdl:input>
      <wsdl:output message="tns:GetQueryDetailsSoapOut">
    </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetDynamicGroups">
      <wsdl:input message="tns:GetDynamicGroupsSoapIn">
    </wsdl:input>
      <wsdl:output message="tns:GetDynamicGroupsSoapOut">
    </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetMAFGroups">
      <wsdl:input message="tns:GetMAFGroupsSoapIn">
    </wsdl:input>
      <wsdl:output message="tns:GetMAFGroupsSoapOut">
    </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetGroupDetails">
      <wsdl:input message="tns:GetGroupDetailsSoapIn">
    </wsdl:input>
      <wsdl:output message="tns:GetGroupDetailsSoapOut">
    </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="AddNewGroup">
      <wsdl:input message="tns:AddNewGroupSoapIn">
    </wsdl:input>
      <wsdl:output message="tns:AddNewGroupSoapOut">
    </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="DeleteGroup">
      <wsdl:input message="tns:DeleteGroupSoapIn">
    </wsdl:input>
      <wsdl:output message="tns:DeleteGroupSoapOut">
    </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="SaveGroup">
      <wsdl:input message="tns:SaveGroupSoapIn">
    </wsdl:input>
      <wsdl:output message="tns:SaveGroupSoapOut">
    </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="AssignDynamicGroups">
      <wsdl:input message="tns:AssignDynamicGroupsSoapIn">
    </wsdl:input>
      <wsdl:output message="tns:AssignDynamicGroupsSoapOut">
    </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="CompleteGroupManagement">
      <wsdl:input message="tns:CompleteGroupManagementSoapIn">
    </wsdl:input>
      <wsdl:output message="tns:CompleteGroupManagementSoapOut">
    </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetSpaceShareDetails">
      <wsdl:input message="tns:GetSpaceShareDetailsSoapIn">
    </wsdl:input>
      <wsdl:output message="tns:GetSpaceShareDetailsSoapOut">
    </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="SendInvite">
      <wsdl:input message="tns:SendInviteSoapIn">
    </wsdl:input>
      <wsdl:output message="tns:SendInviteSoapOut">
    </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="DeleteInvite">
      <wsdl:input message="tns:DeleteInviteSoapIn">
    </wsdl:input>
      <wsdl:output message="tns:DeleteInviteSoapOut">
    </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="UpdateAdminAccess">
      <wsdl:input message="tns:UpdateAdminAccessSoapIn">
    </wsdl:input>
      <wsdl:output message="tns:UpdateAdminAccessSoapOut">
    </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="RemoveAccess">
      <wsdl:input message="tns:RemoveAccessSoapIn">
    </wsdl:input>
      <wsdl:output message="tns:RemoveAccessSoapOut">
    </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="createSMIWebLogin">
      <wsdl:input message="tns:createSMIWebLoginSoapIn">
    </wsdl:input>
      <wsdl:output message="tns:createSMIWebLoginSoapOut">
    </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="swapContentWithSpace">
      <wsdl:input message="tns:swapContentWithSpaceSoapIn">
    </wsdl:input>
      <wsdl:output message="tns:swapContentWithSpaceSoapOut">
    </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getTimeZones">
      <wsdl:input message="tns:getTimeZonesSoapIn">
    </wsdl:input>
      <wsdl:output message="tns:getTimeZonesSoapOut">
    </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="executeCommand">
      <wsdl:input message="tns:executeCommandSoapIn">
    </wsdl:input>
      <wsdl:output message="tns:executeCommandSoapOut">
    </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="updateLogicalModel">
      <wsdl:input message="tns:updateLogicalModelSoapIn">
    </wsdl:input>
      <wsdl:output message="tns:updateLogicalModelSoapOut">
    </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getAccountSummary">
      <wsdl:input message="tns:getAccountSummarySoapIn">
    </wsdl:input>
      <wsdl:output message="tns:getAccountSummarySoapOut">
    </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getNumAccountUsers">
      <wsdl:input message="tns:getNumAccountUsersSoapIn">
    </wsdl:input>
      <wsdl:output message="tns:getNumAccountUsersSoapOut">
    </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getUsersToManage">
      <wsdl:input message="tns:getUsersToManageSoapIn">
    </wsdl:input>
      <wsdl:output message="tns:getUsersToManageSoapOut">
    </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getSpacesToManage">
      <wsdl:input message="tns:getSpacesToManageSoapIn">
    </wsdl:input>
      <wsdl:output message="tns:getSpacesToManageSoapOut">
    </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="addUserToSpace">
      <wsdl:input message="tns:addUserToSpaceSoapIn">
    </wsdl:input>
      <wsdl:output message="tns:addUserToSpaceSoapOut">
    </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="updatePassword">
      <wsdl:input message="tns:updatePasswordSoapIn">
    </wsdl:input>
      <wsdl:output message="tns:updatePasswordSoapOut">
    </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="updateUserStatus">
      <wsdl:input message="tns:updateUserStatusSoapIn">
    </wsdl:input>
      <wsdl:output message="tns:updateUserStatusSoapOut">
    </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="unlockUsers">
      <wsdl:input message="tns:unlockUsersSoapIn">
    </wsdl:input>
      <wsdl:output message="tns:unlockUsersSoapOut">
    </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="updateAccountAdminPriveleges">
      <wsdl:input message="tns:updateAccountAdminPrivelegesSoapIn">
    </wsdl:input>
      <wsdl:output message="tns:updateAccountAdminPrivelegesSoapOut">
    </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="processCreateUserFile">
      <wsdl:input message="tns:processCreateUserFileSoapIn">
    </wsdl:input>
      <wsdl:output message="tns:processCreateUserFileSoapOut">
    </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetSources">
      <wsdl:input message="tns:GetSourcesSoapIn">
    </wsdl:input>
      <wsdl:output message="tns:GetSourcesSoapOut">
    </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetSourceDetails">
      <wsdl:input message="tns:GetSourceDetailsSoapIn">
    </wsdl:input>
      <wsdl:output message="tns:GetSourceDetailsSoapOut">
    </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="saveRepository">
      <wsdl:input message="tns:saveRepositorySoapIn">
    </wsdl:input>
      <wsdl:output message="tns:saveRepositorySoapOut">
    </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="saveStagingColumn">
      <wsdl:input message="tns:saveStagingColumnSoapIn">
    </wsdl:input>
      <wsdl:output message="tns:saveStagingColumnSoapOut">
    </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="removeStagingColumn">
      <wsdl:input message="tns:removeStagingColumnSoapIn">
    </wsdl:input>
      <wsdl:output message="tns:removeStagingColumnSoapOut">
    </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="renameStagingColumn">
      <wsdl:input message="tns:renameStagingColumnSoapIn">
    </wsdl:input>
      <wsdl:output message="tns:renameStagingColumnSoapOut">
    </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="saveHierarchy">
      <wsdl:input message="tns:saveHierarchySoapIn">
    </wsdl:input>
      <wsdl:output message="tns:saveHierarchySoapOut">
    </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="removeHierarchy">
      <wsdl:input message="tns:removeHierarchySoapIn">
    </wsdl:input>
      <wsdl:output message="tns:removeHierarchySoapOut">
    </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="saveLevel">
      <wsdl:input message="tns:saveLevelSoapIn">
    </wsdl:input>
      <wsdl:output message="tns:saveLevelSoapOut">
    </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="saveDataSource">
      <wsdl:input message="tns:saveDataSourceSoapIn">
    </wsdl:input>
      <wsdl:output message="tns:saveDataSourceSoapOut">
    </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="saveDataSourceAndColumns">
      <wsdl:input message="tns:saveDataSourceAndColumnsSoapIn">
    </wsdl:input>
      <wsdl:output message="tns:saveDataSourceAndColumnsSoapOut">
    </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="saveAndValidateScript">
      <wsdl:input message="tns:saveAndValidateScriptSoapIn">
    </wsdl:input>
      <wsdl:output message="tns:saveAndValidateScriptSoapOut">
    </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="executeScript">
      <wsdl:input message="tns:executeScriptSoapIn">
    </wsdl:input>
      <wsdl:output message="tns:executeScriptSoapOut">
    </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="removeDataSource">
      <wsdl:input message="tns:removeDataSourceSoapIn">
    </wsdl:input>
      <wsdl:output message="tns:removeDataSourceSoapOut">
    </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="EmptyDataSource">
      <wsdl:input message="tns:EmptyDataSourceSoapIn">
    </wsdl:input>
      <wsdl:output message="tns:EmptyDataSourceSoapOut">
    </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getRawData">
      <wsdl:input message="tns:getRawDataSoapIn">
    </wsdl:input>
      <wsdl:output message="tns:getRawDataSoapOut">
    </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetRepository">
      <wsdl:input message="tns:GetRepositorySoapIn">
    </wsdl:input>
      <wsdl:output message="tns:GetRepositorySoapOut">
    </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="SetRepository">
      <wsdl:input message="tns:SetRepositorySoapIn">
    </wsdl:input>
      <wsdl:output message="tns:SetRepositorySoapOut">
    </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getSchemaTables">
      <wsdl:input message="tns:getSchemaTablesSoapIn">
    </wsdl:input>
      <wsdl:output message="tns:getSchemaTablesSoapOut">
    </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getCubes">
      <wsdl:input message="tns:getCubesSoapIn">
    </wsdl:input>
      <wsdl:output message="tns:getCubesSoapOut">
    </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getCube">
      <wsdl:input message="tns:getCubeSoapIn">
    </wsdl:input>
      <wsdl:output message="tns:getCubeSoapOut">
    </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getTableSchema">
      <wsdl:input message="tns:getTableSchemaSoapIn">
    </wsdl:input>
      <wsdl:output message="tns:getTableSchemaSoapOut">
    </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="isConnected">
      <wsdl:input message="tns:isConnectedSoapIn">
    </wsdl:input>
      <wsdl:output message="tns:isConnectedSoapOut">
    </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="updateScheduledReport">
      <wsdl:input message="tns:updateScheduledReportSoapIn">
    </wsdl:input>
      <wsdl:output message="tns:updateScheduledReportSoapOut">
    </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="runScheduledReport">
      <wsdl:input message="tns:runScheduledReportSoapIn">
    </wsdl:input>
      <wsdl:output message="tns:runScheduledReportSoapOut">
    </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="addScheduledReport">
      <wsdl:input message="tns:addScheduledReportSoapIn">
    </wsdl:input>
      <wsdl:output message="tns:addScheduledReportSoapOut">
    </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getScheduledReports">
      <wsdl:input message="tns:getScheduledReportsSoapIn">
    </wsdl:input>
      <wsdl:output message="tns:getScheduledReportsSoapOut">
    </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getScheduledReport">
      <wsdl:input message="tns:getScheduledReportSoapIn">
    </wsdl:input>
      <wsdl:output message="tns:getScheduledReportSoapOut">
    </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="deleteScheduledReport">
      <wsdl:input message="tns:deleteScheduledReportSoapIn">
    </wsdl:input>
      <wsdl:output message="tns:deleteScheduledReportSoapOut">
    </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="exportData">
      <wsdl:input message="tns:exportDataSoapIn">
    </wsdl:input>
      <wsdl:output message="tns:exportDataSoapOut">
    </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getExportedDataFiles">
      <wsdl:input message="tns:getExportedDataFilesSoapIn">
    </wsdl:input>
      <wsdl:output message="tns:getExportedDataFilesSoapOut">
    </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getReportList">
      <wsdl:input message="tns:getReportListSoapIn">
    </wsdl:input>
      <wsdl:output message="tns:getReportListSoapOut">
    </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="validateQuery">
      <wsdl:input message="tns:validateQuerySoapIn">
    </wsdl:input>
      <wsdl:output message="tns:validateQuerySoapOut">
    </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getBirstConnectConfigs">
      <wsdl:input message="tns:getBirstConnectConfigsSoapIn">
    </wsdl:input>
      <wsdl:output message="tns:getBirstConnectConfigsSoapOut">
    </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="deleteBirstConnectConfig">
      <wsdl:input message="tns:deleteBirstConnectConfigSoapIn">
    </wsdl:input>
      <wsdl:output message="tns:deleteBirstConnectConfigSoapOut">
    </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="addBirstConnectConfig">
      <wsdl:input message="tns:addBirstConnectConfigSoapIn">
    </wsdl:input>
      <wsdl:output message="tns:addBirstConnectConfigSoapOut">
    </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="renameBirstConnectConfig">
      <wsdl:input message="tns:renameBirstConnectConfigSoapIn">
    </wsdl:input>
      <wsdl:output message="tns:renameBirstConnectConfigSoapOut">
    </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getVariables">
      <wsdl:input message="tns:getVariablesSoapIn">
    </wsdl:input>
      <wsdl:output message="tns:getVariablesSoapOut">
    </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="updateVariable">
      <wsdl:input message="tns:updateVariableSoapIn">
    </wsdl:input>
      <wsdl:output message="tns:updateVariableSoapOut">
    </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="deleteVariable">
      <wsdl:input message="tns:deleteVariableSoapIn">
    </wsdl:input>
      <wsdl:output message="tns:deleteVariableSoapOut">
    </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getLogicalExpressions">
      <wsdl:input message="tns:getLogicalExpressionsSoapIn">
    </wsdl:input>
      <wsdl:output message="tns:getLogicalExpressionsSoapOut">
    </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getAggregates">
      <wsdl:input message="tns:getAggregatesSoapIn">
    </wsdl:input>
      <wsdl:output message="tns:getAggregatesSoapOut">
    </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="updateCustomMeasure">
      <wsdl:input message="tns:updateCustomMeasureSoapIn">
    </wsdl:input>
      <wsdl:output message="tns:updateCustomMeasureSoapOut">
    </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="updateCustomAttribute">
      <wsdl:input message="tns:updateCustomAttributeSoapIn">
    </wsdl:input>
      <wsdl:output message="tns:updateCustomAttributeSoapOut">
    </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="deleteLogicalExpression">
      <wsdl:input message="tns:deleteLogicalExpressionSoapIn">
    </wsdl:input>
      <wsdl:output message="tns:deleteLogicalExpressionSoapOut">
    </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getBucketedMeasures">
      <wsdl:input message="tns:getBucketedMeasuresSoapIn">
    </wsdl:input>
      <wsdl:output message="tns:getBucketedMeasuresSoapOut">
    </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="updateBucketedMeasure">
      <wsdl:input message="tns:updateBucketedMeasureSoapIn">
    </wsdl:input>
      <wsdl:output message="tns:updateBucketedMeasureSoapOut">
    </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="deleteBucketedMeasure">
      <wsdl:input message="tns:deleteBucketedMeasureSoapIn">
    </wsdl:input>
      <wsdl:output message="tns:deleteBucketedMeasureSoapOut">
    </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getMeasureList">
      <wsdl:input message="tns:getMeasureListSoapIn">
    </wsdl:input>
      <wsdl:output message="tns:getMeasureListSoapOut">
    </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getDataTables">
      <wsdl:input message="tns:getDataTablesSoapIn">
    </wsdl:input>
      <wsdl:output message="tns:getDataTablesSoapOut">
    </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getLevelData">
      <wsdl:input message="tns:getLevelDataSoapIn">
    </wsdl:input>
      <wsdl:output message="tns:getLevelDataSoapOut">
    </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getMeasureTableData">
      <wsdl:input message="tns:getMeasureTableDataSoapIn">
    </wsdl:input>
      <wsdl:output message="tns:getMeasureTableDataSoapOut">
    </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getStagingTableData">
      <wsdl:input message="tns:getStagingTableDataSoapIn">
    </wsdl:input>
      <wsdl:output message="tns:getStagingTableDataSoapOut">
    </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getTableQueryString">
      <wsdl:input message="tns:getTableQueryStringSoapIn">
    </wsdl:input>
      <wsdl:output message="tns:getTableQueryStringSoapOut">
    </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getGrainList">
      <wsdl:input message="tns:getGrainListSoapIn">
    </wsdl:input>
      <wsdl:output message="tns:getGrainListSoapOut">
    </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getGrainColumnList">
      <wsdl:input message="tns:getGrainColumnListSoapIn">
    </wsdl:input>
      <wsdl:output message="tns:getGrainColumnListSoapOut">
    </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getLevelList">
      <wsdl:input message="tns:getLevelListSoapIn">
    </wsdl:input>
      <wsdl:output message="tns:getLevelListSoapOut">
    </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getLevelColumnList">
      <wsdl:input message="tns:getLevelColumnListSoapIn">
    </wsdl:input>
      <wsdl:output message="tns:getLevelColumnListSoapOut">
    </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getColumnsAtLowerGrains">
      <wsdl:input message="tns:getColumnsAtLowerGrainsSoapIn">
    </wsdl:input>
      <wsdl:output message="tns:getColumnsAtLowerGrainsSoapOut">
    </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="ping">
      <wsdl:input message="tns:pingSoapIn">
    </wsdl:input>
      <wsdl:output message="tns:pingSoapOut">
    </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetProcessInitData">
      <wsdl:input message="tns:GetProcessInitDataSoapIn">
    </wsdl:input>
      <wsdl:output message="tns:GetProcessInitDataSoapOut">
    </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetProgress">
      <wsdl:input message="tns:GetProgressSoapIn">
    </wsdl:input>
      <wsdl:output message="tns:GetProgressSoapOut">
    </wsdl:output>
    </wsdl:operation>
  </wsdl:portType>
  <wsdl:binding name="AdminServiceSoap12" type="tns:AdminServiceSoap">
    <soap12:binding transport="http://schemas.xmlsoap.org/soap/http"/>
    <wsdl:operation name="getCreateUserAllowedDomains">
      <soap12:operation soapAction="http://www.birst.com/getCreateUserAllowedDomains" style="document"/>
      <wsdl:input>
        <soap12:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="createAccount">
      <soap12:operation soapAction="http://www.birst.com/createAccount" style="document"/>
      <wsdl:input>
        <soap12:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getLoggedInUserDetails">
      <soap12:operation soapAction="http://www.birst.com/getLoggedInUserDetails" style="document"/>
      <wsdl:input>
        <soap12:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="publish">
      <soap12:operation soapAction="http://www.birst.com/publish" style="document"/>
      <wsdl:input>
        <soap12:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="ConfigureDashboards">
      <soap12:operation soapAction="http://www.birst.com/ConfigureDashboards" style="document"/>
      <wsdl:input>
        <soap12:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="PublishWithDashboards">
      <soap12:operation soapAction="http://www.birst.com/PublishWithDashboards" style="document"/>
      <wsdl:input>
        <soap12:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetPublishHistory">
      <soap12:operation soapAction="http://www.birst.com/GetPublishHistory" style="document"/>
      <wsdl:input>
        <soap12:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="CanDeleteLast">
      <soap12:operation soapAction="http://www.birst.com/CanDeleteLast" style="document"/>
      <wsdl:input>
        <soap12:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="DeleteLast">
      <soap12:operation soapAction="http://www.birst.com/DeleteLast" style="document"/>
      <wsdl:input>
        <soap12:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="DeleteAll">
      <soap12:operation soapAction="http://www.birst.com/DeleteAll" style="document"/>
      <wsdl:input>
        <soap12:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="DeleteSpace">
      <soap12:operation soapAction="http://www.birst.com/DeleteSpace" style="document"/>
      <wsdl:input>
        <soap12:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="DeleteOverview">
      <soap12:operation soapAction="http://www.birst.com/DeleteOverview" style="document"/>
      <wsdl:input>
        <soap12:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetTemplateCategories">
      <soap12:operation soapAction="http://www.birst.com/GetTemplateCategories" style="document"/>
      <wsdl:input>
        <soap12:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetTemplates">
      <soap12:operation soapAction="http://www.birst.com/GetTemplates" style="document"/>
      <wsdl:input>
        <soap12:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetAttachmentList">
      <soap12:operation soapAction="http://www.birst.com/GetAttachmentList" style="document"/>
      <wsdl:input>
        <soap12:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetAttachmentProcessOuput">
      <soap12:operation soapAction="http://www.birst.com/GetAttachmentProcessOuput" style="document"/>
      <wsdl:input>
        <soap12:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetCurrentSession">
      <soap12:operation soapAction="http://www.birst.com/GetCurrentSession" style="document"/>
      <wsdl:input>
        <soap12:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="CreateNewTemplate">
      <soap12:operation soapAction="http://www.birst.com/CreateNewTemplate" style="document"/>
      <wsdl:input>
        <soap12:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="RemoveAttachment">
      <soap12:operation soapAction="http://www.birst.com/RemoveAttachment" style="document"/>
      <wsdl:input>
        <soap12:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GenerateSSOCredentials">
      <soap12:operation soapAction="http://www.birst.com/GenerateSSOCredentials" style="document"/>
      <wsdl:input>
        <soap12:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="ConvertSpace">
      <soap12:operation soapAction="http://www.birst.com/ConvertSpace" style="document"/>
      <wsdl:input>
        <soap12:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="ResetSpaceSettings">
      <soap12:operation soapAction="http://www.birst.com/ResetSpaceSettings" style="document"/>
      <wsdl:input>
        <soap12:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetSpaceProperties">
      <soap12:operation soapAction="http://www.birst.com/GetSpaceProperties" style="document"/>
      <wsdl:input>
        <soap12:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="ModifySpaceProperties">
      <soap12:operation soapAction="http://www.birst.com/ModifySpaceProperties" style="document"/>
      <wsdl:input>
        <soap12:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="ProcessUploadedFile">
      <soap12:operation soapAction="http://www.birst.com/ProcessUploadedFile" style="document"/>
      <wsdl:input>
        <soap12:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="ProcessNextStage">
      <soap12:operation soapAction="http://www.birst.com/ProcessNextStage" style="document"/>
      <wsdl:input>
        <soap12:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="CancelNextStage">
      <soap12:operation soapAction="http://www.birst.com/CancelNextStage" style="document"/>
      <wsdl:input>
        <soap12:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="AutoPublish">
      <soap12:operation soapAction="http://www.birst.com/AutoPublish" style="document"/>
      <wsdl:input>
        <soap12:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetAllSpaces">
      <soap12:operation soapAction="http://www.birst.com/GetAllSpaces" style="document"/>
      <wsdl:input>
        <soap12:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetSpaceDetails">
      <soap12:operation soapAction="http://www.birst.com/GetSpaceDetails" style="document"/>
      <wsdl:input>
        <soap12:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetLoggedInSpaceDetails">
      <soap12:operation soapAction="http://www.birst.com/GetLoggedInSpaceDetails" style="document"/>
      <wsdl:input>
        <soap12:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="RemoveMember">
      <soap12:operation soapAction="http://www.birst.com/RemoveMember" style="document"/>
      <wsdl:input>
        <soap12:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="SendInvitationResponse">
      <soap12:operation soapAction="http://www.birst.com/SendInvitationResponse" style="document"/>
      <wsdl:input>
        <soap12:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="CreateSpaceCopy">
      <soap12:operation soapAction="http://www.birst.com/CreateSpaceCopy" style="document"/>
      <wsdl:input>
        <soap12:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="CreateNewSpace">
      <soap12:operation soapAction="http://www.birst.com/CreateNewSpace" style="document"/>
      <wsdl:input>
        <soap12:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="DeleteTemplate">
      <soap12:operation soapAction="http://www.birst.com/DeleteTemplate" style="document"/>
      <wsdl:input>
        <soap12:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetPublishLogDetails">
      <soap12:operation soapAction="http://www.birst.com/GetPublishLogDetails" style="document"/>
      <wsdl:input>
        <soap12:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetFileDetails">
      <soap12:operation soapAction="http://www.birst.com/GetFileDetails" style="document"/>
      <wsdl:input>
        <soap12:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetQueryDetails">
      <soap12:operation soapAction="http://www.birst.com/GetQueryDetails" style="document"/>
      <wsdl:input>
        <soap12:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetDynamicGroups">
      <soap12:operation soapAction="http://www.birst.com/GetDynamicGroups" style="document"/>
      <wsdl:input>
        <soap12:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetMAFGroups">
      <soap12:operation soapAction="http://www.birst.com/GetMAFGroups" style="document"/>
      <wsdl:input>
        <soap12:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetGroupDetails">
      <soap12:operation soapAction="http://www.birst.com/GetGroupDetails" style="document"/>
      <wsdl:input>
        <soap12:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="AddNewGroup">
      <soap12:operation soapAction="http://www.birst.com/AddNewGroup" style="document"/>
      <wsdl:input>
        <soap12:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="DeleteGroup">
      <soap12:operation soapAction="http://www.birst.com/DeleteGroup" style="document"/>
      <wsdl:input>
        <soap12:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="SaveGroup">
      <soap12:operation soapAction="http://www.birst.com/SaveGroup" style="document"/>
      <wsdl:input>
        <soap12:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="AssignDynamicGroups">
      <soap12:operation soapAction="http://www.birst.com/AssignDynamicGroups" style="document"/>
      <wsdl:input>
        <soap12:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="CompleteGroupManagement">
      <soap12:operation soapAction="http://www.birst.com/CompleteGroupManagement" style="document"/>
      <wsdl:input>
        <soap12:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetSpaceShareDetails">
      <soap12:operation soapAction="http://www.birst.com/GetSpaceShareDetails" style="document"/>
      <wsdl:input>
        <soap12:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="SendInvite">
      <soap12:operation soapAction="http://www.birst.com/SendInvite" style="document"/>
      <wsdl:input>
        <soap12:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="DeleteInvite">
      <soap12:operation soapAction="http://www.birst.com/DeleteInvite" style="document"/>
      <wsdl:input>
        <soap12:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="UpdateAdminAccess">
      <soap12:operation soapAction="http://www.birst.com/UpdateAdminAccess" style="document"/>
      <wsdl:input>
        <soap12:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="RemoveAccess">
      <soap12:operation soapAction="http://www.birst.com/RemoveAccess" style="document"/>
      <wsdl:input>
        <soap12:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="createSMIWebLogin">
      <soap12:operation soapAction="http://www.birst.com/createSMIWebLogin" style="document"/>
      <wsdl:input>
        <soap12:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="swapContentWithSpace">
      <soap12:operation soapAction="http://www.birst.com/swapContentWithSpace" style="document"/>
      <wsdl:input>
        <soap12:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getTimeZones">
      <soap12:operation soapAction="http://www.birst.com/getTimeZones" style="document"/>
      <wsdl:input>
        <soap12:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="executeCommand">
      <soap12:operation soapAction="http://www.birst.com/executeCommand" style="document"/>
      <wsdl:input>
        <soap12:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="updateLogicalModel">
      <soap12:operation soapAction="http://www.birst.com/updateLogicalModel" style="document"/>
      <wsdl:input>
        <soap12:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getAccountSummary">
      <soap12:operation soapAction="http://www.birst.com/getAccountSummary" style="document"/>
      <wsdl:input>
        <soap12:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getNumAccountUsers">
      <soap12:operation soapAction="http://www.birst.com/getNumAccountUsers" style="document"/>
      <wsdl:input>
        <soap12:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getUsersToManage">
      <soap12:operation soapAction="http://www.birst.com/getUsersToManage" style="document"/>
      <wsdl:input>
        <soap12:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getSpacesToManage">
      <soap12:operation soapAction="http://www.birst.com/getSpacesToManage" style="document"/>
      <wsdl:input>
        <soap12:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="addUserToSpace">
      <soap12:operation soapAction="http://www.birst.com/addUserToSpace" style="document"/>
      <wsdl:input>
        <soap12:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="updatePassword">
      <soap12:operation soapAction="http://www.birst.com/updatePassword" style="document"/>
      <wsdl:input>
        <soap12:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="updateUserStatus">
      <soap12:operation soapAction="http://www.birst.com/updateUserStatus" style="document"/>
      <wsdl:input>
        <soap12:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="unlockUsers">
      <soap12:operation soapAction="http://www.birst.com/unlockUsers" style="document"/>
      <wsdl:input>
        <soap12:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="updateAccountAdminPriveleges">
      <soap12:operation soapAction="http://www.birst.com/updateAccountAdminPriveleges" style="document"/>
      <wsdl:input>
        <soap12:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="processCreateUserFile">
      <soap12:operation soapAction="http://www.birst.com/processCreateUserFile" style="document"/>
      <wsdl:input>
        <soap12:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetSources">
      <soap12:operation soapAction="http://www.birst.com/GetSources" style="document"/>
      <wsdl:input>
        <soap12:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetSourceDetails">
      <soap12:operation soapAction="http://www.birst.com/GetSourceDetails" style="document"/>
      <wsdl:input>
        <soap12:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="saveRepository">
      <soap12:operation soapAction="http://www.birst.com/saveRepository" style="document"/>
      <wsdl:input>
        <soap12:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="saveStagingColumn">
      <soap12:operation soapAction="http://www.birst.com/saveStagingColumn" style="document"/>
      <wsdl:input>
        <soap12:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="removeStagingColumn">
      <soap12:operation soapAction="http://www.birst.com/removeStagingColumn" style="document"/>
      <wsdl:input>
        <soap12:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="renameStagingColumn">
      <soap12:operation soapAction="http://www.birst.com/renameStagingColumn" style="document"/>
      <wsdl:input>
        <soap12:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="saveHierarchy">
      <soap12:operation soapAction="http://www.birst.com/saveHierarchy" style="document"/>
      <wsdl:input>
        <soap12:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="removeHierarchy">
      <soap12:operation soapAction="http://www.birst.com/removeHierarchy" style="document"/>
      <wsdl:input>
        <soap12:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="saveLevel">
      <soap12:operation soapAction="http://www.birst.com/saveLevel" style="document"/>
      <wsdl:input>
        <soap12:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="saveDataSource">
      <soap12:operation soapAction="http://www.birst.com/saveDataSource" style="document"/>
      <wsdl:input>
        <soap12:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="saveDataSourceAndColumns">
      <soap12:operation soapAction="http://www.birst.com/saveDataSourceAndColumns" style="document"/>
      <wsdl:input>
        <soap12:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="saveAndValidateScript">
      <soap12:operation soapAction="http://www.birst.com/saveAndValidateScript" style="document"/>
      <wsdl:input>
        <soap12:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="executeScript">
      <soap12:operation soapAction="http://www.birst.com/executeScript" style="document"/>
      <wsdl:input>
        <soap12:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="removeDataSource">
      <soap12:operation soapAction="http://www.birst.com/removeDataSource" style="document"/>
      <wsdl:input>
        <soap12:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="EmptyDataSource">
      <soap12:operation soapAction="http://www.birst.com/EmptyDataSource" style="document"/>
      <wsdl:input>
        <soap12:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getRawData">
      <soap12:operation soapAction="http://www.birst.com/getRawData" style="document"/>
      <wsdl:input>
        <soap12:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetRepository">
      <soap12:operation soapAction="http://www.birst.com/GetRepository" style="document"/>
      <wsdl:input>
        <soap12:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="SetRepository">
      <soap12:operation soapAction="http://www.birst.com/SetRepository" style="document"/>
      <wsdl:input>
        <soap12:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getSchemaTables">
      <soap12:operation soapAction="http://www.birst.com/getSchemaTables" style="document"/>
      <wsdl:input>
        <soap12:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getCubes">
      <soap12:operation soapAction="http://www.birst.com/getCubes" style="document"/>
      <wsdl:input>
        <soap12:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getCube">
      <soap12:operation soapAction="http://www.birst.com/getCube" style="document"/>
      <wsdl:input>
        <soap12:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getTableSchema">
      <soap12:operation soapAction="http://www.birst.com/getTableSchema" style="document"/>
      <wsdl:input>
        <soap12:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="isConnected">
      <soap12:operation soapAction="http://www.birst.com/isConnected" style="document"/>
      <wsdl:input>
        <soap12:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="updateScheduledReport">
      <soap12:operation soapAction="http://www.birst.com/updateScheduledReport" style="document"/>
      <wsdl:input>
        <soap12:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="runScheduledReport">
      <soap12:operation soapAction="http://www.birst.com/runScheduledReport" style="document"/>
      <wsdl:input>
        <soap12:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="addScheduledReport">
      <soap12:operation soapAction="http://www.birst.com/addScheduledReport" style="document"/>
      <wsdl:input>
        <soap12:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getScheduledReports">
      <soap12:operation soapAction="http://www.birst.com/getScheduledReports" style="document"/>
      <wsdl:input>
        <soap12:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getScheduledReport">
      <soap12:operation soapAction="http://www.birst.com/getScheduledReport" style="document"/>
      <wsdl:input>
        <soap12:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="deleteScheduledReport">
      <soap12:operation soapAction="http://www.birst.com/deleteScheduledReport" style="document"/>
      <wsdl:input>
        <soap12:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="exportData">
      <soap12:operation soapAction="http://www.birst.com/exportData" style="document"/>
      <wsdl:input>
        <soap12:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getExportedDataFiles">
      <soap12:operation soapAction="http://www.birst.com/getExportedDataFiles" style="document"/>
      <wsdl:input>
        <soap12:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getReportList">
      <soap12:operation soapAction="http://www.birst.com/getReportList" style="document"/>
      <wsdl:input>
        <soap12:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="validateQuery">
      <soap12:operation soapAction="http://www.birst.com/validateQuery" style="document"/>
      <wsdl:input>
        <soap12:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getBirstConnectConfigs">
      <soap12:operation soapAction="http://www.birst.com/getBirstConnectConfigs" style="document"/>
      <wsdl:input>
        <soap12:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="deleteBirstConnectConfig">
      <soap12:operation soapAction="http://www.birst.com/deleteBirstConnectConfig" style="document"/>
      <wsdl:input>
        <soap12:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="addBirstConnectConfig">
      <soap12:operation soapAction="http://www.birst.com/addBirstConnectConfig" style="document"/>
      <wsdl:input>
        <soap12:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="renameBirstConnectConfig">
      <soap12:operation soapAction="http://www.birst.com/renameBirstConnectConfig" style="document"/>
      <wsdl:input>
        <soap12:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getVariables">
      <soap12:operation soapAction="http://www.birst.com/getVariables" style="document"/>
      <wsdl:input>
        <soap12:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="updateVariable">
      <soap12:operation soapAction="http://www.birst.com/updateVariable" style="document"/>
      <wsdl:input>
        <soap12:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="deleteVariable">
      <soap12:operation soapAction="http://www.birst.com/deleteVariable" style="document"/>
      <wsdl:input>
        <soap12:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getLogicalExpressions">
      <soap12:operation soapAction="http://www.birst.com/getLogicalExpressions" style="document"/>
      <wsdl:input>
        <soap12:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getAggregates">
      <soap12:operation soapAction="http://www.birst.com/getAggregates" style="document"/>
      <wsdl:input>
        <soap12:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="updateCustomMeasure">
      <soap12:operation soapAction="http://www.birst.com/updateCustomMeasure" style="document"/>
      <wsdl:input>
        <soap12:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="updateCustomAttribute">
      <soap12:operation soapAction="http://www.birst.com/updateCustomAttribute" style="document"/>
      <wsdl:input>
        <soap12:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="deleteLogicalExpression">
      <soap12:operation soapAction="http://www.birst.com/deleteLogicalExpression" style="document"/>
      <wsdl:input>
        <soap12:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getBucketedMeasures">
      <soap12:operation soapAction="http://www.birst.com/getBucketedMeasures" style="document"/>
      <wsdl:input>
        <soap12:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="updateBucketedMeasure">
      <soap12:operation soapAction="http://www.birst.com/updateBucketedMeasure" style="document"/>
      <wsdl:input>
        <soap12:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="deleteBucketedMeasure">
      <soap12:operation soapAction="http://www.birst.com/deleteBucketedMeasure" style="document"/>
      <wsdl:input>
        <soap12:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getMeasureList">
      <soap12:operation soapAction="http://www.birst.com/getMeasureList" style="document"/>
      <wsdl:input>
        <soap12:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getDataTables">
      <soap12:operation soapAction="http://www.birst.com/getDataTables" style="document"/>
      <wsdl:input>
        <soap12:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getLevelData">
      <soap12:operation soapAction="http://www.birst.com/getLevelData" style="document"/>
      <wsdl:input>
        <soap12:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getMeasureTableData">
      <soap12:operation soapAction="http://www.birst.com/getMeasureTableData" style="document"/>
      <wsdl:input>
        <soap12:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getStagingTableData">
      <soap12:operation soapAction="http://www.birst.com/getStagingTableData" style="document"/>
      <wsdl:input>
        <soap12:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getTableQueryString">
      <soap12:operation soapAction="http://www.birst.com/getTableQueryString" style="document"/>
      <wsdl:input>
        <soap12:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getGrainList">
      <soap12:operation soapAction="http://www.birst.com/getGrainList" style="document"/>
      <wsdl:input>
        <soap12:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getGrainColumnList">
      <soap12:operation soapAction="http://www.birst.com/getGrainColumnList" style="document"/>
      <wsdl:input>
        <soap12:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getLevelList">
      <soap12:operation soapAction="http://www.birst.com/getLevelList" style="document"/>
      <wsdl:input>
        <soap12:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getLevelColumnList">
      <soap12:operation soapAction="http://www.birst.com/getLevelColumnList" style="document"/>
      <wsdl:input>
        <soap12:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getColumnsAtLowerGrains">
      <soap12:operation soapAction="http://www.birst.com/getColumnsAtLowerGrains" style="document"/>
      <wsdl:input>
        <soap12:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="ping">
      <soap12:operation soapAction="http://www.birst.com/ping" style="document"/>
      <wsdl:input>
        <soap12:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetProcessInitData">
      <soap12:operation soapAction="http://www.birst.com/GetProcessInitData" style="document"/>
      <wsdl:input>
        <soap12:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetProgress">
      <soap12:operation soapAction="http://www.birst.com/GetProgress" style="document"/>
      <wsdl:input>
        <soap12:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
  </wsdl:binding>
  <wsdl:binding name="AdminServiceSoap" type="tns:AdminServiceSoap">
    <soap:binding transport="http://schemas.xmlsoap.org/soap/http"/>
    <wsdl:operation name="getCreateUserAllowedDomains">
      <soap:operation soapAction="http://www.birst.com/getCreateUserAllowedDomains" style="document"/>
      <wsdl:input>
        <soap:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="createAccount">
      <soap:operation soapAction="http://www.birst.com/createAccount" style="document"/>
      <wsdl:input>
        <soap:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getLoggedInUserDetails">
      <soap:operation soapAction="http://www.birst.com/getLoggedInUserDetails" style="document"/>
      <wsdl:input>
        <soap:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="publish">
      <soap:operation soapAction="http://www.birst.com/publish" style="document"/>
      <wsdl:input>
        <soap:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="ConfigureDashboards">
      <soap:operation soapAction="http://www.birst.com/ConfigureDashboards" style="document"/>
      <wsdl:input>
        <soap:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="PublishWithDashboards">
      <soap:operation soapAction="http://www.birst.com/PublishWithDashboards" style="document"/>
      <wsdl:input>
        <soap:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetPublishHistory">
      <soap:operation soapAction="http://www.birst.com/GetPublishHistory" style="document"/>
      <wsdl:input>
        <soap:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="CanDeleteLast">
      <soap:operation soapAction="http://www.birst.com/CanDeleteLast" style="document"/>
      <wsdl:input>
        <soap:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="DeleteLast">
      <soap:operation soapAction="http://www.birst.com/DeleteLast" style="document"/>
      <wsdl:input>
        <soap:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="DeleteAll">
      <soap:operation soapAction="http://www.birst.com/DeleteAll" style="document"/>
      <wsdl:input>
        <soap:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="DeleteSpace">
      <soap:operation soapAction="http://www.birst.com/DeleteSpace" style="document"/>
      <wsdl:input>
        <soap:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="DeleteOverview">
      <soap:operation soapAction="http://www.birst.com/DeleteOverview" style="document"/>
      <wsdl:input>
        <soap:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetTemplateCategories">
      <soap:operation soapAction="http://www.birst.com/GetTemplateCategories" style="document"/>
      <wsdl:input>
        <soap:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetTemplates">
      <soap:operation soapAction="http://www.birst.com/GetTemplates" style="document"/>
      <wsdl:input>
        <soap:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetAttachmentList">
      <soap:operation soapAction="http://www.birst.com/GetAttachmentList" style="document"/>
      <wsdl:input>
        <soap:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetAttachmentProcessOuput">
      <soap:operation soapAction="http://www.birst.com/GetAttachmentProcessOuput" style="document"/>
      <wsdl:input>
        <soap:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetCurrentSession">
      <soap:operation soapAction="http://www.birst.com/GetCurrentSession" style="document"/>
      <wsdl:input>
        <soap:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="CreateNewTemplate">
      <soap:operation soapAction="http://www.birst.com/CreateNewTemplate" style="document"/>
      <wsdl:input>
        <soap:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="RemoveAttachment">
      <soap:operation soapAction="http://www.birst.com/RemoveAttachment" style="document"/>
      <wsdl:input>
        <soap:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GenerateSSOCredentials">
      <soap:operation soapAction="http://www.birst.com/GenerateSSOCredentials" style="document"/>
      <wsdl:input>
        <soap:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="ConvertSpace">
      <soap:operation soapAction="http://www.birst.com/ConvertSpace" style="document"/>
      <wsdl:input>
        <soap:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="ResetSpaceSettings">
      <soap:operation soapAction="http://www.birst.com/ResetSpaceSettings" style="document"/>
      <wsdl:input>
        <soap:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetSpaceProperties">
      <soap:operation soapAction="http://www.birst.com/GetSpaceProperties" style="document"/>
      <wsdl:input>
        <soap:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="ModifySpaceProperties">
      <soap:operation soapAction="http://www.birst.com/ModifySpaceProperties" style="document"/>
      <wsdl:input>
        <soap:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="ProcessUploadedFile">
      <soap:operation soapAction="http://www.birst.com/ProcessUploadedFile" style="document"/>
      <wsdl:input>
        <soap:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="ProcessNextStage">
      <soap:operation soapAction="http://www.birst.com/ProcessNextStage" style="document"/>
      <wsdl:input>
        <soap:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="CancelNextStage">
      <soap:operation soapAction="http://www.birst.com/CancelNextStage" style="document"/>
      <wsdl:input>
        <soap:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="AutoPublish">
      <soap:operation soapAction="http://www.birst.com/AutoPublish" style="document"/>
      <wsdl:input>
        <soap:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetAllSpaces">
      <soap:operation soapAction="http://www.birst.com/GetAllSpaces" style="document"/>
      <wsdl:input>
        <soap:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetSpaceDetails">
      <soap:operation soapAction="http://www.birst.com/GetSpaceDetails" style="document"/>
      <wsdl:input>
        <soap:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetLoggedInSpaceDetails">
      <soap:operation soapAction="http://www.birst.com/GetLoggedInSpaceDetails" style="document"/>
      <wsdl:input>
        <soap:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="RemoveMember">
      <soap:operation soapAction="http://www.birst.com/RemoveMember" style="document"/>
      <wsdl:input>
        <soap:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="SendInvitationResponse">
      <soap:operation soapAction="http://www.birst.com/SendInvitationResponse" style="document"/>
      <wsdl:input>
        <soap:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="CreateSpaceCopy">
      <soap:operation soapAction="http://www.birst.com/CreateSpaceCopy" style="document"/>
      <wsdl:input>
        <soap:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="CreateNewSpace">
      <soap:operation soapAction="http://www.birst.com/CreateNewSpace" style="document"/>
      <wsdl:input>
        <soap:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="DeleteTemplate">
      <soap:operation soapAction="http://www.birst.com/DeleteTemplate" style="document"/>
      <wsdl:input>
        <soap:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetPublishLogDetails">
      <soap:operation soapAction="http://www.birst.com/GetPublishLogDetails" style="document"/>
      <wsdl:input>
        <soap:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetFileDetails">
      <soap:operation soapAction="http://www.birst.com/GetFileDetails" style="document"/>
      <wsdl:input>
        <soap:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetQueryDetails">
      <soap:operation soapAction="http://www.birst.com/GetQueryDetails" style="document"/>
      <wsdl:input>
        <soap:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetDynamicGroups">
      <soap:operation soapAction="http://www.birst.com/GetDynamicGroups" style="document"/>
      <wsdl:input>
        <soap:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetMAFGroups">
      <soap:operation soapAction="http://www.birst.com/GetMAFGroups" style="document"/>
      <wsdl:input>
        <soap:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetGroupDetails">
      <soap:operation soapAction="http://www.birst.com/GetGroupDetails" style="document"/>
      <wsdl:input>
        <soap:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="AddNewGroup">
      <soap:operation soapAction="http://www.birst.com/AddNewGroup" style="document"/>
      <wsdl:input>
        <soap:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="DeleteGroup">
      <soap:operation soapAction="http://www.birst.com/DeleteGroup" style="document"/>
      <wsdl:input>
        <soap:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="SaveGroup">
      <soap:operation soapAction="http://www.birst.com/SaveGroup" style="document"/>
      <wsdl:input>
        <soap:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="AssignDynamicGroups">
      <soap:operation soapAction="http://www.birst.com/AssignDynamicGroups" style="document"/>
      <wsdl:input>
        <soap:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="CompleteGroupManagement">
      <soap:operation soapAction="http://www.birst.com/CompleteGroupManagement" style="document"/>
      <wsdl:input>
        <soap:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetSpaceShareDetails">
      <soap:operation soapAction="http://www.birst.com/GetSpaceShareDetails" style="document"/>
      <wsdl:input>
        <soap:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="SendInvite">
      <soap:operation soapAction="http://www.birst.com/SendInvite" style="document"/>
      <wsdl:input>
        <soap:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="DeleteInvite">
      <soap:operation soapAction="http://www.birst.com/DeleteInvite" style="document"/>
      <wsdl:input>
        <soap:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="UpdateAdminAccess">
      <soap:operation soapAction="http://www.birst.com/UpdateAdminAccess" style="document"/>
      <wsdl:input>
        <soap:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="RemoveAccess">
      <soap:operation soapAction="http://www.birst.com/RemoveAccess" style="document"/>
      <wsdl:input>
        <soap:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="createSMIWebLogin">
      <soap:operation soapAction="http://www.birst.com/createSMIWebLogin" style="document"/>
      <wsdl:input>
        <soap:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="swapContentWithSpace">
      <soap:operation soapAction="http://www.birst.com/swapContentWithSpace" style="document"/>
      <wsdl:input>
        <soap:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getTimeZones">
      <soap:operation soapAction="http://www.birst.com/getTimeZones" style="document"/>
      <wsdl:input>
        <soap:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="executeCommand">
      <soap:operation soapAction="http://www.birst.com/executeCommand" style="document"/>
      <wsdl:input>
        <soap:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="updateLogicalModel">
      <soap:operation soapAction="http://www.birst.com/updateLogicalModel" style="document"/>
      <wsdl:input>
        <soap:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getAccountSummary">
      <soap:operation soapAction="http://www.birst.com/getAccountSummary" style="document"/>
      <wsdl:input>
        <soap:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getNumAccountUsers">
      <soap:operation soapAction="http://www.birst.com/getNumAccountUsers" style="document"/>
      <wsdl:input>
        <soap:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getUsersToManage">
      <soap:operation soapAction="http://www.birst.com/getUsersToManage" style="document"/>
      <wsdl:input>
        <soap:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getSpacesToManage">
      <soap:operation soapAction="http://www.birst.com/getSpacesToManage" style="document"/>
      <wsdl:input>
        <soap:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="addUserToSpace">
      <soap:operation soapAction="http://www.birst.com/addUserToSpace" style="document"/>
      <wsdl:input>
        <soap:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="updatePassword">
      <soap:operation soapAction="http://www.birst.com/updatePassword" style="document"/>
      <wsdl:input>
        <soap:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="updateUserStatus">
      <soap:operation soapAction="http://www.birst.com/updateUserStatus" style="document"/>
      <wsdl:input>
        <soap:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="unlockUsers">
      <soap:operation soapAction="http://www.birst.com/unlockUsers" style="document"/>
      <wsdl:input>
        <soap:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="updateAccountAdminPriveleges">
      <soap:operation soapAction="http://www.birst.com/updateAccountAdminPriveleges" style="document"/>
      <wsdl:input>
        <soap:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="processCreateUserFile">
      <soap:operation soapAction="http://www.birst.com/processCreateUserFile" style="document"/>
      <wsdl:input>
        <soap:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetSources">
      <soap:operation soapAction="http://www.birst.com/GetSources" style="document"/>
      <wsdl:input>
        <soap:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetSourceDetails">
      <soap:operation soapAction="http://www.birst.com/GetSourceDetails" style="document"/>
      <wsdl:input>
        <soap:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="saveRepository">
      <soap:operation soapAction="http://www.birst.com/saveRepository" style="document"/>
      <wsdl:input>
        <soap:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="saveStagingColumn">
      <soap:operation soapAction="http://www.birst.com/saveStagingColumn" style="document"/>
      <wsdl:input>
        <soap:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="removeStagingColumn">
      <soap:operation soapAction="http://www.birst.com/removeStagingColumn" style="document"/>
      <wsdl:input>
        <soap:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="renameStagingColumn">
      <soap:operation soapAction="http://www.birst.com/renameStagingColumn" style="document"/>
      <wsdl:input>
        <soap:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="saveHierarchy">
      <soap:operation soapAction="http://www.birst.com/saveHierarchy" style="document"/>
      <wsdl:input>
        <soap:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="removeHierarchy">
      <soap:operation soapAction="http://www.birst.com/removeHierarchy" style="document"/>
      <wsdl:input>
        <soap:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="saveLevel">
      <soap:operation soapAction="http://www.birst.com/saveLevel" style="document"/>
      <wsdl:input>
        <soap:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="saveDataSource">
      <soap:operation soapAction="http://www.birst.com/saveDataSource" style="document"/>
      <wsdl:input>
        <soap:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="saveDataSourceAndColumns">
      <soap:operation soapAction="http://www.birst.com/saveDataSourceAndColumns" style="document"/>
      <wsdl:input>
        <soap:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="saveAndValidateScript">
      <soap:operation soapAction="http://www.birst.com/saveAndValidateScript" style="document"/>
      <wsdl:input>
        <soap:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="executeScript">
      <soap:operation soapAction="http://www.birst.com/executeScript" style="document"/>
      <wsdl:input>
        <soap:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="removeDataSource">
      <soap:operation soapAction="http://www.birst.com/removeDataSource" style="document"/>
      <wsdl:input>
        <soap:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="EmptyDataSource">
      <soap:operation soapAction="http://www.birst.com/EmptyDataSource" style="document"/>
      <wsdl:input>
        <soap:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getRawData">
      <soap:operation soapAction="http://www.birst.com/getRawData" style="document"/>
      <wsdl:input>
        <soap:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetRepository">
      <soap:operation soapAction="http://www.birst.com/GetRepository" style="document"/>
      <wsdl:input>
        <soap:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="SetRepository">
      <soap:operation soapAction="http://www.birst.com/SetRepository" style="document"/>
      <wsdl:input>
        <soap:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getSchemaTables">
      <soap:operation soapAction="http://www.birst.com/getSchemaTables" style="document"/>
      <wsdl:input>
        <soap:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getCubes">
      <soap:operation soapAction="http://www.birst.com/getCubes" style="document"/>
      <wsdl:input>
        <soap:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getCube">
      <soap:operation soapAction="http://www.birst.com/getCube" style="document"/>
      <wsdl:input>
        <soap:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getTableSchema">
      <soap:operation soapAction="http://www.birst.com/getTableSchema" style="document"/>
      <wsdl:input>
        <soap:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="isConnected">
      <soap:operation soapAction="http://www.birst.com/isConnected" style="document"/>
      <wsdl:input>
        <soap:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="updateScheduledReport">
      <soap:operation soapAction="http://www.birst.com/updateScheduledReport" style="document"/>
      <wsdl:input>
        <soap:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="runScheduledReport">
      <soap:operation soapAction="http://www.birst.com/runScheduledReport" style="document"/>
      <wsdl:input>
        <soap:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="addScheduledReport">
      <soap:operation soapAction="http://www.birst.com/addScheduledReport" style="document"/>
      <wsdl:input>
        <soap:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getScheduledReports">
      <soap:operation soapAction="http://www.birst.com/getScheduledReports" style="document"/>
      <wsdl:input>
        <soap:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getScheduledReport">
      <soap:operation soapAction="http://www.birst.com/getScheduledReport" style="document"/>
      <wsdl:input>
        <soap:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="deleteScheduledReport">
      <soap:operation soapAction="http://www.birst.com/deleteScheduledReport" style="document"/>
      <wsdl:input>
        <soap:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="exportData">
      <soap:operation soapAction="http://www.birst.com/exportData" style="document"/>
      <wsdl:input>
        <soap:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getExportedDataFiles">
      <soap:operation soapAction="http://www.birst.com/getExportedDataFiles" style="document"/>
      <wsdl:input>
        <soap:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getReportList">
      <soap:operation soapAction="http://www.birst.com/getReportList" style="document"/>
      <wsdl:input>
        <soap:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="validateQuery">
      <soap:operation soapAction="http://www.birst.com/validateQuery" style="document"/>
      <wsdl:input>
        <soap:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getBirstConnectConfigs">
      <soap:operation soapAction="http://www.birst.com/getBirstConnectConfigs" style="document"/>
      <wsdl:input>
        <soap:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="deleteBirstConnectConfig">
      <soap:operation soapAction="http://www.birst.com/deleteBirstConnectConfig" style="document"/>
      <wsdl:input>
        <soap:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="addBirstConnectConfig">
      <soap:operation soapAction="http://www.birst.com/addBirstConnectConfig" style="document"/>
      <wsdl:input>
        <soap:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="renameBirstConnectConfig">
      <soap:operation soapAction="http://www.birst.com/renameBirstConnectConfig" style="document"/>
      <wsdl:input>
        <soap:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getVariables">
      <soap:operation soapAction="http://www.birst.com/getVariables" style="document"/>
      <wsdl:input>
        <soap:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="updateVariable">
      <soap:operation soapAction="http://www.birst.com/updateVariable" style="document"/>
      <wsdl:input>
        <soap:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="deleteVariable">
      <soap:operation soapAction="http://www.birst.com/deleteVariable" style="document"/>
      <wsdl:input>
        <soap:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getLogicalExpressions">
      <soap:operation soapAction="http://www.birst.com/getLogicalExpressions" style="document"/>
      <wsdl:input>
        <soap:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getAggregates">
      <soap:operation soapAction="http://www.birst.com/getAggregates" style="document"/>
      <wsdl:input>
        <soap:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="updateCustomMeasure">
      <soap:operation soapAction="http://www.birst.com/updateCustomMeasure" style="document"/>
      <wsdl:input>
        <soap:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="updateCustomAttribute">
      <soap:operation soapAction="http://www.birst.com/updateCustomAttribute" style="document"/>
      <wsdl:input>
        <soap:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="deleteLogicalExpression">
      <soap:operation soapAction="http://www.birst.com/deleteLogicalExpression" style="document"/>
      <wsdl:input>
        <soap:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getBucketedMeasures">
      <soap:operation soapAction="http://www.birst.com/getBucketedMeasures" style="document"/>
      <wsdl:input>
        <soap:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="updateBucketedMeasure">
      <soap:operation soapAction="http://www.birst.com/updateBucketedMeasure" style="document"/>
      <wsdl:input>
        <soap:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="deleteBucketedMeasure">
      <soap:operation soapAction="http://www.birst.com/deleteBucketedMeasure" style="document"/>
      <wsdl:input>
        <soap:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getMeasureList">
      <soap:operation soapAction="http://www.birst.com/getMeasureList" style="document"/>
      <wsdl:input>
        <soap:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getDataTables">
      <soap:operation soapAction="http://www.birst.com/getDataTables" style="document"/>
      <wsdl:input>
        <soap:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getLevelData">
      <soap:operation soapAction="http://www.birst.com/getLevelData" style="document"/>
      <wsdl:input>
        <soap:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getMeasureTableData">
      <soap:operation soapAction="http://www.birst.com/getMeasureTableData" style="document"/>
      <wsdl:input>
        <soap:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getStagingTableData">
      <soap:operation soapAction="http://www.birst.com/getStagingTableData" style="document"/>
      <wsdl:input>
        <soap:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getTableQueryString">
      <soap:operation soapAction="http://www.birst.com/getTableQueryString" style="document"/>
      <wsdl:input>
        <soap:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getGrainList">
      <soap:operation soapAction="http://www.birst.com/getGrainList" style="document"/>
      <wsdl:input>
        <soap:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getGrainColumnList">
      <soap:operation soapAction="http://www.birst.com/getGrainColumnList" style="document"/>
      <wsdl:input>
        <soap:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getLevelList">
      <soap:operation soapAction="http://www.birst.com/getLevelList" style="document"/>
      <wsdl:input>
        <soap:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getLevelColumnList">
      <soap:operation soapAction="http://www.birst.com/getLevelColumnList" style="document"/>
      <wsdl:input>
        <soap:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getColumnsAtLowerGrains">
      <soap:operation soapAction="http://www.birst.com/getColumnsAtLowerGrains" style="document"/>
      <wsdl:input>
        <soap:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="ping">
      <soap:operation soapAction="http://www.birst.com/ping" style="document"/>
      <wsdl:input>
        <soap:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetProcessInitData">
      <soap:operation soapAction="http://www.birst.com/GetProcessInitData" style="document"/>
      <wsdl:input>
        <soap:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="GetProgress">
      <soap:operation soapAction="http://www.birst.com/GetProgress" style="document"/>
      <wsdl:input>
        <soap:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
  </wsdl:binding>
  <wsdl:service name="AdminService">
    <wsdl:port binding="tns:AdminServiceSoap12" name="AdminServiceSoap12">
      <soap12:address location="http://localhost:57747/AdminService.asmx"/>
    </wsdl:port>
    <wsdl:port binding="tns:AdminServiceSoap" name="AdminServiceSoap">
      <soap:address location="http://localhost:57747/AdminService.asmx"/>
    </wsdl:port>
  </wsdl:service>
</wsdl:definitions>