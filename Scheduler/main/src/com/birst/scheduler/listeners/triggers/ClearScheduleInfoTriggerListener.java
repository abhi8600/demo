package com.birst.scheduler.listeners.triggers;

import org.apache.log4j.Logger;
import org.quartz.JobExecutionContext;
import org.quartz.Trigger;
import org.quartz.TriggerListener;
import org.quartz.Trigger.CompletedExecutionInstruction;

import com.birst.scheduler.utils.Util;
import com.birst.scheduler.utils.UtilConstants;

public class ClearScheduleInfoTriggerListener implements TriggerListener
{
	private static final Logger logger = Logger.getLogger(ClearScheduleInfoTriggerListener.class);
	
	private String name;
	
	public ClearScheduleInfoTriggerListener(String name)
	{
		this.name = name;
	}
	
	@Override
	public String getName()
	{
		return name;
	}
	
	@Override
	public void triggerComplete(Trigger trigger, JobExecutionContext context,
			CompletedExecutionInstruction triggerInstructionCode)
	{
		String runNowTriggerID = Util.getObjectID(trigger.getKey().getName(), UtilConstants.PREFIX_IMMEDIATE_TRIGGER);
		// log the run now separately
		// Any specific functin post run now
		if(runNowTriggerID != null && runNowTriggerID.length() > 0)
		{
			logger.info("Immediate firing completed for spaceID : " + trigger.getJobKey().getGroup() 
					+ " report ID " + trigger.getJobKey().getName());
		}
		/*
		if(triggerID != null)
		{
			Connection conn = null;
			String schema = UtilConstants.SCHEDULER_DB_SCHEMA;
			try
			{				
				conn = SchedulerDBConnection.getInstance().getConnectionPool().getConnection();			
				if(triggerID != null)
				{
					ScheduleInfo si = SchedulerDatabase.getScheduledInfo(conn, schema, triggerID);
					if(si != null && si.getInterval() == ScheduleInfo.INTERVAL_ONCE)
					{
						SchedulerDatabase.removeScheduleInfo(conn, schema, triggerID);
					}					
				}				
			}
			catch(Exception ex)			
			{
				logger.error("Exception in clearning schedule info " , ex );					
			}
			finally
			{
				try
				{
					if(conn != null)
						conn.close();
				}
				catch(Exception ex)
				{}
					
			}						
		}
		*/
		
	}

	@Override
	public void triggerFired(Trigger trigger, JobExecutionContext context)
	{
	}

	@Override
	public void triggerMisfired(Trigger trigger)
	{			
	}

	@Override
	public boolean vetoJobExecution(Trigger trigger, JobExecutionContext context)
	{
		return false;
	}

}
