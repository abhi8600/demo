package com.birst.scheduler.listeners.jobs;

import java.util.Calendar;
import java.util.TimeZone;
import java.util.UUID;

import org.apache.log4j.Logger;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.JobListener;
import org.quartz.Scheduler;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.TriggerKey;

import com.birst.scheduler.SchedulerServerContext;
import com.birst.scheduler.utils.Util;

public class ConfigureRetryJob implements JobListener
{
	
	private static final Logger logger = Logger.getLogger(ConfigureRetryJob.class);


	public static final String KEY_RETRY_JOB = "RetryJob";	
	public static final String KEY_RETRY_JOB_PARENT_NEXT_FIRETIME = "ParentJobNextFireTime";
	public static final String KEY_RETRY_JOB_PARENT_INTERVAL = "ParentJobCronInterval";
	public static final String KEY_RETRY_JOB_PARENT_TIME_ZONE = "ParentJobTimeZone";
	
	private String name;
	
	public ConfigureRetryJob(String name)
	{
		this.name = name;
	}
	
	@Override
	public String getName()
	{
		return name;
	}

	@Override
	public void jobExecutionVetoed(JobExecutionContext context)
	{
	}

	@Override
	public void jobToBeExecuted(JobExecutionContext context)
	{
	}

	@Override
	public void jobWasExecuted(JobExecutionContext context,
			JobExecutionException jobException)
	{		
		try
		{			
			
			JobDataMap triggerJobDataMap = context.getTrigger().getJobDataMap();
			String retryJob = (String) triggerJobDataMap.get(ConfigureRetryJob.KEY_RETRY_JOB);
			boolean configureRetry = false;
			if(retryJob != null && retryJob.length() > 0)
			{				
				configureRetry = Boolean.parseBoolean(retryJob);
			}
			
			if(configureRetry)
			{
				// Get the next firetime of the original job. This is to make sure we don't step on the time
				// Keep on retrying till the parent's next firetime is reached  
				
				String parentJobNextFireTime = (String) triggerJobDataMap.get(KEY_RETRY_JOB_PARENT_NEXT_FIRETIME);
				String parentJobInterval = (String) triggerJobDataMap.get(KEY_RETRY_JOB_PARENT_INTERVAL);
				//String parentJobTimeZone = (String) triggerJobDataMap.get(KEY_RETRY_JOB_PARENT_TIME_ZONE);
				if(parentJobNextFireTime != null && parentJobNextFireTime.length() > 0)
				{					
					//TimeZone tz = TimeZone.getTimeZone(parentJobTimeZone);
					// make sure that the parent next fire time is in future after retry time
					Calendar parentFireTime = Calendar.getInstance();
					parentFireTime.setTimeInMillis(Long.valueOf(parentJobNextFireTime));
					
					Calendar retryTime = Calendar.getInstance();
					retryTime.add(Calendar.SECOND, Util.getJobRetryTimeRange(parentJobInterval));
					
					if(parentFireTime.compareTo(retryTime) > 0 )
					{	
						String triggerName = Util.getRetryTriggerName(UUID.randomUUID().toString());
						String triggerGroup = context.getTrigger().getKey().getGroup();
						TriggerKey triggerKey = new TriggerKey(triggerName, triggerGroup);
						
						Scheduler scheduler = SchedulerServerContext.getSchedulerInstance();
						Trigger trigger = TriggerBuilder.newTrigger()
						.withIdentity(triggerKey)
						.forJob(context.getJobDetail())
						.startAt(retryTime.getTime())						
						.build();
						// For multiple retries, need to make sure that the original job's trigger next fire time
						// is passed to each retry execution
						trigger.getJobDataMap().put(KEY_RETRY_JOB_PARENT_NEXT_FIRETIME, String.valueOf(parentFireTime.getTimeInMillis()));
						trigger.getJobDataMap().put(KEY_RETRY_JOB_PARENT_INTERVAL, parentJobInterval);
						
						scheduler.scheduleJob(trigger);
						logger.info("Retry time " + retryTime.getTime().toString() + "  set for space id : " + context.getTrigger().getKey().getGroup() +
								" job name " + context.getJobDetail().getKey().getName());
					}
					else
					{
						// clear out the job data map info for the parent next fire time
						triggerJobDataMap.remove(KEY_RETRY_JOB);
						triggerJobDataMap.remove(KEY_RETRY_JOB_PARENT_NEXT_FIRETIME);
						triggerJobDataMap.remove(KEY_RETRY_JOB_PARENT_INTERVAL);
						triggerJobDataMap.remove(KEY_RETRY_JOB_PARENT_TIME_ZONE);
					}
				}
			}
		}catch(Exception ex)
		{
			logger.error("Exception in Configuring Retry Job " , ex );
		}
	}

}
