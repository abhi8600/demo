package com.birst.scheduler.listeners.jobs;


import java.sql.SQLException;
import java.sql.Connection;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

import org.apache.log4j.Logger;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.JobListener;

import com.birst.scheduler.utils.SchedulerDBConnection;
import com.birst.scheduler.utils.SchedulerDatabase;
import com.birst.scheduler.utils.Util;
import com.birst.scheduler.utils.UtilConstants;
import com.birst.scheduler.utils.UtilConstants.RunningJobStatus;

public class PersistJobHistoryInDB implements JobListener
{
	private static final Logger logger = Logger.getLogger(PersistJobHistoryInDB.class);	
	
	public static final String KEY_HISTORY_ID = "historyID";	
	public static final String KEY_STATUS = "status";
	public static final String KEY_ERROR_MESSAGE = "errorMessage";
	
	private String name;
	
	public PersistJobHistoryInDB(String name)
	{
		this.name = name;
	}
	
	@Override
	public String getName()
	{		
		return name;
	}

	@Override
	public void jobExecutionVetoed(JobExecutionContext arg0)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void jobToBeExecuted(JobExecutionContext arg0)
	{
		try
		{
			JobDataMap triggerJobDataMap = arg0.getTrigger().getJobDataMap();
			String historyID = UUID.randomUUID().toString();
			triggerJobDataMap.put(PersistJobHistoryInDB.KEY_HISTORY_ID, historyID);
			String jobID = Util.findJobID(arg0.getJobDetail().getKey().getName());//Util.getObjectID(arg0.getJobDetail().getKey().getName(), UtilConstants.PREFIX_SCHEDULED_REPORT_JOB);
			String scheduleID = Util.findTriggerID(arg0.getTrigger().getKey().getName());//Util.getObjectID(arg0.getTrigger().getKey().getName(), UtilConstants.PREFIX_SCHEDULED_REPORT_TRIGGER);
			Connection conn = null;
			String schema = UtilConstants.SCHEDULER_DB_SCHEMA;
			try
			{
				Date date = new Date();				
				Timestamp startTime = new Timestamp(date.getTime());
				conn = SchedulerDBConnection.getInstance().getConnectionPool().getConnection();
				
				SchedulerDatabase.addJobHistoryEntry(conn, schema, historyID,
						jobID, scheduleID, startTime, null, RunningJobStatus.RUNNING.getStatus(), null);
			} catch (SQLException sqe)
			{
				logger.error("SQLException in jobToBeExecuted ", sqe);
			} finally
			{
				try
				{
					if (conn != null)
						conn.close();
				} catch (Exception ex)
				{
				}

			}

		} catch (Exception ex)
		{
			logger.error("Exception in jobToBeExecuted ", ex);
		}
	}

	@Override
	public void jobWasExecuted(JobExecutionContext arg0,
			JobExecutionException arg1)
	{
		try
		{
			JobDataMap triggerJobDataMap = arg0.getTrigger().getJobDataMap();
			String historyID = (String) triggerJobDataMap.get(PersistJobHistoryInDB.KEY_HISTORY_ID);
			Integer status = (Integer) triggerJobDataMap.get(PersistJobHistoryInDB.KEY_STATUS);						
			String errorMessage = (String) triggerJobDataMap.get(PersistJobHistoryInDB.KEY_ERROR_MESSAGE);
			if(historyID != null && status != null && historyID.length() > 0)
			{				
				Connection conn = null;
				String schema = UtilConstants.SCHEDULER_DB_SCHEMA;
				try
				{
					Date date = new Date();				
					Timestamp endTime = new Timestamp(date.getTime());;
					conn = SchedulerDBConnection.getInstance().getConnectionPool().getConnection();					
					SchedulerDatabase.updateJobHistory(conn, schema, historyID, endTime, status.intValue(), errorMessage);					
				}
				catch(SQLException sqe)			
				{
					logger.error("SQLException in updating " , sqe );					
				}
				finally
				{
					try
					{
						if(conn != null)
							conn.close();
					}
					catch(Exception ex)
					{}
						
				}				
			}
		}catch(Exception ex)
		{
			logger.error("Exception in jobToBeExecuted " , ex );
		}
	}

}
