package com.birst.scheduler.utils;

public class CompareUtils
{
	
	public static boolean compare(String str1, String str2, boolean ignoreCase)
	{
		if(str1 == null && str2 == null){ return true;}
		if(str1 == null && str2 != null) { return false;}
		if(str1 != null && str2 == null) { return false;}		
		if(ignoreCase)
		{
			if(!str1.equalsIgnoreCase(str2)) { return false;}
		}
		else
		{
			if(!str1.equals(str2)) { return false;}
		}
		return true;
	}

	public static boolean compare(String[] array1, String[] array2, boolean ignoreCase)
	{
		if(array1 == null && array2 == null){ return true;}
		if(array1 == null && array2 != null) { return false;}
		if(array1 != null && array2 == null) { return false;}
		if(array1.length != array2.length) { return false;}
		
		for(String str1 : array1)
		{
			boolean exists = false;
			for(String str2 : array2)
			{
				exists = ignoreCase ? str1.equalsIgnoreCase(str2) : str1.equals(str2);						
				if(exists)
				{
					break;
				}
			}
			if(!exists)
			{
				// if any string is not, return false
				return false;
			}
		}
		return true;
	}
}
