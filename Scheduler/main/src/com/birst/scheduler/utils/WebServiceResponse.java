package com.birst.scheduler.utils;

public abstract class WebServiceResponse
{
	// 0 is used for success. Should not be a default value
	public int ErrorCode = -1;
	public String ErrorMessage;
}
