package com.birst.scheduler.utils;

import java.sql.SQLException;

import org.apache.log4j.Logger;

import com.birst.scheduler.exceptions.SchedulerBaseException;

public class AcornAdminDBConnection 
{	

		private static final Logger logger = Logger.getLogger(AcornAdminDBConnection.class);
		private static final String POOL_TYPE = "AcornAdmin";
		private static AcornAdminDBConnection _instance;		
		private DBConnectionPool connectionPool;
		
		private static String _name;
		private static String _driver;
		private static String _connectString;
		private static String _userName;
		private static String _password;
		private static int _maxConnections;
		private static String _dbType;
		
		public static AcornAdminDBConnection getInstance() throws SchedulerBaseException, SQLException, ClassNotFoundException
		{
			if(_instance == null || _instance.connectionPool != null)
			{
				getInstance(_name, _driver, _connectString, _userName, _password, _maxConnections, _dbType);
			}
			
			return _instance;
		}
		
		public synchronized static AcornAdminDBConnection getInstance(String name,
				String driver, String connectString, String userName,
				String password, int maxConnections, String dbType) throws SQLException, ClassNotFoundException 
		{
			_name = name;
			_driver = driver;
			_connectString = connectString;
			_userName = userName;
			_password = password;
			_maxConnections = maxConnections;
			_dbType = dbType;
			
			if(_instance == null)
			{
				_instance = new AcornAdminDBConnection();
				_instance.connectionPool = DBConnectionPool.getInstance(_name, _driver, _connectString, _userName, _password, _maxConnections, _dbType, POOL_TYPE);
			}		
			return _instance;
		}
		
		public DBConnectionPool getConnectionPool()
		{
			return connectionPool;
		}
}
