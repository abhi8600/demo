package com.birst.scheduler.utils;

public class JobConstants
{
	public static final String SCHEDULER_NAME = "SchedulerName";
	public static final String SCHEDULER_DISPLAY_NAME = "SchedulerDisplayName";
	public static final String SCHEDULER_ENABLE_MIGRATION = "SchedulerMigration";
	public static final String IS_PRIMARY_SCHEDULER = "PrimaryScheduler";
	public static final String PRIMARY_SCHEDULER_NAME = "PrimarySchedulerName";
	public static final String JOBS_LIST_NODE = "ScheduledJobList";
	public static final String JOB_NODE = "ScheduledJob";
	public static final String JOB_ID = "Id";
	public static final String JOB_DS_ID = "DataSourceId";
	public static final String JOB_NAME = "Name";
	public static final String JOB_SUMMARY = "Summary";
	public static final String JOB_RUN = "Run";
	public static final String JOB_TYPE = "Type";
	public static final String JOB_SPACE_ID = "SpaceID";
	public static final String JOB_USER_ID = "UserID";
	public static final String JOB_EXECUTION_STATUS = "ExecutionStatus";
	public static final String JOB_SCHEDULE_LIST = "ScheduleList";
	public static final String JOB_DETAILS = "JobDetails";
	public static final String JOB_REPORTS_LIST = "ReportList";
	public static final String JOB_SCHEDULE = "Schedule";
	public static final String JOB_COMMENTS = "Comments";
	public static final String JOB_CREATED_USER = "CreatedUser";
	public static final String JOB_CREATED_DATE = "CreatedDate";
	public static final String JOB_MODIFIED_DATE = "ModifiedDate";
	public static final String JOB_MODIFIED_BY = "ModifiedBy";
	
	public static final String JOB_SCHEDULE_ID = "ScheduleId";
	public static final String JOB_SCHEDULE_EXPRESSION = "Cron";
	public static final String JOB_SCHEDULE_ENABLE = "Enable";
	public static final String JOB_SCHEDULE_TIMEZONE = "TimeZone";
	public static final String JOB_SCHEDULE_JOB_ID = "JobId";
	public static final String JOB_SCHEDULE_OLD_SFDC = "MultipleJobIDs";
	public static final String JOB_SCHEDULE_START_TIME = "StartTime";
	public static final String JOB_SEND_EMAIL = "SendEmail";	
	public static final String JOB_CONN_SCHED_NAME = "Name";
	public static final String JOB_CONN_TYPE = "ConnectorType";
	public static final String JOB_CONN_EXTRACT_GROUPS = "ExtractGroups";
	public static final String JOB_CONN_EXTRACT_ENGINE = "ExtractionEngine";
	public static final String INIT_PROPERTIES = "InitProperties";
	public static final String INIT_PROP_EMAIL_FROM = "scheduleFrom";
	public static final String INIT_PROP_EMAIL_SUBJECT = "scheduleSubject";
	
	/*
	String spaceID, String userID, 
	String report, String triggerReport, String toReport, String type,
	String subject, String body, BirstStringArray toList, 
	BirstStringArray schedules
	*/
	// Scheduled Report
	
	// response and request xml structure
	// If any name is changed, both the response parsing and request sending 
	// will need to be changed on the client side
	public static final String JOB_SR_NODE = "ScheduledReport";
	public static final String JOB_SR_REPORT_PATH = "Report";
	public static final String JOB_SR_REPORT_TYPE = "ReportType";
	public static final String JOB_SR_CSV_SEPARATOR = "CsvSeparator";
	public static final String JOB_SR_CSV_EXTENSION = "CsvExtension";
	public static final String JOB_SR_FILE_NAME = "FileName";
	public static final String JOB_SR_COMPRESSION = "Compression";
	public static final String JOB_SR_TRIGGER_REPORT = "TriggerReport";
	public static final String JOB_SR_TOREPORT = "ToReport";
	public static final String JOB_SR_SUBJECT = "Subject";
	public static final String JOB_SR_BODY = "Body";
	public static final String JOB_SR_TO_LIST = "ToList";	
	public static final String JOB_SR_EMAIL = "Email";
	public static final String JOB_SR_FROM = "From";
	public static final String JOB_SR_CREATED_ADMIN_MODE = "CreatedAdminMode";
	public static final String JOB_SR_PROMPTS = "Prompts";
	public static final String JOB_SR_OVERRIDE_VARIABLES = "OverrideVariables";
	public static final String JOB_SR_VARIABLE = "Variable";
	
	
	// Job History 
	public static final String JOB_HISTORY_JOB_NODE = "ScheduleJob";
	public static final String JOB_HISTORY_JOB_ID = "JobId";
	public static final String JOB_HISTORY_LIST = "JobHistoryList";
	public static final String JOB_HISTORY_NODE = "JobHistory";
	public static final String JOB_HISTORY_SCHEDULE_ID = "ScheduleId";
	public static final String JOB_HISTORY_START_TIME = "StartTime";
	public static final String JOB_HISTORY_END_TIME = "EndTime";
	public static final String JOB_HISTORY_STATUS = "Status";
	public static final String JOB_HISTORY_ERR_MSG = "ErrorMessage";
	
	// Copy Job
	public static final String JOB_COPY_NODE = "CopyJobInfo";
	public static final String JOB_COPY_ID = "JobId";
	public static final String JOB_COPY_FROM = "FromScheduler";
	public static final String JOB_COPY_TO = "ToScheduler";	
	public static final String JOB_COPY_DELETE_FROM = "DeleteFrom";	
	
	// Process Info 
	public static final String JOB_PR_NODE = "ProcessInfo";
	public static final String JOB_SP_LOADDATE = "LoadDate";
	public static final String JOB_SP_SUBGROUPS = "SubGroups";
	public static final String JOB_SP_RETRY_FAILED_LOAD = "RetryFailedLoad";
	
	// Connector Info
	public static final String JOB_CONN_NODE = "ConnectorInfo";
	public static final String JOB_CONN_RUN_NOW_MODE = "RunNowMode";
	public static final String JOB_CONN_PROCESS_AFTER = "ProcessAfter";
	public static final String JOB_CONN_CLIENT_ID = "ClientId";
	public static final String JOB_CONN_SANDBOX_URL = "SandBoxUrl";
	
	// Space status info elements
	public static final String JOB_SP_STATUS_NODE = "SpaceStatus";
	public static final String JOB_SP_LOAD_ID = "LoadId";
	public static final String JOB_SP_LOAD_GROUP = "LoadGroup";
	public static final String JOB_SP_STEP = "Step";
	public static final String JOB_SP_STATUS = "Status";
	public static final String JOB_SP_AVAILABLE = "Available";
	public static final String JOB_SP_CREATED_DATE = "CreatedDate";
	public static final String JOB_SP_MODIFIED_DATE = "ModifiedDate";
	
	// Delete Info
	public static final String JOB_DL_NODE = "DeleteInfo";
	public static final String JOB_DL_RESTORE = "Restore";
	public static final String JOB_DL_TYPE = "DeleteType";
	
	// BDS info elements.
	public static final String JOB_BDS_NODE = "BDSInfo";
}
