package com.birst.scheduler.utils;

import java.sql.Connection;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import com.birst.scheduler.exceptions.SchedulerBaseException;

public class SchedulerConnectionPool 
{
	/*
	private static final Logger logger = Logger.getLogger(SchedulerConnectionPool.class);
	private static SchedulerConnectionPool _instance;
	
	private ConnectionPool connectionPool;
	@SuppressWarnings("unused")
	private DatabaseConnection databaseConnection;
	
	public static SchedulerConnectionPool getInstance() throws SchedulerBaseException
	{
		if(_instance == null)
		{
			throw new SchedulerBaseException("Scheduler Connection Pool not configured properly");
		}
		return _instance;
	}
	
	public static SchedulerConnectionPool getInstance(String driver, String connectString, String userName, String password, int maxConnections) throws SchedulerBaseException
	{
		if(_instance == null)
		{
			_instance = new SchedulerConnectionPool();
			DatabaseConnection dbc = new DatabaseConnection();			
			dbc.Driver = driver;
			dbc.ConnectString = connectString;
			dbc.UserName = userName;
			dbc.Password = password;	
			dbc.ConnectionPoolSize = maxConnections;
			dbc.DBType = DatabaseType.MSSQL2005;
			dbc.Schema = "dbo";
			_instance.databaseConnection = dbc;
			try
			{
				_instance.connectionPool = ConnectionPool.getInstance(dbc, "CST_Quartz_dbPool", driver, connectString, userName, password, maxConnections);
			} catch (Exception e)
			{
				logger.error("Error in configuring connection Pool", e);
				throw new SchedulerBaseException("Unable to get ConnectionPool instance");
			}
		}		
		return _instance;
	}
	
	public Connection getConnection() throws SQLException
	{		
		Connection conn = null;
		if(connectionPool != null)
		{
			conn = connectionPool.getConnection();
		}
		
		return conn;
	}
	*/
}
