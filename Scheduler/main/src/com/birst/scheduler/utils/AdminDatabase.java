package com.birst.scheduler.utils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.apache.log4j.Logger;

import com.birst.scheduler.Migration.MigrationUtils;
import com.birst.scheduler.Report.ScheduledReport;
import com.birst.scheduler.shared.Schedule;

public class AdminDatabase
{
	public static final String PREFERENCES_TABLE = "PREFERENCES";	
	public static final String USERS_TABLE = "USERS";
	public static final String SPACES_TABLE = "SPACES";
	public static final String LOAD_SCHEDULE_TABLE = "LOAD_SCHEDULE";
	public static final String SCHEDULED_REPORTS_TABLE = "SCHEDULED_REPORTS";
	public static final Logger logger = Logger.getLogger(AdminDatabase.class);
	public static final String ADMIN_DB_SCHEMA = "dbo";
	/**
	 * Get the global preference if user preference is not found
	 * @param conn
	 * @param schema
	 * @param preferenceKeys
	 * @return
	 */
	public static String getMergedPreferences(Connection conn, String schema, String userID, String preferenceKey)
	{
		String value = null;
		List<String> preferenceKeyList = new ArrayList<String>();
		preferenceKeyList.add(preferenceKey);
		Map<String,String> map = getMergedPreferences(conn, schema, userID, preferenceKeyList);
		if(map != null && map.size() > 0)			
		{
			value = map.get(preferenceKey);
		}
		return value;
	}
	
	public static Map<String, String> getMergedPreferences(Connection conn, String schema, String userID, List<String> preferenceKeys)	
	{
		Map<String,String> resultMap = null;
		PreparedStatement pstmt = null;
		try
		{
			List<String> toRetrieveKeys = null;
			resultMap = getUserPreferences(conn, schema, userID, preferenceKeys);
			if(resultMap == null)
			{	
				resultMap = new HashMap<String,String>();
			}
			
			// check if any global reference keys are to be obtained
			toRetrieveKeys = new ArrayList<String>(preferenceKeys);
			// remove any keys that are already obtained at the user level
			for(String key : resultMap.keySet())
			{
				toRetrieveKeys.remove(key);
			}
			
			if(toRetrieveKeys != null && toRetrieveKeys.size() > 0)
			{
				String command = "SELECT PreferenceKey, PreferenceValue FROM " + schema + "." + PREFERENCES_TABLE
				+ " WHERE PreferenceType=? AND USER_ID=? AND SPACE_ID=? AND PreferenceKey IN (";
				for(int i =0; i < toRetrieveKeys.size() ; i++)
				{
					command = command + "?,";
				}
				command = command.substring(0, command.length() - 1);
				command = command + ")";
				int queryParamIndex = 1;
				pstmt = conn.prepareStatement(command);
				pstmt.setInt(queryParamIndex++, 0); // 1 is for user type
				pstmt.setString(queryParamIndex++, UtilConstants.EMPTY_GUID);
				pstmt.setString(queryParamIndex++, UtilConstants.EMPTY_GUID);

				for(String str : toRetrieveKeys)
				{
					pstmt.setString(queryParamIndex++, str);
				}
				ResultSet resultSet = pstmt.executeQuery();
				if(resultSet != null)
				{
					while(resultSet.next())
					{
						String preferenceKey = resultSet.getString(1);
						String preferenceValue = resultSet.getString(2);
						resultMap.put(preferenceKey, preferenceValue);					
					}
				}
			}
		}
		catch(Exception ex)
		{			
		}
		finally
		{
			try
			{
				if(pstmt != null)
				{
					pstmt.close();
				}
			}catch(Exception ex2)
			{
				logger.debug(ex2,ex2);
			}
		}
		return resultMap;
	}
	
	public static String getUserPreference(Connection conn, String schema, String userID, String preferenceKey )
	{
		String value = null;
		List<String> preferenceKeyList = new ArrayList<String>();
		preferenceKeyList.add(preferenceKey);
		Map<String,String> map = getUserPreferences(conn, schema, userID, preferenceKeyList);
		if(map != null && map.size() > 0)			
		{
			value = map.get(preferenceKey);
		}
		return value;
	}
	
	public static Map<String, String> getUserPreferences(Connection conn, String schema, String userID, List<String> preferenceKeys )
	{
		Map<String,String> resultMap = null;
		PreparedStatement pstmt = null;
		try
		{
			resultMap = new HashMap<String,String>();
			String command = "SELECT PreferenceKey, PreferenceValue FROM " + schema + "." + PREFERENCES_TABLE
                + " WHERE PreferenceType=? AND USER_ID=? AND SPACE_ID=? AND PreferenceKey IN (";
			for(int i =0; i < preferenceKeys.size() ; i++)
			{
				command = command + "?,";
			}
			command = command.substring(0, command.length() - 1);
			command = command + ")";
			int queryParamIndex = 1;
			pstmt = conn.prepareStatement(command);
			pstmt.setInt(queryParamIndex++, 1); // 1 is for user type
			pstmt.setString(queryParamIndex++, userID);
			pstmt.setString(queryParamIndex++, UtilConstants.EMPTY_GUID);
			
			
			for(String str : preferenceKeys)
			{
				pstmt.setString(queryParamIndex++, str);
			}
			ResultSet resultSet = pstmt.executeQuery();
			if(resultSet != null)
			{
				while(resultSet.next())
				{
					String preferenceKey = resultSet.getString(1);
					String preferenceValue = resultSet.getString(2);
					resultMap.put(preferenceKey, preferenceValue);					
				}
			}
		}
		catch(SQLException ex)
		{
			try
			{
				
			}
			catch(Exception ex2)
			{
				logger.debug(ex2,ex2);
			}
		}
		finally
		{
			
		}
		
		return resultMap;
	}
	
	public static List<PackageSpaceProperties> getPackageSpaceProperties(Connection conn, String schema, String spaceID) throws SQLException
	{
		List<PackageSpaceProperties> response = new ArrayList<PackageSpaceProperties>();
		/*
		SELECT A.PACKAGE_ID, A.PARENT_SPACE_ID, B.Name, B.Directory, A.Available 
		FROM PACKAGE_USAGE_INXN A
		INNER JOIN SPACES B ON A.PARENT_SPACE_ID = B.ID 
		WHERE A.CHILD_SPACE_ID ='b2bae6b4-a265-42db-914f-b52723be46d8' 
		GROUP BY A.PACKAGE_ID, A.PARENT_SPACE_ID, B.Name, B.Directory, A.Available;
		*/
		PreparedStatement pstmt = null;		
		try
		{
			String command = "SELECT A.PACKAGE_ID, A.PARENT_SPACE_ID, B.Name, B.Directory " +
					"FROM " +  schema + "." + " PACKAGE_USAGE_INXN A " +
					"INNER JOIN " +  schema + "." + " SPACES B ON A.PARENT_SPACE_ID = B.ID " +
					"WHERE A.CHILD_SPACE_ID = ? AND A.Available = 1" +
					"GROUP BY A.PACKAGE_ID, A.PARENT_SPACE_ID, B.Name, B.Directory";
			
			pstmt = conn.prepareStatement(command);
			pstmt.setString(1, spaceID);
			ResultSet set = pstmt.executeQuery();
			
			
			while (set.next())
			{	
				PackageSpaceProperties packageSpaceProperties = new PackageSpaceProperties();
				String packageID = set.getString(1);				
				String packageSpaceID = set.getString(2);
				String packageSpaceName = set.getString(3);
				String packageSpaceDirectory = set.getString(4);
				packageSpaceProperties.setPackageID(packageID);
				packageSpaceProperties.setSpaceID(packageSpaceID);
				packageSpaceProperties.setSpaceName(packageSpaceName);
				packageSpaceProperties.setSpaceDirectory(packageSpaceDirectory);
				response.add(packageSpaceProperties);
			}
		}
		catch (SQLException e)
		{
			logger.error("Error while retreiving package space properties info information", e);	
			throw e;
		}
		finally 
		{
			try
			{
				if (pstmt != null)
					pstmt.close();
			}
			catch (Exception e)
			{
				logger.debug("Error in pstmt.close()", e );
			}
		}
		return response;
	}
	
	public static Map<String,String> getUserInfo(Connection conn, String schema, String userID) throws SQLException
	{		
		Map<String,String> resultMap = null;
		PreparedStatement pstmt = null;		
		try
		{
			resultMap = new HashMap<String, String>();
			String command = "SELECT Username,Email FROM "
								+ schema + "." + USERS_TABLE + " WHERE PKID = ?";

			pstmt = conn.prepareStatement(command);
			pstmt.setString(1, userID);
			ResultSet set = pstmt.executeQuery();
			
			int count = 0;
			while (set.next())
			{
				count++;
				if(count > 1)
				{
					// Only one row should be returned for this query. Something is wrong if it returns multiple rows.
					logger.warn("Returning multiple entries for users " + userID);
					return null;
				}
				
				String username = set.getString(1);				
				String email = set.getString(2);
				resultMap.put(UtilConstants.KEY_USERNAME, username);				
				resultMap.put(UtilConstants.KEY_USEREMAIL, email);
			}
		}
		catch (SQLException e)
		{
			logger.error("Error while retreiving schedule info information", e);	
			throw e;
		}
		finally 
		{
			try
			{
				if (pstmt != null)
					pstmt.close();
			}
			catch (Exception e)
			{
				logger.debug("Error in pstmt.close()", e );
			}
		}
		
		return resultMap;
	}
	
	public static Map<String,String> getSpaceInfo(Connection conn, String schema, String spaceID) throws SQLException
	{		
		Map<String,String> resultMap = null;
		PreparedStatement pstmt = null;		
		try
		{
			resultMap = new HashMap<String, String>();
			String command = "SELECT Name, Directory, Deleted FROM "
								+ schema + "." + SPACES_TABLE + " WHERE ID = ?";

			pstmt = conn.prepareStatement(command);
			pstmt.setString(1, spaceID);
			ResultSet set = pstmt.executeQuery();
			int count = 0;
			while (set.next())
			{
				count++;
				if(count > 1)
				{
					// Only one row should be returned for this query. Something is wrong if it returns multiple rows.
					logger.warn("Returning multiple entries for space " + spaceID);
					return null;
				}
				
				String spaceName = set.getString(1);				
				String spaceDirectory = set.getString(2);
				boolean deleted = set.getBoolean(3);
				// if deleted don't put any other parameter
				if(deleted)
				{
					resultMap.put(UtilConstants.KEY_SPACE_DELETE, "true");
				}
				else
				{
					resultMap.put(UtilConstants.KEY_SPACE_NAME, spaceName);				
					resultMap.put(UtilConstants.KEY_SPACE_DIRECTORY, spaceDirectory);					
				}
				
			}
			
		}
		catch (SQLException e)
		{
			logger.error("Error while retreiving schedule info information", e);	
			throw e;
		}
		finally 
		{
			try
			{
				if (pstmt != null)
					pstmt.close();
			}
			catch (Exception e)
			{
				logger.debug("Error in pstmt.close()", e );
			}
		}
		
		return resultMap;
	}
	
	public static Map<String, List<ScheduledReport>> getOldFormatScheduledReport(Connection conn, String schema) throws Exception
	{
		Map<String,List<ScheduledReport>> resultMap = null;
		PreparedStatement pstmt = null;		
		ResultSet resultSet = null;
		try
		{
			resultMap = new HashMap<String, List<ScheduledReport>>();
			String command = "SELECT A.SPACE_ID, A.SCHEDULE_ID, A.USER_ID, A.ReportPath, A.TriggerReportPath, " +
							"A.ToReportPath, A.Type, A.Subject, A.Body, A.List, A.Interval, A.DayOfWeek," +
							" A.DayOfMonth, A.Hour, A.Minute " +
							" FROM " +
							schema + "." + SCHEDULED_REPORTS_TABLE + " A, " +
							schema + "." + LOAD_SCHEDULE_TABLE + " B " + 
							" WHERE " + 
							"B.MODULE = 'SCHEDULEDREPORT'" +
							" AND A.SCHEDULE_ID = B.OBJ_ID " +
							"AND A.SPACE_ID = B.SPACE_ID";
			pstmt = conn.prepareStatement(command);
			resultSet = pstmt.executeQuery();
			while (resultSet.next())
			{				
				String spaceID = (String)resultSet.getObject(1);
				String jobID = (String)resultSet.getObject(2); 
				String userID = (String)resultSet.getObject(3);
				String reportPath = resultSet.getString(4);
				String triggerReportPath = resultSet.getString(5);
				String toReportPath = resultSet.getString(6);
				String type = resultSet.getString(7);
				String subject = resultSet.getString(8);
				String body = resultSet.getString(9);
				String list = resultSet.getString(10);
				int interval = resultSet.getInt(11);
				int dayOfWeek = resultSet.getInt(12);
				int dayOfMonth = resultSet.getInt(13);
				int hour = resultSet.getInt(14);
				int minute = resultSet.getInt(15);
				ScheduledReport sr = new ScheduledReport();
				sr.setSpaceID(spaceID);
				sr.setId(jobID);
				sr.setUserID(userID);
				sr.setReportPath(reportPath);
				sr.setTriggerReportPath(triggerReportPath);
				sr.setToReportPath(toReportPath);
				sr.setType(type);
				sr.setEmailBody(body);
				sr.setSubject(subject);
				String[] toList = list.split(";");
				sr.setToList(toList);
				
				String expression = MigrationUtils.getCron(interval, dayOfWeek, dayOfMonth, hour, minute);
				Schedule schedule = new Schedule();
				// IMP IMP..make sure when we migrate from old to new scheduler
				// we disable all the triggers.
				schedule.setEnable(false);
				schedule.setExpression(expression);
				schedule.setId(UUID.randomUUID().toString());
				schedule.setJobId(jobID);
				schedule.setId(UUID.randomUUID().toString());
				schedule.setTimeZone(Schedule.TIMEZONE_CENTRAL);
				sr.setJobSchedules(new Schedule[] {schedule});
				
				List<ScheduledReport> reports = resultMap.get(spaceID);
				if(reports == null || reports.size() == 0)
				{
					reports = new ArrayList<ScheduledReport>();
					resultMap.put(spaceID, reports);
				}
				
				reports.add(sr);
			}
			return resultMap;
		}
		catch (SQLException e)
		{
			logger.error("Error while retreiving schedule info information", e);	
			throw e;
		}
		finally 
		{
			try
			{
				if(resultSet != null)
				{
					resultSet.close();
				}
				
				if (pstmt != null)
					pstmt.close();
			}
			catch (Exception e)
			{
				logger.debug("Error in closing db command obejcts ", e );
			}
		}
	}
}
