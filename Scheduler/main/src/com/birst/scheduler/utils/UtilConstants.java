package com.birst.scheduler.utils;

public class UtilConstants
{	
	public static final String EMPTY_GUID = "00000000-0000-0000-0000-000000000000";
	// Login Webservice constants
	
	public static final String LOGIN_SPACE_ID = "spaceID";
	public static final String LOGIN_USER_ID = "userID";
	public static final String LOGIN_USER_NAME = "userName";
	public static final String LOGIN_SPACE_REPORT_EMAIL_FROM = "scheduleFrom";
	public static final String LOGIN_SPACE_REPORT_EMAIL_SUBJECT = "scheduleSubject";
	public static final String LOGIN_USER_OPERATIONS_MODE = "operationsUserMode";
	public static final String SESSION_USER_BEAN ="schedulerUserBean"; 
	
	
	public static final String PREFIX_RETRY = "RT-";
	// Scheduled Report
	public static final String PREFIX_SCHEDULED_REPORT_JOB = "SR-JOB-";
	public static final String PREFIX_SCHEDULED_REPORT_TRIGGER = "SR-TRIGGER-";
	public static final String PREFIX_IMMEDIATE_TRIGGER = "RUN-NOW-";
	public static final String PREFIX_DELETE_SPACE_TRIGGER = "DLSP-TRIGGER-";
	public static final String PREFIX_DELETE_SPACE_JOB = "DLSP-JOB-";
	public static final String PREFIX_PROCESS_JOB = "PR-JOB-";
	public static final String PREFIX_PROCESS_JOB_TRIGGER = "PR-TRIGGER-";
	public static final String PREFIX_BDS_JOB = "BDS-JOB-";
	public static final String PREFIX_BDS_JOB_TRIGGER = "BDS-TRIGGER-";
	public static final String PREFIX_CONNECTORS_JOB = "CN-JOB-";
	public static final String PREFIX_CONNECTORS_JOB_TRIGGER = "CN-TRIGGER-";
	public static final String PREFIX_DELETE_LOAD_JOB = "DLOAD-JOB-";
	public static final String PREFIX_DELETE_LOAD_JOB_TRIGGER = "DLOAD-TRIGGER-";
	
	public static final String PREFIX_DELETE_JOB_GROUP_NAME = "DLOAD-";
	public static final String PREFIX_CONNECTORS_JOB_GROUP_NAME = "CN-";
	public static final String PREFIX_BDS_JOB_GROUP_NAME = "BDS-";
	public static final String PREFIX_PROCESS_JOB_GROUP_NAME = "PR-";
	

	public static final String SCHEDULER_DB_SCHEMA = "dbo";
	public static final String SMIWEB_TRUSTED_SERVICE_NAMESPACE = "http://trustedservice.WebServices.successmetricsinc.com";	
	public static final String SMIWEB_TRUSETED_SERVICE_ENDPOINT = "/SMIWeb/services/TrustedService.TrustedServiceHttpSoap11Endpoint";
	
	public static final String BIRST_ADMIN_SERVICE_NAMESPACE = "http://www.birst.com/";
	public static final String BIRST_ADMIN_SERVICE_ENDPOINT = "/AdminService.asmx";
	
	// keys for key=value pairs returned from Acorn web service
	public static final String KEY_USERNAME = "username";
	public static final String KEY_USEREMAIL = "email"; 
	public static final String KEY_LOCALE = "Locale";
	public static final String KEY_TIMEZONE = "TimeZone";
	public static final String KEY_MAX_REPORTS = "MaxReportsAllowed";
	public static final String KEY_SPACE_DIRECTORY = "SpaceDirectory";
	public static final String KEY_SPACE_NAME = "SpaceName";
	public static final String KEY_SPACE_DELETE = "DeletedSpace";
	public static final String KEY_SPACE_ID = "SpaceID";
	
	// All space ids flag
	public static final String ALL_SPACES = "AllSpaces";
	
	public static final String CONNECTOR_SCHEDULE_TYPE_SUFFIX = "_connector";
	
	// Job Listeners Name
	public static final String JOB_LISTENER_PERSIST_JOB_HISTORY = "listeners.PersistJobHistoryInDB";
	public static final String JOB_LISTENER_CONFIGURE_RETRY_JOB = "listeners.ConfigureRetryJob";
	
	// Trigger Listeners Name
	public static final String TRIGGER_LISTENER_ClEAR_SCHEDULEINFO = "listeners.ClearScheduleInfoListener";
	
	// Types of Jobs
	public static enum JobType
	{
		REPORT, SFDCEXTRACTION, PROCESSING, DELETELOAD, BDS, NETSUITE, ALL
	};
	
	public static enum Type
	{
		sfdc, netsuite, bds
	};
	
	// 4 statuses for export report status
	public static enum ReportStatus
	{
		SCHEDULED, RUNNING, COMPLETE, FAILED
	};
	
	// 4 statuses for export report status
	public static enum RunningJobStatus
	{
		RUNNING(1), 
		COMPLETE(2), 
		FAILED(3), 
		KILLED(4); 
		
		private int status;
		RunningJobStatus(int status)
		{
			this.status = status;
		}		
		
		public int getStatus()
		{
			return status;
		}		
	}
	
	// catalog directory prefix
	
	public static final String REPORT_CATALOG_PREFIX = "V{CatalogDir}";
	
	// scheduled reports suffix
	public static final String REPORT_JASPER_SUFFIX = ".jasper";
	
	// system property setup for scheduler home
	public static final String SYSTEM_PROPERTY_HOME = "scheduler.home";
	
	// Cron expression
	public static final String ALL_VALUES = "*";
	public static final String NO_SPECIFIC = "?";
	
	// run now operations adds an instant step in space_status table which prevents any
	// other concurrent operation before it actually gets picked up
	public static final String TRIGGER_MAP_SCHEDULE_STEP = "TriggerScheduleStepId";
	// If a job is running and space status table is checked for any concurrent operation
	// and this step id is encountered, by pass it
	public static final String TRIGGER_MAP_IGNORE_SCHEDULE_STEP = "TriggerIgnoreScheduleStepId";
	
	public static enum DeleteType
	{
		DeleteLast, 
		DeleteAll, 
		DeleteReport, 
		DeleteProcess,
		DeleteSFDCExtract;
	}
	
	public static final String SFDC_EXTRACT_FILE = "SFDC_extract.lock";
	public static final String CONNECTOR_SFDC_EXTRACT_FILE = "SFDC_extract.lock";
	public static final String CONNECTOR_NETSUITE_EXTRACT_FILE = "Netsuite_extract.lock";
	public static final String CONNECTOR_EXTRACT_FILE_SUFFIX = "_extract.lock";
	public static final String BDS_EXTRACT_FILE = "BDS_extract.lock";
	public static final String SFDC_KILL_FILE = "SFDC_kill.lock";
	public static final String BDS_KILL_FILE = "BDS_kill.lock";
	public static final String CHECK_IN_LOAD_FILE = "checkIn-load.txt";
	public static final String CHECK_IN_EXTRACT_FILE = "checkIn-sfdc.txt";
	public static final String BDS_CHECK_IN_EXTRACT_FILE = "checkIn-bds.txt";
	public static final String CHECK_IN_PARAM_TIME = "time";
	public static final String PUBLISH_LOCK_FILE = "publish.lock"; 
	public static final String LOAD_LOCK_FILE = "load.lock";
	
	public static final String DEFAULT_NAME = "Default Name";
	public static final String CONNECTOR_CONNECTION_STRING = "ConnectorConnectionString";
	
	public static final int INDEX_CRON_SECONDS = 0;
	public static final int INDEX_CRON_MINUTES = 1;
	public static final int INDEX_CRON_HOURS = 2;
	public static final int INDEX_CRON_DAYOFMONTH = 3;
	public static final int INDEX_CRON_MONTH = 4;
	public static final int INDEX_CRON_DAYOFWEEK = 5;
	public static final int INDEX_CRON_YEAR = 6;
}
