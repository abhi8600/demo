package com.birst.scheduler.utils;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.apache.log4j.Logger;

public class RunningProcessHandler implements CommandProcessHandler
{
	String spaceID;
	String userID;
	String spaceDirectory;
	String executedProcessCommand; 
	int loadID;
	String loadGroup;
	
	public RunningProcessHandler(String spaceID, String userID, String spaceDirectory, 
			String executedProcessCommand, int loadID, String loadGroup){
		this.spaceDirectory = spaceDirectory;
		this.spaceID = spaceID;
		this.userID = userID;
		this.executedProcessCommand = executedProcessCommand;
		this.loadID = loadID;
		this.loadGroup = loadGroup;
	}
	
	private static Logger logger = Logger.getLogger(RunningProcessHandler.class);
	// publish this to the database
	public void logInfo(){	
		try
		{
			int pid = getPid();
			if(pid >= 0){
				String logFile = CommandUtil.getProcessCommandLogFilePath(spaceDirectory);
				String loadInfo = "LN=" + loadID + ",LG=" + loadGroup;
				String updateRunningProcessInfoInDb = CommandUtil.getUpdateRunningProcessCommand(UUID.randomUUID().toString(), spaceID, userID, logFile, pid, loadInfo);
				CommandUtil.runCommand(CommandUtil.CMD_UPDATE_ADMIN_DB, updateRunningProcessInfoInDb, null, null);
			}
		}
		catch(Exception ex){
			logger.error("Exception while publishing the process details for cmd " + executedProcessCommand);
		}
	}
	
	private int getPid() throws IOException, InterruptedException{
		String toFindPidCmd = "cmd /c for /f \"TOKENS=1\" %a in ('wmic PROCESS where \"CommandLine LIKE '%" + executedProcessCommand.trim().replace("\\", "\\\\") + "%'\" get ProcessID ^| findstr [0-9]') do echo MyPID=%a";
		String filterLine = "MyPID=";

		List<String> linesBasedOnFilter = getFilteredOutputLines(toFindPidCmd, filterLine);
		List<Integer> pids = new ArrayList<Integer>();
		if(linesBasedOnFilter != null && linesBasedOnFilter.size() > 0){
			for(String lineBasedonFilter : linesBasedOnFilter){
				int index = lineBasedonFilter.indexOf(filterLine);
				if(index >=0){
					String value = lineBasedonFilter.substring(index + filterLine.length());
					try
					{
						pids.add(Integer.parseInt(value));
					}
					catch (NumberFormatException ex){
					}
				}
			}

			for(Integer pid : pids){
				String validPidCmd = "cmd /c wmic PROCESS where ProcessID=" + pid + " get ProcessID";
				 linesBasedOnFilter = getFilteredOutputLines(validPidCmd, String.valueOf(pid));
				 if(linesBasedOnFilter != null && linesBasedOnFilter.size() > 0){
					 return pid;
				 }
			}
		}
		/*
		 * for /f "TOKENS=1" %a in ('wmic PROCESS where "Name='powershell.exe'" get ProcessID ^| findstr [0-9]') do echo MyPID=%a
echo  %MyPID%
		if (p != null && 
				(p.getClass().getName().equals("java.lang.Win32Process") ||
				p.getClass().getName().equals("java.lang.ProcessImpl"))) {
			  
			  try {
			    Field f = p.getClass().getDeclaredField("handle");
			    f.setAccessible(true);				
			    long handl = f.getLong(p);
			   
			    Kernel32 kernel = Kernel32.INSTANCE;
			    W32API.HANDLE handle = new W32API.HANDLE();
			    handle.setPointer(Pointer.createConstant(handl));
			    pid = kernel.GetProcessId(handle);
			  } catch (Throwable e) {				
			  }
			}
			*/
		return -1;
	}
	
	private List<String> getFilteredOutputLines(String cmd, String filterLine){
		boolean exitedGracefully = false;
		Process proc = null;
		try
		{
			logger.info("Running command " + cmd + "  : filterLine : " + filterLine);
			Runtime rt = Runtime.getRuntime();		
			proc = rt.exec(cmd, null, null);
			StreamFetchThread errorStream = new StreamFetchThread(proc.getErrorStream(), "GetPID ERROR", null);
			StreamFetchThread outputStream = new StreamFetchThread(proc.getInputStream(), "GetPID OUTPUT", filterLine);
			errorStream.start();
			outputStream.start();
			proc.waitFor();
			int exitValue = proc.waitFor();
			exitedGracefully = true;
			logger.info("Exit value for command " + cmd + " : " + exitValue);
			errorStream.join(CommandUtil.TIMEOUT_PROC_OUTPUT_MILLIS);
			outputStream.join(CommandUtil.TIMEOUT_PROC_OUTPUT_MILLIS);
			return outputStream.getLinesBasedOnFilter();
		}
		catch(Exception ex)
		{
			logger.error("Exception while getting filtered output lines cmd : " + cmd + " : filterLine : " + filterLine, ex);
		}
		finally
		{
			if(!exitedGracefully && proc != null)
			{
				proc.destroy();
			}
		}
		return null;
	}
}
