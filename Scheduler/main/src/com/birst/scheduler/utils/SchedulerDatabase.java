package com.birst.scheduler.utils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.sql.DatabaseMetaData;
import org.apache.log4j.Logger;

import com.birst.scheduler.salesforce.SFDCInfo;
import com.birst.scheduler.Report.ScheduledReport;
import com.birst.scheduler.delete.DeleteInfo;
import com.birst.scheduler.process.ProcessInfo;
import com.birst.scheduler.shared.CopyJob;
import com.birst.scheduler.shared.JobHistory;
import com.birst.scheduler.shared.Schedule;
import com.birst.scheduler.shared.ServerDetails;
import com.birst.scheduler.shared.SpaceStatus;
import com.birst.scheduler.bds.BDSInfo;

public class SchedulerDatabase
{

	private static final Logger logger = Logger.getLogger(SchedulerDatabase.class);
	public static final String SCHEDULED_REPORTS_TABLENAME = "CUST_SCHEDULED_REPORTS";
	public static final String SCHEDULE_INFO_TABLENAME = "CUST_SCHEDULE_INFO";
	public static final String CUST_JOB_HISTORY_TABLENAME = "CUST_JOB_HISTORY";
	public static final String CUST_SERVERS_TABLENAME = "CUST_SERVERS_INFO";
	public static final String CUST_VERSIONS_TABLENAME = "CUST_VERSIONS_INFO";
	
	// Table operations
	public static final int ALTER_ADD = 0;
	public static final int ALTER_DROP = 1;
	public static final int ALTER_COLUMN = 2;
	
	private static DatabaseConnection dConn;
	static
	{
		try
		{
			dConn = SchedulerDBConnection.getInstance().getConnectionPool().getDBConnection();
		}
		catch(Exception e)
		{
			logger.error(e.toString(), e);
		}
	}
	
	public static void createTables(Connection conn, String schema) throws Exception
	{		
		// Create Scheduled Report table
		if(!tableExists(conn, schema, SCHEDULED_REPORTS_TABLENAME))
		{		
			List<String> columnNames = new ArrayList<String>();
			List<String> columnTypes = new ArrayList<String>();	
			columnNames.add("ID");
			columnTypes.add(dConn.getUniqueIdentiferType());
			
			columnNames.add("SPACE_ID");
			columnTypes.add(dConn.getUniqueIdentiferType());
			
			columnNames.add("USER_ID");
			columnTypes.add(dConn.getUniqueIdentiferType());
			
			columnNames.add("Name");
			columnTypes.add(dConn.getNVarcharType(255, false));
			
			columnNames.add("FileName");
			columnTypes.add(dConn.getNVarcharType(255, false));
			
			columnNames.add("ReportPath");
			columnTypes.add(dConn.getNVarcharType(255, false));
			
			columnNames.add("TriggerReportPath");
			columnTypes.add(dConn.getNVarcharType(255, false));
			
			columnNames.add("ToReportPath");
			columnTypes.add(dConn.getVarcharType(255, false));
			
			columnNames.add("Type");
			columnTypes.add(dConn.getVarcharType(128, false));
			
			columnNames.add("Subject");
			columnTypes.add(dConn.getVarcharType(255, false));
			
			columnNames.add("Body");
			columnTypes.add(dConn.getVarcharType(-1, true));
			
			columnNames.add("List");
			columnTypes.add(dConn.getVarcharType(2048, false));
			
			columnNames.add("FromUser");
			columnTypes.add(dConn.getVarcharType(128, false));
			
			columnNames.add("Compression");
			columnTypes.add(dConn.getVarcharType(20, false));

			columnNames.add("CsvSeparator");
			columnTypes.add(dConn.getVarcharType(10, false));
			
			columnNames.add("CsvExtension");
			columnTypes.add(dConn.getVarcharType(20, false));
			
			columnNames.add("Prompts");
			columnTypes.add(dConn.getVarcharType(-1, true));
			
			columnNames.add("CreatedAdminMode");
			columnTypes.add(dConn.getBooleanType("CreatedAdminMode"));
			
			columnNames.add("Comments");
			columnTypes.add(dConn.getVarcharType(2048, false));
			
			columnNames.add("CreatedDate");
			columnTypes.add(dConn.getDateTimeType());
			
			columnNames.add("ModifiedDate");
			columnTypes.add(dConn.getDateTimeType());
			
			columnNames.add("ModifiedBy");
			columnTypes.add(dConn.getVarcharType(512, false));
			
			columnNames.add(SchedulerDBConstants.COL_VARIABLES);
			columnTypes.add(dConn.getVarcharType(-1, true));
			
			createTable(conn, schema, SCHEDULED_REPORTS_TABLENAME, columnNames, columnTypes, true);
			createIndex(conn, schema, SCHEDULED_REPORTS_TABLENAME, "IDX1", "ID");
			createIndex(conn, schema, SCHEDULED_REPORTS_TABLENAME, "IDX2", "SPACE_ID");
			createIndex(conn, schema, SCHEDULED_REPORTS_TABLENAME, "IDX3", "USER_ID");
		}
		
		if(!tableExists(conn, schema, SCHEDULE_INFO_TABLENAME))
		{		
			List<String> columnNames = new ArrayList<String>();
			List<String> columnTypes = new ArrayList<String>();			
			columnNames.add("ID");
			columnTypes.add(dConn.getUniqueIdentiferType());
			
			columnNames.add("JOB_ID");
			columnTypes.add(dConn.getUniqueIdentiferType());
			
			columnNames.add("Interval");
			columnTypes.add("Int");
			
			columnNames.add("DayOfWeek");
			columnTypes.add("Int");
			
			columnNames.add("DayOfMonth");
			columnTypes.add("Int");
			
			columnNames.add("Hour");
			columnTypes.add("Int");
			
			columnNames.add("Minute");
			columnTypes.add("Int");
			
			columnNames.add("CronExpression");
			columnTypes.add(dConn.getVarcharType(2048, false));

			columnNames.add("TimeZone");
			columnTypes.add(dConn.getVarcharType(10, false));

			columnNames.add("Enable");
			columnTypes.add(dConn.getBooleanType("Enable"));
			
			columnNames.add("CreatedDate");
			columnTypes.add(dConn.getDateTimeType());
			
			columnNames.add("ModifiedDate");
			columnTypes.add(dConn.getDateTimeType());
			
			columnNames.add("ModifiedBy");
			columnTypes.add(dConn.getVarcharType(512, false));
			
			columnNames.add("StartDate");
			columnTypes.add(dConn.getVarcharType(30, false));
						
			createTable(conn, schema, SCHEDULE_INFO_TABLENAME, columnNames, columnTypes, true);			
			createIndex(conn, schema, SCHEDULE_INFO_TABLENAME, "IDX1", "ID");
			createIndex(conn, schema, SCHEDULE_INFO_TABLENAME, "IDX2", "JOB_ID");
		}
		
		if(!tableExists(conn, schema, CUST_JOB_HISTORY_TABLENAME))
		{		
			List<String> columnNames = new ArrayList<String>();
			List<String> columnTypes = new ArrayList<String>();
			
			columnNames.add("ID");
			columnTypes.add(dConn.getUniqueIdentiferType());
			
			columnNames.add("JOB_ID");
			columnTypes.add(dConn.getUniqueIdentiferType());
			
			columnNames.add("SCHEDULE_ID");
			columnTypes.add(dConn.getUniqueIdentiferType());
			
			columnNames.add("StartTime");
			columnTypes.add(dConn.getDateTimeType());
			
			columnNames.add("EndTime");
			columnTypes.add(dConn.getDateTimeType());
			
			columnNames.add("Status");
			columnTypes.add("int");
			
			columnNames.add("ErrorMessage");
			columnTypes.add(dConn.getVarcharType(2048, false));		
	
						
			createTable(conn, schema, CUST_JOB_HISTORY_TABLENAME, columnNames, columnTypes, true);			
			createIndex(conn, schema, CUST_JOB_HISTORY_TABLENAME, "IDX1", "ID");			
			createIndex(conn, schema, CUST_JOB_HISTORY_TABLENAME, "IDX2", "JOB_ID");
		}
		
		if(!tableExists(conn, schema, CUST_SERVERS_TABLENAME))
		{		
			List<String> columnNames = new ArrayList<String>();
			List<String> columnTypes = new ArrayList<String>();
			
			columnNames.add("ID");
			columnTypes.add("int PRIMARY KEY NOT NULL");
			
			columnNames.add("SchedulerServerID");
			columnTypes.add("int");
			
			columnNames.add("Name");
			columnTypes.add(dConn.getVarcharType(2048, false));
			
			columnNames.add("URL");
			columnTypes.add(dConn.getVarcharType(2048, false));
			
			columnNames.add("Available");
			columnTypes.add(dConn.getBooleanType("Available"));
			
			columnNames.add("CreatedDate");
			columnTypes.add(dConn.getDateTimeType());
			
			columnNames.add("ModifiedDate");
			columnTypes.add(dConn.getDateTimeType());
											
			createTable(conn, schema, CUST_SERVERS_TABLENAME, columnNames, columnTypes, true);
			createIndex(conn, schema, CUST_SERVERS_TABLENAME, "IDX1", "ID");
		}
		
		if(!tableExists(conn, schema, CUST_VERSIONS_TABLENAME))
		{		
			List<String> columnNames = new ArrayList<String>();
			List<String> columnTypes = new ArrayList<String>();
			
			columnNames.add("CurrentVersion");
			columnTypes.add("int");
			
			columnNames.add("CreatedDate");
			columnTypes.add(dConn.getDateTimeType());
			
			createTable(conn, schema, CUST_VERSIONS_TABLENAME, columnNames, columnTypes, true);
			// Make sure that the version is initially updated with the to_be_version number
			// This ensure any unnecessary error since upgrade should not be called the very first time.
			//SchedulerDatabase.updateVersion(conn, schema, Upgrade.TO_BE_VERSION);
		}

		createSpaceStatusTable(conn, schema);
		createProcessInfoTable(conn, schema);
		createConnectorsInfoTable(conn, schema);
		createBDSInfoTable(conn, schema);
		createDeleteInfoTable(conn, schema);
	}
	
	
	private static void createSpaceStatusTable(Connection conn, String schema) throws Exception
	{
		if(!tableExists(conn, schema, SchedulerDBConstants.CUST_SPACE_STATUS_TABLENAME))
		{
			List<String> columnNames = new ArrayList<String>();
			List<String> columnTypes = new ArrayList<String>();

			columnNames.add(SchedulerDBConstants.COL_ID);
			columnTypes.add(dConn.getUniqueIdentiferType());

			columnNames.add(SchedulerDBConstants.COL_SPACE_ID);
			columnTypes.add(dConn.getUniqueIdentiferType());

			columnNames.add(SchedulerDBConstants.COL_JOB_ID);
			columnTypes.add(dConn.getUniqueIdentiferType());
			
			columnNames.add(SchedulerDBConstants.COL_LOAD_GROUP);
			columnTypes.add(dConn.getVarcharType(256, false));

			columnNames.add(SchedulerDBConstants.COL_LOAD_ID);
			columnTypes.add("int");

			columnNames.add(SchedulerDBConstants.COL_STEP);
			columnTypes.add(dConn.getVarcharType(256, false));

			columnNames.add(SchedulerDBConstants.COL_STATUS);
			columnTypes.add("int");

			columnNames.add(SchedulerDBConstants.COL_CHECKIN_TIME);
			columnTypes.add(dConn.getBigIntType());

			columnNames.add(SchedulerDBConstants.COL_MESSAGE);
			columnTypes.add(dConn.getVarcharType(-1, true));
			
			columnNames.add(SchedulerDBConstants.COL_CREATED_DATE);
			columnTypes.add(dConn.getDateTimeType());
			
			columnNames.add(SchedulerDBConstants.COL_MODIFIED_DATE);
			columnTypes.add(dConn.getDateTimeType());
			
			columnNames.add(SchedulerDBConstants.COL_TERMINIATE);
			columnTypes.add(dConn.getBooleanType(SchedulerDBConstants.COL_TERMINIATE));
			
			createTable(conn, schema, SchedulerDBConstants.CUST_SPACE_STATUS_TABLENAME, columnNames, columnTypes, true);
		}
	}
	
	private static void createProcessInfoTable(Connection conn, String schema) throws Exception
	{
		if(!tableExists(conn, schema, SchedulerDBConstants.CUST_PROCESS_INFO_TABLENAME))
		{
			List<String> columnNames = new ArrayList<String>();
			List<String> columnTypes = new ArrayList<String>();

			columnNames.add(SchedulerDBConstants.COL_ID);
			columnTypes.add(dConn.getUniqueIdentiferType());

			columnNames.add(SchedulerDBConstants.COL_SPACE_ID);
			columnTypes.add(dConn.getUniqueIdentiferType());
			
			columnNames.add(SchedulerDBConstants.COL_USER_ID);
			columnTypes.add(dConn.getUniqueIdentiferType());

			columnNames.add(SchedulerDBConstants.COL_LOAD_ID);
			columnTypes.add("int");
			
			columnNames.add(SchedulerDBConstants.COL_LOAD_DATE);
			columnTypes.add(dConn.getVarcharType(256, false));

			columnNames.add(SchedulerDBConstants.COL_LOAD_GROUP);
			columnTypes.add(dConn.getVarcharType(256, false));

			columnNames.add(SchedulerDBConstants.COL_SUBGROUPS);
			columnTypes.add(dConn.getVarcharType(-1, true));

			columnNames.add(SchedulerDBConstants.COL_RETRY_FAILED);
			columnTypes.add(dConn.getBooleanType(SchedulerDBConstants.COL_RETRY_FAILED));
			
			columnNames.add(SchedulerDBConstants.COL_SEND_EMAIL);
			columnTypes.add(dConn.getBooleanType(SchedulerDBConstants.COL_SEND_EMAIL));

			columnNames.add(SchedulerDBConstants.COL_CREATED_DATE);
			columnTypes.add(dConn.getDateTimeType());
			
			columnNames.add(SchedulerDBConstants.COL_MODIFIED_DATE);
			columnTypes.add(dConn.getDateTimeType());
			
			createTable(conn, schema, SchedulerDBConstants.CUST_PROCESS_INFO_TABLENAME, columnNames, columnTypes, true);
		}
	}
	
	
	private static void createDeleteInfoTable(Connection conn, String schema) throws Exception
	{
		if(!tableExists(conn, schema, SchedulerDBConstants.CUST_DELETE_INFO_TABLENAME))
		{
			List<String> columnNames = new ArrayList<String>();
			List<String> columnTypes = new ArrayList<String>();

			columnNames.add(SchedulerDBConstants.COL_ID);
			columnTypes.add(dConn.getUniqueIdentiferType());

			columnNames.add(SchedulerDBConstants.COL_SPACE_ID);
			columnTypes.add(dConn.getUniqueIdentiferType());
			
			columnNames.add(SchedulerDBConstants.COL_USER_ID);
			columnTypes.add(dConn.getUniqueIdentiferType());
						
			columnNames.add(SchedulerDBConstants.COL_LOAD_GROUP);
			columnTypes.add(dConn.getVarcharType(128, false));
			
			columnNames.add(SchedulerDBConstants.COL_LOAD_ID);
			columnTypes.add("int");
			
			columnNames.add(SchedulerDBConstants.COL_DELETE_TYPE);
			columnTypes.add(dConn.getVarcharType(128, false));
			
			columnNames.add(SchedulerDBConstants.COL_DELETE_RESTORE);
			columnTypes.add(dConn.getBooleanType(SchedulerDBConstants.COL_DELETE_RESTORE));
			
			columnNames.add(SchedulerDBConstants.COL_CREATED_DATE);
			columnTypes.add(dConn.getDateTimeType());
			
			columnNames.add(SchedulerDBConstants.COL_MODIFIED_DATE);
			columnTypes.add(dConn.getDateTimeType());
			
			columnNames.add(SchedulerDBConstants.COL_MODIFIED_BY);
			columnTypes.add(dConn.getVarcharType(128, false));
			
			createTable(conn, schema, SchedulerDBConstants.CUST_DELETE_INFO_TABLENAME, columnNames, columnTypes, true);
		}
	}
	
	private static void createConnectorsInfoTable(Connection conn, String schema) throws Exception
	{
		if(!tableExists(conn, schema, SchedulerDBConstants.CUST_CONNECTORS_INFO_TABLENAME))
		{
			List<String> columnNames = new ArrayList<String>();
			List<String> columnTypes = new ArrayList<String>();

			columnNames.add(SchedulerDBConstants.COL_ID);
			columnTypes.add(dConn.getUniqueIdentiferType());

			columnNames.add(SchedulerDBConstants.COL_SPACE_ID);
			columnTypes.add(dConn.getUniqueIdentiferType());
			
			columnNames.add(SchedulerDBConstants.COL_USER_ID);
			columnTypes.add(dConn.getUniqueIdentiferType());

			columnNames.add(SchedulerDBConstants.COL_RUN_NOW_MODE);
			columnTypes.add(dConn.getBooleanType(SchedulerDBConstants.COL_RUN_NOW_MODE));

			columnNames.add(SchedulerDBConstants.COL_CLIENT_ID);
			columnTypes.add(dConn.getVarcharType(128, false));
			
			columnNames.add(SchedulerDBConstants.COL_SANBOX_URL);
			columnTypes.add(dConn.getVarcharType(512, false));
			
			columnNames.add(SchedulerDBConstants.COL_PROCESS_AFTER);
			columnTypes.add(dConn.getBooleanType(SchedulerDBConstants.COL_PROCESS_AFTER));
			
			columnNames.add(SchedulerDBConstants.COL_LOAD_DATE);
			columnTypes.add(dConn.getVarcharType(256, false));
			
			columnNames.add(SchedulerDBConstants.COL_SUBGROUPS);
			columnTypes.add(dConn.getVarcharType(-1, true));
			
			columnNames.add(SchedulerDBConstants.COL_EXTRACTGROUPS);
			columnTypes.add(dConn.getVarcharType(-1, true));
			
			columnNames.add(SchedulerDBConstants.COL_SEND_EMAIL);
			columnTypes.add(dConn.getBooleanType(SchedulerDBConstants.COL_SEND_EMAIL));
			
			columnNames.add(SchedulerDBConstants.COL_CREATED_DATE);
			columnTypes.add(dConn.getDateTimeType());
			
			columnNames.add(SchedulerDBConstants.COL_MODIFIED_DATE);
			columnTypes.add(dConn.getDateTimeType());
			
			columnNames.add(SchedulerDBConstants.COL_MODIFIED_BY);
			columnTypes.add(dConn.getVarcharType(128, false));
			
			columnNames.add(SchedulerDBConstants.COL_CONN_SCHED_NAME);
			columnTypes.add(dConn.getVarcharType(256, false));
			
			columnNames.add(SchedulerDBConstants.COL_CONN_TYPE);
			columnTypes.add(dConn.getVarcharType(128, false));
			
			columnNames.add(SchedulerDBConstants.COL_CONN_ENGINE);
			columnTypes.add("int");
			
			createTable(conn, schema, SchedulerDBConstants.CUST_CONNECTORS_INFO_TABLENAME, columnNames, columnTypes, true);
		}
	}
	
	private static void createBDSInfoTable(Connection conn, String schema) throws Exception
	{
		if(!tableExists(conn, schema, SchedulerDBConstants.CUST_BDS_INFO_TABLENAME))
		{
			List<String> columnNames = new ArrayList<String>();
			List<String> columnTypes = new ArrayList<String>();

			columnNames.add(SchedulerDBConstants.COL_ID);
			columnTypes.add(dConn.getUniqueIdentiferType());

			columnNames.add(SchedulerDBConstants.COL_SPACE_ID);
			columnTypes.add(dConn.getUniqueIdentiferType());
			
			columnNames.add(SchedulerDBConstants.COL_DS_ID);
			columnTypes.add(dConn.getUniqueIdentiferType());
			
			columnNames.add(SchedulerDBConstants.COL_USER_ID);
			columnTypes.add(dConn.getUniqueIdentiferType());
			
			columnNames.add(SchedulerDBConstants.COL_NAME);
			columnTypes.add(dConn.getVarcharType(256, false));

			columnNames.add(SchedulerDBConstants.COL_SCHEDULE_TYPE);
			columnTypes.add(dConn.getVarcharType(256, false));

			columnNames.add(SchedulerDBConstants.COL_RUN_NOW_MODE);
			columnTypes.add(dConn.getBooleanType(SchedulerDBConstants.COL_RUN_NOW_MODE));
			
			columnNames.add(SchedulerDBConstants.COL_PROCESS_AFTER);
			columnTypes.add(dConn.getBooleanType(SchedulerDBConstants.COL_PROCESS_AFTER));
			
			columnNames.add(SchedulerDBConstants.COL_LOAD_DATE);
			columnTypes.add(dConn.getVarcharType(256, false));
			
			columnNames.add(SchedulerDBConstants.COL_LOAD_GROUP);
			columnTypes.add(dConn.getVarcharType(-1, true));
			
			columnNames.add(SchedulerDBConstants.COL_SEND_EMAIL);
			columnTypes.add(dConn.getBooleanType(SchedulerDBConstants.COL_SEND_EMAIL));
			
			columnNames.add(SchedulerDBConstants.COL_CREATED_DATE);
			columnTypes.add(dConn.getDateTimeType());
			
			columnNames.add(SchedulerDBConstants.COL_MODIFIED_DATE);
			columnTypes.add(dConn.getDateTimeType());

			columnNames.add(SchedulerDBConstants.COL_MODIFIED_BY);
			columnTypes.add(dConn.getVarcharType(128, false));

			createTable(conn, schema, SchedulerDBConstants.CUST_BDS_INFO_TABLENAME, columnNames, columnTypes, true);
		}
	}
	
	public static boolean alterTable(Connection conn, String schema, String tableName,  List<String> columnNames,
			List<String> columnTypes, List<Integer> actions ) throws SQLException
	{
		boolean altered = true;
		if(tableExists(conn, schema, tableName))
		{
			for(int i=0; i < columnNames.size(); i++)
			{
				StringBuilder sb = new StringBuilder();
				sb.append("ALTER TABLE " + getQualifiedTableName(schema, tableName));
				if(actions.get(i) == ALTER_ADD)
				{
					sb.append(" ADD " + columnNames.get(i) + " " + columnTypes.get(i));
				}
				
				if(actions.get(i) == ALTER_DROP)
				{
					if (dConn.isDBTypeOracle())
					{
						sb.append(" DROP COLUMN " + columnNames.get(i));
					}
					else
					{
						sb.append(" DROP " + columnNames.get(i) + " " + columnTypes.get(i));
					}
				}
				
				if(actions.get(i) == ALTER_COLUMN)
				{
					if (dConn.isDBTypeOracle())
					{
						sb.append(" MODIFY( " + columnNames.get(i) + " " + columnTypes.get(i) + ")");
					}
					else
					{
						sb.append(" ALTER COLUMN " + columnNames.get(i) + " " + columnTypes.get(i));
					}
				}
				
				PreparedStatement pstmt = null;
				try
				{
					pstmt = conn.prepareStatement(sb.toString());
					pstmt.execute();
					altered = altered && true;
				}
				catch(Exception ex)
				{
					logger.error("Exception during alter table " + tableName, ex);
					altered = false;
				}
				finally
				{
					try
					{
						if(pstmt != null)
						{
							pstmt.close();
						}
					}
					catch(Exception ex2)
					{
						logger.warn("Error in disposing pstmt.close()");
					}
				}
			}
		}
		return altered;
	}
	
	public static boolean tableExists(Connection conn, String schema, String tableName) throws SQLException
	{
		boolean exists = false;
		List<String> tables = getTables(conn, schema, null, null);
		if (tables != null && tables.size() > 0)
		{
			for (String s : tables)
			{
				if (s.toLowerCase().equals(tableName.toLowerCase()))
				{
					exists = true;
					break;
				}					
			}			
		}
		return exists;
	}
	
	
	public static List<String> getTables(Connection conn, String schema, String catalog, String tableName) throws SQLException
	{		
		if (conn == null)
		{
			return (null);
		}
		DatabaseMetaData meta = conn.getMetaData();
		if (dConn.isDBTypeOracle())
		{
			schema = schema == null ? null : schema.toUpperCase();
			tableName = tableName == null ? null : tableName.toUpperCase();
		}
		ResultSet set = meta.getTables(null, schema, tableName, null);
		List<String> results = new ArrayList<String>();
		while (set.next())
		{
			String catalogName = set.getString(1);
			String schemaName = set.getString(2);
			String tname = set.getString(3);
			String tableType = set.getString(4);
			if (tableType.equals("TABLE"))
			{
				if (catalog == null || catalog.isEmpty() || catalogName.equalsIgnoreCase(catalog))
				{					
					results.add(tname.toLowerCase());
				}
			}
		}
		set.close();
		return (results);
	}
		
	
	private static void createTable(Connection conn, String schema, String tableName, List<String> columnNames,
			List<String> columnTypes, boolean b) throws Exception
	{
		StringBuilder sb = new StringBuilder();
		sb.append("CREATE TABLE " + getQualifiedTableName(schema, tableName) + " (");
		for (int i = 0; i < columnNames.size(); i++)
		{		
			if (i > 0)
			{
				sb.append(',');
			}
			sb.append(columnNames.get(i));
			sb.append(' ');
			sb.append(columnTypes.get(i));			
		}
		sb.append(')');		
		Statement stmt = null;
		try
		{
			stmt = conn.createStatement();			
			stmt.executeUpdate(sb.toString());
		} 
		catch(Exception ex)
		{
			logger.error("Exception in creating table " + tableName , ex);
			throw new Exception("Exception in creating table " + tableName, ex.getCause());
		}
		finally
		{
			if(stmt != null)
			{
				try
				{
					stmt.close();
				}catch(Exception ex)
				{
					logger.warn("Error in stmt.close() for " + sb.toString() );
				}
			}
		}
	}

	public static String getQualifiedTableName(String schema, String tableName)
	{
		return schema != null && schema.length() > 0 ? schema + "." + tableName : tableName;
	}

	private static boolean createIndex(Connection conn, String schema, String tableName, String indexName, String columnName)
	{
		List<String> columnNames = new ArrayList<String>();
		columnNames.add(columnName);
		return createIndex(conn, schema, tableName, indexName, columnNames, null);
	}
	
	private static boolean createIndex(Connection conn, String schema, String tableName, String indexName, List<String> columnNames,
			List<String> includedColumnNames)
	{
		boolean isOracle = dConn.isDBTypeOracle();
		if (isOracle)
		{
			indexName = indexName + "_" + tableName;
            for (String colName: columnNames)
            {
                indexName = indexName + "_" + colName;
            }
            indexName = schema + "." + "IDX_" + Math.abs(indexName.hashCode());
		}
		StringBuilder query = new StringBuilder("CREATE ");
		query.append(isOracle ? "" : "NONCLUSTERED ");
		query.append("INDEX ");
		query.append(indexName);
		query.append(" ON ");
		query.append(getQualifiedTableName(schema, tableName));
		query.append(" (");
		
		boolean firstColumnName = true;
		StringBuilder colNamesBuilder = new StringBuilder();
		for (String colName : columnNames)
		{
			if (firstColumnName)
				firstColumnName = false;
			else
				colNamesBuilder.append(',');
			
				colNamesBuilder.append(colName);
		}
		
		query.append(colNamesBuilder);
		
		if(includedColumnNames != null && includedColumnNames.size() > 0 && !isOracle)
		{
			boolean firstIncludeName = true;
			StringBuilder includeColNamesBuilder = new StringBuilder();
			for (String includeColName : includedColumnNames)
			{
				if (firstIncludeName)
					firstIncludeName = false;
				else
					includeColNamesBuilder.append(',');
				includeColNamesBuilder.append(includeColName);
			}
			query.append(includeColNamesBuilder);
		}
		
		query.append(')');	
		logger.debug(query);
		Statement stmt = null;
		try
		{
			stmt = conn.createStatement();
			stmt.executeUpdate(query.toString());
		} catch (Exception e)
		{
			logger.error(e, e);
		} finally
		{
			if(stmt != null)
			{
				try
				{
					stmt.close();
				}catch(Exception ex)
				{
					logger.warn("Error in stmt.close() for " + query.toString() );
				}
			}
		}
		
		return false;
	}
	
	public static void addScheduledReport(Connection conn, String schema, ScheduledReport scheduledReport) throws SQLException
	{
		String id = scheduledReport.getId();
		String spaceID = scheduledReport.getSpaceID();
		String userID = scheduledReport.getUserID();
		String reportPath = scheduledReport.getReportPath();
		String triggerReportPath = scheduledReport.getTriggerReportPath();
		String toReportPath = scheduledReport.getToReportPath();
		String type = scheduledReport.getType();
		String subject = scheduledReport.getSubject();
		String body = scheduledReport.getEmailBody();
		String[] emailList = scheduledReport.getToList();
		String comments = scheduledReport.getComments();
		String name = scheduledReport.getName();
		String fileName = scheduledReport.getFileName();
		String compression = scheduledReport.getCompression();
		String fromUser = scheduledReport.getFrom();
		String csvSeparator = scheduledReport.getCsvSeparator();
		String csvExtension = scheduledReport.getCsvExtension();
		String prompts = scheduledReport.getPrompts();
		String variables = scheduledReport.getVariables();
		boolean createdAdminMode = scheduledReport.isCreatedAdminMode();
		PreparedStatement pstmt = null;
		try
		{
			pstmt = conn.prepareStatement("INSERT INTO " + getQualifiedTableName(schema, SCHEDULED_REPORTS_TABLENAME)
					+ " (ID, SPACE_ID, USER_ID, Name, FileName,  ReportPath, TriggerReportPath, ToReportPath, " +
					"Type, Subject, Body, List, FromUser, Compression, CsvSeparator , CsvExtension , Prompts, CreatedAdminMode, " +
					"Comments, CreatedDate, Variables) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
			
			// truncate the strings to make sure that they are no bigger than the fields
			pstmt.setString(1, id);
			pstmt.setString(2, spaceID);
			pstmt.setString(3, userID);
			pstmt.setString(4, name);
			pstmt.setString(5, fileName);
			if(reportPath != null)
			{
				pstmt.setString(6, reportPath);
			}
			else
			{
				pstmt.setNull(6, Types.VARCHAR);
			}
			
			if(triggerReportPath != null)
			{
				pstmt.setString(7, triggerReportPath);
			}
			else
			{
				pstmt.setNull(7, Types.VARCHAR);
			}
				
			if(toReportPath != null)
			{
				pstmt.setString(8, toReportPath);
			}
			else
			{
				pstmt.setNull(8, Types.VARCHAR);
			}
			
			if(type != null)
			{
				pstmt.setString(9, type);
			}
			else
			{
				pstmt.setNull(9, Types.VARCHAR);
			}
			
			if(subject != null)
			{
				pstmt.setString(10, subject);
			}
			else
			{
				pstmt.setNull(10, Types.VARCHAR);
			}
			
			if(body != null)
			{
				pstmt.setString(11, body);
			}
			else
			{
				pstmt.setNull(11, Types.VARCHAR);
			}
				
			if (emailList != null)
			{
				StringBuilder emailListString = new StringBuilder();
				boolean first = true;
				for(String email : emailList)
				{
					if(first)
					{
						first = false;
					}
					else
					{
						emailListString.append(';');
					}
					
					emailListString.append(email);
				}
				
				pstmt.setString(12, emailListString.toString());			
			} 
			else
			{
				pstmt.setNull(12, Types.VARCHAR);	
			}
			
			if(fromUser != null)
			{
				pstmt.setString(13, fromUser);
			}
			else
			{
				pstmt.setNull(13, Types.VARCHAR);
			}
			
			if(compression != null)
			{
				pstmt.setString(14, compression);
			}
			else
			{
				pstmt.setNull(14, Types.VARCHAR);
			}
			
			if(csvSeparator != null)
			{
				pstmt.setString(15, csvSeparator);
			}
			else
			{
				pstmt.setNull(15, Types.VARCHAR);
			}
			
			if(csvExtension != null)
			{
				pstmt.setString(16, csvExtension);
			}
			else
			{
				pstmt.setNull(16, Types.VARCHAR);
			}
			
			if(prompts != null)
			{
				pstmt.setString(17, prompts);
			}
			else
			{
				pstmt.setNull(17, Types.VARCHAR);
			}
			
			pstmt.setBoolean(18, createdAdminMode);
			
			if(comments != null && comments.length() > 0)
			{
				pstmt.setString(19, comments);
			}
			else
			{
				pstmt.setNull(19, Types.VARCHAR);
			}
			
			Date date = new Date();				
			Timestamp startTime = new Timestamp(date.getTime());
			pstmt.setTimestamp(20, startTime);
			
			if(variables != null)
			{
				pstmt.setString(21, variables);
			}
			else
			{
				pstmt.setNull(21, Types.VARCHAR);
			}
			
			pstmt.execute();	
			/*
			if(scheduledReport.getSchedules() != null && scheduledReport.getSchedules().length > 0)
			{
				for(ScheduleInfo scheduleInfo : scheduledReport.getSchedules())
				{
					SchedulerDatabase.addScheduleInfo(conn, schema, scheduleInfo);
				}
			}
			*/
			
			/*
			if(scheduledReport.getJobSchedules() != null && scheduledReport.getJobSchedules().length > 0)
			{
				for(Schedule schedule : scheduledReport.getJobSchedules())
				{
					SchedulerDatabase.addSchedule(conn, schema, schedule);
				}
			}
			*/
			logger.debug("Added scheduled report in " + SCHEDULED_REPORTS_TABLENAME + " : " + scheduledReport.toString());
			
		}
		catch (SQLException e)
		{
			logger.error("Could not add Scheduled Report info in the database");
			throw e;
		}
		finally 
		{
			try
			{
				if (pstmt != null)
					pstmt.close();
			}
			catch (SQLException e)
			{
				logger.warn("Error in pstmt.close()", e );
			}
		}
	}
	
	public static void updateScheduledReport(Connection conn, String schema,
			ScheduledReport scheduledReport) throws SQLException
	{
		String id = scheduledReport.getId();
		String spaceID = scheduledReport.getSpaceID();
		String userID = scheduledReport.getUserID();
		String reportPath = scheduledReport.getReportPath();
		String triggerReportPath = scheduledReport.getTriggerReportPath();
		String toReportPath = scheduledReport.getToReportPath();
		String type = scheduledReport.getType();
		String subject = scheduledReport.getSubject();
		String body = scheduledReport.getEmailBody();
		String[] emailList = scheduledReport.getToList();
		String comments = scheduledReport.getComments();
		String name = scheduledReport.getName();
		String fileName = scheduledReport.getFileName();
		String compression = scheduledReport.getCompression();
		String csvSeparator = scheduledReport.getCsvSeparator();
		String csvExtension = scheduledReport.getCsvExtension();
		String fromUser = scheduledReport.getFrom();
		String modifiedBy = scheduledReport.getModifiedBy();
		String prompts = scheduledReport.getPrompts();
		String variables = scheduledReport.getVariables();
		boolean createdAdminMode = scheduledReport.isCreatedAdminMode();
		PreparedStatement pstmt = null;
		try
		{
			pstmt = conn.prepareStatement("UPDATE " + getQualifiedTableName(schema, SCHEDULED_REPORTS_TABLENAME)
					+ " SET SPACE_ID = ?, USER_ID = ?, Name = ?, FileName = ?, ReportPath= ? , TriggerReportPath = ?, ToReportPath = ?, " +
					"Type = ?, Subject = ?, Body = ?, List = ?, FromUser = ?, Compression = ?, CsvSeparator = ?, CsvExtension = ?, " +
					"Prompts = ?, CreatedAdminMode = ?, " +
					"Comments = ?, ModifiedDate = ?, ModifiedBy = ?, " + SchedulerDBConstants.COL_VARIABLES +  " = ? " + 
					" WHERE ID = ?");
			
			// truncate the strings to make sure that they are no bigger than the fields			
			pstmt.setString(1, spaceID);
			pstmt.setString(2, userID);
			pstmt.setString(3, name);
			pstmt.setString(4, fileName);
			if(reportPath != null)
			{
				pstmt.setString(5, reportPath);
			}
			else
			{
				pstmt.setNull(5, Types.VARCHAR);
			}
			
			if(triggerReportPath != null)
			{
				pstmt.setString(6, triggerReportPath);
			}
			else
			{
				pstmt.setNull(6, Types.VARCHAR);
			}
				
			if(toReportPath != null)
			{
				pstmt.setString(7, toReportPath);
			}
			else
			{
				pstmt.setNull(7, Types.VARCHAR);
			}
			
			if(type != null)
			{
				pstmt.setString(8, type);
			}
			else
			{
				pstmt.setNull(8, Types.VARCHAR);
			}
			
			if(subject != null)
			{
				pstmt.setString(9, subject);
			}
			else
			{
				pstmt.setNull(9, Types.VARCHAR);
			}
			
			if(body != null)
			{
				pstmt.setString(10, body);
			}
			else
			{
				pstmt.setNull(10, Types.VARCHAR);
			}
				
			if (emailList != null)
			{
				StringBuilder emailListString = new StringBuilder();
				boolean first = true;
				for(String email : emailList)
				{
					if(first)
					{
						first = false;
					}
					else
					{
						emailListString.append(';');
					}
					
					emailListString.append(email);
				}
				
				pstmt.setString(11, emailListString.toString());			
			} 
			else
			{
				pstmt.setNull(11, Types.VARCHAR);	
			}
			
			if(fromUser != null && fromUser.length() > 0)
			{
				pstmt.setString(12, fromUser);
			}
			else
			{
				pstmt.setNull(12, Types.VARCHAR);
			}
			
			if(compression != null && compression.length() > 0)
			{
				pstmt.setString(13, compression);
			}
			else
			{
				pstmt.setNull(13, Types.VARCHAR);
			}
			
			if(csvSeparator != null && csvSeparator.length() > 0)
			{
				pstmt.setString(14, csvSeparator);
			}
			else
			{
				pstmt.setNull(14, Types.VARCHAR);
			}
			
			if(csvExtension != null && csvExtension.length() > 0)
			{
				pstmt.setString(15, csvExtension);
			}
			else
			{
				pstmt.setNull(15, Types.VARCHAR);
			}
			
			if(prompts != null && prompts.length() > 0)
			{
				pstmt.setString(16, prompts);
			}
			else
			{
				pstmt.setNull(16, Types.VARCHAR);
			}
			
			pstmt.setBoolean(17, createdAdminMode);
			
			if(comments != null && comments.length() > 0)
			{
				pstmt.setString(18, comments);
			}
			else
			{
				pstmt.setNull(18, Types.VARCHAR);
			}
			
			Date date = new Date();				
			Timestamp modifiedDate = new Timestamp(date.getTime());
			pstmt.setTimestamp(19, modifiedDate);
			
			if(modifiedBy != null && modifiedBy.length() > 0)
			{
				pstmt.setString(20, modifiedBy);
			}
			else
			{
				pstmt.setNull(20, Types.VARCHAR);
			}
			
			
			if(variables != null && variables.length() > 0)
			{
				pstmt.setString(21, variables);
			}
			else
			{
				pstmt.setNull(21, Types.VARCHAR);
			}
			
			pstmt.setString(22, id);
			pstmt.execute();		
			logger.debug("Updated scheduled report in " + SCHEDULED_REPORTS_TABLENAME + " : " + scheduledReport.toString());
			
		}
		catch (SQLException e)
		{
			logger.error("Could not add Scheduled Report info in the database");
			throw e;
		}
		finally 
		{
			try
			{
				if (pstmt != null)
					pstmt.close();
			}
			catch (SQLException e)
			{
				logger.warn("Error in pstmt.close()", e );
			}
		}		
	}	
	
	public static void removeScheduledReport(Connection conn, String schema,
			String reportID) throws SQLException
	{
		if(reportID == null || reportID.length() == 0)
			return;
		
		PreparedStatement pstmt = null;
		try
		{
			pstmt = conn.prepareStatement("DELETE FROM " + getQualifiedTableName(schema, SCHEDULED_REPORTS_TABLENAME)
					+ " WHERE ID = '" + reportID + "'");						
			
			pstmt.execute();
			logger.debug("Removed scheduled report from " + SCHEDULED_REPORTS_TABLENAME + " : report ID : " + reportID);
		}
		catch (SQLException e)
		{
			logger.warn("Error while deleting schedule report ID " + reportID);
			throw e;
		}
		finally 
		{
			try
			{
				if (pstmt != null)
					pstmt.close();
			}
			catch (SQLException e)
			{
				logger.warn("Error in pstmt.close()", e );
			}
		}		
	}
	
	public static void addSchedule(Connection conn, String schema, Schedule schedule) throws SQLException	
	{
		String id = schedule.getId();
		String jobId = schedule.getJobId();		
		String expression = schedule.getExpression();
		String timeZone = schedule.getTimeZone();
		boolean enable = schedule.isEnable();
		String startDate = schedule.getStartDate();
		
		PreparedStatement pstmt = null;
		try
		{
			pstmt = conn.prepareStatement("INSERT INTO " + getQualifiedTableName(schema, SCHEDULE_INFO_TABLENAME)
					+ " (ID, JOB_ID, CronExpression, TimeZone, Enable, CreatedDate, StartDate) VALUES (?,?,?,?,?,?,?)");
						
			pstmt.setString(1, id);
			pstmt.setString(2, jobId);
			pstmt.setString(3, expression);
			pstmt.setString(4, timeZone);
			pstmt.setBoolean(5, enable);
			Date date = new Date();				
			Timestamp createdDate = new Timestamp(date.getTime());
			pstmt.setTimestamp(6, createdDate);
			if(startDate != null && startDate.trim().length() > 0)
			{
				pstmt.setString(7, startDate);
			}
			else
			{
				pstmt.setNull(7, Types.VARCHAR);
			}
			pstmt.execute();
			logger.debug("Added schedule to " + SCHEDULE_INFO_TABLENAME + " : " + schedule.toString());
		}
		catch (SQLException e)
		{
			logger.warn("Error while adding schedule information");
			throw e;
		}
		finally 
		{
			try
			{
				if (pstmt != null)
					pstmt.close();
			}
			catch (SQLException e)
			{
				logger.warn("Error in pstmt.close()", e );
			}
		}
	}
	
	public static void updateSchedule(Connection conn, String schema, Schedule schedule) throws SQLException	
	{
		String id = schedule.getId();
		String jobID = schedule.getJobId();		
		String expression = schedule.getExpression();
		String timeZone = schedule.getTimeZone();
		boolean enable = schedule.isEnable();
		String modifiedBy = schedule.getExpression();
		String startDate = schedule.getStartDate();
		
		PreparedStatement pstmt = null;
		try
		{
			pstmt = conn.prepareStatement("Update " + getQualifiedTableName(schema, SCHEDULE_INFO_TABLENAME)
					+ " Set JOB_ID = ?, CronExpression = ?, TimeZone = ?, Enable = ?, ModifiedDate = ?, ModifiedBy = ?, StartDate = ? WHERE ID = ? " );
						
			pstmt.setString(1, jobID);			
			pstmt.setString(2, expression);					
			pstmt.setBoolean(3, enable);
			pstmt.setString(4, timeZone);
			Date date = new Date();				
			Timestamp modifiedDate = new Timestamp(date.getTime());
			pstmt.setTimestamp(5, modifiedDate);
			
			if(modifiedBy != null && modifiedBy.length() > 0)
			{
				pstmt.setString(6, modifiedBy);
			}
			else
			{
				pstmt.setNull(6, Types.VARCHAR);
			}
			
			if(startDate != null && startDate.length() > 0)
			{
				pstmt.setString(7, startDate);
			}
			else
			{
				pstmt.setNull(7, Types.VARCHAR);
			}
			
			pstmt.setString(8, id);	
			
			
			pstmt.execute();
			logger.debug("Updated schedule in " + SCHEDULE_INFO_TABLENAME + " : " + schedule.toString());
		}
		catch (SQLException e)
		{
			logger.warn("Error while updating schedule information for schedule id : " + id);
			throw e;
		}
		finally 
		{
			try
			{
				if (pstmt != null)
					pstmt.close();
			}
			catch (SQLException e)
			{
				logger.warn("Error in pstmt.close()", e );
			}
		}
	}
	
	public static Schedule getSchedule(Connection conn, String schema, String scheduleId) throws SQLException
	{		
		PreparedStatement pstmt = null;
		Schedule si = null;
		try
		{
			pstmt = conn.prepareStatement("SELECT JOB_ID, CronExpression, TimeZone, Enable, StartDate FROM " +
					SchedulerDatabase.getQualifiedTableName(schema, SCHEDULE_INFO_TABLENAME) + 
					" WHERE ID='" + scheduleId + "'" );					
			
			ResultSet set = pstmt.executeQuery();
			boolean first = true;
			while (set.next())
			{
				if(first)
				{
					first = false;
				}
				else
				{
					// Only one row should be returned for this query. Something is wrong if it returns multiple rows.
					logger.warn("Returning multiple entries for scheduled info " + scheduleId);
					return null;
				}
				
							
				String jobID = (String) set.getObject(1);
				String expression = set.getString(2);				
				String timeZone = set.getString(3);
				boolean enable = set.getBoolean(4);
				String startDate = set.getString(5);
				si= new Schedule(scheduleId, jobID, expression, enable, timeZone);
				si.setStartDate(startDate);
				/*
				si.setId(scheduleId);
				si.setJobId(jobID);
				si.setExpression(expression);
				si.setTimeZone(timeZone);
				si.setEnable(enable);
				*/				
			}
			
			return si;
		}
		catch (SQLException e)
		{
			logger.error("Error while retreiving schedule info information", e);	
			throw e;
		}
		finally 
		{
			try
			{
				if (pstmt != null)
					pstmt.close();
			}
			catch (SQLException e)
			{
				logger.warn("Error in pstmt.close()", e );
			}
		}		
	}
	
	public static List<Schedule> getScheduleList(Connection conn, String schema, String jobId) throws SQLException
	{		
		PreparedStatement pstmt = null;
		List<Schedule> scheduleList = null;
		try
		{
			pstmt = conn.prepareStatement("Select ID, CronExpression, TimeZone, Enable, StartDate " + 
					"FROM " +	SchedulerDatabase.getQualifiedTableName(schema, SCHEDULE_INFO_TABLENAME) + 
					" WHERE JOB_ID='" + jobId + "'" );					
			
			ResultSet set = pstmt.executeQuery();
			boolean first = true;
			while (set.next())
			{
				if(first)
				{
					first = false;
					scheduleList = new ArrayList<Schedule>();
				}
				/*
				Schedule si = new Schedule();				
				si.setId((String) set.getObject(1));
				si.setExpression(set.getString(2));
				si.setTimeZone(set.getString(3));
				si.setEnable(set.getBoolean(4));
				si.setJobId(jobId);
				*/
				String id = (String) set.getObject(1);
				String expression = set.getString(2);
				String timeZone = set.getString(3);
				boolean enable = set.getBoolean(4);
				String startDate = set.getString(5);
				
				Schedule si = new Schedule(id, jobId, expression, enable, timeZone);
				si.setStartDate(startDate);
				scheduleList.add(si);
			}
			
			return scheduleList;	
		}
		catch (SQLException e)
		{
			logger.error("Error while retreiving schedule info for report " + jobId, e);
			throw e;
		}
		finally 
		{
			try
			{
				if (pstmt != null)
					pstmt.close();
			}
			catch (SQLException e)
			{
				logger.warn("Error in pstmt.close()", e );
			}
		}		
	}
	
	
	/*
	public static void addScheduleInfo(Connection conn, String schema, ScheduleInfo scheduleInfo) throws SQLException
	{	
		String id = scheduleInfo.getId();
		String jobId = scheduleInfo.getObjectID();
		int interval = scheduleInfo.getInterval();
		int dayOfWeek = scheduleInfo.getDayOfWeek();
		int dayOfMonth = scheduleInfo.getDayOfMonth();
		int hour = scheduleInfo.getHour();
		int minute = scheduleInfo.getMinute();
		boolean enable = scheduleInfo.isEnable();
		
		PreparedStatement pstmt = null;
		try
		{
			pstmt = conn.prepareStatement("INSERT INTO " + getQualifiedTableName(schema, SCHEDULE_INFO_TABLENAME)
					+ " (ID, JOB_ID, Interval, DayOfWeek, DayOfMonth, Hour, Minute, Enable) VALUES (?,?,?,?,?,?,?,?)");
						
			pstmt.setString(1, id);
			pstmt.setString(2, jobId);
			pstmt.setInt(3, interval);
			pstmt.setInt(4, dayOfWeek);
			pstmt.setInt(5, dayOfMonth);
			pstmt.setInt(6, hour);
			pstmt.setInt(7, minute);		
			pstmt.setBoolean(8, enable);
			
			pstmt.execute();
		}
		catch (SQLException e)
		{
			logger.warn("Error while adding schedule information");
			throw e;
		}
		finally 
		{
			try
			{
				if (pstmt != null)
					pstmt.close();
			}
			catch (SQLException e)
			{
				logger.warn("Error in pstmt.close()", e );
			}
		}
	}
*/
	public static void removeSchedule(Connection conn, String schema,
			String triggerID) throws SQLException
	{
		if(triggerID == null || triggerID.length() == 0)
			return;
		
		PreparedStatement pstmt = null;
		try
		{
			pstmt = conn.prepareStatement("DELETE FROM " + getQualifiedTableName(schema, SCHEDULE_INFO_TABLENAME)
					+ " WHERE ID = '" + triggerID + "'");						
			
			pstmt.execute();
			logger.debug("Removed schedule from " + SCHEDULE_INFO_TABLENAME + " : ID : " + triggerID);
		}
		catch (SQLException e)
		{
			logger.warn("Error while deleting schedule information");
			throw e;
		}
		finally 
		{
			try
			{
				if (pstmt != null)
					pstmt.close();
			}
			catch (SQLException e)
			{
				logger.warn("Error in pstmt.close()", e );
			}
		}		
	}
	
	/*
	public static ScheduleInfo getScheduledInfo(Connection conn, String schema, String id) throws SQLException
	{		
		PreparedStatement pstmt = null;
		ScheduleInfo si = null;
		try
		{
			pstmt = conn.prepareStatement("SELECT JOB_ID, Interval, DayOfWeek, DayOfMonth, Hour, " + 
					" Minute, Enable FROM " + 
					SchedulerDatabase.getQualifiedTableName(schema, SCHEDULE_INFO_TABLENAME) + 
					" WHERE ID='" + id + "'" );					
			
			ResultSet set = pstmt.executeQuery();
			boolean first = true;
			while (set.next())
			{
				if(first)
				{
					first = false;
				}
				else
				{
					// Only one row should be returned for this query. Something is wrong if it returns multiple rows.
					logger.warn("Returning multiple entries for scheduled info " + id);
					return null;
				}
				
				si = new ScheduleInfo();			
				String jobID = (String) set.getObject(1);				
				int interval = set.getInt(2);
				int dayOfWeek = set.getInt(3);
				int dayOfMonth = set.getInt(4);
				int hour = set.getInt(5);
				int minute = set.getInt(6);
				boolean enable = set.getBoolean(7);
				
				si.setId(id);
				si.setObjectID(jobID);
				si.setInterval(interval);
				si.setDayOfWeek(dayOfWeek);
				si.setDayOfMonth(dayOfMonth);
				si.setHour(hour);
				si.setMinute(minute);
				si.setEnable(enable);				
			}
			
			return si;
		}
		catch (SQLException e)
		{
			logger.error("Error while retreiving schedule info information", e);	
			throw e;
		}
		finally 
		{
			try
			{
				if (pstmt != null)
					pstmt.close();
			}
			catch (SQLException e)
			{
				logger.warn("Error in pstmt.close()", e );
			}
		}		
	}
	*/
	
	public static List<ScheduledReport> getScheduledReportList(Connection conn, String schema, String spaceID) throws SQLException
	{		
		PreparedStatement pstmt = null;
		List<ScheduledReport> scheduledReportList = null;
		try
		{
			pstmt =conn.prepareStatement("SELECT ID, SPACE_ID, USER_ID, Name, FileName, ReportPath, TriggerReportPath, ToReportPath, " +
					"Type, Subject, Body, List, FromUser, Compression, Comments, CsvSeparator, CsvExtension, Prompts, CreatedAdminMode, " +
					"CreatedDate, ModifiedDate, ModifiedBy, " + SchedulerDBConstants.COL_VARIABLES + " FROM "+ 
					SchedulerDatabase.getQualifiedTableName(schema, SCHEDULED_REPORTS_TABLENAME) + 
					" WHERE SPACE_ID='" + spaceID + "'" );					
			
			ResultSet set = pstmt.executeQuery();
			boolean first = true;
			while (set.next())
			{
				if(first)
				{
					first = false;
					scheduledReportList = new ArrayList<ScheduledReport>();
				}
				ScheduledReport sr = new ScheduledReport();
				String srID = (String) set.getObject(1);				
				String userID = (String) set.getObject(3);
				String name = set.getString(4);
				String fileName = set.getString(5);
				String reportPath = set.getString(6);
				String triggerReportPath = set.getString(7);
				String toReportPath = set.getString(8);
				String type = set.getString(9);
				String subject = set.getString(10);
				String emailBody = set.getString(11);
				String emailList = set.getString(12);
				String[] emails = emailList.split(";");
				String from = set.getString(13);
				String compression = set.getString(14);
				String comments = set.getString(15);
				String csvSeparator = set.getString(16);
				String csvExtension = set.getString(17);
				String prompts = set.getString(18);
				boolean createdAdminMode = set.getBoolean(19);
				Timestamp createdDate = set.getTimestamp(20);
				Timestamp modifiedDate = set.getTimestamp(21);
				String modifiedBy = set.getString(22);
				String variables = set.getString(23);

				/*
				List<ScheduleInfo> scheduleInfoList = SchedulerDatabase.getScheduledInfoList(conn, schema, sr.getId());
				ScheduleInfo[] schedules = null;
				if(scheduleInfoList != null && scheduleInfoList.size() > 0)
				{
					schedules = scheduleInfoList.toArray(new ScheduleInfo[scheduleInfoList.size()]);
				}
				*/
				
				List<Schedule> scheduleList = SchedulerDatabase.getScheduleList(conn, schema, sr.getId());
				Schedule[] jobSchedules = null;
				if(scheduleList != null && scheduleList.size() > 0)
				{
					jobSchedules = scheduleList.toArray(new Schedule[scheduleList.size()]);
				}
				
				sr = new ScheduledReport(srID, spaceID, userID, reportPath, triggerReportPath, toReportPath, 
						type, subject, emailBody , emails, comments, name, fileName, compression, from,
						csvSeparator, csvExtension, prompts, variables, createdAdminMode);
				sr.setCreatedDate(createdDate);
				sr.setModifiedDate(modifiedDate);
				sr.setModifiedBy(modifiedBy);
				//sr.setSchedules(schedules);
				sr.setJobSchedules(jobSchedules);
				scheduledReportList.add(sr);
				
			}
			
			return scheduledReportList;	
		}
		catch (SQLException e)
		{
			logger.error("Error while retreiving schedule reports List for space " + spaceID, e);
			throw e;
		}
		finally 
		{
			try
			{
				if (pstmt != null)
					pstmt.close();
			}
			catch (SQLException e)
			{
				logger.warn("Error in pstmt.close()", e );
			}
		}		
	}
	
	public static ScheduledReport getScheduledReport(Connection conn, String schema,
			String id) throws SQLException
	{		
		PreparedStatement pstmt = null;
		ScheduledReport sr = null;
		try
		{
			pstmt = conn.prepareStatement("SELECT ID, SPACE_ID, USER_ID, Name, FileName, ReportPath, TriggerReportPath, ToReportPath, " +
					"Type, Subject, Body, List, FromUser, Compression, Comments, CsvSeparator, CsvExtension, Prompts, CreatedAdminMode, " +
					"CreatedDate, ModifiedDate, ModifiedBy, " + SchedulerDBConstants.COL_VARIABLES + "  FROM " + 
					SchedulerDatabase.getQualifiedTableName(schema, SCHEDULED_REPORTS_TABLENAME) + 
					" WHERE ID='" + id + "'" );					
			
			ResultSet set = pstmt.executeQuery();
			boolean first = true;
			while (set.next())
			{
				if(first)
				{
					first = false;
				}
				else
				{
					// Only one row should be returned for this query. Something is wrong if it returns multiple rows.
					logger.warn("Returning multiple entries for scheduled report " + id);
					return null;
				}
				
				String srID = (String) set.getObject(1);				
				String spaceID = (String) set.getObject(2);
				String userID = (String) set.getObject(3);
				String name = set.getString(4);
				String fileName = set.getString(5);
				String reportPath = set.getString(6);
				String triggerReportPath = set.getString(7);
				String toReportPath = set.getString(8);
				String type = set.getString(9);
				String subject = set.getString(10);
				String emailBody = set.getString(11);
				String emailList = set.getString(12);
				String[] emails = emailList != null ? emailList.split(";") : null;
				String from = set.getString(13);
				String compression = set.getString(14);
				String comments = set.getString(15);
				String csvSeparator = set.getString(16);
				String csvExtension = set.getString(17);
				String prompts = set.getString(18);
				boolean createdAdminMode = set.getBoolean(19);
				Timestamp createdDate = set.getTimestamp(20);
				Timestamp modifiedDate = set.getTimestamp(21);
				String modifiedBy = set.getString(22);
				String variables = set.getString(23);
				
				sr = new ScheduledReport(srID, spaceID, userID, reportPath, triggerReportPath, toReportPath, 
						type, subject, emailBody , emails, comments, name, fileName, compression, from,
						csvSeparator, csvExtension, prompts, variables, createdAdminMode);
				sr.setCreatedDate(createdDate);
				sr.setModifiedDate(modifiedDate);
				sr.setModifiedBy(modifiedBy);
				/*
				List<ScheduleInfo> siList = SchedulerDatabase.getScheduledInfoList(conn, schema, srID);
				if(siList != null && siList.size() > 0)
				{
					ScheduleInfo[] schedules = (ScheduleInfo[]) siList.toArray(new ScheduleInfo[siList.size()]); 
					sr.setSchedules(schedules);
				}
				*/
				
				List<Schedule> scheduleList = SchedulerDatabase.getScheduleList(conn, schema, sr.getId());
				Schedule[] jobSchedules = null;
				if(scheduleList != null && scheduleList.size() > 0)
				{
					jobSchedules = scheduleList.toArray(new Schedule[scheduleList.size()]);
					sr.setJobSchedules(jobSchedules);
				}
			}
			
			return sr;
		}
		catch (SQLException e)
		{
			logger.error("Error while retreiving schedule report information", e);	
			throw e;
		}
		finally 
		{
			try
			{
				if (pstmt != null)
					pstmt.close();
			}
			catch (SQLException e)
			{
				logger.warn("Error in pstmt.close()", e );
			}
		}		
	}
	/*
	public static List<ScheduleInfo> getScheduledInfoList(Connection conn, String schema, String scheduleReportID) throws SQLException
	{		
		PreparedStatement pstmt = null;
		List<ScheduleInfo> scheduleInfoList = null;
		try
		{
			pstmt = conn.prepareStatement("Select ID, Interval, DayOfWeek, DayOfMonth, Hour, Minute, Enable " + 
					"FROM " +	SchedulerDatabase.getQualifiedTableName(schema, SCHEDULE_INFO_TABLENAME) + 
					" WHERE JOB_ID='" + scheduleReportID + "'" );					
			
			ResultSet set = pstmt.executeQuery();
			boolean first = true;
			while (set.next())
			{
				if(first)
				{
					first = false;
					scheduleInfoList = new ArrayList<ScheduleInfo>();
				}
				ScheduleInfo si = new ScheduleInfo();
				si.setId((String) set.getObject(1));				
				si.setInterval(set.getInt(2));
				si.setDayOfWeek(set.getInt(3));
				si.setDayOfMonth(set.getInt(4));
				si.setHour(set.getInt(5));
				si.setMinute(set.getInt(6));
				si.setEnable(set.getBoolean(7));
				scheduleInfoList.add(si);
			}
			
			return scheduleInfoList;	
		}
		catch (SQLException e)
		{
			logger.error("Error while retreiving schedule info for report " + scheduleReportID, e);
			throw e;
		}
		finally 
		{
			try
			{
				if (pstmt != null)
					pstmt.close();
			}
			catch (SQLException e)
			{
				logger.warn("Error in pstmt.close()", e );
			}
		}		
	}
	*/
	
	public static List<JobHistory>  getJobHistory(Connection conn, String schema, String jobID, String scheduleID) throws SQLException
	{
		
		PreparedStatement pstmt = null;		
		List<JobHistory> jobHistoryList = null;
		try
		{
			String query = "SELECT ID, JOB_ID, SCHEDULE_ID, StartTime, EndTime, Status, ErrorMessage FROM "
					+ SchedulerDatabase.getQualifiedTableName(schema,CUST_JOB_HISTORY_TABLENAME)
					+ " WHERE JOB_ID='"	+ jobID	+ "'" + " order by StartTime desc";

			if(scheduleID != null && scheduleID.length() > 0)
			{
				query = query + "AND SCHEDULE_ID='" + scheduleID + "'";
			}
			
			pstmt = conn.prepareStatement(query);
			ResultSet set = pstmt.executeQuery();
			boolean first = true;
			while (set.next())
			{	
				if(first)
				{
					jobHistoryList = new ArrayList<JobHistory>();
					first = false;
				}
				
				JobHistory jobHistory = new JobHistory();							
				String id = (String) set.getObject(1);
				String historyJobID = (String) set.getObject(2);
				String historyScheduleID = null;
				if(set.getObject(3) != null)
				{
					historyScheduleID = (String) set.getObject(3);					
				}

				Timestamp startTime = set.getTimestamp(4);			
				Timestamp endTime = set.getTimestamp(5);
				int status = set.getInt(6);
				String errorMessage = set.getString(7);
				jobHistory.setId(id);
				jobHistory.setJobID(historyJobID);
				jobHistory.setScheduleID(historyScheduleID);
				jobHistory.setStartTime(startTime);
				jobHistory.setEndTime(endTime);
				jobHistory.setStatus(status);
				jobHistory.setErrorMessage(errorMessage);
				
				jobHistoryList.add(jobHistory);
			}
			
			return jobHistoryList;
		}
		catch (SQLException e)
		{
			logger.error("Error while retreiving schedule info information", e);	
			throw e;
		}
	}
	
	public static ScheduledReport addJobHistoryEntry(Connection conn, String schema,
			String id, String jobID, String scheduleID, Timestamp startTime, Timestamp endTime, int status, String errorMessage) throws SQLException
	{		
		PreparedStatement pstmt = null;
		try
		{
			boolean isOracle = dConn.isDBTypeOracle(); 
			pstmt = conn.prepareStatement("INSERT INTO " + getQualifiedTableName(schema, CUST_JOB_HISTORY_TABLENAME)
					+ " (ID, JOB_ID, SCHEDULE_ID, StartTime, EndTime, Status, ErrorMessage) VALUES (?,?,?,?,?,?,?)");				
			
			pstmt.setString(1, id);
			pstmt.setString(2, jobID);
			
			if(scheduleID != null && scheduleID.length() > 0)
			{
				pstmt.setString(3, scheduleID);
			}
			else
			{
				if(isOracle)
				{
					pstmt.setNull(3, Types.NULL);
				}
				else
				{
					pstmt.setNull(3, Types.OTHER);
				}
			}
			
			if(startTime != null)
			{
				pstmt.setTimestamp(4, startTime);
			}
			else
			{
				pstmt.setNull(4, Types.TIMESTAMP);
			}
			
			if(endTime != null)
			{
				pstmt.setTimestamp(5, endTime);
			}
			else
			{
				pstmt.setNull(5, Types.TIMESTAMP);
			}
			
			pstmt.setInt(6, status);
			
			if(errorMessage != null && errorMessage.length() > 0)
			{
				pstmt.setString(7, errorMessage);
			}
			else
			{
				pstmt.setNull(7, Types.VARCHAR);
			}
			
			pstmt.executeUpdate();
		}
		catch (SQLException e)
		{
			logger.error("Error while adding job history information", e);
			throw e;		
		}
		finally 
		{
			try
			{
				if (pstmt != null)
					pstmt.close();
			}
			catch (SQLException e)
			{
				logger.warn("Error in pstmt.close()", e );
			}
		}
		return null;
	}
	
	public static void updateJobHistory(Connection conn, String schema, String id, Timestamp endTime, int status, String errorMessage ) throws SQLException
	{			
		if(id == null)
		{
			logger.warn("History id required to update the job history ");
			return;
		}
		
		PreparedStatement pstmt = null;
		try
		{			 
			pstmt = conn.prepareStatement(
					"UPDATE " + getQualifiedTableName(schema, CUST_JOB_HISTORY_TABLENAME)
					+ " SET EndTime = ? , Status = ? , ErrorMessage = ?"  
					+ " WHERE ID = ?"
					);
						
			if(endTime != null)
			{
				pstmt.setTimestamp(1, endTime);
			}
			else
			{
				pstmt.setNull(1, Types.TIMESTAMP);
			}
			
			pstmt.setInt(2, status);
			
			if(errorMessage != null)
			{
				pstmt.setString(3, errorMessage);
			}
			else
			{
				pstmt.setNull(3, Types.VARCHAR);
			}
			
			pstmt.setString(4, id);
			pstmt.executeUpdate();
		}
		catch (SQLException e)
		{
			logger.error("Error while updating job history information", e);
			throw e;
		}
		finally 
		{
			try
			{
				if (pstmt != null)
					pstmt.close();
			}
			catch (SQLException e)
			{
				logger.warn("Error in pstmt.close()", e );
			}
		}
	}
	
	public static List<ServerDetails> getSMIWebServers(Connection conn, String schema, int serverID, boolean fetchAvailableOnly) throws SQLException
	{		
		PreparedStatement pstmt = null;
		List<ServerDetails> serversList = null;
		try
		{
			String cmd = "Select SchedulerServerID, Name, URL, Available, CreatedDate, ModifiedDate " + 
			"FROM " +	SchedulerDatabase.getQualifiedTableName(schema, CUST_SERVERS_TABLENAME);
			cmd = cmd + " WHERE SchedulerServerID = ?"; 
			if(fetchAvailableOnly)
			{
				cmd = cmd + " AND Available = ?"; 
			}
			pstmt = conn.prepareStatement(cmd);					
			
			pstmt.setInt(1, serverID);
			if(fetchAvailableOnly)
			{
				pstmt.setBoolean(2, fetchAvailableOnly);
			}
			
			ResultSet set = pstmt.executeQuery();
			boolean first = true;
			while (set.next())
			{
				if(first)
				{
					first = false;
					serversList = new ArrayList<ServerDetails>();
				}
				
				int id = set.getInt(1);
				String name = set.getString(2);
				String url = set.getString(3);
				boolean available = set.getBoolean(4);
				Timestamp createdDate = set.getTimestamp(5);
				Timestamp modifiedDate = set.getTimestamp(6);
				
				ServerDetails serverDetails = new ServerDetails();
				serverDetails.setSchedulerServerID(id);
				serverDetails.setName(name);
				serverDetails.setUrl(url);
				serverDetails.setAvailable(available);
				serverDetails.setCreatedDate(createdDate);
				serverDetails.setModifiedDate(modifiedDate);
				serversList.add(serverDetails);
			}
			
			return serversList;	
		}
		catch (SQLException e)
		{
			logger.error("Error while retreiving servers details ", e);
			throw e;
		}
		finally 
		{
			try
			{
				if (pstmt != null)
					pstmt.close();
			}
			catch (SQLException e)
			{
				logger.warn("getServersList : Error in pstmt.close()", e );
			}
		}		
	}

	public static int getCurrentVersion(Connection conn, String schema) throws SQLException
	{
		PreparedStatement pstmt = null;
		int currentVersion = -1;
		try
		{
			String cmd = "Select CurrentVersion " +  "FROM " +	SchedulerDatabase.getQualifiedTableName(schema, CUST_VERSIONS_TABLENAME);
			pstmt = conn.prepareStatement(cmd);					
			
			ResultSet set = pstmt.executeQuery();
			while (set.next())
			{
				currentVersion  = set.getInt(1);
			}
			
			return currentVersion;	
		}
		catch (SQLException e)
		{
			logger.error("Error while retreiving current version details ", e);
			throw e;
		}
		finally 
		{
			try
			{
				if (pstmt != null)
					pstmt.close();
			}
			catch (SQLException e)
			{
				logger.warn("getCurrentVersion : Error in pstmt.close()", e );
			}
		}		
	}
	
	public static void updateVersion(Connection conn, String schema, int toBeVersion) throws SQLException	
	{
		
		PreparedStatement pstmt = null;
		try
		{
			pstmt = conn.prepareStatement("DELETE FROM " + getQualifiedTableName(schema, CUST_VERSIONS_TABLENAME));
			pstmt.execute();
			
			pstmt = conn.prepareStatement("INSERT INTO " + getQualifiedTableName(schema, CUST_VERSIONS_TABLENAME) + 
					" (CurrentVersion, CreatedDate) Values( ?, ?) ");
						
			pstmt.setInt(1, toBeVersion);			
			Date date = new Date();				
			Timestamp createdDate = new Timestamp(date.getTime());
			pstmt.setTimestamp(2, createdDate);
			
			pstmt.execute();
		}
		catch (SQLException e)
		{
			logger.warn("Error while updating version information to: " + toBeVersion);
			throw e;
		}
		finally 
		{
			try
			{
				if (pstmt != null)
					pstmt.close();
			}
			catch (SQLException e)
			{
				logger.warn("Error in pstmt.close()", e );
			}
		}
	}

	public static void removeScheduledReports(Connection conn, String schedulerDbSchema, String spaceID) throws SQLException
	{
		if(spaceID == null || spaceID.length() == 0)
			return;
		
		PreparedStatement pstmt1 = null;
		PreparedStatement pstmt2 = null;
		try
		{
			
			pstmt1 = conn.prepareStatement("DELETE FROM " + getQualifiedTableName(schedulerDbSchema, SCHEDULE_INFO_TABLENAME)
					+ " WHERE JOB_ID IN (SELECT ID FROM " + getQualifiedTableName(schedulerDbSchema, SCHEDULED_REPORTS_TABLENAME)
					+ " WHERE SPACE_ID = '" + spaceID + "')");						
			
			pstmt1.execute();
			
			pstmt2 = conn.prepareStatement("DELETE FROM " + getQualifiedTableName(schedulerDbSchema, SCHEDULED_REPORTS_TABLENAME)
					+ " WHERE SPACE_ID = '" + spaceID + "'");						
			
			pstmt2.execute();
			logger.debug("Removed scheduled reports from " + SCHEDULE_INFO_TABLENAME + " : spaceId : " + spaceID );
		}
		catch (SQLException e)
		{
			logger.warn("Error while deleting scheduled report for spaceID " + spaceID, e);
			throw e;
		}
		finally 
		{
			try
			{
				if (pstmt1 != null)
					pstmt1.close();
				if (pstmt2 != null)
					pstmt2.close();
			}
			catch (SQLException e)
			{
				logger.warn("Error in pstmt.close()", e );
			}
		}
	}
	
	public static void addSpaceStatus(Connection conn, String schema, SpaceStatus spaceStatus) throws SQLException
	{
		PreparedStatement pstmt = null;
		try
		{
			pstmt = conn.prepareStatement("INSERT INTO " + getQualifiedTableName(schema, SchedulerDBConstants.CUST_SPACE_STATUS_TABLENAME) + 
					" (" + SchedulerDBConstants.COL_ID + "," +  SchedulerDBConstants.COL_SPACE_ID + "," +	SchedulerDBConstants.COL_JOB_ID + "," +							
					SchedulerDBConstants.COL_LOAD_GROUP + "," +  SchedulerDBConstants.COL_LOAD_ID + "," +
					SchedulerDBConstants.COL_STEP + "," + SchedulerDBConstants.COL_STATUS + "," +
					SchedulerDBConstants.COL_CREATED_DATE + ") " + 
					" VALUES (?,?,?,?,?,?,?,?)");
		
			String id = spaceStatus.getID();
			String spaceID = spaceStatus.getSpaceID();
			String jobID = spaceStatus.getJobID();
			String loadGroup = spaceStatus.getLoadGroup();
			int loadNumber = spaceStatus.getLoadNumber();
			String step = spaceStatus.getStep();
			int status = spaceStatus.getStatus();
			
			// truncate the strings to make sure that they are no bigger than the fields
			pstmt.setString(1, id);
			pstmt.setString(2, spaceID);
			pstmt.setString(3,jobID);
			pstmt.setString(4, loadGroup);
			if(loadNumber > 0)
			{
				pstmt.setInt(5, loadNumber);
			}
			else
			{
				pstmt.setNull(5, Types.INTEGER);
			}
			pstmt.setString(6, step);
			if(status > 0)
			{
				pstmt.setInt(7, status);
			}
			else
			{
				pstmt.setNull(7, Types.INTEGER);
			}

			Date date = new Date();				
			Timestamp startTime = new Timestamp(date.getTime());
			pstmt.setTimestamp(8, startTime);  
			pstmt.execute();
			logger.debug("Added space status to " + SchedulerDBConstants.CUST_SPACE_STATUS_TABLENAME + " : " + spaceStatus.toString());
		}
		finally
		{
			try
			{
				if(pstmt != null)
				{
					pstmt.close();
				}
			}catch(Exception ex)
			{
				logger.warn("Error in closing statement", ex);
			}
		}
	}
	
	/**
	 * Returns the top 1 , the most recent space status
	 * entry for a give space and job id
	 * @param conn
	 * @param schema
	 * @param spaceID
	 * @param jobID
	 * @return
	 * @throws SQLException
	 */
	public static SpaceStatus getMostRecentSpaceStatusForJob(Connection conn, String schema, String spaceID, String jobID) throws SQLException
	{
		PreparedStatement pstmt = null;
		SpaceStatus response = null;
		try
		{
			String sql = "SELECT " + (dConn.supportsTop() ? "TOP 1 " : "") + SchedulerDBConstants.COL_ID + "," + SchedulerDBConstants.COL_JOB_ID +  "," +
					 SchedulerDBConstants.COL_LOAD_ID + "," + SchedulerDBConstants.COL_LOAD_GROUP +  "," +
					 SchedulerDBConstants.COL_STEP +  "," + SchedulerDBConstants.COL_STATUS +  "," +
					 SchedulerDBConstants.COL_CHECKIN_TIME +  "," + SchedulerDBConstants.COL_CREATED_DATE +  "," +
					 SchedulerDBConstants.COL_MODIFIED_DATE +
					" FROM " + getQualifiedTableName(schema, SchedulerDBConstants.CUST_SPACE_STATUS_TABLENAME) +
					" WHERE " + SchedulerDBConstants.COL_SPACE_ID + "=? " + 
					" ORDER BY " + SchedulerDBConstants.COL_CREATED_DATE + " DESC";
			if (dConn.supportsRowNum())
			{
				sql = "SELECT * FROM (" + sql + ") WHERE ROWNUM <=1";
			}
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, spaceID);
			
			ResultSet set = pstmt.executeQuery();
			while(set.next())
			{	
				String recordId = (String) set.getObject(1);
				String recordJobId = (String) set.getObject(2);
				int recordLoadId = set.getInt(3);
				String recordLoadGroup = set.getString(4);
				String recordStep = set.getString(5);
				int recordStepStatus = set.getInt(6);
				long recordCheckInTime = set.getLong(7);
				Timestamp createdDate = set.getTimestamp(8);
				Timestamp modifiedDate = set.getTimestamp(9);
				response = new SpaceStatus(recordId, spaceID, recordJobId, recordLoadId, recordLoadGroup, recordStep.toString(), recordStepStatus);
				response.setCheckInTime(recordCheckInTime);
				response.setCreatedDate(createdDate);
				response.setModifiedDate(modifiedDate);
				
			}
			
			return response;
		}finally
		{
			try
			{
				if(pstmt != null)
				{
					pstmt.close();
				}
			}catch(Exception ex)
			{
				logger.warn("Error in closing statement", ex);
			}
		}
	}
	
	/**
	 * Returns all the step statuses for the most recent operation
	 * for given spaceID, loadID, loadGroup
	 * @param conn
	 * @param schema
	 * @param spaceID
	 * @param loadID
	 * @param loadGroup
	 * @return
	 * @throws SQLException
	 */
	public static List<SpaceStatus> getMostRecentSpaceStatus(Connection conn, String schema, String spaceID,
			int loadID, String loadGroup) throws SQLException
	{
		PreparedStatement pstmt = null;
		List<SpaceStatus> response = new ArrayList<SpaceStatus>();
		try
		{
			String subQuery = " SELECT " + (dConn.supportsTop() ? "TOP 1 " : "") + SchedulerDBConstants.COL_JOB_ID +
					" FROM " + getQualifiedTableName(schema, SchedulerDBConstants.CUST_SPACE_STATUS_TABLENAME) +
					" WHERE " + SchedulerDBConstants.COL_SPACE_ID + "=? " + 
					" ORDER BY " + SchedulerDBConstants.COL_CREATED_DATE + " DESC";
			if (dConn.supportsRowNum())
			{
				subQuery = "SELECT * FROM (" + subQuery + ") WHERE ROWNUM <= 1";  
			}
					
			String sql = "SELECT " + SchedulerDBConstants.COL_ID + "," + SchedulerDBConstants.COL_JOB_ID +  "," +
					 SchedulerDBConstants.COL_LOAD_ID + "," + SchedulerDBConstants.COL_LOAD_GROUP +  "," +
					 SchedulerDBConstants.COL_STEP +  "," + SchedulerDBConstants.COL_STATUS +  "," +
					 SchedulerDBConstants.COL_CHECKIN_TIME +  "," + SchedulerDBConstants.COL_CREATED_DATE +  "," +
					 SchedulerDBConstants.COL_MODIFIED_DATE +
					" FROM " + getQualifiedTableName(schema, SchedulerDBConstants.CUST_SPACE_STATUS_TABLENAME) +
					" WHERE " + SchedulerDBConstants.COL_JOB_ID + 
					" IN (" +
					  subQuery +
					" )  ORDER BY " + SchedulerDBConstants.COL_CREATED_DATE + " DESC" ;

			
			List<Object> types = new ArrayList<Object>();
			if(loadID > 0)
			{
				sql = sql +	" AND " + SchedulerDBConstants.COL_LOAD_ID + "=?"; 
				types.add(new Integer(loadID));
			}	
			
			if(loadGroup != null && loadGroup.length() > 0)
			{
				sql = sql + " AND " + SchedulerDBConstants.COL_LOAD_GROUP + "=?";
				types.add(loadGroup);
			}
			
			pstmt = conn.prepareStatement(sql);
			
			pstmt.setString(1, spaceID);
			
			int paramIndex = 2;
			for(Object obj : types)
			{
				if(obj instanceof String)
				{
					pstmt.setString(paramIndex, (String) obj);
				}
				
				if(obj instanceof Integer)
				{
					pstmt.setInt(paramIndex, ((Integer)obj).intValue());
				}
				
				paramIndex++;
			}
			ResultSet set = pstmt.executeQuery();
			while(set.next())
			{	
				String recordId = (String) set.getObject(1);
				String recordJobId = (String) set.getObject(2);
				int recordLoadId = set.getInt(3);
				String recordLoadGroup = set.getString(4);
				String recordStep = set.getString(5);
				int recordStepStatus = set.getInt(6);
				long recordCheckInTime = set.getLong(7);
				Timestamp createdDate = set.getTimestamp(8);
				Timestamp modifiedDate = set.getTimestamp(9);
				SpaceStatus spaceStatus = new SpaceStatus(recordId, spaceID, recordJobId, recordLoadId, recordLoadGroup, recordStep.toString(), recordStepStatus);
				spaceStatus.setCheckInTime(recordCheckInTime);
				spaceStatus.setCreatedDate(createdDate);
				spaceStatus.setModifiedDate(modifiedDate);
				response.add(spaceStatus);
			}
			
			return response;
		}finally
		{
			try
			{
				if(pstmt != null)
				{
					pstmt.close();
				}
			}catch(Exception ex)
			{
				logger.warn("Error in closing statement", ex);
			}
		}
	}
	
	public static SpaceStatus getSpaceStatus(Connection conn, String schema, String spaceStatusId) throws SQLException
	{
		
		PreparedStatement pstmt = null;
		SpaceStatus spaceStatus = null;
		try
		{
			String sql = "SELECT " + SchedulerDBConstants.COL_SPACE_ID + "," + SchedulerDBConstants.COL_JOB_ID +  "," +
					 SchedulerDBConstants.COL_LOAD_ID + "," + SchedulerDBConstants.COL_LOAD_GROUP +  "," +
					 SchedulerDBConstants.COL_STEP +  "," + SchedulerDBConstants.COL_STATUS +  "," +
					 SchedulerDBConstants.COL_CHECKIN_TIME +  "," + SchedulerDBConstants.COL_CREATED_DATE +  "," +
					 SchedulerDBConstants.COL_MODIFIED_DATE +
					" FROM " + getQualifiedTableName(schema, SchedulerDBConstants.CUST_SPACE_STATUS_TABLENAME) + 
					" WHERE " + SchedulerDBConstants.COL_ID + "=? " ;
			
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, spaceStatusId);
			ResultSet set = pstmt.executeQuery();
			while(set.next())
			{				
				spaceStatus = new SpaceStatus();
				String spaceID = (String) set.getObject(1);
				String recordJobId = (String) set.getObject(2);
				int recordLoadId = set.getInt(3);
				String recordLoadGroup = set.getString(4);
				String recordStep = set.getString(5);
				int recordStepStatus = set.getInt(6);
				long recordCheckInTime = set.getLong(7);
				Timestamp createdDate = set.getTimestamp(8);
				Timestamp modifiedDate = set.getTimestamp(9);
				spaceStatus = new SpaceStatus(spaceStatusId, spaceID, recordJobId, recordLoadId, recordLoadGroup, recordStep.toString(), recordStepStatus);
				spaceStatus.setCheckInTime(recordCheckInTime);
				spaceStatus.setCreatedDate(createdDate);
				spaceStatus.setModifiedDate(modifiedDate);
			}
			
			return spaceStatus;
		}
		finally
		{
			try
			{
				if(pstmt != null)
				{
					pstmt.close();
				}
			}catch(Exception ex)
			{
				logger.warn("Error in closing statement", ex);
			}
		}
	}
	public static Map<String, List<SpaceStatus>> getSpaceStatus(Connection conn, String schema, 
			String spaceID, String jobID, int loadID, 
			String loadGroup, String step, int status) throws SQLException
	{
		PreparedStatement pstmt = null;
		Map<String, List<SpaceStatus>> response = new HashMap<String, List<SpaceStatus>>();
		try
		{
			String sql = "SELECT " + SchedulerDBConstants.COL_ID + "," + SchedulerDBConstants.COL_JOB_ID +  "," +
					 SchedulerDBConstants.COL_LOAD_ID + "," + SchedulerDBConstants.COL_LOAD_GROUP +  "," +
					 SchedulerDBConstants.COL_STEP +  "," + SchedulerDBConstants.COL_STATUS +  "," +
					 SchedulerDBConstants.COL_CHECKIN_TIME +  "," + SchedulerDBConstants.COL_CREATED_DATE +  "," +
					 SchedulerDBConstants.COL_MODIFIED_DATE +
					" FROM " + getQualifiedTableName(schema, SchedulerDBConstants.CUST_SPACE_STATUS_TABLENAME) + 
					" WHERE " + SchedulerDBConstants.COL_SPACE_ID + "=? " ;
			
			List<Object> types = new ArrayList<Object>();
			if(jobID != null && jobID.length() > 0)
			{
				sql = sql + " AND " + SchedulerDBConstants.COL_JOB_ID + "=?";
				types.add(jobID);
			}
			
			if(loadID > 0)
			{
				sql = sql +	" AND " + SchedulerDBConstants.COL_LOAD_ID + "=?"; 
				types.add(new Integer(loadID));
			}	
			
			if(loadGroup != null && loadGroup.length() > 0)
			{
				sql = sql + " AND " + SchedulerDBConstants.COL_LOAD_GROUP + "=?";
				types.add(loadGroup);
			}
			
			if(step != null && step.length() > 0)
			{
				sql = sql + " AND " + SchedulerDBConstants.COL_STEP + "=?";
				types.add(step);
			}
			
			if(status >= 0)
			{
				sql = sql + " AND " + SchedulerDBConstants.COL_STATUS + "=?";
				types.add(new Integer(status));
			}
			
			
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, spaceID);
			int paramIndex = 2;
			for(Object obj : types)
			{
				if(obj instanceof String)
				{
					pstmt.setString(paramIndex, (String) obj);
				}
				
				if(obj instanceof Integer)
				{
					pstmt.setInt(paramIndex, ((Integer)obj).intValue());
				}
				
				paramIndex++;
			}
			
			ResultSet set = pstmt.executeQuery();
						
			while(set.next())
			{				
				SpaceStatus spaceStatus = new SpaceStatus();
				String recordId = (String) set.getObject(1);
				String recordJobId = (String) set.getObject(2);
				int recordLoadId = set.getInt(3);
				String recordLoadGroup = set.getString(4);
				String recordStep = set.getString(5);
				int recordStepStatus = set.getInt(6);
				long recordCheckInTime = set.getLong(7);
				Timestamp createdDate = set.getTimestamp(8);
				Timestamp modifiedDate = set.getTimestamp(9);
				spaceStatus = new SpaceStatus(recordId, spaceID, recordJobId, recordLoadId, recordLoadGroup, recordStep.toString(), recordStepStatus);
				spaceStatus.setCheckInTime(recordCheckInTime);
				spaceStatus.setCreatedDate(createdDate);
				spaceStatus.setModifiedDate(modifiedDate);
				
				if(!response.containsKey(Util.getLowerCaseString(recordJobId)))
				{
					response.put(Util.getLowerCaseString(recordJobId), new ArrayList<SpaceStatus>());
				}
				
				response.get(Util.getLowerCaseString(recordJobId)).add(spaceStatus);
			}
			
			return response;
		}finally
		{
			try
			{
				if(pstmt != null)
				{
					pstmt.close();
				}
			}catch(Exception ex)
			{
				logger.warn("Error in closing statement", ex);
			}
		}
	}
	
	public static List<SpaceStatus> getSpaceStatus(Connection conn, String schema, String spaceID, String loadGroup, int loadNumber) throws SQLException
	{
		PreparedStatement pstmt = null;
		List<SpaceStatus> response = new ArrayList<SpaceStatus>();
		try
		{
			String sql = "SELECT " + SchedulerDBConstants.COL_ID +  "," + SchedulerDBConstants.COL_JOB_ID +  "," + 
					SchedulerDBConstants.COL_STEP +  "," +
					SchedulerDBConstants.COL_CHECKIN_TIME +  "," + SchedulerDBConstants.COL_LOAD_GROUP +  "," +
					SchedulerDBConstants.COL_LOAD_ID + " FROM " + getQualifiedTableName(schema, SchedulerDBConstants.CUST_SPACE_STATUS_TABLENAME) + 
					" WHERE " + SchedulerDBConstants.COL_SPACE_ID + "=?"; 
				
			String loadGroupSqlPart = null;
			if(loadGroup != null)
			{
				loadGroupSqlPart = " AND " + SchedulerDBConstants.COL_LOAD_GROUP + "=?";
			}
			
			String loadNumberSqlPart = null;
			if(loadNumber > -1)
			{
				loadNumberSqlPart = " AND " + SchedulerDBConstants.COL_LOAD_ID + "=?";
			}
			
			sql = sql + loadGroupSqlPart + loadNumberSqlPart;
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, spaceID);
			if(loadGroupSqlPart != null)
			{
				pstmt.setString(2, loadGroup);
			}
			if(loadNumberSqlPart != null)
			{
				pstmt.setInt(3, loadNumber);
			}
			
			ResultSet set = pstmt.executeQuery();
			while(set.next())
			{
				SpaceStatus spaceStatus = new SpaceStatus();
				spaceStatus.setSpaceID(spaceID);
				spaceStatus.setId((String)set.getObject(0));
				spaceStatus.setJobID((String)set.getObject(1));
				spaceStatus.setStep(set.getString(2));
				spaceStatus.setCheckInTime(set.getLong(31));
				spaceStatus.setLoadNumber(set.getInt(4));
				spaceStatus.setLoadGroup(set.getString(5));
				response.add(spaceStatus);
			}
		}finally
		{
			try
			{
				if(pstmt != null)
				{
					pstmt.close();
				}
			}catch(Exception ex)
			{
				logger.warn("Error in closing statement", ex);
			}
		}
		return response;
	}	

	public static void updateSpaceStatus(Connection conn, String schema, String id, int status, String message) throws SQLException
	{
		updateSpaceStatus(conn, schema, id, status, message, false);
	}
			
	public static void updateSpaceStatus(Connection conn, String schema, String id, int status, String message, boolean putInEndDate) throws SQLException
	{
		PreparedStatement pstmt = null;
		try
		{
			String sql = "UPDATE " + getQualifiedTableName(schema, SchedulerDBConstants.CUST_SPACE_STATUS_TABLENAME) + 
						" SET " + SchedulerDBConstants.COL_STATUS + "=?," +
						SchedulerDBConstants.COL_MESSAGE + "=?," + SchedulerDBConstants.COL_MODIFIED_DATE + "=?" +
						" WHERE " + SchedulerDBConstants.COL_ID + "=?";
			
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, status);
			if(message != null && message.length() > 0)
			{
				pstmt.setString(2, message);
			}
			else
			{
				pstmt.setNull(2, Types.VARCHAR);
			}
			
			if(putInEndDate)
			{
				Date date = new Date();				
				Timestamp endTime = new Timestamp(date.getTime());
				pstmt.setTimestamp(3, endTime);
			}
			else
			{
				pstmt.setNull(3, Types.TIMESTAMP);
			}
			pstmt.setString(4, id);
			int rows = pstmt.executeUpdate();
			logger.debug("Updated " + rows + " for sql : " + sql + " : " + "id : " + id);
		}finally
		{
			try
			{
				if(pstmt != null)
				{
					pstmt.close();
				}
			}catch(Exception ex)
			{
				logger.warn("Error in closing statement", ex);
			}
		}	
	}
	
	public static void updateSpaceStatus(Connection conn, String schema, 
			String jobId, String stepName, int fromStatus, int toStatus, 
			String message, boolean putInEndDate) throws SQLException
	{
		PreparedStatement pstmt = null;
		try
		{
			String sql = "UPDATE " + getQualifiedTableName(schema, SchedulerDBConstants.CUST_SPACE_STATUS_TABLENAME) + 
						" SET " + SchedulerDBConstants.COL_STATUS + "=?," +
						SchedulerDBConstants.COL_MESSAGE + "=?," + SchedulerDBConstants.COL_MODIFIED_DATE + "=?" +
						" WHERE " + SchedulerDBConstants.COL_JOB_ID + "=? " +
						" AND " + SchedulerDBConstants.COL_STEP + "=?" + 
						" AND " + SchedulerDBConstants.COL_STATUS + "=?";
			
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, toStatus);
			if(message != null && message.length() > 0)
			{
				pstmt.setString(2, message);
			}
			else
			{
				pstmt.setNull(2, Types.VARCHAR);
			}
			
			if(putInEndDate)
			{
				Date date = new Date();				
				Timestamp endTime = new Timestamp(date.getTime());
				pstmt.setTimestamp(3, endTime);
			}
			else
			{
				pstmt.setNull(3, Types.TIMESTAMP);
			}
			pstmt.setString(4, jobId);
			pstmt.setString(5, stepName);
			pstmt.setInt(6, fromStatus);
			int rows = pstmt.executeUpdate();
			logger.debug("Updated " + rows + " for sql : " + sql + " : " + " : jobID : " + jobId);  
			
		}finally
		{
			try
			{
				if(pstmt != null)
				{
					pstmt.close();
				}
			}catch(Exception ex)
			{
				logger.warn("Error in closing statement", ex);
			}
		}	
	}
	
	public void updateSpaceStatusToTerminate(Connection conn, String schema, String id, boolean terminate) throws SQLException
	{
		PreparedStatement pstmt = null;
		try
		{
			String sql = "UPDATE " + getQualifiedTableName(schema, SchedulerDBConstants.CUST_SPACE_STATUS_TABLENAME) + 
						" SET " + SchedulerDBConstants.COL_TERMINIATE + "=?" +
						" WHERE " + SchedulerDBConstants.COL_ID + "=?";
			
			pstmt = conn.prepareStatement(sql);
			pstmt.setBoolean(1, terminate);
			pstmt.setString(2, id);
			int result = pstmt.executeUpdate();
			logger.debug("Flagged " + result + " entry in " + SchedulerDBConstants.CUST_SPACE_STATUS_TABLENAME + 
					" for id : " + id + " : terminate : " + terminate);
			
		}finally
		{
			try
			{
				if(pstmt != null)
				{
					pstmt.close();
				}
			}catch(Exception ex)
			{
				logger.warn("Error in closing statement", ex);
			}
		}
	}
	
	public void updateSpaceStatusLoadNumber(Connection conn, String schema, String id, int loadNumber) throws SQLException
	{
		PreparedStatement pstmt = null;
		try
		{
			String sql = "UPDATE " + getQualifiedTableName(schema, SchedulerDBConstants.CUST_SPACE_STATUS_TABLENAME) + 
						" SET " + SchedulerDBConstants.COL_STATUS + "=?" +
						" WHERE " + SchedulerDBConstants.COL_ID + "=?";
			
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, loadNumber);
			pstmt.setString(2, id);
			int result = pstmt.executeUpdate();
			logger.debug("Updated  " + result + " entries from " + SchedulerDBConstants.CUST_SPACE_STATUS_TABLENAME + 
					" for id : " + id + " : loadId : " + loadNumber);
			
		}finally
		{
			try
			{
				if(pstmt != null)
				{
					pstmt.close();
				}
			}catch(Exception ex)
			{
				logger.warn("Error in closing statement", ex);
			}
		}
	}
	
	public static void updateCheckInTime(Connection conn, String schema, String id, long checkInTimeMillseconds) throws SQLException
	{
		PreparedStatement pstmt = null;
		try
		{
			String sql = "UPDATE " + getQualifiedTableName(schema, SchedulerDBConstants.CUST_SPACE_STATUS_TABLENAME) + 
						" SET " + SchedulerDBConstants.COL_CHECKIN_TIME + "=?" +
						" WHERE " + SchedulerDBConstants.COL_ID + "=?";
			
			pstmt = conn.prepareStatement(sql);
			pstmt.setLong(1, checkInTimeMillseconds);
			pstmt.setString(2, id);
			int result = pstmt.executeUpdate();
			
			logger.debug("Updated " + result + " entries from " + SchedulerDBConstants.CUST_SPACE_STATUS_TABLENAME + 
					" for id : " + id);
			
		}finally
		{
			try
			{
				if(pstmt != null)
				{
					pstmt.close();
				}
			}catch(Exception ex)
			{
				logger.warn("Error in closing statement", ex);
			}
		}
	}
	
	public static void deleteSpaceStatus(Connection conn, String schema, String spaceId, int loadId, String loadGroup) throws SQLException
	{
		PreparedStatement pstmt = null;
		try
		{
			String sql = "DELETE FROM " + getQualifiedTableName(schema, SchedulerDBConstants.CUST_SPACE_STATUS_TABLENAME) +
			" WHERE " + SchedulerDBConstants.COL_SPACE_ID + "=?" + 
			" AND " + SchedulerDBConstants.COL_LOAD_ID + "=?" + 
			" AND " + SchedulerDBConstants.COL_LOAD_GROUP + "=?";

			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, spaceId);
			pstmt.setInt(2, loadId);
			pstmt.setString(3, loadGroup);
			
			int result = pstmt.executeUpdate();
			logger.debug("Deleted " + result + " entries from " + SchedulerDBConstants.CUST_SPACE_STATUS_TABLENAME + 
					" for space : " + spaceId + " : loadId : " + loadId + " : loadGroup : " + loadGroup);
		}
		finally
		{
			try
			{
				if(pstmt != null)
				{
					pstmt.close();
				}
			}catch(Exception ex)
			{
				logger.warn("Error in closing statement", ex);
			}
		}
	}


	public static void addProcessInfo(Connection conn, String schema, ProcessInfo processInfo) throws SQLException
	{
		PreparedStatement pstmt = null;
		try
		{
			String id = processInfo.getId();
			String spaceId = processInfo.getSpaceId();
			String userId = processInfo.getUserId();
			int loadId = processInfo.getLoadId();
			String loadDate = processInfo.getLoadDate();
			String loadGroup = processInfo.getLoadGroup();
			String subGroups = processInfo.getSubGroups();
			boolean retryFailedLoad = processInfo.getRetryFailedLoad();
			boolean sendEmail = processInfo.getSendEmail();
			
			pstmt = conn.prepareStatement("INSERT INTO " + getQualifiedTableName(schema, SchedulerDBConstants.CUST_PROCESS_INFO_TABLENAME) + 
					" (" + SchedulerDBConstants.COL_ID + "," +  SchedulerDBConstants.COL_SPACE_ID + "," +	
					SchedulerDBConstants.COL_USER_ID + "," + SchedulerDBConstants.COL_LOAD_ID + "," +  
					SchedulerDBConstants.COL_LOAD_DATE + " ," + SchedulerDBConstants.COL_LOAD_GROUP + "," +
					SchedulerDBConstants.COL_SUBGROUPS + "," +  SchedulerDBConstants.COL_RETRY_FAILED + "," +
					SchedulerDBConstants.COL_SEND_EMAIL + "," + 
					SchedulerDBConstants.COL_CREATED_DATE + ") " + 
					" VALUES (?,?,?,?,?,?,?,?,?,?)");
			
			pstmt.setString(1, id);
			pstmt.setString(2, spaceId);
			pstmt.setString(3, userId);
			pstmt.setInt(4, loadId);
			pstmt.setString(5, loadDate);
			pstmt.setString(6, loadGroup);
			if(subGroups != null && subGroups.length() > 0)
			{
				pstmt.setString(7, subGroups);
			}
			else
			{
				pstmt.setNull(7, Types.VARCHAR);
			}
			
			pstmt.setBoolean(8, retryFailedLoad);
			pstmt.setBoolean(9, sendEmail);
			Date date = new Date();				
			Timestamp startTime = new Timestamp(date.getTime());
			pstmt.setTimestamp(10, startTime);
			
			int result = pstmt.executeUpdate();
			logger.debug("Added " + result + " entry into table " + SchedulerDBConstants.CUST_PROCESS_INFO_TABLENAME + " for space : " + 
					spaceId + " : loadId : " + loadId + " : loadGroup : " + loadGroup + " : subgroups : " + subGroups );
		}
		finally
		{
			try
			{
				if(pstmt != null)
				{
					pstmt.close();
				}
			}catch(Exception ex)
			{
				logger.warn("Error in closing statement", ex);
			}
		}
	}
	
	public static void removeProcessInfo(Connection conn, String schema, 
			String spaceID , int loadId, String loadGroup) throws SQLException
	{
		PreparedStatement pstmt = null;
		try
		{
			String sql = "DELETE FROM " + getQualifiedTableName(schema, SchedulerDBConstants.CUST_PROCESS_INFO_TABLENAME) +
			" WHERE " + SchedulerDBConstants.COL_SPACE_ID + "=? AND " + 
			SchedulerDBConstants.COL_LOAD_ID + "=? AND " + SchedulerDBConstants.COL_LOAD_GROUP + "=?" ;

			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, spaceID);
			pstmt.setInt(2, loadId);
			pstmt.setString(3, loadGroup);
			
			int result = pstmt.executeUpdate();
			logger.debug("Deleted " + result + " entry from " + SchedulerDBConstants.CUST_PROCESS_INFO_TABLENAME + 
					" for load id : " + loadId + " : loadGroup : " + loadGroup);
		}
		finally
		{
			try
			{
				if(pstmt != null)
				{
					pstmt.close();
				}
			}catch(Exception ex)
			{
				logger.warn("Error in closing statement", ex);
			}
		}
	}
	
	public static void removeProcessInfo(Connection conn, String schema, String id) throws SQLException
	{
		PreparedStatement pstmt = null;
		try
		{
			String sql = "DELETE FROM " + getQualifiedTableName(schema, SchedulerDBConstants.CUST_PROCESS_INFO_TABLENAME) +
			" WHERE " + SchedulerDBConstants.COL_ID + "=?" ;

			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, id);
			
			int result = pstmt.executeUpdate();
			logger.debug("Deleted " + result + " entry from " + SchedulerDBConstants.CUST_PROCESS_INFO_TABLENAME + 
					" for id : " + id);
		}
		finally
		{
			try
			{
				if(pstmt != null)
				{
					pstmt.close();
				}
			}catch(Exception ex)
			{
				logger.warn("Error in closing statement", ex);
			}
		}
	}
	
	public static void addConnectorInfo(Connection conn, String schema, SFDCInfo connectorInfo) throws SQLException
	{
		PreparedStatement pstmt = null;
		try
		{
			String id = connectorInfo.getId();
			String spaceId = connectorInfo.getSpaceId();
			String userId = connectorInfo.getUserId();
			boolean runNowMode = connectorInfo.isRunNowMode();
			boolean processAfter = connectorInfo.isProcessAfter();
			String loadDate = connectorInfo.getLoadDate();
			String extractGroups = connectorInfo.getExtractGroups();
			String subGroups = connectorInfo.getSubGroups();
			String clientId = connectorInfo.getClientId();
			String sandBoxUrl = connectorInfo.getSandBoxUrl();
			boolean sendEmail = connectorInfo.getSendEmail();
			String name = connectorInfo.getName();
			String type = connectorInfo.getType();
			int engine = connectorInfo.getExtractionEngine();
			pstmt = conn.prepareStatement("INSERT INTO " + getQualifiedTableName(schema, SchedulerDBConstants.CUST_CONNECTORS_INFO_TABLENAME) + 
					" (" + SchedulerDBConstants.COL_ID + "," +  SchedulerDBConstants.COL_SPACE_ID + "," +							
					SchedulerDBConstants.COL_USER_ID + "," +  SchedulerDBConstants.COL_RUN_NOW_MODE + "," +
					SchedulerDBConstants.COL_CLIENT_ID + "," +  SchedulerDBConstants.COL_SANBOX_URL + "," +
					SchedulerDBConstants.COL_PROCESS_AFTER + "," +  SchedulerDBConstants.COL_LOAD_DATE + "," +
					SchedulerDBConstants.COL_SUBGROUPS + "," + SchedulerDBConstants.COL_SEND_EMAIL + "," +
					SchedulerDBConstants.COL_CREATED_DATE  + "," + SchedulerDBConstants.COL_CONN_SCHED_NAME + "," +
					SchedulerDBConstants.COL_CONN_TYPE + "," + SchedulerDBConstants.COL_CONN_ENGINE + "," +
					SchedulerDBConstants.COL_EXTRACTGROUPS + ") " +
					" VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
			
			pstmt.setString(1, id);
			pstmt.setString(2, spaceId);
			pstmt.setString(3, userId);
			pstmt.setBoolean(4, runNowMode);
			if(clientId != null && clientId.trim().length() > 0)
			{
				pstmt.setString(5, clientId);
			}
			else
			{
				pstmt.setNull(5, Types.VARCHAR);
			}
			
			if(sandBoxUrl != null && sandBoxUrl.trim().length() > 0)
			{
				pstmt.setString(6, sandBoxUrl);
			}
			else
			{
				pstmt.setNull(6, Types.VARCHAR);
			}
			
			pstmt.setBoolean(7, processAfter);
			if(loadDate != null && loadDate.trim().length() > 0)
			{
				pstmt.setString(8, loadDate);
			}
			else
			{
				pstmt.setNull(8, Types.VARCHAR);
			}
			
			if(subGroups != null && subGroups.trim().length() > 0)
			{
				pstmt.setString(9, subGroups);
			}
			else
			{
				pstmt.setNull(9, Types.VARCHAR);
			}
			
			pstmt.setBoolean(10, sendEmail);
			Date date = new Date();				
			Timestamp startTime = new Timestamp(date.getTime());
			pstmt.setTimestamp(11, startTime);
			if(name != null && name.trim().length() > 0)
			{
				pstmt.setString(12, name);
			}
			else 
			{
				pstmt.setNull(12, Types.VARCHAR);
			}
			pstmt.setString(13, type);
			pstmt.setInt(14, engine);
			if(extractGroups != null && extractGroups.trim().length() > 0)
			{
				pstmt.setString(15, extractGroups);
			}
			else
			{
				pstmt.setNull(15, Types.VARCHAR);
			}
			int result = pstmt.executeUpdate();
			logger.debug("Added " + result + " entry into table " + SchedulerDBConstants.CUST_CONNECTORS_INFO_TABLENAME + " : " + connectorInfo.toString());
 		}
		finally
		{
			try
			{
				if(pstmt != null)
				{
					pstmt.close();
				}
			}catch(Exception ex)
			{
				logger.warn("Error in closing statement", ex);
			}
		}
	}
	
	public static void deleteConnectorInfo(Connection conn, String schema, String id) throws SQLException
	{
		PreparedStatement pstmt = null;
		try
		{
			String sql = "DELETE FROM " + getQualifiedTableName(schema, SchedulerDBConstants.CUST_CONNECTORS_INFO_TABLENAME) +
			" WHERE " + SchedulerDBConstants.COL_ID + "=?" ;

			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, id);
			
			int result = pstmt.executeUpdate();
			logger.debug("Deleted " + result + " entry from " + SchedulerDBConstants.CUST_CONNECTORS_INFO_TABLENAME + 
					" for id : " + id);
		}
		finally
		{
			try
			{
				if(pstmt != null)
				{
					pstmt.close();
				}
			}catch(Exception ex)
			{
				logger.warn("Error in closing statement", ex);
			}
		}
	}


	public static void updateConnectorInfo(Connection conn, String schema, SFDCInfo connectorInfo) throws SQLException
	{
		String id = connectorInfo.getId();
		String spaceID = connectorInfo.getSpaceId();
		String userID = connectorInfo.getUserId();
		String clientId = connectorInfo.getClientId();
		String sandBoxUrl = connectorInfo.getSandBoxUrl();
		boolean process = connectorInfo.isProcessAfter();
		String loadDate = connectorInfo.getLoadDate();
		String subGroups = connectorInfo.getSubGroups();
		String extractGroups = connectorInfo.getExtractGroups();
		String modifiedBy = connectorInfo.getModifiedBy();
		String name = connectorInfo.getName();
		String type = connectorInfo.getType();
		
		PreparedStatement pstmt = null;
		try
		{
			pstmt = conn.prepareStatement("UPDATE " + getQualifiedTableName(schema, SchedulerDBConstants.CUST_CONNECTORS_INFO_TABLENAME) +
					" SET " + SchedulerDBConstants.COL_SPACE_ID + "= ?," +  SchedulerDBConstants.COL_USER_ID + "= ?," +					
					SchedulerDBConstants.COL_CLIENT_ID + "= ?," + 
					SchedulerDBConstants.COL_SANBOX_URL + "= ?," + SchedulerDBConstants.COL_PROCESS_AFTER + "= ?," +
					SchedulerDBConstants.COL_LOAD_DATE + "= ?," +  SchedulerDBConstants.COL_SUBGROUPS + "= ?," +
					SchedulerDBConstants.COL_MODIFIED_DATE + "=?, " + SchedulerDBConstants.COL_MODIFIED_BY + "= ?, " +
					SchedulerDBConstants.COL_CONN_SCHED_NAME + "=?, " + SchedulerDBConstants.COL_CONN_TYPE + "=?, " +
					SchedulerDBConstants.COL_EXTRACTGROUPS + "=? " +
					" WHERE ID = ?");
			
			// truncate the strings to make sure that they are no bigger than the fields			
			pstmt.setString(1, spaceID);
			pstmt.setString(2, userID);
			
			if(clientId != null && clientId.trim().length() > 0)
			{
				pstmt.setString(3, clientId);
			}
			else
			{
				pstmt.setNull(3, Types.VARCHAR);
			}
			
			if(sandBoxUrl != null && sandBoxUrl.trim().length() > 0)
			{
				pstmt.setString(4, sandBoxUrl);
			}
			else
			{
				pstmt.setNull(4, Types.VARCHAR);
			}
			
			pstmt.setBoolean(5, process);
			if(loadDate != null && loadDate.trim().length() > 0)
			{
				pstmt.setString(6, loadDate);
			}
			else
			{
				pstmt.setNull(6, Types.VARCHAR);
			}
			if(subGroups != null && subGroups.trim().length() > 0)
			{
			pstmt.setString(7, subGroups);
			}
			else
			{
				pstmt.setNull(7, Types.VARCHAR);
			}
			
			Date date = new Date();				
			Timestamp modifiedDate = new Timestamp(date.getTime());
			pstmt.setTimestamp(8, modifiedDate);
			
			if(modifiedBy != null && modifiedBy.length() > 0)
			{
				pstmt.setString(9, modifiedBy);
			}
			else
			{
				pstmt.setNull(9, Types.VARCHAR);
			}
			pstmt.setString(10, name);
			pstmt.setString(11, type);
			if(extractGroups != null && extractGroups.trim().length() > 0)
			{
				pstmt.setString(12, extractGroups);
			}
			else
			{
				pstmt.setNull(12, Types.VARCHAR);
			}
			
			pstmt.setString(13, id);			
			
			pstmt.execute();	
			logger.debug("Updated entry in " + SchedulerDBConstants.CUST_CONNECTORS_INFO_TABLENAME + " : " + connectorInfo.toString());
		}
		catch (SQLException e)
		{
			logger.error("Could not add SFDC info in the database");
			throw e;
		}
		finally 
		{
			try
			{
				if (pstmt != null)
					pstmt.close();
			}
			catch (SQLException e)
			{
				logger.warn("Error in pstmt.close()", e );
			}
		}		
	}
	
	
	public static ProcessInfo getProcessInfo(Connection conn, String schema, String uid) throws SQLException
	{
		PreparedStatement pstmt = null;
		ProcessInfo result = null;
		try
		{
			String sql = "SELECT " + SchedulerDBConstants.COL_SPACE_ID +  "," + SchedulerDBConstants.COL_USER_ID + "," +
					SchedulerDBConstants.COL_LOAD_ID +  "," + SchedulerDBConstants.COL_LOAD_DATE + "," +
					SchedulerDBConstants.COL_LOAD_GROUP +  "," + SchedulerDBConstants.COL_SUBGROUPS + "," + 
					SchedulerDBConstants.COL_RETRY_FAILED + "," + SchedulerDBConstants.COL_SEND_EMAIL + "," +
					SchedulerDBConstants.COL_CREATED_DATE + "," + SchedulerDBConstants.COL_MODIFIED_DATE + 
					" FROM " + getQualifiedTableName(schema, SchedulerDBConstants.CUST_PROCESS_INFO_TABLENAME) + 
					" WHERE " + SchedulerDBConstants.COL_ID + "=?"; 
			
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, uid);
			ResultSet set = pstmt.executeQuery();
			while(set.next())
			{
				String spaceID = (String) set.getObject(1);
				String userID = (String) set.getObject(2);
				int loadId = set.getInt(3);
				String loadDate = set.getString(4);
				String loadGroup = set.getString(5);
				String subGroups = set.getString(6);
				boolean retryFailedLoad = set.getBoolean(7);
				boolean sendEmail = set.getBoolean(8);
				Timestamp createdDate = set.getTimestamp(9);
				Timestamp modifiedDate = set.getTimestamp(10);
				ProcessInfo processInfo = new ProcessInfo(uid, spaceID, userID, loadId, loadDate, loadGroup, subGroups, 
						retryFailedLoad, sendEmail);
				processInfo.setCreatedDate(createdDate);
				processInfo.setModifiedDate(modifiedDate);
				result = processInfo;
				break;
			}
		}finally
		{
			try
			{
				if(pstmt != null)
				{
					pstmt.close();
				}
			}catch(Exception ex)
			{
				logger.warn("Error in closing statement", ex);
			}
		}
		
		return result;
	}


	public static SFDCInfo getConnectorInfo(Connection conn,
			String schema, String connectorInfoId) throws SQLException
	{
		PreparedStatement pstmt = null;
		SFDCInfo result = null;
		try
		{
			String sql = "SELECT " + SchedulerDBConstants.COL_SPACE_ID +  "," + SchedulerDBConstants.COL_USER_ID + "," +
					SchedulerDBConstants.COL_RUN_NOW_MODE + "," + SchedulerDBConstants.COL_CLIENT_ID +  "," + 
					SchedulerDBConstants.COL_SANBOX_URL + "," + SchedulerDBConstants.COL_PROCESS_AFTER + "," +
					SchedulerDBConstants.COL_LOAD_DATE +  "," + SchedulerDBConstants.COL_SUBGROUPS + "," + 
					SchedulerDBConstants.COL_SEND_EMAIL + "," +
					SchedulerDBConstants.COL_CREATED_DATE + "," + SchedulerDBConstants.COL_MODIFIED_DATE + "," +
					SchedulerDBConstants.COL_MODIFIED_BY + "," +
					SchedulerDBConstants.COL_CONN_SCHED_NAME + "," + SchedulerDBConstants.COL_CONN_TYPE + "," +
					SchedulerDBConstants.COL_CONN_ENGINE + "," + SchedulerDBConstants.COL_EXTRACTGROUPS +
					" FROM " + getQualifiedTableName(schema, SchedulerDBConstants.CUST_CONNECTORS_INFO_TABLENAME) + 
					" WHERE " + SchedulerDBConstants.COL_ID + "=?"; 
			
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, connectorInfoId);
			ResultSet set = pstmt.executeQuery();
			while(set.next())
			{	
				String spaceID = (String) set.getObject(1);
				String userID = (String) set.getObject(2);
				boolean runNowMode = set.getBoolean(3);
				String clientId = set.getString(4);				
				String sandBoxUrl = set.getString(5);
				boolean processAfter = set.getBoolean(6);				
				String loadDate = set.getString(7);				
				String subGroups = set.getString(8);
				boolean sendEmail = set.getBoolean(9);
				Timestamp createdDate = set.getTimestamp(10);
				Timestamp modifiedDate = set.getTimestamp(11);
				String modifiedBy = set.getString(12);
				String name = set.getString(13);
				String type = set.getString(14);
				int engine = set.getInt(15);
				engine = engine > 0 ? engine : -1;
				String extractGroups = set.getString(16);
				result = new SFDCInfo(connectorInfoId, spaceID, userID, runNowMode, clientId, sandBoxUrl, processAfter, loadDate, 
						subGroups, sendEmail, name, type, engine);
				result.setCreatedDate(createdDate);
				result.setModifiedDate(modifiedDate);
				result.setModifiedBy(modifiedBy);
				result.setType(CopyJob.getScheduleType(result.getType()));
				result.setExtractGroups(extractGroups);
				
				break;
			}
			
			if(result != null)
			{
				List<Schedule> scheduleList = SchedulerDatabase.getScheduleList(conn, schema, result.getId());
				Schedule[] jobSchedules = null;
				if(scheduleList != null && scheduleList.size() > 0)
				{
					jobSchedules = scheduleList.toArray(new Schedule[scheduleList.size()]);
					result.setJobSchedules(jobSchedules);
				}
			}
		}finally
		{
			try
			{
				if(pstmt != null)
				{
					pstmt.close();
				}
			}catch(Exception ex)
			{
				logger.warn("Error in closing statement", ex);
			}
		}
		
		return result;
	}


	public static DeleteInfo getDeleteInfo(Connection conn, String schema,
			String deleteInfoId) throws SQLException
	{
		PreparedStatement pstmt = null;
		DeleteInfo result = null;
		try
		{
			String sql = "SELECT " + SchedulerDBConstants.COL_SPACE_ID +  "," + SchedulerDBConstants.COL_USER_ID + "," +
					SchedulerDBConstants.COL_LOAD_GROUP + "," + SchedulerDBConstants.COL_LOAD_ID + "," +
					SchedulerDBConstants.COL_DELETE_TYPE +  "," + SchedulerDBConstants.COL_DELETE_RESTORE + "," +
					SchedulerDBConstants.COL_CREATED_DATE + "," + SchedulerDBConstants.COL_MODIFIED_DATE + "," +
					SchedulerDBConstants.COL_MODIFIED_BY + 
					" FROM " + getQualifiedTableName(schema, SchedulerDBConstants.CUST_DELETE_INFO_TABLENAME) + 
					" WHERE " + SchedulerDBConstants.COL_ID + "=?"; 
			
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, deleteInfoId);
			ResultSet set = pstmt.executeQuery();
			while(set.next())
			{
				
				String spaceID = (String) set.getObject(1);
				String userID = (String) set.getObject(2);
				String loadGroup = set.getString(3);	
				int loadId = set.getInt(4);
				String deleteType = set.getString(5);	
				boolean restoreRepository =  set.getBoolean(6);
				Timestamp createdDate = set.getTimestamp(7);
				Timestamp modifiedDate = set.getTimestamp(8);
				String modifiedBy = set.getString(9);
				result = new DeleteInfo(deleteInfoId, spaceID, userID, loadGroup, loadId, deleteType, restoreRepository);
				result.setCreatedDate(createdDate);
				result.setModifiedDate(modifiedDate);
				result.setModifiedBy(modifiedBy);
				break;
			}
		}finally
		{
			try
			{
				if(pstmt != null)
				{
					pstmt.close();
				}
			}catch(Exception ex)
			{
				logger.warn("Error in closing statement", ex);
			}
		}
		
		return result;
	}	
	
	public static void addDeleteInfo(Connection conn, String schema, DeleteInfo deleteInfo) throws SQLException
	{
		PreparedStatement pstmt = null;
		try
		{
			String id = deleteInfo.getId();
			String spaceId = deleteInfo.getSpaceID();
			String userId = deleteInfo.getUserID();
			String loadGroup = deleteInfo.getLoadGroup();
			int loadId = deleteInfo.getLoadNumber();
			String deleteType = deleteInfo.getDeleteType();
			boolean restoreRepository = deleteInfo.isRestoreRepository();
			pstmt = conn.prepareStatement("INSERT INTO " + getQualifiedTableName(schema, SchedulerDBConstants.CUST_DELETE_INFO_TABLENAME) + 
					" (" + SchedulerDBConstants.COL_ID + "," +  SchedulerDBConstants.COL_SPACE_ID + "," +							
					SchedulerDBConstants.COL_USER_ID + "," +  SchedulerDBConstants.COL_LOAD_GROUP + "," +
					SchedulerDBConstants.COL_LOAD_ID + "," + SchedulerDBConstants.COL_DELETE_TYPE + "," +  
					SchedulerDBConstants.COL_DELETE_RESTORE + "," + SchedulerDBConstants.COL_CREATED_DATE + ") " + 
					" VALUES (?,?,?,?,?,?,?,?)");
			
			pstmt.setString(1, id);
			pstmt.setString(2, spaceId);
			pstmt.setString(3, userId);
			pstmt.setString(4, loadGroup);
			pstmt.setInt(5, loadId);
			pstmt.setString(6, deleteType);
			pstmt.setBoolean(7, restoreRepository);
			
			Date date = new Date();				
			Timestamp startTime = new Timestamp(date.getTime());
			pstmt.setTimestamp(8, startTime);
			int result = pstmt.executeUpdate();
			logger.debug("Added " + result + " entry into table " + SchedulerDBConstants.CUST_DELETE_INFO_TABLENAME + " : " + deleteInfo.toString());
 		}
		finally
		{
			try
			{
				if(pstmt != null)
				{
					pstmt.close();
				}
			}catch(Exception ex)
			{
				logger.warn("Error in closing statement", ex);
			}
		}
	}
	
	public static void deleteDeleteInfo(Connection conn, String schema, String id) throws SQLException
	{
		PreparedStatement pstmt = null;
		try
		{
			String sql = "DELETE FROM " + getQualifiedTableName(schema, SchedulerDBConstants.CUST_DELETE_INFO_TABLENAME) +
			" WHERE " + SchedulerDBConstants.COL_ID + "=?" ;

			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, id);
			
			int result = pstmt.executeUpdate();
			logger.debug("Deleted " + result + " entry from " + SchedulerDBConstants.CUST_DELETE_INFO_TABLENAME + 
					" for id : " + id);
		}
		finally
		{
			try
			{
				if(pstmt != null)
				{
					pstmt.close();
				}
			}catch(Exception ex)
			{
				logger.warn("Error in closing statement", ex);
			}
		}
	}


	public static void deleteAllPreviousSpaceStatus(Connection conn, String schema, String spaceId, String jobId, 
			String keepThisStepId) throws SQLException
	{
		List<String> keepIdsList = null;
		if(keepThisStepId != null && keepThisStepId.trim().length() > 0)
		{
			keepIdsList = new ArrayList<String>();
			keepIdsList.add(keepThisStepId.trim());
		}
		
		deleteAllPreviousSpaceStatus(conn, schema, spaceId, jobId, keepIdsList);
	}
	
	public static void deleteAllPreviousSpaceStatus(Connection conn,
			String schema, String spaceId, String jobId, List<String> keepThisStepIds) throws SQLException
	{
		PreparedStatement pstmt = null;
		try
		{
			String sql = "DELETE FROM " + getQualifiedTableName(schema, SchedulerDBConstants.CUST_SPACE_STATUS_TABLENAME) +
			" WHERE " + SchedulerDBConstants.COL_SPACE_ID + "=? AND " + SchedulerDBConstants.COL_JOB_ID + "=?" ;
			
			List<String> types = new ArrayList<String>();
			StringBuilder logsb = new StringBuilder();
			if(keepThisStepIds != null && keepThisStepIds.size() > 0)
			{
				sql = sql + " AND " + SchedulerDBConstants.COL_ID + " NOT IN (";
				boolean first = true;
				for(String keepThisStepId : keepThisStepIds)
				{		
					if(!first)
					{
						sql = sql + ",";
					}
					
					sql = sql + "?";
					types.add(keepThisStepId);
					first = first ? false : first;
					logsb.append(keepThisStepId + " ");
				}
				sql = sql + " )";
			}
			
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, spaceId);
			pstmt.setString(2, jobId);
			int paramIndex = 3;
			for(String str : types)
			{
				pstmt.setString(paramIndex, str);
				paramIndex++;
			}
						
			int result = pstmt.executeUpdate();
			logger.debug("Deleted " + result + " entries from " + SchedulerDBConstants.CUST_SPACE_STATUS_TABLENAME + 
					" for spaceID : " + spaceId + " : jobId : " + jobId + " : keepThisScheduleIds : " + logsb.toString());
		}
		finally
		{
			try
			{
				if(pstmt != null)
				{
					pstmt.close();
				}
			}
			catch(Exception ex)
			{
				logger.warn("Error in closing statement", ex);
			}
		}	
	}

	public static void deleteAllPreviousRunNowConnectors(Connection conn,
			String schema, String spaceID, String runningJobId) throws SQLException
	{
		
		PreparedStatement pstmt = null;
		try
		{
			boolean isOracle = dConn.isDBTypeOracle();
			conn.setAutoCommit(false);
			String deleteFromSpaceStatusSql = "DELETE " + (isOracle ? "" : SchedulerDBConstants.CUST_SPACE_STATUS_TABLENAME)  + " FROM " +
					    getQualifiedTableName(schema, SchedulerDBConstants.CUST_SPACE_STATUS_TABLENAME)  + " A" +
					    (isOracle ? " WHERE EXISTS (SELECT 1 FROM " : ", ") + getQualifiedTableName(schema, SchedulerDBConstants.CUST_CONNECTORS_INFO_TABLENAME)  + " B " +
						" WHERE A.SPACE_ID = B.SPACE_ID " +
						" AND A.JOB_ID = B.ID " +
						" AND B.RunNowMode=1" +
						" AND A.SPACE_ID=? " +
						" AND A.JOB_ID <> ?" + (isOracle ? ")" : "");
			
			pstmt = conn.prepareStatement(deleteFromSpaceStatusSql);
			pstmt.setString(1, spaceID);
			pstmt.setString(2, runningJobId);
			int result1 = pstmt.executeUpdate();
			
			String deleteFromConnectorsSql = "DELETE FROM " + getQualifiedTableName(schema, SchedulerDBConstants.CUST_CONNECTORS_INFO_TABLENAME)  + " " +
							" WHERE " +  SchedulerDBConstants.COL_SPACE_ID + "=?" +
							" AND " + SchedulerDBConstants.COL_RUN_NOW_MODE + "=1" + 
							" AND " + SchedulerDBConstants.COL_ID + "<>?";
			
			pstmt = conn.prepareStatement(deleteFromConnectorsSql);
			pstmt.setString(1, spaceID);
			pstmt.setString(2, runningJobId);
			int result2 = pstmt.executeUpdate();
			conn.commit();
			logger.debug("Deleted " + result1 + " rows for " + deleteFromSpaceStatusSql + " spaceID=" + spaceID + " : runningJobId=" + runningJobId);
			logger.debug("Deleted " + result2 + " rows for " + deleteFromConnectorsSql + " spaceID=" + spaceID + " : runningJobId=" + runningJobId);
		}
		catch(SQLException ex)
		{
			conn.rollback();
			throw ex;
		}
		finally
		{	
			try
			{
				if(conn != null)
				{
					conn.setAutoCommit(true);
				}
				if(pstmt != null)
				{
					pstmt.close();
				}
			}
			catch(Exception ex)
			{
				logger.warn("Error in closing statement", ex);
			}
		}
	}

	public static void deleteAllScheduledConnectors(Connection conn,
			String schema, String spaceID) throws SQLException
	{
		
		PreparedStatement pstmt = null;
		try
		{
			boolean isOracle = dConn.isDBTypeOracle();
			conn.setAutoCommit(false);
			String deleteFromScheduleInfoSql = "DELETE " + (isOracle ? "" : SCHEDULE_INFO_TABLENAME) + " FROM " +
					    getQualifiedTableName(schema, SCHEDULE_INFO_TABLENAME)  + " A" +
						(isOracle ? " WHERE EXISTS (SELECT 1 FROM " : ", ") + getQualifiedTableName(schema, SchedulerDBConstants.CUST_CONNECTORS_INFO_TABLENAME)  + " B " +
						" WHERE A.JOB_ID = B.ID " +
						" AND B.RunNowMode=0" +
						" AND B.SPACE_ID=? " + (isOracle ? ")" : "");
						
			
			pstmt = conn.prepareStatement(deleteFromScheduleInfoSql);
			pstmt.setString(1, spaceID);
			int result1 = pstmt.executeUpdate();
			
			String deleteFromConnectorsSql = "DELETE FROM " + getQualifiedTableName(schema, SchedulerDBConstants.CUST_CONNECTORS_INFO_TABLENAME)  + " " +
							" WHERE " +  SchedulerDBConstants.COL_SPACE_ID + "=?" +
							" AND " + SchedulerDBConstants.COL_RUN_NOW_MODE + "=0";
			
			pstmt = conn.prepareStatement(deleteFromConnectorsSql);
			pstmt.setString(1, spaceID);
			int result2 = pstmt.executeUpdate();
			conn.commit();
			logger.debug("Deleted " + result1 + " rows for " + deleteFromScheduleInfoSql + " spaceID=" + spaceID);
			logger.debug("Deleted " + result2 + " rows for " + deleteFromConnectorsSql + " spaceID=" + spaceID);
		}
		catch(SQLException ex)
		{
			conn.rollback();
			throw ex;
		}
		finally
		{	
			try
			{
				if(conn != null)
				{
					conn.setAutoCommit(true);
				}
				if(pstmt != null)
				{
					pstmt.close();
				}
			}
			catch(Exception ex)
			{
				logger.warn("Error in closing statement", ex);
			}
		}
	}
	
	public static List<SFDCInfo> getConnectorInfoList(Connection conn,
			String schema, String spaceID, boolean isRunNowMode) throws SQLException
	{
		PreparedStatement pstmt = null;
		List<SFDCInfo> responseList = new ArrayList<SFDCInfo>();
		try
		{
			String sql = "SELECT " + SchedulerDBConstants.COL_ID +  "," + SchedulerDBConstants.COL_USER_ID + "," +
					SchedulerDBConstants.COL_RUN_NOW_MODE + "," + SchedulerDBConstants.COL_CLIENT_ID +  "," + 
					SchedulerDBConstants.COL_SANBOX_URL + "," + SchedulerDBConstants.COL_PROCESS_AFTER + "," +
					SchedulerDBConstants.COL_LOAD_DATE +  "," + SchedulerDBConstants.COL_SUBGROUPS + "," + 
					SchedulerDBConstants.COL_SEND_EMAIL + "," +
					SchedulerDBConstants.COL_CREATED_DATE + "," + SchedulerDBConstants.COL_MODIFIED_DATE + "," +
					SchedulerDBConstants.COL_MODIFIED_BY + 
					SchedulerDBConstants.COL_CONN_SCHED_NAME + "," + SchedulerDBConstants.COL_CONN_TYPE + "," +
					SchedulerDBConstants.COL_CONN_ENGINE + "," + SchedulerDBConstants.COL_EXTRACTGROUPS +
					" FROM " + getQualifiedTableName(schema, SchedulerDBConstants.CUST_CONNECTORS_INFO_TABLENAME) + 
					" WHERE " + SchedulerDBConstants.COL_SPACE_ID + "=?" +
					" AND " + SchedulerDBConstants.COL_RUN_NOW_MODE + "=?"; 
			
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, spaceID);
			pstmt.setBoolean(2, isRunNowMode);
			ResultSet set = pstmt.executeQuery();
			
			while(set.next())
			{	
				String connectorInfoId = (String) set.getObject(1);
				String userID = (String) set.getObject(2);
				boolean runNowMode = set.getBoolean(3);
				String clientId = set.getString(4);				
				String sandBoxUrl = set.getString(5);
				boolean processAfter = set.getBoolean(6);				
				String loadDate = set.getString(7);				
				String subGroups = set.getString(8);
				boolean sendEmail = set.getBoolean(9);
				Timestamp createdDate = set.getTimestamp(10);
				Timestamp modifiedDate = set.getTimestamp(11);
				String modifiedBy = set.getString(12);
				String name = set.getString(13);
				String type = set.getString(14);
				int engine = set.getInt(15);
				engine = engine > 0 ? engine : -1;
				String extractGroups = set.getString(16);
				SFDCInfo result = new SFDCInfo(connectorInfoId, spaceID, userID, runNowMode, clientId, sandBoxUrl, processAfter, loadDate, 
						subGroups, sendEmail, name, type, engine);
				result.setCreatedDate(createdDate);
				result.setModifiedDate(modifiedDate);
				result.setModifiedBy(modifiedBy);
				result.setExtractGroups(extractGroups);

				List<Schedule> scheduleList = SchedulerDatabase.getScheduleList(conn, schema, result.getId());
				Schedule[] jobSchedules = null;
				if(scheduleList != null && scheduleList.size() > 0)
				{
					jobSchedules = scheduleList.toArray(new Schedule[scheduleList.size()]);
					result.setJobSchedules(jobSchedules);
				}	
				responseList.add(result);
			}
		}finally
		{
			try
			{
				if(pstmt != null)
				{
					pstmt.close();
				}
			}catch(Exception ex)
			{
				logger.warn("Error in closing statement", ex);
			}
		}
		
		return responseList;
	}
	
	public static void addBDSInfo(Connection conn, String schema, BDSInfo bdsInfo) throws SQLException
	{
		PreparedStatement pstmt = null;
		try
		{
			String id = bdsInfo.getId();
			String spaceId = bdsInfo.getSpaceId();
			String dataSourceID = bdsInfo.getDataSourceId();
			String userId = bdsInfo.getUserId();
			boolean runNowMode = bdsInfo.isRunNowMode();
			boolean processAfter = bdsInfo.isProcessAfter();
			String loadDate = bdsInfo.getLoadDate();
			String group = bdsInfo.getGroup();
			boolean sendEmail = bdsInfo.getSendEmail();
			String name = bdsInfo.getName();
			String scheduleType = bdsInfo.getType();
			pstmt = conn.prepareStatement("INSERT INTO " + getQualifiedTableName(schema, SchedulerDBConstants.CUST_BDS_INFO_TABLENAME) + 
					" (" + SchedulerDBConstants.COL_ID + "," + SchedulerDBConstants.COL_SPACE_ID + "," + SchedulerDBConstants.COL_DS_ID + "," +					
					SchedulerDBConstants.COL_USER_ID + "," + SchedulerDBConstants.COL_NAME + "," + SchedulerDBConstants.COL_SCHEDULE_TYPE + "," + 
					SchedulerDBConstants.COL_RUN_NOW_MODE + "," + SchedulerDBConstants.COL_PROCESS_AFTER + "," + SchedulerDBConstants.COL_LOAD_DATE + "," + 
					SchedulerDBConstants.COL_LOAD_GROUP + "," + SchedulerDBConstants.COL_SEND_EMAIL + "," + SchedulerDBConstants.COL_CREATED_DATE + ") " + 
					" VALUES (?,?,?,?,?,?,?,?,?,?,?,?)");
			
			pstmt.setString(1, id);
			pstmt.setString(2, spaceId);
			pstmt.setString(3, dataSourceID);
			pstmt.setString(4, userId);
			pstmt.setString(5, name);
			pstmt.setString(6, scheduleType);
			pstmt.setBoolean(7, runNowMode);			
			pstmt.setBoolean(8, processAfter);
			if(loadDate != null && loadDate.trim().length() > 0)
			{
				pstmt.setString(9, loadDate);
			}
			else
			{
				pstmt.setNull(9, Types.VARCHAR);
			}
			
			if(group != null && group.trim().length() > 0)
			{
				pstmt.setString(10, group);
			}
			else
			{
				pstmt.setNull(10, Types.VARCHAR);
			}
			
			pstmt.setBoolean(11, sendEmail);
			Date date = new Date();				
			Timestamp startTime = new Timestamp(date.getTime());
			pstmt.setTimestamp(12, startTime);
			int result = pstmt.executeUpdate();
			logger.debug("Added " + result + " entry into table " + SchedulerDBConstants.CUST_BDS_INFO_TABLENAME + " : " + bdsInfo.toString());
 		}
		finally
		{
			try
			{
				if(pstmt != null)
				{
					pstmt.close();
				}
			}catch(Exception ex)
			{
				logger.warn("Error in closing statement", ex);
			}
		}
	}
	
	public static void deleteBDSInfo(Connection conn, String schema, String id) throws SQLException
	{
		PreparedStatement pstmt = null;
		try
		{
			String sql = "DELETE FROM " + getQualifiedTableName(schema, SchedulerDBConstants.CUST_BDS_INFO_TABLENAME) +
			" WHERE " + SchedulerDBConstants.COL_ID + "=?";

			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, id);
			
			int result = pstmt.executeUpdate();
			logger.debug("Deleted " + result + " entry from " + SchedulerDBConstants.CUST_BDS_INFO_TABLENAME + 
					" for id : " + id);
		}
		finally
		{
			try
			{
				if(pstmt != null)
				{
					pstmt.close();
				}
			}catch(Exception ex)
			{
				logger.warn("Error in closing statement", ex);
			}
		}
	}
	
	public static void updateBDSInfo(Connection conn, String schema, BDSInfo bdsInfo) throws SQLException
	{
		String id = bdsInfo.getId();
		String spaceID = bdsInfo.getSpaceId();
		String dataSourceID = bdsInfo.getDataSourceId();
		String userID = bdsInfo.getUserId();
		boolean runNowMode = bdsInfo.isRunNowMode();
		boolean process = bdsInfo.isProcessAfter();
		String loadDate = bdsInfo.getLoadDate();
		boolean sendEmail = bdsInfo.getSendEmail();
		String group = bdsInfo.getGroup();
		String modifiedBy = bdsInfo.getModifiedBy();
		String name = bdsInfo.getName();
		String scheduleType = bdsInfo.getType();
		
		PreparedStatement pstmt = null;
		try
		{
			pstmt = conn.prepareStatement("UPDATE " + getQualifiedTableName(schema, SchedulerDBConstants.CUST_BDS_INFO_TABLENAME) +
					" SET " + SchedulerDBConstants.COL_SPACE_ID + "= ?," + SchedulerDBConstants.COL_DS_ID + "= ?," + SchedulerDBConstants.COL_USER_ID + "= ?," + 
					SchedulerDBConstants.COL_NAME + "= ?," + SchedulerDBConstants.COL_SCHEDULE_TYPE + "= ?," + SchedulerDBConstants.COL_RUN_NOW_MODE + "= ?," + 
					SchedulerDBConstants.COL_PROCESS_AFTER + "= ?," + SchedulerDBConstants.COL_LOAD_DATE + "= ?," +  SchedulerDBConstants.COL_LOAD_GROUP + "= ?," + 
					SchedulerDBConstants.COL_SEND_EMAIL + "= ?," + SchedulerDBConstants.COL_MODIFIED_DATE + "= ?," + SchedulerDBConstants.COL_MODIFIED_BY + "= ?" + 
					" WHERE ID = ?");
			
			// truncate the strings to make sure that they are no bigger than the fields			
			pstmt.setString(1, spaceID);
			pstmt.setString(2, dataSourceID);
			pstmt.setString(3, userID);
			pstmt.setString(4, name);
			pstmt.setString(5, scheduleType);
			pstmt.setBoolean(6, runNowMode);
			pstmt.setBoolean(7, process);
			if(loadDate != null && loadDate.trim().length() > 0)
			{
				pstmt.setString(8, loadDate);
			}
			else
			{
				pstmt.setNull(8, Types.VARCHAR);
			}
			pstmt.setString(9, group);
			pstmt.setBoolean(10, sendEmail);
			
			Date date = new Date();
			Timestamp modifiedDate = new Timestamp(date.getTime());
			pstmt.setTimestamp(11, modifiedDate);
			
			if(modifiedBy != null && modifiedBy.length() > 0)
			{
				pstmt.setString(12, modifiedBy);
			}
			else
			{
				pstmt.setNull(12, Types.VARCHAR);
			}
			pstmt.setString(13, id);			
			pstmt.execute();	
			logger.debug("Updated entry in " + SchedulerDBConstants.CUST_BDS_INFO_TABLENAME + " : " + bdsInfo.toString());
		}
		catch (SQLException e)
		{
			logger.error("Could not add BDS info in the database.");
			throw e;
		}
		finally 
		{
			try
			{
				if (pstmt != null)
					pstmt.close();
			}
			catch (SQLException e)
			{
				logger.warn("Error in pstmt.close()", e );
			}
		}		
	}
	
	public static BDSInfo getBDSInfo(Connection conn, String schema, String bdsInfoId) 
	throws SQLException 
	{
		PreparedStatement pstmt = null;
		BDSInfo result = null;
		ResultSet rs = null;
		try
		{
			String sql = "SELECT A." + SchedulerDBConstants.COL_SPACE_ID +  ",A." + SchedulerDBConstants.COL_DS_ID + ",A." +
			SchedulerDBConstants.COL_USER_ID + ",A." + SchedulerDBConstants.COL_NAME + ",A." + 
			SchedulerDBConstants.COL_SCHEDULE_TYPE + ",A." + SchedulerDBConstants.COL_RUN_NOW_MODE + ",A." + 
			SchedulerDBConstants.COL_PROCESS_AFTER + ",A." + SchedulerDBConstants.COL_LOAD_DATE +  ",A." + 
			SchedulerDBConstants.COL_LOAD_GROUP + ",A." + SchedulerDBConstants.COL_SEND_EMAIL + ",A." +
			SchedulerDBConstants.COL_CREATED_DATE + ",A." + SchedulerDBConstants.COL_MODIFIED_DATE + ",A." +
			SchedulerDBConstants.COL_MODIFIED_BY + 
			" FROM " + getQualifiedTableName(schema, SchedulerDBConstants.CUST_BDS_INFO_TABLENAME) + " as A " +
			" WHERE A." + SchedulerDBConstants.COL_ID + "=?"; 

			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, bdsInfoId);
			rs = pstmt.executeQuery();
			int cnt = 0;
			while(rs.next())
			{	
				cnt++;
				String spaceID = (String) rs.getObject(1);
				String dataSourceID = (String) rs.getObject(2);
				String userID = (String) rs.getObject(3);
				String name = rs.getString(4);
				String scheduleType = rs.getString(5);
				boolean runNowMode = rs.getBoolean(6);
				boolean processAfter = rs.getBoolean(7);				
				String loadDate = rs.getString(8);				
				String group = rs.getString(9);
				boolean sendEmail = rs.getBoolean(10);
				Timestamp createdDate = rs.getTimestamp(11);
				Timestamp modifiedDate = rs.getTimestamp(12);
				String modifiedBy = rs.getString(13);
				result = new BDSInfo(bdsInfoId, spaceID, dataSourceID, userID, name, scheduleType, runNowMode, processAfter, loadDate, group, sendEmail, createdDate, modifiedDate, modifiedBy);
				break;
			}

			if(result != null)
			{
				List<Schedule> scheduleList = SchedulerDatabase.getScheduleList(conn, schema, result.getId());
				Schedule[] jobSchedules = null;
				if(scheduleList != null && scheduleList.size() > 0)
				{
					jobSchedules = scheduleList.toArray(new Schedule[scheduleList.size()]);
					result.setJobSchedules(jobSchedules);
				}
			}
		} finally
		{
			try
			{
				if (rs != null)
				{
					rs.close();
				}
				if(pstmt != null)
				{
					pstmt.close();
				}
			} catch(Exception ex)
			{
				logger.warn("Error in closing statement", ex);
			}
		}
		return result;
	}
	
	public static void deleteAllBDSPreviousRunNowJobs(Connection conn,
			String schema, String spaceID, String runningJobId) throws SQLException
	{
		PreparedStatement pstmt = null;
		try
		{
			boolean isOracle = dConn.isDBTypeOracle();
			conn.setAutoCommit(false);
			String deleteFromSpaceStatusSql = "DELETE " + (isOracle ? "" : SchedulerDBConstants.CUST_SPACE_STATUS_TABLENAME)  + " FROM " +
					    getQualifiedTableName(schema, SchedulerDBConstants.CUST_SPACE_STATUS_TABLENAME)  + " A" +
					    (isOracle ? " WHERE EXISTS (SELECT 1 FROM " : ", ") + getQualifiedTableName(schema, SchedulerDBConstants.CUST_BDS_INFO_TABLENAME)  + " B " +
						" WHERE A.SPACE_ID = B.SPACE_ID " +
						" AND A.JOB_ID = B.ID " +
						" AND B.RunNowMode=1" +
						" AND A.SPACE_ID=? " +
						" AND A.JOB_ID <> ?" + (isOracle ? ")" : "");
			
			pstmt = conn.prepareStatement(deleteFromSpaceStatusSql);
			pstmt.setString(1, spaceID);
			pstmt.setString(2, runningJobId);
			int result1 = pstmt.executeUpdate();
			
			String deleteFromConnectorsSql = "DELETE FROM " + getQualifiedTableName(schema, SchedulerDBConstants.CUST_BDS_INFO_TABLENAME)  + " " +
							" WHERE " +  SchedulerDBConstants.COL_SPACE_ID + "=?" +
							" AND " + SchedulerDBConstants.COL_RUN_NOW_MODE + "=1" + 
							" AND " + SchedulerDBConstants.COL_ID + "<>?";
			
			pstmt = conn.prepareStatement(deleteFromConnectorsSql);
			pstmt.setString(1, spaceID);
			pstmt.setString(2, runningJobId);
			int result2 = pstmt.executeUpdate();
			conn.commit();
			logger.debug("Deleted " + result1 + " rows for " + deleteFromSpaceStatusSql + " spaceID=" + spaceID + " : runningJobId=" + runningJobId);
			logger.debug("Deleted " + result2 + " rows for " + deleteFromConnectorsSql + " spaceID=" + spaceID + " : runningJobId=" + runningJobId);
		}
		catch(SQLException ex)
		{
			conn.rollback();
			throw ex;
		}
		finally
		{	
			try
			{
				if(conn != null)
				{
					conn.setAutoCommit(true);
				}
				if(pstmt != null)
				{
					pstmt.close();
				}
			}
			catch(Exception ex)
			{
				logger.warn("Error in closing statement", ex);
			}
		}
	}
	
	public static void deleteAllBDSScheduledJobs(Connection conn,
			String schema, String spaceID) throws SQLException
	{
		PreparedStatement pstmt = null;
		try
		{
			boolean isOracle = dConn.isDBTypeOracle();
			conn.setAutoCommit(false);
			String deleteFromScheduleInfoSql = "DELETE " + (isOracle ? "" : SCHEDULE_INFO_TABLENAME) + " FROM " +
					    getQualifiedTableName(schema, SCHEDULE_INFO_TABLENAME)  + " A" +
						(isOracle ? " WHERE EXISTS (SELECT 1 FROM " : ", ") + getQualifiedTableName(schema, SchedulerDBConstants.CUST_BDS_INFO_TABLENAME)  + " B " +
						" WHERE A.JOB_ID = B.ID " +
						" AND B.RunNowMode=0" +
						" AND B.SPACE_ID=? " + (isOracle ? ")" : "");
						
			
			pstmt = conn.prepareStatement(deleteFromScheduleInfoSql);
			pstmt.setString(1, spaceID);
			int result1 = pstmt.executeUpdate();
			
			String deleteFromConnectorsSql = "DELETE FROM " + getQualifiedTableName(schema, SchedulerDBConstants.CUST_BDS_INFO_TABLENAME)  + " " +
							" WHERE " +  SchedulerDBConstants.COL_SPACE_ID + "=?" +
							" AND " + SchedulerDBConstants.COL_RUN_NOW_MODE + "=0";
			
			pstmt = conn.prepareStatement(deleteFromConnectorsSql);
			pstmt.setString(1, spaceID);
			int result2 = pstmt.executeUpdate();
			conn.commit();
			logger.debug("Deleted " + result1 + " rows for " + deleteFromScheduleInfoSql + " spaceID=" + spaceID);
			logger.debug("Deleted " + result2 + " rows for " + deleteFromConnectorsSql + " spaceID=" + spaceID);
		}
		catch(SQLException ex)
		{
			conn.rollback();
			throw ex;
		}
		finally
		{	
			try
			{
				if(conn != null)
				{
					conn.setAutoCommit(true);
				}
				if(pstmt != null)
				{
					pstmt.close();
				}
			}
			catch(Exception ex)
			{
				logger.warn("Error in closing statement", ex);
			}
		}
	}
	
	public static List<BDSInfo> getBDSInfoList(Connection conn,
			String schema, String spaceID, boolean isRunNowMode) throws SQLException
	{
		PreparedStatement pstmt = null;
		List<BDSInfo> responseList = new ArrayList<BDSInfo>();
		try
		{
			String sql = "SELECT A." + SchedulerDBConstants.COL_ID +  ",A." + SchedulerDBConstants.COL_DS_ID + ",A." + 
					SchedulerDBConstants.COL_USER_ID + ",A." + ",A." + SchedulerDBConstants.COL_NAME + ",A." + 
					SchedulerDBConstants.COL_SCHEDULE_TYPE + SchedulerDBConstants.COL_RUN_NOW_MODE + ",A." +
					SchedulerDBConstants.COL_PROCESS_AFTER + ",A." + SchedulerDBConstants.COL_LOAD_DATE +  ",A." + 
					SchedulerDBConstants.COL_LOAD_GROUP + ",A." + SchedulerDBConstants.COL_SEND_EMAIL + ",A." +
					SchedulerDBConstants.COL_CREATED_DATE + ",A." + SchedulerDBConstants.COL_MODIFIED_DATE + ",A." +
					SchedulerDBConstants.COL_MODIFIED_BY + 
					" FROM " + getQualifiedTableName(schema, SchedulerDBConstants.CUST_BDS_INFO_TABLENAME) + " as A " +
					" WHERE A." + SchedulerDBConstants.COL_SPACE_ID + "=?" +
					" AND A." + SchedulerDBConstants.COL_RUN_NOW_MODE + "=?"; 
			
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, spaceID);
			pstmt.setBoolean(2, isRunNowMode);
			ResultSet set = pstmt.executeQuery();
			
			while(set.next())
			{	
				String connectorInfoId = (String) set.getObject(1);
				String dataSourceID = (String) set.getObject(2);
				String userID = (String) set.getObject(3);
				String name = set.getString(4);
				String scheduleType = set.getString(5);
				boolean runNowMode = set.getBoolean(6);
				boolean processAfter = set.getBoolean(8);				
				String loadDate = set.getString(10);				
				String group = set.getString(11);
				boolean sendEmail = set.getBoolean(12);
				Timestamp createdDate = set.getTimestamp(13);
				Timestamp modifiedDate = set.getTimestamp(14);
				String modifiedBy = set.getString(15);
				BDSInfo result = new BDSInfo(connectorInfoId, spaceID, dataSourceID, userID, name, scheduleType, runNowMode, processAfter, loadDate, group, sendEmail, createdDate, modifiedDate, modifiedBy);
				List<Schedule> scheduleList = SchedulerDatabase.getScheduleList(conn, schema, result.getId());
				Schedule[] jobSchedules = null;
				if(scheduleList != null && scheduleList.size() > 0)
				{
					jobSchedules = scheduleList.toArray(new Schedule[scheduleList.size()]);
					result.setJobSchedules(jobSchedules);
				}	
				responseList.add(result);
			}
		}finally
		{
			try
			{
				if(pstmt != null)
				{
					pstmt.close();
				}
			}catch(Exception ex)
			{
				logger.warn("Error in closing statement", ex);
			}
		}
		
		return responseList;
	}
	
	public static boolean isValidScheduleName(Connection conn, String schema, String spaceID, String jobID, String name) {
		boolean isValid = false;
		PreparedStatement pstmt = null;
		ResultSet set = null;
		try {
			String sql = "select count(" + SchedulerDBConstants.COL_CONN_SCHED_NAME + ") " +
					"from " + getQualifiedTableName(schema, SchedulerDBConstants.CUST_CONNECTORS_INFO_TABLENAME) + " " +
					"where " + SchedulerDBConstants.COL_SPACE_ID + "=? " + 
						"and " + SchedulerDBConstants.COL_CONN_SCHED_NAME + "=? ";
			
			if (jobID != null && !(jobID.equals(""))) {
				sql = sql + "and " + SchedulerDBConstants.COL_ID + "<>? ";
			}

			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, spaceID);
			pstmt.setString(2, name);
			if (jobID != null && !(jobID.equals(""))) {
				pstmt.setString(3, jobID);
			}
			
			set = pstmt.executeQuery();
			while(set.next()) {	
				int nameCnt = set.getInt(1);
				if (nameCnt < 1) {
					isValid = true;
				}
				break;
			}
		} catch (SQLException e) {
			logger.error("Error retrieving schedule count: ", e);
		}
		finally {
			try {
				if(pstmt != null) {
					pstmt.close();
				}
				if(set != null) {
					set.close();
				}
			} catch(Exception ex) {
				logger.warn("Error in closing result set or statement: ", ex);
			}
		}
		return isValid;
	}
	
	public static int getDefaultNameMaxNumber(Connection conn, String schema, String spaceID, String name) {
		int maxNum = 0;
		String maxName = null;
		PreparedStatement pstmt = null;
		ResultSet set = null;
		try {
			// Retrieve max name.
			String sql = "select max(" + SchedulerDBConstants.COL_CONN_SCHED_NAME + ") " +
					"from " + getQualifiedTableName(schema, SchedulerDBConstants.CUST_CONNECTORS_INFO_TABLENAME) + " " +
					"where " + SchedulerDBConstants.COL_SPACE_ID + " =? " + 
						"and " + SchedulerDBConstants.COL_CONN_SCHED_NAME + " like ?";
			
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, spaceID);
			pstmt.setString(2, "%" + name + "%");
			set = pstmt.executeQuery();
			while(set.next()) {	
				maxName = set.getString(1);
				break;
			}
			if (maxName != null) {
				maxName = maxName.trim();
			
				// Parse to get max number.
				if (Character.isDigit(maxName.charAt(maxName.length() - 1))) {
					maxNum = Integer.parseInt(String.valueOf(maxName.charAt(maxName.length() - 1)));
				}
			}
		} catch (Exception e) {
			logger.error("Error retrieving schedule max number: ", e);
		}
		finally {
			try {
				if(pstmt != null) {
					pstmt.close();
				}
				if(set != null) {
					set.close();
				}
			} catch(Exception ex) {
				logger.warn("Error in closing result set or statement: ", ex);
			}
		}
		return maxNum;
	}
	
	public static Map<String, String> getAllSchedules(Connection conn, String schema, String spaceID) {
		PreparedStatement pstmt = null;
		ResultSet set = null;
		Map<String, String> cronExpressions = new HashMap<String, String>();
		try {
			// Retrieve all cron expression for a given space.
			String sql = "select a.ID, CronExpression " +
							"from CUST_CONNECTORS_INFO as a " +
								"inner join CUST_SCHEDULE_INFO as b " +
									"on a.ID = b.JOB_ID " +
							"where SPACE_ID = ? ";
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, spaceID);
			set = pstmt.executeQuery();
			while(set.next()) {
				cronExpressions.put(set.getString(1), set.getString(2));
			}
		} catch (Exception e) {
			logger.error("Error retrieving schedule max number: ", e);
		}
		finally {
			try {
				if(pstmt != null) {
					pstmt.close();
				}
				if(set != null) {
					set.close();
				}
			} catch(Exception ex) {
				logger.warn("Error in closing result set or statement: ", ex);
			}
		}
		return cronExpressions;
	}
}
