package com.birst.scheduler.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.birst.scheduler.SchedulerServerContext;
import com.birst.scheduler.exceptions.InvalidCommandException;

public class CommandUtil
{
	public static final int COMMAND_INVALID = -1000;
	public static final int COMMAND_SUCCESS = 0;		
	
	public static final long TIMEOUT_PROC_OUTPUT_MILLIS = 1*60*60*1000; // 1 hour
	
	private static final Logger logger = Logger.getLogger(CommandUtil.class);
	
	public static final String KEY_COMMAND_UID = "uid";
	// Props to check after app builder is called and before proceeding to the processing stage
	public static final String KEY_LOAD_NUMBER = "loadNumber";
	public static final String KEY_LOAD_GROUP = "loadGroup";
	public static final String KEY_ENGINE_COMMAND = "engineCommand";
	public static final String KEY_ENGINE_ARGS = "args";
	public static final String KEY_BIRST_LOCAL_STARTED = "birstLocalLoad";
	public static final String KEY_LOADER_BIRST_LOCAL_GROUPS = "birstLocalGroups";
	public static final String KEY_ENGINE_SKIP_CHECK_IN = "skipCheckIn";
	
	public static final String CMD_APP_LOADER = "appLoader";
	public static final String CMD_LOAD = "load";
	public static final String CMD_POST_LOAD = "postProcess";
	public static final String CMD_LOAD_EMAIL = "sendProcessEmail";
	public static final String CMD_GET_SFDC_ENGINE = "getSFDCEngineCommand";
	public static final String CMD_GET_BDS_ENGINE = "getBDSEngineCommand";
	public static final String CMD_UPDATE_ADMIN_DB = "updateAdminDB";
	public static final String CMD_POST_SFDC_EXTRACT = "postSFDCExtract";
	public static final String CMD_POST_BDS_EXTRACT = "postBDSExtract";
	public static final String CMD_DELETE_LOAD = "deleteLoad";
	public static final String CMD_POST_DELETE_LOAD = "postDeleteLoad"; // only used if it is delete last
	public static final String CMD_EXTRACT = "extract";
	public static final String CMD_EXTRACTION_EMAIL = "sendExtractionEmail";
	public static final String CMD_EXTRACTION_ONLY = "extractionOnly";
	public static final String CMD_TRANSACTIONAL_OBJECT = "getTransactionObject";
	public static final String CMD_BIRST_PARAMETERS = "getBirstParameters";
	public static final String CMD_CONNECTOR_VARIABLES = "getConnectorVariables";
	
	// birst load failure types
	public static final int EXIT_CODE_CAN_PUBLISH        		= -103;
    public static final int EXIT_CODE_SPACE_BEING_PUBLISHED	= -104;        
    public static final int EXIT_CODE_SPACE_BUSY             	= -105;
    public static final int IS_SPACE_DATA_LIMIT					= -106;
    public static final int IS_SPACE_ROW_LIMIT					= -107;
    
	public static final String FAILURE_TYPE_CANNOT_PUBLISH = "cannotPublish";
	public static final String FAILURE_TYPE_IS_SPACE_PUBLISHED = "isBeingPublished";
	public static final String FAILURE_TYPE_SPACE_BUSY = "isSpaceBusyInOtherOp";
	public static final String FAILURE_TYPE_DATA_LIMIT = "dataLimitExceeded";
	public static final String FAILURE_TYPE_ROW_LIMIT = "rowLimitExceeded";
	
	private static final Map<Integer, String> mapOfExitCodes = new HashMap<Integer, String>(); 
	
	public static String getAppLoaderCommand(String logFile, String uid, String spaceID, String userID, 
			int loadID, String loadGroup, String loadDate, String subGroups, boolean retryFailed) throws InvalidCommandException
	{
		if(spaceID == null || userID == null || loadGroup == null || loadDate == null)
		{
			throw new InvalidCommandException("AppLoader Command Missing Parameters " +
					" : spaceID= " + spaceID +
					" : userID=" +  userID + 
					" : loadGroup=" + loadGroup + 
					" : loadDate=" + loadDate);
		}
		CommandBuilder cmdBuilder = CommandBuilder.getCommandBuilder();
		cmdBuilder.add(getAcornExePath());
		cmdBuilder.addUid(uid);
		cmdBuilder.addLogFile(logFile);
		cmdBuilder.addAcornWebConfigDirectory(SchedulerServerContext.getAcornWebConfigDirectory());
		cmdBuilder.addCommandAction(CMD_APP_LOADER);
		cmdBuilder.addSpaceID(spaceID);
		cmdBuilder.addUserID(userID);
		cmdBuilder.addLoadGroup(loadGroup);
		cmdBuilder.addLoadDate(loadDate);
		cmdBuilder.addLoadNumber(loadID);
		if(subGroups != null && subGroups.trim().length() > 0)
		{
			cmdBuilder.addSubGroups(subGroups);
		}
		cmdBuilder.addRetryFailed(retryFailed);
		
		return cmdBuilder.getCommand();
	}
	
	public static String getSFDCEngineCommand(String logFile, String uid, String spaceID, 
			String userID, String clientId, String sandboxUrl, boolean writeEmailInfo, 
			String connectorType, int extractionEngine) throws InvalidCommandException
	{
		if(spaceID == null || userID == null )
		{
			throw new InvalidCommandException("AppLoader Command Missing Parameters " +
					" : spaceID= " + spaceID +
					" : userID=" +  userID) ;
		}
		
		CommandBuilder cmdBuilder = CommandBuilder.getCommandBuilder();
		cmdBuilder.add(getAcornExePath());
		cmdBuilder.addUid(uid);
		cmdBuilder.addLogFile(logFile);
		cmdBuilder.addAcornWebConfigDirectory(SchedulerServerContext.getAcornWebConfigDirectory());
		cmdBuilder.addCommandAction(CMD_GET_SFDC_ENGINE);
		cmdBuilder.addSpaceID(spaceID);
		cmdBuilder.addUserID(userID);
		if(clientId != null && clientId.length() > 0)
		{
			cmdBuilder.addClientId(clientId);
		}
		if(sandboxUrl != null && sandboxUrl.length() > 0)
		{
			cmdBuilder.addSandBoxUrl(sandboxUrl);
		}
		cmdBuilder.addEmailerInfo(writeEmailInfo);
		cmdBuilder.addNumberSFDCThreads(SchedulerServerContext.getSFDCNumberThreads());
		cmdBuilder.addConnectorType(connectorType);
		if(extractionEngine > 0)
		{
			cmdBuilder.addConnectorExtractionEngineUrl(extractionEngine);
		}
		return cmdBuilder.getCommand();
	}
	
	public static String getTransactionObject(String logFile, String uid, String spaceID, String connectorType) throws InvalidCommandException
	{
		if(spaceID == null || connectorType == null)
		{
			throw new InvalidCommandException("AppLoader Command Missing Parameters " +
					" : spaceID= " + spaceID +
					" : connectorType=" +  connectorType) ;
		}
		
		CommandBuilder cmdBuilder = CommandBuilder.getCommandBuilder();
		cmdBuilder.add(getAcornExePath());
		cmdBuilder.addUid(uid);
		cmdBuilder.addLogFile(logFile);
		cmdBuilder.addAcornWebConfigDirectory(SchedulerServerContext.getAcornWebConfigDirectory());
		cmdBuilder.addCommandAction(CMD_TRANSACTIONAL_OBJECT);
		cmdBuilder.addSpaceID(spaceID);
		cmdBuilder.addConnectorType(connectorType);
		return cmdBuilder.getCommand();
	}
	
	public static String getBirstParameters(String logFile, String uid, String spaceID, String connectorType) throws InvalidCommandException
	{
		if(spaceID == null || connectorType == null)
		{
			throw new InvalidCommandException("AppLoader Command Missing Parameters " +
					" : spaceID= " + spaceID +
					" : connectorType=" +  connectorType) ;
		}
		
		CommandBuilder cmdBuilder = CommandBuilder.getCommandBuilder();
		cmdBuilder.add(getAcornExePath());
		cmdBuilder.addUid(uid);
		cmdBuilder.addLogFile(logFile);
		cmdBuilder.addAcornWebConfigDirectory(SchedulerServerContext.getAcornWebConfigDirectory());
		cmdBuilder.addCommandAction(CMD_BIRST_PARAMETERS);
		cmdBuilder.addSpaceID(spaceID);
		cmdBuilder.addConnectorType(connectorType);
		return cmdBuilder.getCommand();
	}
	
	public static String getConnectorVariables(String logFile, String uid, String spaceID, String connectorType, String connectionString, String extractGroups) throws InvalidCommandException
	{
		if(spaceID == null || connectorType == null)
		{
			throw new InvalidCommandException("AppLoader Command Missing Parameters " +
					" : spaceID= " + spaceID +
					" : connectorType=" +  connectorType) ;
		}
		
		CommandBuilder cmdBuilder = CommandBuilder.getCommandBuilder();
		cmdBuilder.add(getAcornExePath());
		cmdBuilder.addUid(uid);
		cmdBuilder.addLogFile(logFile);
		cmdBuilder.addAcornWebConfigDirectory(SchedulerServerContext.getAcornWebConfigDirectory());
		cmdBuilder.addCommandAction(CMD_CONNECTOR_VARIABLES);
		cmdBuilder.addSpaceID(spaceID);
		cmdBuilder.addConnectorType(connectorType);
		cmdBuilder.addConnectorConnectionString(connectionString);
		cmdBuilder.addExtractGroups(extractGroups);
		return cmdBuilder.getCommand();
	}
	
	public static String getPostSFDCExtractCommand(String logFile, String uid, String spaceID, 
			String userID, String fileListPath, String connectorType, String extractGroups) throws InvalidCommandException
	{
		if(spaceID == null || userID == null || connectorType == null)
		{
			throw new InvalidCommandException("PostSFDCExtract Command Missing Parameters " +
					" : spaceID= " + spaceID +
					" : userID=" +  userID) ;
		}
		
		CommandBuilder cmdBuilder = CommandBuilder.getCommandBuilder();
		cmdBuilder.add(getAcornExePath());
		cmdBuilder.addUid(uid);
		cmdBuilder.addLogFile(logFile);
		cmdBuilder.addAcornWebConfigDirectory(SchedulerServerContext.getAcornWebConfigDirectory());
		cmdBuilder.addCommandAction(CMD_POST_SFDC_EXTRACT);
		cmdBuilder.addSpaceID(spaceID);
		cmdBuilder.addUserID(userID);
		cmdBuilder.addConnectorType(connectorType);
		cmdBuilder.addExtractGroups(extractGroups);
		if(fileListPath != null && fileListPath.length() > 0)
		{
			cmdBuilder.addFileListPath(fileListPath);
		}
		return cmdBuilder.getCommand();
	}
	
	public static String getPostBDSExtractCommand(String logFile, String uid, String spaceID, String userID, String fileListPath) 
		throws InvalidCommandException {
		if (spaceID == null || userID == null ) {
			throw new InvalidCommandException("PostSFDCExtract Command Missing Parameters " +
					" : spaceID= " + spaceID +
					" : userID=" +  userID) ;
		}
		CommandBuilder cmdBuilder = CommandBuilder.getCommandBuilder();
		cmdBuilder.add(getAcornExePath());
		cmdBuilder.addUid(uid);
		cmdBuilder.addLogFile(logFile);
		cmdBuilder.addAcornWebConfigDirectory(SchedulerServerContext.getAcornWebConfigDirectory());
		cmdBuilder.addCommandAction(CMD_POST_BDS_EXTRACT);
		cmdBuilder.addSpaceID(spaceID);
		cmdBuilder.addUserID(userID);
		if(fileListPath != null && fileListPath.length() > 0) {
			cmdBuilder.addFileListPath(fileListPath);
		}
		return cmdBuilder.getCommand();
	}
	
	public static String getDeleteCommandForAcorn(String logFile, String uid, String spaceID, String userID, 
			String loadGroup, int loadNumber, boolean deleteAll) throws InvalidCommandException
	{
		if(spaceID == null || userID == null || loadGroup == null)
		{
			throw new InvalidCommandException("DeleteAll Command Missing Parameters " +
					" : spaceID= " + spaceID +
					" : userID= " + userID +
					" : loadGroup=" +  loadGroup) ;
		}
		
		CommandBuilder cmdBuilder = CommandBuilder.getCommandBuilder();
		cmdBuilder.add(getAcornExePath());
		cmdBuilder.addUid(uid);
		cmdBuilder.addLogFile(logFile);
		cmdBuilder.addAcornWebConfigDirectory(SchedulerServerContext.getAcornWebConfigDirectory());
		cmdBuilder.addCommandAction(CMD_DELETE_LOAD);
		cmdBuilder.addSpaceID(spaceID);
		cmdBuilder.addUserID(userID);
		cmdBuilder.addLoadGroup(loadGroup);
		cmdBuilder.addDeleteAll(deleteAll);
		cmdBuilder.addLoadNumber(loadNumber);
		return cmdBuilder.getCommand();
	}
	
	public static String getPostDeleteLastCommand(String logFile, String uid, String spaceID, String userID, 
			String loadGroup, int loadNumber, boolean restoreRepository) throws InvalidCommandException
	{
		if(spaceID == null || userID == null || loadGroup == null)
		{
			throw new InvalidCommandException("DeleteAll Command Missing Parameters " +
					" : spaceID= " + spaceID +
					" : userID= " + userID +
					" : loadGroup=" +  loadGroup) ;
		}
		
		CommandBuilder cmdBuilder = CommandBuilder.getCommandBuilder();
		cmdBuilder.add(getAcornExePath());
		cmdBuilder.addUid(uid);
		cmdBuilder.addLogFile(logFile);
		cmdBuilder.addAcornWebConfigDirectory(SchedulerServerContext.getAcornWebConfigDirectory());
		cmdBuilder.addCommandAction(CMD_POST_DELETE_LOAD);
		cmdBuilder.addSpaceID(spaceID);
		cmdBuilder.addUserID(userID);
		cmdBuilder.addLoadGroup(loadGroup);
		cmdBuilder.addLoadNumber(loadNumber);
		cmdBuilder.addRestoreRepository(restoreRepository);
		return cmdBuilder.getCommand();
	}
	
	public static String getPostLoadCommand(String logFile, String uid, 
			String spaceID, String userID, String loadGroup, String loadDate, int loadNumber) throws InvalidCommandException
	{
		if(spaceID == null || userID == null || loadGroup == null || loadDate == null)
		{
			throw new InvalidCommandException("PostProcess Command Missing Parameters : " +
					"spaceID= " + spaceID +
					"userID=" +  userID + 
					"loadGroup=" + loadGroup + 
					"loadDate=" + loadDate);
		}
		CommandBuilder cmdBuilder = CommandBuilder.getCommandBuilder();
		cmdBuilder.add(getAcornExePath());
		cmdBuilder.addUid(uid);
		cmdBuilder.addLogFile(logFile);
		//cmdBuilder.addDatabaseInfo("sfogsingh-dell", "BirstAdmin", "smi", "smi");
		cmdBuilder.addAcornWebConfigDirectory(SchedulerServerContext.getAcornWebConfigDirectory());
		cmdBuilder.addCommandAction(CommandUtil.CMD_POST_LOAD);
		cmdBuilder.addSpaceID(spaceID);
		cmdBuilder.addUserID(userID);
		cmdBuilder.addLoadGroup(loadGroup);
		cmdBuilder.addLoadDate(loadDate);
		cmdBuilder.addLoadNumber(loadNumber);
		
		return cmdBuilder.getCommand();
	}
	
	public static String getEmailForFailedLoadCommand(String logFile, String uid, 
			String spaceID, String userID, boolean useSSL, String failureType, String failureDetail, String parentSource) throws InvalidCommandException
	{
		if(spaceID == null || userID == null || failureType == null)
		{
			throw new InvalidCommandException("Send Email Process Failure Command Missing Parameters : " +
					"spaceID= " + spaceID +
					"userID=" +  userID + 
					"failureType=" + failureType);
		}
		CommandBuilder cmdBuilder = CommandBuilder.getCommandBuilder();
		cmdBuilder.add(getAcornExePath());
		cmdBuilder.addUid(uid);
		cmdBuilder.addLogFile(logFile);
		cmdBuilder.addAcornWebConfigDirectory(SchedulerServerContext.getAcornWebConfigDirectory());
		cmdBuilder.addCommandAction(CommandUtil.CMD_LOAD_EMAIL);
		cmdBuilder.addSpaceID(spaceID);
		cmdBuilder.addUserID(userID);
		cmdBuilder.addEmailType("failure");
		cmdBuilder.addFailureType(failureType);
		cmdBuilder.addFailureDetail(failureDetail);
		cmdBuilder.addUseSSL(useSSL);
		cmdBuilder.addParentSource(parentSource);
		return cmdBuilder.getCommand();
	}
	
	public static String getEmailForSuccessLoadCommand(String logFile, String uid, 
			String spaceID, String userID, boolean useSSL, int loadID, String loadGroup, 
			List<String> liveAccessLoadGroups, String parentSource) throws InvalidCommandException
	{
		if(spaceID == null || userID == null || loadID < 0 || loadGroup == null)
		{
			throw new InvalidCommandException("Send Email Process Success Command Missing Parameters : " +
					"spaceID= " + spaceID +
					"userID=" +  userID + 
					"loadId=" +  userID + 
					"loadGroup=" +  userID + 
					"liveAccessGroups=" + liveAccessLoadGroups != null ? liveAccessLoadGroups.toString() : "");
		}
		CommandBuilder cmdBuilder = CommandBuilder.getCommandBuilder();
		cmdBuilder.add(getAcornExePath());
		cmdBuilder.addUid(uid);
		cmdBuilder.addLogFile(logFile);
		cmdBuilder.addAcornWebConfigDirectory(SchedulerServerContext.getAcornWebConfigDirectory());
		cmdBuilder.addCommandAction(CommandUtil.CMD_LOAD_EMAIL);
		cmdBuilder.addSpaceID(spaceID);
		cmdBuilder.addUserID(userID);
		cmdBuilder.addEmailType("success");
		cmdBuilder.addLiveAccessLoadGroups(liveAccessLoadGroups);
		cmdBuilder.addLoadNumber(loadID);
		cmdBuilder.addLoadGroup(loadGroup);
		cmdBuilder.addUseSSL(useSSL);
		cmdBuilder.addParentSource(parentSource);
		return cmdBuilder.getCommand();
	}
	
	public static String getEmailForExtractionCommand(String logFile, String uid, 
			String spaceID, String userID, String type, boolean useSSL, boolean extractOnly, String result, String extractGroups) throws InvalidCommandException
	{
		if (spaceID == null || userID == null)
		{
			throw new InvalidCommandException("Send Email Extraction Success Command Missing Parameters : " +
					"spaceID= " + spaceID +
					"userID=" +  userID +
					"connectorType=" + type);
		}
		CommandBuilder cmdBuilder = CommandBuilder.getCommandBuilder();
		cmdBuilder.add(getAcornExePath());
		cmdBuilder.addUid(uid);
		cmdBuilder.addLogFile(logFile);
		cmdBuilder.addAcornWebConfigDirectory(SchedulerServerContext.getAcornWebConfigDirectory());
		cmdBuilder.addCommandAction(CommandUtil.CMD_EXTRACTION_EMAIL);
		cmdBuilder.addSpaceID(spaceID);
		cmdBuilder.addUserID(userID);
		cmdBuilder.addConnectorType(type);
		cmdBuilder.addEmailType(result);
		cmdBuilder.addUseSSL(useSSL);
		cmdBuilder.addExtractOnly(extractOnly);
		cmdBuilder.addExtractGroups(extractGroups);
		return cmdBuilder.getCommand();
	}
	
	private static String getAcornExePath()
	{
		return SchedulerServerContext.getAcornExecutePath();
		//return "C:\\Workspace\\AcornExecute\\AcornExecute\\bin\\x86\\Debug\\AcornExecute.exe";
	}


	public static String getProcessCommand(String engineCmd, String args) throws IOException, InvalidCommandException
	{		
		if(engineCmd == null || args == null)
		{
			throw new InvalidCommandException("Process Command Missing Parameters : " +
					"engineCmd= " + engineCmd +
					"args=" +  args);
		}
		CommandBuilder cmdBuilder = new CommandBuilder();
		cmdBuilder.add(engineCmd);
		cmdBuilder.add(args);
		return cmdBuilder.getCommand();
	}
	
	public static String getProcessCommandLogFilePath(String spaceDirectory)
	{
		return getSchedulerLogsDirectory(spaceDirectory) + File.separator + "processLog";
	}
	
	public static String getBDSProcessCommandLogFilePath(String spaceDirectory)
	{
		return getSchedulerLogsDirectory(spaceDirectory) + File.separator + "bdsProcessLog";
	}
	
	public static String getMessageFilePath(String spaceDirectory, String uid)
	{
		return getSchedulerLogsDirectory(spaceDirectory) + File.separator + "message." + uid + ".txt";
	}
	
	public static String getSFDCExtractionListFilePath(String spaceDirectory, String uid)
	{
		return getSchedulerLogsDirectory(spaceDirectory) + File.separator + "sfdcList." + uid + ".txt";
	}
	
	public static String getBDSExtractionListFilePath(String spaceDirectory, String uid)
	{
		return getSchedulerLogsDirectory(spaceDirectory) + File.separator + "bdsList." + uid + ".txt";
	}
	
	public static String getSpaceLogsDirectory(String spaceDirectory)
	{
		return spaceDirectory + File.separator + "slogs";
	}
	
	public static String getSchedulerLogsDirectory(String spaceDirectory)
	{
		return spaceDirectory + File.separator + "slogs";
	}
	
	public static boolean isFileExists(String filePath)
	{
		try
		{
			File file = new File(filePath);
			if(file.exists())
			{
				return true;
			}
		}catch(Exception ex)
		{
			logger.warn("Exception while checking for existence of file :" , ex);
		}
		return false;	
	}

	public static Properties getProps(String filePath)
	{	
		FileReader reader = null;
		Properties props = null;
		try
		{
			reader = new FileReader(filePath);
			props = new Properties();
			props.load(reader);
		} catch (IOException e)
		{
			logger.error("Exception while loading properties from file " + filePath, e);
		}
		finally
		{
			if(reader != null)
			{
				try
				{
					reader.close();
				} catch (IOException e)
				{	
				}
			}
		}
		
		return props;
	}
	
	public static int runCommand(String cmdKey, String cmd, String workingDirectory, Map<String, Process> runningProcesses) throws IOException, InterruptedException
	{
		return runCommand(cmdKey, cmd, workingDirectory, runningProcesses, null);
	}
	
	public static int runCommand(String cmdKey, String cmd, String workingDirectory, Map<String, Process> runningProcesses,
			CommandProcessHandler handlr) throws IOException, InterruptedException
	{	
		if(cmd == null)
		{
			logger.error("No command to run " + cmdKey);
			return CommandUtil.COMMAND_INVALID;
		}
		
		Process proc = null;
		boolean exitedGracefully = false;
		try
		{
			logger.info("Running command " + cmd);
			Runtime rt = Runtime.getRuntime();
					
			File file = workingDirectory != null ? new File(workingDirectory) : null;
			proc = rt.exec(cmd, null, file);
			if(runningProcesses != null)
			{
				runningProcesses.put(cmdKey, proc);
			}
			
			StreamFetchThread errorStream = new StreamFetchThread(proc.getErrorStream(), cmdKey + " ERROR", null);
			StreamFetchThread outputStream = new StreamFetchThread(proc.getInputStream(), cmdKey + " OUTPUT", null);
			errorStream.start();
			outputStream.start();
			if(handlr != null){
				handlr.logInfo();
			}
			int exitValue = proc.waitFor();
			exitedGracefully = true;
			logger.info("Exit value for command " + cmd + " : " + exitValue);
			errorStream.join(TIMEOUT_PROC_OUTPUT_MILLIS);
			outputStream.join(TIMEOUT_PROC_OUTPUT_MILLIS);
			return exitValue;
		}		
		finally
		{
			if(!exitedGracefully && proc != null)
			{
				proc.destroy();
			}

			if(runningProcesses != null && runningProcesses.containsKey(cmdKey))
			{	
				runningProcesses.remove(cmdKey);
			}
		}
	}

	public static boolean isSuccessfulExit(int returnCode)
	{
		return returnCode == CommandUtil.COMMAND_SUCCESS;
	}

	public static String getFailureType(int loaderExitCode)
	{
		if(mapOfExitCodes == null || mapOfExitCodes.size() == 0)
		{
			// initialize the map
			mapOfExitCodes.put(EXIT_CODE_CAN_PUBLISH, FAILURE_TYPE_CANNOT_PUBLISH);
			mapOfExitCodes.put(EXIT_CODE_SPACE_BEING_PUBLISHED, FAILURE_TYPE_IS_SPACE_PUBLISHED);
			mapOfExitCodes.put(EXIT_CODE_SPACE_BUSY, FAILURE_TYPE_SPACE_BUSY);
			mapOfExitCodes.put(IS_SPACE_DATA_LIMIT, FAILURE_TYPE_DATA_LIMIT);
			mapOfExitCodes.put(IS_SPACE_ROW_LIMIT, FAILURE_TYPE_ROW_LIMIT);
		}
		
		return mapOfExitCodes.get(loaderExitCode);
	}
	
	public static String getBDSEngineCommand(String logFile, String uid, String spaceID, String userID, boolean writeEmailInfo) 
		throws InvalidCommandException
	{
		if(spaceID == null || userID == null )
		{
			throw new InvalidCommandException("AppLoader Command Missing Parameters " +
					" : spaceID= " + spaceID +
					" : userID=" +  userID) ;
		}
		
		CommandBuilder cmdBuilder = CommandBuilder.getCommandBuilder();
		cmdBuilder.add(getAcornExePath());
		cmdBuilder.addUid(uid);
		cmdBuilder.addLogFile(logFile);
		cmdBuilder.addAcornWebConfigDirectory(SchedulerServerContext.getAcornWebConfigDirectory());
		cmdBuilder.addCommandAction(CMD_GET_BDS_ENGINE);
		cmdBuilder.addSpaceID(spaceID);
		cmdBuilder.addUserID(userID);
		cmdBuilder.addEmailerInfo(writeEmailInfo);
		cmdBuilder.addNumberBDSThreads(SchedulerServerContext.getSFDCNumberThreads());
		return cmdBuilder.getCommand();
	}

	public static String getUpdateRunningProcessCommand(String uid, String spaceID, String userID, 
			String logFile, int pid, String loadInfo) throws InvalidCommandException
	{
		if(spaceID == null || userID == null || pid < 0)
		{
			throw new InvalidCommandException("AppLoader Command Missing Parameters " +
					" : spaceID= " + spaceID +
					" : userID=" +  userID + 
					" : pid=" + pid) ;
		}
		
		CommandBuilder cmdBuilder = CommandBuilder.getCommandBuilder();
		cmdBuilder.add(getAcornExePath());
		cmdBuilder.addUid(uid);
		cmdBuilder.addLogFile(logFile);
		cmdBuilder.addAcornWebConfigDirectory(SchedulerServerContext.getAcornWebConfigDirectory());
		cmdBuilder.addCommandAction(CMD_UPDATE_ADMIN_DB);
		cmdBuilder.addSpaceID(spaceID);
		cmdBuilder.addUserID(userID);
		String paramName = "runningProcessParam";
		String semiColonSeparatedVals = String.valueOf(pid) + ";" + loadInfo;
		cmdBuilder.addAdminDbParams(paramName, semiColonSeparatedVals);

		return cmdBuilder.getCommand();
	}
}
