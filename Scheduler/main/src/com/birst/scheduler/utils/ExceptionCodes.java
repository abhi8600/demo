package com.birst.scheduler.utils;

import java.util.HashMap;
import java.util.Map;

public class ExceptionCodes
{
	// exit code prefix for the command lines
	public static final String PREFIX_EXIT_CODE = "EC:";

	public static final int INTERNAL_ERROR 						   = -555;
	public static final int EXIT_CODE_GENERAL                      = -100;
    public static final int EXIT_CODE_INVALID_ARGUMENT             = -102;

    public static final int EXIT_CODE_SPACE_PUBLISHABILITY         = -103;
    public static final int EXIT_CODE_CONCURRENT_SPACE_PUBLISH     = -104;        
    public static final int EXIT_CODE_SPACE_BUSY_OTHER             = -105;
    public static final int EXIT_CODE_SPACE_LOAD_AUTHORIZATION     = -106;

    public static final int EXIT_CODE_SFORCE_NOT_FOUND             = -107;
    public static final int EXIT_CODE_SFORCE_NO_OBJECTS            = -108;
    public static final int EXIT_CODE_CONCURRENT_SFORCE_EXTRACT    = -109;
    public static final int EXIT_CODE_SFORCE_UPLOAD_ERRORS         = -110;
    
    public static final int EXIT_CODE_CONCURRENT_SPACE_COPY			= -111;
    public static final int EXIT_CODE_CONCURRENT_SPACE_DELETE		= -112;
    public static final int EXIT_CODE_CONCURRENT_OTHER				= -113;
    public static final int EXIT_CODE_EXTRACTION_SUCCESS_FILE 		= -114;
    
	public static final String EXIT_MSG_GENERAL                      = "";
    public static final String EXIT_MSG_INVALID_ARGUMENT             = "Invalid Arg";

    public static final String EXIT_MSG_SPACE_PUBLISHABILITY         = "Publishing";
    public static final String EXIT_MSG_SPACE_PUBLISH_CONCURRENT     = "Concurrent Publishing";    
    public static final String EXIT_MSG_SPACE_BUSY_OTHER             = "Space Already Publising";
    public static final String EXIT_MSG_SPACE_LOAD_AUTHORIZATION     = "Load is not authorized";

    public static final String EXIT_MSG_SFORCE_NOT_FOUND             = "Sforce not found";
    public static final String EXIT_MSG_SFORCE_NO_OBJECTS            = "No Objects on SalesForce";
    public static final String EXIT_MSG_SFORCE_EXTRACT_CONCURRENT    = "SForce extraction already in progress";
    public static final String EXIT_MSG_SFORCE_UPLOAD_ERRORS         = "Error in uploading files";
    public static final String EXIT_MSG_CONCURRENT_SPACE_COPY		 = "Copying in progress";
    public static final String EXIT_MSG_CONCURRENT_SPACE_DELETE		 = "Deletion in progress";
    public static final String EXIT_MSG_CONCURRENT_OTHER			 = "Space busy in other operation";
    
    private static Map<Integer,String> exitCodeToMessageMapping;
    static 
    {
    	exitCodeToMessageMapping = new HashMap<Integer, String>();
    	exitCodeToMessageMapping.put(EXIT_CODE_GENERAL, EXIT_MSG_GENERAL);
    	exitCodeToMessageMapping.put(EXIT_CODE_INVALID_ARGUMENT, EXIT_MSG_INVALID_ARGUMENT);
    	exitCodeToMessageMapping.put(EXIT_CODE_SPACE_PUBLISHABILITY, EXIT_MSG_SPACE_PUBLISHABILITY);
    	exitCodeToMessageMapping.put(EXIT_CODE_CONCURRENT_SPACE_PUBLISH, EXIT_MSG_SPACE_PUBLISH_CONCURRENT);
    	exitCodeToMessageMapping.put(EXIT_CODE_SPACE_BUSY_OTHER, EXIT_MSG_SPACE_BUSY_OTHER);
    	exitCodeToMessageMapping.put(EXIT_CODE_SPACE_LOAD_AUTHORIZATION, EXIT_MSG_SPACE_LOAD_AUTHORIZATION);
    	exitCodeToMessageMapping.put(EXIT_CODE_SFORCE_NOT_FOUND, EXIT_MSG_SFORCE_NOT_FOUND);
    	exitCodeToMessageMapping.put(EXIT_CODE_SFORCE_NO_OBJECTS, EXIT_MSG_SFORCE_NO_OBJECTS);
    	exitCodeToMessageMapping.put(EXIT_CODE_CONCURRENT_SFORCE_EXTRACT, EXIT_MSG_SFORCE_EXTRACT_CONCURRENT);
    	exitCodeToMessageMapping.put(EXIT_CODE_SFORCE_UPLOAD_ERRORS, EXIT_MSG_SFORCE_UPLOAD_ERRORS);
    	exitCodeToMessageMapping.put(EXIT_CODE_CONCURRENT_SPACE_COPY, EXIT_MSG_CONCURRENT_SPACE_COPY);
    	exitCodeToMessageMapping.put(EXIT_CODE_CONCURRENT_SPACE_DELETE, EXIT_MSG_CONCURRENT_SPACE_DELETE);
    	exitCodeToMessageMapping.put(EXIT_CODE_CONCURRENT_OTHER, EXIT_MSG_CONCURRENT_OTHER);
    }
    
    public static String getMessage(int exitCode)
    {
    	return exitCodeToMessageMapping.get(exitCode);
    }
}
