package com.birst.scheduler.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.net.URL;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.Random;
import java.util.Set;
import java.util.TimeZone;
import java.util.UUID;
import java.util.Map.Entry;

import javax.xml.namespace.QName;
import javax.xml.stream.FactoryConfigurationError;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMAttribute;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
import org.apache.axiom.om.impl.builder.StAXOMBuilder;
import org.apache.axis2.AxisFault;
import org.apache.axis2.addressing.EndpointReference;
import org.apache.axis2.client.Options;
import org.apache.axis2.client.ServiceClient;
import org.apache.axis2.context.ConfigurationContext;
import org.apache.axis2.transport.http.HTTPConstants;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.MultiThreadedHttpConnectionManager;
import org.apache.commons.httpclient.params.HttpConnectionManagerParams;
import org.apache.log4j.Logger;
import org.apache.log4j.MDC;
import org.quartz.CronTrigger;
import org.quartz.Job;
import org.quartz.JobBuilder;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobKey;
import org.quartz.JobListener;
import org.quartz.Matcher;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.TriggerKey;
import org.quartz.TriggerListener;
import org.quartz.TriggerUtils;
import org.quartz.JobBuilder.*;
import org.quartz.impl.matchers.EverythingMatcher;
import org.quartz.impl.matchers.GroupMatcher;

import com.birst.scheduler.SchedulerServerContext;
import com.birst.scheduler.Report.ScheduledReport;
import com.birst.scheduler.Report.ScheduledReportJob;
import com.birst.scheduler.exceptions.ConcurrentSpaceOperationException;
import com.birst.scheduler.exceptions.SchedulerBaseException;
import com.birst.scheduler.exceptions.SchedulerNotRunningException;
import com.birst.scheduler.listeners.jobs.ConfigureRetryJob;
import com.birst.scheduler.listeners.jobs.PersistJobHistoryInDB;
import com.birst.scheduler.salesforce.SFDCInfo;
import com.birst.scheduler.security.Net;
import com.birst.scheduler.shared.BaseJobObject;
import com.birst.scheduler.shared.CopyJob;
import com.birst.scheduler.shared.DeleteSpaceJobs;
import com.birst.scheduler.shared.JobStatus;
import com.birst.scheduler.shared.Schedule;
import com.birst.scheduler.shared.ScheduleInfo;
import com.birst.scheduler.shared.SchedulerVersion;
import com.birst.scheduler.shared.ServerDetails;
import com.birst.scheduler.shared.SpaceStatus;
import com.birst.scheduler.utils.UtilConstants.JobType;
import com.birst.scheduler.utils.UtilConstants.RunningJobStatus;
import com.birst.scheduler.webServices.admin.JobAdminUtil;

public class Util
{
	public static final Logger logger = Logger.getLogger(Util.class);
	private static boolean CONFIGURED_CONNECTION_PARAMS = false;
	private static List<String> PREFIX_ALL_JOBS;
	private static List<String> PREFIX_ALL_JOBS_TRIGGERS;
	
	public static OMElement createPayLoad(String namespace, String operationName, String paramName, Object value) throws Exception
	{			
		LinkedHashMap<String,Object> map = new LinkedHashMap<String, Object>();
		map.put(paramName, value);		
		return createPayLoad(namespace, operationName, map);
	}
	
	/**
	 * 
	 * @param namespace
	 * @param operationName
	 * @param paramValuesMap - make sure to use the linkedHashMap because we want to keep the insertion order intact
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static OMElement createPayLoad(String namespace, String operationName, LinkedHashMap<String, Object> paramValuesMap)
	{
		
		if(paramValuesMap.size() != paramValuesMap.size()){
			return null;
		}
		
		OMFactory fac = OMAbstractFactory.getOMFactory();
		OMNamespace omNs = fac.createOMNamespace(namespace, "ns1");		
		OMElement method = fac.createOMElement(operationName, omNs);
		
		for (String key : paramValuesMap.keySet())
		{
			Object paramValue = paramValuesMap.get(key);		
			if (paramValue != null && paramValue instanceof ArrayList)
			{
				ArrayList<String> list = (ArrayList<String>) paramValue;
				for (String val : list)
				{
					OMElement listElement = fac.createOMElement(key, omNs);
					listElement.setText(val.toString());
					method.addChild(listElement);
				}
			} 
			else if(paramValue != null && paramValue.toString().length() > 0)
			{				
				OMElement value = fac.createOMElement(key, omNs);
				value.setText(paramValue.toString());
				method.addChild(value);
			}
			else
			{
				OMElement nullElement = fac.createOMElement(key, omNs);
				OMNamespace xsi = fac.createOMNamespace("http://www.w3.org/2001/XMLSchema-instance", "xsi");
				OMAttribute nil = fac.createOMAttribute("nil", xsi, "true");
				nullElement.addAttribute(nil);
				method.addChild(nullElement);			
			}
		}		
		return method;
	}
	
	public static ServiceClient getServiceClient(String serviceEndpoint) throws AxisFault
	{
		ServiceClient serviceClient = new ServiceClient(null, null);
		configureConnectionParams(serviceClient);
		Options opts = new Options();
		opts.setTo(new EndpointReference(serviceEndpoint));		
		serviceClient.setOptions(opts);
		return serviceClient;
	}	
	
	public static void configureConnectionParams(ServiceClient serviceClient)
	{
		if(!CONFIGURED_CONNECTION_PARAMS)
		{
			if(serviceClient != null)
			{
				ConfigurationContext configurationContext = serviceClient.getServiceContext().getConfigurationContext();
				MultiThreadedHttpConnectionManager multiThreadedHttpConnectionManager = new MultiThreadedHttpConnectionManager();

				HttpConnectionManagerParams params = new HttpConnectionManagerParams();
				params.setDefaultMaxConnectionsPerHost(SchedulerServerContext.getMaxConnectionsPerHost());
				params.setConnectionTimeout(SchedulerServerContext.getAxisClientTimeOut());
				params.setSoTimeout(SchedulerServerContext.getAxisClientTimeOut());
				multiThreadedHttpConnectionManager.setParams(params);
				HttpClient httpClient = new HttpClient(multiThreadedHttpConnectionManager);
				configurationContext.setProperty(HTTPConstants.CACHED_HTTP_CLIENT, httpClient);
				CONFIGURED_CONNECTION_PARAMS = true;
			}
		}
	}
	
	public static OMElement sendAndCleanUp(ServiceClient serviceClient, OMElement payLoad) throws AxisFault
	{
		OMElement response = null;
		try
		{
			 response = serviceClient.sendReceive(payLoad);
		}
		finally
		{
			serviceClient.cleanup();
			serviceClient.cleanupTransport();
		}
		return response;
	}
	
	public static SMIWebServiceResponse parseSMIWebResponse(String namespace, OMElement result)
	{
		SMIWebServiceResponse response = new SMIWebServiceResponse();
		OMElement resultElement = result.getFirstChildWithName(new QName(namespace, "return"));
		OMElement birstWSElement = null;
		if(resultElement != null)
		{
			birstWSElement = resultElement.getFirstChildWithName(new QName("com.successmetricsinc.WebServices.BirstWebServiceResult"));
			if (birstWSElement != null)
			{
				response.BirstWebServiceResultNode = birstWSElement;
				OMElement errorCodeElement = birstWSElement.getFirstChildWithName(new QName("ErrorCode"));
				if (errorCodeElement != null)
				{
					response.ErrorCode = Integer.parseInt(errorCodeElement.getText());
				}
				OMElement errorMessageElement = birstWSElement.getFirstChildWithName(new QName("ErrorMessage"));
				if (errorMessageElement != null)
				{
					response.ErrorMessage = errorMessageElement.getText();
				}
			}
		}
		
		return response;
	}
	
	public static BirstWebServiceResponse parseBirstWebServiceResponse(String namespace, String resultNodeName, OMElement inputElement)
	{		
		BirstWebServiceResponse response = new BirstWebServiceResponse();
		OMElement result = inputElement.getFirstChildWithName(new QName(namespace, resultNodeName));		
		if(result != null)
		{			
			response.resultNode = result;
			OMElement error = result.getFirstChildWithName(new QName(namespace, "Error"));
			if (error != null)
			{				
				OMElement errorType = error.getFirstChildWithName(new QName(namespace, "ErrorType"));
				if (errorType != null)
				{
					response.ErrorCode = Integer.parseInt(errorType.getText());					
				}
				OMElement errorMessage = error.getFirstChildWithName(new QName(namespace, "ErrorMessage"));
				if (errorMessage != null)
				{
					response.ErrorMessage = errorMessage.getText();
				}
			}		
		}
		return response;
	}

	public static boolean isSuccessful(WebServiceResponse response)
	{
		boolean success = false;
		if(response != null)
		{
			success = response.ErrorCode == 0 ? true : false;
		}
		return success;
	}	 	
	
	public static String getErrorMessage(SMIWebServiceResponse response)
	{
		return response != null ? response.ErrorMessage : null;
	}
	
	public static String getObjectID(String name, String prefix)
	{	
		String objectID = null;
		// id should be UUID
		try
		{
			if(prefix != null && name.startsWith(prefix) )
			{
				objectID = name.substring(prefix.length());				
			}
		}
		catch(Exception ex)
		{
			
		}
		return objectID;
	}
	
	public static String findObjectID(String name, List<String> prefixes)
	{
		// first see if it is Report, then Process, then SFDC
		String objectID = null;
		if(prefixes != null && prefixes.size() > 0)
		{
			for(String prefix : prefixes)
			{
				objectID = Util.getObjectID(name, prefix);
				if(objectID != null && objectID.length() > 0)
				{
					break;
				}	
			}
		}
		
		return objectID;
	}
	
	public static String findJobID(String name)
	{
		if(PREFIX_ALL_JOBS == null || PREFIX_ALL_JOBS.size() == 0)
		{
			PREFIX_ALL_JOBS = new ArrayList<String>();
			PREFIX_ALL_JOBS.add(UtilConstants.PREFIX_SCHEDULED_REPORT_JOB);
			PREFIX_ALL_JOBS.add(UtilConstants.PREFIX_CONNECTORS_JOB);
			PREFIX_ALL_JOBS.add(UtilConstants.PREFIX_BDS_JOB);
			PREFIX_ALL_JOBS.add(UtilConstants.PREFIX_DELETE_SPACE_JOB);
			PREFIX_ALL_JOBS.add(UtilConstants.PREFIX_PROCESS_JOB);
		}
		
		return findObjectID(name, PREFIX_ALL_JOBS);
	}
	
	public static String findTriggerID(String name)
	{
		if(PREFIX_ALL_JOBS_TRIGGERS == null || PREFIX_ALL_JOBS_TRIGGERS.size() == 0)
		{
			PREFIX_ALL_JOBS_TRIGGERS = new ArrayList<String>();
			PREFIX_ALL_JOBS_TRIGGERS.add(UtilConstants.PREFIX_SCHEDULED_REPORT_TRIGGER);
			PREFIX_ALL_JOBS_TRIGGERS.add(UtilConstants.PREFIX_CONNECTORS_JOB_TRIGGER);
			PREFIX_ALL_JOBS_TRIGGERS.add(UtilConstants.PREFIX_BDS_JOB_TRIGGER);
			PREFIX_ALL_JOBS_TRIGGERS.add(UtilConstants.PREFIX_DELETE_SPACE_TRIGGER);
			PREFIX_ALL_JOBS_TRIGGERS.add(UtilConstants.PREFIX_PROCESS_JOB_TRIGGER);
		}
		
		return findObjectID(name, PREFIX_ALL_JOBS_TRIGGERS);
	}
	
	@SuppressWarnings("unchecked")
	public static List<String> getRunningJobs(String groupName) throws SchedulerException, SchedulerNotRunningException
	{		
		return getRunningJobs(null, groupName);
	}	
	
	/***
	 * 
	 * @param jobPrefix - Name of the job. If jobName is null, all the jobs from the same group are returned
	 * @param groupName - Generally space id since jobs are grouped by space ids. Can be NULL . in that case every running job
	 * of ALL spaces will be returned
	 * @return - List of all the running jobs with the matching jobPrefix and groupName
	 * @throws SchedulerException
	 * @throws SchedulerNotRunningException
	 */
	@SuppressWarnings("unchecked")
	public static List<String> getRunningJobs(String jobName, String groupName) throws SchedulerException, SchedulerNotRunningException
	{		
		Scheduler scheduler = SchedulerServerContext.getSchedulerInstance();
		List<JobExecutionContext> ctxList = scheduler.getCurrentlyExecutingJobs();
		List<String> currentlyExecutingJobs = new ArrayList<String>();
		if(ctxList != null && ctxList.size() > 0)
		{
			for(JobExecutionContext ctx : ctxList)
			{
				JobDetail jobDetail = ctx.getJobDetail();
				String runningJobName = jobDetail.getKey().getName();
				String jobGroup = jobDetail.getKey().getGroup();
				if(groupName == null)
				{
					// get all the jobs
					currentlyExecutingJobs.add(runningJobName);
					continue;
				}				
				
				if(jobName == null && groupName != null && jobGroup.equalsIgnoreCase(groupName))
				{
					currentlyExecutingJobs.add(runningJobName);
					continue;
				}
				
				if(jobName != null && runningJobName.equalsIgnoreCase(jobName) && jobGroup.equalsIgnoreCase(groupName))
				{
					currentlyExecutingJobs.add(runningJobName);
				}				
			}
		}
		return currentlyExecutingJobs;
	}	
	/*
	public static Trigger getTrigger(ScheduleInfo scheduleInfo)
	{
		int interval = scheduleInfo.getInterval();
		int dayOfMonth = scheduleInfo.getDayOfMonth();
		int dayOfWeek = scheduleInfo.getDayOfWeek();
		int hour = scheduleInfo.getHour();
		int minute = scheduleInfo.getMinute();
		
		Trigger trigger = null;
		if (interval == ScheduleInfo.INTERVAL_MONTH)
		{
			trigger = TriggerUtils.makeMonthlyTrigger(dayOfMonth, hour, minute);					
		} else if (interval == ScheduleInfo.INTERVAL_WEEK)
		{
			trigger = TriggerUtils.makeWeeklyTrigger(dayOfWeek, hour, minute);					
		} else if (interval == ScheduleInfo.INTERVAL_DAY)
		{
			trigger = TriggerUtils.makeDailyTrigger(hour, minute);
		} else if (interval == ScheduleInfo.INTERVAL_ONCE)
		{
			trigger = TriggerUtils.makeImmediateTrigger(0, 0);
		}
		return trigger;
	}
	
	*/
	
	public static String getConnectorOutputStatusFileName(String spaceDirectory, String connectorName, String fileNamePart, String extractInfoId)
	{
		String fileName = connectorName + "Status" + "-" + fileNamePart +  "-" + extractInfoId + ".txt";
		return getConnectorsDirectoryName(spaceDirectory) + File.separator + fileName;
	}
	
	public static String getConnectorsDirectoryName(String spaceDirectory)
	{
		return spaceDirectory + File.separator + "connectors"; 
	}
	
	public static String getExtractInfoName(String spaceDirectory)
	{
		return getConnectorsDirectoryName(spaceDirectory) + File.separator + "extract_info.txt"; 
	}
	
	public static String validateTrustedInternalRequest(String requestIP)
	{	
		StringBuilder sb = new StringBuilder();
		// check for trusted sender
		List<String> loginAllowedIPs = SchedulerServerContext.getSchedulerLoginAllowedIps();
		if (loginAllowedIPs != null)
		{
			if (!Net.containIp(loginAllowedIPs, requestIP))
			{
				logger.error("SchedulerAccess : Request IP not allowed: bad request IP: " + requestIP);
				logger.error("SchedulerAccess: Please check the SchedulerLogin.AllowsIPs in web.xml");
				sb.append("Request IP not allowed: bad request IP");
				return sb.toString();
			}
			if (logger.isTraceEnabled())
				logger.trace("SchedulerAccess: Valid request from host " + requestIP);
		} else
		{
			logger.error("No valid IPs list for Scheduler Login Service.");
			logger.error("Please check the SchedulerLogin.AllowsIPs in web.xml or customer.properties");
			sb.append("No valid IPs list for Scheduler Login Service.");
			return sb.toString();
		}
		
		return null;
	}
	
	
	
	public static String getCustReportJobName(String reportId)
	{
		return UtilConstants.PREFIX_SCHEDULED_REPORT_JOB + reportId;
	}
	
	public static String getBDSCustJobName(String reportId)
	{
		return UtilConstants.PREFIX_BDS_JOB + reportId;
	}
	
	public static String getCustReportTriggerName(String triggerId)
	{
		return UtilConstants.PREFIX_SCHEDULED_REPORT_TRIGGER + triggerId;
	}
	
	public static String getBDSCustTriggerName(String triggerId)
	{
		return UtilConstants.PREFIX_BDS_JOB_TRIGGER + triggerId;
	}
	
	public static String getRetryTriggerName(String triggerId)
	{
		return UtilConstants.PREFIX_RETRY + triggerId;
	}
	
	public static String getCustImmediateTriggerName(String triggerId)
	{
		return UtilConstants.PREFIX_IMMEDIATE_TRIGGER + triggerId;
	}	
	
	public static String getCustDeleteSpaceJobName(String reportId)
	{
		return UtilConstants.PREFIX_DELETE_SPACE_JOB + reportId;
	}
	
	public static String getCustDeleteSpaceTriggerName(String triggerId)
	{
		return UtilConstants.PREFIX_DELETE_SPACE_TRIGGER + triggerId;
	}
	
	public static String getCustProcessJobName(String id)
	{
		return getPrefixedName(UtilConstants.PREFIX_PROCESS_JOB, id);
	}
	
	public static String getCustProcessTriggerName(String triggerId)
	{
		return getPrefixedName(UtilConstants.PREFIX_PROCESS_JOB_TRIGGER , triggerId);
	}
	
	public static String getCustConnectorJobName(String id)
	{
		return getPrefixedName(UtilConstants.PREFIX_CONNECTORS_JOB , id);
	}
	
	public static String getCustConnectorTriggerName(String triggerId)
	{
		return getPrefixedName(UtilConstants.PREFIX_CONNECTORS_JOB_TRIGGER, triggerId);
	}
	
	public static String getCustDeleteJobName(String id)
	{
		return getPrefixedName(UtilConstants.PREFIX_DELETE_LOAD_JOB , id);
	}
	
	public static String getCustDeleteTriggerName(String triggerId)
	{
		return getPrefixedName(UtilConstants.PREFIX_DELETE_LOAD_JOB_TRIGGER, triggerId);
	}
	
	public static String getPrefixedName(String prefix, String id)
	{
		return prefix + id;
	}
	
	public static String getCustJobGroupNameForReport(String spaceID)
	{
		return spaceID;
	}
	
	public static String getCustJobGroupNameForSFDC(String spaceID)
	{
		return getPrefixedName(UtilConstants.PREFIX_CONNECTORS_JOB_GROUP_NAME, spaceID);
	}
	
	public static String getCustJobGroupNameForDelete(String spaceID)
	{
		return getPrefixedName(UtilConstants.PREFIX_DELETE_JOB_GROUP_NAME, spaceID);
	}
	
	public static String getCustJobGroupNameForProcess(String spaceID)
	{
		return getPrefixedName(UtilConstants.PREFIX_PROCESS_JOB_GROUP_NAME, spaceID);
	}
	
	public static String getCustJobGroupNameForBDS(String spaceID)
	{
		return getPrefixedName(UtilConstants.PREFIX_BDS_JOB_GROUP_NAME, spaceID);
	}

	public static Trigger getImmediateTrigger(String spaceID, JobDetail jobDetail, String uidString )
	{
		Trigger trigger = TriggerBuilder.newTrigger()
		.withIdentity(Util.getCustImmediateTriggerName(uidString), spaceID)
		.startAt(Calendar.getInstance().getTime())
		.forJob(jobDetail)		
		.build();
		return trigger;
	}

	public static JobType getJobType(String jobName)
	{
		JobType jobType = null;
		if(jobName.startsWith(UtilConstants.PREFIX_SCHEDULED_REPORT_JOB))
		{
			jobType = JobType.REPORT;
		}
		
		return jobType;
	}
	
	/**
	 * Use this methods to create job detail instances in all the scheduler.
	 * All the jobs are set to durable meaning if there are no triggers, job is not removed from the scheduler
	 * This helps us to provide enable/disable trigger functionality on the UI
	 * @return
	 */
	public static JobDetail getBirstJobDetail()
	{
		return null;
	}
	public static JobBuilder getBirstJobBuilder(Class<? extends Job> jobClassName, String jobName, String groupName)
	{
		return getBirstJobBuilder(jobClassName, jobName, groupName, true);
	}
	
	public static JobBuilder getBirstJobBuilder(Class<? extends Job> jobClassName, String jobName, String groupName, boolean makeTheJobDurable)
	{
		/*
		JobDetail jobDetail = new JobDetail();
		jobDetail.setDurability(true);
		*/
		
		JobBuilder jobBuilder = JobBuilder.newJob(jobClassName);
		jobBuilder.withIdentity(jobName, groupName);
		if(makeTheJobDurable)
		{
			jobBuilder.storeDurably().build();
		}
		
		return jobBuilder;
	}
	
	/*
	public static void addListener(JobDetail jobDetail, String listener)
	{	
		jobDetail.addJobListener(listener);
	}
	*/
	
	@SuppressWarnings("unchecked")
	public static void addAllJobsListener(JobListener jobListener) throws SchedulerNotRunningException, SchedulerException
	{		
		SchedulerServerContext.getSchedulerInstance().getListenerManager().addJobListener(jobListener, EverythingMatcher.allJobs());
	}
	
	@SuppressWarnings("unchecked")
	public static void addAllTriggerListener(TriggerListener triggerListener) throws SchedulerNotRunningException, SchedulerException
	{		
		SchedulerServerContext.getSchedulerInstance().getListenerManager().addTriggerListener(triggerListener, EverythingMatcher.allTriggers());
	}

	// get lower case string
	public static String getLowerCaseString(String str)
	{
		return str != null ? str.toLowerCase(Locale.ENGLISH) : null;
	}
	
	public static int getJobRetryTimeRange(String interval)
	{
		int retrySeconds = 2*60*60; // 2 hours
		if(interval != null && interval.length() > 0)
		{
			if(interval.equalsIgnoreCase(Schedule.INTERVAL_DAILY))
			{
				retrySeconds = SchedulerServerContext.getDailyRetrySeconds();
			}
			else if(interval.equalsIgnoreCase(Schedule.INTERVAL_WEEKLY))
			{
				retrySeconds = SchedulerServerContext.getWeeklyRetrySeconds();
			}
			else if(interval.equalsIgnoreCase(Schedule.INTERVAL_MONTHLY))
			{
				retrySeconds = SchedulerServerContext.getMonthlyRetrySeconds();
			}
		}
		return retrySeconds;
	}
	
	
	public static String getInterval(String cronExpression)
	{	
		
		int INDEX_CRON_DAYOFWEEK = 5;
		int INDEX_CRON_MONTH = 4;
		int INDEX_CRON_DAYOFMONTH = 3;
		
		String interval = null;
		String[] cronParts = cronExpression.split(" ");
		if(equalsAllValues(cronParts[INDEX_CRON_DAYOFWEEK])
			&& equalsAllValues(cronParts[INDEX_CRON_MONTH])
			&& equalsNoSpecificValue(cronParts[INDEX_CRON_DAYOFMONTH]))
		{
			interval = Schedule.INTERVAL_DAILY;
		}


		if(!equalsAllValues(cronParts[INDEX_CRON_DAYOFWEEK])  
			&& equalsAllValues(cronParts[INDEX_CRON_MONTH])
			&& equalsNoSpecificValue(cronParts[INDEX_CRON_DAYOFMONTH]))
		{
			interval = Schedule.INTERVAL_WEEKLY;

		}

		if(equalsNoSpecificValue(cronParts[INDEX_CRON_DAYOFWEEK]) 
			&& equalsAllValues(cronParts[INDEX_CRON_MONTH])
			&& (!equalsNoSpecificValue(cronParts[INDEX_CRON_DAYOFMONTH]) || !equalsAllValues(cronParts[INDEX_CRON_DAYOFMONTH])))
		{
			interval = Schedule.INTERVAL_MONTHLY;

		}

		return interval;
		
	}
	
	private static boolean equalsAllValues(String str)
	{
		return UtilConstants.ALL_VALUES.equals(str);
	}
	
	private static boolean equalsNoSpecificValue(String str)
	{
		return UtilConstants.NO_SPECIFIC.equals(str);
	}
	
	public static boolean isServerAvailable(String smiWebUrl)
	{
		boolean available = false;
		try
		{
			
		}
		catch(Exception ex)
		{
			
		}
		return available;
	}

	public static void configureForRetry(JobExecutionContext ctx, JobStatus executionStatus)
	{
		if(executionStatus != null){
			configureForRetry(ctx, executionStatus.getJobStatus(), executionStatus.isRetry(), executionStatus.getErrorMessage());
		}
	}
	
	public static void configureForRetry(JobExecutionContext ctx, RunningJobStatus jobStatus, 
			boolean retry, String errorMessage)
	{
		JobDataMap triggerJobDataMap = ctx.getTrigger().getJobDataMap();
		triggerJobDataMap.put(PersistJobHistoryInDB.KEY_STATUS, jobStatus.getStatus());
		triggerJobDataMap.put(PersistJobHistoryInDB.KEY_ERROR_MESSAGE, errorMessage);
		String parentJobNextFireTime = (String) triggerJobDataMap.get(ConfigureRetryJob.KEY_RETRY_JOB_PARENT_NEXT_FIRETIME);
		if(retry)
		{	
				
			if(parentJobNextFireTime == null)
			{
				Date nextFireTime = ctx.getTrigger().getNextFireTime();
				// if both parentJobNextFireTime and nextFireTime is null meaning this is run now job. 
				// Ignoring the use case for retrying
				if(nextFireTime == null)
				{
					retry = false;
				}
				else
				{
					Calendar cal = Calendar.getInstance();
					cal.setTime(nextFireTime);
					parentJobNextFireTime = String.valueOf(cal.getTimeInMillis());
				}
			}
			
			if(retry)
			{
				triggerJobDataMap.put(ConfigureRetryJob.KEY_RETRY_JOB_PARENT_NEXT_FIRETIME, parentJobNextFireTime);
				triggerJobDataMap.put(ConfigureRetryJob.KEY_RETRY_JOB, "true");
				String parentJobIntervalValue = null;
				String parentJobTimeZone = null;
				if(ctx.getTrigger() instanceof CronTrigger)
				{
					CronTrigger cronTrigger = (CronTrigger) ctx.getTrigger();
					parentJobIntervalValue = Util.getInterval(cronTrigger.getCronExpression());
					TimeZone tz = cronTrigger.getTimeZone();
					parentJobTimeZone = tz.getDisplayName();
				}
				else
				{
					// If this is a retry job, then the trigger won't be an instance of CronTrigger
					// In that case look for parameter that defines the interval for the job
					parentJobIntervalValue = (String) ctx.getTrigger().getJobDataMap().get(ConfigureRetryJob.KEY_RETRY_JOB_PARENT_INTERVAL);
					parentJobTimeZone = (String) ctx.getTrigger().getJobDataMap().get(ConfigureRetryJob.KEY_RETRY_JOB_PARENT_TIME_ZONE);
				}
				
				if(parentJobIntervalValue != null && parentJobIntervalValue.length() > 0)
				{
					triggerJobDataMap.put(ConfigureRetryJob.KEY_RETRY_JOB_PARENT_INTERVAL, parentJobIntervalValue);
				}
				
				if(parentJobTimeZone != null && parentJobTimeZone.length() > 0)
				{
					triggerJobDataMap.put(ConfigureRetryJob.KEY_RETRY_JOB_PARENT_TIME_ZONE, parentJobTimeZone);
				}
			}
		}
		else
		{				
			triggerJobDataMap.remove(ConfigureRetryJob.KEY_RETRY_JOB_PARENT_NEXT_FIRETIME);
			triggerJobDataMap.remove(ConfigureRetryJob.KEY_RETRY_JOB);
			triggerJobDataMap.remove(ConfigureRetryJob.KEY_RETRY_JOB_PARENT_INTERVAL);
			triggerJobDataMap.remove(ConfigureRetryJob.KEY_RETRY_JOB_PARENT_TIME_ZONE);
		}
	}
	
	public static String getDecryptedString(String text)
	{
		EncryptionService es = EncryptionService.getInstance();
		return es.decrypt(text);
	}
	
	public static List<ServerDetails> retrieveSMIWebServers(int schedulerServerID) throws SchedulerBaseException, ClassNotFoundException
	{
		Connection conn = null;
		String schema = UtilConstants.SCHEDULER_DB_SCHEMA;
		List<ServerDetails> serverDetails = null;
		try
		{			
			conn = SchedulerDBConnection.getInstance().getConnectionPool().getConnection();
			serverDetails = SchedulerDatabase.getSMIWebServers(conn, schema, schedulerServerID, true);
			
		} catch (SQLException sqe)
		{
			logger.error("SQLException in retrieveSMIWebServers ", sqe);
		} finally
		{
			try
			{
				if (conn != null)
					conn.close();
			} catch (Exception ex)
			{
			}
			
		}
		return serverDetails;
	}
	
	public static boolean isAnyScheduleEnabled(Schedule[] jobSchedules)
	{
		boolean enabled = false;
		if(jobSchedules != null && jobSchedules.length > 0)
		{
			for (Schedule schedule : jobSchedules)
			{
				if(schedule.isEnable())
				{
					enabled = true;
					break;				
				}
			}
		}
		return enabled;
	}
	
	@SuppressWarnings("unchecked")
	// Map of job id to type
	public static List<BaseJobObject> parseRunStatusesXmlString(String jobOptionsXmlString)
	{

		XMLStreamReader reader = null;
		List<BaseJobObject> response = new ArrayList<BaseJobObject>();
		try
		{
			reader = XMLInputFactory.newInstance().createXMLStreamReader(new StringReader(jobOptionsXmlString));
			StAXOMBuilder builder = new StAXOMBuilder(reader);
			OMElement docElement = builder.getDocumentElement();
			docElement.build();
			docElement.detach();

			OMElement element = docElement.getFirstChildWithName(new QName("", JobConstants.JOBS_LIST_NODE));
			// see if the root element itself is ScheduledReport
			if(element == null)
			{
				if (docElement.getLocalName() == JobConstants.JOBS_LIST_NODE)
				{
					element = docElement;
				}
			}
			if(element != null)
			{			
				Iterator<OMElement> iterator = element.getChildrenWithName(new QName("", JobConstants.JOB_NODE));
				while(iterator.hasNext())
				{
					OMElement rootElement = iterator.next();
					Iterator<OMElement> scheduleJobDetailsIterator = rootElement.getChildElements();
					BaseJobObject jobObject = new BaseJobObject();
					while(scheduleJobDetailsIterator.hasNext())
					{						
						OMElement childElement = scheduleJobDetailsIterator.next();
						String elementName = childElement.getLocalName();
						String elementText = childElement.getText();					

						if(JobConstants.JOB_TYPE.equals(elementName))
						{
							jobObject.setType(elementText);
						}

						if(JobConstants.JOB_ID.equals(elementName))
						{
							jobObject.setJobId(elementText);
						}

						if(JobConstants.JOB_RUN.equals(elementName))
						{
							jobObject.setEnabled(Boolean.parseBoolean(elementText));
						}
					
					}
					response.add(jobObject);
				}
			}

		} catch (XMLStreamException e)
		{
			logger.error(e,e);
		} catch (FactoryConfigurationError e)
		{
			logger.error(e,e);
		}
		return response;
	}
	
	/**
	 * childElement is the ScheduleList element
	 * @param childElement
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static List<Schedule> parseSchedules(OMElement childElement)
	{
		List<Schedule> schedules = new ArrayList<Schedule>();
		Iterator<OMElement> schedulesIterator = childElement.getChildrenWithName(new QName("", JobConstants.JOB_SCHEDULE));
		while(schedulesIterator.hasNext())
		{
			Schedule schedule = new Schedule();							
			OMElement scheduleElement = schedulesIterator.next();
			Iterator<OMElement> schedulePropertiesElement = scheduleElement.getChildElements();
			while(schedulePropertiesElement.hasNext())
			{								
				OMElement propElement = schedulePropertiesElement.next();
				String name = propElement.getLocalName();
				String text = propElement.getText();
				if(JobConstants.JOB_SCHEDULE_EXPRESSION.equals(name))
				{
					schedule.setExpression(text);
				}

				if(JobConstants.JOB_SCHEDULE_TIMEZONE.equals(name))
				{
					schedule.setTimeZone(text);
				}

				if(JobConstants.JOB_SCHEDULE_ENABLE.equals(name))
				{
					schedule.setEnable(Boolean.valueOf(text));
				}
				if(JobConstants.JOB_SCHEDULE_ID.equals(name))
				{
					schedule.setId(text);
				}
				if(JobConstants.JOB_SCHEDULE_JOB_ID.equals(name))
				{
					schedule.setJobId(text);
				}
				
				if(JobConstants.JOB_SCHEDULE_START_TIME.equals(name))
				{
					schedule.setStartDate(text);
				}
			}
			schedules.add(schedule);
		}
		return schedules;
	}
	
	/***
	 * Returns the Date from string in MM/dd/yyyy format
	 * @param dateString
	 * @return
	 */
	public static Date getDate(String dateString)
	{
		if(dateString == null)
			return null;
		Date date = null;
		// we expect it to be in mm/dd/yyyy format					
		SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
		try
		{
			date = format.parse(dateString);
			
		} catch (Exception e)
		{
			logger.error("Unable to parse start date for schedule ", e);
		}
		
		return date;
	}
	
	@SuppressWarnings("unchecked")
	public static CopyJob parseCopyJobXmlString(String spaceID, String userID, String copyJobOptionsXmlString) throws Exception
	{
		CopyJob copyJob = null;
		XMLStreamReader reader = null;
		
		try
		{
			reader = XMLInputFactory.newInstance().createXMLStreamReader(new StringReader(copyJobOptionsXmlString));
			StAXOMBuilder builder = new StAXOMBuilder(reader);
			OMElement docElement = builder.getDocumentElement();
			docElement.build();
			docElement.detach();
			
			OMElement element = docElement.getFirstChildWithName(new QName("", JobConstants.JOB_COPY_NODE));
			// see if the root element iteself is ScheduledReport
			if(element == null)
			{
				if (docElement.getLocalName() == JobConstants.JOB_COPY_NODE)
				{
					element = docElement;
				}
			}
			if(element != null)
			{
				copyJob = new CopyJob();	
				copyJob.setSpaceId(spaceID);
				copyJob.setUserId(userID);
				Iterator<OMElement> iterator = element.getChildElements();
				
				while(iterator.hasNext())
				{
					OMElement childElement = iterator.next();
					String elementName = childElement.getLocalName();
					String elementText = childElement.getText();					
					
					if(JobConstants.JOB_COPY_ID.equals(elementName))
					{
						copyJob.setJobId(elementText);
					}
					
					if(JobConstants.JOB_TYPE.equals(elementName))
					{
						String jobType = elementText;
						if (!SchedulerServerContext.getUseNewConnectorFramework())
						{
							jobType = JobAdminUtil.getJobTypeForOldFramework(jobType);
						}
						copyJob.setJobType(jobType);
					}
					
					if(JobConstants.JOB_COPY_FROM.equals(elementName))
					{
						SchedulerVersion fromSchedulerVersion = SchedulerServerContext.getSchedulerVersion(elementText);
						if(fromSchedulerVersion == null)
						{
							throw new Exception("Unable to find from scheduler with display Name : " + elementText);
						}
						
						boolean isPrimary = SchedulerServerContext.getPrimaryScheduler() == fromSchedulerVersion.getScheduler();
						if(!fromSchedulerVersion.getEnableMigration() && !isPrimary)
						{
							throw new Exception("This version of scheduler is not allowed for migration from : " + elementText);
						}
						copyJob.setFromScheduler(fromSchedulerVersion.getScheduler());
					}
					
					if(JobConstants.JOB_COPY_TO.equals(elementName))
					{
						SchedulerVersion toSchedulerVersion = SchedulerServerContext.getSchedulerVersion(elementText);
						if(toSchedulerVersion == null)
						{
							throw new Exception("Unable to find to scheduler with display Name : " + elementText);
						}
						copyJob.setToScheduler(toSchedulerVersion.getScheduler());
					}
					
					
					if(JobConstants.JOB_COPY_DELETE_FROM.equals(elementName))
					{
						boolean deleteFrom = Boolean.parseBoolean(elementText);
						copyJob.setDeleteFrom(deleteFrom);
					}
					
					if(JobConstants.JOB_SCHEDULE_OLD_SFDC.equals(elementName))
					{
						boolean hasMultipleJobIDs = Boolean.parseBoolean(elementText);
						copyJob.setHasMultipleJobIDs(hasMultipleJobIDs);
					}
				}
				
				// if the to scheduler is not given, assume it is the current scheduler
				if(copyJob.getToScheduler() == null)
				{
					copyJob.setToScheduler(SchedulerServerContext.getPrimaryScheduler());
				}
				
				if(copyJob.getJobId() == null)
				{
					throw new Exception("No JobId is given : " + copyJobOptionsXmlString);
				}
			}
			
		} catch (XMLStreamException e)
		{
			logger.error(e,e);
		} catch (FactoryConfigurationError e)
		{
			logger.error(e,e);
		}
		return copyJob;
	}
		
	public static String getAndAddUserFromMap(
			Map<String, String> userIdToNameMapping, String userID)
	{
		String userName = null;
		Connection conn = null;
		String schema = AdminDatabase.ADMIN_DB_SCHEMA;
		try
		{
			if(userIdToNameMapping.get(userID) == null)
			{
				conn = AcornAdminDBConnection.getInstance().getConnectionPool().getConnection();
				Map<String,String> userInfo = AdminDatabase.getUserInfo(conn, schema, userID);
				if(userInfo != null && userInfo.size() > 0)
				{
					String uName = userInfo.get(UtilConstants.KEY_USERNAME);
					if(uName != null)
					{
						userIdToNameMapping.put(userID, uName);
					}			
				}
			}
			
			userName = userIdToNameMapping.get(userID);
		}
		catch(Exception ex){
			logger.warn("Error in getting user info for " + userID, ex);
		}
		finally
		{
			if(conn != null)
			{
				try
				{
					conn.close();
				}catch(Exception ex2){
					logger.warn("Error in closing connection : getUserFromMap", ex2);
				}
			}
		}
		return userName;
	}
	
	public static String addCatalogPrefix(String reportPath)
	{
		if(reportPath == null)
		{
			return null;
		}
		
		if(!reportPath.startsWith(UtilConstants.REPORT_CATALOG_PREFIX))
		{
			return UtilConstants.REPORT_CATALOG_PREFIX + reportPath;
		}
		
		return reportPath;
	}
	
	/**
	 * read in a properties file, optionally search for it in 'standard' locations (scheduler.home/conf, classpath)
	 * @param file
	 * @param search
	 * @return
	 */
	public static Properties loadPropertiesFromFile(String file, boolean search)
	{
		Properties result = new Properties();
		// 1) try the provided filename
		FileInputStream fis = null;
		try
		{
			fis = new FileInputStream(file);
			result.load(fis);
			logger.debug("Using properties file: " + file);
		} catch (FileNotFoundException e)
		{
			logger.debug("Properties file was not found: " + file);
		} catch (IOException e)
		{
			logger.debug("IOException reading properties file: " + file);
		} finally
		{
			if (fis != null)
			{
				try
				{
					fis.close();
				} catch (Exception e)
				{
				}
			}
		}
		// If no 'search' requested, end search here.
		if (!search)
		{
			logger.debug("No properties file - " + file + " in current directory and no request to continue search.");
			return null;
		}
		// 2) Try <scheduler.home>/conf
		if (result.isEmpty())
		{
			// But only if the 1st file path didn't succeed...
			String schedulerHome = System.getProperty(UtilConstants.SYSTEM_PROPERTY_HOME);
			if (schedulerHome != null && schedulerHome.length() > 0)
			{
				String path = schedulerHome + "/conf/" + file;
				fis = null;
				try
				{
					fis = new FileInputStream(path);
					result.load(fis);
					logger.debug("Using properties file: " + path);
					return result;
				} catch (FileNotFoundException e)
				{
					logger.debug("scheduler.home properties file (" + path + ") was not found.");
				} catch (IOException e)
				{
					logger.debug("scheduler.home properties file (" + path + ") found but errors reading file");
				} finally
				{
					if (fis != null)
					{
						try
						{
							fis.close();
						} catch (Exception e)
						{
						}
					}
				}
			}
			// 3) try the classpath
			logger.trace("Continuing search for properties file in classpath.");
			InputStream in = null;
			try
			{
				in = (new Util()).getClass().getClassLoader().getResourceAsStream(file);
				if (in != null)
				{
					result.load(in);
					logger.debug("Using properties file: " + file + " (found via the classpath)");
				}
			} catch (IOException e)
			{
				logger.debug("classpath properties file (" + file + ") not found.");
			} finally
			{
				if (in != null)
				{
					try
					{
						in.close();
					} catch (Exception e)
					{
					}
				}
			}
		}
		// Report results of search
		if (result.isEmpty())
		{
			logger.debug("Properties file (" + file + ") not found or empty after full search was performed.");
		} else
		{	
			if (logger.isDebugEnabled())
				printProperties(result);
		}
		return result;
	}
	
	/**
	 * dump the properties to the log file
	 * @param props
	 */
	private static void printProperties(Properties props)
	{
		logger.debug("== Properties files ==");
		for (Entry<Object, Object> item : props.entrySet())
		{
			// Dont spit out username/password
			String key = item.getKey().toString();
			if(key.equalsIgnoreCase(SchedulerServerContext.ACORN_ADMIN_USERNAME) 
					|| key.equalsIgnoreCase(SchedulerServerContext.ACORN_ADMIN_PASSWORD))
					{
						continue;
					}
			if(key.toLowerCase().startsWith(SchedulerServerContext.QUARTZ_JOBSTORE_DATASOURCE_PREFIX.toLowerCase()) 
					&& (key.toLowerCase().endsWith(SchedulerServerContext.QUARTZ_JOBSTORE_DATASOURCE_SUFFIX_USER.toLowerCase())
							|| key.toLowerCase().endsWith(SchedulerServerContext.QUARTZ_JOBSTORE_DATASOURCE_SUFFIX_PASSWORD.toLowerCase())
					))
			{
				continue;
			}
			logger.debug(item.getKey().toString() + "=" + item.getValue().toString());
		}
		logger.debug("====");
	}
	
	public static void clearOutMDC()
	{
		MDC.remove("sessionID");
		MDC.remove("userID");
		MDC.remove("spaceID");
	}
	
	public static void setMDC(String userID, String spaceID)
	{
		if(userID != null)
		{
			MDC.put("userID", userID);
		}
		if(spaceID != null)
		{
			MDC.put("spaceID", spaceID);
		}
	}

	public static String findFile(String fileName)
	{	
		URL url = (new Util()).getClass().getClassLoader().getResource(fileName);
		if(url != null)
		{	
			return url.getFile();
		}
		return null;

	}
	public static String getSMIWebUrl() throws Exception
	{
		List<ServerDetails> serverDetails = Util.retrieveSMIWebServers(SchedulerServerContext.getSchedulerServerID());
		if(serverDetails != null && serverDetails.size() > 0)
		{
			// Pick a randomn from the list
			Random rand = new Random();
			ServerDetails chosenSMIWeb = serverDetails.get(rand.nextInt(serverDetails.size()));
			return chosenSMIWeb.getUrl();
		}
		else
		{
			throw new Exception("No SMIWeb Server available. Please add some server information to the database");
		}
	}

	public static boolean isSpaceDeleted(Map<String, String> propertiesMap)
	{
		boolean isDeleted = false;
		if(propertiesMap != null && propertiesMap.size() > 0)
		{
			String value = propertiesMap.get(UtilConstants.KEY_SPACE_DELETE);
			if(value != null && Boolean.TRUE.toString().equalsIgnoreCase(value))
			{
				isDeleted = true;
			}
		}
		return isDeleted;
	}

	public static void scheduleDeletedSpaceJob(String spaceID) throws SchedulerNotRunningException, SchedulerException
	{
		if(spaceID != null)
		{
			String groupName = "DeleteSpaceGroup-" + spaceID;
			JobDetail jobDetail = Util.getBirstJobBuilder(DeleteSpaceJobs.class, Util.getCustDeleteSpaceJobName(UUID.randomUUID().toString()), groupName, false).build();
			jobDetail.getJobDataMap().put(UtilConstants.KEY_SPACE_ID, spaceID);
			
			// just give a buffer time of 5 minutes so that the job which 
			// scheduled the delete job is finished
			Calendar deleteJobTime = Calendar.getInstance();
			deleteJobTime.add(Calendar.MINUTE, 5);
			
			String triggerName = Util.getCustDeleteSpaceTriggerName(UUID.randomUUID().toString());
			TriggerKey triggerKey = new TriggerKey(triggerName, groupName);
			Trigger trigger = TriggerBuilder.newTrigger()
			.withIdentity(triggerKey)
			.forJob(jobDetail)
			.startAt(deleteJobTime.getTime())				
			.build();
			
			Scheduler scheduler = SchedulerServerContext.getSchedulerInstance();
			scheduler.scheduleJob(jobDetail, trigger);
		}
	}

	public static void removeScheduledReports(String spaceID) throws SQLException, SchedulerBaseException, ClassNotFoundException
	{
		Connection conn = null;
		try
		{
			conn = SchedulerDBConnection.getInstance().getConnectionPool().getConnection();
			SchedulerDatabase.removeScheduledReports(conn, UtilConstants.SCHEDULER_DB_SCHEMA, spaceID);
		}
		finally
		{
			try
			{
				conn.close();
			}catch(Exception ex)
			{
				logger.warn("Error in closing db connection ", ex);
			}
		}
	}	
	
	public static Map<String, String> getSpaceInfoMap(String spaceID) throws SQLException, SchedulerBaseException, ClassNotFoundException
	{
		Map<String,String> response = null;
		Connection conn = null;
		try
		{
			conn = AcornAdminDBConnection.getInstance().getConnectionPool().getConnection();		
			response = AdminDatabase.getSpaceInfo(conn, AdminDatabase.ADMIN_DB_SCHEMA, spaceID);
			return response;
		}
		finally
		{			
			if(conn != null)
			{
				conn.close();
			}
		}
	}
	
	public static String getSpaceDirectory(String spaceID) throws SQLException, SchedulerBaseException, ClassNotFoundException
	{
		Map<String,String> spaceInfoMap = getSpaceInfoMap(spaceID);
		if(spaceInfoMap != null && spaceInfoMap.size() > 0)
		{
			return spaceInfoMap.get(UtilConstants.KEY_SPACE_DIRECTORY);
		}
		
		return null;
	}
	 
/*	public static String getSpaceDirectory(String spaceID) throws SQLException, SchedulerBaseException, ClassNotFoundException
	{
		String spaceDir = null;
		Connection conn = null;
		try
		{
			conn = AcornAdminDBConnection.getInstance().getConnectionPool().getConnection();		
			Map<String, String> spaceInfo = AdminDatabase.getSpaceInfo(conn, AdminDatabase.ADMIN_DB_SCHEMA, spaceID);
			spaceDir = spaceInfo.get(UtilConstants.KEY_SPACE_DIRECTORY);
			
		}finally
		{			
			if(conn != null)
			{
				conn.close();
			}
		}
		
		return spaceDir;
	}*/
	
	/**
	 * Unschedules all the retries triggers, runNow triggers.
	 * 
	 * @param jobID
	 * @throws SchedulerNotRunningException
	 * @throws SchedulerException 
	 */
	public static void removeAllNonScheduledTriggers(JobKey jobKey) throws SchedulerNotRunningException, SchedulerException
	{	
		Scheduler scheduler = SchedulerServerContext.getSchedulerInstance();
		List<?extends Trigger> triggerList = scheduler.getTriggersOfJob(jobKey);
		if(triggerList != null)
		{
			for(Trigger trigger : triggerList)
			{
				TriggerKey triggerKey = trigger.getKey();
				String triggerName = triggerKey.getName();
				if(triggerName.startsWith(UtilConstants.PREFIX_RETRY) || triggerName.startsWith(UtilConstants.PREFIX_IMMEDIATE_TRIGGER))
				{
					// remove the trigger
					scheduler.unscheduleJob(triggerKey);
				}
			}
		}
	}

	public static boolean isPrimaryScheduler(Scheduler scheduler)
	{
		return SchedulerServerContext.getPrimaryScheduler() == scheduler;
	}
	
	public static String replaceSchedulerHomeValue(String str)
	{
		String replacedString = str;
		if(replacedString != null)
		{
			String schedulerInstallDir = System.getProperty(UtilConstants.SYSTEM_PROPERTY_HOME);
			logger.debug("System Property - Scheduler Home : " + schedulerInstallDir);
			// get the override file name and properties
			replacedString = replacedString.replace(SchedulerServerContext.SCHEDULER_INSTALL_DIR_TAG, schedulerInstallDir);
		}
		return replacedString;
	}

	public static boolean isAnyScheduleRunNow(Schedule[] schedules)
	{
		boolean anyScheduleToRunNow = false;
		if(schedules != null && schedules.length > 0)
		{
			for (Schedule schedule : schedules)
			{
				// if any schedule is to for immediate , just flag it. 
				if(schedule.isScheduleNow())
				{
					anyScheduleToRunNow = true;
					break;
				}
			}
		}
		return anyScheduleToRunNow;
	}
	
	public static void createSFDCExtractFile(String spaceID) throws SQLException, SchedulerBaseException, ClassNotFoundException, IOException
	{
		validateUIDFormat(spaceID);
		try
		{
			String spaceDirectory = Util.getSpaceDirectory(spaceID);
			createSFDCExtractFileInDirectory(spaceDirectory);
		}
		finally
		{}
	}
	
	public static void createBDSExtractFile(String spaceID) 
		throws SQLException, SchedulerBaseException, ClassNotFoundException, IOException {
		validateUIDFormat(spaceID);
		try {
			String spaceDirectory = Util.getSpaceDirectory(spaceID);
			createBDSExtractFileInDirectory(spaceDirectory);
		} finally {}
	}
	
	public static boolean isLoadLockFilePresent(String spaceDirectory)
	{	
		try
		{	
			File file = new File(getLoadLockFileName(spaceDirectory));
			return file.exists();
		}
		finally
		{}
	}
	
	public static boolean isConnectorSpecificExtractFilePresent(String spaceDirectory, String connectorName)
	{	
		try{	
			String fileName = Util.getConnectorExtractFileName(spaceDirectory, connectorName);
			if(fileName != null){
				File file = new File(fileName);
				return file.exists();
			}
			return false;
		}
		finally{}
	}
	
	public static boolean isSFDCExtractFilePresent(String spaceDirectory)
	{	
		try
		{	
			File file = new File(getSFDCExtractFileName(spaceDirectory));
			return file.exists();
		}
		finally
		{}
	}
	
	public static boolean isConnectorExtractFilePresent(String spaceDirectory, String connectorName) {
		try
		{	
			File file = new File(getConnectorExtractFileName(spaceDirectory, connectorName));
			return file.exists();
		}
		finally
		{}
	}
	
	public static boolean isPublishLockPresent(String spaceDirectory)
	{	
		try
		{	
			File file = new File(getPublishLockFileName(spaceDirectory));
			return file.exists();
		}
		finally
		{}
	}
	
	public static void validateNoLoadLockFilePresent(String spaceDirectory) throws ConcurrentSpaceOperationException
	{
		if(Util.isLoadLockFilePresent(spaceDirectory))
		{
			throw new ConcurrentSpaceOperationException("Space is loading. Load Lock file is found in the space : " + spaceDirectory);
		}
	}
	
	public static void validateNoExtractFilePresent(String spaceDirectory) throws ConcurrentSpaceOperationException
	{
		if(Util.isSFDCExtractFilePresent(spaceDirectory))
		{
			throw new ConcurrentSpaceOperationException("SFDC extract file is found in the space : " + spaceDirectory);
		}
	}
	
	public static void validateNoPublishLockFilePresent(String spaceDirectory) throws ConcurrentSpaceOperationException
	{
		if(Util.isPublishLockPresent(spaceDirectory))
		{
			throw new ConcurrentSpaceOperationException("Publish lock file is found in the space : " + spaceDirectory);
		}
	}
	
	
	public static void createConnectorSpecificExtractFile(String connectorName, String spaceDirectory) throws SQLException, SchedulerBaseException, ClassNotFoundException, IOException
	{
		String configDirPath = spaceDirectory + File.separator + "connectors";
		File configDir = new File(configDirPath);
		if (!configDir.exists() || !configDir.isDirectory())
			configDir.mkdir();
		
		String fileName = Util.getConnectorExtractFileName(spaceDirectory, connectorName);
		if(fileName != null){
			File file = new File(fileName);
			if(!file.exists()){
				file.createNewFile();
			}
		}
	}
	
	public static void createSFDCExtractFileInDirectory(String spaceDirectory) throws SQLException, SchedulerBaseException, ClassNotFoundException, IOException
	{
		try
		{	
			File file = new File(getSFDCExtractFileName(spaceDirectory));
			if(!file.exists())
			{
				file.createNewFile();
			}
		}
		finally
		{
			
		}
	}
	
	public static void createConnectorExtractFileInDirectory(String spaceDirectory, String connectorName) throws SQLException, SchedulerBaseException, ClassNotFoundException, IOException
	{
		try
		{	
			File file = new File(getConnectorExtractFileName(spaceDirectory, connectorName));
			if(!file.exists())
			{
				file.createNewFile();
			}
		}
		finally
		{
			
		}
	}
	
	public static void createBDSExtractFileInDirectory(String spaceDirectory) 
		throws SQLException, SchedulerBaseException, ClassNotFoundException, IOException {
		try {	
			File file = new File(getBDSExtractFileName(spaceDirectory));
			if(!file.exists())
			{
				file.createNewFile();
			}
		} finally {}
	}
	
	public static String getPublishLockFileName(String spaceDirectory)
	{
		return spaceDirectory + File.separator + UtilConstants.PUBLISH_LOCK_FILE; 
	}
	
	public static String getSFDCExtractFileName(String spaceDirectory)
	{
		return spaceDirectory + File.separator + UtilConstants.SFDC_EXTRACT_FILE; 
	}
	
	public static String getConnectorExtractFileName(String spaceDirectory, String connectorName)
	{
		String fileName = null; 
		if(connectorName.equals(SFDCInfo.TYPE_NETSUITE)){
			fileName = UtilConstants.CONNECTOR_NETSUITE_EXTRACT_FILE;
		}
		else if(connectorName.equals(SFDCInfo.TYPE_SFDC)){
			fileName = UtilConstants.CONNECTOR_SFDC_EXTRACT_FILE;
		}
		else if (connectorName.endsWith(UtilConstants.CONNECTOR_SCHEDULE_TYPE_SUFFIX)) {
			fileName = connectorName.substring(0, connectorName.indexOf(UtilConstants.CONNECTOR_SCHEDULE_TYPE_SUFFIX));
			if (fileName.equals(SFDCInfo.TYPE_SFDC))
				fileName = UtilConstants.CONNECTOR_SFDC_EXTRACT_FILE;
			else
				fileName = fileName.substring(0, 1).toUpperCase() + fileName.substring(1) + UtilConstants.CONNECTOR_EXTRACT_FILE_SUFFIX;
		}			
		if(fileName == null){
			return null;
		}
		return spaceDirectory + File.separator + "connectors" + File.separator + fileName; 
	}
	
	public static String getBDSExtractFileName(String spaceDirectory)
	{
		return spaceDirectory + File.separator + UtilConstants.BDS_EXTRACT_FILE; 
	}
	
	public static String getLoadLockFileName(String spaceDirectory)
	{
		return spaceDirectory + File.separator + UtilConstants.LOAD_LOCK_FILE; 
	}
	
	/**
	 * Can be used to validate various Guids
	 * @param uId
	 */
	public static void validateUIDFormat(String uId)
	{
		UUID.fromString(uId);
	}
	
	public static void deleteSFDCExtractFile(String spaceID) throws SQLException, SchedulerBaseException, ClassNotFoundException
	{
		validateUIDFormat(spaceID);
		try
		{
			String spaceDirectory = Util.getSpaceDirectory(spaceID);
			deleteSFDCExtractFileFromDirectory(spaceDirectory);
		}
		finally
		{}
	}
	
	public static void deleteConnectorSpecificFile(String connectorName, String spaceDirectory) throws SQLException, SchedulerBaseException, ClassNotFoundException
	{
		String fileName = Util.getConnectorExtractFileName(spaceDirectory, connectorName);
		if(fileName != null){
			File file = new File(fileName);
			if(file.exists()){
				file.delete();
			}
		}
	}
	
	public static void deleteSFDCExtractFileFromDirectory(String spaceDirectory) throws SQLException, SchedulerBaseException, ClassNotFoundException
	{
		try
		{	
			File file = new File(getSFDCExtractFileName(spaceDirectory));
			if(file.exists())
			{
				file.delete();
			}
		}
		finally
		{
			
		}
	}
	
	public static void deleteConnectorExtractFileFromDirectory(String spaceDirectory, String connectorName) throws SQLException, SchedulerBaseException, ClassNotFoundException
	{
		try
		{	
			File file = new File(getConnectorExtractFileName(spaceDirectory, connectorName));
			if(file.exists())
			{
				file.delete();
			}
		}
		finally
		{
			
		}
	}
	
	public static void deleteBDSExtractFileFromDirectory(String spaceDirectory) throws SQLException, SchedulerBaseException, ClassNotFoundException
	{
		try
		{	
			File file = new File(getBDSExtractFileName(spaceDirectory));
			if(file.exists())
			{
				file.delete();
			}
		}
		finally
		{
			
		}
	}

	public static boolean useSSLForEmail()
	{
		// TODO Auto-generated method stub
		return false;
	}

	public static boolean successExtractFileExists(String uid, String spaceDirectory)
	{
		try
		{	
			File file = new File(spaceDirectory + File.separator + "slogs" + File.separator + "success-extract-" + uid + ".txt");
			return file.exists();
		}
		catch(Exception ex)
		{
			logger.error(ex, ex);
		}
		
		return false;
	}
	
	public static boolean successConnectorExtractFileExists(String spaceDirectory, String connectorName, String uid)
	{
		try
		{	
			File file = new File(spaceDirectory + File.separator + "connectors" + File.separator + connectorName + "Status-success-extract-" + uid + ".txt");
			return file.exists();
		}
		catch(Exception ex)
		{
			logger.error(ex, ex);
		}
		
		return false;
	}
	
	public static boolean failedConnectorExtractFileExists(String spaceDirectory, String connectorName)
	{
		try
		{	
			File file = new File(spaceDirectory + File.separator + "slogs" + File.separator + "failed-extract-" + connectorName + ".txt");
			return file.exists();
		}
		catch(Exception ex)
		{
			logger.error(ex, ex);
		}
		
		return false;
	}
	
	public static boolean killedConnectorExtractFileExists(String spaceDirectory, String connectorName)
	{
		try
		{	
			File file = new File(spaceDirectory + File.separator + "slogs" + File.separator + "killed-extract-" + connectorName + ".txt");
			return file.exists();
		}
		catch(Exception ex)
		{
			logger.error(ex, ex);
		}
		
		return false;
	}
	
	public static boolean failedExtractFileExists(String uid, String spaceDirectory)
	{
		try
		{	
			File file = new File(spaceDirectory + File.separator + "slogs" + File.separator + "failed-extract-" + uid + ".txt");
			return file.exists();
		}
		catch(Exception ex)
		{
			logger.error(ex, ex);
		}
		
		return false;
	}
	
	public static boolean killedExtractFileExists(String uid, String spaceDirectory)
	{
		try
		{	
			File file = new File(spaceDirectory + File.separator + "slogs" + File.separator + "killed-extract-" + uid + ".txt");
			return file.exists();
		}
		catch(Exception ex)
		{
			logger.error(ex, ex);
		}
		
		return false;
	}
	
/**
 * Reschedules the schedules for a given jobGroupName.
 * e.g. if SFDC is scheduled at 5 times, all of them gets rescheduled
 * except the one which is defined by the skipThisTrigger (which is generally the calling job trigger run).
 * This takes care of the scenario where extract finished at let's say 6:50 am and another one was scheduled at 
 * 6:49 am. There is no need to run 6:49 am extract now and it should be rescheduled to next day or next week 6:49 pm.
 * @param spaceID
 * @param jobGroupName
 * @param skipThisTrigger
 */
	@SuppressWarnings("unchecked")
	public static void rescheduleTriggersInJobGroup(String spaceID,
			String jobGroupName, Trigger skipThisTrigger)
	{
		try
		{
			TriggerKey skipTriggerKey = skipThisTrigger.getKey();
			String skipTriggerName = skipTriggerKey.getName();
			String skipTriggerGroup = skipTriggerKey.getGroup();
			
			Scheduler scheduler = SchedulerServerContext.getPrimaryScheduler();
			Set<JobKey> jobKeys = scheduler.getJobKeys(GroupMatcher.groupEquals(jobGroupName));
			if(jobKeys != null && jobKeys.size() > 0)
			{
				for(JobKey jobKey : jobKeys)
				{
					List<? extends Trigger> triggerList = scheduler.getTriggersOfJob(jobKey);
					if(triggerList != null && triggerList.size() > 0)
					{
						for(Trigger trigger : triggerList)
						{
							if(trigger instanceof CronTrigger)
							{
								TriggerKey triggerKey = trigger.getKey();
								String triggerName = triggerKey.getName();
								String triggerGroup = triggerKey.getGroup();
								if(triggerName.equalsIgnoreCase(skipTriggerName) &&
										triggerGroup.equalsIgnoreCase(skipTriggerGroup))
								{
									continue;
								}
								
								
								if(trigger.getNextFireTime().getTime() - System.currentTimeMillis() < 0)
								{
									// means the next fire time is already passed but this has not been
									// picked up by the scheduler (lack of resources). These triggers will need to be reschduled
									// so that they do not unnecessarily get picked up
									TriggerBuilder<CronTrigger> newCronTriggerBuilder = ((CronTrigger) trigger).getTriggerBuilder();
									CronTrigger newCronTrigger = newCronTriggerBuilder.startNow().build();
									scheduler.rescheduleJob(triggerKey, newCronTrigger);
								}
							}
						}
					}					
				}
			}			
		}
		catch(Exception ex)
		{
			logger.error("Error in rescheduling other schedules for spaceID : " + spaceID + " : groupName : " + jobGroupName, ex);
		}
		
	}

	private static String getFormattedPackageSpacePropertiesString(PackageSpaceProperties packageSpaceProperties)
	{
		if(packageSpaceProperties == null)
		{
			return null;
		}
		StringBuilder sb = new StringBuilder();
		sb.append("packageID=" + packageSpaceProperties.getPackageID());
		sb.append(",");
		sb.append("spaceID=" + packageSpaceProperties.getSpaceID());
		sb.append(",");
		sb.append("spaceDirectory=" + packageSpaceProperties.getSpaceDirectory());
		sb.append(",");
		sb.append("spaceName=" + packageSpaceProperties.getSpaceName());
		return sb.toString();
	}
	
	public static List<String> getPackageSpacePropertiesArray(String spaceID)
	{
		List<String> response = new ArrayList<String>();
		Connection conn = null;
		String schema = AdminDatabase.ADMIN_DB_SCHEMA;
		try
		{		
			conn = AcornAdminDBConnection.getInstance().getConnectionPool().getConnection();
			List<PackageSpaceProperties> packageSpacePropertiesList = AdminDatabase.getPackageSpaceProperties(conn, schema, spaceID);
			if(packageSpacePropertiesList != null && packageSpacePropertiesList.size() > 0)
			{
				for(PackageSpaceProperties packageSpaceProperties : packageSpacePropertiesList)
				{
					String formattedString = getFormattedPackageSpacePropertiesString(packageSpaceProperties);
					if(formattedString != null && formattedString.trim().length() > 0)
					{
						response.add(formattedString);
					}
				}
			}
		}
		catch(Exception ex){
			logger.warn("Error in getting package space info for child space " + spaceID, ex);
		}
		finally
		{
			if(conn != null)
			{
				try
				{
					conn.close();
				}catch(Exception ex2){
					logger.warn("Error in closing connection : getPackageSpacePropertiesArray", ex2);
				}
			}
		}
		return response.size() > 0 ? response : null;
	}
	
	public static Scheduler getSelectedScheduler(String spaceID, String jobID) 
		throws ClassNotFoundException, SQLException, SchedulerBaseException, SchedulerException 
	{
		SchedulerVersion schedulerVersion = getSelectedSchedulerVersion(spaceID, jobID);
		return (schedulerVersion != null) ? schedulerVersion.getScheduler() : null;
	}
	
	public static SchedulerVersion getSelectedSchedulerVersion(String spaceID, String jobID) 
		throws ClassNotFoundException, SQLException, SchedulerBaseException, SchedulerException
	{
		SchedulerVersion selectedSchedulerVersion = null;		
		Map<String,SchedulerVersion> schedulersMap = SchedulerServerContext.getAllSchedulers();
		for(Entry<String,SchedulerVersion> entry : schedulersMap.entrySet())
		{
			SchedulerVersion schedulerVersion = entry.getValue();
			Scheduler scheduler = schedulerVersion.getScheduler();
			List<String> jobIDs = getSchedulerJobIDs(scheduler, spaceID);
			if (jobIDs != null && jobIDs.size() > 0 && jobIDs.contains(jobID)) {
				selectedSchedulerVersion = schedulerVersion;
				break;
			}
		}
		return selectedSchedulerVersion;
	}
	
	@SuppressWarnings("unchecked")
	public static List<String> getSchedulerJobIDs(Scheduler scheduler, String spaceID) 
		throws SchedulerException 
	{
		List<String> jobIDs = new ArrayList<String>();
		Set<JobKey> jobKeys = new HashSet<JobKey>();
		String findGroupName = Util.getCustJobGroupNameForSFDC(spaceID);
		jobKeys = scheduler.getJobKeys(GroupMatcher.groupEquals(findGroupName));
		if (jobKeys == null || jobKeys.size() == 0) {
			logger.warn("No Job ID found for space : " + spaceID);
			return jobIDs;
		}
		for(JobKey jobKey : jobKeys)
		{
			String sfdcJobID = Util.getObjectID(jobKey.getName(), UtilConstants.PREFIX_CONNECTORS_JOB);
			jobIDs.add(sfdcJobID);
		}
		return jobIDs;
	}
	
	public static Map<Integer, String> parseCronExpression(String cronExpression) {
		Map<Integer, String> cronParts = new HashMap<Integer, String>();
		String[] parts = cronExpression.split(" ");
		int pos = 0;
		for (String part : parts) {
			cronParts.put(pos, part);
			pos++;
		}
		return cronParts;
	}
}
