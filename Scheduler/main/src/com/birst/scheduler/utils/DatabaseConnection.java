package com.birst.scheduler.utils;

public class DatabaseConnection
{
	public static final String MSSQL_DRIVER_NAME = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
	public static final String ORACLE_DRIVER_NAME = "oracle.jdbc.OracleDriver";
	private DatabaseType DBType;
	
	public enum DatabaseType
	{
		MSSQL, MYSQL, Infobright, Oracle, ParAccel
	}
	
	public static DatabaseType getDBTypeFromDriver(String driver)
	{
		if (driver.equals("com.microsoft.sqlserver.jdbc.SQLServerDriver"))
		{
			return DatabaseType.MSSQL;
		} else if (driver.equals("com.paraccel.Driver"))
		{
			return DatabaseType.ParAccel;
		} else if (driver.equals("oracle.jdbc.OracleDriver"))
		{
			return DatabaseType.Oracle;
		} else
		{
			return DatabaseType.MSSQL;
		}
	}
	
	public void setDBTypeFromDriver(String driver)
	{
		DBType = getDBTypeFromDriver(driver);
	}
	
	public  void setDBType(String type)
	{
		if (type.equals("MySQL"))
		{
			DBType = DatabaseType.MYSQL;
		} else if (type.equals("MS SQL Server"))
		{
			DBType = DatabaseType.MSSQL;
		} else if (type.equals("Infobright"))
		{
			DBType = DatabaseType.Infobright;
		} else if (type.equals("Oracle"))
		{
			DBType = DatabaseType.Oracle;
		} else if (type.equals("ParAccel"))
		{
			DBType = DatabaseType.ParAccel;		
		} else
		{
			DBType = DatabaseType.MSSQL;
		}
	}
	
	public DatabaseType getDBType()
	{
		return DBType;
	}
	
	public boolean isDBTypeOracle()
	{
		return DBType == DatabaseType.Oracle;
	}
	
	public boolean isDBTypeMSSQL()
	{
		return DBType == DatabaseType.MSSQL;
	}
	
	public boolean isDBTypeParAccel()
	{
		return DBType == DatabaseType.ParAccel;
	}

	public boolean isDBTypeMySQL()
	{
		return DBType == DatabaseType.MYSQL;
	}
	
	public String getCurrentTimestamp()
	{
		if (DBType == DatabaseType.Infobright || DBType == DatabaseType.MYSQL)
			return "current_timestamp()";
		else if (DBType == DatabaseType.Oracle)
			return "current_timestamp";
		return "getdate()";
	}
	
	public String getBigIntType()
	{
		if (DBType == DatabaseType.Oracle)
			return "NUMBER(19)";
		return "BIGINT";
	}
	
	public String getUniqueIdentiferType()
    {
        if (isDBTypeOracle())
            return "Varchar2(36)";
        return "UniqueIdentifier";
    }
	
	public String getBooleanType(String column)
    {
        if (isDBTypeOracle())
            return "NUMBER(1) CHECK (" + column + " IN ( 0, 1 ))";
        return "bit";
    }
	
	public String getDateTimeType()
	{
		if (DBType == DatabaseType.Oracle || DBType == DatabaseType.ParAccel)
			return "TIMESTAMP";
		return "DATETIME";
	}
	
	public boolean supportsLimit()
	{
		if (DBType == DatabaseType.Infobright || isDBTypeMySQL() || isDBTypeParAccel())
			return true;
		return false;
	}

	public boolean supportsRowNum()
	{
		if (isDBTypeOracle())
			return true;
		return false;
	}

	public boolean supportsTop()
	{
		if (isDBTypeMSSQL())
			return true;
		return false;
	}
	
	public String getLargeTextType()
	{
		if (DBType == DatabaseType.Infobright || DBType == DatabaseType.MYSQL)
			return "TEXT";
		else if (isDBTypeOracle()) 
		{
			return "VARCHAR2(4000)";
		}
		else if (DBType == DatabaseType.ParAccel)
		{
			return "VARCHAR(4096)";
		}
		return "VARCHAR(MAX)";
	}
	
	public String getVarcharType(int width, boolean useMax)
	{
		if (useMax)
		{
			return getLargeTextType();
		} else
		{
			if (DBType == DatabaseType.ParAccel)
			{
				return "VARCHAR(" + 2 * width + ")";
			}
			if (DBType == DatabaseType.Oracle)
			{
				width = width > 4000 ? 4000 : width;
				return "VARCHAR2(" + width + ")";
			}
			return "VARCHAR(" + width + ")";
		}
	}
	
	public String getNVarcharType(int width, boolean useMax)
	{
		if (useMax)
		{
			return getLargeTextType();
		} else
		{
			if (DBType == DatabaseType.ParAccel)
			{
				return "nvarchar(" + 2 * width + ")";
			}
			if (DBType == DatabaseType.Oracle)
			{
				width = width > 2000 ? 2000 : width;
				return "nvarchar2(" + width + ")";
			}
			return "nvarchar(" + width + ")";
		}
	}
}
