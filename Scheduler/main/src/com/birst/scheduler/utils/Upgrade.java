package com.birst.scheduler.utils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.birst.scheduler.salesforce.SFDCInfo;

public class Upgrade
{
	public static final int TO_BE_VERSION = 8;
	private static final Logger logger = Logger.getLogger(Upgrade.class);
	
	public static void UpgradeDatabaseSchema(Connection conn, String schema) throws SQLException
	{
		if(SchedulerDatabase.tableExists(conn, schema, SchedulerDatabase.CUST_VERSIONS_TABLENAME))
		{
			int currentVersion = SchedulerDatabase.getCurrentVersion(conn, schema);
		
			if(currentVersion < TO_BE_VERSION)
			{
				logger.info("Starting Upgrade from version " + currentVersion + " to " + TO_BE_VERSION);
				if(currentVersion < 1)
				{
					upgradeTo1(conn, schema);
				}
				if(currentVersion < 2)
				{
					upgradeTo2(conn, schema);
				}
				if(currentVersion < 3)
				{
					upgradeTo3(conn, schema);
				}				
				if(currentVersion < 4)
				{
					upgradeTo4(conn, schema);
				}				
				if(currentVersion < 5)
				{
					upgradeTo5(conn, schema);
				}
				if(currentVersion < 6)
				{
					upgradeTo6(conn, schema);
				}
				if(currentVersion < 7)
				{
					upgradeTo7(conn, schema);
				}
				if(currentVersion < 8)
				{
					upgradeTo8(conn, schema);
				}
				SchedulerDatabase.updateVersion(conn, schema, TO_BE_VERSION);
				logger.info("Finished Upgrade from version " + currentVersion + " to " + TO_BE_VERSION);
			}
		}
	}
	
	private static void upgradeTo1(Connection conn, String schema)
	{
		try
		{
			DatabaseConnection dConn = SchedulerDBConnection.getInstance().getConnectionPool().getDBConnection();
			
			List<String> columnNames = new ArrayList<String>();
			List<String> columnTypes = new ArrayList<String>();	
			List<Integer> actions = new ArrayList<Integer>();
			
			columnNames.add("CsvSeparator");
			columnTypes.add(dConn.getVarcharType(10, false));
			actions.add(SchedulerDatabase.ALTER_ADD);
			
			columnNames.add("CsvExtension");
			columnTypes.add(dConn.getVarcharType(20, false));
			actions.add(SchedulerDatabase.ALTER_ADD);
			
			columnNames.add("Prompts");
			columnTypes.add(dConn.getVarcharType(-1 , true));
			actions.add(SchedulerDatabase.ALTER_ADD);
			
			columnNames.add("CreatedAdminMode");
			columnTypes.add(dConn.getBooleanType("CreatedAdminMode"));
			actions.add(SchedulerDatabase.ALTER_ADD);
			
			columnNames.add("Comments");
			columnTypes.add(dConn.getVarcharType(2048, false));
			actions.add(SchedulerDatabase.ALTER_ADD);
			
			columnNames.add("CreatedDate");
			columnTypes.add("DateTime");
			actions.add(SchedulerDatabase.ALTER_ADD);
			
			columnNames.add("ModifiedDate");
			columnTypes.add(dConn.getDateTimeType());
			actions.add(SchedulerDatabase.ALTER_ADD);
			
			columnNames.add("ModifiedBy");
			columnTypes.add(dConn.getVarcharType(512, false));
			actions.add(SchedulerDatabase.ALTER_ADD);
			
			SchedulerDatabase.alterTable(conn, schema, SchedulerDatabase.SCHEDULED_REPORTS_TABLENAME, columnNames, columnTypes, actions);
		}
		catch(Exception ex)
		{
			logger.error("Exception while upgrading to 1 " ,ex);
		}
	}
	
	private static void upgradeTo2(Connection conn, String schema)
	{
		try
		{
			DatabaseConnection dConn = SchedulerDBConnection.getInstance().getConnectionPool().getDBConnection();
			
			List<String> columnNames = new ArrayList<String>();
			List<String> columnTypes = new ArrayList<String>();	
			List<Integer> actions = new ArrayList<Integer>();
			
			
			columnNames.add("Type");
			columnTypes.add(dConn.getVarcharType(128, false));
			actions.add(SchedulerDatabase.ALTER_COLUMN);
			
			SchedulerDatabase.alterTable(conn, schema, SchedulerDatabase.SCHEDULED_REPORTS_TABLENAME, columnNames, columnTypes, actions);
		}
		catch(Exception ex)
		{
			logger.error("Exception while upgrading to 2 " ,ex);
		}
	}
	
	private static void upgradeTo3(Connection conn, String schema)
	{
		try
		{
			DatabaseConnection dConn = SchedulerDBConnection.getInstance().getConnectionPool().getDBConnection();
			
			List<String> columnNames = new ArrayList<String>();
			List<String> columnTypes = new ArrayList<String>();	
			List<Integer> actions = new ArrayList<Integer>();
			
			
			columnNames.add("StartDate");
			columnTypes.add(dConn.getVarcharType(30, false));
			actions.add(SchedulerDatabase.ALTER_ADD);
			
			SchedulerDatabase.alterTable(conn, schema, SchedulerDatabase.SCHEDULE_INFO_TABLENAME, columnNames, columnTypes, actions);
		}
		catch(Exception ex)
		{
			logger.error("Exception while upgrading to 3 " ,ex);
		}
	}
	
	private static void upgradeTo4(Connection conn, String schema)
	{
		try
		{
			DatabaseConnection dConn = SchedulerDBConnection.getInstance().getConnectionPool().getDBConnection();
			
			List<String> columnNames = new ArrayList<String>();
			List<String> columnTypes = new ArrayList<String>();	
			List<Integer> actions = new ArrayList<Integer>();
			
			
			columnNames.add(SchedulerDBConstants.COL_VARIABLES);
			columnTypes.add(dConn.getVarcharType(-1, true));			
			actions.add(SchedulerDatabase.ALTER_ADD);
			
			SchedulerDatabase.alterTable(conn, schema, SchedulerDatabase.SCHEDULED_REPORTS_TABLENAME, columnNames, columnTypes, actions);
		}
		catch(Exception ex)
		{
			logger.error("Exception while upgrading to 4 " ,ex);
		}
	}
	
	private static void upgradeTo5(Connection conn, String schema)
	{
		try
		{
			DatabaseConnection dConn = SchedulerDBConnection.getInstance().getConnectionPool().getDBConnection();
			
			List<String> columnNames = new ArrayList<String>();
			List<String> columnTypes = new ArrayList<String>();	
			List<Integer> actions = new ArrayList<Integer>();
			
			
			columnNames.add(SchedulerDBConstants.COL_CONN_SCHED_NAME);
			columnTypes.add(dConn.getVarcharType(256, false));			
			actions.add(SchedulerDatabase.ALTER_ADD);
			
			columnNames.add(SchedulerDBConstants.COL_CONN_TYPE);
			columnTypes.add(dConn.getVarcharType(128, false));			
			actions.add(SchedulerDatabase.ALTER_ADD);
			
			columnNames.add(SchedulerDBConstants.COL_CONN_ENGINE);
			columnTypes.add("int");			
			actions.add(SchedulerDatabase.ALTER_ADD);
			
			SchedulerDatabase.alterTable(conn, schema, SchedulerDBConstants.CUST_CONNECTORS_INFO_TABLENAME, columnNames, columnTypes, actions);
			
			PreparedStatement pstmt = null;
			try
			{
				String sql = "UPDATE " + SchedulerDatabase.getQualifiedTableName(schema, SchedulerDBConstants.CUST_CONNECTORS_INFO_TABLENAME) +
				" SET " + SchedulerDBConstants.COL_CONN_TYPE + "=" + SFDCInfo.TYPE_SFDC;
				
				pstmt = conn.prepareStatement(sql);
				pstmt.executeUpdate();
			}
			finally
			{
				try
				{
					if(pstmt != null)
					{
						pstmt.close();
					}
				}catch(Exception ex)
				{
					logger.warn("Error in closing statement", ex);
				}
			}
		}
		catch(Exception ex)
		{
			logger.error("Exception while upgrading to 5 " ,ex);
		}
	}
	
	private static void upgradeTo6(Connection conn, String schema)
	{
		try
		{
			DatabaseConnection dConn = SchedulerDBConnection.getInstance().getConnectionPool().getDBConnection();
			
			List<String> columnNames = new ArrayList<String>();
			List<String> columnTypes = new ArrayList<String>();	
			List<Integer> actions = new ArrayList<Integer>();
			
			
			columnNames.add(SchedulerDBConstants.COL_EXTRACTGROUPS);
			columnTypes.add(dConn.getVarcharType(-1, true));			
			actions.add(SchedulerDatabase.ALTER_ADD);
			
			SchedulerDatabase.alterTable(conn, schema, SchedulerDBConstants.CUST_CONNECTORS_INFO_TABLENAME, columnNames, columnTypes, actions);
		}
		catch(Exception ex)
		{
			logger.error("Exception while upgrading to 6 " ,ex);
		}
	}
	
	private static void upgradeTo7(Connection conn, String schema)
	{
		try
		{
			DatabaseConnection dConn = SchedulerDBConnection.getInstance().getConnectionPool().getDBConnection();
			
			List<String> columnNames = new ArrayList<String>();
			List<String> columnTypes = new ArrayList<String>();	
			List<Integer> actions = new ArrayList<Integer>();
			
			
			columnNames.add("Name");
			columnTypes.add(dConn.getNVarcharType(255, false));
			actions.add(SchedulerDatabase.ALTER_COLUMN);
			
			columnNames.add("ReportPath");
			columnTypes.add(dConn.getNVarcharType(255, false));
			actions.add(SchedulerDatabase.ALTER_COLUMN);
			
			columnNames.add("FileName");
			columnTypes.add(dConn.getNVarcharType(255, false));
			actions.add(SchedulerDatabase.ALTER_COLUMN);
			
			
			SchedulerDatabase.alterTable(conn, schema, SchedulerDatabase.SCHEDULED_REPORTS_TABLENAME, columnNames, columnTypes, actions);
		}
		catch(Exception ex)
		{
			logger.error("Exception while upgrading to 7 " ,ex);
		}
	}
	
	private static void upgradeTo8(Connection conn, String schema)
	{
		try
		{
			DatabaseConnection dConn = SchedulerDBConnection.getInstance().getConnectionPool().getDBConnection();
			
			List<String> columnNames = new ArrayList<String>();
			List<String> columnTypes = new ArrayList<String>();	
			List<Integer> actions = new ArrayList<Integer>();
			
			
			columnNames.add("TriggerReportPath");
			columnTypes.add(dConn.getNVarcharType(255, false));
			actions.add(SchedulerDatabase.ALTER_COLUMN);

			SchedulerDatabase.alterTable(conn, schema, SchedulerDatabase.SCHEDULED_REPORTS_TABLENAME, columnNames, columnTypes, actions);
		}
		catch(Exception ex)
		{
			logger.error("Exception while upgrading to 8 " ,ex);
		}
	}
	
}
