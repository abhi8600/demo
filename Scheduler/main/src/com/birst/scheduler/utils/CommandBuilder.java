package com.birst.scheduler.utils;

import java.util.List;
import java.util.UUID;

import com.birst.scheduler.SchedulerServerContext;

public class CommandBuilder
{
	private StringBuilder builder;
	
	public CommandBuilder()
	{
		builder = new StringBuilder();
	}
	
	public static CommandBuilder getCommandBuilder()
	{
		return new CommandBuilder();
	}
	
	public String getCommand()
	{
		return builder.toString();
	}
	
	public void addCommandAction(String commandAction)
	{
		addToBuilder("cmdAction=" + commandAction);
	}
	
	
	public void addUid(String uid)
	{
		addToBuilder("uid=" + uid);
	}
		
	public void addLogFile(String logfile)
	{
		addToBuilder("logfile=" + logfile);
	}
	
	public void addDatabaseInfo(String dbserver, String dbName, String dbuser, String dbpwd)
	{
		addToBuilder("dbserver=" + dbserver);
		addToBuilder("dbName=" + dbName);
		addToBuilder("dbuser=" + dbuser);
		addToBuilder("dbpwd=" + dbpwd);
	}

	public void addAcornWebConfigDirectory(String dir)
	{
		addToBuilder("webConfigDir=" + dir);
	}
	
	
	public void addLiveAccessLoadGroups(List<String> liveAccessLoadGroups)
	{
		StringBuilder sb = new StringBuilder();
		if(liveAccessLoadGroups != null && liveAccessLoadGroups.size() > 0)
		{
			boolean first = true;
			for(String liveAccessLoadGroup : liveAccessLoadGroups)
			{	
				if(!first)
				{
					sb.append(";");
				}
				sb.append(liveAccessLoadGroup);
				if(first)
				{
					first = false;
				}
			}
		}
		add("liveAccessLoadGroups=" + sb.toString());
	}
	
	public void addFailureType(String failureType)
	{
		add("failureType=" + failureType);
	}
	
	public void addFailureDetail(String failureDetail)
	{
		add("failureDetail=" + failureDetail);
	}
	
	public void addUseSSL(boolean useSSL)
	{
		add("useSSL=" + useSSL);
	}
	
	public void addSpaceID(String spaceID)
	{
		add("spaceID=" + spaceID);
	}
	
	public void addUserID(String userID)
	{
		add("userID=" + userID);
	}
	
	public void addLoadGroup(String loadGroup)
	{
		add("loadGroup=" + loadGroup);
	}
	
	public void addLoadDate(String loadDate)
	{
		add("loadDate=" + loadDate);
	}
	
	public void addClientId(String clientId)
	{
		add("clientId=" + clientId);
	}
	
	public void addSandBoxUrl(String sandBoxUrl)
	{
		add("sandBoxUrl=" + sandBoxUrl);
	}
	
	public void addConnectorType(String type)
	{
		add("connectorType=" + type);
	}
	
	public void addConnectorConnectionString(String connectionString)
	{
		add("connectorConnectionString=" + connectionString);
	}
	
	public void addExtractGroups(String extractGroups)
	{
		if (extractGroups != null && extractGroups.trim().length() > 0)
		{
			add("extractGroups=" + getQuotedString(extractGroups));
		}
	}
	
	private String getQuotedString(String str)
	{
		String result = str;
		if (str != null && str.length() > 0)
		{
			result = str.trim();
			if (!result.startsWith("\"") && !result.endsWith("\""))
			{
				result = "\"" + result + "\"";
			}
		}
		return result;
	}
	
	public void addConnectorExtractionEngineUrl(int runNowExtractionEngine)
	{
		add("extractionEngine=" + runNowExtractionEngine);
	}
	
	public void addFileListPath(String fileListPath)
	{
		add("fileListPath=" + fileListPath);
	}
	
	public void addLoadNumber(int loadNumber)
	{
		add("loadNumber=" + loadNumber);
	}
	
	public void addDeleteAll(boolean deleteAll)
	{
		add("deleteAll=" + (deleteAll ? "true":"false"));
	}
	
	public void addRestoreRepository(boolean restoreRepository)
	{
		add("restoreRepository=" + (restoreRepository ? "true":"false"));
	}
	
	public void add(String str)
	{
		addToBuilder(str);
	}
	
	private void addToBuilder(String str)
	{		
		builder.append(str);
		builder.append(" ");
	}

	public void addEmailType(String emailType)
	{
		add("emailType=" + emailType);
	}

	public void addSubGroups(String subGroups)
	{
		add("subGroups=" + subGroups);
	}

	public void addEmailerInfo(boolean writeEmailInfo)
	{
		add("passEmailInfo=" + (writeEmailInfo ? "true":"false"));
	}

	public void addRetryFailed(boolean retryFailed)
	{
		add("retryFailed=" + (retryFailed ? "true" : "false"));
	}
	
	public void addNumberSFDCThreads(int numThreads)
	{
		add("numSFDCThreads=" + numThreads);
	}
	
	public void addNumberBDSThreads(int numThreads)
	{
		add("numBDSThreads=" + numThreads);
	}

	public void addAdminDbParams(String paramName, String commaSeparatedVals)
	{
		add("dbparam=" + paramName);
		add("dbparamValues=" + commaSeparatedVals);
	}
	
	public void addExtractOnly(boolean extractOnly)
	{
		add("extractionOnly=" + extractOnly);
	}
	
	public void addParentSource(String parentSource)
	{
		add("parentSource=" + parentSource);
	}
}
