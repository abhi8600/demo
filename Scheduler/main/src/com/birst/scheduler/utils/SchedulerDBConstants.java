package com.birst.scheduler.utils;

public class SchedulerDBConstants
{
	// List of all db constants ..table name column names etc
	
	public static final String CUST_SPACE_STATUS_TABLENAME = "CUST_SPACE_STATUS";
	// columns from SPACE_STATUS table
	public static final String COL_ID 			= "ID";
	public static final String COL_SPACE_ID 	= "SPACE_ID";
	public static final String COL_USER_ID 		= "USER_ID";
	public static final String COL_JOB_ID 		= "JOB_ID";
	public static final String COL_LOAD_GROUP 	= "LoadGroup";
	public static final String COL_LOAD_ID		= "LoadID";
	public static final String COL_STEP		 	= "Step";
	public static final String COL_STATUS		= "Status";
	public static final String COL_CHECKIN_TIME	= "CheckInTime";
	public static final String COL_CREATED_DATE = "CreatedDate";
	public static final String COL_MODIFIED_DATE 	= "ModifiedDate";
	public static final String COL_MODIFIED_BY 	= "ModifiedBy";
	public static final String COL_TERMINIATE	= "Terminate";
	public static final String COL_MESSAGE		= "Message";
	public static final String COL_DS_ID		= "DATA_SOURCE_ID";
	
	public static final String CUST_PROCESS_INFO_TABLENAME = "CUST_PROCESS_INFO";
	// columns
	// COL_ID; 
	// COL_SPACE_ID;
	// COL_LOAD_ID
	public static final String COL_LOAD_DATE = "LoadDate";
	//COL_LOAD_GROUP;
	public static final String COL_SUBGROUPS = "SubGroups";
	public static final String COL_EXTRACTGROUPS = "ExtractGroups";
	public static final String COL_RETRY_FAILED = "RetryFailed";
	public static final String COL_SEND_EMAIL = "SendEmail";
	public static final String COL_NAME = "Name";
	public static final String COL_SCHEDULE_TYPE = "Type";
	
	public static final String CUST_CONNECTORS_INFO_TABLENAME = "CUST_CONNECTORS_INFO";	
	public static final String CUST_BDS_INFO_TABLENAME = "CUST_BDS_INFO";
	
	// COL_ID
	// COL_SPACE_ID
	// COL_USER_ID
	public static final String COL_RUN_NOW_MODE = "RunNowMode";
	public static final String COL_CLIENT_ID = "ClientId";
	public static final String COL_SANBOX_URL = "SandBoxUrl";
	public static final String COL_PROCESS_AFTER = "Process";
	// COL_LOAD_DATE
	// COL_SUBGROUPS
	// COL_SEND_EMAIL
	// COL_CREATED_DATE
	// COL_MODIFIED_DATE
	// COL_MODIFIED_BY
	public static final String COL_CONN_SCHED_NAME = "Name";
	public static final String COL_CONN_TYPE = "Type";
	public static final String COL_CONN_ENGINE = "ExtractEngine"; // Only used for run now operation
	
	public static final String CUST_DELETE_INFO_TABLENAME = "CUST_DELETE_INFO";
	// COL_ID
	// COL_SPACE_ID
	// COL_USER_ID
	public static final String COL_DELETE_TYPE = "DeleteType"; 
	// COL_LOADGROUP
	public static final String COL_DELETE_RESTORE = "RestoreRepository";
	// COL_CREATED_DATE
	// COL_MODIFIED_DATE
	// COL_MODIFIED_BY
	
	// Column in SCHEDULED_REPORTS_TABLENAME table
	public static final String COL_VARIABLES = "Variables";
}
