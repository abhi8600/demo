package com.birst.scheduler.utils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.UUID;


import org.apache.log4j.Logger;

import com.birst.scheduler.exceptions.SchedulerBaseException;
import com.birst.scheduler.exceptions.ValidationException;
import com.birst.scheduler.shared.SpaceStatus;

public class StatusUtils
{
	private static final Logger logger = Logger.getLogger(StatusUtils.class);
	
	public static Map<String, Integer> stepsSequence = new HashMap<String, Integer>();
	
	public enum Step
	{
		Scheduled, ScheduledProcess, ScheduledExtract, ScheduledCopy, ScheduledDelete,
		SFDCEngineCmd, SFDCExtract, SFDCPostExtract, AppLoader, Process, PostProcess,
		BirstLocal, DeleteAll, DeleteLastPre, DeleteLast, DeleteLastPost, SendEmail, 
		BDS, BDSProcess, BDSUpload, BDSEngineCmd, BDSExtract, BDSPostExtract
	}
	
	public static int getStepNumber(String stepName)
	{
		if(stepsSequence == null || stepsSequence.size() == 0)
		{	
			stepsSequence.put(Step.ScheduledProcess.toString().toLowerCase(Locale.ENGLISH), 1);
			stepsSequence.put(Step.ScheduledExtract.toString().toLowerCase(Locale.ENGLISH), 2);
			stepsSequence.put(Step.ScheduledCopy.toString().toLowerCase(Locale.ENGLISH), 3);
			stepsSequence.put(Step.ScheduledDelete.toString().toLowerCase(Locale.ENGLISH), 4);
			stepsSequence.put(Step.SFDCEngineCmd.toString().toLowerCase(Locale.ENGLISH), 5);
			stepsSequence.put(Step.SFDCExtract.toString().toLowerCase(Locale.ENGLISH), 6);
			stepsSequence.put(Step.SFDCPostExtract.toString().toLowerCase(Locale.ENGLISH), 7);
			stepsSequence.put(Step.AppLoader.toString().toLowerCase(Locale.ENGLISH), 8);
			stepsSequence.put(Step.Process.toString().toLowerCase(Locale.ENGLISH), 9);
			stepsSequence.put(Step.PostProcess.toString().toLowerCase(Locale.ENGLISH), 10);
			stepsSequence.put(Step.BirstLocal.toString().toLowerCase(Locale.ENGLISH), 11);
			stepsSequence.put(Step.DeleteAll.toString().toLowerCase(Locale.ENGLISH), 12);
			stepsSequence.put(Step.DeleteLastPre.toString().toLowerCase(Locale.ENGLISH), 13);
			stepsSequence.put(Step.DeleteLast.toString().toLowerCase(Locale.ENGLISH), 14);
			stepsSequence.put(Step.DeleteLastPost.toString().toLowerCase(Locale.ENGLISH), 15);
			stepsSequence.put(Step.SendEmail.toString().toLowerCase(Locale.ENGLISH), 16);
			stepsSequence.put(Step.BDS.toString().toLowerCase(Locale.ENGLISH), 17);
			stepsSequence.put(Step.BDSProcess.toString().toLowerCase(Locale.ENGLISH), 18);
			stepsSequence.put(Step.BDSUpload.toString().toLowerCase(Locale.ENGLISH), 19);
			stepsSequence.put(Step.BDSEngineCmd.toString().toLowerCase(Locale.ENGLISH), 20);
			stepsSequence.put(Step.BDSExtract.toString().toLowerCase(Locale.ENGLISH), 21);
			stepsSequence.put(Step.BDSPostExtract.toString().toLowerCase(Locale.ENGLISH), 22);
		}
		
		return stepsSequence.get(stepName.toLowerCase(Locale.ENGLISH));
	}
	
	public static enum StepStatus
	{
		NONE(0),
		RUNNING(1), 
		SUCCESS(2), 
		FAILED(3),
		DEAD(4);
		
		private int status;
		StepStatus(int status)
		{
			this.status = status;
		}		
		
		public int getStatus()
		{
			return status;
		}		
	}
	
	
	public static void addSpaceStatus(SpaceStatus spaceStatus) throws SchedulerBaseException, ClassNotFoundException, SQLException
	{
		Connection conn = null;
		String schema = UtilConstants.SCHEDULER_DB_SCHEMA;
		try
		{			
			conn = SchedulerDBConnection.getInstance().getConnectionPool().getConnection();
			SchedulerDatabase.addSpaceStatus(conn, schema, spaceStatus);			
		} 
		finally
		{
			try
			{
				if (conn != null)
					conn.close();
			} catch (Exception ex)
			{
			}
		}
	}
	
	public void resetSpaceStatus(String spaceID, int loadId, String loadGroup) throws SQLException, SchedulerBaseException, ClassNotFoundException
	{
		Connection conn = null;
		String schema = UtilConstants.SCHEDULER_DB_SCHEMA;		
		try
		{			
			conn = SchedulerDBConnection.getInstance().getConnectionPool().getConnection();		
			SchedulerDatabase.deleteSpaceStatus(conn, schema, spaceID, loadId, loadGroup);
			
		} finally
		{
			try
			{
				if (conn != null)
					conn.close();
			} catch (Exception ex)
			{
			}
		}
	}

	public static String resetAndAddStatus(String spaceID, int loadID,
			String loadGroup, String newUid, String step, int status) throws Exception
	{
		Connection conn = null;
		String schema = UtilConstants.SCHEDULER_DB_SCHEMA;		
		try
		{			
			conn = SchedulerDBConnection.getInstance().getConnectionPool().getConnection();	
			conn.setAutoCommit(false);
			SchedulerDatabase.deleteSpaceStatus(conn, schema, spaceID, loadID, loadGroup);
			SpaceStatus spaceStatus = new SpaceStatus();
			spaceStatus.setId(newUid);
			spaceStatus.setSpaceID(spaceID);
			spaceStatus.setLoadGroup(loadGroup);
			spaceStatus.setLoadNumber(loadID);
			spaceStatus.setStep(step);
			spaceStatus.setStatus(status);
			
			SchedulerDatabase.addSpaceStatus(conn, schema, spaceStatus);
			conn.commit();
			return newUid;
		} 
		catch(Exception ex)
		{
			try
			{
				conn.rollback();
			}catch(Exception ex2){
				logger.error("Exception in rolling back ", ex2);
			}
			throw new Exception("Exception in resetAndAddStatus", ex);
		}
		finally
		{
			try
			{
				if (conn != null)
				{	
					conn.setAutoCommit(true);
					conn.close();
				}
			} catch (Exception ex)
			{
				logger.warn("Exception in closing connection", ex);
			}
		}
	}
	
	/**
	 * Just add the space status entry with the status done in the space status table.
	 * The actual status check for birst local group is done on the Acorn side
	 * @param props
	 * @param spaceID
	 * @param loadId
	 * @param loadGroup
	 * @throws SQLException
	 * @throws SchedulerBaseException
	 * @throws ClassNotFoundException
	 */
	public static void updateStatusForBirstLocal(Properties props, String runningJobId, String spaceID, 
			int loadId, String loadGroup) throws SQLException, SchedulerBaseException, ClassNotFoundException
	{
		if(props != null)
		{
			String birstLocalStarted = props.getProperty(CommandUtil.KEY_BIRST_LOCAL_STARTED);
			if(birstLocalStarted != null && birstLocalStarted.trim().length() > 0)
			{
				logger.debug("Birst Local group entry inserted for space : " + spaceID + " : loadID : " + loadId);
				String id = UUID.randomUUID().toString();
				StatusUtils.addSpaceStatus(id, spaceID, runningJobId, loadId, loadGroup, StatusUtils.Step.BirstLocal, StatusUtils.StepStatus.SUCCESS);
			}
		}
	}
	
	
	public static void addSpaceStatus(String uid, String spaceID, String jobID, 
			int loadId, String loadGroup, Step step, StepStatus stepStatus) throws SQLException, SchedulerBaseException, ClassNotFoundException
	{		
		Connection conn = null;
		String schema = UtilConstants.SCHEDULER_DB_SCHEMA;		
		try
		{			
			conn = SchedulerDBConnection.getInstance().getConnectionPool().getConnection();
			SpaceStatus spaceStatus = new SpaceStatus(uid, spaceID, jobID, loadId, loadGroup, step, stepStatus);
			SchedulerDatabase.addSpaceStatus(conn, schema, spaceStatus);
		}
		finally
		{
			try
			{
				if (conn != null)
				{	
					conn.close();
				}
			} catch (Exception ex)
			{
				logger.warn("Exception in closing db conn", ex);
			}
		}
	}
	
	public static void updateSpaceStatus(String uid, int exitCode) throws SchedulerBaseException, ClassNotFoundException, SQLException
	{
		StatusUtils.StepStatus exitStatus = StatusUtils.StepStatus.SUCCESS;
		String message = null;
		if(exitCode != CommandUtil.COMMAND_SUCCESS)
		{
			exitStatus = StatusUtils.StepStatus.FAILED;
			message = ExceptionCodes.PREFIX_EXIT_CODE + exitCode;
		}
		
		updateSpaceStatus(uid, exitStatus.getStatus(), message, true);
	}
	
	public static void updateSpaceStatus(String uid, int status, String message) throws SchedulerBaseException, ClassNotFoundException, SQLException
	{
		updateSpaceStatus(uid, status, message, false);
	}
	
	public static void updateSpaceStatus(String uid, int status, String message, boolean putInEndDate) throws SchedulerBaseException, ClassNotFoundException, SQLException
	{
		Connection conn = null;
		String schema = UtilConstants.SCHEDULER_DB_SCHEMA;		
		try
		{			
			conn = SchedulerDBConnection.getInstance().getConnectionPool().getConnection();		
			SchedulerDatabase.updateSpaceStatus(conn, schema, uid, status, message, putInEndDate);
			
		} finally
		{
			try
			{
				if (conn != null)
					conn.close();
			} catch (Exception ex)
			{
				logger.warn("Exception in closing db conn", ex);
			}
		}
	}

	private static final long ACTIVE_DURATION = 20*60*1000;
	public static final String REFERENCE_PROCESS_START_ID = "ReferenceProcessStartId";
	
	
	/**
	 * Get the most updated status from the list. Make sure that the list belongs to the same jobId.
	 * This is done by looking at the succeeding step number and figuring out if it is still running
	 * @param spaceStatusList
	 * @return
	 * @throws ValidationException 
	 * @throws Exception 
	 */
	private static SpaceStatus getMaximumSpaceStatus(List<SpaceStatus> spaceStatusList) throws ValidationException
	{
		SpaceStatus currentSpaceStatus = null;
		if(spaceStatusList != null && spaceStatusList.size() > 0)
		{
			// first get the most updated step
			// then see if it is still running by looking at the checkInTime
			currentSpaceStatus = spaceStatusList.get(0);
			String jobId = currentSpaceStatus.getJobID();
			for(SpaceStatus spaceStatus : spaceStatusList)
			{
				if(!spaceStatus.getJobID().equalsIgnoreCase(jobId))
				{
					throw new ValidationException("Job Id in the space list should all be the same");
				}
				int currentStepNum = StatusUtils.getStepNumber(currentSpaceStatus.getStep());
				int stepNum = StatusUtils.getStepNumber(spaceStatus.getStep());
				if(stepNum > currentStepNum)
				{
					currentSpaceStatus = spaceStatus;
				}
			}
		}
		return currentSpaceStatus;
	}
	
	public static boolean isStepActive(Connection conn, String schema, SpaceStatus currentSpaceStatus) throws SchedulerBaseException, ClassNotFoundException, SQLException
	{
		if(StepStatus.RUNNING.getStatus() == currentSpaceStatus.getStatus())
		{
			return true;
			/*
			// if the step is SCHEDULED, we never update checkInTime, hence we always assume this step to be active			
			String stepName = currentSpaceStatus.getStep();
			if(Step.ScheduledProcess.toString().equalsIgnoreCase(stepName)
					|| Step.ScheduledExtract.toString().equalsIgnoreCase(stepName)
					|| Step.ScheduledCopy.toString().equalsIgnoreCase(stepName)
					|| Step.ScheduledDelete.toString().equalsIgnoreCase(stepName)
					)	
			{
				return true;
			}
			
			long lastUpdatedTime = getCheckInTime(currentSpaceStatus);
			long currentTime = System.currentTimeMillis();
			if(currentTime - lastUpdatedTime < ACTIVE_DURATION)
			{
				return true;
			}
			
			// mark it as dead
			SchedulerDatabase.updateSpaceStatus(conn, schema, currentSpaceStatus.getId(), StepStatus.DEAD.getStatus(), "Marking dead", true);
			// also mark dead any start schedule process
			
			SchedulerDatabase.updateSpaceStatus(conn, schema, currentSpaceStatus.getJobID(), Step.ScheduledProcess.toString(), StepStatus.RUNNING.getStatus(), StepStatus.DEAD.getStatus(), "Marking dead", true);
			SchedulerDatabase.updateSpaceStatus(conn, schema, currentSpaceStatus.getJobID(), Step.ScheduledExtract.toString(), StepStatus.RUNNING.getStatus(), StepStatus.DEAD.getStatus(), "Marking dead", true);
			SchedulerDatabase.updateSpaceStatus(conn, schema, currentSpaceStatus.getJobID(), Step.ScheduledCopy.toString(), StepStatus.RUNNING.getStatus(), StepStatus.DEAD.getStatus(), "Marking dead", true);
			SchedulerDatabase.updateSpaceStatus(conn, schema, currentSpaceStatus.getJobID(), Step.ScheduledDelete.toString(), StepStatus.RUNNING.getStatus(), StepStatus.DEAD.getStatus(), "Marking dead", true);
			*/
		}
		return false;
	}
	
	public static SpaceStatus getJobStatus(String spaceID, int loadID, String loadGroup) throws Exception
	{	
		SpaceStatus responseStatus = null;
		// see if anything is running
		responseStatus = StatusUtils.getActiveSpaceStatus(spaceID, loadID, loadGroup);
		// if active space status is null, retrieves the most updated one 
		if(responseStatus == null)
		{
			// list contains all the recent space status for the last run
			List<SpaceStatus> recentSpaceStatusList = StatusUtils.getMostRecentSpaceStatus(spaceID, loadID, loadGroup);
			// check if there is a birst local step, if there is one, return the birst local step
			if(recentSpaceStatusList != null && recentSpaceStatusList.size() > 0)
			{
				responseStatus = recentSpaceStatusList.get(0);
				for(SpaceStatus spaceStatus : recentSpaceStatusList)
				{
					if(Step.BirstLocal.toString().equalsIgnoreCase(spaceStatus.getStep()))
					{
						// found birst local step
						responseStatus = spaceStatus;
						break;
					}
				}
			}
		}
		return responseStatus;
	}

	
	public static long getCheckInTime(SpaceStatus currentSpaceStatus)
	{	
		String checkInFileName = null;
		try
		{
			String spaceDirectory = Util.getSpaceDirectory(currentSpaceStatus.getSpaceID());
			if(isLoadStatus(currentSpaceStatus))
			{				
				checkInFileName = spaceDirectory + File.separator + UtilConstants.CHECK_IN_LOAD_FILE;
			}
			else if(isExtractStatus(currentSpaceStatus))
			{
				checkInFileName = spaceDirectory + File.separator + UtilConstants.CHECK_IN_EXTRACT_FILE;
			}
			
			if(checkInFileName != null)
			{
				String timePropertyName = UtilConstants.CHECK_IN_PARAM_TIME;
				Properties props = CommandUtil.getProps(checkInFileName);
				if(props != null && props.getProperty(timePropertyName) != null)
				{
					return Long.parseLong(props.getProperty(timePropertyName));
				}
			}
		}
		catch(Exception ex)
		{
			logger.error("Error while getting check in time for file : " + checkInFileName, ex);
		}
		
		// if not able to retrieve check in time- give benefit of doubt that it is still processing
		// this is a better return case instead of allowing it to overlap with other step
		return System.currentTimeMillis();
	}
	
	public static boolean isLoadStatus(SpaceStatus status)
	{	
		if(status != null && Step.Process.toString().equalsIgnoreCase(status.getStep()))
		{
			return true;
		}
		return false;
	}
	
	public static boolean isExtractStatus(SpaceStatus status)
	{
		if(status != null && Step.SFDCExtract.toString().equalsIgnoreCase(status.getStep()))
		{
			return true;
		}
		return false;
	}
	/**
	 * Retrieves the list of most recent operation. This takes into account the created date
	 * This method should be used to get 
	 * @param spaceID
	 * @return
	 * @throws ClassNotFoundException 
	 * @throws SchedulerBaseException 
	 * @throws SQLException 
	 */
	public static List<SpaceStatus> getMostRecentSpaceStatus(String spaceID, int loadID, String loadGroup) throws SQLException, SchedulerBaseException, ClassNotFoundException
	{	
		Connection conn = null;
		try
		{
			conn = SchedulerDBConnection.getInstance().getConnectionPool().getConnection();
			String schema = UtilConstants.SCHEDULER_DB_SCHEMA;
			return SchedulerDatabase.getMostRecentSpaceStatus(conn, schema, spaceID, loadID, loadGroup);
		}
		finally
		{
			try
			{
				if (conn != null)
					conn.close();
			} catch (Exception ex)
			{
				logger.warn("Exception in closing db conn", ex);
			}
		}
	}
	
	public static SpaceStatus getActiveSpaceStatus(String spaceID) throws SQLException, SchedulerBaseException, ClassNotFoundException 
	{
		return getActiveSpaceStatus(spaceID, -1, null, true) ;
	}
	
	public static SpaceStatus getActiveSpaceStatus(String spaceID, boolean detailedStep) throws SQLException, SchedulerBaseException, ClassNotFoundException 
	{
		return getActiveSpaceStatus(spaceID, -1, null, detailedStep) ;
	}
	
	public static SpaceStatus getActiveSpaceStatus(String spaceID, int loadId, 
			String loadGroup) throws SQLException, SchedulerBaseException, ClassNotFoundException
	{
		return getActiveSpaceStatus(spaceID, loadId, loadGroup, true);
	}
	
	public static SpaceStatus getActiveSpaceStatus(String spaceID, int loadId, 
			String loadGroup, boolean detailedStep) throws SQLException, SchedulerBaseException, ClassNotFoundException
	{

		SpaceStatus activeSpaceStatus = null;
		Connection conn = null;
		try
		{
			conn = SchedulerDBConnection.getInstance().getConnectionPool().getConnection();
			String schema = UtilConstants.SCHEDULER_DB_SCHEMA;
			Map<String, List<SpaceStatus>> spaceStatusMap = SchedulerDatabase.getSpaceStatus(conn, schema, spaceID, null, loadId, loadGroup, null, StepStatus.RUNNING.getStatus());
			if(spaceStatusMap != null && spaceStatusMap.size() > 0)
			{
				for(List<SpaceStatus> spaceStatusList : spaceStatusMap.values())
				{
					SpaceStatus currentSpaceStatus = getMaximumSpaceStatus(spaceStatusList);
					if(StatusUtils.isStepActive(conn, schema, currentSpaceStatus))
					{
						
						// There are times when the next step is to be executed and the previous one is finished
						// e.g.
						//ScheduleDelete, PreDelete, Delete, PostDelete
						// ScheduleDelete will always be 1 during the entire process and will only be updated at the every end
						// At times when Delete step is finished but PostDelete is not started, 
						// In this case, ScheduleDelete will be returned back which is not 100% correct.
						// Hence we check for the active space status step name and if it is any of the ScheduledDelete
						// make sure that there is not another one which is later on
						String stepName = currentSpaceStatus.getStep();
						if(detailedStep 
								&& (Step.ScheduledProcess.toString().equalsIgnoreCase(stepName)
								|| Step.ScheduledExtract.toString().equalsIgnoreCase(stepName)
								|| Step.ScheduledCopy.toString().equalsIgnoreCase(stepName)
								|| Step.ScheduledDelete.toString().equalsIgnoreCase(stepName))
								)
						{
							activeSpaceStatus = SchedulerDatabase.getMostRecentSpaceStatusForJob(conn, schema, spaceID, currentSpaceStatus.getJobID());
							// mark the step as running to signal the space is not yet available
							activeSpaceStatus.setStatus(StepStatus.RUNNING.getStatus());
						}
						else
						{
							activeSpaceStatus = currentSpaceStatus;
						}
						
						break;
					}
				}
			}
			return activeSpaceStatus;
		}
		finally
		{
			try
			{
				if (conn != null)
					conn.close();
			} catch (Exception ex)
			{
				logger.warn("Exception in closing db conn", ex);
			}
		}
	}

	public static String getConcurrentExitCodeMessage(SpaceStatus anyConcurrentSpaceStatus)
	{	
		if(anyConcurrentSpaceStatus != null && anyConcurrentSpaceStatus.getStep() != null && anyConcurrentSpaceStatus.getStatus() == StepStatus.RUNNING.getStatus())
		{
			String step = anyConcurrentSpaceStatus.getStep();
			String prefixCode = ExceptionCodes.PREFIX_EXIT_CODE;
			if(step != null && step.trim().length() > 0)
			{
				if(Step.ScheduledProcess.toString().equalsIgnoreCase(step)){ return prefixCode + ExceptionCodes.EXIT_CODE_CONCURRENT_SPACE_PUBLISH;}
				if(Step.Process.toString().equalsIgnoreCase(step)){ return prefixCode + ExceptionCodes.EXIT_CODE_CONCURRENT_SPACE_PUBLISH;}
				if(Step.PostProcess.toString().equalsIgnoreCase(step)){ return prefixCode + ExceptionCodes.EXIT_CODE_CONCURRENT_SPACE_PUBLISH;}				
				if(Step.ScheduledExtract.toString().equalsIgnoreCase(step)){ return prefixCode + ExceptionCodes.EXIT_CODE_CONCURRENT_SFORCE_EXTRACT;}
				if(Step.ScheduledCopy.toString().equalsIgnoreCase(step)){ return prefixCode + ExceptionCodes.EXIT_CODE_CONCURRENT_SPACE_COPY;}
				if(Step.ScheduledDelete.toString().equalsIgnoreCase(step)){ return prefixCode + ExceptionCodes.EXIT_CODE_CONCURRENT_SPACE_DELETE;}
				return prefixCode + ExceptionCodes.EXIT_CODE_CONCURRENT_OTHER;
			}
		}
		return null;
	}
	
	public static void createCheckInFile(String checkInFileName)
	{
		BufferedWriter writer = null;
		try {
			writer = new BufferedWriter(new FileWriter(checkInFileName, false));
			writer.write(UtilConstants.CHECK_IN_PARAM_TIME + "=" + System.currentTimeMillis());
		} catch (IOException e) {
			logger.error("Error in writing to file : " + checkInFileName, e);
		}
		finally
		{
			if(writer != null)
			{
				try {
					writer.close();
				} catch (IOException e) {
					logger.warn("Error in closing file writer : " + checkInFileName, e);
				}
			}
		}
	}
	
}
