/**
 * $Id: BaseException.java,v 1.25 2012-12-12 05:17:40 BIRST\mpandit Exp $
 *
 * Copyright (C) 2009 Birst, Inc. All rights reserved.
 * BIRST PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.birst.scheduler.utils;

public class BaseException extends Exception {	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static final int SUCCESS					= 0;
	public static final int ERROR_NAVIGATION 		= -100;
	public static final int ERROR_SYNTAX			= -101;
	public static final int ERROR_BAD_COLUMN		= -102;
	public static final int ERROR_DATA_UNAVAILABLE	= -103;
	public static final int ERROR_QUERY_CANCELLED	= -3;
	public static final int ERROR_PUBLISHING		= -105;
	public static final int ERROR_NOT_LOGGED_IN		= -1;
	public static final int ERROR_RESULTSET_TOOBIG	= -107;
	public static final int ERROR_OTHER				= -2;
	public static final int ERROR_PERMISSION		= -108;
	public static final int ERROR_FILE_NOT_FOUND	= -109;
	public static final int ERROR_JASPER_OTHER  	= -110;
	public static final int ERROR_JASPER_DISPLAY	= -111;
	public static final int ERROR_QUERY_TIMEOUT		= -113;
	public static final int ERROR_PIVOT_TABLE_TOO_WIDE = -114;
	public static final int ERROR_SESSION_VARIABLE_UNAVAILABLE = -115;
	public static final int ERROR_GOOGLE_ANALYTICS = -116;
	public static final int ERROR_PERMISSION_QUERY_GENERATION = -117;
	public static final int ERROR_REPOSITORY_ERROR	= -118;
	public static final int ERROR_NO_DEFAULT_BOOKMARK = -119;
	public static final int ERROR_NO_DATA = -120;
	public static final int ERROR_INVALID_DATA = -121;
	public static final int ERROR_INVALID_COLOR = -122;
	public static final int ERROR_XML_PARSE = -123;
	public static final int ERROR_INVALID_IP = -124;
	public static final int ERROR_UNTERMINATED_QUOTE = -125;
	public static final int ERROR_SAVED_EXPRESSION_NOT_FOUND = -126;
	public static final int ERROR_SAVED_EXPRESSION_DUPLICATE_NAME = -127;
	public static final int ERROR_SWAP_SPACE_IN_PROGRESS = -128;
	public static final int ERROR_SECURITY_FILTER = -129;
	public static final int ERROR_DRILL_ROW_DUPLICATE_DRILL_FROM = -130;
	public static final int ERROR_DRILL_MAP_DUPLICATE_NAME = -131;
	public static final int ERROR_UNAUTHORIZED_OPERATION = -132;
	public static final int ERROR_TRELLIS_TOO_LARGE = -133;
	public static final int ERROR_CONNECTOR_LOGIN_FAILURE = -134;
	public static final int ERROR_CONNECTOR_INVALID_QUERY = -135;
	
	
	protected BaseException()
	{
		super();
	}
	
	protected BaseException(String s)
	{
		super(s);
	}
	
	protected BaseException(String s, Throwable cause)
	{
		super(s, cause);
	}

	public int getErrorCode()
	{
		return ERROR_OTHER;
	}
}
