package com.birst.scheduler.utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.axiom.om.OMAttribute;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
import org.apache.axiom.om.OMText;
import org.apache.axiom.om.impl.llom.util.AXIOMUtil;

import com.birst.scheduler.webServices.StringWebServiceResult;


public class AxisXmlUtils
{
	public static final String XML_EOL = "&#xD;";
	public static void addContent(OMFactory fac, OMElement parent, String name, int value, OMNamespace ns) {
		AxisXmlUtils.addContent(fac, parent, name, String.valueOf(value), ns);
	}
	public static void addContent(OMFactory fac, OMElement parent, String name, long value, OMNamespace ns) {
		AxisXmlUtils.addContent(fac, parent, name, String.valueOf(value), ns);
	}
	
	public static void addContent(OMFactory fac, OMElement parent, String name, Date value, String formatPattern, OMNamespace ns) {
		if (value != null && formatPattern != null) {
			AxisXmlUtils.addContent(fac, parent, name, new SimpleDateFormat(formatPattern).format(value), ns);
		}
	}
	
	public static void addContent(OMFactory fac, OMElement parent, String name, Date value, OMNamespace ns) {
		if (value != null) {
			AxisXmlUtils.addContent(fac, parent, name, new SimpleDateFormat().format(value), ns);
		}
	}
	public static void addContent(OMFactory fac, OMElement parent, String name, boolean value, OMNamespace ns) {
		AxisXmlUtils.addContent(fac, parent, name, Boolean.toString(value), ns);
	}

	public static OMElement addContent(OMFactory fac, OMElement parent, String name, OMNamespace ns) {
		OMElement el = fac.createOMElement(name, ns);
		parent.addChild(el);
		return el;
	}
	
	public static void addContent(OMFactory fac, OMElement parent, String name, String value, OMNamespace ns) {
		OMElement el = fac.createOMElement(name, ns);
		OMText text = fac.createOMText(value);
		el.addChild(text);
		parent.addChild(el);
	}
	
	public static void addContent(OMFactory fac, OMElement parent, String name, double value, OMNamespace ns) {
		AxisXmlUtils.addContent(fac, parent, name, String.valueOf(value), ns);
	}
	
	public static void addAttribute(OMFactory fac, OMElement element, String name, String value, OMNamespace ns){
		OMAttribute attribute = fac.createOMAttribute(name, ns, value);
		element.addAttribute(attribute);
	}
	
	public static void addContent(OMFactory fac, OMElement parent, String name, List<String> values, OMNamespace ns) {		
		if (values != null && values.size() > 0) {
			OMElement el = fac.createOMElement(name, ns);
			for (String value: values) {
				AxisXmlUtils.addContent(fac, el, name, value, ns); 
			}
			parent.addChild(el);
		}
	}
	
	public static String encode(String s) 
	{
		if (s == null)
			return null;
		
		s = s.replaceAll("&", "&amp;").
			replaceAll(">", "&gt;").
			replaceAll("<", "&lt;").
			replaceAll("%", "&#37;").
			replaceAll("\"", "&quot;").
			replaceAll("'", "&apos;").
			replaceAll("\u221E", "&#x221E;");
		return s.replaceAll("\r\n", "\n").replaceAll("\r", "\n").replaceAll("\n", XML_EOL);
	}
	
	public static String decode(String s) 
	{
		if (s == null)
			return null;
		
		s = s.replaceAll(XML_EOL, "\r\n");
		return s.replaceAll("&apos;", "'").
			replaceAll("&#x221E;", "\u221E").
			replaceAll("&quot;", "\"").
			replaceAll("&#37;", "%").
			replaceAll("&lt;", "<").
			replaceAll("&gt;", ">").
			replaceAll("&amp;", "&");
	}
	
	public static void addXmlStringToParent( OMFactory factory, OMElement parent, String str, OMNamespace ns)
	{
		if(str == null)
		{
			return;
		}
		
		String decodedString = decode(str);
		StringWebServiceResult stringWSResult = new StringWebServiceResult(decodedString);
		stringWSResult.addContent(parent, factory, ns);
		
	}
}
