package com.birst.scheduler.utils;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.birst.scheduler.shared.ServerDetails;


public class RoundRobin
{
	private static final Logger logger = Logger.getLogger(RoundRobin.class);
	private static Map<String, RoundRobin> context = new HashMap<String, RoundRobin>();
	
	private Iterator<ServerDetails> iterator;
	private List<ServerDetails> list;
	
	public void setList(List<ServerDetails> list)
	{
		this.list = list;
		this.iterator = list.iterator();
	}
	
	public synchronized ServerDetails next() throws Exception
	{
		if(list == null || list.size() == 0)
		{
			throw new Exception("No elements to round robin");
		}
		
		if(!iterator.hasNext())
		{
			iterator = list.iterator();
		}
		
		return iterator.next();
	}
	
	public synchronized static ServerDetails getNextAvailableElement(String key) throws Exception
	{
		RoundRobin value = context.get(key);
		if(value != null)
		{
			return value.next();
		}
		
		return null;
	}
	
	public static void addRoundRobinSource(String key, List<ServerDetails> roundRobinElements)
	{
		RoundRobin roundRobin = new RoundRobin();
		roundRobin.setList(roundRobinElements);
		RoundRobin existingValue = context.get(key);		
		if(existingValue != null)
		{				
			logger.warn("Overrwriting round robin source for " + key);
		}
		context.put(key, roundRobin);
	}
}
