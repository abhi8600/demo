package com.birst.scheduler.utils;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.Map.Entry;

import org.apache.log4j.Logger;

import com.birst.scheduler.utils.DatabaseConnection.DatabaseType;

public class DBConnectionPool
{
	
	private static final Logger logger = Logger.getLogger(DBConnectionPool.class);	
	private static Map<String, DBConnectionPool> connectionPoolCache = new HashMap<String, DBConnectionPool>();
	//private ConnectionPool connectionPool;	
	//private DatabaseConnection databaseConnection;
		
	protected static final String PING_QUERY = "select 1";
	protected Map<Thread, Connection> poolForThreads;
	protected String connectString;
	protected String username;
	protected String password;
	protected int size;
	protected String driverName;
	protected DatabaseConnection dbConn;
	
	public static DBConnectionPool getInstance(String name,
			String driver, String connectString, String userName,
			String password, int maxConnections, String dbType, String poolType) throws SQLException, ClassNotFoundException 
	{
		String key = poolType + "_" + connectString; // should be sufficient for a key
		DBConnectionPool connectionPool = connectionPoolCache.get(key);
		if (connectionPool == null)
		{
			connectionPool = new DBConnectionPool(name, driver, connectString, userName, password, maxConnections, dbType);				
			connectionPoolCache.put(key, connectionPool);
		} else
		{
			logger.debug("Using existing database connection pool using connection string " + connectString);
		}
		return connectionPool;
	}
	
	
	protected DBConnectionPool(String name, String driverName,
			String connectStr, String username, String password, int size, String dbType)
			throws SQLException, ClassNotFoundException
	{
		logger.debug("Database connection pool created for " + name + " using connection string " + connectStr + " and driver " + driverName);
		if (driverName.equalsIgnoreCase("none") || driverName.isEmpty())
			return;
		poolForThreads = new HashMap<Thread, Connection>();		
		Class.forName(driverName);
		connectString = connectStr;		
		this.username = username;
		this.password = password;
		this.size = size;
		this.driverName = driverName;
		dbConn = new DatabaseConnection();
		if (dbType == null || dbType.isEmpty())
		 {
			 dbConn.setDBTypeFromDriver(driverName);
		 }
		 else
		 {
			 dbConn.setDBType(dbType);
		 }
		// get version information for logging purposes
		try
		{
			// note that this is only for version information, no need to add any of the properties that we add for real
			// connection
			String url = connectString;
			Properties p = new Properties();
			p.setProperty("user", username);
			if (password != null && !password.isEmpty())
				p.setProperty("password", password);
			Connection conn = DriverManager.getConnection(url, p);
			DatabaseMetaData dmd = conn.getMetaData();
			logger.debug("JDBC Driver information: " + dmd.getDriverName() + ", " + dmd.getDriverVersion() + ", JDBC Version: " + dmd.getJDBCMajorVersion()
					+ "." + dmd.getJDBCMinorVersion());
			conn.close();
		} catch (SQLException e)
		{
			logger.error("failure on getting JDBC driver version information");
			throw e;
		}
	}
	
	public DatabaseConnection getDBConnection()
	{
		return dbConn;
	}
	
	public synchronized Connection getConnection() throws SQLException
	{
		// use a thread-based connection pool
		if (poolForThreads == null)
			return null;
		Connection conn = poolForThreads.get(Thread.currentThread());
		if (!isValidConnection(conn))
		{			
			poolForThreads.remove(Thread.currentThread());
			reapConnections();
			conn = getConnectionFromDatabase();
			poolForThreads.put(Thread.currentThread(), conn);
		}
		logger.trace("Connections in the thread pool - " + poolForThreads.size());				
		return conn;
	}
	
	private Connection getConnectionFromDatabase() throws SQLException
	{
		Connection conn;
		String url = connectString;
		Properties p = new Properties();
		p.setProperty("user", username);
		if (password != null && !password.isEmpty())
			p.setProperty("password", password);		
		try
		{
			conn = DriverManager.getConnection(url, p);
		} catch (SQLException sqe)
		{
			logger.error(sqe.getMessage());
			// try it a second time... really should be a loop that repeats and sleeps
			conn = DriverManager.getConnection(url, p);
		}
		return conn;
	}
	
	private boolean isValidConnection(Connection connection)
	{
		if (connection == null)
			return false;
		try
		{
			if (connection.isClosed())
				return false;
		}
		catch (Exception ex)
		{
			logger.debug(ex, ex);
		}
		Statement stmt = null;
		ResultSet rs = null;
		long startTime = System.currentTimeMillis();
		try
		{
			stmt = connection.createStatement();
			String query = PING_QUERY;
			if (dbConn.getDBType() == DatabaseType.Oracle)
			{
				query = query + " FROM dual";
			}
			rs = stmt.executeQuery(query);
		}
		catch (SQLException e)
		{
			logger.warn("Ping query to DB failed");
			try
			{
				connection.close();
			}
			catch (Exception ex)
			{
				logger.debug(ex, ex);
			}
			return false;
		}
		finally
		{
			try
			{
				if (rs != null)
					rs.close();
				if (stmt != null)
					stmt.close();
			}
			catch (SQLException e)
			{
				logger.debug(e, e);
				return false;
			}
		}		
		long endTime = System.currentTimeMillis();
		long delta = endTime - startTime;
		if (delta > 50)
		{
			logger.warn("Ping to database took " + delta + " milliseconds.");
		} else if (delta > 10)
		{
			if (logger.isTraceEnabled())
				logger.trace("Ping to database took " + delta + " milliseconds.");
		}
		return true;
	}
	
	private void reapConnections()
	{
		try
		{
			Set<Thread> remove = new HashSet<Thread>();
			for (Entry<Thread, Connection> entry : poolForThreads.entrySet())
			{
				if (!entry.getKey().isAlive()) 
				{
					try
					{
						Connection conn = entry.getValue();
						if(conn != null && !conn.isClosed())
						{
							entry.getValue().close();
						}
					} catch (Exception ex)
					{
						logger.error(ex.getMessage(), ex);
					}
					logger.debug("Reaping connection associated with dead thread: " + entry.getKey().toString());
					remove.add(entry.getKey());							
				}
			}
			for (Thread th : remove)
			{
				poolForThreads.remove(th);
			}
		} catch (Exception ex)
		{
			logger.error(ex, ex);
		}
		return;
	}
}
