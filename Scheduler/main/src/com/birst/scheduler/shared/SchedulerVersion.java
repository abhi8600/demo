package com.birst.scheduler.shared;

import org.quartz.Scheduler;

public class SchedulerVersion
{
	Scheduler scheduler;
	String displayName;
	boolean enableMigration;
	
	public SchedulerVersion(){
		
	}
	
	public SchedulerVersion(Scheduler scheduler, String displayName, boolean enableMigration)
	{
		this.scheduler = scheduler;
		this.displayName = displayName;
		this.enableMigration = enableMigration;
	}
	
	public Scheduler getScheduler()
	{
		return scheduler;
	}
	public String getDisplayName()
	{
		return displayName;
	}
	public boolean getEnableMigration()
	{
		return enableMigration;
	}
	public void setScheduler(Scheduler scheduler)
	{
		this.scheduler = scheduler;
	}
	public void setDisplayName(String displayName)
	{
		this.displayName = displayName;
	}
	public void setEnableMigration(boolean enableMigration)
	{
		this.enableMigration = enableMigration;
	}
	
	public static boolean isPastRelease(int releaseType)
	{
		return releaseType < 0 ? true : false;
	}
	
	public static boolean isFutureRelease(int releaseType)
	{
		return releaseType > 0 ? true : false;
	}
	
	public static boolean isCurrentRelease(int releaseType)
	{
		return releaseType == 0 ? true : false;
	}
}
