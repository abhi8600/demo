package com.birst.scheduler.shared;

import java.sql.Timestamp;

public class JobHistory
{	
	private String id;
	private String jobID;
	private String scheduleID;
	private Timestamp startTime;
	private Timestamp endTime;
	private int status;
	private String errorMessage;
	
	public String getId()
	{
		return id;
	}
	public void setId(String id)
	{
		this.id = id;
	}
	
	
	public String getJobID()
	{
		return jobID;
	}
	public void setJobID(String jobID)
	{
		this.jobID = jobID;
	}
	public String getScheduleID()
	{
		return scheduleID;
	}
	public void setScheduleID(String scheduleID)
	{
		this.scheduleID = scheduleID;
	}
	public Timestamp getStartTime()
	{
		return startTime;
	}
	public void setStartTime(Timestamp startTime)
	{
		this.startTime = startTime;
	}
	public Timestamp getEndTime()
	{
		return endTime;
	}
	public void setEndTime(Timestamp endTime)
	{
		this.endTime = endTime;
	}
	public int getStatus()
	{
		return status;
	}
	public void setStatus(int status)
	{
		this.status = status;
	}
	public String getErrorMessage()
	{
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage)
	{
		this.errorMessage = errorMessage;
	}

}
