package com.birst.scheduler.shared;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.Map.Entry;

import org.apache.log4j.Logger;
import org.quartz.Scheduler;

import com.birst.scheduler.SchedulerServerContext;
import com.birst.scheduler.Report.ScheduledReport;
import com.birst.scheduler.Report.ScheduledReportOperation;
import com.birst.scheduler.exceptions.SchedulerBaseException;
import com.birst.scheduler.salesforce.SFDCInfo;
import com.birst.scheduler.salesforce.SFDCJobOperation;
import com.birst.scheduler.utils.SchedulerDBConnection;
import com.birst.scheduler.utils.SchedulerDatabase;
import com.birst.scheduler.utils.Util;
import com.birst.scheduler.utils.UtilConstants;
import com.birst.scheduler.utils.UtilConstants.JobType;
import com.birst.scheduler.utils.UtilConstants.Type;
import com.birst.scheduler.webServices.response.SchedulerWebServiceResult;

public class CopyJob
{	
	String jobId;
	String spaceId;
	String userId;
	String type;
	Scheduler fromScheduler;
	Scheduler toScheduler;
	boolean deleteFrom;
	// This is basically any modified version of the job that is passed from the client side
	// Takes care of the scenario of the implicit save and migrate if user is modifying the report schedule
	// of a old release on a new release
	String jobOptionsXmlString;
	boolean hasMultipleJobIDs;
	
	public String getJobId()
	{
		return jobId;
	}
	public String getSpaceId()
	{
		return spaceId;
	}
	public String getUserId()
	{
		return userId;
	}
	public String getJobType()
	{
		return type;
	}
	public Scheduler getFromScheduler()
	{
		return fromScheduler;
	}
	public Scheduler getToScheduler()
	{
		return toScheduler;
	}
	public boolean getHasMultipleJobIDs()
	{
		return hasMultipleJobIDs;
	}
	public boolean isDeleteFrom()
	{
		return deleteFrom;
	}
	public void setJobId(String jobId)
	{
		this.jobId = jobId;
	}
	public void setUserId(String userId)
	{
		this.userId = userId;
	}
	public void setSpaceId(String spaceId)
	{
		this.spaceId = spaceId;
	}
	public void setJobType(String type)
	{
		this.type = type;
	}
	public void setFromScheduler(Scheduler fromScheduler)
	{
		this.fromScheduler = fromScheduler;
	}
	public void setToScheduler(Scheduler toScheduler)
	{
		this.toScheduler = toScheduler;
	}
	public void setDeleteFrom(boolean deleteFrom)
	{
		this.deleteFrom = deleteFrom;
	}
	public void setHasMultipleJobIDs(boolean hasMultipleJobIDs)
	{
		this.hasMultipleJobIDs = hasMultipleJobIDs;
	}
	public String getJobOptionsXmlString()
	{
		return jobOptionsXmlString;
	}
	public void setJobOptionsXmlString(String jobOptionsXmlString)
	{
		 this.jobOptionsXmlString  = jobOptionsXmlString;
	}
	
	public void performCopyBetweenSchedulers(boolean hasMultipleJobIDs, boolean sameVersions) throws Exception
	{
		performCopyBetweenSchedulers(null, hasMultipleJobIDs, sameVersions);
	}
	
	public void performCopyBetweenSchedulers(SchedulerWebServiceResult result, boolean hasMultipleJobIDs, boolean sameVersions) throws Exception
	{			
		if(JobType.REPORT.toString().equalsIgnoreCase(type))
		{
			copyReportScheduleBetweenSchedulers();
		} 
		else if (JobType.SFDCEXTRACTION.toString().equalsIgnoreCase(type) || Type.sfdc.toString().equalsIgnoreCase(type) || Type.netsuite.toString().equals(type) 
				||	type.endsWith(UtilConstants.CONNECTOR_SCHEDULE_TYPE_SUFFIX))
		{
			if (hasMultipleJobIDs) {
				copySFDCMultipleSchedulesBetweenSchedulers(result, hasMultipleJobIDs, sameVersions);
			} else {
				copySFDCSchedulesBetweenSchedulers();
			}
		}
	}
	
	private void copyReportScheduleBetweenSchedulers() throws Exception
	{		
		ScheduledReport originalOldReport = ScheduledReportOperation.getScheduledReportDetails(spaceId, jobId);
		ScheduledReport modifiedReport = null;
		if(jobOptionsXmlString != null && jobOptionsXmlString.length() > 0)
		{
			modifiedReport = ScheduledReportOperation.parseReportDetailsXmlString(spaceId, userId, jobOptionsXmlString);
			String reportPath = Util.addCatalogPrefix(modifiedReport.getReportPath());
			String toReportPath = Util.addCatalogPrefix(modifiedReport.getToReportPath());
			String triggerReportPath = Util.addCatalogPrefix(modifiedReport.getTriggerReportPath());				
			modifiedReport.setReportPath(reportPath);
			modifiedReport.setToReportPath(toReportPath);
			modifiedReport.setTriggerReportPath(triggerReportPath);
		}

		boolean enableJobAfterMigration = false;
		Schedule[] oldJobSchedules = originalOldReport.getJobSchedules();
		if(oldJobSchedules != null && oldJobSchedules.length > 0)
		{
			enableJobAfterMigration = oldJobSchedules[0].isEnable();
		}

		// create a duplicate copy but in other scheduler
		// first copy. Only if the entire operation is successful, enable the schedule -- if it needs to be

		ScheduledReport newReport = ScheduledReportOperation.getNewCopiedReport(originalOldReport);
		// Make sure that all the changes during saving to the new release are propagated properly
		newReport = ScheduledReportOperation.copyChanges(modifiedReport, newReport, false);
		newReport.setModifiedBy(userId);
		ScheduledReportOperation.addReport(toScheduler, spaceId, newReport);
		if(deleteFrom)
		{
			ScheduledReportOperation.removeReport(fromScheduler, spaceId, jobId);
		}
		// now enable the schedule in the new job if the original one was enabled
		if(enableJobAfterMigration && newReport.getJobSchedules() != null && newReport.getJobSchedules().length > 0)
		{	
			ScheduledReportOperation.flagAllSchedules(toScheduler, spaceId, newReport.getId(), true);
		}
	}
	
	private void copySFDCSchedulesBetweenSchedulers() throws Exception
	{	
		SFDCInfo originalSFDCInfo = SFDCJobOperation.getConnectorInfo(jobId);		
		SFDCInfo modifiedSFDCInfo = null;
		if(jobOptionsXmlString != null && jobOptionsXmlString.length() > 0)
		{
			modifiedSFDCInfo = SFDCJobOperation.parseConnectorOptionsXmlString(spaceId, userId, jobOptionsXmlString);
			
		}

		// create a duplicate copy but in other scheduler
		// first copy. Only if the entire operation is successful, enable the schedule -- if it needs to be
		SFDCInfo newSFDCInfo = SFDCJobOperation.getNewCopiedSFDCInfo(originalSFDCInfo);
		// Make sure that all the changes during saving to the new release are propagated properly
		newSFDCInfo = SFDCJobOperation.copyChanges(modifiedSFDCInfo, newSFDCInfo, false);
		newSFDCInfo.setModifiedBy(userId);
		newSFDCInfo.setName(getScheduleName(newSFDCInfo.getName()));
		newSFDCInfo.setType(getScheduleType(newSFDCInfo.getType()));
		
		// create a map of cron expression to count of times it is enabled. 
		// This is used later on to activate the schedules when it has been removed successfully from the old ones. 
		// Theoretically in our system, you could have multiple schedules for a given cron expression 
		// its unlikely any user will have the exact schedules but in case they. 
		Map<String, Integer> cronExpressionToEnableFlagMap = new HashMap<String, Integer>();
		
		Schedule[] orignalSchedules = modifiedSFDCInfo != null ? modifiedSFDCInfo.getJobSchedules() : originalSFDCInfo.getJobSchedules();
		for(Schedule oldSchedule : orignalSchedules)
		{
			// we are by default disabling all the schedules but want to store 
			// what is enabled from orignal schedules and later on use this information
			if(!oldSchedule.isEnable())
			{
				continue;
			}
			
			Integer count = cronExpressionToEnableFlagMap.get(oldSchedule.getExpression()) == null ? 0 : cronExpressionToEnableFlagMap.get(oldSchedule.getExpression());
			cronExpressionToEnableFlagMap.put(oldSchedule.getExpression(), count+1);
		}
		
		SFDCJobOperation.addConnectorInfo(toScheduler, spaceId, newSFDCInfo, true);
		if(deleteFrom)
		{
			SFDCJobOperation.removeSFDCInfo(fromScheduler, spaceId, jobId);
		}
		
		Schedule[] newSchedules = newSFDCInfo.getJobSchedules();
		// now enable the schedule in the new job if the original one was enabled
		for(Entry<String,Integer> entry : cronExpressionToEnableFlagMap.entrySet())
		{
			String cronExpression = entry.getKey();
			Integer value = entry.getValue();
			for(Schedule updatedSchedule : newSchedules)
			{	
				if(updatedSchedule.getExpression().equalsIgnoreCase(cronExpression) && value > 0)
				{
					updatedSchedule.setEnable(true);
					value = value -1;
				}
			}
		}
		
		if(newSchedules != null && newSchedules.length > 0)
		{	
			SFDCJobOperation.updateSFDCInfo(toScheduler, spaceId, newSFDCInfo, true);
		}
	}
	
	public static String getScheduleName(String name)
	{
		return (name == null || name.equals("")) ? UtilConstants.DEFAULT_NAME : name;
	}
	
	private void copySFDCMultipleSchedulesBetweenSchedulers(SchedulerWebServiceResult result, boolean hasMultipleJobIDs, boolean sameSchedulerVersions) throws Exception
	{	
		Connection conn = null;
		try {
			conn = SchedulerDBConnection.getInstance().getConnectionPool().getConnection();
			String schema = UtilConstants.SCHEDULER_DB_SCHEMA;
			SFDCInfo originalSFDCInfo = SFDCJobOperation.getConnectorInfo(jobId);
			SFDCInfo modifiedSFDCInfo = null;
			String name = getScheduleName(originalSFDCInfo.getName());
			if(jobOptionsXmlString != null && jobOptionsXmlString.length() > 0)
			{
				modifiedSFDCInfo = SFDCJobOperation.parseConnectorOptionsXmlString(spaceId, userId, jobOptionsXmlString);
				name = getScheduleName(modifiedSFDCInfo.getName());
			}
	
			// create a duplicate copy but in other scheduler
			// first copy. Only if the entire operation is successful, enable the schedule -- if it needs to be
			int scheduleCnt = originalSFDCInfo.getJobSchedules().length;
			if (originalSFDCInfo.getJobSchedules() != null && scheduleCnt > 0) {
				conn = SchedulerDBConnection.getInstance().getConnectionPool().getConnection();
				int maxNum = SchedulerDatabase.getDefaultNameMaxNumber(conn, schema, originalSFDCInfo.getSpaceId(), name);
				int cnt = 1 + maxNum;
				boolean isMigration = (hasMultipleJobIDs || !sameSchedulerVersions);
				boolean isValid = true;
				for (Schedule oldSch : originalSFDCInfo.getJobSchedules()) {
					SFDCInfo newSFDCInfo = SFDCJobOperation.getNewCopiedSFDCInfo(originalSFDCInfo);
					String scheduleId = UUID.randomUUID().toString();
					List<Schedule> newSchedules = new ArrayList<Schedule>();
		    		Schedule newSchedule = new Schedule(scheduleId, newSFDCInfo.getId(), getFixedCronExpression(oldSch.getExpression()), oldSch.isEnable(), oldSch.getTimeZone());
		    		newSchedule.setStartDate(oldSch.getStartDate());
		    		newSchedules.add(newSchedule);
		    		newSFDCInfo.setJobSchedules((Schedule [])newSchedules.toArray(new Schedule[newSchedules.size()]));
					newSFDCInfo = SFDCJobOperation.copyChanges(modifiedSFDCInfo, newSFDCInfo, false);
					newSFDCInfo.setModifiedBy(userId);
					newSFDCInfo.setName((!deleteFrom) ? name + " - " + cnt : name);
					newSFDCInfo.setType(getScheduleType(originalSFDCInfo.getType()));
					
					SFDCInfo response = SFDCJobOperation.addConnectorInfo(toScheduler, spaceId, newSFDCInfo, isMigration);
					if (!response.currentExecutionStatus) {
						isValid = response.currentExecutionStatus;
					}
					if(deleteFrom)
					{
						SFDCJobOperation.removeSFDCInfo(fromScheduler, spaceId, jobId);
					}
					cnt++;
				}
				
				if (!isValid) {
					result.setErrorCode(SchedulerBaseException.ERROR_DUPLICATE_SCHEDULE_COPY);
					result.setErrorMessage("A schedule already exists with the same scheduled time.");
				}
			}
		} finally {
			conn.close();
		}
	}
	
	public static String getScheduleType(String type) {
		return (type == null || type.equals("")) ? UtilConstants.Type.sfdc.toString() : type;
	}
	
	// Quick fix for old SFDC connector scheduler for monthly schedule bug.
	private String getFixedCronExpression(String cronExp) {
		String fixedExpression = cronExp;
		if (cronExp.equals("0 0 0  * ? *") || cronExp.equals("0 0 0 * ? *")) {
			fixedExpression = "0 0 0 1 * ? *";
		}
		return fixedExpression;
	}
}
