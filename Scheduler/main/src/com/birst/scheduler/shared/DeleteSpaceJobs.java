package com.birst.scheduler.shared;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.impl.matchers.GroupMatcher;

import com.birst.scheduler.SchedulerServerContext;
import com.birst.scheduler.utils.SchedulerDatabase;
import com.birst.scheduler.utils.Util;
import com.birst.scheduler.utils.UtilConstants;


public class DeleteSpaceJobs implements Job
{

	private static final Logger logger = Logger.getLogger(JobExecutionException.class);	
	@SuppressWarnings("unchecked")
	@Override
	public void execute(JobExecutionContext arg0) throws JobExecutionException
	{
		String spaceID = null;
		try
		{
			JobDataMap map = arg0.getMergedJobDataMap();
			Object obj = map.get(UtilConstants.KEY_SPACE_ID);
			if(obj == null)
			{
				logger.error("No spaceID provided for deletion of space jobs");
				return;
			}
			
			spaceID = (String) obj;
			Scheduler scheduler = SchedulerServerContext.getSchedulerInstance();
			List<JobKey> allJobKeys = new ArrayList<JobKey>();
			Set<JobKey> jobKeysReport = scheduler.getJobKeys(GroupMatcher.groupEquals(Util.getCustJobGroupNameForReport(spaceID)));
			Set<JobKey> jobKeysSFDC = scheduler.getJobKeys(GroupMatcher.groupEquals(Util.getCustJobGroupNameForSFDC(spaceID)));
			Set<JobKey> jobKeysDelete = scheduler.getJobKeys(GroupMatcher.groupEquals(Util.getCustJobGroupNameForDelete(spaceID)));
			Set<JobKey> jobKeysProcess = scheduler.getJobKeys(GroupMatcher.groupEquals(Util.getCustJobGroupNameForProcess(spaceID)));
			
			allJobKeys.addAll(jobKeysReport);
			allJobKeys.addAll(jobKeysSFDC);
			allJobKeys.addAll(jobKeysDelete);
			allJobKeys.addAll(jobKeysProcess);
			
			if(allJobKeys != null)
			{
				for(JobKey jobKey : allJobKeys)
				{
					try
					{
						scheduler.deleteJob(jobKey);
						// delete the information from database
					}
					catch(Exception ex)
					{
						logger.error("Error in deleting job : " + jobKey != null ? jobKey.getName() : null);
						// continue to the next one and see if it could be deleted
					}
				}
				
				// Delete all the information from customized tables
				//Util.removeScheduledReports(spaceID);
				
			}
			
		}
		catch(Exception ex)
		{	
			logger.error("Error in deleting all jobs for space " + spaceID, ex);
			throw new JobExecutionException("Error while running reportJob", ex.getCause());	
		}

	}

}
