package com.birst.scheduler.shared;

public class BaseJobObject
{
	private String jobId;
	private boolean enabled;
	private String type;
	private String spaceID;
	
	public String getJobId()
	{
		return jobId;
	}
	public void setJobId(String jobId)
	{
		this.jobId = jobId;
	}
	
	public boolean isEnabled()
	{
		return enabled;
	}
	
	public void setEnabled(boolean runStatus)
	{
		this.enabled = runStatus;
	}
	
	public String getType()
	{
		return type;
	}
	
	public void setType(String type)
	{
		this.type = type;
	}
	
	public String getSpaceID()
	{
		return spaceID;
	}
	
	public void setSpaceID(String spaceID)
	{
		this.spaceID = spaceID;
	}

}
