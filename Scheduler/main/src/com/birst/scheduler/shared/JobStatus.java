package com.birst.scheduler.shared;

import com.birst.scheduler.utils.UtilConstants.RunningJobStatus;

public class JobStatus
{	
	RunningJobStatus jobStatus;
	boolean retry;
	String errorMessage;
	String exitErrorCodeMsg;
	
	public JobStatus(){
	}

	public RunningJobStatus getJobStatus()
	{
		return jobStatus;
	}

	public boolean isRetry()
	{
		return retry;
	}

	public String getErrorMessage()
	{
		return errorMessage;
	}

	public String getExitErrorCodeMsg()
	{
		return exitErrorCodeMsg;
	}

	public void setJobStatus(RunningJobStatus jobStatus)
	{
		this.jobStatus = jobStatus;
	}

	public void setRetry(boolean retry)
	{
		this.retry = retry;
	}

	public void setErrorMessage(String errorMessage)
	{
		this.errorMessage = errorMessage;
	}

	public void setExitErrorCodeMsg(String exitErrorCodeMsg)
	{
		this.exitErrorCodeMsg = exitErrorCodeMsg;
	}

	public void markTheJobStatus(RunningJobStatus status, String errorMsg)
	{
		markTheJobStatus(status, errorMsg, false);
	}
	
	public void markTheJobStatus(RunningJobStatus status, String errorMsg, boolean retryJob)
	{
		markTheJobStatus(status, errorMsg, false, null);
	}
	
	public void markTheJobStatus(RunningJobStatus status, String errorMsg, boolean retryJob, String exitCodeMsg)
	{
		jobStatus = status;
		errorMessage = errorMsg;
		retry = retryJob;
		exitErrorCodeMsg = exitCodeMsg;
	}
	
}
