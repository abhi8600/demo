package com.birst.scheduler.shared;

import com.birst.scheduler.Report.ScheduledReportOperation;
import com.birst.scheduler.delete.DeleteJobOperation;
import com.birst.scheduler.process.ProcessJobOperation;
import com.birst.scheduler.salesforce.SFDCJobOperation;
import com.birst.scheduler.utils.UtilConstants;
import com.birst.scheduler.bds.BDSJobOperation;

public class OperationFactory
{
	public static Operation getOperationType(String jobType)
	{
		if(jobType == null)
		{
			return null;
		}
		
		if(jobType.equalsIgnoreCase(UtilConstants.JobType.REPORT.toString()))
			return new ScheduledReportOperation();
		if(jobType.equalsIgnoreCase(UtilConstants.JobType.SFDCEXTRACTION.toString()) || jobType.equalsIgnoreCase(UtilConstants.JobType.NETSUITE.toString()) 
				|| jobType.equalsIgnoreCase(UtilConstants.Type.sfdc.toString()) || jobType.equalsIgnoreCase(UtilConstants.Type.netsuite.toString()))
			return new SFDCJobOperation();
		if (jobType.endsWith(UtilConstants.CONNECTOR_SCHEDULE_TYPE_SUFFIX))//new framework connectors
			return new SFDCJobOperation();
		if(jobType.equalsIgnoreCase(UtilConstants.JobType.PROCESSING.toString()))
			return new ProcessJobOperation();
		if(jobType.equalsIgnoreCase(UtilConstants.JobType.DELETELOAD.toString()))
			return new DeleteJobOperation();
		if(jobType.equalsIgnoreCase(UtilConstants.JobType.BDS.toString()) || jobType.equalsIgnoreCase(UtilConstants.Type.bds.toString()))
			return new BDSJobOperation();
						
		return null;
	}
}
