package com.birst.scheduler.shared;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.birst.scheduler.exceptions.ValidationException;
import com.birst.scheduler.utils.CompareUtils;

public class ScheduleInfo
{
	@Override
	public boolean equals(Object obj)
	{	
		if (obj == null){ return false;	}
		if(!(obj instanceof ScheduleInfo)){return false;}
		ScheduleInfo si = (ScheduleInfo) obj;
		if(CompareUtils.compare(si.getId(), this.getId(), true))		
		if(CompareUtils.compare(si.getObjectID(), this.getObjectID(), true))
		if (si.getDayOfMonth() != this.getDayOfMonth()){ return false;	}
		if (si.getDayOfWeek() != this.getDayOfWeek()){ return false;	}
		if (si.getHour() != this.getHour()){ return false;	}
		if (si.getMinute() != this.getMinute()){ return false;	}
		if(si.isEnable() != this.isEnable()){ return false;}
		
		return true;
	}

	public static int INTERVAL_DAY = 0;
    public static int INTERVAL_WEEK = 1;
    public static int INTERVAL_MONTH = 2;
    // Used for Run Now option
    public static int INTERVAL_ONCE = 3;
	
	private String id;
	private String objectID;
	private Boolean enable;
	private int interval;
	
	/*
	Field Name  	   	Allowed Values  	   	Allowed Special Characters
	Seconds 	  			0-59 	  					, - * /
	Minutes 	  			0-59 	  					, - * /
	Hours 	  				0-23 					  	, - * /
	Day-of-month 	  		1-31 					  	, - * ? / L W
	Month 	  				1-12 or JAN-DEC 		  	, - * /
	Day-of-Week 	  		1-7 or SUN-SAT 	 		 	, - * ? / L #
	Year (Optional) 		empty, 1970-2199 		  	, - * /
	   
	*/
		 
	private int minute; 
	private int hour; // 24 hour schedule
	private int dayOfMonth;	
	private int dayOfWeek;	
 
    // SCHEDULE INFO FORMAT PASSED VIA WEBSERVICE
	public static int LENGTH_FORMAT_NO_ID = 6;	 // interval:dayOfMonth:dayOfWeek:hour:minute:enable
	public static int LENGTH_FORMAT_WITH_ID = 7; // id:interval:dayOfMonth:dayOfWeek:hour:minute:enable
	public static String FORMAT_DELIMITER = ":";
	
    public ScheduleInfo(){    	
    }
    
    public ScheduleInfo(String id, String jobID, int interval, int dayOfWeek, int dayOfMonth, int hour, int minute, boolean enable)
    {    	    	
    	this.id = id;
    	this.objectID = jobID;
    	this.interval = interval;
    	this.dayOfWeek = dayOfWeek;
    	this.dayOfMonth = dayOfMonth;
    	this.hour = hour;
    	this.minute = minute;        	
    	this.enable = enable;
    }        
    
	public static boolean validateScheduleFormat(String schedule)
	{
		boolean valid = false;
		if(schedule != null)
		{
			String[] scheduleDetails = schedule.split(":");
			int length = scheduleDetails.length;
			if(length == LENGTH_FORMAT_NO_ID || length == LENGTH_FORMAT_WITH_ID)
			{
				valid = true;				
				for(int i = 0; i < length; i++)
				{					
					try
					{
						String detail = scheduleDetails[i];
						if(i == 0 && length == LENGTH_FORMAT_WITH_ID)
						{
							// check if the id passed is the correct UUID format
							UUID.fromString(detail);
							continue;
						}
						
						if(i == length -1)
						{
							Boolean.parseBoolean(detail);
							continue;
						}
						Integer.parseInt(detail);
					}
					catch(Exception ex)
					{
						valid = false;
						break;
					}
				}				
			}
		}
		return valid;
	}
	
	public static ScheduleInfo parseSchedule(String schedule, String objectID) throws ValidationException
	{
		List<ScheduleInfo> scheduleInfoList = parseSchedules(new String[]{schedule}, objectID);
		return scheduleInfoList != null && scheduleInfoList.size() == 1 ? scheduleInfoList.get(0) : null;
	}
	
	public static List<ScheduleInfo> parseSchedules(String[] schedules, String objectID) throws ValidationException
	{		
		List<ScheduleInfo> scheduleInfoList = null;
		if (schedules != null && schedules.length > 0)
		{
			scheduleInfoList = new ArrayList<ScheduleInfo>();
			for (String str : schedules)
			{				
				if (!validateScheduleFormat(str))
				{			
					throw new ValidationException("Invalid schedule format for " + str);					
				}
				ScheduleInfo scheduleInfo = new ScheduleInfo();
				String[] scheduleDetail = str.split(FORMAT_DELIMITER);
				int length = scheduleDetail.length;
				int i = 0;
				String uniqueID = null;
				if(length == LENGTH_FORMAT_WITH_ID)
				{				
					uniqueID = scheduleDetail[0];
					i = i+1;
				}	
				else
				{
					uniqueID = UUID.randomUUID().toString();					
				}
				
				scheduleInfo.setId(uniqueID);
				scheduleInfo.setObjectID(objectID);
				int interval = Integer.parseInt(scheduleDetail[i]);
				int dayOfMonth = Integer.parseInt(scheduleDetail[i+1]);
				int dayOfWeek = Integer.parseInt(scheduleDetail[i+2]);				
				int hour = Integer.parseInt(scheduleDetail[i+3]);
				int minute = Integer.parseInt(scheduleDetail[i+4]);
				boolean enable = Boolean.parseBoolean(scheduleDetail[i+5]);
				scheduleInfo.setInterval(interval);
				scheduleInfo.setDayOfWeek(dayOfWeek);
				scheduleInfo.setDayOfMonth(dayOfMonth);
				scheduleInfo.setHour(hour);
				scheduleInfo.setMinute(minute);
				scheduleInfo.setEnable(enable);
				
				scheduleInfoList.add(scheduleInfo);
			}
		}
		
		return scheduleInfoList;
	}
    
    
    public String getId()
	{
		return id;
	} 
		
	public void setId(String id)
	{
		UUID.fromString(id);
		this.id = id;
	}
	
	public String getObjectID()
	{
		return objectID;
	}

	public void setObjectID(String jobID)
	{
		this.objectID = jobID;
	}

	public int getInterval()
	{
		return interval;
	}

	public void setInterval(int interval)
	{
		this.interval = interval;
	}

	public int getDayOfWeek()
	{
		return dayOfWeek;
	}

	public void setDayOfWeek(int dayOfWeek)
	{
		this.dayOfWeek = dayOfWeek;
	}

	public int getDayOfMonth()
	{
		return dayOfMonth;
	}

	public void setDayOfMonth(int dayOfMonth)
	{
		this.dayOfMonth = dayOfMonth;
	}

	public int getHour()
	{
		return hour;
	}

	public void setHour(int hour)
	{
		this.hour = hour;
	}

	public int getMinute()
	{
		return minute;
	}

	public void setMinute(int minute)
	{
		this.minute = minute;
	}
	
	public boolean isEnable()
	{
		return enable;
	}

	public void setEnable(boolean enable)
	{
		this.enable = enable;
	}
	
	@Override
	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		if(id != null && id.trim().length() > 0)
		{
			sb.append("id=" + id);
			sb.append(" ");
		}
		return sb.toString();
	}	
}
