package com.birst.scheduler.shared;

public interface ExecutionLogic
{
	public void executeOperation() throws Exception;
	public String getDetails();
}
