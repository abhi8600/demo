package com.birst.scheduler.shared;

import org.apache.log4j.Logger;

public class RunNowOperation
{
	private static final Logger logger = Logger.getLogger(RunNowOperation.class);
	
	private ExecutionLogic jobObject;
	
	public RunNowOperation(){}
	public RunNowOperation(ExecutionLogic executionLogic)
	{
		this.jobObject = executionLogic;
	}
	
	public void startOperation() 
	{
		RunNowOperationThread t = new RunNowOperationThread();
		t.start();
	}
	
	private class RunNowOperationThread extends Thread 
	{	
		@Override
		public void run()
		{
			if(jobObject != null)
			{
				try
				{
					logger.debug("Starting run now operation " + jobObject.getDetails());
					jobObject.executeOperation();
					logger.debug("Finising run now operation " + jobObject.getDetails());
				} catch (Exception e)
				{
					logger.debug("Error in run now operation " + jobObject.getDetails());
					logger.error("Error while executing operation for operation ", e);
				}
			}
		}
	}
}
