package com.birst.scheduler.shared;

public class SchedulerUserBean
{

	public String userID;
	public String userName;
	public String spaceID;
	public boolean operationsMode;
	private DefaultEmailProperties reportEmailProps;
	
	public String getUserID()
	{
		return userID;
	}
	public void setUserID(String userID)
	{
		this.userID = userID;
	}
	public String getUserName()
	{
		return userName;
	}
	public void setUserName(String userName)
	{
		this.userName = userName;
	}
	public String getSpaceID()
	{
		return spaceID;
	}
	public void setSpaceID(String spaceID)
	{
		this.spaceID = spaceID;
	}
	public boolean isOperationsMode()
	{
		return operationsMode;
	}
	public void setOperationsMode(boolean operationsMode)
	{
		this.operationsMode = operationsMode;
	}
	
	@Override
	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		
		if(userID != null)
		{
			sb.append("userID="+userID);
		}
		
		if(spaceID != null)
		{
			sb.append(";");
			sb.append("spaceID=" + spaceID);
		}
		
		
		return sb.toString();
	}
	
	public void setReportEmailProperties(DefaultEmailProperties emailProps)
	{
		this.reportEmailProps = emailProps;		
	}
	
	public DefaultEmailProperties getReportEmailProperties()
	{
		return reportEmailProps;		
	}
}
