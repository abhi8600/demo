package com.birst.scheduler.shared;

public class DefaultEmailProperties
{
	
	private String from;
	private String subject;
	
	public DefaultEmailProperties(){
		
	}
	
	public DefaultEmailProperties(String from, String subject){
		this.from = from;
		this.subject = subject;
	}
	
	public String getFrom()
	{
		return from;
	}
	public void setFrom(String from)
	{
		this.from = from;
	}
	public String getSubject()
	{
		return subject;
	}
	public void setSubject(String subject)
	{
		this.subject = subject;
	}
	
	
}
