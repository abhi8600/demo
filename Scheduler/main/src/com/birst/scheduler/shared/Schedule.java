package com.birst.scheduler.shared;

import java.util.HashMap;
import java.util.Map;
import com.birst.scheduler.utils.CompareUtils;

public class Schedule
{
	
	public static final String INTERVAL_DAILY = "Daily";
	public static final String INTERVAL_WEEKLY = "Weekly";
	public static final String INTERVAL_MONTHLY = "Monthly";
	
	public static final String TIMEZONE_CENTRAL = "CST";
	public static final String TIMEZONE_PACIFIC = "PST";
	public static final String TIMEZONE_EASTERN = "EST";
	
	public static final Map<String, String> TIME_ZONE_MAP;
	static
	{
		TIME_ZONE_MAP = new HashMap<String,String>();
		TIME_ZONE_MAP.put("CST", "America/Chicago");
		TIME_ZONE_MAP.put("EST", "America/New_York");
		TIME_ZONE_MAP.put("PST", "America/Los_Angeles");
		TIME_ZONE_MAP.put("MST", "America/Denver");
	}
	
	@Override
	public boolean equals(Object obj)
	{	
		if (obj == null){ return false;	}
		if(!(obj instanceof Schedule)){return false;}
		Schedule si = (Schedule) obj;
		if(!CompareUtils.compare(si.getId(), this.getId(), true)){return false;}
		if(!CompareUtils.compare(si.getExpression(), this.getExpression(), true)){return false;}	
		if(si.isEnable() != this.isEnable()){ return false;}
		if(!CompareUtils.compare(si.getTimeZone(), this.getTimeZone(), true)){return false;}
		if(!CompareUtils.compare(si.getStartDate(), this.getStartDate(), true)){return false;}
		return true;
	}
	
	// Reserved word for expression
	public static final String SCHEDULE_NOW = "ScheduleNow";
	private String id;
	private String jobId;
	private String expression;
	private boolean enable;
	private String timeZone;
	private String ModifiedBy;
	
	// not stored in db, used at the time of adding or updating schedule
	private String startDate;
	
	public Schedule(){}
	
	public Schedule(String id, String jobId, String expression, boolean enable, String timeZone)
	{
		this.id = id;
		this.jobId = jobId;
		this.expression = expression;
		this.enable = enable;
		this.timeZone = getValidTimeZone(timeZone);
	}
	
	private String getValidTimeZone(String timeZone)
	{
		// the default is Central time
		if(timeZone == null || timeZone.length() == 0)
		{
			return Schedule.TIMEZONE_CENTRAL;
		}
		
		return timeZone;
	}
	
	public static boolean isTimeZoneValid(String timeZone)
	{
		return getTimeZoneID(timeZone) != null ? true : false;
	}
	
	public static String getTimeZoneID(String timeZone)
	{
		String value =  null;
		if(timeZone != null)
		{
			value = TIME_ZONE_MAP.get(timeZone.toUpperCase());
		}
		return value;
	}
	
	public String getId()
	{
		return id;
	}
	public void setId(String id)
	{
		this.id = id;
	}
	public String getExpression()
	{
		return expression;
	}
	public void setExpression(String expression)
	{
		this.expression = expression;
	}

	public boolean isEnable()
	{
		return enable;
	}

	public void setEnable(boolean enable)
	{
		this.enable = enable;
	}
	
	public String getJobId()
	{
		return jobId;
	}
	public void setJobId(String jobId)
	{
		this.jobId = jobId;
	}
	public String getTimeZone()
	{
		return timeZone;
	}
	public void setTimeZone(String timeZone)
	{
		this.timeZone = timeZone;
	}

	public String getModifiedBy()
	{
		return ModifiedBy;
	}

	public void setModifiedBy(String modifiedBy)
	{
		ModifiedBy = modifiedBy;
	}
	
	public boolean isScheduleNow()
	{
		return expression != null && expression.trim().length() > 0 ? Schedule.SCHEDULE_NOW.equalsIgnoreCase(expression) : false; 
	}

	public void setStartDate(String startDate)
	{
		this.startDate = startDate;
	}
	
	public String getStartDate()
	{
		return this.startDate;
	}
	
	@Override
	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		if(id != null && id.trim().length() > 0)
		{
			sb.append("id=" + id);
			sb.append(" ");
		}
		if(jobId != null && jobId.trim().length() > 0)
		{
			sb.append("jobId=" + jobId);
			sb.append(" ");
		}
		if(expression != null && expression.trim().length() > 0)
		{
			sb.append("expression=" + expression);
			sb.append(" ");
		}
		
		if(timeZone != null && timeZone.trim().length() > 0)
		{
			sb.append("timeZone=" + timeZone);
			sb.append(" ");
		}
		sb.append("enable=" + enable);
		
		return sb.toString();
	}
}
