package com.birst.scheduler.shared;

import com.birst.scheduler.webServices.response.SchedulerWebServiceResult;

public interface Operation
{
	public void addJob(SchedulerWebServiceResult result, String spaceID, String userID, String jobOptionsXmlString) throws Exception;
	public void getJobDetails(SchedulerWebServiceResult result, String spaceID, String jobID) throws Exception;
	public void updateJob(SchedulerWebServiceResult result, String spaceID, String userID, String jobID, String jobOptionsXmlString) throws Exception;
	public void removeJob(SchedulerWebServiceResult result, String spaceID, String jobID) throws Exception;
	public void getJobStatus(SchedulerWebServiceResult result, String spaceID, String jobOptionsXmlString) throws Exception;
	public void getAllJobs(SchedulerWebServiceResult result, String spaceID, String onlyCurrentExecuting, String userID, String filterOnUser) throws Exception;
	public void addScheduleToJob(SchedulerWebServiceResult result, String spaceID, String jobID, String jobOptionsXmlString) throws Exception;
	public void removeScheduleFromJob(SchedulerWebServiceResult result, String spaceID, String jobID, String scheduleID) throws Exception;
	public void runNow(SchedulerWebServiceResult result, String spaceID, String typeID) throws Exception;	
	public void pauseJob(SchedulerWebServiceResult result, String spaceID, String typeID) throws Exception;
	public void resumeJob(SchedulerWebServiceResult result, String spaceID, String typeID) throws Exception;
	public void killJob(SchedulerWebServiceResult result, String spaceID, String jobsRunStatusXmlString) throws Exception;
	
}
