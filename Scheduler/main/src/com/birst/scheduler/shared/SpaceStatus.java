package com.birst.scheduler.shared;

import java.sql.Timestamp;

import com.birst.scheduler.utils.StatusUtils;
import com.birst.scheduler.utils.StatusUtils.Step;
import com.birst.scheduler.utils.StatusUtils.StepStatus;
public class SpaceStatus
{
	
	private String id;
	private String spaceID;
	private String jobID;
	private int loadNumber;
	private String loadGroup;
	private String step;
	private int status;
	private long checkInTime;
	private Timestamp createdDate;
	private Timestamp modifiedDate;
	private boolean terminate;
	private String message;
	
	public SpaceStatus()
	{
	
	}
	
	public SpaceStatus(String id, String spaceID, String jobID, int loadNumber, String loadGroup, Step step, StepStatus stepStatus)
	{
		this(id, spaceID, jobID, loadNumber, loadGroup, step.toString(), stepStatus.getStatus());
	}
	
	public SpaceStatus(String id, String spaceID, String jobID, int loadNumber, String loadGroup, String step, int stepStatus)
	{
		this.id = id;
		this.spaceID = spaceID;
		this.jobID = jobID;
		this.loadNumber = loadNumber;
		this.loadGroup = loadGroup;
		this.step = step;
		this.status = stepStatus;
	}
	
	public String getID()
	{
		return id;
	}
	public void setID(String id)
	{
		this.id = id;
	}
	
	public String getSpaceID()
	{
		return spaceID;
	}
	public void setSpaceID(String spaceID)
	{
		this.spaceID = spaceID;
	}
	
	public String getJobID()
	{
		return jobID;
	}
	public void setJobID(String jobID)
	{
		this.jobID = jobID;
	}
	public int getLoadNumber()
	{
		return loadNumber;
	}
	public void setLoadNumber(int loadNumber)
	{
		this.loadNumber = loadNumber;
	}
	public String getLoadGroup()
	{
		return loadGroup;
	}
	public void setLoadGroup(String loadGroup)
	{
		this.loadGroup = loadGroup;
	}
	public int getStatus()
	{
		return status;
	}
	public void setStatus(int status)
	{
		this.status = status;
	}
	public long getCheckInTime()
	{
		return checkInTime;
	}
	public void setCheckInTime(long checkInTime)
	{
		this.checkInTime = checkInTime;
	}
	
	public void setStep(String step)
	{
		this.step = step;
	}
	
	public String getStep()
	{
		return step;
	}
	public Timestamp getCreatedDate()
	{
		return createdDate;
	}
	public void setCreatedDate(Timestamp createdDate)
	{
		this.createdDate = createdDate;
	}
	public Timestamp getModifiedDate()
	{
		return modifiedDate;
	}
	public void setModifiedDate(Timestamp endDate)
	{
		modifiedDate = endDate;
	}
	public boolean isTerminate()
	{
		return terminate;
	}
	public void setTerminate(boolean terminate)
	{
		this.terminate = terminate;
	}
	public String getId()
	{
		return id;
	}
	public void setId(String id)
	{
		this.id = id;
	}
	public String getMessage()
	{
		return message;
	}
	public void setMessage(String message)
	{
		this.message = message;
	}
	
	@Override
	public String toString()
	{	
		StringBuilder sb = new StringBuilder();
		if(id != null)
			sb.append("id : " + id);
		sb.append(" ");
		if(jobID != null)
			sb.append("jobId : " + jobID);
		sb.append(" ");
		if(spaceID != null)
			sb.append("space : " + spaceID);
		sb.append(" ");
		if(loadNumber > 0)
			sb.append("loadId : " + loadNumber);
		sb.append(" ");
		if(loadGroup != null)
			sb.append("loadGroup : " + loadGroup);
		sb.append(" ");
		if(step != null)
			sb.append("Step : " + step);
		return sb.toString();
	}
}
