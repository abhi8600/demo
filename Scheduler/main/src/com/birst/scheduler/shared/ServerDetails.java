package com.birst.scheduler.shared;

import java.sql.Timestamp;

public class ServerDetails
{
	
	private int schedulerServerID;
	private String name;
	private String url;
	private boolean available;
	private Timestamp createdDate;
	private Timestamp modifiedDate;
	
	public int getSchedulerServerId()
	{
		return schedulerServerID;
	}
	
	public void setSchedulerServerID(int id)
	{
		this.schedulerServerID = id;
	}
	
	public String getName()
	{
		return name;
	}
	
	public void setName(String name)
	{
		this.name = name;
	}
	public String getUrl()
	{
		return url;
	}
	
	public void setUrl(String url)
	{
		this.url = url;
	}
	
	public boolean isAvailable()
	{
		return available;
	}
	
	public void setAvailable(boolean available)
	{
		this.available = available;
	}

	public Timestamp getCreatedDate()
	{
		return createdDate;
	}

	public void setCreatedDate(Timestamp createdDate)
	{
		this.createdDate = createdDate;
	}

	public Timestamp getModifiedDate()
	{
		return modifiedDate;
	}

	public void setModifiedDate(Timestamp modifiedDate)
	{
		this.modifiedDate = modifiedDate;
	}	
}
