package com.birst.scheduler.process;

import org.apache.log4j.Logger;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import com.birst.scheduler.shared.JobStatus;
import com.birst.scheduler.utils.Util;
import com.birst.scheduler.utils.UtilConstants;

public class ProcessJob implements Job
{

	private static final Logger logger = Logger.getLogger(ProcessJob.class);
	private String runningJobId;
	
	JobStatus executionStatus;	
	
	public void execute(JobExecutionContext context) throws JobExecutionException
	{
		try
		{
			String groupName = context.getJobDetail().getKey().getGroup();
			String spaceID = groupName.substring(UtilConstants.PREFIX_PROCESS_JOB_GROUP_NAME.length());
			
			logger.debug("Process Job starting for space " + spaceID);
			runningJobId = Util.getObjectID(context.getJobDetail().getKey().getName(), UtilConstants.PREFIX_PROCESS_JOB);
			ProcessExecution processExecution = new ProcessExecution(spaceID, runningJobId, null);
			processExecution.executeOperation();
			executionStatus = processExecution.getExecutionStatus();
			logger.error("ProcessJob execution output : " + executionStatus.getJobStatus().toString());
		}
		catch(Exception ex)
		{
			logger.error("Exception while running ProcessJob" , ex);
			throw new JobExecutionException("Error while running processJob", ex.getCause());	
		}
		finally
		{
			Util.configureForRetry(context, executionStatus);
		}
	}
}
