package com.birst.scheduler.process;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.UUID;

import org.apache.log4j.Logger;
import org.quartz.JobExecutionException;
import org.quartz.UnableToInterruptJobException;

import com.birst.scheduler.exceptions.ConcurrentSpaceOperationException;
import com.birst.scheduler.exceptions.SchedulerBaseException;
import com.birst.scheduler.shared.ExecutionLogic;
import com.birst.scheduler.shared.JobStatus;
import com.birst.scheduler.shared.SpaceStatus;
import com.birst.scheduler.utils.CommandUtil;
import com.birst.scheduler.utils.ExceptionCodes;
import com.birst.scheduler.utils.RunningProcessHandler;
import com.birst.scheduler.utils.SchedulerDBConnection;
import com.birst.scheduler.utils.SchedulerDBConstants;
import com.birst.scheduler.utils.SchedulerDatabase;
import com.birst.scheduler.utils.StatusUtils;
import com.birst.scheduler.utils.Util;
import com.birst.scheduler.utils.UtilConstants;
import com.birst.scheduler.utils.StatusUtils.Step;
import com.birst.scheduler.utils.StatusUtils.StepStatus;
import com.birst.scheduler.utils.UtilConstants.RunningJobStatus;

public class ProcessExecution implements ExecutionLogic
{

	private static final Logger logger = Logger.getLogger(ProcessExecution.class);
	private String spaceID;
	private String runningJobId;
	private String processScheduleStepId;
	private String parentScheduleStepId;
	private String parentSource;
	private JobStatus executionStatus = new JobStatus();
	Map<String,Process> runningCommandProcesses = new HashMap<String,Process>();
	
	
	public ProcessExecution(){}
	public ProcessExecution(String spaceID, String runningJobId, String processScheduleStepId)
	{
		this.spaceID = spaceID;
		this.runningJobId = runningJobId;
		this.processScheduleStepId = processScheduleStepId;
	}
	
	@Override
	public void executeOperation() throws Exception
	{
		// TODO Auto-generated method stub
		Connection conn = null;
		ProcessInfo processInfo = null;
		try
		{	
			logger.debug("Process Job starting for space " + spaceID);
			runningJobId = Util.getLowerCaseString(runningJobId);
			conn = SchedulerDBConnection.getInstance().getConnectionPool().getConnection();
			String schema = UtilConstants.SCHEDULER_DB_SCHEMA;
			
			if(processScheduleStepId == null)
			{
				Map<String, List<SpaceStatus>> map = SchedulerDatabase.getSpaceStatus(conn, schema, spaceID, runningJobId, -1, null, Step.ScheduledProcess.toString(), StepStatus.RUNNING.getStatus() );
				// it should contain only one value
				SpaceStatus validSpaceStatus  = null;
				if(map.size() == 1 && map.get(runningJobId).size() == 1)
				{
					validSpaceStatus = map.get(runningJobId).get(0);
				}

				if(validSpaceStatus == null)
				{
					throw new Exception("Schedule Entry not found in " + SchedulerDBConstants.CUST_SPACE_STATUS_TABLENAME + 
							" spaceID : " + spaceID + " : jobId : " + runningJobId);
				}
				// this is the id of the schedule step which is put in by the caller during the time of 
				// adding process schedule
				processScheduleStepId = validSpaceStatus.getId();
			}
			
			processInfo = SchedulerDatabase.getProcessInfo(conn, schema, runningJobId);
			if(processInfo == null)
			{
				throw new Exception("Process Job Info not found");
			}
			
			// Make sure no other concurrent process is running
			SpaceStatus anyConcurrentSpaceStatus = StatusUtils.getActiveSpaceStatus(spaceID, false);
			boolean isConcurrentRunning = anyConcurrentSpaceStatus != null && anyConcurrentSpaceStatus.getId() != null ? true : false ;
			if(isConcurrentRunning)
			{	
				String concurrentSpaceStatusId = anyConcurrentSpaceStatus.getId();
				if(concurrentSpaceStatusId.equalsIgnoreCase(processScheduleStepId) 
					|| (parentScheduleStepId != null && concurrentSpaceStatusId.equalsIgnoreCase((String)parentScheduleStepId)))
				{
					isConcurrentRunning = false;
				}	
			}
			if(isConcurrentRunning)
			{
				logger.error("Another job is running for this space : " + anyConcurrentSpaceStatus.toString());
				executionStatus.markTheJobStatus(RunningJobStatus.FAILED, "Concurrent job running", false, StatusUtils.getConcurrentExitCodeMessage(anyConcurrentSpaceStatus));
				throw new ConcurrentSpaceOperationException("Another job is running for this space : " + anyConcurrentSpaceStatus.toString());
			}
			
			String userID = processInfo.getUserId();
			int loadID = processInfo.getLoadId();
			String loadGroup = processInfo.getLoadGroup();
			String loadDate = processInfo.getLoadDate();
			boolean sendEmail = processInfo.getSendEmail();
			String subGroups = processInfo.getSubGroups();
			if (subGroups != null && subGroups.indexOf(" ")!=-1)
			{
				String[] tSubGroups = subGroups.split(";");
				subGroups = "";
				boolean first = true;
				for(String sg : tSubGroups)
				{
					if (first)
					{
						first = false;
					}
					else
					{
						subGroups+=";";
					}
					subGroups += (sg != null && sg.indexOf(" ") != -1 && !sg.startsWith("\"") && !sg.endsWith("\"")) ? "\"" + sg + "\"" : sg;
				}
			}
			boolean retryFailed = processInfo.getRetryFailedLoad();
			
			Map<String,String> propertiesMap = Util.getSpaceInfoMap(spaceID);
			String spaceDirectory = propertiesMap != null ? propertiesMap.get(UtilConstants.KEY_SPACE_DIRECTORY) : null;
			if(spaceDirectory == null)
			{
				executionStatus.markTheJobStatus(RunningJobStatus.FAILED, "Space Directory not found", false);
				logger.info("Unable to proceed to load. Space Directory not found : " + spaceID);
				boolean isSpaceDeleted = Util.isSpaceDeleted(propertiesMap);
            	if(isSpaceDeleted)
            	{
            		logger.info("Scheduling a delet space job for " + spaceID);
            		Util.scheduleDeletedSpaceJob(spaceID);
            	}
				return;
			}
			
			Util.validateNoPublishLockFilePresent(spaceDirectory);
			buildLoadAndProcess(spaceDirectory, spaceID, userID, retryFailed, loadID, loadDate, loadGroup, subGroups, sendEmail);
			executionStatus.markTheJobStatus(RunningJobStatus.COMPLETE, null);

		}
		catch(Exception ex)
		{
			logger.error("Exception while running ProcessJob" , ex);
			executionStatus.markTheJobStatus(RunningJobStatus.FAILED, "Exception while running ProcessJob", false);
			throw new Exception("Error while running processJob", ex.getCause());	
		}
		finally
		{	
			try
			{
				if(processScheduleStepId != null)
				{
					StepStatus scheduleStepStatus = StepStatus.SUCCESS;
					if(executionStatus.getJobStatus().getStatus() != RunningJobStatus.COMPLETE.getStatus())
					{
						scheduleStepStatus = StepStatus.FAILED;
					}
					StatusUtils.updateSpaceStatus(processScheduleStepId, scheduleStepStatus.getStatus(), executionStatus.getExitErrorCodeMsg(), true);
				}
			}catch(Exception ex)
			{
				logger.error("Exception while updating scheduled status for uid : " + processScheduleStepId, ex);
			}
		}
	}

	public void buildLoadAndProcess(String spaceDirectory, String spaceID, String userID, boolean retryFailed, 
			int loadID, String loadDate, String loadGroup, 
			String subGroups, boolean sendEmail) throws SQLException, SchedulerBaseException, ClassNotFoundException, IOException, InterruptedException
	{	
		String buildStepUid = UUID.randomUUID().toString();
		
		try
		{
			int loaderExitCode = buildBeforeLoad(spaceDirectory, buildStepUid, spaceID, userID, loadID, loadDate, loadGroup, subGroups, retryFailed);
			boolean continueToLoad = CommandUtil.isSuccessfulExit(loaderExitCode);
			// path of the file containing various props like updated load id or 
			// failed property like data limit exceeded etc
			String messageFilePath = CommandUtil.getMessageFilePath(spaceDirectory, buildStepUid);
			if(continueToLoad)
			{
				// get the load number from the returned props
				String loadStepUid = UUID.randomUUID().toString();
				boolean isLoadSuccessful = load(spaceDirectory, messageFilePath, loadStepUid, spaceID, loadGroup, loadID, userID);
				if(isLoadSuccessful)
				{
					// each command to Acorn Wrapper should get a unique id
					String postLoadStepUid = UUID.randomUUID().toString();
					if(postLoadOperation(spaceDirectory, postLoadStepUid, spaceID, userID, loadGroup, loadDate, loadID))
					{
						if(sendEmail)
						{
							sendEmailPostLoad(messageFilePath, spaceID, userID, spaceDirectory, loadID, loadGroup);
						}
					}
				}
			}
			else
			{
				// loader didn't success, check the message file path if there are any reasons for failure
				// if there are send the failure email
				if(sendEmail)
				{
					String failureType = CommandUtil.getFailureType(loaderExitCode);
					this.sendFailedProcessEmail(spaceDirectory, spaceID, userID, loadID, loadGroup, failureType, null);
				}
			}
		}finally
		{
			
		}
	}

	public int buildBeforeLoad(String spaceDirectory, String uid, String spaceID, String userID, int loadID, 
			String loadDate, String loadGroup, String subGroups, boolean retryFailed) throws IOException, InterruptedException, SchedulerBaseException, ClassNotFoundException, SQLException
	{
		int exitValue = -1;
		try
		{
			StatusUtils.addSpaceStatus(uid, spaceID, runningJobId, loadID, loadGroup, StatusUtils.Step.AppLoader, StatusUtils.StepStatus.RUNNING);
			String logFile = CommandUtil.getProcessCommandLogFilePath(spaceDirectory);
			String cmd = CommandUtil.getAppLoaderCommand(logFile, uid, spaceID, userID, loadID, loadGroup, 
					loadDate, subGroups, retryFailed);
			exitValue = CommandUtil.runCommand(CommandUtil.CMD_APP_LOADER, cmd, null, runningCommandProcesses);
			//	int exitValue = CommandUtil.runTestCommand(CommandUtil.CMD_APP_LOADER, cmd, null, runningCommandProcesses);			
		}
		catch(Exception ex)
		{
			logger.error("Error in building before Load ", ex);
			exitValue = ExceptionCodes.INTERNAL_ERROR;
		}
		finally
		{
			StatusUtils.updateSpaceStatus(uid, exitValue);
		}
		
		return exitValue;
	}


	public boolean load(String spaceDirectory, String messageFilePath, String uid, String spaceID, String loadGroup, 
			int loadId, String userID) throws IOException, InterruptedException, SQLException, ClassNotFoundException, SchedulerBaseException
	{		
		int exitValue = -1;
		try
		{	
			StatusUtils.addSpaceStatus(uid, spaceID, runningJobId, loadId, loadGroup, StatusUtils.Step.Process, StatusUtils.StepStatus.RUNNING);
			// get the load number from the returned props
			//String messageFilePath = CommandUtil.getMessageFilePath(spaceDir, uid);
			StatusUtils.createCheckInFile(spaceDirectory + File.separator + UtilConstants.CHECK_IN_LOAD_FILE);
			Properties props = CommandUtil.getProps(messageFilePath);
			if(props != null)
			{
				StatusUtils.updateStatusForBirstLocal(props, runningJobId, spaceID, loadId, loadGroup);
				String engineCommand = props.getProperty(CommandUtil.KEY_ENGINE_COMMAND);
				String args = props.getProperty(CommandUtil.KEY_ENGINE_ARGS);
				/*
				String skipCheckIn = props.getProperty(CommandUtil.KEY_ENGINE_SKIP_CHECK_IN);
				if(skipCheckIn != null && skipCheckIn.equalsIgnoreCase("true"))
				{
					// this ensures that the scheduler does not rely on the timestamp to check if the status is running.
					// supports backward compatibility to pre 5.1 process versions
					StatusUtils.createCheckInFile(spaceDirectory + File.separator + UtilConstants.CHECK_IN_LOAD_FILE, -1);
				}
				*/
				if(engineCommand != null && engineCommand.trim().length() > 0
						&& args != null && args.trim().length() > 0)
				{
					engineCommand = engineCommand.replace("//", File.separator);
					args = args.replace("//", File.separator);
					String cmd = CommandUtil.getProcessCommand(engineCommand, args);
					File file = new File(engineCommand);
					if(file.exists())
					{
						String workingDirectory = file.getParent();
						RunningProcessHandler handlr = new RunningProcessHandler(spaceID, userID, spaceDirectory, cmd, loadId, loadGroup);
						exitValue = CommandUtil.runCommand(CommandUtil.CMD_LOAD, cmd, workingDirectory, runningCommandProcesses, handlr);
					}
				}
			}
		}
		catch(Exception ex)
		{
			logger.error("Exception during load command ", ex);
			exitValue = ExceptionCodes.INTERNAL_ERROR;
		}
		finally
		{
			StatusUtils.updateSpaceStatus(uid, exitValue);
		}
		
		return CommandUtil.isSuccessfulExit(exitValue);
	}
	
	public boolean postLoadOperation(String spaceDirectory, String uid, String spaceID, String userID, 
			String loadGroup, String loadDate, int loadID) throws IOException, InterruptedException, SchedulerBaseException, ClassNotFoundException, SQLException
	{
		
		int exitValue = -1;
		try
		{
			StatusUtils.addSpaceStatus(uid, spaceID, runningJobId, loadID, loadGroup, StatusUtils.Step.PostProcess, StatusUtils.StepStatus.RUNNING);
			String logFile = CommandUtil.getProcessCommandLogFilePath(spaceDirectory);
			String cmd = CommandUtil.getPostLoadCommand(logFile, uid, spaceID, userID, loadGroup, loadDate, loadID);
			exitValue = CommandUtil.runCommand(CommandUtil.CMD_POST_LOAD, cmd, null, runningCommandProcesses);
		}
		catch(Exception ex)
		{
			logger.error("Exception in post load operation ", ex);
			exitValue = ExceptionCodes.INTERNAL_ERROR;
		}
		finally
		{
			StatusUtils.updateSpaceStatus(uid, exitValue);
		}
		
		return CommandUtil.isSuccessfulExit(exitValue);
	}
	
	public boolean sendEmailPostLoad(String outputFileDuringInitLoader,
			String spaceID, String userID, String spaceDirectory, int loadID, String loadGroup) throws SchedulerBaseException, ClassNotFoundException, SQLException
	{
		
		int exitValue = -1;
		String uid = null;
		try
		{	
			uid = UUID.randomUUID().toString();
			StatusUtils.addSpaceStatus(uid, spaceID, runningJobId, loadID, loadGroup, StatusUtils.Step.SendEmail, StatusUtils.StepStatus.RUNNING);
			List<String> liveAccessGroups = new ArrayList<String>();
			Properties props = CommandUtil.getProps(outputFileDuringInitLoader);
			if(props != null)
			{	
				String birstLocalGroups = props.getProperty(CommandUtil.KEY_LOADER_BIRST_LOCAL_GROUPS);
				if(birstLocalGroups != null && birstLocalGroups.trim().length() > 0)
				{
					for(String localGroup : liveAccessGroups)
					{
						liveAccessGroups.add(localGroup);
					}
				}
			}

			String logFile = CommandUtil.getProcessCommandLogFilePath(spaceDirectory);
			String cmd = CommandUtil.getEmailForSuccessLoadCommand(logFile, uid, spaceID, userID, Util.useSSLForEmail(), loadID, loadGroup, liveAccessGroups, parentSource);
			exitValue = CommandUtil.runCommand(CommandUtil.CMD_LOAD_EMAIL, cmd, null, runningCommandProcesses);
		}
		catch(Exception ex)
		{
			logger.error("Exception in post load operation ", ex);
			exitValue = ExceptionCodes.INTERNAL_ERROR;
		}
		finally
		{
			StatusUtils.updateSpaceStatus(uid, exitValue);
		}
		
		return CommandUtil.isSuccessfulExit(exitValue);
	}
	
	public boolean sendFailedProcessEmail(String spaceDirectory, String spaceID, String userID,
			int loadID, String loadGroup, String failureType, String failureDetail) throws SchedulerBaseException, ClassNotFoundException, SQLException
	{
		return sendFailedProcessEmail(spaceDirectory, spaceID, userID, loadID, loadGroup, failureType, failureDetail, null);
	}
	
	public boolean sendFailedProcessEmail(String spaceDirectory, String spaceID, String userID,
			int loadID, String loadGroup, String failureType, String failureDetail, String parentSource) throws SchedulerBaseException, ClassNotFoundException, SQLException
	{
		int exitValue = -1;
		String uid = null;
		try
		{
			uid = UUID.randomUUID().toString();
			StatusUtils.addSpaceStatus(uid, spaceID, runningJobId, loadID, loadGroup, StatusUtils.Step.SendEmail, StatusUtils.StepStatus.RUNNING);
			String logFile = CommandUtil.getProcessCommandLogFilePath(spaceDirectory);
			String cmd = CommandUtil.getEmailForFailedLoadCommand(logFile, uid, spaceID, userID, Util.useSSLForEmail(), failureType, failureDetail, parentSource);
			exitValue = CommandUtil.runCommand(CommandUtil.CMD_LOAD_EMAIL, cmd, null, runningCommandProcesses);

			
		}
		catch(Exception ex)
		{
			logger.error("Exception in post load operation ", ex);
			exitValue = ExceptionCodes.INTERNAL_ERROR;
		}
		finally
		{
			StatusUtils.updateSpaceStatus(uid, exitValue);
		}
		
		return CommandUtil.isSuccessfulExit(exitValue);
	}
		

	@Override
	public String getDetails()
	{
		StringBuilder sb = new StringBuilder();
		sb.append("ProcessExecution");
		sb.append(" : ");
		sb.append("spaceID=");
		sb.append(spaceID);
		sb.append(" : ");
		sb.append("jobId=");
		sb.append(runningJobId);
		sb.append(" : ");
		sb.append("scheduleId=");
		sb.append(processScheduleStepId);
		return sb.toString();
	}
	public void setParentScheduleId(String parentScheduleStepID)
	{
		this.parentScheduleStepId = parentScheduleStepID;
		
	}
	
	public void setParentSource(String parentSource) {
		this.parentSource = parentSource;
	}
	
	public JobStatus getExecutionStatus()
	{
		return executionStatus;
	}
	

}
