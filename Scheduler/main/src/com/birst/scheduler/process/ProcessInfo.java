package com.birst.scheduler.process;

import java.sql.Timestamp;

public class ProcessInfo
{
	private String id;
	private String spaceId;
	private String userId;
	private int loadId;
	private String loadDate;
	private String loadGroup;
	// Comma separated list
	private String subGroups;
	private boolean retryFailedLoad;
	

	private boolean sendEmail;
	private Timestamp CreatedDate;
	private Timestamp ModifiedDate;
	
	public ProcessInfo()
	{
		
	}

	public ProcessInfo(String id, String spaceId, String userId, int loadId, String loadDate, String loadGroup, 
			String subGroups, boolean retryFailedLoad, boolean sendEmail)
	{
		this.id = id;
		this.spaceId = spaceId;
		this.userId = userId;
		this.loadId = loadId;
		this.loadDate = loadDate;
		this.loadGroup = loadGroup;
		this.subGroups = subGroups;
		this.retryFailedLoad = retryFailedLoad;
		this.sendEmail = sendEmail;		
	}
	
	public String getId()
	{
		return id;
	}
	public String getSpaceId()
	{
		return spaceId;
	}
	public int getLoadId()
	{
		return loadId;
	}	
	public String getUserId()
	{
		return userId;
	}

	public String getLoadGroup()
	{
		return loadGroup;
	}
	
	public Timestamp getCreatedDate()
	{
		return CreatedDate;
	}
	public Timestamp getModifiedDate()
	{
		return ModifiedDate;
	}
	public void setId(String id)
	{
		this.id = id;
	}
	public void setSpaceId(String spaceId)
	{
		this.spaceId = spaceId;
	}
	public void setUserId(String userId)
	{
		this.userId = userId;
	}	
	public void setLoadId(int loadId)
	{
		this.loadId = loadId;
	}
	
	public void setLoadGroup(String loadGroup)
	{
		this.loadGroup = loadGroup;
	}
	
	public void setCreatedDate(Timestamp createdDate)
	{
		CreatedDate = createdDate;
	}
	public void setModifiedDate(Timestamp modifiedDate)
	{
		ModifiedDate = modifiedDate;
	}

	public String getLoadDate()
	{
		return loadDate;
	}

	public String getSubGroups()
	{
		return subGroups;
	}

	public void setLoadDate(String loadDate)
	{
		this.loadDate = loadDate;
	}

	public void setSubGroups(String subGroups)
	{
		this.subGroups = subGroups;
	}
	
	public boolean getSendEmail()
	{
		return sendEmail;
	}
	
	public void setSendEmail(boolean sendEmail)
	{
		this.sendEmail = sendEmail;
	}
	
	public boolean getRetryFailedLoad()
	{
		return retryFailedLoad;
	}

	public void setRetryFailedLoad(boolean retryFailedLoad)
	{
		this.retryFailedLoad = retryFailedLoad;
	}
}
