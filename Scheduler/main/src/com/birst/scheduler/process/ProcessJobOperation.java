package com.birst.scheduler.process;

import java.io.StringReader;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

import javax.xml.namespace.QName;
import javax.xml.stream.FactoryConfigurationError;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
import org.apache.axiom.om.OMText;
import org.apache.axiom.om.impl.builder.StAXOMBuilder;
import org.apache.log4j.Logger;
import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;

import com.birst.scheduler.SchedulerServerContext;
import com.birst.scheduler.exceptions.ConcurrentSpaceOperationException;
import com.birst.scheduler.exceptions.JobRunningException;
import com.birst.scheduler.exceptions.SchedulerBaseException;
import com.birst.scheduler.shared.Operation;
import com.birst.scheduler.shared.RunNowOperation;
import com.birst.scheduler.shared.SpaceStatus;
import com.birst.scheduler.utils.AxisXmlUtils;
import com.birst.scheduler.utils.JobConstants;
import com.birst.scheduler.utils.SchedulerDBConnection;
import com.birst.scheduler.utils.SchedulerDatabase;
import com.birst.scheduler.utils.StatusUtils;
import com.birst.scheduler.utils.Util;
import com.birst.scheduler.utils.UtilConstants;
import com.birst.scheduler.utils.StatusUtils.Step;
import com.birst.scheduler.utils.StatusUtils.StepStatus;
import com.birst.scheduler.webServices.XMLWebServiceResult;
import com.birst.scheduler.webServices.response.SchedulerWebServiceResult;

public class ProcessJobOperation implements Operation
{
	private static final Logger logger = Logger.getLogger(ProcessJobOperation.class);

	@SuppressWarnings("unchecked")
	public static ProcessInfo parseProcessJobXmlString(String spaceID, String userID, String jobOptionsXmlString)
	{
		ProcessInfo processInfo = null;

		if(jobOptionsXmlString == null || jobOptionsXmlString.trim().length() == 0)
		{
			processInfo = new ProcessInfo();
			processInfo.setSpaceId(spaceID);
			processInfo.setUserId(userID);
			processInfo.setLoadId(-1);
			return processInfo;
		}
		XMLStreamReader reader = null;

		try
		{	
			reader = XMLInputFactory.newInstance().createXMLStreamReader(new StringReader(jobOptionsXmlString));
			StAXOMBuilder builder = new StAXOMBuilder(reader);
			OMElement docElement = builder.getDocumentElement();
			docElement.build();
			docElement.detach();

			OMElement element = docElement.getFirstChildWithName(new QName("", JobConstants.JOB_PR_NODE));
			// see if the root element itself is ScheduledReport
			if(element == null)
			{
				if (docElement.getLocalName() == JobConstants.JOB_PR_NODE)
				{
					element = docElement;
				}
			}
			if(element != null)
			{
				processInfo = new ProcessInfo();
				processInfo.setSpaceId(spaceID);
				processInfo.setUserId(userID);
				Iterator<OMElement> iterator = element.getChildElements();
				while(iterator.hasNext())
				{
					OMElement childElement = iterator.next();
					String elementName = childElement.getLocalName();
					String elementText = childElement.getText();					

					if(elementText == null || elementText.trim().length() == 0)
					{
						continue;
					}

					if(JobConstants.JOB_SP_LOAD_ID.equals(elementName))
					{	
						processInfo.setLoadId(Integer.parseInt(elementText));
					}
					
					if(JobConstants.JOB_SP_LOAD_GROUP.equals(elementName))
					{
						processInfo.setLoadGroup(elementText);
					}

					if(JobConstants.JOB_SP_LOADDATE.equals(elementName))
					{
						processInfo.setLoadDate(elementText);
					}

					if(JobConstants.JOB_SP_SUBGROUPS.equals(elementName))
					{
						processInfo.setSubGroups(elementText);
					}
					
					if(JobConstants.JOB_SP_RETRY_FAILED_LOAD.equals(elementName))
					{
						processInfo.setRetryFailedLoad(Boolean.parseBoolean(elementText));
					}
					
					if(JobConstants.JOB_SEND_EMAIL.equals(elementName))
					{	
						processInfo.setSendEmail(Boolean.parseBoolean(elementText));
					}
				}
			}

		} catch (XMLStreamException e)
		{
			logger.error(e,e);
		} catch (FactoryConfigurationError e)
		{
			logger.error(e,e);
		}
		return processInfo;
	}
	
	public static ProcessInfo addProcessJob(String spaceID, String userID, int loadID, String loadDate, 
			String loadGroup, String subGroups, String ignoreSpaceStatusScheduleId, 
			boolean retryFailedLoad, boolean sendEmail) throws SQLException, SchedulerBaseException, ClassNotFoundException, SchedulerException
	{
		return addProcessJob(spaceID, userID, loadID, loadDate, loadGroup, subGroups, ignoreSpaceStatusScheduleId, retryFailedLoad, sendEmail, null);
	}

	public static ProcessInfo addProcessJob(String spaceID, String userID, int loadID, String loadDate, 
			String loadGroup, String subGroups, 
			boolean retryFailedLoad, boolean sendEmail) throws SQLException, SchedulerBaseException, ClassNotFoundException, SchedulerException
	{
		return addProcessJob(spaceID, userID, loadID, loadDate, loadGroup, subGroups, null, retryFailedLoad, sendEmail, null);
	}
	
	public static ProcessInfo addProcessJob(String spaceID, String userID, int loadID, String loadDate, String loadGroup, String subGroups, 
			String ignoreSpaceStatusScheduleId, boolean retryFailedLoad, boolean sendEmail, String parentSource) throws SQLException, SchedulerBaseException, ClassNotFoundException, SchedulerException
	{
		Connection conn = null;
		try
		{
			SpaceStatus anyConcurrentSpaceStatus = StatusUtils.getActiveSpaceStatus(spaceID, false);
			if(anyConcurrentSpaceStatus != null && !anyConcurrentSpaceStatus.getId().equalsIgnoreCase(ignoreSpaceStatusScheduleId))
			{
				throw new ConcurrentSpaceOperationException("Another job is running for this space : " + anyConcurrentSpaceStatus.toString());
			}

			String jobId = UUID.randomUUID().toString();
			//Scheduler scheduler = SchedulerServerContext.getSchedulerInstance();
			conn = SchedulerDBConnection.getInstance().getConnectionPool().getConnection();
			String schema = UtilConstants.SCHEDULER_DB_SCHEMA;

			// Clear up old completed/failed steps for this loadID and loadGroup
			SchedulerDatabase.deleteSpaceStatus(conn, schema, spaceID, loadID, loadGroup);
			SchedulerDatabase.removeProcessInfo(conn, schema, spaceID, loadID, loadGroup);
/*
			JobDetail jobDetail = Util.getBirstJobBuilder(ProcessJob.class, 
					Util.getCustProcessJobName(jobId), 
					Util.getCustJobGroupNameForProcess(spaceID), false).build();
			*/

			ProcessInfo processInfo = new ProcessInfo(jobId, spaceID, userID, loadID, loadDate, loadGroup, subGroups, 
					retryFailedLoad, sendEmail);
			SchedulerDatabase.addProcessInfo(conn, schema, processInfo);
			SpaceStatus spaceStatus = new SpaceStatus(UUID.randomUUID().toString(), spaceID, jobId, loadID, loadGroup, Step.ScheduledProcess, StepStatus.RUNNING ); 
			SchedulerDatabase.addSpaceStatus(conn, schema, spaceStatus);
			ProcessExecution processExecution = new ProcessExecution(spaceID, processInfo.getId(), spaceStatus.getId());
			if(ignoreSpaceStatusScheduleId != null && ignoreSpaceStatusScheduleId.trim().length() > 0)
			{
				processExecution.setParentScheduleId(ignoreSpaceStatusScheduleId);
			}
			if (parentSource != null) {
				processExecution.setParentSource(parentSource);
			}
			RunNowOperation runNowOperation = new RunNowOperation(processExecution);
			runNowOperation.startOperation();
			/*Trigger trigger = Util.getImmediateTrigger(spaceID, jobDetail, UUID.randomUUID().toString());
			trigger.getJobDataMap().put(UtilConstants.TRIGGER_MAP_SCHEDULE_STEP, spaceStatus.getId());
			if(ignoreSpaceStatusScheduleId != null && ignoreSpaceStatusScheduleId.trim().length() > 0)
			{
				trigger.getJobDataMap().put(UtilConstants.TRIGGER_MAP_IGNORE_SCHEDULE_STEP, ignoreSpaceStatusScheduleId);
			}
			
			scheduler.scheduleJob(jobDetail, trigger);*/
			return SchedulerDatabase.getProcessInfo(conn, schema, jobId);
		}
		finally
		{
			if(conn != null)
			{
				try
				{
					conn.close();
				}catch(Exception ex)
				{
					logger.warn("Exception in closing connection ",ex);
				}
			}
		}
	}

	public static void removeProcessJob(String processJobId) throws SQLException, SchedulerBaseException, ClassNotFoundException, SchedulerException
	{
		Connection conn = null;
		try
		{
			conn = SchedulerDBConnection.getInstance().getConnectionPool().getConnection();
			String schema = UtilConstants.SCHEDULER_DB_SCHEMA;
			ProcessInfo processInfo = SchedulerDatabase.getProcessInfo(conn, schema, processJobId);
			if(processInfo == null)
			{
				return;
			}

			String jobName = Util.getCustProcessJobName(processInfo.getId());
			String groupName = processInfo.getSpaceId();
			List<String> runningJobs = Util.getRunningJobs(jobName, groupName);
			if(runningJobs != null && runningJobs.size() > 0)
			{
				// schedule is in progress. Cannot remove process info
				throw new JobRunningException("Process currently running " + processJobId );
			}

			Scheduler scheduler = SchedulerServerContext.getSchedulerInstance();			
			scheduler.deleteJob(new JobKey(jobName, processInfo.getSpaceId()));
			SchedulerDatabase.removeProcessInfo(conn, schema, processInfo.getId());		
		}
		finally
		{
			if(conn != null)
			{
				try
				{
					conn.close();
				}catch(Exception ex)
				{
					logger.warn("Exception in closing connection ",ex);
				}
			}
		}
	}

	public static SpaceStatus getProcessStatus(String spaceID, int loadID, String loadGroup) throws Exception
	{	
		return StatusUtils.getJobStatus(spaceID, loadID, loadGroup);
	}

	@Override
	public void addJob(SchedulerWebServiceResult response, String spaceID,
			String userID, String jobOptionsXmlString) throws Exception
	{
		ProcessInfo processInfo = ProcessJobOperation.parseProcessJobXmlString(spaceID, userID, jobOptionsXmlString);
		ProcessInfo pr = ProcessJobOperation.addProcessJob(spaceID, userID, processInfo.getLoadId(), processInfo.getLoadDate(), processInfo.getLoadGroup(), 
				processInfo.getSubGroups(), processInfo.getRetryFailedLoad(), processInfo.getSendEmail());
		OMFactory factory = OMAbstractFactory.getOMFactory();
		OMNamespace ns = null;			
		OMElement jobIdElement = factory.createOMElement(JobConstants.JOB_ID, ns);
		OMText text = factory.createOMText(pr.getId());
		jobIdElement.addChild(text);
		response.setResult(new XMLWebServiceResult(jobIdElement));		
	}

	@Override
	public void addScheduleToJob(SchedulerWebServiceResult result,
			String spaceID, String jobID, String jobOptionsXmlString) throws Exception
	{

	}

	@Override
	public void getAllJobs(SchedulerWebServiceResult result, String spaceID,
			String onlyCurrentExecuting, String userID, String filterOnUser) throws Exception
	{

	}

	@Override
	public void getJobDetails(SchedulerWebServiceResult result, String spaceID, String jobID) throws Exception 
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void getJobStatus(SchedulerWebServiceResult response, String spaceID, String jobOptionsXmlString) throws Exception
	{
		OMFactory factory = OMAbstractFactory.getOMFactory();
		OMNamespace ns = null;
		OMElement status = factory.createOMElement(JobConstants.JOB_SP_STATUS_NODE, ns);

		ProcessInfo processInfo = ProcessJobOperation.parseProcessJobXmlString(spaceID, null, jobOptionsXmlString);
		SpaceStatus spaceStatus = StatusUtils.getJobStatus(spaceID, processInfo.getLoadId() > 0 ? processInfo.getLoadId() : -1,
				processInfo.getLoadGroup());

		// based on the space status mark it as available for operation
		boolean available = false;
		if(spaceStatus != null)
		{
			if(spaceStatus.getStatus() != StepStatus.RUNNING.getStatus())
			{
				available = true;
			}
			/*
			// check for failed status first. If any step is failed means its available
			if(spaceStatus.getStatus() != StepStatus.FAILED.getStatus() || spaceStatus.getStatus() == StepStatus.DEAD.getStatus())
			{
				available = true;
			}  
			else if(spaceStatus.getStatus() != StepStatus.RUNNING.getStatus() 
					&& Util.getLowerCaseString(StatusUtils.Step.PostProcess.toString()).equals(Util.getLowerCaseString(spaceStatus.getStep())))
			{
				// if no failed status, make sure that the last UpdatePublish step is done. We do not check
				// for just RUNNING status because there could be a little time between 2 consecutive step insertions
				// where db might return NON running status. Want to avoid that use case
				available = true;
			}		
			*/
			AxisXmlUtils.addContent(factory, status, JobConstants.JOB_SP_AVAILABLE, available, ns);
			AxisXmlUtils.addContent(factory, status, JobConstants.JOB_SP_STATUS, spaceStatus.getStatus(), ns);
			AxisXmlUtils.addContent(factory, status, JobConstants.JOB_SP_STEP, spaceStatus.getStep(), ns);
			AxisXmlUtils.addContent(factory, status, JobConstants.JOB_SP_LOAD_ID, spaceStatus.getLoadNumber(), ns);
			AxisXmlUtils.addContent(factory, status, JobConstants.JOB_SP_LOAD_GROUP, spaceStatus.getLoadGroup(), ns);
			AxisXmlUtils.addContent(factory, status, JobConstants.JOB_SP_CREATED_DATE, spaceStatus.getCreatedDate(), "MM/dd/yyyy HH:mm:ss", ns);
			AxisXmlUtils.addContent(factory, status, JobConstants.JOB_SP_MODIFIED_DATE, spaceStatus.getModifiedDate(), "MM/dd/yyyy HH:mm:ss", ns);
		}
		else
		{
			available = true;
			AxisXmlUtils.addContent(factory, status, JobConstants.JOB_SP_AVAILABLE, available, ns);
		}

		response.setResult(new XMLWebServiceResult(status));

	}

	@Override
	public void killJob(SchedulerWebServiceResult result, String spaceID, String jobsRunStatusXmlString) throws Exception
	{	
		SpaceStatus spaceStatus = StatusUtils.getJobStatus(spaceID, -1, null);
		if(spaceStatus != null && spaceStatus.getStatus() == StepStatus.RUNNING.getStatus())
		{
			Connection conn = null;
			try
			{
				conn = SchedulerDBConnection.getInstance().getConnectionPool().getConnection();
				String schema = UtilConstants.SCHEDULER_DB_SCHEMA;
				// mark the load as killed
				SchedulerDatabase.updateSpaceStatus(conn, schema, spaceStatus.getId(), StepStatus.DEAD.getStatus(), "Marking killed", true);
				SchedulerDatabase.updateSpaceStatus(conn, schema, spaceStatus.getJobID(), Step.ScheduledProcess.toString(), StepStatus.RUNNING.getStatus(), StepStatus.DEAD.getStatus(), "Marking killed", true);
			}
			finally
			{
				try
				{
					if (conn != null)
						conn.close();
				} catch (Exception ex)
				{
					logger.warn("Exception in closing db conn", ex);
				}
			}
		}
	}

	@Override
	public void pauseJob(SchedulerWebServiceResult result, String spaceID,	String typeID) throws Exception
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void removeJob(SchedulerWebServiceResult result, String spaceID, String jobID) throws Exception
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void removeScheduleFromJob(SchedulerWebServiceResult result, String spaceID, String jobID, String scheduleID)
	throws Exception
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void resumeJob(SchedulerWebServiceResult result, String spaceID,
			String typeID) throws Exception
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void runNow(SchedulerWebServiceResult result, String spaceID, String typeID) throws Exception
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void updateJob(SchedulerWebServiceResult result, String spaceID,
			String userID, String jobID, String jobOptionsXmlString)
	throws Exception
	{
		// TODO Auto-generated method stub

	}
	
}
