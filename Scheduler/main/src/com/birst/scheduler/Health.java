package com.birst.scheduler;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;

/**
 * This servlet is used to check the basic health of the tomcat server 
 */

public class Health extends HttpServlet {

	private static final long serialVersionUID = 1L;

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException 
	{
		response.setContentType("text/plain");
		PrintWriter writer = response.getWriter();
		writer.write("success");
		writer.flush();
		writer.close();
	}
}