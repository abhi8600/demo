package com.birst.scheduler.Migration;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.quartz.SchedulerException;

import com.birst.scheduler.Report.ScheduledReport;
import com.birst.scheduler.Report.ScheduledReportOperation;
import com.birst.scheduler.exceptions.SchedulerBaseException;
import com.birst.scheduler.shared.Schedule;
import com.birst.scheduler.utils.AcornAdminDBConnection;
import com.birst.scheduler.utils.AdminDatabase;
import com.birst.scheduler.utils.SchedulerDBConnection;
import com.birst.scheduler.utils.SchedulerDatabase;
import com.birst.scheduler.utils.UtilConstants;

public class MigrationUtils
{	
	private static final Logger logger = Logger.getLogger(MigrationUtils.class);
	private static final int INTERVAL_DAILY = 0;
	private static final int INTERVAL_WEEKLY = 1;
	private static final int INTERVAL_MONTHLY = 2;
	
	public static final String MIGRATED_SCHEDULED_REPORTS = "Migrated Report";
	
	public static final String MIGRATION_JOB_GROUP = "Migration";
	public static final String MIGRATION_JOB_TYPE_REPORT = "MigrationReportJob";
	
	public static final String MIGRATION_TRIGGER_GROUP = "Migration";
	public static final String MIGRATION_TRIGGER_TYPE_REPORT = "MigrateReportsTrigger";
	
	public static enum MigrationStatus
	{
		None, Running, Completed, Failed;
	}
	
	public static enum MigrationType
	{
		REPORT, SFDCEXTRACTION, PROCESSING
	}
	
	public static String getCron(int interval, int dayOfWeek, int dayOfMonth,
			int hour, int minute)
	{	
		String expression =  " 0 " +  minute + " " + hour + " " + 
		getCronDayOfMonth(interval, dayOfMonth) + " " + 
		getCronMonth() + " " + 
		getCronDayOfWeek(interval, dayOfWeek) + " " +
		"*";
		return expression;
	}
	
	private static String getCronDayOfMonth(int interval, int dayOfMonth)
	{
		String cronDayOfMonth = null;
		if(interval == INTERVAL_DAILY || interval == INTERVAL_WEEKLY)			
		{
			cronDayOfMonth = "?";			
		}
		
		if(interval == INTERVAL_MONTHLY)				
		{				
			cronDayOfMonth = String.valueOf(dayOfMonth);			
		}
		return cronDayOfMonth;
		 
	}
	
	private static String getCronMonth()
	{
		return "*";
	}
	
	private static String getCronDayOfWeek(int interval, int dayOfWeek)
	{
		String cronDayOfWeek = null;
		if(interval == INTERVAL_DAILY)				
		{
			cronDayOfWeek = "*";
		}
		
		if(interval == INTERVAL_WEEKLY)				
		{
			int dw = getNewFormatDayOfWeek(dayOfWeek);
			if(dw != -1)
			{
				cronDayOfWeek = String.valueOf(dw);
			}
		}
		
		if(interval == INTERVAL_MONTHLY)				
		{
			cronDayOfWeek = "?";
		}
		
		return cronDayOfWeek;
	}
	
	private static int getNewFormatDayOfWeek(int dayOfWeek)
	{
		if(dayOfWeek == 0) {return 1;}
		if(dayOfWeek == 1) {return 2;}
		if(dayOfWeek == 2) {return 3;}
		if(dayOfWeek == 4) {return 4;}
		if(dayOfWeek == 8) {return 5;}
		if(dayOfWeek == 16) {return 6;}
		if(dayOfWeek == 32) {return 7;}
		return -1;
	}
	
	
	/**
	 * This function migrates the scheduled reports from Acorn - Load_Schedule table
	 * and adds to the Birst Scheduler database. Note that it does not enable these migrated reports
	 * The migrated reports can be enabled via another function to provide flexibility
	 * @throws Exception
	 */
	public static void migrateOldScheduledReports() throws Exception
	{
		Connection adminDBConn = null;		
		Connection schedulerDBConn = null;
		try
		{	
			String adminDBSchema = AdminDatabase.ADMIN_DB_SCHEMA;			
			adminDBConn = AcornAdminDBConnection.getInstance().getConnectionPool().getConnection();
			Map<String, List<ScheduledReport>> spaceIDToScheduledReportMap = AdminDatabase.getOldFormatScheduledReport(adminDBConn, adminDBSchema);
			if(spaceIDToScheduledReportMap != null && spaceIDToScheduledReportMap.size() > 0)
			{
				Set<Map.Entry<String, List<ScheduledReport>>> entrySet = spaceIDToScheduledReportMap.entrySet();
				for(Map.Entry<String, List<ScheduledReport>> entry : entrySet)
				{
					String spaceID = entry.getKey();
					List<ScheduledReport> reportList = entry.getValue();
					List<ScheduledReport> toAddReportsList = new ArrayList<ScheduledReport>();
					List<ScheduledReport> toUpdateReportsList = new ArrayList<ScheduledReport>();
					
					schedulerDBConn = SchedulerDBConnection.getInstance().getConnectionPool().getConnection();
					String schedulerDBSchema = UtilConstants.SCHEDULER_DB_SCHEMA;
					if(reportList != null && reportList.size() > 0)
					{
						for(ScheduledReport report : reportList)
						{							
							report.setComments(MigrationUtils.MIGRATED_SCHEDULED_REPORTS);
							ScheduledReport existingReport = SchedulerDatabase.getScheduledReport(schedulerDBConn, schedulerDBSchema, report.getId());
							if(existingReport == null)
							{
								logger.debug("Adding Scheduled Report for migration" + report.getId());
								toAddReportsList.add(report);
							}
							else
							{
								logger.debug("Updating already migrating scheduled report" + report.getId());
								toUpdateReportsList.add(report);
							}
						}
						
						for(ScheduledReport toAddReport : toAddReportsList)
						{							
							ScheduledReportOperation.addReport(spaceID, toAddReport);
						}
						
						for(ScheduledReport toUpdateReport : toUpdateReportsList)
						{
							ScheduledReportOperation.updateReport(spaceID, toUpdateReport);
						}
					}
				}
			}			
		}
		finally
		{
			if(adminDBConn != null)
			{
				try
				{
					adminDBConn.close();
				}catch(Exception ex){}
			}
			if(schedulerDBConn != null)
			{
				try
				{
					schedulerDBConn.close();
				}catch(Exception ex){}
			}
		}
	}
	
	public static void enableAllMigratedReports() throws SQLException, SchedulerBaseException, ClassNotFoundException, SchedulerException, ParseException
	{
		List<ScheduledReport> migratedReports = getMigratedReports();
		if(migratedReports != null && migratedReports.size() > 0)
		{
			for(ScheduledReport report : migratedReports)
			{
				Schedule[] jobSchedules = report.getJobSchedules();
				if(jobSchedules != null && jobSchedules.length > 0)
				{
					for(Schedule jobSchedule : jobSchedules)
					{
						jobSchedule.setEnable(true);
						ScheduledReportOperation.updateReport(report.getSpaceID(), report);
					}
				}
			}
		}
	}
	
	public static void disableAllMigratedReports() throws SQLException, SchedulerBaseException, ClassNotFoundException, SchedulerException, ParseException
	{
		List<ScheduledReport> migratedReports = getMigratedReports();
		if(migratedReports != null && migratedReports.size() > 0)
		{
			for(ScheduledReport report : migratedReports)
			{
				Schedule[] jobSchedules = report.getJobSchedules();
				if(jobSchedules != null && jobSchedules.length > 0)
				{
					for(Schedule jobSchedule : jobSchedules)
					{
						jobSchedule.setEnable(false);
						ScheduledReportOperation.updateReport(report.getSpaceID(), report);
					}
				}
			}
		}
	}
	
	public static List<ScheduledReport> getMigratedReports() throws SQLException, SchedulerBaseException, ClassNotFoundException
	{
		PreparedStatement pstmt = null;
		Connection conn = null;
		List<ScheduledReport> reportList = new ArrayList<ScheduledReport>();
		try
		{
			conn = SchedulerDBConnection.getInstance().getConnectionPool().getConnection();
			String schedulerSchema = UtilConstants.SCHEDULER_DB_SCHEMA;
			pstmt =conn.prepareStatement("SELECT ID, SPACE_ID, USER_ID, ReportPath, TriggerReportPath, ToReportPath, " +
					"Type, Subject, Body, List, Comments FROM " + 
					SchedulerDatabase.getQualifiedTableName(schedulerSchema, SchedulerDatabase.SCHEDULED_REPORTS_TABLENAME) + 
					" WHERE Comments='" + MigrationUtils.MIGRATED_SCHEDULED_REPORTS + "'" );
			ResultSet set = pstmt.executeQuery();
			while (set.next())
			{
				ScheduledReport sr = new ScheduledReport();
				String srID = (String) set.getObject(1);	
				String spaceID = (String) set.getObject(2);
				String userID = (String) set.getObject(3);
				String reportPath = set.getString(4);
				String triggerReportPath = set.getString(5);
				String toReportPath = set.getString(6);
				String type = set.getString(7);
				String subject = set.getString(8);
				String emailBody = set.getString(9);
				String emailList = set.getString(10);
				String[] emails = emailList.split(";");
				String comments = set.getString(11);
				
				List<Schedule> scheduleList = SchedulerDatabase.getScheduleList(conn, schedulerSchema, srID);
				Schedule[] jobSchedules = null;
				if(scheduleList != null && scheduleList.size() > 0)
				{
					jobSchedules = scheduleList.toArray(new Schedule[scheduleList.size()]);
				}
				
				sr = new ScheduledReport(srID, spaceID, userID, reportPath, triggerReportPath, toReportPath, 
						type, subject, emailBody , emails, comments, null, null, null, null, null, null, null, null, true);				
				sr.setJobSchedules(jobSchedules);
				reportList.add(sr);
			}
			return reportList;
		}
		finally
		{
			try
			{
				if (pstmt != null)
					pstmt.close();
			}
			catch (SQLException e)
			{
				logger.warn("Error in pstmt.close()", e );
			}
	
			if(conn != null)
			{
				try
				{
					conn.close();
				}catch(Exception ex){
					logger.warn("Exception in closing connection ", ex);
				}
			}
		}	
	}
}
