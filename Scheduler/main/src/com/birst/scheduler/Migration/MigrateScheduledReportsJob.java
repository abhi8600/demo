package com.birst.scheduler.Migration;

import java.util.Calendar;

import org.apache.log4j.Logger;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.birst.scheduler.Migration.MigrationUtils.MigrationStatus;
import com.birst.scheduler.Migration.MigrationUtils.MigrationType;

//@DisallowConcurrentExecution 
public class MigrateScheduledReportsJob implements Job
{

	private static final Logger logger = Logger.getLogger(MigrateScheduledReportsJob.class);
	
	@Override
	public void execute(JobExecutionContext context)
			throws JobExecutionException
	{		
		logger.info("******Starting Migration*****" + Calendar.getInstance().getTime().toString());
		
		try
		{
			MigrationOperation.setMigrationStatus(MigrationType.REPORT, MigrationStatus.Running);
			MigrationUtils.migrateOldScheduledReports();
			Thread.sleep(300000);
		} catch (Exception ex)
		{
			logger.error("Error in migration " , ex);
			MigrationOperation.setMigrationStatus(MigrationType.REPORT, MigrationStatus.Failed);
			throw new JobExecutionException("Migration failed. See the log for details ");
		}
		
		MigrationOperation.setMigrationStatus(MigrationType.REPORT, MigrationStatus.Completed);
		logger.info("******Ending Migration*****" + Calendar.getInstance().getTime().toString());
	}

}
