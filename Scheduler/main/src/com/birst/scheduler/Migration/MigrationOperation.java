package com.birst.scheduler.Migration;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.TriggerKey;

import com.birst.scheduler.SchedulerServerContext;
import com.birst.scheduler.Migration.MigrationUtils.MigrationStatus;
import com.birst.scheduler.Migration.MigrationUtils.MigrationType;
import com.birst.scheduler.Report.ScheduledReport;
import com.birst.scheduler.Report.ScheduledReportJob;
import com.birst.scheduler.exceptions.SchedulerBaseException;
import com.birst.scheduler.exceptions.SchedulerNotRunningException;

public class MigrationOperation
{
	
	public static Map<String, MigrationStatus> migrationProgress = new HashMap<String, MigrationStatus>();
	
	public static synchronized void setMigrationStatus(MigrationType migrationType, MigrationStatus status)
	{
		migrationProgress.put(migrationType.toString(), status);
	}
	
	public static String getMigrationStatus(String migrationType)
	{
		String status = null;
		if(migrationProgress != null && migrationProgress.containsKey(migrationType))
		{
			status = migrationProgress.get(migrationType) != null ? migrationProgress.get(migrationType).toString() : null;
		}
		return status;
	}
	
	public static void addMigrationJob(String migrationType) throws SchedulerException, SchedulerNotRunningException
	{
		if(migrationType.equalsIgnoreCase(MigrationType.REPORT.toString()))
		{
			Scheduler scheduler = SchedulerServerContext.getSchedulerInstance();
			JobKey jobKey = new JobKey(MigrationUtils.MIGRATION_JOB_TYPE_REPORT, MigrationUtils.MIGRATION_JOB_GROUP);			
			TriggerKey triggerKey = new TriggerKey(MigrationUtils.MIGRATION_TRIGGER_TYPE_REPORT, MigrationUtils.MIGRATION_TRIGGER_GROUP);
			
			JobDetail jobDetail = JobBuilder.newJob(MigrateScheduledReportsJob.class)
			.withIdentity(jobKey)			
			.build();
			
			Trigger trigger = TriggerBuilder.newTrigger()
			.withIdentity(triggerKey)
			.startAt(Calendar.getInstance().getTime())
			.forJob(jobDetail)		
			.build();
			
			scheduler.scheduleJob(jobDetail, trigger);
			
		}
	}
	
	public static void enableMigratedReports() throws SQLException,
			SchedulerBaseException, ClassNotFoundException, SchedulerException,
			ParseException
	{
		MigrationUtils.enableAllMigratedReports();
	}
	
	public static void disableMigratedReports() throws SQLException,
			SchedulerBaseException, ClassNotFoundException, SchedulerException,
			ParseException
	{
		MigrationUtils.enableAllMigratedReports();
	}
	
	public static List<ScheduledReport> getMigratedReports()
			throws SQLException, SchedulerBaseException, ClassNotFoundException
	{
		return MigrationUtils.getMigratedReports();
	}
}
