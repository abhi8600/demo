package com.birst.scheduler.Report;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.birst.scheduler.SchedulerServerContext;
import com.birst.scheduler.shared.Schedule;
import com.birst.scheduler.shared.ScheduleInfo;
import com.birst.scheduler.utils.CompareUtils;

public class ScheduledReport
{
    @Override
	public boolean equals(Object obj)
	{
		if (obj == null){ return false;	}
		if(!(obj instanceof ScheduledReport)){return false;}
		ScheduledReport sr = (ScheduledReport) obj;
		if(!CompareUtils.compare(sr.getId(), this.getId(), true)) { return false;}
		if(!CompareUtils.compare(sr.getUserID(), this.getUserID(), true)) { return false;}
		if(!CompareUtils.compare(sr.getSpaceID(), this.getSpaceID(), true)) { return false;}
		if(!CompareUtils.compare(sr.getReportPath(), this.getReportPath(), true)) { return false;}
		if(!CompareUtils.compare(sr.getType(), this.getType(), true)) { return false;}
		if(!CompareUtils.compare(sr.getSubject(), this.getSubject(), true)) { return false;}
		if(!CompareUtils.compare(sr.getTriggerReportPath(), this.getTriggerReportPath(), true)) { return false;}
		if(!CompareUtils.compare(sr.getEmailBody(), this.getEmailBody(), true)) { return false;}
		if(!CompareUtils.compare(sr.getToReportPath(), this.getToReportPath(), true)) { return false;}
		if(!CompareUtils.compare(sr.getToList(), this.getToList(), true)) { return false;}
		if(!CompareUtils.compare(sr.getComments(), this.getComments(), true)) { return false;}
		if(!CompareUtils.compare(sr.getName(), this.getName(), true)) { return false;}
		if(!CompareUtils.compare(sr.getFileName(), this.getFileName(), true)) { return false;}
		if(!CompareUtils.compare(sr.getCompression(), this.getCompression(), true)) { return false;}
		if(!CompareUtils.compare(sr.getFrom(), this.getFrom(), true)) { return false;}
		if(!CompareUtils.compare(sr.getCsvExtension(), this.getCsvExtension(), true)) { return false;}
		if(!CompareUtils.compare(sr.getCsvSeparator(), this.getCsvSeparator(), true)) { return false;}
		if(!CompareUtils.compare(sr.getPrompts(), this.getPrompts(), true)) { return false;}
		if(!CompareUtils.compare(sr.getVariables(), this.getVariables(), true)) { return false;}
		return true;
	}

	private String id;
    private String spaceID;
    private String userID;
    private String reportPath;    
    private String type;
    private String csvSeparator; // used when type contains csv
    private String csvExtension; // used when type contains csv
    private String subject;
    private String emailBody;
    private String triggerReportPath;
    private String[] toList;
    private String toReportPath;    
    private Schedule[] jobSchedules;
    private String comments;
    private String jobName;
    private String fileName;
    private String compression;
    private String from;
	private String ModifiedBy;
	private Timestamp CreatedDate;
	private Timestamp ModifiedDate;
	private String prompts;
	private String variables;
	private boolean createdAdminMode;
    
    
    
	// only used for returning status. Not persisted in the database
    public boolean currentExecutionStatus;
    
    public ScheduledReport(){
    	
    }
    
    public ScheduledReport(String id, String spaceID, String userID, String reportPath, 
    		String triggerReportPath, String toReportPath, String type, 
    		String subject, String emailBody , String[] toList, String comments,
    		String name, String fileName, String compression, String from,
    		String csvSeparator, String csvExtension, String prompts, String variables, boolean createdAdminMode)
    {
    	this.id = id;
    	this.spaceID = spaceID;
    	this.userID = userID;
    	this.reportPath = reportPath;
    	this.triggerReportPath = triggerReportPath;
    	this.toReportPath = toReportPath;
    	this.type = type;
    	this.subject = subject;
    	this.emailBody = emailBody;    	
    	this.toList = toList;
    	this.comments = comments;
    	this.jobName = name != null && name.length() > 0 ? name : inferJobNameFromReportPath(reportPath);
    	this.fileName = fileName != null && fileName.length() > 0 ? fileName : inferFileNameFromReportPath(reportPath);
    	this.compression = compression;
    	this.from = from != null && from.length() > 0 ? from: SchedulerServerContext.getEmailDeliveryAddress();
    	this.csvSeparator = csvSeparator;
    	this.csvExtension = csvExtension;
    	this.prompts = prompts;
    	this.variables = variables;
    	this.createdAdminMode = createdAdminMode;
    }
    
    /*
    public ScheduleInfo addScheduleInformation(int interval, int dayOfWeek, int dayOfMonth, int hour, int minute, boolean enable)
    {   
    	List<ScheduleInfo> list = new ArrayList<ScheduleInfo>();
    	if(this.schedules != null && this.schedules.length > 0)
    	{
    		for(ScheduleInfo si : schedules)
    		{
    			list.add(si);
    		}
    	}
    	
    	String scheduleInfoID = UUID.randomUUID().toString();    	
    	ScheduleInfo newScheduleInfo = new ScheduleInfo(scheduleInfoID, this.getId(), interval, dayOfWeek, dayOfMonth, hour, minute, enable);    	
    	list.add(newScheduleInfo);
    	schedules = (ScheduleInfo[]) list.toArray(new ScheduleInfo[list.size()]);
    	return newScheduleInfo;
    }
    
    
    public void addScheduleInformation(ScheduleInfo si)
    {   
    	List<ScheduleInfo> list = new ArrayList<ScheduleInfo>();
    	if(this.schedules != null && this.schedules.length > 0)
    	{
    		for(ScheduleInfo schedule : schedules)
    		{
    			list.add(schedule);
    		}
    	}   	
    	
    	list.add(si);
    	schedules = (ScheduleInfo[]) list.toArray(new ScheduleInfo[list.size()]);
    }
    */
    
    private String inferFileNameFromReportPath(String fullReportPath)
	{
    	String outputFileName = fullReportPath;
    	int lastFileSeparatorIndex = fullReportPath.lastIndexOf("\\");
    	lastFileSeparatorIndex = lastFileSeparatorIndex == -1 ? 0 : lastFileSeparatorIndex + 1;
		
		int lastDotSeparator = fullReportPath.lastIndexOf(".");
		if(lastDotSeparator == -1)
		{
			outputFileName = fullReportPath.substring(lastFileSeparatorIndex);
		}
		else
		{
			outputFileName = fullReportPath.substring(lastFileSeparatorIndex, lastDotSeparator);
		}
		
		return outputFileName;
	}

	private String inferJobNameFromReportPath(String fullReportPath)
	{
		String outputJobName = fullReportPath;
    	int lastFileSeparatorIndex = fullReportPath.lastIndexOf("\\");
    	lastFileSeparatorIndex = lastFileSeparatorIndex == -1 ? 0 : lastFileSeparatorIndex + 1;
		
		int lastDotSeparator = fullReportPath.lastIndexOf(".");
		if(lastDotSeparator == -1)
		{
			outputJobName = fullReportPath.substring(lastFileSeparatorIndex);
		}
		else
		{
			outputJobName = fullReportPath.substring(lastFileSeparatorIndex, lastDotSeparator);
		}
		
		return outputJobName;
	}

	public String getId()
	{
		return id;
	}

	public void setId(String id)
	{
		this.id = id;
	}

	public String getSpaceID()
	{
		return spaceID;
	}

	public void setSpaceID(String spaceID)
	{
		this.spaceID = spaceID;
	}

	public String getUserID()
	{
		return userID;
	}

	public void setUserID(String userID)
	{
		this.userID = userID;
	}

	public String getReportPath()
	{
		return reportPath;
	}

	public void setReportPath(String reportPath)
	{
		this.reportPath = reportPath;
	}

	public String getType()
	{
		return type;
	}

	public void setType(String type)
	{
		this.type = type;
	}

	public String getSubject()
	{
		return subject;
	}

	public void setSubject(String subject)
	{
		this.subject = subject;
	}

	public String getEmailBody()
	{
		return emailBody;
	}

	public void setEmailBody(String emailBody)
	{
		this.emailBody = emailBody;
	}

	public String getTriggerReportPath()
	{
		return triggerReportPath;
	}

	public void setTriggerReportPath(String triggerReportPath)
	{
		this.triggerReportPath = triggerReportPath;
	}

	public String[] getToList()
	{
		return toList;
	}

	public void setToList(String[] toList)
	{
		this.toList = toList;
	}

	public String getToReportPath()
	{
		return toReportPath;
	}

	public void setToReportPath(String toReportPath)
	{
		this.toReportPath = toReportPath;
	}
/*
	public ScheduleInfo[] getSchedules()
	{
		return schedules;
	}

	public void setSchedules(ScheduleInfo[] schedules)
	{
		this.schedules = schedules;
	}
*/
	public boolean isCurrentExecutionStatus()
	{
		return currentExecutionStatus;
	}

	public void setCurrentExecutionStatus(boolean currentExecutionStatus)
	{
		this.currentExecutionStatus = currentExecutionStatus;
	}
	
	public Schedule[] getJobSchedules()
	{
		return jobSchedules;
	}

	public void setJobSchedules(Schedule[] jobSchedules)
	{
		this.jobSchedules = jobSchedules;
	}

	public String getComments()
	{
		return comments;
	}

	public void setComments(String comments)
	{
		this.comments = comments;
	}

	public String getName()
	{
		return jobName;
	}

	public void setName(String jobName)
	{
		this.jobName = jobName;
	}

	public String getFileName()
	{
		return fileName;
	}

	public void setFileName(String fileName)
	{
		this.fileName = fileName;
	}

	public String getCompression()
	{
		return compression;
	}

	public void setCompression(String compression)
	{
		this.compression = compression;
	}

	public String getFrom()
	{
		return from;
	}

	public void setFrom(String from)
	{
		this.from = from;
	}

	public String getModifiedBy()
	{
		return ModifiedBy;
	}

	public void setModifiedBy(String modifiedBy)
	{
		ModifiedBy = modifiedBy;
	}

	public String getCsvSeparator()
	{
		return csvSeparator;
	}

	public void setCsvSeparator(String csvSeparator)
	{
		this.csvSeparator = csvSeparator;
	}

	public String getCsvExtension()
	{
		return csvExtension;
	}

	public void setCsvExtension(String csvExtension)
	{
		this.csvExtension = csvExtension;
	}

	public Timestamp getCreatedDate()
	{
		return CreatedDate;
	}

	public void setCreatedDate(Timestamp createdDate)
	{
		CreatedDate = createdDate;
	}

	public Timestamp getModifiedDate()
	{
		return ModifiedDate;
	}

	public void setModifiedDate(Timestamp modifiedDate)
	{
		ModifiedDate = modifiedDate;
	}

	public String getPrompts()
	{
		return prompts;
	}

	public void setPrompts(String prompts)
	{
		this.prompts = prompts;
	}
	
	public String getVariables()
	{
		return variables;
	}

	public void setVariables(String variables)
	{
		this.variables = variables;
	}

	public boolean isCreatedAdminMode()
	{
		return createdAdminMode;
	}

	public void setCreatedAdminMode(boolean createdAdminMode)
	{
		this.createdAdminMode = createdAdminMode;
	}
	
	@Override
	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		if(id != null && id.trim().length() > 0)
		{
			sb.append("id=" + id);
			sb.append(" ");
		}
		if(spaceID != null && spaceID.trim().length() > 0)
		{
			sb.append("spaceId=" + spaceID);
			sb.append(" ");
		}
		if(userID != null && userID.trim().length() > 0)
		{
			sb.append("userId=" + userID);
			sb.append(" ");
		}
		if(this.reportPath != null && reportPath.trim().length() > 0)
		{
			sb.append("reportPath=" + reportPath);
			sb.append(" ");
		}
		if(this.triggerReportPath != null && triggerReportPath.trim().length() > 0)
		{
			sb.append("triggerReportPath=" + triggerReportPath);
			sb.append(" ");
		}
		if(this.fileName != null && fileName.trim().length() > 0)
		{
			sb.append("fileName=" + fileName);
			sb.append(" ");
		}
		if(this.jobName != null && jobName.trim().length() > 0)
		{
			sb.append("jobName=" + jobName);
			sb.append(" ");
		}
		if(this.toReportPath != null && toReportPath.trim().length() > 0)
		{
			sb.append("toReportPath=" + toReportPath);
			sb.append(" ");
		}
		
		return sb.toString();
	}	

}
