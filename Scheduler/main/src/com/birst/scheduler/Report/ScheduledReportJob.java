package com.birst.scheduler.Report;


import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import javax.xml.namespace.QName;

import org.apache.axiom.om.OMElement;
import org.apache.axis2.AxisFault;
import org.apache.axis2.client.ServiceClient;
import org.apache.log4j.Logger;
import org.quartz.CronTrigger;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import com.birst.scheduler.SchedulerServerContext;
import com.birst.scheduler.exceptions.SchedulerBaseException;
import com.birst.scheduler.listeners.jobs.PersistJobHistoryInDB;
import com.birst.scheduler.listeners.jobs.ConfigureRetryJob;
import com.birst.scheduler.utils.AcornAdminDBConnection;
import com.birst.scheduler.utils.AdminDatabase;
import com.birst.scheduler.utils.AxisXmlUtils;
import com.birst.scheduler.utils.BirstWebServiceResponse;
import com.birst.scheduler.utils.SMIWebServiceResponse;

import com.birst.scheduler.utils.SchedulerDBConnection;
import com.birst.scheduler.utils.SchedulerDatabase;
import com.birst.scheduler.utils.Util;
import com.birst.scheduler.utils.UtilConstants;
import com.birst.scheduler.utils.UtilConstants.ReportStatus;
import com.birst.scheduler.utils.UtilConstants.RunningJobStatus;


public class ScheduledReportJob implements Job
{
	public static final int POLL_DURATION_MILLISECONDS = 5*1000;
	private static final Logger logger = Logger.getLogger(ScheduledReportJob.class);	
	private RunningJobStatus jobStatus = RunningJobStatus.RUNNING;
	private String errorMessage;
	private boolean retry;
	
	public ScheduledReportJob(){
		
	}
	
	@Override
	public void execute(JobExecutionContext ctx) throws JobExecutionException
	{
		logger.info("Scheduled Report Job started");
		//JobDataMap jobDataMap = ctx.getMergedJobDataMap();
		JobDataMap triggerJobDataMap = ctx.getTrigger().getJobDataMap();
		Connection conn = null;
		String schema = UtilConstants.SCHEDULER_DB_SCHEMA;
		String id = ctx.getJobDetail().getKey().getName();
		if(!id.startsWith(UtilConstants.PREFIX_SCHEDULED_REPORT_JOB))
		{
			throw new JobExecutionException("job id " + id + " does not start with the prefix " + UtilConstants.PREFIX_SCHEDULED_REPORT_JOB);
		}
		
		String reportId = Util.getObjectID(ctx.getJobDetail().getKey().getName(), UtilConstants.PREFIX_SCHEDULED_REPORT_JOB);
		ScheduledReport sr = null;
		try
		{
			conn = SchedulerDBConnection.getInstance().getConnectionPool().getConnection();
			sr = SchedulerDatabase.getScheduledReport(conn, schema, reportId);
			if(sr == null)
			{
				String msg = "ScheduledReport not found";
				logger.error(msg + " : " + reportId);
				markTheJobStatus(RunningJobStatus.FAILED, msg);
				return;
			}			
			
			String userID = sr.getUserID();
			String spaceID = sr.getSpaceID();
			
            // get details form acorn e.g. username, spacedirectory, maxreports, locale, timezone
			Map<String,String> propertiesMap = this.getUserAndSpaceInfo(sr.getUserID(), spaceID);
            if(propertiesMap == null || propertiesMap.size() == 0)
            {
            	markTheJobStatus(RunningJobStatus.FAILED, "No Details found for user : " + userID + " : space : " + spaceID);            	
            	return;
            }
            
            String username = propertiesMap.get(UtilConstants.KEY_USERNAME);
            if(username == null || username.length() == 0)
            {
            	markTheJobStatus(RunningJobStatus.FAILED, "No username found for user : " + userID );            	
            	return;
            }
            	
            String spaceDirectory = propertiesMap.get(UtilConstants.KEY_SPACE_DIRECTORY);
            if(spaceDirectory == null || spaceDirectory.length() == 0)
            {
            	String msg = "No space details found for space : " + spaceID;
            	logger.error("Error while running job " + msg);
            	markTheJobStatus(RunningJobStatus.FAILED, msg );
            	// check if the space is deleted
            	boolean isSpaceDeleted = Util.isSpaceDeleted(propertiesMap);
            	if(isSpaceDeleted)
            	{
            		logger.info("Scheduling a delete space job for " + spaceID);
            		Util.scheduleDeletedSpaceJob(spaceID);
            		return;
            	}
            	return;
            }
            
            String maxReportsString = propertiesMap.get(UtilConstants.KEY_MAX_REPORTS);            
            int maxReports = maxReportsString != null && maxReportsString.length() > 0 ? Integer.parseInt(maxReportsString) : -1;
            
            String reportLocale = propertiesMap.get(UtilConstants.KEY_LOCALE);
            if(reportLocale == null || reportLocale.length() == 0)
            {
            	String msg = "No Locale found for user : " + userID + " : space : " + spaceID;
            	logger.error("Error while running job : " + msg);
            	markTheJobStatus(RunningJobStatus.FAILED, msg);          	
            	return;
            }
            
            String reportTimeZone = propertiesMap.get(UtilConstants.KEY_TIMEZONE);
            
            // send the webservice request to SMIWeb and poll till completion or failed status
            scheduleAndPollExportReport(sr, spaceDirectory, username, maxReports, reportLocale, reportTimeZone);
			
		} catch (Exception e)
		{
			markTheJobStatus(RunningJobStatus.FAILED, "Exception thrown : " + e.getMessage(), true);
			logger.error("Error in running scheduled report", e);
			throw new JobExecutionException("Error while running reportJob", e.getCause());			
		}
		finally
		{			
			triggerJobDataMap.put(PersistJobHistoryInDB.KEY_STATUS, jobStatus.getStatus());
			triggerJobDataMap.put(PersistJobHistoryInDB.KEY_ERROR_MESSAGE, errorMessage);
			String parentJobNextFireTime = (String) triggerJobDataMap.get(ConfigureRetryJob.KEY_RETRY_JOB_PARENT_NEXT_FIRETIME);
			if(retry)
			{	
					
				if(parentJobNextFireTime == null)
				{
					Date nextFireTime = ctx.getTrigger().getNextFireTime();
					// if both parentJobNextFireTime and nextFireTime is null meaning this is run now job. 
					// Ignoring the use case for retrying
					if(nextFireTime == null)
					{
						retry = false;
					}
					else
					{
						Calendar cal = Calendar.getInstance();
						cal.setTime(nextFireTime);
						parentJobNextFireTime = String.valueOf(cal.getTimeInMillis());
					}
				}
				
				if(retry)
				{
					triggerJobDataMap.put(ConfigureRetryJob.KEY_RETRY_JOB_PARENT_NEXT_FIRETIME, parentJobNextFireTime);
					triggerJobDataMap.put(ConfigureRetryJob.KEY_RETRY_JOB, "true");
					String parentJobIntervalValue = null;
					String parentJobTimeZone = null;
					if(ctx.getTrigger() instanceof CronTrigger)
					{
						CronTrigger cronTrigger = (CronTrigger) ctx.getTrigger();
						parentJobIntervalValue = Util.getInterval(cronTrigger.getCronExpression());
						TimeZone tz = cronTrigger.getTimeZone();
						parentJobTimeZone = tz.getDisplayName();
					}
					else
					{
						// If this is a retry job, then the trigger won't be an instance of CronTrigger
						// In that case look for parameter that defines the interval for the job
						parentJobIntervalValue = (String) ctx.getTrigger().getJobDataMap().get(ConfigureRetryJob.KEY_RETRY_JOB_PARENT_INTERVAL);
						parentJobTimeZone = (String) ctx.getTrigger().getJobDataMap().get(ConfigureRetryJob.KEY_RETRY_JOB_PARENT_TIME_ZONE);
					}
					
					if(parentJobIntervalValue != null && parentJobIntervalValue.length() > 0)
					{
						triggerJobDataMap.put(ConfigureRetryJob.KEY_RETRY_JOB_PARENT_INTERVAL, parentJobIntervalValue);
					}
					
					if(parentJobTimeZone != null && parentJobTimeZone.length() > 0)
					{
						triggerJobDataMap.put(ConfigureRetryJob.KEY_RETRY_JOB_PARENT_TIME_ZONE, parentJobTimeZone);
					}
				}
			}
			else
			{				
				triggerJobDataMap.remove(ConfigureRetryJob.KEY_RETRY_JOB_PARENT_NEXT_FIRETIME);
				triggerJobDataMap.remove(ConfigureRetryJob.KEY_RETRY_JOB);
				triggerJobDataMap.remove(ConfigureRetryJob.KEY_RETRY_JOB_PARENT_INTERVAL);
				triggerJobDataMap.remove(ConfigureRetryJob.KEY_RETRY_JOB_PARENT_TIME_ZONE);
			}
			
			if(conn != null)
			{
				try
				{
					conn.close();
				}
				catch(Exception ex)
				{}
			}
		}
	}
	
	private void scheduleAndPollExportReport(ScheduledReport sr, String spaceDirectory, String username, int maxReports, String reportLocale, String reportTimeZone ) throws Exception
	{
		ServiceClient serviceClient = null;
		try
		{
			String spaceID = sr.getSpaceID();
			String report = sr.getReportPath() + UtilConstants.REPORT_JASPER_SUFFIX;
			// if explicit report name is given infer from the report name
			String filename = sr.getFileName();
			if(filename == null || filename.length() == 0)
			{
				int index = sr.getReportPath().lastIndexOf('\\');
				filename = index >= 0 ? report.substring(index + 1) : report;
			}
			
			String toReport = sr.getToReportPath() == null ? null : sr.getToReportPath() + UtilConstants.REPORT_JASPER_SUFFIX;
			String triggerReport = sr.getTriggerReportPath() == null ? null : sr.getTriggerReportPath() + UtilConstants.REPORT_JASPER_SUFFIX;
			
			String body = sr.getEmailBody() == null ? "" : sr.getEmailBody();
			String subject = sr.getSubject();
			String type = sr.getType();
			String from = sr.getFrom();
			List<String> toList = null;
			if(sr.getToList() != null && sr.getToList().length > 0)
			{	
				toList = new ArrayList<String>();
				for(String toEmail : sr.getToList())
				{
					toList.add(toEmail);
				}
			}	
			String compressionFormat = sr.getCompression() == null || sr.getCompression().length() == 0 ? "none" : sr.getCompression();
			String csvSeparator = sr.getCsvSeparator() == null ? "" : sr.getCsvSeparator();
			String csvExtension = sr.getCsvExtension() == null  ? "" : sr.getCsvExtension();
			String promptsXmlString = sr.getPrompts();
			String variablesXmlString = sr.getVariables();
			List<String> packageSpacePropertiesList = Util.getPackageSpacePropertiesArray(spaceID);
			
			LinkedHashMap<String, Object> inputValuesMap = new LinkedHashMap<String, Object>();
			inputValuesMap.put("path", spaceDirectory);
			inputValuesMap.put("spaceID", spaceID);
			inputValuesMap.put("type", type);
			inputValuesMap.put("name", filename);
			inputValuesMap.put("username", username);
			inputValuesMap.put("from", from);
			inputValuesMap.put("subject", subject);
			inputValuesMap.put("body", body);
			inputValuesMap.put("report", report);
			inputValuesMap.put("toList", toList);
			inputValuesMap.put("toReport", toReport);
			inputValuesMap.put("triggerReport", triggerReport);
			inputValuesMap.put("maxReports", maxReports);
			inputValuesMap.put("reportLocale", reportLocale);
			inputValuesMap.put("reportTimeZone", reportTimeZone);
			inputValuesMap.put("reportTimeZone", reportTimeZone);
			inputValuesMap.put("compressionFormat", compressionFormat);
			inputValuesMap.put("csvSeparator", csvSeparator);
			inputValuesMap.put("csvExtension", csvExtension);
			inputValuesMap.put("mailHost", SchedulerServerContext.getMailerHost());	
			inputValuesMap.put("promptsXmlString", AxisXmlUtils.encode(promptsXmlString));
			inputValuesMap.put("variablesXmlString", AxisXmlUtils.encode(variablesXmlString));
			inputValuesMap.put("packageSpacesProperties", packageSpacePropertiesList);
			String smiWebUrl = Util.getSMIWebUrl();
			serviceClient = Util.getServiceClient(smiWebUrl + UtilConstants.SMIWEB_TRUSETED_SERVICE_ENDPOINT);
			//		serviceClient.getOptions().setAction(UtilConstants.SMIWEB_TRUSTED_SERVICE_NAMESPACE + "scheduleExportReport");
			serviceClient.getOptions().setAction("urn:scheduleExportReport");
			OMElement payLoad = Util.createPayLoad(UtilConstants.SMIWEB_TRUSTED_SERVICE_NAMESPACE, "scheduleExportReport", inputValuesMap);
			OMElement result = Util.sendAndCleanUp(serviceClient, payLoad);
			SMIWebServiceResponse response = Util.parseSMIWebResponse(UtilConstants.SMIWEB_TRUSTED_SERVICE_NAMESPACE, result);
			if(Util.isSuccessful(response))
			{
				String uuid = getUUIDFromResponse(response);
				if (uuid != null)
				{
					pollStatusTillCompletion(smiWebUrl, uuid);
				} else
				{
					markTheJobStatus(RunningJobStatus.FAILED, "Unable to get identifier from SMIWeb", true);
					return;
				}
			} else
			{
				// Report error, update the scheduled report status
				// TO DO : Find any use cases which need retry
				markTheJobStatus(RunningJobStatus.FAILED, Util.getErrorMessage(response), true);
				return;
			}
			} 
		finally
		{			
		}

	}
	
	private void markTheJobStatus(RunningJobStatus status, String errorMsg)
	{
		markTheJobStatus(status, errorMsg, false);
	}
	
	private void markTheJobStatus(RunningJobStatus status, String errorMsg, boolean retryJob)
	{
		jobStatus = status;
		errorMessage = errorMsg;
		retry = retryJob;
	}	
	
	private HashMap<String, String> getUserAndSpaceInfo(String userID, String spaceID) throws Exception
	{
		HashMap<String,String> responseMap = null;
		Connection conn = null;		
		try
		{			
			conn = AcornAdminDBConnection.getInstance().getConnectionPool().getConnection();
			responseMap = new HashMap<String,String>();
			String reportLocaleValue = AdminDatabase.getMergedPreferences(conn, AdminDatabase.ADMIN_DB_SCHEMA, userID, UtilConstants.KEY_LOCALE);
			if(reportLocaleValue != null && reportLocaleValue.length() > 0)
			{
				responseMap.put(UtilConstants.KEY_LOCALE, reportLocaleValue);
			}
			String timeZoneValue = AdminDatabase.getUserPreference(conn, AdminDatabase.ADMIN_DB_SCHEMA, userID, UtilConstants.KEY_TIMEZONE);
			if(timeZoneValue != null && timeZoneValue.length() > 0)
			{
				responseMap.put(UtilConstants.KEY_TIMEZONE, timeZoneValue);
			}
			
			Map<String, String> userInfo = AdminDatabase.getUserInfo(conn, AdminDatabase.ADMIN_DB_SCHEMA, userID);
			if(userInfo != null && userInfo.size() > 0)
			{
				for(String userDetailKey : userInfo.keySet())
				{
					responseMap.put(userDetailKey, userInfo.get(userDetailKey));
				}			
			}
			
			Map<String, String> spaceInfo = AdminDatabase.getSpaceInfo(conn, AdminDatabase.ADMIN_DB_SCHEMA, spaceID);
			if(spaceInfo != null && spaceInfo.size() > 0)
			{
				for(String spaceDetailKey : spaceInfo.keySet())
				{
					responseMap.put(spaceDetailKey, spaceInfo.get(spaceDetailKey));
				}			
			}
		}
		finally
		{		
			if(conn != null)
			{
				try
				{
					conn.close();
				}catch(Exception ex)
				{
					logger.debug("Problem in closing connection " ,ex);
				}			
			}
		}
		
		return responseMap;
	}
	
	@SuppressWarnings("unchecked")
	private HashMap<String, String> getUserAndSpaceProps(String userID, String spaceID) throws Exception
	{
		HashMap<String,String> responseMap = null;	
		ServiceClient acornServiceClient	= null;
		try
		{		
			acornServiceClient = Util.getServiceClient(SchedulerServerContext.getBirstAdminUrl() + UtilConstants.BIRST_ADMIN_SERVICE_ENDPOINT);
			acornServiceClient.getOptions().setAction(UtilConstants.BIRST_ADMIN_SERVICE_NAMESPACE + "getUserAndSpaceDetails");
			LinkedHashMap<String, Object> inputValuesMap = new LinkedHashMap<String, Object>();
			inputValuesMap.put("sharedServersToken", "TEST");
			inputValuesMap.put("userID", userID);
			inputValuesMap.put("spaceID", spaceID);
			
			OMElement acornServicePayLoad = Util.createPayLoad(UtilConstants.BIRST_ADMIN_SERVICE_NAMESPACE, "getUserAndSpaceDetails", inputValuesMap);
			OMElement result = acornServiceClient.sendReceive(acornServicePayLoad);
			BirstWebServiceResponse response  = Util.parseBirstWebServiceResponse(UtilConstants.BIRST_ADMIN_SERVICE_NAMESPACE, "getUserAndSpaceDetailsResult", result);
	
			if(Util.isSuccessful(response))
			{
				responseMap = new HashMap<String,String>();
				OMElement getUserAndSpaceDetailsResult = response.resultNode;
				if(getUserAndSpaceDetailsResult != null)
				{
					OMElement other = getUserAndSpaceDetailsResult.getFirstChildWithName(new QName(UtilConstants.BIRST_ADMIN_SERVICE_NAMESPACE, "other"));
					if(other != null)
					{
						Iterator<OMElement> stringArrayIterator = other.getChildrenWithName(new QName(UtilConstants.BIRST_ADMIN_SERVICE_NAMESPACE, "string"));
						if(stringArrayIterator != null)
						{
							while(stringArrayIterator.hasNext())
							{
								OMElement stringElement = stringArrayIterator.next();
								String keyValuePairString = stringElement.getText();
								if(keyValuePairString != null && keyValuePairString.split("=").length == 2)
								{
									String[] keyValuePair = keyValuePairString.split("=");
									responseMap.put(keyValuePair[0], keyValuePair[1]);
								}
							}
						}
					}
				}
			}
		}
		finally
		{
			if(acornServiceClient != null)
			{
				acornServiceClient.cleanup();
				acornServiceClient.cleanupTransport();
			}
		}
		return responseMap;
	}	

	

	private String getUUIDFromResponse(SMIWebServiceResponse response)
	{
		OMElement element = response.BirstWebServiceResultNode;
		String uid = null;
		if(element != null)
		{
			OMElement resultElement = element.getFirstChildWithName(new QName("Result"));
			if (resultElement != null)
			{				
				OMElement exportReportStatusElement = resultElement.getFirstChildWithName(new QName("ExportReportStatus"));
				if (exportReportStatusElement != null)
				{
					OMElement uuidElement = exportReportStatusElement.getFirstChildWithName(new QName("uid"));
					if(uuidElement != null)
					{
						uid = uuidElement.getText();
					}
				}				
			}
		}
		return uid;
	}
	
	private void pollStatusTillCompletion(String smiWebUrl, String uid) throws AxisFault, InterruptedException
	{	
		boolean continuePolling = true;
		while(continuePolling)
		{
			//ServiceClient serviceClient = Util.getServiceClient(SchedulerServerContext.getSMIWebUrl() + UtilConstants.SMIWEB_TRUSETED_SERVICE_ENDPOINT);
			ServiceClient serviceClient = Util.getServiceClient(smiWebUrl + UtilConstants.SMIWEB_TRUSETED_SERVICE_ENDPOINT);
			LinkedHashMap<String, Object> inputValuesMap = new LinkedHashMap<String, Object>();
			inputValuesMap.put("uuid", uid);
			OMElement payLoad = Util.createPayLoad(UtilConstants.SMIWEB_TRUSTED_SERVICE_NAMESPACE, "getExportReportStatus", inputValuesMap);
			serviceClient.getOptions().setAction("urn:getExportReportStatus");
			OMElement result = Util.sendAndCleanUp(serviceClient, payLoad);
			SMIWebServiceResponse response = Util.parseSMIWebResponse(UtilConstants.SMIWEB_TRUSTED_SERVICE_NAMESPACE, result);
			if(Util.isSuccessful(response))
			{
				String status = parseAndGetRunningStatus(response);
				if(status == null)
				{
					continuePolling = false;
					markTheJobStatus(RunningJobStatus.FAILED,"Unable to get updated report status");					
				}
				else if(status.equalsIgnoreCase(ReportStatus.COMPLETE.toString()) || status.equalsIgnoreCase(ReportStatus.FAILED.toString()))
				{
					continuePolling = false;
					markTheJobStatus(RunningJobStatus.COMPLETE, null);
				}				
			}
			else
			{
				continuePolling = false;
				markTheJobStatus(RunningJobStatus.FAILED, response.ErrorMessage);				
			}
			
			if(continuePolling)
			{
				Thread.sleep(ScheduledReportJob.POLL_DURATION_MILLISECONDS);
			}
		}
	}
	
	private String parseAndGetRunningStatus(SMIWebServiceResponse response)
	{			
		OMElement element = response.BirstWebServiceResultNode;
		String status = null;
		if(element != null)
		{
			OMElement resultElement = element.getFirstChildWithName(new QName("Result"));
			if (resultElement != null)
			{				
				OMElement exportReportStatusElement = resultElement.getFirstChildWithName(new QName("ExportReportStatus"));
				if (exportReportStatusElement != null)
				{
					OMElement statusElement = exportReportStatusElement.getFirstChildWithName(new QName("status"));
					if(statusElement != null)
					{
						status = statusElement.getText();
					}
				}				
			}
		}
		return status;
	}	
}
