package com.birst.scheduler.Report;

import java.io.StringReader;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import java.util.UUID;
import java.util.Map.Entry;

import javax.xml.namespace.QName;
import javax.xml.stream.FactoryConfigurationError;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
import org.apache.axiom.om.OMText;
import org.apache.axiom.om.impl.builder.StAXOMBuilder;
import org.apache.axis2.AxisFault;
import org.apache.axis2.client.ServiceClient;
import org.apache.log4j.Logger;
import org.quartz.CronTrigger;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SimpleScheduleBuilder;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.TriggerKey;
import org.quartz.TriggerUtils;
import org.quartz.impl.matchers.GroupMatcher;

import static org.quartz.CronScheduleBuilder.cronSchedule;

import com.birst.scheduler.SchedulerServerContext;
import com.birst.scheduler.exceptions.JobRunningException;
import com.birst.scheduler.exceptions.ObjectUnavailableException;
import com.birst.scheduler.exceptions.SchedulerBaseException;
import com.birst.scheduler.exceptions.ValidationException;
import com.birst.scheduler.listeners.jobs.PersistJobHistoryInDB;
import com.birst.scheduler.listeners.triggers.ClearScheduleInfoTriggerListener;
import com.birst.scheduler.shared.Operation;
import com.birst.scheduler.shared.Schedule;
import com.birst.scheduler.shared.ScheduleInfo;
import com.birst.scheduler.shared.SchedulerVersion;
import com.birst.scheduler.utils.AxisXmlUtils;
import com.birst.scheduler.utils.BirstWebServiceResponse;
import com.birst.scheduler.utils.CompareUtils;
import com.birst.scheduler.utils.JobConstants;
import com.birst.scheduler.utils.SchedulerDBConnection;
import com.birst.scheduler.utils.SchedulerDatabase;
import com.birst.scheduler.utils.Util;
import com.birst.scheduler.utils.UtilConstants;
import com.birst.scheduler.webServices.XMLWebServiceResult;
import com.birst.scheduler.webServices.response.ScheduledReportListResult;
import com.birst.scheduler.webServices.response.SchedulerWebServiceResult;

public class ScheduledReportOperation implements Operation
{
	private static Logger logger = Logger.getLogger(ScheduledReportOperation.class);

	@SuppressWarnings("unchecked")
	public static ScheduledReport parseReportDetailsXmlString(String spaceID, String userID, String jobOptionsXmlString)
	{
		ScheduledReport sr = null;
		XMLStreamReader reader = null;

		try
		{
			reader = XMLInputFactory.newInstance().createXMLStreamReader(new StringReader(jobOptionsXmlString));
			StAXOMBuilder builder = new StAXOMBuilder(reader);
			OMElement docElement = builder.getDocumentElement();
			docElement.build();
			docElement.detach();

			OMElement element = docElement.getFirstChildWithName(new QName("", JobConstants.JOB_SR_NODE));
			// see if the root element iteself is ScheduledReport
			if(element == null)
			{
				if (docElement.getLocalName() == JobConstants.JOB_SR_NODE)
				{
					element = docElement;
				}
			}
			if(element != null)
			{
				sr = new ScheduledReport();				
				Iterator<OMElement> iterator = element.getChildElements();
				while(iterator.hasNext())
				{
					OMElement childElement = iterator.next();
					String elementName = childElement.getLocalName();
					String elementText = childElement.getText();					

					if(JobConstants.JOB_SR_REPORT_PATH.equals(elementName))
					{
						sr.setReportPath(elementText);
					}

					if(JobConstants.JOB_SR_REPORT_TYPE.equals(elementName))
					{
						sr.setType(elementText);
					}

					if(JobConstants.JOB_SR_CSV_SEPARATOR.equals(elementName))
					{
						sr.setCsvSeparator(elementText);
					}

					if(JobConstants.JOB_SR_CSV_EXTENSION.equals(elementName))
					{
						sr.setCsvExtension(elementText);
					}

					if(JobConstants.JOB_SR_TRIGGER_REPORT.equals(elementName))
					{
						sr.setTriggerReportPath(elementText);
					}

					if(JobConstants.JOB_SR_TOREPORT.equals(elementName))
					{
						sr.setToReportPath(elementText);
					}

					if(JobConstants.JOB_SR_TO_LIST.equals(elementName))
					{
						List<String> toList = new ArrayList<String>();
						Iterator<OMElement> emailsIterator = childElement.getChildrenWithName(new QName("", JobConstants.JOB_SR_EMAIL));
						while(emailsIterator.hasNext())
						{
							OMElement emailElement = emailsIterator.next();
							toList.add(emailElement.getText());
						}
						sr.setToList((String [])toList.toArray(new String[toList.size()]));
					}

					if(JobConstants.JOB_SR_SUBJECT.equals(elementName))
					{
						sr.setSubject(elementText);
					}

					if(JobConstants.JOB_SR_FROM.equals(elementName))
					{
						sr.setFrom(elementText);
					}

					if(JobConstants.JOB_SR_BODY.equals(elementName))
					{
						sr.setEmailBody(elementText);
					}

					if(JobConstants.JOB_COMMENTS.equals(elementName))
					{
						sr.setComments(elementText);
					}

					if(JobConstants.JOB_SR_FILE_NAME.equals(elementName))
					{
						sr.setFileName(elementText);
					}

					if(JobConstants.JOB_SR_COMPRESSION.equals(elementName))
					{
						sr.setCompression(elementText);
					}

					if(JobConstants.JOB_NAME.equals(elementName))
					{
						sr.setName(elementText);
					}

					if(JobConstants.JOB_SR_PROMPTS.equals(elementName))
					{
						sr.setPrompts(childElement.toString());
					}
					
					if(JobConstants.JOB_SR_OVERRIDE_VARIABLES.equals(elementName))
					{
						// see if the override variables contains any variables at all
						Iterator<OMElement> variableIterator = childElement.getChildrenWithName(new QName("", JobConstants.JOB_SR_VARIABLE));
						if(variableIterator.hasNext())
						{
							sr.setVariables(childElement.toString());
						}
					}

					if(JobConstants.JOB_SR_CREATED_ADMIN_MODE.equals(elementName))
					{
						sr.setCreatedAdminMode(Boolean.parseBoolean(elementText));
					}

					if(JobConstants.JOB_SCHEDULE_LIST.equals(elementName))
					{
						List<Schedule> schedules = Util.parseSchedules(childElement);

						sr.setJobSchedules((Schedule [])schedules.toArray(new Schedule[schedules.size()]));
					}
				}
			}

		} catch (XMLStreamException e)
		{
			logger.error(e,e);
		} catch (FactoryConfigurationError e)
		{
			logger.error(e,e);
		}
		return sr;
	}



	public static ScheduledReport addReport(String spaceID, String userID, 
			String report, String triggerReport, String toReport,  String type,			
			String subject, String emailBody,  String[] toList, String comments,
			String jobName, String fileName, String compression, String from,
			String csvSeparator, String csvExtension, String prompts, String variables, boolean createdAdminMode,
			Schedule[] schedules) throws SQLException, SchedulerBaseException, SchedulerException, ParseException, ClassNotFoundException
			{

		// Add the info to the customized tables
		String jobID = UUID.randomUUID().toString();
		ScheduledReport sr = new ScheduledReport(jobID, spaceID, userID, 
				report, triggerReport, toReport, type, subject, emailBody , 
				toList, comments, jobName, fileName, compression, from, csvSeparator, 
				csvExtension, prompts, variables, createdAdminMode);
		sr.setJobSchedules(schedules);

		return addReport(spaceID, sr);
	}

	public static ScheduledReport addReport(String spaceID, ScheduledReport sr) throws SQLException, SchedulerBaseException, SchedulerException, ParseException, ClassNotFoundException
	{
		Scheduler scheduler = SchedulerServerContext.getSchedulerInstance();
		return addReport(scheduler, spaceID, sr);
	}
	
	public static ScheduledReport addReport(Scheduler scheduler, String spaceID, ScheduledReport sr) throws SQLException, SchedulerBaseException, SchedulerException, ParseException, ClassNotFoundException
	{
		Connection conn = null;
		try
		{
			String jobID = UUID.randomUUID().toString();
			sr.setId(jobID);
			
			conn = SchedulerDBConnection.getInstance().getConnectionPool().getConnection();
			String schema = UtilConstants.SCHEDULER_DB_SCHEMA;
			// Add report and schedule info to the database
			SchedulerDatabase.addScheduledReport(conn, schema, sr);
			
			// Create job and trigger details and schedule it
			JobBuilder jobBuilder = Util.getBirstJobBuilder(ScheduledReportJob.class, 
					Util.getCustReportJobName(sr.getId()), 
					Util.getCustJobGroupNameForReport(spaceID));
			JobDetail jobDetail = jobBuilder.build();
			
			scheduler.addJob(jobDetail, false);
			Schedule[] schedules = sr.getJobSchedules();
			if(schedules != null && schedules.length > 0)
			{
				for (Schedule schedule : schedules)
				{
					// Add the trigger only if enable flag is set.
					if(schedule.getId() == null)
					{
						schedule.setId(UUID.randomUUID().toString());
					}
					schedule.setJobId(jobID);
					SchedulerDatabase.addSchedule(conn, schema, schedule);
					if(!schedule.isEnable())
					{
						continue;						
					}

					String timeZoneID = Schedule.getTimeZoneID(schedule.getTimeZone());

					CronTrigger cronTrigger = TriggerBuilder.newTrigger()
					.withIdentity(Util.getCustReportTriggerName(schedule.getId()), spaceID)
					.withSchedule(cronSchedule(schedule.getExpression()).inTimeZone(TimeZone.getTimeZone(timeZoneID)))
					.forJob(jobDetail)
					.build();
				
					scheduler.scheduleJob(cronTrigger);


				}
			}

			ScheduledReport addedScheduledReport = SchedulerDatabase.getScheduledReport(conn, schema, jobID);
			return addedScheduledReport;
		}
		finally{
			if(conn != null)
			{
				try
				{
					conn.close();
				}catch(Exception ex)
				{
					logger.warn("Exception in closing connection ",ex);
				}
			}
		}
	}



	public static void addSchedule(String spaceID, String scheduledReportID,
			Schedule schedule) throws SQLException, SchedulerBaseException,	SchedulerException, ParseException, ClassNotFoundException
			{

		// Basically remove the old one and create a new one
		Connection conn = null;
		try
		{

			if(schedule.getId() != null)
			{
				schedule.setId(UUID.randomUUID().toString());
			}

			conn = SchedulerDBConnection.getInstance().getConnectionPool().getConnection();
			String schema = UtilConstants.SCHEDULER_DB_SCHEMA;
			ScheduledReport sr = SchedulerDatabase.getScheduledReport(conn,
					schema, scheduledReportID);
			if (sr == null)
			{
				throw new ObjectUnavailableException(
						"Schedule Report not found for id " + scheduledReportID);
			}			

			SchedulerDatabase.addSchedule(conn, schema, schedule);
			Scheduler scheduler = SchedulerServerContext.getSchedulerInstance();
			if (schedule.isEnable())
			{
				JobKey jobKey = new JobKey(Util.getCustReportJobName(sr.getId()), sr.getSpaceID());
				JobDetail jobDetail = scheduler.getJobDetail(jobKey);

				String timeZoneID = schedule.getTimeZone();
				CronTrigger cronTrigger = TriggerBuilder.newTrigger()
				.withIdentity(Util.getCustReportTriggerName(schedule.getId()), spaceID)
				.withSchedule(cronSchedule(schedule.getExpression()).inTimeZone(TimeZone.getTimeZone(timeZoneID)))		
				.build();

				
				scheduler.scheduleJob(jobDetail, cronTrigger);
			}
		} finally
		{
			if (conn != null)
			{
				try
				{
					conn.close();
				} catch (Exception ex)
				{
				}
			}
		}
			}


	public void removeScheduleFromJob(SchedulerWebServiceResult result, String spaceID, String scheduledReportID, String scheduleInfoID)
	throws SQLException, SchedulerBaseException, SchedulerException, ClassNotFoundException
	{

		// Basically remove the old one and create a new one
		Connection conn = null;
		try
		{
			conn = SchedulerDBConnection.getInstance().getConnectionPool().getConnection();
			String schema = UtilConstants.SCHEDULER_DB_SCHEMA;
			ScheduledReport sr = SchedulerDatabase.getScheduledReport(conn, schema, scheduledReportID);
			if(sr != null && sr.getJobSchedules() != null )
			{
				for(Schedule schedule : sr.getJobSchedules())
				{
					if(schedule.getId().equalsIgnoreCase(scheduleInfoID))
					{
						Scheduler scheduler = SchedulerServerContext.getSchedulerInstance();						
						scheduler.unscheduleJob(new TriggerKey(Util.getCustReportTriggerName(schedule.getId()), sr.getSpaceID()));
						SchedulerDatabase.removeSchedule(conn, schema, schedule.getId());
						break;
					}
				}
			}			
		} finally
		{
			if (conn != null)
			{
				try
				{
					conn.close();
				} catch (Exception ex)
				{
				}
			}
		}
	}
	
	public static ScheduledReport updateReport(String spaceID,
			ScheduledReport updatedReport) throws SchedulerException,
			SchedulerBaseException, SQLException, ParseException,
			ClassNotFoundException
	{
		Scheduler scheduler = SchedulerServerContext.getSchedulerInstance();
		return updateReport(scheduler, spaceID, updatedReport);
	}
	
	public static ScheduledReport updateReport(Scheduler scheduler, String spaceID,
			ScheduledReport updatedReport) throws SchedulerException,
			SchedulerBaseException, SQLException, ParseException,
			ClassNotFoundException
	{
		// Basically remove the old one and create a new one
		Connection conn = null;
		try
		{
			conn = SchedulerDBConnection.getInstance().getConnectionPool().getConnection();
			String schema = UtilConstants.SCHEDULER_DB_SCHEMA;
			String scheduledReportID = updatedReport.getId();
			ScheduledReport sr = SchedulerDatabase.getScheduledReport(conn, schema, scheduledReportID);
			if(sr == null)
			{
				logger.warn("No Scheduled Report found for " + scheduledReportID);
				return null;
			}

			// the created user id should never change. Modified by user can change
			if(updatedReport.getUserID() == null)
			{
				updatedReport.setUserID(sr.getUserID());
			}

			Schedule[] updatedSchedules = updatedReport.getJobSchedules();
			// Create a tmp data structure for comparison to see if anything has changed
			/*
			ScheduledReport tmp = new ScheduledReport(sr.getId(), sr.getSpaceID(), sr.getUserID(), report, triggerReport,
					toReport, type, subject, emailBody, toList, comments, name, fileName, compression, from);
			 */    	
			// Created Mode should never ever change. Make sure that the it is always same
			updatedReport.setCreatedAdminMode(sr.isCreatedAdminMode());
			boolean changed = !sr.equals(updatedReport);
			if(changed)
			{
				SchedulerDatabase.updateScheduledReport(conn, schema, updatedReport);
				sr = SchedulerDatabase.getScheduledReport(conn, schema, scheduledReportID);
			}

			List<Schedule> existingSchedules = new ArrayList<Schedule>();
			if(sr.getJobSchedules() != null && sr.getJobSchedules().length > 0)
			{
				existingSchedules = Arrays.asList(sr.getJobSchedules());
			}

			// For adding
			List<Schedule> toBeAdded = new ArrayList<Schedule>();			
			// For removing			
			List<Schedule> toBeRemoved = new ArrayList<Schedule>(existingSchedules);

			if(updatedSchedules != null && updatedSchedules.length > 0)
			{
				for(Schedule updatedSchedule : updatedSchedules)
				{
					Schedule current = null;
					for(Schedule existingSchedule : existingSchedules)
					{
						if(existingSchedule.getId().equalsIgnoreCase(updatedSchedule.getId()))
						{
							current = existingSchedule;
							break;
						}
					}

					if(current == null)
					{
						toBeAdded.add(updatedSchedule);
					}
					else 
					{
						if (current.equals(updatedSchedule))
						{
							toBeRemoved.remove(current);						
						}
						else
						{
							toBeAdded.add(updatedSchedule);
						}
					}					
				}
			}
			
			for(Schedule schedule : toBeRemoved)
			{
				scheduler.unscheduleJob(new TriggerKey(Util.getCustReportTriggerName(schedule.getId()), sr.getSpaceID()));
				SchedulerDatabase.removeSchedule(conn, schema, schedule.getId());
			}

			String jobName = Util.getCustReportJobName(scheduledReportID);
			JobDetail jobDetail = scheduler.getJobDetail(new JobKey(jobName, sr.getSpaceID()));
			for(Schedule schedule : toBeAdded)
			{
				// New schedule, make sure each new schedule has a UID
				//if(schedule.getId() == null)
				//{
				schedule.setId(UUID.randomUUID().toString());
				//}
				SchedulerDatabase.addSchedule(conn, schema, schedule);
				if(!schedule.isEnable())
				{
					continue;
				}

				if(jobDetail == null)
				{
					throw new ObjectUnavailableException("JobDetail not found for jobName" + Util.getCustReportJobName(sr.getId()));					
				}

				String timeZoneID = Schedule.getTimeZoneID(schedule.getTimeZone());
				CronTrigger cronTrigger = TriggerBuilder.newTrigger()
				.withIdentity(Util.getCustReportTriggerName(schedule.getId()), spaceID)
				.withSchedule(cronSchedule(schedule.getExpression()).inTimeZone(TimeZone.getTimeZone(timeZoneID)))
				.forJob(jobDetail)
				.build();

				//CronTrigger cronTrigger = new CronTrigger();				
				//cronTrigger.setCronExpression(schedule.getExpression());				
				//cronTrigger.setName(Util.getCustReportTriggerName(schedule.getId()));
				//cronTrigger.setGroup(sr.getSpaceID());
				//cronTrigger.addTriggerListener(UtilConstants.TRIGGER_LISTENER_ClEAR_SCHEDULEINFO);
				// Add job details. name and group
				//cronTrigger.setJobName(jobName);
				//cronTrigger.setJobGroup(sr.getSpaceID());

				scheduler.scheduleJob(cronTrigger);				
			}

			// unschedule all the retry jobs and run now jobs
			Util.removeAllNonScheduledTriggers(jobDetail.getKey());
			ScheduledReport updatedScheduledReport = SchedulerDatabase.getScheduledReport(conn, schema, scheduledReportID);
			return updatedScheduledReport;
		} 
		finally
		{
			if(conn != null)
			{
				try
				{
					conn.close();
				}catch(Exception ex)
				{
					logger.warn("Exception in closing connection ",ex);
				}
			}
		}
			}	

	@Override
	public void removeJob(SchedulerWebServiceResult result, String spaceID, String jobID) throws Exception
	{
		Scheduler scheduler = SchedulerServerContext.getSchedulerInstance();	
		removeReport(scheduler, spaceID, jobID);
	}
	
	public static void removeReport(Scheduler scheduler, String spaceID, String scheduledReportID) throws SchedulerException, SQLException, SchedulerBaseException, ClassNotFoundException
	{	
		Connection conn = null;		
		try
		{
			conn = SchedulerDBConnection.getInstance().getConnectionPool().getConnection();
			String schema = UtilConstants.SCHEDULER_DB_SCHEMA;
			ScheduledReport sr = SchedulerDatabase.getScheduledReport(conn, schema, scheduledReportID);
			if(sr == null)
			{
				return;
			}

			String jobName = Util.getCustReportJobName(sr.getId());
			String groupName = sr.getSpaceID();
			List<String> runningJobs = Util.getRunningJobs(jobName, groupName);
			if(runningJobs != null && runningJobs.size() > 0)
			{
				// schedule is in progress. Cannot remove report
				throw new JobRunningException("Scheduled Report currently running " + scheduledReportID );
			}
						
			scheduler.deleteJob(new JobKey(jobName, sr.getSpaceID()));
			// Now that the job has been removed from scheduler. Remove the scheduleInfo and report info from db
			Schedule[] schedules = sr.getJobSchedules();
			if(schedules != null && schedules.length > 0)
			{
				for(Schedule schedule : schedules)
				{
					SchedulerDatabase.removeSchedule(conn, schema, schedule.getId());
				}
			}

			SchedulerDatabase.removeScheduledReport(conn, schema, scheduledReportID);				

		}
		finally
		{
			if(conn != null)
			{
				try
				{
					conn.close();
				} catch (Exception ex)
				{}
			}
		}

	}


	@SuppressWarnings("unchecked")
	public static List<ScheduledReport> getScheduledReports(Scheduler scheduler, String spaceID, String userID) throws SchedulerException, SQLException, SchedulerBaseException, ClassNotFoundException
	{		
		//String[] reportJobNames = scheduler.getJobKeys(matcher)jogetJobNames(spaceID);		
		Set<JobKey> jobKeys = new HashSet<JobKey>();
		if(spaceID.equalsIgnoreCase(UtilConstants.ALL_SPACES))
		{
			for(String groupName : scheduler.getJobGroupNames())
			{
				jobKeys.addAll(scheduler.getJobKeys(GroupMatcher.groupEquals(groupName)));
			}
		}
		else
		{
			jobKeys = scheduler.getJobKeys(GroupMatcher.groupEquals(spaceID));
		}

		return getScheduledReports(scheduler, spaceID, jobKeys, userID);
	}

	public static List<ScheduledReport> getScheduledReports(Scheduler scheduler, String spaceID, Set<JobKey> jobKeys, String userID) throws SchedulerException, SQLException, SchedulerBaseException, ClassNotFoundException
	{		
		List<ScheduledReport> reportList = new ArrayList<ScheduledReport>();
		//String[] reportJobNames = scheduler.getJobKeys(matcher)jogetJobNames(spaceID);
		//if(reportJobNames != null && reportJobNames.length > 0)
		if(jobKeys != null && jobKeys.size() > 0)
		{							
			List<String> currentlyExecutingJobs = Util.isPrimaryScheduler(scheduler) ? Util.getRunningJobs(spaceID.equalsIgnoreCase(UtilConstants.ALL_SPACES) ? null : spaceID) : null;
			//for(String reportJobName : reportJobNames)

			Connection conn = null;
			try
			{
				conn = SchedulerDBConnection.getInstance().getConnectionPool().getConnection();
				String schema = UtilConstants.SCHEDULER_DB_SCHEMA;
				for(JobKey jobKey : jobKeys)
				{
					String reportJobName = jobKey.getName();
					String scheduledReportID = Util.getObjectID(reportJobName, UtilConstants.PREFIX_SCHEDULED_REPORT_JOB);
					if(scheduledReportID != null)
					{
						ScheduledReport sr = SchedulerDatabase.getScheduledReport(conn, schema, scheduledReportID);
						if(sr == null)
						{
							logger.warn("Cannot find scheduled report info for " + scheduledReportID);
							if(Util.isPrimaryScheduler(scheduler))
							{
								// In this case, we should just delete the job from the scheduler
								scheduler.deleteJob(new JobKey(reportJobName, spaceID));
							}
							continue;
						}

						// filter on user ids
						if(userID != null && userID.length() > 0 && !userID.equalsIgnoreCase(sr.getUserID()))
						{
							continue;
						}
						sr.currentExecutionStatus = currentlyExecutingJobs != null ? currentlyExecutingJobs.contains(reportJobName) : false;
						reportList.add(sr);
					}

				}
			}
			finally
			{					
				if(conn != null)
				{
					try
					{
						conn.close();
					} catch (Exception ex)
					{}
				}

			}
		}		
		return reportList;
	}


	public void runNow(SchedulerWebServiceResult result, String spaceID, String reportID) throws SchedulerBaseException, SchedulerException
	{		
		Scheduler scheduler   = SchedulerServerContext.getSchedulerInstance();
		JobDetail jobDetail = scheduler.getJobDetail(new JobKey(Util.getCustReportJobName(reportID), spaceID));

		String uidString = UUID.randomUUID().toString();

		Trigger trigger = Util.getImmediateTrigger(spaceID, jobDetail, uidString);
		scheduler.scheduleJob(trigger);
	}

	// not using the pause function of the scheduler
	// reason is that when resume, it will go through all the misfire instructions
	// which might result in multiple emails being sent

	// So pause is equivalent to removing the trigger for now
	// When we resume, we will add the trigger again. We have all the trigger information in the schedule table

	public void pauseJob(SchedulerWebServiceResult result, String spaceID, String reportID)
	throws SchedulerException, SchedulerBaseException, SQLException,
	ParseException, ClassNotFoundException
	{

		flagAllSchedules(spaceID, reportID, false);
	}

	public void resumeJob(SchedulerWebServiceResult result, String spaceID, String reportID)
	throws SchedulerException, SchedulerBaseException, SQLException,
	ParseException, ClassNotFoundException
	{
		flagAllSchedules(spaceID, reportID, true);
	}


	/**	 * 
	 * @param spaceID - spaceID 
	 * @param reportID - unique identifier for report
	 * @param enable - flag to enable/disable all the schedules. If false, removes all the triggers from the scheduler instance
	 * If true, it adds all the triggers to the scheduler instance
	 * @throws ClassNotFoundException 
	 * @throws ParseException 
	 * @throws SQLException 
	 * @throws SchedulerBaseException 
	 * @throws SchedulerException 
	 * 
	 */
	
	
	
	public static void flagAllSchedules(String spaceID, String reportID,
			boolean enable) throws SchedulerException, SchedulerBaseException,
			SQLException, ParseException, ClassNotFoundException 
	{
		Scheduler scheduler = SchedulerServerContext.getSchedulerInstance();
		flagAllSchedules(scheduler, spaceID, reportID, enable);
	}
	
	public static void flagAllSchedules(Scheduler scheduler, String spaceID, String reportID,
			boolean enable) throws SchedulerException, SchedulerBaseException, SQLException, ParseException, ClassNotFoundException 
	{
		ScheduledReport scheduledReport = ScheduledReportOperation.getScheduledReportDetails(spaceID, reportID);
		if(scheduledReport != null)
		{
			Schedule[] schedules = scheduledReport.getJobSchedules();
			for(Schedule schedule : schedules)
			{
				schedule.setEnable(enable);
			}

			ScheduledReportOperation.updateReport(scheduler, spaceID, scheduledReport);
		}
	}

	@SuppressWarnings("unchecked")
	public static List<String> getListOfReports(String spaceID, String userID) throws AxisFault
	{
		List<String> reportsList = new ArrayList<String>();
		ServiceClient acornServiceClient = Util.getServiceClient(SchedulerServerContext.getBirstAdminUrl() + UtilConstants.BIRST_ADMIN_SERVICE_ENDPOINT);
		acornServiceClient.getOptions().setAction(UtilConstants.BIRST_ADMIN_SERVICE_NAMESPACE + "getCatalogReports");
		LinkedHashMap<String, Object> inputValuesMap = new LinkedHashMap<String, Object>();
		inputValuesMap.put("sharedServersToken", "TEST");
		inputValuesMap.put("userID", userID);
		inputValuesMap.put("spaceID", spaceID);

		OMElement acornServicePayLoad = Util.createPayLoad(UtilConstants.BIRST_ADMIN_SERVICE_NAMESPACE, "getCatalogReports", inputValuesMap);		
		OMElement result = acornServiceClient.sendReceive(acornServicePayLoad);				
		BirstWebServiceResponse response  = Util.parseBirstWebServiceResponse(UtilConstants.BIRST_ADMIN_SERVICE_NAMESPACE, "getCatalogReportsResult", result);
		if(Util.isSuccessful(response))
		{
			OMElement getCatalogReportsResult = response.resultNode;
			if(getCatalogReportsResult != null)
			{
				OMElement other = getCatalogReportsResult.getFirstChildWithName(new QName(UtilConstants.BIRST_ADMIN_SERVICE_NAMESPACE, "other"));
				if(other != null)
				{
					Iterator<OMElement> stringArrayIterator = other.getChildrenWithName(new QName(UtilConstants.BIRST_ADMIN_SERVICE_NAMESPACE, "string"));
					if(stringArrayIterator != null)
					{
						while(stringArrayIterator.hasNext())
						{
							OMElement stringElement = stringArrayIterator.next();
							String report = stringElement.getText();
							reportsList.add(report);
						}					
					}
				}
			}
		}

		return reportsList;
	}
	
	public static ScheduledReport getNewCopiedReport(ScheduledReport oldReport)
	{		
		String jobId = UUID.randomUUID().toString();		
		String spaceID = oldReport.getSpaceID();
		String userID = oldReport.getUserID();
    	String reportPath = oldReport.getReportPath();
    	String triggerReportPath = oldReport.getTriggerReportPath();
    	String toReportPath = oldReport.getToReportPath();
    	String type = oldReport.getType();
    	String subject = oldReport.getSubject();
    	String emailBody = oldReport.getEmailBody();    	
    	String[] toList = oldReport.getToList();
    	String comments = oldReport.getComments();
    	String jobName = oldReport.getName();
    	String fileName = oldReport.getFileName();
    	String compression = oldReport.getCompression();
    	String from = oldReport.getFrom();
    	String csvSeparator = oldReport.getCsvSeparator();
    	String csvExtension = oldReport.getCsvExtension();
    	String prompts = oldReport.getPrompts();
    	String variables = oldReport.getVariables();
    	boolean createdAdminMode = oldReport.isCreatedAdminMode();
    	Schedule[] jobSchedules = oldReport.getJobSchedules();
    	List<Schedule> newSchedules = new ArrayList<Schedule>();
    	for(Schedule oldSchedule : jobSchedules)
    	{
    		String scheduleId = UUID.randomUUID().toString();
    		Schedule newSchedule = new Schedule(scheduleId, jobId, oldSchedule.getExpression(), oldSchedule.isEnable(), oldSchedule.getTimeZone());
    		newSchedules.add(newSchedule);
    	}
    	
		ScheduledReport newReport = new ScheduledReport();
		newReport.setId(jobId);
		newReport.setSpaceID(spaceID);
		newReport.setUserID(userID);
		newReport.setReportPath(reportPath);
		newReport.setTriggerReportPath(triggerReportPath);
		newReport.setToReportPath(toReportPath);
		newReport.setType(type);
		newReport.setSubject(subject);
		newReport.setEmailBody(emailBody);
		newReport.setToList(toList);
		newReport.setComments(comments);
		newReport.setName(jobName);
		newReport.setFileName(fileName);
		newReport.setCompression(compression);
		newReport.setFrom(from);
		newReport.setCsvSeparator(csvSeparator);
		newReport.setCsvExtension(csvExtension);
		newReport.setPrompts(prompts);
		newReport.setVariables(variables);
		newReport.setCreatedAdminMode(createdAdminMode);
		newReport.setJobSchedules((Schedule [])newSchedules.toArray(new Schedule[newSchedules.size()]));
		
		return newReport;
	}
	
	public static ScheduledReport getScheduledReportDetails(String spaceID, String scheduledReportID) throws SQLException, SchedulerBaseException, ClassNotFoundException
	{
		ScheduledReport sr = null;
		Connection conn = null;
		try
		{
			conn = SchedulerDBConnection.getInstance().getConnectionPool().getConnection();
			String schema = UtilConstants.SCHEDULER_DB_SCHEMA;
			sr = SchedulerDatabase.getScheduledReport(conn, schema, scheduledReportID);
			if(sr == null)
			{
				logger.error("No Scheduled Report found for " + scheduledReportID);
				throw new ObjectUnavailableException("No Scheduled Report found");
			}
		}
		finally
		{	
			if(conn != null)
			{
				try
				{
					conn.close();
				} catch (Exception ex)
				{}
			}
		}
		return sr;
	}

	public static ScheduledReport copyChanges(ScheduledReport report1, ScheduledReport report2, boolean enableNewSchedule)
	{
		if(report1 == null)
		{
			Schedule[] report2JobSchedules = report2.getJobSchedules();
			for(Schedule report2Schedule : report2JobSchedules)
			{
				boolean enableReport2Schedule = enableNewSchedule ? report2Schedule.isEnable() : false;
				report2Schedule.setEnable(enableReport2Schedule);
			}
			return report2;
		}
		ScheduledReport newReport = new ScheduledReport();
		
		String reportId = report2.getId();		
		String spaceID = report2.getSpaceID();
		String userID = report2.getUserID();
		
		String reportPath1 = report1.getReportPath();
		String reportPath2 = report2.getReportPath();
		if(reportPath2 == null || (reportPath1 != null && !CompareUtils.compare(reportPath1, reportPath2, true)))
		{
			reportPath2 = reportPath1;
		}
		
		String triggerReportPath1 = report1.getTriggerReportPath();
		String triggerReportPath2 = report2.getTriggerReportPath();		
		if(triggerReportPath2 == null || (triggerReportPath1 != null && !CompareUtils.compare(triggerReportPath1, triggerReportPath2, true)))
		{
			triggerReportPath2 = triggerReportPath1;
		}
		
		String toReportPath1 = report1.getToReportPath();
		String toReportPath2 = report2.getToReportPath();	
		if(toReportPath2 == null || (toReportPath1 != null && !CompareUtils.compare(toReportPath1, toReportPath2, true)))
		{
			toReportPath2 = toReportPath1;
		}
		
		String type1 = report1.getType();
		String type2 = report2.getType();	
		if(type2 == null || (type1 != null && !CompareUtils.compare(type1, type2, true)))
		{
			type2 = type1;
		}
		
		
		String subject1 = report1.getSubject();
		String subject2 = report2.getSubject();	
		if(subject2 == null || (subject1 != null && !CompareUtils.compare(subject1, subject2, true)))
		{
			subject2 = subject1;
		}
		
		String from1 = report1.getFrom();
		String from2 = report2.getFrom();	
		if(from2 == null || (from1 != null && !CompareUtils.compare(from1, from2, true)))
		{
			from2 = from1;
		}
		
		String emailBody1 = report1.getEmailBody();
		String emailBody2 = report2.getEmailBody();	
		if(emailBody2 == null || (emailBody1 != null && !CompareUtils.compare(emailBody1, emailBody2, true)))
		{
			emailBody2 = emailBody1;
		}
		
		
		String[] toList1 = report1.getToList();
		String[] toList2 = report2.getToList();
		if(toList2 == null || (toList1 != null && !CompareUtils.compare(toList1, toList2, true)))
		{
			toList2 = toList1;
		}
		
		String comments1 = report1.getComments();
		String comments2 = report2.getComments();	
		if(comments2 == null || (comments1 != null && !CompareUtils.compare(comments1, comments2, true)))
		{
			comments2 = comments1;
		}
		
		String jobName1 = report1.getName();
		String jobName2 = report2.getName();	
		if(jobName2 == null || (jobName1 != null && !CompareUtils.compare(jobName1, jobName2, true)))
		{
			jobName2 = jobName1;
		}
		
		String fileName1 = report1.getFileName();
		String fileName2 = report2.getFileName();	
		if(fileName2 == null || (fileName1 != null && !CompareUtils.compare(fileName1, fileName2, true)))
		{
			fileName2 = fileName1;
		}
		
		String compression1 = report1.getCompression();
		String compression2 = report2.getCompression();	
		if(compression2 == null || (compression1 != null && !CompareUtils.compare(compression1, compression2, true)))
		{
			compression2 = compression1;
		}
		
		String csvSeparator1 = report1.getCsvSeparator();
		String csvSeparator2 = report2.getCsvSeparator();	
		if(csvSeparator2 == null || (csvSeparator1 != null && !CompareUtils.compare(csvSeparator1, csvSeparator2, true)))
		{
			csvSeparator2 = csvSeparator1;
		}
		
		String csvExtension1 = report1.getCsvExtension();
		String csvExtension2 = report2.getCsvExtension();	
		if(csvExtension2 == null || (csvExtension1 != null && !CompareUtils.compare(csvExtension1, csvExtension2, true)))
		{
			csvExtension2 = csvExtension1;
		}
		
		String prompts1 = report1.getPrompts();
		String prompts2 = report2.getPrompts();	
		if(prompts2 == null || (prompts1 != null && !CompareUtils.compare(prompts1, prompts2, true)))
		{
			prompts2 = prompts1;
		}
		
		String variables1 = report1.getVariables();
		String variables2 = report2.getVariables();	
		if(variables2 == null || (variables1 != null && !CompareUtils.compare(variables1, variables2, true)))
		{
			variables2 = variables1;
		}
		
		boolean createdAdminMode1 = report1.isCreatedAdminMode();
		boolean createdAdminMode2 = report2.isCreatedAdminMode();
		if(createdAdminMode1 != createdAdminMode2)
		{
			createdAdminMode2 = createdAdminMode1;
		}
		
		Schedule[] jobSchedules1 = report1.getJobSchedules();
		Schedule[] jobSchedules2 = report2.getJobSchedules();
		if(jobSchedules1 != null && jobSchedules1.length > 0)
		{
			// just copy everything from 1 to 2
			List<Schedule> newSchedules = new ArrayList<Schedule>();
	    	for(Schedule oldSchedule : jobSchedules1)
	    	{
	    		String scheduleId = UUID.randomUUID().toString();
	    		boolean enableSchedule = enableNewSchedule ? oldSchedule.isEnable() : false;
	    		Schedule newSchedule = new Schedule(scheduleId, reportId, oldSchedule.getExpression(), enableSchedule, oldSchedule.getTimeZone());
	    		newSchedules.add(newSchedule);
	    	}
	    	jobSchedules2 = (Schedule [])newSchedules.toArray(new Schedule[newSchedules.size()]);
		}
		
		
		newReport.setSpaceID(spaceID);
		newReport.setUserID(userID);
		newReport.setReportPath(reportPath2);
		newReport.setTriggerReportPath(triggerReportPath2);
		newReport.setToReportPath(toReportPath2);
		newReport.setType(type2);
		newReport.setSubject(subject2);
		newReport.setEmailBody(emailBody2);
		newReport.setToList(toList2);
		newReport.setComments(comments2);
		newReport.setName(jobName2);
		newReport.setFileName(fileName2);
		newReport.setCompression(compression2);
		newReport.setFrom(from2);
		newReport.setCsvSeparator(csvSeparator2);
		newReport.setCsvExtension(csvExtension2);
		newReport.setPrompts(prompts2);
		newReport.setCreatedAdminMode(createdAdminMode2);
		newReport.setJobSchedules(jobSchedules2);
    	
		return newReport;
	}


	@Override
	public void addJob(SchedulerWebServiceResult response, String spaceID, String userID, String jobOptionsXmlString) throws Exception
	{
		ScheduledReport tempReport = ScheduledReportOperation.parseReportDetailsXmlString(spaceID, userID, jobOptionsXmlString);
		String reportPath = Util.addCatalogPrefix(tempReport.getReportPath());
		String toReportPath = Util.addCatalogPrefix(tempReport.getToReportPath());
		String triggerReportPath = Util.addCatalogPrefix(tempReport.getTriggerReportPath());

		tempReport.setReportPath(reportPath);
		tempReport.setToReportPath(toReportPath);
		tempReport.setTriggerReportPath(triggerReportPath);
		tempReport.setSpaceID(spaceID);
		tempReport.setUserID(userID);

		ScheduledReport sr = ScheduledReportOperation.addReport(spaceID, tempReport);
		OMFactory factory = OMAbstractFactory.getOMFactory();
		OMNamespace ns = null;			
		OMElement jobIdElement = factory.createOMElement(JobConstants.JOB_ID, ns);
		OMText text = factory.createOMText(sr.getId());
		jobIdElement.addChild(text);
		response.setResult(new XMLWebServiceResult(jobIdElement));		
	}



	@Override
	public void addScheduleToJob(SchedulerWebServiceResult result,
			String spaceID, String jobID, String jobOptionsXmlString)
	throws Exception
	{
		ScheduledReport tempReport = ScheduledReportOperation.parseReportDetailsXmlString(spaceID, null, jobOptionsXmlString);				
		Schedule[] jobSchedules = tempReport.getJobSchedules();

		if(jobSchedules != null && jobSchedules.length > 0)
		{
			for(Schedule schedule : jobSchedules)
			{
				ScheduledReportOperation.addSchedule(spaceID, jobID, schedule);
			}
		}
		else
		{
			logger.warn("No schedules to add for space " + spaceID + " : job " + jobID);
		}
	}



	@Override
	public void getAllJobs(SchedulerWebServiceResult response, String spaceID,
			String onlyCurrentExecuting, String userID, String filterOnUser) throws Exception
	{
		boolean returnCurrentExecuting = Boolean.valueOf(onlyCurrentExecuting);
		boolean filterByUser = Boolean.parseBoolean(filterOnUser);
		OMFactory factory = OMAbstractFactory.getOMFactory();
		OMNamespace ns = null;			
		OMElement scheduledJobs = factory.createOMElement(JobConstants.JOBS_LIST_NODE, ns);
		AxisXmlUtils.addContent(factory, scheduledJobs, JobConstants.PRIMARY_SCHEDULER_NAME, SchedulerServerContext.getPrimarySchedulerDisplayName(), ns);
		Map<String, String> userIdToNameMapping = new HashMap<String, String>();

		// Get the scheduledReport from all schedulers
		Map<String,SchedulerVersion> schedulersMap = SchedulerServerContext.getAllSchedulers();
		for(Entry<String,SchedulerVersion> entry : schedulersMap.entrySet())
		{
			String schedulerDisplayName = entry.getKey();
			SchedulerVersion schedulerVersion = entry.getValue();
			Scheduler scheduler = schedulerVersion.getScheduler();

			List<ScheduledReport> reportList = ScheduledReportOperation.getScheduledReports(scheduler, spaceID, filterByUser ? userID:null);
			if(reportList != null)
			{										
				for(ScheduledReport scheduledReport : reportList)
				{
					// if only current executing jobs are required, filter on the status
					if(returnCurrentExecuting && !scheduledReport.isCurrentExecutionStatus())
					{
						continue;
					}

					// TO DO :  if user id is given, filter on the user id
					// First it will require modification on the current way of displaying scheduled reports
					OMElement sr = factory.createOMElement(JobConstants.JOB_NODE, ns);
					AxisXmlUtils.addContent(factory, sr, JobConstants.SCHEDULER_NAME, scheduler.getSchedulerName(), ns);
					AxisXmlUtils.addContent(factory, sr, JobConstants.SCHEDULER_DISPLAY_NAME, schedulerDisplayName, ns);
					AxisXmlUtils.addContent(factory, sr, JobConstants.SCHEDULER_ENABLE_MIGRATION, schedulerVersion.getEnableMigration(), ns);
					AxisXmlUtils.addContent(factory, sr, JobConstants.IS_PRIMARY_SCHEDULER, Util.isPrimaryScheduler(scheduler), ns);
					AxisXmlUtils.addContent(factory, sr, JobConstants.JOB_ID, scheduledReport.getId(), ns);
					AxisXmlUtils.addContent(factory, sr, JobConstants.JOB_NAME, scheduledReport.getName(), ns);
					AxisXmlUtils.addContent(factory, sr, JobConstants.JOB_SUMMARY, scheduledReport.getReportPath(), ns);
					AxisXmlUtils.addContent(factory, sr, JobConstants.JOB_SPACE_ID, scheduledReport.getSpaceID(), ns);
					AxisXmlUtils.addContent(factory, sr, JobConstants.JOB_TYPE, UtilConstants.JobType.REPORT.toString(), ns);
					AxisXmlUtils.addContent(factory, sr, JobConstants.JOB_USER_ID, scheduledReport.getUserID(), ns);						
					AxisXmlUtils.addContent(factory, sr, JobConstants.JOB_EXECUTION_STATUS, scheduledReport.isCurrentExecutionStatus(), ns);
					AxisXmlUtils.addContent(factory, sr, JobConstants.JOB_COMMENTS, scheduledReport.getComments(), ns);
					AxisXmlUtils.addContent(factory, sr, JobConstants.JOB_CREATED_DATE, scheduledReport.getCreatedDate(), ns);
					AxisXmlUtils.addContent(factory, sr, JobConstants.JOB_MODIFIED_DATE, scheduledReport.getModifiedDate(), ns);
					AxisXmlUtils.addContent(factory, sr, JobConstants.JOB_MODIFIED_BY, scheduledReport.getModifiedBy(), ns);

					// get the usernames from the map, if not present, get from acorn db and store it

					String createdUserName = Util.getAndAddUserFromMap(userIdToNameMapping, scheduledReport.getUserID());
					AxisXmlUtils.addContent(factory, sr, JobConstants.JOB_CREATED_USER, createdUserName, ns);

					// for report even the summary contains recipients, subject, reportpath

					OMElement schedules = factory.createOMElement(JobConstants.JOB_SCHEDULE_LIST, ns);
					Schedule[] jobSchedules = scheduledReport.getJobSchedules();
					// The job is scheduled to run if any of its schedule is enabled
					boolean isJobEnabled = Util.isAnyScheduleEnabled(jobSchedules);
					AxisXmlUtils.addContent(factory, sr, JobConstants.JOB_RUN, isJobEnabled, ns);

					if(jobSchedules != null && jobSchedules.length > 0)
					{
						for(Schedule jobSchedule : jobSchedules)
						{
							OMElement schedule = factory.createOMElement(JobConstants.JOB_SCHEDULE, ns);
							AxisXmlUtils.addContent(factory, schedule, JobConstants.JOB_SCHEDULE_ID, jobSchedule.getId(), ns);								
							AxisXmlUtils.addContent(factory, schedule, JobConstants.JOB_SCHEDULE_JOB_ID, jobSchedule.getJobId(), ns);
							AxisXmlUtils.addContent(factory, schedule, JobConstants.JOB_SCHEDULE_EXPRESSION, jobSchedule.getExpression(), ns);
							AxisXmlUtils.addContent(factory, schedule, JobConstants.JOB_SCHEDULE_TIMEZONE, jobSchedule.getTimeZone(), ns);
							AxisXmlUtils.addContent(factory, schedule, JobConstants.JOB_SCHEDULE_ENABLE, jobSchedule.isEnable(), ns);
							schedules.addChild(schedule);
						}
					}
					sr.addChild(schedules);
					scheduledJobs.addChild(sr);
				}
			}
		}
		
		response.setResult(new XMLWebServiceResult(scheduledJobs));
	}



	@Override
	public void getJobDetails(SchedulerWebServiceResult response, String spaceID,
			String jobID) throws Exception
	{
		OMFactory factory = OMAbstractFactory.getOMFactory();
		OMNamespace ns = null;			
		OMElement scheduledJob = factory.createOMElement(JobConstants.JOB_NODE, ns);


		ScheduledReport scheduledReport = ScheduledReportOperation.getScheduledReportDetails(spaceID, jobID);
		if(scheduledReport != null)
		{
			AxisXmlUtils.addContent(factory, scheduledJob, JobConstants.JOB_TYPE, UtilConstants.JobType.REPORT.toString(), ns);
			AxisXmlUtils.addContent(factory, scheduledJob, JobConstants.JOB_ID, scheduledReport.getId(), ns);
			AxisXmlUtils.addContent(factory, scheduledJob, JobConstants.JOB_NAME, scheduledReport.getName(), ns);
			AxisXmlUtils.addContent(factory, scheduledJob, JobConstants.JOB_SPACE_ID, scheduledReport.getSpaceID(), ns);						
			AxisXmlUtils.addContent(factory, scheduledJob, JobConstants.JOB_USER_ID, scheduledReport.getUserID(), ns);
			AxisXmlUtils.addContent(factory, scheduledJob, JobConstants.JOB_EXECUTION_STATUS, scheduledReport.isCurrentExecutionStatus(), ns);
			AxisXmlUtils.addContent(factory, scheduledJob, JobConstants.JOB_COMMENTS, scheduledReport.getComments(), ns);
			AxisXmlUtils.addContent(factory, scheduledJob, JobConstants.JOB_CREATED_DATE, scheduledReport.getCreatedDate(), ns);
			AxisXmlUtils.addContent(factory, scheduledJob, JobConstants.JOB_MODIFIED_DATE, scheduledReport.getModifiedDate(), ns);
			AxisXmlUtils.addContent(factory, scheduledJob, JobConstants.JOB_MODIFIED_BY, scheduledReport.getModifiedBy(), ns);

			Schedule[] jobSchedules = scheduledReport.getJobSchedules();
			// The job is scheduled to run if any of its schedule is enabled
			boolean isJobEnabled = Util.isAnyScheduleEnabled(jobSchedules);
			AxisXmlUtils.addContent(factory, scheduledJob, JobConstants.JOB_RUN, isJobEnabled, ns);

			OMElement jobDetailsElement = factory.createOMElement(JobConstants.JOB_DETAILS, ns);												
			AxisXmlUtils.addContent(factory, jobDetailsElement, JobConstants.JOB_SR_REPORT_PATH, scheduledReport.getReportPath(), ns);
			AxisXmlUtils.addContent(factory, jobDetailsElement, JobConstants.JOB_SR_FILE_NAME, scheduledReport.getFileName(), ns);
			AxisXmlUtils.addContent(factory, jobDetailsElement, JobConstants.JOB_SR_COMPRESSION, scheduledReport.getCompression(), ns);
			AxisXmlUtils.addContent(factory, jobDetailsElement, JobConstants.JOB_SR_REPORT_TYPE, scheduledReport.getType(), ns);
			AxisXmlUtils.addContent(factory, jobDetailsElement, JobConstants.JOB_SR_TOREPORT, scheduledReport.getToReportPath(), ns);
			AxisXmlUtils.addContent(factory, jobDetailsElement, JobConstants.JOB_SR_TRIGGER_REPORT, scheduledReport.getTriggerReportPath(), ns);
			AxisXmlUtils.addContent(factory, jobDetailsElement, JobConstants.JOB_SR_CSV_SEPARATOR, scheduledReport.getCsvSeparator(), ns);
			AxisXmlUtils.addContent(factory, jobDetailsElement, JobConstants.JOB_SR_CSV_EXTENSION, scheduledReport.getCsvExtension(), ns);
			AxisXmlUtils.addContent(factory, jobDetailsElement, JobConstants.JOB_SR_CREATED_ADMIN_MODE, scheduledReport.isCreatedAdminMode(), ns);
			AxisXmlUtils.addXmlStringToParent(factory, jobDetailsElement, scheduledReport.getPrompts(), ns);
			AxisXmlUtils.addXmlStringToParent(factory, jobDetailsElement, scheduledReport.getVariables(), ns);

			String[] toList = scheduledReport.getToList();
			if(toList != null && toList.length > 0)
			{
				OMElement toListElement = factory.createOMElement(JobConstants.JOB_SR_TO_LIST, ns);
				for(String emailId : toList)
				{
					AxisXmlUtils.addContent(factory, toListElement, JobConstants.JOB_SR_EMAIL, emailId, ns);
				}
				jobDetailsElement.addChild(toListElement);
			}

			AxisXmlUtils.addContent(factory, jobDetailsElement, JobConstants.JOB_SR_SUBJECT, scheduledReport.getSubject(), ns);						
			AxisXmlUtils.addContent(factory, jobDetailsElement, JobConstants.JOB_SR_BODY, scheduledReport.getEmailBody(), ns);
			AxisXmlUtils.addContent(factory, jobDetailsElement, JobConstants.JOB_SR_FROM, scheduledReport.getFrom(), ns);

			OMElement schedules = factory.createOMElement(JobConstants.JOB_SCHEDULE_LIST, ns);


			if(jobSchedules != null && jobSchedules.length > 0)
			{
				for(Schedule jobSchedule : jobSchedules)
				{
					OMElement schedule = factory.createOMElement(JobConstants.JOB_SCHEDULE, ns);
					AxisXmlUtils.addContent(factory, schedule, JobConstants.JOB_SCHEDULE_ID, jobSchedule.getId(), ns);								
					AxisXmlUtils.addContent(factory, schedule, JobConstants.JOB_SCHEDULE_JOB_ID, jobSchedule.getJobId(), ns);
					AxisXmlUtils.addContent(factory, schedule, JobConstants.JOB_SCHEDULE_EXPRESSION, jobSchedule.getExpression(), ns);
					AxisXmlUtils.addContent(factory, schedule, JobConstants.JOB_SCHEDULE_TIMEZONE, jobSchedule.getTimeZone(), ns);
					AxisXmlUtils.addContent(factory, schedule, JobConstants.JOB_SCHEDULE_ENABLE, jobSchedule.isEnable(), ns);
					schedules.addChild(schedule);
				}
			}
			scheduledJob.addChild(schedules);
			scheduledJob.addChild(jobDetailsElement);						
		}


		response.setResult(new XMLWebServiceResult(scheduledJob));

	}



	@Override
	public void getJobStatus(SchedulerWebServiceResult result, String spaceID, String jobOptionsXmlString) throws Exception
	{
		// TODO Auto-generated method stub

	}



	@Override
	public void killJob(SchedulerWebServiceResult result, String spaceID, String jobsRunStatusXmlString) throws Exception
	{
		// TODO Auto-generated method stub

	}


	@Override
	public void updateJob(SchedulerWebServiceResult result, String spaceID,
			String userID, String jobID, String jobOptionsXmlString) throws Exception
	{
		ScheduledReport tempReport = ScheduledReportOperation.parseReportDetailsXmlString(spaceID, userID, jobOptionsXmlString);

		String reportPath = Util.addCatalogPrefix(tempReport.getReportPath());
		String toReportPath = Util.addCatalogPrefix(tempReport.getToReportPath());
		String triggerReportPath = Util.addCatalogPrefix(tempReport.getTriggerReportPath());

		tempReport.setReportPath(reportPath);
		tempReport.setToReportPath(toReportPath);
		tempReport.setTriggerReportPath(triggerReportPath);
		tempReport.setId(jobID);

		if(tempReport.getSpaceID() == null)
		{
			tempReport.setSpaceID(spaceID);
		}

		ScheduledReportOperation.updateReport(spaceID, tempReport);

	}
}
