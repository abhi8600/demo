package com.birst.scheduler.webServices.login;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.axis2.context.MessageContext;
import org.apache.axis2.transport.http.HTTPConstants;
import org.apache.log4j.Logger;

import com.birst.scheduler.SchedulerServerContext;
import com.birst.scheduler.security.Net;
import com.birst.scheduler.shared.DefaultEmailProperties;
import com.birst.scheduler.shared.SchedulerUserBean;
import com.birst.scheduler.utils.Util;
import com.birst.scheduler.utils.UtilConstants;

public class SchedulerAccess
{
	public static final Logger logger = Logger.getLogger(SchedulerAccess.class);
	

	public String login(String[] loginCredentials)
	{

		boolean success = false;
		String result = null;
		StringBuilder sb = new StringBuilder();
		try
		{
			MessageContext context = MessageContext.getCurrentMessageContext();
			HttpServletRequest request = (HttpServletRequest) context.getProperty(HTTPConstants.MC_HTTP_SERVLETREQUEST);
			String requestIP = request.getRemoteAddr();
			String errorStr = Util.validateTrustedInternalRequest(requestIP);
			if(errorStr != null && errorStr.length() > 0)
			{
				return errorStr;
			}
/*			// check for trusted sender
			List<String> loginAllowedIPs = SchedulerServerContext.getSchedulerLoginAllowedIps();
			if (loginAllowedIPs != null)
			{
				if (!Net.containIp(loginAllowedIPs, requestIP))
				{
					logger.error("SchedulerAccess : Request IP not allowed: bad request IP: " + requestIP);
					logger.error("SchedulerAccess: Please check the SchedulerLogin.AllowsIPs in web.xml");
					sb.append("Request IP not allowed: bad request IP");
					return sb.toString();
				}
				if (logger.isTraceEnabled())
					logger.trace("SchedulerAccess: Valid request from host " + requestIP);
			} else
			{
				logger.error("No valid IPs list for Scheduler Login Service.");
				logger.error("Please check the SchedulerLogin.AllowsIPs in web.xml or customer.properties");
				sb.append("No valid IPs list for Scheduler Login Service.");
				return sb.toString();
			}*/

			HttpSession session = request.getSession(false);
			String userCredentials = "user id : "
					+ getParamValue(loginCredentials, UtilConstants.LOGIN_USER_ID)
					+ " , username : "
					+ getParamValue(loginCredentials, UtilConstants.LOGIN_USER_NAME);
			
			String spaceCredentials = " space id : "
					+ getParamValue(loginCredentials, UtilConstants.LOGIN_SPACE_ID);
			
			boolean validSession = false;
			if(session != null)
			{
				// Make sure that for a given session, the required bean exists.
				// If not just invalidate the current session and create a new one
				Object obj = session.getAttribute(UtilConstants.SESSION_USER_BEAN);
				if(obj != null)
				{
					validSession = true;
					updateSchedulerUserBean((SchedulerUserBean) obj, loginCredentials);
				}
				else
				{
					logger.debug("Logging out of the current invalid session : " + session.getId());
					session.invalidate();
				}
			}
			
			if (!validSession)
			{				
				success = true;
				logger.debug("Creating new session - " + userCredentials + " , " + spaceCredentials);
				session = request.getSession();
				SchedulerUserBean schedulerUserBean = createSchedulerUserBean(loginCredentials);
				session.setAttribute(UtilConstants.SESSION_USER_BEAN,schedulerUserBean);
				
			} else
			{				
				success = true;				
				logger.debug("Valid session existing for " + userCredentials + " , " + spaceCredentials);
			}

		} catch (Exception e)
		{
			success = false;
			logger.error("Exception in Schedler Login. Not able to login : " + e.getMessage(), e);
			sb.append("Exception during login. Please check Scheduler Log: " + e.getMessage());
		}
		if (success)
		{
			if (logger.isTraceEnabled())
				logger.trace("Scheduler: Successful Login");
			result = Boolean.toString(success);
		} else
		{
			result = sb.toString();
		}
		return result;
	}
	
	private void updateSchedulerUserBean(SchedulerUserBean schedulerUserBean, String[] loginCredentials)
	{
		if(schedulerUserBean != null && loginCredentials != null && loginCredentials.length > 0)
		{
			String defaultReportEmailFrom = getParamValue(loginCredentials, UtilConstants.LOGIN_SPACE_REPORT_EMAIL_FROM);
			String defaultReportEmailSubject = getParamValue(loginCredentials, UtilConstants.LOGIN_SPACE_REPORT_EMAIL_SUBJECT);
			schedulerUserBean.setReportEmailProperties(new DefaultEmailProperties(defaultReportEmailFrom, defaultReportEmailSubject));
		}
	}
	
	private SchedulerUserBean createSchedulerUserBean(String[] loginCredentials)
	{
		SchedulerUserBean schedulerUserBean = new SchedulerUserBean();
		schedulerUserBean.setUserID(getParamValue(loginCredentials, UtilConstants.LOGIN_USER_ID));
		schedulerUserBean.setUserName(getParamValue(loginCredentials, UtilConstants.LOGIN_USER_NAME));
		schedulerUserBean.setOperationsMode(Boolean.valueOf(getParamValue(loginCredentials, UtilConstants.LOGIN_USER_OPERATIONS_MODE)));
		schedulerUserBean.setSpaceID(getParamValue(loginCredentials, UtilConstants.LOGIN_SPACE_ID));
		String defaultReportEmailFrom = getParamValue(loginCredentials, UtilConstants.LOGIN_SPACE_REPORT_EMAIL_FROM);
		String defaultReportEmailSubject = getParamValue(loginCredentials, UtilConstants.LOGIN_SPACE_REPORT_EMAIL_SUBJECT);
		schedulerUserBean.setReportEmailProperties(new DefaultEmailProperties(defaultReportEmailFrom, defaultReportEmailSubject));
		return schedulerUserBean;
	}

	/*
	 * Logout server - invalidate the session, triggering the release of
	 * resources
	 */
	public String logout()
	{
		boolean success = false;
		try
		{
			MessageContext context = MessageContext.getCurrentMessageContext();
			HttpServletRequest request = (HttpServletRequest) context.getProperty(HTTPConstants.MC_HTTP_SERVLETREQUEST);
			HttpSession session = request.getSession(false);
			if (session != null)
			{
				logger.info("invalidating the session");
				session.invalidate();
				success = true;
			} else
			{
				logger.warn("No Session found in Scheduler logout. Nothing to invalidate");
				// already got logged out before
				success = true;
			}
		} catch (Exception e)
		{
			logger.error("Exception in logout operation : " + e.getMessage(), e);
			success = false;
		}
		String result = Boolean.toString(success);
		return result;
	}

	/*
	 * Polling service - does nothing, used to stop the session from timing out
	 */
	public void poll()
	{
		MessageContext context = MessageContext.getCurrentMessageContext();
		HttpServletRequest request = (HttpServletRequest) context.getProperty(HTTPConstants.MC_HTTP_SERVLETREQUEST);
		HttpSession session = request.getSession(false);
		if (session != null)
		{
			if (logger.isTraceEnabled())
				logger.trace("Polling trace: Session " + session.getId());
		}
	}

	private String getParamValue(String[] keyValuePairArray, String param)
	{
		String value = null;
		for (String str : keyValuePairArray)
		{
			if (str.startsWith(param))
			{
				value = str.substring(param.length() + 1);
				break;
			}
		}
		return value;
	}

}
