package com.birst.scheduler.webServices.connector;

/**
 * 
 * @author mpandit
 *
 */
public class RecoverableException extends Exception {
	public RecoverableException(String message, Throwable cause)
	{
		super(message, cause);
	}
}
