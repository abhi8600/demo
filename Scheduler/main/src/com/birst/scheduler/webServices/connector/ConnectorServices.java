package com.birst.scheduler.webServices.connector;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;

import com.birst.scheduler.SchedulerServerContext;
import com.birst.scheduler.salesforce.SFDCInfo;
import com.birst.scheduler.utils.BaseException;
import com.birst.scheduler.utils.EncryptionService;
import com.birst.scheduler.utils.XmlUtils;

public class ConnectorServices {
	private static final Logger logger = Logger.getLogger(ConnectorServices.class);
	public static final String NODE_CONNECTOR_SERVER_URL = "ConnectorServerURL";
	public static final String NODE_CONNECTOR_APPLICATION = "ConnectorApplication";
	private static final String CONNECTORCONTROLLER_APPLICATION = "ConnectorController";
	private static final String LISTCONNECTORS_SERVLET = "ListConnectors";
	public static final String ICONNECTOR_SERVLET = "IConnectorServlet";
	private static final String USER_ID = "UserID";
	private static final String SPACE_ID = "SpaceID";
	private static final String BIRST_CONNECTOR_VERSION = "BirstConnectorVersion";
	public static final int BirstConnectorVersion = 1;
	private static final String CLIENT = "Client";
	private static final String CLIENT_VALUE = "Scheduler";
	private static final String CONNECTION_STRING = "ConnectionString";
	private static final String METHOD = "Method";
	private static final String START_EXTRACT = "startExtract";
	private static final String SERVLET_EXCEPTION = "Problem in getConnectorServletURL for ";
	private static final String CONNECTOR_LIST_EXCEPTION = "Problem in getConnectorsList for ";
	private static final String NODE_ISINCREMENTAL_ROOT = "IsIncrementalRoot";
	private static final String NODE_EXTRACT_GROUPS = "ExtractGroups";
	private static final String NODE_EXTRACT_GROUP = "ExtractGroup";
	private static final String NODE_EXTRACTION_OBJECT = "ExtractionObject";
	private static final String NODE_EXTRACTION_OBJECTS = "ExtractionObjects";
	private static final String NODE_OBJECT_NAME = "ObjectName";
	private static final String NODE_IS_INCREMENTAL = "IsIncremental";
	private static final String NODE_NAME = "Name";
	private static final String NODE_USE_LAST_MODIFIED = "UseLastModified";
	private static final String NODE_CONNECTOR_INFO = "ConnectorInfo";
	private static final String NODE_SRC_FILE_NAME_PRE = "SourceFileNamePrefix";
	private static final String NODE_CONNECTION_PROPS = "ConnectionProperties";
	private static final String NODE_Connector_API_VERSION = "ConnectorAPIVersion";
	private static final String NODE_CONNECTOR_NAME = "ConnectorName";
	private static final String NODE_CONNECTOR_VERSION = "ConnectorVersion";
	private static final String NODE_CONNECTOR_CONNECTIONSTRING = "ConnectorConnectionString";
	private static final String NODE_CONNECTOR_IS_DEFAULT_VERSION = "IsDefaultAPIVersion";
	private static final String NODE_CONNECTION_PROP = "ConnectionProperty";
	private static final String CREDENTIALS_XML = "CredentialsXML";
	private static final String IS_INCREMENTAL_XML = "isIncrementalXml";
	private static final String UID = "UID";
	private static final String NUM_OF_THREADS = "NumThreads";
	private static final String VARIABLES = "Variables";
	private static final String EXTRACT_GROUPS_XML = "ExtractGroupsXML";
	private static final String CONNECTOR_URI_PREFIX = "connector:birst://";
	private static final String CONNECTOR_DIR = "connectors";
	private static final String TRANSACTION_OBJECT_FILE = "transactional-objects.txt";
	private static final String BIRST_PARAMETER_FILE = "birst-parameters.txt";
	private static final String BIRST_CONNECTOR_VARIABLES_FILE = "variables.txt";
	private static final String CONFIG_SUFFIX = "_config.xml";
	private static final String COLON = ":";
	private static final char PIPE = '|';
	private static final String EMPTY_STRING = "";
	private static final String DASH = " - ";
	public static final String UTF8 = "UTF-8";
	private static final String SUCCESS = "Success";
	private static final String RESULT = "Result";
	private static final String REASON = "Reason";
	private static final String ERROR_CODE = "ErrorCode";
	private static final String VALUE = "Value";
	private static final String IS_ENCRYPTED = "IsEncrypted";
	private static final String DOC_EXCEPTION = "Can not build document object";
	private static final String STAGING_TABLE_PREFIX = "ST_";
	public static final int SLEEP = 5000;
	public static final int THREAD_CNT = 4;
	
	public class ConnectorInfo {
		public String connectorServerURL;
		public String connectorApplication;
	}
	
	private static Document buildDocument(String connectorType, String spaceDirectory) throws Exception {
		String file = null;
		if (connectorType.indexOf(":") >= 0)
		{
			file = spaceDirectory + File.separator + CONNECTOR_DIR + File.separator + getConnectorNameFromConnectorConnectionString(connectorType) + CONFIG_SUFFIX;
		}
		else
		{
			file = spaceDirectory + File.separator + CONNECTOR_DIR + File.separator + connectorType + CONFIG_SUFFIX;
			File f = new File(file);
			if (!f.exists() && "sfdc".equalsIgnoreCase(connectorType))
			{
				file = spaceDirectory + File.separator + "sforce.xml";
			}
		}
		
		StringBuilder sb = new StringBuilder();
		Document doc = null;
		try {
			FileInputStream fis = new FileInputStream(file);
			InputStreamReader in = new InputStreamReader(fis, UTF8);
			BufferedReader br = new BufferedReader(in);
			String nextLine;
			while ((nextLine = br.readLine()) != null) {
			     sb.append(nextLine.trim());
			}
			br.close();
			SAXBuilder saxBuilder = new SAXBuilder();
			doc = saxBuilder.build(new StringReader(sb.toString()));
		} catch (Exception ex) {
			throw new Exception(DOC_EXCEPTION);
		}
		return doc;
	}
	
	public String findConnectorAPIVersionInConfig(String connectorName, String spaceDirectory) {
		try {
			// Parse connector config file.
			Document doc = buildDocument(connectorName, spaceDirectory);
			if (doc != null)
			{
				Element connAPIVersionElement = doc.getRootElement().getChild(NODE_Connector_API_VERSION);
				if (connAPIVersionElement != null)
				{
					return connAPIVersionElement.getText();
				}
			}			
		} catch (Exception ex) {
			logger.error(ex.toString(), ex);
		}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	public static String getCredentialsXml(SFDCInfo connectorInfo, String connectorType, String spaceDirectory) {
		String credentialsXml = EMPTY_STRING;
		OMFactory factory = OMAbstractFactory.getOMFactory();
		OMNamespace ns = null;
		try {
			// Parse connector config file.
			Document doc = buildDocument(connectorType, spaceDirectory);
			Element credentialNodes = doc.getRootElement().getChild(NODE_CONNECTION_PROPS);
			List<Element> credentialChildren = credentialNodes.getChildren();
			String name = EMPTY_STRING;
			String value = EMPTY_STRING;
			String isEncrypted = EMPTY_STRING;
			
			OMElement credentialRootEl = factory.createOMElement(NODE_CONNECTOR_INFO, ns);
			for (Iterator<Element> iter = credentialChildren.iterator(); iter.hasNext(); ) {
				Element chElement = iter.next();
				if (chElement.getName().equals(NODE_CONNECTION_PROP)) {
					name = (chElement.getChild(NODE_NAME) != null) ? chElement.getChild(NODE_NAME).getValue() : EMPTY_STRING;
					value = (chElement.getChild(VALUE) != null) ? chElement.getChild(VALUE).getValue() : EMPTY_STRING;
					isEncrypted = (chElement.getChild(IS_ENCRYPTED) != null) ? chElement.getChild(IS_ENCRYPTED).getValue() : Boolean.FALSE.toString();
					if (value != null && !value.isEmpty())
					{
						value = (Boolean.parseBoolean(isEncrypted)) ? EncryptionService.getInstance().decrypt(value) : value;
					}
					XmlUtils.addContent(factory, credentialRootEl, name, value, ns);					
				}
			}
			credentialsXml = XmlUtils.convertToString(credentialRootEl);
		} catch (Exception ex) {
			System.out.println(ex);
		}
		return credentialsXml;
	}
	
	@SuppressWarnings("unchecked")
	private static String getConnectorConfigurationValue(String connectorType, String spaceDirectory, String searchFor) {
		String returnValue = EMPTY_STRING;
		try {
			Document doc = buildDocument(connectorType, spaceDirectory);
			Element credentialNodes = doc.getRootElement().getChild(NODE_CONNECTION_PROPS);
			List<Element> credentialChildren = credentialNodes.getChildren();
			Iterator<Element> iterator = (Iterator<Element>) credentialChildren.iterator();
			String name = EMPTY_STRING;
			String value = EMPTY_STRING;
			Element node;
			while (iterator.hasNext()) {
				node = iterator.next();
				if (node.getName().equals(NODE_CONNECTION_PROP)) {
					name = (node.getChild(NODE_NAME) != null) ? node.getChild(NODE_NAME).getValue() : EMPTY_STRING;
					value = (node.getChild(VALUE) != null) ? node.getChild(VALUE).getValue() : EMPTY_STRING;
									
					if (name.equals(searchFor)) {
						returnValue = value;
					}
				}
			}
		} catch (Exception ex) {
			System.out.println(ex);
		}
		return returnValue;
	}
	
	@SuppressWarnings("unchecked")
	public static String getIncrementalXml(String connectorType, String spaceDirectory) {
		//return "<IsIncrementalRoot><ExtractionObject><ObjectName>Account</ObjectName><IsIncremental>false</IsIncremental></ExtractionObject></IsIncrementalRoot>";
		String isIncrementalXml = EMPTY_STRING; 
		OMFactory factory = OMAbstractFactory.getOMFactory();
		OMNamespace ns = null;
		try {			
			// Parse connector config file.
			Document doc = buildDocument(connectorType, spaceDirectory);
			Element extractionNodes = doc.getRootElement().getChild(NODE_EXTRACTION_OBJECTS);
			List<Element> extractionChildren = extractionNodes.getChildren();
			Iterator<Element> iterator = (Iterator<Element>) extractionChildren.iterator();
			String name = EMPTY_STRING;
			String useLastModified = EMPTY_STRING;
			String filePath = getConnectorTransactionalObjectFilePath(spaceDirectory, connectorType);
			List<String> lines = FileUtils.readLines(new File(filePath), UTF8);
			File file = new File(filePath);
			OMElement isIncrementalRootEl = factory.createOMElement(NODE_ISINCREMENTAL_ROOT, ns);
			String prefix = getConnectorConfigurationValue(connectorType, spaceDirectory, NODE_SRC_FILE_NAME_PRE);
			prefix = (prefix != null && !prefix.equals(EMPTY_STRING)) ? prefix + "_" : EMPTY_STRING;
			Element node;
			OMElement eobjEl;
			while (iterator.hasNext()) {
				node = iterator.next();
				if (node.getName().equals(NODE_EXTRACTION_OBJECT)) {
					name = node.getChild(NODE_NAME).getValue();
					if (!file.exists()) {
						useLastModified = node.getChild(NODE_USE_LAST_MODIFIED).getValue();
					} else {
						useLastModified = (lines.contains(STAGING_TABLE_PREFIX + prefix + name)) ? Boolean.TRUE.toString() : Boolean.FALSE.toString();
					}
					eobjEl = factory.createOMElement(NODE_EXTRACTION_OBJECT, ns);
					XmlUtils.addContent(factory, eobjEl, NODE_OBJECT_NAME, name, ns);
					XmlUtils.addContent(factory, eobjEl, NODE_IS_INCREMENTAL, useLastModified, ns);
					isIncrementalRootEl.addChild(eobjEl);
				}
			}

			// Build incremental xml.
			isIncrementalXml = XmlUtils.convertToString(isIncrementalRootEl);
		} catch (Exception ex) {
			System.out.println(ex);
		}
			
		return isIncrementalXml;
	}
	
	private static String getConnectorTransactionalObjectFilePath(String spaceDirectory, String connectorType) {
		return spaceDirectory + File.separator + CONNECTOR_DIR + File.separator + TRANSACTION_OBJECT_FILE;
	}
	
	public static String getBirstParametersFilePath(String spaceDirectory, String connectorType) {
		return spaceDirectory + File.separator + CONNECTOR_DIR + File.separator + BIRST_PARAMETER_FILE;
	}
	
	public static String getConnectorVariableFilePath(String spaceDirectory) {
		return spaceDirectory + File.separator + CONNECTOR_DIR + File.separator + BIRST_CONNECTOR_VARIABLES_FILE;
	}
	
	public static String getConnectorNameFromConnectorConnectionString(String connectionString)
	{
		String connName = connectionString.substring(CONNECTOR_URI_PREFIX.length());
		if (connName.indexOf(COLON) >= 0)
			connName = connName.substring(0, connName.indexOf(COLON));
		return connName;
	}
	
	public static Map<String, String> getConnectorDefaultArguments(String userID, String spaceID, String spaceDirectory) {
		Map<String, String> argumentsMap = new HashMap<String, String>();
		argumentsMap.put(USER_ID, userID);
		argumentsMap.put(SPACE_ID, spaceID);
		if (spaceDirectory != null)
		{
			argumentsMap.put("SpaceDirectory", spaceDirectory);
		}
		argumentsMap.put(CLIENT, CLIENT_VALUE);
		argumentsMap.put(BIRST_CONNECTOR_VERSION, String.valueOf(BirstConnectorVersion));
		return argumentsMap;
	}
	
	@SuppressWarnings("unchecked")
	public ConnectorInfo getConnectorServletURL(String userID, String spaceID, String spaceDirectory, String connectionString) {
		ConnectorInfo connectorInfo = null;
		try {
			ConnectorHttpClient client = new ConnectorHttpClient();
			Map<String, String> argumentsMap = getConnectorDefaultArguments(userID, spaceID, spaceDirectory);
			argumentsMap.put(CONNECTION_STRING, connectionString);
			String result = client.callConnectorServlet(SchedulerServerContext.getConnectorControllerURI(), CONNECTORCONTROLLER_APPLICATION, LISTCONNECTORS_SERVLET, true, argumentsMap);
			OMElement connectorsEl = XmlUtils.convertToOMElement(result);
			if (connectorsEl == null)
			{
				throw new Exception();
			}
			String connectorServerURL = null;
			String connectorApplication = null;
			if (connectorsEl != null)
			{
				Iterator<OMElement> connectorIterator = connectorsEl.getChildElements();
				OMElement connectorEl = connectorIterator.next();
				for (Iterator<OMElement> connectorDetailIterator = connectorEl.getChildElements(); connectorDetailIterator.hasNext(); )
				{
					OMElement el = connectorDetailIterator.next();
					String elName = el.getLocalName();
					if (NODE_CONNECTOR_SERVER_URL.equals(elName))
					{
						connectorServerURL = XmlUtils.getStringContent(el);
					}
					else if (NODE_CONNECTOR_APPLICATION.equals(elName))
					{
						connectorApplication = XmlUtils.getStringContent(el);
					}
				}
			}
			if (connectorServerURL != null && connectorApplication != null)
			{
				connectorInfo = new ConnectorInfo();
				connectorInfo.connectorServerURL = connectorServerURL;
				connectorInfo.connectorApplication = connectorApplication;				
			}
		}
		catch (Exception ex)
		{
			logger.error(SERVLET_EXCEPTION + connectionString + DASH + ex.getMessage(), ex);
		}		
		return connectorInfo;
	}
	
	public class Connector
    {
        String ConnectorName;
        String ConnectorAPIVersion;
        boolean IsDefaultAPIVersion;
        boolean IsSelectedAPIVersion;
        String ConnectionString;
		public boolean isIsSelectedAPIVersion()
		{
			return IsSelectedAPIVersion;
		}
		public void setIsSelectedAPIVersion(boolean isSelectedAPIVersion)
		{
			IsSelectedAPIVersion = isSelectedAPIVersion;
		}
		public String getConnectorName()
		{
			return ConnectorName;
		}
		public String getConnectorAPIVersion()
		{
			return ConnectorAPIVersion;
		}
		public boolean isIsDefaultAPIVersion()
		{
			return IsDefaultAPIVersion;
		}
		public String getConnectionString()
		{
			return ConnectionString;
		}
    }
	
	public List<Connector> getConnectorsList(String userID, String spaceID, String spaceDirectory) {
		OMElement root = null;
		List<Connector> connectors = new ArrayList<Connector>();
		try {
			ConnectorHttpClient client = new ConnectorHttpClient();
			Map<String, String> argumentsMap = getConnectorDefaultArguments(userID, spaceID, spaceDirectory);
			String result = client.callConnectorServlet(SchedulerServerContext.getConnectorControllerURI(), CONNECTORCONTROLLER_APPLICATION, LISTCONNECTORS_SERVLET, true, argumentsMap);
			root = XmlUtils.convertToOMElement(result);
			if (root == null)
			{
				UnrecoverableException ure = new UnrecoverableException("Unable to get List of Connectors", new Exception());
				ure.setErrorCode(BaseException.ERROR_OTHER);
				throw ure;
			}
			
			Iterator<OMElement> iter = root.getChildren();
			Iterator<OMElement> iter2;
			while (iter.hasNext()) {
				OMElement connectorRootElement = iter.next();
				iter2 = connectorRootElement.getChildren();
				Connector conn = new Connector();
				while (iter2.hasNext()) {
					OMElement connectorElement = iter2.next();
					String name = connectorElement.getLocalName();
					switch (name)
					{
						case NODE_CONNECTOR_NAME	:	conn.ConnectorName = connectorElement.getText(); break;
						case NODE_CONNECTOR_VERSION	:	conn.ConnectorAPIVersion = connectorElement.getText(); break;
						case NODE_CONNECTOR_CONNECTIONSTRING	:	conn.ConnectionString = connectorElement.getText(); break;
						case NODE_CONNECTOR_IS_DEFAULT_VERSION	:	conn.IsDefaultAPIVersion = Boolean.parseBoolean(connectorElement.getText()); break;
					}
				}	
				connectors.add(conn);
			}
		}catch(Exception ex){
			logger.error(CONNECTOR_LIST_EXCEPTION + spaceID + DASH + ex.getMessage(), ex);
		}
		return connectors;
	}
	
	public boolean startExtract(String userID, String spaceID, String spaceDirectory, ConnectorInfo connectorInfo, String credentialsXml, 
			String isIncrementalXml, String uid, String numThreads, String variables, String extractGroupsXML) throws Exception
	{
		ConnectorHttpClient client = new ConnectorHttpClient();
		Map<String, String> argumentsMap = getConnectorDefaultArguments(userID, spaceID, spaceDirectory);
		argumentsMap.put(METHOD, START_EXTRACT);
		argumentsMap.put(CREDENTIALS_XML, credentialsXml);
		argumentsMap.put(IS_INCREMENTAL_XML, isIncrementalXml);
		argumentsMap.put(UID, uid);
		argumentsMap.put(NUM_OF_THREADS, numThreads);
		argumentsMap.put(VARIABLES, variables);
		argumentsMap.put(EXTRACT_GROUPS_XML, extractGroupsXML);
		String result = client.callConnectorServlet(connectorInfo.connectorServerURL, connectorInfo.connectorApplication, ICONNECTOR_SERVLET, true, argumentsMap);
		return parseConnectorBooleanResult(result);		
	}
	
	@SuppressWarnings("unchecked")
	public static boolean parseConnectorBooleanResult(String result) throws Exception
	{
		OMElement resultEl = XmlUtils.convertToOMElement(result);
		boolean isSuccess = false;
		for (Iterator<OMElement> resultIterator = resultEl.getChildElements(); resultIterator.hasNext(); )
		{
			OMElement el = resultIterator.next();
			if (SUCCESS.equals(el.getLocalName()))
			{
				isSuccess = Boolean.valueOf(XmlUtils.getBooleanContent(el));
				break;
			}
		}
		boolean resultValue = false;
		if (isSuccess)
		{
			for (Iterator<OMElement> resultIterator = resultEl.getChildElements(); resultIterator.hasNext(); )
			{
				OMElement el = resultIterator.next();
				if (RESULT.equals(el.getLocalName()))
				{
					resultValue = Boolean.valueOf(XmlUtils.getBooleanContent(el));								
				}
			}
		}
		else
		{
			String message = null;
			int errorCode = BaseException.ERROR_OTHER;
			for (Iterator<OMElement> resultIterator = resultEl.getChildElements(); resultIterator.hasNext(); )
			{
				OMElement el = resultIterator.next();
				if (REASON.equals(el.getLocalName()))
				{
					message = el.getText();
				}
				if (ERROR_CODE.equals(el.getLocalName()))
				{
					errorCode = Integer.parseInt(el.getText());
				}
			}
			UnrecoverableException ure = new UnrecoverableException(message, new Exception());
			ure.setErrorCode(errorCode);
			throw ure;
		}		
		return resultValue;
	}
	
	@SuppressWarnings("unchecked")
	public static String getVariables(String spaceDirectory, String connectorType) throws IOException {
		StringBuilder sb = new StringBuilder();
		String filePath = getConnectorVariableFilePath(spaceDirectory);
		List<String> lines = FileUtils.readLines(new File(filePath), ConnectorServices.UTF8);
		for (String line : lines) {
			sb.append(line).append(PIPE);
		}
		return sb.toString();
	}
	
	public static String getExtractGroupsXML(String extractGroups)
	{
		OMFactory factory = OMAbstractFactory.getOMFactory();
		OMNamespace ns = null;
		OMElement extractGroupsEl = factory.createOMElement(NODE_EXTRACT_GROUPS, ns);
		if (extractGroups != null && extractGroups.trim().length() > 0)
		{
			String[] extractionGroups = extractGroups.split(",");
			for (String eg : extractionGroups)
			{
				XmlUtils.addContent(factory, extractGroupsEl, NODE_EXTRACT_GROUP, eg, ns);
			}
		}
		return XmlUtils.convertToString(extractGroupsEl);
	}
}
