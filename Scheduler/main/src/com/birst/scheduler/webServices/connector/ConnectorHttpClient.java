package com.birst.scheduler.webServices.connector;

import java.io.IOException;
import java.net.Socket;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.ProtocolVersion;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.impl.DefaultHttpClientConnection;
import org.apache.http.message.BasicHttpEntityEnclosingRequest;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.BasicHttpProcessor;
import org.apache.http.protocol.HTTP;
import org.apache.http.protocol.HttpContext;
import org.apache.http.protocol.HttpProcessor;
import org.apache.http.protocol.HttpRequestExecutor;
import org.apache.http.protocol.RequestConnControl;
import org.apache.http.protocol.RequestContent;
import org.apache.http.protocol.RequestExpectContinue;
import org.apache.http.protocol.RequestTargetHost;
import org.apache.http.protocol.RequestUserAgent;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;

public class ConnectorHttpClient
{
	private static final Logger logger = Logger.getLogger(ConnectorHttpClient.class);
	private static final String CHAR_SET = "UTF-8";
	private static final ProtocolVersion HTTP_VERSION = HttpVersion.HTTP_1_1;
	private static final String USER_AGENT = "SMIWeb/1.1";
	private static final String HTTP_CONNECTION_ATTR = "http.connection";
	private static final String HTTP_TARGET_HOST = "http.target_host";
	private static final String POST_METHOD = "POST";
	private static final String REQUEST_DESC = ">> Request URI: ";
	private static final String RESPONSE_DESC = "<< Response: ";
	private static final String SEPARATOR = "==============";
	private static final String FORWARD_SLASH = "/";
  
	@SuppressWarnings("unchecked")
	public String callConnectorServlet(String url, String servletApplicationName, String servletName, boolean isPostRequest, Map<String, String> parameters)
	{
		List nvp = new ArrayList();
		if (parameters != null)
		{
		  for (String key : parameters.keySet())
		  {
		    nvp.add(new BasicNameValuePair(key, (String)parameters.get(key)));
		  }
		}
		
		// Build http parameters.
		HttpParams params = new BasicHttpParams();
		HttpProtocolParams.setVersion(params, HTTP_VERSION);
		HttpProtocolParams.setContentCharset(params, CHAR_SET);
		HttpProtocolParams.setUserAgent(params, USER_AGENT);
		HttpProtocolParams.setUseExpectContinue(params, true);

		// Build http processor.
		HttpProcessor httpproc = new BasicHttpProcessor();
		((BasicHttpProcessor)httpproc).addInterceptor(new RequestContent());
		((BasicHttpProcessor)httpproc).addInterceptor(new RequestTargetHost());
		((BasicHttpProcessor)httpproc).addInterceptor(new RequestConnControl());
		((BasicHttpProcessor)httpproc).addInterceptor(new RequestUserAgent());
		((BasicHttpProcessor)httpproc).addInterceptor(new RequestExpectContinue());

		// Build connector host and connection.
		HttpContext context = new BasicHttpContext(null);
		URI uri = null;
		try {
			uri = new URI(url);
		}
		catch (URISyntaxException e) {
			e.printStackTrace();
		}
		HttpHost host = new HttpHost(uri.getHost(), uri.getPort(), uri.getScheme());
		DefaultHttpClientConnection conn = new DefaultHttpClientConnection();
		context.setAttribute(HTTP_CONNECTION_ATTR, conn);
		context.setAttribute(HTTP_TARGET_HOST, host);

		// Execute connector call.
		String responseStr = null;
		try
		{
			HttpRequestExecutor httpexecutor = new HttpRequestExecutor();
			if (!conn.isOpen()) {
				Socket socket = new Socket(host.getHostName(), host.getPort());
				conn.bind(socket, params);
			}
			BasicHttpEntityEnclosingRequest request = new BasicHttpEntityEnclosingRequest(POST_METHOD, FORWARD_SLASH + servletApplicationName + FORWARD_SLASH + servletName);
			System.out.println(REQUEST_DESC + request.getRequestLine().getUri());

			request.setEntity(new UrlEncodedFormEntity(nvp, CHAR_SET));
			request.setParams(params);
			httpexecutor.preProcess(request, httpproc, context);
			HttpResponse response = httpexecutor.execute(request, conn, context);
			response.setParams(params);
			httpexecutor.postProcess(response, httpproc, context);

			System.out.println(RESPONSE_DESC + response.getStatusLine());
			responseStr = EntityUtils.toString(response.getEntity(), HTTP.UTF_8);
			System.out.println(responseStr);
			System.out.println(SEPARATOR);
		}
		catch (Exception ioe)
		{
		  ioe.printStackTrace();
		}
		finally
		{
			try {
				conn.close();
			}
			catch (IOException ioe)
			{
				logger.error(ioe.getMessage(), ioe);
			}
		}
		return responseStr;
	}
}