/**
 * 
 */
package com.birst.scheduler.webServices;

import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
import org.apache.axiom.om.OMNode;

public class XMLWebServiceResult implements IWebServiceResult {

	protected OMNode result;
	
	protected XMLWebServiceResult() {}
	
	public XMLWebServiceResult(OMNode r) {
		this.result = r;
	}
	/* (non-Javadoc)
	 * @see com.successmetricsinc.WebServices.IWebServiceResult#addContent(org.apache.axiom.om.OMElement, org.apache.axiom.om.OMFactory, org.apache.axiom.om.OMNamespace)
	 */
	@Override
	public void addContent(OMElement parent, OMFactory factory, OMNamespace ns) {
		parent.addChild(result);
	}

}
