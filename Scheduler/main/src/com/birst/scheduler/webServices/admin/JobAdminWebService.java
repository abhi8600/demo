package com.birst.scheduler.webServices.admin;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.Map.Entry;


import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
import org.apache.axiom.om.OMText;
import org.apache.log4j.Logger;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.matchers.GroupMatcher;

import com.birst.scheduler.SchedulerServerContext;
import com.birst.scheduler.Migration.MigrationOperation;
import com.birst.scheduler.Migration.MigrationUtils.MigrationType;
import com.birst.scheduler.Report.ScheduledReport;
import com.birst.scheduler.Report.ScheduledReportOperation;
import com.birst.scheduler.exceptions.NotLoggedInException;
import com.birst.scheduler.exceptions.UnAuthorizedRequestCredentials;
import com.birst.scheduler.process.ProcessInfo;
import com.birst.scheduler.process.ProcessJobOperation;
import com.birst.scheduler.shared.BaseJobObject;
import com.birst.scheduler.shared.CopyJob;
import com.birst.scheduler.shared.JobHistory;
import com.birst.scheduler.shared.Schedule;
import com.birst.scheduler.shared.SchedulerUserBean;
import com.birst.scheduler.utils.AxisXmlUtils;
import com.birst.scheduler.utils.JobConstants;
import com.birst.scheduler.utils.SchedulerDBConnection;
import com.birst.scheduler.utils.SchedulerDatabase;
import com.birst.scheduler.utils.Util;
import com.birst.scheduler.utils.UtilConstants;
import com.birst.scheduler.utils.UtilConstants.JobType;
import com.birst.scheduler.webServices.SchedulerWebService;
import com.birst.scheduler.webServices.XMLWebServiceResult;
import com.birst.scheduler.webServices.response.SchedulerWebServiceResult;



public class JobAdminWebService extends SchedulerWebService
{
	private Logger logger = Logger.getLogger(JobAdminWebService.class);	
	
	public OMElement addJob(String spaceID, String userID, String jobType, String jobOptionsXmlString)
	{
		logger.debug("Request for adding job");
		SchedulerWebServiceResult response = new SchedulerWebServiceResult();
		boolean typeFound = false;
		try
		{		
			if(!isLoggedIn())
			{
				throw new NotLoggedInException();
			}
			
			if(!isAuthorized(spaceID))
			{
				throw new UnAuthorizedRequestCredentials();
			}
			response = JobAdminUtil.addJob(spaceID, userID, jobType, jobOptionsXmlString);

		}
		catch(Exception ex)
		{
			logger.error("Exception in adding for space " + spaceID, ex);			
			response.formatException(ex);
		}
		return response.toOMElement();
	}
	

	public OMElement getJobDetails(String spaceID, String jobType, String jobID)
	{
		logger.debug("Request for getting job details : jobID : " + jobID );		
		SchedulerWebServiceResult response = new SchedulerWebServiceResult();
		try
		{	
			if(!isLoggedIn())
			{
				throw new NotLoggedInException();
			}
			
			if(!isAuthorized(spaceID))
			{
				throw new UnAuthorizedRequestCredentials();
			}
			
			response = JobAdminUtil.getJobDetails(spaceID, jobType, jobID);

		}
		catch(Exception ex)
		{
			logger.error("Exception in getting job details for space " + spaceID + " " + jobID, ex);			
			response.formatException(ex);
		}
		return response.toOMElement();
	}
	
	public OMElement copyJob(String spaceID, String userID, String jobType, String copyOptionsXmlString, String jobOptionsXmlString)
	{
		logger.debug("Request for migrating job : spaceID : " + spaceID);
		SchedulerWebServiceResult response = new SchedulerWebServiceResult();
		try
		{	
			if(!isLoggedIn())
			{
				throw new NotLoggedInException();
			}
			
			if(!isAuthorized(spaceID))
			{
				throw new UnAuthorizedRequestCredentials();
			}
			
			response = JobAdminUtil.copyJob(spaceID, userID, jobType, copyOptionsXmlString, jobOptionsXmlString);
			
			
		}
		catch(Exception ex)
		{
			logger.error("Exception in copying job details for space " + spaceID, ex );			
			response.formatException(ex);
		}
		return response.toOMElement();		
	}
	
	public OMElement updateJob(String spaceID, String userID, String jobType, String jobID, String jobOptionsXmlString)
	{
		logger.debug("Request for updating job : jobID : " + jobID);
		SchedulerWebServiceResult response = new SchedulerWebServiceResult();
		try
		{	
			if(!isLoggedIn())
			{
				throw new NotLoggedInException();
			}
			
			if(!isAuthorized(spaceID))
			{
				throw new UnAuthorizedRequestCredentials();
			}
			
			response = JobAdminUtil.updateJob(spaceID, userID, jobType, jobID, jobOptionsXmlString);			
			
		}
		catch(Exception ex)
		{
			logger.error("Exception in updating job details for space " + spaceID + " job " + jobID, ex );			
			response.formatException(ex);
		}
		return response.toOMElement();		
	}
	
	
	public OMElement removeJob(String spaceID, String jobType, String jobID)
	{		
		logger.debug("Request for removing job : jobID : " + jobID);
		SchedulerWebServiceResult response = new SchedulerWebServiceResult();
		try
		{				
			logger.debug("Request for removing job" + jobID);
			if(!isLoggedIn())
			{
				throw new NotLoggedInException();
			}
			
			if(!isAuthorized(spaceID))
			{
				throw new UnAuthorizedRequestCredentials();
			}
			
			response = JobAdminUtil.removeJob(spaceID, jobType, jobID);			
		}
		catch(Exception ex)
		{
			logger.error("Exception in removing job space " + spaceID + " job " + jobID, ex );			
			response.formatException(ex);
		}
		return response.toOMElement();	
	}
	
	
	@SuppressWarnings("unchecked")
	public OMElement getJobStatus(String spaceID, String jobType, String jobOptionsXmlString)
	{
		logger.debug("Request for getting job status : space : " + spaceID + " : type : " + jobType);
		SchedulerWebServiceResult response = new SchedulerWebServiceResult();
		String jobId = null;
		try
		{	// if the request is internally by pass the check
			// This is for the web service request from Acorn similar to TrustedService in SMIWeb
			if(!isLoggedIn())
			{
				throw new NotLoggedInException();
			}
			
			if(!isAuthorized(spaceID))
			{
				throw new UnAuthorizedRequestCredentials();
			}
			
			response = JobAdminUtil.getJobStatus(spaceID, jobType, jobOptionsXmlString);			
		}
		catch(Exception ex)
		{
			logger.error("Exception in getting job status for space " + spaceID + " type " + jobType + (jobId != null ? "jobId : " + jobId : null), ex );			
			response.formatException(ex);
		}
		return response.toOMElement();	
	}
	
	public OMElement getInitProps()
	{
		logger.debug("Request for getting init props ");		
		SchedulerWebServiceResult response = new SchedulerWebServiceResult();
		try
		{		
			if(!isLoggedIn())
			{
				throw new NotLoggedInException();
			}
						
			SchedulerUserBean schedulerUserBean = getSchedulerUserBean();
			OMFactory factory = OMAbstractFactory.getOMFactory();
			OMNamespace ns = null;			
			OMElement result = factory.createOMElement(JobConstants.INIT_PROPERTIES, ns);
			AxisXmlUtils.addContent(factory, result, JobConstants.INIT_PROP_EMAIL_FROM, schedulerUserBean.getReportEmailProperties().getFrom(), ns);
			AxisXmlUtils.addContent(factory, result, JobConstants.INIT_PROP_EMAIL_SUBJECT, schedulerUserBean.getReportEmailProperties().getSubject(), ns);
			response.setResult(new XMLWebServiceResult(result));			
		}
		catch(Exception ex)
		{
			logger.error("Exception in getting init props ", ex );			
			response.formatException(ex);
		}
		return response.toOMElement();	
	}
	
	public OMElement getAllJobs(String spaceID, String type, String onlyCurrentExecuting, String userID, String filterOnUser)
	{
		type = type != null && type.trim().length() == 0 ? null : type;
		userID = userID != null && userID.trim().length() == 0 ? null : userID;
		logger.debug("Request for getting all Jobs ");
		
		SchedulerWebServiceResult response = new SchedulerWebServiceResult();
		try
		{		
			if(!isLoggedIn())
			{
				throw new NotLoggedInException();
			}
			
			if(!isAuthorized(spaceID))
			{
				throw new UnAuthorizedRequestCredentials();
			}
			
			response = JobAdminUtil.getAllJobs(spaceID, type, onlyCurrentExecuting, userID, filterOnUser);
			
		}
		catch(Exception ex)
		{
			logger.error("Exception in getting all jobs for space " + spaceID + " and user " + userID, ex );			
			response.formatException(ex);
		}
		return response.toOMElement();	
	}
	
	public OMElement getJobHistory(String spaceID, String jobType, String jobId)
	{
		logger.debug("Request for getting job history : jobID : " + jobId);
		SchedulerWebServiceResult response = new SchedulerWebServiceResult();
		Connection conn = null;
		try
		{		
			if(!isLoggedIn())
			{
				throw new NotLoggedInException();
			}
			
			if(!isAuthorized(spaceID))
			{
				throw new UnAuthorizedRequestCredentials();
			}
			
			if(isTypeAllowed(jobType))
			{				
				conn = SchedulerDBConnection.getInstance().getConnectionPool().getConnection();
				String schema = UtilConstants.SCHEDULER_DB_SCHEMA;
				List<JobHistory> listOfJobHistory = SchedulerDatabase.getJobHistory(conn, schema, jobId, null);
				if(listOfJobHistory != null && listOfJobHistory.size() > 0)
				{
					OMFactory factory = OMAbstractFactory.getOMFactory();
					OMNamespace ns = null;			
					OMElement scheduleJob = factory.createOMElement(JobConstants.JOB_HISTORY_JOB_NODE, ns);
					AxisXmlUtils.addContent(factory, scheduleJob, JobConstants.JOB_HISTORY_JOB_ID, jobId, ns);
					OMElement jobHistoryListElement = factory.createOMElement(JobConstants.JOB_HISTORY_LIST, ns);					
					for(JobHistory jobHistory : listOfJobHistory)
					{
						OMElement jobHistoryElement = factory.createOMElement(JobConstants.JOB_HISTORY_NODE, ns);
						AxisXmlUtils.addContent(factory, jobHistoryElement, JobConstants.JOB_HISTORY_SCHEDULE_ID, jobHistory.getScheduleID(), ns);
						AxisXmlUtils.addContent(factory, jobHistoryElement, JobConstants.JOB_HISTORY_START_TIME, jobHistory.getStartTime(), ns);
						AxisXmlUtils.addContent(factory, jobHistoryElement, JobConstants.JOB_HISTORY_END_TIME, jobHistory.getEndTime(), ns);
						AxisXmlUtils.addContent(factory, jobHistoryElement, JobConstants.JOB_HISTORY_STATUS, jobHistory.getStatus(), ns);
						AxisXmlUtils.addContent(factory, jobHistoryElement, JobConstants.JOB_HISTORY_ERR_MSG, jobHistory.getErrorMessage(), ns);
						jobHistoryListElement.addChild(jobHistoryElement);
					}
					
					scheduleJob.addChild(jobHistoryListElement);
					response.setResult(new XMLWebServiceResult(scheduleJob));
				}
			}
			else
			{
				response.errorCode = SchedulerWebServiceResult.ERROR_CODE_INTERNAL;
				response.errorMessage = "Job Type not found";
			}			
		}
		catch(Exception ex)
		{
			logger.error("Exception in getting job history for space " + spaceID + " job " + jobId, ex );			
			response.formatException(ex);
		}
		finally
		{
			if(conn != null)
			{
				try
				{
					conn.close();
				} catch (SQLException e)
				{
					logger.warn("Error while releasing resources", e);
				}
			}
		}
		return response.toOMElement();	
	}	
	
	
	public OMElement addScheduleToJob(String spaceID, String type, String jobID, String jobOptionsXmlString)
	{
		logger.debug("Request for adding schedule to job : jobID : " + jobID);
		SchedulerWebServiceResult result = new SchedulerWebServiceResult();
		try
		{
			if(!isLoggedIn())
			{
				throw new NotLoggedInException();
			}
			
			if(!isAuthorized(spaceID))
			{
				throw new UnAuthorizedRequestCredentials();
			}
			
			result = JobAdminUtil.addScheduleToJob(spaceID, type, jobID, jobOptionsXmlString);
			
		}catch(Exception ex)
		{
			logger.error("Exception in adding schedule for space " + spaceID + " : report " + jobID, ex);
			result.formatException(ex);	
		}
		return result.toOMElement();
	}
	
	public OMElement removeScheduleFromJob(String spaceID, String type, String jobID, String scheduleID)
	{
		logger.debug("Request for removing schedule from job : jobID : " + jobID);
		SchedulerWebServiceResult result = new SchedulerWebServiceResult();
		try
		{
			if(!isLoggedIn())
			{
				throw new NotLoggedInException();
			}
			
			if(!isAuthorized(spaceID))
			{
				throw new UnAuthorizedRequestCredentials();
			}
			
			result = JobAdminUtil.removeScheduleFromJob(spaceID, type, jobID, scheduleID);
			
		}catch(Exception ex)
		{
			logger.error("Exception in removing schedule " + scheduleID + " for report " + jobID, ex);
			result.formatException(ex);
		}
		
		return result.toOMElement();
	}
	
	public OMElement runNow(String spaceID, String type, String typeID)
	{
		logger.debug("Request for run Now job : jobID : " + typeID);
		SchedulerWebServiceResult result = new SchedulerWebServiceResult();
		try
		{
			if(!isLoggedIn())
			{
				throw new NotLoggedInException();
			}
			
			if(!isAuthorized(spaceID))
			{
				throw new UnAuthorizedRequestCredentials();
			}
			
			result = JobAdminUtil.runNow(spaceID, type, typeID);
			
		
		}
		catch(Exception ex)
		{
			logger.error("Exception in running immediately for " + type + " " + typeID , ex);
			result.formatException(ex);			
		}
		return result.toOMElement();
	}
	
	public OMElement updateJobsRunStatus(String spaceID, String jobsRunStatusXmlString)
	{
		logger.debug("Request for updating run status ");
		SchedulerWebServiceResult result = new SchedulerWebServiceResult();
		try
		{
			if(!isLoggedIn())
			{
				throw new NotLoggedInException();
			}
			
			if(!isAuthorized(spaceID))
			{
				throw new UnAuthorizedRequestCredentials();
			}
			
			result = JobAdminUtil.updateJobsRunStatus(spaceID, jobsRunStatusXmlString);
			
			
		}
		catch(Exception ex)
		{
			logger.error("Exception in updating jobs status for space : " + spaceID , ex);
			result.formatException(ex);			
		}
		return result.toOMElement();
	}
	
	/**
	 * A single method to stop various kinds of job. Type could mean
	 * ScheduledReport, Processing, Extraction
	 * @param spaceID
	 * @param type
	 * @param typeID
	 * @return
	 */
	public SchedulerWebServiceResult pauseJob(String spaceID, String type, String typeID)
	{
		logger.debug("Request for pausing job : jobID : " + typeID);
		SchedulerWebServiceResult result = new SchedulerWebServiceResult();
		try
		{
			if(!isLoggedIn())
			{
				throw new NotLoggedInException();
			}
			
			if(!isAuthorized(spaceID))
			{
				throw new UnAuthorizedRequestCredentials();
			}
			
			result = JobAdminUtil.pauseJob(spaceID, type, typeID);
			
		}
		catch(Exception ex)
		{
			logger.error("Exception in pausing " + type + " " + typeID , ex);
			result.formatException(ex);			
		}
		return result;
	}
	
	/**
	 * A single method to resume various kinds of job. Type could mean
	 * ScheduledReport, Processing, Extraction
	 * @param spaceID
	 * @param type
	 * @param typeID
	 * @return
	 */
	public SchedulerWebServiceResult resumeJob(String spaceID, String type, String typeID)
	{
		logger.debug("Request for resuming job : jobID " + typeID);
		SchedulerWebServiceResult result = new SchedulerWebServiceResult();
		try
		{
			if(!isLoggedIn())
			{
				throw new NotLoggedInException();
			}
			
			if(!isAuthorized(spaceID))
			{
				throw new UnAuthorizedRequestCredentials();
			}
			
			result = JobAdminUtil.resumeJob(spaceID, type, typeID);
			
		}
		catch(Exception ex)
		{
			logger.error("Exception in resuming " + type + " " + typeID , ex);
			result.formatException(ex);			
		}
		return result;
	}
	
	/**
	 * Only available
	 */
	public OMElement killJob(String spaceID, String jobsRunStatusXmlString)
	{
		logger.debug("Request for killing run status " + spaceID);
		SchedulerWebServiceResult result = new SchedulerWebServiceResult();
		try
		{
			if(!isLoggedIn())
			{
				throw new NotLoggedInException();
			}
			
			if(!isAuthorized(spaceID))
			{
				throw new UnAuthorizedRequestCredentials();
			}
			
			result = JobAdminUtil.killJob(spaceID, null, jobsRunStatusXmlString);
			
			
		}
		catch(Exception ex)
		{
			logger.error("Exception in updating jobs status for space : " + spaceID , ex);
			result.formatException(ex);			
		}
		return result.toOMElement();
	}
	
	private static boolean isTypeAllowed(String jobType)
	{		
		if(jobType != null && 
				(jobType.equalsIgnoreCase(UtilConstants.JobType.REPORT.toString())
				|| jobType.equalsIgnoreCase(UtilConstants.JobType.SFDCEXTRACTION.toString()) 
				|| jobType.equalsIgnoreCase(UtilConstants.JobType.PROCESSING.toString())
				|| jobType.equalsIgnoreCase(UtilConstants.JobType.DELETELOAD.toString())
				|| jobType.equalsIgnoreCase(UtilConstants.JobType.BDS.toString())
				|| jobType.equalsIgnoreCase(UtilConstants.JobType.NETSUITE.toString())
				|| jobType.equalsIgnoreCase(UtilConstants.JobType.ALL.toString())
				|| jobType.equalsIgnoreCase(UtilConstants.Type.bds.toString())
				|| jobType.equalsIgnoreCase(UtilConstants.Type.sfdc.toString())
				|| jobType.equalsIgnoreCase(UtilConstants.Type.netsuite.toString())
			))
		{
			return true;
		}
		return false;
	}
}
