package com.birst.scheduler.webServices.admin;


import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
import org.apache.log4j.Logger;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;

import com.birst.scheduler.SchedulerServerContext;
import com.birst.scheduler.bds.BDSJobOperation;
import com.birst.scheduler.exceptions.SchedulerBaseException;
import com.birst.scheduler.exceptions.TypeNotFoundException;
import com.birst.scheduler.salesforce.SFDCJobOperation;
import com.birst.scheduler.shared.BaseJobObject;
import com.birst.scheduler.shared.CopyJob;
import com.birst.scheduler.shared.OperationFactory;
import com.birst.scheduler.utils.AxisXmlUtils;
import com.birst.scheduler.utils.JobConstants;
import com.birst.scheduler.utils.Util;
import com.birst.scheduler.utils.UtilConstants;
import com.birst.scheduler.webServices.XMLWebServiceResult;
import com.birst.scheduler.webServices.response.SchedulerWebServiceResult;

public class JobAdminUtil
{

	private static Logger logger = Logger.getLogger(JobAdminUtil.class);
	public static SchedulerWebServiceResult addJob(String spaceID, String userID, String jobType, String jobOptionsXmlString) throws Exception
	{
		logger.debug("Request for adding job");
		SchedulerWebServiceResult response = new SchedulerWebServiceResult();
		if (!SchedulerServerContext.getUseNewConnectorFramework())
		{
			jobType = getJobTypeForOldFramework(jobType);
		}
		validateJobType(jobType);
		OperationFactory.getOperationType(jobType).addJob(response, spaceID, userID, jobOptionsXmlString);		
		return response;
	}


	public static SchedulerWebServiceResult getJobDetails(String spaceID, String jobType, String jobID) throws Exception
	{
		logger.debug("Request for getting job details : jobID : " + jobID );		
		SchedulerWebServiceResult response = new SchedulerWebServiceResult();
		if (!SchedulerServerContext.getUseNewConnectorFramework())
		{
			jobType = getJobTypeForOldFramework(jobType);
		}
		validateJobType(jobType);
		OperationFactory.getOperationType(jobType).getJobDetails(response, spaceID, jobID);
		return response;
	}

	public static SchedulerWebServiceResult updateJob(String spaceID, String userID, String jobType, String jobID, String jobOptionsXmlString) throws Exception
	{
		logger.debug("Request for updating job : jobID : " + jobID);
		SchedulerWebServiceResult response = new SchedulerWebServiceResult();
		if (!SchedulerServerContext.getUseNewConnectorFramework())
		{
			jobType = getJobTypeForOldFramework(jobType);
		}
		validateJobType(jobType);
		OperationFactory.getOperationType(jobType).updateJob(response, spaceID, userID, jobID, jobOptionsXmlString);		
		return response;
	}


	public static SchedulerWebServiceResult removeJob(String spaceID, String jobType, String jobID) throws Exception
	{		
		logger.debug("Request for removing job : jobID : " + jobID);
		SchedulerWebServiceResult response = new SchedulerWebServiceResult();
		validateJobType(jobType);
		OperationFactory.getOperationType(jobType).removeJob(response, spaceID, jobID);
		return response;	
	}


	@SuppressWarnings("unchecked")
	public static SchedulerWebServiceResult getJobStatus(String spaceID, String jobType, String jobOptionsXmlString) throws Exception
	{
		logger.debug("Request for getting job status : space : " + spaceID + " : type : " + jobType);
		SchedulerWebServiceResult response = new SchedulerWebServiceResult();
		String jobId = null;

		validateJobType(jobType);
		OperationFactory.getOperationType(jobType).getJobStatus(response, spaceID, jobOptionsXmlString);
		return response;
	}

	public static SchedulerWebServiceResult getAllJobs(String spaceID, String type, String onlyCurrentExecuting, String userID, String filterOnUser) throws Exception
	{
		type = type != null && type.trim().length() == 0 ? null : type;
		userID = userID != null && userID.trim().length() == 0 ? null : userID;
		logger.debug("Request for getting all Jobs ");
		validateJobType(type);
		SchedulerWebServiceResult response = new SchedulerWebServiceResult();
		if (type.equalsIgnoreCase(UtilConstants.JobType.ALL.toString())) {
			// Get BDS and SFDC job xml.
			OMFactory factory = OMAbstractFactory.getOMFactory();
			OMNamespace ns = null;
			OMElement scheduledJobs = factory.createOMElement(JobConstants.JOBS_LIST_NODE, ns);
			AxisXmlUtils.addContent(factory, scheduledJobs, JobConstants.PRIMARY_SCHEDULER_NAME, SchedulerServerContext.getPrimarySchedulerDisplayName(), ns);
			SFDCJobOperation.fillAllJobs(spaceID, onlyCurrentExecuting, userID, filterOnUser, factory, ns, scheduledJobs);
			BDSJobOperation.fillAllJobs(spaceID, onlyCurrentExecuting, userID, filterOnUser, factory, ns, scheduledJobs);
			response.setResult(new XMLWebServiceResult(scheduledJobs));
		} else {
			OperationFactory.getOperationType(type).getAllJobs(response, spaceID, onlyCurrentExecuting, userID, filterOnUser);
		}
		return response;	
	}

	public static SchedulerWebServiceResult addScheduleToJob(String spaceID, String type, String jobID, String jobOptionsXmlString) throws Exception
	{
		logger.debug("Request for adding schedule to job : jobID : " + jobID);
		SchedulerWebServiceResult result = new SchedulerWebServiceResult();
		validateJobType(type);
		OperationFactory.getOperationType(type).addScheduleToJob(result, spaceID, jobID, jobOptionsXmlString);		
		return result;
	}

	public static SchedulerWebServiceResult removeScheduleFromJob(String spaceID, String type, String jobID, String scheduleID) throws Exception
	{
		logger.debug("Request for removing schedule from job : jobID : " + jobID);
		validateJobType(type);
		SchedulerWebServiceResult result = new SchedulerWebServiceResult();
		validateJobType(type);
		OperationFactory.getOperationType(type).removeScheduleFromJob(result, spaceID, jobID, scheduleID);		
		return result;
	}

	public static SchedulerWebServiceResult runNow(String spaceID, String type, String typeID) throws Exception
	{
		logger.debug("Request for run Now job : jobID : " + typeID);
		SchedulerWebServiceResult result = new SchedulerWebServiceResult();
		validateJobType(type);
		OperationFactory.getOperationType(type).runNow(result, spaceID, typeID);
		return result;
	}

	public static SchedulerWebServiceResult updateJobsRunStatus(String spaceID, String jobsRunStatusXmlString) throws Exception
	{
		logger.debug("Request for updating run status ");
		SchedulerWebServiceResult result = new SchedulerWebServiceResult();

		List<BaseJobObject> baseJobObjectList = Util.parseRunStatusesXmlString(jobsRunStatusXmlString);
		for(BaseJobObject object : baseJobObjectList)
		{
			String jobId = object.getJobId();
			String type = object.getType();
			boolean enable = object.isEnabled();
			validateJobType(type);				
			if(enable){
				OperationFactory.getOperationType(type).resumeJob(result, spaceID, jobId);				
			}
			else
			{
				OperationFactory.getOperationType(type).pauseJob(result, spaceID, jobId);				
			}
		}

		return result;
	}

	/**
	 * A single method to stop various kinds of job. Type could mean
	 * ScheduledReport, Processing, Extraction
	 * @param spaceID
	 * @param type
	 * @param typeID
	 * @return
	 * @throws ClassNotFoundException 
	 * @throws ParseException 
	 * @throws SQLException 
	 * @throws SchedulerBaseException 
	 * @throws SchedulerException 
	 */
	public static SchedulerWebServiceResult pauseJob(String spaceID, String type, String typeID) throws Exception
	{
		logger.debug("Request for pausing job : jobID : " + typeID);
		SchedulerWebServiceResult result = new SchedulerWebServiceResult();
		validateJobType(type);
		OperationFactory.getOperationType(type).pauseJob(result, spaceID, typeID);
		return result;
	}

	/**
	 * A single method to resume various kinds of job. Type could mean
	 * ScheduledReport, Processing, Extraction
	 * @param spaceID
	 * @param type
	 * @param typeID
	 * @return
	 * @throws ClassNotFoundException 
	 * @throws ParseException 
	 * @throws SQLException 
	 * @throws SchedulerBaseException 
	 * @throws SchedulerException 
	 */
	public static SchedulerWebServiceResult resumeJob(String spaceID, String type, String typeID) throws Exception
	{
		logger.debug("Request for resuming job : jobID " + typeID);
		SchedulerWebServiceResult result = new SchedulerWebServiceResult();
		validateJobType(type);
		OperationFactory.getOperationType(type).resumeJob(result, spaceID, typeID);		
		return result;
	}

	/**
	 * Only available
	 * @throws SchedulerException 
	 * @throws TypeNotFoundException 
	 */
	public static SchedulerWebServiceResult killJob(String spaceID, String type, String jobsRunStatusXmlString) throws Exception
	{
		logger.debug("Request for killing run status " + spaceID);
		SchedulerWebServiceResult result = new SchedulerWebServiceResult();
		validateJobType(type);
		OperationFactory.getOperationType(type).killJob(result, spaceID, jobsRunStatusXmlString);
		
		return result;
	}

	private static boolean isTypeAllowed(String jobType)
	{		
		if(jobType != null && 
				(jobType.equalsIgnoreCase(UtilConstants.JobType.REPORT.toString())
						|| jobType.equalsIgnoreCase(UtilConstants.JobType.SFDCEXTRACTION.toString())
						|| jobType.equalsIgnoreCase(UtilConstants.JobType.NETSUITE.toString())
						|| jobType.equalsIgnoreCase(UtilConstants.JobType.PROCESSING.toString())
						|| jobType.equalsIgnoreCase(UtilConstants.JobType.DELETELOAD.toString())
						|| jobType.equalsIgnoreCase(UtilConstants.JobType.BDS.toString())
						|| jobType.equalsIgnoreCase(UtilConstants.JobType.ALL.toString())
						|| jobType.equalsIgnoreCase(UtilConstants.Type.bds.toString())
						|| jobType.equalsIgnoreCase(UtilConstants.Type.sfdc.toString())
						|| jobType.equalsIgnoreCase(UtilConstants.Type.netsuite.toString())
				)
			)
		{
			return true;
		}
		else if (jobType != null && jobType.endsWith(UtilConstants.CONNECTOR_SCHEDULE_TYPE_SUFFIX))//new framework connectors
		{
			return true;
		}
		return false;
	}
	
	public static String getJobTypeForOldFramework(String jobType)
	{
		if (jobType.endsWith(UtilConstants.CONNECTOR_SCHEDULE_TYPE_SUFFIX))
		{
			jobType = jobType.substring(0, jobType.indexOf(UtilConstants.CONNECTOR_SCHEDULE_TYPE_SUFFIX));
		}
		else if (jobType.equals("2"))
		{
			jobType = UtilConstants.Type.sfdc.toString();
		}
		else if (jobType.equals("3"))
		{
			jobType = UtilConstants.Type.netsuite.toString();
		}
		return jobType;
	}

	private static void validateJobType(String jobType) throws TypeNotFoundException
	{
		if(!isTypeAllowed(jobType))
		{
			throw new TypeNotFoundException();
		}
	}


	public static SchedulerWebServiceResult copyJob(String spaceID,
			String userID, String type, String copyOptionsXmlString,
			String jobOptionsXmlString) throws Exception
	{
		SchedulerWebServiceResult result = new SchedulerWebServiceResult();
		if (!SchedulerServerContext.getUseNewConnectorFramework())
		{
			type = getJobTypeForOldFramework(type);
		}
		validateJobType(type);
		CopyJob copyJob = Util.parseCopyJobXmlString(spaceID, userID, copyOptionsXmlString);
		copyJob.setJobOptionsXmlString(jobOptionsXmlString);
		String fromScheduler = copyJob.getFromScheduler().getSchedulerName();
		String currentScheduler = SchedulerServerContext.getSchedulerInstance().getSchedulerName();		
		copyJob.performCopyBetweenSchedulers(result, copyJob.getHasMultipleJobIDs(), fromScheduler.equals(currentScheduler));
		return result;
	}
}
