package com.birst.scheduler.webServices.admin;

import org.apache.axiom.om.OMElement;
import org.apache.log4j.Logger;
import com.birst.scheduler.exceptions.InvalidTrustedIPRequestException;
import com.birst.scheduler.exceptions.NotLoggedInException;
import com.birst.scheduler.exceptions.UnAuthorizedRequestCredentials;
import com.birst.scheduler.webServices.SchedulerWebService;
import com.birst.scheduler.webServices.response.SchedulerWebServiceResult;

public class JobAdminTrustedService extends SchedulerWebService
{
	// The purpose of this wrapper class is so that Acorn can consume this webservice
	// and does not need to do a proper Client or a polling mechanism as compared to the flex client
	
	private static Logger logger = Logger.getLogger(JobAdminTrustedService.class);
	public OMElement getAllJobsTrustedService(String spaceID, String type, String onlyCurrentExecuting, String userID, String filterOnUser)
	{
		SchedulerWebServiceResult response = new SchedulerWebServiceResult();
		try
		{
			if(!isTrustedInternalRequest())
			{
				throw new InvalidTrustedIPRequestException();
			}
			response = JobAdminUtil.getAllJobs(spaceID, type, onlyCurrentExecuting, userID, filterOnUser);
		}
		catch(Exception ex)
		{
			logger.error("Exception in getting all jobs for space " + spaceID, ex);			
			response.formatException(ex);
		}
		return response.toOMElement();
	}

	public OMElement addJobTrustedService(String spaceID, String userID, String jobType, String jobOptionsXmlString)
	{
		SchedulerWebServiceResult response = new SchedulerWebServiceResult();
		try
		{
			if(!isTrustedInternalRequest())
			{
				throw new InvalidTrustedIPRequestException();
			}
			response = JobAdminUtil.addJob(spaceID, userID, jobType, jobOptionsXmlString);
		}
		catch(Exception ex)
		{
			logger.error("Exception in adding job for space " + spaceID, ex);			
			response.formatException(ex);
		}
		return response.toOMElement();
	}
	
	public OMElement updateJobTrustedService(String spaceID, String userID, String jobType, 
			String jobID, String jobOptionsXmlString)
	{
		SchedulerWebServiceResult response = new SchedulerWebServiceResult();
		try
		{
			if(!isTrustedInternalRequest())
			{
				throw new InvalidTrustedIPRequestException();
			}
			response = JobAdminUtil.updateJob(spaceID, userID, jobType, jobID, jobOptionsXmlString);
		}
		catch(Exception ex)
		{
			logger.error("Exception in updating job for space " + spaceID, ex);			
			response.formatException(ex);
		}
		return response.toOMElement();
	}
	
	public OMElement removeJobTrustedService(String spaceID, String jobType, String jobID)
	{
		SchedulerWebServiceResult response = new SchedulerWebServiceResult();
		try
		{
			if(!isTrustedInternalRequest())
			{
				throw new InvalidTrustedIPRequestException();
			}
			response = JobAdminUtil.removeJob(spaceID, jobType, jobID);
		}
		catch(Exception ex)
		{
			logger.error("Exception in removing job from space " + spaceID, ex);			
			response.formatException(ex);
		}
		return response.toOMElement();
	}
	
	public OMElement copyJob(String spaceID, String userID, String jobType, String copyOptionsXmlString, String jobOptionsXmlString)
	{
		logger.debug("Request for migrating job : spaceID : " + spaceID);
		SchedulerWebServiceResult response = new SchedulerWebServiceResult();
		try
		{	
			if(!isTrustedInternalRequest())
			{
				throw new InvalidTrustedIPRequestException();
			}			
			response = JobAdminUtil.copyJob(spaceID, userID, jobType, copyOptionsXmlString, jobOptionsXmlString);			
		}
		catch(Exception ex)
		{
			logger.error("Exception in copying job details for space " + spaceID, ex );			
			response.formatException(ex);
		}
		return response.toOMElement();		
	}
	
	public OMElement getJobDetailsTrustedService(String spaceID, String jobType, String jobID)
	{
		SchedulerWebServiceResult response = new SchedulerWebServiceResult();
		try
		{
			if(!isTrustedInternalRequest())
			{
				throw new InvalidTrustedIPRequestException();
			}
			response = JobAdminUtil.getJobDetails(spaceID, jobType, jobID);
		}
		catch(Exception ex)
		{
			logger.error("Exception in getting job details for space " + spaceID, ex);			
			response.formatException(ex);
		}
		return response.toOMElement();
	}
	
	public OMElement getJobStatusTrustedService(String spaceID, String jobType, String jobOptionsXmlString)
	{
		SchedulerWebServiceResult response = new SchedulerWebServiceResult();
		try
		{
			if(!isTrustedInternalRequest())
			{
				throw new InvalidTrustedIPRequestException();
			}
			response = JobAdminUtil.getJobStatus(spaceID, jobType, jobOptionsXmlString);
		}
		catch(Exception ex)
		{
			logger.error("Exception in getting job status for space " + spaceID, ex);			
			response.formatException(ex);
		}
		return response.toOMElement();
	}
	
	public OMElement removeScheduleFromJobTrustedService(String spaceID, String type, String jobID, String scheduleID)
	{
		SchedulerWebServiceResult response = new SchedulerWebServiceResult();
		try
		{
			if(isTrustedInternalRequest())
			{
				throw new InvalidTrustedIPRequestException();
			}
			response = JobAdminUtil.removeScheduleFromJob(spaceID, type, jobID, scheduleID);
		}
		catch(Exception ex)
		{
			logger.error("Exception in removing job schedule for  space " + spaceID, ex);			
			response.formatException(ex);
		}
		return response.toOMElement();
	}
	
	public OMElement addScheduleToJobTrustedService(String spaceID, String type, String jobID, String jobOptionsXmlString)
	{
		SchedulerWebServiceResult response = new SchedulerWebServiceResult();
		try
		{
			if(!isTrustedInternalRequest())
			{
				throw new InvalidTrustedIPRequestException();
			}
			response = JobAdminUtil.addScheduleToJob(spaceID, type, jobID, jobOptionsXmlString);
		}
		catch(Exception ex)
		{
			logger.error("Exception in adding job schedule for space " + spaceID, ex);			
			response.formatException(ex);
		}
		return response.toOMElement();
	}
	
	public OMElement runNowTrusedService(String spaceID, String type, String typeID)
	{
		SchedulerWebServiceResult response = new SchedulerWebServiceResult();
		try
		{
			if(!isTrustedInternalRequest())
			{
				throw new InvalidTrustedIPRequestException();
			}
			response = JobAdminUtil.runNow(spaceID, type, typeID);
		}
		catch(Exception ex)
		{
			logger.error("Exception in run now for space " + spaceID, ex);			
			response.formatException(ex);
		}
		return response.toOMElement();
	}
	
	public OMElement pauseJobTrustedService(String spaceID, String type, String typeID)
	{
		SchedulerWebServiceResult response = new SchedulerWebServiceResult();
		try
		{
			if(!isTrustedInternalRequest())
			{
				throw new InvalidTrustedIPRequestException();
			}
			response = JobAdminUtil.pauseJob(spaceID, type, typeID);
		}
		catch(Exception ex)
		{
			logger.error("Exception in pausing job for space " + spaceID, ex);			
			response.formatException(ex);
		}
		return response.toOMElement();
	}
	
	public OMElement resumeJobTrustedService(String spaceID, String type, String typeID)
	{
		SchedulerWebServiceResult response = new SchedulerWebServiceResult();
		try
		{
			if(!isTrustedInternalRequest())
			{
				throw new InvalidTrustedIPRequestException();
			}
			response = JobAdminUtil.resumeJob(spaceID, type, typeID);
		}
		catch(Exception ex)
		{
			logger.error("Exception in resuming job  for space " + spaceID, ex);			
			response.formatException(ex);
		}
		return response.toOMElement();
	}
	
	public OMElement updateJobsRunStatusTrusedService(String spaceID, String jobsRunStatusXmlString)
	{
		SchedulerWebServiceResult response = new SchedulerWebServiceResult();
		try
		{
			if(!isTrustedInternalRequest())
			{
				throw new InvalidTrustedIPRequestException();
			}
			response = JobAdminUtil.updateJobsRunStatus(spaceID, jobsRunStatusXmlString);
		}
		catch(Exception ex)
		{
			logger.error("Exception in updating job for space " + spaceID, ex);			
			response.formatException(ex);
		}
		return response.toOMElement();
	}
	
	public OMElement killJob(String spaceID, String type, String jobsRunStatusXmlString)
	{
		logger.debug("Request for killing run status : " + spaceID);
		SchedulerWebServiceResult result = new SchedulerWebServiceResult();
		try
		{
			if(!isTrustedInternalRequest())
			{
				throw new InvalidTrustedIPRequestException();
			}
			
			result = JobAdminUtil.killJob(spaceID, type, jobsRunStatusXmlString);
		}
		catch(Exception ex)
		{
			logger.error("Exception in updating jobs status for space : " + spaceID , ex);
			result.formatException(ex);			
		}
		return result.toOMElement();
	}
}
