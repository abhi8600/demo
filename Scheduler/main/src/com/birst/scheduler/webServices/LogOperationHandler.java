/**
 * $Id: LogOperationHandler.java,v 1.1 2011-04-19 02:11:33 gsingh Exp $
 *
 * Copyright (C) 2009 Birst, Inc. All rights reserved.
 * BIRST PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.birst.scheduler.webServices;

import org.apache.axis2.AxisFault;
import org.apache.axis2.context.MessageContext;
import org.apache.axis2.handlers.AbstractHandler;
import org.apache.log4j.Logger;

public class LogOperationHandler extends AbstractHandler
{
	private static Logger logger = Logger.getLogger(LogOperationHandler.class);

	public LogOperationHandler()
	{
	}

	public InvocationResponse invoke(MessageContext msgContext) throws AxisFault
	{
		if (logger.isTraceEnabled())
		{
			logger.trace("service: " + msgContext.getAxisService()
					+ ", operation: " + msgContext.getAxisOperation().getName());
		}
		return InvocationResponse.CONTINUE;
	}
}