/**
 * 
 */
package com.birst.scheduler.webServices;

import java.io.StringReader;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamReader;

import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
import org.apache.axiom.om.OMText;
import org.apache.axiom.om.impl.builder.StAXOMBuilder;
import org.apache.log4j.Logger;


public class StringWebServiceResult implements IWebServiceResult {
	private static Logger logger = Logger.getLogger(StringWebServiceResult.class);

	private String str;
	
	public StringWebServiceResult(String string) {
		str = string;
	}
	@Override
	public void addContent(OMElement parent, OMFactory factory, OMNamespace ns) {
		try {
			XMLStreamReader parser = XMLInputFactory.newInstance().createXMLStreamReader(new StringReader(str));
			StAXOMBuilder builder = new StAXOMBuilder(parser);
			OMElement doc = null;
			doc = builder.getDocumentElement();
			parent.addChild(doc);
			return;
		}
		catch (Exception e) {
			logger.warn(e, e);
		}
		
		OMText text = factory.createOMText(str);
		parent.addChild(text);
	}

}
