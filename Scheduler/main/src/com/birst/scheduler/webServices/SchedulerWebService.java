package com.birst.scheduler.webServices;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.axis2.context.MessageContext;
import org.apache.axis2.transport.http.HTTPConstants;

import com.birst.scheduler.shared.SchedulerUserBean;
import com.birst.scheduler.utils.Util;
import com.birst.scheduler.utils.UtilConstants;

public abstract class SchedulerWebService
{
	/**
	 * Is this user logged into the Scheduler system.
	 * @return	true if the user is logged in, false otherwise.
	 */
	public boolean isLoggedIn() {
        return getSession() != null;
    }
		
	public boolean inOperationsMode()
	{
		boolean operationsMode = false;
		SchedulerUserBean schedulerUserBean = getSchedulerUserBean();
		if(schedulerUserBean != null)
		{
			operationsMode  = schedulerUserBean.isOperationsMode();
		}
		return operationsMode;
	}
	
	public boolean isAuthorized(String spaceID)
	{
		boolean isAuthorized = false;
		SchedulerUserBean schedulerUserBean = getSchedulerUserBean();
		if(schedulerUserBean != null)
		{
			if(inOperationsMode())
			{
				isAuthorized = true;
			}
			else
			{
				isAuthorized = spaceID.equalsIgnoreCase(schedulerUserBean.getSpaceID());
			}
		}
		
		return isAuthorized;
	}
	
	public boolean isTrustedInternalRequest()
	{
		HttpServletRequest request = (HttpServletRequest) getMessageContext().getProperty(HTTPConstants.MC_HTTP_SERVLETREQUEST);
		String requestIP = request.getRemoteAddr();
		String errorStr = Util.validateTrustedInternalRequest(requestIP);
		if(errorStr != null && errorStr.length() > 0)
		{
			return false;
		}
		
		return true;
	}
	
	protected static MessageContext getMessageContext() {
		return MessageContext.getCurrentMessageContext();
	}
	/**
	 * Gets the Session object for this web service request
	 * @return	the Session; null if there is no session because a user is not logged in.
	 */
	protected static HttpSession getSession() {        
		HttpServletRequest request = getHttpServletRequest();
		if (request != null) {
			return request.getSession(false);
		}

		return null;
	}
	
	public static HttpServletRequest getHttpServletRequest() {
		MessageContext context = getMessageContext();
		if (context != null) {
			return (HttpServletRequest) context.getProperty(HTTPConstants.MC_HTTP_SERVLETREQUEST);
		}
		return null;
	}
	
	public static SchedulerUserBean getSchedulerUserBean()
	{
		return (SchedulerUserBean) getSessionObject(UtilConstants.SESSION_USER_BEAN);		 
	}
	
	public static Object getSessionObject(String name) {        
		HttpSession session = getSession();
		if (session != null) {
			return session.getAttribute(name);
		}
		return null;
	}
	
	public static void setSessionObject(String name, Object o) {
		HttpSession session = getSession();
		if (session != null) {
			session.setAttribute(name, o);
		}
	}
	
	
}
