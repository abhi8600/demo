package com.birst.scheduler.webServices.response;

import org.apache.log4j.Logger;

import com.birst.scheduler.Report.ScheduledReport;

public class ScheduledReportListResult 
{	 
	private static final Logger logger = Logger.getLogger(ScheduledReportListResult.class);
	public ScheduledReport[] ReportList;
	public SchedulerWebServiceResult WebServiceResult;
	
	public SchedulerWebServiceResult getWebServiceResult()
	{
		return WebServiceResult;
	}
	public void setWebServiceResult(SchedulerWebServiceResult webServiceResult)
	{
		WebServiceResult = webServiceResult;
	}
	public ScheduledReport[] getReportList()
	{
		return ReportList;
	}
	public void setReportList(ScheduledReport[] reportList)
	{
		ReportList = reportList;
	}
	
	
}
