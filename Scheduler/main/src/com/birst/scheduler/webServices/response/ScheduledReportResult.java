package com.birst.scheduler.webServices.response;

import org.apache.log4j.Logger;

import com.birst.scheduler.Report.ScheduledReport;

public class ScheduledReportResult
{	
	public ScheduledReport Report;
	public SchedulerWebServiceResult WebServiceResult;
	
	public ScheduledReport getReport()
	{
		return Report;
	}
	public void setReport(ScheduledReport report)
	{
		Report = report;
	}
	public SchedulerWebServiceResult getWebServiceResult()
	{
		return WebServiceResult;
	}
	public void setWebServiceResult(SchedulerWebServiceResult webServiceResult)
	{
		WebServiceResult = webServiceResult;
	}

}
