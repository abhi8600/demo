package com.birst.scheduler.webServices.response;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
import com.birst.scheduler.exceptions.SchedulerBaseException;
import com.birst.scheduler.utils.AxisXmlUtils;
import com.birst.scheduler.webServices.IWebServiceResult;

public class SchedulerWebServiceResult
{	


	public String errorMessage = "";
	public int errorCode = 0; // success by default
	public final static String ERR_CODE = "ErrorCode";
	public final static String ERR_MSG = "ErrorMessage";
	public final static String RESULT = "Result";
		
	private IWebServiceResult result;
	
	public static final String ERROR_INTERNAL_MSG = "Internal Error occured";
	public static final int ERROR_CODE_INTERNAL = -500;
	
	public void formatException(Exception ex)
	{
		if(ex instanceof SchedulerBaseException)
		{
			SchedulerBaseException sEx = (SchedulerBaseException)ex;
			errorCode = sEx.getErrorCode();
			errorMessage = sEx.getErrorMessage();			
		}
		else
		{
			errorCode = ERROR_CODE_INTERNAL;
			errorMessage = ERROR_INTERNAL_MSG;
		}
	}
	
	public String getErrorMessage()
	{
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage)
	{
		this.errorMessage = errorMessage;
	}

	public int getErrorCode()
	{
		return errorCode;
	}

	public void setErrorCode(int errorCode)
	{
		this.errorCode = errorCode;
	}

	public IWebServiceResult getResult() {
		return result;
	}
	
	/**
	 * Sets the result of the web service call
	 * @param result	the new result of the web service call.
	 */
	public void setResult(IWebServiceResult result) {
		this.result = result;
	}
	
	/**
	 * Convert the result object into it's OMElement representation
	 * @return
	 */
	public OMElement toOMElement() {
		OMFactory fac = OMAbstractFactory.getOMFactory();
		OMNamespace ns = fac.createOMNamespace("", "");
		OMElement ret = fac.createOMElement(this.getClass().getName(), ns);
		AxisXmlUtils.addContent(fac, ret, ERR_CODE, this.errorCode, ns);
		AxisXmlUtils.addContent(fac, ret, ERR_MSG, this.errorMessage, ns);
		
		if (this.result != null) {
			OMElement result = fac.createOMElement(RESULT, ns);
			this.result.addContent(result, fac, ns);
			ret.addChild(result);
		}
		return ret;
	}
}
