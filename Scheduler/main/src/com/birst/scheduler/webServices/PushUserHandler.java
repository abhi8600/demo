/**
 * $Id: PushUserHandler.java,v 1.2 2011-07-26 00:19:36 gsingh Exp $
 *
 * Copyright (C) 2009 Birst, Inc. All rights reserved.
 * BIRST PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.birst.scheduler.webServices;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.axis2.AxisFault;
import org.apache.axis2.context.MessageContext;
import org.apache.axis2.handlers.AbstractHandler;
import org.apache.axis2.transport.http.HTTPConstants;
import org.apache.log4j.MDC;

import com.birst.scheduler.shared.SchedulerUserBean;
import com.birst.scheduler.utils.Util;
import com.birst.scheduler.utils.UtilConstants;

public class PushUserHandler extends AbstractHandler
{
	public PushUserHandler()
	{
	}

	public InvocationResponse invoke(MessageContext msgContext) throws AxisFault
	{	
		Util.clearOutMDC();
		HttpSession session = getSession(msgContext);
		if(session != null)
		{	
			MDC.put("sessionID", session.getId());
			Object obj = session.getAttribute(UtilConstants.SESSION_USER_BEAN);
			if(obj != null && obj instanceof SchedulerUserBean )
			{
				SchedulerUserBean schedulerUserBean = (SchedulerUserBean) obj;
				Util.setMDC(schedulerUserBean.getUserID(), schedulerUserBean.getSpaceID());
			}
		}
		return InvocationResponse.CONTINUE;
	}

	private HttpSession getSession(MessageContext msgContext)
	{
		if(msgContext == null)
		{
			return null;
		}
		
		HttpServletRequest request = (HttpServletRequest) msgContext.getProperty(HTTPConstants.MC_HTTP_SERVLETREQUEST);
		if(request == null)
		{
			return null;
		}
		
		HttpSession session = request.getSession(false);
		if(session == null)
		{
			return null;
		}
		
		return session;
	}	
}