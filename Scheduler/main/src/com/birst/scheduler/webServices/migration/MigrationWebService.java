package com.birst.scheduler.webServices.migration;

import java.util.List;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
import org.apache.log4j.Logger;
import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.Trigger;

import com.birst.scheduler.SchedulerServerContext;
import com.birst.scheduler.Migration.MigrationOperation;
import com.birst.scheduler.Migration.MigrationUtils;
import com.birst.scheduler.Migration.MigrationUtils.MigrationType;
import com.birst.scheduler.Report.ScheduledReport;
import com.birst.scheduler.exceptions.NotLoggedInException;
import com.birst.scheduler.exceptions.UnAuthorizedRequestCredentials;
import com.birst.scheduler.shared.Schedule;
import com.birst.scheduler.utils.AxisXmlUtils;
import com.birst.scheduler.utils.JobConstants;
import com.birst.scheduler.utils.Util;
import com.birst.scheduler.utils.UtilConstants;
import com.birst.scheduler.webServices.SchedulerWebService;
import com.birst.scheduler.webServices.XMLWebServiceResult;
import com.birst.scheduler.webServices.response.SchedulerWebServiceResult;

public class MigrationWebService extends SchedulerWebService
{
	private Logger logger = Logger.getLogger(MigrationWebService.class);
	
	public OMElement startMigration(String migrationType)
	{
		logger.info("Request to start migration");
		SchedulerWebServiceResult response = new SchedulerWebServiceResult();
		boolean typeFound = false;
		try
		{		
			if(!isLoggedIn())
			{
				throw new NotLoggedInException();
			}
			
			if(!this.inOperationsMode())
			{
				throw new UnAuthorizedRequestCredentials();
			}
			
			if(migrationType != null && (migrationType.equalsIgnoreCase(MigrationType.REPORT.toString())))				
			{
				typeFound = true;			
				MigrationOperation.addMigrationJob(migrationType);
			}
			
			if(!typeFound)
			{
				response.errorCode = SchedulerWebServiceResult.ERROR_CODE_INTERNAL;
				response.errorMessage = "Job Type not found";
			}
			
		}
		catch(Exception ex)
		{
			logger.error("Exception in adding migration job ", ex);			
			response.formatException(ex);
		}
		return response.toOMElement();
	}
	
	public OMElement getMigratedSchedules(String migrationType)
	{
		logger.info("Request to get migration schedules for job type " + migrationType);
		SchedulerWebServiceResult response = new SchedulerWebServiceResult();
		boolean typeFound = false;
		try
		{		
			if(!isLoggedIn())
			{
				throw new NotLoggedInException();
			}
			
			if(!this.inOperationsMode())
			{
				throw new UnAuthorizedRequestCredentials();
			}
			
			OMFactory factory = OMAbstractFactory.getOMFactory();
			OMNamespace ns = null;			
			OMElement scheduledJobs = factory.createOMElement(JobConstants.JOBS_LIST_NODE, ns);
			
			if(migrationType != null && (migrationType.equalsIgnoreCase(MigrationType.REPORT.toString())))				
			{
				typeFound = true;				
				List<ScheduledReport> reportsList = MigrationOperation.getMigratedReports();
				if(reportsList != null)
				{										
					for(ScheduledReport scheduledReport : reportsList)
					{
						OMElement sr = factory.createOMElement(JobConstants.JOB_NODE, ns);
						AxisXmlUtils.addContent(factory, sr, JobConstants.JOB_ID, scheduledReport.getId(), ns);
						// jobname and file name for migrated reports depend directly on report path
						AxisXmlUtils.addContent(factory, sr, JobConstants.JOB_NAME, scheduledReport.getName(), ns);						
						AxisXmlUtils.addContent(factory, sr, JobConstants.JOB_SUMMARY, scheduledReport.getSubject(), ns);
						AxisXmlUtils.addContent(factory, sr, JobConstants.JOB_SPACE_ID, scheduledReport.getSpaceID(), ns);
						AxisXmlUtils.addContent(factory, sr, JobConstants.JOB_TYPE, UtilConstants.JobType.REPORT.toString(), ns);
						AxisXmlUtils.addContent(factory, sr, JobConstants.JOB_USER_ID, scheduledReport.getUserID(), ns);						
						AxisXmlUtils.addContent(factory, sr, JobConstants.JOB_EXECUTION_STATUS, scheduledReport.isCurrentExecutionStatus(), ns);
						AxisXmlUtils.addContent(factory, sr, JobConstants.JOB_COMMENTS, scheduledReport.getComments(), ns);

						OMElement schedules = factory.createOMElement(JobConstants.JOB_SCHEDULE_LIST, ns);
						Schedule[] jobSchedules = scheduledReport.getJobSchedules();
						if(jobSchedules != null && jobSchedules.length > 0)
						{
							for(Schedule jobSchedule : jobSchedules)
							{
								OMElement schedule = factory.createOMElement(JobConstants.JOB_SCHEDULE, ns);
								AxisXmlUtils.addContent(factory, schedule, JobConstants.JOB_SCHEDULE_ID, jobSchedule.getId(), ns);								
								AxisXmlUtils.addContent(factory, schedule, JobConstants.JOB_SCHEDULE_JOB_ID, jobSchedule.getJobId(), ns);
								AxisXmlUtils.addContent(factory, schedule, JobConstants.JOB_SCHEDULE_EXPRESSION, jobSchedule.getExpression(), ns);
								AxisXmlUtils.addContent(factory, schedule, JobConstants.JOB_SCHEDULE_TIMEZONE, jobSchedule.getTimeZone(), ns);
								AxisXmlUtils.addContent(factory, schedule, JobConstants.JOB_SCHEDULE_ENABLE, jobSchedule.isEnable(), ns);
								schedules.addChild(schedule);
							}
						}
						sr.addChild(schedules);
						scheduledJobs.addChild(sr);
					}
				}
			}
			
			if(migrationType != null && (migrationType.equalsIgnoreCase(MigrationType.REPORT.toString())))	
			
			if(!typeFound)
			{
				response.errorCode = SchedulerWebServiceResult.ERROR_CODE_INTERNAL;
				response.errorMessage = "Migration Type not found";
			}
			
			response.setResult(new XMLWebServiceResult(scheduledJobs));
		}
		catch(Exception ex)
		{
			logger.error("Exception in adding migration job ", ex);			
			response.formatException(ex);
		}
		return response.toOMElement();
	}
	
	@SuppressWarnings("unchecked")
	public OMElement getMigrationJobStatus(String migrationType)
	{
		logger.info("Request to get migration job");
		SchedulerWebServiceResult response = new SchedulerWebServiceResult();
		boolean typeFound = false;
		try
		{		
			if(!isLoggedIn())
			{
				throw new NotLoggedInException();
			}
			
			if(!this.inOperationsMode())
			{
				throw new UnAuthorizedRequestCredentials();
			}
			
			OMFactory factory = OMAbstractFactory.getOMFactory();
			OMNamespace ns = null;			
			OMElement scheduledJobs = factory.createOMElement(JobConstants.JOBS_LIST_NODE, ns);
			Scheduler scheduler = SchedulerServerContext.getSchedulerInstance();
			if(migrationType != null && (migrationType.equalsIgnoreCase(MigrationType.REPORT.toString())))				
			{
				typeFound = true;
				String status = MigrationOperation.getMigrationStatus(MigrationType.REPORT.toString());	
				JobKey jobKey = new JobKey(MigrationUtils.MIGRATION_JOB_TYPE_REPORT, MigrationUtils.MIGRATION_JOB_GROUP);
				JobDetail jobDetail = scheduler.getJobDetail(jobKey);				
				String jobName = jobKey.getName();
				String jobGroup = jobKey.getGroup();
				
				if(jobDetail != null)
				{	
					OMElement job = factory.createOMElement(JobConstants.JOB_NODE, ns);
					AxisXmlUtils.addContent(factory, job, JobConstants.JOB_ID, jobName, ns);
					AxisXmlUtils.addContent(factory, job, JobConstants.JOB_NAME, jobName, ns);
					AxisXmlUtils.addContent(factory, job, JobConstants.JOB_SUMMARY, jobGroup, ns);
					AxisXmlUtils.addContent(factory, job, JobConstants.JOB_TYPE, MigrationType.REPORT.toString(), ns);						
					AxisXmlUtils.addContent(factory, job, JobConstants.JOB_COMMENTS, status, ns);
					scheduledJobs.addChild(job);
				}
			}
			if(!typeFound)
			{
				response.errorCode = SchedulerWebServiceResult.ERROR_CODE_INTERNAL;
				response.errorMessage = "Migration Type not found";
			}
			
			response.setResult(new XMLWebServiceResult(scheduledJobs));
		}
		catch(Exception ex)
		{
			logger.error("Exception in adding migration job ", ex);			
			response.formatException(ex);
		}
		return response.toOMElement();
	}
	
	public OMElement getMigrationStatus(String migrationType)
	{
		logger.info("Request for getting migration status ");
		SchedulerWebServiceResult response = new SchedulerWebServiceResult();
		try
		{			
			if(!isLoggedIn())
			{
				throw new NotLoggedInException();
			}
			
			if(!inOperationsMode())
			{
				throw new UnAuthorizedRequestCredentials();
			}
			
			String status = MigrationOperation.getMigrationStatus(migrationType);
			OMFactory factory = OMAbstractFactory.getOMFactory();
			OMNamespace ns = null;			
			OMElement migrationElement = factory.createOMElement("Migration", ns);
			AxisXmlUtils.addContent(factory, migrationElement, "MigrationType", migrationType, ns);
			AxisXmlUtils.addContent(factory, migrationElement, "Status", status, ns);			
			response.setResult(new XMLWebServiceResult(migrationElement));
		}
		catch(Exception ex)
		{
			logger.error("Exception in getting migration status for migration type " + migrationType  , ex );			
			response.formatException(ex);
		}
		return response.toOMElement();	
	}
	
	public OMElement enableMigratedSchedules(String migrationType)
	{
		logger.info("Request for enabling migrated schedules type " + migrationType);
		SchedulerWebServiceResult response = new SchedulerWebServiceResult();
		try
		{			
			if(!isLoggedIn())
			{
				throw new NotLoggedInException();
			}
			
			if(!inOperationsMode())
			{
				throw new UnAuthorizedRequestCredentials();
			}
			
			if(migrationType != null && migrationType.equalsIgnoreCase(MigrationType.REPORT.toString()))
			{
				MigrationOperation.enableMigratedReports();
			}
		}
		catch(Exception ex)
		{
			logger.error("Exception in getting migration status for migration type " + migrationType  , ex );			
			response.formatException(ex);
		}
		return response.toOMElement();	
	}
	
	public OMElement disableMigratedSchedules(String migrationType)
	{
		logger.info("Request for disabling migrated schedules for type " + migrationType);
		SchedulerWebServiceResult response = new SchedulerWebServiceResult();
		try
		{			
			if(!isLoggedIn())
			{
				throw new NotLoggedInException();
			}
			
			if(!inOperationsMode())
			{
				throw new UnAuthorizedRequestCredentials();
			}
			
			if(migrationType != null && migrationType.equalsIgnoreCase(MigrationType.REPORT.toString()))
			{
				MigrationOperation.disableMigratedReports();
			}
		}
		catch(Exception ex)
		{
			logger.error("Exception in getting migration status for migration type " + migrationType  , ex );			
			response.formatException(ex);
		}
		return response.toOMElement();	
	}
}
