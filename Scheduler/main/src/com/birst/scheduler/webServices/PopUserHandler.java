/**
 * $Id: PopUserHandler.java,v 1.2 2011-07-26 00:19:36 gsingh Exp $
 *
 * Copyright (C) 2009 Birst, Inc. All rights reserved.
 * BIRST PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package com.birst.scheduler.webServices;

import org.apache.axis2.AxisFault;
import org.apache.axis2.context.MessageContext;
import org.apache.axis2.handlers.AbstractHandler;

import com.birst.scheduler.utils.Util;

public class PopUserHandler extends AbstractHandler
{
	public PopUserHandler()
	{
	}
	public InvocationResponse invoke(MessageContext msgContext) throws AxisFault
	{
		Util.clearOutMDC();
		return InvocationResponse.CONTINUE;
	}
}