

package com.birst.scheduler.webServices;

import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;

public interface IWebServiceResult {

	/**
	 * Adds the content of this result object to the result object
	 * @param parent	the parent XML node
	 * @param factory	the OMFactory to use while adding content
	 * @param ns	the namespace to use
	 */
	public void addContent(OMElement parent, OMFactory factory, OMNamespace ns);
	
}
