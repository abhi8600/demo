package com.birst.scheduler.salesforce;

import org.apache.log4j.Logger;
import org.quartz.InterruptableJob;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.UnableToInterruptJobException;

import com.birst.scheduler.exceptions.ConcurrentSpaceOperationException;
import com.birst.scheduler.shared.JobStatus;
import com.birst.scheduler.utils.Util;
import com.birst.scheduler.utils.UtilConstants;
import com.birst.scheduler.utils.UtilConstants.RunningJobStatus;

//@DisallowConcurrentExecution
public class SFDCJob implements InterruptableJob
{
	String sfdcScheduleStepId;
	String runningJobId;
	JobStatus executionStatus;

	public static final Logger logger = Logger.getLogger(SFDCJob.class);
	@Override
	public void interrupt() throws UnableToInterruptJobException
	{
	}

	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException
	{
		String spaceID = null;
		try
		{
			String groupName = context.getJobDetail().getKey().getGroup();
			spaceID = groupName.substring(UtilConstants.PREFIX_CONNECTORS_JOB_GROUP_NAME.length());
			logger.debug("SFDC Job starting for space " + spaceID);
			
			runningJobId = Util.getObjectID(context.getJobDetail().getKey().getName(), UtilConstants.PREFIX_CONNECTORS_JOB);
			SFDCExecution sfdcExecution = new SFDCExecution(spaceID, runningJobId, null, false);
			sfdcExecution.executeOperation();
			
			executionStatus = sfdcExecution.getExecutionStatus();
			if(executionStatus.getJobStatus().getStatus() == RunningJobStatus.FAILED.getStatus())
			{
				return;
			}
	
			Util.rescheduleTriggersInJobGroup(spaceID, groupName, context.getTrigger());
		}
		catch(Exception ex)
		{	
			logger.error("Exception while running SFDCJob" , ex);
			if(ex instanceof ConcurrentSpaceOperationException)
			{
				logger.error("Concurrent exception detected : " + spaceID);
			}
			throw new JobExecutionException("Error while running SFDCJob", ex.getCause());	
		}
		finally
		{	
			Util.configureForRetry(context, executionStatus);
		}
	}
}
