package com.birst.scheduler.salesforce;

import static org.quartz.CronScheduleBuilder.cronSchedule;

import java.io.IOException;
import java.io.StringReader;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import java.util.UUID;
import java.util.Map.Entry;

import javax.xml.namespace.QName;
import javax.xml.stream.FactoryConfigurationError;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
import org.apache.axiom.om.impl.builder.StAXOMBuilder;
import org.apache.log4j.Logger;
import org.quartz.CronTrigger;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.TriggerKey;
import org.quartz.impl.matchers.GroupMatcher;

import com.birst.scheduler.SchedulerServerContext;
import com.birst.scheduler.exceptions.ConcurrentSpaceOperationException;
import com.birst.scheduler.exceptions.JobRunningException;
import com.birst.scheduler.exceptions.ObjectUnavailableException;
import com.birst.scheduler.exceptions.SchedulerBaseException;
import com.birst.scheduler.shared.CopyJob;
import com.birst.scheduler.shared.Operation;
import com.birst.scheduler.shared.RunNowOperation;
import com.birst.scheduler.shared.Schedule;
import com.birst.scheduler.shared.SchedulerVersion;
import com.birst.scheduler.shared.SpaceStatus;
import com.birst.scheduler.utils.AxisXmlUtils;
import com.birst.scheduler.utils.CompareUtils;
import com.birst.scheduler.utils.JobConstants;
import com.birst.scheduler.utils.SchedulerDBConnection;
import com.birst.scheduler.utils.SchedulerDatabase;
import com.birst.scheduler.utils.StatusUtils;
import com.birst.scheduler.utils.Util;
import com.birst.scheduler.utils.UtilConstants;
import com.birst.scheduler.utils.StatusUtils.Step;
import com.birst.scheduler.utils.StatusUtils.StepStatus;
import com.birst.scheduler.utils.UtilConstants.Type;
import com.birst.scheduler.webServices.XMLWebServiceResult;
import com.birst.scheduler.webServices.admin.JobAdminUtil;
import com.birst.scheduler.webServices.response.SchedulerWebServiceResult;

public class SFDCJobOperation implements Operation
{
	public static final Logger logger = Logger.getLogger(SFDCJobOperation.class);
	@SuppressWarnings("unchecked")
	public static SFDCInfo parseConnectorOptionsXmlString(String spaceId, String userId, String jobOptionsXmlString)
	{
		SFDCInfo connectorInfo = null;
		if(jobOptionsXmlString == null || jobOptionsXmlString.trim().length() == 0)
		{
			connectorInfo = new SFDCInfo();
			connectorInfo.setSpaceId(spaceId);
			connectorInfo.setUserId(userId);
			return connectorInfo;
		}
		XMLStreamReader reader = null;

		try
		{	
			reader = XMLInputFactory.newInstance().createXMLStreamReader(new StringReader(jobOptionsXmlString));
			StAXOMBuilder builder = new StAXOMBuilder(reader);
			OMElement docElement = builder.getDocumentElement();
			docElement.build();
			docElement.detach();

			OMElement element = docElement.getFirstChildWithName(new QName("", JobConstants.JOB_CONN_NODE));
			// see if the root element itself is ConnectorInfo
			if(element == null)
			{
				if (docElement.getLocalName() == JobConstants.JOB_CONN_NODE)
				{
					element = docElement;
				}
			}
			if(element != null)
			{
				connectorInfo = new SFDCInfo();
				connectorInfo.setSpaceId(spaceId);
				connectorInfo.setUserId(userId);
				Iterator<OMElement> iterator = element.getChildElements();
				while(iterator.hasNext())
				{
					OMElement childElement = iterator.next();
					String elementName = childElement.getLocalName();
					String elementText = childElement.getText();					

					if(JobConstants.JOB_ID.equals(elementName))
					{	
						connectorInfo.setId(elementText);
					}
					
					if(JobConstants.JOB_SP_LOADDATE.equals(elementName))
					{	
						connectorInfo.setLoadDate(elementText);
					}
					
					if(JobConstants.JOB_SP_SUBGROUPS.equals(elementName))
					{	
						connectorInfo.setSubGroups(elementText);
					}
					
					if(JobConstants.JOB_CONN_EXTRACT_GROUPS.equals(elementName))
					{	
						connectorInfo.setExtractGroups(elementText);
					}
					
					if(JobConstants.JOB_CONN_CLIENT_ID.equals(elementName))
					{	
						connectorInfo.setClientId(elementText);
					}
					
					if(JobConstants.JOB_CONN_SANDBOX_URL.equals(elementName))
					{	
						connectorInfo.setSandBoxUrl(elementText);
					}
					
					if(JobConstants.JOB_CONN_RUN_NOW_MODE.equals(elementName))
					{	
						connectorInfo.setScheduledMode(Boolean.parseBoolean(elementText));
					}
					
					if(JobConstants.JOB_CONN_PROCESS_AFTER.equals(elementName))
					{	
						connectorInfo.setProcessAfter(Boolean.parseBoolean(elementText));
					}
					
					if(JobConstants.JOB_SEND_EMAIL.equals(elementName))
					{	
						connectorInfo.setSendEmail(Boolean.parseBoolean(elementText));
					}
					
					if(JobConstants.JOB_CONN_SCHED_NAME.equals(elementName))
					{	
						connectorInfo.setName(elementText);
					}
					
					if(JobConstants.JOB_CONN_TYPE.equals(elementName))
					{	
						String jobType = elementText;
						if (!SchedulerServerContext.getUseNewConnectorFramework())
						{
							jobType = JobAdminUtil.getJobTypeForOldFramework(jobType);
						}
						connectorInfo.setType(jobType);
					}
					
					if(JobConstants.JOB_CONN_EXTRACT_ENGINE.equals(elementName))
					{
						if(elementText != null && elementText.trim().length() > 0)
						{
							connectorInfo.setExtractionEngine(Integer.parseInt(elementText));
						}
					}
					if(JobConstants.JOB_SCHEDULE_LIST.equals(elementName))
					{
						List<Schedule> schedules = Util.parseSchedules(childElement);
						connectorInfo.setJobSchedules((Schedule [])schedules.toArray(new Schedule[schedules.size()]));
					}
				}
			}
		} catch (XMLStreamException e)
		{
			logger.error(e,e);
		} catch (FactoryConfigurationError e)
		{
			logger.error(e,e);
		}
		return connectorInfo;
	}

	@Override
	public void addJob(SchedulerWebServiceResult result, String spaceID, String userID, String jobOptionsXmlString) throws Exception
	{	
		SFDCInfo connectorInfo = SFDCJobOperation.parseConnectorOptionsXmlString(spaceID, userID, jobOptionsXmlString);
		connectorInfo = SFDCJobOperation.addConnectorInfo(result, spaceID, connectorInfo);
	}

	public static SFDCInfo addConnectorInfo(String spaceID, SFDCInfo connectorInfo) throws SQLException, SchedulerBaseException, ClassNotFoundException, SchedulerException, ParseException, IOException
	{
		Scheduler scheduler = SchedulerServerContext.getSchedulerInstance();
		return SFDCJobOperation.addConnectorInfo(null, scheduler, spaceID, connectorInfo, false);
	}
	
	public static SFDCInfo addConnectorInfo(SchedulerWebServiceResult result, String spaceID, SFDCInfo connectorInfo) throws SQLException, SchedulerBaseException, ClassNotFoundException, SchedulerException, ParseException, IOException
	{
		Scheduler scheduler = SchedulerServerContext.getSchedulerInstance();
		return SFDCJobOperation.addConnectorInfo(result, scheduler, spaceID, connectorInfo, false);
	}
	
	public static SFDCInfo addConnectorInfo(Scheduler scheduler, String spaceID, SFDCInfo connectorInfo, boolean isMigration) 
		throws SQLException, SchedulerBaseException, ClassNotFoundException, SchedulerException, ParseException, IOException {
		return addConnectorInfo(null, scheduler, spaceID, connectorInfo, isMigration);
	}
	
	public static SFDCInfo addConnectorInfo(Scheduler scheduler, String spaceID, SFDCInfo connectorInfo) 
		throws SQLException, SchedulerBaseException, ClassNotFoundException, SchedulerException, ParseException, IOException {
		return addConnectorInfo(null, scheduler, spaceID, connectorInfo, false);
	}
	
	public static SFDCInfo addConnectorInfo(SchedulerWebServiceResult result, Scheduler scheduler, String spaceID, SFDCInfo connectorInfo, boolean isMigration) 
		throws SQLException, SchedulerBaseException, ClassNotFoundException, SchedulerException, ParseException, IOException
	{
		Connection conn = null;
		try
		{
			String jobID = UUID.randomUUID().toString();
			connectorInfo.setId(jobID);			
			conn = SchedulerDBConnection.getInstance().getConnectionPool().getConnection();
			String schema = UtilConstants.SCHEDULER_DB_SCHEMA;
			boolean isValid = true;
			
			if (SchedulerDatabase.isValidScheduleName(conn, schema, spaceID, null, connectorInfo.getName()) || isMigration) {
				isValid = isValidScheduleTime(conn, schema, connectorInfo);
				if (isValid || isMigration) {
					// schedules are filled as part of the supplied connectorInfo object
					Schedule[] schedules = connectorInfo.getJobSchedules();
					
					// Add sfdc info to the database
					SchedulerDatabase.addConnectorInfo(conn, schema, connectorInfo);
					if(connectorInfo.isRunNowMode())
					{				
						runConnectorNow(spaceID, connectorInfo);
					}
					else
					{	
						// If no run now schedule, do normal scheduling				
						// Create job and trigger details and schedule it
						JobBuilder jobBuilder = Util.getBirstJobBuilder(SFDCJob.class, 
								Util.getCustConnectorJobName(connectorInfo.getId()), 
								Util.getCustJobGroupNameForSFDC(spaceID));
						JobDetail jobDetail = jobBuilder.build();
						scheduler.addJob(jobDetail, false);
						if(schedules != null && schedules.length > 0)
						{
							for (Schedule schedule : schedules)
							{
								// Add the trigger only if enable flag is set.
								if(schedule.getId() == null)
								{
									schedule.setId(UUID.randomUUID().toString());
								}
								schedule.setJobId(jobID);
								SchedulerDatabase.addSchedule(conn, schema, schedule);
								if(!schedule.isEnable())
								{
									continue;						
								}
		
								String timeZoneID = Schedule.getTimeZoneID(schedule.getTimeZone());
								
								/*CronTrigger cronTrigger = TriggerBuilder.newTrigger()
								.withIdentity(Util.getCustConnectorTriggerName(schedule.getId()), spaceID)
								.withSchedule(cronSchedule(schedule.getExpression()).inTimeZone(TimeZone.getTimeZone(timeZoneID)))
								.forJob(jobDetail)
								.build();*/
								
								TriggerBuilder<CronTrigger> triggerBuilder = TriggerBuilder.newTrigger()
								.withIdentity(Util.getCustConnectorTriggerName(schedule.getId()), spaceID)
								.withSchedule(cronSchedule(schedule.getExpression()).inTimeZone(TimeZone.getTimeZone(timeZoneID)))
								.forJob(jobDetail);
								
								Date startDate = Util.getDate(schedule.getStartDate());
								if(startDate != null && startDate.after(Calendar.getInstance().getTime()))
								{	
									triggerBuilder.startAt(startDate);
								}
								
								CronTrigger cronTrigger = triggerBuilder.build();
								
								scheduler.scheduleJob(cronTrigger);
							}
						}
					}
				} else {
					result.setErrorCode(SchedulerBaseException.ERROR_DUPLICATE_SCHEDULE);
					result.setErrorMessage("A schedule already exists with the same scheduled time.");
				}
			} else {
				if (result != null) {
					result.setErrorCode(SchedulerBaseException.ERROR_DUPLICATE_JOB_NAME);
					result.setErrorMessage("A schedule already exists with the same schedule name.");
				}
			}
			SFDCInfo connInfo = SFDCJobOperation.getConnectorInfo(jobID);
			if (!isValid && isMigration) {
				connInfo.currentExecutionStatus = false;
			}
			return connInfo;
		}
		finally{
			if(conn != null)
			{
				try
				{
					conn.close();
				}catch(Exception ex)
				{
					logger.warn("Exception in closing connection ",ex);
				}
			}
		}
	}
	
	public static SFDCInfo getConnectorInfo(String connectorInfoId) throws SQLException, SchedulerBaseException, ClassNotFoundException
	{		
		Connection conn = null;
		try
		{
			conn = SchedulerDBConnection.getInstance().getConnectionPool().getConnection();
			String schema = UtilConstants.SCHEDULER_DB_SCHEMA;
			return SchedulerDatabase.getConnectorInfo(conn, schema, connectorInfoId);
		}
		finally
		{
			if(conn != null)
			{
				try
				{
					conn.close();
				}catch(Exception ex)
				{
					logger.warn("Exception in closing connection ",ex);
				}
			}
		}
	}
	
	public static void addAndRunNowSFDCJob(String spaceID, SFDCInfo connectorInfo) 
		throws SQLException, SchedulerBaseException, ClassNotFoundException, SchedulerException, IOException
	{
		// if any schedule is run now, then create a new job which is non-durable so that it gets cleaned 
		// after it is run and no trigger are associated with it
		JobBuilder jobBuilder = Util.getBirstJobBuilder(SFDCJob.class, Util.getCustConnectorJobName(connectorInfo.getId()), 
				Util.getCustJobGroupNameForSFDC(spaceID), false);
		JobDetail jobDetail = jobBuilder.build();
		runConnectorNow(spaceID, connectorInfo);
	}
	
	/**
	 * If the jobdetail is null, the method implies that job has already been 
	 * added to the scheduler and can be inferred via connectorInfoId.
	 * If on the other hand, jobdetail is specified, then that jobDetail is used
	 * to schedule immediate run
	 * @param spaceID
	 * @param connectorInfoId
	 * @param jobDetail
	 * @throws SQLException
	 * @throws SchedulerBaseException
	 * @throws ClassNotFoundException
	 * @throws SchedulerException
	 * @throws IOException 
	 */
	public static void runConnectorNow(String spaceID, SFDCInfo connectorInfo) throws SQLException, SchedulerBaseException, ClassNotFoundException, SchedulerException, IOException
	{
		SpaceStatus anyConcurrentSpaceStatus = StatusUtils.getActiveSpaceStatus(spaceID);
		if(anyConcurrentSpaceStatus != null)
		{
			throw new ConcurrentSpaceOperationException("Another job is running for this space : " + anyConcurrentSpaceStatus.toString());
		}
		
		Connection conn = null;
		try
		{
			conn = SchedulerDBConnection.getInstance().getConnectionPool().getConnection();
			String schema = UtilConstants.SCHEDULER_DB_SCHEMA;
			
			SpaceStatus spaceStatus = new SpaceStatus(UUID.randomUUID().toString(), spaceID, connectorInfo.getId(), -1, null, Step.ScheduledExtract, StepStatus.RUNNING); 
			SchedulerDatabase.addSpaceStatus(conn, schema, spaceStatus);
			SFDCExecution sfdcExecution = new SFDCExecution(spaceID, connectorInfo.getId(), spaceStatus.getId(), true);
			RunNowOperation runNowOperation = new RunNowOperation(sfdcExecution);
			String spaceDirectory = Util.getSpaceDirectory(spaceID);
			Util.createSFDCExtractFileInDirectory(spaceDirectory);
			Util.createConnectorSpecificExtractFile(connectorInfo.getType(), spaceDirectory);
			runNowOperation.startOperation();
			/*
			Scheduler scheduler   = SchedulerServerContext.getSchedulerInstance();
			
			JobDetail sfdcJobDetail = null;
			if(jobDetail == null)
			{
				sfdcJobDetail = scheduler.getJobDetail(new JobKey(Util.getCustConnectorJobName(connectorInfoId), spaceID));
			}
			else
			{
				sfdcJobDetail = jobDetail;
			}
			String uidString = UUID.randomUUID().toString();
			Trigger trigger = Util.getImmediateTrigger(spaceID, sfdcJobDetail, uidString);
			trigger.getJobDataMap().put(UtilConstants.TRIGGER_MAP_SCHEDULE_STEP, spaceStatus.getId());
			scheduler.scheduleJob(sfdcJobDetail, trigger);
			*/
			// Put in the extract lock file to signal the extraction in progress
			// this takes care of backward compatibility extraction detection logic
			
		}finally
		{
			if(conn != null)
			{
				try
				{
					conn.close();
				} catch (Exception ex)
				{}
			}
		}
	}
	
	@Override
	public void addScheduleToJob(SchedulerWebServiceResult result,
			String spaceID, String jobID, String jobOptionsXmlString) throws Exception
	{
		// TODO Auto-generated method stub
		
	}
	
	/**
	 * This method should return only one SFDC job which is scheduled.
	 * If it returns more than one job, there is some problem. Handle it on the clientn
	 * side
	 */
	@Override
	public void getAllJobs(SchedulerWebServiceResult result, String spaceID, String onlyCurrentExecuting, String userID, String filterOnUser)
			throws Exception
	{
		OMFactory factory = OMAbstractFactory.getOMFactory();
		OMNamespace ns = null;			
		OMElement scheduledJobs = factory.createOMElement(JobConstants.JOBS_LIST_NODE, ns);
		AxisXmlUtils.addContent(factory, scheduledJobs, JobConstants.PRIMARY_SCHEDULER_NAME, SchedulerServerContext.getPrimarySchedulerDisplayName(), ns);
		fillAllJobs(spaceID, onlyCurrentExecuting, userID, filterOnUser, factory, ns, scheduledJobs);	
		result.setResult(new XMLWebServiceResult(scheduledJobs));
	}
	
	public static void fillAllJobs(String spaceID, String onlyCurrentExecuting, String userID, String filterOnUser, 
			OMFactory factory, OMNamespace ns, OMElement scheduledJobs) throws Exception {
		// Get the sfdc info from all schedulers
		Map<String, String> userIdToNameMapping = new HashMap<String, String>();
		boolean returnCurrentExecuting = Boolean.valueOf(onlyCurrentExecuting);
		boolean filterByUser = Boolean.parseBoolean(filterOnUser);
		Map<String,SchedulerVersion> schedulersMap = SchedulerServerContext.getAllSchedulers();
		for(Entry<String,SchedulerVersion> entry : schedulersMap.entrySet())
		{
			String schedulerDisplayName = entry.getKey();
			SchedulerVersion schedulerVersion = entry.getValue();
			Scheduler scheduler = schedulerVersion.getScheduler();			
			List<SFDCInfo> sfdcInfoList = SFDCJobOperation.getSFDCInfo(scheduler, spaceID, filterByUser ? userID:null);
			if(sfdcInfoList != null)
			{
				for(SFDCInfo sfdcInfo : sfdcInfoList)
				{
					// if only current executing jobs are required, filter on the status
					if(returnCurrentExecuting && !sfdcInfo.isCurrentExecutionStatus())
					{
						continue;
					}

					// filter out the run now mode jobs
					if(sfdcInfo.isRunNowMode())
					{
						continue;
					}
					
					// job name logic
					String name = sfdcInfo.getName();
					if (name == null || name.equals("")) {
						name = UtilConstants.DEFAULT_NAME;
					}
					
					// job type logic
					String type = sfdcInfo.getType();
					if (type == null || type == "") {
						type = Type.sfdc.toString();
					}

					OMElement sr = factory.createOMElement(JobConstants.JOB_NODE, ns);
					AxisXmlUtils.addContent(factory, sr, JobConstants.SCHEDULER_NAME, scheduler.getSchedulerName(), ns);
					AxisXmlUtils.addContent(factory, sr, JobConstants.SCHEDULER_DISPLAY_NAME, schedulerDisplayName, ns);
					AxisXmlUtils.addContent(factory, sr, JobConstants.SCHEDULER_ENABLE_MIGRATION, schedulerVersion.getEnableMigration(), ns);
					AxisXmlUtils.addContent(factory, sr, JobConstants.IS_PRIMARY_SCHEDULER, Util.isPrimaryScheduler(scheduler), ns);
					AxisXmlUtils.addContent(factory, sr, JobConstants.JOB_ID, sfdcInfo.getId(), ns);
					AxisXmlUtils.addContent(factory, sr, JobConstants.JOB_CONN_RUN_NOW_MODE, sfdcInfo.isRunNowMode(), ns);
					AxisXmlUtils.addContent(factory, sr, JobConstants.JOB_CONN_PROCESS_AFTER, sfdcInfo.isProcessAfter(), ns);
					AxisXmlUtils.addContent(factory, sr, JobConstants.JOB_SP_SUBGROUPS, sfdcInfo.getSubGroups(), ns);
					AxisXmlUtils.addContent(factory, sr, JobConstants.JOB_CONN_SANDBOX_URL, sfdcInfo.getSandBoxUrl(), ns);
					AxisXmlUtils.addContent(factory, sr, JobConstants.JOB_CONN_CLIENT_ID, sfdcInfo.getClientId(), ns);
					AxisXmlUtils.addContent(factory, sr, JobConstants.JOB_SPACE_ID, sfdcInfo.getSpaceId(), ns);
					AxisXmlUtils.addContent(factory, sr, JobConstants.JOB_TYPE, type, ns);
					AxisXmlUtils.addContent(factory, sr, JobConstants.JOB_CONN_EXTRACT_GROUPS, sfdcInfo.getExtractGroups(), ns);
					AxisXmlUtils.addContent(factory, sr, JobConstants.JOB_USER_ID, sfdcInfo.getUserId(), ns);						
					AxisXmlUtils.addContent(factory, sr, JobConstants.JOB_EXECUTION_STATUS, sfdcInfo.isCurrentExecutionStatus(), ns);
					AxisXmlUtils.addContent(factory, sr, JobConstants.JOB_CREATED_DATE, sfdcInfo.getCreatedDate(), ns);
					AxisXmlUtils.addContent(factory, sr, JobConstants.JOB_MODIFIED_DATE, sfdcInfo.getModifiedDate(), ns);
					AxisXmlUtils.addContent(factory, sr, JobConstants.JOB_MODIFIED_BY, sfdcInfo.getModifiedBy(), ns);
					AxisXmlUtils.addContent(factory, sr, JobConstants.JOB_NAME, name, ns);
					
					// get the usernames from the map, if not present, get from acorn db and store it
					String createdUserName = Util.getAndAddUserFromMap(userIdToNameMapping, sfdcInfo.getUserId());
					AxisXmlUtils.addContent(factory, sr, JobConstants.JOB_CREATED_USER, createdUserName, ns);

					OMElement schedules = factory.createOMElement(JobConstants.JOB_SCHEDULE_LIST, ns);
					Schedule[] jobSchedules = sfdcInfo.getJobSchedules();
					// The job is scheduled to run if any of its schedule is enabled
					boolean isJobEnabled = Util.isAnyScheduleEnabled(jobSchedules);
					AxisXmlUtils.addContent(factory, sr, JobConstants.JOB_RUN, isJobEnabled, ns);

					if(jobSchedules != null && jobSchedules.length > 0)
					{
						for(Schedule jobSchedule : jobSchedules)
						{
							OMElement schedule = factory.createOMElement(JobConstants.JOB_SCHEDULE, ns);
							AxisXmlUtils.addContent(factory, schedule, JobConstants.JOB_SCHEDULE_ID, jobSchedule.getId(), ns);								
							AxisXmlUtils.addContent(factory, schedule, JobConstants.JOB_SCHEDULE_JOB_ID, jobSchedule.getJobId(), ns);
							AxisXmlUtils.addContent(factory, schedule, JobConstants.JOB_SCHEDULE_EXPRESSION, jobSchedule.getExpression(), ns);
							AxisXmlUtils.addContent(factory, schedule, JobConstants.JOB_SCHEDULE_TIMEZONE, jobSchedule.getTimeZone(), ns);
							AxisXmlUtils.addContent(factory, schedule, JobConstants.JOB_SCHEDULE_ENABLE, jobSchedule.isEnable(), ns);
							schedules.addChild(schedule);
						}
					}
					sr.addChild(schedules);
					scheduledJobs.addChild(sr);
				}
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	public static List<SFDCInfo> getSFDCInfo(Scheduler scheduler, String spaceID, String userID) throws SchedulerException, SQLException, SchedulerBaseException, ClassNotFoundException
	{	
		Set<JobKey> jobKeys = new HashSet<JobKey>();
		if(spaceID.equalsIgnoreCase(UtilConstants.ALL_SPACES))
		{
			for(String groupName : scheduler.getJobGroupNames())
			{
				if(groupName.startsWith(UtilConstants.PREFIX_CONNECTORS_JOB_GROUP_NAME))
				{
					jobKeys.addAll(scheduler.getJobKeys(GroupMatcher.groupEquals(groupName)));
				}
			}
		}
		else
		{
			String findGroupName = Util.getCustJobGroupNameForSFDC(spaceID);
			jobKeys = scheduler.getJobKeys(GroupMatcher.groupEquals(findGroupName));
		}	
		return getSFDCInfo(scheduler, spaceID, jobKeys, userID);
	}
	
	public static SFDCInfo getSFDCInfo(String sfdcInfoId) throws SQLException, SchedulerBaseException, ClassNotFoundException {		
		Connection conn = null;
		try {
			conn = SchedulerDBConnection.getInstance().getConnectionPool().getConnection();
			String schema = UtilConstants.SCHEDULER_DB_SCHEMA;
			return SchedulerDatabase.getConnectorInfo(conn, schema, sfdcInfoId);
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch(Exception ex) {
					logger.warn("Exception in closing connection : ",ex);
				}
			}
		}
	}

	public static List<SFDCInfo> getSFDCInfo(Scheduler scheduler, String spaceID, Set<JobKey> jobKeys, String userID) throws SchedulerException, SQLException, SchedulerBaseException, ClassNotFoundException
	{		
		List<SFDCInfo> sfdcInfoList = new ArrayList<SFDCInfo>();
		if(jobKeys != null && jobKeys.size() > 0)
		{							
			List<String> currentlyExecutingJobs = Util.isPrimaryScheduler(scheduler) ? Util.getRunningJobs(spaceID.equalsIgnoreCase(UtilConstants.ALL_SPACES) ? null : spaceID) : null;
			Connection conn = null;
			try
			{
				conn = SchedulerDBConnection.getInstance().getConnectionPool().getConnection();
				String schema = UtilConstants.SCHEDULER_DB_SCHEMA;
				for(JobKey jobKey : jobKeys)
				{
					String sfdcJobName = jobKey.getName();
					String sfdcJobGroupName = jobKey.getGroup();
					String sfdcJobID = Util.getObjectID(sfdcJobName, UtilConstants.PREFIX_CONNECTORS_JOB);

					if(sfdcJobID != null)
					{
						SFDCInfo sfdcInfo = SchedulerDatabase.getConnectorInfo(conn, schema, sfdcJobID);
						if(sfdcInfo == null)
						{
							logger.warn("Cannot find sfdc info for " + sfdcJobID);
							if(Util.isPrimaryScheduler(scheduler))
							{
								// In this case, we should just delete the job from the scheduler
								scheduler.deleteJob(new JobKey(sfdcJobName, sfdcJobGroupName));
							}
							continue;
						}

						// filter on user ids
						if(userID != null && userID.length() > 0 && !userID.equalsIgnoreCase(sfdcInfo.getUserId()))
						{
							continue;
						}
						sfdcInfo.currentExecutionStatus = currentlyExecutingJobs != null ? currentlyExecutingJobs.contains(sfdcJobName) : false;
						sfdcInfoList.add(sfdcInfo);
					}
				}
			}
			finally
			{					
				if(conn != null)
				{
					try
					{
						conn.close();
					} catch (Exception ex)
					{}
				}
			}
		}		
		return sfdcInfoList;
	}
	
	
	@Override
	public void getJobDetails(SchedulerWebServiceResult result, String spaceID, String jobID) throws Exception
	{
		OMFactory factory = OMAbstractFactory.getOMFactory();
		OMNamespace ns = null;			
		Map<String, String> userIdToNameMapping = new HashMap<String, String>();
		OMElement scheduledJobs = factory.createOMElement(JobConstants.JOBS_LIST_NODE, ns);
		OMElement sc = factory.createOMElement(JobConstants.JOB_NODE, ns);
		SFDCInfo scheduledJobDetails = getSFDCInfo(jobID);

		if(scheduledJobDetails != null)
		{
			// job name logic
			String name = scheduledJobDetails.getName();
			if (name == null || name.equals("")) {
				name = UtilConstants.DEFAULT_NAME;;
			}
			
			// job type logic
			String type = scheduledJobDetails.getType();
			if (type == null || type == "") {
				type = Type.sfdc.toString();
			}
			
			AxisXmlUtils.addContent(factory, sc, JobConstants.JOB_ID, scheduledJobDetails.getId(), ns);
			AxisXmlUtils.addContent(factory, sc, JobConstants.JOB_CONN_RUN_NOW_MODE, scheduledJobDetails.isRunNowMode(), ns);
			AxisXmlUtils.addContent(factory, sc, JobConstants.JOB_CONN_PROCESS_AFTER, scheduledJobDetails.isProcessAfter(), ns);
			AxisXmlUtils.addContent(factory, sc, JobConstants.JOB_SP_SUBGROUPS, scheduledJobDetails.getSubGroups(), ns);
			AxisXmlUtils.addContent(factory, sc, JobConstants.JOB_SPACE_ID, scheduledJobDetails.getSpaceId(), ns);
			AxisXmlUtils.addContent(factory, sc, JobConstants.JOB_TYPE, type, ns);
			AxisXmlUtils.addContent(factory, sc, JobConstants.JOB_CONN_EXTRACT_GROUPS, scheduledJobDetails.getExtractGroups(), ns);
			AxisXmlUtils.addContent(factory, sc, JobConstants.JOB_USER_ID, scheduledJobDetails.getUserId(), ns);						
			AxisXmlUtils.addContent(factory, sc, JobConstants.JOB_EXECUTION_STATUS, scheduledJobDetails.isCurrentExecutionStatus(), ns);
			AxisXmlUtils.addContent(factory, sc, JobConstants.JOB_CREATED_DATE, scheduledJobDetails.getCreatedDate(), ns);
			AxisXmlUtils.addContent(factory, sc, JobConstants.JOB_MODIFIED_DATE, scheduledJobDetails.getModifiedDate(), ns);
			AxisXmlUtils.addContent(factory, sc, JobConstants.JOB_MODIFIED_BY, scheduledJobDetails.getModifiedBy(), ns);
			AxisXmlUtils.addContent(factory, sc, JobConstants.JOB_NAME, name, ns);
			
			// Get the usernames from the map, if not present, get from acorn db and store it.
			String createdUserName = Util.getAndAddUserFromMap(userIdToNameMapping, scheduledJobDetails.getUserId());
			AxisXmlUtils.addContent(factory, sc, JobConstants.JOB_CREATED_USER, createdUserName, ns);

			// The job is scheduled to run if any of its schedule is enabled.
			OMElement schedules = factory.createOMElement(JobConstants.JOB_SCHEDULE_LIST, ns);
			Schedule[] jobSchedules = scheduledJobDetails.getJobSchedules();
			boolean isJobEnabled = Util.isAnyScheduleEnabled(jobSchedules);
			AxisXmlUtils.addContent(factory, sc, JobConstants.JOB_RUN, isJobEnabled, ns);
			if (jobSchedules != null && jobSchedules.length > 0) {
				for (Schedule jobSchedule : jobSchedules) {
					OMElement schedule = factory.createOMElement(JobConstants.JOB_SCHEDULE, ns);
					AxisXmlUtils.addContent(factory, schedule, JobConstants.JOB_SCHEDULE_ID, jobSchedule.getId(), ns);								
					AxisXmlUtils.addContent(factory, schedule, JobConstants.JOB_SCHEDULE_JOB_ID, jobSchedule.getJobId(), ns);
					AxisXmlUtils.addContent(factory, schedule, JobConstants.JOB_SCHEDULE_EXPRESSION, jobSchedule.getExpression(), ns);
					AxisXmlUtils.addContent(factory, schedule, JobConstants.JOB_SCHEDULE_TIMEZONE, jobSchedule.getTimeZone(), ns);
					AxisXmlUtils.addContent(factory, schedule, JobConstants.JOB_SCHEDULE_ENABLE, jobSchedule.isEnable(), ns);
					schedules.addChild(schedule);
				}
			}
			sc.addChild(schedules);
			scheduledJobs.addChild(sc);
		}
		result.setResult(new XMLWebServiceResult(scheduledJobs));
	}

	@Override
	public void getJobStatus(SchedulerWebServiceResult result, String spaceID,
			String jobOptionsXmlString) throws Exception
	{
		OMFactory factory = OMAbstractFactory.getOMFactory();
		OMNamespace ns = null;
		OMElement status = factory.createOMElement(JobConstants.JOB_SP_STATUS_NODE, ns);

		SFDCInfo sfdcInfo = SFDCJobOperation.parseConnectorOptionsXmlString(spaceID, null, jobOptionsXmlString);
		SpaceStatus spaceStatus = StatusUtils.getJobStatus(spaceID, -1, null);

		// based on the space status mark it as available for operation
		boolean available = false;
		if(spaceStatus != null)
		{
			if(spaceStatus.getStatus() != StepStatus.RUNNING.getStatus())
			{
				available = true;
			}
		
			AxisXmlUtils.addContent(factory, status, JobConstants.JOB_SP_AVAILABLE, available, ns);
			AxisXmlUtils.addContent(factory, status, JobConstants.JOB_SP_STATUS, spaceStatus.getStatus(), ns);
			AxisXmlUtils.addContent(factory, status, JobConstants.JOB_SP_STEP, spaceStatus.getStep(), ns);
			AxisXmlUtils.addContent(factory, status, JobConstants.JOB_SP_LOAD_ID, spaceStatus.getLoadNumber(), ns);
			AxisXmlUtils.addContent(factory, status, JobConstants.JOB_SP_LOAD_GROUP, spaceStatus.getLoadGroup(), ns);
			AxisXmlUtils.addContent(factory, status, JobConstants.JOB_SP_CREATED_DATE, spaceStatus.getCreatedDate(), "MM/dd/yyyy HH:mm:ss", ns);
			AxisXmlUtils.addContent(factory, status, JobConstants.JOB_SP_MODIFIED_DATE, spaceStatus.getModifiedDate(), "MM/dd/yyyy HH:mm:ss", ns);
		}
		else
		{
			available = true;
			AxisXmlUtils.addContent(factory, status, JobConstants.JOB_SP_AVAILABLE, available, ns);
		}

		result.setResult(new XMLWebServiceResult(status));
		
	}
	
	@Override
	public void killJob(SchedulerWebServiceResult result, String spaceID,
			String jobsRunStatusXmlString) throws Exception
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pauseJob(SchedulerWebServiceResult result, String spaceID, String typeID) throws Exception
	{
		flagAllSchedules(spaceID, typeID, false);
	}
	
	public static void flagAllSchedules(String spaceID, String typeID, boolean enable) 
		throws SchedulerException, SchedulerBaseException, SQLException, ParseException, ClassNotFoundException 
	{
		Scheduler scheduler = Util.getSelectedScheduler(spaceID, typeID);
		if (scheduler != null) {
			flagAllSchedules(scheduler, spaceID, typeID, enable);
		}
	}
	
	public static void flagAllSchedules(Scheduler scheduler, String spaceID, String typeID, boolean enable) 
		throws SchedulerException, SchedulerBaseException, SQLException, ParseException, ClassNotFoundException 
	{
		SFDCInfo sfdcInfo = SFDCJobOperation.getConnectorInfo(typeID);
		if(sfdcInfo != null)
		{
			Schedule[] schedules = sfdcInfo.getJobSchedules();
			for(Schedule schedule : schedules)
			{
				schedule.setEnable(enable);
			}
			SFDCJobOperation.updateSFDCInfo(scheduler, spaceID, sfdcInfo, true, false);
		}
	}
	
	@Override
	public void removeJob(SchedulerWebServiceResult result, String spaceID, String jobID) throws Exception
	{
		// Scheduler scheduler = SchedulerServerContext.getSchedulerInstance();
		Scheduler scheduler = Util.getSelectedScheduler(spaceID, jobID);
		removeSFDCInfo(scheduler, spaceID, jobID);
	}
	
	public static void removeAllScheduledConnectors(String spaceID) throws SQLException, SchedulerBaseException, ClassNotFoundException
	{
		Connection conn = null;		
		try
		{
			conn = SchedulerDBConnection.getInstance().getConnectionPool().getConnection();
			String schema = UtilConstants.SCHEDULER_DB_SCHEMA;
			SchedulerDatabase.deleteAllScheduledConnectors(conn, schema, spaceID);
		}
		finally
		{
			if(conn != null)
			{
				try
				{
					conn.close();
				} catch (Exception ex)
				{}
			}
		}
	}
	
	public static boolean removeSFDCInfo(Scheduler scheduler, String spaceID, String sfdcInfoId) throws SQLException, SchedulerBaseException, ClassNotFoundException, SchedulerException
	{
		Connection conn = null;		
		try
		{
			conn = SchedulerDBConnection.getInstance().getConnectionPool().getConnection();
			String schema = UtilConstants.SCHEDULER_DB_SCHEMA;
			SFDCInfo sfdcInfo = SchedulerDatabase.getConnectorInfo(conn, schema, sfdcInfoId);
			if(sfdcInfo == null)
			{
				return false;
			}

			String jobName = Util.getCustConnectorJobName(sfdcInfo.getId()); 
			String groupName = Util.getCustJobGroupNameForSFDC(spaceID);
			List<String> runningJobs = Util.getRunningJobs(jobName, groupName);
			if(runningJobs != null && runningJobs.size() > 0)
			{
				// schedule is in progress. Cannot remove sfdc info job
				throw new JobRunningException("SFDC job currently running " + sfdcInfoId );
			}
						
			scheduler.deleteJob(new JobKey(jobName, groupName));
			// Now that the job has been removed from scheduler. Remove the scheduleInfo and sfdc info from db
			Schedule[] schedules = sfdcInfo.getJobSchedules();
			if(schedules != null && schedules.length > 0)
			{
				for(Schedule schedule : schedules)
				{
					SchedulerDatabase.removeSchedule(conn, schema, schedule.getId());
				}
			}

			SchedulerDatabase.deleteConnectorInfo(conn, schema, sfdcInfoId);
			return true;
		}
		finally
		{
			if(conn != null)
			{
				try
				{
					conn.close();
				} catch (Exception ex)
				{}
			}
		}
	}
		
	@Override
	public void removeScheduleFromJob(SchedulerWebServiceResult result,
			String spaceID, String jobID, String scheduleID) throws Exception
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resumeJob(SchedulerWebServiceResult result, String spaceID, String typeID) throws Exception
	{
		flagAllSchedules(spaceID, typeID, true);
	}

	@Override
	public void runNow(SchedulerWebServiceResult result, String spaceID, String typeID) throws Exception
	{
		Connection conn = null;
		try {
			conn = SchedulerDBConnection.getInstance().getConnectionPool().getConnection();
			String schema = UtilConstants.SCHEDULER_DB_SCHEMA;
			runConnectorNow(spaceID, SchedulerDatabase.getConnectorInfo(conn, schema, typeID));
		} catch (Exception e)
		{
			logger.error("Exception in running job: ", e);
			throw new SchedulerException();
		} finally {
			if (conn != null) {
				conn.close();
			}
		}
	}
	
	public void runOldSchedulerNow(SchedulerWebServiceResult result, String spaceID, String typeID) throws Exception
	{
		try {
			Scheduler scheduler = SchedulerServerContext.getSchedulerInstance();
			JobDetail jobDetail = scheduler.getJobDetail(new JobKey(Util.getCustConnectorJobName(typeID), Util.getCustJobGroupNameForSFDC(spaceID)));
			String uidString = UUID.randomUUID().toString();
			Trigger trigger = Util.getImmediateTrigger(spaceID, jobDetail, uidString);
			trigger.getJobDataMap().put(UtilConstants.TRIGGER_MAP_SCHEDULE_STEP, typeID);
			scheduler.scheduleJob(trigger);
		} catch (Exception e)
		{
			logger.error("Exception in running job: ", e);
			throw new SchedulerException();
		}
	}

	@Override
	public void updateJob(SchedulerWebServiceResult result, String spaceID,
			String userID, String jobID, String jobOptionsXmlString)
			throws Exception
	{
		SFDCInfo sfdcInfo = SFDCJobOperation.parseConnectorOptionsXmlString(spaceID, userID, jobOptionsXmlString);
		sfdcInfo.setId(jobID);
		if(sfdcInfo.getSpaceId() == null)
		{
			sfdcInfo.setSpaceId(spaceID);
		}
		SFDCJobOperation.updateSFDCInfo(result, spaceID, sfdcInfo);
	}

	
	public static SFDCInfo updateSFDCInfo(String spaceID, SFDCInfo sfdcInfo) 
		throws SchedulerException, SchedulerBaseException, SQLException, ParseException, ClassNotFoundException
	{
		Scheduler scheduler = SchedulerServerContext.getSchedulerInstance();
		return updateSFDCInfo(scheduler, spaceID, sfdcInfo);
	}
	
	public static SFDCInfo updateSFDCInfo(SchedulerWebServiceResult result, String spaceID, SFDCInfo sfdcInfo) 
		throws SchedulerException, SchedulerBaseException, SQLException, ParseException, ClassNotFoundException
	{
		Scheduler scheduler = SchedulerServerContext.getSchedulerInstance();
		return updateSFDCInfo(result, scheduler, spaceID, sfdcInfo, false);
	}
	
	public static SFDCInfo updateSFDCInfo(Scheduler scheduler, String spaceID,
			SFDCInfo updatedSfdcInfo) throws SchedulerException,
			SchedulerBaseException, SQLException, ParseException,
			ClassNotFoundException
	{
		return updateSFDCInfo(null, scheduler, spaceID, updatedSfdcInfo, false);
	}
	
	public static SFDCInfo updateSFDCInfo(Scheduler scheduler, String spaceID,
			SFDCInfo updatedSfdcInfo, boolean isMigration) throws SchedulerException,
			SchedulerBaseException, SQLException, ParseException,
			ClassNotFoundException
	{
		return updateSFDCInfo(null, scheduler, spaceID, updatedSfdcInfo, isMigration);
	}
	
	public static SFDCInfo updateSFDCInfo(Scheduler scheduler, String spaceID,
			SFDCInfo updatedSfdcInfo, boolean isMigration, boolean compareJobIds) throws SchedulerException,
			SchedulerBaseException, SQLException, ParseException, ClassNotFoundException
	{
		return updateSFDCInfo(null, scheduler, spaceID, updatedSfdcInfo, isMigration, compareJobIds);
	}
	
	public static SFDCInfo updateSFDCInfo(SchedulerWebServiceResult result, Scheduler scheduler, String spaceID, SFDCInfo updatedSfdcInfo, 
			boolean isMigration)
		throws SchedulerException, SchedulerBaseException, SQLException, ParseException, ClassNotFoundException
	{
		return updateSFDCInfo(result, scheduler, spaceID, updatedSfdcInfo, isMigration, true);
	}
	
	public static SFDCInfo updateSFDCInfo(SchedulerWebServiceResult result, Scheduler scheduler, String spaceID, SFDCInfo updatedSfdcInfo, 
			boolean isMigration, boolean compareJobIds) 
		throws SchedulerException, SchedulerBaseException, SQLException, ParseException, ClassNotFoundException
	{
		// Basically remove the old one and create a new one.
		Connection conn = null;
		try
		{
			conn = SchedulerDBConnection.getInstance().getConnectionPool().getConnection();
			String schema = UtilConstants.SCHEDULER_DB_SCHEMA;
			updatedSfdcInfo.setName(CopyJob.getScheduleName(updatedSfdcInfo.getName()));
			if (SchedulerDatabase.isValidScheduleName(conn, schema, spaceID, updatedSfdcInfo.getId(), updatedSfdcInfo.getName()) || isMigration) {
				if (isValidScheduleTime(conn, schema, updatedSfdcInfo) || isMigration || !compareJobIds) {
					String sfdcInfoId = updatedSfdcInfo.getId();
					SFDCInfo sInfo = SchedulerDatabase.getConnectorInfo(conn, schema, sfdcInfoId);
					if(sInfo == null)
					{
						logger.warn("No Scheduled SFDC Info found for " + sfdcInfoId);
						return null;
					}
	
					// The created user id should never change. Modified by user can change.
					if(updatedSfdcInfo.getUserId() == null)
					{
						updatedSfdcInfo.setUserId(sInfo.getUserId());
					}

					Schedule[] updatedSchedules = updatedSfdcInfo.getJobSchedules();
					boolean changed = !sInfo.equals(updatedSfdcInfo);
					if(changed)
					{
						SchedulerDatabase.updateConnectorInfo(conn, schema, updatedSfdcInfo);
						sInfo = SchedulerDatabase.getConnectorInfo(conn, schema, sfdcInfoId);
					}
		
					List<Schedule> existingSchedules = new ArrayList<Schedule>();
					if(sInfo.getJobSchedules() != null && sInfo.getJobSchedules().length > 0)
					{
						existingSchedules = Arrays.asList(sInfo.getJobSchedules());
					}
		
					// For adding schedules.
					List<Schedule> toBeAdded = new ArrayList<Schedule>();
					
					// For removing	schedules.
					List<Schedule> toBeRemoved = new ArrayList<Schedule>(existingSchedules);
		
					if(updatedSchedules != null && updatedSchedules.length > 0)
					{
						for(Schedule updatedSchedule : updatedSchedules)
						{
							Schedule current = null;
							for(Schedule existingSchedule : existingSchedules)
							{
								if(existingSchedule.getId().equalsIgnoreCase(updatedSchedule.getId()))
								{
									current = existingSchedule;
									break;
								}
							}
		
							if(current == null)
							{
								toBeAdded.add(updatedSchedule);
							}
							else 
							{
								if (current.equals(updatedSchedule))
								{
									toBeRemoved.remove(current);						
								}
								else
								{
									toBeAdded.add(updatedSchedule);
								}
							}					
						}
					}
					
					for(Schedule schedule : toBeRemoved)
					{
						scheduler.unscheduleJob(new TriggerKey(Util.getCustConnectorTriggerName(schedule.getId()), sInfo.getSpaceId()));
						SchedulerDatabase.removeSchedule(conn, schema, schedule.getId());
					}
		
					String jobName = Util.getCustConnectorJobName(sfdcInfoId);
					String groupName = Util.getCustJobGroupNameForSFDC(sInfo.getSpaceId());
					JobDetail jobDetail = scheduler.getJobDetail(new JobKey(jobName,groupName));
					for(Schedule schedule : toBeAdded)
					{
						// New schedule, make sure each new schedule has a UID.
						if(schedule.getJobId() == null || schedule.getJobId().trim().length() == 0)
						{
							schedule.setJobId(sfdcInfoId);
						}
						schedule.setId(UUID.randomUUID().toString());
						SchedulerDatabase.addSchedule(conn, schema, schedule);
						if(!schedule.isEnable())
						{
							continue;
						}
		
						if(jobDetail == null)
						{
							throw new ObjectUnavailableException("JobDetail not found for jobName" + Util.getCustConnectorJobName(sInfo.getId()));					
						}
		
						String timeZoneID = Schedule.getTimeZoneID(schedule.getTimeZone());		
						TriggerBuilder<CronTrigger> triggerBuilder = TriggerBuilder.newTrigger()
						.withIdentity(Util.getCustConnectorTriggerName(schedule.getId()), spaceID)
						.withSchedule(cronSchedule(schedule.getExpression()).inTimeZone(TimeZone.getTimeZone(timeZoneID)))
						.forJob(jobDetail);
		
						Date startDate = Util.getDate(schedule.getStartDate());
						if(startDate != null && startDate.after(Calendar.getInstance().getTime()))
						{	
							triggerBuilder.startAt(startDate);
						}
						
						CronTrigger cronTrigger = triggerBuilder.build();
						scheduler.scheduleJob(cronTrigger);				
					}
	
					// Unschedule all the retry jobs and run now jobs.
					Util.removeAllNonScheduledTriggers(jobDetail.getKey());
					SFDCInfo updated = SchedulerDatabase.getConnectorInfo(conn, schema, sfdcInfoId);
					return updated;
				} else {
					result.setErrorCode(SchedulerBaseException.ERROR_DUPLICATE_SCHEDULE);
					result.setErrorMessage("A schedule already exists with the same scheduled time.");
					return updatedSfdcInfo;
				}
			} else {
				if (result != null) {
					result.setErrorCode(SchedulerBaseException.ERROR_DUPLICATE_JOB_NAME);
					result.setErrorMessage("A schedule already exists with the same schedule name.");
				}
				return updatedSfdcInfo;
			}
		} catch (Exception ex) {
			logger.error("Error updating info : " + ex.toString());
			return updatedSfdcInfo;
		}
		finally
		{
			if(conn != null)
			{
				try
				{
					conn.close();
				}catch(Exception ex)
				{
					logger.warn("Exception in closing connection ",ex);
				}
			}
		}
	}

	public static SFDCInfo getNewCopiedSFDCInfo(SFDCInfo oldSFDCInfo)
	{
		String jobId = UUID.randomUUID().toString();		
		String spaceId = oldSFDCInfo.getSpaceId();
		String userId = oldSFDCInfo.getUserId();
    	String clientId = oldSFDCInfo.getClientId();
    	String loadDate = oldSFDCInfo.getLoadDate();
    	String sandBoxUrl = oldSFDCInfo.getSandBoxUrl();
    	boolean sendEmail = oldSFDCInfo.getSendEmail();
    	String subGroups = oldSFDCInfo.getSubGroups();
    	boolean processAfter = oldSFDCInfo.isProcessAfter();
    	// ideally, this should be false for all the copied sfdc info -- should be set at the calling function stack level
    	boolean runNowMode = oldSFDCInfo.isRunNowMode();
    	String name = oldSFDCInfo.getName();
    	String type = oldSFDCInfo.getType();
    	int engine = oldSFDCInfo.getExtractionEngine();
    	String extractGroups = oldSFDCInfo.getExtractGroups();
    	
    	Schedule[] jobSchedules = oldSFDCInfo.getJobSchedules();
    	List<Schedule> newSchedules = new ArrayList<Schedule>();
    	for(Schedule oldSchedule : jobSchedules)
    	{
    		String scheduleId = UUID.randomUUID().toString();
    		Schedule newSchedule = new Schedule(scheduleId, jobId, oldSchedule.getExpression(), oldSchedule.isEnable(), oldSchedule.getTimeZone());
    		newSchedule.setStartDate(oldSchedule.getStartDate());
    		newSchedules.add(newSchedule);
    	}
    	
    	SFDCInfo newSFDCInfo = new SFDCInfo();
    	newSFDCInfo.setId(jobId);
    	newSFDCInfo.setSpaceId(spaceId);
    	newSFDCInfo.setUserId(userId);
    	newSFDCInfo.setClientId(clientId);
    	newSFDCInfo.setLoadDate(loadDate);
    	newSFDCInfo.setSandBoxUrl(sandBoxUrl);
    	newSFDCInfo.setSendEmail(sendEmail);
    	newSFDCInfo.setSubGroups(subGroups);
    	newSFDCInfo.setProcessAfter(processAfter);
    	newSFDCInfo.setScheduledMode(runNowMode);
    	newSFDCInfo.setName(name);
    	newSFDCInfo.setType(type);
    	newSFDCInfo.setExtractionEngine(engine);
    	newSFDCInfo.setExtractGroups(extractGroups);
    	newSFDCInfo.setJobSchedules((Schedule [])newSchedules.toArray(new Schedule[newSchedules.size()]));
		
		return newSFDCInfo;
	}

	public static SFDCInfo copyChanges(SFDCInfo sfdcInfo1, SFDCInfo sfdcInfo2, boolean enableNewSchedule)
	{
		if(sfdcInfo1 == null)
		{
			Schedule[] sfdcInfo2JobSchedules = sfdcInfo2.getJobSchedules();
			for(Schedule sfdcInfo2Schedule : sfdcInfo2JobSchedules)
			{
				boolean enablesfdcInfo2Schedule = enableNewSchedule ? sfdcInfo2Schedule.isEnable() : false;
				sfdcInfo2Schedule.setEnable(enablesfdcInfo2Schedule);
			}
			return sfdcInfo2;
		}
		SFDCInfo newSFDCInfo = new SFDCInfo();
		
		String sfdcInfoJobId = sfdcInfo2.getId();		
		String spaceId = sfdcInfo2.getSpaceId();
		String userId = sfdcInfo2.getUserId();
		
		String clientId1 = sfdcInfo1.getClientId();
		String clientId2 = sfdcInfo2.getClientId();
		if(clientId2 == null || (clientId1 != null && !CompareUtils.compare(clientId1, clientId2, true)))
		{
			clientId2 = clientId1;
		}
		
		String sandBoxUrl1 = sfdcInfo1.getSandBoxUrl();
		String sandBoxUrl2 = sfdcInfo2.getSandBoxUrl();
		if(sandBoxUrl2 == null || (sandBoxUrl1 != null && !CompareUtils.compare(sandBoxUrl1, sandBoxUrl2, true)))
		{
			sandBoxUrl2 = sandBoxUrl1;
		}
		
		String loadDate1 = sfdcInfo1.getLoadDate();
		String loadDate2 = sfdcInfo2.getLoadDate();	
		if(loadDate2 == null || (loadDate1 != null && !CompareUtils.compare(loadDate1, loadDate2, true)))
		{
			loadDate2 = loadDate1;
		}
		
		String subGroups1 = sfdcInfo1.getSubGroups();
		String subGroups2 = sfdcInfo2.getSubGroups();	
		if(subGroups2 == null || (subGroups1 != null && !CompareUtils.compare(subGroups1, subGroups2, true)))
		{
			subGroups2 = subGroups1;
		}
		
		boolean sendEmail1 = sfdcInfo1.getSendEmail();
		boolean sendEmail2 = sfdcInfo2.getSendEmail();
		if(sendEmail1 != sendEmail2)
		{
			sendEmail2 = sendEmail1;
		}
		
		boolean processAfter1 = sfdcInfo1.isProcessAfter();
		boolean processAfter2 = sfdcInfo2.isProcessAfter();
		if(processAfter1 != processAfter2)
		{
			processAfter2 = processAfter1;
		}
		
		boolean runNowMode1 = sfdcInfo1.isRunNowMode();
		boolean runNowMode2 = sfdcInfo2.isRunNowMode();
		if(runNowMode1 != runNowMode2)
		{
			runNowMode2 = runNowMode1;
		}		
		
		String name1 = sfdcInfo1.getName();
		String name2 = sfdcInfo2.getName();
		if(name2 == null || (name1 != null && !CompareUtils.compare(name1, name2, true)))
		{
			name2 = name1;
		}
		
		Schedule[] jobSchedules1 = sfdcInfo1.getJobSchedules();
		Schedule[] jobSchedules2 = sfdcInfo2.getJobSchedules();
		if(jobSchedules1 != null && jobSchedules1.length > 0)
		{
			// just copy everything from 1 to 2
			List<Schedule> newSchedules = new ArrayList<Schedule>();
	    	for(Schedule schedule1 : jobSchedules1)
	    	{
	    		String scheduleId = UUID.randomUUID().toString();
	    		boolean enableSchedule = enableNewSchedule ? schedule1.isEnable() : false;
	    		Schedule newSchedule = new Schedule(scheduleId, sfdcInfoJobId, schedule1.getExpression(), enableSchedule, schedule1.getTimeZone());
	    		newSchedule.setStartDate(schedule1.getStartDate());
	    		newSchedules.add(newSchedule);
	    	}
	    	jobSchedules2 = (Schedule [])newSchedules.toArray(new Schedule[newSchedules.size()]);
		}
		
		newSFDCInfo.setId(sfdcInfoJobId);
		newSFDCInfo.setSpaceId(spaceId);
		newSFDCInfo.setUserId(userId);
		newSFDCInfo.setClientId(clientId2);
		newSFDCInfo.setLoadDate(loadDate2);
		newSFDCInfo.setProcessAfter(processAfter2);
		newSFDCInfo.setSandBoxUrl(sandBoxUrl2);
		newSFDCInfo.setScheduledMode(runNowMode2);
		newSFDCInfo.setSendEmail(sendEmail2);
		newSFDCInfo.setSubGroups(subGroups2);
		newSFDCInfo.setJobSchedules(jobSchedules2);
		newSFDCInfo.setName(name2);
		newSFDCInfo.setType(sfdcInfo2.getType());
		newSFDCInfo.setExtractionEngine(sfdcInfo2.getExtractionEngine());
    	
		return newSFDCInfo;
	}
	
	public static boolean isValidScheduleTime(Connection conn, String schema, SFDCInfo connectorInfo) throws SQLException {
		boolean isValidScheduleTime = true;
		try
		{
			String spaceID = connectorInfo.getSpaceId();
			String jobID = connectorInfo.getId();
			Map<String, String> cronExpressions = SchedulerDatabase.getAllSchedules(conn, schema, spaceID);
			Schedule[] schedules = connectorInfo.getJobSchedules();
			String scheduleCronExp = "";
			String existingCronExp = "";
			Map<Integer, String> scheduleCronParts = null;
			Map<Integer, String> existingCronParts = null;
			String existingSecs = "";
			String existingMins = "";
			String existingHours = "";
			String existingDays = "";
			String scheduleSecs = "";
			String scheduleMins = "";
			String scheduleHours = "";
			String scheduleDays = "";
			boolean compareSchedules;
			if (schedules != null)
			{
				for (Schedule schedule : schedules) {
					scheduleCronExp = schedule.getExpression();
					for (String cronJobID : cronExpressions.keySet()) {
						compareSchedules = false;
						if (!cronJobID.equalsIgnoreCase(jobID)) {	
							existingCronExp = cronExpressions.get(cronJobID);
													
							// If exact same schedule, it's invalid.
							if (scheduleCronExp.equals(existingCronExp)) {
								isValidScheduleTime = false;
								break;
							}
							
							// Retrieve cron expression parts.
							scheduleCronParts = Util.parseCronExpression(scheduleCronExp);
							existingCronParts = Util.parseCronExpression(existingCronExp);
	
							// Check for duplicates.
							if (isDailySchedule(scheduleCronParts)) {
								compareSchedules = true;
							} else if (isWeeklySchedule(scheduleCronParts)) {
								if (isDailySchedule(existingCronParts) || isWeeklySchedule(existingCronParts)) {
									if (isWeeklySchedule(existingCronParts)) {
										scheduleDays = scheduleCronParts.get(UtilConstants.INDEX_CRON_DAYOFWEEK);
										existingDays = existingCronParts.get(UtilConstants.INDEX_CRON_DAYOFWEEK);
										String[] scheduleDayParts = scheduleDays.split(",");
										String[] existingDayParts = existingDays.split(",");
										for (String scheduleDay : scheduleDayParts) {
											for (String existingDay : existingDayParts) {
												if (scheduleDay.equals(existingDay)) {
													compareSchedules = true;
												}
											}
										}
									} else {
										compareSchedules = true;
									}
								}
							} else if (isMonthlySchedule(scheduleCronParts)) {
								if (isDailySchedule(existingCronParts)) {
									compareSchedules = true;
								}
							}
							
							if (compareSchedules) {
								existingSecs = existingCronParts.get(UtilConstants.INDEX_CRON_SECONDS);
								existingMins = existingCronParts.get(UtilConstants.INDEX_CRON_MINUTES);
								existingHours = existingCronParts.get(UtilConstants.INDEX_CRON_HOURS);
								scheduleSecs = scheduleCronParts.get(UtilConstants.INDEX_CRON_SECONDS);
								scheduleMins = scheduleCronParts.get(UtilConstants.INDEX_CRON_MINUTES);
								scheduleHours = scheduleCronParts.get(UtilConstants.INDEX_CRON_HOURS);
								if (scheduleSecs.equals(existingSecs) && scheduleMins.equals(existingMins) && scheduleHours.equals(existingHours)) {
									isValidScheduleTime = false;
									break;
								}
							}
						}
					}
				}
			}
		} catch (Exception ex) {
			logger.warn("Exception in closing connection: ", ex);
		}
		return isValidScheduleTime;
	}
	
	private static boolean isDailySchedule(Map<Integer, String> cronParts) {
		boolean isDailySchedule = false;
		if (cronParts.get(UtilConstants.INDEX_CRON_DAYOFMONTH).equals("?")) {
			if (cronParts.get(UtilConstants.INDEX_CRON_MONTH).equals("*") && 
					cronParts.get(UtilConstants.INDEX_CRON_DAYOFWEEK).equals("*") && 
					cronParts.get(UtilConstants.INDEX_CRON_YEAR).equals("*")) {
				isDailySchedule = true;
			}
		}
		return isDailySchedule;
	}
	
	private static boolean isWeeklySchedule(Map<Integer, String> cronParts) {
		boolean isWeeklySchedule = false;
		if (cronParts.get(UtilConstants.INDEX_CRON_DAYOFMONTH).equals("?")) {
			if (cronParts.get(UtilConstants.INDEX_CRON_MONTH).equals("*") && 
					!cronParts.get(UtilConstants.INDEX_CRON_DAYOFWEEK).equals("*") &&
					!cronParts.get(UtilConstants.INDEX_CRON_DAYOFWEEK).equals("?") &&
					cronParts.get(UtilConstants.INDEX_CRON_YEAR).equals("*")) {
				isWeeklySchedule = true;
			}
		}
		return isWeeklySchedule;
	}
	
	private static boolean isMonthlySchedule(Map<Integer, String> cronParts) {
		boolean isMonthlySchedule = false;
		if (!cronParts.get(UtilConstants.INDEX_CRON_DAYOFMONTH).equals("?")) {
			isMonthlySchedule = true;
		}
		return isMonthlySchedule;
	}
}
