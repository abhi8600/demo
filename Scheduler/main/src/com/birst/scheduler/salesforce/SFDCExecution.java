package com.birst.scheduler.salesforce;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.UUID;

import org.apache.axiom.om.OMElement;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.quartz.SchedulerException;

import com.birst.scheduler.SchedulerServerContext;
import com.birst.scheduler.exceptions.ConcurrentSpaceOperationException;
import com.birst.scheduler.exceptions.SchedulerBaseException;
import com.birst.scheduler.process.ProcessJobOperation;
import com.birst.scheduler.shared.ExecutionLogic;
import com.birst.scheduler.shared.JobStatus;
import com.birst.scheduler.shared.SpaceStatus;
import com.birst.scheduler.utils.CommandUtil;
import com.birst.scheduler.utils.ExceptionCodes;
import com.birst.scheduler.utils.SchedulerDBConnection;
import com.birst.scheduler.utils.SchedulerDatabase;
import com.birst.scheduler.utils.StatusUtils;
import com.birst.scheduler.utils.Util;
import com.birst.scheduler.utils.UtilConstants;
import com.birst.scheduler.utils.XmlUtils;
import com.birst.scheduler.utils.StatusUtils.Step;
import com.birst.scheduler.utils.StatusUtils.StepStatus;
import com.birst.scheduler.utils.UtilConstants.RunningJobStatus;
import com.birst.scheduler.webServices.connector.ConnectorServices;
import com.birst.scheduler.webServices.connector.ConnectorServices.ConnectorInfo;

public class SFDCExecution implements ExecutionLogic
{
	private static final Logger logger = Logger.getLogger(SFDCExecution.class);

	String spaceID;
	String sfdcScheduleStepId;
	String runningJobId;
	boolean runNowOperation;
	
	JobStatus executionStatus = new JobStatus();
	Map<String,Process> runningCommandProcesses = new HashMap<String,Process>();
	String extractInfoId;
	
	private static final int SUCCESS = 0;
	private static final int FAILED = 1;
	private static final int KILLED = 2;
	
	public SFDCExecution()
	{	
	}
	
	public SFDCExecution(String spaceID, String runningJobId, String sfdcScheduleStepId, boolean runNowOperation)
	{
		this.spaceID = spaceID;
		this.runningJobId = runningJobId;
		this.sfdcScheduleStepId = sfdcScheduleStepId;
		this.runNowOperation = runNowOperation;
	}
	
	
	@Override
	public void executeOperation() throws Exception
	{
		Connection conn = null;
		SFDCInfo connectorInfo = null;
		int result = 0;
		String spaceDirectory = null;
		try
		{	
			logger.debug("SFDC Job starting for space " + spaceID);
			SpaceStatus anyConcurrentSpaceStatus = StatusUtils.getActiveSpaceStatus(spaceID, false);
			if(anyConcurrentSpaceStatus != null && (sfdcScheduleStepId == null || !anyConcurrentSpaceStatus.getId().equalsIgnoreCase(sfdcScheduleStepId)))
			{
				logger.error("Another job is running for this space : " + anyConcurrentSpaceStatus.toString());
				executionStatus.markTheJobStatus(RunningJobStatus.FAILED, "Concurrent job running", false, StatusUtils.getConcurrentExitCodeMessage(anyConcurrentSpaceStatus));
				throw new ConcurrentSpaceOperationException("Another job is running for this space : " + anyConcurrentSpaceStatus.toString());
			}
			
			runningJobId = Util.getLowerCaseString(runningJobId);
			conn = SchedulerDBConnection.getInstance().getConnectionPool().getConnection();
			String schema = UtilConstants.SCHEDULER_DB_SCHEMA;
			
			if(sfdcScheduleStepId == null)
			{	
				SpaceStatus spaceStatus = new SpaceStatus(UUID.randomUUID().toString(), spaceID, runningJobId, -1, null, Step.ScheduledExtract, StepStatus.RUNNING ); 
				SchedulerDatabase.addSpaceStatus(conn, schema, spaceStatus);
				sfdcScheduleStepId = spaceStatus.getId();
			}
			
			// this is the id of the schedule step which is put in by the caller during the time of 
			// adding process schedule
			connectorInfo = SchedulerDatabase.getConnectorInfo(conn, schema, runningJobId);
			if(connectorInfo == null)
			{
				throw new Exception("Connector Job Info not found : id : " + runningJobId);
			}
			
			Map<String,String> propertiesMap = Util.getSpaceInfoMap(spaceID);
			spaceDirectory = propertiesMap != null ? propertiesMap.get(UtilConstants.KEY_SPACE_DIRECTORY) : null;
			if(spaceDirectory == null)
			{
				executionStatus.markTheJobStatus(RunningJobStatus.FAILED, "Space Directory not found", false);
				logger.info("Unable to extract. Space Directory not found : " + spaceID);
				boolean isSpaceDeleted = Util.isSpaceDeleted(propertiesMap);
            	if(isSpaceDeleted)
            	{
            		logger.info("Scheduling a delet space job for " + spaceID);
            		Util.scheduleDeletedSpaceJob(spaceID);
            	}
				return;
			}
			
			// if it is NOT a run now opearation
			// and is a scheduled run, make sure that no other extraction is running
			if(!runNowOperation)
			{
				Util.validateNoExtractFilePresent(spaceDirectory);
			}
			Util.validateNoLoadLockFilePresent(spaceDirectory);
			// delete all the old entries
			SchedulerDatabase.deleteAllPreviousSpaceStatus(conn, schema, spaceID, runningJobId, sfdcScheduleStepId);
			// delete all the old entries for run now modes also
			SchedulerDatabase.deleteAllPreviousRunNowConnectors(conn, schema, spaceID, runningJobId);
			createExtractInfoFile(spaceDirectory, connectorInfo.getConnectorType());
			clearOutOldStatusFiles(spaceDirectory, connectorInfo.getConnectorType());
			result = extractAndProcess(spaceDirectory, connectorInfo);
			executionStatus.markTheJobStatus(RunningJobStatus.COMPLETE, null);
		}
		catch(Exception ex)
		{
			logger.error("Exception while running SFDCExecution" , ex);
			executionStatus.markTheJobStatus(RunningJobStatus.FAILED, "Exception while running SFDCJob", false);
			result = FAILED;
			throw new Exception("Error while running SFDCJob", ex.getCause());	
		}
		finally
		{	
			try
			{	
				writeOutputFile(spaceDirectory, result, connectorInfo);
				if(sfdcScheduleStepId != null)
				{
					StepStatus scheduleStepStatus = StepStatus.SUCCESS;
					if(executionStatus.getJobStatus().getStatus() != RunningJobStatus.COMPLETE.getStatus())
					{
						scheduleStepStatus = StepStatus.FAILED;
					}
					StatusUtils.updateSpaceStatus(sfdcScheduleStepId, scheduleStepStatus.getStatus(), executionStatus.getExitErrorCodeMsg(), true);
				}
			}catch(Exception ex)
			{
				logger.error("Exception while updating scheduled status for uid : " + sfdcScheduleStepId, ex);
			}
		}
	}	

	protected int extractAndProcess(String spaceDirectory, SFDCInfo connectorInfo) throws SchedulerBaseException, ClassNotFoundException, SQLException, SchedulerException, IOException
	{	
		try
		{
			int result = -1;
			String extractionStepUid = UUID.randomUUID().toString();
			boolean useNewConnectorFramework = SchedulerServerContext.getUseNewConnectorFramework();
			if (useNewConnectorFramework) {
				Util.createConnectorExtractFileInDirectory(spaceDirectory, connectorInfo.getType());
				Util.createConnectorSpecificExtractFile(connectorInfo.getType(), spaceDirectory);
				if (runExtraction(spaceDirectory, extractionStepUid, connectorInfo)) {
					String uploadAfterExtractionStepId = UUID.randomUUID().toString();
					String fileListSFDCSources = CommandUtil.getSFDCExtractionListFilePath(spaceDirectory, extractionStepUid);
					boolean uploadSuccessFull = uploaderAfterExtraction(spaceDirectory, uploadAfterExtractionStepId, fileListSFDCSources, connectorInfo);
					Util.deleteConnectorExtractFileFromDirectory(spaceDirectory, connectorInfo.getType());
					Util.deleteConnectorSpecificFile(connectorInfo.getType(), spaceDirectory);
					if (uploadSuccessFull) {
						result = SUCCESS;
						if (connectorInfo.processAfter) {	
							String outputFilePath = CommandUtil.getMessageFilePath(spaceDirectory, uploadAfterExtractionStepId);
							processAfterExtraction(outputFilePath, connectorInfo);
						}
						else {
							logger.debug("Only extraction required for connector : id : " + connectorInfo.toString());
							sendExtractionEmailPostLoad(spaceDirectory, connectorInfo, "success");
						}
					} else {
						result = FAILED;
						sendExtractionEmailPostLoad(spaceDirectory, connectorInfo, "failure");
					}
				} else {
					result = FAILED;
					if(Util.killedExtractFileExists(extractionStepUid, spaceDirectory)){
						result = KILLED;
					}
					logger.warn("Extraction did not seem to run successfully. Skipping the post extract process");
					sendExtractionEmailPostLoad(spaceDirectory, connectorInfo, "failure");
					StatusUtils.updateSpaceStatus(extractionStepUid, ExceptionCodes.EXIT_CODE_EXTRACTION_SUCCESS_FILE);
				}
			} else {
				Util.createSFDCExtractFileInDirectory(spaceDirectory);
				Util.createConnectorSpecificExtractFile(connectorInfo.getType(), spaceDirectory);
				String engineCommandStepId = UUID.randomUUID().toString();
				if(getConnectorEngineCommand(spaceDirectory, engineCommandStepId, connectorInfo))
				{
					String messageFilePath = CommandUtil.getMessageFilePath(spaceDirectory, engineCommandStepId);
					// check if the success-<uid>.txt exists. This ensures that the bat files 
					// finished the entire process	
					if(runExtraction(spaceDirectory, extractionStepUid, messageFilePath, connectorInfo)) {
						// extraction finished. See if it found success file -- spit out on successful extraction of all objects
						if(Util.successExtractFileExists(engineCommandStepId, spaceDirectory))
						{
							// Get the output of all the files extracted through the list of 
							// filenames
							String uploadAfterExtractionStepId = UUID.randomUUID().toString();
							String fileListSFDCSources = CommandUtil.getSFDCExtractionListFilePath(spaceDirectory, extractionStepUid);
							boolean uploadSuccessFull = uploaderAfterExtraction(spaceDirectory, uploadAfterExtractionStepId, fileListSFDCSources, connectorInfo);
							Util.deleteSFDCExtractFileFromDirectory(spaceDirectory);
							Util.deleteConnectorSpecificFile(connectorInfo.getType(), spaceDirectory);
							if(uploadSuccessFull)
							{	
								result = SUCCESS;
								if(connectorInfo.processAfter)
								{	
									String outputFilePath = CommandUtil.getMessageFilePath(spaceDirectory, uploadAfterExtractionStepId);
									processAfterExtraction(outputFilePath, connectorInfo);
								}
								else
								{
									logger.debug("Only extraction required for connector : id : " + connectorInfo.toString());
									sendExtractionEmailPostLoad(spaceDirectory, connectorInfo, "success");
								}
							}
							else
							{
								result = FAILED;
								sendExtractionEmailPostLoad(spaceDirectory, connectorInfo, "failure");
							}
						}
						else
						{
							result = FAILED; // failed
							if(Util.killedExtractFileExists(engineCommandStepId, spaceDirectory)){
								result = KILLED;
							}
							
							logger.error("Extraction finished but no success file present. Some objects extraction failed");
							sendExtractionEmailPostLoad(spaceDirectory, connectorInfo, "failure");
							StatusUtils.updateSpaceStatus(extractionStepUid, ExceptionCodes.EXIT_CODE_EXTRACTION_SUCCESS_FILE);
						}
					}
					else
					{	
						result = FAILED; // failed
						if(Util.killedExtractFileExists(engineCommandStepId, spaceDirectory)){
							result = KILLED;
						}
						logger.warn("Extraction did not seem to run successfully. Skipping the post extract process");
						sendExtractionEmailPostLoad(spaceDirectory, connectorInfo, "failure");
					}
				}
			}
			return result;
		}
		finally
		{
			Util.deleteSFDCExtractFileFromDirectory(spaceDirectory);
			Util.deleteConnectorSpecificFile(connectorInfo.getType(), spaceDirectory);
		}
	}
	
	private void sendExtractionEmailPostLoad(String spaceDirectory, SFDCInfo connectorInfo, String result) 
		throws SchedulerBaseException, SQLException, ClassNotFoundException
	{
		if (connectorInfo.sendEmail) {
			sendExtractionEmailPostLoad(connectorInfo.getSpaceId(), connectorInfo.getUserId(), connectorInfo.getConnectorType(), spaceDirectory, connectorInfo.processAfter, result, connectorInfo.getExtractGroups());
		}
	}
	
	private boolean sendExtractionEmailPostLoad(String spaceID, String userID, String type, String spaceDirectory, boolean extractOnly, String result, String extractGroups) 
		throws ClassNotFoundException, SQLException, SchedulerBaseException 
	{
		int exitValue = -1;
		String uid = null;
		try
		{	
			uid = UUID.randomUUID().toString();
			StatusUtils.addSpaceStatus(uid, spaceID, runningJobId, 0, null, StatusUtils.Step.SendEmail, StatusUtils.StepStatus.RUNNING);
			String logFile = CommandUtil.getProcessCommandLogFilePath(spaceDirectory);
			String cmd = CommandUtil.getEmailForExtractionCommand(logFile, uid, spaceID, userID, type, Util.useSSLForEmail(), extractOnly, result, extractGroups);
			exitValue = CommandUtil.runCommand(CommandUtil.CMD_EXTRACTION_EMAIL, cmd, null, runningCommandProcesses);
		}
		catch(Exception ex)
		{
			logger.error("Exception in post load operation: ", ex);
			exitValue = ExceptionCodes.INTERNAL_ERROR;
		}
		finally
		{
			StatusUtils.updateSpaceStatus(uid, exitValue);
		}
		
		return CommandUtil.isSuccessfulExit(exitValue);
	}
	
	protected boolean getConnectorEngineCommand(String spaceDirectory, String uid, SFDCInfo connectorInfo) throws SchedulerBaseException, ClassNotFoundException, SQLException
	{
		int exitValue = -1;
		try
		{
			StatusUtils.addSpaceStatus(uid, connectorInfo.getSpaceId(), runningJobId, -1, null, StatusUtils.Step.SFDCEngineCmd, StatusUtils.StepStatus.RUNNING);
			String logFile = CommandUtil.getProcessCommandLogFilePath(spaceDirectory);
			String cmd = CommandUtil.getSFDCEngineCommand(logFile, uid, connectorInfo.getSpaceId(), connectorInfo.getUserId(), 
					connectorInfo.getClientId(), connectorInfo.getSandBoxUrl(), connectorInfo.getSendEmail(),
					connectorInfo.getConnectorType(), connectorInfo.getExtractionEngine());
			exitValue = CommandUtil.runCommand(CommandUtil.CMD_GET_SFDC_ENGINE, cmd, null, runningCommandProcesses);
		}
		catch(Exception ex)
		{
			logger.error("Error in retreiving engine command for connector ", ex);
			exitValue = ExceptionCodes.INTERNAL_ERROR;
		}
		finally
		{
			StatusUtils.updateSpaceStatus(uid, exitValue);
		}
		
		return CommandUtil.isSuccessfulExit(exitValue);
	}
	
	protected boolean getTransactionObjectList(String spaceDirectory, String uid, SFDCInfo connectorInfo) throws SchedulerBaseException, ClassNotFoundException, SQLException
	{
		int exitValue = -1;
		try
		{
			String logFile = CommandUtil.getProcessCommandLogFilePath(spaceDirectory);
			String cmd = CommandUtil.getTransactionObject(logFile, uid, connectorInfo.getSpaceId(), connectorInfo.getConnectorType());
			exitValue = CommandUtil.runCommand(CommandUtil.CMD_TRANSACTIONAL_OBJECT, cmd, null, runningCommandProcesses);
		}
		catch(Exception ex)
		{
			logger.error("Error in retreiving transaction object list for connector ", ex);
			exitValue = ExceptionCodes.INTERNAL_ERROR;
		}
		finally
		{
			StatusUtils.updateSpaceStatus(uid, exitValue);
		}
		
		return CommandUtil.isSuccessfulExit(exitValue);
	}
	
	protected boolean getConnectorVariables(String spaceDirectory, String uid, SFDCInfo connectorInfo, String connectorConnectionString) throws SchedulerBaseException, ClassNotFoundException, SQLException
	{
		int exitValue = -1;
		try
		{
			String logFile = CommandUtil.getProcessCommandLogFilePath(spaceDirectory);
			String cmd = CommandUtil.getConnectorVariables(logFile, uid, connectorInfo.getSpaceId(), connectorInfo.getConnectorType(), connectorConnectionString, connectorInfo.getExtractGroups());
			exitValue = CommandUtil.runCommand(CommandUtil.CMD_CONNECTOR_VARIABLES, cmd, null, runningCommandProcesses);
		}
		catch(Exception ex)
		{
			logger.error("Error in retreiving variables for connector ", ex);
			exitValue = ExceptionCodes.INTERNAL_ERROR;
		}
		finally
		{
			StatusUtils.updateSpaceStatus(uid, exitValue);
		}
		
		return CommandUtil.isSuccessfulExit(exitValue);
	}
	
	private String getDefaultVersionForConnectionString(List<ConnectorServices.Connector> connectors, String connectorName) 
	{
		for (ConnectorServices.Connector conn : connectors)
		{
			if (connectorName.equals(conn.getConnectorName()) && conn.isIsDefaultAPIVersion())
			{
				return conn.getConnectionString();			
			}
		}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	protected boolean runExtraction(String spaceDirectory, String stepUid, SFDCInfo connectorInfo) 
		throws SchedulerBaseException, ClassNotFoundException, SQLException
	{
		int exitValue = -1;
		String connectorType = null;
		String connectorInfoType = connectorInfo.getConnectorType();
		String spaceID = connectorInfo.getSpaceId();
		try
		{	
			// Create lock files.
			StatusUtils.addSpaceStatus(stepUid, connectorInfo.getSpaceId(), runningJobId, -1, null, StatusUtils.Step.SFDCExtract, StatusUtils.StepStatus.RUNNING);
			StatusUtils.createCheckInFile(spaceDirectory + File.separator + UtilConstants.CHECK_IN_EXTRACT_FILE);
			
			// Get connector url.
			ConnectorServices connectorService = new ConnectorServices();
			String currentAPIVersionInConfig = connectorService.findConnectorAPIVersionInConfig(connectorInfoType, spaceDirectory);
			List<ConnectorServices.Connector> connectors = connectorService.getConnectorsList(connectorInfo.getUserId(), connectorInfo.getSpaceId(), spaceDirectory);
			if (connectors == null)
			{
				throw new Exception("Could not get list of connectors");
			}
			String connectorConnectionString = currentAPIVersionInConfig;
			boolean selectedVersionIsValid = false;
			if (currentAPIVersionInConfig != null && !currentAPIVersionInConfig.isEmpty())
			{
				for (ConnectorServices.Connector conn : connectors)
				{
					if (currentAPIVersionInConfig.equals(conn.getConnectionString()))
					{
						selectedVersionIsValid = true;
						break;
					}
				}
				if (!selectedVersionIsValid)
				{
					//use default version
					String connectorName = ConnectorServices.getConnectorNameFromConnectorConnectionString(currentAPIVersionInConfig);
					connectorConnectionString= getDefaultVersionForConnectionString(connectors, connectorName);
					logger.debug("CurrentAPIVersionInConfig:" + currentAPIVersionInConfig + " is invalid, using default version:" + connectorConnectionString);					
				}
			}
			else
			{
				//use default version
				for (ConnectorServices.Connector conn : connectors)
				{
					String connectorName = conn.getConnectionString().substring("connector:birst://".length(), conn.getConnectionString().lastIndexOf(":")).toLowerCase();
					if (connectorName.equals(connectorInfoType) && conn.isIsDefaultAPIVersion())
					{
						connectorConnectionString = conn.getConnectionString();
						break;
					}
					
				}				
			}
			
			if (connectorConnectionString == null) {
				throw new Exception("Could not find connector type " + connectorInfo.getConnectorType());
			}
			
			// Get connector url info.
			ConnectorInfo connectorURLInfo = connectorService.getConnectorServletURL(connectorInfo.getUserId(), connectorInfo.getSpaceId(), spaceDirectory, connectorConnectionString);
			
			// Generate variables list.
			if (!getConnectorVariables(spaceDirectory, stepUid, connectorInfo, connectorConnectionString)) {
				throw new Exception("AcornExecute failed trying to get variables.");
			}
			
			// Build credentials xml.
			String credentialsXml = null;
			credentialsXml = ConnectorServices.getCredentialsXml(connectorInfo, connectorConnectionString, spaceDirectory);			

			// Generate transactional object list.
			if (!getTransactionObjectList(spaceDirectory, stepUid, connectorInfo)) {
				throw new Exception("AcornExecute failed trying to get transactional objects.");
			}
			
			// Begin the extract.
			boolean success = connectorService.startExtract(connectorInfo.getUserId(), spaceID, spaceDirectory, connectorURLInfo, 
					credentialsXml, ConnectorServices.getIncrementalXml(connectorConnectionString, spaceDirectory), 
					stepUid, String.valueOf(ConnectorServices.THREAD_CNT), ConnectorServices.getVariables(spaceDirectory, connectorConnectionString),
					ConnectorServices.getExtractGroupsXML(connectorInfo.getExtractGroups()));
			
			// Check for completion.
			if (success) {
				while (!Util.successExtractFileExists(stepUid, spaceDirectory) && 
						!Util.failedExtractFileExists(stepUid, spaceDirectory) && 
						!Util.killedExtractFileExists(stepUid, spaceDirectory)) {
					Thread.sleep(ConnectorServices.SLEEP);
				}
				if (Util.successExtractFileExists(stepUid, spaceDirectory)) {
					exitValue = 0;
				} else if (Util.failedExtractFileExists(stepUid, spaceDirectory) || Util.killedExtractFileExists(stepUid, spaceDirectory)) {
					exitValue = -1;
				}
			}
		}
		catch(Exception ex)
		{
			logger.error("Exception during connector extraction ", ex);
			exitValue = ExceptionCodes.INTERNAL_ERROR;
		}
		finally
		{
			StatusUtils.updateSpaceStatus(stepUid, exitValue);
		}
		
		return CommandUtil.isSuccessfulExit(exitValue);
	}
	
	protected boolean runExtraction(String spaceDirectory, String stepUid, String messageFilePath, 
			SFDCInfo connectorInfo) throws SchedulerBaseException, ClassNotFoundException, SQLException
	{
		int exitValue = -1;
		try
		{	
			StatusUtils.addSpaceStatus(stepUid, connectorInfo.getSpaceId(), runningJobId, -1, null, StatusUtils.Step.SFDCExtract, StatusUtils.StepStatus.RUNNING);
			StatusUtils.createCheckInFile(spaceDirectory + File.separator + UtilConstants.CHECK_IN_EXTRACT_FILE);
			// get the load number from the returned props
			//String messageFilePath = CommandUtil.getMessageFilePath(spaceDir, uid);
			Properties props = CommandUtil.getProps(messageFilePath);
			if(props != null)
			{
				String engineCommand = props.getProperty(CommandUtil.KEY_ENGINE_COMMAND);
				String args = props.getProperty(CommandUtil.KEY_ENGINE_ARGS);

				engineCommand = engineCommand.replace("//", File.separator);
				args = args.replace("//", File.separator);
				String cmd = CommandUtil.getProcessCommand(engineCommand, args);
				File file = new File(engineCommand);
				if(file.exists())
				{
					String workingDirectory = file.getParent();
					exitValue = CommandUtil.runCommand(CommandUtil.CMD_EXTRACT, cmd, workingDirectory, runningCommandProcesses);
				}
			}
		}
		catch(Exception ex)
		{
			logger.error("Exception during load command ", ex);
			exitValue = ExceptionCodes.INTERNAL_ERROR;
		}
		finally
		{
			StatusUtils.updateSpaceStatus(stepUid, exitValue);
		}
		
		return CommandUtil.isSuccessfulExit(exitValue);
	}
	
	protected boolean uploaderAfterExtraction(String spaceDirectory, String uploaderStepId, String fileListPath, SFDCInfo connectorInfo) throws SchedulerBaseException, ClassNotFoundException, SQLException
	{
		int exitValue = -1;
		try
		{
			StatusUtils.addSpaceStatus(uploaderStepId, connectorInfo.getSpaceId(), runningJobId, -1, null, StatusUtils.Step.SFDCPostExtract, StatusUtils.StepStatus.RUNNING);
			String logFile = CommandUtil.getProcessCommandLogFilePath(spaceDirectory);
			String cmd = CommandUtil.getPostSFDCExtractCommand(logFile, uploaderStepId, 
					connectorInfo.getSpaceId(), connectorInfo.getUserId(), fileListPath, connectorInfo.getConnectorType(), connectorInfo.getExtractGroups());
			exitValue = CommandUtil.runCommand(CommandUtil.CMD_POST_SFDC_EXTRACT, cmd, null, runningCommandProcesses);			
		}
		catch(Exception ex)
		{
			logger.error("Error in building before Load ", ex);
			exitValue = ExceptionCodes.INTERNAL_ERROR;
		}
		finally
		{
			StatusUtils.updateSpaceStatus(uploaderStepId, exitValue);
		}
		
		return CommandUtil.isSuccessfulExit(exitValue);
	}
	
	protected void processAfterExtraction(String messageFilePath, SFDCInfo connectorInfo) throws SQLException, SchedulerBaseException, ClassNotFoundException, SchedulerException, IOException
	{
		Properties props = CommandUtil.getProps(messageFilePath);
		if(props != null)
		{
			String loadIdString = props.getProperty(CommandUtil.KEY_LOAD_NUMBER);
			int loadId = Integer.parseInt(loadIdString) + 1; // increment the loadID to get the next available one
			String loadGroup = props.getProperty(CommandUtil.KEY_LOAD_GROUP);
			String loadDate = connectorInfo.getLoadDate();
			if(loadDate == null)
			{
				SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/dd/yyyy");
				// Note this is just to pass the date to Acorn. This date will be reformatted 
				// depending on the data type on the Acorn side. So no need to do anything database specific here
				// We should not split the functionality at different places and should have only one point of failure.
				Date lDate = Calendar.getInstance().getTime();
				loadDate = simpleDateFormat.format(lDate);
			}
			ProcessJobOperation.addProcessJob(connectorInfo.getSpaceId(), connectorInfo.getUserId(), loadId, loadDate,
					loadGroup,connectorInfo.getSubGroups(), sfdcScheduleStepId, false, connectorInfo.getSendEmail(), "Connector");
		}
		else
		{
			logger.error("Unable to add processing after extraction. No params found in " + messageFilePath);
		}		
	}
	
	private void writeOutputFile(String spaceDirectory , int result, SFDCInfo connectorInfo){
		if(spaceDirectory == null || spaceDirectory.trim().length() == 0){
			return;
		}
		if(result == SUCCESS || result == FAILED || result == KILLED)
		{
			try{
				String connectorName = connectorInfo.getConnectorType();
				connectorName = connectorName == null || connectorName.trim().length() == 0 ? "sfdc" : connectorName;
				String fileNamePart = result == SUCCESS ? "success" : (result == KILLED ? "killed" : "failed");  
				String connectorsDirectory = Util.getConnectorsDirectoryName(spaceDirectory);
				File dir = new File(connectorsDirectory);
				if(dir.exists()){
					String outputFileName = Util.getConnectorOutputStatusFileName(spaceDirectory, connectorName, fileNamePart, extractInfoId);
					File file = new File(outputFileName);
					if(!file.exists()){
						if(file.createNewFile());
					}
				}
			}catch(Exception ex){
				logger.warn("Exception while creating output connector status file : spaceDirectory" + spaceDirectory);
			}
		}
	}
	
	private void clearOutOldStatusFiles(String spaceDirectory, String connectorName){
		try{
		if(this.extractInfoId != null){
			 String connectorsDirectory = Util.getConnectorsDirectoryName(spaceDirectory);
	            File dir = new File(connectorsDirectory);
	            if(dir.exists()){
	            	File[] files = dir.listFiles();
	            	if(files != null && files.length > 0){
	            		for(File file : files){
	            			String name = file.getName();
	            			if(name.indexOf(connectorName + "Status") >= 0 && !name.endsWith(extractInfoId + ".txt")){
	            				file.delete();
	            			}
	            		}
	            	}
	            	if(dir.mkdir()){
	            		logger.error("Cannot create directory " + connectorsDirectory);
	            		return;
	            	}
	            }
		}
		}catch(Exception ex){
			logger.warn("Error while cleaning out old status files " + spaceDirectory);
		}
	}
	
	private  void createExtractInfoFile(String spaceDirectory, String connectorName) throws Exception
    {
		FileInputStream in = null;
		FileOutputStream out = null;
        try
        {
        	this.extractInfoId = UUID.randomUUID().toString();
            String connectorsDirectory = Util.getConnectorsDirectoryName(spaceDirectory);
            File dir = new File(connectorsDirectory);
            if(!dir.exists()){
            	if(dir.mkdir()){
            		logger.error("Cannot create directory " + connectorsDirectory);
            		return;
            	}
            }
            
            Properties props = new Properties();
            File extractInfoFile = new File(Util.getExtractInfoName(spaceDirectory));
            if(!extractInfoFile.exists()){
            	extractInfoFile.createNewFile();
            }
            else{
            	in = new FileInputStream(extractInfoFile);
            	props.load(in);
            	in.close();
            }
            props.setProperty(connectorName, extractInfoId);
            out = new FileOutputStream(extractInfoFile);
            props.store(out, null);
            out.close();
        }
        catch (Exception ex)
        {
            logger.warn("Error in creating extract info file " + spaceDirectory);
            throw ex;
        }
    }
	
	public JobStatus getExecutionStatus()
	{
		return executionStatus;
	}

	@Override
	public String getDetails()
	{
		StringBuilder sb = new StringBuilder();
		sb.append("SFDCExecution");
		sb.append(" : ");
		sb.append("spaceID=");
		sb.append(spaceID);
		sb.append(" : ");
		sb.append("jobId=");
		sb.append(runningJobId);
		sb.append(" : ");
		sb.append("scheduleId=");
		sb.append(sfdcScheduleStepId);
		return sb.toString();
	}
}
