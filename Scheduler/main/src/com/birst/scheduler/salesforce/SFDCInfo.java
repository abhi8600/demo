package com.birst.scheduler.salesforce;

import java.sql.Timestamp;

import com.birst.scheduler.shared.Schedule;
import com.birst.scheduler.utils.CompareUtils;
import com.birst.scheduler.utils.UtilConstants;


public class SFDCInfo
{
	
    @Override
	public boolean equals(Object obj)
	{
		if (obj == null){ return false;	}
		if(!(obj instanceof SFDCInfo)){return false;}
		SFDCInfo sfdcInfo = (SFDCInfo) obj;
		if(!CompareUtils.compare(sfdcInfo.getId(), this.getId(), true)) { return false;}
		if(!CompareUtils.compare(sfdcInfo.getUserId(), this.getUserId(), true)) { return false;}
		if(!CompareUtils.compare(sfdcInfo.getSpaceId(), this.getSpaceId(), true)) { return false;}
		if(!CompareUtils.compare(sfdcInfo.getClientId(), this.getClientId(), true)) { return false;}
		if(!CompareUtils.compare(sfdcInfo.getSandBoxUrl(), this.getSandBoxUrl(), true)) { return false;}
		if(!CompareUtils.compare(sfdcInfo.getLoadDate(), this.getLoadDate(), true)) { return false;}
		if(!CompareUtils.compare(sfdcInfo.getSubGroups(), this.getSubGroups(), true)) { return false;}
		if(!CompareUtils.compare(sfdcInfo.getExtractGroups(), this.getExtractGroups(), true)) { return false;}
		if(!(sfdcInfo.isProcessAfter() == this.isProcessAfter())) { return false;}
		if(!CompareUtils.compare(sfdcInfo.getName(), this.getName(), true)) { return false;}
		if(sfdcInfo.getType() != getType()) { return false;}
		if(sfdcInfo.getConnectorType() != getConnectorType()) { return false;}
		if(sfdcInfo.getExtractionEngine() != getExtractionEngine()) { return false;}
		return true;
	}
	
	String id;
	String spaceId;
	String userId;	
	boolean runNowMode;
	String clientId;
	String sandBoxUrl;
	boolean processAfter;
	// for processing , need more information
	String loadDate;
	String subGroups;
	String extractGroups;
	Schedule[] jobSchedules;
	Timestamp createdDate;
	Timestamp modifiedDate;
	String modifiedBy;
	boolean sendEmail;
	String name;
	String type;
	int extractionEngine;
	
	// only used for returning status. Not persisted in the database
    public boolean currentExecutionStatus;
	
    public static final String TYPE_SFDC = "sfdc";
    public static final String TYPE_NETSUITE = "netsuite";
    
	public SFDCInfo()
	{		
	}
	
	public SFDCInfo(String id, String spaceId, String userId, boolean scheduledMode,
			String clientId, String sandBoxUrl, boolean processAfter, String loadDate, 
			String subGroups, boolean sendEmail, String name, String type, int extractionEngine)
	{
		this.id = id;
		this.spaceId = spaceId;
		this.userId = userId;
		this.runNowMode = scheduledMode;
		this.clientId = clientId;
		this.sandBoxUrl = sandBoxUrl;
		this.processAfter = processAfter;
		this.loadDate = loadDate;
		this.subGroups = subGroups;
		this.sendEmail = sendEmail;
		this.name = name;
		this.type = type;
		this.extractionEngine = extractionEngine;
	}
	
	public String getId()
	{
		return id;
	}
	
	public String getSpaceId()
	{
		return spaceId;
	}
	public String getUserId()
	{
		return userId;
	}
	public boolean isProcessAfter()
	{
		return processAfter;
	}
	public boolean isRunNowMode()
	{
		return runNowMode;
	}
	public String getClientId()
	{
		return clientId;
	}

	public String getSandBoxUrl()
	{
		return sandBoxUrl;
	}

	public void setClientId(String clientId)
	{
		this.clientId = clientId;
	}

	public void setSandBoxUrl(String sandBoxUrl)
	{
		this.sandBoxUrl = sandBoxUrl;
	}

	public void setId(String id)
	{
		this.id = id;
	}
	public void setSpaceId(String spaceId)
	{
		this.spaceId = spaceId;
	}
	public void setUserId(String userId)
	{
		this.userId = userId;
	}
	public void setProcessAfter(boolean processAfter)
	{
		this.processAfter = processAfter;
	}
	public void setScheduledMode(boolean scheduledMode)
	{
		this.runNowMode = scheduledMode;
	}
	public String getLoadDate()
	{
		return loadDate;
	}
	public String getSubGroups()
	{
		return subGroups;
	}
	public void setLoadDate(String loadDate)
	{
		this.loadDate = loadDate;
	}
	public void setSubGroups(String subGroups)
	{
		this.subGroups = subGroups;
	}
	
	public String getExtractGroups()
	{
		return this.extractGroups;
	}
	
	public void setExtractGroups(String extractGroups)
	{
		this.extractGroups = extractGroups;
	}
	
	public Timestamp getCreatedDate()
	{
		return createdDate;
	}
	public Timestamp getModifiedDate()
	{
		return modifiedDate;
	}
	public String getModifiedBy()
	{
		return modifiedBy;
	}
	public void setCreatedDate(Timestamp createdDate)
	{
		this.createdDate = createdDate;
	}
	public void setModifiedDate(Timestamp modifiedDate)
	{
		this.modifiedDate = modifiedDate;
	}
	public void setModifiedBy(String modifiedBy)
	{
		this.modifiedBy = modifiedBy;
	}
	public Schedule[] getJobSchedules()
	{
		return jobSchedules;
	}
	public void setJobSchedules(Schedule[] jobSchedules)
	{
		this.jobSchedules = jobSchedules;
	}
	
	public boolean isCurrentExecutionStatus()
	{
		return currentExecutionStatus;
	}
	
	public boolean getSendEmail()
	{
		return sendEmail;
	}
	
	public void setSendEmail(boolean sendEmail)
	{
		this.sendEmail = sendEmail;
	}
	
	public String getName()
	{
		return name;
	}

	public String getType()
	{
		return type;
	}
	
	public String getConnectorType()
	{
		if (type.endsWith(UtilConstants.CONNECTOR_SCHEDULE_TYPE_SUFFIX))
			return type.substring(0, type.indexOf(UtilConstants.CONNECTOR_SCHEDULE_TYPE_SUFFIX));
		return type;
	}

	public int getExtractionEngine()
	{
		return extractionEngine;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public void setType(String type)
	{
		this.type = type;
	}

	public void setExtractionEngine(int extractionEngine)
	{
		this.extractionEngine = extractionEngine;
	}

	@Override
	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		if(id != null && id.trim().length() > 0)
		{
			sb.append("id=" + id);
			sb.append(" ");
		}
		if(spaceId != null && spaceId.trim().length() > 0)
		{
			sb.append("spaceId=" + spaceId);
			sb.append(" ");
		}
		if(userId != null && userId.trim().length() > 0)
		{
			sb.append("userId=" + userId);
			sb.append(" ");
		}
		sb.append("scheduled=" + runNowMode);
		sb.append(" ");
		sb.append("processAfter=" + processAfter);
		sb.append(" ");
		if(subGroups != null && subGroups.trim().length() > 0)
		{
			sb.append("subGroups=" + subGroups);
		}
		return sb.toString();
	}	
}
