package com.birst.scheduler.exceptions;

public class ValidationException extends SchedulerBaseException
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public ValidationException() {
		super();
	}
	
	public ValidationException(String s) {
		super(s);
	}
	
	public ValidationException(String s, Throwable cause) {
		super(s, cause);
	}

	@Override
	public int getErrorCode()
	{		
		return ERROR_VALIDATION;
	}
	
}
