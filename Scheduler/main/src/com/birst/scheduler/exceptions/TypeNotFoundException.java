package com.birst.scheduler.exceptions;

public class TypeNotFoundException extends SchedulerBaseException
{
	
	private static final long serialVersionUID = 1L;
	
	public TypeNotFoundException() {
		super();
	}
	
	public TypeNotFoundException(String s) {
		super(s);
	}
	
	public TypeNotFoundException(String s, Throwable cause) {
		super(s, cause);
	}

	@Override
	public int getErrorCode()
	{		
		return ERROR_JOB_TYPE_NOT_FOUND;
	}
	
	@Override
	public String getErrorMessage()
	{
		return "Job Type Not found";
	}

}
