package com.birst.scheduler.exceptions;

public class ObjectUnavailableException extends SchedulerBaseException
{


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public ObjectUnavailableException() {
		super();
	}
	
	public ObjectUnavailableException(String s) {
		super(s);
	}
	
	public ObjectUnavailableException(String s, Throwable cause) {
		super(s, cause);
	}

	@Override
	public int getErrorCode()
	{		
		return ERROR_OBJECT_UNAVAILABLE;
	}

}
