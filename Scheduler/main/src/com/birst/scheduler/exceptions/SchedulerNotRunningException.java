package com.birst.scheduler.exceptions;

public class SchedulerNotRunningException extends SchedulerBaseException
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;		
	
	public SchedulerNotRunningException()
	{
		super();
	}
	
	public SchedulerNotRunningException(String s)
	{
		super(s);
	}
	
	public SchedulerNotRunningException(String s, Throwable cause)
	{
		super(s, cause);
	}

	public int getErrorCode()
	{
		return ERROR_OTHER;
	}

}
