package com.birst.scheduler.exceptions;

public class JobRunningException extends SchedulerBaseException
{

	public JobRunningException() {
		super();
	}

	public JobRunningException(String s) {
		super(s);		
	}

	public JobRunningException(String s, Throwable cause) {
		super(s, cause);
	}
	
	public int getErrorCode()
	{
		return ERROR_JOB_RUNNING;
	}

}
