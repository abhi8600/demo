package com.birst.scheduler.exceptions;

public class UnAuthorizedRequestCredentials extends SchedulerBaseException
{
	private static final long serialVersionUID = 1L;
	
	public UnAuthorizedRequestCredentials()
	{
		super();
	}
	
	public UnAuthorizedRequestCredentials(String s)
	{
		super(s);
	}
	
	public UnAuthorizedRequestCredentials(String s, Throwable cause)
	{
		super(s, cause);
	}

	public int getErrorCode()
	{
		return ERROR_INVALID_CREDENTIALS;
	}
}
