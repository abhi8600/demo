package com.birst.scheduler.exceptions;

public class ConcurrentSpaceOperationException extends SchedulerBaseException
{
	
	private static final long serialVersionUID = 1L;
	
	public ConcurrentSpaceOperationException() {
		super();
	}
	
	public ConcurrentSpaceOperationException(String s) {
		super(s);
	}
	
	public ConcurrentSpaceOperationException(String s, Throwable cause) {
		super(s, cause);
	}

	@Override
	public int getErrorCode()
	{		
		return ERROR_CONCURRENT_SPACE_OP;
	}

}