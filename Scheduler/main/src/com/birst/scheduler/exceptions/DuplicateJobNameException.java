package com.birst.scheduler.exceptions;

public class DuplicateJobNameException extends SchedulerBaseException
{
	
	private static final long serialVersionUID = 1L;
	
	public DuplicateJobNameException() {
		super();
	}
	
	public DuplicateJobNameException(String s) {
		super(s);
	}
	
	public DuplicateJobNameException(String s, Throwable cause) {
		super(s, cause);
	}

	@Override
	public int getErrorCode()
	{		
		return ERROR_DUPLICATE_JOB_NAME;
	}
	
	@Override
	public String getErrorMessage()
	{
		return "Another schedule with the same name already exists. Please enter another name.";
	}

}
