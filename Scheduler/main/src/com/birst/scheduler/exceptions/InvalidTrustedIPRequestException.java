package com.birst.scheduler.exceptions;

public class InvalidTrustedIPRequestException extends SchedulerBaseException
{
	private static final long serialVersionUID = 1L;

	public InvalidTrustedIPRequestException()
	{
		super();
	}

	public InvalidTrustedIPRequestException(String s)
	{
		super(s);
	}

	public InvalidTrustedIPRequestException(String s, Throwable cause)
	{
		super(s, cause);
	}

	public int getErrorCode()
	{
		return ERROR_INVALID_TRUSTED_IP;
	}
}
