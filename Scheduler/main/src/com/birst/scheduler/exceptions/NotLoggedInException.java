package com.birst.scheduler.exceptions;

public class NotLoggedInException extends SchedulerBaseException
{

	private static final long serialVersionUID = 1L;
	
	public NotLoggedInException()
	{
		super();
	}
	
	public NotLoggedInException(String s)
	{
		super(s);
	}
	
	public NotLoggedInException(String s, Throwable cause)
	{
		super(s, cause);
	}

	public int getErrorCode()
	{
		return ERROR_NOT_LOGGED_IN;
	}

}
