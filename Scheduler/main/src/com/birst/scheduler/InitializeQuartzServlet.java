package com.birst.scheduler;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import javax.servlet.*;

import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.ee.servlet.QuartzInitializerServlet;
import org.quartz.impl.StdScheduler;
import org.quartz.impl.StdSchedulerFactory;

import com.birst.scheduler.exceptions.SchedulerBaseException;
import com.birst.scheduler.shared.SchedulerVersion;
import com.birst.scheduler.utils.Util;


public class InitializeQuartzServlet extends HttpServlet
{
    public static final String QUARTZ_FACTORY_KEY = "org.quartz.impl.StdSchedulerFactory.KEY";

    private boolean performShutdown = true;

    private Scheduler mainScheduler = null;
    private String primarySchedulerDisplayName = null;
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;	
	private static Logger logger = Logger.getLogger(InitializeQuartzServlet.class);
	
	public void init(ServletConfig cfg) throws ServletException
	{
		try
		{	
			// set up override properties
			SchedulerServerContext.setUpOverrideConfigProperties(cfg.getServletContext());
			// get all the schedulers defined
			String configFiles = getInitParameter(cfg, "config-file");
			if(configFiles == null || configFiles.length() == 0)
			{	
				throw new Exception("config-file not setup in the web.config or overrides properties file");
			}
			
			// replace any SCHEDULER_INSTALL reference
			configFiles = Util.replaceSchedulerHomeValue(configFiles);
			// parse and make sure only one is enabled, otherwise throw an exception
			initAllSchedulers(cfg, configFiles);
			
		}
		catch (Exception e) {
            logger.error("Quartz Scheduler failed to initialize: " + e.toString(), e);
            throw new ServletException(e);
        }        
	}
	
	
	private void initAllSchedulers(ServletConfig servletConfig, String configFiles) throws Exception
	{
		String primaryConfigFile = null;
		// see if multiple schedulers are provided
		String[] configs = configFiles.split(",");
		// if its only one, then this is the primary scheduler
		if(configs.length == 1)
		{
			String[] array = configFiles.split("::");
			if(array.length != 3)
			{
				throw new Exception("Incorrect config setup for scheduler : configFiles : " + configFiles);
			}
			
			int schedulerType = Integer.parseInt(array[1]);
			if(!SchedulerVersion.isCurrentRelease(schedulerType))
			{
				throw new Exception("Only one scheduler is defined and that too is not enabled");
			}
			
			if(array[2] == null || array[2].trim().length() == 0)
			{
				throw new Exception("Display Name of the Scheduler is not provided");
			}
			
			
				primaryConfigFile = array[0];				
				mainScheduler = initializeScheduler(primaryConfigFile);
				primarySchedulerDisplayName = array[2];			
			
		}
		else
		{
			// if multiple scheduler config files are provided, make sure only one is enabled
			for(String config : configs)
			{
				// get the enable/disable flag
				String[] str = config.split("::");
				if(str.length != 3)
				{
					throw new Exception("Wrong config-file path format given : " + config);
				}
				
				String file = str[0];
				int schedulerType = Integer.parseInt(str[1]);
				boolean isPrimaryScheduler = SchedulerVersion.isCurrentRelease(schedulerType);
				String schedulerDisplayName = str[2];
				// if the primary scheduler has already been defined, don't allow another one
				if(mainScheduler != null && isPrimaryScheduler)
				{
					throw new Exception("Only one primary scheduler at a time can be enabled ");
				}

				Scheduler sched = initializeScheduler(file);
				// if the scheduler has been added to the same name, we should throw an exception and not 
				// allow a duplicate entry. It could cause multiple scheduler instances of the same release
				// and might cause issues
				if(SchedulerServerContext.getSchedulerVersion(schedulerDisplayName) != null)
				{
					throw new Exception("Scheduler with display name " + schedulerDisplayName + " already present. Not allowed a duplicate entry");
				}
				SchedulerServerContext.addToSchedulerMap(schedulerDisplayName, sched, schedulerType);
				if(isPrimaryScheduler)
				{
					mainScheduler = sched;					
					primaryConfigFile = file;
					primarySchedulerDisplayName = schedulerDisplayName;
				}
			}
		}
		
		startPrimaryScheduler(servletConfig, primaryConfigFile);
	}
	
	private void startPrimaryScheduler(ServletConfig cfg, String primaryConfigFile) throws Exception
	{
		String shutdownPref = getInitParameter(cfg, "shutdown-on-unload");

        if (shutdownPref != null) {
            performShutdown = Boolean.valueOf(shutdownPref).booleanValue();
        }

        // Should the Scheduler being started now or later
        String startOnLoad = getInitParameter(cfg, "start-scheduler-on-load");

        int startDelay = 0;
        String startDelayS = getInitParameter(cfg, "start-delay-seconds");
        try {
            if(startDelayS != null && startDelayS.trim().length() > 0)
                startDelay = Integer.parseInt(startDelayS);
        } catch(Exception e) {
        	logger.error("Cannot parse value of 'start-delay-seconds' to an integer: " + startDelayS + ", defaulting to 5 seconds.", e);
            startDelay = 5;
        }
        
        SchedulerServerContext.initContext(primaryConfigFile, primarySchedulerDisplayName, mainScheduler, cfg.getServletContext());
        String mainSchedulerName = mainScheduler.getSchedulerName();
        /*
         * If the "start-scheduler-on-load" init-parameter is not specified,
         * the scheduler will be started. This is to maintain backwards
         * compatability.
         */
        if (startOnLoad == null || (Boolean.valueOf(startOnLoad).booleanValue())) {
            if(startDelay <= 0) {
                // Start now
                mainScheduler.start();
                logger.info("Main Scheduler : " + mainSchedulerName + " : has been started... : " );
            }
            else {
                // Start delayed
                mainScheduler.startDelayed(startDelay);
                logger.info("Main Scheduler : " + mainSchedulerName + " : will start in " + startDelay + " seconds.");
            }
        } else {
        	logger.info("Main Scheduler : " + mainSchedulerName + " : has not been started. Use scheduler.start()");
        }        
	}
	
	private Scheduler initializeScheduler(String configFile) throws Exception
	{
		Properties props = Util.loadPropertiesFromFile(configFile, true);
		if(props == null || props.size() == 0)
		{
			throw new Exception("Unable to load properties from file " + configFile);
		}
		String isClustedProperty = props.getProperty("org.quartz.jobStore.isClustered");
		if(isClustedProperty == null || !isClustedProperty.equalsIgnoreCase("true") )
		{
			throw new Exception("org.quartz.jobStore.isClustered property should always be set to true : " + configFile );
		}
		// make sure you are decrypting username/password before passing onto the scheduler
		String dataSourcePrefixKey = SchedulerServerContext.getDataSourcePrefixKey(props);
		String userKey = SchedulerServerContext.getSchedulerUserKey(dataSourcePrefixKey);
		String user = props.getProperty(userKey);
		if(user == null)
		{	
			throw new SchedulerBaseException("Unable to find user in configFile " + configFile);
		}
		props.setProperty(userKey, Util.getDecryptedString(user));
		String pwdKey = SchedulerServerContext.getSchedulerPasswordKey(dataSourcePrefixKey);
		String pwd = props.getProperty(pwdKey);
		if(pwd == null)
		{	
			throw new SchedulerBaseException("Unable to find pwd in configFile " + configFile);
		}
		props.setProperty(pwdKey, Util.getDecryptedString(pwd));
		// throw exception if any of them is in non-clustered mode
		StdSchedulerFactory factory = new StdSchedulerFactory(props);
		
		// Always want to get the scheduler, even if it isn't starting, 
        // to make sure it is both initialized and registered.
		return factory.getScheduler();
	}
	
	/**
	 * This looks for initParameter first in the serverContext and then in the servletConfig
	 * @param context
	 * @param name
	 * @return
	 */
	private String getInitParameter(ServletConfig config, String name)
	{
		String value = SchedulerServerContext.getInitParameter(config.getServletContext(), name);
		if(value == null)
		{
			value = config.getInitParameter(name);
		}		
		
		return value;
	}
	
	/*public void init2(ServletConfig cfg) throws ServletException
	{		
        logger.info("Quartz Initializer Servlet loaded, initializing Scheduler...");

        StdSchedulerFactory factory;
        try {

            String configFile = cfg.getInitParameter("config-file");
            String shutdownPref = cfg.getInitParameter("shutdown-on-unload");

            if (shutdownPref != null) {
                performShutdown = Boolean.valueOf(shutdownPref).booleanValue();
            }

            // get Properties
            if (configFile != null) {
                factory = new StdSchedulerFactory(configFile);
            } else {
            	logger.error("Config file not setup in the web.config file");
            	throw new Exception("Config File not specified");
            }
            
            // Always want to get the scheduler, even if it isn't starting, 
            // to make sure it is both initialized and registered.
            scheduler = factory.getScheduler();
            
            // Should the Scheduler being started now or later
            String startOnLoad = cfg.getInitParameter("start-scheduler-on-load");

            int startDelay = 0;
            String startDelayS = cfg.getInitParameter("start-delay-seconds");
            try {
                if(startDelayS != null && startDelayS.trim().length() > 0)
                    startDelay = Integer.parseInt(startDelayS);
            } catch(Exception e) {
            	logger.error("Cannot parse value of 'start-delay-seconds' to an integer: " + startDelayS + ", defaulting to 5 seconds.", e);
                startDelay = 5;
            }
            
            SchedulerServerContext.initContext(configFile, scheduler, cfg.getServletContext());
            
            
             * If the "start-scheduler-on-load" init-parameter is not specified,
             * the scheduler will be started. This is to maintain backwards
             * compatability.
             
            if (startOnLoad == null || (Boolean.valueOf(startOnLoad).booleanValue())) {
                if(startDelay <= 0) {
                    // Start now
                    scheduler.start();
                    logger.info("Scheduler has been started...");
                }
                else {
                    // Start delayed
                    scheduler.startDelayed(startDelay);
                    logger.info("Scheduler will start in " + startDelay + " seconds.");
                }
            } else {
            	logger.info("Scheduler has not been started. Use scheduler.start()");
            }

            String factoryKey = cfg.getInitParameter("servlet-context-factory-key");
            if (factoryKey == null) {
                factoryKey = QUARTZ_FACTORY_KEY;
            }
            
            logger.info("Storing the Quartz Scheduler Factory in the servlet context at key: " + factoryKey);
            cfg.getServletContext().setAttribute(factoryKey, factory);     

        } catch (Exception e) {
            logger.error("Quartz Scheduler failed to initialize: " + e.toString(), e);
            throw new ServletException(e);
        }        
    }*/
	
    public void destroy() {

        if (!performShutdown) {
            return;
        }

        try {
            if (mainScheduler != null) {
            	mainScheduler.shutdown();
            }
        } catch (Exception e) {
            logger.info("Quartz Scheduler failed to shutdown cleanly: " + e.toString());
            e.printStackTrace();
        }

        logger.info("Quartz Scheduler successful shutdown.");
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
        response.sendError(HttpServletResponse.SC_FORBIDDEN);
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
        response.sendError(HttpServletResponse.SC_FORBIDDEN);
    }
    
    public void hey(ServletConfig config){
		logger.info("Check for the Scheduler status ");
		ServletContext context = config.getServletContext();
		StdSchedulerFactory factory = (StdSchedulerFactory)context.getAttribute(QuartzInitializerServlet.QUARTZ_FACTORY_KEY);
		StdScheduler scheduler = null;
		try
		{
			scheduler = (StdScheduler) factory.getScheduler();
		} catch (SchedulerException ex)
		{
			logger.error("Problem in getting schduler started ", ex);
			return;
		}
		
		if(scheduler == null)
		{
			logger.fatal("Unable to start scheduler ");
			return;
		}
		
		logger.info("Scheduler Started " +  scheduler.isStarted());
				
	}
}
