package com.birst.scheduler.delete;

import java.io.StringReader;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.Iterator;
import java.util.UUID;

import javax.xml.namespace.QName;
import javax.xml.stream.FactoryConfigurationError;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
import org.apache.axiom.om.OMText;
import org.apache.axiom.om.impl.builder.StAXOMBuilder;
import org.apache.log4j.Logger;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;

import com.birst.scheduler.SchedulerServerContext;
import com.birst.scheduler.exceptions.ConcurrentSpaceOperationException;
import com.birst.scheduler.exceptions.ObjectUnavailableException;
import com.birst.scheduler.exceptions.SchedulerBaseException;
import com.birst.scheduler.process.ProcessInfo;
import com.birst.scheduler.process.ProcessJobOperation;
import com.birst.scheduler.shared.Operation;
import com.birst.scheduler.shared.SpaceStatus;
import com.birst.scheduler.utils.AxisXmlUtils;
import com.birst.scheduler.utils.JobConstants;
import com.birst.scheduler.utils.SchedulerDBConnection;
import com.birst.scheduler.utils.SchedulerDatabase;
import com.birst.scheduler.utils.StatusUtils;
import com.birst.scheduler.utils.Util;
import com.birst.scheduler.utils.UtilConstants;
import com.birst.scheduler.utils.StatusUtils.Step;
import com.birst.scheduler.utils.StatusUtils.StepStatus;
import com.birst.scheduler.utils.UtilConstants.DeleteType;
import com.birst.scheduler.webServices.XMLWebServiceResult;
import com.birst.scheduler.webServices.response.SchedulerWebServiceResult;

public class DeleteJobOperation implements Operation
{

	private static final Logger logger = Logger.getLogger(DeleteJobOperation.class);
	@Override
	public void addJob(SchedulerWebServiceResult result, String spaceID,
			String userID, String jobOptionsXmlString) throws SQLException,
			SchedulerBaseException, SchedulerException, ParseException,
			ClassNotFoundException
	{
		DeleteInfo deleteInfo = DeleteJobOperation.parseDeleteJobXmlString(spaceID, userID, jobOptionsXmlString);
		DeleteInfo dInfo = DeleteJobOperation.addDeleteJob(spaceID, userID, deleteInfo.getDeleteType(), 
				deleteInfo.getLoadNumber(), deleteInfo.getLoadGroup(), deleteInfo.isRestoreRepository());
		OMFactory factory = OMAbstractFactory.getOMFactory();
		OMNamespace ns = null;			
		OMElement jobIdElement = factory.createOMElement(JobConstants.JOB_ID, ns);
		OMText text = factory.createOMText(dInfo.getId());
		jobIdElement.addChild(text);
		result.setResult(new XMLWebServiceResult(jobIdElement));		
	}
	
	
	public static DeleteInfo addDeleteJob(String spaceID, String userID, String deleteType, 
			int loadID, String loadGroup, boolean restoreRepository) throws SQLException, SchedulerBaseException, ClassNotFoundException, SchedulerException
	{
		Connection conn = null;
		try
		{
			SpaceStatus anyConcurrentSpaceStatus = StatusUtils.getActiveSpaceStatus(spaceID);
			if(anyConcurrentSpaceStatus != null)
			{
				throw new ConcurrentSpaceOperationException("Another job is running for this space : " + anyConcurrentSpaceStatus.toString());
			}

			String jobId = UUID.randomUUID().toString();
			Scheduler scheduler = SchedulerServerContext.getSchedulerInstance();
			conn = SchedulerDBConnection.getInstance().getConnectionPool().getConnection();
			String schema = UtilConstants.SCHEDULER_DB_SCHEMA;

			// Clear up old completed/failed steps for this loadID and loadGroup
			SchedulerDatabase.deleteSpaceStatus(conn, schema, spaceID, loadID, loadGroup);
			JobDetail jobDetail = Util.getBirstJobBuilder(DeleteLoadJob.class, 
					Util.getCustDeleteJobName(jobId), 
					Util.getCustJobGroupNameForDelete(spaceID)).build();
			scheduler.addJob(jobDetail, false);

			DeleteInfo deleteInfo = new DeleteInfo(jobId, spaceID, userID, loadGroup, loadID, deleteType, restoreRepository);
			SchedulerDatabase.addDeleteInfo(conn, schema, deleteInfo);
			SpaceStatus spaceStatus = new SpaceStatus(UUID.randomUUID().toString(), spaceID, jobId, loadID, loadGroup, Step.ScheduledDelete, StepStatus.RUNNING ); 
			SchedulerDatabase.addSpaceStatus(conn, schema, spaceStatus);

			Trigger trigger = Util.getImmediateTrigger(spaceID, jobDetail, UUID.randomUUID().toString());
			trigger.getJobDataMap().put(UtilConstants.TRIGGER_MAP_SCHEDULE_STEP, spaceStatus.getId());
			scheduler.scheduleJob(trigger);
			
			return SchedulerDatabase.getDeleteInfo(conn, schema, jobId);
		}
		finally
		{
			if(conn != null)
			{
				try
				{
					conn.close();
				}catch(Exception ex)
				{
					logger.warn("Exception in closing connection ",ex);
				}
			}
		}
	}
	
	
	@SuppressWarnings("unchecked")
	public static DeleteInfo parseDeleteJobXmlString(String spaceID, String userID, String jobOptionsXmlString)
	{
		DeleteInfo deleteInfo = null;

		if(jobOptionsXmlString == null || jobOptionsXmlString.trim().length() == 0)
		{
			deleteInfo = new DeleteInfo();
			deleteInfo.setSpaceID(spaceID);
			deleteInfo.setUserID(userID);
			return deleteInfo;
		}
		XMLStreamReader reader = null;

		try
		{	
			reader = XMLInputFactory.newInstance().createXMLStreamReader(new StringReader(jobOptionsXmlString));
			StAXOMBuilder builder = new StAXOMBuilder(reader);
			OMElement docElement = builder.getDocumentElement();
			docElement.build();
			docElement.detach();

			OMElement element = docElement.getFirstChildWithName(new QName("", JobConstants.JOB_DL_NODE));
			// see if the root element itself is ScheduledReport
			if(element == null)
			{
				if (docElement.getLocalName() == JobConstants.JOB_DL_NODE)
				{
					element = docElement;
				}
			}
			if(element != null)
			{
				deleteInfo = new DeleteInfo();
				deleteInfo.setSpaceID(spaceID);
				deleteInfo.setUserID(userID);
				Iterator<OMElement> iterator = element.getChildElements();
				while(iterator.hasNext())
				{
					OMElement childElement = iterator.next();
					String elementName = childElement.getLocalName();
					String elementText = childElement.getText();					

					if(elementText == null || elementText.trim().length() == 0)
					{
						continue;
					}

					if(JobConstants.JOB_SP_LOAD_ID.equals(elementName))
					{	
						deleteInfo.setLoadNumber(Integer.parseInt(elementText));
					}

					if(JobConstants.JOB_SP_LOAD_GROUP.equals(elementName))
					{
						deleteInfo.setLoadGroup(elementText);
					}
					
					if(JobConstants.JOB_DL_TYPE.equals(elementName))
					{	
						DeleteType deleteType = null;
						if(DeleteType.DeleteAll.toString().equalsIgnoreCase(elementText))
						{
							deleteType = DeleteType.DeleteAll;
						}
						else if(DeleteType.DeleteLast.toString().equalsIgnoreCase(elementText))
						{
							deleteType = DeleteType.DeleteLast;
						}
						
						deleteInfo.setDeleteType(deleteType.toString());
					}

					if(JobConstants.JOB_DL_RESTORE.equals(elementName))
					{	
						deleteInfo.setRestoreRepository(Boolean.parseBoolean(elementText));
					}
				}
			}

		} catch (XMLStreamException e)
		{
			logger.error(e,e);
		} catch (FactoryConfigurationError e)
		{
			logger.error(e,e);
		}
		return deleteInfo;
	}

	@Override
	public void addScheduleToJob(SchedulerWebServiceResult result,
			String spaceID, String jobID, String jobOptionsXmlString)
			throws SQLException, SchedulerBaseException, SchedulerException,
			ParseException, ClassNotFoundException
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void getAllJobs(SchedulerWebServiceResult result, String spaceID,
			String onlyCurrentExecuting, String userID, String filterOnUser)
			throws SchedulerException, SQLException, SchedulerBaseException,
			ClassNotFoundException
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void getJobDetails(SchedulerWebServiceResult result, String spaceID,
			String jobID) throws SQLException, SchedulerBaseException,
			ClassNotFoundException
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void getJobStatus(SchedulerWebServiceResult result, String spaceID,
			String jobOptionsXmlString) throws Exception
	{
		OMFactory factory = OMAbstractFactory.getOMFactory();
		OMNamespace ns = null;
		OMElement status = factory.createOMElement(JobConstants.JOB_SP_STATUS_NODE, ns);

		DeleteInfo deleteInfo = DeleteJobOperation.parseDeleteJobXmlString(spaceID, null, jobOptionsXmlString);
		SpaceStatus spaceStatus = StatusUtils.getJobStatus(spaceID, deleteInfo.getLoadNumber() > 0 ? deleteInfo.getLoadNumber() : -1,
				deleteInfo.getLoadGroup());

		// based on the space status mark it as available for operation
		boolean available = false;
		if(spaceStatus != null)
		{
			if(spaceStatus.getStatus() != StepStatus.RUNNING.getStatus())
			{
				available = true;
			}
			
			AxisXmlUtils.addContent(factory, status, JobConstants.JOB_SP_AVAILABLE, available, ns);
			AxisXmlUtils.addContent(factory, status, JobConstants.JOB_SP_STATUS, spaceStatus.getStatus(), ns);
			AxisXmlUtils.addContent(factory, status, JobConstants.JOB_SP_STEP, spaceStatus.getStep(), ns);
			AxisXmlUtils.addContent(factory, status, JobConstants.JOB_SP_LOAD_ID, spaceStatus.getLoadNumber(), ns);
			AxisXmlUtils.addContent(factory, status, JobConstants.JOB_SP_LOAD_GROUP, spaceStatus.getLoadGroup(), ns);
			AxisXmlUtils.addContent(factory, status, JobConstants.JOB_SP_CREATED_DATE, spaceStatus.getCreatedDate(), "MM/dd/yyyy HH:mm:ss", ns);
			AxisXmlUtils.addContent(factory, status, JobConstants.JOB_SP_MODIFIED_DATE, spaceStatus.getModifiedDate(), "MM/dd/yyyy HH:mm:ss", ns);
		}
		else
		{
			available = true;
			AxisXmlUtils.addContent(factory, status, JobConstants.JOB_SP_AVAILABLE, available, ns);
		}

		result.setResult(new XMLWebServiceResult(status));
		
	}

	@Override
	public void killJob(SchedulerWebServiceResult result, String spaceID,
			String jobsRunStatusXmlString) throws SchedulerException
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pauseJob(SchedulerWebServiceResult result, String spaceID,
			String typeID) throws SchedulerException, SchedulerBaseException,
			SQLException, ParseException, ClassNotFoundException
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void removeJob(SchedulerWebServiceResult result, String spaceID,
			String jobID) throws SchedulerException, SQLException,
			SchedulerBaseException, ClassNotFoundException
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void removeScheduleFromJob(SchedulerWebServiceResult result,
			String spaceID, String jobID, String scheduleID)
			throws SQLException, SchedulerBaseException, SchedulerException,
			ClassNotFoundException
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resumeJob(SchedulerWebServiceResult result, String spaceID,
			String typeID) throws SchedulerException, SchedulerBaseException,
			SQLException, ParseException, ClassNotFoundException
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void runNow(SchedulerWebServiceResult result, String spaceID,
			String typeID) throws SchedulerBaseException, SchedulerException
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updateJob(SchedulerWebServiceResult result, String spaceID,
			String userID, String jobID, String jobOptionsXmlString)
			throws SchedulerException, SchedulerBaseException, SQLException,
			ParseException, ClassNotFoundException
	{
		// TODO Auto-generated method stub
		
	}

}
