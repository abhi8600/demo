package com.birst.scheduler.delete;

import java.sql.Timestamp;

public class DeleteInfo
{
	String id;
	String spaceID;
	String userID;
	String loadGroup;
	int loadNumber;
	boolean restoreRepository;
	String deleteType;
	Timestamp createdDate;
	Timestamp modifiedDate;
	String modifiedBy;
	
	public DeleteInfo()
	{	
	}
	public DeleteInfo(String deleteInfoId, String spaceID, String userID,
			String loadGroup, int loadNumber, String deleteType, boolean restoreRepository) 
	{
		this.id = deleteInfoId;
		this.spaceID = spaceID;
		this.userID = userID;
		this.loadGroup = loadGroup;
		this.loadNumber = loadNumber;
		this.deleteType = deleteType;
		this.restoreRepository = restoreRepository;
	}
	
	public String getId()
	{
		return id;
	}
	public String getSpaceID()
	{
		return spaceID;
	}
	public String getUserID()
	{
		return userID;
	}
	public String getLoadGroup()
	{
		return loadGroup;
	}
	public String getDeleteType()
	{
		return deleteType;
	}
	public Timestamp getCreatedDate()
	{
		return createdDate;
	}
	public Timestamp getModifiedDate()
	{
		return modifiedDate;
	}
	public void setId(String id)
	{
		this.id = id;
	}
	public void setSpaceID(String spaceID)
	{
		this.spaceID = spaceID;
	}
	public void setUserID(String userID)
	{
		this.userID = userID;
	}
	public void setLoadGroup(String loadGroup)
	{
		this.loadGroup = loadGroup;
	}
	public void setDeleteType(String deleteType)
	{
		this.deleteType = deleteType;
	}
	public void setCreatedDate(Timestamp createdDate)
	{
		this.createdDate = createdDate;
	}
	public void setModifiedDate(Timestamp modifiedDate)
	{
		this.modifiedDate = modifiedDate;
	}
	public String getModifiedBy()
	{
		return modifiedBy;
	}
	public void setModifiedBy(String modifiedBy)
	{
		this.modifiedBy = modifiedBy;
	}
	

	public int getLoadNumber()
	{
		return loadNumber;
	}

	public void setLoadNumber(int loadNumber)
	{
		this.loadNumber = loadNumber;
	}

	public boolean isRestoreRepository()
	{
		return restoreRepository;
	}

	public void setRestoreRepository(boolean restoreRepository)
	{
		this.restoreRepository = restoreRepository;
	}

	@Override
	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		if(id != null && id.trim().length() > 0)
		{
			sb.append("id=" + id);
		}
		if(spaceID != null && spaceID.trim().length() > 0)
		{
			sb.append("spaceID=" + spaceID);
		}
		if(userID != null && userID.trim().length() > 0)
		{
			sb.append("userID=" + userID);
		}
		if(deleteType != null && deleteType.trim().length() > 0)
		{
			sb.append("deleteType=" + deleteType);
		}
		if(loadGroup != null && loadGroup.trim().length() > 0)
		{
			sb.append("loadGroup=" + loadGroup);
		}
		return sb.toString();
	}

	
}
