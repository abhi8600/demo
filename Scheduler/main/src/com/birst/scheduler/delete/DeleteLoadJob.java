package com.birst.scheduler.delete;

import java.io.File;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.UUID;

import org.apache.log4j.Logger;
import org.quartz.InterruptableJob;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.UnableToInterruptJobException;

import com.birst.scheduler.exceptions.ConcurrentSpaceOperationException;
import com.birst.scheduler.exceptions.SchedulerBaseException;
import com.birst.scheduler.shared.SpaceStatus;
import com.birst.scheduler.utils.CommandUtil;
import com.birst.scheduler.utils.ExceptionCodes;
import com.birst.scheduler.utils.SchedulerDBConnection;
import com.birst.scheduler.utils.SchedulerDatabase;
import com.birst.scheduler.utils.StatusUtils;
import com.birst.scheduler.utils.Util;
import com.birst.scheduler.utils.UtilConstants;
import com.birst.scheduler.utils.StatusUtils.Step;
import com.birst.scheduler.utils.StatusUtils.StepStatus;
import com.birst.scheduler.utils.UtilConstants.DeleteType;
import com.birst.scheduler.utils.UtilConstants.RunningJobStatus;

public class DeleteLoadJob implements InterruptableJob
{
	String deleteLoadScheduleStepId;
	String runningJobId;
	RunningJobStatus jobStatus;
	boolean retry;
	String errorMessage;
	String exitErrorCodeMsg;
	Map<String,Process> runningCommandProcesses = new HashMap<String,Process>();
	
	private static Logger logger = Logger.getLogger(DeleteLoadJob.class);
	@Override
	public void interrupt() throws UnableToInterruptJobException
	{
	
	}
	
	@Override
	public void execute(JobExecutionContext context)
			throws JobExecutionException
	{
		Connection conn = null;
		DeleteInfo deleteInfo = null;
		try
		{
			String groupName = context.getJobDetail().getKey().getGroup();
			String spaceID = groupName.substring(UtilConstants.PREFIX_DELETE_JOB_GROUP_NAME.length());
			logger.debug("DeleteLoad Job starting for space " + spaceID);
			
			// Get the spacestatus step id if it is in the trigger hash map
			// this happens if its a run now operation which will always be the case for delete last or delete all operation
			Object obj = context.getMergedJobDataMap().get(UtilConstants.TRIGGER_MAP_SCHEDULE_STEP);
			if(obj != null)
			{
				deleteLoadScheduleStepId = (String) obj;
				// validate if its a right format
				UUID.fromString(deleteLoadScheduleStepId);
			}
			// Make sure no other concurrent process is running
			SpaceStatus anyConcurrentSpaceStatus = StatusUtils.getActiveSpaceStatus(spaceID, false);
			if(anyConcurrentSpaceStatus != null && (deleteLoadScheduleStepId == null || !anyConcurrentSpaceStatus.getId().equalsIgnoreCase(deleteLoadScheduleStepId)))
			{
				logger.error("Another job is running for this space : " + anyConcurrentSpaceStatus.toString());
				markTheJobStatus(RunningJobStatus.FAILED, "Concurrent job running", false, StatusUtils.getConcurrentExitCodeMessage(anyConcurrentSpaceStatus));
				throw new ConcurrentSpaceOperationException("Another job is running for this space : " + anyConcurrentSpaceStatus.toString());
			}
			
			String spaceDirectory = Util.getSpaceDirectory(spaceID);
			if(spaceDirectory == null)
			{
				markTheJobStatus(RunningJobStatus.FAILED, "Space Directory not found", false);
				logger.info("Unable to delete load. Space Directory not found : " + spaceID);
				return;
			}
			
			Util.validateNoExtractFilePresent(spaceDirectory);
			Util.validateNoPublishLockFilePresent(spaceDirectory);
			
			runningJobId = Util.getObjectID(context.getJobDetail().getKey().getName(), UtilConstants.PREFIX_DELETE_LOAD_JOB);
			runningJobId = Util.getLowerCaseString(runningJobId);
			conn = SchedulerDBConnection.getInstance().getConnectionPool().getConnection();
			String schema = UtilConstants.SCHEDULER_DB_SCHEMA;
			
			// this is the id of the schedule step which is put in by the caller during the time of 
			// adding process schedule
			deleteInfo = SchedulerDatabase.getDeleteInfo(conn, schema, runningJobId);
			if(deleteInfo == null)
			{
				throw new Exception("Delete Job Info not found : id : " + runningJobId);
			}
			
			if(deleteLoadScheduleStepId == null)
			{
				SpaceStatus spaceStatus = new SpaceStatus(UUID.randomUUID().toString(), spaceID, runningJobId, deleteInfo.getLoadNumber(), deleteInfo.getLoadGroup(), Step.ScheduledDelete, StepStatus.RUNNING ); 
				SchedulerDatabase.addSpaceStatus(conn, schema, spaceStatus);
				deleteLoadScheduleStepId = spaceStatus.getId();
			}
			
			
			deleteLoad(spaceDirectory, deleteInfo);
			markTheJobStatus(RunningJobStatus.COMPLETE, null);

		}
		catch(Exception ex)
		{
			logger.error("Exception while running DeleteJob" , ex);
			markTheJobStatus(RunningJobStatus.FAILED, "Exception while running DeleteJob", false);
			throw new JobExecutionException("Error while running DeleteJob", ex.getCause());	
		}
		finally
		{
			Util.configureForRetry(context, jobStatus, retry, errorMessage);
			try
			{
				if(deleteLoadScheduleStepId != null)
				{
					StepStatus scheduleStepStatus = StepStatus.SUCCESS;
					if(jobStatus.getStatus() != RunningJobStatus.COMPLETE.getStatus())
					{
						scheduleStepStatus = StepStatus.FAILED;
					}
					StatusUtils.updateSpaceStatus(deleteLoadScheduleStepId, scheduleStepStatus.getStatus(), exitErrorCodeMsg, true);
				}
			}catch(Exception ex)
			{
				logger.error("Exception while updating scheduled status for uid : " + deleteLoadScheduleStepId, ex);
			}
		}
	}
	
	private void deleteLoad(String spaceDirectory, DeleteInfo deleteInfo) throws SchedulerBaseException, ClassNotFoundException, SQLException
	{
		// check the type if it is delete all or delete last
		// if delete all,everything is handled by Acorn
		
		// if delete last , then need to get the right command from Acorn
		// and then proceed to performance engine
		
		String deleteType = deleteInfo.getDeleteType();
		boolean deleteAll = false;
		if(DeleteType.DeleteAll.toString().equalsIgnoreCase(deleteType) || 
				(DeleteType.DeleteLast.toString().equalsIgnoreCase(deleteType) && deleteInfo.getLoadNumber() == 1))
		{
			deleteAll = true;
		}
		
		String deleteStepStatusId = UUID.randomUUID().toString();
		boolean success = runDeleteLoad(spaceDirectory, deleteStepStatusId, deleteAll, deleteInfo);
		if(success)
		{
			// if it is delete all return
			// else continue to the actual deleteLoad via Performance engine
			// followed by Post delete
			
				String messageFilePath = CommandUtil.getMessageFilePath(spaceDirectory, deleteStepStatusId);
				if(CommandUtil.isFileExists(messageFilePath))
				{
					Properties props = CommandUtil.getProps(messageFilePath);
					StatusUtils.updateStatusForBirstLocal(props, deleteStepStatusId, deleteInfo.getSpaceID(), 
							deleteInfo.getLoadNumber(), deleteInfo.getLoadGroup());
				}
				if(!deleteAll)
				{
					String deleteLastStepId = UUID.randomUUID().toString();
					if(runDeleteLastViaPerformanceEngine(deleteLastStepId, messageFilePath, deleteInfo))
					{
						// run Post
						String postStepStatusUId = UUID.randomUUID().toString();
						runPostDeleteLoad(spaceDirectory, postStepStatusUId, deleteInfo);
					}
				}			
		}		
	}
	
	
	private boolean runPostDeleteLoad(String spaceDirectory, String deletePostStepStatusId, DeleteInfo deleteInfo) throws SchedulerBaseException, ClassNotFoundException, SQLException
	{
		int exitValue = -1;
		try
		{		
			StatusUtils.addSpaceStatus(deletePostStepStatusId, deleteInfo.getSpaceID(), runningJobId, deleteInfo.getLoadNumber(), deleteInfo.getLoadGroup(), StatusUtils.Step.DeleteLastPost, StatusUtils.StepStatus.RUNNING);
			String logFile = CommandUtil.getProcessCommandLogFilePath(spaceDirectory);
			String cmd = CommandUtil.getPostDeleteLastCommand(logFile, deletePostStepStatusId, deleteInfo.getSpaceID(), 
					deleteInfo.getUserID(), deleteInfo.getLoadGroup(), deleteInfo.getLoadNumber(), deleteInfo.isRestoreRepository());
			exitValue = CommandUtil.runCommand(CommandUtil.CMD_POST_DELETE_LOAD, cmd, null, runningCommandProcesses);			
		}
		catch(Exception ex)
		{
			logger.error("Error in building before Load ", ex);
			exitValue = ExceptionCodes.INTERNAL_ERROR;
		}
		finally
		{
			StatusUtils.updateSpaceStatus(deletePostStepStatusId, exitValue);
		}
		
		return CommandUtil.isSuccessfulExit(exitValue);
	}
	
	private boolean runDeleteLoad(String spaceDirectory, String deleteStepStatusId, boolean deleteAll, DeleteInfo deleteInfo) throws SchedulerBaseException, ClassNotFoundException, SQLException
	{
		int exitValue = -1;
		try
		{
			StatusUtils.Step step = deleteAll ? StatusUtils.Step.DeleteAll : StatusUtils.Step.DeleteLastPre;			
			StatusUtils.addSpaceStatus(deleteStepStatusId, deleteInfo.getSpaceID(), runningJobId, deleteInfo.getLoadNumber(), 
					deleteInfo.getLoadGroup(), step, StatusUtils.StepStatus.RUNNING);
			String logFile = CommandUtil.getProcessCommandLogFilePath(spaceDirectory);
			String cmd = CommandUtil.getDeleteCommandForAcorn(logFile, deleteStepStatusId, deleteInfo.getSpaceID(), 
					deleteInfo.getUserID(), deleteInfo.getLoadGroup(), deleteInfo.getLoadNumber(), deleteAll);
			exitValue = CommandUtil.runCommand(CommandUtil.CMD_DELETE_LOAD, cmd, null, runningCommandProcesses);			
		}
		catch(Exception ex)
		{
			logger.error("Error in building before Load ", ex);
			exitValue = ExceptionCodes.INTERNAL_ERROR;
		}
		finally
		{
			StatusUtils.updateSpaceStatus(deleteStepStatusId, exitValue);
		}
		
		return CommandUtil.isSuccessfulExit(exitValue);
	}
	
	private boolean runDeleteLastViaPerformanceEngine(String stepUid, String messageFilePath, DeleteInfo deleteInfo) throws SchedulerBaseException, ClassNotFoundException, SQLException
	{
		int exitValue = -1;
		try
		{	
			StatusUtils.addSpaceStatus(stepUid, deleteInfo.getSpaceID(), runningJobId, deleteInfo.getLoadNumber(), 
					deleteInfo.getLoadGroup(), StatusUtils.Step.DeleteLast, StatusUtils.StepStatus.RUNNING);
			// get the load number from the returned props
			//String messageFilePath = CommandUtil.getMessageFilePath(spaceDir, uid);
			Properties props = CommandUtil.getProps(messageFilePath);
			if(props != null)
			{
				String engineCommand = props.getProperty(CommandUtil.KEY_ENGINE_COMMAND);
				String args = props.getProperty(CommandUtil.KEY_ENGINE_ARGS);

				engineCommand = engineCommand.replace("//", File.separator);
				args = args.replace("//", File.separator);
				String cmd = CommandUtil.getProcessCommand(engineCommand, args);
				File file = new File(engineCommand);
				if(file.exists())
				{
					String workingDirectory = file.getParent();
					exitValue = CommandUtil.runCommand(CommandUtil.CMD_DELETE_LOAD, cmd, workingDirectory, runningCommandProcesses);
				}
			}
		}
		catch(Exception ex)
		{
			logger.error("Exception during load command ", ex);
			exitValue = ExceptionCodes.INTERNAL_ERROR;
		}
		finally
		{
			StatusUtils.updateSpaceStatus(stepUid, exitValue);
		}
		
		return CommandUtil.isSuccessfulExit(exitValue);
	}
	
	private void markTheJobStatus(RunningJobStatus status, String errorMsg)
	{
		markTheJobStatus(status, errorMsg, false);
	}
	
	private void markTheJobStatus(RunningJobStatus status, String errorMsg, boolean retryJob)
	{
		markTheJobStatus(status, errorMsg, false, null);
	}
	
	private void markTheJobStatus(RunningJobStatus status, String errorMsg, boolean retryJob, String exitCodeMsg)
	{
		jobStatus = status;
		errorMessage = errorMsg;
		retry = retryJob;
		exitErrorCodeMsg = exitCodeMsg;
	}
	
}
