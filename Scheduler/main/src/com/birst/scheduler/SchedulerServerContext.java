package com.birst.scheduler;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.StringTokenizer;
import java.util.UUID;

import javax.servlet.ServletContext;

import org.apache.log4j.Logger;
import org.quartz.JobListener;
import org.quartz.Matcher;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.TriggerListener;
import org.quartz.impl.matchers.NameMatcher;


/**
 * This class contains the instance of scheduler and other  
 * global config params required
 *
 * @author gsingh
 *
 */
import com.birst.scheduler.exceptions.SchedulerBaseException;
import com.birst.scheduler.exceptions.SchedulerNotRunningException;
import com.birst.scheduler.listeners.jobs.ConfigureRetryJob;
import com.birst.scheduler.listeners.jobs.PersistJobHistoryInDB;
import com.birst.scheduler.listeners.triggers.ClearScheduleInfoTriggerListener;
import com.birst.scheduler.shared.SchedulerVersion;
import com.birst.scheduler.shared.ServerDetails;
import com.birst.scheduler.utils.AcornAdminDBConnection;
import com.birst.scheduler.utils.RoundRobin;
import com.birst.scheduler.utils.SchedulerDBConnection;
import com.birst.scheduler.utils.SchedulerDatabase;
import com.birst.scheduler.utils.Upgrade;
import com.birst.scheduler.utils.Util;
import com.birst.scheduler.utils.UtilConstants;
import com.birst.scheduler.utils.DatabaseConnection;

public class SchedulerServerContext
{
	private static Logger logger = Logger.getLogger(SchedulerServerContext.class);
		
	// some of the properties defined in quartz properties file
	
	public static final String QUARTZ_JOBSTORE_DATASOURCE = "org.quartz.jobStore.dataSource";
	public static final String QUARTZ_JOBSTORE_DATASOURCE_PREFIX = "org.quartz.dataSource";
	public static final String QUARTZ_JOBSTORE_DATASOURCE_SUFFIX_DRIVER = "driver";
	public static final String QUARTZ_JOBSTORE_DATASOURCE_SUFFIX_DBTYPE = "schedulerDBType";
	public static final String QUARTZ_JOBSTORE_DATASOURCE_SUFFIX_URL = "URL";
	public static final String QUARTZ_JOBSTORE_DATASOURCE_SUFFIX_USER = "user";
	public static final String QUARTZ_JOBSTORE_DATASOURCE_SUFFIX_PASSWORD = "password";
	public static final String QUARTZ_JOBSTORE_DATASOURCE_SUFFIX_MAXCONNECTIONS = "maxConnections";
	public static final String CONFIG_SMIWEB_IPS = "SMIWeb.ServerList";
	public static final String CONFIG_BIRSTADMIN_IPS = "Birst.ServerList";
	public static final String CONFIG_EMAIL_DELIVERY_FROM = "DeliveryFrom" ;
	private static final String CONFIG_AXIS_MAX_CONNECTIONS_PER_HOST = "MaxConnectionsPerHost" ;
	private static final String CONFIG_AXIS_CLIENT_TIMEOUT = "AxisClientTimeOut" ;
	// AcornExecute exe path -- wrapper around Acorn
	public static final String CONFIG_ACORN_EXECUTE_PATH_KEY = "AcornExecutePath";
	public static final String CONFIG_ACORN_WEB_DIR_PATH_KEY = "AcornWebConfigDirectory";
	private static final String CONFIG_SFDC_NUM_PARALLEL_THREADS = "SFDCNumThreads";
	
	private static final String CONFIG_DEPLOYMENT_TYPE = "Deployment" ;
		
	// AcornAdmin DB Configuration
	public static final String ACORN_ADMIN_CONNECT_STRING = "AcornAdminConnectString";
	public static final String ACORN_ADMIN_USERNAME = "AcornAdminUser";
	public static final String ACORN_ADMIN_PASSWORD = "AcornAdminPwd";
	public static final String ACORN_ADMIN_DBTYPE = "AcornDBType";
	public static final String ACORN_ADMIN_DRIVER_NAME = "AcornDriver";
	
	
	public static final String SCHEDULER_SERVER_ID = "Scheduler.VersionID";
	public static final String SCHEDULER_LOGIN_ALLOWED_IPS = "SchedulerLogin.AllowedIPs";
	
	public static boolean TEST_MODE = false;
	public static String SMIWEB_TEST_URL = "http://sfogsingh:6103/SMIWeb";
	public static String BIRSTADMIN_TEST_URL = "http://sfogsingh:57747";
	
	//private static Scheduler scheduler;
	private static Properties quartzConfigProps;
	private static List<ServerDetails> smiWebServerURLs;
	private static List<String> birstAdminURLs;
	private static List<String> scheduerLoginServiceAllowedIPs;
	private static String emailDeliveryAddress ;
	private static int schedulerServerID;
	private static int axisMaxConnectionsPerHost;
	private static int axisClientTimeOutValue;
	private static String acornExecutePathValue;
	private static String acornWebConfigDirectory;
	private static int sfdcNumberParallelThreads;
	
	private static final int DEFAULT_AXIS_CLIENT_TIMEOUT = 120*1000;
	private static final int DEFAULT_MAX_CONNECTIONS_PER_HOST = 2;
	private static final int DEFAULT_SFDC_NUM_THREADS = 4;
	// Retry params
	public static final String DAILY_RETRY_SECONDS = "DailyRetrySeconds";
	public static final String WEEKLY_RETRY_SECONDS = "WeeklyRetrySeconds";
	public static final String MONTHLY_RETRY_SECONDS = "MonthlyRetrySeconds";
	
	public static final int DEFAULT_DAILY_RETRY_SECONDS = 3600;
	public static final int DEFAULT_WEEKLY_RETRY_SECONDS = 86400;
	public static final int DEFAULT_MONTHLY_RETRY_SECONDS = 86400;
	
	private static Map<String, Integer> retryTimeMap;
	
	private static final String KEY_ROUNDROBIN_SMIWEB = "SMIWebSources";
	
	private static boolean initialized = false;
	private static String mailerHost;
	// overrides contains any overriden properties
	private static Properties overrides;
	private static String connectorControllerURI;
	private static boolean useNewConnectorFramework;
	
	public static final String CONFIG_MAILER_HOST_PARAM = "MailHost";
	public static final String CONFIG_OVERRIDE_FILEPATH_PARAM = "OverrideProperties.FilePath"; 
	public static final String DEFAULT_OVERRIDE_PROPERTIES_FILENAME = "birstscheduler.properties";
	public static final String SCHEDULER_INSTALL_DIR_TAG = "V{SCHEDULER_INSTALL_DIR}";
	
	//private static Map<String,Scheduler> nonPrimarySchedulersMap = new HashMap<String,Scheduler>();
	private static Map<String,SchedulerVersion> allSchedulersMap = new HashMap<String,SchedulerVersion>();
	private static SchedulerVersion primarySchedulerVersion;
	private static String CONNECTOR_CONTROLLER_URI = "ConnectorControllerURI";
	private static String USE_NEW_CONNECTOR_FRAMEWORK = "UseNewConnectorFramework";
	
	
	public static void setTestMode(boolean testMode)
	{
		TEST_MODE = testMode;
	}
	
	public static void initContext(String configFile, String primarySchedulerDisplayName, Scheduler _scheduler, ServletContext context) throws Exception
	{		
		if(initialized)
		{
			return;
		}
		setPrimaryScheduler(primarySchedulerDisplayName, _scheduler);
		quartzConfigProps = parsePropertiesFile(configFile);
		logQuartzProps();
		//setUpOverrideConfigProperties(context);
		setSchedulerProperties(context);
		registerJobListeners();
		registerTriggerListeners();
		//setBirstAdminIPs(context);
		setDeliveryAddress(context);		
		setSchedulerLoginAllowedIps(context);		
		setUpSchedulerDB();
		setUpAcornAdminDatabase(context);		
		setUpRetryMap(context);
		setUpAxisConnectionsPerHost(context);
		setUpAxisClientTimeOut(context);
		setAcornExecutePath(context);
		setAcornWebDirectoryPath(context);
		setUpSFDCNumberThreads(context);
		setConnectorControllerURI(context);
		setUseNewConnectorFramework(context);
		initialized = true;
	}
	
	public static void setUpOverrideConfigProperties(ServletContext context)
	{
		if(TEST_MODE)
		{
			return;
		}
	
		// get the override file name and properties
		String file = context.getInitParameter(CONFIG_OVERRIDE_FILEPATH_PARAM);
		file = Util.replaceSchedulerHomeValue(file);
		if (file == null || file.isEmpty())
			file = DEFAULT_OVERRIDE_PROPERTIES_FILENAME;
		overrides = Util.loadPropertiesFromFile(file, true);	
	}
	
	
	public static String getInitParameter(ServletContext context, String parameter)
	{
		// property file always overrides the web.xml setting
		String value = null;
		if (overrides != null)
		{
			value = overrides.getProperty(parameter);
		}
		if (value == null)
		{
			value = context.getInitParameter(parameter);
		}
		else
		{
			if(!SchedulerServerContext.ACORN_ADMIN_USERNAME.equalsIgnoreCase(parameter) && 
					!SchedulerServerContext.ACORN_ADMIN_PASSWORD.equalsIgnoreCase(parameter))
			{
				logger.debug("Using override for " + parameter + ": " + value);
			}
		}
		return value;
	}
	
	private static void setSchedulerProperties(ServletContext context)
	{
		
		String serverID = getInitParameter(context, SCHEDULER_SERVER_ID);
		if(serverID != null && serverID.length() > 0)
		{	
			try
			{
				schedulerServerID = Integer.parseInt(serverID);
			}
			catch(NumberFormatException ex)
			{
				schedulerServerID = -1;
				logger.error("Scheduler Server ID is incorrectly set to " + serverID + " ",  ex);
				throw ex;
			}
		}
		logger.info("Scheduler Server ID : " + serverID);
		mailerHost = getInitParameter(context, CONFIG_MAILER_HOST_PARAM);
		logger.debug("Mail Host : " + mailerHost);
	}

	private static void setSchedulerLoginAllowedIps(ServletContext context)
	{		
		String ips = getInitParameter(context, SCHEDULER_LOGIN_ALLOWED_IPS);
		if (ips != null)
		{
			StringTokenizer st = new StringTokenizer(ips, ", ");
			scheduerLoginServiceAllowedIPs = new LinkedList<String>();
			while (st.hasMoreTokens())
			{
				String ip = st.nextToken();
				scheduerLoginServiceAllowedIPs.add(ip);
			}
		}
	}
	
	public static int getSchedulerServerID()
	{
		return schedulerServerID;
	}
	
	public static List<String> getSchedulerLoginAllowedIps()
	{
		
		return scheduerLoginServiceAllowedIPs;
	}

	private static void registerJobListeners() throws SchedulerException, SchedulerNotRunningException
	{
		JobListener persistJobHistoryListener = new PersistJobHistoryInDB(UtilConstants.JOB_LISTENER_PERSIST_JOB_HISTORY);
		Util.addAllJobsListener(persistJobHistoryListener);
        //scheduler.addJobListener(persistJobHistoryListener);
        JobListener configureRetryJobListener = new ConfigureRetryJob(UtilConstants.JOB_LISTENER_CONFIGURE_RETRY_JOB);
        Util.addAllJobsListener(configureRetryJobListener);
        //scheduler.addJobListener(configureRetryJobListener);
	}
	
	
	@SuppressWarnings("unchecked")
	private static void registerTriggerListeners() throws SchedulerException
	{
	
		// register the listener now and use it at discretion for various jobs
		// This is one way we can just register it without any info or job/trigger
		Matcher randomnMatcher = NameMatcher.nameEquals(UUID.randomUUID().toString());
		TriggerListener triggerListener = new ClearScheduleInfoTriggerListener(UtilConstants.TRIGGER_LISTENER_ClEAR_SCHEDULEINFO);
		primarySchedulerVersion.getScheduler().getListenerManager().addTriggerListener(triggerListener, randomnMatcher);
	}
	
	
/*	private static void setSMIWebIPs(ServletContext context) throws SchedulerBaseException, ClassNotFoundException
	{
		if(TEST_MODE)
		{
			return;
		}
		
		List<ServerDetails> serverDetails = Util.retrieveSMIWebServers(schedulerServerID);
		if(serverDetails == null || serverDetails.size() == 0)
		{	
			String msg = "No SMIWeb servers configured in db. Please configure SMIWeb servers";
			logger.fatal(msg);
			return;
			//throw new SchedulerBaseException(msg);
		}
		
		smiWebServerURLs = serverDetails;
		RoundRobin.addRoundRobinSource(KEY_ROUNDROBIN_SMIWEB, smiWebServerURLs);
	}*/
	
	/*
	private static void setSMIWebIPs(ServletContext context) throws SchedulerBaseException
	{
		if(TEST_MODE)
		{
			return;
		}
		
		String ips = getInitParameter(CONFIG_SMIWEB_IPS);
		if (ips != null)
		{
			StringTokenizer st = new StringTokenizer(ips, ", ");
			smiWebServerURLs = new ArrayList<String>();
			while (st.hasMoreTokens())
			{
				String ip = st.nextToken();
				smiWebServerURLs.add(ip);
			}
		}
		else
		{
			String msg = "No login SMIWeb Ips configured. Please configure " + CONFIG_SMIWEB_IPS;
			logger.fatal(msg);
			throw new SchedulerBaseException(msg);
		}
		
		RoundRobin.addRoundRobinSource(KEY_ROUNDROBIN_SMIWEB, smiWebServerURLs);
	}
	*/
	
	/*
	private static void setBirstAdminIPs(ServletContext context) throws SchedulerBaseException
	{
		if(TEST_MODE)
		{
			return;
		}
		
		String ips = getInitParameter(CONFIG_BIRSTADMIN_IPS);
		if (ips != null)
		{
			StringTokenizer st = new StringTokenizer(ips, ", ");
			birstAdminURLs = new ArrayList<String>();
			while (st.hasMoreTokens())
			{
				String ip = st.nextToken();
				birstAdminURLs.add(ip);
			}
		}
		else
		{
			String msg = "No login Birst Ips configured. Please configure " + CONFIG_BIRSTADMIN_IPS;
			logger.fatal(msg);
			throw new SchedulerBaseException(msg);
		}
	}
	*/
	
	private static void setUpAxisConnectionsPerHost(ServletContext context)
	{
		String maxConnectionsPerHost = getInitParameter(context, CONFIG_AXIS_MAX_CONNECTIONS_PER_HOST);
		if (maxConnectionsPerHost != null && maxConnectionsPerHost.length() > 0)
		{
			try
			{
				axisMaxConnectionsPerHost = Integer.parseInt(maxConnectionsPerHost.trim());
			}catch(Exception ex)
			{
				logger.error("Exception while parsing maxConnectionPerHost value", ex);
				logger.warn("No Valid MaxConnectionsPerHost configured. Using default value " + DEFAULT_MAX_CONNECTIONS_PER_HOST);
				axisMaxConnectionsPerHost = DEFAULT_MAX_CONNECTIONS_PER_HOST;
			}
		}
	}

	private static void setUpAxisClientTimeOut(ServletContext context)
	{
		String valueString = getInitParameter(context, CONFIG_AXIS_CLIENT_TIMEOUT);
		if (valueString != null && valueString.length() > 0)
		{
			try
			{
				axisClientTimeOutValue = Integer.parseInt(valueString.trim()) * 1000;
			}catch(Exception ex)
			{
				logger.error("Exception while parsing axisClientTimeout value", ex);
				logger.warn("No Valid AxisClientTimeout configured. Using default value " + DEFAULT_AXIS_CLIENT_TIMEOUT);
				axisClientTimeOutValue = DEFAULT_AXIS_CLIENT_TIMEOUT;
			}
		}
	}
	
	private static void setDeliveryAddress(ServletContext context) throws SchedulerBaseException
	{		
		String deliveryAddress = getInitParameter(context, CONFIG_EMAIL_DELIVERY_FROM);
		if (deliveryAddress != null && deliveryAddress.length() > 0 && deliveryAddress.contains("@"))
		{
			emailDeliveryAddress = deliveryAddress;
		}
		else
		{			
			String msg = "No Delivery Address configured. Please configure " + CONFIG_EMAIL_DELIVERY_FROM;
			logger.fatal(msg);
			throw new SchedulerBaseException(msg);
		}
	}
	
	
	// To do 
	// Return the randomn ip from the given list and then store the ip in the database
	// so that scheduler knows which ip to go to for a particular job check status
	public static String getSMIWebUrlRoundRobinStyle() throws Exception
	{		 
		if(TEST_MODE)
		{
			return SMIWEB_TEST_URL;
		}
		
		String smiWebUrl = null;
		ServerDetails serverDetails = RoundRobin.getNextAvailableElement(KEY_ROUNDROBIN_SMIWEB);
		ServerDetails first = serverDetails;
		while(!serverDetails.isAvailable())
		{
			serverDetails = RoundRobin.getNextAvailableElement(KEY_ROUNDROBIN_SMIWEB);
			// if after looping through the entire list, we cannot find any available connection, error out
			if(first.getSchedulerServerId() == serverDetails.getSchedulerServerId())
			{
				logger.error("No active server found ");
				throw new Exception("No Available SMIWeb server found");
			}
		}
		smiWebUrl = serverDetails != null ? serverDetails.getUrl() : null;
		return smiWebUrl;
	}
	
	public static String getBirstAdminUrl()
	{		 
		if(TEST_MODE)
		{
			return BIRSTADMIN_TEST_URL;
		}
		return birstAdminURLs.get(0);
	}
	
	
	private static void setUpRetryMap(ServletContext context)
	{
		Integer dailyRetrySeconds = getRetrySecs(context, DAILY_RETRY_SECONDS);
		dailyRetrySeconds = dailyRetrySeconds == -1 ? 60 : dailyRetrySeconds;
		
		Integer weeklyRetrySeconds = getRetrySecs(context, WEEKLY_RETRY_SECONDS);
		weeklyRetrySeconds = weeklyRetrySeconds == -1 ? 86400 : weeklyRetrySeconds;
		
		Integer monthlyRetrySeconds = getRetrySecs(context, MONTHLY_RETRY_SECONDS);
		monthlyRetrySeconds = monthlyRetrySeconds == -1 ? 86400 : monthlyRetrySeconds;
		retryTimeMap = new HashMap<String, Integer>();
		retryTimeMap.put(DAILY_RETRY_SECONDS, dailyRetrySeconds);
		retryTimeMap.put(WEEKLY_RETRY_SECONDS, weeklyRetrySeconds);
		retryTimeMap.put(MONTHLY_RETRY_SECONDS, monthlyRetrySeconds);
	}
	
	private static Integer getRetrySecs(ServletContext context, String intervalRetrySeconds)
	{
		Integer retrySecs = -1;
		String retrySecsString = getInitParameter(context, intervalRetrySeconds);
		try
		{
			retrySecs = Integer.parseInt(retrySecsString);
		}
		catch(Exception ex)
		{
			logger.warn("Exception in getting " + intervalRetrySeconds , ex);
		}
		
		return retrySecs;
	}
	
	
	public static Map<String, Integer> getJobRetryTimeMap()
	{
		return retryTimeMap;
	}
	
	public static String getEmailDeliveryAddress()
	{
		return emailDeliveryAddress;
	}
	
	public static String getSchedulerUserKey(String dataSourcePrefixKey)
	{
		return dataSourcePrefixKey + QUARTZ_JOBSTORE_DATASOURCE_SUFFIX_USER;
	}
	
	public static String getSchedulerPasswordKey(String dataSourcePrefixKey)
	{
		return dataSourcePrefixKey + QUARTZ_JOBSTORE_DATASOURCE_SUFFIX_PASSWORD;
	}
	
	public static String getDataSourcePrefixKey(Properties props)
	{
		String datasourceName = props.getProperty(SchedulerServerContext.QUARTZ_JOBSTORE_DATASOURCE);
		return QUARTZ_JOBSTORE_DATASOURCE_PREFIX + "." + datasourceName + ".";
	}
	
	private static void setUpSchedulerDB() throws Exception
	{
		// set up connection pool for 
		
		String dataSourcePrefix = getDataSourcePrefixKey(quartzConfigProps);
		
		String driverKey =  dataSourcePrefix + QUARTZ_JOBSTORE_DATASOURCE_SUFFIX_DRIVER;
		String dbTypeKey = dataSourcePrefix + QUARTZ_JOBSTORE_DATASOURCE_SUFFIX_DBTYPE;
		String urlKey = dataSourcePrefix + QUARTZ_JOBSTORE_DATASOURCE_SUFFIX_URL;
		String userKey = getSchedulerUserKey(dataSourcePrefix);
		String pwdKey = getSchedulerPasswordKey(dataSourcePrefix);
		String maxConnectionsKey = dataSourcePrefix + QUARTZ_JOBSTORE_DATASOURCE_SUFFIX_MAXCONNECTIONS;
		logger.debug("DriverKey " + driverKey);
		logger.debug("dbTypeKey " + dbTypeKey);
		logger.debug("urlKey " + urlKey);
		//logger.debug("userKey " + userKey);
		//logger.debug("pwdKey " + pwdKey);
		logger.debug("maxConnectionsKey " + maxConnectionsKey);
		String driver = quartzConfigProps.getProperty(driverKey);
		if(driver == null)
		{	
			throw new SchedulerBaseException("Unable to find driver for setting up ConnectionPool");
		}
		String url = quartzConfigProps.getProperty(urlKey);
		if(url == null)
		{	
			throw new SchedulerBaseException("Unable to find url for setting up ConnectionPool");
		}
		String user = quartzConfigProps.getProperty(userKey);
		if(user == null)
		{	
			throw new SchedulerBaseException("Unable to find user for setting up ConnectionPool");
		}
		String pwd = quartzConfigProps.getProperty(pwdKey);
		if(pwd == null)
		{	
			throw new SchedulerBaseException("Unable to find pwd for setting up ConnectionPool");
		}
		int maxConnections = Integer.parseInt(quartzConfigProps.getProperty(maxConnectionsKey));
		if(maxConnections <= 0)
		{	
			throw new SchedulerBaseException("Unable to find maxConnections for setting up ConnectionPool");
		}
		String dbType = quartzConfigProps.getProperty(dbTypeKey);
		SchedulerDBConnection.getInstance("QRTZ_DB_1" , driver, url, Util.getDecryptedString(user), Util.getDecryptedString(pwd), maxConnections, dbType);
		Connection conn = SchedulerDBConnection.getInstance().getConnectionPool().getConnection();
		String schema = UtilConstants.SCHEDULER_DB_SCHEMA;
		
		// Upgrade is called before createTables.
		// Because create table should have all the updated columm/type/size, calling upgrade after createTables
		// will throw error on duplicating operations. It makes sense to call createTable after upgrade
		upgradeSchedulerDatabase();
		SchedulerDatabase.createTables(conn, schema);
	}
	
	private static void upgradeSchedulerDatabase() throws SQLException, SchedulerBaseException, ClassNotFoundException
	{
		Connection conn = SchedulerDBConnection.getInstance().getConnectionPool().getConnection();
		String schema = UtilConstants.SCHEDULER_DB_SCHEMA;
		Upgrade.UpgradeDatabaseSchema(conn, schema);
	}
	
	private static void setUpAcornAdminDatabase(ServletContext context) throws SchedulerBaseException, SQLException, ClassNotFoundException
	{	
		String driver = getInitParameter(context, ACORN_ADMIN_DRIVER_NAME);
		if (driver == null || driver.isEmpty())
			driver = DatabaseConnection.MSSQL_DRIVER_NAME;
		String url = getInitParameter(context, ACORN_ADMIN_CONNECT_STRING);
		String user = getInitParameter(context, ACORN_ADMIN_USERNAME);
		String pwd = getInitParameter(context, ACORN_ADMIN_PASSWORD);
		String dbType = getInitParameter(context, ACORN_ADMIN_DBTYPE);
		int maxConnections = -1;
		// get deployment type from web.xml or any overriden properties
		/*String deploymentType = getInitParameter(context, CONFIG_DEPLOYMENT_TYPE);
		if(!deploymentType.equalsIgnoreCase("dev"))
		{
			Properties props = parsePropertiesFile(deploymentType + "-" + "overrides.properties");
			String overrideurl = props.getProperty(ACORN_ADMIN_CONNECT_STRING);
			String overrideuser = props.getProperty(ACORN_ADMIN_USERNAME);
			String overridepwd = props.getProperty(ACORN_ADMIN_PASSWORD);
		}	*/	
		
		AcornAdminDBConnection.getInstance("ACORN_ADMIN" , driver, url, Util.getDecryptedString(user), Util.getDecryptedString(pwd), maxConnections, dbType);				
	}

	public static Scheduler getSchedulerInstance() throws SchedulerNotRunningException
	{		
		if(primarySchedulerVersion == null)
		{			
			logger.error("Main Scheduler is not running");
			throw new SchedulerNotRunningException("Scheduler not running");
		}
		return 	primarySchedulerVersion.getScheduler();
	}
	
	public static Properties parsePropertiesFile(String filename) throws SchedulerBaseException
	{
		Properties props = null;
		InputStream is = null;        
		
        is = Thread.currentThread().getContextClassLoader().getResourceAsStream(filename);
        
        try {
            if(is != null) {
                is = new BufferedInputStream(is);                
            } else {
                is = new BufferedInputStream(new FileInputStream(filename));                
            }
            props = new Properties();
            props.load(is);
        } catch (Exception ex) {
        	logger.error("Unable to load " + filename , ex);
        	throw new SchedulerBaseException("Unable to find and load: '" + filename , ex);
        }
        finally {
            if(is != null)
                try { is.close(); } catch(IOException ignore) {}
        }

		return props;
	}
	
	public static Properties getQuartzProps()
	{
		return quartzConfigProps;
	}
	
	public static void logQuartzProps() throws SchedulerBaseException
	{		
		if(quartzConfigProps == null || quartzConfigProps.size() == 0)
		{
			logger.error("No quartz props found ");
        	throw new SchedulerBaseException("No quartz Properties Loaded");
		}
		
		logger.debug("---Quartz Properties----");
		for(Object propKey : quartzConfigProps.keySet())
		{
			String key = (String)propKey;
			String value = quartzConfigProps.getProperty(key);			
			if(key.toLowerCase().startsWith(SchedulerServerContext.QUARTZ_JOBSTORE_DATASOURCE_PREFIX.toLowerCase()) 
					&& (key.toLowerCase().endsWith(SchedulerServerContext.QUARTZ_JOBSTORE_DATASOURCE_SUFFIX_USER.toLowerCase())
							|| key.toLowerCase().endsWith(SchedulerServerContext.QUARTZ_JOBSTORE_DATASOURCE_SUFFIX_PASSWORD.toLowerCase())
					))
			{
				continue;
			}
			logger.debug(key + " = " + value);
		}
	}

	public static int getDailyRetrySeconds()
	{
		return retryTimeMap != null ? retryTimeMap.get(DAILY_RETRY_SECONDS) : DEFAULT_DAILY_RETRY_SECONDS; 
	}

	public static int getWeeklyRetrySeconds()
	{
		return retryTimeMap != null ? retryTimeMap.get(WEEKLY_RETRY_SECONDS) : DEFAULT_WEEKLY_RETRY_SECONDS;
	}

	public static int getMonthlyRetrySeconds()
	{
		return retryTimeMap != null ? retryTimeMap.get(MONTHLY_RETRY_SECONDS) : DEFAULT_MONTHLY_RETRY_SECONDS;
	}
	
	public static String getMailerHost()
	{
		return mailerHost;
	}

	public static int getMaxConnectionsPerHost()
	{
		return axisMaxConnectionsPerHost <= 0 ? DEFAULT_MAX_CONNECTIONS_PER_HOST : axisMaxConnectionsPerHost;
	}
	
	public static int getAxisClientTimeOut()
	{
		return axisClientTimeOutValue <= 0 ? DEFAULT_AXIS_CLIENT_TIMEOUT : axisClientTimeOutValue;
	}
	
	public static Map<String,SchedulerVersion> getAllSchedulers()
	{
		return allSchedulersMap;
	}
	
	public static void addToSchedulerMap(String name, Scheduler scheduler, int schedulerType) throws Exception
	{	
		boolean migrationAllowedFromScheduler = SchedulerVersion.isPastRelease(schedulerType);
		SchedulerVersion schedulerRelease = new SchedulerVersion(scheduler, name, migrationAllowedFromScheduler);
		allSchedulersMap.put(name, schedulerRelease);
	}
	
	private static void setPrimaryScheduler(String displayName, Scheduler scheduler) throws SchedulerException
	{
		primarySchedulerVersion = new SchedulerVersion(scheduler, displayName, false);
		allSchedulersMap.put(displayName, primarySchedulerVersion);
	}
	
	public static Scheduler getPrimaryScheduler()
	{
		return primarySchedulerVersion.getScheduler();
	}
	
	public static String getPrimarySchedulerDisplayName()
	{
		return primarySchedulerVersion.getDisplayName();
	}
	
	public static SchedulerVersion getSchedulerVersion(String displayName)
	{
		SchedulerVersion schedulerVersion = null;
		for(String key : allSchedulersMap.keySet())
		{
			if(key.equalsIgnoreCase(displayName))
			{
				schedulerVersion = allSchedulersMap.get(key);
			}
		}
		return schedulerVersion;
	}
	
	private static void setAcornExecutePath(ServletContext context) throws SchedulerBaseException
	{		
		acornExecutePathValue = getInitParameter(context, CONFIG_ACORN_EXECUTE_PATH_KEY);
		if (acornExecutePathValue == null || acornExecutePathValue.length() == 0 )
		{
			String msg = "No AcornExecute path configured. Please configure " + CONFIG_ACORN_EXECUTE_PATH_KEY;
			logger.fatal(msg);
			throw new SchedulerBaseException(msg);
		}
	}
	
	public static String getAcornExecutePath()
	{
		return acornExecutePathValue;
	}

	private static void setAcornWebDirectoryPath(ServletContext context) throws SchedulerBaseException
	{		
		acornWebConfigDirectory = getInitParameter(context, CONFIG_ACORN_WEB_DIR_PATH_KEY);
		if (acornWebConfigDirectory == null || acornWebConfigDirectory.length() == 0 )
		{
			String msg = "No Acorn Web Config Directory path configured. Please configure " + CONFIG_ACORN_WEB_DIR_PATH_KEY;
			logger.fatal(msg);
			throw new SchedulerBaseException(msg);
		}
	}
	
	public static String getAcornWebConfigDirectory()
	{	
		return acornWebConfigDirectory;
	}
	
	private static void setUpSFDCNumberThreads(ServletContext context)
	{
		String numSFDCThreads = getInitParameter(context, CONFIG_SFDC_NUM_PARALLEL_THREADS);
		if (numSFDCThreads != null && numSFDCThreads.length() > 0)
		{
			try
			{
				sfdcNumberParallelThreads = Integer.parseInt(numSFDCThreads.trim());
			}catch(Exception ex)
			{
				logger.error("Exception while parsing SFDCNumThreads value", ex);
				logger.warn("No Valid SFDCNumThreads configured. Using default value " + DEFAULT_SFDC_NUM_THREADS);
				sfdcNumberParallelThreads = DEFAULT_SFDC_NUM_THREADS;
			}
		}
	}
	
	public static int getSFDCNumberThreads()
	{
		return sfdcNumberParallelThreads <= 0 ? DEFAULT_SFDC_NUM_THREADS : sfdcNumberParallelThreads;
	}
	
	public static void setConnectorControllerURI(ServletContext context) {
		connectorControllerURI = getInitParameter(context, CONNECTOR_CONTROLLER_URI);
	}
	
	public static String getConnectorControllerURI() {
		return connectorControllerURI;
	}
	
	public static void setUseNewConnectorFramework(ServletContext context) {
		String useNewConnector = getInitParameter(context, USE_NEW_CONNECTOR_FRAMEWORK);
		useNewConnectorFramework = (useNewConnector != null) ? Boolean.parseBoolean(useNewConnector) : false;
	}
	
	public static boolean getUseNewConnectorFramework() {
		return useNewConnectorFramework;
	}
}
