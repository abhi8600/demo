/*
 * Copyright 2007 Tatsuya Anno(taapps@gmail.com). All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/

/*
 * minor modifications to fit within our environment
 */

package com.birst.scheduler.security;

import java.util.List;
import java.util.Iterator;
import java.net.InetAddress;

public class Net{
	public static String getLocalHost(){
		try{
			InetAddress localHostInet=InetAddress.getLocalHost();
			return localHostInet.getHostAddress();
		}catch(Exception e){
			return null;
		}
	}
	public static boolean isSame(InetAddress pAddr1, InetAddress pAddr2){
		if (pAddr1 == null && pAddr2 == null)
			return true;
		if (pAddr1 == null || pAddr2 == null)
			return false;
		return pAddr1.equals(pAddr2);
	}
	
	public static InetAddress getMaskedIp(byte[] pAddressBytes, byte[] pMaskBytes){
		if (pAddressBytes == null || pMaskBytes == null)
			return null;
		if(pAddressBytes.length!=pMaskBytes.length){
			return null;
		}
		
		byte[] maskedAddressBytes=new byte[pAddressBytes.length];
		for(int i=0;i<pAddressBytes.length;i++){
			byte address      =pAddressBytes[i];
			byte mask         =pMaskBytes[i];
			maskedAddressBytes[i]=(byte)(address&mask);
		}
		
		if (maskedAddressBytes == null){
			return null;
		}
		try{
			return InetAddress.getByAddress(maskedAddressBytes);
		}catch(Exception e){
			return null;
		}
	}
	
	public static String convertCidrToNetmaskStr(String pCidrStr){
		if(pCidrStr == null){
			return null;
		}
		
		try{
			int cidrInt=Integer.parseInt(pCidrStr);
			int cidrMask = 0xffffffff<<(32-cidrInt);
			int maskSegment1=cidrMask>>24 & 0xff;
			int maskSegment2=cidrMask>>16 & 0xff;
			int maskSegment3=cidrMask>>8 & 0xff;
			int maskSegment4=cidrMask>>0 & 0xff;
			
			StringBuilder maskAddressBuf=new StringBuilder();
			maskAddressBuf.append(maskSegment1);
			maskAddressBuf.append(".");
			maskAddressBuf.append(maskSegment2);
			maskAddressBuf.append(".");
			maskAddressBuf.append(maskSegment3);
			maskAddressBuf.append(".");
			maskAddressBuf.append(maskSegment4);
			
			return maskAddressBuf.toString();
		}catch(Exception e){
			return null;
		}
	}
	
	public static boolean containIp(List pIpList,String pRemoteIp){
		if (pIpList == null){
			return false;
		}
		if (pRemoteIp == null){
			return false;
		}
		
		Iterator ipIterator=pIpList.iterator();
		while(ipIterator.hasNext()){
			String listedAddressStr=(String)ipIterator.next();
			if(listedAddressStr == null){
				continue;
			}

			boolean compare=containIp(listedAddressStr,pRemoteIp);
			if(compare){
				return true;
			}
		}
		return false;
	}

	public static boolean containIp(String pAddressStr,String pRemoteIp){
		if (pAddressStr == null){
			return false;
		}
		if (pRemoteIp == null){
			return false;
		}
			
		String[] listedAddressArray=pAddressStr.split("/");
		if (listedAddressArray == null){
			return false;
		}

		String listedIpStr=listedAddressArray[0];
		String listedCidrStr=null;
		if(listedAddressArray.length>=2){
			listedCidrStr=listedAddressArray[1];
		}
		else{
			listedCidrStr="24";
		}
		
		String listedCidrNetmaskStr=convertCidrToNetmaskStr(listedCidrStr);

		try{
			InetAddress listedIpInet  =InetAddress.getByName(listedIpStr);
			byte[] listedIpBytes=listedIpInet.getAddress();
				
			InetAddress listedMaskInet=InetAddress.getByName(listedCidrNetmaskStr);
			byte[] listedMaskBytes=listedMaskInet.getAddress();
			
			InetAddress remoteIpInet  =InetAddress.getByName(pRemoteIp);
			byte[] remoteIpBytes=remoteIpInet.getAddress();

			if(listedIpBytes == null || listedMaskBytes == null || remoteIpBytes == null)
			{
				return false;
			}
			/* address ipv4 and v6 compatibility */
			if(listedIpBytes.length!=listedMaskBytes.length
				||listedMaskBytes.length!=remoteIpBytes.length)
			{
				return false;
			}
			
			InetAddress maskedListIp=getMaskedIp(listedIpBytes,listedMaskBytes);
			InetAddress maskedRemoteIp=getMaskedIp(remoteIpBytes,listedMaskBytes);
			
			boolean compare=isSame(maskedListIp,maskedRemoteIp);
			if(compare){
				return true;
			}
		}catch(Exception e){
			return false;
		}
		return false;
	}
}
