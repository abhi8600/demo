package com.birst.scheduler.bds;

import static org.quartz.CronScheduleBuilder.cronSchedule;
import java.io.IOException;
import java.io.StringReader;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import java.util.UUID;
import java.util.Map.Entry;
import javax.xml.namespace.QName;
import javax.xml.stream.FactoryConfigurationError;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axiom.om.OMNamespace;
import org.apache.axiom.om.OMText;
import org.apache.axiom.om.impl.builder.StAXOMBuilder;
import org.apache.log4j.Logger;
import org.quartz.CronTrigger;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.TriggerKey;
import org.quartz.impl.matchers.GroupMatcher;
import com.birst.scheduler.SchedulerServerContext;
import com.birst.scheduler.exceptions.ConcurrentSpaceOperationException;
import com.birst.scheduler.exceptions.JobRunningException;
import com.birst.scheduler.exceptions.ObjectUnavailableException;
import com.birst.scheduler.exceptions.SchedulerBaseException;
import com.birst.scheduler.shared.Operation;
import com.birst.scheduler.shared.Schedule;
import com.birst.scheduler.shared.SchedulerVersion;
import com.birst.scheduler.shared.SpaceStatus;
import com.birst.scheduler.utils.AxisXmlUtils;
import com.birst.scheduler.utils.CompareUtils;
import com.birst.scheduler.utils.JobConstants;
import com.birst.scheduler.utils.SchedulerDBConnection;
import com.birst.scheduler.utils.SchedulerDatabase;
import com.birst.scheduler.utils.StatusUtils;
import com.birst.scheduler.utils.Util;
import com.birst.scheduler.utils.UtilConstants;
import com.birst.scheduler.utils.StatusUtils.Step;
import com.birst.scheduler.utils.StatusUtils.StepStatus;
import com.birst.scheduler.webServices.XMLWebServiceResult;
import com.birst.scheduler.webServices.response.SchedulerWebServiceResult;
import com.birst.scheduler.bds.BDSInfo;

public class BDSJobOperation implements Operation {
	// Job operation member variables.
	public static final Logger logger = Logger.getLogger(BDSJobOperation.class);
	
	public static void addSchedule(String spaceID, String jobID, Schedule schedule) 
		throws SQLException, SchedulerBaseException, SchedulerException, ParseException, ClassNotFoundException {
		// Remove the old one and create a new one.
		Connection conn = null;
		try {
			if (schedule.getId() != null) {
				schedule.setId(UUID.randomUUID().toString());
			}

			conn = SchedulerDBConnection.getInstance().getConnectionPool().getConnection();
			String schema = UtilConstants.SCHEDULER_DB_SCHEMA;
			BDSInfo bdsInfo = SchedulerDatabase.getBDSInfo(conn, schema, jobID);
			if (bdsInfo == null) {
				throw new ObjectUnavailableException("BDSInfo not found for id : " + jobID + ".");
			}

			SchedulerDatabase.addSchedule(conn, schema, schedule);
			Scheduler scheduler = SchedulerServerContext.getSchedulerInstance();
			if (schedule.isEnable()) {
				JobKey jobKey = new JobKey(Util.getBDSCustJobName(bdsInfo.getId()), bdsInfo.getSpaceId());
				JobDetail jobDetail = scheduler.getJobDetail(jobKey);
				String timeZoneID = schedule.getTimeZone();
				CronTrigger cronTrigger = TriggerBuilder.newTrigger()
				.withIdentity(Util.getBDSCustTriggerName(schedule.getId()), spaceID)
				.withSchedule(cronSchedule(schedule.getExpression()).inTimeZone(TimeZone.getTimeZone(timeZoneID)))		
				.build();
				scheduler.scheduleJob(jobDetail, cronTrigger);
			}
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (Exception ex) {
				}
			}
		}
	}
	
	@Override
	public void addScheduleToJob(SchedulerWebServiceResult result, String spaceID, String jobID, String jobOptionsXmlString) 
		throws Exception {
		BDSInfo bdsInfo = BDSJobOperation.parseScheduleDetailsXmlString(spaceID, null, null, jobOptionsXmlString);				
		Schedule[] jobSchedules = bdsInfo.getJobSchedules();
		if (jobSchedules != null && jobSchedules.length > 0) {
			for (Schedule schedule : jobSchedules) {
				BDSJobOperation.addSchedule(spaceID, jobID, schedule);
			}
		} else {
			logger.warn("No schedules to add for space " + spaceID + " : job " + jobID + ".");
		}
	}

	@Override
	public void addJob(SchedulerWebServiceResult result, String spaceID, String userID, String jobOptionsXmlString) 
		throws Exception {	
		// Parse xml options and add new scheduled job.
		BDSInfo bdsInfo = BDSJobOperation.parseOptionsXmlString(spaceID, userID, jobOptionsXmlString);
		bdsInfo = BDSJobOperation.parseScheduleDetailsXmlString(spaceID, userID, bdsInfo, jobOptionsXmlString);
		try {
			bdsInfo = BDSJobOperation.addBDSInfo(spaceID, bdsInfo);
		} catch (IOException e) {
			logger.error("Exception in adding job : ", e);
			throw new SchedulerException();
		}
		OMFactory factory = OMAbstractFactory.getOMFactory();
		OMNamespace ns = null;			
		OMElement jobIdElement = factory.createOMElement(JobConstants.JOB_ID, ns);
		OMText text = factory.createOMText(bdsInfo.getId());
		jobIdElement.addChild(text);
		result.setResult(new XMLWebServiceResult(jobIdElement));	
	}

	public static BDSInfo addBDSInfo(String spaceID, BDSInfo bdsInfo) 
		throws SQLException, SchedulerBaseException, ClassNotFoundException, SchedulerException, ParseException, IOException {
		Scheduler scheduler = SchedulerServerContext.getSchedulerInstance();
		return BDSJobOperation.addBDSInfo(scheduler, spaceID, bdsInfo);
	}
	
	public static BDSInfo addBDSInfo(Scheduler scheduler, String spaceID, BDSInfo bdsInfo) 
		throws SQLException, SchedulerBaseException, ClassNotFoundException, SchedulerException, ParseException, IOException {
		Connection conn = null;
		try
		{
			// Initialize db info.
			String jobID = UUID.randomUUID().toString();
			bdsInfo.setId(jobID);
			conn = SchedulerDBConnection.getInstance().getConnectionPool().getConnection();
			String schema = UtilConstants.SCHEDULER_DB_SCHEMA;
			
			// Schedules are filled as part of the supplied bdsInfo object.
			Schedule[] schedules = bdsInfo.getJobSchedules();

			// Add bds info to the database.
			SchedulerDatabase.addBDSInfo(conn, schema, bdsInfo);
			if (bdsInfo.isRunNowMode()) {				
				BDSJobOperation.addAndRunNowBDSJob(spaceID, bdsInfo);
			}
			else {	
				// If no run now schedule, do normal scheduling.				
				// Create job, trigger details and schedule it.
				JobBuilder jobBuilder = Util.getBirstJobBuilder(BDSJob.class, Util.getBDSCustJobName(bdsInfo.getId()), 
						Util.getCustJobGroupNameForBDS(spaceID));
				JobDetail jobDetail = jobBuilder.build();
				scheduler.addJob(jobDetail, false);
				if (schedules != null && schedules.length > 0) {
					for (Schedule schedule : schedules) {
						// Add the trigger only if enable flag is set.
						if (schedule.getId() == null) {
							schedule.setId(UUID.randomUUID().toString());
						}
						schedule.setJobId(jobID);
						SchedulerDatabase.addSchedule(conn, schema, schedule);
						if (!schedule.isEnable()) {
							continue;						
						}
						
						// Schedule the trigger.
						String timeZoneID = Schedule.getTimeZoneID(schedule.getTimeZone());
						CronTrigger cronTrigger = TriggerBuilder.newTrigger()
						.withIdentity(Util.getBDSCustTriggerName(schedule.getId()), spaceID)
						.withSchedule(cronSchedule(schedule.getExpression()).inTimeZone(TimeZone.getTimeZone(timeZoneID)))
						.forJob(jobDetail)
						.build();
						scheduler.scheduleJob(cronTrigger);
					}
				}
			}
			BDSInfo connInfo = BDSJobOperation.getBDSInfo(jobID);
			return connInfo;
		} finally {
			if (conn != null) {
				try
				{
					conn.close();
				} catch(Exception ex) {
					logger.warn("Exception in closing connection : ",ex);
				}
			}
		}
	}
	
	public static void addAndRunNowBDSJob(String spaceID, BDSInfo bdsInfo) 
		throws SQLException, SchedulerBaseException, ClassNotFoundException, SchedulerException, IOException {
		// If any schedule is run now, then create a new job which is non-durable. 
		// It gets cleaned after it is run and no trigger are associated with it.
		JobBuilder jobBuilder = Util.getBirstJobBuilder(BDSJob.class, Util.getBDSCustJobName(bdsInfo.getId()), 
				Util.getCustJobGroupNameForBDS(spaceID), false);
		JobDetail jobDetail = jobBuilder.build();
		runBDSNow(spaceID, bdsInfo.getId(), jobDetail);
	}
	
	public static BDSInfo copyChanges(BDSInfo bdsInfo1, BDSInfo bdsInfo2, boolean enableNewSchedule) {
		// Check if schedules are exist.
		if (bdsInfo1 == null) {
			Schedule[] bdsInfo2JobSchedules = bdsInfo2.getJobSchedules();
			for (Schedule bdsInfo2Schedule : bdsInfo2JobSchedules) {
				boolean enablebdsInfo2Schedule = enableNewSchedule ? bdsInfo2Schedule.isEnable() : false;
				bdsInfo2Schedule.setEnable(enablebdsInfo2Schedule);
			}
			return bdsInfo2;
		}
		
		// Initialize for copying info.
		BDSInfo newBDSInfo = new BDSInfo();
		String bdsInfoJobId = bdsInfo2.getId();		
		String spaceId = bdsInfo2.getSpaceId();
		String dataSourceId = bdsInfo2.getDataSourceId();
		String userId = bdsInfo2.getUserId();
		
		// Initialize load date info.
		String loadDate1 = bdsInfo1.getLoadDate();
		String loadDate2 = bdsInfo2.getLoadDate();	
		if (loadDate2 == null || (loadDate1 != null && !CompareUtils.compare(loadDate1, loadDate2, true))) {
			loadDate2 = loadDate1;
		}
		
		// Initialize sub-group info.
		String group1 = bdsInfo1.getGroup();
		String group2 = bdsInfo2.getGroup();	
		if (group2 == null || (group1 != null && !CompareUtils.compare(group1, group2, true))) {
			group2 = group1;
		}
		
		// Initialize send email info.
		boolean sendEmail1 = bdsInfo1.getSendEmail();
		boolean sendEmail2 = bdsInfo2.getSendEmail();
		if (sendEmail1 != sendEmail2) {
			sendEmail2 = sendEmail1;
		}
		
		// Initialize process after info.
		boolean processAfter1 = bdsInfo1.isProcessAfter();
		boolean processAfter2 = bdsInfo2.isProcessAfter();
		if (processAfter1 != processAfter2) {
			processAfter2 = processAfter1;
		}
		
		// Initialize run now info.
		boolean runNowMode1 = bdsInfo1.isRunNowMode();
		boolean runNowMode2 = bdsInfo2.isRunNowMode();
		if (runNowMode1 != runNowMode2) {
			runNowMode2 = runNowMode1;
		}		
		
		// Initialize job schedule info.
		Schedule[] jobSchedules1 = bdsInfo1.getJobSchedules();
		Schedule[] jobSchedules2 = bdsInfo2.getJobSchedules();
		if (jobSchedules1 != null && jobSchedules1.length > 0) {
			// Just copy everything from 1 to 2.
			List<Schedule> newSchedules = new ArrayList<Schedule>();
	    	for (Schedule schedule1 : jobSchedules1) {
	    		String scheduleId = UUID.randomUUID().toString();
	    		boolean enableSchedule = enableNewSchedule ? schedule1.isEnable() : false;
	    		Schedule newSchedule = new Schedule(scheduleId, bdsInfoJobId, schedule1.getExpression(), enableSchedule, schedule1.getTimeZone());
	    		newSchedule.setStartDate(schedule1.getStartDate());
	    		newSchedules.add(newSchedule);
	    	}
	    	jobSchedules2 = (Schedule [])newSchedules.toArray(new Schedule[newSchedules.size()]);
		}
		
		// Set job info.
		newBDSInfo.setId(bdsInfoJobId);
		newBDSInfo.setSpaceId(spaceId);
		newBDSInfo.setUserId(userId);
		newBDSInfo.setDataSourceId(dataSourceId);
		newBDSInfo.setLoadDate(loadDate2);
		newBDSInfo.setProcessAfter(processAfter2);
		newBDSInfo.setScheduledMode(runNowMode2);
		newBDSInfo.setSendEmail(sendEmail2);
		newBDSInfo.setGroup(group2);
		newBDSInfo.setJobSchedules(jobSchedules2);
		return newBDSInfo;
	}
	
	public static void flagAllSchedules(String spaceID, String bdsInfoID, boolean enable) 
	throws SchedulerException, SchedulerBaseException,
		SQLException, ParseException, ClassNotFoundException 
	{
		Scheduler scheduler = SchedulerServerContext.getSchedulerInstance();
		flagAllSchedules(scheduler, spaceID, bdsInfoID, enable);
	}
	
	public static void flagAllSchedules(Scheduler scheduler, String spaceID, String bdsInfoID, boolean enable) 
		throws SchedulerException, SchedulerBaseException, SQLException, ParseException, ClassNotFoundException 
	{
		BDSInfo bdsInfo = BDSJobOperation.getBDSInfo(bdsInfoID);
		if(bdsInfo != null)
		{
			Schedule[] schedules = bdsInfo.getJobSchedules();
			for(Schedule schedule : schedules)
			{
				schedule.setEnable(enable);
			}
			BDSJobOperation.updateBDSInfo(spaceID, bdsInfo);
		}
	}
	
	public static BDSInfo getBDSInfo(String bdsInfoId) throws SQLException, SchedulerBaseException, ClassNotFoundException {		
		Connection conn = null;
		try {
			conn = SchedulerDBConnection.getInstance().getConnectionPool().getConnection();
			String schema = UtilConstants.SCHEDULER_DB_SCHEMA;
			return SchedulerDatabase.getBDSInfo(conn, schema, bdsInfoId);
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch(Exception ex) {
					logger.warn("Exception in closing connection : ",ex);
				}
			}
		}
	}
	
	/**
	 * This method should return only one BDS job which is scheduled.
	 * If it returns more than one job, there is some problem. Handle it on the client side.
	 */
	@Override
	public void getAllJobs(SchedulerWebServiceResult result, String spaceID, String onlyCurrentExecuting, 
			String userID, String filterOnUser) throws Exception {
		OMFactory factory = OMAbstractFactory.getOMFactory();
		OMNamespace ns = null;
		OMElement scheduledJobs = factory.createOMElement(JobConstants.JOBS_LIST_NODE, ns);
		AxisXmlUtils.addContent(factory, scheduledJobs, JobConstants.PRIMARY_SCHEDULER_NAME, SchedulerServerContext.getPrimarySchedulerDisplayName(), ns);
		fillAllJobs(spaceID, onlyCurrentExecuting, userID, filterOnUser, factory, ns, scheduledJobs);
		result.setResult(new XMLWebServiceResult(scheduledJobs));
	}
	
	public static void fillAllJobs(String spaceID, String onlyCurrentExecuting, String userID, String filterOnUser, 
			OMFactory factory, OMNamespace ns, OMElement scheduledJobs) throws Exception {
		// Set up to retrieve all scheduler information.
		boolean returnCurrentExecuting = Boolean.valueOf(onlyCurrentExecuting);
		boolean filterByUser = Boolean.parseBoolean(filterOnUser);
		Map<String, String> userIdToNameMapping = new HashMap<String, String>();

		// Get the bds info from all schedulers.
		Map<String,SchedulerVersion> schedulersMap = SchedulerServerContext.getAllSchedulers();
		for (Entry<String,SchedulerVersion> entry : schedulersMap.entrySet()) {
			String schedulerDisplayName = entry.getKey();
			SchedulerVersion schedulerVersion = entry.getValue();
			Scheduler scheduler = schedulerVersion.getScheduler();
			List<BDSInfo> bdsInfoList = BDSJobOperation.getBDSInfo(scheduler, spaceID, filterByUser ? userID:null);
			if (bdsInfoList != null) {										
				for (BDSInfo bdsInfo : bdsInfoList) {
					// If only current executing jobs are required, filter on the status.
					if (returnCurrentExecuting && !bdsInfo.isCurrentExecutionStatus()) {
						continue;
					}

					// Filter out the run now mode jobs.
					if (bdsInfo.isRunNowMode()) {
						continue;
					}
					OMElement sc = factory.createOMElement(JobConstants.JOB_NODE, ns);
					AxisXmlUtils.addContent(factory, sc, JobConstants.SCHEDULER_NAME, scheduler.getSchedulerName(), ns);
					AxisXmlUtils.addContent(factory, sc, JobConstants.SCHEDULER_DISPLAY_NAME, schedulerDisplayName, ns);
					AxisXmlUtils.addContent(factory, sc, JobConstants.SCHEDULER_ENABLE_MIGRATION, schedulerVersion.getEnableMigration(), ns);
					AxisXmlUtils.addContent(factory, sc, JobConstants.IS_PRIMARY_SCHEDULER, Util.isPrimaryScheduler(scheduler), ns);
					AxisXmlUtils.addContent(factory, sc, JobConstants.JOB_ID, bdsInfo.getId(), ns);
					AxisXmlUtils.addContent(factory, sc, JobConstants.JOB_NAME, bdsInfo.getName(), ns);
					AxisXmlUtils.addContent(factory, sc, JobConstants.JOB_CONN_RUN_NOW_MODE, bdsInfo.isRunNowMode(), ns);
					AxisXmlUtils.addContent(factory, sc, JobConstants.JOB_CONN_PROCESS_AFTER, bdsInfo.isProcessAfter(), ns);
					AxisXmlUtils.addContent(factory, sc, JobConstants.JOB_SPACE_ID, bdsInfo.getSpaceId(), ns);
					AxisXmlUtils.addContent(factory, sc, JobConstants.JOB_TYPE, UtilConstants.JobType.BDS.toString(), ns);
					AxisXmlUtils.addContent(factory, sc, JobConstants.JOB_USER_ID, bdsInfo.getUserId(), ns);						
					AxisXmlUtils.addContent(factory, sc, JobConstants.JOB_EXECUTION_STATUS, bdsInfo.isCurrentExecutionStatus(), ns);
					AxisXmlUtils.addContent(factory, sc, JobConstants.JOB_CREATED_DATE, bdsInfo.getCreatedDate(), ns);
					AxisXmlUtils.addContent(factory, sc, JobConstants.JOB_MODIFIED_DATE, bdsInfo.getModifiedDate(), ns);
					AxisXmlUtils.addContent(factory, sc, JobConstants.JOB_MODIFIED_BY, bdsInfo.getModifiedBy(), ns);

					// Get the usernames from the map, if not present, get from acorn db and store it.
					String createdUserName = Util.getAndAddUserFromMap(userIdToNameMapping, bdsInfo.getUserId());
					AxisXmlUtils.addContent(factory, sc, JobConstants.JOB_CREATED_USER, createdUserName, ns);

					// The job is scheduled to run if any of its schedule is enabled.
					OMElement schedules = factory.createOMElement(JobConstants.JOB_SCHEDULE_LIST, ns);
					Schedule[] jobSchedules = bdsInfo.getJobSchedules();
					boolean isJobEnabled = Util.isAnyScheduleEnabled(jobSchedules);
					AxisXmlUtils.addContent(factory, sc, JobConstants.JOB_RUN, isJobEnabled, ns);
					if (jobSchedules != null && jobSchedules.length > 0) {
						for (Schedule jobSchedule : jobSchedules) {
							OMElement schedule = factory.createOMElement(JobConstants.JOB_SCHEDULE, ns);
							AxisXmlUtils.addContent(factory, schedule, JobConstants.JOB_SCHEDULE_ID, jobSchedule.getId(), ns);								
							AxisXmlUtils.addContent(factory, schedule, JobConstants.JOB_SCHEDULE_JOB_ID, jobSchedule.getJobId(), ns);
							AxisXmlUtils.addContent(factory, schedule, JobConstants.JOB_SCHEDULE_EXPRESSION, jobSchedule.getExpression(), ns);
							AxisXmlUtils.addContent(factory, schedule, JobConstants.JOB_SCHEDULE_TIMEZONE, jobSchedule.getTimeZone(), ns);
							AxisXmlUtils.addContent(factory, schedule, JobConstants.JOB_SCHEDULE_ENABLE, jobSchedule.isEnable(), ns);
							schedules.addChild(schedule);
						}
					}
					sc.addChild(schedules);
					scheduledJobs.addChild(sc);
				}
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	public static List<BDSInfo> getBDSInfo(Scheduler scheduler, String spaceID, String userID) 
		throws SchedulerException, SQLException, SchedulerBaseException, ClassNotFoundException {	
		Set<JobKey> jobKeys = new HashSet<JobKey>();
		if (spaceID.equalsIgnoreCase(UtilConstants.ALL_SPACES)) {
			for (String groupName : scheduler.getJobGroupNames()) {
				if (groupName.startsWith(UtilConstants.PREFIX_BDS_JOB_GROUP_NAME)) {
					jobKeys.addAll(scheduler.getJobKeys(GroupMatcher.groupEquals(groupName)));
				}
			}
		} else {
			String findGroupName = Util.getCustJobGroupNameForBDS(spaceID);
			jobKeys = scheduler.getJobKeys(GroupMatcher.groupEquals(findGroupName));
		}
		return getBDSInfo(scheduler, spaceID, jobKeys, userID);
	}

	public static List<BDSInfo> getBDSInfo(Scheduler scheduler, String spaceID, Set<JobKey> jobKeys, String userID) 
		throws SchedulerException, SQLException, SchedulerBaseException, ClassNotFoundException {		
		List<BDSInfo> bdsInfoList = new ArrayList<BDSInfo>();
		if (jobKeys != null && jobKeys.size() > 0) {							
			List<String> currentlyExecutingJobs = Util.isPrimaryScheduler(scheduler) ? 
					Util.getRunningJobs(spaceID.equalsIgnoreCase(UtilConstants.ALL_SPACES) ? null : spaceID) : null;
			Connection conn = null;
			try {
				conn = SchedulerDBConnection.getInstance().getConnectionPool().getConnection();
				String schema = UtilConstants.SCHEDULER_DB_SCHEMA;
				for (JobKey jobKey : jobKeys) {
					String bdsJobName = jobKey.getName();
					String bdsJobGroupName = jobKey.getGroup();
					String bdsJobID = Util.getObjectID(bdsJobName, UtilConstants.PREFIX_BDS_JOB);
					if (bdsJobID != null) {
						BDSInfo bdsInfo = SchedulerDatabase.getBDSInfo(conn, schema, bdsJobID);
						if (bdsInfo == null) {
							logger.warn("Cannot find bds info for " + bdsJobID);
							if (Util.isPrimaryScheduler(scheduler)) {
								// In this case, we should just delete the job from the scheduler.
								scheduler.deleteJob(new JobKey(bdsJobName, bdsJobGroupName));
							}
							continue;
						}

						// Filter on user ids.
						if (userID != null && userID.length() > 0 && !userID.equalsIgnoreCase(bdsInfo.getUserId())) {
							continue;
						}
						bdsInfo.currentExecutionStatus = currentlyExecutingJobs != null ? currentlyExecutingJobs.contains(bdsJobName) : false;
						bdsInfoList.add(bdsInfo);
					}
				}
			}
			finally {					
				if (conn != null) {
					try {
						conn.close();
					} catch (Exception ex) {}
				}
			}
		}		
		return bdsInfoList;
	}
	
	@Override
	public void getJobDetails(SchedulerWebServiceResult result, String spaceID, String jobID) throws Exception {
		OMFactory factory = OMAbstractFactory.getOMFactory();
		OMNamespace ns = null;			
		Map<String, String> userIdToNameMapping = new HashMap<String, String>();
		OMElement scheduledJobs = factory.createOMElement(JobConstants.JOBS_LIST_NODE, ns);
		OMElement sc = factory.createOMElement(JobConstants.JOB_NODE, ns);
		BDSInfo scheduledJobDetails = getBDSInfo(jobID);

		if(scheduledJobDetails != null)
		{
			AxisXmlUtils.addContent(factory, sc, JobConstants.JOB_ID, scheduledJobDetails.getId(), ns);
			AxisXmlUtils.addContent(factory, sc, JobConstants.JOB_NAME, scheduledJobDetails.getName(), ns);
			AxisXmlUtils.addContent(factory, sc, JobConstants.JOB_CONN_RUN_NOW_MODE, scheduledJobDetails.isRunNowMode(), ns);
			AxisXmlUtils.addContent(factory, sc, JobConstants.JOB_CONN_PROCESS_AFTER, scheduledJobDetails.isProcessAfter(), ns);
			AxisXmlUtils.addContent(factory, sc, JobConstants.JOB_SPACE_ID, scheduledJobDetails.getSpaceId(), ns);
			AxisXmlUtils.addContent(factory, sc, JobConstants.JOB_TYPE, scheduledJobDetails.getType(), ns);
			AxisXmlUtils.addContent(factory, sc, JobConstants.JOB_USER_ID, scheduledJobDetails.getUserId(), ns);						
			AxisXmlUtils.addContent(factory, sc, JobConstants.JOB_EXECUTION_STATUS, scheduledJobDetails.isCurrentExecutionStatus(), ns);
			AxisXmlUtils.addContent(factory, sc, JobConstants.JOB_CREATED_DATE, scheduledJobDetails.getCreatedDate(), ns);
			AxisXmlUtils.addContent(factory, sc, JobConstants.JOB_MODIFIED_DATE, scheduledJobDetails.getModifiedDate(), ns);
			AxisXmlUtils.addContent(factory, sc, JobConstants.JOB_MODIFIED_BY, scheduledJobDetails.getModifiedBy(), ns);

			// Get the usernames from the map, if not present, get from acorn db and store it.
			String createdUserName = Util.getAndAddUserFromMap(userIdToNameMapping, scheduledJobDetails.getUserId());
			AxisXmlUtils.addContent(factory, sc, JobConstants.JOB_CREATED_USER, createdUserName, ns);

			// The job is scheduled to run if any of its schedule is enabled.
			OMElement schedules = factory.createOMElement(JobConstants.JOB_SCHEDULE_LIST, ns);
			Schedule[] jobSchedules = scheduledJobDetails.getJobSchedules();
			boolean isJobEnabled = Util.isAnyScheduleEnabled(jobSchedules);
			AxisXmlUtils.addContent(factory, sc, JobConstants.JOB_RUN, isJobEnabled, ns);
			if (jobSchedules != null && jobSchedules.length > 0) {
				for (Schedule jobSchedule : jobSchedules) {
					OMElement schedule = factory.createOMElement(JobConstants.JOB_SCHEDULE, ns);
					AxisXmlUtils.addContent(factory, schedule, JobConstants.JOB_SCHEDULE_ID, jobSchedule.getId(), ns);								
					AxisXmlUtils.addContent(factory, schedule, JobConstants.JOB_SCHEDULE_JOB_ID, jobSchedule.getJobId(), ns);
					AxisXmlUtils.addContent(factory, schedule, JobConstants.JOB_SCHEDULE_EXPRESSION, jobSchedule.getExpression(), ns);
					AxisXmlUtils.addContent(factory, schedule, JobConstants.JOB_SCHEDULE_TIMEZONE, jobSchedule.getTimeZone(), ns);
					AxisXmlUtils.addContent(factory, schedule, JobConstants.JOB_SCHEDULE_ENABLE, jobSchedule.isEnable(), ns);
					schedules.addChild(schedule);
				}
			}
			sc.addChild(schedules);
			scheduledJobs.addChild(sc);
		}
		result.setResult(new XMLWebServiceResult(scheduledJobs));
	}
	
	@Override
	public void getJobStatus(SchedulerWebServiceResult result, String spaceID, String jobOptionsXmlString) throws Exception	{
		// Get job status info.
		OMFactory factory = OMAbstractFactory.getOMFactory();
		OMNamespace ns = null;
		OMElement status = factory.createOMElement(JobConstants.JOB_SP_STATUS_NODE, ns);
		SpaceStatus spaceStatus = StatusUtils.getJobStatus(spaceID, -1, null);

		// Based on the space status mark it as available for operation.
		boolean available = false;
		if (spaceStatus != null) {
			if (spaceStatus.getStatus() != StepStatus.RUNNING.getStatus()) {
				available = true;
			}
			AxisXmlUtils.addContent(factory, status, JobConstants.JOB_SP_AVAILABLE, available, ns);
			AxisXmlUtils.addContent(factory, status, JobConstants.JOB_SP_STATUS, spaceStatus.getStatus(), ns);
			AxisXmlUtils.addContent(factory, status, JobConstants.JOB_SP_STEP, spaceStatus.getStep(), ns);
			AxisXmlUtils.addContent(factory, status, JobConstants.JOB_SP_LOAD_ID, spaceStatus.getLoadNumber(), ns);
			AxisXmlUtils.addContent(factory, status, JobConstants.JOB_SP_LOAD_GROUP, spaceStatus.getLoadGroup(), ns);
			AxisXmlUtils.addContent(factory, status, JobConstants.JOB_SP_CREATED_DATE, spaceStatus.getCreatedDate(), "MM/dd/yyyy HH:mm:ss", ns);
			AxisXmlUtils.addContent(factory, status, JobConstants.JOB_SP_MODIFIED_DATE, spaceStatus.getModifiedDate(), "MM/dd/yyyy HH:mm:ss", ns);
		} else {
			available = true;
			AxisXmlUtils.addContent(factory, status, JobConstants.JOB_SP_AVAILABLE, available, ns);
		}
		result.setResult(new XMLWebServiceResult(status));
	}
	
	public static BDSInfo getNewCopiedBDSInfo(BDSInfo oldBDSInfo) {
		String jobId = UUID.randomUUID().toString();		
		String spaceId = oldBDSInfo.getSpaceId();
		String dataSourceId = oldBDSInfo.getDataSourceId();
		String userId = oldBDSInfo.getUserId();
    	String loadDate = oldBDSInfo.getLoadDate();
    	boolean sendEmail = oldBDSInfo.getSendEmail();
    	String group = oldBDSInfo.getGroup();
    	boolean processAfter = oldBDSInfo.isProcessAfter();
    	// Ideally, this should be false for all the copied bds info -- should be set at the calling function stack level.
    	boolean runNowMode = oldBDSInfo.isRunNowMode();
    	
    	// Create a new schedule instance for copy.
    	Schedule[] jobSchedules = oldBDSInfo.getJobSchedules();
    	List<Schedule> newSchedules = new ArrayList<Schedule>();
    	for (Schedule oldSchedule : jobSchedules) {
    		String scheduleId = UUID.randomUUID().toString();
    		Schedule newSchedule = new Schedule(scheduleId, jobId, oldSchedule.getExpression(), oldSchedule.isEnable(), oldSchedule.getTimeZone());
    		newSchedule.setStartDate(oldSchedule.getStartDate());
    		newSchedules.add(newSchedule);
    	}
    	
    	// Set information for new copied schedule.
    	BDSInfo newBDSInfo = new BDSInfo();
    	newBDSInfo.setId(jobId);
    	newBDSInfo.setSpaceId(spaceId);
    	newBDSInfo.setUserId(userId);
    	newBDSInfo.setDataSourceId(dataSourceId);
    	newBDSInfo.setLoadDate(loadDate);
    	newBDSInfo.setSendEmail(sendEmail);
    	newBDSInfo.setGroup(group);
    	newBDSInfo.setProcessAfter(processAfter);
    	newBDSInfo.setScheduledMode(runNowMode);
    	newBDSInfo.setJobSchedules((Schedule [])newSchedules.toArray(new Schedule[newSchedules.size()]));
		return newBDSInfo;
	}
	
	@Override
	public void killJob(SchedulerWebServiceResult result, String spaceID, String jobsRunStatusXmlString) throws Exception {
		// TODO Auto-generated method stub
	}
	
	@SuppressWarnings("unchecked")
	public static BDSInfo parseOptionsXmlString(String spaceId, String userId, String jobOptionsXmlString) {
		// Initialize BDS info.
		BDSInfo bdsInfo = null;
		if (jobOptionsXmlString == null || jobOptionsXmlString.trim().length() == 0) {
			bdsInfo = new BDSInfo();
			bdsInfo.setSpaceId(spaceId);
			bdsInfo.setUserId(userId);
			return bdsInfo;
		}
		
		// Parse job option xml.
		XMLStreamReader reader = null;
		try
		{	
			reader = XMLInputFactory.newInstance().createXMLStreamReader(new StringReader(jobOptionsXmlString));
			StAXOMBuilder builder = new StAXOMBuilder(reader);
			OMElement docElement = builder.getDocumentElement();
			docElement.build();
			docElement.detach();
			OMElement element = docElement.getFirstChildWithName(new QName("", JobConstants.JOB_BDS_NODE));

			// Check if the root element itself is bdsInfo.
			if (element == null) {
				if (docElement.getLocalName() == JobConstants.JOB_BDS_NODE) {
					element = docElement;
				}
			}
			if (element != null) {
				bdsInfo = new BDSInfo();
				bdsInfo.setSpaceId(spaceId);
				bdsInfo.setUserId(userId);
				Iterator<OMElement> iterator = element.getChildElements();
				while(iterator.hasNext()) {
					OMElement childElement = iterator.next();
					String elementName = childElement.getLocalName();
					String elementText = childElement.getText();

					if (JobConstants.JOB_ID.equals(elementName)) {	
						bdsInfo.setId(elementText);
					}
					
					if (JobConstants.JOB_DS_ID.equals(elementName)) {	
						bdsInfo.setDataSourceId(elementText);
					}
					
					if (JobConstants.JOB_NAME.equals(elementName)) {	
						bdsInfo.setName(elementText);
					}
					
					if (JobConstants.JOB_TYPE.equals(elementName)) {	
						bdsInfo.setType(elementText);
					}
					
					if (JobConstants.JOB_CONN_RUN_NOW_MODE.equals(elementName)) {	
						bdsInfo.setScheduledMode(Boolean.parseBoolean(elementText));
					}
					
					if (JobConstants.JOB_CONN_PROCESS_AFTER.equals(elementName)) {	
						bdsInfo.setProcessAfter(Boolean.parseBoolean(elementText));
					}
					
					if (JobConstants.JOB_SP_LOADDATE.equals(elementName)) {	
						bdsInfo.setLoadDate(elementText);
					}
					
					if (JobConstants.JOB_SP_LOAD_GROUP.equals(elementName)) {	
						bdsInfo.setGroup(elementText);
					}
					
					if (JobConstants.JOB_SEND_EMAIL.equals(elementName)) {	
						bdsInfo.setSendEmail(Boolean.parseBoolean(elementText));
					}
					
					if (JobConstants.JOB_MODIFIED_BY.equals(elementName)) {	
						bdsInfo.setModifiedBy(elementText);
					}
				}
			}
		} catch (XMLStreamException e) {
			logger.error(e,e);
		} catch (FactoryConfigurationError e) {
			logger.error(e,e);
		}
		return bdsInfo;
	}
	
	@SuppressWarnings("unchecked")
	public static BDSInfo parseScheduleDetailsXmlString(String spaceID, String userID, BDSInfo bdsInfo, String jobOptionsXmlString)
	{
		if (bdsInfo == null) {
			bdsInfo = new BDSInfo();
		}
		XMLStreamReader reader = null;
		try {
			reader = XMLInputFactory.newInstance().createXMLStreamReader(new StringReader(jobOptionsXmlString));
			StAXOMBuilder builder = new StAXOMBuilder(reader);
			OMElement docElement = builder.getDocumentElement();
			docElement.build();
			docElement.detach();
			OMElement element = docElement.getFirstChildWithName(new QName("", JobConstants.JOB_BDS_NODE));
			
			// See if the root element iteself is BDSInfo.
			if (element == null) {
				if (docElement.getLocalName() == JobConstants.JOB_BDS_NODE) {
					element = docElement;
				}
			}
			if (element != null) {		
				Iterator<OMElement> iterator = element.getChildElements();
				while(iterator.hasNext()) {
					OMElement childElement = iterator.next();
					String elementName = childElement.getLocalName();
					if(JobConstants.JOB_SCHEDULE_LIST.equals(elementName)) {
						List<Schedule> schedules = Util.parseSchedules(childElement);
						bdsInfo.setJobSchedules((Schedule[])schedules.toArray(new Schedule[schedules.size()]));
					}
				}
			}
		} catch (XMLStreamException e) {
			logger.error(e, e);
		} catch (FactoryConfigurationError e) {
			logger.error(e, e);
		}
		return bdsInfo;
	}

	@Override
	public void pauseJob(SchedulerWebServiceResult result, String spaceID, String reportID)
		throws SchedulerException, SchedulerBaseException, SQLException,
	ParseException, ClassNotFoundException
	{
		flagAllSchedules(spaceID, reportID, false);
	}

	@Override
	public void removeJob(SchedulerWebServiceResult result, String spaceID, String jobID) throws Exception {
		Scheduler scheduler = SchedulerServerContext.getSchedulerInstance();
		if (removeBDSInfo(scheduler, spaceID, jobID)) {
			// THERE SHOULD BE ONLY ONE BDS JOB PER SPACE (RunNowMode = 0).
			// If there are more than 1 bds job (runNowMode=0), there is some problem since we only show front end users only one job. 
			// Just delete the remnants (in case). 
			// The only case I can think of if schedulers are configured wrong such that one of the releases is missed in configuration.
			// removeAllScheduledBDSJobs(spaceID);
		}
	}
	
	public static void removeAllScheduledBDSJobs(String spaceID) throws SQLException, SchedulerBaseException, ClassNotFoundException {
		Connection conn = null;		
		try {
			conn = SchedulerDBConnection.getInstance().getConnectionPool().getConnection();
			String schema = UtilConstants.SCHEDULER_DB_SCHEMA;
			SchedulerDatabase.deleteAllBDSScheduledJobs(conn, schema, spaceID);
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (Exception ex) {}
			}
		}
	}
	
	public static boolean removeBDSInfo(Scheduler scheduler, String spaceID, String bdsInfoId) 
		throws SQLException, SchedulerBaseException, ClassNotFoundException, SchedulerException {
		Connection conn = null;		
		try {
			// Set up removing BDS info.
			conn = SchedulerDBConnection.getInstance().getConnectionPool().getConnection();
			String schema = UtilConstants.SCHEDULER_DB_SCHEMA;
			BDSInfo bdsInfo = SchedulerDatabase.getBDSInfo(conn, schema, bdsInfoId);
			if (bdsInfo == null) {
				return false;
			}

			// Check if any jobs are being scheduled.
			String jobName = Util.getBDSCustJobName(bdsInfo.getId()); 
			String groupName = Util.getCustJobGroupNameForBDS(spaceID);
			List<String> runningJobs = Util.getRunningJobs(jobName, groupName);
			if (runningJobs != null && runningJobs.size() > 0) {
				// Schedule is in progress. Cannot remove bds info job.
				throw new JobRunningException("BDS job currently running " + bdsInfoId );
			}
			
			// Remove scheduleInfo from scheduler and database.
			scheduler.deleteJob(new JobKey(jobName, groupName));
			Schedule[] schedules = bdsInfo.getJobSchedules();
			if (schedules != null && schedules.length > 0) {
				for (Schedule schedule : schedules) {
					SchedulerDatabase.removeSchedule(conn, schema, schedule.getId());
				}
			}
			SchedulerDatabase.deleteBDSInfo(conn, schema, bdsInfoId);
			return true;
		}
		finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (Exception ex) {}
			}
		}
	}
		
	@Override
	public void removeScheduleFromJob(SchedulerWebServiceResult result, String spaceID, String jobID, String scheduleID) 
		throws Exception {
		// TODO Auto-generated method stub
	}

	@Override
	public void resumeJob(SchedulerWebServiceResult result, String spaceID, String typeID) throws Exception {
		// Type id is the primary id for the table, not the job id.
		flagAllSchedules(spaceID, typeID, true);
	}
	
	public static void runBDSNow(String spaceID, String bdsInfoId) 
		throws SQLException, SchedulerBaseException, ClassNotFoundException, SchedulerException, IOException {
		runBDSNow(spaceID, bdsInfoId, null);
	}
	
	/**
	 * If the jobdetail is null, the method implies that job has already been 
	 * added to the scheduler and can be inferred via bdsInfoId.
	 * If on the other hand, jobdetail is specified, then that jobDetail is used
	 * to schedule immediate run
	 * @param spaceID
	 * @param bdsInfoId
	 * @param jobDetail
	 * @throws SQLException
	 * @throws SchedulerBaseException
	 * @throws ClassNotFoundException
	 * @throws SchedulerException
	 * @throws IOException 
	 */
	public static void runBDSNow(String spaceID, String bdsInfoId, JobDetail jobDetail) 
		throws SQLException, SchedulerBaseException, ClassNotFoundException, SchedulerException, IOException {
	// Check if another job is running.
	SpaceStatus anyConcurrentSpaceStatus = StatusUtils.getActiveSpaceStatus(spaceID);
	if (anyConcurrentSpaceStatus != null) {
		throw new ConcurrentSpaceOperationException("Another job is running for this space : " + anyConcurrentSpaceStatus.toString());
	}
	
		// Schedule the job to run now.
		Connection conn = null;
		try {
			conn = SchedulerDBConnection.getInstance().getConnectionPool().getConnection();
			String schema = UtilConstants.SCHEDULER_DB_SCHEMA;
			SpaceStatus spaceStatus = new SpaceStatus(UUID.randomUUID().toString(), spaceID, bdsInfoId, -1, null, Step.BDSExtract, StepStatus.RUNNING); 
			SchedulerDatabase.addSpaceStatus(conn, schema, spaceStatus);
			
			// Set up job information and run it.
			// Put in the extract lock file to signal the extraction in progress.
			// This takes care of backward compatibility extraction detection logic.
			Scheduler scheduler = SchedulerServerContext.getSchedulerInstance();
			JobDetail bdsJobDetail = null;
			if (jobDetail == null) {
				bdsJobDetail = scheduler.getJobDetail(new JobKey(Util.getBDSCustJobName(bdsInfoId), spaceID));
			} else {
				bdsJobDetail = jobDetail;
			}
			String uidString = UUID.randomUUID().toString();
			Trigger trigger = Util.getImmediateTrigger(spaceID, bdsJobDetail, uidString);
			trigger.getJobDataMap().put(UtilConstants.TRIGGER_MAP_SCHEDULE_STEP, spaceStatus.getId());
			scheduler.scheduleJob(bdsJobDetail, trigger);
			Util.createBDSExtractFile(spaceID);
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (Exception ex) {}
			}
		}
	}

	@Override
	public void runNow(SchedulerWebServiceResult result, String spaceID, String typeID) throws Exception {
		try {
			Scheduler scheduler = SchedulerServerContext.getSchedulerInstance();
			JobDetail jobDetail = scheduler.getJobDetail(new JobKey(Util.getBDSCustJobName(typeID), Util.getCustJobGroupNameForBDS(spaceID)));
			String uidString = UUID.randomUUID().toString();
			Trigger trigger = Util.getImmediateTrigger(spaceID, jobDetail, uidString);
			trigger.getJobDataMap().put(UtilConstants.TRIGGER_MAP_SCHEDULE_STEP, typeID);
			scheduler.scheduleJob(trigger);
		} catch (Exception e)
		{
			logger.error("Exception in adding job: ", e);
			throw new SchedulerException();
		}
	}

	@Override
	public void updateJob(SchedulerWebServiceResult result, String spaceID, String userID, String jobID, String jobOptionsXmlString)
			throws Exception {
		// Update job information based on user input.
		BDSInfo bdsInfo = BDSJobOperation.parseOptionsXmlString(spaceID, userID, jobOptionsXmlString);
		bdsInfo = BDSJobOperation.parseScheduleDetailsXmlString(spaceID, userID, bdsInfo, jobOptionsXmlString);
		bdsInfo.setId(jobID);
		if (bdsInfo.getSpaceId() == null) {
			bdsInfo.setSpaceId(spaceID);
		}
		BDSJobOperation.updateBDSInfo(spaceID, bdsInfo);
	}

	public static BDSInfo updateBDSInfo(String spaceID, BDSInfo bdsInfo)  
		throws SchedulerException, SchedulerBaseException, SQLException, ParseException, ClassNotFoundException {
		Scheduler scheduler = SchedulerServerContext.getSchedulerInstance();
		return updateBDSInfo(scheduler, spaceID, bdsInfo);
	}
	
	public static BDSInfo updateBDSInfo(Scheduler scheduler, String spaceID, BDSInfo updatedBdsInfo) 
		throws SchedulerException, SchedulerBaseException, SQLException, ParseException, ClassNotFoundException {
		// Basically remove the old one and create a new one.
		Connection conn = null;
		try {
			conn = SchedulerDBConnection.getInstance().getConnectionPool().getConnection();
			String schema = UtilConstants.SCHEDULER_DB_SCHEMA;
			String bdsInfoId = updatedBdsInfo.getId();
			BDSInfo sInfo = SchedulerDatabase.getBDSInfo(conn, schema, bdsInfoId);
			if (sInfo == null) {
				logger.warn("No Scheduled BDS Info found for : " + bdsInfoId + ".");
				return null;
			}

			// The created user id should never change. Modified by user can change.
			if (updatedBdsInfo.getUserId() == null) {
				updatedBdsInfo.setUserId(sInfo.getUserId());
			}
			
			// Determine if job schedules have changed.
			Schedule[] updatedSchedules = updatedBdsInfo.getJobSchedules();
			boolean changed = !sInfo.equals(updatedBdsInfo);
			if (changed) {
				SchedulerDatabase.updateBDSInfo(conn, schema, updatedBdsInfo);
				sInfo = SchedulerDatabase.getBDSInfo(conn, schema, bdsInfoId);
			}

			// Retrieve existing schedules.
			List<Schedule> existingSchedules = new ArrayList<Schedule>();
			if (sInfo.getJobSchedules() != null && sInfo.getJobSchedules().length > 0) {
				existingSchedules = Arrays.asList(sInfo.getJobSchedules());
			}

			// Schedules to be added and removed.
			List<Schedule> toBeAdded = new ArrayList<Schedule>();			
			List<Schedule> toBeRemoved = new ArrayList<Schedule>(existingSchedules);
			if (updatedSchedules != null && updatedSchedules.length > 0) {
				for (Schedule updatedSchedule : updatedSchedules) {
					Schedule current = null;
					for (Schedule existingSchedule : existingSchedules) {
						if (existingSchedule.getId().equalsIgnoreCase(updatedSchedule.getId())) {
							current = existingSchedule;
							break;
						}
					}

					if (current == null) {
						toBeAdded.add(updatedSchedule);
					} else {
						if (current.equals(updatedSchedule)) {
							toBeRemoved.remove(current);						
						} else {
							toBeAdded.add(updatedSchedule);
						}
					}					
				}
			}
			
			// Delete schedules.
			for (Schedule schedule : toBeRemoved) {
				scheduler.unscheduleJob(new TriggerKey(Util.getBDSCustTriggerName(schedule.getId()), sInfo.getSpaceId()));
				SchedulerDatabase.removeSchedule(conn, schema, schedule.getId());
			}

			// Create new schedules.
			String jobName = Util.getBDSCustJobName(bdsInfoId);
			String groupName = Util.getCustJobGroupNameForBDS(sInfo.getSpaceId());
			JobDetail jobDetail = scheduler.getJobDetail(new JobKey(jobName, groupName));
			for (Schedule schedule : toBeAdded) {
				// New schedule, make sure each new schedule has a UID.
				if (schedule.getJobId() == null || schedule.getJobId().trim().length() == 0) {
					schedule.setJobId(bdsInfoId);
				}
				schedule.setId(UUID.randomUUID().toString());
				SchedulerDatabase.addSchedule(conn, schema, schedule);
				if (!schedule.isEnable()) {
					continue;
				}

				if (jobDetail == null) {
					throw new ObjectUnavailableException("JobDetail not found for jobName : " + Util.getBDSCustJobName(sInfo.getId()) + ".");					
				}

				// Create new trigger.
				String timeZoneID = Schedule.getTimeZoneID(schedule.getTimeZone());				
				TriggerBuilder<CronTrigger> triggerBuilder = TriggerBuilder.newTrigger()
				.withIdentity(Util.getBDSCustTriggerName(schedule.getId()), spaceID)
				.withSchedule(cronSchedule(schedule.getExpression()).inTimeZone(TimeZone.getTimeZone(timeZoneID)))
				.forJob(jobDetail);

				Date startDate = Util.getDate(schedule.getStartDate());
				if (startDate != null && startDate.after(Calendar.getInstance().getTime())) {	
					triggerBuilder.startAt(startDate);
				}
		
				// Schedule the new job.
				CronTrigger cronTrigger = triggerBuilder.build();
				scheduler.scheduleJob(cronTrigger);				
			}

			// Unschedule all the retry jobs and run now jobs.
			Util.removeAllNonScheduledTriggers(jobDetail.getKey());
			BDSInfo updated = SchedulerDatabase.getBDSInfo(conn, schema, bdsInfoId);
			return updated;
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch(Exception ex) {
					logger.warn("Exception in closing connection : ",ex);
				}
			}
		}
	}
}
