package com.birst.scheduler.bds;

import java.sql.Timestamp;
import com.birst.scheduler.shared.Schedule;
import com.birst.scheduler.utils.CompareUtils;

public class BDSInfo {
    // BDS info member variables.
	private String id;
	private String spaceId;
	private String dataSourceId;
	private String userId;
	private boolean runNowMode;
	private boolean processAfter;
	private String loadDate;
	private String group;
	private boolean sendEmail;
	private Timestamp createdDate;
	private Timestamp modifiedDate;
	private String modifiedBy;
	private String name;
	private String type;
	private Schedule[] jobSchedules;
	public boolean currentExecutionStatus; // Only used for returning status. Not persisted in the database.

	public BDSInfo() {		
	}
	
	public BDSInfo(String id, String spaceId, String dataSourceId, String userId, String name, String type,
			boolean scheduledMode, boolean processAfter, String loadDate, String group, boolean sendEmail, 
			Timestamp createdDate, Timestamp modifiedDate, String modifiedBy) {
		this.id = id;
		this.spaceId = spaceId;
		this.dataSourceId = dataSourceId;
		this.userId = userId;
		this.name = name;
		this.type = type;
		this.runNowMode = scheduledMode;
		this.processAfter = processAfter;
		this.loadDate = loadDate;
		this.group = group;
		this.sendEmail = sendEmail;
		this.createdDate = createdDate;
		this.modifiedDate = modifiedDate;
		this.modifiedBy = modifiedBy;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == null){ return false;	}
		if (!(obj instanceof BDSInfo)){return false;}
		BDSInfo bdsInfo = (BDSInfo) obj;
		if (!CompareUtils.compare(bdsInfo.getId(), this.getId(), true)) { return false;}
		if (!CompareUtils.compare(bdsInfo.getSpaceId(), this.getSpaceId(), true)) { return false;}
		if (!CompareUtils.compare(bdsInfo.getDataSourceId(), this.getDataSourceId(), true)) { return false;}
		if (!CompareUtils.compare(bdsInfo.getUserId(), this.getUserId(), true)) { return false;}
		if (!(bdsInfo.isRunNowMode() == this.isRunNowMode())) { return false;}
		if (!(bdsInfo.isProcessAfter() == this.isProcessAfter())) { return false;}
		if (!CompareUtils.compare(bdsInfo.getLoadDate(), this.getLoadDate(), true)) { return false;}
		if (!CompareUtils.compare(bdsInfo.getGroup(), this.getGroup(), true)) { return false;}
		if (!(bdsInfo.getSendEmail() == this.getSendEmail())) { return false;}
		if (!(bdsInfo.getCreatedDate().compareTo(this.getCreatedDate()) != 0)) { return false;}
		if (!(bdsInfo.getModifiedDate().compareTo(this.getModifiedDate()) != 0)) { return false;}
		if (!CompareUtils.compare(bdsInfo.getModifiedBy(), this.getModifiedBy(), true)) { return false;}
		return true;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		if (getId() != null && getId().trim().length() > 0) {
			sb.append("id=" + getId());
			sb.append(" ");
		}
		if (getSpaceId() != null && getSpaceId().trim().length() > 0) {
			sb.append("spaceId=" + getSpaceId());
			sb.append(" ");
		}
		if (getDataSourceId() != null && getDataSourceId().trim().length() > 0) {
			sb.append("dataSourceId=" + getDataSourceId());
			sb.append(" ");
		}
		if (getUserId() != null && getUserId().trim().length() > 0) {
			sb.append("userId=" + getUserId());
			sb.append(" ");
		}
		sb.append("runNowMode=" + isRunNowMode());
		sb.append(" ");
		sb.append("processAfter=" + isProcessAfter());
		sb.append(" ");
		if (getLoadDate() != null && getLoadDate().trim().length() > 0) {
			sb.append("loadDate=" + getLoadDate());
			sb.append(" ");
		}
		sb.append(" ");
		if (getGroup() != null && getGroup().trim().length() > 0) {
			sb.append("group=" + getGroup());
		}
		sb.append(" ");
		sb.append("sendEmail=" + getSendEmail());
		sb.append(" ");
		if (getCreatedDate() != null) {
			sb.append("createdDate=" + getCreatedDate());
			sb.append(" ");
		}
		if (getModifiedDate() != null) {
			sb.append("modifiedDate=" + getModifiedDate());
			sb.append(" ");
		}
		if (getModifiedBy() != null && getModifiedBy().trim().length() > 0) {
			sb.append("modifiedBy=" + getModifiedBy());
			sb.append(" ");
		}
		return sb.toString();
	}
	
	// Get methods.
	public String getId() {
		return id;
	}
	public String getSpaceId() {
		return spaceId;
	}
	public String getUserId() {
		return userId;
	}
	public String getDataSourceId() {
		return dataSourceId;
	}
	public String getLoadDate() {
		return loadDate;
	}
	public String getGroup() {
		return group;
	}
	public Timestamp getCreatedDate() {
		return createdDate;
	}
	public Timestamp getModifiedDate() {
		return modifiedDate;
	}
	public String getModifiedBy() {
		return modifiedBy;
	}
	public boolean getSendEmail() {
		return sendEmail;
	}
	public Schedule[] getJobSchedules()
	{
		return jobSchedules;
	}
	public String getName() {
		return name;
	}
	public String getType() {
		return type;
	}

	// Status methods.
	public boolean isProcessAfter() {
		return processAfter;
	}
	public boolean isRunNowMode() {
		return runNowMode;
	}
	public boolean isCurrentExecutionStatus()
	{
		return currentExecutionStatus;
	}
	
	// Set methods.
	public void setDataSourceId(String dataSourceId) {
		this.dataSourceId = dataSourceId;
	}
	public void setId(String id) {
		this.id = id;
	}
	public void setSpaceId(String spaceId) {
		this.spaceId = spaceId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public void setProcessAfter(boolean processAfter) {
		this.processAfter = processAfter;
	}
	public void setScheduledMode(boolean scheduledMode) {
		this.runNowMode = scheduledMode;
	}
	public void setLoadDate(String loadDate) {
		this.loadDate = loadDate;
	}
	public void setGroup(String group) {
		this.group = group;
	}	
	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}
	public void setModifiedDate(Timestamp modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	public void setSendEmail(boolean sendEmail) {
		this.sendEmail = sendEmail;
	}
	public void setJobSchedules(Schedule[] jobSchedules)
	{
		this.jobSchedules = jobSchedules;
	}
	public void setCurrentExecutionStatus(boolean currentExecutionStatus)
	{
		this.currentExecutionStatus = currentExecutionStatus;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setType(String type) {
		this.type = type;
	}
}
