package com.birst.scheduler.bds;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.UUID;

import org.apache.log4j.Logger;
import org.quartz.InterruptableJob;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.SchedulerException;
import org.quartz.UnableToInterruptJobException;

import com.birst.scheduler.exceptions.ConcurrentSpaceOperationException;
import com.birst.scheduler.exceptions.SchedulerBaseException;
import com.birst.scheduler.process.ProcessJobOperation;
import com.birst.scheduler.bds.BDSInfo;
import com.birst.scheduler.shared.SpaceStatus;
import com.birst.scheduler.utils.CommandUtil;
import com.birst.scheduler.utils.ExceptionCodes;
import com.birst.scheduler.utils.SchedulerDBConnection;
import com.birst.scheduler.utils.SchedulerDatabase;
import com.birst.scheduler.utils.StatusUtils;
import com.birst.scheduler.utils.Util;
import com.birst.scheduler.utils.UtilConstants;
import com.birst.scheduler.utils.StatusUtils.Step;
import com.birst.scheduler.utils.StatusUtils.StepStatus;
import com.birst.scheduler.utils.UtilConstants.RunningJobStatus;

public class BDSJob implements InterruptableJob {
	// Job member variables.
	Map<String,Process> runningCommandProcesses = new HashMap<String,Process>();
	String bdsScheduleStepId;
	String runningJobId;
	RunningJobStatus jobStatus;
	String errorMessage;
	String exitErrorCodeMsg;
	boolean retry;
	public static final Logger logger = Logger.getLogger(BDSJob.class);

	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		Connection conn = null;
		BDSInfo bdsInfo = null;
		String spaceID = null;
		boolean runNowOperation = false;

		try {
			// Get group and space information.
			String groupName = context.getJobDetail().getKey().getGroup();
			spaceID = groupName.substring(UtilConstants.PREFIX_BDS_JOB_GROUP_NAME.length());
			logger.debug("BDS job starting for space : " + spaceID + ".");
			
			// Get the CUST_SPACE_STATUS step id if it is in the trigger hash map.
			// This happens if its a run now operation.
			Object obj = context.getMergedJobDataMap().get(UtilConstants.TRIGGER_MAP_SCHEDULE_STEP);
			if (obj != null) {
				// Validate if its a right format.
				bdsScheduleStepId = (String) obj;
				UUID.fromString(bdsScheduleStepId);
				runNowOperation = true;
			}
			
			runningJobId = Util.getObjectID(context.getJobDetail().getKey().getName(), UtilConstants.PREFIX_BDS_JOB);
			runningJobId = Util.getLowerCaseString(runningJobId);
			String schema = UtilConstants.SCHEDULER_DB_SCHEMA;
			SpaceStatus spaceStatus = null;
			if (bdsScheduleStepId == null) {
				spaceStatus = new SpaceStatus(UUID.randomUUID().toString(), spaceID, runningJobId, -1, null, Step.BDS, StepStatus.RUNNING);
				bdsScheduleStepId = spaceStatus.getId();
			} else {
				spaceStatus = new SpaceStatus(bdsScheduleStepId, spaceID, runningJobId, -1, null, Step.BDS, StepStatus.RUNNING);
			}
			
			// Make sure no other concurrent processes are running.
			// If not, add new schedule.
			SpaceStatus anyConcurrentSpaceStatus = StatusUtils.getActiveSpaceStatus(spaceID, false);
			if (anyConcurrentSpaceStatus != null && (bdsScheduleStepId == null || !anyConcurrentSpaceStatus.getId().equalsIgnoreCase(bdsScheduleStepId))) {
				logger.error("Another job is running for this space : " + anyConcurrentSpaceStatus.toString() + ".");
				markTheJobStatus(RunningJobStatus.FAILED, "Concurrent job running : ", false, StatusUtils.getConcurrentExitCodeMessage(anyConcurrentSpaceStatus));
				throw new ConcurrentSpaceOperationException("Another job is running for this space : " + anyConcurrentSpaceStatus.toString() + ".");
			}
			
			// Mark the wrapper job as running.
			conn = SchedulerDBConnection.getInstance().getConnectionPool().getConnection();
			SchedulerDatabase.addSpaceStatus(conn, schema, spaceStatus);
			markTheJobStatus(RunningJobStatus.RUNNING, null);
			
			// This is the id of the schedule step which is entered in by the caller.
			// Happens during the time of adding process schedule.
			bdsInfo = SchedulerDatabase.getBDSInfo(conn, schema, runningJobId);
			if (bdsInfo == null) {
				throw new Exception("Job info not found : id = " + runningJobId + ".");
			}
			
			// If space directory does not exist, mark as failed.
			Map<String,String> propertiesMap = Util.getSpaceInfoMap(spaceID);
			String spaceDirectory = propertiesMap != null ? propertiesMap.get(UtilConstants.KEY_SPACE_DIRECTORY) : null;
			if (spaceDirectory == null) {
				markTheJobStatus(RunningJobStatus.FAILED, "Space directory not found.", false);
				logger.info("Unable to extract. Space Directory not found : " + spaceID + ".");
				boolean isSpaceDeleted = Util.isSpaceDeleted(propertiesMap);
            	if (isSpaceDeleted) {
            		logger.info("Scheduling a delete space job for : " + spaceID + ".");
            		Util.scheduleDeletedSpaceJob(spaceID);
            	}
				return;
			}
			
			// If it is NOT a run now opearation and is a scheduled run, then make sure that no other extraction is running
			if (!runNowOperation) {
				Util.validateNoExtractFilePresent(spaceDirectory);
			}
			Util.validateNoLoadLockFilePresent(spaceDirectory);
			
			// Run the MR process, upload results, and mark the job status.
			if (!processBDS(spaceDirectory, bdsInfo)) {
				throw new Exception("BDS processing failed.");
			}
			if (!uploadBDSResults(spaceDirectory, bdsInfo)) {
				throw new Exception("BDS upload failed.");
			}
			markTheJobStatus(RunningJobStatus.COMPLETE, null);
			Util.rescheduleTriggersInJobGroup(spaceID, groupName, context.getTrigger());
		} catch(Exception ex) {
			logger.error("Exception while running BDSJob : " , ex);
			markTheJobStatus(RunningJobStatus.FAILED, "Exception while running BDSJob.", false);
			throw new JobExecutionException("Error while running BDSJob : ", ex.getCause());	
		} finally {
			Util.configureForRetry(context, jobStatus, retry, errorMessage);
			try {	
				if (bdsScheduleStepId != null) {
					StepStatus scheduleStepStatus = StepStatus.SUCCESS;
					if (jobStatus.getStatus() != RunningJobStatus.COMPLETE.getStatus()) {
						scheduleStepStatus = StepStatus.FAILED;
					}
					StatusUtils.updateSpaceStatus(bdsScheduleStepId, scheduleStepStatus.getStatus(), exitErrorCodeMsg, true);
				}
			} catch(Exception ex) {
				logger.error("Exception while updating scheduled status for uid : " + bdsScheduleStepId, ex);
			}
		}
	}
	
	protected boolean extractAndProcess(String spaceDirectory, BDSInfo connectorInfo) 
		throws SchedulerBaseException, ClassNotFoundException, SQLException, SchedulerException, IOException {	
		try {
			// Begin extraction process.
			Util.createBDSExtractFileInDirectory(spaceDirectory);
			boolean extractionDone = false;
			String engineCommandStepId = UUID.randomUUID().toString();
			if (getConnectorEngineCommand(spaceDirectory, engineCommandStepId, connectorInfo)) {
				String messageFilePath = CommandUtil.getMessageFilePath(spaceDirectory, engineCommandStepId);
				String extractionStepUid = UUID.randomUUID().toString();
				
				// Check if the success-<uid>.txt exists. This ensures that the bat files finished the entire process.	
				if (runExtraction(spaceDirectory, extractionStepUid, messageFilePath, connectorInfo)) {
					// See if it found success file after extraction -- spit out on successful extraction of all objects
					if (Util.successExtractFileExists(engineCommandStepId, spaceDirectory)) {
						// Get the output of all the files extracted through the list of filenames.
						String uploadAfterExtractionStepId = UUID.randomUUID().toString();
						String fileListBDSSources = CommandUtil.getBDSExtractionListFilePath(spaceDirectory, extractionStepUid);
						boolean uploadSuccessFull = uploaderAfterExtraction(spaceDirectory, uploadAfterExtractionStepId, fileListBDSSources, connectorInfo);
						Util.deleteBDSExtractFileFromDirectory(spaceDirectory);
						if (uploadSuccessFull) {
							if (connectorInfo.isProcessAfter()) {	
								String outputFilePath = CommandUtil.getMessageFilePath(spaceDirectory, uploadAfterExtractionStepId);
								processAfterExtraction(outputFilePath, connectorInfo);
							} else {
								logger.debug("Only extraction required for connector : id = " + connectorInfo.toString() + ".");
							}
						}
					} else {
						logger.error("Extraction finished but no success file present. Some objects failed to extract.");
						StatusUtils.updateSpaceStatus(extractionStepUid, ExceptionCodes.EXIT_CODE_EXTRACTION_SUCCESS_FILE);
					}
				} else {
					logger.warn("Extraction did not seem to run successfully. Skipping the post extract process.");
				}
			}
			return extractionDone;
		}
		finally {
			Util.deleteBDSExtractFileFromDirectory(spaceDirectory);
		}
	}
	
	protected boolean getConnectorEngineCommand(String spaceDirectory, String uid, BDSInfo connectorInfo) 
		throws SchedulerBaseException, ClassNotFoundException, SQLException {
		int exitValue = -1;
		try {
			// Add the current status to the space.
			StatusUtils.addSpaceStatus(uid, connectorInfo.getSpaceId(), runningJobId, -1, null, StatusUtils.Step.BDSEngineCmd, StatusUtils.StepStatus.RUNNING);
			String logFile = CommandUtil.getProcessCommandLogFilePath(spaceDirectory);
			String cmd = CommandUtil.getBDSEngineCommand(logFile, uid, connectorInfo.getSpaceId(), connectorInfo.getUserId(), connectorInfo.getSendEmail());
			exitValue = CommandUtil.runCommand(CommandUtil.CMD_GET_BDS_ENGINE, cmd, null, runningCommandProcesses);
		} catch(Exception ex) {
			logger.error("Error in retreiving engine command for connector : ", ex);
			exitValue = ExceptionCodes.INTERNAL_ERROR;
		} finally {
			StatusUtils.updateSpaceStatus(uid, exitValue);
		}
		return CommandUtil.isSuccessfulExit(exitValue);
	}
	
	@Override
	public void interrupt() throws UnableToInterruptJobException {
	}
	
	private void markTheJobStatus(RunningJobStatus status, String errorMsg) {
		markTheJobStatus(status, errorMsg, false);
	}
	
	private void markTheJobStatus(RunningJobStatus status, String errorMsg, boolean retryJob) {
		markTheJobStatus(status, errorMsg, false, null);
	}
	
	private void markTheJobStatus(RunningJobStatus status, String errorMsg, boolean retryJob, String exitCodeMsg) {
		jobStatus = status;
		errorMessage = errorMsg;
		retry = retryJob;
		exitErrorCodeMsg = exitCodeMsg;
	}

	protected boolean processBDS(String spaceDirectory, BDSInfo connectorInfo) 
		throws SchedulerBaseException, ClassNotFoundException, SQLException, SchedulerException, IOException {
		boolean status = true;
		try {
			logger.info("Processing BDS data...");
			String stepUid = UUID.randomUUID().toString();
			StatusUtils.addSpaceStatus(stepUid, connectorInfo.getSpaceId(), runningJobId, -1, null, StatusUtils.Step.BDSProcess, StatusUtils.StepStatus.RUNNING);
			String[] cmd = new String[]{"C:\\cygwin\\bin\\ssh.exe", "hadoop@sfo0-hadoop-06", "bash -s", "<", "/hadoop/demoV1/run.sh", "exit"};
			Runtime r = Runtime.getRuntime();
			Process pr = r.exec(cmd);
			BufferedReader buf = new BufferedReader(new InputStreamReader(pr.getInputStream()));
			String line = "";
			while ((line = buf.readLine()) != null) {
				logger.info(line);
			}
			BufferedReader buf2 = new BufferedReader(new InputStreamReader(pr.getErrorStream()));
			String line2 = "";
			while ((line2 = buf2.readLine()) != null) {
				logger.info(line2);
			}
			int returnCode = pr.waitFor();
			if (returnCode != 0) {
				throw new SchedulerException("BDS upload failed.");
			}
			updateSpaceStatus(stepUid);
			StatusUtils.updateSpaceStatus(stepUid, StatusUtils.StepStatus.SUCCESS.getStatus(), exitErrorCodeMsg, true);
			logger.info("Complete.");
		} catch (Exception e) {
			logger.error("processBDS: " + e.toString());
		} 
		return status;
	}
	
	protected void processAfterExtraction(String messageFilePath, BDSInfo connectorInfo) 
		throws SQLException, SchedulerBaseException, ClassNotFoundException, SchedulerException, IOException {
		Properties props = CommandUtil.getProps(messageFilePath);
		if (props != null) {
			String loadIdString = props.getProperty(CommandUtil.KEY_LOAD_NUMBER);
			int loadId = Integer.parseInt(loadIdString) + 1;
			String loadGroup = props.getProperty(CommandUtil.KEY_LOAD_GROUP);
			String loadDate = connectorInfo.getLoadDate();
			if(loadDate == null) {
				SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/dd/yyyy");
				// Note this is just to pass the date to Acorn. This date will be reformatted depending on the data type on the Acorn side. 
				// So no need to do anything database specific here. 
				// We should not split the functionality at different places and should have only one point of failure.
				Date lDate = Calendar.getInstance().getTime();
				loadDate = simpleDateFormat.format(lDate);
				
			}
			ProcessJobOperation.addProcessJob(connectorInfo.getSpaceId(), connectorInfo.getUserId(), loadId, loadDate,
					loadGroup,connectorInfo.getGroup(), bdsScheduleStepId, false, connectorInfo.getSendEmail());
		} else {
			logger.error("Unable to add processing after extraction. No params found in : " + messageFilePath);
		}		
	}
	
	protected boolean runExtraction(String spaceDirectory, String stepUid, String messageFilePath, BDSInfo connectorInfo) 
		throws SchedulerBaseException, ClassNotFoundException, SQLException {
		int exitValue = -1;
		try {
			// Add space status and create a check-in file.
			StatusUtils.addSpaceStatus(stepUid, connectorInfo.getSpaceId(), runningJobId, -1, null, StatusUtils.Step.BDSExtract, StatusUtils.StepStatus.RUNNING);
			StatusUtils.createCheckInFile(spaceDirectory + File.separator + UtilConstants.BDS_CHECK_IN_EXTRACT_FILE);
			
			// Get the load number from the returned props.
			Properties props = CommandUtil.getProps(messageFilePath);
			if (props != null) {
				String engineCommand = props.getProperty(CommandUtil.KEY_ENGINE_COMMAND);
				String args = props.getProperty(CommandUtil.KEY_ENGINE_ARGS);
				engineCommand = engineCommand.replace("//", File.separator);
				args = args.replace("//", File.separator);
				String cmd = CommandUtil.getProcessCommand(engineCommand, args);
				File file = new File(engineCommand);
				if(file.exists()) {
					String workingDirectory = file.getParent();
					exitValue = CommandUtil.runCommand(CommandUtil.CMD_EXTRACT, cmd, workingDirectory, runningCommandProcesses);
				}
			}
		} catch(Exception ex) {
			logger.error("Exception during load command : ", ex);
			exitValue = ExceptionCodes.INTERNAL_ERROR;
		} finally {
			StatusUtils.updateSpaceStatus(stepUid, exitValue);
		}
		return CommandUtil.isSuccessfulExit(exitValue);
	}
	
	protected boolean uploadBDSResults(String spaceDirectory, BDSInfo connectorInfo) 
		throws SchedulerBaseException, ClassNotFoundException, SQLException, SchedulerException, IOException, InterruptedException {
		logger.info("Uploading BDS results...");
		String stepUid = UUID.randomUUID().toString();
		StatusUtils.addSpaceStatus(stepUid, connectorInfo.getSpaceId(), runningJobId, -1, null, StatusUtils.Step.BDSUpload, StatusUtils.StepStatus.RUNNING);
		String[] cmd = new String[]{"C:\\cygwin\\bin\\ssh.exe", "hadoop@sfo0-hadoop-06", "bash -s", "<", "/home/hadoop/upload.sh", "exit"};
		Runtime r = Runtime.getRuntime();
		Process pr = r.exec(cmd);
		BufferedReader buf = new BufferedReader(new InputStreamReader(pr.getInputStream()));
		String line = "";
		while ((line = buf.readLine()) != null) {
			logger.info(line);
		}
		BufferedReader buf2 = new BufferedReader(new InputStreamReader(pr.getErrorStream()));
		String line2 = "";
		while ((line2 = buf2.readLine()) != null) {
			logger.info(line2);
		}
		int status = pr.waitFor();
		if (status != 0) {
			throw new SchedulerException("BDS upload failed.");
		}
		updateSpaceStatus(stepUid);
		StatusUtils.updateSpaceStatus(stepUid, StatusUtils.StepStatus.SUCCESS.getStatus(), exitErrorCodeMsg, true);
		logger.info("Complete.");
		return true;
	}
	
	protected void updateSpaceStatus(String stepUid) throws SQLException, ClassNotFoundException, SchedulerBaseException {
		if (stepUid != null) {
			StepStatus scheduleStepStatus = StepStatus.SUCCESS;
			if (jobStatus.getStatus() != RunningJobStatus.COMPLETE.getStatus()) {
				scheduleStepStatus = StepStatus.FAILED;
			}
			StatusUtils.updateSpaceStatus(stepUid, scheduleStepStatus.getStatus(), exitErrorCodeMsg, true);
		}
	}
	
	protected boolean uploaderAfterExtraction(String spaceDirectory, String uploaderStepId, String fileListPath, BDSInfo connectorInfo) 
		throws SchedulerBaseException, ClassNotFoundException, SQLException {
		int exitValue = -1;
		try {
			StatusUtils.addSpaceStatus(uploaderStepId, connectorInfo.getSpaceId(), runningJobId, -1, null, StatusUtils.Step.BDSPostExtract, StatusUtils.StepStatus.RUNNING);
			String logFile = CommandUtil.getBDSProcessCommandLogFilePath(spaceDirectory);
			String cmd = CommandUtil.getPostBDSExtractCommand(logFile, uploaderStepId, connectorInfo.getSpaceId(), connectorInfo.getUserId(), fileListPath);
			exitValue = CommandUtil.runCommand(CommandUtil.CMD_POST_BDS_EXTRACT, cmd, null, runningCommandProcesses);			
		} catch(Exception ex) {
			logger.error("Error in building before Load ", ex);
			exitValue = ExceptionCodes.INTERNAL_ERROR;
		} finally {
			StatusUtils.updateSpaceStatus(uploaderStepId, exitValue);
		}
		return CommandUtil.isSuccessfulExit(exitValue);
	}
}
