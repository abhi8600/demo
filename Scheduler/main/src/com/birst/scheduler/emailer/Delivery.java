package com.birst.scheduler.emailer;

import java.util.Date;
import java.util.Hashtable;
import java.util.Properties;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import javax.mail.Message;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;

import org.apache.log4j.Logger;


public class Delivery
{
	private static Logger logger = Logger.getLogger(Delivery.class);
	private String smtpServer;
	private javax.mail.Session mailSession;
	private static Hashtable<String, Delivery> deliveryHostMap = new Hashtable<String, Delivery>();
	private BlockingQueue<Message> messageList;
	private SendThread sendThread;
	private int SLEEP_TIME = 5 * 1000;
	
	public static synchronized Delivery getInstance(String mailHost)
	{
		Delivery delivery = deliveryHostMap.get(mailHost);
		if (delivery != null)
			return delivery;
		delivery = new Delivery(mailHost);
		delivery.start();
		deliveryHostMap.put(mailHost, delivery);
		return delivery;
	}
	
	public Delivery(){
		
	}
	
	public Delivery(String mailHost)
	{
		this.messageList = new LinkedBlockingQueue<Message>();
		this.smtpServer = mailHost;
		this.sendThread = new SendThread(mailHost, messageList);
	}
	
	public void sendEmail(String from, String to, String subject, String bodyText)
	{
		try
		{
			Properties props = new Properties();
			props.put("mail.smtp.host", smtpServer);
			if(mailSession == null)
			{
				mailSession = javax.mail.Session.getInstance(props, null);
			}
			
			Message msg = new MimeMessage(mailSession);
			InternetAddress addressFrom = new InternetAddress(from);
			msg.setFrom(addressFrom);
			msg.setReplyTo(new InternetAddress[]
			{ addressFrom });
			String[] recipients = to.split(";");
			InternetAddress[] addressTo = new InternetAddress[recipients.length];
			for (int i = 0; i < recipients.length; i++)
			{
				addressTo[i] = new InternetAddress(recipients[i]);
			}
			msg.setRecipients(Message.RecipientType.TO, addressTo);
			msg.setSubject(subject);
			msg.setSentDate(new Date());
			MimeBodyPart messagePart = new MimeBodyPart();
			messagePart.setText(bodyText);
			Transport.send(msg);
		} catch (Exception ex)
		{
			logger.error(ex.getMessage(), ex);
		}
	}
	
	private void start()
	{
		sendThread.start();
	}
	
	private class SendThread extends Thread
	{
		private final BlockingQueue<Message> queue;
		
		public SendThread(String server, BlockingQueue<Message> q)
		{
			this.setName("Mail Send - " + server);
			this.setDaemon(true); // allows the VM to exit
			queue = q;
		}

		public void send(Message msg)
		{
			queue.add(msg);
		}

		public void run()
		{
			while (true)
			{
				try
				{
					Message msg = (Message) queue.take();
					Transport.send(msg);
				} catch (Exception ex)
				{
					logger.error(ex.getMessage(), ex);
				}
				try
				{
					Thread.sleep(SLEEP_TIME);
				} catch (Exception ex)
				{
					logger.error(ex.getMessage(), ex);
				}
			}
		}
	}
}
