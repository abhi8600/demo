/*!
 * Birststrap Grunt task for icons data generation
 */
'use strict';
var fs = require('fs');

module.exports = function generatePolychromaticIconsData( grunt ) {
	// Pass encoding, utf8, so `readFileSync` will return a string instead of a
	// buffer
	var iconsFile = fs.readFileSync('lib/birststrap/sass/partials/_icons.scss', 'utf8');
	var iconLines = iconsFile.split('\n');

	var iconClassName = /^\.(\S+-icon\S*)/;
	var iconsData = '# This file is generated via Grunt task. **Do not edit directly.**\n' + '# See the \'build-polychromatic-icons-data\' task in Gruntfile.js.\n\n';
	var iconsYml = 'lib/birststrap/docs/_data/polychromatic_icons.yml';

	for (var i = 0, len = iconLines.length; i < len; i++) {
		var match = iconLines[i].match(iconClassName);

		if (match !== null) {
			iconsData += '- ' + match[1] + '\n';
		}
	}

	// Create the `_data` directory if it doesn't already exist
	if (!fs.existsSync('lib/birststrap/docs/_data')) {
		fs.mkdirSync('lib/birststrap/docs/_data');
	}

	try {
		fs.writeFileSync(iconsYml, iconsData);
	}
	catch (err) {
		grunt.fail.warn(err);
	}
	grunt.log.writeln('File ' + iconsYml.cyan + ' created.');
};
