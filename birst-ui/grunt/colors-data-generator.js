/*!
 * Birststrap Grunt task for color data generation
 */
'use strict';
var fs = require('fs');

module.exports = function generateColorsData( grunt ) {
	// Pass encoding, utf8, so `readFileSync` will return a string instead of a buffer
	var variablesFile = fs.readFileSync('lib/birststrap/sass/partials/_variables.scss', 'utf8');
	var variableLines = variablesFile.split('\n');

	var colorGroupComment = /====(.+)/;
	var colorVariable = /^(\$[\w_-]+):\s*((#|rgb|rgba)[^\;]+)/;
	var colorsData = '# This file is generated via Grunt task. **Do not edit directly.**\n' + '# See the \'build-colors-data\' task in Gruntfile.js.\n\n';
	var colorsYml = 'lib/birststrap/docs/_data/colors.yml';

	var groupMatch, colorMatch;

	for (var i = 0, len = variableLines.length; i < len; i++) {
		groupMatch = variableLines[i].match( colorGroupComment );
		colorMatch = variableLines[i].match( colorVariable );
		if ( groupMatch ) {
			colorsData += '-\n    name: ' + groupMatch[1] + '\n    colors:\n';
		} else if ( colorMatch ) {
			colorsData += '        -\n            name: ' + colorMatch[1] + '\n            value: "' + colorMatch[2] + '"\n';
		}
	}

	// Create the `_data` directory if it doesn't already exist
	if ( !fs.existsSync( 'lib/birststrap/docs/_data' ) ) {
		fs.mkdirSync( 'lib/birststrap/docs/_data' );
	}

	try {
		fs.writeFileSync( colorsYml, colorsData );
	}
	catch ( err ) {
		grunt.fail.warn( err );
	}
	grunt.log.writeln( 'File ' + colorsYml.cyan + ' created.' );
};
