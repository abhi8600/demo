/*!
 * Birststrap Grunt task for birstglyphs data generation
 */
'use strict';
var fs = require('fs');

module.exports = function generateBirstglyphsData( grunt ) {
	// Pass encoding, utf8, so `readFileSync` will return a string instead of a
	// buffer
	var birstglyphsFile = fs.readFileSync('lib/birststrap/sass/partials/_birstglyphs.scss', 'utf8');
	var birstglyphsLines = birstglyphsFile.split('\n');

	// Use any line that starts with ".birstglyph-" and capture the class name
	var iconClassName = /^\.(icon-[^\s]+):before \{ content/;
	var birstglyphsData = '# This file is generated via Grunt task. **Do not edit directly.**\n' + '# See the \'build-birstglyphs-data\' task in Gruntfile.js.\n\n';
	var birstglyphsYml = 'lib/birststrap/docs/_data/birstglyphs.yml';

	for (var i = 0, len = birstglyphsLines.length; i < len; i++) {
		var match = birstglyphsLines[i].match(iconClassName);

		if (match !== null) {
			birstglyphsData += '- ' + match[1] + '\n';
		}
	}

	// Create the `_data` directory if it doesn't already exist
	if (!fs.existsSync('lib/birststrap/docs/_data')) {
		fs.mkdirSync('lib/birststrap/docs/_data');
	}

	try {
		fs.writeFileSync(birstglyphsYml, birstglyphsData);
	}
	catch (err) {
		grunt.fail.warn(err);
	}
	grunt.log.writeln('File ' + birstglyphsYml.cyan + ' created.');
};
