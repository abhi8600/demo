var canv = document.createElement("canvas");
canv.width = canv.height = 600;
document.body.appendChild(canv);

var output = document.createElement("p");
document.body.appendChild(output);

var ctxt = canv.getContext("2d");

var symmetry = 8;

var points = [];

function rotate(pts) {
    var ret = [];
    for(var ii = 0; ii < symmetry; ii++) {
        ret = ret.concat(pts.map(function(pt) {
            var r = Math.sqrt(Math.pow(pt[0] - .5, 2) + Math.pow(pt[1] - .5, 2));
            var theta = Math.atan2(pt[1] - .5, pt[0] - .5);
            theta += Math.PI*2*ii/symmetry;
            return [.5 + r*Math.cos(theta), .5 + r*Math.sin(theta)];
        }));
    }
    return ret;
}

function render() {
    ctxt.clearRect(0, 0, 600, 600);
    
    ctxt.strokeStyle = "#999";
    ctxt.beginPath();
    ctxt.moveTo(200, 200);
    ctxt.lineTo(400, 200);
    ctxt.lineTo(400, 400);
    ctxt.lineTo(200, 400);
    ctxt.closePath();
    ctxt.stroke();

    var pts = rotate(points);

    if(pts.length > 2) {
        ctxt.strokeStyle = "#6bf";
        ctxt.fillStyle = "#6bf";
        ctxt.beginPath();
        ctxt.moveTo(200 + 200*pts[0][0], 200 + 200*pts[0][1]);
        pts.map(function(point) {
            ctxt.lineTo(200 + 200*point[0], 200 + 200*point[1]);
        });
        ctxt.closePath();
        ctxt.stroke();
        ctxt.fill();
    }

    ctxt.strokeStyle = "#bbb";
    for(var ii = 0; ii < 30; ii++) {
        ctxt.beginPath();
        ctxt.moveTo(0, ii*20);
        ctxt.lineTo(600, ii*20);
        ctxt.stroke();

        ctxt.beginPath();
        ctxt.moveTo(ii*20, 0);
        ctxt.lineTo(ii*20, 600);
        ctxt.stroke();
    }

    pts.map(function(point) {
        ctxt.fillStyle = "#bf6";
        ctxt.fillRect(197 + 200*point[0], 197 + 200*point[1], 6, 6);
    });
    output.innerHTML = JSON.stringify(pts);
}

render();
    
canv.onclick = function(ev) {
    points.push([ (ev.offsetX - 200)/200, (ev.offsetY - 200)/200 ]);
    render();
}

document.body.onkeydown = function(ev) {
    if(ev.which === 90) {
        points.pop();
        render();
    }
}
