
module.exports = function(bql, $, subjectArea, BqlNode, G) {

    var placeholder = new G.Terminal(
        function(str) {
            // never offer a completion - a placeholder can only be added by construction of the default node.
            return [];
        },
        {type: 'placeholder', string: 'placeholder'},
        0,
        true,
        // never parse to a placeholder
        function() { return null; }
    );
            
    bql.placeholder.initialize([new G.Production([placeholder], function() { return new BqlNode({type: 'placeholder'}, []); })], true);
}
