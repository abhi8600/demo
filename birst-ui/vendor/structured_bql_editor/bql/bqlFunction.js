module.exports = function(bql, $, subjectArea, BqlNode, G) {

    var Fns = require("./functions");

    function placeholderFor(arg) {
        if(arg.length) {
            return "Argument";
        }
        switch(arg) {
        case "int":
            return "Number";
        case "float":
            return "Number";
        case "string":
            return "String";
        case "bool":
            return "Condition";
        case "date":
            return "Date";
        case "datetime":
            return "Date and Time";
        case "datepart":
            return "Date Component";
        case "datetimepart":
            return "Date and Time Component";
        default:
            throw "Unexpected arg type: \"" + arg + "\"";
        }
    }

    function functionProductions() {
        var functionsBySignature = {};
        Fns.functions.map(function(fn) {
            var expTypes = ["int", "float", "string", "bool", "date", "datetime"];
            var signature = fn.args.map(function(arg) {
                if (expTypes.indexOf(arg) > -1 || arg.every && arg.every(function(it) {
                    return expTypes.indexOf(it) > -1;
                })) {
                    return "expression";
                } else if (arg === "datepart" || arg === "datetimepart") {
                    return arg;
                } else {
                    throw "Unexpected arg type: \"" + arg + "\"";
                }
            });
            var sigString = JSON.stringify(signature);
            if (!(sigString in functionsBySignature)) {
                functionsBySignature[sigString] = [];
            }
            functionsBySignature[sigString].push(fn);
        });

        return Object.keys(functionsBySignature).map(function(sigString) {
            var fns = functionsBySignature[sigString];
            var signature = JSON.parse(sigString);
            var nameToken = G.listToken(
                fns.map(function(fn) {
                    return {
                        type: 'function',
                        className: 'function-completion',
                        value: fn.label,
                        string: fn.label
                    };
                }), 2
            );
            var components = [nameToken, G.string("(")];
            for (var ii = 0; ii < signature.length; ii++) {
                if (ii > 0) components.push(G.string(","));
                var component = signature[ii] === "expression" && bql.expression || signature[ii] === "datepart" && bql.datepart || signature[ii] === "datetimepart" && bql.datetimepart;

                components.push(component);
            }
            components.push(G.string(")"));
            return new G.Production(components, function() {
                var children = [].slice.call(arguments, 0);
                var fn = children.shift();
                // remove parens
                children.shift();
                children.pop();
                // remove commas
                children = children.filter(function(child, ii) {
                    return ii % 2 === 0;
                });

                return new BqlNode({
                    type: "function",
                    value: fn.value
                }, children);
            }, function(children) {
                var fnName = children[0].value;

                var fn = Fns.functions.filter(function(fn) { return fn.label === fnName.value; })[0];
                var placeholders = [null, null]; // fnName itself and '('
                fn.args.map(function(arg, ii) {
                    if(ii !== 0) placeholders.push(null) // ','
                    
                    if(fn.placeholders) {
                        placeholders.push(fn.placeholders[ii]);
                    } else {
                        placeholders.push(placeholderFor(arg));
                    }
                });
                placeholders.push(null); // ')'
                return placeholders;
            });
        });
    }

    var operator = G.listToken(
        Fns.operators.map(function(op) {
            return {
                type: 'operator',
                className: 'operator-completion',
                string: op.label,
                value: op.label,
                nickname: op.nickname
            };
        }), 5);

    var statToken = G.listToken([{
        type: 'function',
        className: 'function-completion',
        value: "Stat",
        string: "Stat"
    }], 2);
    var paramListOrQuery = new G.NonTerminal();
    paramListOrQuery.initialize([
        new G.Production([bql.query], function(query) {
            return {
                query: query,
                params: []
            };
        }, function() { return ["Query"] }),
        new G.Production([bql.expression, G.string(","), paramListOrQuery], function(exp, _, queryParams) {
            return {
                query: queryParams.query,
                params: [exp].concat(queryParams.params)
            };
        }, function() { return ["Parameter", null, "Parameters and Query"]; })
    ], true);
    var statProduction = new G.Production([
        statToken,
        G.string("("), bql.statType,
        G.string(","), bql['int'],
        G.string(","), paramListOrQuery,
        G.string(")")
    ], function(statFn, _, statType, _, index, _, queryParams) {
        var args = [statType, index].concat(queryParams.params).concat([queryParams.query]);
        return new BqlNode({
            type: 'function',
            value: 'stat'
        }, args);
    }, function() { 
        return [null, null, "Aggregation", null, "Column Index", null, "Parameters and Query"];
    });

    var medianToken = G.listToken([{
        type: 'function',
        className: 'function-completion',
        value: "Median",
        string: "Median"
    }], 2);
    var rankBy = new G.NonTerminal([
        new G.Production([], function() { return []; }),
        new G.Production([G.string("by"), G.separatedList(bql.expression, G.string(","), false, function(vals, seps) { return vals; })], function(_, list) {
            return list;
        })
    ], true);
    var medianProduction = new G.Production([
        medianToken, 
        G.string("("), 
        bql.expression,
        rankBy,
        G.string(")")
    ], function(medianFn, _, arg, ranks, _) {
        if(ranks.length) {
            arg = BqlNode.fromRankBy(arg, ranks);
        } 
        return new BqlNode({
            type: 'function',
            value: 'median'
        }, [arg]);
    }, function() { return [null, null, "Argument", null, null]; });

    var rsumToken = G.listToken([{
        type: 'function',
        className: 'function-completion',
        value: "RSum",
        string: "RSum"
    }], 2);
    var rsumProduction = new G.Production([
        rsumToken, 
        G.string("("), 
        bql['int'],
        G.string(","),
        bql.expression,
        rankBy,
        G.string(")")
    ], function(rsumFn, _, wind, _, arg, ranks, _) {
        if(ranks.length) {
            arg = BqlNode.fromRankBy(arg, ranks);
        } 
        return new BqlNode({
            type: 'function',
            value: 'rsum'
        }, [wind, arg]);
    }, function() { return [null, null, "Window", null, "Summand", null, null]; });

    bql.operator.initialize([new G.Production([operator], function(op) {
        return op;
    })], true);

    bql.bqlFunction.initialize(functionProductions().concat([medianProduction, rsumProduction /*, statProduction */]), true);
};
