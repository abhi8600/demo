module.exports = function(bql, $, subjectArea, BqlNode, G) {
    bql.simpleExpression.initialize([
        new G.Production([bql.placeholder], Function.id),
        new G.Production([bql['int']], Function.id),
        new G.Production([bql['float']], Function.id),
        new G.Production([bql.bqlFunction], Function.id),
        new G.Production([bql.measure], Function.id),
        new G.Production([bql.attribute], Function.id),
        new G.Production([bql.string], Function.id),
        new G.Production([bql.date], Function.id),
        new G.Production([bql.groupedExpression], Function.id),
        new G.Production([bql.parameter], Function.id)
    ], true);
}
