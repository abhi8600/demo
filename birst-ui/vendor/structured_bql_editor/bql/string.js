module.exports = function(bql, $, subjectArea, BqlNode, G) {
    function stringToStringToken(str) {
        var match = str.match(/^"((?:[^"]|"")*)"?$/);
        if (match) {
            var innerString = match[1].replace(/""/g, '"');
            return {
                type: 'string',
                className: 'string-completion',
                string: '"' + innerString + '"',
                value: innerString
            };
        }
        return null;
    }
    var stringConstant = G.stringToken(
        stringToStringToken, '""', 0,
        function(str, idx) {
            // skip whitespace
            while(idx < str.length && str.charAt(idx).match(/\s/)) idx++;
            var start = idx;
            
            if(str.charAt(start) !== "'") return null;
            while(true) {
                idx++;
                if(idx >= str.length) return null;
                if(str.charAt(idx) === "'") {
                    var innerString = str.substring(start+1, idx).replace(/\\(.)/g, "\\1");
                    var tok = {
                        type: 'string',
                        className: 'string-completion',
                        string: '"' + innerString + '"',
                        value: innerString
                    };
                    return {value: tok, index: idx+1};
                }
                if(str.charAt(idx) === '\\') {
                    idx++;
                    if(idx >= str.length) return null;
                } 
            }
        }
    );

    bql.string.initialize([new G.Production([stringConstant], function(sc) {
        return BqlNode.fromString(sc.value);
    })], true);
}
