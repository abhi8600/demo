module.exports = function(bql, $, subjectArea, BqlNode, G) {
    var dateToken = G.listToken(["Year", "Month", "Day"].map(function(part) {
        return {
            type: "datepart",
            className: 'datepart-completion',
            string: part,
            value: part
        };
    }), 1);

    var dateTimeToken = G.listToken(["Year", "Month", "Day", "Hour", "Minute", "Second"].map(function(part) {
        return {
            type: "datepart",
            className: 'datepart-completion datepart-time-completion',
            string: part,
            value: part
        };
    }), 1);

    bql.datepart.initialize([
        new G.Production([dateToken], function(it) {
            return new BqlNode({
                type: "datepart",
                value: it.value
            }, []);
        })
    ], true);

    bql.datetimepart.initialize([
        new G.Production([dateTimeToken], function(it) {
            return new BqlNode({
                type: "datetimepart",
                value: it.value
            }, []);
        })
    ], true);
}
