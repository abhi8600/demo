module.exports = function(bql, $, subjectArea, BqlNode, G) {
    var dateConstant = G.stringToken(
        function(str) {
            var match = str.match(/^#([^#]*)#?$/);
            if(!match) {
                return null;
            }

            var dateString = match[1];
            return {
                type: 'datetime',
                className: 'datetime-completion',
                string: '#' + dateString + '#',
                value: '#' + dateString + '#'
            };
        }, '##', 0,
        function(str, idx) {
            // skip whitespace
            while(idx < str.length && str.charAt(idx).match(/\s/)) idx++;
            var start = idx;
            
            if(str.charAt(start) !== "#") return null;
            for(idx++; idx < str.length; idx++) {
                if(str.charAt(idx) === "#") {
                    var innerString = str.substring(start+1, idx);
                    var tok = {
                        type: 'datetime',
                        className: 'datetime-completion',
                        string: '#' + innerString + '#',
                        value: '#' + innerString + '#'
                    };
                    return {value: tok, index: idx+1};
                }
            }
            return null;
        }
    );
    var dateNow = G.listToken(
        ["NOW", "NOWDATE"].map(function(time) {
            return {
                string: time,
                type: 'datetime',
                className: 'datetime-completion',
                value: time
            };
        }), 0
    );

    bql.date.initialize([
        new G.Production([dateConstant], function(dt) {
            return BqlNode.fromDateTime(dt.value);
        }),
        new G.Production([dateNow], function(dt) {
            return BqlNode.fromDateTime(dt.value);
        })
    ], true);
}
