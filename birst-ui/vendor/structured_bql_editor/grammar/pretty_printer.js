module.exports = function($) {

    var blankWidth = 6;
    var indentWidth = 15;

    function blankSpace() {
        return $("<span>").css({display: 'inline-block'}).html("&nbsp").width(blankWidth);
    }

    function indenter(w) {
        return $("<span>").css({display: 'inline-block'}).html("&nbsp").width(w*indentWidth);
    }

    function spanWidth(s) {
        var hiddenDiv = $("<div>").css({position: 'absolute', left: "-3000px"}).appendTo($("body"));
        hiddenDiv.append(s);
        var ret = s.width();
        hiddenDiv.detach();
        return ret;
    }

    function span(s) {
        var w = spanWidth(s);
        return {
            width: w,
            print: function(left, limit, breakSpaces, indent) {
                return {left: left + w,  maxLeft: left + w, render: [s], indent: indent};
            }
        };
    }

    function blank() {
        return {
            width: blankWidth,
            print: function(left, limit, breakSpaces, indent) {
                if(breakSpaces) {
                    return {left: indent*indentWidth, maxLeft: indent*indentWidth, indent: indent, render: [$("<br>"), indenter(indent)]};
                } else {
                    return {left: left + blankWidth, maxLeft: left + blankWidth, indent: indent, render: [blankSpace()]};
                }
            }
        };
    }

    function group() {
        var lst = [].slice.call(arguments);
        var w = 0;
        lst.map(function(it) { w += it.width; });
        return {
            width: w,
            print: function(left, limit, breakSpaces, indent) {
                var breakChildren = left + w > limit;
                var render = [];
                var maxLeft = left;
                lst.map(function(it) {
                    var ret = it.print(left, limit, breakChildren, indent+1);
                    left = ret.left;
                    maxLeft = Math.max(maxLeft, ret.maxLeft);
                    render.push.apply(render, ret.render);
                });
                return {left: left, maxLeft: maxLeft, indent: indent, render: render};
            }
        };
    }

    function pp(w, it) {
        var container = $("<div>").width(w-100).addClass('pp');
        var inner = $("<div>").width(w-100).height('100%').appendTo(container);
        var printed = it.print(0, w-130, false, 0);
        // if the printed elements overflow the given width, expand the container to add a scroll
        inner.width(Math.max(w-130, printed.maxLeft));
        printed.render.map(function(elem) {
            inner.append(elem);
        });
        return container;
    }

    return {
        group: group,
        blank: blank,
        span: span,
        pp: pp
    };
};
