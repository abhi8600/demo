module.exports = function($) {

    var G = require("./grammar.js");
    //var A = require("./ast.js");
    var E = require("./editor.js")($);

    return {
        Production: G.Production,
        NonTerminal: G.NonTerminal,
        Terminal: G.Terminal,
        string: E.string,
        listToken: E.listToken,
        stringToken: E.stringToken,
        optional: E.optional,
        separatedList: E.separatedList,
        editAtPath: E.editAtPath
    };
};
