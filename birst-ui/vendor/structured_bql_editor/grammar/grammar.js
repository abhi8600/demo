var assert = require("assert");
var AST = require("./ast");


// A NonTerminal is a letter on the left side of a grammar production, like A -> aA, A -> b.
// It is represented with a list of all its right-side productions.

function NonTerminal(productions, grouped) {
    if (arguments.length) {
        this.initialize(productions, grouped);
    }
}

function Terminal(stringFunction, defaultValue, priority, isRequired, parser) {
    this.stringFunction = stringFunction;
    this.defaultValue = defaultValue;
    this.priority = priority;
    this.isRequired = isRequired;
    this.parser = parser;
}

// A Production is just a (possibly-empty) list of components, which are grammar elements (NonTerminals or Terminals)

function Production(components, evaluator, placeholders) {
    assert(components.every(function(component) {
        return (component instanceof NonTerminal) || (component instanceof Terminal);
    }), "Components should be one of the two Grammar types (Terminal or NonTerminal)");
    assert(evaluator, "Evaluator is required for any Production");

    this.components = components;
    this.evaluator = evaluator;
    this.placeholders = placeholders;
}

NonTerminal.prototype.initialize = function(productions, grouped) {
    assert(productions.every(function(production) {
        return production instanceof Production;
    }), "First argument to NonTerminal should be a list of Productions");

    this.productions = productions;

    // Grouped indicates whether to group this nonterminal for pretty-printing
    this.grouped = grouped;
};

// Initial / default value for a particular nonterminal. Note that the
// *first* production is chosen, so initial values are determined by
// the order in which productions are defined. Note also that a poor
// choice of initial productions could lead to infinite recursion
// here!
NonTerminal.prototype.initial = function() {
    var production = this.productions[0];

    var initialChildren = production.components.map(function(component) {
        return component.initial();
    });

    return new AST.Node(this, production, initialChildren);
};

Terminal.prototype.initial = function() {
    return new AST.Leaf(this, this.defaultValue, true);
};

// Return the initial value only if it is an empty tree (i.e. no terminals in the expansion).
// otherwise known as an epsilon production
NonTerminal.prototype.empty = function() {
    var init = this.initial();
    if (init.isEmpty()) {
        return init;
    }
    return null;
};

Terminal.prototype.empty = function() {
    return null;
};

function streamEnd(success, failure) {
    failure();
}

function streamAppend(s1, s2) {
    return function(success, failure) {
        s1(function(val, idx, tail) {
            success(val, idx, streamAppend(tail, s2));
        }, function() {
            s2(success, failure);
        });
    };
}

function streamSeq(s, fs) {
    return function(success, failure) {
        s(function(val, idx, tail) {
            fs(val, idx)(function(val, idx, tail2) {
                success(val, idx, streamAppend(tail2, streamSeq(tail, fs)));
            }, function() {
                streamSeq(tail, fs)(success, failure);
            });
        }, failure);
    };
}

function streamMap(s, f) {
    return function(success, failure) {
        s(function(val, idx, tail) {
            success(f(val), idx, streamMap(tail, f));
        }, failure);
    };
}

function streamReturn(val, idx) {
    return function(success /*, failure*/) {
        success(val, idx, streamEnd);
    };
}

NonTerminal.prototype.parse = function(str) {
    var ret = null;
    var trimmed = str.trim();
    this.tryParse(trimmed, 0)(function succ(val, idx, tail) {
        if (idx === trimmed.length) {
            ret = val;
        } else {
            tail(succ, function() {});
        }
    }, function() {});
    return ret;
};

NonTerminal.prototype.tryParse = function(str, index) {
    var NT = this;
    return this.productions.map(tryParseProduction).reduceRight(streamAppend, streamEnd);

    function tryParseProduction(production) {
        var parseComponents = production.components.reduce(function(init, last) {
            return streamSeq(init, function(initVals, idx, tail) {
                return streamMap(last.tryParse(str, idx), function(val) {
                    return initVals.concat([val]);
                });
            });
        }, streamReturn([], index));

        return streamMap(parseComponents, function(children) {
            return new AST.Node(NT, production, children);
        });
    }
};

Terminal.prototype.tryParse = function(str, index) {
    var self = this;
    return function(success, failure) {
        var result = self.parser(str, index);
        if (!result) {
            failure();
            return;
        }
        success(new AST.Leaf(self, result.value), result.index, streamEnd);
    };
};

// Tokens which can be at the start of this nonterminal. e.g., if you have
// A -> a
// A -> BC
// B -> b
// C -> C
//
// then firstTokens would include a and b, but not c.
//
// Besides the token itself, we return three other values:
//
//   zipper: turns a Terminal produced from the given token, into a Node corresponding to this top-level NonTerminal
//   relPath: path from the top of the resulting Node to the terminal
//   topPath: passed in and returned unchanged, this represents the path to the NonTerminal within the larger AST
NonTerminal.prototype.firstTokens = function(topPath) {

    return go(this, [], function(node) {
        return node;
    });

    function go(grammar, path, zipper) {
        if (grammar instanceof Terminal) {
            return [{
                topPath: topPath,
                relPath: path,
                token: grammar,
                zipper: zipper
            }];
        }

        return grammar.productions.concatMap(productionFirstTokens);

        function productionFirstTokens(production) {
            var tokens = [];
            var finished = false;
            production.components.map(function(component, ii) {
                if (finished) return;
                tokens = tokens.concat(go(component, path.concat([ii]), function(node) {
                    var children = production.components.map(function(component, jj) {
                        if (ii === jj) {
                            return node;
                        } else {
                            return component.initial();
                        }
                    });
                    return zipper(new AST.Node(grammar, production, children));
                }));
                if (!component.empty()) {
                    finished = true;
                }
            });
            return tokens;
        }
    }
};

module.exports = {
    Terminal: Terminal,
    NonTerminal: NonTerminal,
    Production: Production
};
