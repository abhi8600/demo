module.exports = function($) {

    var PP = require("./pretty_printer")($);
    var G = require("./grammar");
    var A = require("./ast");
    var Auto = require("../autocomplete")($);

    // Prevent chrome from going back when backspace is held down.
    $(document).off('keydown.preventBack').on('keydown.preventBack', function(ev) {
        if(ev.keyCode === 8) {
            if(!$(ev.target).is("input, textarea")) {
                ev.preventDefault();
            }
        }
    });

    function prettyPrint(width, node, renderLeaf, path, completionWidget) {
        return PP.pp(width, PP.group.apply({}, go(node, [], null)));

        function go(node, nodePath, placeholder) {
            if (path && A.equalPath(nodePath, path)) {
                return [PP.span(completionWidget(placeholder))];
            }
            if (node instanceof A.Leaf) {
                return [PP.span(
                    renderLeaf(node, nodePath, placeholder)
                )];
            } else {
                var children = [];
                var placeholders = node.production.placeholders && node.production.placeholders(node.children);
                for (var ii = 0; ii < node.children.length; ii++) {
                    if (ii !== 0) children.push(PP.blank());
                    children = children.concat(go(node.children[ii], nodePath.concat([ii]), placeholders ? placeholders[ii] : placeholder));
                }
                return node.nonterminal.grouped ? [PP.group.apply({}, children)] : children;
            }
        }
    }


    function editAtPath(container, ast, onChange, path, continuationMethod, takeFocus) {
        var nextPath = function(ast, path) { return ast.nextPath(path); }
        var backspacePath = function(ast, path) { return ast.backspacePath(path); }
        var prevTokenPath = function(ast, path) { return ast.topEquivalentPath(ast.prevTokenPath(path)) };

        if(!continuationMethod) {
            continuationMethod = nextPath;
        }
        onChange(ast);
        var tokens = ast.followingTokens(path);
        if(tokens.length === 1) {
            var defaultVal = tokens[0].token.defaultValue;
            if(defaultVal.type === 'syntax') {
                defaultVal.token = tokens[0].token;
                defaultVal.zipper = tokens[0].zipper;
                defaultVal.topPath = tokens[0].topPath;
                defaultVal.relPath = tokens[0].relPath;
                continueWith(defaultVal, continuationMethod);
                return;
            }
        }

        var completionWidget = function(placeholder) {
            return Auto.autocomplete(tokenCompletions(tokens), {
                back: path.length > 0 ? onBack : null,
                escape: onEscape,
                finish: onFinish,
                onLeft: onLeft,
                onRight: onRight
            }, "", placeholder, takeFocus);
        }


        container.find('.pp').remove();
        container.append(prettyPrint(container.width(), ast, function(node, nodePath, placeholder) {
            var span;
            if(node.terminal.customRenderer) {
                span = node.terminal.customRenderer(node);
            } else if(node.initialFlag && placeholder) {
                span = $("<span>").css({fontSize: '90%', fontStyle: 'italic'}).text(placeholder);
            } else {
                span = $("<span>").text(node.value.string);
            }
            return span.on('click', function() {
                editAtPath(container, ast, onChange, ast.topEquivalentPath(nodePath), nextPath, true);
            });
        }, path, completionWidget));

        function onBack() {
            var currPath = ast.topEquivalentPath(path);
            ast = ast.setChild(currPath, ast.getChild(currPath).initial());
            var prevPath = ast.backspacePath(currPath);
            ast = ast.setChild(prevPath, ast.getChild(prevPath).initial());
            editAtPath(container, ast, onChange, prevPath, backspacePath, true);
        }

        function onEscape() {
            editAtPath(container, ast, onChange, ast.topEquivalentPath(ast.lastTokenPath()), nextPath, true);
        }

        function onFinish(val) {
            continueWith(val, nextPath);
        }

        function onLeft() {
            var leftPath = ast.topEquivalentPath(ast.prevTokenPath(path));
            editAtPath(container, ast, onChange, leftPath, prevTokenPath, true);
        }

        function onRight() {
            var rightPath = ast.topEquivalentPath(ast.nextTokenPath(path));
            editAtPath(container, ast, onChange, rightPath, nextPath, true);
        }

        function continueWith(val, method) {
            ast = ast.setChild(val.topPath, val.zipper(new A.Leaf(val.token, val)))
            var fullPath = val.topPath.concat(val.relPath);
            var next = method(ast, fullPath);
            editAtPath(container, ast, onChange, next, method, true);
        }
    }


    function tokenCompletions(tokens) {
        tokens = tokens.sort(function(left, right) {
            return right.token.priority - left.token.priority;
        });
        return function(string) {

            var completions = [],
                completionsStartsWith = [],
                completionsContains = [];
            tokens.map(function(tokenWithPath) {
                var tokenVals = tokenWithPath.token.stringFunction(string);
                tokenVals = tokenVals.map(function(tokenVal) {
                    tokenVal.token = tokenWithPath.token;
                    tokenVal.zipper = tokenWithPath.zipper;
                    tokenVal.topPath = tokenWithPath.topPath;
                    tokenVal.relPath = tokenWithPath.relPath;
                    if (tokenVal.string.toLowerCase().indexOf(string.toLowerCase()) === 0) {
                        completionsStartsWith.push(tokenVal);
                    } else {
                        completionsContains.push(tokenVal);
                    }
                    return tokenVal;
                });
            });
            completions = completionsStartsWith.concat(completionsContains);

            return completions.map(function(completion) {
                return Auto.stringCompletion(completion);
            });
        };
    }

    function stringToken(stringFunction, defaultString, priority, parser) {
        return new G.Terminal(
            function(str) {
                var value = stringFunction(str);
                if (value) return [value];
                return [];
            },
            stringFunction(defaultString),
            priority,
            true,
            parser
        );
    }

    function listToken(values, priority, parser) {
        return new G.Terminal(
            function(str) {
                var exactMatch = values.filter(function(value) {
                    return value.string.toLowerCase() === str.toLowerCase();
                });
                var containingValues = values.filter(function(value) {
                    return value.string.toLowerCase().indexOf(str.toLowerCase()) > -1;
                });
                return exactMatch.concat(containingValues.filter(function(val) {
                    return exactMatch.indexOf(val) === -1;
                }));
            },
            values[0],
            priority,
            false,
            parser || parseValuesCaseInsensitive(values)
        );
    }


    function parseValuesCaseInsensitive(values) {
        return function(str, idx) {
            var matchingStrings = values.map(function(val) { return val.string.toLowerCase(); });
            // skip whitespace
            while(idx < str.length && str.charAt(idx).match(/\s/)) idx++;
            var start = idx;

            var strIdx = 0;
            while(true) {
                var stopMatching = false;
                if(idx === str.length) {
                    stopMatching = true;
                } else {
                    var nextChar = str.charAt(idx).toLowerCase();
                    var nextMatches = matchingStrings.filter(function(match) {
                        return match.length > strIdx && match.charAt(strIdx) === nextChar;
                    });
                    stopMatching = (nextMatches.length === 0);
                }
                if(stopMatching) {
                    var exactMatches = matchingStrings.filter(function(match) {
                        return match.length === strIdx
                    });
                    if(exactMatches.length) {
                        var matchingValue = values.filter(function(val) { return val.string.toLowerCase() === exactMatches[0].toLowerCase() })[0]
                        return {index: idx, value: matchingValue};
                    } else {
                        return null;
                    }
                }
                matchingStrings = nextMatches;
                strIdx++;
                idx++;
            }
        }
    }

    function constantString(str) {
        return listToken([{
            type: 'syntax',
            className: 'syntax-completion',
            string: str
        }], 100);
    }

    function optional(elem) {
        return new G.NonTerminal([
            new G.Production([], function() { return undefined; }),
            new G.Production([elem], Function.id)
        ], true);
    }

    function separatedList(elem, sep, allowEmpty, finalize) {
        var tail = new G.NonTerminal([
            new G.Production([], function() {
                return {
                    values: [],
                    separators: []
                };
            })
        ], false);
        tail.productions.push(
            new G.Production([sep, elem, tail], function(sep, val, tailVal) {
                tailVal.values.unshift(val);
                tailVal.separators.unshift(sep);
                return tailVal;
            })
        );

        return new G.NonTerminal((allowEmpty ? [new G.Production([], function() {
            return finalize([], []);
        })] : []).concat([
            new G.Production([elem, tail], function(val, tailVal) {
                tailVal.values.unshift(val);
                return finalize(tailVal.values, tailVal.separators);
            })
        ]), true);
    }


    return {
        string: constantString,
        listToken: listToken,
        stringToken: stringToken,
        optional: optional,
        separatedList: separatedList,
        editAtPath: editAtPath
    };

    /*
      function runTests() {
      var t1 = new Terminal("t1", function(){}, "");

      var p1 = new Production([]);
      var p2 = new Production([t1]);
      var g1 = new NonTerminal([p1, p2]);

      var g1Init = g1.initial();
      assert(g1Init instanceof Node, "Initial form for grammar 1 should be a Node");
      assert(g1Init.isInitial(), "Initial form for grammar 1 should be initial");
      assert(g1Init.isEmpty(), "Initial form for grammar 1 should be empty");

      var g2 = new NonTerminal([p2]);
      var g2Init = g2.initial();
      assert(g2Init instanceof Node, "Initial form for grammar 2 should be a Node");
      assert(g2Init.isInitial(), "Initial form for grammar 2 should be initial");
      assert(!g2Init.isEmpty(), "Initial form for grammar 2 should not be empty");

      g1.productions[1] = new Production([t1, g1]);
      assert(g1Init instanceof Node, "Initial form for grammar 1 should be a Node");
      assert(g1Init.isInitial(), "Initial form for grammar 1 should be initial");
      assert(g1Init.isEmpty(), "Initial form for grammar 1 should be empty");
      }


      runTests();
    */

};
