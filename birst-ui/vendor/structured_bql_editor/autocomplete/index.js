module.exports = function($) {
    var C = require("./completion")($);

    return {
        autocomplete: require("./autocomplete")($),
        Completion: C.Completion,
        stringCompletion: C.stringCompletion
    };
}
