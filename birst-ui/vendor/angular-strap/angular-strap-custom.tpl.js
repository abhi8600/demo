/**
 * angular-strap
 * @version v2.0.2 - 2014-04-27
 * @link http://mgcrea.github.io/angular-strap
 * @author Olivier Louvignes (olivier@mg-crea.com)
 * @license MIT License, http://www.opensource.org/licenses/MIT
 */
(function(window, document, undefined) {
'use strict';

// Source: alert.tpl.js
angular.module('mgcrea.ngStrap.alert').run([
  '$templateCache',
  function ($templateCache) {
    $templateCache.put('alert/alert.tpl.html', '<div id="{{ id }}" class="top-right"><div class="alert alert-dismissable" tabindex="-1" ng-class="[type ? \'alert-\' + type : null]"><button type="button" class="close" ng-click="$hide()">&times;</button> <strong ng-show="title" ng-bind="title + \' \'"></strong><span ng-bind-html="content"></span><span ng-show="extra" ng-init="showExtra=false" ng-click="showExtra=!showExtra"> <span style="opacity: 0.5; cursor: pointer;">(<span style="text-decoration: underline;"><span ng-if="!showExtra">more</span><span ng-if="showExtra">less</span></span>)</span></span><div ng-show="showExtra" ng-bind="extra" style="max-width: 350px;"></div></div></div>');
  }
]);

})(window, document);
