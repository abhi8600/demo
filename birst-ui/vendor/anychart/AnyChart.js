(function(){function e(a) {
  throw a;
}
var h = void 0, j = !0, p = null, q = !1;
function aa() {
  return function(a) {
    return a
  }
}
function s() {
  return function() {
  }
}
function t(a) {
  return function(b) {
    this[a] = b
  }
}
function u(a) {
  return function() {
    return this[a]
  }
}
function x(a) {
  return function() {
    return a
  }
}
var z, A = A || {};
A.global = this;
A.eB = q;
A.aP = "en";
A.OR = function(a) {
  A.dI(a)
};
A.$R = function(a) {
  A.eB || (a = a || "", e(Error("Importing test-only code into non-debug environment" + a ? ": " + a : ".")))
};
A.dI = function(a, b, c) {
  a = a.split(".");
  c = c || A.global;
  !(a[0] in c) && c.execScript && c.execScript("var " + a[0]);
  for(var d;a.length && (d = a.shift());) {
    !a.length && A.Uo(b) ? c[d] = b : c = c[d] ? c[d] : c[d] = {}
  }
};
A.BM = function() {
  for(var a = ["window", "event"], b = A.global, c;c = a.shift();) {
    if(A.dN(b[c])) {
      b = b[c]
    }else {
      return p
    }
  }
  return b
};
A.RQ = function(a, b) {
  var c = b || A.global, d;
  for(d in a) {
    c[d] = a[d]
  }
};
A.DP = s();
A.LO = j;
A.TR = s();
A.RP = "";
A.WJ = s();
A.WQ = aa();
A.xa = function() {
  e(Error("unimplemented abstract method"))
};
A.EP = function(a) {
  a.FQ = function() {
    return a.VM || (a.VM = new a)
  }
};
A.Hj = function(a) {
  var b = typeof a;
  if("object" == b) {
    if(a) {
      if(a instanceof Array) {
        return"array"
      }
      if(a instanceof Object) {
        return b
      }
      var c = Object.prototype.toString.call(a);
      if("[object Window]" == c) {
        return"object"
      }
      if("[object Array]" == c || "number" == typeof a.length && "undefined" != typeof a.splice && "undefined" != typeof a.propertyIsEnumerable && !a.propertyIsEnumerable("splice")) {
        return"array"
      }
      if("[object Function]" == c || "undefined" != typeof a.call && "undefined" != typeof a.propertyIsEnumerable && !a.propertyIsEnumerable("call")) {
        return"function"
      }
    }else {
      return"null"
    }
  }else {
    if("function" == b && "undefined" == typeof a.call) {
      return"object"
    }
  }
  return b
};
A.Uo = function(a) {
  return a !== h
};
A.tR = function(a) {
  return a === p
};
A.dN = function(a) {
  return a != p
};
A.isArray = function(a) {
  return"array" == A.Hj(a)
};
A.$e = function(a) {
  var b = A.Hj(a);
  return"array" == b || "object" == b && "number" == typeof a.length
};
A.rR = function(a) {
  return A.gn(a) && "function" == typeof a.getFullYear
};
A.Qc = function(a) {
  return"string" == typeof a
};
A.cN = function(a) {
  return"boolean" == typeof a
};
A.lJ = function(a) {
  return"number" == typeof a
};
A.Ey = function(a) {
  return"function" == A.Hj(a)
};
A.gn = function(a) {
  var b = typeof a;
  return"object" == b && a != p || "function" == b
};
A.Tm = function(a) {
  return a[A.rw] || (a[A.rw] = ++A.dO)
};
A.LN = function(a) {
  "removeAttribute" in a && a.removeAttribute(A.rw);
  try {
    delete a[A.rw]
  }catch(b) {
  }
};
A.rw = "closure_uid_" + Math.floor(2147483648 * Math.random()).toString(36);
A.dO = 0;
A.DQ = A.Tm;
A.RR = A.LN;
A.DL = function(a) {
  var b = A.Hj(a);
  if("object" == b || "array" == b) {
    if(a.Ca) {
      return a.Ca()
    }
    var b = "array" == b ? [] : {}, c;
    for(c in a) {
      b[c] = A.DL(a[c])
    }
    return b
  }
  return a
};
A.zL = function(a, b, c) {
  return a.call.apply(a.bind, arguments)
};
A.yL = function(a, b, c) {
  a || e(Error());
  if(2 < arguments.length) {
    var d = Array.prototype.slice.call(arguments, 2);
    return function() {
      var c = Array.prototype.slice.call(arguments);
      Array.prototype.unshift.apply(c, d);
      return a.apply(b, c)
    }
  }
  return function() {
    return a.apply(b, arguments)
  }
};
A.bind = function(a, b, c) {
  A.bind = Function.prototype.bind && -1 != Function.prototype.bind.toString().indexOf("native code") ? A.zL : A.yL;
  return A.bind.apply(p, arguments)
};
A.eK = function(a, b) {
  var c = Array.prototype.slice.call(arguments, 1);
  return function() {
    var b = Array.prototype.slice.call(arguments);
    b.unshift.apply(b, c);
    return a.apply(this, b)
  }
};
A.CR = function(a, b) {
  for(var c in b) {
    a[c] = b[c]
  }
};
A.now = Date.now || function() {
  return+new Date
};
A.QQ = function(a) {
  if(A.global.execScript) {
    A.global.execScript(a, "JavaScript")
  }else {
    if(A.global.eval) {
      if(A.Ex == p && (A.global.eval("var _et_ = 1;"), "undefined" != typeof A.global._et_ ? (delete A.global._et_, A.Ex = j) : A.Ex = q), A.Ex) {
        A.global.eval(a)
      }else {
        var b = A.global.document, c = b.createElement("script");
        c.type = "text/javascript";
        c.defer = q;
        c.appendChild(b.createTextNode(a));
        b.body.appendChild(c);
        b.body.removeChild(c)
      }
    }else {
      e(Error("goog.globalEval not available"))
    }
  }
};
A.Ex = p;
A.CQ = function(a, b) {
  function c(a) {
    return A.zH[a] || a
  }
  var d;
  d = A.zH ? "BY_WHOLE" == A.ML ? c : function(a) {
    for(var a = a.split("-"), b = [], d = 0;d < a.length;d++) {
      b.push(c(a[d]))
    }
    return b.join("-")
  } : aa();
  return b ? a + "-" + d(b) : d(a)
};
A.WR = function(a, b) {
  A.zH = a;
  A.ML = b
};
A.IQ = function(a, b) {
  var c = b || {}, d;
  for(d in c) {
    var f = ("" + c[d]).replace(/\$/g, "$$$$"), a = a.replace(RegExp("\\{\\$" + d + "\\}", "gi"), f)
  }
  return a
};
A.Gg = function(a, b) {
  A.dI(a, b, h)
};
A.W = function(a, b, c) {
  a[b] = c
};
A.e = function(a, b) {
  function c() {
  }
  c.prototype = b.prototype;
  a.f = b.prototype;
  a.prototype = new c;
  a.prototype.constructor = a
};
A.QP = function(a, b, c) {
  var d = arguments.callee.caller;
  if(d.f) {
    return d.f.constructor.apply(a, Array.prototype.slice.call(arguments, 1))
  }
  for(var f = Array.prototype.slice.call(arguments, 2), g = q, k = a.constructor;k;k = k.f && k.f.constructor) {
    if(k.prototype[b] === d) {
      g = j
    }else {
      if(g) {
        return k.prototype[b].apply(a, f)
      }
    }
  }
  if(a[b] === d) {
    return a.constructor.prototype[b].apply(a, f)
  }
  e(Error("goog.base called from a method of one name to a method of a different name"))
};
A.scope = function(a) {
  a.call(A.global)
};
A.debug = {};
function ba(a) {
  this.stack = Error().stack || "";
  a && (this.message = "" + a)
}
A.e(ba, Error);
ba.prototype.name = "CustomError";
A.U = {};
A.U.YN = function(a) {
  return 0 == a.lastIndexOf("/", 0)
};
A.U.fM = function(a) {
  var b = a.length - 1;
  return 0 <= b && a.indexOf("/", b) == b
};
A.U.aQ = function(a, b) {
  return 0 == A.U.$G(b, a.substr(0, b.length))
};
A.U.$P = function(a, b) {
  return 0 == A.U.$G(b, a.substr(a.length - b.length, b.length))
};
A.U.ZN = function(a, b) {
  for(var c = 1;c < arguments.length;c++) {
    var d = ("" + arguments[c]).replace(/\$/g, "$$$$"), a = a.replace(/\%s/, d)
  }
  return a
};
A.U.eQ = function(a) {
  return a.replace(/[\s\xa0]+/g, " ").replace(/^\s+|\s+$/g, "")
};
A.U.ri = function(a) {
  return/^[\s\xa0]*$/.test(a)
};
A.U.sR = function(a) {
  return A.U.ri(A.U.nN(a))
};
A.U.qR = function(a) {
  return!/[^\t\n\r ]/.test(a)
};
A.U.oR = function(a) {
  return!/[^a-zA-Z]/.test(a)
};
A.U.uR = function(a) {
  return!/[^0-9]/.test(a)
};
A.U.pR = function(a) {
  return!/[^a-zA-Z0-9]/.test(a)
};
A.U.wR = function(a) {
  return" " == a
};
A.U.xR = function(a) {
  return 1 == a.length && " " <= a && "~" >= a || "\u0080" <= a && "\ufffd" >= a
};
A.U.eS = function(a) {
  return a.replace(/(\r\n|\r|\n)+/g, " ")
};
A.U.ZP = function(a) {
  return a.replace(/(\r\n|\r|\n)/g, "\n")
};
A.U.GR = function(a) {
  return a.replace(/\xa0|\s/g, " ")
};
A.U.FR = function(a) {
  return a.replace(/\xa0|[ \t]+/g, " ")
};
A.U.dQ = function(a) {
  return a.replace(/[\t\r\n ]+/g, " ").replace(/^[\t\r\n ]+|[\t\r\n ]+$/g, "")
};
A.U.trim = function(a) {
  return a.replace(/^[\s\xa0]+|[\s\xa0]+$/g, "")
};
A.U.trimLeft = function(a) {
  return a.replace(/^[\s\xa0]+/, "")
};
A.U.trimRight = function(a) {
  return a.replace(/[\s\xa0]+$/, "")
};
A.U.$G = function(a, b) {
  var c = ("" + a).toLowerCase(), d = ("" + b).toLowerCase();
  return c < d ? -1 : c == d ? 0 : 1
};
A.U.YJ = /(\.\d+)|(\d+)|(\D+)/g;
A.U.HR = function(a, b) {
  if(a == b) {
    return 0
  }
  if(!a) {
    return-1
  }
  if(!b) {
    return 1
  }
  for(var c = a.toLowerCase().match(A.U.YJ), d = b.toLowerCase().match(A.U.YJ), f = Math.min(c.length, d.length), g = 0;g < f;g++) {
    var k = c[g], l = d[g];
    if(k != l) {
      return c = parseInt(k, 10), !isNaN(c) && (d = parseInt(l, 10), !isNaN(d) && c - d) ? c - d : k < l ? -1 : 1
    }
  }
  return c.length != d.length ? c.length - d.length : a < b ? -1 : 1
};
A.U.eM = /^[a-zA-Z0-9\-_.!~*'()]*$/;
A.U.rs = function(a) {
  a = "" + a;
  return!A.U.eM.test(a) ? encodeURIComponent(a) : a
};
A.U.PA = function(a) {
  return decodeURIComponent(a.replace(/\+/g, " "))
};
A.U.sN = function(a, b) {
  return a.replace(/(\r\n|\r|\n)/g, b ? "<br />" : "<br>")
};
A.U.NI = function(a) {
  if(!A.U.tL.test(a)) {
    return a
  }
  -1 != a.indexOf("&") && (a = a.replace(A.U.uL, "&amp;"));
  -1 != a.indexOf("<") && (a = a.replace(A.U.mN, "&lt;"));
  -1 != a.indexOf(">") && (a = a.replace(A.U.JM, "&gt;"));
  -1 != a.indexOf('"') && (a = a.replace(A.U.EN, "&quot;"));
  return a
};
A.U.uL = /&/g;
A.U.mN = /</g;
A.U.JM = />/g;
A.U.EN = /\"/g;
A.U.tL = /[&<>\"]/;
A.U.UK = function(a) {
  return A.U.contains(a, "&") ? "document" in A.global ? A.U.eO(a) : A.U.fO(a) : a
};
A.U.eO = function(a) {
  var b = {"&amp;":"&", "&lt;":"<", "&gt;":">", "&quot;":'"'}, c = document.createElement("div");
  return a.replace(A.U.nL, function(a, f) {
    var g = b[a];
    if(g) {
      return g
    }
    if("#" == f.charAt(0)) {
      var k = Number("0" + f.substr(1));
      isNaN(k) || (g = String.fromCharCode(k))
    }
    g || (c.innerHTML = a + " ", g = c.firstChild.nodeValue.slice(0, -1));
    return b[a] = g
  })
};
A.U.fO = function(a) {
  return a.replace(/&([^;]+);/g, function(a, c) {
    switch(c) {
      case "amp":
        return"&";
      case "lt":
        return"<";
      case "gt":
        return">";
      case "quot":
        return'"';
      default:
        if("#" == c.charAt(0)) {
          var d = Number("0" + c.substr(1));
          if(!isNaN(d)) {
            return String.fromCharCode(d)
          }
        }
        return a
    }
  })
};
A.U.nL = /&([^;\s<&]+);?/g;
A.U.rS = function(a, b) {
  return A.U.sN(a.replace(/  /g, " &#160;"), b)
};
A.U.fS = function(a, b) {
  for(var c = b.length, d = 0;d < c;d++) {
    var f = 1 == c ? b : b.charAt(d);
    if(a.charAt(0) == f && a.charAt(a.length - 1) == f) {
      return a.substring(1, a.length - 1)
    }
  }
  return a
};
A.U.truncate = function(a, b, c) {
  c && (a = A.U.UK(a));
  a.length > b && (a = a.substring(0, b - 3) + "...");
  c && (a = A.U.NI(a));
  return a
};
A.U.lS = function(a, b, c, d) {
  c && (a = A.U.UK(a));
  if(d && a.length > b) {
    d > b && (d = b), a = a.substring(0, b - d) + "..." + a.substring(a.length - d)
  }else {
    if(a.length > b) {
      var d = Math.floor(b / 2), f = a.length - d, a = a.substring(0, d + b % 2) + "..." + a.substring(f)
    }
  }
  c && (a = A.U.NI(a));
  return a
};
A.U.UF = {"\x00":"\\0", "\u0008":"\\b", "\u000c":"\\f", "\n":"\\n", "\r":"\\r", "\t":"\\t", "\x0B":"\\x0B", '"':'\\"', "\\":"\\\\"};
A.U.Py = {"'":"\\'"};
A.U.quote = function(a) {
  a = "" + a;
  if(a.quote) {
    return a.quote()
  }
  for(var b = ['"'], c = 0;c < a.length;c++) {
    var d = a.charAt(c), f = d.charCodeAt(0);
    b[c + 1] = A.U.UF[d] || (31 < f && 127 > f ? d : A.U.$H(d))
  }
  b.push('"');
  return b.join("")
};
A.U.sQ = function(a) {
  for(var b = [], c = 0;c < a.length;c++) {
    b[c] = A.U.$H(a.charAt(c))
  }
  return b.join("")
};
A.U.$H = function(a) {
  if(a in A.U.Py) {
    return A.U.Py[a]
  }
  if(a in A.U.UF) {
    return A.U.Py[a] = A.U.UF[a]
  }
  var b = a, c = a.charCodeAt(0);
  if(31 < c && 127 > c) {
    b = a
  }else {
    if(256 > c) {
      if(b = "\\x", 16 > c || 256 < c) {
        b += "0"
      }
    }else {
      b = "\\u", 4096 > c && (b += "0")
    }
    b += c.toString(16).toUpperCase()
  }
  return A.U.Py[a] = b
};
A.U.iS = function(a) {
  for(var b = {}, c = 0;c < a.length;c++) {
    b[a.charAt(c)] = j
  }
  return b
};
A.U.contains = function(a, b) {
  return-1 != a.indexOf(b)
};
A.U.gQ = function(a, b) {
  return a && b ? a.split(b).length - 1 : 0
};
A.U.Jz = function(a, b, c) {
  var d = a;
  0 <= b && b < a.length && 0 < c && (d = a.substr(0, b) + a.substr(b + c, a.length - b - c));
  return d
};
A.U.remove = function(a, b) {
  var c = RegExp(A.U.oK(b), "");
  return a.replace(c, "")
};
A.U.qK = function(a, b) {
  var c = RegExp(A.U.oK(b), "g");
  return a.replace(c, "")
};
A.U.oK = function(a) {
  return("" + a).replace(/([-()\[\]{}+?*.$\^|,:#<!\\])/g, "\\$1").replace(/\x08/g, "\\x08")
};
A.U.repeat = function(a, b) {
  return Array(b + 1).join(a)
};
A.U.JR = function(a, b, c) {
  a = A.Uo(c) ? a.toFixed(c) : "" + a;
  c = a.indexOf(".");
  -1 == c && (c = a.length);
  return A.U.repeat("0", Math.max(0, b - c)) + a
};
A.U.nN = function(a) {
  return a == p ? "" : "" + a
};
A.U.AL = function(a) {
  return Array.prototype.join.call(arguments, "")
};
A.U.AI = function() {
  return Math.floor(2147483648 * Math.random()).toString(36) + Math.abs(Math.floor(2147483648 * Math.random()) ^ A.now()).toString(36)
};
A.U.hH = function(a, b) {
  for(var c = 0, d = A.U.trim("" + a).split("."), f = A.U.trim("" + b).split("."), g = Math.max(d.length, f.length), k = 0;0 == c && k < g;k++) {
    var l = d[k] || "", n = f[k] || "", m = RegExp("(\\d*)(\\D*)", "g"), o = RegExp("(\\d*)(\\D*)", "g");
    do {
      var v = m.exec(l) || ["", "", ""], w = o.exec(n) || ["", "", ""];
      if(0 == v[0].length && 0 == w[0].length) {
        break
      }
      c = A.U.TB(0 == v[1].length ? 0 : parseInt(v[1], 10), 0 == w[1].length ? 0 : parseInt(w[1], 10)) || A.U.TB(0 == v[2].length, 0 == w[2].length) || A.U.TB(v[2], w[2])
    }while(0 == c)
  }
  return c
};
A.U.TB = function(a, b) {
  return a < b ? -1 : a > b ? 1 : 0
};
A.U.mL = 4294967296;
A.U.UQ = function(a) {
  for(var b = 0, c = 0;c < a.length;++c) {
    b = 31 * b + a.charCodeAt(c), b %= A.U.mL
  }
  return b
};
A.U.hO = 2147483648 * Math.random() | 0;
A.U.iQ = function() {
  return"goog_" + A.U.hO++
};
A.U.jS = function(a) {
  var b = Number(a);
  return 0 == b && A.U.ri(a) ? NaN : b
};
A.U.QK = {};
A.U.hS = function(a) {
  return A.U.QK[a] || (A.U.QK[a] = ("" + a).replace(/\-([a-z])/g, function(a, c) {
    return c.toUpperCase()
  }))
};
A.U.RK = {};
A.U.kS = function(a) {
  return A.U.RK[a] || (A.U.RK[a] = ("" + a).replace(/([A-Z])/g, "-$1").toLowerCase())
};
A.Xc = {};
A.Xc.Sk = A.eB;
function da(a, b) {
  b.unshift(a);
  ba.call(this, A.U.ZN.apply(p, b));
  b.shift()
}
A.e(da, ba);
da.prototype.name = "AssertionError";
A.Xc.Em = function(a, b, c, d) {
  var f = "Assertion failed";
  if(c) {
    var f = f + (": " + c), g = d
  }else {
    a && (f += ": " + a, g = b)
  }
  e(new da("" + f, g || []))
};
A.Xc.assert = function(a, b, c) {
  A.Xc.Sk && !a && A.Xc.Em("", p, b, Array.prototype.slice.call(arguments, 2));
  return a
};
A.Xc.vQ = function(a, b) {
  A.Xc.Sk && e(new da("Failure" + (a ? ": " + a : ""), Array.prototype.slice.call(arguments, 1)))
};
A.Xc.MP = function(a, b, c) {
  A.Xc.Sk && !A.lJ(a) && A.Xc.Em("Expected number but got %s: %s.", [A.Hj(a), a], b, Array.prototype.slice.call(arguments, 2));
  return a
};
A.Xc.OP = function(a, b, c) {
  A.Xc.Sk && !A.Qc(a) && A.Xc.Em("Expected string but got %s: %s.", [A.Hj(a), a], b, Array.prototype.slice.call(arguments, 2));
  return a
};
A.Xc.KP = function(a, b, c) {
  A.Xc.Sk && !A.Ey(a) && A.Xc.Em("Expected function but got %s: %s.", [A.Hj(a), a], b, Array.prototype.slice.call(arguments, 2));
  return a
};
A.Xc.NP = function(a, b, c) {
  A.Xc.Sk && !A.gn(a) && A.Xc.Em("Expected object but got %s: %s.", [A.Hj(a), a], b, Array.prototype.slice.call(arguments, 2));
  return a
};
A.Xc.IP = function(a, b, c) {
  A.Xc.Sk && !A.isArray(a) && A.Xc.Em("Expected array but got %s: %s.", [A.Hj(a), a], b, Array.prototype.slice.call(arguments, 2));
  return a
};
A.Xc.JP = function(a, b, c) {
  A.Xc.Sk && !A.cN(a) && A.Xc.Em("Expected boolean but got %s: %s.", [A.Hj(a), a], b, Array.prototype.slice.call(arguments, 2));
  return a
};
A.Xc.LP = function(a, b, c, d) {
  A.Xc.Sk && !(a instanceof b) && A.Xc.Em("instanceof check failed.", p, c, Array.prototype.slice.call(arguments, 3))
};
A.$ = {};
A.Yn = j;
A.$.KR = function(a) {
  return a[a.length - 1]
};
A.$.Nd = Array.prototype;
A.$.indexOf = A.Yn && A.$.Nd.indexOf ? function(a, b, c) {
  return A.$.Nd.indexOf.call(a, b, c)
} : function(a, b, c) {
  c = c == p ? 0 : 0 > c ? Math.max(0, a.length + c) : c;
  if(A.Qc(a)) {
    return!A.Qc(b) || 1 != b.length ? -1 : a.indexOf(b, c)
  }
  for(;c < a.length;c++) {
    if(c in a && a[c] === b) {
      return c
    }
  }
  return-1
};
A.$.lastIndexOf = A.Yn && A.$.Nd.lastIndexOf ? function(a, b, c) {
  return A.$.Nd.lastIndexOf.call(a, b, c == p ? a.length - 1 : c)
} : function(a, b, c) {
  c = c == p ? a.length - 1 : c;
  0 > c && (c = Math.max(0, a.length + c));
  if(A.Qc(a)) {
    return!A.Qc(b) || 1 != b.length ? -1 : a.lastIndexOf(b, c)
  }
  for(;0 <= c;c--) {
    if(c in a && a[c] === b) {
      return c
    }
  }
  return-1
};
A.$.forEach = A.Yn && A.$.Nd.forEach ? function(a, b, c) {
  A.$.Nd.forEach.call(a, b, c)
} : function(a, b, c) {
  for(var d = a.length, f = A.Qc(a) ? a.split("") : a, g = 0;g < d;g++) {
    g in f && b.call(c, f[g], g, a)
  }
};
A.$.jM = function(a, b) {
  for(var c = a.length, d = A.Qc(a) ? a.split("") : a, c = c - 1;0 <= c;--c) {
    c in d && b.call(h, d[c], c, a)
  }
};
A.$.filter = A.Yn && A.$.Nd.filter ? function(a, b, c) {
  return A.$.Nd.filter.call(a, b, c)
} : function(a, b, c) {
  for(var d = a.length, f = [], g = 0, k = A.Qc(a) ? a.split("") : a, l = 0;l < d;l++) {
    if(l in k) {
      var n = k[l];
      b.call(c, n, l, a) && (f[g++] = n)
    }
  }
  return f
};
A.$.map = A.Yn && A.$.Nd.map ? function(a, b, c) {
  return A.$.Nd.map.call(a, b, c)
} : function(a, b, c) {
  for(var d = a.length, f = Array(d), g = A.Qc(a) ? a.split("") : a, k = 0;k < d;k++) {
    k in g && (f[k] = b.call(c, g[k], k, a))
  }
  return f
};
A.$.reduce = function(a, b, c, d) {
  if(a.reduce) {
    return d ? a.reduce(A.bind(b, d), c) : a.reduce(b, c)
  }
  var f = c;
  A.$.forEach(a, function(c, k) {
    f = b.call(d, f, c, k, a)
  });
  return f
};
A.$.reduceRight = function(a, b, c, d) {
  if(a.reduceRight) {
    return d ? a.reduceRight(A.bind(b, d), c) : a.reduceRight(b, c)
  }
  var f = c;
  A.$.jM(a, function(c, k) {
    f = b.call(d, f, c, k, a)
  });
  return f
};
A.$.some = A.Yn && A.$.Nd.some ? function(a, b, c) {
  return A.$.Nd.some.call(a, b, c)
} : function(a, b, c) {
  for(var d = a.length, f = A.Qc(a) ? a.split("") : a, g = 0;g < d;g++) {
    if(g in f && b.call(c, f[g], g, a)) {
      return j
    }
  }
  return q
};
A.$.every = A.Yn && A.$.Nd.every ? function(a, b, c) {
  return A.$.Nd.every.call(a, b, c)
} : function(a, b, c) {
  for(var d = a.length, f = A.Qc(a) ? a.split("") : a, g = 0;g < d;g++) {
    if(g in f && !b.call(c, f[g], g, a)) {
      return q
    }
  }
  return j
};
A.$.find = function(a, b, c) {
  b = A.$.hI(a, b, c);
  return 0 > b ? p : A.Qc(a) ? a.charAt(b) : a[b]
};
A.$.hI = function(a, b, c) {
  for(var d = a.length, f = A.Qc(a) ? a.split("") : a, g = 0;g < d;g++) {
    if(g in f && b.call(c, f[g], g, a)) {
      return g
    }
  }
  return-1
};
A.$.wQ = function(a, b, c) {
  b = A.$.gM(a, b, c);
  return 0 > b ? p : A.Qc(a) ? a.charAt(b) : a[b]
};
A.$.gM = function(a, b, c) {
  for(var d = a.length, f = A.Qc(a) ? a.split("") : a, d = d - 1;0 <= d;d--) {
    if(d in f && b.call(c, f[d], d, a)) {
      return d
    }
  }
  return-1
};
A.$.contains = function(a, b) {
  return 0 <= A.$.indexOf(a, b)
};
A.$.ri = function(a) {
  return 0 == a.length
};
A.$.clear = function(a) {
  if(!A.isArray(a)) {
    for(var b = a.length - 1;0 <= b;b--) {
      delete a[b]
    }
  }
  a.length = 0
};
A.$.XQ = function(a, b) {
  A.$.contains(a, b) || a.push(b)
};
A.$.UI = function(a, b, c) {
  A.$.splice(a, c, 0, b)
};
A.$.YQ = function(a, b, c) {
  A.eK(A.$.splice, a, c, 0).apply(p, b)
};
A.$.insertBefore = function(a, b, c) {
  var d;
  2 == arguments.length || 0 > (d = A.$.indexOf(a, c)) ? a.push(b) : A.$.UI(a, b, d)
};
A.$.remove = function(a, b) {
  var c = A.$.indexOf(a, b), d;
  (d = 0 <= c) && A.$.Jz(a, c);
  return d
};
A.$.Jz = function(a, b) {
  return 1 == A.$.Nd.splice.call(a, b, 1).length
};
A.$.SR = function(a, b, c) {
  b = A.$.hI(a, b, c);
  return 0 <= b ? (A.$.Jz(a, b), j) : q
};
A.$.concat = function(a) {
  return A.$.Nd.concat.apply(A.$.Nd, arguments)
};
A.$.Ca = function(a) {
  if(A.isArray(a)) {
    return A.$.concat(a)
  }
  for(var b = [], c = 0, d = a.length;c < d;c++) {
    b[c] = a[c]
  }
  return b
};
A.$.kG = function(a) {
  return A.isArray(a) ? A.$.concat(a) : A.$.Ca(a)
};
A.$.extend = function(a, b) {
  for(var c = 1;c < arguments.length;c++) {
    var d = arguments[c], f;
    if(A.isArray(d) || (f = A.$e(d)) && d.hasOwnProperty("callee")) {
      a.push.apply(a, d)
    }else {
      if(f) {
        for(var g = a.length, k = d.length, l = 0;l < k;l++) {
          a[g + l] = d[l]
        }
      }else {
        a.push(d)
      }
    }
  }
};
A.$.splice = function(a, b, c, d) {
  return A.$.Nd.splice.apply(a, A.$.slice(arguments, 1))
};
A.$.slice = function(a, b, c) {
  return 2 >= arguments.length ? A.$.Nd.slice.call(a, b) : A.$.Nd.slice.call(a, b, c)
};
A.$.QR = function(a, b) {
  for(var c = b || a, d = {}, f = 0, g = 0;g < a.length;) {
    var k = a[g++], l = A.gn(k) ? "o" + A.Tm(k) : (typeof k).charAt(0) + k;
    Object.prototype.hasOwnProperty.call(d, l) || (d[l] = j, c[f++] = k)
  }
  c.length = f
};
A.$.SG = function(a, b, c) {
  return A.$.TG(a, c || A.$.to, q, b)
};
A.$.UP = function(a, b, c) {
  return A.$.TG(a, b, j, h, c)
};
A.$.TG = function(a, b, c, d, f) {
  for(var g = 0, k = a.length, l;g < k;) {
    var n = g + k >> 1, m;
    m = c ? b.call(f, a[n], n, a) : b(d, a[n]);
    0 < m ? g = n + 1 : (k = n, l = !m)
  }
  return l ? g : ~g
};
A.$.sort = function(a, b) {
  A.$.Nd.sort.call(a, b || A.$.to)
};
A.$.dS = function(a, b) {
  for(var c = 0;c < a.length;c++) {
    a[c] = {index:c, value:a[c]}
  }
  var d = b || A.$.to;
  A.$.sort(a, function(a, b) {
    return d(a.value, b.value) || a.index - b.index
  });
  for(c = 0;c < a.length;c++) {
    a[c] = a[c].value
  }
};
A.$.cS = function(a, b, c) {
  var d = c || A.$.to;
  A.$.sort(a, function(a, c) {
    return d(a[b], c[b])
  })
};
A.$.vR = function(a, b, c) {
  for(var b = b || A.$.to, d = 1;d < a.length;d++) {
    var f = b(a[d - 1], a[d]);
    if(0 < f || 0 == f && c) {
      return q
    }
  }
  return j
};
A.$.DC = function(a, b, c) {
  if(!A.$e(a) || !A.$e(b) || a.length != b.length) {
    return q
  }
  for(var d = a.length, c = c || A.$.QL, f = 0;f < d;f++) {
    if(!c(a[f], b[f])) {
      return q
    }
  }
  return j
};
A.$.gH = function(a, b, c) {
  return A.$.DC(a, b, c)
};
A.$.fQ = function(a, b, c) {
  for(var c = c || A.$.to, d = Math.min(a.length, b.length), f = 0;f < d;f++) {
    var g = c(a[f], b[f]);
    if(0 != g) {
      return g
    }
  }
  return A.$.to(a.length, b.length)
};
A.$.to = function(a, b) {
  return a > b ? 1 : a < b ? -1 : 0
};
A.$.QL = function(a, b) {
  return a === b
};
A.$.SP = function(a, b, c) {
  c = A.$.SG(a, b, c);
  return 0 > c ? (A.$.UI(a, b, -(c + 1)), j) : q
};
A.$.TP = function(a, b, c) {
  b = A.$.SG(a, b, c);
  return 0 <= b ? A.$.Jz(a, b) : q
};
A.$.VP = function(a, b) {
  for(var c = {}, d = 0;d < a.length;d++) {
    var f = a[d], g = b(f, d, a);
    A.Uo(g) && (c[g] || (c[g] = [])).push(f)
  }
  return c
};
A.$.repeat = function(a, b) {
  for(var c = [], d = 0;d < b;d++) {
    c[d] = a
  }
  return c
};
A.$.iM = function(a) {
  for(var b = [], c = 0;c < arguments.length;c++) {
    var d = arguments[c];
    A.isArray(d) ? b.push.apply(b, A.$.iM.apply(p, d)) : b.push(d)
  }
  return b
};
A.$.rotate = function(a, b) {
  a.length && (b %= a.length, 0 < b ? A.$.Nd.unshift.apply(a, a.splice(-b, b)) : 0 > b && A.$.Nd.push.apply(a, a.splice(0, -b)));
  return a
};
A.$.sS = function(a) {
  if(!arguments.length) {
    return[]
  }
  for(var b = [], c = 0;;c++) {
    for(var d = [], f = 0;f < arguments.length;f++) {
      var g = arguments[f];
      if(c >= g.length) {
        return b
      }
      d.push(g[c])
    }
    b.push(d)
  }
};
A.$.bS = function(a, b) {
  for(var c = b || Math.random, d = a.length - 1;0 < d;d--) {
    var f = Math.floor(c() * (d + 1)), g = a[d];
    a[d] = a[f];
    a[f] = g
  }
};
A.debug.Xe = {};
A.debug.NO = s();
A.debug.Xe.Rr = [];
A.debug.Xe.HE = [];
A.debug.Xe.KJ = q;
A.debug.Xe.lF = function(a) {
  A.debug.Xe.Rr[A.debug.Xe.Rr.length] = a;
  if(A.debug.Xe.KJ) {
    for(var b = A.debug.Xe.HE, c = 0;c < b.length;c++) {
      a(A.bind(b[c].pO, b[c]))
    }
  }
};
A.debug.Xe.DR = function(a) {
  A.debug.Xe.KJ = j;
  for(var b = A.bind(a.pO, a), c = 0;c < A.debug.Xe.Rr.length;c++) {
    A.debug.Xe.Rr[c](b)
  }
  A.debug.Xe.HE.push(a)
};
A.debug.Xe.nS = function(a) {
  for(var b = A.debug.Xe.HE, a = A.bind(a.pS, a), c = 0;c < A.debug.Xe.Rr.length;c++) {
    A.debug.Xe.Rr[c](a)
  }
  b.length--
};
A.debug.rQ = {jK:aa()};
A.userAgent = {};
A.userAgent.zG = q;
A.userAgent.yG = q;
A.userAgent.DG = q;
A.userAgent.cB = q;
A.userAgent.CG = q;
A.userAgent.dL = q;
A.userAgent.$p = A.userAgent.zG || A.userAgent.yG || A.userAgent.cB || A.userAgent.DG || A.userAgent.CG;
A.userAgent.HI = function() {
  return A.global.navigator ? A.global.navigator.userAgent : p
};
A.userAgent.Yx = function() {
  return A.global.navigator
};
A.userAgent.UM = function() {
  A.userAgent.Ft = q;
  A.userAgent.JH = q;
  A.userAgent.xx = q;
  A.userAgent.KH = q;
  A.userAgent.IH = q;
  var a;
  if(!A.userAgent.$p && (a = A.userAgent.HI())) {
    var b = A.userAgent.Yx();
    A.userAgent.Ft = 0 == a.indexOf("Opera");
    A.userAgent.JH = !A.userAgent.Ft && -1 != a.indexOf("MSIE");
    A.userAgent.xx = !A.userAgent.Ft && -1 != a.indexOf("WebKit");
    A.userAgent.KH = A.userAgent.xx && -1 != a.indexOf("Mobile");
    A.userAgent.IH = !A.userAgent.Ft && !A.userAgent.xx && "Gecko" == b.product
  }
};
A.userAgent.$p || A.userAgent.UM();
A.userAgent.gB = A.userAgent.$p ? A.userAgent.CG : A.userAgent.Ft;
A.userAgent.Jj = A.userAgent.$p ? A.userAgent.zG : A.userAgent.JH;
A.userAgent.pw = A.userAgent.$p ? A.userAgent.yG : A.userAgent.IH;
A.userAgent.gm = A.userAgent.$p ? A.userAgent.DG || A.userAgent.cB : A.userAgent.xx;
A.userAgent.fP = A.userAgent.cB || A.userAgent.KH;
A.userAgent.oP = A.userAgent.gm;
A.userAgent.XL = function() {
  var a = A.userAgent.Yx();
  return a && a.platform || ""
};
A.userAgent.iB = A.userAgent.XL();
A.userAgent.BG = q;
A.userAgent.EG = q;
A.userAgent.AG = q;
A.userAgent.FG = q;
A.userAgent.Os = A.userAgent.BG || A.userAgent.EG || A.userAgent.AG || A.userAgent.FG;
A.userAgent.TM = function() {
  A.userAgent.UL = A.U.contains(A.userAgent.iB, "Mac");
  A.userAgent.VL = A.U.contains(A.userAgent.iB, "Win");
  A.userAgent.TL = A.U.contains(A.userAgent.iB, "Linux");
  A.userAgent.WL = !!A.userAgent.Yx() && A.U.contains(A.userAgent.Yx().appVersion || "", "X11")
};
A.userAgent.Os || A.userAgent.TM();
A.userAgent.cP = A.userAgent.Os ? A.userAgent.BG : A.userAgent.UL;
A.userAgent.AP = A.userAgent.Os ? A.userAgent.EG : A.userAgent.VL;
A.userAgent.ZO = A.userAgent.Os ? A.userAgent.AG : A.userAgent.TL;
A.userAgent.BP = A.userAgent.Os ? A.userAgent.FG : A.userAgent.WL;
A.userAgent.YL = function() {
  var a = "", b;
  A.userAgent.gB && A.global.opera ? (a = A.global.opera.version, a = "function" == typeof a ? a() : a) : (A.userAgent.pw ? b = /rv\:([^\);]+)(\)|;)/ : A.userAgent.Jj ? b = /MSIE\s+([^\);]+)(\)|;)/ : A.userAgent.gm && (b = /WebKit\/(\S+)/), b && (a = (a = b.exec(A.userAgent.HI())) ? a[1] : ""));
  return A.userAgent.Jj && (b = A.userAgent.tM(), b > parseFloat(a)) ? "" + b : a
};
A.userAgent.tM = function() {
  var a = A.global.document;
  return a ? a.documentMode : h
};
A.userAgent.VERSION = A.userAgent.YL();
A.userAgent.gH = function(a, b) {
  return A.U.hH(a, b)
};
A.userAgent.wJ = {};
A.userAgent.Zo = function(a) {
  return A.userAgent.dL || A.userAgent.wJ[a] || (A.userAgent.wJ[a] = 0 <= A.U.hH(A.userAgent.VERSION, a))
};
A.userAgent.cJ = {};
A.userAgent.bJ = function() {
  return A.userAgent.cJ[9] || (A.userAgent.cJ[9] = A.userAgent.Jj && !!document.documentMode && 9 <= document.documentMode)
};
A.I = {};
!A.userAgent.Jj || A.userAgent.bJ();
var ea = !A.userAgent.Jj || A.userAgent.bJ(), fa = A.userAgent.Jj && !A.userAgent.Zo("8");
!A.userAgent.gm || A.userAgent.Zo("528");
A.userAgent.pw && A.userAgent.Zo("1.9b") || A.userAgent.Jj && A.userAgent.Zo("8") || A.userAgent.gB && A.userAgent.Zo("9.5") || A.userAgent.gm && A.userAgent.Zo("528");
!A.userAgent.pw || A.userAgent.Zo("8");
A.ZL = {};
A.ZL.UO = s();
function ga() {
}
ga.prototype.PH = q;
ga.prototype.aj = function() {
  this.PH || (this.PH = j, this.Dm())
};
ga.prototype.Dm = function() {
  this.RL && A.OH.apply(p, this.RL)
};
A.aj = function(a) {
  a && "function" == typeof a.aj && a.aj()
};
A.OH = function(a) {
  for(var b = 0, c = arguments.length;b < c;++b) {
    var d = arguments[b];
    A.$e(d) ? A.OH.apply(p, d) : A.aj(d)
  }
};
function ha(a, b) {
  this.type = a;
  this.currentTarget = this.target = b
}
A.e(ha, ga);
z = ha.prototype;
z.Dm = function() {
  delete this.type;
  delete this.target;
  delete this.currentTarget
};
z.Ap = q;
z.sv = j;
z.stopPropagation = function() {
  this.Ap = j
};
z.preventDefault = function() {
  this.sv = q
};
ha.stopPropagation = function(a) {
  a.stopPropagation()
};
ha.preventDefault = function(a) {
  a.preventDefault()
};
var ka = {dB:"click", EO:"dblclick", fm:"mousedown", Ns:"mouseup", Ms:"mouseover", Ls:"mouseout", Xn:"mousemove", rP:"selectstart", XO:"keypress", WO:"keydown", YO:"keyup", xO:"blur", PO:"focus", FO:"deactivate", QO:A.userAgent.Jj ? "focusin" : "DOMFocusIn", RO:A.userAgent.Jj ? "focusout" : "DOMFocusOut", zO:"change", qP:"select", sP:"submit", VO:"input", mP:"propertychange", JO:"dragstart", GO:"dragenter", IO:"dragover", HO:"dragleave", KO:"drop", wP:"touchstart", vP:"touchmove", uP:"touchend",
tP:"touchcancel", BO:"contextmenu", MO:"error", TO:"help", $O:"load", bP:"losecapture", nP:"readystatechange", qw:"resize", pP:"scroll", yP:"unload", SO:"hashchange", iP:"pagehide", jP:"pageshow", lP:"popstate", CO:"copy", kP:"paste", DO:"cut", uO:"beforecopy", vO:"beforecut", wO:"beforepaste", dP:"message", AO:"connect", xP:A.userAgent.gm ? "webkitTransitionEnd" : A.userAgent.gB ? "oTransitionEnd" : "transitionend"};
A.Bp = {};
A.Bp.object = function(a, b) {
  return b
};
A.Bp.RF = function(a) {
  A.Bp.RF[" "](a);
  return a
};
A.Bp.RF[" "] = A.WJ;
A.Bp.BL = function(a) {
  try {
    return A.Bp.RF(a.nodeName), j
  }catch(b) {
  }
  return q
};
function la(a, b) {
  a && this.Ha(a, b)
}
A.e(la, ha);
z = la.prototype;
z.target = p;
z.relatedTarget = p;
z.offsetX = 0;
z.offsetY = 0;
z.clientX = 0;
z.clientY = 0;
z.screenX = 0;
z.screenY = 0;
z.button = 0;
z.keyCode = 0;
z.charCode = 0;
z.ctrlKey = q;
z.altKey = q;
z.shiftKey = q;
z.metaKey = q;
z.Im = p;
z.Ha = function(a, b) {
  var c = this.type = a.type;
  ha.call(this, c);
  this.target = a.target || a.srcElement;
  this.currentTarget = b;
  var d = a.relatedTarget;
  d ? A.userAgent.pw && (A.Bp.BL(d) || (d = p)) : c == ka.Ms ? d = a.fromElement : c == ka.Ls && (d = a.toElement);
  this.relatedTarget = d;
  this.offsetX = A.userAgent.gm || a.offsetX !== h ? a.offsetX : a.layerX;
  this.offsetY = A.userAgent.gm || a.offsetY !== h ? a.offsetY : a.layerY;
  this.clientX = a.clientX !== h ? a.clientX : a.pageX;
  this.clientY = a.clientY !== h ? a.clientY : a.pageY;
  this.screenX = a.screenX || 0;
  this.screenY = a.screenY || 0;
  this.button = a.button;
  this.keyCode = a.keyCode || 0;
  this.charCode = a.charCode || ("keypress" == c ? a.keyCode : 0);
  this.ctrlKey = a.ctrlKey;
  this.altKey = a.altKey;
  this.shiftKey = a.shiftKey;
  this.metaKey = a.metaKey;
  this.state = a.state;
  this.Im = a;
  delete this.sv;
  delete this.Ap
};
z.stopPropagation = function() {
  la.f.stopPropagation.call(this);
  this.Im.stopPropagation ? this.Im.stopPropagation() : this.Im.cancelBubble = j
};
z.preventDefault = function() {
  la.f.preventDefault.call(this);
  var a = this.Im;
  if(a.preventDefault) {
    a.preventDefault()
  }else {
    if(a.returnValue = q, fa) {
      try {
        if(a.ctrlKey || 112 <= a.keyCode && 123 >= a.keyCode) {
          a.keyCode = -1
        }
      }catch(b) {
      }
    }
  }
};
z.Dm = function() {
  la.f.Dm.call(this);
  this.relatedTarget = this.currentTarget = this.target = this.Im = p
};
A.I.IG = s();
A.I.IG.prototype.nb = s();
A.I.IG.prototype.ed = s();
function na() {
}
var oa = 0;
z = na.prototype;
z.key = 0;
z.Cp = q;
z.NB = q;
z.Ha = function(a, b, c, d, f, g) {
  A.Ey(a) ? this.gJ = j : a && a.handleEvent && A.Ey(a.handleEvent) ? this.gJ = q : e(Error("Invalid listener argument"));
  this.Br = a;
  this.kK = b;
  this.src = c;
  this.type = d;
  this.capture = !!f;
  this.ly = g;
  this.NB = q;
  this.key = ++oa;
  this.Cp = q
};
z.handleEvent = function(a) {
  return this.gJ ? this.Br.call(this.ly || this.src, a) : this.Br.handleEvent.call(this.Br, a)
};
A.object = {};
A.object.forEach = function(a, b, c) {
  for(var d in a) {
    b.call(c, a[d], d, a)
  }
};
A.object.filter = function(a, b, c) {
  var d = {}, f;
  for(f in a) {
    b.call(c, a[f], f, a) && (d[f] = a[f])
  }
  return d
};
A.object.map = function(a, b, c) {
  var d = {}, f;
  for(f in a) {
    d[f] = b.call(c, a[f], f, a)
  }
  return d
};
A.object.some = function(a, b, c) {
  for(var d in a) {
    if(b.call(c, a[d], d, a)) {
      return j
    }
  }
  return q
};
A.object.every = function(a, b, c) {
  for(var d in a) {
    if(!b.call(c, a[d], d, a)) {
      return q
    }
  }
  return j
};
A.object.Sm = function(a) {
  var b = 0, c;
  for(c in a) {
    b++
  }
  return b
};
A.object.zQ = function(a) {
  for(var b in a) {
    return b
  }
};
A.object.AQ = function(a) {
  for(var b in a) {
    return a[b]
  }
};
A.object.contains = function(a, b) {
  return A.object.yq(a, b)
};
A.object.Ef = function(a) {
  var b = [], c = 0, d;
  for(d in a) {
    b[c++] = a[d]
  }
  return b
};
A.object.mg = function(a) {
  var b = [], c = 0, d;
  for(d in a) {
    b[c++] = d
  }
  return b
};
A.object.PQ = function(a, b) {
  for(var c = A.$e(b), d = c ? b : arguments, c = c ? 0 : 1;c < d.length && !(a = a[d[c]], !A.Uo(a));c++) {
  }
  return a
};
A.object.Yj = function(a, b) {
  return b in a
};
A.object.yq = function(a, b) {
  for(var c in a) {
    if(a[c] == b) {
      return j
    }
  }
  return q
};
A.object.hM = function(a, b, c) {
  for(var d in a) {
    if(b.call(c, a[d], d, a)) {
      return d
    }
  }
};
A.object.xQ = function(a, b, c) {
  return(b = A.object.hM(a, b, c)) && a[b]
};
A.object.ri = function(a) {
  for(var b in a) {
    return q
  }
  return j
};
A.object.clear = function(a) {
  for(var b in a) {
    delete a[b]
  }
};
A.object.remove = function(a, b) {
  var c;
  (c = b in a) && delete a[b];
  return c
};
A.object.add = function(a, b, c) {
  b in a && e(Error('The object already contains the key "' + b + '"'));
  A.object.set(a, b, c)
};
A.object.get = function(a, b, c) {
  return b in a ? a[b] : c
};
A.object.set = function(a, b, c) {
  a[b] = c
};
A.object.ZR = function(a, b, c) {
  return b in a ? a[b] : a[b] = c
};
A.object.Ca = function(a) {
  var b = {}, c;
  for(c in a) {
    b[c] = a[c]
  }
  return b
};
A.object.iO = function(a) {
  var b = A.Hj(a);
  if("object" == b || "array" == b) {
    if(a.Ca) {
      return a.Ca()
    }
    var b = "array" == b ? [] : {}, c;
    for(c in a) {
      b[c] = A.object.iO(a[c])
    }
    return b
  }
  return a
};
A.object.cO = function(a) {
  var b = {}, c;
  for(c in a) {
    b[a[c]] = c
  }
  return b
};
A.object.JG = "constructor,hasOwnProperty,isPrototypeOf,propertyIsEnumerable,toLocaleString,toString,valueOf".split(",");
A.object.extend = function(a, b) {
  for(var c, d, f = 1;f < arguments.length;f++) {
    d = arguments[f];
    for(c in d) {
      a[c] = d[c]
    }
    for(var g = 0;g < A.object.JG.length;g++) {
      c = A.object.JG[g], Object.prototype.hasOwnProperty.call(d, c) && (a[c] = d[c])
    }
  }
};
A.object.create = function(a) {
  var b = arguments.length;
  if(1 == b && A.isArray(arguments[0])) {
    return A.object.create.apply(p, arguments[0])
  }
  b % 2 && e(Error("Uneven number of arguments"));
  for(var c = {}, d = 0;d < b;d += 2) {
    c[arguments[d]] = arguments[d + 1]
  }
  return c
};
A.object.JL = function(a) {
  var b = arguments.length;
  if(1 == b && A.isArray(arguments[0])) {
    return A.object.JL.apply(p, arguments[0])
  }
  for(var c = {}, d = 0;d < b;d++) {
    c[arguments[d]] = j
  }
  return c
};
A.I.tO = q;
A.I.fe = {};
A.I.Pg = {};
A.I.Kk = {};
A.I.yN = "on";
A.I.TE = {};
A.I.yR = "_";
A.I.nb = function(a, b, c, d, f) {
  if(b) {
    if(A.isArray(b)) {
      for(var g = 0;g < b.length;g++) {
        A.I.nb(a, b[g], c, d, f)
      }
      return p
    }
    var d = !!d, k = A.I.Pg;
    b in k || (k[b] = {zc:0, Bi:0});
    k = k[b];
    d in k || (k[d] = {zc:0, Bi:0}, k.zc++);
    var k = k[d], l = A.Tm(a), n;
    k.Bi++;
    if(k[l]) {
      n = k[l];
      for(g = 0;g < n.length;g++) {
        if(k = n[g], k.Br == c && k.ly == f) {
          if(k.Cp) {
            break
          }
          return n[g].key
        }
      }
    }else {
      n = k[l] = [], k.zc++
    }
    g = A.I.EM();
    g.src = a;
    k = new na;
    k.Ha(c, g, a, b, d, f);
    c = k.key;
    g.key = c;
    n.push(k);
    A.I.fe[c] = k;
    A.I.Kk[l] || (A.I.Kk[l] = []);
    A.I.Kk[l].push(k);
    a.addEventListener ? (a == A.global || !a.AH) && a.addEventListener(b, g, d) : a.attachEvent(A.I.xI(b), g);
    return c
  }
  e(Error("Invalid event type"))
};
A.I.EM = function() {
  var a = A.I.hu, b = ea ? function(c) {
    return a.call(b.src, b.key, c)
  } : function(c) {
    c = a.call(b.src, b.key, c);
    if(!c) {
      return c
    }
  };
  return b
};
A.I.kN = function(a, b, c, d, f) {
  if(A.isArray(b)) {
    for(var g = 0;g < b.length;g++) {
      A.I.kN(a, b[g], c, d, f)
    }
  }else {
    a = A.I.nb(a, b, c, d, f), A.I.fe[a].NB = j
  }
};
A.I.zR = function(a, b, c, d, f) {
  b.nb(a, c, d, f)
};
A.I.ed = function(a, b, c, d, f) {
  if(A.isArray(b)) {
    for(var g = 0;g < b.length;g++) {
      A.I.ed(a, b[g], c, d, f)
    }
    return p
  }
  d = !!d;
  a = A.I.eD(a, b, d);
  if(!a) {
    return q
  }
  for(g = 0;g < a.length;g++) {
    if(a[g].Br == c && a[g].capture == d && a[g].ly == f) {
      return A.I.MA(a[g].key)
    }
  }
  return q
};
A.I.MA = function(a) {
  if(!A.I.fe[a]) {
    return q
  }
  var b = A.I.fe[a];
  if(b.Cp) {
    return q
  }
  var c = b.src, d = b.type, f = b.kK, g = b.capture;
  c.removeEventListener ? (c == A.global || !c.AH) && c.removeEventListener(d, f, g) : c.detachEvent && c.detachEvent(A.I.xI(d), f);
  c = A.Tm(c);
  f = A.I.Pg[d][g][c];
  if(A.I.Kk[c]) {
    var k = A.I.Kk[c];
    A.$.remove(k, b);
    0 == k.length && delete A.I.Kk[c]
  }
  b.Cp = j;
  f.UJ = j;
  A.I.dH(d, g, c, f);
  delete A.I.fe[a];
  return j
};
A.I.mS = function(a, b, c, d, f) {
  b.ed(a, c, d, f)
};
A.I.dH = function(a, b, c, d) {
  if(!d.Wy && d.UJ) {
    for(var f = 0, g = 0;f < d.length;f++) {
      d[f].Cp ? d[f].kK.src = p : (f != g && (d[g] = d[f]), g++)
    }
    d.length = g;
    d.UJ = q;
    0 == g && (delete A.I.Pg[a][b][c], A.I.Pg[a][b].zc--, 0 == A.I.Pg[a][b].zc && (delete A.I.Pg[a][b], A.I.Pg[a].zc--), 0 == A.I.Pg[a].zc && delete A.I.Pg[a])
  }
};
A.I.qK = function(a, b, c) {
  var d = 0, f = b == p, g = c == p, c = !!c;
  if(a == p) {
    A.object.forEach(A.I.Kk, function(a) {
      for(var k = a.length - 1;0 <= k;k--) {
        var l = a[k];
        if((f || b == l.type) && (g || c == l.capture)) {
          A.I.MA(l.key), d++
        }
      }
    })
  }else {
    if(a = A.Tm(a), A.I.Kk[a]) {
      for(var a = A.I.Kk[a], k = a.length - 1;0 <= k;k--) {
        var l = a[k];
        if((f || b == l.type) && (g || c == l.capture)) {
          A.I.MA(l.key), d++
        }
      }
    }
  }
  return d
};
A.I.HQ = function(a, b, c) {
  return A.I.eD(a, b, c) || []
};
A.I.eD = function(a, b, c) {
  var d = A.I.Pg;
  return b in d && (d = d[b], c in d && (d = d[c], a = A.Tm(a), d[a])) ? d[a] : p
};
A.I.GQ = function(a, b, c, d, f) {
  d = !!d;
  if(a = A.I.eD(a, b, d)) {
    for(b = 0;b < a.length;b++) {
      if(!a[b].Cp && a[b].Br == c && a[b].capture == d && a[b].ly == f) {
        return a[b]
      }
    }
  }
  return p
};
A.I.SQ = function(a, b, c) {
  var a = A.Tm(a), d = A.I.Kk[a];
  if(d) {
    var f = A.Uo(b), g = A.Uo(c);
    return f && g ? (d = A.I.Pg[b], !!d && !!d[c] && a in d[c]) : !f && !g ? j : A.$.some(d, function(a) {
      return f && a.type == b || g && a.capture == c
    })
  }
  return q
};
A.I.tQ = function(a) {
  var b = [], c;
  for(c in a) {
    a[c] && a[c].id ? b.push(c + " = " + a[c] + " (" + a[c].id + ")") : b.push(c + " = " + a[c])
  }
  return b.join("\n")
};
A.I.xI = function(a) {
  return a in A.I.TE ? A.I.TE[a] : A.I.TE[a] = A.I.yN + a
};
A.I.yQ = function(a, b, c, d) {
  var f = A.I.Pg;
  return b in f && (f = f[b], c in f) ? A.I.Mq(f[c], a, b, c, d) : j
};
A.I.Mq = function(a, b, c, d, f) {
  var g = 1, b = A.Tm(b);
  if(a[b]) {
    a.Bi--;
    a = a[b];
    a.Wy ? a.Wy++ : a.Wy = 1;
    try {
      for(var k = a.length, l = 0;l < k;l++) {
        var n = a[l];
        n && !n.Cp && (g &= A.I.LC(n, f) !== q)
      }
    }finally {
      a.Wy--, A.I.dH(c, d, b, a)
    }
  }
  return Boolean(g)
};
A.I.LC = function(a, b) {
  var c = a.handleEvent(b);
  a.NB && A.I.MA(a.key);
  return c
};
A.I.NQ = function() {
  return A.object.Sm(A.I.fe)
};
A.I.dispatchEvent = function(a, b) {
  var c = b.type || b, d = A.I.Pg;
  if(!(c in d)) {
    return j
  }
  if(A.Qc(b)) {
    b = new ha(b, a)
  }else {
    if(b instanceof ha) {
      b.target = b.target || a
    }else {
      var f = b, b = new ha(c, a);
      A.object.extend(b, f)
    }
  }
  var f = 1, g, d = d[c], c = j in d, k;
  if(c) {
    g = [];
    for(k = a;k;k = k.YE) {
      g.push(k)
    }
    k = d[j];
    k.Bi = k.zc;
    for(var l = g.length - 1;!b.Ap && 0 <= l && k.Bi;l--) {
      b.currentTarget = g[l], f &= A.I.Mq(k, g[l], b.type, j, b) && b.sv != q
    }
  }
  if(q in d) {
    if(k = d[q], k.Bi = k.zc, c) {
      for(l = 0;!b.Ap && l < g.length && k.Bi;l++) {
        b.currentTarget = g[l], f &= A.I.Mq(k, g[l], b.type, q, b) && b.sv != q
      }
    }else {
      for(d = a;!b.Ap && d && k.Bi;d = d.YE) {
        b.currentTarget = d, f &= A.I.Mq(k, d, b.type, q, b) && b.sv != q
      }
    }
  }
  return Boolean(f)
};
A.I.MR = function(a) {
  A.I.hu = a.jK(A.I.hu)
};
A.I.hu = function(a, b) {
  if(!A.I.fe[a]) {
    return j
  }
  var c = A.I.fe[a], d = c.type, f = A.I.Pg;
  if(!(d in f)) {
    return j
  }
  var f = f[d], g, k;
  if(!ea) {
    g = b || A.BM();
    var l = j in f, n = q in f;
    if(l) {
      if(A.I.fN(g)) {
        return j
      }
      A.I.pN(g)
    }
    var m = new la;
    m.Ha(g, this);
    g = j;
    try {
      if(l) {
        for(var o = [], v = m.currentTarget;v;v = v.parentNode) {
          o.push(v)
        }
        k = f[j];
        k.Bi = k.zc;
        for(var w = o.length - 1;!m.Ap && 0 <= w && k.Bi;w--) {
          m.currentTarget = o[w], g &= A.I.Mq(k, o[w], d, j, m)
        }
        if(n) {
          k = f[q];
          k.Bi = k.zc;
          for(w = 0;!m.Ap && w < o.length && k.Bi;w++) {
            m.currentTarget = o[w], g &= A.I.Mq(k, o[w], d, q, m)
          }
        }
      }else {
        g = A.I.LC(c, m)
      }
    }finally {
      o && (o.length = 0), m.aj()
    }
    return g
  }
  d = new la(b, this);
  try {
    g = A.I.LC(c, d)
  }finally {
    d.aj()
  }
  return g
};
A.I.pN = function(a) {
  var b = q;
  if(0 == a.keyCode) {
    try {
      a.keyCode = -1;
      return
    }catch(c) {
      b = j
    }
  }
  if(b || a.returnValue == h) {
    a.returnValue = j
  }
};
A.I.fN = function(a) {
  return 0 > a.keyCode || a.returnValue != h
};
A.I.gO = 0;
A.I.OQ = function(a) {
  return a + "_" + A.I.gO++
};
A.debug.Xe.lF(function(a) {
  A.I.hu = a(A.I.hu)
});
function pa() {
}
A.e(pa, ga);
z = pa.prototype;
z.AH = j;
z.YE = p;
z.addEventListener = function(a, b, c, d) {
  A.I.nb(this, a, b, c, d)
};
z.removeEventListener = function(a, b, c, d) {
  A.I.ed(this, a, b, c, d)
};
z.dispatchEvent = function(a) {
  return A.I.dispatchEvent(this, a)
};
z.Dm = function() {
  pa.f.Dm.call(this);
  A.I.qK(this);
  this.YE = p
};
var qa = A.global.window;
A.Cu = {};
A.Cu.hN = function(a) {
  return/^\s*$/.test(a) ? q : /^[\],:{}\s\u2028\u2029]*$/.test(a.replace(/\\["\\\/bfnrtu]/g, "@").replace(/"[^"\\\n\r\u2028\u2029\x00-\x08\x10-\x1f\x80-\x9f]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, "]").replace(/(?:^|:|,)(?:[\s\u2028\u2029]*\[)+/g, ""))
};
A.Cu.parse = function(a) {
  a = "" + a;
  if(A.Cu.hN(a)) {
    try {
      return eval("(" + a + ")")
    }catch(b) {
    }
  }
  e(Error("Invalid JSON string: " + a))
};
A.Cu.oS = function(a) {
  return eval("(" + a + ")")
};
A.Cu.mb = function(a, b) {
  return(new sa(b)).mb(a)
};
function sa(a) {
  this.Kz = a
}
sa.prototype.mb = function(a) {
  var b = [];
  ta(this, a, b);
  return b.join("")
};
function ta(a, b, c) {
  switch(typeof b) {
    case "string":
      ua(b, c);
      break;
    case "number":
      c.push(isFinite(b) && !isNaN(b) ? b : "null");
      break;
    case "boolean":
      c.push(b);
      break;
    case "undefined":
      c.push("null");
      break;
    case "object":
      if(b == p) {
        c.push("null");
        break
      }
      if(A.isArray(b)) {
        var d = b.length;
        c.push("[");
        for(var f = "", g = 0;g < d;g++) {
          c.push(f), f = b[g], ta(a, a.Kz ? a.Kz.call(b, "" + g, f) : f, c), f = ","
        }
        c.push("]");
        break
      }
      c.push("{");
      d = "";
      for(g in b) {
        Object.prototype.hasOwnProperty.call(b, g) && (f = b[g], "function" != typeof f && (c.push(d), ua(g, c), c.push(":"), ta(a, a.Kz ? a.Kz.call(b, g, f) : f, c), d = ","))
      }
      c.push("}");
      break;
    case "function":
      break;
    default:
      e(Error("Unknown type: " + typeof b))
  }
}
var va = {'"':'\\"', "\\":"\\\\", "/":"\\/", "\u0008":"\\b", "\u000c":"\\f", "\n":"\\n", "\r":"\\r", "\t":"\\t", "\x0B":"\\u000b"}, wa = /\uffff/.test("\uffff") ? /[\\\"\x00-\x1f\x7f-\uffff]/g : /[\\\"\x00-\x1f\x7f-\xff]/g;
function ua(a, b) {
  b.push('"', a.replace(wa, function(a) {
    if(a in va) {
      return va[a]
    }
    var b = a.charCodeAt(0), f = "\\u";
    16 > b ? f += "000" : 256 > b ? f += "00" : 4096 > b && (f += "0");
    return va[a] = f + b.toString(16)
  }), '"')
}
;A.lj = {};
function xa() {
}
xa.prototype.XG = p;
xa.prototype.bu = function() {
  var a;
  if(!(a = this.XG)) {
    a = {}, ya(this) && (a[0] = j, a[1] = j), a = this.XG = a
  }
  return a
};
function za(a, b) {
  this.qO = a;
  this.BN = b
}
A.e(za, xa);
za.prototype.XB = function() {
  return this.qO()
};
za.prototype.bu = function() {
  return this.BN()
};
A.lj.Kj = function() {
  return A.lj.Kj.fI.XB()
};
A.lj.Kj.bu = function() {
  return A.lj.Kj.fI.bu()
};
A.lj.Kj.XR = function(a, b) {
  A.lj.Kj.zK(new za(a, b))
};
A.lj.Kj.zK = function(a) {
  A.lj.Kj.fI = a
};
function Aa() {
}
A.e(Aa, xa);
Aa.prototype.XB = function() {
  var a = ya(this);
  return a ? new ActiveXObject(a) : new XMLHttpRequest
};
Aa.prototype.GD = p;
function ya(a) {
  if(!a.GD && "undefined" == typeof XMLHttpRequest && "undefined" != typeof ActiveXObject) {
    for(var b = ["MSXML2.XMLHTTP.6.0", "MSXML2.XMLHTTP.3.0", "MSXML2.XMLHTTP", "Microsoft.XMLHTTP"], c = 0;c < b.length;c++) {
      var d = b[c];
      try {
        return new ActiveXObject(d), a.GD = d
      }catch(f) {
      }
    }
    e(Error("Could not create ActiveXObject. ActiveX might be disabled, or MSXML might not be installed"))
  }
  return a.GD
}
A.lj.Kj.zK(new Aa);
A.zd = {};
A.zd.Sm = function(a) {
  return"function" == typeof a.Sm ? a.Sm() : A.$e(a) || A.Qc(a) ? a.length : A.object.Sm(a)
};
A.zd.Ef = function(a) {
  if("function" == typeof a.Ef) {
    return a.Ef()
  }
  if(A.Qc(a)) {
    return a.split("")
  }
  if(A.$e(a)) {
    for(var b = [], c = a.length, d = 0;d < c;d++) {
      b.push(a[d])
    }
    return b
  }
  return A.object.Ef(a)
};
A.zd.mg = function(a) {
  if("function" == typeof a.mg) {
    return a.mg()
  }
  if("function" != typeof a.Ef) {
    if(A.$e(a) || A.Qc(a)) {
      for(var b = [], a = a.length, c = 0;c < a;c++) {
        b.push(c)
      }
      return b
    }
    return A.object.mg(a)
  }
};
A.zd.contains = function(a, b) {
  return"function" == typeof a.contains ? a.contains(b) : "function" == typeof a.yq ? a.yq(b) : A.$e(a) || A.Qc(a) ? A.$.contains(a, b) : A.object.yq(a, b)
};
A.zd.ri = function(a) {
  return"function" == typeof a.ri ? a.ri() : A.$e(a) || A.Qc(a) ? A.$.ri(a) : A.object.ri(a)
};
A.zd.clear = function(a) {
  "function" == typeof a.clear ? a.clear() : A.$e(a) ? A.$.clear(a) : A.object.clear(a)
};
A.zd.forEach = function(a, b, c) {
  if("function" == typeof a.forEach) {
    a.forEach(b, c)
  }else {
    if(A.$e(a) || A.Qc(a)) {
      A.$.forEach(a, b, c)
    }else {
      for(var d = A.zd.mg(a), f = A.zd.Ef(a), g = f.length, k = 0;k < g;k++) {
        b.call(c, f[k], d && d[k], a)
      }
    }
  }
};
A.zd.filter = function(a, b, c) {
  if("function" == typeof a.filter) {
    return a.filter(b, c)
  }
  if(A.$e(a) || A.Qc(a)) {
    return A.$.filter(a, b, c)
  }
  var d, f = A.zd.mg(a), g = A.zd.Ef(a), k = g.length;
  if(f) {
    d = {};
    for(var l = 0;l < k;l++) {
      b.call(c, g[l], f[l], a) && (d[f[l]] = g[l])
    }
  }else {
    d = [];
    for(l = 0;l < k;l++) {
      b.call(c, g[l], h, a) && d.push(g[l])
    }
  }
  return d
};
A.zd.map = function(a, b, c) {
  if("function" == typeof a.map) {
    return a.map(b, c)
  }
  if(A.$e(a) || A.Qc(a)) {
    return A.$.map(a, b, c)
  }
  var d, f = A.zd.mg(a), g = A.zd.Ef(a), k = g.length;
  if(f) {
    d = {};
    for(var l = 0;l < k;l++) {
      d[f[l]] = b.call(c, g[l], f[l], a)
    }
  }else {
    d = [];
    for(l = 0;l < k;l++) {
      d[l] = b.call(c, g[l], h, a)
    }
  }
  return d
};
A.zd.some = function(a, b, c) {
  if("function" == typeof a.some) {
    return a.some(b, c)
  }
  if(A.$e(a) || A.Qc(a)) {
    return A.$.some(a, b, c)
  }
  for(var d = A.zd.mg(a), f = A.zd.Ef(a), g = f.length, k = 0;k < g;k++) {
    if(b.call(c, f[k], d && d[k], a)) {
      return j
    }
  }
  return q
};
A.zd.every = function(a, b, c) {
  if("function" == typeof a.every) {
    return a.every(b, c)
  }
  if(A.$e(a) || A.Qc(a)) {
    return A.$.every(a, b, c)
  }
  for(var d = A.zd.mg(a), f = A.zd.Ef(a), g = f.length, k = 0;k < g;k++) {
    if(!b.call(c, f[k], d && d[k], a)) {
      return q
    }
  }
  return j
};
A.sb = {};
A.sb.Of = "StopIteration" in A.global ? A.global.StopIteration : Error("StopIteration");
function Ba() {
}
Ba.prototype.next = function() {
  e(A.sb.Of)
};
Ba.prototype.jB = function() {
  return this
};
A.sb.Sh = function(a) {
  if(a instanceof Ba) {
    return a
  }
  if("function" == typeof a.jB) {
    return a.jB(q)
  }
  if(A.$e(a)) {
    var b = 0, c = new Ba;
    c.next = function() {
      for(;;) {
        b >= a.length && e(A.sb.Of);
        if(b in a) {
          return a[b++]
        }
        b++
      }
    };
    return c
  }
  e(Error("Not implemented"))
};
A.sb.forEach = function(a, b, c) {
  if(A.$e(a)) {
    try {
      A.$.forEach(a, b, c)
    }catch(d) {
      d !== A.sb.Of && e(d)
    }
  }else {
    a = A.sb.Sh(a);
    try {
      for(;;) {
        b.call(c, a.next(), h, a)
      }
    }catch(f) {
      f !== A.sb.Of && e(f)
    }
  }
};
A.sb.filter = function(a, b, c) {
  var a = A.sb.Sh(a), d = new Ba;
  d.next = function() {
    for(;;) {
      var d = a.next();
      if(b.call(c, d, h, a)) {
        return d
      }
    }
  };
  return d
};
A.sb.PR = function(a, b, c) {
  var d = 0, f = a, g = c || 1;
  1 < arguments.length && (d = a, f = b);
  0 == g && e(Error("Range step argument must not be zero"));
  var k = new Ba;
  k.next = function() {
    (g > 0 && d >= f || g < 0 && d <= f) && e(A.sb.Of);
    var a = d;
    d = d + g;
    return a
  };
  return k
};
A.sb.join = function(a, b) {
  return A.sb.kG(a).join(b)
};
A.sb.map = function(a, b, c) {
  var a = A.sb.Sh(a), d = new Ba;
  d.next = function() {
    for(;;) {
      var d = a.next();
      return b.call(c, d, h, a)
    }
  };
  return d
};
A.sb.reduce = function(a, b, c, d) {
  var f = c;
  A.sb.forEach(a, function(a) {
    f = b.call(d, f, a)
  });
  return f
};
A.sb.some = function(a, b, c) {
  a = A.sb.Sh(a);
  try {
    for(;;) {
      if(b.call(c, a.next(), h, a)) {
        return j
      }
    }
  }catch(d) {
    d !== A.sb.Of && e(d)
  }
  return q
};
A.sb.every = function(a, b, c) {
  a = A.sb.Sh(a);
  try {
    for(;;) {
      if(!b.call(c, a.next(), h, a)) {
        return q
      }
    }
  }catch(d) {
    d !== A.sb.Of && e(d)
  }
  return j
};
A.sb.bQ = function(a) {
  var b = arguments, c = b.length, d = 0, f = new Ba;
  f.next = function() {
    try {
      return d >= c && e(A.sb.Of), A.sb.Sh(b[d]).next()
    }catch(a) {
      return(a !== A.sb.Of || d >= c) && e(a), d++, this.next()
    }
  };
  return f
};
A.sb.oQ = function(a, b, c) {
  var a = A.sb.Sh(a), d = new Ba, f = j;
  d.next = function() {
    for(;;) {
      var d = a.next();
      if(!f || !b.call(c, d, h, a)) {
        return f = q, d
      }
    }
  };
  return d
};
A.sb.gS = function(a, b, c) {
  var a = A.sb.Sh(a), d = new Ba, f = j;
  d.next = function() {
    for(;;) {
      if(f) {
        var d = a.next();
        if(b.call(c, d, h, a)) {
          return d
        }
        f = q
      }else {
        e(A.sb.Of)
      }
    }
  };
  return d
};
A.sb.kG = function(a) {
  if(A.$e(a)) {
    return A.$.kG(a)
  }
  var a = A.sb.Sh(a), b = [];
  A.sb.forEach(a, function(a) {
    b.push(a)
  });
  return b
};
A.sb.DC = function(a, b) {
  var a = A.sb.Sh(a), b = A.sb.Sh(b), c, d;
  try {
    for(;;) {
      c = d = q;
      var f = a.next();
      c = j;
      var g = b.next();
      d = j;
      if(f != g) {
        break
      }
    }
  }catch(k) {
    k !== A.sb.Of && e(k);
    if(c && !d) {
      return q
    }
    if(!d) {
      try {
        b.next()
      }catch(l) {
        return l !== A.sb.Of && e(l), j
      }
    }
  }
  return q
};
A.sb.ER = function(a, b) {
  try {
    return A.sb.Sh(a).next()
  }catch(c) {
    return c != A.sb.Of && e(c), b
  }
};
A.sb.product = function(a) {
  if(A.$.some(arguments, function(a) {
    return!a.length
  }) || !arguments.length) {
    return new Ba
  }
  var b = new Ba, c = arguments, d = A.$.repeat(0, c.length);
  b.next = function() {
    if(d) {
      for(var a = A.$.map(d, function(a, b) {
        return c[b][a]
      }), b = d.length - 1;0 <= b;b--) {
        if(d[b] < c[b].length - 1) {
          d[b]++;
          break
        }
        if(0 == b) {
          d = p;
          break
        }
        d[b] = 0
      }
      return a
    }
    e(A.sb.Of)
  };
  return b
};
A.sb.lQ = function(a) {
  var b = A.sb.Sh(a), c = [], d = 0, a = new Ba, f = q;
  a.next = function() {
    var a = p;
    if(!f) {
      try {
        return a = b.next(), c.push(a), a
      }catch(k) {
        (k != A.sb.Of || A.$.ri(c)) && e(k), f = j
      }
    }
    a = c[d];
    d = (d + 1) % c.length;
    return a
  };
  return a
};
function Ca(a, b) {
  this.Rg = {};
  this.pb = [];
  var c = arguments.length;
  if(1 < c) {
    c % 2 && e(Error("Uneven number of arguments"));
    for(var d = 0;d < c;d += 2) {
      this.set(arguments[d], arguments[d + 1])
    }
  }else {
    if(a) {
      a instanceof Ca ? (c = a.mg(), d = a.Ef()) : (c = A.object.mg(a), d = A.object.Ef(a));
      for(var f = 0;f < c.length;f++) {
        this.set(c[f], d[f])
      }
    }
  }
}
z = Ca.prototype;
z.zc = 0;
z.jw = 0;
z.Sm = u("zc");
z.Ef = function() {
  Da(this);
  for(var a = [], b = 0;b < this.pb.length;b++) {
    a.push(this.Rg[this.pb[b]])
  }
  return a
};
z.mg = function() {
  Da(this);
  return this.pb.concat()
};
z.Yj = function(a) {
  return Ea(this.Rg, a)
};
z.yq = function(a) {
  for(var b = 0;b < this.pb.length;b++) {
    var c = this.pb[b];
    if(Ea(this.Rg, c) && this.Rg[c] == a) {
      return j
    }
  }
  return q
};
z.DC = function(a, b) {
  if(this === a) {
    return j
  }
  if(this.zc != a.Sm()) {
    return q
  }
  var c = b || Fa;
  Da(this);
  for(var d, f = 0;d = this.pb[f];f++) {
    if(!c(this.get(d), a.get(d))) {
      return q
    }
  }
  return j
};
function Fa(a, b) {
  return a === b
}
z.ri = function() {
  return 0 == this.zc
};
z.clear = function() {
  this.Rg = {};
  this.jw = this.zc = this.pb.length = 0
};
z.remove = function(a) {
  return Ea(this.Rg, a) ? (delete this.Rg[a], this.zc--, this.jw++, this.pb.length > 2 * this.zc && Da(this), j) : q
};
function Da(a) {
  if(a.zc != a.pb.length) {
    for(var b = 0, c = 0;b < a.pb.length;) {
      var d = a.pb[b];
      Ea(a.Rg, d) && (a.pb[c++] = d);
      b++
    }
    a.pb.length = c
  }
  if(a.zc != a.pb.length) {
    for(var f = {}, c = b = 0;b < a.pb.length;) {
      d = a.pb[b], Ea(f, d) || (a.pb[c++] = d, f[d] = 1), b++
    }
    a.pb.length = c
  }
}
z.get = function(a, b) {
  return Ea(this.Rg, a) ? this.Rg[a] : b
};
z.set = function(a, b) {
  Ea(this.Rg, a) || (this.zc++, this.pb.push(a), this.jw++);
  this.Rg[a] = b
};
z.Ca = function() {
  return new Ca(this)
};
z.cO = function() {
  for(var a = new Ca, b = 0;b < this.pb.length;b++) {
    var c = this.pb[b];
    a.set(this.Rg[c], c)
  }
  return a
};
z.jB = function(a) {
  Da(this);
  var b = 0, c = this.pb, d = this.Rg, f = this.jw, g = this, k = new Ba;
  k.next = function() {
    for(;;) {
      f != g.jw && e(Error("The map has changed since the iterator was created"));
      b >= c.length && e(A.sb.Of);
      var k = c[b++];
      return a ? k : d[k]
    }
  };
  return k
};
function Ea(a, b) {
  return Object.prototype.hasOwnProperty.call(a, b)
}
;A.uri = {};
A.uri.R = {};
A.uri.R.UG = function(a, b, c, d, f, g, k) {
  var l = [];
  a && l.push(a, ":");
  c && (l.push("//"), b && l.push(b, "@"), l.push(c), d && l.push(":", d));
  f && l.push(f);
  g && l.push("?", g);
  k && l.push("#", k);
  return l.join("")
};
A.uri.R.XN = RegExp("^(?:([^:/?#.]+):)?(?://(?:([^/?#]*)@)?([\\w\\d\\-\\u0100-\\uffff.%]*)(?::([0-9]+))?)?([^?#]+)?(?:\\?([^#]*))?(?:#(.*))?$");
A.uri.R.split = function(a) {
  return a.match(A.uri.R.XN)
};
A.uri.R.tx = function(a) {
  return a && decodeURIComponent(a)
};
A.uri.R.Uq = function(a, b) {
  return A.uri.R.split(b)[a] || p
};
A.uri.R.nD = function(a) {
  return A.uri.R.Uq(1, a)
};
A.uri.R.vM = function(a) {
  a = A.uri.R.nD(a);
  !a && self.location && (a = self.location.protocol, a = a.substr(0, a.length - 1));
  return a ? a.toLowerCase() : ""
};
A.uri.R.IM = function(a) {
  return A.uri.R.Uq(2, a)
};
A.uri.R.II = function(a) {
  return A.uri.R.tx(A.uri.R.IM(a))
};
A.uri.R.uM = function(a) {
  return A.uri.R.Uq(3, a)
};
A.uri.R.qI = function(a) {
  return A.uri.R.tx(A.uri.R.uM(a))
};
A.uri.R.jD = function(a) {
  return Number(A.uri.R.Uq(4, a)) || p
};
A.uri.R.DM = function(a) {
  return A.uri.R.Uq(5, a)
};
A.uri.R.cu = function(a) {
  return A.uri.R.tx(A.uri.R.DM(a))
};
A.uri.R.zI = function(a) {
  return A.uri.R.Uq(6, a)
};
A.uri.R.xM = function(a) {
  var b = a.indexOf("#");
  return 0 > b ? p : a.substr(b + 1)
};
A.uri.R.YR = function(a, b) {
  return A.uri.R.JN(a) + (b ? "#" + b : "")
};
A.uri.R.sI = function(a) {
  return A.uri.R.tx(A.uri.R.xM(a))
};
A.uri.R.EQ = function(a) {
  a = A.uri.R.split(a);
  return A.uri.R.UG(a[1], a[2], a[3], a[4])
};
A.uri.R.MQ = function(a) {
  a = A.uri.R.split(a);
  return A.uri.R.UG(p, p, p, p, a[5], a[6], a[7])
};
A.uri.R.JN = function(a) {
  var b = a.indexOf("#");
  return 0 > b ? a : a.substr(0, b)
};
A.uri.R.VQ = function(a, b) {
  var c = A.uri.R.split(a), d = A.uri.R.split(b);
  return c[3] == d[3] && c[1] == d[1] && c[4] == d[4]
};
A.uri.R.wL = function(a) {
  A.eB && (0 <= a.indexOf("#") || 0 <= a.indexOf("?")) && e(Error("goog.uri.utils: Fragment or query identifiers are not supported: [" + a + "]"))
};
A.uri.R.mB = function(a) {
  if(a[1]) {
    var b = a[0], c = b.indexOf("#");
    0 <= c && (a.push(b.substr(c)), a[0] = b = b.substr(0, c));
    c = b.indexOf("?");
    0 > c ? a[1] = "?" : c == b.length - 1 && (a[1] = h)
  }
  return a.join("")
};
A.uri.R.PG = function(a, b, c) {
  if(A.isArray(b)) {
    for(var d = 0;d < b.length;d++) {
      c.push("&", a), "" !== b[d] && c.push("=", A.U.rs(b[d]))
    }
  }else {
    b != p && (c.push("&", a), "" !== b && c.push("=", A.U.rs(b)))
  }
};
A.uri.R.DB = function(a, b, c) {
  for(c = c || 0;c < b.length;c += 2) {
    A.uri.R.PG(b[c], b[c + 1], a)
  }
  return a
};
A.uri.R.WP = function(a, b) {
  var c = A.uri.R.DB([], a, b);
  c[0] = "";
  return c.join("")
};
A.uri.R.VG = function(a, b) {
  for(var c in b) {
    A.uri.R.PG(c, b[c], a)
  }
  return a
};
A.uri.R.XP = function(a) {
  a = A.uri.R.VG([], a);
  a[0] = "";
  return a.join("")
};
A.uri.R.FP = function(a, b) {
  return A.uri.R.mB(2 == arguments.length ? A.uri.R.DB([a], arguments[1], 0) : A.uri.R.DB([a], arguments, 1))
};
A.uri.R.GP = function(a, b) {
  return A.uri.R.mB(A.uri.R.VG([a], b))
};
A.uri.R.vL = function(a, b) {
  return A.uri.R.mB([a, "&", "zx", "=", A.U.rs(b)])
};
A.uri.R.Jx = function(a, b, c, d) {
  for(var f = c.length;0 <= (b = a.indexOf(c, b)) && b < d;) {
    var g = a.charCodeAt(b - 1);
    if(38 == g || 63 == g) {
      if(g = a.charCodeAt(b + f), !g || 61 == g || 38 == g || 35 == g) {
        return b
      }
    }
    b += f + 1
  }
  return-1
};
A.uri.R.my = /#|$/;
A.uri.R.TQ = function(a, b) {
  return 0 <= A.uri.R.Jx(a, 0, b, a.search(A.uri.R.my))
};
A.uri.R.KQ = function(a, b) {
  var c = a.search(A.uri.R.my), d = A.uri.R.Jx(a, 0, b, c);
  if(0 > d) {
    return p
  }
  var f = a.indexOf("&", d);
  if(0 > f || f > c) {
    f = c
  }
  d += b.length + 1;
  return A.U.PA(a.substr(d, f - d))
};
A.uri.R.LQ = function(a, b) {
  for(var c = a.search(A.uri.R.my), d = 0, f, g = [];0 <= (f = A.uri.R.Jx(a, d, b, c));) {
    d = a.indexOf("&", f);
    if(0 > d || d > c) {
      d = c
    }
    f += b.length + 1;
    g.push(A.U.PA(a.substr(f, d - f)))
  }
  return g
};
A.uri.R.bO = /[?&]($|#)/;
A.uri.R.KN = function(a) {
  for(var b = a.search(A.uri.R.my), c = 0, d, f = [];0 <= (d = A.uri.R.Jx(a, c, "zx", b));) {
    f.push(a.substring(c, d)), c = Math.min(a.indexOf("&", d) + 1 || b, b)
  }
  f.push(a.substr(c));
  return f.join("").replace(A.uri.R.bO, "$1")
};
A.uri.R.TN = function(a) {
  var b = A.U.AI();
  return A.uri.R.vL(A.uri.R.KN(a), b)
};
A.uri.R.HP = function(a, b) {
  A.uri.R.wL(a);
  A.U.fM(a) && (a = a.substr(0, a.length - 1));
  A.U.YN(b) && (b = b.substr(1));
  return A.U.AL(a, "/", b)
};
A.uri.R.oN = function(a) {
  return A.uri.R.TN(a)
};
function Ga(a) {
  pa.call(this);
  this.headers = new Ca;
  this.$A = a || p
}
A.e(Ga, pa);
Ga.GG = "Content-Type";
Ga.oL = /^https?$/i;
Ga.lL = "application/x-www-form-urlencoded;charset=utf-8";
Ga.uF = [];
Ga.send = function(a, b, c, d, f, g) {
  var k = new Ga;
  Ga.uF.push(k);
  b && A.I.nb(k, "complete", b);
  A.I.nb(k, "ready", A.eK(Ga.CL, k));
  g && (k.jG = Math.max(0, g));
  k.send(a, c, d, f)
};
Ga.cQ = function() {
  for(var a = Ga.uF;a.length;) {
    a.pop().aj()
  }
};
Ga.NR = function(a) {
  Ga.prototype.bv = a.jK(Ga.prototype.bv)
};
Ga.CL = function(a) {
  a.aj();
  A.$.remove(Ga.uF, a)
};
z = Ga.prototype;
z.hm = q;
z.Bc = p;
z.ZA = p;
z.AJ = "";
z.EC = q;
z.ry = q;
z.JD = q;
z.Oo = q;
z.jG = 0;
z.Up = p;
z.tK = "";
z.vG = q;
z.send = function(a, b, c, d) {
  this.Bc && e(Error("[goog.net.XhrIo] Object is active with another request"));
  b = b ? b.toUpperCase() : "GET";
  this.AJ = a;
  this.EC = q;
  this.hm = j;
  this.Bc = this.$A ? this.$A.XB() : A.lj.Kj();
  this.ZA = this.$A ? this.$A.bu() : A.lj.Kj.bu();
  this.Bc.onreadystatechange = A.bind(this.dK, this);
  try {
    this.JD = j, this.Bc.open(b, a, j), this.JD = q
  }catch(f) {
    Ha(this);
    return
  }
  var a = c || "", g = this.headers.Ca();
  d && A.zd.forEach(d, function(a, b) {
    g.set(b, a)
  });
  "POST" == b && !g.Yj(Ga.GG) && g.set(Ga.GG, Ga.lL);
  A.zd.forEach(g, function(a, b) {
    this.Bc.setRequestHeader(b, a)
  }, this);
  this.tK && (this.Bc.responseType = this.tK);
  A.object.Yj(this.Bc, "withCredentials") && (this.Bc.withCredentials = this.vG);
  try {
    this.Up && (qa.clearTimeout(this.Up), this.Up = p), 0 < this.jG && (this.Up = qa.setTimeout(A.bind(this.$N, this), this.jG)), this.ry = j, this.Bc.send(a), this.ry = q
  }catch(k) {
    Ha(this)
  }
};
z.$N = function() {
  "undefined" != typeof A && this.Bc && (this.dispatchEvent("timeout"), this.abort(8))
};
function Ha(a) {
  a.hm = q;
  a.Bc && (a.Oo = j, a.Bc.abort(), a.Oo = q);
  Ia(a);
  Ja(a)
}
function Ia(a) {
  a.EC || (a.EC = j, a.dispatchEvent("complete"), a.dispatchEvent("error"))
}
z.abort = function() {
  this.Bc && this.hm && (this.hm = q, this.Oo = j, this.Bc.abort(), this.Oo = q, this.dispatchEvent("complete"), this.dispatchEvent("abort"), Ja(this))
};
z.Dm = function() {
  this.Bc && (this.hm && (this.hm = q, this.Oo = j, this.Bc.abort(), this.Oo = q), Ja(this, j));
  Ga.f.Dm.call(this)
};
z.dK = function() {
  !this.JD && !this.ry && !this.Oo ? this.bv() : Ka(this)
};
z.bv = function() {
  Ka(this)
};
function Ka(a) {
  if(a.hm && "undefined" != typeof A && (!a.ZA[1] || !(4 == (a.Bc ? a.Bc.readyState : 0) && 2 == La(a)))) {
    if(a.ry && 4 == (a.Bc ? a.Bc.readyState : 0)) {
      qa.setTimeout(A.bind(a.dK, a), 0)
    }else {
      if(a.dispatchEvent("readystatechange"), 4 == (a.Bc ? a.Bc.readyState : 0)) {
        a.hm = q;
        var b = La(a), c;
        a: {
          switch(b) {
            case 200:
            ;
            case 201:
            ;
            case 202:
            ;
            case 204:
            ;
            case 304:
            ;
            case 1223:
              c = j;
              break a;
            default:
              c = q
          }
        }
        if(!c) {
          if(b = 0 === b) {
            b = A.uri.R.vM("" + a.AJ), b = !Ga.oL.test(b)
          }
          c = b
        }
        c ? (a.dispatchEvent("complete"), a.dispatchEvent("success")) : Ia(a);
        Ja(a)
      }
    }
  }
}
function Ja(a, b) {
  if(a.Bc) {
    var c = a.Bc, d = a.ZA[0] ? A.WJ : p;
    a.Bc = p;
    a.ZA = p;
    a.Up && (qa.clearTimeout(a.Up), a.Up = p);
    b || a.dispatchEvent("ready");
    try {
      c.onreadystatechange = d
    }catch(f) {
    }
  }
}
function La(a) {
  try {
    return 2 < (a.Bc ? a.Bc.readyState : 0) ? a.Bc.status : -1
  }catch(b) {
    return-1
  }
}
A.debug.Xe.lF(function(a) {
  Ga.prototype.bv = a(Ga.prototype.bv)
});
function Ma(a) {
  a = typeof a;
  return"string" == a || "number" == a || "boolean" == a
}
function Na(a) {
  return"string" != typeof a && "undefined" != typeof a.length
}
function Oa(a, b) {
  var c = "<" + a;
  "undefined" != typeof b.functionName && (b["function"] = b.functionName, delete b.functionName);
  for(var d in b) {
    "format" != d && "text" != d && "custom_attribute_value" != d && "attr" != d && Ma(b[d]) && (c += " " + d + '="' + b[d] + '"')
  }
  c += ">";
  for(d in b) {
    if("arg" == d && Na(b[d])) {
      for(var f = b[d], g = 0;g < f.length;g++) {
        c += "<arg><![CDATA[" + f[g] + "]]\></arg>"
      }
    }else {
      if("custom_attribute_value" == d || "attr" == d) {
        c += "<![CDATA[" + b[d] + "]]\>"
      }else {
        if("format" == d || "text" == d) {
          c += "<" + d + "><![CDATA[" + b[d] + "]]\></" + d + ">"
        }else {
          if(Na(b[d])) {
            f = b[d];
            for(g = 0;g < f.length;g++) {
              c += Oa(d, f[g])
            }
          }else {
            Ma(b[d]) || (c += Oa(d, b[d]))
          }
        }
      }
    }
  }
  return c + ("</" + a + ">")
}
function Pa() {
}
z = Pa.prototype;
z.oa = p;
z.B = p;
z.Nm = p;
z.oz = p;
z.nz = p;
z.TJ = p;
z.ME = p;
z.Ix = p;
z.Ha = function(a) {
  this.oa = a;
  this.ME = this.TJ = this.nz = this.oz = this.ul = q;
  this.ai = this.oa.enableFirefoxPrintPreviewFix
};
z.ul = q;
z.ZJ = function() {
  this.ul = j;
  this.oz && this.Hi(this.oa.xmlFile);
  this.nz && this.Oh(this.oa.xmlData);
  this.TJ && this.Jn(this.oa.jsonFile);
  this.ME && this.xj(this.oa.jsonData)
};
z.$J = function() {
  this.ai && Qa(this)
};
z.Ic = x(p);
z.ku = function() {
  this.oa.visible = q;
  this.B && (this.B.style.visibility = "hidden", this.B.setAttribute("width", "1px"), this.B.setAttribute("height", "1px"))
};
z.show = function() {
  this.oa.visible = j;
  this.B && (this.B.style.visibility = "visible", this.B.setAttribute("width", this.ab + ""), this.B.setAttribute("height", this.ob + ""))
};
z.Fq = function(a, b) {
  a && "OBJECT" == a.nodeName && (B.platform.sr && B.platform.$o ? (a.style.display = "none", function() {
    4 == a.readyState ? (B.platform.xk && b != p && B.Ht(window[b]), B.Ht(a)) : setTimeout(arguments.callee, 10)
  }()) : a.parentNode.removeChild(a))
};
z.Ht = function(a) {
  for(var b in a) {
    "function" == typeof a[b] && (a[b] = p)
  }
  a.parentNode && a.parentNode.removeChild(a)
};
z.ko = function() {
  return B.platform.CD && this.oa.swfFile != p
};
z.write = function(a) {
  var b = this.oa.width + "", c = this.oa.height + "", d = this.oa.preloaderSWFFile ? this.oa.preloaderSWFFile : this.oa.swfFile;
  if(this.ai = this.ai && B.platform.eJ) {
    this.Ix = new Ra
  }
  if(B.platform.sr && B.platform.$o) {
    var f;
    f = '<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"' + (' id="' + this.oa.id + '"');
    f = f + (' width="' + b + '"') + (' height="' + c + '"') + (' style="visibility:' + (this.oa.visible ? "visible" : "hidden") + '"');
    f += ' codebase="' + B.platform.protocol + '//fpdownload.macromedia.com/get/flashplayer/current/swflash.cab">';
    f += Sa("movie", d);
    f += Sa("bgcolor", this.oa.bgColor);
    f += Sa("allowScriptAccess", "always");
    f += Sa("flashvars", Ta(this));
    this.oa.wMode != p && (f += Sa("wmode", this.oa.wMode));
    f += "</object>";
    if(B.platform.xk) {
      b = p;
      for(c = a;c;) {
        if(c.nodeName != p && "form" == c.nodeName.toLowerCase()) {
          b = c;
          break
        }
        c = c.parentNode
      }
      if(b != p) {
        window[this.oa.id] = {};
        window[this.oa.id].qL = s();
        a.innerHTML = f;
        window[this.oa.id].qL = p;
        var a = {}, g;
        for(g in window[this.oa.id]) {
          "function" == typeof window[this.oa.id][g] && (a[g] = window[this.oa.id][g])
        }
        this.B = window[this.oa.id] = b[this.oa.id];
        this.oa.flashObject = this.B;
        for(g in a) {
          Ua(g)
        }
      }else {
        a.innerHTML = f, this.B = document.getElementById(this.oa.id), this.oa.flashObject = this.B
      }
    }else {
      a.innerHTML = f, this.B = document.getElementById(this.oa.id), this.oa.flashObject = this.B
    }
  }else {
    g = document.createElement("object");
    g.setAttribute("type", "application/x-shockwave-flash");
    g.setAttribute("id", this.oa.id);
    g.setAttribute("width", b);
    g.setAttribute("height", c);
    g.setAttribute("data", d);
    g.setAttribute("style", "visibility: " + (this.oa.visible ? "visible" : "hidden"));
    Va(g, "movie", d);
    Va(g, "bgcolor", this.oa.bgColor);
    Va(g, "allowScriptAccess", "always");
    Va(g, "flashvars", Ta(this));
    this.oa.wMode != p && Va(g, "wmode", this.oa.wMode);
    if(a.hasChildNodes()) {
      for(;0 < a.childNodes.length;) {
        a.removeChild(a.firstChild)
      }
    }
    this.B = a.appendChild(g);
    this.oa.flashObject = this.B;
    this.Nm = a
  }
  return this.B != p
};
function Sa(a, b) {
  return'<param name="' + a + '" value="' + b + '" />'
}
function Va(a, b, c) {
  var d = document.createElement("param");
  d.setAttribute("name", b);
  d.setAttribute("value", c);
  a.appendChild(d)
}
function Ua(a) {
  eval('obj[functionName] = function(){return eval(this.CallFunction("<invoke name=\\"' + a + '\\" returntype=\\"javascript\\">" + __flash__argumentsToXML(arguments,0) + "</invoke>"));}')
}
function Ta(a) {
  var b = "__externalobjid=" + a.oa.id;
  a.oa.preloaderSWFFile && (b += "&swffile=" + a.oa.swfFile);
  a.oa.xmlFile && (b += "&xmlfile=" + a.oa.xmlFile);
  a.oa.enabledChartEvents && (b += "&__enableevents=1");
  a.oa.enabledChartMouseEvents && (b += "&__enablechartmouseevents=1");
  b += "&preloaderInit=" + a.oa.messages.preloaderInit;
  b += "&preloaderLoading=" + a.oa.messages.preloaderLoading;
  b += "&init=" + a.oa.messages.init;
  b += "&loadingConfig=" + a.oa.messages.loadingConfig;
  b += "&loadingTemplates=" + a.oa.messages.loadingTemplates;
  b += "&loadingResources=" + a.oa.messages.loadingResources;
  b += "&noData=" + a.oa.messages.noData;
  return b += "&waitingForData=" + a.oa.messages.waitingForData
}
z.Zs = x(j);
z.OA = function() {
  (this.ai || this.Nm) && this.Ix.aj();
  Qa(this)
};
z.Mp = function(a, b) {
  this.B && (this.B.setAttribute("width", a + ""), this.B.setAttribute("height", b + ""))
};
z.remove = function() {
  this.B && (this.B.Dispose && this.B.Dispose(), Wa(this.B, this.Zm), this.ai && (this.ai || this.Nm) && this.Ix.aj());
  this.B = p;
  this.oa.flashObject = this.B;
  if(B && B.QM()) {
    for(var a = B.wI(), b = 0;b < a;b++) {
      if(B.nI()[b] == this) {
        B.nI().splice(b, 1);
        break
      }
    }
  }
  B && B.oI() && this.Zm != p && (B.oI()[this.Zm] = p)
};
z.clear = function() {
  this.B && this.B.Clear && this.B.Clear()
};
z.refresh = function() {
  this.B != p && this.B.Refresh != p && this.B.Refresh()
};
z.os = function(a, b) {
  this.B != p && this.B.UpdateData != p && this.B.UpdateData(a, b)
};
z.cA = function(a) {
  this.B && this.B.SetLoading && this.B.SetLoading(a)
};
z.Ox = function() {
  return this.B.GetInformation()
};
z.Hi = function(a) {
  this.B && this.ul ? this.B.SetXMLDataFromURL(a) : this.oz = j
};
z.Oh = function(a) {
  this.B && this.ul ? this.B.SetXMLDataFromString(a) : this.nz = j
};
z.Jn = s();
z.xj = function(a) {
  this.B && this.ul ? this.Oh(Oa("anychart", a)) : this.ME = j
};
z.Gv = function(a) {
  this.B && this.ul ? this.B.SetXMLDataFromString(a) : this.nz = j
};
z.kA = function(a) {
  this.B && this.ul ? this.B.SetXMLDataFromURL(a) : this.oz = j
};
z.Np = function(a, b) {
  this.B && this.ul && this.B.UpdateViewFromURL(a, b)
};
z.gA = function(a, b) {
  this.B && this.ul && this.B.UpdateViewFromString(a, b)
};
z.Od = function(a, b) {
  this.B != p && this.B.AddPoint != p && this.B.AddPoint(a, b)
};
z.Lj = function(a, b, c) {
  this.B != p && this.B.AddPointAt != p && this.B.AddPointAt(a, b, c)
};
z.Ck = function(a, b) {
  this.B != p && this.B.RemovePoint != p && this.B.RemovePoint(a, b)
};
z.Ok = function(a, b, c) {
  this.B != p && this.B.UpdatePoint != p && this.B.UpdatePoint(a, b, c)
};
z.Pk = function(a, b, c) {
  this.B != p && this.B.UpdatePointData != p && this.B.UpdatePointData(a, b, c)
};
z.nk = function(a, b, c) {
  this.B != p && this.B.HighlightPoint != p && this.B.HighlightPoint(a, b, c)
};
z.uj = function(a, b, c) {
  this.B != p && this.B.SelectPoint != p && this.B.SelectPoint(a, b, c)
};
z.NA = function(a, b, c, d) {
  this.B && this.B.updatePointWithAnimation != p && this.B.updatePointWithAnimation(a, b, c, d)
};
z.vA = function() {
  this.B && this.B.startPointsUpdate != p && this.B.startPointsUpdate()
};
z.Mj = function(a) {
  this.B != p && this.B.AddSeries != p && this.B.AddSeries(a)
};
z.Nj = function(a, b) {
  this.B != p && this.B.AddSeriesAt != p && this.B.AddSeriesAt(a, b)
};
z.Dk = function(a) {
  this.B != p && this.B.RemoveSeries != p && this.B.RemoveSeries(a)
};
z.Qk = function(a, b) {
  this.B != p && this.B.UpdateSeries != p && this.B.UpdateSeries(a, b)
};
z.Jk = function(a, b) {
  this.B != p && this.B.ShowSeries != p && this.B.ShowSeries(a, b)
};
z.ok = function(a, b) {
  this.B != p && this.B.HighlightSeries != p && this.B.HighlightSeries(a, b)
};
z.mk = function(a, b) {
  this.B != p && this.B.HighlightCategory != p && this.B.HighlightCategory(a, b)
};
z.rA = function(a, b) {
  this.B != p && this.B.ShowAxisMarker != p && this.B.ShowAxisMarker(a, b)
};
z.Ik = function(a, b) {
  this.B != p && this.B.SetPlotCustomAttribute != p && this.B.SetPlotCustomAttribute(a, b)
};
z.Gs = function(a, b, c) {
  this.B != p && this.B.View_SetPlotCustomAttribute != p && this.B.View_SetPlotCustomAttribute(a, b, c)
};
z.xs = function(a, b) {
  this.B != p && this.B.View_AddSeries != p && this.B.View_AddSeries(a, b)
};
z.Es = function(a, b) {
  this.B != p && this.B.View_RemoveSeries != p && this.B.View_RemoveSeries(a, b)
};
z.ys = function(a, b, c) {
  this.B != p && this.B.View_AddSeriesAt != p && this.B.View_AddSeriesAt(a, b, c)
};
z.Js = function(a, b, c) {
  this.B != p && this.B.View_UpdateSeries != p && this.B.View_UpdateSeries(a, b, c)
};
z.Hs = function(a, b, c) {
  this.B != p && this.B.View_ShowSeries != p && this.B.View_ShowSeries(a, b, c)
};
z.vs = function(a, b, c) {
  this.B != p && this.B.View_AddPoint != p && this.B.View_AddPoint(a, b, c)
};
z.ws = function(a, b, c, d) {
  this.B != p && this.B.View_AddPointAt != p && this.B.View_AddPointAt(a, b, c, d)
};
z.Ds = function(a, b, c) {
  this.B != p && this.B.View_RemovePoint != p && this.B.View_RemovePoint(a, b, c)
};
z.Is = function(a, b, c, d) {
  this.B != p && this.B.View_UpdatePoint != p && this.B.View_UpdatePoint(a, b, c, d)
};
z.VA = function(a) {
  this.B != p && this.B.View_Clear != p && this.B.View_Clear(a)
};
z.Cs = function(a) {
  this.B != p && this.B.View_Refresh != p && this.B.View_Refresh(a)
};
z.Bs = function(a, b, c) {
  this.B != p && this.B.View_HighlightSeries != p && this.B.View_HighlightSeries(a, b, c)
};
z.As = function(a, b, c, d) {
  this.B != p && this.B.View_HighlightPoint != p && this.B.View_HighlightPoint(a, b, c, d)
};
z.zs = function(a, b, c) {
  this.B != p && this.B.View_HighlightCategory != p && this.B.View_HighlightCategory(a, b, c)
};
z.Fs = function(a, b, c, d) {
  this.B != p && this.B.View_SelectPoint != p && this.B.View_SelectPoint(a, b, c, d)
};
z.ps = function(a, b, c, d) {
  this.B != p && this.B.UpdateViewPointData != p && this.B.UpdateViewPointData(b, c, d)
};
z.Zq = function(a, b) {
  return this.B.GetPngScreen(a, b)
};
z.ay = function(a, b) {
  return this.B.GetPdfScreen(a, b)
};
z.Hz = function() {
  this.B.PrintChart()
};
z.Uz = function(a, b) {
  this.B.SaveAsImage(a, b)
};
z.Vz = function(a, b) {
  this.B.SaveAsPDF(a, b)
};
z.Wz = function(a) {
  this.B != p && this.B.ScrollXTo != p && this.B.ScrollXTo(a)
};
z.Xz = function(a) {
  this.B != p && this.B.ScrollYTo != p && this.B.ScrollYTo(a)
};
z.scrollTo = function(a, b) {
  this.B != p && this.B.ScrollTo != p && this.B.ScrollTo(a, b)
};
z.SA = function(a, b) {
  this.B != p && this.B.ViewScrollXTo != p && this.B.ViewScrollXTo(a, b)
};
z.TA = function(a, b) {
  this.B != p && this.B.ViewScrollYTo != p && this.B.ViewScrollYTo(a, b)
};
z.hy = function() {
  return this.B != p && this.B.GetXScrollInfo != p ? this.B.GetXScrollInfo() : p
};
z.iy = function() {
  return this.B != p && this.B.GetYScrollInfo != p ? this.B.GetYScrollInfo() : p
};
z.fy = function(a) {
  if(this.B != p && this.B.GetViewXScrollInfo != p) {
    return this.B.GetViewXScrollInfo(a)
  }
};
z.gy = function(a) {
  if(this.B != p && this.B.GetViewYScrollInfo != p) {
    return this.B.GetViewYScrollInfo(a)
  }
};
z.lA = function(a) {
  this.B != p && this.B.SetXZoom != p && this.B.SetXZoom(a)
};
z.mA = function(a) {
  this.B != p && this.B.SetYZoom != p && this.B.SetYZoom(a)
};
z.nA = function(a, b) {
  this.B != p && this.B.SetZoom != p && this.B.SetZoom(a, b)
};
z.hA = function(a, b) {
  this.B != p && this.B.SetViewXZoom != p && this.B.SetViewXZoom(a, b)
};
z.iA = function(a, b) {
  this.B != p && this.B.SetViewYZoom != p && this.B.SetViewYZoom(a, b)
};
z.jA = function(a, b, c) {
  this.B != p && this.B.SetViewZoom != p && this.B.SetViewZoom(a, b, c)
};
z.Aw = function() {
  this.B != p && this.B.RepeatAnimation != p && this.B.RepeatAnimation()
};
function Wa(a, b) {
  a && "OBJECT" == a.nodeName && (B.platform.sr && B.platform.$o ? (a.style.display = "none", function() {
    4 == a.readyState ? (B.platform.xk && b && this.Ht(window[b]), B.Ht(a)) : setTimeout(arguments.callee, 10)
  }()) : a.parentNode.removeChild(a))
}
z.Ht = function(a) {
  for(var b in a) {
    "function" == typeof a[b] && (a[b] = p)
  }
  a.parentNode && a.parentNode.removeChild(a)
};
function Qa(a) {
  if(a.ai && a.Nm) {
    var b = a.Zq();
    if(b && 0 != b.length) {
      var c = a.Nm.getAttribute("id");
      c || (c = "__chart_generated_container__" + a.Zm, a.Nm.setAttribute("id", c));
      var d = a.Ix, f = a.Nm, g = a.ab + "", k = a.ob + "", a = a.Nm.nodeName, l = document.getElementsByTagName("head");
      if(l = 0 < l.length ? l[0] : p) {
        var n;
        n = l;
        var m = document.createElement("style");
        m.setAttribute("type", "text/css");
        m.setAttribute("media", "screen");
        var o = a + "#" + c, v = o + " img";
        m.appendChild(document.createTextNode(o + (" object { width:" + g + "; height:" + k + ";padding:0; margin:0; }\n")));
        m.appendChild(document.createTextNode(v + " { display: none; }"));
        n = n.appendChild(m);
        d.Nt = n;
        n = document.createElement("style");
        n.setAttribute("type", "text/css");
        n.setAttribute("media", "print");
        a = a + "#" + c;
        c = a + " img";
        g = " { display: block; width: " + g + "; height: " + k + "; }";
        n.appendChild(document.createTextNode(a + " object { display: none; }\n"));
        n.appendChild(document.createTextNode(c + g));
        g = l.appendChild(n);
        d.Ot = g;
        g = document.createElement("img");
        g = f.appendChild(g);
        g.src = "data:image/png;base64," + b;
        d.Mt = g
      }
    }
  }
}
function Ra() {
}
z.Nt = p;
z.Ot = p;
z.Mt = p;
Ra.prototype.aj = function() {
  this.Mt && this.Mt.parentNode && this.Mt.parentNode.removeChild(this.Mt);
  this.Nt && (this.Nt.parentNode.removeChild(this.Nt), this.Nt = p);
  this.Ot && (this.Ot.parentNode.removeChild(this.Ot), this.Ot = p)
};
A.Gg("anychart.scope.goog", A);
A.Gg("anychart.scope.Event", ha);
A.Gg("anychart.scope.EventTarget", pa);
A.Gg("anychart.scope.EventType", ka);
A.Gg("anychart.scope.XhrIo", Ga);
var Xa = {};
A.Gg("anychart.WMode", Xa);
A.W(Xa, "WINDOW", "window");
A.W(Xa, "TRANSPARENT", "transparent");
A.W(Xa, "OPAQUE", "opaque");
var Ya = {};
A.Gg("anychart.RenderingType", Ya);
A.W(Ya, "FLASH_ONLY", "flash");
A.W(Ya, "SVG_ONLY", "svg");
A.W(Ya, "FLASH_PREFERRED", "flashPreferred");
A.W(Ya, "SVG_PREFERRED", "svgPreferred");
function B(a, b, c) {
  c && (this.id = c);
  this.swfFile = B.swfFile;
  this.preloaderSWFFile = B.preloaderSWFFile;
  this.width = B.width;
  this.height = B.height;
  this.enableFirefoxPrintPreviewFix = B.enableFirefoxPrintPreviewFix;
  this.enabledChartEvents = B.enabledChartEvents;
  this.enabledChartMouseEvents = B.enabledChartMouseEvents;
  this.renderingType = B.renderingType;
  this.watermark = B.watermark;
  this.messages.preloaderInit = B.messages.preloaderInit;
  this.messages.preloaderLoading = B.messages.preloaderLoading;
  this.messages.init = B.messages.init;
  this.messages.loadingConfig = B.messages.loadingConfig;
  this.messages.loadingResources = B.messages.loadingResources;
  this.messages.loadingTemplates = B.messages.loadingTemplates;
  this.messages.noData = B.messages.noData;
  this.messages.waitingForData = B.messages.waitingForData;
  a && (this.swfFile = a);
  b && (this.preloaderSWFFile = b);
  this.fe = {};
  B.IN(this)
}
A.Gg("AnyChart", B);
B.VERSION = "6.0.10 (build #36916)";
A.W(B, "VERSION", B.VERSION);
B.width = 550;
A.W(B, "width", B.width);
B.height = 400;
A.W(B, "height", B.height);
B.nF = "flash";
A.W(B, "renderingType", B.nF);
B.yC = j;
A.W(B, "enabledChartEvents", B.yC);
B.zC = q;
A.W(B, "enabledChartMouseEvents", B.zC);
B.hG = p;
A.W(B, "swfFile", B.hG);
B.hF = p;
A.W(B, "preloaderSWFFile", B.hF);
B.ai = j;
A.W(B, "enableFirefoxPrintPreviewFix", B.ai);
B.prototype.id = p;
A.W(B.prototype, "id", B.prototype.id);
B.gt = [];
B.QM = function() {
  return B.gt && 0 < B.wI()
};
B.wI = function() {
  return B.gt.length
};
B.nI = function() {
  return B.gt
};
B.tq = {};
B.oI = function() {
  return B.tq
};
B.prototype.nF = p;
A.W(B.prototype, "renderingType", B.prototype.nF);
B.prototype.va = p;
B.prototype.sk = q;
B.prototype.sO = p;
A.W(B.prototype, "xmlFile", B.prototype.sO);
B.prototype.jN = p;
A.W(B.prototype, "jsonFile", B.prototype.jN);
B.prototype.rO = p;
A.W(B.prototype, "xmlData", B.prototype.rO);
B.prototype.iN = p;
A.W(B.prototype, "jsonData", B.prototype.iN);
B.prototype.width = p;
A.W(B.prototype, "width", B.prototype.width);
B.prototype.height = p;
A.W(B.prototype, "height", B.prototype.height);
B.prototype.oO = p;
A.W(B.prototype, "watermark", B.prototype.oO);
B.prototype.hG = p;
A.W(B.prototype, "swfFile", B.prototype.hG);
B.prototype.hF = p;
A.W(B.prototype, "preloaderSWFFile", B.prototype.hF);
B.prototype.ai = j;
A.W(B.prototype, "enableFirefoxPrintPreviewFix", B.prototype.ai);
B.prototype.nO = p;
A.W(B.prototype, "wMode", B.prototype.nO);
B.prototype.yC = j;
A.W(B.prototype, "enabledChartEvents", B.prototype.yC);
B.prototype.zC = q;
A.W(B.prototype, "enabledChartMouseEvents", B.prototype.zC);
B.prototype.fe = p;
B.prototype.bgColor = "#FFFFFF";
A.W(B.prototype, "bgColor", B.prototype.bgColor);
B.prototype.Ks = j;
A.W(B.prototype, "visible", B.prototype.Ks);
B.prototype.show = function() {
  this.visible = j;
  this.va.show()
};
A.W(B.prototype, "show", B.prototype.show);
B.prototype.ku = function() {
  this.visible = q;
  this.va.ku()
};
A.W(B.prototype, "hide", B.prototype.ku);
B.R = {};
B.R.BD = function(a) {
  return"undefined" != typeof a
};
B.R.push = function(a, b) {
  a[a.length] = b
};
var Za = navigator && navigator.userAgent ? navigator.userAgent.toLowerCase() : p, $a = navigator && navigator.platform ? navigator.platform.toLowerCase() : p;
B.platform = {};
A.W(B, "platform", B.platform);
B.platform.$o = $a ? /win/.test($a) : /win/.test(Za);
A.W(B.platform, "isWin", B.platform.$o);
B.platform.eN = !B.platform.$o && ($a ? /mac/.test($a) : /mac/.test(Za));
A.W(B.platform, "isMac", B.platform.eN);
B.platform.LI = "undefined" != typeof document.getElementById && "undefined" != typeof document.getElementsByTagName && "undefiend" != typeof document.createElement;
A.W(B.platform, "hasDom", B.platform.LI);
B.platform.ZK = /webkit/.test(Za) ? parseFloat(Za.replace(/^.*webkit\/(\d+(\.\d+)?).*$/, "$1")) : q;
A.W(B.platform, "webKit", B.platform.ZK);
B.platform.sr = /msie/.test(Za);
A.W(B.platform, "isIE", B.platform.sr);
B.platform.eJ = /firefox/.test(Za);
A.W(B.platform, "isFirefox", B.platform.eJ);
B.platform.protocol = "https:" == location.protocol ? "https:" : "http:";
A.W(B.platform, "protocol", B.platform.protocol);
B.fJ = function() {
  B.platform.ii = [0, 0, 0];
  var a;
  if(navigator.plugins && "object" == typeof navigator.plugins["Shockwave Flash"]) {
    if((a = navigator.plugins["Shockwave Flash"].description) && (!navigator.mimeTypes || !navigator.mimeTypes[["application/x-shockwave-flash"]] || navigator.mimeTypes["application/x-shockwave-flash"].enabledPlugin)) {
      a = a.replace(/^.*\s+(\S+\s+\S+$)/, "$1"), B.platform.ii[0] = parseInt(a.replace(/^(.*)\..*$/, "$1"), 10), B.platform.ii[1] = parseInt(a.replace(/^.*\.(.*)\s.*$/, "$1"), 10), B.platform.ii[2] = /[a-zA-Z]/.test(a) ? parseInt(a.replace(/^.*[a-zA-Z]+(.*)$/, "$1"), 10) : 0
    }
  }else {
    if("undefined" != typeof window.ActiveXObject) {
      try {
        var b = new window.ActiveXObject("ShockwaveFlash.ShockwaveFlash");
        if(b && (a = b.GetVariable("$version"))) {
          a = a.split(" ")[1].split(","), B.platform.ii = [parseInt(a[0], 10), parseInt(a[1], 10), parseInt(a[2], 10)]
        }
      }catch(c) {
      }
    }
  }
  return B.platform.ii != p && 9 <= Number(B.platform.ii[0])
};
A.W(B, "isFlashPlayerValidVersion", B.fJ);
B.platform.CD = B.fJ();
A.W(B.platform, "hasRequiredVersion", B.platform.CD);
B.platform.ii = B.platform.ii;
A.W(B.platform, "flashPlayerVersion", B.platform.ii);
B.platform.xk = B.platform.CD && B.platform.sr && B.platform.$o;
A.W(B.platform, "needFormFix", B.platform.xk);
B.platform.xk && (B.platform.xk = 9 == Number(B.platform.ii[0]), B.platform.xk = B.platform.xk && 0 == Number(B.platform.ii[1]), B.platform.xk = B.platform.xk && 115 > Number(B.platform.ii[2]));
B.CC = [];
B.FN = function(a) {
  A.Ey(a) && B.CC.push(a)
};
A.W(B, "ready", B.FN);
B.lN = function(a) {
  if(a == p || a == h) {
    a = B.nM()
  }
  var a = a + "AnyChartHTML5.js", b = new Ga;
  b.vG = j;
  A.I.nb(b, "complete", B.cK, p, B);
  A.I.nb(b, ["error", "abort"], B.bK, p, B);
  b.send(a, "GET", p, "anychart.com")
};
A.W(B, "loadHTML5Engine", B.lN);
B.cK = function(a) {
  var a = a.target, b;
  try {
    b = a.Bc ? a.Bc.responseText : ""
  }catch(c) {
    b = ""
  }
  eval(b.toString());
  b = B.CC.length;
  for(var d = 0;d < b;d++) {
    B.CC[d]()
  }
  B.SL(a)
};
B.bK = function() {
  e(Error("Can not find AnyChart html5 engine in JS folder."))
};
B.SL = function(a) {
  A.I.ed(a, "complete", B.cK, p, B);
  A.I.ed(a, ["error", "abort"], B.bK, p, B)
};
B.nM = function() {
  for(var a = A.global.document.getElementsByTagName("script"), b = a.length - 1;b >= 0;--b) {
    var c = a[b].src, d = c.lastIndexOf("?"), d = d == -1 ? c.length : d, c = c.substr(0, d), f = c.split("/"), f = f[f.length - 1].toLowerCase(), g = f.length;
    if(f.indexOf("anychart") != -1 && f.substr(g - 3, 3) == ".js") {
      return c.substr(0, d - g)
    }
  }
  return p
};
B.R.attachEvent = function(a, b, c) {
  a.attachEvent(b, c);
  B.R.push(B.R.AR, [a, b, c])
};
B.R.sL = function(a, b) {
  if(B.R.BD(window.addEventListener)) {
    window.addEventListener(a, b, q)
  }else {
    if(B.R.BD(document.addEventListener)) {
      document.addEventListener(a, b, q)
    }else {
      if(B.R.BD(window.attachEvent)) {
        B.R.attachEvent(window, "on" + a, b)
      }else {
        if(typeof window["on" + a] == "function") {
          var c = window["on" + a];
          window["on" + a] = function() {
            c();
            b()
          }
        }else {
          window["on" + a] = b
        }
      }
    }
  }
};
A.Gg("AnyChart.utils.addGlobalEventListener", B.R.sL);
B.R.qr = q;
B.R.oC = [];
B.R.rL = function(a) {
  B.R.qr ? a() : B.R.oC.push(a)
};
B.R.Iq = function() {
  if(!B.R.qr) {
    try {
      var a = document.getElementsByTagName("body")[0].appendChild(document.createElement("span"));
      a.parentNode.removeChild(a)
    }catch(b) {
      return
    }
    B.R.qr = j;
    for(var a = B.R.oC.length, c = 0;c < a;c++) {
      B.R.oC[c]()
    }
  }
};
B.R.HN = function() {
  if(B.platform.LI) {
    typeof document.readyState != "undefined" && (document.readyState == "complete" || document.getElementsByTagName("body")[0] || document.body) && B.R.Iq();
    if(!B.R.qr) {
      document.addEventListener && document.addEventListener("DOMContentLoaded", B.R.Iq, q);
      if(B.platform.sr && B.platform.$o) {
        document.attachEvent("onreadystatechange", function() {
          if(document.readyState == "complete") {
            document.detachEvent("onreadystatechange", arguments.callee);
            B.R.Iq()
          }
        });
        window == top && function() {
          if(!B.R.qr) {
            try {
              document.documentElement.doScroll("left")
            }catch(a) {
              setTimeout(arguments.callee, 0);
              return
            }
            B.R.Iq()
          }
        }()
      }
      B.platform.ZK && function() {
        B.R.qr || (/loaded|complete/.test(document.readyState) ? B.R.Iq() : setTimeout(arguments.callee, 0))
      }();
      A.I.nb(window, "load", B.R.Iq)
    }
  }
};
B.R.HN();
B.dispatchEvent = function(a, b) {
  B.bH(a) && b != p && B.tq[a].dispatchEvent(b)
};
A.W(B, "dispatchEvent", B.dispatchEvent);
function ab(a) {
  switch(a.renderingType) {
    case "svg":
      return new window.anychart.render.svg.SVGRenderer;
    case "flash":
      return new Pa;
    default:
    ;
    case "flashPreferred":
      return B.platform.hasRequiredVersion ? new Pa : new window.anychart.render.svg.SVGRenderer;
    case "svgPreferred":
      return window.SVGAngle != h ? new window.anychart.render.svg.SVGRenderer : new Pa
  }
}
B.IN = function(a) {
  a.id = a.id == h ? "__AnyChart___" + B.gt.length : a.id;
  B.tq[a.id] = a;
  B.gt.push(a)
};
B.bH = function(a) {
  return a && B.tq && B.tq[a]
};
B.qM = function(a) {
  return B.bH(a) ? B.tq[a] : p
};
A.W(B, "getChartById", B.qM);
B.$L = function(a, b) {
  B.Fq(a, b)
};
A.W(B, "disposeFlashObject", B.$L);
B.Fq = function(a, b) {
  a.Fq(a, b)
};
A.W(B, "disposeVisual", B.Fq);
B.prototype.Fq = function(a, b) {
  this.va.Fq(a, b)
};
B.R.lf = {};
A.Gg("AnyChart.utils.JSConverter", B.R.lf);
B.R.lf.$D = function(a) {
  a = typeof a;
  return a == "string" || a == "number" || a == "boolean"
};
A.W(B.R.lf, "isAttribute", B.R.lf.$D);
B.R.lf.isArray = function(a) {
  return typeof a != "string" && typeof a.length != "undefined"
};
A.W(B.R.lf, "isArray", B.R.lf.isArray);
B.R.lf.FL = function(a) {
  return B.R.lf.YB("anychart", a)
};
A.W(B.R.lf, "convert", B.R.lf.FL);
B.R.lf.YB = function(a, b) {
  var c, d, f = "<" + a;
  if(typeof b.functionName != "undefined") {
    b["function"] = b.functionName;
    delete b.functionName
  }
  for(c in b) {
    c != "format" && c != "text" && c != "custom_attribute_value" && c != "attr" && B.R.lf.$D(b[c]) && (f = f + (" " + c + '="' + b[c] + '"'))
  }
  f = f + ">";
  for(c in b) {
    if(c == "arg" && B.R.lf.isArray(b[c])) {
      var g = b[c];
      for(d = 0;d < g.length;d++) {
        f = f + ("<arg><![CDATA[" + g[d] + "]]\></arg>")
      }
    }else {
      if(c == "custom_attribute_value" || c == "attr") {
        f = f + ("<![CDATA[" + b[c] + "]]\>")
      }else {
        if(c == "format" || c == "text") {
          f = f + ("<" + c + "><![CDATA[" + b[c] + "]]\></" + c + ">")
        }else {
          if(B.R.lf.isArray(b[c])) {
            g = b[c];
            for(d = 0;d < g.length;d++) {
              f = f + B.R.lf.YB(c, g[d])
            }
          }else {
            B.R.lf.$D(b[c]) || (f = f + B.R.lf.YB(c, b[c]))
          }
        }
      }
    }
  }
  return f + ("</" + a + ">")
};
B.messages = {};
B.messages.preloaderInit = " ";
B.messages.preloaderLoading = "Loading... ";
B.messages.init = "Initializing...";
B.messages.loadingConfig = "Loading config...";
B.messages.loadingResources = "Loading resources...";
B.messages.loadingTemplates = "Loading templates...";
B.messages.noData = "No Data";
B.messages.waitingForData = "Waiting for data...";
B.watermark = p;
B.prototype.qN = {};
A.W(B.prototype, "messages", B.prototype.qN);
B.prototype.messages.preloaderInit = p;
B.prototype.messages.preloaderLoading = p;
B.prototype.messages.init = p;
B.prototype.messages.loadingConfig = p;
B.prototype.messages.loadingResources = p;
B.prototype.messages.loadingTemplates = p;
B.prototype.messages.noData = p;
B.prototype.messages.waitingForData = p;
B.prototype.addEventListener = function(a, b) {
  this.fe[a] == p && (this.fe[a] = []);
  this.fe[a].push(b)
};
A.W(B.prototype, "addEventListener", B.prototype.addEventListener);
B.prototype.removeEventListener = function(a, b) {
  if(!(this.fe == p || this.fe[a] == p)) {
    for(var c = -1, d = 0;d < this.fe[a].length;d++) {
      if(this.fe[a][d] == b) {
        c = d;
        break
      }
    }
    c != -1 && this.fe[a].splice(c, 1)
  }
};
A.W(B.prototype, "removeEventListener", B.prototype.removeEventListener);
B.prototype.dispatchEvent = function(a) {
  if(!(a == p || a.type == p)) {
    a.type == "create" ? this.va.ZJ() : a.type == "draw" && this.va.$J();
    a.target = this;
    if(!(this.fe == p || this.fe[a.type] == p)) {
      for(var b = this.fe[a.type].length, c = 0;c < b;c++) {
        this.fe[a.type][c](a)
      }
    }
  }
};
A.W(B.prototype, "dispatchEvent", B.prototype.dispatchEvent);
B.prototype.ko = function() {
  return this.va.ko()
};
A.W(B.prototype, "canWrite", B.prototype.ko);
B.prototype.write = function(a) {
  this.va = ab(this);
  this.va.Ha(this);
  if(this.ko()) {
    if(a == h) {
      a = "__chart_generated_container__" + this.id;
      document.write('<div id="' + a + '"></div>')
    }
    var b = this;
    B.R.rL(function() {
      typeof a == "string" ? db(b, document.getElementById(a)) : db(b, a)
    })
  }
};
A.W(B.prototype, "write", B.prototype.write);
function db(a, b) {
  a.sk = a.va.write(b);
  a.sk && a.Zs() && (a.xmlFile != p ? a.Hi(a.xmlFile) : a.jsonFile != p ? a.Jn(a.jsonFile) : a.xmlData != p ? a.Gv(a.xmlData) : a.jsonData != p && a.xj(a.jsonData))
}
B.prototype.Zs = function() {
  return this.va.Zs()
};
A.W(B.prototype, "canSetConfigAfterWrite", B.prototype.Zs);
B.prototype.OA = function() {
  this.va.OA()
};
A.W(B.prototype, "updatePrintForFirefox", B.prototype.OA);
B.prototype.Mp = function(a, b) {
  this.width = a;
  this.height = b;
  this.va.Mp(a, b)
};
A.W(B.prototype, "setSize", B.prototype.Mp);
A.Gg("AnyChart.prototype.resize", B.prototype.Mp);
B.prototype.remove = function() {
  this.va.remove()
};
A.W(B.prototype, "remove", B.prototype.remove);
B.prototype.clear = function() {
  this.va.clear()
};
A.W(B.prototype, "clear", B.prototype.clear);
B.prototype.refresh = function() {
  this.va.refresh()
};
A.W(B.prototype, "refresh", B.prototype.refresh);
B.prototype.os = function(a, b) {
  this.va.os(a, b)
};
A.W(B.prototype, "updateData", B.prototype.os);
B.prototype.cA = function(a) {
  this.va.cA(a)
};
A.W(B.prototype, "setLoading", B.prototype.cA);
B.prototype.Ox = function() {
  return this.va.Ox()
};
A.W(B.prototype, "getInformation", B.prototype.Ox);
B.prototype.Hi = function(a) {
  this.xmlFile = a;
  this.sk && this.va.Hi(a)
};
A.W(B.prototype, "setXMLFile", B.prototype.Hi);
B.prototype.Oh = function(a) {
  this.xmlData = "" + a;
  this.sk && this.va.Oh(this.xmlData)
};
A.W(B.prototype, "setXMLData", B.prototype.Oh);
B.prototype.Gv = function(a) {
  this.sk && this.va.Gv(a)
};
A.W(B.prototype, "setXMLDataFromString", B.prototype.Gv);
B.prototype.kA = function(a) {
  this.sk && this.va.kA(a)
};
A.W(B.prototype, "setXMLDataFromURL", B.prototype.kA);
B.prototype.Np = function(a, b) {
  this.va.Np(a, b)
};
A.W(B.prototype, "setViewXMLFile", B.prototype.Np);
B.prototype.gA = function(a, b) {
  this.va.gA(a, b)
};
A.W(B.prototype, "setViewData", B.prototype.gA);
B.prototype.Jn = function(a) {
  this.jsonFile = a;
  this.sk && this.va.Jn(a)
};
A.W(B.prototype, "setJSONFile", B.prototype.Jn);
B.prototype.xj = function(a) {
  this.jsonData = a;
  this.sk && this.va.xj(this.jsonData)
};
A.W(B.prototype, "setJSONData", B.prototype.xj);
A.Gg("AnyChart.prototype.setJSData", B.prototype.xj);
B.prototype.setData = function(a) {
  if(a != p) {
    for(var b = "" + a;b.charAt(0) == " " && b.length > 0;) {
      b = b.substr(1)
    }
    if(b.charAt(0) == "<") {
      this.xmlData = "" + a;
      this.sk && this.va.Oh(this.xmlData)
    }else {
      this.jsonData = a;
      this.sk && this.va.xj(this.jsonData)
    }
  }
};
A.W(B.prototype, "setData", B.prototype.setData);
B.prototype.Od = function(a, b) {
  this.va.Od(a, b)
};
A.W(B.prototype, "addPoint", B.prototype.Od);
B.prototype.Lj = function(a, b, c) {
  this.va.Lj(a, b, c)
};
A.W(B.prototype, "addPointAt", B.prototype.Lj);
B.prototype.Ck = function(a, b) {
  this.va.Ck(a, b)
};
A.W(B.prototype, "removePoint", B.prototype.Ck);
B.prototype.Ok = function(a, b, c) {
  this.va.Ok(a, b, c)
};
A.W(B.prototype, "updatePoint", B.prototype.Ok);
B.prototype.Pk = function(a, b, c) {
  this.va.Pk(a, b, c)
};
A.W(B.prototype, "updatePointData", B.prototype.Pk);
B.prototype.nk = function(a, b, c) {
  this.va.nk(a, b, c)
};
A.W(B.prototype, "highlightPoint", B.prototype.nk);
B.prototype.uj = function(a, b, c) {
  this.va.uj(a, b, c)
};
A.W(B.prototype, "selectPoint", B.prototype.uj);
B.prototype.NA = function(a, b, c, d) {
  this.va.NA(a, b, c, d)
};
A.W(B.prototype, "updatePointWithAnimation", B.prototype.NA);
B.prototype.vA = function() {
  this.va.vA()
};
A.W(B.prototype, "startPointsAnimation", B.prototype.vA);
B.prototype.Mj = function(a) {
  this.va.Mj(a)
};
A.W(B.prototype, "addSeries", B.prototype.Mj);
B.prototype.Nj = function(a, b) {
  this.va.Nj(a, b)
};
A.W(B.prototype, "addSeriesAt", B.prototype.Nj);
B.prototype.Dk = function(a) {
  this.va.Dk(a)
};
A.W(B.prototype, "removeSeries", B.prototype.Dk);
B.prototype.Qk = function(a, b) {
  this.va.Qk(a, b)
};
A.W(B.prototype, "updateSeries", B.prototype.Qk);
B.prototype.Jk = function(a, b) {
  this.va.Jk(a, b)
};
A.W(B.prototype, "showSeries", B.prototype.Jk);
B.prototype.ok = function(a, b) {
  this.va.ok(a, b)
};
A.W(B.prototype, "highlightSeries", B.prototype.ok);
B.prototype.mk = function(a, b) {
  this.va.mk(a, b)
};
A.W(B.prototype, "highlightCategory", B.prototype.mk);
B.prototype.rA = function(a, b) {
  this.va.rA(a, b)
};
A.W(B.prototype, "showAxisMarker", B.prototype.rA);
B.prototype.Ik = function(a, b) {
  this.va.Ik(a, b)
};
A.W(B.prototype, "setPlotCustomAttribute", B.prototype.Ik);
B.prototype.Gs = function(a, b, c) {
  this.va.Gs(a, b, c)
};
A.W(B.prototype, "view_setPlotCustomAttribute", B.prototype.Gs);
B.prototype.xs = function(a, b) {
  this.va.xs(a, b)
};
A.W(B.prototype, "view_addSeries", B.prototype.xs);
B.prototype.Es = function(a, b) {
  this.va.Es(a, b)
};
A.W(B.prototype, "view_removeSeries", B.prototype.Es);
B.prototype.ys = function(a, b, c) {
  this.va.ys(a, b, c)
};
A.W(B.prototype, "view_addSeriesAt", B.prototype.ys);
B.prototype.Js = function(a, b, c) {
  this.va.Js(a, b, c)
};
A.W(B.prototype, "view_updateSeries", B.prototype.Js);
B.prototype.Hs = function(a, b, c) {
  this.va.Hs(a, b, c)
};
A.W(B.prototype, "view_showSeries", B.prototype.Hs);
B.prototype.vs = function(a, b, c) {
  this.va.vs(a, b, c)
};
A.W(B.prototype, "view_addPoint", B.prototype.vs);
B.prototype.ws = function(a, b, c, d) {
  this.va.ws(a, b, c, d)
};
A.W(B.prototype, "view_addPointAt", B.prototype.ws);
B.prototype.Ds = function(a, b, c) {
  this.va.Ds(a, b, c)
};
A.W(B.prototype, "view_removePoint", B.prototype.Ds);
B.prototype.Is = function(a, b, c, d) {
  this.va.Is(a, b, c, d)
};
A.W(B.prototype, "view_updatePoint", B.prototype.Is);
B.prototype.VA = function(a) {
  this.va.VA(a)
};
A.W(B.prototype, "view_clear", B.prototype.VA);
B.prototype.Cs = function(a) {
  this.va.Cs(a)
};
A.W(B.prototype, "view_refresh", B.prototype.Cs);
B.prototype.Bs = function(a, b, c) {
  this.va.Bs(a, b, c)
};
A.W(B.prototype, "view_highlightSeries", B.prototype.Bs);
B.prototype.As = function(a, b, c, d) {
  this.va.As(a, b, c, d)
};
A.W(B.prototype, "view_highlightPoint", B.prototype.As);
B.prototype.zs = function(a, b, c) {
  this.va.zs(a, b, c)
};
A.W(B.prototype, "view_highlightCategory", B.prototype.zs);
B.prototype.Fs = function(a, b, c, d) {
  this.va.Fs(a, b, c, d)
};
A.W(B.prototype, "view_selectPoint", B.prototype.Fs);
B.prototype.ps = function(a, b, c, d) {
  this.va.ps(a, b, c, d)
};
A.W(B.prototype, "updateViewPointData", B.prototype.ps);
B.prototype.Zq = function(a, b) {
  if(a == p || a == h) {
    a = NaN
  }
  if(b == p || b == h) {
    b = NaN
  }
  return this.va.Zq(a, b)
};
A.W(B.prototype, "getPNG", B.prototype.Zq);
A.Gg("AnyChart.prototype.getPNGImage", B.prototype.Zq);
B.prototype.ay = function(a, b) {
  if(a == p || a == h) {
    a = NaN
  }
  if(b == p || b == h) {
    b = NaN
  }
  return this.va.ay(a, b)
};
A.W(B.prototype, "getPDF", B.prototype.ay);
B.prototype.Hz = function() {
  this.va.Hz()
};
A.W(B.prototype, "printChart", B.prototype.Hz);
B.prototype.Uz = function(a, b) {
  if(a == p || a == h) {
    a = NaN
  }
  if(b == p || b == h) {
    b = NaN
  }
  this.va.Uz(a, b)
};
A.W(B.prototype, "saveAsImage", B.prototype.Uz);
B.prototype.Vz = function(a, b) {
  if(a == p || a == h) {
    a = NaN
  }
  if(b == p || b == h) {
    b = NaN
  }
  this.va.Vz(a, b)
};
A.W(B.prototype, "saveAsPDF", B.prototype.Vz);
B.prototype.Wz = function(a) {
  this.va.Wz(a)
};
A.W(B.prototype, "scrollXTo", B.prototype.Wz);
B.prototype.Xz = function(a) {
  this.va.Xz(a)
};
A.W(B.prototype, "scrollYTo", B.prototype.Xz);
B.prototype.scrollTo = function(a, b) {
  this.va.scrollTo(a, b)
};
A.W(B.prototype, "scrollTo", B.prototype.scrollTo);
B.prototype.SA = function(a, b) {
  this.va.SA(a, b)
};
A.W(B.prototype, "viewScrollXTo", B.prototype.SA);
B.prototype.TA = function(a, b) {
  this.va.TA(a, b)
};
A.W(B.prototype, "viewScrollYTo", B.prototype.TA);
B.prototype.hy = function() {
  return this.va.hy()
};
A.W(B.prototype, "getXScrollInfo", B.prototype.hy);
B.prototype.iy = function() {
  return this.va.iy()
};
A.W(B.prototype, "getYScrollInfo", B.prototype.iy);
B.prototype.fy = function(a) {
  return this.va.fy(a)
};
A.W(B.prototype, "getViewXScrollInfo", B.prototype.fy);
B.prototype.gy = function(a) {
  return this.va.gy(a)
};
A.W(B.prototype, "getViewYScrollInfo", B.prototype.gy);
B.prototype.lA = function(a) {
  this.va.lA(a)
};
A.W(B.prototype, "setXZoom", B.prototype.lA);
B.prototype.mA = function(a) {
  this.va.mA(a)
};
A.W(B.prototype, "setYZoom", B.prototype.mA);
B.prototype.nA = function(a, b) {
  this.va.nA(a, b)
};
A.W(B.prototype, "setZoom", B.prototype.nA);
B.prototype.hA = function(a, b) {
  this.va.hA(a, b)
};
A.W(B.prototype, "setViewXZoom", B.prototype.hA);
B.prototype.iA = function(a, b) {
  this.va.iA(a, b)
};
A.W(B.prototype, "setViewYZoom", B.prototype.iA);
B.prototype.jA = function(a, b, c) {
  this.va.jA(a, b, c)
};
A.W(B.prototype, "setViewZoom", B.prototype.jA);
B.prototype.Aw = function() {
  this.va.Aw()
};
A.W(B.prototype, "animate", B.prototype.Aw);
B.prototype.Ic = function(a) {
  return this.va.Ic(a)
};
A.W(B.prototype, "getSVG", B.prototype.Ic);
})();
