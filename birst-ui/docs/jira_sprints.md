
# Sprints

Sprints last two weeks - they start every other Thursday, run through the next week, and then there is a **feature freeze** on the following Monday at 6:00pm. This gives you eight working days to build out the features you were assigned.  There is then a **code freeze** at 6:00pm that Wednesday, giving two days for any bug fixes QA may deem necessary.

# JIRA

JIRA is the central location where all stories (features) and bugs are tracked. Each issue can either be assigned to a sprint, or to the "Visualizer_backlog". 

To get an overview of all the issues for the current sprint, visit the [Visualizer Board](https://mybirst.atlassian.net/secure/RapidBoard.jspa?rapidView=27&view=detail).

When you begin working on an issue, click the "Start Progress" button so that others are aware of what you are currently tackling. If you make commits related to an issue which don't resolve the issue (because you're still working on it), you can click "More -> Log Work" and log the amount of time you worked as well as the commit ID and with any other relevant comments.

Once you feel you have resolved the issue, click "Resolve Issue" and follow the same process as logging work (i.e. enter time spent, commit id, comments, and any other fields that are necessary). This issue will then be tested by QA, and either marked as "Resolved" or be "Reopened" if more work is necessary.

Speak with **Jerome Fong** to get set up with a JIRA account.