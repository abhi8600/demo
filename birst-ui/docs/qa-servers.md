
# QA Servers

There are currently two QA servers [QA4](https://sfo0-qa-04.birst.com) (https://sfo0-qa-04.birst.com) and [QA5](https://sfo0-qaapp-005.birst.com:9443) (https://sfo0-qaapp-005.birst.com:9443). At the current time, they both reflect the most recent dev build. Sometimes there are intermittent issues on one, and swapping to the other for a while often allows you to continue working without trouble.

The QA servers are updated with the most recent successful build each day at 9:00am, 3:00pm, 9:00pm, and 3:00am.

### Account

Speak with **Jerome Fong** to get set up with an account on the QA servers. You will also need some "spaces" associated with the account, so ask Jerome to copy someone else's spaces (**Ben**'s or **Jeff**'s) over to your account.