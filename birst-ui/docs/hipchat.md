
# HipChat

In **Junaid**'s words:

> HipChat is the default IM tool for the entire organization. IM is central to effective intra team communication and communication is central to be a productive team. So please move to it if you haven't already.

HipChat is an extremely useful way to communicate with everyone in the office, as well as stay aware of things as they happen.

### Setup

Download and install the [HipChat client](https://www.hipchat.com/downloads).  Talk to **Patrick Auld** to get an account. Set up HipChat to open on login, and optionally you can download the HipChat app on your phone.

Once you sign in, join any rooms you want and the history is saved such that you can follow all the conversations that occur(ed).