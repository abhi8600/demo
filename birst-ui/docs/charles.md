
# Charles

### Motivation

We do our development using QA servers. These servers are running a relatively recent build of the product - but as you make changes locally, your changes won't be reflected on the QA servers until you have committed them. To be able to see your local changes as you make them we use the Charles Proxy, which allows you to map local directories to remote hosts.

### Setup

1. Install the [Charles Proxy](http://www.charlesproxy.com/download/).
1. Register Charles with the registered name: `Birst, Inc.`, and license: `2348dbf1b3efe3f3d9`.
1. Since the QA servers are accessed over HTTPS, we must do some additional configuration. Follow the instructions in the "Mac OS X" section on the [Charless SSL Certificates](http://www.charlesproxy.com/documentation/using-charles/ssl-certificates/) page.
1. Navigate to `Proxy -> Proxy Settings`, and then in the `SSL` pane under "Locations", add an entry for the QA5 server using host: `sfo0-qaapp-005.birst.com` and port: `9443`.
1. In Charles, make sure **Recording** is toggled on.
1. Visit [QA5](https://sfo0-qaapp-005.birst.com:9443), and log in.
1. Navigate to Visualizer: [https://sfo0-qaapp-005.birst.com:9443/Visualizer/index.html#/](https://sfo0-qaapp-005.birst.com:9443/Visualizer/index.html#/).
1. You should now have a listing for the QA server in the "structure" pane. Expand the listing to find the `Visualizer` folder. Right-click this folder and select "Map Local", which will bring up the "Edit Mapping" pane. In the "Map To" section, change the local path to be `your/local/path/to/Designer2.0/app`.
1. Do the same thing for `index.html`, mapping it locally to `your/local/path/to/Designer2.0/app/index.html`.
1. Do the same mapping from `visualizer.aspx` to `your/local/path/to/Designer2.0/app/index.html`.
1. Turn off recording, and `File -> Save` this Charles configuration.  Whenever you start up Charles in the future, you will open this configuration, and all of your local mappings will be present.