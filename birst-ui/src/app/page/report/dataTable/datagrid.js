angular.module( 'app.grid', [] )

.directive( 'datagrid', function( $interpolate, $log ) {
	'use strict';
	var str = '<span>{{text}}</span>';
	var TEXT = 'text';
	var IMAGE = 'image';
	var BUTTON = 'button';
	return {
		link : function( scope, el, attrs ) {
			var fragment = $( document.createDocumentFragment() );
			angular.forEach( scope.report.itemCollection, function( item ) {
				switch( item.type ) {
					case TEXT:
						fragment.append( $( $interpolate( str )( item ) ).css( item.style ) );
						break;
					case IMAGE:
						var $el = $( '<img/>' ).attr( item.attrs );
						if( item.link ) {
							$el = $( '<a/>' ).css( item.style ).append( $el );
						} else {
							$el.css( item.style );
						}
						fragment.append( $el );
						break;
					case BUTTON:
						var $btnEl = $( '<button/>' ).text( item.text ).css( item.style );
						fragment.append( $btnEl );
						$btnEl.on( 'click', handleClick, item );
						break;
					default:
						$log.info( 'unhandled datagrid item' );
						$log.info( item );
				}
			} );
			el.html( fragment );
			var handleClick = function( e ) {
				//TODO: PORT (CONSUMPTION)
				// var link = new String( eOpts.getAttribute('link') );
				// if (link.indexOf('javascript:AdhocContent.DrillThru(') === 0) {
				// 	// drill or navigate
				// 	var parms = link.substring(link.indexOf('('), link.length);
				// 	var paramArray = parms.split(',');
				// 	for (var i = 0; i < paramArray.length; i++) {
				// 		paramArray[i] = Ext.String.trim(paramArray[i]);
				// 	}
				// 	if (paramArray[1] === "'Drill Down'") {
				// 		// drill down
				// 	// send this.xmlDoc, paramArray[2], paramArray[3]
				// 		Birst.core.Webservice.dashboardRequest('drillDown',
				// 			[
				// 				{key: 'xmlReport', value: xml2Str(this.dashletCtrl.adhocReportXml)},
				// 				{key: 'drillCols', value: paramArray[3].slice(1, -1)},
				// 				{key: 'filters', value: paramArray[2].slice(1, -1)}
				// 			],
				// 			this,
				// 			function(response, options) {
				// 				var xml = response.responseXML;
				// 				var xmlReportsArray = xml.getElementsByTagName('com.successmetricsinc.adhoc.AdhocReport');
				// 				this.dashletCtrl.adhocReportXml = xmlReportsArray[0];
				// 				var newPrompts = xml.getElementsByTagName('Prompts');
				// 				if (newPrompts.length > 0) {
				// 					this.dashletCtrl.promptsController.addPrompts(newPrompts[0]);
				// 				}
				// 				else {
				// 					if (this.dashletCtrl.promptsController) {
				// 						this.dashletCtrl.renderByReportXml(this.dashletCtrl.promptsController.getPromptsData());
				// 					}
				// 					else {
				// 						this.dashletCtrl.renderByReportXml('<Prompts/>');
				// 					}
				// 				}
				// 		});
				// 	}
				// 	else if (paramArray[2] === "'Drill To Dashboard'") {
				// 		var ctrlr = Dashboard.getApplication().getController('DashboardTabLists');
				// 		if (ctrlr) {
				// 			ctrlr.navigateToDash(paramArray[0].slice(2, -1), paramArray[1].slice(1, -1), paramArray[3].slice(1, -1));
				// 		}
				// 	}

				// }
				// else if (link) {
				// 	var dest = eOpts.getAttribute('target');
				// 	if (!dest) {
				// 		dest = '_blank';
				// 	}
				// 	window.open(link, dest);
				// }
			};
		}

	};
} );
