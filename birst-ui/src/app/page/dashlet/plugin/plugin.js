angular.module( 'app.plugin', [ 'plugins' ] )

.controller( 'pluginCtrl', function( $scope, Report ) {
	'use strict';
	Report.getPluginReport( [
		{ key : 'name', value : $scope.layout.dashlet.Path },
		{ key : 'pagePath', value : $scope.page['@dir'] },
		{ key : 'guid', value : 'noguidset' },
		{ key : 'logUsage', value : 'true' },
		{ key : 'isDashletEdit', value : 'true' }
	] ).then( function( pluginReport ) {
		$scope.layout.dashlet.pluginData = pluginReport.pluginData;
	} );
} )

.directive( 'plugin', function( plugins, $compile ) {
	'use strict';
	return {
		controller : 'pluginCtrl',
		link : function( scope, el, attrs ) {
			scope.$watch( 'layout.dashlet.pluginData', function( newVal, oldVal ) {
				if( newVal ) {
					plugins[ scope.layout.dashlet.plugin ]( scope.layout.dashlet.pluginData, null ).render( el, {} );
				}
			}, true );
		}

	};

} );
