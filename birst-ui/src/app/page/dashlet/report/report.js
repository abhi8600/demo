/* global Microsoft */
angular.module( 'app.report', [ 'models.chart', 'app.grid', 'directives.bingMap' ] )

.controller( 'reportCtrl', function( Report, $scope, $timeout ) {
	'use strict';

	var reportPath = $scope.layout.dashlet.Path || $scope.reportPath;

	var setupReportScope = function( report ) {
		if( report ) {
			$scope.reports[ reportPath ] = report;
		}
		$scope.report = report.report;
		$scope.selectors = report.selectors;
		$scope.adhocXml = report.adhocXml;
		$scope.pager = report.pager;
		$scope.errorCode = report.errorCode;
		$scope.errorMessage = report.errorMessage;
		$scope.errorMessageReason = report.errorMessageReason;
		$scope.hasError = report.errorCode && report.errorCode !== '0';
	};

	$scope.hasChart = false;

	if( !$scope.reports[ reportPath ] ) {
		$scope.loading = true;
		Report.getRenderedReport( { name : reportPath } )
		.then( setupReportScope )
		.finally( function() {
			$scope.loading = false;
		} );
	} else {
		// This enables the element to be fully rendered before triggering scope.report watch
		$timeout( function() {
			setupReportScope( $scope.reports[ reportPath ] );
		} );
	}
	$scope.redrawChart = function( w, h ) {
		if( $scope.chartObj && !$scope.report.static ) {
			$scope.chartObj.resize( w, h );
		}
	};
} )

.directive( 'report', function( Chart, lm ) {
	'use strict';
	return {

		controller : 'reportCtrl',

		link : function( scope, el, attrs, $window ) {
			var svgs = [];
			var resizeable = false;
			var throttle;

			var setSvgAttrs = function() {
				if( resizeable ) {
					svgs = svgs.length ? svgs : el.children( 'svg' );
					var w = el.innerWidth();
					var h = el.innerHeight();
					svgs.each( function( i, svgEl ) {
						svgEl.setAttribute( 'viewBox', '0 0 ' + w + ' ' + h );
						svgEl.setAttribute( 'preserveAspectRatio', 'none' );
						$( svgEl ).css( { width : '100%', height : '100%' } );
					} );
				}
			};

			var unsetSvgAttrs = function() {
				if( resizeable ) {
					svgs.each( function( i, svgEl ) {
						svgEl.removeAttribute( 'viewBox' );
						svgEl.removeAttribute( 'preserveAspectRatio' );
					} );
				}
			};

			var handleOnResize = function() {
				if ( scope.chartObj && resizeable ) {
					unsetSvgAttrs();
					scope.redrawChart( el.width(), el.height() );
					setSvgAttrs();
				}
			};

			var setChartError = function( typeStr ) {
				scope.error = scope.error || {};
				scope.error.errorCode = '9';
				scope.error.errorMessage = lm.get( 'CHART_NOTSUPPORTED', { chartType : typeStr } );
			};

			angular.element( window ).smartresize( handleOnResize, 300 );
			scope.$on( 'resize', handleOnResize );

			scope.$watch( 'layout.resized', function( newVal ) {
				if ( newVal ) {
					handleOnResize();
					scope.layout.resized = false;
				}
			} );

			scope.$watch( 'report' , function( newVal, oldVal ) {
				if( newVal ) {
					resizeable = !scope.report.static;
					angular.forEach( scope.report.chartCollection, function( chartOpts, index ) {
						var chartModel = new Chart( angular.extend( chartOpts, { width : el.innerWidth(), height : el.innerHeight(), static : scope.report.static } ) );

						switch( chartModel.chartType ){
							case 'UMap':
								scope.map.bingMap = chartModel.bingMap;
								break;
							case 'Radar':
								setChartError( 'Radar');
								break;
							case 'Map':
								setChartError( 'Map' );
								break;
							default:
								scope.chartObj = new AnyChart();
								scope.chartObj.width = scope.report.static  ? chartModel.anyChart.width : el.width();
								scope.chartObj.height = scope.report.static ? chartModel.anyChart.height : el.height();
								scope.chartObj.enableResize = false;
								scope.chartObj.setData( chartModel.anyChart.chartData );
								scope.loading = true;
								scope.chartObj.addEventListener( 'render', function() {
									scope.loading = false;
								} );
								scope.chartObj.write( el[0] );
								if( !resizeable ) {
									el.children( 'svg' ).eq( index ).css( { position: 'absolute', top : chartModel.y, left : chartModel.x } );
								}
						}
						setSvgAttrs();
					} );
				}
			} );
		}
	};
} );
