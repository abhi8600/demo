angular.module( 'app.dashlet', [] )

.controller( 'dashletCtrl', function( $scope ) {
	'use strict';
	$scope.file = $scope.layout.dashlet.file;
	// Define empty error object;
	$scope.file.error = {};
	$scope.map = {};
} )

.directive( 'dashlet', function() {
	'use strict';

	var PERCENT_SYMBOL = '%';
	var targets = [ 'percentLeft', 'percentTop', 'percentWidth', 'percentHeight' ];
	var targetNames = [ 'left', 'top', 'width', 'height' ];

	return {
		templateUrl : 'page/dashlet/dashlet.tpl.html',

		restrict : 'E',

		controller : 'dashletCtrl',

		link : function( scope, element, attrs) {
			var layout = scope.layout;
			// Store the event property name we want to use
			var targetProperty;
			element.on( 'transitionend', function( e ) {
				// Only run once, otherwise will run for each top, left, w, h and redraw four times
				if( e.propertyName === targetProperty ) {
					scope.$broadcast( 'resize' );
					targetProperty = undefined;
				}
			} );

			scope.$watch( 'layout', function( newLayout, oldLayout ) {
				if ( !layout.resizing && !layout.moving ) {
					// Only target one legitimate property change of transtion
					_.each( targets, function( val, i ) {
						if( newLayout[ val ] !== oldLayout[ val ] ) {
							targetProperty = targetNames[ i ];
						}
					} );
					element.css( {
						left : layout.percentLeft + PERCENT_SYMBOL,
						top : layout.percentTop + PERCENT_SYMBOL,
						width : layout.percentWidth + PERCENT_SYMBOL,
						height : layout.percentHeight + PERCENT_SYMBOL,
						zIndex : layout.zIndex
					} );
				}
			}, true );
		}
	};
} );
