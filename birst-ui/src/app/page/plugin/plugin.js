angular.module( 'app.plugin', [ 'plugins' ] )

.controller( 'pluginCtrl', function( $scope, Report ) {
	'use strict';
	var reportPath = $scope.file.fullPath;
	if( $scope.reports.hasOwnProperty( reportPath ) ){
		$scope.pluginData = $scope.reports[ reportPath ];
		$scope.file.loading = false;
	} else {
		Report.getPluginReport( [
			{ key : 'name', value : $scope.file.fullPath },
			{ key : 'pagePath', value : $scope.page['@dir'] },
			{ key : 'guid', value : 'noguidset' },
			{ key : 'logUsage', value : 'true' },
			{ key : 'isDashletEdit', value : 'true' }
		] ).then( function( pluginReport ) {
			$scope.pluginData = $scope.reports[ reportPath ] = pluginReport.pluginData;
		} ).finally( function() {
			$scope.file.loading = false;
		} );
	}
} )

.directive( 'plugin', function( plugins, $compile ) {
	'use strict';
	return {
		controller : 'pluginCtrl',
		link : function( scope, el, attrs ) {
			var plugin = scope.file.getPlugin();
			scope.$watch( 'pluginData', function( newVal, oldVal ) {
				if( newVal ) {
					plugins[ plugin ]( scope.pluginData, null ).render( el, {} );
				}
			}, true );
		}

	};

} );
