angular.module( 'app.promptEditor', [ 'models.Prompt' ] )

.controller( 'promptEditorCtrl', function( $scope, Prompt ) {
	'use strict';
	$scope.savePrompt = function() {
		var isUpdate = $scope.prompt.hasOwnProperty( 'uuid' );
		$scope.prompt.post().then( function() {
			$scope.closePromptEditor();
			if( !isUpdate ) {
				$scope.prompts.push( $scope.prompt );
			}
			$scope.prompt = new Prompt();
		} );
	};
	$scope.prompt = $scope.prompt || new Prompt();
} )

.directive( 'promptEditor', function( Prompt ) {
	'use strict';
	return {
		templateUrl : 'page/prompt/promptForm.tpl.html',
		controller : 'promptEditorCtrl',
		link : function( scope, el, attrs ) {
			var $m = el.parents( '.modal' );
			scope.closePromptEditor = function(){
				$m.modal( 'hide' );
			};
		}
	};
} );
