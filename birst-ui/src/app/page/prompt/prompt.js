angular.module( 'app.prompt', [ 'directives.onOffSwitch', 'app.promptEditor' ] )

.controller( 'promptCtrl', function( $scope ) {
	'use strict';

	function setSelected( prompt ) {
		if( prompt.$$hashKey !== $scope.prompt.$$hashKey ) {
			prompt.selected = false;
		} else {
			prompt.selected = !prompt.selected;
		}
	}

	$scope.toggleSelected = function() {
		angular.forEach( $scope.prompts, setSelected );
	};
} )

.directive( 'prompt', function() {
	'use strict';
	return {
		controller : 'promptCtrl',
		templateUrl : 'page/prompt/prompt.tpl.html',
		scope : {
			prompt : '=',
			prompts : '=',
			editMode : '='
		}
	};
} );
