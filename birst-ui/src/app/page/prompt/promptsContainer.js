angular.module( 'app.promptsContainer', [] )

.controller( 'promptsContainerCtrl', function( $scope ) {
	'use strict';
	var selectedIndex;
	// initialize prompts
	angular.forEach( $scope.page.prompts, function( id ) {
		var prompt = _.find( $scope.prompts, id );
		if( prompt ) {
			prompt.active = true;
		}
	} );

	$scope.$watch( 'prompts', function() {
		var active = _.where( $scope.prompts, { active : true } );
		$scope.promptSelected = $scope.isPromptSelected();
		var ids = [];
		if( active ) {
			ids = _.map( active, function( p ) {
				return _.pick( p, 'uuid' );
			} );
		} else {
			ids = [];
		}
		$scope.page.prompts = ids;
	}, true );

	$scope.selectedPrompt = function() {
		return _.find( $scope.prompts, function( prompt, i ) {
			var retVal = false;
			if( prompt.selected === true ) {
				retVal = true;
				selectedIndex = i;
			}
			return retVal;
		} );
	};

	$scope.isPromptSelected = function() {
		return typeof $scope.selectedPrompt() !== 'undefined';
	};

	$scope.deletePrompt = function() {
		var prompt = $scope.selectedPrompt();
		prompt.delete().then( function() {
			$scope.prompts.splice( selectedIndex, 1 );
		} );
	};

	$scope.editPrompt = function() {
		$scope.prompt = $scope.selectedPrompt();
	};

} )

.directive( 'promptsContainer', function() {
	'use strict';
	return {
		controller : 'promptsContainerCtrl',
		templateUrl : 'page/prompt/promptsContainer.tpl.html'
	};
} );
