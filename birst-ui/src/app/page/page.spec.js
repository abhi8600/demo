describe( 'page section', function() {
	'use strict';
	var $http, $httpBackend, $rootScope, $scope, ctrl, $injector, $state, Page, pagesMock, promptMock, state = 'page', state2 = 'newpage';
	beforeEach( module( 'app.page' ) );

	beforeEach( function() {
		window.Meny = {
			create : function() {}
		};
		module( 'dashboards', function( $provide ) {
			$provide.value( 'Page', pagesMock = {} );
			$provide.value( 'Prompt', promptMock = {} );
		});
		inject( function( _$http_, _$httpBackend_, _$injector_, _$state_ ) {
			$http = _$http_;
			$httpBackend = _$httpBackend_;
			$injector = _$injector_;
			$state = _$state_;
		} );
	} );

	beforeEach( inject( function( _$rootScope_, _$controller_ ) {
		$rootScope = _$rootScope_;
		$scope = _$rootScope_.$new();
	} ) );

	it('should respond to URL with dbname and page id', function() {
		expect( $state.href( state, { dbname: 'fakedbname' ,uuid: 'fakeid' } ) ).toEqual('#/dashboards/fakedbname/page/fakeid');
	});

	it('should respond to URL with dbname and no page id', function() {
		expect( $state.href( state2, { dbname: 'fakedbname' } ) ).toEqual('#/dashboards/fakedbname/page');
	});

	it( 'should resolve page data', function() {
		/* jshint ignore:start */
		pagesMock.get = jasmine.createSpy( 'get' ).andReturn( 'get' );
		promptMock.all = jasmine.createSpy( 'all' ).andReturn( 'all' );
		/* jshint ignore:end */
		$state.go( state, { id : 'test' } );
		$rootScope.$digest();
		expect( $state.current.name ).toBe( state );
		// Call invoke to inject dependencies and run function
		expect( $injector.invoke( $state.current.resolve.page ) ).toBe( 'get' );
		expect( $injector.invoke( $state.current.resolve.prompts ) ).toBe( 'all' );
	} );

});
