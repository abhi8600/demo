angular.module( 'app.page', [
	'ui.router',
	'models.page',
	'models.pageItem',
	'models.dashlet',
	'models.report',
	'app.dashlet',
	'app.report',
	'app.plugin',
	'app.grid',
	'directives.bMoveable',
	'directives.bSelectable',
	'directives.fileBrowser.fileSystemB',
	'contenteditable',
	'app.fileStage',
	'directives.bcarousel',
	'app.prompt',
	'directives.sideDrawer',
	'models.Prompt',
	'app.promptsContainer',
	//'models.SubjectArea'
] )

.config(function config( $stateProvider ) {
	'use strict';
	// Existing page, has id in route

	var routeDefaults = {
		controller : 'PageCtrl',
		templateUrl : 'page/page.tpl.html',
		data : { pageTitle : 'Create Page', editMode : true, current : 'dashboards' },
	};
	var newPage = function ( Page, $stateParams ) {
		return new Page( {
			'@name' : 'New Page',
			'@dir' : '/' + decodeURI( $stateParams.dbname ),
			'ClickToOpenClosePrompts' : 'false',
			LayoutV2 : []
		} );
	};
	var dropObject = function( Dashlet, $stateParams ) {
		var path = $stateParams.path.replace( /:/g, '/' ).replace( /%3A/g, '/' );
		return {
			name : path + '.viz.dashlet',
			label : Dashlet.getLabelFromPath( path )
		};
	};
	var existingPage = function( Page, $stateParams ){
		return Page.get( $stateParams.uuid );
	};
	var prompts = function( Prompt ) {
		return Prompt.all();
	};

	var resolveNoItem = function() { return false; };
	var resolveItem = [ 'Dashlet', '$stateParams', dropObject ];
	var resolvePage = [ 'Page', '$stateParams', existingPage ];
	var resolveNewPage = [ 'Page', '$stateParams', newPage ];
	var resolvePrompts = [ 'Prompt', prompts ];

	$stateProvider.state( 'newpage', angular.extend( {}, routeDefaults, {
		url : '/dashboards/:dbname/page',
		resolve : {
			page : resolveNewPage,
			item : resolveNoItem,
			prompts : resolvePrompts
		}
	}) );

	$stateProvider.state( 'page', angular.extend( {}, routeDefaults, {
		url : '/dashboards/:dbname/page/:uuid',
		data : { pageTitle : 'View Page', editMode : false, current : 'dashboards' },
		resolve : {
			page : resolvePage,
			item : resolveNoItem,
			prompts : resolvePrompts
		}
	} ) );

	$stateProvider.state( 'publishtopage', angular.extend( {}, routeDefaults, {
		url : '/dashboards/:dbname/page/:uuid/add/:path',
		data : { pageTitle : 'View Page', editMode : true, current : 'dashboards' },
		resolve : {
			page : resolvePage,
			item : resolveItem,
			prompts : resolvePrompts
		}
	} ) );

	$stateProvider.state( 'publishtonew', angular.extend( {}, routeDefaults, {
		url : '/dashboards/:dbname/page/add/:path',
		resolve : {
			page : resolveNewPage,
			item : resolveItem,
			prompts : resolvePrompts
		}
	} ) );
} )

/**
 * And of course we define a controller for our route.
 */
.controller( 'PageCtrl', function PageController( $scope, page, PageItem, Page, $state, item, $stateParams, lm, prompts ) {
	'use strict';
	$scope.page = page;
	$scope.disableInline = false;
	$scope.prompts = prompts;
	$scope.editMode = $state.current.data.editMode;
	var initialLoad = true;
	$scope.reports = {};
	//SubjectArea.getSubjectArea();
	$scope.filterSearchPlaceholder = lm.get( 'SEARCH_FILTERS' );
	var handleMoving = function( obj, isMoving ) {
		$scope.disableInline = isMoving;
	};

	$scope.createReport = function() {
		var params = {
			dbpage : page.uuid || 'new',
			dbname : $stateParams.dbname
		};
		window.location.href = '/Visualizer/index.html#/?' + $.param( params );
	};

	$scope.$on( 'bmoving', handleMoving );
	$scope.$on( 'bresizing', handleMoving );

	$scope.onDrop = function( droppedObject, dropCoordinates ) {
		var pageItem = new PageItem( { file : droppedObject } );
		pageItem.setCenter( dropCoordinates );
		pageItem.zIndex = page.LayoutV2.length;
		pageItem.dashlet.referenceReport( droppedObject );
		page.LayoutV2.push( pageItem );
	};

	$scope.reLayer = function( topLayerIndex ) {
		var testIndex = $scope.page.LayoutV2[topLayerIndex].zIndex;
		angular.forEach( $scope.page.LayoutV2, function( obj, index ) {
			if( topLayerIndex === index ) {
				obj.zIndex = $scope.page.LayoutV2.length - 1;
			} else if( obj.zIndex > testIndex ) {
				obj.zIndex -= 1;
			}
		} );
	};

	$scope.removeLayout = function( layout ) {
		_.pull( $scope.page.LayoutV2, layout );
	};

	$scope.editPage = function() {
		$scope.editMode = true;
	};

	$scope.savePage = function(){
		page.post().then( function() {
			$scope.nameUpdated = false;
			$scope.layoutUpdated = false;
			$scope.editMode = false;
			$scope.$broadcast( 'saved', $scope.page['@name'] );
		} );
	};

	// detects when any dashlets are added, removed, or modified and allows
	// re-enables ability to save the page.
	$scope.$watch( 'page.LayoutV2', function( newVal, oldVal ) {
		if ( initialLoad ) {
			initialLoad = false;
		} else {
			$scope.layoutUpdated = true;
		}
	}, true );

	$scope.$watch( 'processSelectedFiles', function( newVal, oldVal ) {
		if( newVal ) {
			var ln = $scope.selectedFiles.length;
			var half = Math.floor( ln / 2 );
			var initial = half < 48 ? 50 - half : 0;
			var coords = { x : initial, y : initial - 2 };
			_.each( $scope.selectedFiles, function( file ) {
				$scope.onDrop( file, coords );
				coords.x += 2;
				coords.y += 2;
			} );
			$scope.selectedFiles = [];
			$scope.processSelectedFiles = false;
		}
	} );
	if( item ) {
		$scope.onDrop( item, { x : 50, y : 50 } );
	}
} );
