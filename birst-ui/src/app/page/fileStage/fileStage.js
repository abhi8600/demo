angular.module( 'app.fileStage', [ 'app.stagedFile' ] )

.controller( 'fileStageCtrl', function( $scope, groupsOfFilter ) {
	'use strict';
	$scope.deleteStagedFile = function( parentI, i ) {
		$scope.selectedFiles.splice( ( parentI * 5 ) + i, 1 );
		$scope.$emit( 'updateBCarousel', window.Math.ceil( $scope.selectedFiles.length / 5 ) );
	};

	$scope.fileStageGroups = [];

	$scope.$watchCollection( 'selectedFiles', function() {
		$scope.fileStageGroups = groupsOfFilter( $scope.selectedFiles, 5, $scope.fileStageGroups );
	} );

} )

.directive( 'fileStage', function() {
	'use strict';
	return {
		templateUrl : 'page/fileStage/fileStage.tpl.html',
		controller : 'fileStageCtrl'
	};
} );
