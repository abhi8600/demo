angular.module( 'app.stagedFile', [] )

.controller( 'stagedFileCtrl', function( $scope ) {
	'use strict';
	$scope.map = {};
	$scope.file.error = {};
	$scope.minimal = true;
	$scope.path = $scope.file.fullPath;
	$scope.file.loading = true;
} )

.directive( 'stagedFile', function() {
	'use strict';
	return {
		controller : 'stagedFileCtrl'
	};
} );
