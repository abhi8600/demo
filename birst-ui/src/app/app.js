angular.module( 'dashboards', [
	'ngSanitize',
	'templates-app',
	'templates-common',
	'app.page',
	'ui.router',
	'locale.messages' ,
	'filters',
	'services.localizedMessages',
	'directives.lm',
	'directives.bDraggable',
	'directives.bDropTarget',
	'directives.bResizable',
	'directives.meny',
	'directives.lang',
	'visualizer'
])

.constant( 'APP_CONFIG', {
	adhocEndpoint : '/SMIWeb/services/Adhoc.AdhocHttpSoap11Endpoint',
	adhocNamespace : 'http://adhoc.WebServices.successmetricsinc.com',
	fileEndpoint : '/SMIWeb/services/File.FileHttpSoap11Endpoint',
	fileNamespace : 'http://file.WebServices.successmetricsinc.com',
	bingMapsApiKey : 'AmgA_JU8O0xHVGR43nD1MgKGCRif6NjLNkwJj7r31ZlNm5z7iVjJXt1wCuykqgDk',
	promptEndpoint : '/SMIWeb/rest/dashboards/prompt',
	subjectAreaEndpoint : '/SMIWeb/services/SubjectArea.SubjectAreaHttpSoap11Endpoint',
	subjectAreaNamespace : 'http://SubjectArea.WebServices.successmetricsinc.com'
} )

.config( function myAppConfig ( $stateProvider, $urlRouterProvider ) {
	'use strict';
	$urlRouterProvider.otherwise( 'visualizer' );
	AnyChart.renderingType = anychart.RenderingType.SVG_ONLY;
	AnyChart.enabledChartMouseEvents = true;
})

.run( function run () {
})

.controller( 'AppCtrl', function AppCtrl ( $scope, $location, $http, lm, $rootScope ) {
	'use strict';
	$rootScope.$watch( 'locale', function( newVal, oldVal ) {
		lm.setLocale( $scope.locale );
	} );
	$scope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams){
		if ( angular.isDefined( toState.data.pageTitle ) ) {
			$scope.pageTitle = toState.data.pageTitle + ' | birst' ;
		}
		if ( angular.isDefined( toState.data.current ) ) {
			$scope.current = toState.data.current;
		}
		if ( angular.isDefined( toState.data.editMode ) ) {
			$scope.editMode = toState.data.editMode;
		}
	});
})

;

