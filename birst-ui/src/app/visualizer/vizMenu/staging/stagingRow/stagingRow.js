angular.module('visualizer').

directive('stagingRow', function () {
	'use strict';
    return {
        require: '^?designer',
        replace: true,
        templateUrl: 'visualizer/vizMenu/staging/stagingRow/stagingRow.tpl.html',
        scope: true,

        link: function (scope, elem, attrs) {
            //scope.editingMode = false;
            scope.column = scope.$eval(attrs.stagingRow);
            scope.$watch(function() {
                return scope.$eval(attrs.stagingRow);
            }, function(val) { scope.column = val; });
            scope.serie = scope.$eval(attrs.serie);
            scope.stagingType = attrs.stagingType;
            scope.columnPropertiesOpen = true;
        }
    };
} );
