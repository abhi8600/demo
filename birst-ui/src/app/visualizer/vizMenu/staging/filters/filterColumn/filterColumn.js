angular.module('visualizer')

.directive('filterColumn', function() {
	'use strict';
    return {
        replace: true,
        scope: {
            filterColumn: '=',
            filterData: '='
        },
        templateUrl: 'visualizer/vizMenu/staging/filters/filterColumn/filterColumn.tpl.html',
        controller: function($scope, $element) {

            $scope.$watch('filterData', function(data) {
                if (!data) {
                    return;
                }

                var column = data.column,
                    columnName = column.name,
                    filterSetting = data.column.filter || data.column.displayFilter,
                    allColumns = data.allColumns,
                    resultSet = data.resultSet,
                    columnIndex = _.indexOf(allColumns, column),
                    filterArray = _.uniq(_.pluck(resultSet.rows, columnIndex)),
                    columnDataType = resultSet.dataTypes[columnIndex],
                    lowestValue,
                    highestValue;

                var mapSelectedValues = function() {
                    var items = _.map(filterSetting.items, function(item) {
                        return item.operand;
                    });
                    return _.map($scope.filterValues, function(item) {
                        if (_.contains(items, item)) {
                            return true;
                        } else {
                            return false;
                        }
                    });
                };

                var findRangeMin = function(items) {
                    return _.find(items, function(item) {
                        return item.operator === '>' || item.operator === '>=';
                    });
                };

                var findRangeMax = function(items) {
                    return _.find(items, function(item) {
                        return item.operator === '<' || item.operator === '<=' || item.operator === '=' || item.operator === '<>';
                    });
                };

                var findRangeType = function(items) {
                    if (findRangeMax(items) && findRangeMin(items)) {
                        return 'Between';
                    } else if (findRangeMax(items) && !findRangeMin(items)) {
                        return '<';
                    } else if (findRangeMin(items) && !findRangeMax(items)) {
                        return '>';
                    } else {
                        return '=';
                    }
                };

                function sortValues() {
                    if ($scope.filterType === 'date') {
                        filterArray = _.map(filterArray, function(date) {
                            return new Date(date);
                        });
                        $scope.filterValues = filterArray.sort(function(x, y) {
                            return x > y ? -1 : x < y ? 1 : 0;
                        });
                    } else if ($scope.filterType === 'range') {
                        $scope.filterValues = filterArray.sort();
                    } else if ($scope.filterType === 'items') {
                        $scope.filterValues = columnDataType === 'VARCHAR' ? filterArray.sort() : filterArray.sort(function(x, y) {
                            return x - y;
                        });
                    }

                    lowestValue = _.min(filterArray);
                    highestValue = _.max(filterArray);
                }

                function setUIValues() {
                    var rangeMin, rangeMax;

                    $scope.title = columnName;
                    $scope.filterInfo = data.filterInfo;
                    $scope.filterTypes = data.filterTypes;
                    $scope.filterInfoReverse = data.filterInfoReverse;

                    // UI values for number based filtering using slider
                    $scope.sliderObj = {};
                    $scope.sliderObj.roundingPoints = 0; // may be user controlled later?
                    $scope.sliderObj.ranges = {};
                    $scope.sliderObj.selectedType = filterSetting && filterSetting.subType ? filterSetting.subType : undefined;
                    $scope.sliderObj.showSlider = true;

                    // // UI values for string based filtering using select box or text input
                    $scope.itemFilter = {};
                    $scope.itemFilter.selectedType = filterSetting && filterSetting.subType ? filterSetting.subType : '=';
                    $scope.itemFilter.selectOrInput = filterSetting && filterSetting.selectOrInput ? filterSetting.selectOrInput : 'select';
                    $scope.itemFilter.selectedItems = filterSetting && filterSetting.items &&
                        filterSetting.selectOrInput === 'select' &&
                        filterSetting.items.length ? mapSelectedValues() : [];
                    $scope.itemFilter.textInput = filterSetting && filterSetting.selectOrInput === 'input' && filterSetting.items.length === 1 ? filterSetting.items[0].operand : '';

                    // // UI values for date based filtering
                    $scope.datePicker = {};
                    $scope.datePicker.ranges = {};
                    $scope.datePicker.selectedType = filterSetting && filterSetting.subType ? filterSetting.subType : undefined;
                    $scope.datePicker.showDatepicker = true;

                    if ($scope.filterType === 'date') {
                        rangeMin = filterSetting && filterSetting.items && findRangeMin(filterSetting.items);
                        rangeMax = filterSetting && filterSetting.items && findRangeMax(filterSetting.items);
                        $scope.datePicker.ranges.min = rangeMin ? rangeMin.operand : lowestValue;
                        $scope.datePicker.ranges.max = rangeMax ? rangeMax.operand : highestValue;
                        $scope.datePicker.selectedType = $scope.datePicker.selectedType || (filterSetting && filterSetting.items && findRangeType(filterSetting.items)) || '=';
                    } else if ($scope.filterType === 'range') {
                        rangeMin = filterSetting && filterSetting.items && findRangeMin(filterSetting.items);
                        rangeMax = filterSetting && filterSetting.items && findRangeMax(filterSetting.items);
                        $scope.sliderObj.ranges.min = rangeMin ? rangeMin.operand : lowestValue;
                        $scope.sliderObj.ranges.max = rangeMax ? rangeMax.operand : highestValue;
                        $scope.sliderObj.selectedType = $scope.sliderObj.selectedType || (filterSetting && filterSetting.items && findRangeType(filterSetting.items)) || 'Between';
                        $scope.sliderObj.lowestValue = lowestValue;
                        $scope.sliderObj.highestValue = highestValue;

                        if ($scope.sliderObj.ranges.min === -Infinity || $scope.sliderObj.ranges.max === Infinity) {
                            $scope.sliderObj.ranges.min = 0; // zero
                            $scope.sliderObj.ranges.max = 100000000000000; // 100 trillion
                        } else if ($scope.sliderObj.ranges.min === $scope.sliderObj.ranges.max) {
                            $scope.sliderObj.ranges.min = 0;
                        }
                        $scope.sliderObj.ranges.min = $scope.sliderObj.ranges.min && Number($scope.sliderObj.ranges.min).toFixed($scope.sliderObj.roundingPoints);
                        $scope.sliderObj.ranges.max = $scope.sliderObj.ranges.max && Number($scope.sliderObj.ranges.max).toFixed($scope.sliderObj.roundingPoints);
                    }
                }

                function initializeFilters() {
                    filterArray = _.without(_.compact(filterArray), '(is missing)');
                    if (!$scope.filterType) {
                        // Detemine which type of filter to use: date, range, or items
                        if (columnDataType === "TIMESTAMP" || columnDataType === 'DATE') {
                            $scope.filterType = 'date';
                            filterArray = _.map(filterArray, function(date) {
                                return new Date(date);
                            });
                        } else if (_.every(filterArray, _.isNumber) && columnDataType !== 'VARCHAR') {
                            $scope.filterType = 'range';
                        } else if (columnDataType === 'VARCHAR') {
                            $scope.filterType = 'items';
                        } else {
                            $scope.filterType = 'range';
                        }
                    }

                    sortValues();
                    setUIValues();
                }

                initializeFilters();

            });

            $scope.$watch('itemFilter.selectedType', function(option) {
                if (option === '=' || option === '<>') {
                    $scope.itemFilter.selectOrInput = 'select';
                } else if (option === ' LIKE ' || option === ' NOT LIKE ') {
                    $scope.itemFilter.selectOrInput = 'input';
                } else if (option === ' IS NULL ' || option === ' IS NOT NULL ') {
                    $scope.itemFilter.selectOrInput = 'missing';
                }
            });

            $scope.$watch('sliderObj.selectedType', function(option) {
                if (option === ' IS NULL ' || option === ' IS NOT NULL ') {
                    $scope.sliderObj.showSlider = false;
                } else {
                    if ($scope.sliderObj) {
                        $scope.sliderObj.showSlider = true;
                    }
                }
            });

            $scope.$watch('datePicker.selectedType', function(option) {
                if (option === ' IS NULL ' || option === ' IS NOT NULL ') {
                    $scope.datePicker.showDatepicker = false;
                } else {
                    if ($scope.datePicker) {
                        $scope.datePicker.showDatepicker = true;
                    }
                }
            });

            $scope.toggleFilter = function() {
                if ($scope.filterType === 'range') {
                    $scope.filterType = 'items';
                } else if ($scope.filterType === 'items') {
                    $scope.filterType = 'range';
                }
            };

            $scope.save = function(itemFilter, sliderRanges, dateRanges) {
                var filter = {}, rangeMin, rangeMax;

                var saveItems = function() {
                    var items = _.map(itemFilter.selectedItems, function(item, index) {
                        return item === true ? $scope.filterValues[index] : null;
                    });
                    items = _.compact(items);
                    filter.type = 'items';
                    if (itemFilter.selectOrInput === 'select') {
                        filter.items = _.map(items, function(item) {
                            return {
                                operator: itemFilter.selectedType,
                                operand: item
                            };
                        });
                    } else if (itemFilter.selectOrInput === 'input') {
                        filter.items = [{
                            operator: itemFilter.selectedType,
                            operand: itemFilter.selectedType.indexOf('LIKE') !== -1 && itemFilter.textInput.indexOf('%') === -1 ? itemFilter.textInput + '%' : itemFilter.textInput
                        }];
                    } else if (itemFilter.selectOrInput === 'missing') {
                        filter.items = [{
                            operator: itemFilter.selectedType
                        }];
                    }
                    filter.subType = itemFilter.selectedType;
                    filter.selectOrInput = itemFilter.selectOrInput;
                };

                var saveRange = function() {
                    filter.type = 'range';
                    filter.items = [];
                    filter.subType = $scope.sliderObj.selectedType;

                    rangeMin = {
                        operator: filter.subType === 'Between' ? '>' : filter.subType === 'Outside' ? '<' : filter.subType,
                        operand: sliderRanges.min
                    };
                    rangeMax = {
                        operator: filter.subType === 'Between' ? '<' : filter.subType === 'Outside' ? '>' : filter.subType,
                        operand: sliderRanges.max
                    };

                    if (filter.subType === 'Between' || filter.subType === 'Outside') {
                        filter.items.push(rangeMin, rangeMax);
                    } else if (filter.subType === '>' || filter.subType === '>=') {
                        filter.items.push(rangeMin);
                    } else if (filter.subType === '<' || filter.subType === '<=' || filter.subType === '=' || filter.subType === '<>') {
                        filter.items.push(rangeMax);
                    } else if (filter.subType === ' IS NULL ' || filter.subType === ' IS NOT NULL ') {
                        filter.items = [{
                            operator: filter.subType
                        }];
                    }
                };

                var saveDates = function() {
                    filter.type = 'date';
                    filter.items = [];
                    filter.subType = $scope.datePicker.selectedType;

                    var momentDateFormat = $scope.filterColumn.properties.warehouseDataType === 'DateTime' ? 'YYYY/MM/DD HH:mm:ss' : 'YYYY/MM/DD';
                    // remove last 15 characters from TIMESTAMPs
                    if ($scope.filterColumn.properties.warehouseDataType === 'DateTime') {
                        dateRanges.min = dateRanges.min && dateRanges.min.toISOString();
                        dateRanges.max = dateRanges.max && dateRanges.max.toISOString();
                    }

                    rangeMin = {
                        operator: filter.subType === 'Between' ? '>' : filter.subType,
                        operand: window.moment(dateRanges.min).format(momentDateFormat)
                    };
                    rangeMax = {
                        operator: filter.subType === 'Between' ? '<' : filter.subType,
                        operand: window.moment(dateRanges.max).format(momentDateFormat)
                    };

                    if (filter.subType === 'Between') {
                        filter.items.push(rangeMin, rangeMax);
                    } else if (filter.subType === '>' || filter.subType === '>=') {
                        filter.items.push(rangeMin);
                    } else if (filter.subType === '<' || filter.subType === '<=' || filter.subType === '=') {
                        filter.items.push(rangeMax);
                    } else if (filter.subType === ' IS NULL ' || filter.subType === ' IS NOT NULL ') {
                        filter.items = [{
                            operator: filter.subType
                        }];
                    }
                };

                var saveFilter = function() {
                    if ($scope.filterType === 'items') {
                        saveItems();
                    } else if ($scope.filterType === 'range') {
                        saveRange();
                    } else if ($scope.filterType === 'date') {
                        saveDates();
                    }

                    if (filter.items && filter.items.length === 0) {
                        filter = null;
                    }

                    filter = angular.copy(filter);
                    if (!$scope.filterColumn.expression.isNotBasicColumn()) {
                        if ($scope.filterColumn.displayFilter) {
                            $scope.filterColumn.displayFilter = filter;
                        } else {
                            $scope.filterColumn.filter = filter;
                        }
                    } else {
                        if ($scope.filterColumn.filter) {
                            $scope.filterColumn.filter = filter;
                        } else {
                            $scope.filterColumn.displayFilter = filter;
                        }
                    }
                };

                saveFilter();
                $element.closest('[menu-child="nested"]').removeClass('active');
                $scope.$emit('runNewQueries', null);
            };

            $scope.reset = function() {
                $scope.filterColumn.filter = null;
                $scope.filterColumn.displayFilter = null;
                $element.closest('[menu-child="nested"]').removeClass('active');
                $scope.$emit('runNewQueries', null);
            };
        }
    };
} );
