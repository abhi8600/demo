angular.module('visualizer').

directive('filterSummary', function() {
	'use strict';
    return {
        replace: true,
        scope: true,
        templateUrl: 'visualizer/vizMenu/staging/filters/filterSummary/filterSummary.tpl.html',
        link: function(scope, elem, attrs) {
            scope.column = scope.$eval(attrs.filterSummary);
        }
    };
} );
