angular.module('visualizer')
.directive('staging', function (chartStateService, globalStateService, Stage, alertService) {
	'use strict';
	return {
		require: '^?designer',
		replace: true,
		templateUrl: 'visualizer/vizMenu/staging/staging.tpl.html',
		link: function (scope) {
			//console.log(scope);

			scope.dims = _.without(Stage.dimensions, 'HorizontalTrellis', 'VerticalTrellis');
			scope.dataFormatting = {
				decimalPrecision: [{
					key: 'All',
					value: 10 // 10 is a secret number that means do not apply formatting, see underscore_ext.js dataFormatting
				}, {
					key: '1 Decimal',
					value: 1
				}, {
					key: '2 Decimals',
					value: 2
				}, {
					key: '3 Decimals',
					value: 3
				}, {
					key: 'None',
					value: 0
				}],
				currency: ['None', '$', '€', '£', '¥', '₹', '₩', '¤']
			};

			scope.setSeriesType = function (series, type) {
				series.type = type;
			};

			scope.seriesChartTypes = _.without(chartStateService.getNativeChartTypes(), 'table');

			scope.currentChartType = chartStateService.getCurrentChartType();
			scope.$watch(function(){ return chartStateService.getCurrentChartType(); }, function() {
				scope.currentChartType = chartStateService.getCurrentChartType();
			});

			scope.resetDisabled = function() {
				var subjectArea = scope.subjectArea;
				var measuresAndAttributes;

				if ( subjectArea && subjectArea.measures ) {
					measuresAndAttributes = subjectArea.measures.concat(subjectArea.attributes);

					_.each(measuresAndAttributes, function(protoColumn) {
						protoColumn.disabled = false;
					});
				}
			};

			scope.clearStage = function(ignore) {
				var finalReset = function() {
					scope.stage.clear();
					scope.selectChartType('column', false);
					// scope.$broadcast('error', {
					//     recallError: true
					// });
					scope.table.editMode = false;
					alertService.clearAll();
					scope.reportName = null;
					scope.table.results = {};
					chartStateService.setTitle('Type Title');
					chartStateService.resetPreviousDimensionValues();
					chartStateService.reportExpressions = [];
					scope.fileSystem.newFileName = null;
					scope.fileSystem.clearCurrentFile();

					scope.resetDisabled();
					setTimeout(function () {
						//scope.loadAnim('Off');
						if (scope.flags.loaders) {
							scope.$broadcast('loader-all-hide');
						} else {
							scope.loadAnim('Off');
						}
					}, 1001);
				};

				if (!ignore && globalStateService.hasSavePermissions() && scope.fileSystem.hasFileChanged() ) {
					scope.fileSystem.modal = {
						showModal: true,
						title: 'Would you like to save?',
						text: 'Do you want to save the changes you have made to ' + (scope.reportName || 'this unsaved report') + '?',
						successButtonText: 'Save',
						successCallback: function() {
							scope.fileSystem.nextAction = 'reset';
							scope.fileSystem.save();
							scope.fileSystem.closeModal();
						},
						cancelButtonText: 'Don\'t Save',
						cancelCallback: function() {
							scope.fileSystem.closeModal();
							finalReset();
						}
					};
				} else {
					finalReset();
				}
			};

			scope.removeColumn = function(column, chartType) {
				scope.stage.removeColumn(column, chartType);
				if (scope.stage.allColumns().length === 0) {
					scope.showXY = true;
					scope.xDisplayable = true;
					scope.yDisplayable = true;
					scope.guidedMode = false;

					scope.resetDisabled();
				}
				if (!_.find(scope.trashBucket.columns, function(trashColumn) {
					return trashColumn.name === column.name;
				})) {
					scope.stage.removeFilter(column, chartType);
					scope.trashBucket.columns.unshift(column);
				} else {
					scope.trashBucket.columns = _.filter(scope.trashBucket.columns, function(trashColumn) {
						return trashColumn.name !== column.name;
					});
					scope.trashBucket.columns.unshift(column);
				}
			};
			scope.addColumnFromTrash = function(column) {
				//console.log(column)
				scope.stage.addColumn(column, null, chartStateService.getCurrentChartType());
				scope.updateChart();
				scope.trashBucket.columns = _.without(scope.trashBucket.columns, column);
			};

			scope.dragFromStage = function(column) {
				return _.extend(column, { dragSource: 'stage' });
			};

			scope.dragFromTrash = function(column) {
				return _.extend(column, { dragSource: 'trash' });
			};

			scope.dragfromFilters = function(column) {
				return _.extend(column, { dragSource: 'filter' });
			};

			scope.dragSort = function(column) {
				return _.extend(column, { dragSource: 'sort' });
			};

			scope.sortableOptions = {
				update: function() {
					setTimeout(function(){
						scope.stage.updateSortOrder();
						scope.updateChart();
					}, 0);
				}
			};

			scope.titleForDim = function(dim) {
				// Category should be called "Secondary Measure" if it is being used for scatter
				if(dim !== 'Category') {
					return dim;
				}
				if(scope.stage.Category) {
					if(scope.stage.Category.expression.isMeasure()) {
						return "Secondary Measure";
					} else {
						return "Category";
					}
				}
				if(_.contains(['scatter', 'bubble'], chartStateService.getCurrentChartType())) {
					return "Secondary Measure";
				}
				return "Category";
			};
		}
	};
} );
