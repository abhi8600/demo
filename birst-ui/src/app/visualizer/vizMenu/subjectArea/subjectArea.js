angular.module('visualizer')

.directive('subjectAreas', function($filter, subjectAreaWebService, globalStateService, SubjectAreaProcessor, AdhocWebService, chartStateService, columnCreator, alertService) {
	'use strict';
	return {
		require: '?^designer',
		replace: true,
		templateUrl: 'visualizer/vizMenu/subjectArea/subjectArea.tpl.html',

		link: function(scope, elem, attrs, designerCtrl) {
			//call with "On"/"Off" arg to turn whole-screen anim on and off

			subjectAreaWebService.listSubjectAreas(function(subjectAreas) {
				if (subjectAreas && subjectAreas.length > 0) {
					scope.subjectAreas = subjectAreas;
				} else {
					scope.subjectAreas = ['Default Subject Area'];
				}

				scope.selectedSubjectArea = scope.subjectAreas[0];

				scope.safeApply();
			}, angular.noop() );

			scope.createColumn = function(protoColumn) {
				return columnCreator.createFromProto(protoColumn);
			};

			scope.stageColumn = function(protoColumn, dimension, chartType) {
				if (!protoColumn.disabled) {
					var column = scope.createColumn(protoColumn);

					scope.stage.addColumn(column, dimension, chartType || chartStateService.getCurrentChartType());
					scope.updateChart();
					scope.searchVariable.string = '';
					scope.searchGrouped = [
						[],
						[],
						[]
					];
				}

			};

			scope.$watch('subjectAreaConcat', function() {
				if (scope.subjectAreaConcat) {
					scope.reportAttributes = scope.subjectAreaConcat.filter(function(col) {
						return col.type === 'report expression' && col.category === 'attribute';
					});
					scope.measuresAndReportMeasures = _.sortBy(scope.subjectAreaConcat.filter(function(col) {
						return col.type === 'measure' || (col.type === 'report expression' && col.category === 'measure');
					}), function(col) {
						return col.label.toLowerCase();
					});
				}
			}, true);


			function getSubjectAreaContentSuccess(data) {
				var SAP = SubjectAreaProcessor;
				//console.log(JSON.stringify(data));
				SubjectAreaProcessor.reset();

				if (data.children.length) {
					//console.log(JSON.stringify(SAP.process(data, scope.selectedSubjectArea, scope)));
					if (designerCtrl) {
						designerCtrl.setSubjectArea(SAP.process(data, scope.selectedSubjectArea, scope));
					} else {
						scope.subjectArea = SAP.process(data, scope.selectedSubjectArea, scope);
						scope.subjectAreaConcat = [].concat(scope.subjectArea.measures).concat(scope.subjectArea.attributes).concat(scope.subjectArea.uncategorized);
					}
				} else {
					if (designerCtrl) {
						designerCtrl.setSubjectArea(SAP.process(data, scope.selectedSubjectArea, scope));
					} else {
						scope.subjectArea = SAP.process(data, scope.selectedSubjectArea, scope);
						scope.subjectAreaConcat = [].concat(scope.subjectArea.measures).concat(scope.subjectArea.attributes).concat(scope.subjectArea.uncategorized);
					}
					//console.log('warning: no measures/attributes returned for subject area.  likely an unhandled error.');
				}

				if (!designerCtrl) {
					scope.subjectArea = scope.selectedSubjectArea;
					scope.subjectAreaConcat = [].concat(scope.subjectArea.measures).concat(scope.subjectArea.attributes).concat(scope.subjectArea.uncategorized);
					//addFilters(scope, scope.prompts);
				}

				//scope.loadAnim('Off');
				if (scope.flags.loaders) {
					scope.$broadcast('loader-sa-hide');
					scope.keyboardSearch({}); // Trigger search based on user input while the loader was active
				} else {
					scope.loadAnim('Off');
				}
			}

			function getSubjectAreaContentError( /* error */ ) {
				//console.log(error);
				// scope.$broadcast('error', {
				//     basicText: visualizer.misc.errors.ERR_SA_BASIC,
				//     moreInfoText: visualizer.misc.errors.ERR_SA_MORE
				// });
				alertService.alert({
					content: visualizer.misc.errors.ERR_SA_BASIC,
					extra: visualizer.misc.errors.ERR_SA_MORE,
					id: 'error-subjectAreaError'
				});
			}

			function initializeLoader() {
				alertService.alert({
					container: '[alerts-sa]',
					content: visualizer.misc.feedback.FEEDBACK_RENDERING_SA_INITIAL,
					type: 'success',
					id: 'success-rendering-sa',
					duration: 3
				});
				scope.$broadcast('loader-sa-show');
			}

			scope.switchSubjectArea = function(area) {
				setTimeout(function() {
					elem.find('[menu-parent="Subjects"] [menu-child]').removeClass('active');
				}, 0);
				scope.stage.clear();
				scope.$broadcast('blankChart', []);
				scope.lastSubjectArea = scope.selectedSubjectArea;
				scope.selectedSubjectArea = area;
			};

			scope.$watch('selectedSubjectArea', function() {
				if (designerCtrl) {
					//scope.loadAnim('On');
					if (!scope.flags.loaders) {
						scope.loadAnim('On');
					}
				}

				if (!scope.selectedSubjectArea) {
					return;
				}

				subjectAreaWebService.getSubjectAreaContent(scope.selectedSubjectArea, getSubjectAreaContentSuccess, getSubjectAreaContentError);
			}, true);

			if (scope.flags && scope.flags.loaders) {
				// Show loader on initial page load, along with a message:
				initializeLoader();
			}
		}
	};
} )
.directive('reportExpression', function( alertService ) {
	'use strict';
	return {
		scope: true,
		replace: true,
		templateUrl: 'visualizer/vizMenu/subjectArea/reportExpression.tpl.html',
		link: function(scope, elem, attrs) {
			scope.showControls = false;
			scope.showModal = false;
			scope.showDeleteDialog = function() {
				var allUsedColumns = scope.stage.allColumnsAndUnvisualized();
				var isUsedInReport = _.contains(
					_.pluck(allUsedColumns, 'reportExpressionId'),
					scope.column.reportExpressionId
				);
				if (isUsedInReport) {
					// scope.$broadcast('error', {
					//     basicText: "Unable to delete ${0}, because it's being used in the report".replace("${0}", scope.column.label),
					//     moreInfoText: ''
					// });
					alertService.alert({
						container: '[alerts-sa]',
						content: "Unable to delete ${0}, because it's being used in the report".replace("${0}", scope.column.label),
						id: 'warning-unableToDelete',
						type: 'warning',
						duration: 3
					});
				} else {
					scope.showModal = true;
				}
			};
			scope.hideDeleteDialog = function() {
				scope.showModal = false;
			};
			scope.column = scope.$eval(attrs.reportExpression);
			scope.$watch(attrs.reportExpression, function(val) {
				scope.column = val;
			});
		}
	};
});
