/* global describe, it, beforeEach, expect, inject, $httpBackend */

describe('FileSystem', function() {
	'use strict';
    var scope, fsScope, BqlNode;

    beforeEach(module('visualizer'));

	beforeEach( inject( function( _BqlNode_ ) {
		BqlNode = _BqlNode_;
	} ) );

    beforeEach(inject(function($rootScope, $compile, $injector, $templateCache) {

        var $httpBackend = $injector.get('$httpBackend');

        $httpBackend.when('POST', '/SMIWeb/services/SubjectArea.SubjectAreaHttpSoap11Endpoint/').respond("");

        $templateCache.put('partials/designer/subjectArea.html', '<div file-system></div>');

        scope = $rootScope.$new();
        var reportView = $compile("<div report-view></div>")(scope);
        fsScope = reportView.scope();
        scope.$digest();
    }));

    it('should render a v1.0.0 report correctly', function() {
        expect(fsScope.fileSystem).toBeDefined();
        var v100_file = {"version":{"full":"1.0.0","major":"1","minor":"0","dot":"0","codeName":"Pivot Control"},"stage":{"Category":{"name":"Time.Year/Month","expression":{"head":{"type":"attribute","value":{"path":["Time","Time.Year/Month"],"label":"Time.Year/Month","type":"attribute","$$hashKey":"098","bqlExport":"[Time.Year/Month]"}},"children":[]},"properties":{"isFilterOn":true,"visualized":true,"sortDirection":"ASC","type":"VARCHAR"},"uniqueId":"2","$$hashKey":"0G0","export":{"path":["Time","Time.Year/Month"],"label":"Time.Year/Month","type":"attribute","$$hashKey":"098","bqlExport":"[Time.Year/Month]"}},"Color":null,"Shape":null,"Size":null,"HorizontalTrellis":null,"VerticalTrellis":null,"measures":[{"name":"New Axis","series":[{"column":{"name":"Revenue","expression":{"head":{"type":"measure","value":{"path":["Revenue"],"aggregations":["Count distinct","Count","Avg","Max","Min","Sum"],"dateTypes":["By Order_Date","By Forecast_Date","By Load Date","By Inventory_Date","By Ship_Date"],"timeSeriesTypes":["QTD","QAGO","MAGO","YTD","T3M","TTM","YAGO"],"label":"Revenue","type":"measure","$$hashKey":"01U","bqlExport":"[Order_Date: Sum: Revenue]"}},"children":[{"head":{"type":"aggregation","value":"Sum"},"children":[]},{"head":{"type":"dateType","value":"By Order_Date"},"children":[]},{"head":{"type":"timeSeries","value":null},"children":[]}]},"properties":{"isFilterOn":true,"visualized":true,"sortDirection":null,"type":"DOUBLE"},"uniqueId":"1","export":{"path":["Revenue"],"aggregations":["Count distinct","Count","Avg","Max","Min","Sum"],"dateTypes":["By Order_Date","By Forecast_Date","By Load Date","By Inventory_Date","By Ship_Date"],"timeSeriesTypes":["QTD","QAGO","MAGO","YTD","T3M","TTM","YAGO"],"label":"Revenue","type":"measure","$$hashKey":"01U","bqlExport":"[Order_Date: Sum: Revenue]"}},"type":"column","seriesDimensions":{"Color":true,"Shape":true,"Size":true},"colorDistribution":"Stack","$$hashKey":"0BW","min":86758945.2500001,"max":806453506.699993}],"temporary":false,"min":86758945.2500001,"max":806453506.699993,"$$hashKey":"0BT"}],"unused":[],"internalSortOrder":[{"name":"Time.Year/Month","expression":{"head":{"type":"attribute","value":{"path":["Time","Time.Year/Month"],"label":"Time.Year/Month","type":"attribute","$$hashKey":"098","bqlExport":"[Time.Year/Month]"}},"children":[]},"properties":{"isFilterOn":true,"visualized":true,"sortDirection":"ASC","type":"VARCHAR"},"uniqueId":"2","$$hashKey":"0G0","export":{"path":["Time","Time.Year/Month"],"label":"Time.Year/Month","type":"attribute","$$hashKey":"098","bqlExport":"[Time.Year/Month]"}}],"externalSortOrder":[{"name":"Time.Year/Month","expression":{"head":{"type":"attribute","value":{"path":["Time","Time.Year/Month"],"label":"Time.Year/Month","type":"attribute","$$hashKey":"098","bqlExport":"[Time.Year/Month]"}},"children":[]},"properties":{"isFilterOn":true,"visualized":true,"sortDirection":"ASC","type":"VARCHAR"},"uniqueId":"2","$$hashKey":"0G0","export":{"path":["Time","Time.Year/Month"],"label":"Time.Year/Month","type":"attribute","$$hashKey":"098","bqlExport":"[Time.Year/Month]"}}],"colorDistributionControl":true,"colorDistribution":"Stack"},"previousDimensionValues":{"measure":[700467915.390003,607486812.240005,119985556.43,707406693.950001,170273001.689997,121630048.56,99556793.9899997,150176975.779999,402210372.639992,660642037.55,116427037.74,419879641.529994,769103487.220008,795231829.629999,86758945.2500001,560490973.110014,110567762.669999,440919016.450004,162266215.719998,109585573.29,111152461.049999,155032815.929998,131521110.359999,150052521.289998,673495708.37999,424372936.599998,130311374.969999,95888166.0099994,111529210.83,806453506.699993,744926936.919991,125418939.57,168774915.179997,631819090.060004,467646025.129998,430399666.96,673008402.000002,93928718.5499996,116154771.479999,119043180.79,96220304.4300005,448441384.780004,106554647.399999],"Category":["2013/05","2013/04","2011/08","2013/03","2011/06","2011/11","2010/12","2011/03","2012/01","2012/03","2010/07","2012/12","2013/02","2013/06","2010/10","2012/04","2010/05","2012/09","2011/05","2010/01","2010/02","2011/02","2011/12","2011/07","2012/07","2013/01","2011/01","2010/08","2010/03","2013/07","2012/06","2011/09","2011/04","2012/05","2012/08","2012/10","2012/02","2010/11","2011/10","2010/06","2010/09","2012/11","2010/04"]},"formatting":null,"undoStack":[]};
        fsScope.fileSystem.render(JSON.stringify(v100_file));
        expect(fsScope.stage.Category.name).toBe("Time.Year/Month");
        expect(fsScope.stage.Category.expression.head.type).toBe("attribute");
        var measure = fsScope.stage.measures[0].series[0].column;
        expect(measure.expression.head.type).toBe("measure");
        expect(measure.name).toBe("Revenue");
    });

    it('should save a stage correctly', function() {
        var testExp = new BqlNode({type: 'string', value: 'foo'}, []);
        var testCol = {expression: testExp, name: 'fooString', properties:{isFilterOn:false,visualized:true,sortDirection:"ASC",type:"VARCHAR"}};
        fsScope.$broadcast('stageColumn', testCol, 'Category');
        var testCol2 = {expression: testExp, name: 'fooString2', properties:{isFilterOn:false,visualized:true,sortDirection:"ASC",type:"VARCHAR"}};
        fsScope.$broadcast('stageColumn', testCol2, null, 'area');
        var file = fsScope.fileSystem.createFile();
        expect(JSON.parse( JSON.stringify( file.stage.Category ) )).toEqual(
            { expression : { head : { type : 'string', value : 'foo' }, children : [  ], _class: 'BqlNode' }, name : 'fooString', properties : { isFilterOn : true, visualized : true, sortDirection : 'ASC', type : 'VARCHAR' }, uniqueId : '3'}
        );
        expect(JSON.parse( JSON.stringify( file.stage.measures) )).toEqual(
            [ { name : 'New Axis', series : [ { column : { expression : { _class: 'BqlNode', head : { type : 'string', value : 'foo' }, children : [  ] }, name : 'fooString2', properties : { isFilterOn : true, visualized : true, sortDirection : 'ASC', type : 'VARCHAR' }, uniqueId : '4'}, type : 'area', seriesDimensions : { Color : true, Shape : true, Size : true }, colorDistribution : 'Stack' } ], temporary : true, min : 0, max : 0 } ]
        );
    });
});
