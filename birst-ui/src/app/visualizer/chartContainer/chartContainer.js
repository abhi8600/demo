angular.module('visualizer')

.directive('chartContainer', function() {
	'use strict';
    return {
        replace: true,
        templateUrl: 'visualizer/chartContainer/chartContainer.tpl.html'
    };
});
