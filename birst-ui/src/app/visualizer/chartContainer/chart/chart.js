angular.module('visualizer')

.directive('chart', function(chartStateService, chartLayout, $document, globalStateService, alertService) {
	'use strict';
    return {
        controller: function($scope, $element) {

            $scope.currentMousePos = {
                x: -1,
                y: -1
            };

            window.Highcharts.setOptions({
                lang: {
                    numericSymbols: [ " K" , " M" , " B" , " T" , " Q" , " QU"]
                    //numericSymbols: [ " thousand" , " million" , " billion" , " trillion" , " quadrillion" , " quintillion"]
                }
            });

            /* Update re-renders a chart. */

            function update(resultSet) {
                //console.log('update ResultSet', resultSet, resultSet[0], resultSet[0].rows );

                if ($scope.chartType !== 'table' && resultSet && resultSet[0] && resultSet[0].rows.length > 999) {
                    $scope.$broadcast('blankChart', []);

                    // $scope.$broadcast('error', {
                    //     basicText: visualizer.misc.errors.ERR_RESULT_LARGE_NEED_FILTER,
                    //     moreInfoText: ''
                    // });
                    alertService.alert([visualizer.misc.errors.ERR_RESULT_LARGE_NEED_FILTER, 'error-tooLargeResultSet']);

                    $scope.safeApply();

                    //clearTimeout($scope.animTimeout);
                    //loadAnim('Off');
                    return;
                }

                //if we're launching taking the "reportView route", no need to updateChartRequirements
                if (globalStateService.getProductLaunchedAs() !== 'reportView') {
                    $scope.updateChartRequirements(); // would be better to handle from Stage.addColumn etc. but they don't have access to $scope
                }

                function displayErrors() {
                    if ($scope.stage.chartType === 'table') {
                        return;
                    }

                    var unsupportedFilter = "One or more filters in this report are not supported and have not been applied";

                    if ($scope.chartType !== 'table' && !$scope.stage.Category && !$scope.stage.allSeries().length && $scope.stage.allColumns().length) {
                        $scope.$emit('error', {
                            basicText: visualizer.misc.errors.ERR_CANT_CHART_WO_ATTRIB
                        });
                    } else {
                        if ($scope.error && $scope.error.basicText !== unsupportedFilter) {
                            $scope.$emit('error', {
                                recallError: true
                            });
                        }
                    }
                }

                function createVisualizations( /*viewportWidth, viewportHeight*/ ) {
                    var stage = $scope.stage;

                    if ($element.highcharts()) {
                        $element.highcharts().destroy();
                    }

                    if ($scope.chartType === 'table') {
                        $scope.table.renderTable(resultSet);
                    } else {
                        var cl = chartLayout(stage, $scope, resultSet, false, $element.width(), $element.height());
                        $element.empty().append(cl.container);

                        $(window).off('resize.chartResize').on('resize.chartResize', _.throttle(function() {
                            cl.resize($element.width(), $element.height());
                        }, 50));

                        $scope.$on('resize-charts', function() {
                            cl.resize($element.width(), $element.height());
                        });
                    }
                }

                function bindTooltipHandlers() {
                    $element.on('mouseout', '.highcharts-series-group rect, .highcharts-series-group path', function() {
                        $element.closest('[report-view], #chart').find('[tooltip-container]').hide();
                    });

                    $element.on('mouseover', '.highcharts-series-group rect, .highcharts-series-group path', function() {
                        $element.closest('[report-view], #chart').find('[tooltip-container]').show();
                    });

                    $document.mousemove(function(event) {
                        var offsetRatio;
                        // ratio of mouse position to document size - puts the tooltip to right of cursor when towards left of chart, and vice versa.
                        var offsetX, offsetY;
                        var $tooltip = $element.closest('[report-view], #chart').find('[tooltip-container]');

                        if (globalStateService.getProductLaunchedAs() === 'reportView') {
                            offsetX = 0;
                            offsetY = 20;
                            offsetRatio = event.pageX / $document.width();
                            $tooltip.css({
                                'left': event.pageX - $element.closest('[report-view]').offset().left - ($tooltip.width() * offsetRatio),
                                'top': event.pageY - $element.closest('[report-view]').offset().top - offsetY - $tooltip.height()
                            });
                        } else {
                            offsetX = 430;
                            offsetY = 140;
                            offsetRatio = (event.pageX - 430) / ($document.width() - 430);
                            $tooltip.css({
                                'left': event.pageX - offsetX - ($tooltip.width() * offsetRatio),
                                'top': event.pageY - offsetY - $tooltip.height()
                            });
                        }
                    });
                }

                //checkForUnusableColumns();
                displayErrors();
                if (globalStateService.getProductLaunchedAs() !== 'reportView') {
                    createVisualizations();
                    if ($scope.flags.loaders) {
                        $scope.$broadcast('loader-chart-hide');
                    } else {
                        clearTimeout($scope.animTimeout);
                        $scope.loadAnim('Off');
                    }
                } else {
                    //pass IFrame dims to createVisualization
                    var viewportWidth = $(window).width(),
                        viewportHeight = $(window).height();
                    createVisualizations(viewportWidth, viewportHeight);
                }
                bindTooltipHandlers();
            }

            //broadcast from chart editing
            $scope.$on('updateChart', function(event, resultSet) {
                setTimeout(function() {
                    update(resultSet);
                    $scope.safeApply();
                }, 100);
            });

            $scope.$on('resizeChart', update);

            $scope.$on('blankChart', function(e, message) {
                $element.empty();
                if (message === 'no rows' && globalStateService.getProductLaunchedAs() === 'reportView') {
                    $element.append('<div class="no-data">Query returned no data.<br>Try removing some filters.</div>');
                }
            });
        }
    };
} );
