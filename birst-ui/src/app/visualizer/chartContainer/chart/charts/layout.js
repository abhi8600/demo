/* jshint bitwise:false */
angular.module('visualizer')

.factory('chartLayout', function(Embedding, HighchartsBuilder, legendService, chartStateService, globalStateService) {
	'use strict';
    var layout = function(stage, scope, resultSets, inverted, width, height) {

        //trip standard logic and show new highchart containing 1 measure OR 1 category only... see "specialCaseChart.js"
        $('.popline').remove();
        if ((stage.allSeries().length && !_.isObject(stage.Category)) || (stage.allSeries().length === 0 && _.isObject(stage.Category))) {
            return window.specialCaseChart(HighchartsBuilder, stage, resultSets, width, height, scope.errorHandler);
        }
        // end - trip logic

        var titleEnabled = false;//globalStateService.isTitleAndEditingEnabled();

        var scrollContainer = $("<div>").width(width).height(height).css({
            'position': 'absolute',
            'left': 0,
            top: 0,
            overflow: 'auto'
        });

        var container = $("<div>").appendTo(scrollContainer);

        // if (titleEnabled) {
        //     container.addClass('titleEnabled');
        // }

        var embedding = new Embedding(stage, resultSets, inverted, width, height - 50, scope.errorHandler);

        var configs = embedding.generateConfigs(stage, scope.errorHandler);

        /* title input edit code */
        // var titleContainer = $("<div>")
        //     .css({
        //         textAlign: 'center',
        //         'height': 50
        //     })
        //     .addClass('hideUntilHover');

        //titleContainer.appendTo(container);

        // var titleInput;
        // var titleSpan = $("<span>")
        //     .css(chartStateService.getChartFormat('Title'))
        //     .text(chartStateService.getTitle())
        //     .click(function() {
        //         if (titleEnabled) {
        //             titleInput.val(chartStateService.getTitle());
        //             chartStateService.saveOriginalCaption('Title', titleInput.val());

        //             titleInput.css(chartStateService.getChartFormat('Title'));

        //             titleSpan.detach();
        //             titleContainer.append(titleInput);
        //             titleInput.focus();
        //         }
        //     })
        //     .appendTo(titleContainer);

        // var titleBlur = function() {
        //     if (_.isObject(titleInput)) {
        //         titleInput.detach();
        //     }
        //     titleContainer.append(titleSpan);
        //     chartStateService.applyFormattingfromEditor(titleSpan, titleInput.val(), this, 'Title');
        //     chartStateService.setTitle(titleInput.val());
        //     scrollContainer.find("*").off();
        //     scope.updateChart();
        //     scope.safeApply();
        // };

        // titleInput = $("<input class='chart-input title-input'>")
        //     .attr('type', 'text')
        //     .text(chartStateService.getTitle())
        //     .on('blur', titleBlur)
        //     .on('focus', function() {
        //         this.select();
        //     })
        //     .on('keyup keypress', function(e) {
        //         if (e.keyCode === 13) {
        //             titleBlur(e);
        //         }
        //         if (e.keyCode !== 27) {
        //             return; //respond only to ESC clicked
        //         }
        //         titleInput.val(chartStateService.revertOriginalCaption('Title'));
        //         if (_.isObject(titleInput)) {
        //             titleInput.detach();
        //         }
        //         titleContainer.append(titleSpan);
        //         e.stopPropagation();
        //     });

        // $('svg *').on('click', function(e) {
        //     titleBlur(e);

        // });

        // titleContainer.popline({
        //     position: "relative",
        //     className: 'edit-title'
        // }, {
        //     target: titleInput
        // });
        /* end -- title input edit code */

        //check to see if more than 1 series..if true, then see if there are any pies--if there's a pie and more than 1 series, error out
        if (configs[0][0].series.length > 1) {
            _.each(configs[0][0].series, function(element, index) {
                if (configs[0][0].series[index].type === 'pie') {
                    scope.errorHandler({
                        basicText: visualizer.misc.errors.ERR_PIE_COMBINE,
                        moreInfoText: ''
                    });
                    throw new Error('PIE chart cannot be combined with other charts');
                }
            });
        }

        var charts = _.map(configs, function(columnConfigs) {
            var columnContainer = $("<div>")
                .css({
                    display: 'inline-block',
                    position: 'relative'
                })
                .width(width / configs.length)
                .height(height)
                .appendTo(container);
            return _.map(columnConfigs, function(config) {
                return $("<div>")
                    .css({
                        'display': 'block',
                        position: 'relative'
                    })
                    .height(height / columnConfigs.length)
                    .width(width / configs.length)
                    .highcharts(config)
                    .appendTo(columnContainer);
            });
        });


        if (_.flatten(charts).length === 0) {
            return false; //exit if nothing to chart so no blank legend
        }

        var legendContainer = $("<div>").addClass('legend').css({
            textAlign: 'center'
        }).appendTo(container);

        //of pie chart, no need for legend now that category splices are displaying right
        if (stage.allSeries()[0].type === 'pie') {
            legendContainer.css('display', 'none');
        }

        /* xAxis input edit code -- only if allowed based on how prod was launched */
        var xAxisContainer = $("<div>").css({
            textAlign: 'center', paddingBottom: '10px'
        }).insertBefore(legendContainer);

        var xAxisText = _.last(stage.Category.name.split('.')).replace('_', ' ');

        var xAxisInput;
        var xAxisSpan = $("<span>")
            .css(chartStateService.getChartFormat('XAxis'))
            .text(xAxisText)
            .click(function() {
                if (titleEnabled) {
                    chartStateService.saveOriginalCaption('XAxis', xAxisText);
                    xAxisInput.val(xAxisText);
                    xAxisInput.css(chartStateService.getChartFormat('XAxis'));
                    xAxisSpan.detach();
                    xAxisContainer.append(xAxisInput);
                    xAxisInput.focus();
                }
            })
            .appendTo(xAxisContainer);

        var xAxisBlur = function() {
            if (_.isObject(xAxisInput)) {
                xAxisInput.detach();
            }
            xAxisContainer.append(xAxisSpan);
            chartStateService.applyFormattingfromEditor(xAxisSpan, xAxisInput.val(), this, 'XAxis');
            chartStateService.renameColumnInStaging('XAxis', xAxisInput.val(), stage);
            scrollContainer.find("*").off();
            scope.updateChart();
            scope.safeApply();
            //safety valve, ensure editor is hidden in the offchance it is refusing to hide
            $.popline.current.hide();
        };

        xAxisInput = $("<input class='chart-input x-axis-input'>")
            .attr('type', 'text')
            .text(xAxisText)
            .on('blur', xAxisBlur)
            .on('keyup keypress', function(e) {
                if (e.keyCode === 13) {
                    xAxisBlur(e);
                }
                if (e.keyCode !== 27) {
                    return; //respond only to ESC clicked
                }
                xAxisInput.val(chartStateService.revertOriginalCaption('XAxis'));
                if (_.isObject(xAxisInput)) {
                    xAxisInput.detach();
                }
                xAxisContainer.append(xAxisSpan);
                e.stopPropagation();
            })
            .on('focus', function() {
                this.select();
            });

        $('svg *').on('click', function(e) {
            xAxisBlur(e);
        });

        xAxisContainer.popline({
            position: "relative",
            className: 'edit-x-axis'
        }, {
            target: xAxisInput
        });
        /* end -- xAxis input edit code */

        /* YAxis input edit code  --   mothball for now, disable calling*/
        var isOneSerieTypeBarChart = _.some(stage.allSeries(), function(serie) {
            return serie.type === 'bar';
        });

        function createYAxis() {
            $(".highcharts-container .newYaxis").remove();
            $(".highcharts-container div.highcharts-axis span").each(function(index, element) {
                var $el = $(element);
                var $container = $el.closest('.highcharts-container');
                var styles = chartStateService.getChartFormat('YAxis');

                var css = _.extend({}, $el.css(['top', 'left', 'width', 'height']), styles);

                var yAxisContainer = $('<div class="newYaxis"></div>').appendTo($container);

                var html = $el.html();
                var text = ~html.indexOf('<br>') ? html.split('<br>').splice(1).join("    -    ") : $el.text();
                var yAxisSpan = $('<span class="newYaxisSpan"></span>')
                    .css(css)
                    .html(html)
                    .click(function() {
                        if (titleEnabled) {
                            // if merged axis, return
                            var axisText = ~$(this).html().indexOf('<br>') ? $(this).html().split('<br>').splice(1).join("    -    ") : $(this).text();
                            var isMergedAxis = _.find(stage.measures, function(axis) {
                                var map = _.map(axis.series, function(serie) {
                                    return serie.column.name;
                                });
                                return axis.series.length > 1 && map.join("    -    ") === axisText;
                            });

                            if (isMergedAxis) {
                                console.log('merged axis, throw an error?');
                                return;
                            }

                            // save original text, apply current CSS, detach span, attach input, focus
                            chartStateService.saveOriginalCaption('YAxis', axisText, index);
                            yAxisInput.val(text).css(_.extend({}, $el.css(['top', 'left']), chartStateService.getChartFormat('YAxis')));
                            yAxisInput.css('left', _.max([yAxisInput.css('left'), 0]));
                            yAxisSpan.detach();
                            yAxisContainer.append(yAxisInput);
                            yAxisInput.focus();
                        }
                    })
                    .appendTo(yAxisContainer);

                if (!isOneSerieTypeBarChart) {
                    yAxisSpan.addClass('rotate');
                }

                function fixYAxisSpan() {
                    yAxisSpan.css({
                        'top': $el.css('top'),
                        'opacity': 1
                    });
                }

                setTimeout(fixYAxisSpan, 500);
                scope.$on('resize-charts', fixYAxisSpan);

                var blurHandler = function() {

                    // save new text into model, detach input, attach span, update chart
                    var oldName = chartStateService.revertOriginalCaption('YAxis', index);
                    var allSeries, matchingSerie;
                    if (oldName) {
                        allSeries = stage.allSeries();
                        matchingSerie = _.find(allSeries, function(serie) {
                            return _.endsWith(oldName, serie.column.name);
                        });
                    }

                    if (!matchingSerie) {
                        return;
                    } else {
                        matchingSerie.column.name = yAxisInput.val();
                        _.each(scope.stage.unused, function(col) {
                            col.measuresToFilter[yAxisInput.val()] = col.measuresToFilter[oldName];
                            delete col.measuresToFilter[oldName];
                        });
                    }

                    if (_.isObject(yAxisInput)) {
                        try {
                            yAxisInput.detach();
                        } catch (e) {}
                    }
                    yAxisContainer.append(yAxisSpan);

                    chartStateService.applyFormattingfromEditor(yAxisSpan, yAxisInput.val(), yAxisInput[0], 'YAxis');
                    scrollContainer.find("*").off();
                    scope.updateChart();
                    scope.safeApply();
                    //safety valve, ensure editor is hidden in the offchance it is refusing to hide
                    $.popline.current.hide();


                    //turn off svg click event -- needed to put aggressive timer because otherwise a click event registers right after "off" so off never sticks
                    setTimeout(function() {
                        $('svg *').off();
                    }, 1000);
                };

                var yAxisInput = $('<input class="chart-input y-axis-input y-' + index + '">')
                    .attr('type', 'text')
                    .val(text)
                    .on('blur', blurHandler)
                    .on('keyup', function(e) {
                        if (e.keyCode === 13) {
                            blurHandler(e);
                        }
                        if (e.keyCode !== 27) {
                            return; //respond only to ESC clicked
                        }
                        yAxisInput.val(chartStateService.revertOriginalCaption('YAxis', index));
                        if (_.isObject(yAxisInput)) {
                            try {
                                yAxisInput.detach();
                            } catch (e) {}
                        }
                        yAxisContainer.append(yAxisSpan);
                        e.stopPropagation();
                    })
                    .on('focus', function() {
                        this.select();
                    });

                $('svg *').on('click', function(e) {
                    blurHandler(e);

                });

                $container.popline({
                    position: 'relative',
                    className: 'edit-y-axis-' + index
                }, {
                    target: yAxisInput
                });
            });
        }
        /* end -- YAxis input edit code */

        var setVisibility = function(tag, isVisible) {
            var tagVal = JSON.parse(tag);
            if (tagVal[0] !== 'series') {
                stage[tagVal[0]].displayFiltered = stage[tagVal[0]].displayFiltered || [];
                if (isVisible) {
                    stage[tagVal[0]].displayFiltered = _.without(stage[tagVal[0]].displayFiltered, tagVal[1]);
                } else {
                    stage[tagVal[0]].displayFiltered.push(tagVal[1]);
                }
            }
            _.each(_.flatten(charts), function(chart) {
                embedding.setVisibility(tag, isVisible, chart.highcharts());
            });
        };

        var setHighlight = function(tag, isHighlit) {
            _.each(_.flatten(charts), function(chart) {
                embedding.setHighlight(tag, isHighlit, chart.highcharts());
            });
        };

        _.each(legendService(embedding.dimensionValues, embedding.previousDimensionValues, stage, setVisibility, setHighlight), function(legend) {
            legend.appendTo(legendContainer);
        });

        function clearLegendCollapse(legend_element){
            $(legend_element).removeClass('collapsed');
            $(legend_element).children().removeClass('legend-hidden-item');
            $(legend_element).off('click', '.legend-collapse-toggle');
        }

        function initializeLegendCollapse(legend_element){
            var toggle = $('.legend-collapse-toggle', legend_element);
            var title = $('.legend-title', legend_element);
            var items = $('.legend-item', legend_element);
            var dotdotdot = $('.legend-dotdotdot', legend_element);

            $(legend_element).children().hide();
            $(toggle).css('display','inline-block');
            $(title).css('display','inline-block');
            $(dotdotdot).css('display','inline-block');
            var initialHeight = $(legend_element).height();
            var collapse = false;

            _.each(items, function(item, key, list){
                $(item).css('display','inline-block');
                if ($(legend_element).height() > initialHeight){
                    $(item).addClass("legend-hidden-item");
                    if (collapse === false){
                        $(item).prev().addClass("legend-hidden-item");
                        collapse = true;
                    }
                }
            });
            if (collapse){
                $(legend_element).addClass('collapsed');
                //$('.legend-hidden-item', legend_element).hide();
                $(legend_element).on('click', '.legend-collapse-toggle', function(){
                    if ($(legend_element).hasClass('collapsed')){
                        $(legend_element).removeClass('collapsed');
                    } else {
                        $(legend_element).addClass('collapsed');
                    }
                });
            } else {
                $(toggle).hide();
                $(dotdotdot).hide();
            }
        }

        function resize(newWidth, newHeight) {
            // console.log('layout.js resizing to ', newWidth, newHeight);

            var padding = 15;

            scrollContainer.css({width: newWidth + 'px', height: newHeight + 'px', padding: padding + 'px'});

            //container.css({padding: padding + 'px'});

            //Collapsing Legends
            _.each($(legendContainer).children(), function(value, key, list){
                clearLegendCollapse(value);
                initializeLegendCollapse(value);
            });

            // allow space for scrollbars
            newWidth -= 15 + padding * 2;
            newHeight -= 15 + padding * 2;

            //console.log('width after room for scrollbars ', newWidth, newHeight);

            var totalMargin = _.sum(_.map(charts, function(column) {
                return column[0].highcharts().chartWidth - column[0].highcharts().plotWidth;
            }));

            totalMargin = parseInt(totalMargin, 10);

            var totalVerticalMargin = _.sum(_.map(charts[0], function(chart) {
                return chart.highcharts().chartHeight - chart.highcharts().plotHeight;
            }));

            totalVerticalMargin = parseInt(totalVerticalMargin, 10);

            var minWidth = charts[0][0].highcharts().xAxis[0].type === 'categories' ?
                charts[0][0].highcharts().xAxis[0].categories.length * 12 : 100;

            var minPlotWidth = Math.max(100, minWidth);

            var plotWidth = Math.max(minPlotWidth, (newWidth - totalMargin) / charts.length);

            var other_content_height = legendContainer.outerHeight() + xAxisContainer.outerHeight() + 10 /*+ titleContainer.outerHeight() */ + totalVerticalMargin;
            var plotHeight = Math.max(100, (newHeight - other_content_height) / charts[0].length);
            var chartHeight = plotHeight * charts[0].length + totalVerticalMargin;
            container.width(plotWidth*charts.length + totalMargin);

            _.each(charts, function(column, ii) {
                _.each(column, function(chart, jj) {
                    setTimeout(function() {
                        var hc = chart.highcharts();
                        var w = plotWidth + hc.chartWidth - hc.plotWidth;
                        //console.log('width = ', plotWidth, hc.chartWidth, hc.plotWidth)
                        var h = plotHeight + hc.chartHeight - hc.plotHeight;

                        if (jj === 0 && charts.length > 2) {
                            //truncate each chart's title allowing 15 pixels per char
                            var trellisTitle = embedding.dimensionValues.HorizontalTrellis[ii];
                            var maxTitleLength = Math.floor(w / 15);

                            if (trellisTitle.length > maxTitleLength) {
                                trellisTitle = trellisTitle.substr(0, maxTitleLength - 3) + "...";
                            }
                            hc.setTitle({
                                text: trellisTitle
                            });
                        }
                        chart.parent().width(w).height(chartHeight);
                        chart.width(w).height(h);
                        //console.log('setting size: ', w, h);
                        hc.setSize(w, h);
                    }, 0);

                });
            });
        }

        // Resize after container is added to DOM
        setTimeout(function() {
            resize(width, height);
            resize(width, height);
            createYAxis();
        }, 0);

        return {
            container: scrollContainer,
            resize: resize
        };
    };
    return layout;
} );
