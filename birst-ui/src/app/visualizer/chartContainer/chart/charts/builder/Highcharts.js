angular.module('visualizer')

.factory('HighchartsBuilder', function (chartStateService) {
	'use strict';
    function Builder() {
        this._inverted = false;
        this.xAxis = {};
        this.yAxes = [];
        this.series = [];

        this.linearSeriesCount = 0;
        this.columnarSeriesCount = 0;
        this.areaSeriesCount = 0;
        this.otherSeriesCount = 0;

        this.taggedSeries = {};
        this.stackNumber = 1;
    }

    // private method - interface only exposes more specific methods, addColumnSeries etc.

    function addSeriesOnAxis(builder, series, axis) {
        if (series.type === 'line' || series.type === 'spline') { // line above column, area
            builder.linearSeriesCount += 1;
            series.index = builder.linearSeriesCount * 100;
        } else if (series.type === 'column') { // column above area
            builder.columnarSeriesCount += 1;
            series.index = builder.columnarSeriesCount * 10;
        } else if (series.type === 'area' || series.type === 'areaspline') { // area furthest back
            builder.areaSeriesCount += 1;
            series.index = builder.areaSeriesCount * 1;
        } else { // points, scatter, bubbles most on top
            builder.otherSeriesCount += 1;
            series.index = builder.otherSeriesCount * 1000;
        }

        var axisNumber = _.indexOf(builder.yAxes, axis);
        //console.log(builder, axis, axisNumber)
        window.assert(axisNumber >= 0, "Axis passed into addSeriesOnAxis should be present already in builder");
        series.yAxis = axisNumber;

        builder.series.push(series);
    }

    // Note: showTitle and xAxisLabel are only passed in by specialCaseChart.js to handle 1M or 1C charts
    Builder.prototype = {
        xCategories: function(categories, isTrellised, showTitle, xAxisLabel, showLabels, errorHandler) {


            //prevent null categories from plotting
            categories = _.without(categories, null);

            if (!_.isArray(categories)) {
                errorHandler({
                    basicText: visualizer.misc.errors.ERR_NO_RESULT_MOD_SELECTION,
                    moreInfoText: ''
                });
                categories = [];
            }

            window.assert.isArray(categories);

            var rotation = 0,
                margin = -30; //angle to rotate label text: if trellis, make it 90, if nStep>1, make it 45, otherwise leave alone

            var nStep = Math.ceil(categories.length / 10);

            var xAxisClassString = 'x-axis';

            if (isTrellised || nStep > 1) {
                xAxisClassString += ' vertical-text';
            } else if (nStep > 1) {
                xAxisClassString += ' vertical-text';
            }

            if (isTrellised) {
                rotation = -45;
                margin = -20;
            } else if (nStep > 1) {
                rotation = -45;
                margin = -20;
            }

            var title;
            //normally hide axis titles with exception being special charts i.e. 1 M or 1 C
            if (!showTitle) {
                showTitle = false;
                title = '';
            } else if (showTitle === true) {
                title = xAxisLabel + ' (displaying sample of ' + categories.length + ' items)';
            }

            this.xAxis = {
                title: {
                    enabled: showTitle,
                    text: title,
                    margin: margin
                },
                categories: categories,
                labels: {
                    formatter: function() {
                        var index = _.indexOf(categories, this.value);
                        var val = _.htmlEscape(this.value);
                        if (nStep === 1 || index % nStep === 0) {
                            if (val.length > 14) {
                                return "<span class='" + xAxisClassString + "' title=\"" + val + "\">" + val.substring(0, 14) + "..." + "</span>";
                            } else {
                                return "<span class='" + xAxisClassString + "'>" + val + "</span>";
                            }
                        } else {
                            return "<span class='" + xAxisClassString + "'></span>";
                        }
                    },
                    overflow: 'justify',
                    enabled: showLabels,
                    rotation: rotation,
                    step: nStep
                },
                type: 'categories',
                showLastLabel: true,
                startOnTick: true,
            };
        },

        xLinear: function(showLabels) {
            this.xAxis = {
                title: {
                    enabled: false,
                    text: ''
                },
                min: 0,
                labels: {
                    enabled: showLabels
                },
                showLastLabel: true,
                startOnTick: true,
                type: 'linear'
            };
        },

        addYAxis: function(title, enabled) {
            window.assert.isString(title);

            var yAxis = {
                title: {
                    enabled: enabled,
                    text: title,
                    useHTML: true,
                    style: _.extend({}, chartStateService.getChartFormat('YAxis'), {
                        color: '#fff'
                    })
                },
                labels: {
                    enabled: enabled
                },
                gridLineColor: '#eee',
                showLastLabel: true,
                min: 0
            };

            this.yAxes.push(yAxis);

            return yAxis;
        },

        // make Highcharts not show the y axis

        addYAxisNoMax: function( /*title, enabled*/ ) {
            var yAxis = {
                min: 100,
                max: 0,
                title: {
                    text: ''
                }
            };
            this.yAxes.push(yAxis);
            return yAxis;
        },


        addColumnSeriesOnAxis: function(name, type, groups, axis) {
            addSeriesOnAxis(this, {
                name: name,
                type: type,
                groups: groups
            }, axis);
        },

        addLinearSeriesOnAxis: function(name, type, color, yValues, axis, tags) {
            addSeriesOnAxis(this, {
                name: name,
                type: type,
                color: color,
                yValues: yValues,
                tags: tags
            }, axis);
        },

        addScatterSeriesOnAxis: function(name, points, axis, tags) {
            addSeriesOnAxis(this, {
                name: name,
                type: 'scatter',
                points: points,
                tags: tags
            }, axis);
        },

        // addBubbleSeriesOnAxis: function(name, points, axis, tags) {
        //     addSeriesOnAxis(this, {name: name, type: 'bubble', points: points, tags: tags}, axis);
        // },

        inverted: function(value) {
            window.assert.isBoolean(value);
            this._inverted = value;
        },

        height: function(value) {
            window.assert.isNumber(value);
            this._height = value;
        },

        width: function(value) {
            window.assert.isNumber(value);
            this._width = value;
        },

        title: function(value) {
            window.assert.isString(value);
            this._title = value;
        },

        turnTooltipOff: function() {
            this._tooltip_enabled = false;
        },

        seriesNumbersForTag: function(tag) {
            return _.map(this.taggedSeries[tag], function(highchartsSerie) {
                return _.indexOf(this.highchartsSeries, highchartsSerie);
            }, this);
        },

        numberOfLineCharts: function(series) {
            var lineCharts = 0;

            _.each(series, function(serie) {
                if (serie.type === 'spline' || serie.type === 'line') {
                    ++lineCharts;
                }
            }, this);

            return lineCharts;
        },

        generateConfig: function(stage) {
            // with a line broken down by color it was stacking, which yMax doesnt take into account so
            // it was causing lines to run off the top of the graph...
            // series: stacking normal ... lines stack as I would expect..
            // the main problem is the runoff the top..
            // and also we normally want lines not to stack...
            var that = this;
            var generateSeries = this.generateSeries(stage);

            var config = {
                chart: {
                    height: this._height,
                    width: this._width,
                    spacingBottom: 10,
                    spacingTop: 20,
                    spacingLeft: 0,
                    spacingRight: 0
                },
                title: {
                    text: this._title
                },
                xAxis: this.xAxis,
                yAxis: this.yAxes,
                tooltip: { //note: this tooltip looks pretty for column, line, area and trelli based on those -- not scatter or stacked
                    shared: false,
                    useHTML: true,

                    formatter: generateSeries[0].tooltip(),
                    /*
                    headerFormat: '<small><u>{point.key}</u></small><table>',
                    pointFormat: '<tr><td>{series.name}: </td>' +
                        '<td style="text-align: right"><b>{point.y}</b></td></tr>',
                    footerFormat: '</table>',
                    */
                    valueDecimals: 0,
                    enabled: this._tooltip_enabled === false ? false : true,
                    scatter: false
                },
                plotOptions: {
                    scatter: {
                        tooltip: { //note: this tooltip looks pretty for column, line, area and trelli based on those -- not scatter or stacked
                            shared: false,
                            useHTML: true,
                            formatter: function() {
                                return '<small><u>' + that.yAxes[this.series.yAxis].name + ': {point.key}</u></small><table>';
                            },
                            //                                headerFormat: '',
                            //                                pointFormat: '',
                            //                                footerFormat: '',
                            /*                                headerFormat: '<small><u>{ chart.yAxis[this.series.yAxis].name }: {point.key}</u></small><table>',
                            pointFormat: '<tr><td>{series.name}: </td>' +
                                '<td style="text-align: right"><b>{point.y}</b></td></tr>',
                            footerFormat: '</table>',*/
                            valueDecimals: 0,
                            enabled: this._tooltip_enabled === false ? false : true
                        }
                    },
                    column: {
                        stacking: this._stackType
                    },
                    bar: {
                        stacking: this._stackType
                    },
                    area: {
                        stacking: this._stackType
                    },
                    areaspline: {
                        stacking: this._stackType
                    },
                    series: {
                        lineWidth: 4, // this linewidth overrides line and spline linewidth
                        cursor: 'pointer',
                        marker: {
                            lineWidth: 1
                        }
                    },
                    line: {

                    },
                    spline: {

                    },
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: true,
                            color: '#000000',
                            connectorColor: '#000000',
                            format: '<b>{point.x}</b>: {point.percentage:.1f} %'
                        }
                    }
                },
                credits: {
                    enabled: false
                },
                legend: {
                    enabled: false
                },
                series: generateSeries,
                colors: chartStateService.getColorPalette()
            };

            //console.log('hcconfig, ', config);

            // calculate number of line charts

            var numberOfLineCharts = this.numberOfLineCharts(config.series);

            //console.log('numberOfLineCharts', numberOfLineCharts);

            var lineWidth;

            if (numberOfLineCharts < 7) {
                lineWidth = 5;
            } else if (numberOfLineCharts < 13) {
                lineWidth = 3;
            } else {
                lineWidth = 1;
            }

            config.plotOptions.series.lineWidth = lineWidth;

            this.highchartsSeries = _.map(config.series, function(x) {
                return x;
            });

            return config;
        },

        generateSeries: function(stage) {
            var self = this;
            // Each logical series may become multiple highcharts series objects, i.e. in the case of column.
            // Use concatMap to join together the list of highcharts series for each logical series.
            var generate = _.map(
                _.concatMap(this.series, function(serie, i) {
                    return self.highchartsSeries(serie, i);
                }),
                function(serie, j) {
                    serie.originalOrdering = j;
                    serie.tooltip = function() {
                        return function() {
                            var xAxisTitle = stage.Category.name.split('.')[stage.Category.name.split('.').length - 1].replace('_', ' ');
                            xAxisTitle = _.escape(xAxisTitle);
                            var additionalRows = '';
                            var isMissing;
                            var isMissingStyle = 'font-style: italic;';
                            var getProp = function(dim, key) {
                                if (_.isString(dim)) {
                                    return stage[dim].properties[key];
                                } else if (_.isNumber(dim)) {
                                    return stage.allSeries()[dim].column.properties[key];
                                }
                            };

                            if (stage.Color) {
                                var colorName = stage.Color.name.split('.')[stage.Color.name.split('.').length - 1].replace('_', ' ');
                                var colorValue = _.isNumber(this.point.colorValue) ? _.dataFormat(this.point.colorValue, getProp('Color', 'currency'), getProp('Color', 'decimalPrecision')) : this.point.colorValue;
                                colorName = _.escape(colorName);
                                colorValue = _.escape(colorValue);
                                isMissing = colorValue === '(is missing)' ? isMissingStyle : '';
                                additionalRows += '<tr><td>' + colorName + ': </td><td style="text-align: right;' + isMissing + '"><b>' + colorValue + '</b></td></tr>';
                            }

                            if (stage.Color && stage.getColorGrouping() === 'Percent') {
                                var groupingName = 'Percentage';
                                var groupingValue = '' + window.Highcharts.numberFormat(this.point.percentage, 1) + '%';
                                isMissing = groupingValue === '(is missing)' ? isMissingStyle : '';
                                additionalRows += '<tr><td>' + groupingName + ': </td><td style="text-align: right;' + isMissing + '"><b>' + groupingValue + '</b></td></tr>';
                            }

                            if (stage.Shape) {
                                var shapeName = stage.Shape.name.split('.')[stage.Shape.name.split('.').length - 1].replace('_', ' ');
                                shapeName = _.escape(shapeName);
                                isMissing = this.point.shapeValue === '(is missing)' ? isMissingStyle : '';
                                additionalRows += '<tr><td>' + shapeName + ': </td><td style="text-align: right;' + isMissing + '"><b>' + this.point.shapeValue + '</b></td></tr>';
                            }

                            if (stage.Size) {
                                var sizeName = stage.Size.name.split('.')[stage.Size.name.split('.').length - 1].replace('_', ' ');
                                sizeName = _.escape(sizeName);
                                isMissing = this.point.sizeValue === '(is missing)' ? isMissingStyle : '';
                                additionalRows += '<tr><td>' + sizeName + ': </td><td style="text-align: right;' + isMissing + '"><b>' +
                                    _.dataFormat(this.point.sizeValue, getProp('Size', 'currency'), getProp('Size', 'decimalPrecision')) + '</b></td></tr>';
                            }

                            if (stage.HorizontalTrellis) {
                                var hTrellisName = stage.HorizontalTrellis.name.split('.')[stage.HorizontalTrellis.name.split('.').length - 1].replace('_', ' ');
                                hTrellisName = _.escape(hTrellisName);
                                isMissing = this.point.hTrellisValue === '(is missing)' ? isMissingStyle : '';
                                additionalRows += '<tr><td>' + hTrellisName + ': </td><td style="text-align: right;' + isMissing + '"><b>' + this.point.hTrellisValue + '</b></td></tr>';
                            }

                            if (stage.VerticalTrellis) {
                                var vTrellisName = stage.VerticalTrellis.name.split('.')[stage.VerticalTrellis.name.split('.').length - 1].replace('_', ' ');
                                vTrellisName = _.escape(vTrellisName);
                                isMissing = this.point.vTrellisValue === '(is missing)' ? isMissingStyle : '';
                                additionalRows += '<tr><td>' + vTrellisName + ': </td><td style="text-align: right;' + isMissing + '"><b>' + this.point.vTrellisValue + '</b></td></tr>';
                            }

                            var number;

                            number = _.dataFormat(this.point.y, getProp(j, 'currency'), getProp(j, 'decimalPrecision'));

                            isMissing = _.escape((this.point.category === undefined ? this.point.x : this.point.category)) === '(is missing)' ? isMissingStyle : '';
                            var xAxisTooltip = '<tr><td>' + xAxisTitle + ': </td><td style="text-align: right;' + isMissing + '"><b>' + _.escape((this.point.category === undefined ? this.point.x : this.point.category)) + '</b></td></tr>';
                            isMissing = number === '(is missing)' ? isMissingStyle : '';
                            var seriesTooltip = '<tr><td>' + this.series.name + ': </td><td style="text-align: right;' + isMissing + '"><b>' + number + '</b></td></tr>';

                            var tooltipHTML = '<div tooltip><table>' +
                                xAxisTooltip +
                                seriesTooltip +
                                additionalRows +
                                '</table></div>';

                            $('[tooltip-container]').html(tooltipHTML);

                            return;
                        };
                    };
                    return serie;
                });
            return generate;
        },

        highchartsSeries: function(serie, serieNumber) {
            switch (serie.type) {
            case 'pie':
                return this.pieHighchartsSeries(serie, serieNumber);
            case 'bar':
            case 'column':
                return this.columnHighchartsSeries(serie, serieNumber);
            case 'scatter':
            case 'points':
            case 'bubble':
                return [this.scatterHighchartsSeries(serie, serieNumber)];
                // case 'bubble':
                //     return [this.bubbleHighchartsSeries(serie, serieNumber)];
            default:
                return [this.linearHighchartsSeries(serie, serieNumber)];
            }
        },

        tagSeries: function(serie, highchartsSerie) {
            _.each(serie.tags, function(tag) {
                this.taggedSeries[tag] = this.taggedSeries[tag] || [];
                this.taggedSeries[tag].push(highchartsSerie);
            }, this);
        },

        pieHighchartsSeries: function(serie) {
            window.assert(this.xAxis.categories.length === serie.yValues.length, "Incorrect number of categories or yValues");

            var data = serie.yValues;

            _.each(data, function(datum, i) {
                data[i].x = this.xAxis.categories[i];
            }, this);

            var highchartsSerie = {
                type: serie.type,
                name: serie.name,
                yAxis: serie.yAxis,
                data: data //_.zip(this.xAxis.categories, serie.yValues)
            };

            this.tagSeries(serie, highchartsSerie);

            return highchartsSerie;
        },

        // currently we do not support line/spline/area/areaspline stacking

        linearHighchartsSeries: function(serie) {
            var color = serie.color;

            //console.log('linearHighchartsSeries yValues', serie.yValues);

            var highchartsSerie = {
                type: serie.type,
                name: serie.name,
                yAxis: serie.yAxis,
                color: color,
                data: serie.yValues,
                index: serie.index
            };

            this.tagSeries(serie, highchartsSerie);

            return highchartsSerie;
        },

        scatterHighchartsSeries: function(serie) {
            var defaultSize = 8;
            var defaultShape = 'circle';
            var highchartsSerie = {
                type: 'scatter',
                lineWidth: 0,
                name: serie.name,
                yAxis: serie.yAxis,
                data: _.map(serie.points, function(point) {
                    var x = this.xAxis.type === 'categories' ?
                        _.indexOf(this.xAxis.categories, point.x) :
                        point.x;
                    return {
                        x: x,
                        y: point.y,

                        colorValue: point.colorValue,
                        sizeValue: point.sizeValue,
                        shapeValue: point.shapeValue,
                        hTrellisValue: point.hTrellisValue,
                        vTrellisValue: point.vTrellisValue,

                        marker: {
                            lineColor: point.color,
                            lineWidth: point.negative ? 3 : 0,
                            fillColor: point.negative ? 'rgba(0,0,0,0)' : point.color,
                            radius: point.size || defaultSize,
                            symbol: point.shape || defaultShape,
                            states: {
                                hover: {
                                    lineColor: '#000',
                                    fillColor: '#000',
                                    lineWidth: point.negative ? 3 : 0,
                                    radius: point.size || defaultSize,
                                    symbol: point.shape || defaultShape
                                }
                            }
                        }
                    };
                }, this),
                index: serie.index
            };

            if (this.xAxis.type === 'categories') {
                // Highcharts will elide x values which don't have a point in the series, so add in
                // null points here.
                var missingPoints = _.difference(this.xAxis.categories, _.pluck(serie.points, 'x'));
                highchartsSerie.data = highchartsSerie.data.concat(_.map(missingPoints, function(xVal) {
                    return {
                        x: _.indexOf(this.xAxis.categories, xVal),
                        y: null
                    };
                }, this));
            }

            this.tagSeries(serie, highchartsSerie);

            return highchartsSerie;
        },

        nextStackNumber: function() {
            return this.stackNumber++;
        },

        /*
        Due to limitations of Highcharts, if we stack by percent any series of a given type (line, bar, column, etc.) then
         all other series of the same type must also be stacked by percent.

        Similarly, if we stack any series of a given type, we must explicitly set a different stack index for any
        series we do not want to have stacked (Jason already does this) in order to avoid it being stacked with the other
        series of the same type.
        */

        columnHighchartsSeries: function(serie) {
            if (serie.groups.length === 0) {
                return [];
            }

            var self = this;

            // We keep the columns in the format groups > stacks >
            // points. But highcharts wants each series to cut across all
            // groups, and uses a number to keep track of which series are
            // on the same stack. So we need invert our structure. We go
            // through the first group, and for each point in that group
            // we bring it together with the corresponding points from
            // other groups.

            // .stacks is not assigned for line, spline, area, areaspline...
            // need a UI control to set it, then need some kind of representation in Stage
            // of whether a series is stacked or not and then send it through to Highcharts.js
            // in H.js then linearhighchartseries generates
            // ... rather in embedding it generates multiple series by attribute, so give each
            // of those series the same stacking number... then you get stacked, or diff
            // stacking numbers to get individual

            return _.concatMap(serie.groups[0].stacks, function(stack, i) {
                var stackNumber = self.nextStackNumber();

                return _.map(stack.points, function(point, j) {
                    var highchartsSerie = {
                        type: serie.type,
                        name: serie.name,
                        yAxis: serie.yAxis,
                        stack: stackNumber,
                        data: _.map(serie.groups, function(group) {
                            var point = group.stacks[i].points[j];

                            return {
                                color: point.color,
                                colorValue: point.colorValue,
                                hTrellisValue: point.hTrellisValue,
                                vTrellisValue: point.vTrellisValue,
                                y: point.y
                            };
                        }),
                        index: serie.index
                    };

                    _.each(point.tags, function(tag) {
                        self.taggedSeries[tag] = self.taggedSeries[tag] || [];
                        self.taggedSeries[tag].push(highchartsSerie);
                    }, this);

                    return highchartsSerie;
                });
            });
        },

        yAxisMax: function(axis) {
            return Math.max(0, this.yAxisMaxOrMin(axis, _.max));
        },

        yAxisMin: function(axis) {
            return Math.min(0, this.yAxisMaxOrMin(axis, _.min));
        },

        yAxisMaxOrMin: function(axis, aggregation) {
            var axisNumber = _.indexOf(this.yAxes, axis);
            var axisSeries = _.filter(this.series, function(serie) {
                return serie.yAxis === axisNumber;
            });
            var stackedTypes = ['area', 'areaspline'];
            var stackedSeries = _.filter(axisSeries, function(serie) {
                return _.contains(stackedTypes, serie.type);
            });
            var stackedYValues = _.zip.apply(_, _.pluck(stackedSeries, 'yValues'));
            var stackedSums = _.map(stackedYValues, function(yVals) {
                return _.sum(_.pluck(yVals, 'y'));
            });
            var stackedMax = aggregation(stackedSums);
            return aggregation([stackedMax,
                aggregation(_.map(axisSeries, function(serie) {
                    return this.seriesMaxOrMin(serie, aggregation);
                }, this))
            ]);
        },

        seriesMaxOrMin: function(series, aggregation) {
            switch (series.type) {
                case 'points':
                case 'bubble':
                    // 1.05 gives a little extra space for large symbols
                case 'scatter':
                    return 1.05 * aggregation(_.compact(_.pluck(series.points, 'y')));

                case 'bar':
                case 'column':
                    return aggregation(_.map(series.groups, function(group) {
                        return aggregation(_.map(group.stacks, function(stack) {
                            return _.sum(_.compact(_.pluck(stack.points, 'y')));
                        }));
                    }));

                case 'line':
                case 'spline':
                case 'area':
                case 'areaspline':
                case 'pie':
                    return aggregation(_.compact(_.pluck(series.yValues, 'y')));
                default:
                    return aggregation(_.compact(series.yValues));
            }
        }
    };

    return Builder;

} );
