// visualizer.legends returns an array of legends, one for each of the visual variables used by the chart,
// each of which can be a "legend" (visual variable legend) or a "seriesLegend"

angular.module("visualizer").factory('legendService', function(chartStateService) {
    'use strict';
    function colorLegends(series, dimensionValues, previousDimensionValues, stage, setVisibility, setHighlight) {
        var usedGradients = 0;
        var colorIsMeasure = stage.Color && stage.Color.expression.isMeasure();

        var legends = [];
        var colorlessSeries = [];
        _.each(series, function(serieColumns, ii) {
            var seriesName = stage.allValidSeries()[ii].column.name;
            if (_.contains(_.pluck(serieColumns, 'dim'), 'Color')) {
                var legendLabel = stage.Color.name + " (" + seriesName + "): ";
                if (colorIsMeasure) {
                    legends.push(
                        legendContainer(legendLabel, gradientLegend('Color', _.max(_.map(dimensionValues.Color, Math.abs)), usedGradients))
                    );
                    usedGradients++;
                } else {
                    legends.push(
                        legendContainer(legendLabel, categoryLegend('Color', dimensionValues.Color, previousDimensionValues.Color, series.length, ii, setVisibility, setHighlight, stage.Color.displayFiltered || []))
                    );
                }
            } else {
                colorlessSeries.push({
                    number: ii,
                    name: seriesName
                });
            }
        });
        if (colorlessSeries.length) {
            // Color is not being used as a dimension, so we should show a series key.
            legends.push(seriesLegend(colorlessSeries, series.length, setVisibility, setHighlight));
        }
        return legends;
    }

    function shapeLegends(series, dimensionValues, previousDimensionValues, stage, setVisibility, setHighlight) {
        if (!dimensionValues.Shape) {
            return [];
        }

        var legends = [];
        _.each(series, function(serieColumns, ii) {
            var seriesName = stage.allValidSeries()[ii].column.name;
            if (_.contains(_.pluck(serieColumns, 'dim'), 'Shape')) {
                var legendLabel = stage.Shape.name + " (" + seriesName + "): ";
                legends.push(
                    legendContainer(legendLabel, categoryLegend('Shape', dimensionValues.Shape, previousDimensionValues.Shape, series.length, ii, setVisibility, setHighlight, stage.Shape.displayFiltered || []))
                );
            }
        });
        return legends;
    }

    function sizeLegends(dimensionValues, stage) {
        if (!dimensionValues.Size) {
            return [];
        } else {
            return [legendContainer(
                stage.Size.name + ":",
                gradientLegend('Size', _.max(_.map(dimensionValues.Size, Math.abs)))
            )];
        }
    }

    // A legend for a data series.  For example, "Series 1: (blue), Series 2: (green)"

    function seriesLegend(colorlessSeries, totalSeries, setVisibility, setHighlight) {
        var icons = _.map(colorlessSeries, function(serie) {
            return colorIcon(chartStateService.getColorForSeries(totalSeries, serie.number, 0));
        });
        var seriesIcons = _.map(icons, function(icon, ii) {
            var seriesNumber = colorlessSeries[ii].number;
            return seriesVisibilityToggle(JSON.stringify(["series", seriesNumber]), setVisibility, setHighlight, true,
                $("<div>").addClass('legend-item').append(icon, getLegendTitleTag(colorlessSeries[ii].name)));
        });
        var contents = $("<div>");
        _.each(seriesIcons, function(icon) {
            contents.append(icon);
        });
        return legendContainer("Series: ", contents.children());
    }

    // legend for a color gradient

    function gradientLegend(dim, maxVal, gradientNumber) {
        // var width = 120;
        // var numbers = $("<div>").css({
        //     'position': 'relative',
        //     textAlign: 'left'
        // });
        // var numberStopCount = 3;
        // _.map(_.range(numberStopCount), function(ii) {
        //     var numberStop =
        //         $("<div>")
        //         .text(formatNumber(maxVal * ii / (numberStopCount - 1)))
        //         .appendTo(numbers);
        //     //if (ii > 0) {
        //         // First numberStop is not absolute positioned, to give div height
        //         numberStop.css({
        //             'position': 'absolute',
        //             left: ii * width / (numberStopCount - 1),
        //             top: 0
        //         });
        //     //}
        //     setTimeout(function() {
        //         // Center the number horizontally, after it has been added to DOM
        //         numberStop.css('marginLeft', - numberStop.width() / 2);
        //     }, 0);
        // });

        var start_number =  $("<div>").addClass('legend-gradient-start').text(formatNumber(maxVal * 0));
        var end_number =  $("<div>").addClass('legend-gradient-end').text(formatNumber(maxVal));

        var gradient;
        switch (dim) {
        case "Color":
            //gradient = colorGradient(gradientNumber, width);
            var stops = _.map(_.range(20), function(n) {
                return chartStateService.getColorGradient(gradientNumber, n / 20) + " " + (n * 5) + "%";
            });
            var gradient_css = "linear-gradient(" + ["90deg"].concat(stops).join(",") + ")";
            gradient = $("<div>").addClass('legend-color-gradient').css({background: gradient_css}).html("&nbsp;");
            break;
        case "Size":
            //gradient = sizeGradient(width);
            gradient = $("<div>").addClass('legend-size-gradient');
            break;
        default:
            throw "Unhandled dimension type in gradientLegend: " + dim;
        }

        return $("<div>").addClass('legend-item legend-item-gradient').append(
            start_number,
            gradient,
            end_number
        );
    }

    // used to generate legends for color (hue) and shape

    function categoryLegend(dim, values, previousValues, totalSeries, seriesNumber, setVisibility, setHighlight, valuesFiltered) {
        var icons;

        switch (dim) {
        case "Color":
            icons = _.map(values, function(val) {
                return colorIcon(chartStateService.getColorForSeries(totalSeries, seriesNumber, _.indexOf(previousValues, val)));
            });
            break;
        case "Shape":
            icons = _.map(values, function(val) {
                return shapeIcon(chartStateService.getShapeForSeries(totalSeries, seriesNumber, _.indexOf(previousValues, val)));
            });
            break;
        default:
            throw "Unhandled dimension type in categoryLegend: " + dim;
        }

        var legendDiv = $("<div>");

        _.map(icons, function(icon, ii) {
            var tag = JSON.stringify([dim, values[ii]]);
            var legendItem = seriesVisibilityToggle(tag, setVisibility, setHighlight, !_.contains(valuesFiltered, values[ii]),
                $("<div>").addClass('legend-item').append(icon, getLegendTitleTag(values[ii])));

            //var legendItem = $("<div>").addClass('legend-item').append(icon, $("<div>").addClass('legend-item-title').html(values[ii]));
            legendDiv.append(legendItem);
        });

        return legendDiv.children();
    }

    // function colorGradient(gradientNumber, width) {
    //     var stops = _.map(_.range(20), function(n) {
    //         return chartStateService.getColorGradient(gradientNumber, n / 20) + " " + (n * 5) + "%";
    //     });

    //     var gradient = "linear-gradient(" + ["90deg"].concat(stops).join(",") + ")";

    //     return $("<div>").addClass('color-gradient').css({background: gradient}).html("&nbsp;");

    // css({
    //     display: 'inline-block',
    //     width: width,
    //     height: 24,
    //     background: gradient
    // }).html("&nbsp;");
    // }

    // function sizeGradient(width) {
    //     return $("<div>").css({width: width}).addClass('size-gradient');
    // .css({
    //     display: 'inline-block',
    //     borderRight: width + 'px solid #999',
    //     borderTop: '24px solid transparent'
    // });
    // }

    function colorIcon(color) {
        return $("<div>").addClass('legend-color-icon').css({
            backgroundColor: color
        });
    }

    function shapeIcon(shape) {
        var icon = $("<div>").addClass('legend-size-icon');
        var renderer = new window.Highcharts.Renderer(icon[0], 18, 18);
        renderer.symbol(shape || 'circle', 4, 4, 12, 12).add().attr('fill', '#555');
        return icon;
    }

    function legendContainer(title, contents) {
        var legend_collapse_toggle = $("<div>").addClass("legend-collapse-toggle").html('<span class="plus">\u002B</span><span class="minus">\u2212</span>');
        var legend_title = $("<div>").addClass("legend-title").html(title);
        var legend_dotdotdot = $("<div>").addClass("legend-dotdotdot").html('&bullet;&bullet;&bullet;');
        return $("<div>").addClass('legend-container').append(legend_collapse_toggle, legend_title, contents, legend_dotdotdot);
    }

    // returns a series toggle switch that...

    // it seems that the tag is used to identify the series to toggle, and that's where a problem is

    function seriesVisibilityToggle(tag, setVisibility, setHighlight, visible, div) {
        div.css('cursor', 'pointer').on('click', function() {
            visible = !visible;
            setVisibility(tag, visible);
            div.css('color', visible ? '#777' : '#bbb');
        }).hover(function() {
            setHighlight(tag, true);
        }, function() {
            setHighlight(tag, false);
        });
        div.css('color', visible ? '#777' : '#bbb');
        if (!visible) {
            setVisibility(tag, false);
        }
        return div;
    }

    function getLegendTitleTag(text) {
        return $("<div>").addClass('legend-item-title').html(text);
    }

    function formatNumber(val) {
        var suffixes = [
            [Math.pow(10, 12), "T"],
            [Math.pow(10, 9), "B"],
            [Math.pow(10, 6), "M"],
            [Math.pow(10, 3), "K"],
            [1, ""]
        ];
        for (var ii = 0; ii < suffixes.length; ii++) {
            if (val > suffixes[ii][0]) {
                var part = val / suffixes[ii][0];
                var decimals = part < 100 ? 1 : 0;
                return window.Highcharts.numberFormat(part, decimals) + suffixes[ii][1];
            }
        }
        return window.Highcharts.numberFormat(val);
    }

    return function(dimensionValues, previousDimensionValues, stage, setVisibility, setHighlight) {
        var series = stage.getAllColumnsPerSeriesQuery();

        return [].concat(
            colorLegends(series, dimensionValues, previousDimensionValues, stage, setVisibility, setHighlight)
        ).concat(
            shapeLegends(series, dimensionValues, previousDimensionValues, stage, setVisibility, setHighlight)
        ).concat(
            sizeLegends(dimensionValues, stage, setVisibility, setHighlight)
        );
    };
} );
