angular.module('visualizer')

.directive('chartTypeSelectorMenu', function(chartStateService) {
    'use strict';
    var open = false;

    function openMenu(elem) {
        elem.find('.options').css({
            height: '0',
            width: '0',
            bottom: '0',
            right: '0',
            visibility: 'visible'
        });

        elem.find('.options').animate({
            height: '340px',
            width: '460px',
            bottom: '0',
            right: '0'
        }, function() {
            open = true;
        });
    }

    function closeMenu(elem) {
        elem.find('.options').animate({
            height: '0',
            width: '0',
            bottom: '0',
            right: '0'
        }, function() {
            open = false;
            elem.find('.options').css({
                visibility: 'hidden'
            }); // eliminates scroll bar from forming while hidden
        });
    }

    return {
        replace: false,
        templateUrl: 'visualizer/chartContainer/chartTypeSelector/chartTypeSelectorMenu.tpl.html',

        link: function(scope, elem) {
            // returns something different than getCurrentChartType...
            // naming could be improved...
            // basically this one is combo-aware, whereas getCurrentChartType only looks at the most recent chart type
            // but actually now that we're disregarding chart type for preview mode it doesn't matter whether this or
            // the most recent chart type is returned

            scope.getSelectedChartType = function() {
                var chartTypes = {};

                var axes = scope.stage.measures;

                _.each(axes, function(axis) {
                    _.each(axis.series, function(seriesColumn) {
                        var chartType = seriesColumn.type; // each series has a column, which is the measure
                        //console.log('axis', axis)
                        //console.log('chartType', chartType)
                        if (chartType === 'spline') {
                            chartType = 'line';
                        } else if (chartType === 'areaspline') {
                            chartType = 'area';
                        }

                        chartTypes[chartType] = true;
                    });
                });

                if (chartTypes.area && chartTypes.column && chartTypes.line) {
                    return 'combo-area-column-line';
                } else if (chartTypes.area && chartTypes.column) {
                    return 'combo-area-column';
                } else if (chartTypes.area && chartTypes.line) {
                    return 'combo-area-line';
                } else if (chartTypes.column && chartTypes.line) {
                    return 'combo-column-line';
                }

                return chartStateService.getUIChartType();
            };

            scope.chartStateTypes = chartStateService.getNativeChartTypes();
            if (!scope.flags.table) {
                scope.chartStateTypes = _.without(scope.chartStateTypes, 'table');
            }

            scope.selectChartType = function(chartType, guidedMode) {
                //traverse all series and change type to chartType
                _.each(scope.stage.allSeries(), function(element, index) {
                    scope.stage.allSeries()[index].type = chartType;
                });

                // Update colorDistribution if new chart type doesn't support it
                if (!_.contains(['column', 'bar', 'area', 'areaspline'], chartType)) {
                    scope.stage.setColorGrouping('Stack');
                }

                //update model, affects all future
                chartStateService.setCurrentChartType(chartType);
                scope.chartType = chartType;
                var extraColumns = scope.stage.convertStageTo(chartType);
                scope.trashBucket.columns.concat(extraColumns);

                scope.guidedMode = guidedMode === undefined ? true : false;
                scope.updateChartRequirements();
                scope.table.editMode = false;

                scope.updateChart();
            };

            elem.closest('body').on('click', function() {
                if (open === true) {
                    closeMenu(elem);
                }
            });

            elem.find(".current-state").on("click", function() {
                if (open === false) {
                    openMenu(elem);
                }
            });
        }

    };
} );
