angular.module('visualizer')
.directive( 'help', function(AdhocWebService, alertService) {
	'use strict';
    return {
        restrict: 'E',
        templateUrl: 'visualizer/help/help.tpl.html',
        link: function(scope) {
            scope.showHelpOnStartupCheckbox = false;
            scope.showHelp = false;
            scope.showHelpButton = "OK, let's go!";

            scope.hideHelp = function(event) {
                var $target = $(event.target);
                if ($target.hasClass('thumbnail') || $target.hasClass('checkbox-target')) {
                    return;
                } else {
                    scope.showHelp = false;
                    console.log('checkbox', scope.showHelpOnStartupCheckbox);
                    // pass opposite of checkbox - checked means store false in db
                    AdhocWebService.setPreferences(!scope.showHelpOnStartupCheckbox);
                }
            };

            scope.showHelpScreen = function() {
                AdhocWebService.getPreferences(
                    function(data) {
                        scope.showHelp = data ? JSON.parse(data) : false;
                        scope.safeApply();
                    },

                    function(errorCause) {
                        if (errorCause === 'query timed out') {
                            // scope.$broadcast('error', {
                            //     basicText: visualizer.misc.errors.ERR_QUERY_TO,
                            //     moreInfoText: ''
                            // });
                            alertService.alert([visualizer.misc.errors.ERR_QUERY_TO, 'error-helpQueryTimedOut']);
                        } else {
                            // scope.$broadcast('error', {
                            //     basicText: errorCause,
                            //     moreInfoText: ''
                            // });
                            alertService.alert([errorCause, 'error-helpOtherError']);
                        }

                    }
                );

            };
        }
    };
} );
