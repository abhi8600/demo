angular.module('visualizer')

.directive('sliderRange', function() {
	'use strict';
    return {
        replace: false,
        scope: true,
        link: function(scope, element) {
            scope.$watch('sliderObj.selectedType', function() {

                var MIN = 0,
                    MAX = 1,
                    step = scope.sliderObj.step ? scope.sliderObj.step : (scope.sliderObj.ranges.max - scope.sliderObj.ranges.min) / 100,
                    valuesKey, // key: value for jquery ui slider where key can be value or values
                    valuesValue, // and value can be a number (value) or an array (values)
                    valuesObj = {},
                    selectedType = true;

                if (scope.sliderObj.selectedType === 'Between' || scope.sliderObj.selectedType === 'Outside') {
                    valuesValue = [scope.sliderObj.ranges.min, scope.sliderObj.ranges.max];
                    valuesKey = 'values';
                } else if (scope.sliderObj.selectedType === '>' || scope.sliderObj.selectedType === '>=') {
                    selectedType = 'max';
                    valuesValue = scope.sliderObj.ranges.min;
                    valuesKey = 'value';
                } else if (scope.sliderObj.selectedType === '<' || scope.sliderObj.selectedType === '<=') {
                    selectedType = 'min';
                    valuesValue = scope.sliderObj.ranges.max;
                    valuesKey = 'value';
                } else if (scope.sliderObj.selectedType === '=' || scope.sliderObj.selectedType === '<>') {
                    selectedType = false;
                    valuesValue = scope.sliderObj.ranges.max;
                    valuesKey = 'value';
                } else if (scope.sliderObj.selectedType === ' IS NULL ' || scope.sliderObj.selectedType === ' IS NOT NULL ') {

                } else {
                    console.log('this option not currently supported. your option is: ' + scope.sliderObj.selectedType);
                }

                var inputsFollowSlideHandles = function() {
                    if (valuesKey === 'values') {
                        scope.sliderObj.ranges.min = element.slider('values', MIN).toFixed(scope.sliderObj.roundingPoints);
                        scope.sliderObj.ranges.max = element.slider('values', MAX).toFixed(scope.sliderObj.roundingPoints);
                        element.next('.inputs').find('.min').css('bottom', element.find('.ui-slider-handle').eq(MIN).css('bottom') );
                        element.next('.inputs').find('.max').css('bottom', element.find('.ui-slider-handle').eq(MAX).css('bottom') );
                    } else if (valuesKey === 'value' && (scope.sliderObj.selectedType === '>' ||scope.sliderObj.selectedType === '>=')) {
                        scope.sliderObj.ranges.min = element.slider('value').toFixed(scope.sliderObj.roundingPoints);
                        element.next('.inputs').find('.min').css('bottom', element.find('.ui-slider-handle').eq(MIN).css('bottom') );
                    } else if (valuesKey === 'value' && (scope.sliderObj.selectedType === '<' || scope.sliderObj.selectedType === '<=' || scope.sliderObj.selectedType === '=' || scope.sliderObj.selectedType === '<>')) {
                        scope.sliderObj.ranges.max = element.slider('value').toFixed(scope.sliderObj.roundingPoints);
                        element.next('.inputs').find('.max').css('bottom', element.find('.ui-slider-handle').eq(MIN).css('bottom') );
                    }
                    scope.$apply();
                };

                var jqSliderObj = {
                    orientation: "vertical",
                    min: scope.sliderObj.lowestValue - (2 * step),
                    max: scope.sliderObj.highestValue + (2 * step),
                    step: step,
                    slide: inputsFollowSlideHandles,
                    change: inputsFollowSlideHandles
                };

                valuesObj = {};
                valuesObj.range = selectedType;
                valuesObj[valuesKey] = valuesValue;
                _.extend(jqSliderObj, valuesObj);

                if (element.slider()) {
                    element.slider('destroy');
                }
                element.slider(jqSliderObj);
                element.bind('mouseup', function () {
                    inputsFollowSlideHandles();
                });
                setTimeout(inputsFollowSlideHandles, 100);
            });
        }
    };
} );
