angular.module('visualizer')

.directive('loader', function(alertService, $q, $timeout) {
	'use strict';
    return {
        link: function (scope, element, attrs) {
            scope.loaders[attrs.area + 'Timeout'] = $q.defer();

            function showLoader() {
                element.addClass('loading');
            }

            function hideLoader() {
                $timeout.cancel(scope.loaders[attrs.area + 'Timeout']);
                scope.loaders[attrs.area + 'Timeout'] = $q.defer();
                alertService.clearById('success-rendering-' + attrs.area);
                element.removeClass('loading');
            }

            scope.$on('loader-{area}-show'.replace('{area}', attrs.area), showLoader);
            scope.$on('loader-{area}-hide'.replace('{area}', attrs.area), hideLoader);
            scope.$on('loader-all-hide', hideLoader);

            // Whenever an error is thrown, hide all loaders.
            scope.$on('alertService-newAlert-danger', function () {
                $timeout(function () {
                    hideLoader();
                }, 200);
            });
        }
    };
} );
