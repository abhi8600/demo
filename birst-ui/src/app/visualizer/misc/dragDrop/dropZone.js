/* handle drop zone */
angular.module('visualizer')

.service('dropZoneService', function() {
	'use strict';
    this.UItype = function(scope) {
        if (scope.stage.allColumns().length === 0) {
            return 'x/y';
        } else if (scope.stage.Category && scope.stage.allSeries().length > 0) {
            return "9squares";
        } else {
            return;
        }
    };

    //process the XY drop and pass onto core logic
    this.processXYDrop = function(scope, element, column) {

        if (scope.measureORattribute === 'Measure') {
            scope.stage.addColumn(column, null, scope.chartPlotType);
        } else {
            scope.stage.addColumn(column, 'Category');
            scope.droppedIntoX = scope.measureORattribute;
            scope.measureORattribute = null; //reset for next time
        }


        scope.updateChart();
    };

    this.showPreviewHover = function(x, y) {
        var previewZones = $('[previewIndex]');

        previewZones.each(function(index, previewZone) {
            var $previewZone = $(previewZone);

            if ($previewZone.offset().left <= x && $previewZone.offset().left + 176 >= x && $previewZone.offset().top <= y && $previewZone.offset().top + 176 >= y) {
                $previewZone.addClass('drag-hover-state');
            } else {
                $previewZone.removeClass('drag-hover-state');
            }
        });
    };

    this.processPreviewDrop = function(scope, element, column, x, y) {

        var previewZones = $('[previewIndex]');

        previewZones.each(function(index, previewZone) {
            var $previewZone = $(previewZone);
            if ($previewZone.offset().left <= x && $previewZone.offset().left + 176 >= x && $previewZone.offset().top <= y && $previewZone.offset().top + 176 >= y) {
                var change = scope.previews[index];
                change.action();
                scope.updateChart();
            }
        });
    };
} )
.directive('dropZone', function(dropZoneService, chartStateService) {
	'use strict';
    return {
        require: '^?designer',
        replace: true,
        templateUrl: 'visualizer/misc/dragDrop/dropZoneUI.tpl.html',
        link: function(scope, element) {

            scope.showXY = true;
            //watch the staging area and disable XY overlay if user is adding items by double-clicking or any other way
            scope.$watch('stage.allColumns().length', function(value) {
                if (value === 0) {
                    scope.showXY = true;
                } else {
                    scope.showXY = false;
                }
            });

            //get the type of chart user is expecting, esp if it's not the typical 'line', so each dropped series can have the correct type (e.g. 'scatter')

            scope.chartPlotType = chartStateService.getCurrentChartType();

            scope.dropZone = {
                "start": function(a, column) {
                    if(scope.chartType === 'table') {
                        return;
                    }
                    if (column.sortObject === true) {
                        return;
                    }

                    scope.selectedUI = dropZoneService.UItype(scope);

                    if (scope.selectedUI === 'x/y') {
                        scope.showXY = true;
                        scope.showPreviews = false;
                    } else if (scope.selectedUI === '9squares') {
                        scope.showXY = false;
                        scope.showPreviews = true;
                        var newPreviews = scope.stage.chartRecommendations(column, chartStateService.getCurrentChartType());
                        scope.previews = newPreviews;
                    }

                    scope.safeApply();

                    if (column.expression && column.expression.isAttribute()) {
                        scope.measureORattribute = "Attribute";
                    } else {
                        scope.measureORattribute = "Measure";
                    }
                },
                "over": function(x, y) {
                    if(scope.chartType === 'table') {
                        return;
                    }
                    if (scope.showPreviews) {
                        dropZoneService.showPreviewHover(x, y);
                        return;
                    }

                    if (scope.stage.allColumns().length === 0) {
                        scope.showXY = true;
                        element.find('.chart-Drop-Zone').addClass('drag-hover-state');
                    }

                    scope.safeApply();
                },
                "out": function() {
                    if(scope.chartType === 'table') {
                        return;
                    }
                    element.find('.chart-Drop-Zone').removeClass('drag-hover-state');
                    scope.safeApply();
                },
                "cancel": function() {
                    if(scope.chartType === 'table') {
                        return;
                    }
                    //restore default X/Y background image
                    scope.showPreviews = false;
                    if (scope.stage.allColumns().length) {
                        scope.showXY = false;
                    } else {
                        scope.showXY = true;
                    }
                    element.find('.chart-Drop-Zone').removeClass('drag-hover-state');
                    scope.safeApply();
                },
                "drop": function(column, x, y) {
                    if(scope.chartType === 'table') {
                        return;
                    }
                    if (scope.selectedUI === 'x/y') {
                        element.find('.chart-Drop-Zone').removeClass('drag-hover-state');
                        dropZoneService.processXYDrop(scope, element, column, x, y);
                    } else if (scope.showPreviews) {
                        dropZoneService.processPreviewDrop(scope, element, column, x, y);
                    } else {
                        dropZoneService.processXYDrop(scope, element, column, x, y);
                    }

                    scope.showXY = false;
                    scope.showPreviews = false;
                    scope.stage.updateSortOrder();
                    scope.searchVariable.string = '';
                    scope.searchGrouped = [
                        [],
                        [],
                        []
                    ];
                    //in case column is in recently used, remove it from there since we just dropped it
                    scope.trashBucket.columns = _.without(scope.trashBucket.columns, column);
                    scope.safeApply();
                }
            };

            scope.isItXorY = function(x, y, element) {
                var x1 = x - scope.offsetX;
                y = y - scope.offsetY;
                var h = element.css('height').replace(/\D/g, '');
                var w = element.css('width').replace(/\D/g, '');
                //flip coords
                y = h - y;
                var y1 = x1 * h / w;
                if (y < y1 && x > scope.offsetX) {
                    return 'X';
                } else if (y > y1 && x > scope.offsetX) {
                    return 'Y';
                } else {
                    return null;
                }
            };
        }
    };
});
