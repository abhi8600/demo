// Usage:

// drop-target="dropDim(dim)"
// draggable="attributeToColumn(attribute)"

/*

What would I like to have?

draggable: on-drag-start, on-drag-end, on-drop**, drag-value*

drop-target: on-drop**

Notice that both the draggable and the droptarget have an on-drop.
"Drag-value" might be useful as some value to send to the drop target, exposing some part of the element's scope.

It may also be useful to have a distinction between dragging a value and dragging/relocating the whole DOM element

*/

/*

To follow "val" the global variable, we need to

1) notice that it's a global
2) see that it is set in startDragging, from the value "value" passed into startDragging
3) see that on mousedown, startDragging is called with the value parameter set to scope.$eval(attrs.draggable)
4) comment: "draggable" gets treated as a value expression, rather than a boolean, and has an effect other than determining if something is true or false
   the pattern HTML follows is onclick="some action" rather than clickable="some action", so we should follow HTML's example

*/

angular.module('visualizer').

service('dragDropService', function() {
	'use strict';
    var dropTargets = [];
    var rects       = [];

    var startDrag = null;
    var elementBeingDragged  = false;
    var val       = null;
    var drop      = null;


    function endDrag(x,y) { // called on mouseup
        //add 2nd param to drop return fcn to return x,y of mouse when dropped
        if ( drop ) {
            drop.drop( val, x,y );
        }

        dropTargets.map(function(dropTarget) {
            dropTarget.cancel(val);
            dropTarget.elem.unbind('.dragordrop');
        });

        elementBeingDragged.remove();

        elementBeingDragged = null;
        val      = null;
        drop     = null;
    }

    function checkRects(x, y) {
        _.map(rects, function(rect, ii) {
            // note: doesn't deal with overlaps at all.
            if ( rect.left <= x && rect.left + rect.width >= x && rect.top <= y && rect.top + rect.height >= y ) {
                if ( drop !== dropTargets[ii]) {
                    if (drop) {
                        drop.out();
                    }
                    drop = dropTargets[ii];
                    drop.over(x, y, true);
                } else {
                    drop.over(x, y, false);
                }
            } else {
                if ( drop === dropTargets[ii] ) {
                    drop.out();
                    drop = null;
                }
            }
        });
    }

    $(document.body).bind('mouseup', function(ev) {
        if ( startDrag ) {
            clearTimeout(startDrag);
            startDrag = null;
        }

        if ( elementBeingDragged ) {
            endDrag(ev.pageX, ev.pageY);
        }
    });

    $(document.body).bind('mousemove', function(ev) {
        if ( !elementBeingDragged ) {
            return true;
        }

        var x = ev.pageX,
            y = ev.pageY;

        checkRects(x, y);

        //for now, make offset static..no point in calcing actual offset on body; too much work for no real improvement
        //drag offset changed to 0 from 100 for now so draggable element is lef-aligned with cursor--easier for dropping where user expects drop

        elementBeingDragged.css('left', x - 0).css('top', y - 0);
        return false;
    });

    return {
        register: function(dropTarget) {
            if ( dropTargets.indexOf(dropTarget ) === -1 ) {
                dropTargets.push(dropTarget);
            }
        },
        deregister: function(dropTarget) {
            if ( dropTargets.indexOf(dropTarget) !== -1 ) {
                dropTargets.splice( dropTargets.indexOf(dropTarget), 1 );
            }
        },
        startDragging: function(elem, value, tag) {

            // setTimeout is used to avoid cloning the element and doing other work
            // if the user is simply clicking an element (mousedown can be interpreted
            // as either the beginning of a click or a beginning of a drag)

            startDrag = setTimeout(function() {
                dropTargets.map(function(dropTarget) {
                    dropTarget.start(tag, value); // calls the drag start event handler on the dropTarget
                });

                rects = [];
                // dropTarget.start could change drop target dimensions, so wait 50 ms before measuring.
                setTimeout(function() {
                    rects = dropTargets.map(function(dropTarget) {

                        return {
                            left:   dropTarget.elem.offset().left,
                            top:    dropTarget.elem.offset().top,
                            width:  dropTarget.elem.width(),
                            height: dropTarget.elem.height()
                        };
                    });
                }, 50);

                elementBeingDragged = elem.clone().appendTo(document.body);

                elementBeingDragged.css({
                    cursor:    'pointer',
                    position:  'absolute',
                    left:      elem.offset().left,
                    top:       elem.offset().top
                });

                val = value;
                startDrag = null;
            }, 200);
        }
    };
} )
.directive('draggable', function(dragDropService) {
	'use strict';
    var LEFT_BUTTON = 1;

    return {
        link: function(scope, element, attrs) {
            element.bind('mousedown', function(event) {
                var mouseButtonClicked = event.which;

                if ( mouseButtonClicked !== LEFT_BUTTON ) {
                    return true;
                }

                if ( scope.$eval(attrs.draggable) ) {
                    dragDropService.startDragging(element, scope.$eval(attrs.draggable));
                }

                return false;
            });
        }
    };
})
.directive('dropTarget', function(dragDropService) {
	'use strict';
    return {
        link: function(scope, elem, attrs) {
            var myDropTarget;
            function registerDropTarget(dropTarget) {
                // comments would be helpful here because the watch below means we're constantly checking for changes
                // to the drop-target function and updating something as a result,
                // but it appears if someone clears the drop-target function (odd thing to call a function BTW),
                // we'll just ignore that and continue executing the previously defined function in that attribute,
                // and also it appears we'll repeatedly register more drop targets for the same element
                // if the function changes several times, because we only remove drop targets when the element
                // itself is destroyed rather than when the function changes...

                // maybe I'm reading this wrong, but that's why it would be nice to have comments
                // to explain what's happening or at least what is intended to happen

                if ( !dropTarget ) {
                    return;
                }

                if ( $.isFunction(dropTarget) ) {
                    var fn = dropTarget;

                    dropTarget = {
                        drop: function(val) {
                            scope.$apply(function() {
                                fn(val);
                            });
                        },
                        cancel: function() { },
                        over:   function() { },
                        out:    function() { },
                        start:  function() { },
                        elem: elem
                    };
                } else {
                    dropTarget = {
                        start: dropTarget.start || function() {},
                        cancel: dropTarget.cancel || function() {},
                        over: dropTarget.over || function() {},
                        out: dropTarget.out || function() {},
                        drop: dropTarget.drop || function() {},
                        elem: elem
                    };
                }

                if(myDropTarget) {
                    dragDropService.deregister(myDropTarget);
                }
                myDropTarget = dropTarget;
                dragDropService.register(dropTarget);

            }

            elem.bind('$destroy', function() {
                if(myDropTarget) {
                    dragDropService.deregister(myDropTarget);
                }
            });

            scope.$watch(function() {
                    return scope.$eval(attrs.dropTarget);
                }, registerDropTarget
            );
        }
    };
} );
