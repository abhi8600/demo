angular.module('visualizer')
.directive( 'messageCenter', function () {
    'use strict';
    return {
        replace: true,
        templateUrl: 'visualizer/misc/messageCenter/messageCenter.tpl.html',
        controller: function ($scope, $element, alertService) {
            $scope.alerts = alertService;

            $scope.alerts.messageCenter = {};
            $scope.alerts.messageCenter.newNotificationCount = 0;
            $scope.alerts.messageCenter.visible = false;
            $scope.alerts.messageCenter.toggle = function () {
                if ($scope.flags.messageCenter) {
                    $scope.alerts.messageCenter.visible = !$scope.alerts.messageCenter.visible;
                    $scope.alerts.messageCenter.newNotificationCount = 0;
                } else {
                    var potentialError = $scope.alerts.queueContainsError();
                    if (potentialError === parseInt(potentialError, 10)) {
                        // If an integer [index] is returned by queueContainsError(),
                        // re-trigger the error.
                        $scope.alerts.alert($scope.alerts.alertQueue[potentialError]);
                    }
                }
            };

            $scope.$on('alertService-newAlert', function (event, properties) {
                if ($scope.alerts.messageCenter.newNotificationCount < properties.maxNotifications) {
                    $scope.alerts.messageCenter.newNotificationCount += 1;
                }
            });
            $scope.$on('alertService-clearAll', function () {
                $scope.alerts.messageCenter.newNotificationCount = 0;
            });
            $scope.$on('updateChart', function () {
                $scope.alerts.clearByType('danger');
                $element.find('.icn-alert-center-flag').removeClass('icn-alert-center-flag-red');
            });
            // $scope.$on('blankChart', function () {
            //     console.log('blankChart heard!  clearing all alerts.');
            //     //$scope.alerts.clearAll();
            // });
            $scope.$on('alertService-newAlert-danger', function () {
                $element.find('.icn-alert-center-flag').addClass('icn-alert-center-flag-red');
            });

            $scope.alerts.messageCenter.filterState = {success: true, warning: true, danger: true};

            // The actual filter that is applied to alerts.alertQueue
            $scope.alerts.messageCenter.filters = function (alert) {
                return $scope.alerts.messageCenter.filterState[alert.type];
            };
        }
    };
} );
