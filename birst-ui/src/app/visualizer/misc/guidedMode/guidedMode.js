angular.module('visualizer')

.directive( 'guidedMode', function(chartStateService) {
	'use strict';
    return {
        link: function(scope) {

            scope.updateChartRequirements = function() {

                var chartType = chartStateService.getCurrentChartType();

                if (chartType !== 'table' && scope.stage.allSeries().length && _.every(["Category", "Color", "Shape", "Size"], function(dim) {
                    return scope.stage.guidedModeRecommendation(dim, chartType).ok;
                })) {
                    // if the chart is already valid, turn off guided mode immediately
                    scope.guidedMode = false;
                }

                if (chartType === 'table') {
                    scope.guidedMode = false;
                }
            };
        }
    };
} );
