angular.module('visualizer')

.directive('buttonDropdownGroup', function() {
	'use strict';
    return {
        replace: true,
        scope: {
            model: '=',
            options: '=',
            type: '@'
        },
        templateUrl: 'visualizer/misc/buttonDropdownGroup/buttonDropdownGroup.tpl.html',
        link: function(scope) {
            scope.optionsLength = _.size(scope.options.text[scope.type]);
            scope.showMoreOptions = false;
            scope.toggleMoreOptions = function() {
                scope.showMoreOptions = !scope.showMoreOptions;
            };
            scope.setModel = function(index) {
                scope.model = scope.options.lookup[scope.type][index];
                scope.showMoreOptions = false;
            };
        }
    };
} );
