/* list of errors in english */

visualizer.misc.errors = {
	'ERR_UNABLE_DBBASIC': 'Unable to connect to your space on this server.',
	'ERR_UNABLE_DBMORE': 'Check (1) server name/IP address (2) spaceID (3) port (4) http vs. https',

	'ERR_TOOMANYTRELLIS': 'Too many trellis charts to display.  Try adding a filter.',

	'ERR_PIE_COMBINE': 'PIE chart cannot be combined with other charts. Please select either single measure for pie-chart only, or change chart type.',

	'ERR_QUERY_NO_DATA': 'No data returned by query.',

	'ERR_NO_COLOR_TILL_CAT': 'Cannot chart column for Color until a Category is added.',

	'ERR_SA_PROCESS_BASIC': 'Error in subject area processing.',
	'ERR_SA_PROCESS_MORE': 'see console for tree content.',

	'ERR_BAD_CREDS': 'Unable to connect to your space with the user credentials provided.',

	'ERR_BAD_REPORT': 'Unable to read report.',

	'ERR_QUERY_TO': 'Query timed out.',

	'ERR_SA_BASIC': 'Error in subject area content',
	'ERR_SA_MORE': 'see console',

	'ERR_SORT_DIRECT': 'You cannot sort directly from the subject area. Please drag a column that is being visualized from Chart Builder.',

	'ERR_CANT_FILTER1': 'Your selection, ',
	'ERR_CANT_FILTER2': ', cannot be filtered yet. Please add a column to Measures or Category first.',
	'ERR_CANT_FILTER3': ', is an expression and we cannot currently filter on an unvisualized expression.',

	'ERR_NEED_CATEGORY': ', cannot be filtered yet. Please add an attribute Category first.',

	'ERR_ATTR_AS_MSR1': 'Your selection, ',
	'ERR_ATTR_AS_MSR2': ', is an attribute and cannot be used as a measure.',

	'ERR_CANT_BE_DIM': '" cannot be used as a ',

	'ERR_RESULT_LARGE_NEED_FILTER': 'Result is too large to chart.  Try adding a filter.',

	'ERR_CANT_CHART_WO_ATTRIB': 'We are unable to chart anything except your single measure until you add an attribute to Category.',

	'ERR_CANT_DIM_CURRENT_LAYER1': '" cannot be used as a ',
	'ERR_CANT_DIM_CURRENT_LAYER2': ' with current chart layers. Remove it, move it to a different bucket, or change the chart type.',

	'ERR_TOO_MANY_SERIES_ADD_FILTER': 'Too many series to chart.  Try adding a filter to the column in color or shape.',

	'ERR_NO_RESULT_MOD_SELECTION': 'Unfortunately, your query returns no results. Please try modifying your selections in the Chart Builder.',

	'ERR_CAT_B4_MULT_MSR': 'Note: please add a Category before attempting to plot multiple Measures.',

	'MULTIPLE_X_NOT_SUPPORTED': 'Multiple X axes are not currently supported.',

	'UNABLE_TO_CHART': 'We are currently unable to handle some of the features in your report.',

	'UNSUPPORTED_CHART_TYPE': 'Visualizer currently doesn\'t support one or more of the chart types in your report.',

	'UNABLE_TO_PROCESS_COLUMN': 'We were unable to process one of the columns in your chart.',

	'UNKNOWN_ERROR': 'An unknown error occurred.',

	'ERR_LOGGED_OUT': 'Your session has expired. Please log out and log back in.',

	'ERR_ERR_PRESENT': 'Cannot filter while an error is present.',
	'ERR_CHART_TIMEOUT': 'Visualizer rendering timed out (60 seconds).',
	'ERR_FILTER_TIMEOUT': 'Filter loading timed out (30 seconds).'
};

visualizer.misc.feedback = {
	'FEEDBACK_RENDERING_CHART': 'Rendering Visualization.',
	'FEEDBACK_RENDERING_SA': 'Loading Filter Values.',
	'FEEDBACK_RENDERING_SA_INITIAL': 'Loading Subject Area.',

	'FEEDBACK_LOADING_FILTER': 'Loading Filter Values.'
};
