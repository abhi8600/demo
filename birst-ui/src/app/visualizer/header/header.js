angular.module('visualizer')

.directive('header', function(globalStateService, AdhocWebService) {
	'use strict';
	return {
		restrict: 'A',
		templateUrl: 'visualizer/header/header.tpl.html',
		link: function(scope) {
			scope.isAdminButtonVisible = false;
			scope.isDesignerButtonVisible = false;
			scope.isDashboardButtonVisible = false;
			scope.spaceName = '';


			if (globalStateService.determineLaunchState() === 'standalone' || globalStateService.determineLaunchState() === 'dev' || globalStateService.determineLaunchState() === 'standalone_trial') {
				//displaying nav bar options is last priority so give prod some breathing room before doing it
				setTimeout(function() {
					AdhocWebService.getNavBarAccess(
						function(data) {
							scope.isAdminButtonVisible = data.adminInNav;
							scope.isDesignerButtonVisible = data.DesignerInNav;
							scope.isDashboardButtonVisible = data.DashboardInNav;
							scope.spaceName = data.spaceName;
							scope.safeApply();
						});
				}, 500);
			}
		}
	};
} );
