/**
 * Called by the "reportView" route.  Renders chart taking up max document window, based on Stage
 */

angular.module('visualizer')

.directive('reportView', [ 'queryService', 'chartStateService', 'globalStateService', 'AdhocWebService', 'ReportImporter', 'columnCreator', 'Stage', 'alertService', function(queryService, chartStateService, globalStateService, AdhocWebService, ReportImporter, columnCreator, Stage, alertService ) {
	'use strict';
	return {
		scope: true,
		templateUrl: 'visualizer/reportView.tpl.html',
		controller: function($scope, $rootElement, $location) {
			$scope.reportName = ($scope.file && $scope.file.fullPath) || $location.search().reportName;

			if ($scope.reportName && $scope.reportName.match(/\.viz\.dashlet$/)) {
				var parts = $scope.reportName.split("/");
				var fileName = parts.pop();
				var folder = parts.join("/") + "/";
				// fileSystem is not on scope yet, so use a setTimeout I guess
				setTimeout(function() {
					$scope.fileSystem.open(folder, fileName);
				}, 0);
			}

			var RI = ReportImporter;

			function updatePrompts(prompts) {
				//console.log('trying to add prompts: ', prompts);
				try {
					_.each(prompts, function(properties, columnName) {
						addPrompt(columnName, properties);
					});
				} catch (e) {
					console.log(e);
					console.log('Failed to import one or more filters.');
				}

				$scope.updateChart();
			}

			function addPrompt(columnName, properties) {
				// try to find the column....

				if (columnName === 'P1') {
					return;
				}

				if (properties.selectedValues.length === 1 && properties.selectedValues[0] === '') {
                    //console.log('empty value prompt', properties);
                    return;
                }

				var dataType = properties.dataType;
				var filterType;

				// from AdhocWebService.js line 251 (currently)

				if (dataType === 'TIMESTAMP' || dataType === 'DATE') {
					filterType = 'date';
				} else if (dataType === 'VARCHAR') {
					filterType = 'items';
				} else if (dataType === 'INTEGER' || dataType === 'DOUBLE' || dataType === 'FLOAT') {
					filterType = 'range';
				} else {
					filterType = 'items';

					if (properties.operator !== '=') {
						filterType = 'range';
					}
				}

				var column = {
					//fullName: columnName,
					name: columnName, //_.last(columnName.split('.')),
					bql: '[' + columnName + ']',
					prompts: {
						type: filterType,
						subType: '='
					},
					type: 'unvisualized'
				};

				// try to find the prompt first...

				// seems that multiSelectType should always be OR... a second prompt would create an AND
				var visualizerFilters = [];


				if (properties.operator !== 'between') {
					_.each(properties.selectedValues, function(value) {
						visualizerFilters.push({
							operator: properties.operator,
							operand: value
						});
					});
				} else if (properties.operator === 'between') {
					var min = _.min(properties.selectedValues);
					var max = _.max(properties.selectedValues);

					column.prompts.type = 'range';

					visualizerFilters.push({
						operator: '>=',
						operand: min
					});
					visualizerFilters.push({
						operator: '<=',
						operand: max
					});
				}

				column.prompts.items = visualizerFilters;

				// FIXME: Refactor this to explicitly create an "external"
				// column, rather than using the dummy {} subject area. This
				// will be easier after column object refactor has been merged
				// in.
				var preparedColumn = RI.prepareColumnForStage(column, {}, $scope.stage);

				preparedColumn.uniqueId = _.uniqueId();

				if (properties.selectedValues && properties.selectedValues[0] === 'NO_FILTER') {
					$scope.stage.updatePrompt(preparedColumn, {
						isFilterOn: true,
						visualized: false
					}, 'remove');
					return;
				} else {
					$scope.stage.updatePrompt(preparedColumn, {
						isFilterOn: true,
						visualized: false
					}, 'add');

					_.each($scope.stage.allSeries(), function(measure) {
						preparedColumn.measuresToFilter[measure.column.name] = true;
					});
				}
			}

			globalStateService.setProductLaunchedAs('reportView');

			$scope.launchMode = globalStateService.getProductLaunchedAs();

			$scope.stage = new Stage();
			$scope.updateStage = function(newStage) {
				$scope.stage = newStage;
			};

			// For Using VisualizerPlugin (no iframe):
			$scope.$on('prompts', function(e, prompts) {
				updatePrompts(prompts);
			});
			var gotReportMessage;
			// For Using VisualizerIframePlugin:
			$(window).on("message", function(e) {
				if (!gotReportMessage) {
					gotReportMessage = true;

					try {
						var data = JSON.parse(e.originalEvent.data) || {};
						if ($scope.fileSystem) {
							try {
								$scope.fileSystem.render(JSON.stringify(data.report));
							} catch (e) {
								console.log('error while trying to render report');
							}

							//console.log('!!!! trying to add filters');
							updatePrompts(data.prompts);
						}
					} catch (e) {
						console.log('Error trying to load report: ', e);
					}
				}
			});

			setInterval(function() {
				var $el = $rootElement.find('[ng-app]').parent();
				var elWidth = $el.width();
				var elHeight = $el.height();

				var $chartChild = $rootElement.find('[chart]').children().first();

				var lastElWidth = $chartChild.width();
				var lastElHeight = $chartChild.height();

				if (lastElHeight !== elHeight || lastElWidth !== elWidth) {
					$scope.$broadcast('resize-charts');
				}
			}, 1000);

			$scope.safeApply = function() {
				var phase = this.$parent.$$phase;
				if (phase === '$apply' || phase === '$digest') {
					return;
				} else {
					$scope.$apply();
				}
			};

			$scope.errorHandler = function(errorObject, eraseChart) {
				//$scope.$broadcast('error', errorObject);
				alertService.alert([errorObject.basicText, 'error-reportView']);
				if (eraseChart) {
					if ($scope.flags && $scope.flags.loaders) {
						$scope.$broadcast('loader-chart-hide');
						console.log('(2) broadcasting: loader-chart-hide');
					}
					$scope.$broadcast('blankChart', []);
				}
			};

			$scope.clearStage = function() {
				return true;
			};

			$scope.createColumn = function(protoColumn) {
				return columnCreator.createFromProto(protoColumn);
			};

			$scope.stageColumn = function(protoColumn, dimension, chartType) {
				if (!protoColumn.disabled) {
					var column = $scope.createColumn(protoColumn);

					$scope.stage.addColumn(column, dimension, chartType || chartStateService.getCurrentChartType());
					$scope.updateChart();
					$scope.searchVariable.string = '';
					$scope.searchGrouped = [
						[],
						[],
						[]
					];
				}

			};

			$scope.$on('stageColumn', function(event, column, dim, type) {
				$scope.stage.addColumn(column, dim, type || chartStateService.getCurrentChartType());
				$scope.updateChart();
				$scope.searchGrouped = [
					[],
					[],
					[]
				];
			});

			var queryHandle = null;
			$scope.updateChart = function() {
				setTimeout(function() {

					$scope.$broadcast('blankChart');

					var canceled = false;

					if (queryHandle) {
						queryHandle();
					}

					queryHandle = function() {
						canceled = true;
					};

					queryService.runQueriesForStage($scope.stage, function(resultSets) {
						if (!canceled) {
							queryHandle = null;

							if ($scope.chartType !== 'table' && resultSets && resultSets[0] && resultSets[0].rows.length > 999) {
								$scope.$broadcast('blankChart', []);

								// $scope.$broadcast('error', {
								//     basicText: visualizer.misc.errors.ERR_RESULT_LARGE_NEED_FILTER,
								//     moreInfoText: ''
								// });
								alertService.alert([visualizer.misc.errors.ERR_RESULT_LARGE_NEED_FILTER, 'error-reportViewTooLarge']);

								$scope.safeApply();
							} else {
								$scope.stage.addMinMax(resultSets);
								$scope.stage.addDataTypeToColumns(resultSets);
								$scope.$broadcast('updateChart', resultSets);
							}
						}
					}, $scope);
				}, 0);
			};

		}
	};
}]);
