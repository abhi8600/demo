angular.module('visualizer').
directive('tableFormatting', function() {
	'use strict';
    return {
        replace: true,
        templateUrl: 'visualizer/formatting/tableFormatting.tpl.html'
    };
});
