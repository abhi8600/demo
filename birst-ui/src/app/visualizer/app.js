/*jshint -W079 */

// Declare app level module which depends on filters, and services

var visualizer = {
	directives: {},
	services: {},
	webservices: {},
	models: {},
	controllers: {},
	filters: {},
	misc: {}
};

angular.module( 'visualizer', [
	//'ngRoute',
	'ui.router',
	//'ngAnimate',
	'ngSanitize',
	//'ngTouch',
	'ui.bootstrap',
	'ui.sortable',
	'mgcrea.ngStrap',
	'templates-app',
	'models.stage'
]).
config( function( $stateProvider ) {
	'use strict';

	var data = { pageTitle : 'Create Page', editMode : false, current : 'visualizer' };

	$stateProvider.state( 'vizReportName', {
		url : '/visualizer/reportName/:reportName',
		templateUrl : 'visualizer/designer.tpl.html',
		data : angular.extend( {}, data, { pageTitle : 'Visualizer Report' } )
	});
	$stateProvider.state( 'vizHome', {
		url : '/visualizer',
		templateUrl: 'visualizer/designer.tpl.html',
		data : angular.extend( {}, data, { pageTitle : 'Visualizer Home' } )
	});
	// $stateProvider.when('/login', {
	//     templateUrl: 'visualizer//login.html'
	// });
	$stateProvider.state( 'vizReportView', {
		url : '/visualizer/reportView',
		templateUrl: 'visualizer/reportViewShell.tpl.html',
		data : angular.extend( {}, data, { pageTitle : 'Visualizer Report View' } )
	});
	// $stateProvider.otherwise({
	//     redirectTo: '/'
	// });
} );
