angular.module('visualizer').
    directive('toolbar', function ($stateParams, $window) {
	'use strict';
    return {
        replace: true,
        templateUrl: 'visualizer/toolbar/toolbar.tpl.html',
        controller: function ($scope) {
            $scope.showFilePath = false;
            $scope.reopenHelpScreen = function () {
                $scope.showHelpButton = "OK, let's go!";
                $scope.showHelp = true;
            };

            $scope.dashboardName = typeof $stateParams.dbname === 'undefined' ? '' : $stateParams.dbname;
            $scope.dashboardPage = typeof $stateParams.dbpage === 'undefined' ? '' : $stateParams.dbpage;

            $scope.publishToDashboards = function (activeDirectory, reportName) {
                var dashboard_url,
                    page = '',
                    name = encodeURIComponent($scope.dashboardName),
                    dir = encodeURIComponent(activeDirectory.replace(/\//g, ':')),
                    report = encodeURIComponent(':' + reportName);
                if ($scope.dashboardPage !== 'new') {
                    page = encodeURIComponent($scope.dashboardPage) + '/';
                }
                dashboard_url = '/Dashboards2.0/#/dashboards/' + name + '/page/' + page + 'add/' + dir + report;
                //console.log('dashboard URL:', dashboard_url);
                $window.location.href = dashboard_url;
            };
        }
    };
} );
