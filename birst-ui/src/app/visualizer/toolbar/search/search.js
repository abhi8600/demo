angular.module('visualizer')

.directive('search', function() {
	'use strict';
    return {
        restrict: 'A',
        link: function(scope, elem) {
            var $vizMenu = elem.closest('[toolbar]').next('[viz-menu]');

            var activateSearch = function() {
                scope.activeSearch = true;
                scope.searchVariable.string = '';
                scope.searchGrouped = [
                    [],
                    [],
                    []
                ];
                scope.safeApply();
            };
            var deactivateSearch = function() {
                scope.activeSearch = false;
                scope.searchVariable.string = '';
                scope.searchGrouped = [
                    [],
                    [],
                    []
                ];
                scope.safeApply();
            };

            elem.focus(activateSearch);
            elem.blur(function() {
                setTimeout(deactivateSearch, 100);
            });

            scope.highlightColumn = function(event) {
                $vizMenu.find('[search-results] > div.search-selected').removeClass('search-selected');
                $vizMenu.find(event.target).closest('[column]').addClass('search-selected');
            };
            scope.resetSearch = function() {
                scope.searchVariable.string = '';
                scope.searchGrouped = [
                    [],
                    [],
                    []
                ];
            };
            scope.openFolder = function(folder) {
                deactivateSearch();
                $vizMenu.find('[dimensions] > [menu-child]').addClass('active').css('z-index', scope.navCurrentZIndex++);
                $vizMenu.find('[folder].' + folder.label.replace(' ', '') + ' > [menu-child]').addClass('active').css('z-index', scope.navCurrentZIndex++);
            };

            var searchResults = function(search) {
                if (search.length === 0) {
                    scope.searchList = [];
                    scope.$apply();
                    return;
                }
                var searchGrouped = [
                    /* startsWith: */
                    [],
                    /* contains: */
                    [],
                    /* endsWith: */
                    []
                ];

                var addToSearchGrouped = function(result, protoColumn, isFolder) {
                    if (result) {
                        if (isFolder === true) {
                            protoColumn = {
                                label: result.input,
                                type: 'folder'
                            };

                            var folderExists = false; // don't continue adding the folder more than once

                            _.each(searchGrouped, function(group) {
                                var groupFiltered = _.filter(group, function(item) {
                                    return item.type === 'folder' && item.label === protoColumn.label;
                                });
                                if (groupFiltered.length) {
                                    folderExists = true;
                                }
                            });
                            if (folderExists === true) {
                                return;
                            }
                        }

                        if (result.index === 0) {
                            searchGrouped[0].push(protoColumn);
                        } else if (result.index + result[0].length === result.input.length) {
                            searchGrouped[2].push(protoColumn);
                        } else {
                            searchGrouped[1].push(protoColumn);
                        }
                    }
                };

                var regex_search = new RegExp(_.regexEscape(search).replace(/\\\s/g, '[\\s_]'), 'i');

                _.each(scope.subjectAreaConcat, function(protoColumn) {

                    var folderName = protoColumn.type === 'attribute' ? _.first(protoColumn.path) : false;
                    addToSearchGrouped(regex_search.exec(folderName), null, true);

                    var protoColumnName = protoColumn.type === 'attribute' ? _.last(protoColumn.label.split('.')) : protoColumn.label;
                    addToSearchGrouped(regex_search.exec(protoColumnName), protoColumn, false);

                });

                var searchList = [],
                    i = 0;
                _.each(searchGrouped, function(group) {
                    _.each(group, function(protoColumn) {
                        if (i++ < 50) {
                            searchList.push(protoColumn);
                        }
                    });
                });

                if (!_.isEqual(searchList, scope.searchList)) {
                    scope.searchList = searchList;
                    scope.$apply();
                    $vizMenu.find('[search-results] [column].search-selected').removeClass('search-selected');
                    setTimeout(function() {
                        $vizMenu.find('[search-results] [column]').first().addClass('search-selected');
                    }, 200);
                }
            };

            var lazySearch = _.debounce(searchResults, 200);

            scope.keyboardSearch = function(event) {
                var key = event.keyCode,
                    $search = $vizMenu.find('[search-results] [column]'),
                    $index,
                    $filtered,
                    $next,
                    $prev;
                if (key === 27) { // escape
                    scope.activeSearch = false;
                    scope.searchVariable.string = '';
                    scope.searchGrouped = [
                        [],
                        [],
                        []
                    ];
                    elem.blur();
                } else if (key === 8) { // backspace
                    scope.searchVariable.string = scope.searchVariable.string.substring(0, scope.searchVariable.string.length);
                    $search.find('.search-selected').removeClass('search-selected');
                    setTimeout(function() {
                        $search.first().addClass('search-selected');
                    }, 100);
                    lazySearch(scope.searchVariable.string);
                } else if (key === 40) { // down arrow
                    if ($search.last().hasClass('search-selected')) {
                        return;
                    } else {
                        $filtered = $search.filter('.search-selected');
                        $index = $search.index($filtered);
                        $search.eq($index).removeClass('search-selected');
                        $next = $search.eq($index + 1);
                        $next.addClass('search-selected');
                        if ($next.offset().top > (document.height - 62) || $next.offset().top < 114) {
                            $vizMenu.find('[search-results]').scrollTop($index * 62);
                        }
                    }
                } else if (key === 38) { // up arrow
                    if ($search.first().hasClass('search-selected')) {
                        return;
                    } else {
                        $filtered = $search.filter('.search-selected');
                        $index = $search.index($filtered);
                        $search.eq($index).removeClass('search-selected');
                        $prev = $search.eq($index - 1);
                        $prev.addClass('search-selected');
                        if ($prev.offset().top < 168 || $prev.offset().top > (document.height - 50)) {
                            $vizMenu.find('[search-results]').scrollTop(($index - 2) * 62);
                        }
                    }
                } else if (key === 13) { // enter
                    var $searchResults = $vizMenu.find('[search-results]');
                    var $protoColumn = $searchResults.find('[column].search-selected') || $searchResults.find('[column]').first();
                    if (!$protoColumn.parent().scope()) {
                        return;
                    }
                    var protoColumn = $protoColumn.parent().scope().protoColumn;
                    if (protoColumn.type === 'attribute') {
                        scope.stageColumn(protoColumn);
                    } else if (protoColumn.type === 'measure') {
                        scope.stageColumn(protoColumn);
                    } else if (protoColumn.type === 'folder') {
                        elem.blur();
                        scope.openFolder(protoColumn);
                    } else {
                        scope.stageColumn(protoColumn);
                    }
                } else {
                    lazySearch(scope.searchVariable.string);
                }
                return false;
            };
        }
    };
} );
