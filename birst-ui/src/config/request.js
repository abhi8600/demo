angular.module( 'config.request', [] )

.constant( 'DEFAULT_REQUEST_CONFIG', {
	method : 'POST',
	xsrfHeaderName : 'X-XSRF-TOKEN',
	xsrfCookieName : 'XSRF-TOKEN',
	requestTimeout : 300000
} );
