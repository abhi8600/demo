# The `src/common/config` Directory

The `src/common/config` directory houses configuration objects such as jQuery
plugins, jQuery configs, and other library configs/overrides.

```
src/
  |- common/
  |  |- config/
  |  |- jQuery.config.js
```

- `jQuery.config.js` - holds jQuery specific configurations