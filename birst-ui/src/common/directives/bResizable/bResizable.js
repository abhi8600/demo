angular.module( 'directives.bResizable', [ 'services.math', 'services.string' ] )

.directive( 'bResizable', function( math, string, $templateCache ) {
	'use strict';
	var PERCENT = 'percent';
	var PERCENT_SYMBOL = '%';
	var RESIZING_CLASS = 'resizing';

	var resizeFns = {
		e: function( originalState, dx) {
			return {
				width : originalState.width + dx
			};
		},
		w: function( originalState, dx) {
			return {
				left : originalState.left + dx,
				width : originalState.width - dx
			};
		},
		n: function( originalState, dx, dy) {
			return {
				top : originalState.top + dy,
				height : originalState.height - dy
			};
		},
		s: function( originalState, dx, dy) {
			return {
				height : originalState.height + dy
			};
		},
		se: function( originalState, dx, dy) {
			return _.extend(resizeFns.s.apply(this, arguments), resizeFns.e.apply(this, arguments));
		},
		sw: function( originalState, dx, dy) {
			return _.extend(resizeFns.s.apply(this, arguments), resizeFns.w.apply(this, arguments));
		},
		ne: function( originalState, dx, dy) {
			return _.extend(resizeFns.n.apply(this, arguments), resizeFns.e.apply(this, arguments));
		},
		nw: function( originalState, dx, dy) {
			return _.extend(resizeFns.n.apply(this, arguments), resizeFns.w.apply(this, arguments));
		}
	};

	var convertToPercentProps = function( properties ) {
		var convertedProps = {};
		_.each( properties, function( v, k ) {
			convertedProps[ PERCENT + string.capitalize( k ) ] = v;
		} );
		return convertedProps;
	};

	var $scrollContainer = $( '.mcont > ui-view' );
	var $body = $( 'body' );
	var $window = $( window );

	return {
		link : function( scope, element, attrs) {
			var template = $templateCache.get( 'directives/bResizable/bResizable.tpl.html' );
			element.append( template );
			var $handles = element.find( '.resize-handle, .resize-side' );
			var $parentEl = element.parent();
			var layout = scope.layout;
			var layoutOrSelectionGroup, originalState, axis, $activeHandle;

			scope.$watchCollection( 'layout', function( newLayout, oldLayout ) {
				if ( layout.resizing && newLayout ) {
					element.css( {
						left : newLayout.percentLeft + PERCENT_SYMBOL,
						top : newLayout.percentTop + PERCENT_SYMBOL,
						width : newLayout.percentWidth + PERCENT_SYMBOL,
						height : newLayout.percentHeight + PERCENT_SYMBOL,
						zIndex : newLayout.zIndex
					} );

				}
			} );

			scope.$watch( 'layout.resizing', function( newVal, oldVal) {
				if ( newVal !== oldVal ) {
					if ( newVal ) {
						element.addClass( RESIZING_CLASS );
					} else {
						element.removeClass( RESIZING_CLASS );
					}
				}
			} );

			var addClasses = function() {
				$body.addClass( 'noselect resizing-' + axis );
				$activeHandle.addClass( 'active' );
			};

			var removeClasses = function() {
				$body.removeClass( 'noselect resizing-' + axis );
				$activeHandle.removeClass( 'active' );
			};

			var handleResizeStart = function( e ) {
				e.stopPropagation();
				if ( e.which === 1 ) {
					$activeHandle = $( this );
					axis = this.className.match(/(handle|side)-(se|sw|ne|nw|n|e|s|w)/i)[2];
					layoutOrSelectionGroup = layout.selectionGroup ? layout.selectionGroup : layout;
					var originalPosition = element.position();
					originalState = {
						pageX : e.pageX,
						pageY : e.pageY,
						left : layoutOrSelectionGroup.percentLeft,
						top : layoutOrSelectionGroup.percentTop,
						width : layoutOrSelectionGroup.percentWidth,
						height : layoutOrSelectionGroup.percentHeight,
						parentWidth : $parentEl.width(),
						parentHeight : $parentEl.height(),
						scrollTop : $scrollContainer.scrollTop()
					};
					$window.on( 'mousemove.resizing', handleResize );
					$window.one( 'mouseup.resizing', handleResizeEnd );
				}
			};

			var handleResize = function( e ) {
				e.stopPropagation();
				var deltaX = e.pageX - originalState.pageX;
				var deltaY = e.pageY - originalState.pageY + $scrollContainer.scrollTop() - originalState.scrollTop;
				var deltaXInPercent = math.percent( deltaX, originalState.parentWidth );
				var deltaYInPercent = math.percent( deltaY, originalState.parentHeight );

				var newProperties = resizeFns[ axis ]( originalState, deltaXInPercent, deltaYInPercent );
				newProperties = convertToPercentProps( newProperties );
				// TODO: there may be a potential optimization here to have the layout object
				// tell us if the setProperties call resulted in a change in the layout model.
				// If so, we could then call scope.$apply() to start the digest cycle. This may
				// go against angular's way of watching for model changes, but it may be worth it
				// for this case where there may be 20-30 invocations of this handler before the
				// model actually changes due to the model's position correction logic.
				//
				// Example:
				//
				// if ( scope.layout.setProperties( newProperties ) ) {
				// 	scope.$apply();
				// }
				scope.$apply( function() {
					if ( !layoutOrSelectionGroup.resizing ) {
						layoutOrSelectionGroup.setState( { resizing : true } );
						scope.$emit( 'bresizing', true );
						addClasses();
					}
					layoutOrSelectionGroup.setProperties( newProperties, {
						resize : true,
						axis : axis
					});
				} );
			};

			var handleResizeEnd = function( e ) {
				removeClasses();
				$window.off( '.resizing' );
				scope.$apply( function() {
					layoutOrSelectionGroup.setState( {
						resized : true,
						resizing : false
					} );
					scope.$emit( 'bresizing', false );
				} );
			};

			var handleModeChange = function() {
				if( scope.editMode && !scope.promptSelected ) {
					$handles.on( 'mousedown.bResizable', handleResizeStart );
				} else {
					$handles.off( 'mousedown.bResizable' );
				}
			};
			scope.$watch( 'editMode', handleModeChange );
			scope.$watch( 'promptSelected', handleModeChange );
		}
	};

} );
