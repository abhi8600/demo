angular.module( 'directives.bDropTarget', [ 'services.math' ] ).

directive( 'bDropTarget', function( $parse, math ) {
	'use strict';
	return {
		link: function( scope, element, attrs ) {
			element.on( 'dragenter', function( e ) {
				element.addClass( 'over' );
			} );
			element.on( 'dragover', function( e ) {
				e.preventDefault();
				return false;
			} );
			element.on( 'dragleave', function( e ) {
				element.removeClass( 'over' );
			} );

			var fn = $parse( attrs.bDropTarget );

			element.on( 'drop', function( e ) {
				e.preventDefault();
				var data = e.originalEvent.dataTransfer.getData( 'application/json' );
				if( data.indexOf( '{' ) === 0 ) {
					var droppedObject = JSON.parse( data );
					var canvasOffset = element.offset();
					var xPosOnCanvas = e.originalEvent.pageX - canvasOffset.left;
					var yPosOnCanvas = e.originalEvent.pageY - canvasOffset.top + element.scrollTop();
					var dropCoordinates = {
						x : math.percent( xPosOnCanvas, element.width() ),
						y : math.percent( yPosOnCanvas, element.height() )
					};
					scope.$apply( function() {
						fn( scope, {
							$dropCoordinates : dropCoordinates,
							$droppedObject : droppedObject
						} );
					} );
				}
			} );
		}
	};
} );

