angular.module( 'directives.lang', [] )

.directive( 'lang', function( $rootScope ) {
	'use strict';
	return {
		link : function( scope, el, attrs ) {
			$rootScope.locale = attrs.lang || 'en_US';
			$rootScope.$watch( 'locale', function() {
				el.attr( 'lang', $rootScope.locale );
			} );
		}
	};
} );
