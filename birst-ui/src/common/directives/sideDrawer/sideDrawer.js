angular.module( 'directives.sideDrawer', [] )

.directive( 'sideDrawer', function() {
	'use strict';
	return {
		restrict : 'C',
		link : function( scope, el, attrs ) {
			var btnEl = $( '<button class="btn side-drawer-tab"><i class="shape-triangle-up"></i></button>' );
			el.append( btnEl );
			btnEl.on( 'click', function() {
				scope.toggleDrawer();
			} );
			scope.toggleDrawer = function() {
				el.toggleClass( 'side-drawer-open' );
			};
		}
	};
} );
