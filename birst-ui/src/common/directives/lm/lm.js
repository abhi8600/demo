angular.module( 'directives.lm', [ 'services.localizedMessages' ] )

.directive( 'lm', function( lm ) {
	'use strict';
	return {
		restrict : 'AE',
		link : function( scope, element, attrs ) {
			var key = ( attrs.lm ? attrs.lm : element.text() ).trim();
			element.html( lm.get( key ) );
			scope.$on( 'localechange', function() {
				element.html( lm.get( key ) );
			} );
		}
	};
} );
