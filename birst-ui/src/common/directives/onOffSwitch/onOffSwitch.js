angular.module( 'directives.onOffSwitch', [] )

.directive( 'onOffSwitch', function( $timeout ) {
	'use strict';
	return {
		restrict : 'EA',
		replace : true,
		require : '?ngModel',
		scope : {
			'default': '=',
			'onChange': '&'
		},
		templateUrl : 'directives/onOffSwitch/onOffSwitch.tpl.html',
		link : function( scope, el, attrs, ngModel ) {
			if( !ngModel ) { return; }

			var $inpEl = el.children( 'input' );

			var handleChange = function( e ) {
				scope.$apply( function() {
					ngModel.$setViewValue( $inpEl.prop( 'checked' ) );
					scope.onChange();
				} );
			};

			// generate id for input id field / label for field
			scope.iOSToggleCounter = _.uniqueId( 'onoff_' );

			// model -> view
			ngModel.$render = function() {
				$inpEl.prop( 'checked', ngModel.$viewValue || false );
				scope.onChange();
			};

			// initialize default
			if( typeof scope.default !== 'undefined' && _.isBoolean( scope.default ) ) {
				ngModel.$setViewValue( scope.default );
				ngModel.$render();
			}

			$inpEl.on( 'change', handleChange );
		}
	};
} );
