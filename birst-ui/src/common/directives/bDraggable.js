angular.module( 'directives.bDraggable', [ ] ).

directive( 'bDraggable', function() {
	'use strict';

	return {
		link: function( scope, element, attrs ) {
			element.attr( 'draggable', 'true' ); // decorate element with HTML5 Drag and Drop's draggable attribute
			element.on( 'dragstart', function( e ) {
				e.originalEvent.dataTransfer.setData('application/json', JSON.stringify( scope.report ) );
			} );
		}
	};
} );

