angular.module( 'directives.bSelectable', [ 'models.selectionGroup', 'models.hashMap' ] )

.directive( 'bSelectable', function( SelectionGroup, HashMap, $rootScope, $parse ) {
	'use strict';

	var CTRL_KEYCODE = 17;
	var COMMAND_KEYCODE = 91;
	var ESC_KEYCODE = 27;
	var MULTI_SELECT_KEYCODES = [ CTRL_KEYCODE, COMMAND_KEYCODE ];
	var BACKSPACE_KEYCODE = 8;
	var DELETE_KEYCODE = 46;
	var DELETE_KEYCODES = [ BACKSPACE_KEYCODE, DELETE_KEYCODE ];
	var SELECTED = 'selected';

	var $window = $( window );
	var selectionGroup = new SelectionGroup();
	var selectedModelScopes = new HashMap();
	var windowListenersEnabled = false;
	var multiSelectKeyDown;

	var clearSelections = function() {
		selectionGroup.clear();
		selectedModelScopes.clear();
	};

	var addSelection = function( model, scope ) {
		selectionGroup.add( model );
		selectedModelScopes.set( model, scope );
	};

	var removeSelection = function( model ) {
		selectionGroup.remove( model );
		selectedModelScopes.remove( model );
	};

	var getModelScope = function( model ) {
		return selectedModelScopes.get( model );
	};

	return {
		link : function( scope, element, attrs) {

			var model = scope.$eval( attrs.bSelectable );
			var onSelectCallbackFn = $parse( attrs.bOnSelect );
			var onDeleteCallbackFn = $parse( attrs.bOnDelete );

			scope.$watch( attrs.bSelectable + '.selected', function( newVal, oldVal ) {
				if ( newVal ) {
					element.addClass( SELECTED );
				} else {
					element.removeClass( SELECTED );
				}
			} );

			scope.$watch( attrs.bSelectable + '.zIndex', function( newVal, oldVal ) {
				element.css( {
					zIndex : newVal
				} );
			} );

			element.on( 'mousedown', function( e ) {
				if ( e.which === 1 ) {
					e.stopPropagation();
					scope.$apply( function() {
						if ( !model.selected && !multiSelectKeyDown ) {
							clearSelections();
							addSelection( model, scope );
							onSelectCallbackFn( scope );
						} else if ( model.selected && multiSelectKeyDown ) {
							removeSelection( model );
						} else {
							addSelection( model, scope );
							onSelectCallbackFn( scope );
						}
					} );
				}
			} );

			// Here we set up a single set of event listeners on the window object when the
			// first bSelectable instance is instantiated.
			//
			// NOTE: $rootScope was required here given there is only one set of window listeners
			// for the collection of bSelectable instances. If we used that instance's scope
			// local variable, we could delete that instance and it's scope.$apply would refer to
			// angular.noop.
			if ( !windowListenersEnabled ) {
				$window.on( 'mousedown', function( e ) {
					if ( e.which === 1 ) {
						$rootScope.$apply( function() {
							clearSelections();
						} );
					}
				} );
				$window.on( 'keydown', function( e ) {
					if ( _.contains( DELETE_KEYCODES, e.which ) && !_.isEmpty( selectionGroup.elements ) ) {
						$rootScope.$apply( function() {
							_.each( selectionGroup.elements, function( element ) {
								onDeleteCallbackFn( getModelScope( element ), element );
							} );
							clearSelections();
						} );
						return false; // ensure the browser doesn't back navigate
					} else if ( e.which === ESC_KEYCODE && !_.isEmpty( selectionGroup.elements ) ) {
						$rootScope.$apply( function() {
							clearSelections();
						} );
					} else {
						multiSelectKeyDown = _.contains( MULTI_SELECT_KEYCODES, e.which );
					}
				} );
				$window.on( 'keyup', function( e ) {
					if ( _.contains( MULTI_SELECT_KEYCODES, e.which ) ) {
						multiSelectKeyDown = false;
					}
				} );
				windowListenersEnabled = true;
			}
		}
	};

} );
