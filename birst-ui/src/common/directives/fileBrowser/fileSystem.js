angular.module( 'directives.fileBrowser.fileSystemB', [ 'services.localizedMessages', 'models.file', 'models.searchSession' ] ).

directive( 'fileSystemB', function( lm, File, SearchSession, $document, $window, $filter ) {
	'use strict';

	var ERROR = 'error';
	var QUERY_TIMED_OUT = 'query timed out';
	var EMPTY_STRING = '';
	var EMPTY_MSG = lm.get( 'MSG_EMPTY_DIRECTORY' );
	var NO_SEARCH_RESULTS = lm.get( 'NO_SEARCH_RESULTS' );
	var TIMEOUT_MSG = lm.get( 'ERR_QUERY_TO' );
	var ERR_UNKNOWN_MSG = lm.get( 'ERR_UNKNOWN' );
	var SEARCH_PLACEHOLDER_MSG = lm.get( 'SEARCH_PLACEHOLDER' );
	var NARROW_RESULTS_MSG = lm.get( 'NARROW_RESULTS' );
	var YOUVE_SELECTED_MSG = lm.get('YOUVE_SELECTED');
	var REMOVE_ALL_MSG = lm.get('REMOVE_ALL');
	var TYPING_WAIT = 300;

	var $body = $document.find( 'body' );

	return {
		link : function( scope, element, attrs ) {
			scope.selectedFiles = [];
			scope.processSelectedFiles = false;
			scope.emptyMsg = EMPTY_MSG;
			scope.noSearchResultsMsg = NO_SEARCH_RESULTS;
			scope.searchPlaceholderMsg = SEARCH_PLACEHOLDER_MSG;

			var handleError = function( cause ) {
				if ( cause === QUERY_TIMED_OUT ) {
					scope.$broadcast( ERROR, {
						basicText : TIMEOUT_MSG,
						moreInfoText : EMPTY_STRING
					} );
				} else {
					scope.$broadcast( ERROR, {
						basicText: window.errors ? window.errors[cause] : ERR_UNKNOWN_MSG,
						moreInfoText: EMPTY_STRING
					});
				}
			};

			var trimMe = function( element, index, array ) {
				return element.trim();
			};

			var whiteList = attrs.hasOwnProperty( 'fsWhitelist' ) ? attrs.fsWhitelist.split( ',' ).map( trimMe ) : [];
			scope.fileSystem = {

				isNavigatorOpen: false,
				activeDirectory: '/shared',
				modal: {},
				multiSelectOn : attrs.hasOwnProperty( 'fsMulti' ),
				closeButtonOn : attrs.hasOwnProperty( 'fsClose' ),
				buttonPanelOn : attrs.hasOwnProperty( 'fsButtons' ),
				renameOn :  attrs.hasOwnProperty( 'fsRename' ),
				deleteOn :  attrs.hasOwnProperty( 'fsDelete' ),
				editOn : attrs.hasOwnProperty( 'fsEdit' ),
				searchOn : attrs.hasOwnProperty( 'fsSearch' ),

				startRename : function( file ) {
					file.editing = !file.editing;
					file.newName = file.reportName;
				},

				confirmRename : function( file ) {
					if ( !file.fileName && !file.reportName && !file.modified ) {
						// create new folder
						File.createDirectory( {
							dirName : scope.fileSystem.activeDirectory,
							directory : file.newName
						} )
						.then(
							function() {
								scope.fileSystem.openDirectory( scope.fileSystem.activeDirectory );
							},
							handleError
						);
					} else {
						// actually rename
						var oldName = file.fileName;

						file.editing = !file.editing;
						//TODO: make work for all file types
						file.fileName = file.type === 'file' ? file.newName + '.viz.dashlet' : file.newName;
						//file.fileName = getUniqueFileName(file);

						File.renameFile( {
							directory : scope.activeDirectory,
							oldName : oldName,
							newName : file.fileName
						} )

						.then( angular.noop, handleError );
					}
				},

				delete : function( file ) {
					scope.fileSystem.activeFile = file;
					scope.fileSystem.modal = {
						showModal : true,
						title : lm.get( 'TITLE_DELETE_CONFIRM' ),
						text : lm.get( 'MSG_DELETE_CONFIRM' ),
						successButtonText : lm.get( 'LB_DELETE' ),
						successCallback : function() {
							scope.fileSystem.closeModal();
							File.deleteFile( {
								directory : scope.fileSystem.activeDirectory,
								fileName : scope.fileSystem.activeFile.fileName
							} ).then( function() {
								scope.fileSystem.currentDirectoryList = _.without( scope.fileSystem.currentDirectoryList, scope.fileSystem.activeFile );
							}, handleError );
						},
						cancelCallback: scope.fileSystem.closeModal
					};
				},

				closeModal : function() {
					scope.fileSystem.modal.showModal = false;
				},

				openNavigator : function( mode ) {
					var that = this;
					if( scope.searchSession ) {
						scope.searchSession.reset();
					}
					if ( mode === 'open' ) {
						scope.fileSystem.navigatorMode = 'open';
					}

					if( $( document ).height() > $( window ).height() ) {
						$body.addClass( 'disable-scroll' );
					}

					setTimeout( function() {
						scope.$apply( function() {
							scope.fileSystem.isNavigatorOpen = true;
							element.find( '[file-navigator-b]' ).scrollTop( 0 );
						} );
					}, 50 );

					File.getVisibleFolderStructure( { writeable : false } )

					.then( function( folderStructure ) {
						scope.fileSystem.folderStructure = folderStructure;
						// var activeTarget = _.find( folderStructure.children, { path : scope.fileSystem.activeDirectory } );
						// if( activeTarget ) {
						// 	activeTarget.open = true;
						// }
						scope.fileSystem.openDirectory( scope.fileSystem.activeDirectory );
					}, handleError );
				},

				closeNavigator : function( processFiles ) {
					scope.fileSystem.isNavigatorOpen = false;
					$body.removeClass( 'disable-scroll' );
					if( processFiles ) {
						setTimeout( function() {
							scope.$apply( function() {
								scope.processSelectedFiles = true;
							} );
						}, 300 );
					} else {
						this.purgeSelected();
					}
				},

				/*TEMPOMIT: getUserPreferences: function() {
					preferencesService.getUserPreferences(function($preferences) {
						scope.fileSystem.activeDirectory = $preferences.children('Value').text();
					}, handleError);
				},*/

				purgeSelected : function() {
					scope.selectedFiles = [];
				},

				openDirectory : function( directory ) {
					directory = directory || scope.fileSystem.activeDirectory;
					scope.opening = true;
					//Items must be passed in the proper order for this soap action
					File.getDirectory( [
						{ key : 'dir', value : directory },
						{ key : 'writeable', value : 'false' }
					] ).then( function( files ) {
						_.each( files, function( file )  {
							if( file.isFile() ){
								file.fullPath = directory + '/' + file.fileName;
							}
						} );
						scope.fileSystem.currentDirectoryList = $filter( 'filter' )( files, function( f ) { return whiteList.indexOf( f.fileType ) !== -1; } );
					}, handleError )
					.finally( function() {
						scope.opening = false;
					} );
					scope.fileSystem.folderStructure.walkFolderTree( scope.fileSystem.activeDirectory );
				},

				isSelected : function( file ) {
					return _.find( scope.selectedFiles, { fullPath : file.fullPath } ) !== undefined;
				},

				setDirectory : function( directory ) {
					directory.open = !directory.open;
					if ( scope.fileSystem.activeDirectory !== directory.path ) {
						scope.fileSystem.activeDirectory = directory.path;
						scope.fileSystem.currentDirectoryList = scope.fileSystem.openDirectory( scope.fileSystem.activeDirectory );
					}
				},

				openFile : function( file, $event ) {
					var $target = $( $event.target );
					var $cb = $( $event.delegateTarget ).find( 'input[type=checkbox]' );
					if ( file.type === 'folder' ) {
						scope.fileSystem.activeDirectory = scope.fileSystem.activeDirectory === '/' ? '/' + file.reportName : scope.fileSystem.activeDirectory + '/' + file.reportName;
						var folderClicked = _.recursiveSearch( scope.fileSystem.folderStructure, 'children', 'path', scope.fileSystem.activeDirectory );
						if( folderClicked ) {
							folderClicked.open = true;
						}

						scope.fileSystem.openDirectory( scope.fileSystem.activeDirectory );
					} else {
						if( this.multiSelectOn ) {
							if( $target.is( 'label' ) ) {
								$event.preventDefault();
							}
							$cb.prop( 'checked', !$cb.prop( 'checked' ) );
							this.toggleSelection( file, $cb.prop( 'checked' ) );
						} else {
							if ( scope.fileSystem.navigatorMode === 'save' ) {
								scope.fileSystem.newFileName = file.reportName;
								return;
							}
							scope.fileSystem.isNavigatorOpen = false;
							scope.fileSystem.open( scope.fileSystem.activeDirectory, file.reportName );
						}
					}
				},

				toggleSelection : function( file, checked ) {
					if( checked ) {
						scope.selectedFiles.push( file );
					} else {
						_.remove( scope.selectedFiles, function( item ) {
							return item.fullPath === file.fullPath;
						} );
					}
				},

				newFolder: function() {
					scope.fileSystem.currentDirectoryList.unshift( new File( {
						fileName: null,
						reportName: null,
						newName: '(new folder)',
						type: 'folder',
						owner: null,
						modified: null,
						dashboard: null,
						write: 'true',
						editing: true
					} ) );
				},

				getSortType: function() {
					return 'Recent';
				}
			};

			scope.searchSession = scope.fileSystem.searchOn ? new SearchSession( File ) : undefined;

			var searchTimer;
			if( scope.fileSystem.searchOn )  {
				scope.$watch( 'searchSession.query', function( newVal, oldVal ) {
					if ( newVal ) {
						clearTimeout( searchTimer );
						scope.searchSession.waiting = true;
						scope.searchSession.clearSelectedFilters( {
							preventSearch : true
						} );
						searchTimer = setTimeout( function() {
							scope.searchSession.search();
						}, TYPING_WAIT );
						scope.searchIsActive = true;
					} else {
						scope.searchSession.reset();
						scope.searchIsActive = false;
					}
				} );

				scope.showPreviousSearchResults = function() {
					scope.searchIsActive = !_.isEmpty( scope.searchSession.hits );
				};
			}

			scope.narrowResultsHeading = NARROW_RESULTS_MSG;
			scope.selectedFiltersHeading = YOUVE_SELECTED_MSG;
			scope.removeAllCopy = REMOVE_ALL_MSG;
		}
	};
} )
.directive( 'fileSystemUiB', function() {
	'use strict';
	return {
		replace: true,
		templateUrl: 'directives/fileBrowser/fileSystemUi.tpl.html'
	};
} )

.directive( 'fileSystemTreeFolder', function() {
	'use strict';
	return {
		templateUrl : 'directives/fileBrowser/fileSystem.tpl.html'
	};
} );
