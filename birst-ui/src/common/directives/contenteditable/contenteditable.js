/**
 * ADAPTED FROM ....
 * @see http://docs.angularjs.org/guide/concepts
 * @see http://docs.angularjs.org/api/ng.directive:ngModel.NgModelController
 * @see https://github.com/angular/angular.js/issues/528#issuecomment-7573166
 */

angular.module( 'contenteditable', [] )

.directive( 'contenteditable', function( $timeout, $filter, $log ) {
	'use strict';
	return {
		restrict : 'A',
		require : '?ngModel',
		link : function( $scope, $element, attrs, ngModel ) {
			var saved;
			$scope.$on( 'saved', function( nge, val ) {
				if( ngModel.$viewValue === val ) {
					saved = ngModel.$viewValue;
					ngModel.$setPristine();
				}
			} );
			// don't do anything unless this is actually bound to a model
			if( !ngModel ) {
				return;
			}
			var promise;
			var limit = attrs.hasOwnProperty( 'limitTo' ) ? parseInt( attrs.limitTo ) : undefined;
			var hasLimit = typeof limit === 'number' ? true : false;

			var handleInput = function( e ) {

				// If it is triggerd by an input, disable the keydown listener, yay!!!
				if( e.type === 'input' ) {
					$timeout.cancel( promise );
					$element.off( 'keydown' );
				}

				$scope.$apply( function() {
					var html, html2, rerender;
					html = $element.html();
					rerender = false;

					if( attrs.stripBr && attrs.stripBr !== 'false' ) {
						html = html.replace( /<br>$/, '' );
					}

					if( attrs.noLineBreaks && attrs.noLineBreaks !== 'false' ) {
						html2 = html.replace( /<div>/g, '' ).replace( /<br>/g, '' ).replace( /<\/div>/g, '' );
						if( html2 !== html ) {
							rerender = true;
							html = html2;
						}
					}

					if( hasLimit ) {
						// Remove arbitrarily added nonbreaking spaces, because they will be counted in string length.
						html = html.replace( '&nbsp;',  ' ' );
						html2 = $filter( 'limitTo' )( html, limit );
						if( html2 !== html && html.length >= limit ) {
							rerender = true;
							html = html2;
						}
					}

					ngModel.$setViewValue( html );
					if( rerender ) {
						ngModel.$render();
					}

					if( html === saved ) {
						ngModel.$setPristine();
					}

					if( html === '' ) {
						// Set validity if not empty is requested
						if( attrs.notEmpty === 'true' ) {
							ngModel.$setValidity( 'empty', false );
						}
						// the cursor disappears if the contents is empty
						// so we need to refocus
						$timeout( function() {
							$element[0].blur();
							$element[0].focus();
						} );
					} else {
						ngModel.$setValidity( 'empty', true );
					}
				} );
			};

			// view -> model
			$element.on( 'input', handleInput );

			$element.on( 'paste', function( e ) {
				e.preventDefault();
				var pastedText;
				if ( window.clipboardData && window.clipboardData.getData ) { // IE
					pastedText = window.clipboardData.getData( 'Text' );
				} else if ( e.originalEvent.clipboardData && e.originalEvent.clipboardData.getData ) {
					pastedText = e.originalEvent.clipboardData.getData( 'text/plain' );
				}
				ngModel.$setViewValue( ngModel.$viewValue + pastedText );
				ngModel.$render();
			} );

			// IE does not support input
			$element.on( 'keydown', function( e ) {
				promise = $timeout( function() {
					handleInput.call( this, e );
				});
			} );

			// model -> view
			var oldRender = ngModel.$render;
			ngModel.$render = function() {
				var el, el2, range, sel;
				if( !!oldRender ) {
					oldRender();
				}
				if( !saved ) {
					saved = ngModel.$viewValue;
				}
				$element.html( ngModel.$viewValue || '' );
				el = $element[0];
				range = document.createRange();
				sel = window.getSelection();
				if( el.childNodes.length > 0 ) {
					el2 = el.childNodes[ el.childNodes.length - 1 ];
					range.setStartAfter( el2 );
				} else {
					range.setStartAfter( el );
				}
				range.collapse( true );
				sel.removeAllRanges();
				sel.addRange( range );
			};
			if( attrs.selectNonEditable && attrs.selectNonEditable !== 'false' ) {
				$element.bind('click', function(e) {
					var range, sel, target;
					target = e.toElement;
					if( target !== this && angular.element( target ).attr( 'contenteditable' ) === 'false' ) {
						range = document.createRange();
						sel = window.getSelection();
						range.setStartBefore( target );
						range.setEndAfter( target );
						sel.removeAllRanges();
						sel.addRange( range );
					}
				} );
			}
		}
	};
} );
