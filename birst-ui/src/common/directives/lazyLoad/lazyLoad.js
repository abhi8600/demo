/*angular.module( 'directives.lazyLoad', [] )

.directive('lazyLoad', ['$window', '$q', function ( $window, $q, $log ) {
	'use strict';

	function loadScript( src ) {
		var s = document.createElement('script'); // use global document since Angular's $document is weak
		s.src = src;
		document.body.appendChild(s);
	}

	function lazyLoadApi( src ) {
		var deferred = $q.defer();
		$window.initialize = function () {
			deferred.resolve();
		};
		// thanks to Emil Stenström: http://friendlybit.com/js/lazy-loading-asyncronous-javascript/
		if ( $window.attachEvent ) {
			$window.attachEvent( 'onload', function() {
				loadScript( src );
			} );
		} else {
			$window.addEventListener( 'load', function() {
				loadScript( src );
			}, false );
		}
		return deferred.promise;
	}

	return {
		restrict: 'A',
		link: function ( scope, element, attrs ) {
			scope.lazyLoaded = scope.lazyloaded || {};
			var src = attrs.lazyLoadPath;
			if ( scope.lazyLoaded.hasOwnProperty( src ) ) {
				scope.lazyLoaded[src] = $q.defer();
			} else {
				lazyLoadApi( src ).then( function () {
					scope.lazyLoaded[src].resolve();
				}, function () {
					scope.lazyLoaded[src].reject();
				});
			}
		}
	};
}]);*/
