angular.module( 'directives.bMoveable', [ 'services.math' ] )

.directive( 'bMoveable', function( $parse, math ) {
	'use strict';
	var PERCENT_SYMBOL = '%';
	var MOVING_CLASS = 'moving';

	var $scrollContainer = $( '.mcont > ui-view' );
	var $body = $( 'body' );
	var $window = $( window );

	return {

		link : function( scope, el, attrs) {
			var layout = scope.layout;
			var $parentEl = el.parent();
			var swappable = attrs.swappable === 'true';
			var layoutOrSelectionGroup, originalState;

			// Because bResizable directive also has a watch on percentLeft and percentTop values,
			// we'll only update the DOM here if this dashlet instance is being moved by the user.
			scope.$watchCollection( '[layout.percentLeft, layout.percentTop]', function( newVals, oldVals ) {
				if ( layout.moving && newVals ) {
					el.css( {
						left : layout.percentLeft + PERCENT_SYMBOL,
						top : layout.percentTop + PERCENT_SYMBOL
					} );
				}
			} );

			scope.$watch( 'layout.moving', function( newVal, oldVal) {
				if ( newVal ) {
					el.addClass( MOVING_CLASS );
				} else {
					el.removeClass( MOVING_CLASS );
				}
			} );

			var addClasses = function() {
				$body.addClass( 'noselect' );
			};

			var removeClasses = function() {
				$body.removeClass( 'noselect' );
			};

			var handleDragStart = function( e ) {
				if ( e.which === 1 ) {
					layoutOrSelectionGroup = layout.selectionGroup ? layout.selectionGroup : layout;
					originalState = {
						pageX : e.pageX,
						pageY : e.pageY,
						left : layoutOrSelectionGroup.percentLeft,
						top : layoutOrSelectionGroup.percentTop,
						parentWidth : $parentEl.width(),
						parentHeight : $parentEl.height(),
						scrollTop : $scrollContainer.scrollTop()
					};
					$window.on( 'mousemove.draglayout', handleDrag );
					$window.one( 'mouseup.draglayout', handleDragEnd );
				}
			};

			var handleDrag = function( e ) {
				var deltaX = e.pageX - originalState.pageX;
				var deltaY = e.pageY - originalState.pageY + $scrollContainer.scrollTop() - originalState.scrollTop;
				var deltaXInPercent = math.percent( deltaX, originalState.parentWidth );
				var deltaYInPercent = math.percent( deltaY, originalState.parentHeight );

				scope.$apply( function() {
					if ( !layoutOrSelectionGroup.moving ) {
						layoutOrSelectionGroup.setState( { moving : true } );
						scope.$emit( 'bmoving', true );
						addClasses();
					}
					layoutOrSelectionGroup.setProperties( {
						percentLeft : originalState.left + deltaXInPercent,
						percentTop : originalState.top + deltaYInPercent
					} );
				} );
			};

			var handleDragEnd = function( e ) {
				removeClasses();
				$window.off( '.draglayout' );
				scope.$apply( function() {
					layoutOrSelectionGroup.setState( { moving : false } );
					scope.$emit( 'bmoving', false );
				} );
			};

			var modeChange = function() {
				if( scope.editMode && !scope.promptSelected ) {
					el.on( 'mousedown.bMoveable', handleDragStart );
				} else {
					el.off( 'mousedown.bMoveable' );
				}
			};

			scope.$watch( 'editMode', modeChange );
			scope.$watch( 'promptSelected', modeChange );
		}
	};

} );
