/* global Microsoft */
angular.module( 'directives.bingMap', [] )

.directive( 'bingMap', [ 'APP_CONFIG', function( conf ) {
	'use strict';

	return {
		link : function( scope, el, attrs ) {
			var mapEvents = {
				//When user clicks on a marker or polygon / region, we check if it is a drill to
				//dashboard.
				onClick : function( e ){
					if( e.originalEvent ){
						var type = e.targetType;
						if( type === 'pushpin' || type === 'polygon' ){
							var polygon = e.target.trueEntity;
							var drillCfg = this.nameDrill[ polygon.drillKey ];
							if( drillCfg ){
								var dashParts = drillCfg.targetURL.split( ',' );
								if( dashParts.length > 1 ) {
									//TODO: TEMPOMIT ctrlr.navigateToDash( dashParts[0], dashParts[1], drillCfg.filters );
									//TODO: Trigger Route when we have dashboards working.
									console.log( 'drill to dash ' + dashParts[0] + ' ' + dashParts[1] + ' ' + drillCfg.filters );
								}
							}
						}
					}
				},

				pointToLocation : function( e ) {
					var point = new Microsoft.Maps.Point( e.getX(), e.getY() );
					var loc = this.map.tryPixelToLocation( point );
					return new Microsoft.Maps.Location( loc.latitude, loc.longitude );
				},

				//When user clicks on map and ctrl key enabled, we check if are doing a visual filtering
				onMouseDown : function( e ){
					console.log( 'mousedown' );
					var evt = e.originalEvent;
					if( evt.ctrlKey && e.targetType === 'map' ){
						this.visualSelect = e.handled = true;
						this.selectStartLoc = this.pointToLocation( e );
					}else{
						e.handled = false;
					}
				},

				//After the user has visually select by releasing mouse, we check if any of our
				//markers are within the bounding box. Collect and filter on them
				onMouseUp : function( e ){
					if( this.visualSelect && this.selectPolygon ) {
						var locs = this.selectPolygon.getLocations();

						//Sometimes user select from the bottom up instead of top down
						var noFlip = this.selectStartLoc.latitude > this.selectEndLoc.latitude;
						var topLeft = noFlip ? this.selectStartLoc : this.selectEndLoc;
						var bottomRight = noFlip ? this.selectEndLoc : this.selectStartLoc;

						var selectedMarkers = [];
						for( var i = 0, ln = this.markers.length; i < ln; i++ ) {
							var marker = this.markers[ i ];
							var loc = marker.getLocation();
							var lon = loc.longitude;
							var lat = loc.latitude;
							if( topLeft.longitude > bottomRight.longitude ) {
								if( topLeft.longitude >= lon && bottomRight.longitude <= lon &&
								topLeft.latitude >= lat && bottomRight.latitude <= lat ) {
									selectedMarkers.push( marker );
								}
							} else {
								if( topLeft.longitude <= lon && bottomRight.longitude >= lon &&
								topLeft.latitude >= lat && bottomRight.latitude <= lat ){
									selectedMarkers.push( marker );
								}
							}
						}

						if( selectedMarkers.length > 0 ){
							var newPrompts = {
								column : []
							};
							for ( var n = 0, ln2 = selectedMarkers.length; n < ln2; n++ ) {
								var key = selectedMarkers[ n ].drillKey;
								var attributes = this.nameAttributes[ key ];
								if( attributes ) {
									// find columns and getFilterPrompts
									var column = attributes.dimension0 + '.' + attributes.label0;
									var value = attributes.value0;
									newPrompts.column.push( value );
								}
							}
							this.renderer.requestFilterPrompt( newPrompts );
						}
					}

					//Reset
					this.visualSelect = false;
					this.selectStartLoc = this.selectEndLoc = null;
					if( this.selectPolygon ) {
						this.map.entities.remove( this.selectPolygon );
						this.selectPolygon = null;
					}
					e.handled = false;
				},

				//As the mouse moves when we are visual filtering, create a new polyline rectangle each time
				onMouseMove : function( e ){
					var evt = e.originalEvent;
					if( evt.ctrlKey && this.visualSelect ) {
						e.handled = true;

						if( this.selectPolygon ){
							this.map.entities.remove( this.selectPolygon );
						}

						this.selectEndLoc = this.pointToLocation( e );
						if( this.selectEndLoc ) {
							var topRightLoc = new Microsoft.Maps.Location( this.selectStartLoc.latitude, this.selectEndLoc.longitude );
							var bottomLeftLoc = new Microsoft.Maps.Location( this.selectEndLoc.latitude, this.selectStartLoc.longitude );
							var polys = [ this.selectStartLoc, topRightLoc, this.selectEndLoc, bottomLeftLoc, this.selectStartLoc ];
							this.selectPolygon = new Microsoft.Maps.Polyline( polys, { strokeThickness : 1 } ); //new Microsoft.Maps.Polygon(polys, null);
							this.map.entities.push( this.selectPolygon );
						}
					}else{
						e.handled = false;
					}
				}
			};
			var bingMap = scope.map.bingMap;
			if( !bingMap.map ) {
				bingMap.markers = [];
				Microsoft.Maps.loadModule( 'Microsoft.Maps.Themes.BingTheme', {
					callback : function() {
						if( bingMap.static ) {
							console.log( bingMap );
							el.css( { width : bingMap.width, height : bingMap.height } );
						}
						bingMap.map = new Microsoft.Maps.Map( el[0], {
							credentials : conf.bingMapsApiKey,
							mapTypeId : Microsoft.Maps.MapTypeId.road,
							enableSearchLogo : false,
							enableClickableLogo : false,
							customizeOverlays : true,
							showBreadcrumb : false,
							theme : new Microsoft.Maps.Themes.BingTheme()
						});

						Microsoft.Maps.Events.addHandler( bingMap.map, 'click', _.bind(  mapEvents.onClick, bingMap ) );
						Microsoft.Maps.Events.addHandler( bingMap.map, 'mousedown', _.bind(  mapEvents.onMouseDown, bingMap ) );
						Microsoft.Maps.Events.addHandler( bingMap.map, 'mouseup', _.bind(  mapEvents.onMouseUp, bingMap ) );
						Microsoft.Maps.Events.addHandler( bingMap.map, 'mousemove', _.bind(  mapEvents.onMouseMove, bingMap ) );

						bingMap.canvasLayer = new Microsoft.Maps.EntityCollection();
						Microsoft.Maps.Events.addHandler( bingMap.canvasLayer, 'entityadded', function ( e ) {
							if ( e.entity._canvasID ) {
								e.entity.renderCanvas();
							}
						});
						bingMap.map.entities.push( bingMap.canvasLayer );

						Microsoft.Maps.Events.addHandler( bingMap.map, 'maptypechanged', function( e ) {
							for( var i = 0; i < bingMap.canvasLayer.getLength(); i++ ){
								var marker = bingMap.canvasLayer.get( i );
								if ( marker._canvasID ) {
									marker.renderCanvas();
								}
							}
						});

						bingMap.parseMarkersGetPlacemarks();
					}
				} );
			}

		}
	};

} ] );
