angular.module( 'directives.bcarousel', [] )

.directive( 'bcarousel', function( $log ) {
	'use strict';
	var rt = 'bc-shift-right';
	var lt = 'bc-shift-left';

	return {
		restrict : 'E',
		link : function( scope, el, attrs ) {
			var left = el.find( '.bc-left' );
			var right = el.find( '.bc-right' );
			scope.viewport = el.find( '.bc-viewport' );
			scope.currentPosition = 0;
			scope.lastPosition = function() {
				return scope.currentPosition >= scope.viewport.children().length - 1;
			};
			scope.shiftLeft = function() {
				scope.currentPosition -= 1;
			};
			scope.shiftRight = function() {
				scope.currentPosition += 1;
			};
			scope.$on( 'updateBCarousel', function( e, max ) {
				if( scope.currentPosition > max ) {
					console.log( 'here' );
					scope.currentPosition = max;
				}
			} );
		}
	};

} );
