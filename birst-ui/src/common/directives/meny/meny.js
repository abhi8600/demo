/* global Meny */
angular.module( 'directives.meny', [] )

.directive( 'meny', function() {
	'use strict';
	return {
		link : function( scope, el, attrs ) {
			// Cached jQuery Objects
			var $content = el.find( '.mcont' );
			var $nav = el.find( '.mnav' );
			var $body = $( 'body' );

			// Attribute Options
			var pos = attrs.mpos ||'left';
			var height = attrs.mht ? parseInt( attrs.mht, 10 ) : 200;
			var width = attrs.mwd ? parseInt( attrs.mwd, 10 ) : 200;
			var angle = attrs.mangle ? parseInt( attrs.mangle, 10 ) : 30;
			var threshold = attrs.mthresh ? parseInt( attrs.mthresh, 10 ) : 40;
			var overlap = attrs.moverlap ? parseInt( attrs.moverlap, 10 ) : 6;
			var transitionDuration = attrs.transitionDuration || '0.5s';
			var mouse = attrs.hasOwnProperty( 'mmouse' ) && attrs.mmouse !== 'false';
			var touch = attrs.hasOwnProperty( 'mtouch' ) && attrs.mtouch !== 'false';

			var meny = Meny.create( {
				// The element that will be animated in from off screen
				menuElement: $nav[0],

				// The contents that gets pushed aside while Meny is active
				contentsElement: $content[0],

				// The alignment of the menu (top/right/bottom/left)
				position: pos,

				// The height of the menu (when using top/bottom position)
				height: height,

				// The width of the menu (when using left/right position)
				width: width,

				// The angle at which the contents will rotate to.
				angle: angle,

				// The mouse distance from menu position which can trigger menu to open.
				threshold: threshold,

				// Width(in px) of the thin line you see on screen when menu is in closed position.
				overlap: overlap,

				// The total time taken by menu animation.
				transitionDuration: transitionDuration,

				// Transition style for menu animations
				transitionEasing: 'ease',

				// Gradient overlay for the contents
				gradient: 'rgba(0,0,0,0.20) 0%, rgba(0,0,0,0.65) 100%)',

				// Use mouse movement to automatically open/close
				mouse: mouse,

				// Use touch swipe events to open/close
				touch: touch
			} );

			function openMeny( e ) {
				e.preventDefault();
				if( !meny.isOpen() ) {
					meny.open();
					$body.on( 'click.meny', closeMeny );
				}
			}

			function closeMeny( e ) {
				if( meny.isOpen() ) {
					meny.close();
				}
				$body.off( 'click.meny' );
			}

			if( meny ) {

				meny.addEventListener( 'closed', function() {
					el.removeClass( 'meny-open' );
				} );

				meny.addEventListener( 'open', function() {
					el.addClass( 'meny-open' );
				} );

				el.on( 'click', '.btn-meny', openMeny );

				$content.add( $nav ).on( 'click', closeMeny );

			}
		}
	};

} );
