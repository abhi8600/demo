angular.module( 'services.xml', [] ).

factory( 'xml', function() {
	'use strict';
	return {
		str2Xml : function( str ){
			if ( window.DOMParser ) {
				var parser = new window.DOMParser();
				return parser.parseFromString( str, 'text/xml' );
			}

			var xmlDoc = new window.ActiveXObject( 'Microsoft.XMLDOM' );
			xmlDoc.async = false;
			xmlDoc.loadXML(str);
			return xmlDoc;
		},
		xml2Str : function( xmlNode ) {
			try {
				// Gecko- and Webkit-based browsers (Firefox, Chrome), Opera.
				return ( new window.XMLSerializer() ).serializeToString( xmlNode );
			} catch (e) {
				try {
					// Internet Explorer.
					return xmlNode.xml;
				} catch (e1) {
					//Other browsers without XML Serializer
					console.log( 'Xmlserializer not supported' );
				}
			}
			return false;
		},

		escapeXml : function( str ) {
			return str ? str.replace( /&/g, '&amp;' ).replace( /</g, '&lt;' ).replace( />/g, '&gt;' ).replace( /"/g, '&quot;' ) : '';
		},

		getXmlValue : function( node ) {
			var child = node.firstChild;
			if (child && child.nodeName === '#text') {
				if( child.nodeValue ) {
					return child.nodeValue;
				}
				return child.wholeText;
			}
			return null;
		},

		intValue : function( str ) {
			return str ? parseInt( str, 10 ) : 0;
		},

		getAttributeIntValue : function( item, key ) {
			return this.intValue( item.getAttribute( key ) );
		},

		getXmlChildValue : function( node, childName) {
			var child = node.getElementsByTagName( childName );
			if ( child && child.length > 0 ) {
				return this.getXmlValue( child[0] );
			}
			return null;
		},

		getRootPath : function() {
			if( !this.rootPath ) {
				var path = window.location.href;
				if( path.indexOf( 'dashboards2.0' ) > 0 ) {
					this.rootPath = '/dashboards2.0/';
				} else {
					this.rootPath = '';
				}
			}
			return this.rootPath;
		}
	};
} );
