describe( 'xml service', function() {
	'use strict';

	var xml;
	var xmlStr = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"><soapenv:Body><ns:getDirectoryResponse xmlns:ns="http://file.WebServices.successmetricsinc.com"><ns:return><com.successmetricsinc.WebServices.BirstWebServiceResult><ErrorCode>0</ErrorCode><ErrorMessage></ErrorMessage><Result><FileNode n="shared" l="shared" dir="true" w="true" m="2014-03-12 13:38:14"><ch><FileNode n="Auto-generated" l="Auto-generated" dir="true" w="true" m="2013-12-27 09:54:15" /><FileNode n="styles" l="styles" dir="true" w="true" m="2013-12-27 09:53:19" /><FileNode n="categories.viz.dashlet" l="categories" m="2014-02-25 10:58:31" /><FileNode n="myfirstvizreport.viz.dashlet" l="myfirstvizreport" m="2014-02-24 15:17:41" /><FileNode n="new test.viz.dashlet" l="new test" m="2014-02-25 11:06:02" /><FileNode n="Pivot Table Example.AdhocReport" l="Pivot Table Example" CreatedBy="rmcginty" m="2014-01-10 12:44:28" /><FileNode n="test report.viz.dashlet" l="test report" m="2014-02-24 16:31:06" /><FileNode n="testrename.viz.dashlet" l="testrename" m="2014-02-25 11:14:55" /></ch></FileNode></Result></com.successmetricsinc.WebServices.BirstWebServiceResult></ns:return></ns:getDirectoryResponse></soapenv:Body></soapenv:Envelope>';
	var xmlDoc = $.parseXML( xmlStr );
	var childValueSubject = $.parseXML( '<parent><child>childvalue</child><child2><grandchild>grandchild value</grandchild><child2></parent>' );

	beforeEach( function() {
		module( 'services.xml' );
		inject( function( _xml_ ) {
			xml = _xml_;
		} );
	} );

	it( 'should convert valid xml string to xml', function() {
		expect( $.isXMLDoc( xml.str2Xml( xmlStr ) ) ).toBe( true );
	} );

	it( 'should convert an xml doc to a string', function() {
		expect( typeof xml.xml2Str( xmlDoc ) ).toBe( 'string' );
	} );

	it( 'should get an xml value from an xml node', function() {
		expect( xml.getXmlValue( xmlDoc.getElementsByTagName( 'ErrorCode' )[0] ) ).toBe( '0' );
	} );

	it( 'should return a xml child node\'s elements value', function() {
		expect( xml.getXmlChildValue( childValueSubject, 'grandchild' ) ).toBe( 'grandchild value' );
	} );

	// NEED UNIT TESTING:

	// escapeXml : function( str ) {
	// 	return str ? str.replace( /&/g, '&amp;' ).replace( /</g, '&lt;' ).replace( />/g, '&gt;' ).replace( /"/g, '&quot;' ) : '';
	// },

	// getRootPath : function() {
	// 	if( !this.rootPath ) {
	// 		var path = window.location.href;
	// 		if( path.indexOf( 'dashboards2.0' ) > 0 ) {
	// 			this.rootPath = '/dashboards2.0/';
	// 		} else {
	// 			this.rootPath = '';
	// 		}
	// 	}
	// 	return this.rootPath;
	// }

} );

