/* global sinon */
describe('localized messages', function () {
	'use strict';
	var lm, i18nmessages = {};

	beforeEach(function () {
		angular.module( 'test', [ 'services.localizedMessages' ] ).value( 'I18NMESSAGES', i18nmessages );
		module('test');
		inject( function ( _lm_ ) {
			lm = _lm_;
		} );
	});

	it( 'should return a localized message if defined', function () {
		i18nmessages.existing = 'Existing message!';
		expect( lm.get( 'existing' ) ).toEqual( 'Existing message!' );
	});

	it( 'should return a message key surrounded by a question mark for non-existing messages', function () {
		expect( lm.get( 'non.existing' ) ).toEqual( '?non.existing?' );
	});

	it( 'should interpolate parameters', function () {
		i18nmessages.sth = 'en {{param}} us';
		expect(lm.get( 'sth', { param : 'value' } ) ).toEqual( 'en value us' );
	});

	it('should not break for missing params', function () {
		i18nmessages.sth = 'en {{param}} us';
		expect( lm.get( 'sth' ) ).toEqual( 'en  us' );
		expect( lm.get( 'sth', { other:'value' } )).toEqual( 'en  us' );
	});
});
