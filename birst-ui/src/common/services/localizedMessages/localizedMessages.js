angular.module( 'services.localizedMessages', [] )

.factory( 'lm', function ( $interpolate, I18NMESSAGES, $http, $rootScope ) {
	'use strict';
	// Keep a cache of english for defaults
	var cachedMessages = I18NMESSAGES;
	var messageStore = { 'en_US' : I18NMESSAGES };

	var handleNotFound = function ( msg, msgKey ) {
		return msg || '?' + msgKey + '?';
	};

	return {
		get : function ( msgKey, interpolateParams ) {
			var msg =  I18NMESSAGES[ msgKey ];
			if ( msg ) {
				return $interpolate( msg )( interpolateParams );
			} else {
				return handleNotFound( msg, msgKey );
			}
		},
		currentLocale : 'en_US',
		changeLocale : function( localeKey ) {
			I18NMESSAGES = messageStore[localeKey];
			this.currentLocale = localeKey;
			// Tell the app to switch
			$rootScope.$broadcast( 'localechange' );
		},
		setLocale : function( localeKey ) {
			var that = this;
			if( !messageStore.hasOwnProperty( localeKey ) ) {
				// Get the data
				$http.get( 'assets/lang/' + localeKey + '.json' )
				.then( function( localization ) {
					// If it exists set it to the current locale and add to the messageStore
					if( localization.data && _.isObject( localization.data ) ) {
						// Show english version if undefined
						messageStore[localeKey] = angular.extend( {}, cachedMessages, localization.data );
						that.changeLocale( localeKey );
					}
				} );
			} else {
				// Use previously saved locale
				this.changeLocale( localeKey );
			}
		}
	};
} );
