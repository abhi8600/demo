describe( 'soap service', function() {
	'use strict';

	var envelope = '<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">' +
	'<SOAP-ENV:Body><ns:FNAME xmlns:ns="NS">' +
	'<ns:KEY1>VALUE1</ns:KEY1>' +
	'<ns:KEY2>VALUE2</ns:KEY2>' +
	'</ns:FNAME></SOAP-ENV:Body></SOAP-ENV:Envelope>';

	var orderedParams = '<ns:KEY2>VALUE2</ns:KEY2><ns:KEY1>VALUE1</ns:KEY1>';

	var endpoint = 'ENDPOINT';
	var namespace = 'NS';
	var soap, SoapConstructor, soapResource, config;

	beforeEach( function() {
		module( 'services.soap' );
		inject( function( _soap_ ) {
			soap = _soap_;
		} );
	} );

	it( 'should return a soap resource', function() {
		SoapConstructor = soap( endpoint, namespace );
		expect( SoapConstructor.hasOwnProperty( 'request' ) ).toBe( true );
	} );

	it( 'should return an instance that extends an object', function() {
		soapResource = new SoapConstructor( { foo : 'bar' } );
		expect( soapResource.foo ).toBe( 'bar' );
	} );

	it( 'should add parameters to soap envelope in array format', function() {
		var arrConfig = SoapConstructor.getRequestConfig( 'FNAME', [ { key : 'KEY1', value : 'VALUE1' }, { key : 'KEY2', value : 'VALUE2' } ] );
		expect( arrConfig.data ).toBe( envelope );
	} );

	it( 'should add parameters to soap envelope in object format', function() {
		config = SoapConstructor.getRequestConfig( 'FNAME', { KEY1 : 'VALUE1', KEY2 : 'VALUE2' } );
		expect( config.data ).toBe( envelope );
	} );

	it( 'should return parameters in object format with a supplied order', function() {
		var paramStr = SoapConstructor.getOrderedParams( { KEY1 : 'VALUE1', KEY2 : 'VALUE2' }, [ 'KEY2', 'KEY1' ] );
		expect( paramStr ).toBe( orderedParams );
	} );

	it( 'should receive the header from the factory method', function() {
		expect( config.url ).toBe( endpoint );
	} );

} );
