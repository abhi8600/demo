angular.module( 'services.soap', [ 'services.xml', 'services.genericResource' ] ).

service( 'soap', function( xml, $http, genericResource ) {
	'use strict';

	var tpl = _.template( '<ns:<%= key %>><%= value %></ns:<%= key %>>' );

	var DEFAULT_SOAPACTION_PREFIX = 'urn:';

	var SoapServiceFactory = function( url, ns, actionPrefix, Class ) {

		actionPrefix = actionPrefix || DEFAULT_SOAPACTION_PREFIX;

		var SoapResource = genericResource( Class );

		SoapResource.request = function( fnName, args, confOverrides, argsOrder, cookie, scb, ecb ) {
			var config = $.extend( true, this.getRequestConfig( fnName, args, argsOrder, cookie ), confOverrides );
			return SoapResource.thenFactoryMethod( $http( config ), scb, ecb );
		};

		SoapResource.getRequestConfig = function( fnName, args, argsOrder, cookie ) {
			var envelope = SoapResource.createEnvelope( fnName, args, ns, argsOrder ),
				headers = {
					'Content-Type' : 'text/xml; charset=utf-8',
					'SOAPAction' : actionPrefix + fnName,
					'tns' : DEFAULT_SOAPACTION_PREFIX
				};

			if ( cookie ) {
				headers.Cookie = cookie;
			}

			return {
				url : url,
				method : 'POST',
				headers : headers,
				xsrfHeaderName : 'X-XSRF-TOKEN',
				xsrfCookieName : 'XSRF-TOKEN',
				data : envelope,
				timeout : 300000,
				transformResponse : function ( data, headers ) {
					return xml.str2Xml( data );
				}
			};
		};

		SoapResource.getOrderedParams = function( params, order ) {
			var paramXml = '';
			var isArr = _.isArray( params );
			if( isArr ) {
				_.each( params, function( value ) {
					_.each( value, function( val, key ) {
						value[key] = xml.escapeXml( val );
					} );
					paramXml += tpl( value );
				} );
			} else if( order ) {
				_.each( order, function( value ) {
					paramXml += tpl( { key : value, value :  xml.escapeXml( params[value] ) } );
				}, this );
			} else {
				_.each( params, function( value, key ) {
					paramXml += tpl( { key : key, value : xml.escapeXml( value ) } );
				} );
			}
			return paramXml;
		};

		SoapResource.createEnvelope = function( fnName, args, ns, argsOrder ) {
			var soap = '<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"><SOAP-ENV:Body><ns:';
			soap += fnName + ' xmlns:ns="' + ns + '">';
			soap += SoapResource.getOrderedParams( args, argsOrder );
			soap += '</ns:' + fnName + '></SOAP-ENV:Body></SOAP-ENV:Envelope>';
			return soap;
		};


		angular.extend( SoapResource.prototype, {
			//Instance methods here
		} );


		return SoapResource;
	};

	return SoapServiceFactory;
} );
