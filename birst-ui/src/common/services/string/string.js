'use strict';

angular.module( 'services.string', [] )

.factory( 'string', function() {

	return {
		capitalize : function( string ) {
			return string.charAt( 0 ).toUpperCase() + string.slice( 1 );
		}
	};

} );
