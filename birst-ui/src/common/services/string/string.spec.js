describe( 'string service', function() {
	'use strict';

	var string;

	beforeEach( function() {
		angular.module( 'test', [ 'services.string' ] );
		module( 'test' );
		inject( function( _string_ ) {
			string = _string_;
		} );
	} );

	it( 'should capitalize the first character in a string', function() {
		expect( string.capitalize( 'hello world' ) ).toBe( 'Hello world' );
	} );

} );