angular.module( 'services.iconClass', [] )

.factory( 'iconClass', function() {
	'use strict';

	var CHART_CLASSES = {
		areaspline : 'area-spline',
		bingmap : 'geo-map',
		geomap : 'geo-map',
		heatmap : 'heat-map',
		gauge : 'meter',
		newgauge : 'gauge',
		percentstackedbar : 'stacked-bar-100-perc',
		percentstackedcolumn : 'stacked-column-100-perc',
		rangebar : 'range-bar',
		rangecolumn : 'range-column',
		scatter : 'scatter-plot',
		stackedarea : 'stacked-area',
		stackedbar : 'stacked-bar',
		stackedcolumn : 'stacked-column',
		undefined : 'no-chart',
	};

	return {
		get : function( key ) {
			key = (key || '').toLowerCase();
			return CHART_CLASSES[ key ] || key;
		}
	};

} );