describe( 'boundary', function() {
	'use strict';

	var boundary, props;

	beforeEach( module( 'services.boundary' ) );

	beforeEach( inject( function( $injector ) {
		boundary = $injector.get( 'boundary' );
		props = {
			percentLeft : 0,
			percentTop : 0,
			percentWidth : 0,
			percentHeight : 0
		};
	} ) );

	describe( 'boundedProperties()', function() {

		describe( 'correction of percentLeft', function() {

			it( 'should correct percentLeft property to ensure it is > 0', function() {
				props.percentLeft = -10;
				var boundedProps = boundary.boundedProperties( props );
				expect( boundedProps.percentLeft ).toBe( 0 );
			} );

			it( 'should correct percentLeft property to ensure percentLeft and percentWidth combined is <= 100', function() {
				props.percentLeft = 60;
				props.percentWidth = 50;
				var boundedProps = boundary.boundedProperties( props );
				expect( boundedProps.percentLeft ).toBe( 50 );
			} );

			it( 'should NOT correct percentLeft property if it is 0', function() {
				props.percentLeft = 0;
				var boundedProps = boundary.boundedProperties( props );
				expect( boundedProps.percentLeft ).toBe( 0 );
			} );

			it( 'should NOT correct percentLeft property if it and percentWidth combined is <= 100', function() {
				props.percentLeft = 40;
				props.percentWidth = 60;
				var boundedProps = boundary.boundedProperties( props );
				expect( boundedProps.percentLeft ).toBe( 40 );
			} );

		} );

		describe( 'correction of percentTop', function() {

			it( 'should correct percentTop property to ensure it is > 0', function() {
				props.percentTop = -10;
				var boundedProps = boundary.boundedProperties( props );
				expect( boundedProps.percentTop ).toBe( 0 );
			} );

			it( 'should NOT correct percentTop property if it is 0', function() {
				props.percentTop = 0;
				var boundedProps = boundary.boundedProperties( props );
				expect( boundedProps.percentTop ).toBe( 0 );
			} );

			it( 'should NOT correct percentTop property if it is a positive number', function() {
				props.percentTop = 2000;
				var boundedProps = boundary.boundedProperties( props );
				expect( boundedProps.percentTop ).toBe( 2000 );
			} );

		} );

		describe( 'correction of percentWidth', function() {

			it( 'should correct percentWidth property to ensure it is >= 0', function() {
				props.percentWidth = -10;
				var boundedProps = boundary.boundedProperties( props );
				expect( boundedProps.percentWidth ).toBe( 0 );
			} );

			it( 'should correct percentWidth property to ensure percentWidth is <= 100', function() {
				props.percentWidth = 110;
				var boundedProps = boundary.boundedProperties( props );
				expect( boundedProps.percentWidth ).toBe( 100 );
			} );

			it( 'should NOT correct percentWidth property if it is 0', function() {
				props.percentWidth = 0;
				var boundedProps = boundary.boundedProperties( props );
				expect( boundedProps.percentWidth ).toBe( 0 );
			} );

			it( 'should NOT correct percentWidth property if it between 0 and 100', function() {
				props.percentWidth = 33;
				var boundedProps = boundary.boundedProperties( props );
				expect( boundedProps.percentWidth ).toBe( 33 );
			} );

			it( 'should NOT correct percentWidth property if it is 100', function() {
				props.percentWidth = 100;
				var boundedProps = boundary.boundedProperties( props );
				expect( boundedProps.percentWidth ).toBe( 100 );
			} );

		} );

		describe( 'correction of percentHeight', function() {

			it( 'should correct percentHeight property to ensure it is > 0', function() {
				props.percentHeight = -10;
				var boundedProps = boundary.boundedProperties( props );
				expect( boundedProps.percentHeight ).toBe( 0 );
			} );

			it( 'should NOT correct percentHeight property if it is 0', function() {
				props.percentHeight = 0;
				var boundedProps = boundary.boundedProperties( props );
				expect( boundedProps.percentHeight ).toBe( 0 );
			} );

			it( 'should NOT correct percentHeight property if it is a positive number', function() {
				props.percentHeight = 2000;
				var boundedProps = boundary.boundedProperties( props );
				expect( boundedProps.percentHeight ).toBe( 2000 );
			} );

		} );

	} );

	describe( 'boundedResize()', function() {

		var heightOnlyTest = function( axis ) {
			it( 'should correct percentHeight without modifying percentTop', function() {
				props.percentTop = 0;
				props.percentHeight = -10;
				var boundedProps = boundary.boundedResize( props, axis );
				expect( boundedProps.percentTop ).toBe( 0 );
				expect( boundedProps.percentHeight ).toBe( 0 );
			} );
		};

		var widthOnlyTest = function( axis ) {
			it( 'should correct percentWidth without modifying percentLeft', function() {
				props.percentLeft = 50;
				props.percentWidth = 80;
				var boundedProps = boundary.boundedResize( props, axis );
				expect( boundedProps.percentLeft ).toBe( props.percentLeft );
				expect( boundedProps.percentWidth ).toBe( 50 );
			} );
		};

		var topModifyHeightTest = function( axis ) {
			it( 'should correct percentTop and modify percentHeight as well', function() {
				props.percentTop = -10;
				props.percentHeight = 60;
				var boundedProps = boundary.boundedResize( props, axis );
				expect( boundedProps.percentTop ).toBe( 0 );
				expect( boundedProps.percentHeight ).toBe( 50 );
			} );
		};

		var leftModifyWidthTest = function( axis ) {
			it( 'should correct percentLeft and modify percentWidth as well', function() {
				props.percentLeft = -10;
				props.percentWidth = 60;
				var boundedProps = boundary.boundedResize( props, axis );
				expect( boundedProps.percentLeft ).toBe( 0 );
				expect( boundedProps.percentWidth ).toBe( 50 );
			} );
		};

		describe( 'with \'s\' axis', function() {

			heightOnlyTest( 's' );

		} );

		describe( 'with \'se\' axis', function() {

			var axis = 'se';
			heightOnlyTest( axis );
			widthOnlyTest( axis );

		} );

		describe( 'with \'e\' axis', function() {

			widthOnlyTest( 'e' );

		} );

		describe( 'with \'ne\' axis', function() {

			var axis = 'ne';
			topModifyHeightTest( axis );
			widthOnlyTest( axis );

		} );

		describe( 'with \'n\' axis', function() {

			var axis = 'n';
			topModifyHeightTest( axis );

		} );

		describe( 'with \'nw\' axis', function() {

			var axis = 'nw';
			topModifyHeightTest( axis );
			leftModifyWidthTest( axis );

		} );

		describe( 'with \'w\' axis', function() {

			leftModifyWidthTest( 'w' );

		} );

		describe( 'with \'sw\' axis', function() {

			var axis = 'sw';
			leftModifyWidthTest( axis );
			heightOnlyTest( axis );

		} );

	} );

});
