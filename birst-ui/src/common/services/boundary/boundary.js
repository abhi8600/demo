angular.module( 'services.boundary', [] )

.factory( 'boundary', function( $location ) {
	'use strict';

	var GRID_SIZE_PERCENT = 1;

	var PERCENT_LEFT = 'percentLeft';
	var PERCENT_TOP = 'percentTop';
	var PERCENT_WIDTH = 'percentWidth';
	var PERCENT_HEIGHT = 'percentHeight';

	var boundedValue = function( value, lowerBound, upperBound, upperBoundMargin ) {
		upperBoundMargin = upperBoundMargin || 0;
		if ( value < lowerBound ) {
			value = lowerBound;
		} else if ( value  > upperBound - upperBoundMargin ) {
			value = upperBound - upperBoundMargin;
		}
		return value;
	};

	var snapToGrid = function( val ) {
		return GRID_SIZE_PERCENT * Math.round( val / GRID_SIZE_PERCENT );
	};

	return {
		boundedProperties : function( properties ) {
			var initialBoundedWidth = boundedValue( properties.percentWidth, 0, 100 );
			var boundedLeft = boundedValue( properties.percentLeft, 0, 100, initialBoundedWidth );
			return {
				percentLeft : boundedLeft,
				percentTop : boundedValue( properties.percentTop, 0, Infinity ),
				percentWidth : boundedValue( initialBoundedWidth, 0, 100, boundedLeft ),
				percentHeight : boundedValue( properties.percentHeight, 0, Infinity )
			};
		},

		boundedResize : function( properties, axis ) {
			var boundedProps = _.extend( {}, properties);
			var deltaLeft, deltaTop;

			if ( /^(se|s|e)$/.test( axis ) ) {
				boundedProps.percentWidth = boundedValue( properties.percentWidth, 0, 100, properties.percentLeft );
				boundedProps.percentHeight = boundedValue( properties.percentHeight, 0, Infinity );
			} else if ( /^(ne)$/.test( axis ) ) {
				boundedProps.percentTop = boundedValue( properties.percentTop, 0, Infinity );
				deltaTop = boundedProps.percentTop - properties.percentTop;
				boundedProps.percentWidth = boundedValue( properties.percentWidth, 0, 100, properties.percentLeft );
				boundedProps.percentHeight = boundedValue( properties.percentHeight - deltaTop, 0, Infinity );
			} else if ( /^(sw)$/.test( axis ) ) {
				boundedProps.percentLeft = boundedValue( properties.percentLeft, 0, 100 );
				deltaLeft = boundedProps.percentLeft - properties.percentLeft;
				boundedProps.percentWidth = boundedValue( properties.percentWidth - deltaLeft, 0, 100, properties.percentLeft );
				boundedProps.percentHeight = boundedValue( properties.percentHeight, 0, Infinity );
			} else {
				boundedProps.percentTop = boundedValue( properties.percentTop, 0, Infinity );
				deltaTop = boundedProps.percentTop - properties.percentTop;
				boundedProps.percentHeight = boundedValue( properties.percentHeight - deltaTop, 0, Infinity );

				boundedProps.percentLeft = boundedValue( properties.percentLeft, 0, 100 );
				deltaLeft = boundedProps.percentLeft - properties.percentLeft;
				boundedProps.percentWidth = boundedValue( properties.percentWidth - deltaLeft, 0, 100, properties.percentLeft );
			}

			return boundedProps;
		},

		minSizedProperties : function( properties ) {
			return angular.extend( {}, properties, {
				percentWidth : boundedValue( properties.percentWidth, 6, 100 ),
				percentHeight : boundedValue( properties.percentHeight, 6, Infinity )
			} );
		},

		gridAlignedProperties : function( properties ) {
			return {
				percentLeft : snapToGrid( properties.percentLeft ),
				percentTop : snapToGrid( properties.percentTop ),
				percentWidth : snapToGrid( properties.percentWidth ),
				percentHeight : snapToGrid( properties.percentHeight )
			};
		},

		gridAlignedResize : function( newProps, oldProps, axis ) {
			var ox = snapToGrid( newProps.percentWidth - oldProps.percentWidth );
			var oy = snapToGrid( newProps.percentHeight - oldProps.percentHeight );
			var newWidth = oldProps.percentWidth + ox;
			var newHeight = oldProps.percentHeight + oy;
			var alignedProps = _.pick( oldProps, PERCENT_LEFT, PERCENT_TOP, PERCENT_WIDTH, PERCENT_HEIGHT );
			if ( /^(se|s|e)$/.test( axis ) ) {
				alignedProps.percentWidth = newWidth;
				alignedProps.percentHeight = newHeight;
			} else if ( /^(ne)$/.test( axis ) ) {
				alignedProps.percentWidth = newWidth;
				alignedProps.percentHeight = newHeight;
				alignedProps.percentTop = oldProps.percentTop - oy;
			} else if ( /^(sw)$/.test( axis ) ) {
				alignedProps.percentWidth = newWidth;
				alignedProps.percentHeight = newHeight;
				alignedProps.percentLeft = oldProps.percentLeft - ox;
			} else {
				if ( newHeight - GRID_SIZE_PERCENT > 0 ) {
					alignedProps.percentHeight = newHeight;
					alignedProps.percentTop = oldProps.percentTop - oy;
				} else {
					alignedProps.percentHeight = GRID_SIZE_PERCENT;
					alignedProps.percentTop = oldProps.percentTop + oldProps.percentHeight - GRID_SIZE_PERCENT;
				}
				if ( newWidth - GRID_SIZE_PERCENT > 0 ) {
					alignedProps.percentWidth = newWidth;
					alignedProps.percentLeft = oldProps.percentLeft - ox;
				} else {
					alignedProps.percentWidth = GRID_SIZE_PERCENT;
					alignedProps.percentLeft = oldProps.percentLeft + oldProps.percentWidth - GRID_SIZE_PERCENT;
				}
			}
			return alignedProps;
		}
	};

} );
