/* global describe, inject, module, beforeEach, it, expect, _, fakeServices */

describe('queryService', function() {
	'use strict';

	var stage;

	beforeEach(module('visualizer'));

	beforeEach(inject(function($rootScope, $compile, $injector, $httpBackend, $templateCache) {

		fakeServices($httpBackend,
					 [
						 {folder: 'Time', name: 'Time.Year'},
						 {folder: 'Product', name: 'Product.Category'},
						 {folder: 'Retail', name: 'Retail.Region'}
					 ],
					 [
						 {name: 'Revenue', byDates: ['OrderDate', 'SaleDate'], aggregations: ['Sum', 'Avg']},
						 {name: 'Quantity', byDates: ['OrderDate', 'SaleDate'], aggregations: ['Sum', 'Avg']}
					 ],
					 [
						 ["2010", "Mobile", "North", [19100, 18560], [200, 180]],
						 ["2010", "Mobile", "South", [18100, 17560], [210, 240]],
						 ["2010", "Desktop", "North", [19200, 18660], [100, 100]],
						 ["2010", "Desktop", "South", [18200, 17660], [20, 10]],
						 ["2011", "Mobile", "North", [19300, 18760], [250, 230]],
						 ["2011", "Mobile", "South", [18300, 17760], [150, 150]],
						 ["2011", "Desktop", "North", [19400, 18860], [190, 190]],
						 ["2011", "Desktop", "South", [18400, 17860], [200, 220]],
						 ["2012", "Mobile", "North", [19500, 18960], [100, 150]],
						 ["2012", "Mobile", "South", [18500, 17960], [190, 240]],
						 ["2012", "Desktop", "North", [19600, 18260], [200, 220]],
						 ["2012", "Desktop", "South", [18600, 17260], [160, 140]]
					 ]
					);
	}));

	// TODO: Fix This Test
	// it('should be able to find queryService', inject(function(SubjectAreaWebService, SubjectAreaProcessor, queryService, $httpBackend, columnCreator, Stage) {
 //        SubjectAreaWebService.getSubjectAreaContent('Default Subject Area', function(data) {
 //            var subjectArea = SubjectAreaProcessor.process(data, "Default Subject Area", {errorHandler: function(obj) { console.log(obj); }});

 //            var stage = new Stage();
 //            stage.addColumn(columnCreator.createFromProto(subjectArea.measures[0]), null, 'column');
 //            stage.addColumn(columnCreator.createFromProto(subjectArea.attributes[0]), null, 'column');
 //            stage.addColumn(columnCreator.createFromProto(subjectArea.attributes[1]), null, 'column');

 //            queryService.runQueriesForStage(stage,
 //                                            function(results) {
 //                                                expect(results.length).toBe(1);
 //                                                expect(results[0].rows.length).toBe(6);
 //                                                expect(results[0].rows[0][0]).toBe(830);
 //                                            },
 //                                            {
 //                                                errorHandler: function(obj) { console.log("error", obj); },
 //                                                "$broadcast": function(event) { console.log("Broadcast", event); },
 //                                                chartType: 'column'
 //                                            });

 //            stage.addColumn(columnCreator.createFromProto(subjectArea.measures[1]), null, 'column');

 //            queryService.runQueriesForStage(stage,
 //                                            function(results) {
 //                                                expect(results.length).toBe(2);
 //                                                expect(results[0].rows.length).toBe(6);
 //                                                expect(results[1].rows.length).toBe(6);
 //                                                expect(results[0].rows[0][0]).toBe(830);
 //                                                expect(results[1].rows[1][0]).toBe(73720);
 //                                            },
 //                                            {
 //                                                errorHandler: function(obj) { console.log("error", obj); },
 //                                                "$broadcast": function(event) { console.log("Broadcast", event); },
 //                                                chartType: 'column'
 //                                            });

 //        }, function() {
 //            console.log("Error getting subject area");
 //        });
 //        $httpBackend.flush();
	// }));

});
