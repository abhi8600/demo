
angular.module('visualizer').
service('queryService', ['chartStateService', 'globalStateService', 'AdhocWebService', 'subjectAreaWebService', 'alertService', 'Stage', function(chartStateService, globalStateService, AdhocWebService, subjectAreaWebService, alertService, Stage) {
    'use strict';
    var queryCache = [];
    var cachedQueryLimit = 100;

    function subsetResultSet(results, indices) {
        var takeIndices = function(arr) {
            return _.map(indices, function(idx) {
                return arr[idx];
            });
        };
        return {
            rows: _.map(results.rows, takeIndices),
            columnNames: takeIndices(results.columnNames),
            displayNames: takeIndices(results.displayNames),
            dataTypes: takeIndices(results.dataTypes),
            hasMoreRows: results.hasMoreRows,
            warehouseColumnDataTypes: takeIndices(results.warehouseColumnDataTypes),
            columnFormats: takeIndices(results.columnFormats)
        };
    }

    return {
        queryString: function(allColumns, filterColumns, sortString) {
            sortString = sortString || '';

            var createColumnFilters = function(alias, filters) {
                var filtersToOr = _.filter(filters.items, function(item) {
                    return item.operator === '=' || item.operator === ' LIKE ' || filters.subType === 'Outside';
                });
                var filtersToAnd = _.filter(filters.items, function(item) {
                    return item.operator !== '=' && item.operator !== ' LIKE ' && filters.subType !== 'Outside';
                });

                filtersToOr = _.map(filtersToOr,
                    function(filter) {
                        if (filter.operand === null) {
                            filter.operand = String(null);
                        }

                        if (filter.operand) {
                            filter.operand = typeof filter.operand !== 'number' ? filter.operand.replace('\'', '\\\'') : filter.operand;
                        }

                        if (filters.type === 'items') {
                            if (filter.operand) {
                                return alias + filter.operator + '\'' + filter.operand + '\'';
                            } else {
                                return alias + filter.operator;
                            }
                        } else if (filters.type === 'range') {
                            if (filter.operand) {
                                return alias + filter.operator + filter.operand;
                            } else {
                                return alias + filter.operator;
                            }
                        } else if (filters.type === 'date') {
                            if (filter.operand) {
                                return alias + filter.operator + '#' + filter.operand + '#';
                            } else {
                                return alias + filter.operator;
                            }
                        }
                    }
                ).join(' OR ');

                filtersToAnd = _.map(filtersToAnd,
                    function(filter) {
                        if (filter.operand === null) {
                            filter.operand = String(null);
                        }

                        if (filter.operand) {
                            filter.operand = typeof filter.operand !== 'number' ? filter.operand.replace('\'', '\\\'') : filter.operand;
                        }

                        if (filters.type === 'items') {
                            if (filter.operand) {
                                return alias + filter.operator + '\'' + filter.operand + '\'';
                            } else {
                                return alias + filter.operator;
                            }
                        } else if (filters.type === 'range') {
                            if (filter.operand) {
                                return alias + filter.operator + filter.operand;
                            } else {
                                return alias + filter.operator;
                            }
                        } else if (filters.type === 'date') {
                            if (filter.operand) {
                                return alias + filter.operator + '#' + filter.operand + '#';
                            } else {
                                return alias + filter.operator;
                            }
                        }
                    }
                ).join(' AND ');

                if (filtersToOr && filtersToAnd) {
                    return '( ' + filtersToOr + ' ) AND ' + filtersToAnd;
                } else if (filtersToOr && !filtersToAnd) {
                    return '( ' + filtersToOr + ' )';
                } else if (!filtersToOr && filtersToAnd) {
                    return filtersToAnd;
                }

            };

            var filterString = _.map(_.filter(filterColumns, function(column) {
                return column.prompts || column.filter; // keep the column if it has a data filter (not falsy/null)
            }), function(column) {
                var columnBql = column.expression.expressionToString();

                // create strings to correctly implement filters
                if (column.prompts && column.prompts.items) {
                    return createColumnFilters(columnBql, column.prompts);
                } else if (column.filter && column.filter.items) {
                    return createColumnFilters(columnBql, column.filter);
                }
            }).join(' AND ');

            var displayFilterString = _.map(_.filter(filterColumns, function(column) {
                return column.displayFilter; // keep the column if it has a display filter (not falsy/null)
            }), function(column) {
                var alias = _.queryAlias('[' + column.name + column.uniqueId + ']');

                // create strings to correctly implement display filters
                if (column.displayFilter && column.displayFilter.items) {
                    return createColumnFilters(alias, column.displayFilter);
                }
            }).join(' AND ');

            if (filterString !== '' && filterString !== ' AND ') {
                filterString = ' WHERE ( ' + filterString + ' ) ';
            } else {
                filterString = '';
            }

            if (displayFilterString !== '' && displayFilterString !== ' AND ') {
                displayFilterString = ' DISPLAY WHERE ( ' + displayFilterString + ' ) ';
            } else {
                displayFilterString = '';
            }

            var projectionList = _.map(allColumns, function(col) {
                return col.expression.expressionToString() + ' \'' + _.queryAlias(col.name + col.uniqueId) + '\' ';
            }).join(", ");

            var query = "SELECT USING OUTER JOIN " +
                projectionList +
                " FROM [ALL]" +
                filterString +
                displayFilterString +
                sortString;

            return query;
        },

        runCachedQuery: function(query, cb, errorHandler) {
            // queryCache can be in one of three states:
            //   no value                        - this query has not been run
            //   {running: [list of callbacks]}  - this query is running; when it has finished, the callbacks in the list will be run
            //   {finished: [results]}           - query is finished

            var cacheIndex = _.findIndex(queryCache, function(item) {
                return item[0] === query;
            });

            var entry;

            if (cacheIndex === -1) {
                entry = [query, {
                    running: [cb]
                }];

                queryCache.unshift(entry);

                if (queryCache.length > cachedQueryLimit) {
                    queryCache.pop();
                }
                this.runQuery(query, function(results) {
                    _.each(entry[1].running, function(cb) {
                        cb(results);
                    });
                    entry[1] = {
                        finished: results
                    };
                }, errorHandler);
            } else {
                // Bring query cache entry to the front
                entry = queryCache[cacheIndex];

                queryCache = _.without(queryCache, entry);

                queryCache.unshift(entry);

                // if query is running then add our callback to the list; if it is finished then call the callback immediately.
                if (entry[1].running) {
                    entry[1].running.push(cb);
                } else {
                    cb(entry[1].finished);
                }
            }
        },

        optimizeQueries: function(queryList, callbacks, errorHandler) {

            var that = this;

            function isQuerySubset(leftQuery, rightQuery) {
                // Sorting and filters should be the same
                if (leftQuery.sortString !== rightQuery.sortString) {
                    return false;
                }

                var filtersIdentical = leftQuery.filterColumns.length === rightQuery.filterColumns.length &&
                    _.difference(leftQuery.filterColumns, rightQuery.filterColumns).length === 0;

                if (!filtersIdentical) {
                    return false;
                }

                // attributes should be a subset.
                var leftAttribs = _.filter(leftQuery.columns, function(col) {
                    return col.expression.isAttributeOrCustomExpression();
                });

                var rightAttribs = _.filter(rightQuery.columns, function(col) {
                    return col.expression.isAttributeOrCustomExpression();
                });

                return _.difference(leftAttribs, rightAttribs).length === 0;
            }

            var distinctQuerySets = [];
            _.each(queryList, function(query) {
                for (var ii = 0; ii < distinctQuerySets.length; ii++) {
                    var qs = distinctQuerySets[ii];

                    if (isQuerySubset(qs[0], query)) {
                        // Keep the biggest query at the head of the list
                        qs.unshift(query);
                        return;
                    } else if (isQuerySubset(query, qs[0])) {
                        qs.push(query);
                        return;
                    }
                }
                // if we're here, then there is no query set applicable to this query - start a new one.
                distinctQuerySets.push([query]);
            });

            var notSummableQuerySets = [];

            _.each(distinctQuerySets, function(qs) {
                _.each(qs, function(query) {
                    var summable = _.every(query.columns, function(col) {
                        return col.expression.isAttributeOrCustomExpression() || col.expression.clientAggregation();
                    });

                    if (!summable && !isQuerySubset(qs[0], query)) {
                        // Even though this query has a subset of the
                        // head query's attributes, it needs its own
                        // query because the measure is not summable
                        // on the client side.

                        for (var ii = 0; ii < notSummableQuerySets.length; ii++) {
                            var nsqs = notSummableQuerySets[ii];
                            if (isQuerySubset(nsqs[0], query) && isQuerySubset(query, nsqs[0])) {
                                nsqs.push(query);
                                return;
                            }
                        }
                        notSummableQuerySets.push([query]);
                    }
                });
            });

            // Remove the queries covered by notSummableQuerySets
            distinctQuerySets = _.map(distinctQuerySets, function(qs) {
                return _.difference(qs, _.flatten(notSummableQuerySets));
            });

            _.each(distinctQuerySets, function(qs) {
                var queryCallbacks = _.map(qs, function(query) {
                    return {
                        query: query,
                        callback: callbacks[_.indexOf(queryList, query)]
                    };
                });

                var attributes = _.filter(qs[0].columns, function(col) {
                    return col.expression.isAttributeOrCustomExpression();
                });
                queryCallbacks = _.map(queryCallbacks, function(queryCallback) {
                    if (isQuerySubset(qs[0], queryCallback.query)) {
                        return queryCallback;
                    } else {
                        return increaseQueryGrain(queryCallback.query, queryCallback.callback, attributes);
                    }
                });

                runMultipleQueriesAtSameGrain.call(this, _.pluck(queryCallbacks, 'query'), _.pluck(queryCallbacks, 'callback'));
            }, this);

            _.each(notSummableQuerySets, function(nsqs) {
                runMultipleQueriesAtSameGrain.call(this, nsqs, _.map(nsqs, function(query) {
                    return callbacks[_.indexOf(queryList, query)];
                }));
            }, this);

            function runMultipleQueriesAtSameGrain(queries, callbacks) {
                //console.log(queries)
                if (queries.length === 0) {
                    return;
                }
                // Attributes - should be the same across all queries
                var attributes = _.filter(queries[0].columns, function(col) {
                    return col.expression.isAttributeOrCustomExpression();
                });
                var measures = [];
                _.each(queries, function(query) {
                    var queryMeasures = _.filter(query.columns, function(col) {
                        return !col.expression.isAttributeOrCustomExpression();
                    });
                    measures.push.apply(measures, queryMeasures);
                });

                var allColumns = measures.concat(attributes);
                allColumns = allColumns.sort(Stage.columnSort);

                that.runCachedQuery(that.queryString(allColumns, queries[0].filterColumns, queries[0].sortString), function(results) {
                    _.each(queries, function(query, ii) {
                        var indices = _.map(query.columns, function(col) {
                            return _.indexOf(allColumns, col);
                        });
                        var queryResults = subsetResultSet(results, indices);
                        callbacks[ii](queryResults);
                    });
                }, errorHandler);
            }


            function increaseQueryGrain(query, callback, attributes) {
                // Transform a query with N attributes to one with N + K attributes
                var newQuery = {
                    columns: query.columns,
                    filterColumns: query.filterColumns,
                    sortString: query.sortString
                };
                var extraAttribs = _.filter(attributes, function(attrib) {
                    return _.indexOf(newQuery.columns, attrib) === -1;
                });
                newQuery.columns = newQuery.columns.concat(extraAttribs).sort(Stage.columnSort);

                return {
                    query: newQuery,
                    callback: function(result) {
                        var aggregatedResults = [];
                        var aggregatedRows = {};
                        var attribIndices = _.map(_.difference(attributes, extraAttribs), function(attrib) {
                            return _.indexOf(newQuery.columns, attrib);
                        });
                        var indices = _.map(extraAttribs, function(attrib) {
                            return _.indexOf(newQuery.columns, attrib);
                        });

                        _.each(result.rows, function(row) {
                            var tuple = _.filter(row, function(item, idx) {
                                return _.contains(attribIndices, idx);
                            });
                            var key = JSON.stringify(tuple);

                            var newRow = _.filter(row, function(item, idx) {
                                return !_.contains(indices, idx);
                            });
                            if (!aggregatedRows[key]) {
                                aggregatedRows[key] = newRow;
                                aggregatedResults.push(newRow);
                            } else {
                                var aggRow = aggregatedRows[key];
                                _.each(query.columns, function(col, ii) {
                                    var aggregate = col.expression.clientAggregation();
                                    if (aggregate) {
                                        aggRow[ii] = aggregate(aggRow[ii], newRow[ii]);
                                    }
                                });
                            }
                        });

                        var originalIndices = _.difference(_.range(query.columns.length), indices);

                        var aggregatedResultSet = subsetResultSet(result, originalIndices);
                        aggregatedResultSet.rows = aggregatedResults;
                        callback(aggregatedResultSet);
                    }
                };
            }
        },

        runQueriesForStage: function(stage, callback, scope) {
            var sortString = stage.sortBQLString() || '';

            function updateSubjectArea(query) {
                try {
                    if (globalStateService.useInternalWebServices() && query) {
                        var SAS = subjectAreaWebService;

                        SAS.getVisualizerUpdatedSubjectArea(query, function(columnStatesPerSubjectArea) {
                            // json will be an array of nodes with .valid and .path (array) properties

                            var subjectArea = scope.subjectArea;

                            if (!subjectArea) {
                                return;
                            }

                            var columnStates = [];

                            //console.log('got column states');
                            //console.log(columnStatesPerSubjectArea);

                            if (columnStatesPerSubjectArea && columnStatesPerSubjectArea[scope.selectedSubjectArea]) {
                                columnStates = columnStatesPerSubjectArea[scope.selectedSubjectArea];
                            }

                            _.each(columnStates, function(columnState) {
                                //console.log('columnState = ', columnState);
                                var path = columnState.path.slice(1);
                                var disabled = !columnState.valid;

                                if (!disabled) {
                                    //console.log('re-enabling', columnState);
                                }

                                var column = null;

                                if (path[0] === 'Measures') {
                                    //console.log('columnState = ', columnState);
                                    //console.log('original measure path', path);
                                    path = path.slice(1);

                                    _.each(subjectArea.measures, function(measure) {
                                        //var measureShortPath = measure.path.slice(0, path.length);

                                        if (_.difference(path, measure.path).length === 0) {
                                            column = measure;
                                            //console.log('found measure', column);
                                            //console.log('paths:', path, measure.path);
                                            //console.log('disabling ', column);
                                            column.disabled = disabled;
                                        }
                                    });

                                    if (column === null) {
                                        //console.log('could not match measure with paths', path, measure.path);
                                    }
                                } else if (path[0] === 'Attributes') {
                                    //console.log('original attribute path', path);
                                    path = path.slice(1);

                                    _.each(subjectArea.attributes, function(attribute) {
                                        var attributeShortPath = attribute.path.slice(0, path.length);

                                        if (_.difference(path, attributeShortPath).length === 0) {
                                            column = attribute;
                                            //console.log('found attribute', column);
                                            //console.log('paths:', path, attributeShortPath, attribute.path);
                                            //console.log('disabling ', column)
                                            column.disabled = true;
                                        }
                                    });

                                    if (column === null) {
                                        //console.log('could not match attribute with paths', path, attribute.path);
                                    }
                                } else { // top level
                                    // temporarily ignore
                                }
                            });

                        }, function(err) {
                            console.log('error in SAS.getVisualizerUpdatedSubjectArea: ', err);
                        });
                    }
                } catch (e) {
                    console.log('error while handling getVisualizerUpdatedSubjectArea response: ', e);
                }
            }

            var tableView = scope.chartType === 'table';

            if (!tableView && stage.allSeries().length >= 1 && !_.isObject(stage.Category)) {
                if (_.isObject(stage.Color)) {

                    scope.errorHandler({
                        basicText: visualizer.misc.errors.ERR_NO_COLOR_TILL_CAT,
                        moreInfoText: ''
                    }, true);

                    return;
                }

                var seriesQuery = this.queryString(_.pluck(stage.allSeries(), 'column'), [], sortString);
                this.runQuery(seriesQuery, function(results) {
                    callback([results]);
                }, scope.errorHandler);
                updateSubjectArea(seriesQuery);
                return;
            }

            if (!tableView && stage.allSeries().length === 0 && _.isObject(stage.Category)) {
                var categoryQuery = this.queryString([stage.Category], [], sortString);
                this.runQuery(categoryQuery, function(results) {
                    callback([results]);
                }, scope.errorHandler);
                updateSubjectArea(categoryQuery);
                return;
            }

            var queries = [];
            if (!tableView) {
                queries = _.map(stage.getAllColumnsPerSeriesQuery(), function(serieColumnGroup) {
                    var measureColumn = _.find(serieColumnGroup, function(col) {
                        return col.dim === 'measure';
                    });

                    var activeFilterColumns = [];

                    if (measureColumn) {
                        activeFilterColumns = _.filter(stage.allColumnsAndUnvisualized(), function(column) {
                            return (column.properties.visualized && column.properties.isFilterOn) ||
                                (!column.properties.visualized && (column.measuresToFilter ? column.measuresToFilter[measureColumn.column.name] : false));
                            // filter out unvisualized columns (columns to potentially filter by) which we are not using as filters
                        });
                    }

                    var columns = _.pluck(serieColumnGroup, 'column');
                    var displayFilters = _.filter(_.difference(activeFilterColumns, columns), function(column) {
                        return column.displayFilter;
                    });
                    return {
                        columns: columns.concat(displayFilters),
                        displayFilters: displayFilters,
                        filterColumns: activeFilterColumns,
                        sortString: sortString
                    };
                });
            } else {
                var columns = stage.Columns;
                if (!columns.length) {
                    scope.table.results = {};
                    scope.$apply();
                    return;
                }
                var filterColumns = _.filter(stage.allColumnsAndUnvisualized(scope.chartType), function(column) {
                    return (column.properties.visualized && column.properties.isFilterOn) ||
                        (!column.properties.visualized && (column.measuresToFilter ? true : false));
                });
                var displayFilters = _.filter(_.difference(filterColumns, columns), function(column) {
                    return column.displayFilter;
                });

                queries = [{
                    columns: columns,
                    filterColumns: filterColumns,
                    displayFilters: displayFilters,
                    sortString: sortString
                }];
            }

            var resultSets = [];
            var updateSA = _.once(updateSubjectArea);
            var finish = _.after(queries.length, callback);
            var that = this;
            this.optimizeQueries(queries, _.map(queries, function(query, ii) {
                return function(resultSet) {
                    // Remove display-filtered columns from resultSet
                    resultSet = subsetResultSet(resultSet, _.range(query.columns.length - query.displayFilters.length));
                    if(tableView) {
                        resultSets[ii] = resultSet;
                    } else {
                        resultSets[ii] = that.performClientSideAggregation(query, resultSet, scope);
                    }
                    var firstQuery = that.queryString(queries[0].columns, queries[0].filterColumns, queries[0].sortString);
                    updateSA(firstQuery);
                    finish(resultSets);
                };
            }), scope.errorHandler);
        },

        performClientSideAggregation: function(query, resultSet, scope) {

            // If the query returned multiple rows with the same attribute set, then sum those rows.
            var cols = query.columns.slice(0, query.columns.length - query.displayFilters.length);

            var attribIndices = [],
                measureIndices = [],
                measureAggregations = [];
            var defaultAggregationsUsed = [];
            _.map(cols, function(col, ii) {
                var isMeasure = col.category === 'measure' || col.subtype === 'measure' || col.expression.isMeasure() || col.expression.isMeasureExpression();
                if (!isMeasure) {
                    // If it's not definitely a measure, then treat it as an attribute and do not aggregate.
                    attribIndices.push(ii);
                } else {
                    measureIndices.push(ii);
                    var agg = col.expression.clientAggregation();
                    if (!agg) {
                        switch(col.displayAggregation) {
                        case "Min":
                            agg = Math.min;
                            break;
                        case "Max":
                            agg = Math.max;
                            break;
                        case "Sum":
                            agg = function(x, y) {
                                return x + y;
                            };
                            break;
                        default:
                            agg = function(x, y) {
                                return x + y;
                            };
                            defaultAggregationsUsed.push(col.name);
                        }
                    }
                    measureAggregations.push(agg);
                }
            });

            var byAttrib = {};
            var aggregatedRows = [];
            var someAggregated = false;
            _.map(resultSet.rows, function(row) {
                var key = JSON.stringify(attribIndices.map(function(idx) {
                    return row[idx];
                }));
                if (!byAttrib[key]) {
                    byAttrib[key] = row;
                    aggregatedRows.push(row);
                } else {
                    someAggregated = true;
                    _.map(measureIndices, function(idx, ii) {
                        byAttrib[key][idx] = measureAggregations[ii](byAttrib[key][idx], row[idx]);
                    });
                }
            });
            resultSet.rows = aggregatedRows;

            if (someAggregated && defaultAggregationsUsed.length > 0) {
                var aggregationsList = $("<span>");
                _.map(defaultAggregationsUsed, function(col, ii) {
                    if(ii > 0) {
                        aggregationsList.append($("<span>").text(","));
                    }
                    aggregationsList.append($("<a>").css('cursor','pointer').text(col).click(function() {
                        scope.$broadcast('openDisplayAggregationsInStage', col);
                    }));
                });
                alertService.alert({
                    content: "The following columns have been aggregated by sum, which may give inaccurate results.<br />Click the column name to customize: ",
                    extraAppend: aggregationsList,
                    type: 'warning',
                    id: 'warning-clientSideAggregation',
                    duration: 10
                });
            } else {
                alertService.clearById('warning-clientSideAggregation');
            }
            return resultSet;
        },

        runQuery: function(query, cont, errorHandler) {
            function errorCallback(errorCause) {
                if (errorCause === 'query timed out') {
                    errorHandler({
                        basicText: visualizer.misc.errors.ERR_QUERY_TO,
                        moreInfoText: ''
                    });
                } else {
                    errorHandler({
                        basicText: errorCause,
                        moreInfoText: ''
                    });
                }
            }

            function responseHandler(json) {
                // process the arguments before continuing to cont

                /*
                    executeQueryInSpace returns an object like this:

                    json = {
                         rows: jsonRows,
                         columnNames: columnNames,
                         displayNames: displayNames,
                         dataTypes: dataTypes,
                         length: jsonRows.length,
                         hasMoreRows: $executeQueryInSpaceResult.find('hasMoreRows').first().text()
                     };

                     ... to which we want to two more properties: 'orderToIdMapping' and 'idToOrderMapping'.

                    The property 'orderToIdMap' will contain an object that maps from the order of allColumns in the query string to
                    the unique ids of the allColumns as represented in the Subject Area.

                    The property 'idToOrderMap' will contain an object that provides the reverse lookup.
                */

                //no rows returned, show user error

                if (_.isEmpty(json.rows)) {

                    errorHandler({
                        basicText: visualizer.misc.errors.ERR_QUERY_NO_DATA,
                        moreInfoText: query
                    }, true);

                    return [];
                }

                cont.apply(cont, arguments);
            }

            AdhocWebService.executeQuery(query, responseHandler, errorCallback);
        }
    };
}]);
