/* jshint bitwise:false */
angular.module('visualizer')

.factory('adhocConverter', function() {
	'use strict';
    var convert = function(xml, reportName) {
        var $xml = $(xml);
        if ($xml.find('soapenv\\:Body > ns\\:getDashboardRenderedReportResponse').length) {
            // used in some tests
            $xml = $xml.find('root').children('com\\.successmetricsinc\\.adhoc\\.AdhocReport');
        } else {
            // used by pivot control
            $xml = $xml.find('Result > com\\.successmetricsinc\\.adhoc\\.AdhocReport');
        }

        var entities = $xml.find('Entities');

        //check if report XML is invalid and show error if that's the case

        // make this fire an event instead and handle it elsewhere by showing the error

        // ERROR HANDLER
        var errorMsg = {};

        if (_.isEmpty(entities) || entities == null) {
            errorMsg = {
                basicText: visualizer.misc.errors.ERR_BAD_REPORT,
                moreInfoText: 'Report name: ' + reportName.replace('.jasper', '.AdhocReport')
            };
        }

        // END ERROR HANDLER

        var adhocColumns = entities.find('com\\.successmetricsinc\\.adhoc\\.AdhocColumn');

        adhocColumns = _.map(adhocColumns, function(rawAdhocColumn) {
            var adhocColumn = $(rawAdhocColumn);

            // we need the ReportIndex
            // confirmed with Al that the <Categories><Int>3</Int></Categories> are reportIndexes

            var expressionObject = {
                name: _.last(adhocColumn.children('Name').text().split(': ')),
                columnType: adhocColumn.children('ColumnType').text(),
                isMeasureExpression: adhocColumn.children('IsMeasureExpression').text() === 'true',
                bql: adhocColumn.children('DisplayExpression').text(), // deprecated: use displayExpression
                displayExpression: adhocColumn.children('DisplayExpression').text(),
                reportIndex: adhocColumn.children('ReportIndex').text()
            };

            //console.log('AdhocColumn: ', expressionObject);

            //  console.log('expressionObject', expressionObject);

            return expressionObject;
        });

        //attempt to pre-set the correct chart type based on what we read in
        var chartTypeInReport = entities.find('ChartType').text().toLowerCase();

        if (this.chartStateService) {
            if (_.indexOf(this.chartStateService.getNativeChartTypes(), chartTypeInReport) > -1) {
                this.chartStateService.setCurrentChartType(chartTypeInReport);
            }
        }

        var supportedFilterTypes = ['=', '>', '<', '>=', '<=', '<>', ' LIKE ', ' NOT LIKE '];

        var createFilterColumn = function(object, bql, name, dataType, operator, operand, filterType) {
            dataType = dataType.toUpperCase();

            if (operand === "NO_FILTER") {
                errorMsg = {
                    basicText: "One or more filters in this report are not supported and have not been applied"
                };
                console.log('prompt ignored');
                return;
            }
            if (!_.contains(supportedFilterTypes, operator)) {
                errorMsg = {
                    basicText: "One or more filters in this report are not supported and have not been applied"
                };
                console.log('filter type not supported');
                return;
            }
            if (!object[bql]) {
                object[bql] = {
                    // create column
                    bql: bql,
                    name: name,
                    type: 'unvisualized'
                };
                object[bql][filterType] = {
                    type: null,
                    subType: '=',
                    items: [{
                        operator: operator,
                        operand: operand
                    }]
                };

                if (dataType === 'TIMESTAMP' || dataType === 'DATE') {
                    object[bql][filterType].type = 'date';
                } else if (dataType === 'VARCHAR') {
                    object[bql][filterType].type = 'items';
                    object[bql][filterType].selectOrInput = 'select';
                } else if (dataType === 'INTEGER' || dataType === 'DOUBLE' || dataType === 'FLOAT') {
                    object[bql][filterType].type = 'range';
                } else {
                    object[bql][filterType].type = 'items';
                    object[bql][filterType].selectOrInput = 'select';
                }

            } else {
                // add to column
                object[bql][filterType].items.push({
                    operator: operator,
                    operand: operand
                });
            }
        };

        var filterHashtable = {
            "And": ' AND ', // Unsure of support
            "Equal": '=',
            "Greater than": '>',
            "Greater than or equal": '>=',
            "IN": ' IN ', // Unsure of support
            "Less than": '<',
            "Less than or equal": '<=',
            "LIKE": ' LIKE ',
            "Not equal": '<>',
            "NOT IN": ' NOT IN ', // Unsure of support
            "NOT LIKE": ' NOT LIKE ',
            "Or": ' OR ', // Unsure of support
            "IS NULL": ' IS NULL ', // Unsure of support
            "IS NOT NULL": ' IS NOT NULL ', // Unsure of support
        };

        // put all display filters in a displayFilters object
        var displayFilters = {};
        $xml.find('DisplayFilters').find('Filter').each(function() {
            var col = $(this).find('ColumnName').text(),
                dim = $(this).find('TableName').text(),
                bql = "[" + (dim ? [dim, col].join(".") : col) + "]",
                name = dim ? [dim, col].join(".") : _.last(col.split(': ')),
                operator = filterHashtable[$(this).find('Operator').text()] || $(this).find('Operator').text(),
                operand = $(this).find('Operand').text(),
                dataType = $(this).find('DataType').text();

            createFilterColumn(displayFilters, bql, name, dataType, operator, operand, 'displayFilter');

        });

        // put all data filters in a dataFilters object
        var dataFilters = {};

        $xml.find('Filters').find('Filter').map(function() {
            var col = $(this).find('Column').text(),
                dim = $(this).find('Dimension').text(),
                bql = "[" + (dim ? [dim, col].join(".") : col) + "]",
                name = dim ? [dim, col].join(".") : _.last(col.split(': ')),
                operator = filterHashtable[$(this).find('Operator').text()] || $(this).find('Operator').text(),
                operand = $(this).find('Operand').text(),
                dataType = $(this).find('DataType').text();

            createFilterColumn(dataFilters, bql, name, dataType, operator, operand, 'filter');
        });

        // if filtered column exists already in adhocColumns, remove it and then move it from dataFilters or displayFilters to adhocColumns.

        function findDataFilter(column) {
            return dataFilters[column.bql];
        }

        function findDisplayFilter(column) {
            if (column.columnType.toLowerCase() === 'expression') {
                return _.find(displayFilters, function(displayFilterColumn) {
                    return ~displayFilterColumn.name.indexOf(column.name);
                });
            }
            return displayFilters[column.bql];
        }


        function deleteDataFilter(column) {
            delete dataFilters[column.bql];
        }

        function deleteDisplayFilter(column) {
            if (column.columnType.toLowerCase() === 'expression') {
                _.each(displayFilters, function(displayFilterColumn, key) {
                    if (~displayFilterColumn.name.indexOf(column.name)) {
                        errorMsg = {
                            basicText: "One or more filters in this report are not supported and have not been applied",
                            id: column.name
                        };
                        console.log('display filter not in projection list', column.name);
                        delete displayFilters[key];
                    }
                });
            } else {
                delete displayFilters[column.bql];
            }
        }

        _.each(adhocColumns, function(column) {
            var dataFilter = findDataFilter(column);
            var displayFilter = findDisplayFilter(column);

            if (dataFilter) {
                _.extend(column, {
                    filter: dataFilter.filter,
                    type: 'unvisualized'
                });
                deleteDataFilter(column);
            } else if (displayFilter) {
                // currently we can't do display filters on unvisualized display filters
                if (column.columnType.toLowerCase() !== 'expression') {
                    _.extend(column, {
                        displayFilter: displayFilter.displayFilter,
                        type: 'unvisualized'
                    });
                }
                deleteDisplayFilter(column);
            }
        });

        // convert objects to arrays, keeping name the same
        dataFilters = _.map(dataFilters, function(column) {
            return column;
        });
        displayFilters = _.map(displayFilters, function(column) {
            return column;
        });

        /*

                Returns something like:

                charts: {
                    chart: {
                        categories: ['1'],
                        series: ['3', '5'],
                        series2: ['7'] ,
                        series3: ['9'],
                        chartType: 'Column'
                    }
                }

                */

        var charts = $xml.find('Charts').find('Chart').map(function() {
            //console.log('chart (xml):  ', chartXml);

            var $this = $(this);

            var chart = {
                chartType: $this.find('ChartType').text(),

                categories: $this.find('Categories').first().find('Int').map(function() {
                    return $(this).text();
                }).get(),

                series: $this.find('Series').first().find('Int').map(function() { // DOES NOT ERROR if there is no <Series>
                    return $(this).text();
                }).get(),

                series2: $this.find('Series2').first().find('Int').map(function() { // DOES NOT ERROR if there is no <Series2>
                    return $(this).text();
                }).get(),

                series3: $this.find('Series3').first().find('Int').map(function() { // DOES NOT ERROR if there is no <Series3>
                    return $(this).text();
                }).get(),

                yAxisShare: $this.find('Axes').first().find('YAxisShare').text() === 'true'
            };

            if (chart.chartType === 'Line') {
                chart.lineSeriesType = $this.find('DataPlotSettings').children('LineSeries').find('Type').text();
                chart.lineThickness = parseInt($this.find('DataPlotSettings').children('LineSeries').find('Thickness').text(), 10);
            }

            if (chart.chartType === 'Area') {
                chart.areaSeriesType = $this.find('DataPlotSettings').children('AreaSeries').find('Type').text();
            }

            //console.log('CHART!: ', chart);

            return chart;
        });

        _.each(charts, function(chart) {
            _.each([chart.categories, chart.series, chart.series2, chart.series3], function(visualizedColumns) {
                _.each(visualizedColumns, function(column) { // column is the ReportIndex of the column, so '1'
                    var adhocColumn = _.find(adhocColumns, function(adhocColumn) {
                        return adhocColumn.reportIndex === column;
                    });

                    adhocColumn.type = 'visualized';
                    adhocColumn.isFilterOn = true;
                    errorMsg = errorMsg && errorMsg.id === adhocColumn.name ? {} : errorMsg;
                });
            });
        });


        // finally create an object that has all columns properly prepared
        var report = {
            columns: adhocColumns,
            dataFilters: dataFilters,
            displayFilters: displayFilters,

            charts: charts,
            chartType: chartTypeInReport
        };

        //console.log('GOT REPORT: ', report);
        return {
            report: report,
            errorMsg: errorMsg
        };
    };

    return { convert: convert };
} );
