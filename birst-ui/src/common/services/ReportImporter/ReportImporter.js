angular.module('visualizer')

.factory('ReportImporter', function( BqlNode ) {
	'use strict';
	var MULTIPLE_X_NOT_SUPPORTED = 'MULTIPLE_X_NOT_SUPPORTED';
	var UNABLE_TO_CHART = 'UNABLE_TO_CHART';
	var UNSUPPORTED_CHART_TYPE = 'UNSUPPORTED_CHART_TYPE';
	var UNABLE_TO_PROCESS_COLUMN = 'UNABLE_TO_PROCESS_COLUMN';

	function ReportImporter() {
		this.errors = [];
	}

	ReportImporter.prototype.hasErrors = function() {
		return this.errors.length > 0;
	};

	ReportImporter.prototype.error = function(error) {
		this.errors.push(error);
	};

	ReportImporter.prototype.displayErrors = function(errorHandler) {
		console.log('DISPLAY ERRORS!!!!');

		errorHandler({
			basicText: visualizer.misc.errors[this.errors[this.errors.length - 1]],
			moreInfoText: ''
		});
	};

	ReportImporter.prototype.getChartHorizontalColumnIndex = function(chart) {
		var chartHorizontalColumnIndex;

		if (chart.chartType === 'Scatter' || chart.chartType === 'Bubble') {
			//chartHorizontalMeasure     = true;
			chartHorizontalColumnIndex = chart.series[0];
		} else {
			//chartHorizontalMeasure     = false;
			chartHorizontalColumnIndex = chart.categories[0];
		}

		return chartHorizontalColumnIndex;
	};

	ReportImporter.prototype.getMeasureIndices = function(array, columns) {
		var measureIndices = [];

		_.each(array, function(index) {
			//console.log('index', index,  columns[index]);
			if (columns[index].rawColumn.isMeasureExpression) {
				measureIndices.push(index);
			}
		});

		//console.log('measureIndices', measureIndices);

		return measureIndices;
	};

	ReportImporter.prototype.getDimensionIndices = function(array, columns) {
		var dimensionIndices = [];

		_.each(array, function(index) {
			if (!columns[index].rawColumn.isMeasureExpression) {
				//console.log('dimension: ', columns[index]);
				dimensionIndices.push(index);
			}
		});

		return dimensionIndices;
	};

	ReportImporter.prototype.getChartVerticalColumnIndices = function(chart, columns) {
		var measureIndices;

		if (chart.chartType === 'Scatter' || chart.chartType === 'Bubble') {
			return chart.series2;
		} else {
			// what if stacked? then we can support a maximum of one measure and one attribute in .series
			if (this.isStacked(chart)) {
				measureIndices = this.getMeasureIndices(chart.series, columns);

				//console.log('measureIndices, ', measureIndices);

				if (measureIndices.length > 1) {
					this.error(UNABLE_TO_CHART);

					return [measureIndices[0]];
				} else {
					return measureIndices;
				}
			} else {
				return this.getMeasureIndices(chart.series, columns);
			}
		}
	};

	ReportImporter.prototype.getChartDifferentiatorColumnIndices = function(chart, columns) {
		var dimensionIndices;

		if (chart.chartType === 'Scatter' || chart.chartType === 'Bubble') {
			return chart.categories;
		} else {
			if (this.isStacked(chart)) {
				dimensionIndices = this.getDimensionIndices(chart.series, columns);

				//console.log('dimensionIndices', dimensionIndices);

				if (dimensionIndices.length > 1) {
					this.error(UNABLE_TO_CHART);

					return [dimensionIndices[0]];
				} else {
					return dimensionIndices;
				}
			} else {
				return this.getDimensionIndices(chart.series, columns);
			}
		}
	};

	ReportImporter.prototype.getChartSizeColumnIndices = function(chart) {
		if (chart.chartType === 'Scatter' || chart.chartType === 'Bubble') {
			return chart.series3;
		} else {
			return [];
		}
	};

	ReportImporter.prototype.prepareColumnForStage = function(column, subjectArea, stage) {
		var expression,
			name,
			prompts = null,
			filter = null,
			displayFilter = null,
			isFilterOn = true,
			visualized = true,
			columnPreparedForStage;

		if (typeof column === 'object' && column.type === 'unvisualized') {
			expression = BqlNode.parseFromString(column.bql, subjectArea, column.isMeasureExpression);
			name = column.name;
			prompts = column.prompts;
			filter = column.filter;
			displayFilter = column.displayFilter;
			visualized = false;
			isFilterOn = false;
		} else if (typeof column === 'object' && column.type !== 'unvisualized') {
			expression = BqlNode.parseFromString(column.bql, subjectArea, column.isMeasureExpression);
			name = column.name;
			prompts = column.prompts;
			filter = column.filter;
			displayFilter = column.displayFilter;
			visualized = true;
			isFilterOn = column.isFilterOn ? column.isFilterOn : false;
		} else {
			console.log('type is ', typeof column);
		}

		columnPreparedForStage = {
			name: name,

			properties: {
				isFilterOn: isFilterOn,
				visualized: visualized,
				sortDirection: null,
				type: null
			},

			rawColumn: column,
			measuresToFilter: { },
			prompts: prompts || null,
			filter: filter || null,
			displayFilter: displayFilter || null,
			expression: expression
		};

		if (column.type === 'unvisualized') {
			var allSeries = stage.allSeries();

			_.each(allSeries, function(measure) {
				columnPreparedForStage.measuresToFilter[measure.column.name] = false;
			});
		}

		return columnPreparedForStage;
	};

	ReportImporter.prototype.getVisualizerChartType = function(chart) {
		var designerChartType = chart.chartType;

		if (designerChartType === 'Line' && chart.lineThickness === 0) {
			return 'points';
		}

		if (designerChartType === 'Line' && chart.lineSeriesType !== 'Line') {
			return 'spline';
		} else if (designerChartType === 'Line') {
			return 'line';
		}

		if (designerChartType === 'Area' && chart.areaSeriesType !== 'Area') {
			return 'areaspline';
		} else if (designerChartType === 'Area') {
			return 'area';
		}

		if (designerChartType === 'StackedBar' || designerChartType === 'PercentStackedBar') {
			return 'bar';
		} else if (designerChartType === 'StackedColumn' || designerChartType === 'PercentStackedColumn') {
			return 'column';
		} else if (designerChartType === 'StackedArea' || designerChartType === 'PercentStackedArea') {
			return 'area';
		}

		if (designerChartType === 'Doughnut') {
			return 'pie';
		}

		return designerChartType.toLowerCase();
	};

	ReportImporter.prototype.isSupportedChartType = function(chartType) {
		return _.contains(['column', 'bar', 'line', 'spline', 'area', 'areaspline', 'points', 'scatter', 'bubble', 'pie'], chartType);
	};

	ReportImporter.prototype.getStackingType = function(chart) {
		var designerChartType = chart.chartType;

		if (designerChartType.indexOf('Percent') === 0) {
			return 'Percent';
		} else if (designerChartType.indexOf('Stacked') === 0) {
			return 'Stack';
		} else {
			return 'Group';
		}
	};

	ReportImporter.prototype.isStacked = function(chart) {
		return this.getStackingType(chart) !== 'Group';
	};

	ReportImporter.prototype.addChart = function(chart, columns, scope) {
		var stage = scope.stage;
		var chartType = this.getVisualizerChartType(chart);
		var stackType = this.getStackingType(chart);

		if (!this.isSupportedChartType(chartType)) {
			this.error(UNSUPPORTED_CHART_TYPE);

			return;
		}

		//console.log('horizconindex', this.getChartHorizontalColumnIndex(chart));

		if (!stage.getCategoryColumn(chart)) {
			//console.log('trying to set cat column', this.getChartHorizontalColumnIndex(chart), columns[this.getChartHorizontalColumnIndex(chart)]);
			stage.setCategoryColumn(columns[this.getChartHorizontalColumnIndex(chart)]);
		}

		//console.log('getChartVerticalColumnIndices', this.getChartVerticalColumnIndices(chart, columns));

		var yAxis;

		if (!this.firstLayerYAxis) {
			yAxis = stage.addAxis(); // each Designer chart is a layer and a layer only has a single y axis
			// layers MIGHT share axes... check chart.yAxisShare (share axis with first layer)
			this.firstLayerYAxis = yAxis;
		} else if (chart.yAxisShare) {
			yAxis = this.firstLayerYAxis;
		} else {
			yAxis = stage.addAxis();
		}

		_.each(this.getChartVerticalColumnIndices(chart, columns), function(index) {
			var column = columns[index];

			//console.log('adding vertical ', index);
			//console.log('column', index, column);
			//console.log('columns', columns);

			stage.addColumn(column, undefined, chartType, {
				axis: yAxis,
				stackType: stackType
			}); // we should try to add to the same axis
		});

		//console.log('done adding vertical');

		_.each(this.getChartSizeColumnIndices(chart), function(index) { // empty array, so no iterations, for all charts other than bubble
			var column = columns[index];

			//console.log('adding size ', index);
			stage.addColumn(column, 'Size');
		});

		//console.log('done adding size');

		// a "differentiator column" splits a single scatter point into many, or one bubble into many

		_.each(this.getChartDifferentiatorColumnIndices(chart, columns), function(index) { // empty array, so no iterations, for all charts other than bubble and scatter
			var column = columns[index];

			//console.log('adding color ', index);
			stage.addColumn(column, 'Color');
		});

		//console.log('done adding color');

	};

	/* called when prod is launched as pivot and we have a report to parse and place in recently used */

	ReportImporter.prototype.import = function(report, scope) {
		//console.log('report: ', report);

		this.errors = [];
		this.firstLayerYAxis = undefined;

		scope.stage.clear();

		var adhocColumnsPreparedForStage = {}; // hashmap indexed by reportIndex

		var that = this;

		// loop over all columns, add to recently used and save prepared columns in adhocColumnsPreparedForStage

		_.each([report.columns, report.dataFilters, report.displayFilters], function(columnGroup, columnGroupIndex) {
			_.each(columnGroup, function(column) {
				var columnPreparedForStage = that.prepareColumnForStage(column, scope.subjectArea, scope.stage);

				if (!columnPreparedForStage) {
					this.error(UNABLE_TO_PROCESS_COLUMN);
					return;
				}

				var reportIndex = column.reportIndex;

				if (reportIndex && columnGroupIndex === 0) {
					adhocColumnsPreparedForStage[reportIndex] = columnPreparedForStage;
				}

				if ((column.filter || column.displayFilter) && column.type === 'unvisualized') {
					columnPreparedForStage.uniqueId = _.uniqueId();
					scope.stage.addFilter(
						columnPreparedForStage,

						{
							isFilterOn: false,
							visualized: false
						}
					);
					scope.trashBucket.columns.push(columnPreparedForStage);
				} else {
					scope.trashBucket.columns.push(columnPreparedForStage);
				}
			});
		});

		// CHECK WHETHER CHART IS POSSIBLE... we only support 1 X axis

		// associate reportIndexes with real columns...

		// horizontalColumnIndex: used for tracking which column is on the horizontal axis
		// because Visualizer doesn't support having multiple columns on the horizontal axis

		var horizontalColumnIndex;
		//var horizontalMeasure;
		//var stackingType; // Visualizer only supports one stacking type... or we'll have to put things on different axes
		// and then ensure axes of different types don't get merged

		_.each(report.charts, function(chart) {
			var chartHorizontalColumnIndex;
			//var chartHorizontalMeasure;

			if (chart.categories.length > 1) {
				that.error(MULTIPLE_X_NOT_SUPPORTED);
			}

			// Designer doesn't allow a Dimension on series for scatter or bubble, so we cannot make Scatter coexist with Bar, Column, etc.

			chartHorizontalColumnIndex = that.getChartHorizontalColumnIndex(chart);

			// Visualizer doesn't allow multiple horizontal axes

			// ** Also, because Scatter and Bubble have a measure on the horizontal axis and all other charts do not,   **
			// ** this happens to solve the problem of not being able to support Scatter/Bubble with other chart types. **

			if (horizontalColumnIndex && chartHorizontalColumnIndex !== horizontalColumnIndex) {
				that.error(MULTIPLE_X_NOT_SUPPORTED);
			} else if (!horizontalColumnIndex) {
				horizontalColumnIndex = chartHorizontalColumnIndex;
				//horizontalMeasure      = chartHorizontalMeasure;
			}

			that.addChart(chart, adhocColumnsPreparedForStage, scope);
		});

		if (!this.hasErrors()) {
			scope.updateChart();
			scope.safeApply();
		} else {
			scope.stage.clear(); // clear stage, else the chart will update and clear the error
			this.displayErrors(scope.errorHandler);
		}
	};

	return new ReportImporter();
} );

/*
	For Reference:

	<ChartType>Scatter</ChartType>
		<Categories>
		<Int>1</Int>
		</Categories>
		<Series>
		<Int>3</Int>
		</Series>
		<Series2>
		<Int>6</Int> <!-- y axis -->
		</Series2>

	<ChartType>Bubble</ChartType>
		<Categories>
		<Int>1</Int>
		</Categories>
		<Series>
		<Int>10</Int>
		</Series>
		<Series2>
		<Int>3</Int>
		</Series2>
		<Series3>
		<Int>6</Int>
		</Series3>

	<ChartType>Bar</ChartType> <ChartType>Column</ChartType> <ChartType>Line</ChartType>
	<ChartType>Area</ChartType> <ChartType>Pie</ChartType> <ChartType>Doughnut</ChartType>
		<Categories>
		<Int>1</Int>
		</Categories>
		<Series>
		<Int>10</Int>
		</Series>

	For Spline: <ChartType>Line</ChartType>

		<LineSeries>
			<Type>Spline</Type>
		</LineSeries>

	For AreaSpline: <ChartType>Area</ChartType>

		<AreaSeries>
			<Type>SplineArea</Type>
		</AreaSeries>

	<ChartType>StackedColumn</ChartType> <ChartType>StackedBar</ChartType> <ChartType>PercentStackedColumn</ChartType> <ChartType>PercentStackedBar</ChartType>
		<Categories>
		<Int>1</Int>
		</Categories>
		<Series>
		<Int>3</Int>
		<Int>8</Int> <-- if one of these is an attribute then it gets split out the way Visualizer does it
		</Series>
*/
