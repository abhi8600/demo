angular.module( 'services.genericResource', [ 'config.request' ] )

.factory( 'genericResource', function( $http, $log, DEFAULT_REQUEST_CONFIG ) {
	'use strict';

	var genericResource = function( Class ) {

		var config, getConfig, blacklist = [];

		var GenericResource = Class || function( data ) {
			if( _.isObject( data ) ) {
				angular.extend( this, data );
			} else {
				this.data = data;
			}
		};

		GenericResource.setEndpoint = function( url ) {
			config = angular.extend( {}, DEFAULT_REQUEST_CONFIG, {
				url : url
			} );
			getConfig = angular.extend( {}, config, { method : 'GET' } );
		};

		// either pass array or list of arguments to exclude from POST
		GenericResource.setBlacklist = function( list ) {
			var arrayList = _.isArray( list ) ? list : arguments;
			blacklist = list;
		};

		GenericResource.thenFactoryMethod = function ( httpPromise, successcb, errorcb ) {
			//Success and Error Callbacks
			var scb = successcb || angular.noop;
			var ecb = errorcb || angular.noop;

			var globalSuccessCallback = function( response ) {
				var result;
				// Handle collections
				if ( angular.isArray( response.data ) ) {
					result = [];
					angular.forEach( response.data, function( element ) {
						result.push ( new GenericResource( element ) );
					} );
				// Handle single instances
				} else {
					result = new GenericResource( response.data );
				}
				scb( result, response.status, response.headers, response.config );
				return result;
			};

			var globalErrorCallback = function( response ) {
				ecb( undefined, response.status, response.headers, response.config );
				return undefined;
			};

			// Return an instance when ready.
			return httpPromise.then( globalSuccessCallback, globalErrorCallback );
		};

		GenericResource.all = function() {
			return GenericResource.thenFactoryMethod( $http( getConfig ) );
		};

		Class = Class || {};

		GenericResource.prototype = angular.extend( {
			post : function() {
				var that = this;
				if( config ) {
					var reqConf = angular.extend( {}, config, { data : JSON.stringify( _.omit( angular.copy( this ), blacklist ) ) } );
					if( this.hasOwnProperty( 'uuid' ) ) {
						reqConf.url += '/' + this.uuid;
					}
					return $http( reqConf ).then( function( resp ) {
						angular.extend( that, resp.data );
					},
					function( resp ) {
						$log.info( resp.data );
					} );
				} else {
					$log.warn( 'No config was set for this resource.' );
				}
			},
			delete : function() {
				var that = this;
				var reqConf = angular.extend( {}, config, { method : 'DELETE', url : config.url + '/' + this.uuid } );
				return $http( reqConf ).then( function( resp ) {
					angular.extend( that, resp.data );
				},
				function( resp ) {
					$log.info( resp.data );
				} );
			}
		}, Class );

		return GenericResource;

	};

	return genericResource;

} );
