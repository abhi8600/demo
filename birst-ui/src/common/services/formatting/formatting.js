angular.module('visualizer')

.service('formatting', function() {
	'use strict';
	var formattingOptions = {
		table: {
			borderSizes: [0, 1, 2, 3, 4],
			fontSizes: [10, 12, 14, 16, 18, 20],
			fontPicker: ['Source Sans Pro', 'Lato', 'Arvo', 'PT Sans', 'PT Serif', 'Open Sans', 'Vollkorn', 'Old Standard TT', 'Roboto'],
			fontStyles: ['normal', 'italic', 'bold'],
			defaultColors: ['transparent', '#333333', '#555555', '#dddddd', '#f3b400', '#fafafa', '#ffffff']
		}
	};

	var formattingDefaults = {
		table: {
			headerText: {
				order: 0,
				title: 'Header Text',
				open: true,
				hasSwitch: false,
				properties: {
					font: {
						widget: 'fontPicker',
						text: 'Font face',
						key: 'font-family',
						value: 'Open Sans'
					},
					style: {
						widget: 'fontStyles',
						text: 'Font style',
						key: {
							'bold': 'font-weight',
							'italic': 'font-style',
							'normal': 'font-style'
						},
						value: 'bold'
					},
					size: {
						widget: 'fontSizes',
						text: 'Font size',
						key: 'font-size',
						value: 14
					},
					color: {
						widget: 'colorPicker',
						text: 'Font color',
						key: 'color',
						value: '#333333'
					},
					highlight: {
						widget: 'colorPicker',
						text: 'Highlight',
						key: 'background-color',
						value: 'transparent'
					}
				}
			},
			headerBackground: {
				order: 1,
				title: 'Header Background',
				open: false,
				hasSwitch: false,
				properties: {
					backgroundColor: {
						widget: 'colorPicker',
						text: 'Color',
						key: 'background-color',
						value: 'transparent'
					}
				}
			},
			bodyText: {
				order: 2,
				title: 'Cell Text',
				open: false,
				hasSwitch: false,
				properties: {
					font: {
						widget: 'fontPicker',
						text: 'Font face',
						key: 'font-family',
						value: 'Open Sans'
					},
					style: {
						widget: 'fontStyles',
						text: 'Font style',
						key: {
							'bold': 'font-weight',
							'italic': 'font-style',
							'normal': 'font-style'
						},
						value: 'normal'
					},
					size: {
						widget: 'fontSizes',
						text: 'Font size',
						key: 'font-size',
						value: 14
					},
					color: {
						widget: 'colorPicker',
						text: 'Font color',
						key: 'color',
						value: '#333333'
					},
					highlight: {
						widget: 'colorPicker',
						text: 'Highlight',
						key: 'background-color',
						value: 'transparent'
					}
				}
			},
			background: {
				order: 3,
				title: 'Alternating Rows',
				open: false,
				hasSwitch: true,
				switchState: true,
				properties: {
					backgroundColor: {
						widget: 'colorPicker',
						text: 'Even Row Color',
						key: 'background-color',
						value: '#fafafa'
					},
					altColor: {
						widget: 'colorPicker',
						text: 'Odd Row Color',
						key: 'background-color',
						value: '#ffffff'
					}
				}
			},
			borders: {
				order: 4,
				title: 'Borders',
				open: false,
				hasSwitch: true,
				switchState: true,
				properties: {
					color: {
						widget: 'colorPicker',
						text: 'Color',
						key: 'border-color',
						value: '#dddddd'
					},
					size: {
						widget: 'borderSizes',
						text: 'Size',
						key: 'border-width',
						value: 1
					}
				}
			}
		}
		// title: {

		// },
		// xAxis: {

		// },
		// yAxis: {

		// },
		// tooltip: {

		// },
		// legends: {

		// }
	};

	var formatting = angular.copy(formattingDefaults);

	var deepDiffMapper = (function() {
		return {
			VALUE_CREATED: 'created',
			VALUE_UPDATED: 'updated',
			VALUE_DELETED: 'deleted',
			VALUE_UNCHANGED: 'unchanged',
			map: function(obj1, obj2) {
				if (_.isFunction(obj1) || _.isFunction(obj2)) {
					throw 'Invalid argument. Function given, object expected.';
				}
				if (this.isValue(obj1) || this.isValue(obj2)) {
					if (this.compareValues(obj1, obj2) === 'updated') {
						return obj1;
					} else {
						return;
					}
				}

				var diff = {};
				var temp;
				for (var key in obj1) {
					if( obj1.hasOwnProperty( key ) ) {
						if (_.isFunction(obj1[key]) || key === '$$hashKey') {
							continue;
						}

						var value2;
						if ('undefined' !== typeof(obj2[key])) {
							value2 = obj2[key];
						}

						temp = this.map(obj1[key], value2);
						if (temp) {
							diff[key] = temp;
						}
					}
				}

				return diff;

			},
			compareValues: function(value1, value2) {
				if (value1 === value2) {
					return this.VALUE_UNCHANGED;
				}
				if ('undefined' === typeof(value1)) {
					return this.VALUE_CREATED;
				}
				if ('undefined' === typeof(value2)) {
					return this.VALUE_DELETED;
				}

				return this.VALUE_UPDATED;
			},
			isValue: function(obj) {
				return !_.isObject(obj) && !_.isArray(obj);
			}
		};
	})();

	var removeEmptyObjects = function(obj) {
		for (var key in obj) {
			if( obj.hasOwnProperty( key )  ) {
				if (_.isObject(obj[key]) && _.keys(obj[key]).length !== 0) {
					removeEmptyObjects(obj[key]);
				}
				if (_.isObject(obj[key]) && _.keys(obj[key]).length === 0) {
					delete obj[key];
				}
			}
		}
		return obj;
	};

	return {

		getAllFormatting: function() {
			return formatting;
		},

		setFormatting: function(obj, key1, key2) {
			if (key1 && key2 && obj) {
				formatting[key1][key2] = obj;
			} else if (key1 && obj) {
				formatting[key1] = obj;
			} else if (obj) {
				formatting = obj;
			}
		},

		export: function(newFormatting) {
			return removeEmptyObjects(deepDiffMapper.map(newFormatting, formattingDefaults.table));
		},

		import: function(savedFormatting) {
			formatting.table = $.extend(true, {}, formattingDefaults.table, savedFormatting);
		},

		getFormattingOptions: function(key1, key2) {
			if (formattingOptions[key1] && formattingOptions[key1][key2]) {
				return formattingOptions[key1][key2];
			} else if (formattingOptions[key1] && !formattingOptions[key1][key2]) {
				return formattingOptions[key1];
			} else {
				return;
			}
		}

	};
} );
