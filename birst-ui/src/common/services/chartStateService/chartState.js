//save chart state, for now just title, eventually localization, format (colors, fonts, etc.)
/* gloabals jasmine */
angular.module('visualizer')

.factory('chartStateService', function( globalStateService ) {
	'use strict';

    var chartTitle = 'Type Title'; //start with generic title, then hold set state and propagate to new charts
    var chartAutoMode = false; // auto mode is set to manual initially...when user toggles "auto" switch UI, toggle mode here and remember for other chart types


    var shapes = ['circle', 'diamond', 'triangle', 'triangle-down', 'star', 'plus', 'sextagon', 'pentagon', 'octagon', 'snowflake', 'uparrowhead', 'spacemine', 'castle', 'zigzag', 'bowtie'];
    var nativeChartTypes = ['column', 'bar', 'line', 'spline', 'area', 'areaspline', 'points', 'scatter', 'bubble', 'pie', /*'geo-map', 'heat-map',*/ 'table'];
    var uiOnlyChartTypes = ['points'];

    //this var represents the selected chart type--source of truth on what the currently-selected chart type is
    var currentChartType = 'column';
    var uiChartType = 'column';

    // Some decent color gradients from colorbrewer2.org - RGB values
    var gradientList = [
        [
            [247, 251, 255],
            [222, 235, 247],
            [198, 219, 239],
            [158, 202, 225],
            [107, 174, 214],
            [66, 146, 198],
            [33, 113, 181],
            [8, 81, 156],
            [8, 48, 107]
        ],
        [
            [255, 245, 235],
            [254, 230, 206],
            [253, 208, 162],
            [253, 174, 107],
            [253, 141, 60],
            [241, 105, 19],
            [217, 72, 1],
            [166, 54, 3],
            [127, 39, 4]
        ],
        [
            [247, 251, 255],
            [222, 235, 247],
            [198, 219, 239],
            [158, 202, 225],
            [107, 174, 214],
            [66, 146, 198],
            [33, 113, 181],
            [8, 81, 156],
            [8, 48, 107]
        ],
        [
            [255, 255, 255],
            [240, 240, 240],
            [217, 217, 217],
            [189, 189, 189],
            [150, 150, 150],
            [115, 115, 115],
            [82, 82, 82],
            [37, 37, 37],
            [0, 0, 0]
        ]
    ];

    //keeping core structure abstructed so in the future can include other attribs like localization etc common to all charts
    var chartFormat = {
        color: 'rgb(77, 117, 158)',
        fontWeight: 'bold',
        fontFamily: '"Helvetica Neue",Helvetica,Arial,sans-serif',
        fontSize: '12px',
        fontStyle: 'normal',
        visible: true,
        originalCaption: null // this curious var is used to hold editable element's caption at start of edit in case user edits text
    };

    var formatChartTitle = _.extend({}, chartFormat);
    var formatChartX = _.extend({}, chartFormat);
    var formatChartY = _.extend({}, chartFormat);
    var formatChartLegend = _.extend({}, chartFormat);

    formatChartTitle.fontSize = '20px';
    formatChartY.originalCaption = [];

    var previousDimensionValues = {};

    return {

        reportExpressions: [],

        setTitle: function(strTitle) {
            chartTitle = strTitle;
            return 0;
        },

        getTitle: function() {
            //careful not to display "Type Title" in reportView on dashboard--only display chart titles there if there's an actual meaningful title
            if (!globalStateService.isTitleAndEditingEnabled() && chartTitle === 'Type Title') {
                return "";
            }
            return chartTitle;
        },

        getChartAutoMode: function() {
            return chartAutoMode;
        },

        setChartAutoMode: function(mode) {
            chartAutoMode = mode;
            return 0;
        },

        getColorPalette: function() {
            var allColors = ["#91c4e5", "#a7c877", "#bdaadd", "#57cec7", "#d7d243", "#6295b7", "#7f9c53", "#876eb1", "#3c8f89", "#9a961e", "#e37e8f", "#f3b400", "#fe95f3", "#fe7f48", "#ead037", "#a35b67", "#b37e00", "#c65dbc", "#d15f2e", "#c1a401", "#c7a083", "#a6c2d4", "#d9c5d9", "#c8ceae", "#b9baba", "#876d59", "#6d808c", "#958095", "#888c76", "#797a7a"];
            return allColors;
        },

        generateColor: function(color, index) {
            var r = parseInt(color.substr(1, 2), 16);
            var g = parseInt(color.substr(3, 2), 16);
            var b = parseInt(color.substr(5, 2), 16);

            var hsl = _.rgbToHsl(r, g, b);
            var colorModifier = index * 0.005;
            var h = hsl[0];
            var s = hsl[1];
            var l = hsl[2];

            if (h + colorModifier >= 1) {
                h = h - colorModifier;
            } else if (h + colorModifier <= 1) {
                h = h + colorModifier;
            }

            if (s + colorModifier >= 1) {
                s = s - colorModifier;
            } else if (h + colorModifier <= 1) {
                s = s + colorModifier;
            }

            var hex = _.hslToRgb(h, s, l);
            return hex;
        },

        // use colors 1-29, then for 30 send color 1 and 30 as modifier

        getColorForSeries: function(totalSeries, seriesIndex, colorIndex) {
            var seriesColors;
            var allColors = this.getColorPalette();
            var coolColors = ["#91c4e5", "#a7c877", "#bdaadd", "#57cec7", "#d7d243", "#6295b7", "#7f9c53", "#876eb1", "#3c8f89", "#9a961e"];
            var warmColors = ["#e37e8f", "#f3b400", "#fe95f3", "#fe7f48", "#ead037", "#a35b67", "#b37e00", "#c65dbc", "#d15f2e", "#c1a401"];
            var greyGreens = ["#c7a083", "#a6c2d4", "#d9c5d9", "#c8ceae", "#b9baba", "#876d59", "#6d808c", "#958095", "#888c76", "#797a7a"];
            if (totalSeries === 1) {
                return allColors[colorIndex] || this.generateColor(allColors[colorIndex % allColors.length], colorIndex);
            }
            if (totalSeries === 2) {
                seriesColors = seriesIndex === 0 ? coolColors : warmColors;
                return seriesColors[colorIndex] || this.generateColor(seriesColors[colorIndex % seriesColors.length], colorIndex);
            }
            // 3 or more.
            var colors1 = _.difference(coolColors, greyGreens);
            var colors2 = _.difference(warmColors, greyGreens);
            var colors3 = greyGreens;

            seriesColors = ([colors1, colors2, colors3])[seriesIndex % 3];
            // partition the colors among the series sharing the same color palette, in case n > 3

            // I'm sure there's a closed-form solution to this but I'm too tired to think of it.
            var seriesUsingTheseColors = 0;
            for (var ii = 0; ii < totalSeries; ii++) {
                if (ii % 3 === seriesIndex % 3) {
                    seriesUsingTheseColors++;
                }
            }
            var shareIndex = Math.floor(seriesIndex / 3);
            //console.log(shareIndex, seriesColors.length, seriesUsingTheseColors);
            seriesColors = seriesColors.slice(
                Math.round(seriesColors.length * shareIndex / seriesUsingTheseColors),
                Math.round(seriesColors.length * (shareIndex + 1) / seriesUsingTheseColors));
            //console.log("seriesColors", seriesColors);

            return seriesColors[colorIndex % seriesColors.length];
        },

        getShapeForSeries: function(totalSeries, seriesIndex, shapeIndex) {
            var seriesShapes = shapes.slice(Math.round(shapes.length * seriesIndex / totalSeries), Math.round(shapes.length * (seriesIndex + 1) / totalSeries));
            return seriesShapes[shapeIndex % seriesShapes.length];
        },

        getShapeChoices: function() {
            return shapes;
        },

        getNativeChartTypes: function() {
            return nativeChartTypes;
        },

        getCurrentChartType: function() {
            //console.log('getCurrentChartType', currentChartType);
            return currentChartType;
        },

        getUIChartType: function() {
            //console.log('getCurrentChartType', currentChartType);
            return uiChartType;
        },

        //pass in desired chart type, kick out if not supported; otherwise, set currentChartType to desired selection
        setCurrentChartType: function(desiredChartType) {
            if (_.contains(nativeChartTypes.concat(uiOnlyChartTypes), desiredChartType)) {
                /*
             if ( desiredChartType === 'bubble' || desiredChartType === 'bubble' ) {
             uiChartType = desiredChartType;
             currentChartType = 'scatter';
             } else {
             uiChartType = desiredChartType;
             currentChartType = desiredChartType;
             }*/
                uiChartType = desiredChartType;
                currentChartType = desiredChartType;
            } else {
                //console.log('refused chart type ' + desiredChartType);
            }
        },


        getColorGradientObject: function(gradientNumber, fraction) {
            var colorStops = gradientList[gradientNumber] || gradientList[0];

            // Don't allow colors that are too faint.
            fraction = 0.2 + 0.8 * fraction;

            var len = colorStops.length - 1;

            // Determine the closest gradient color stop
            var whole = Math.floor(fraction * len);

            // Make sure that we don't go past the array, in case fraction is exactly 1.0
            if (whole === len) {
                --whole;
            }

            // The fraction left over from this color stop to the next
            var part = fraction * len - whole;

            // Interpolate through rgb color space from previous to next color stop
            var rgb = _.map(colorStops[whole], function(coord, ii) {
                return Math.round(coord * (1 - part) + colorStops[whole + 1][ii] * part);
            });
            return {
                r: rgb[0],
                g: rgb[1],
                b: rgb[2]
            };
        },

        getColorGradient: function(gradientNumber, fraction) {
            var color = this.getColorGradientObject(gradientNumber, fraction);
            return "rgb(" + [color.r, color.g, color.b].join(",") + ")";
        },

        //called by routines generating highcharts to get formatting for title + axes
        //call is along the lines of: font = chartStateService.getChartFormat('XAxis').font
        getChartFormat: function(whichElement) {
            switch (whichElement) {
                case 'Title':
                    return formatChartTitle;
                case 'XAxis':
                    return formatChartX;
                case 'YAxis':
                    return formatChartY;
                case 'Legend':
                    return formatChartLegend;
            }
        },

        //call from where chart texts are being edited (e.g. layout.js for title), pass in name of element to format and attribs e.g. color etc.
        //note: for right now, X & Y formatting are synchronized
        setChartFormat: function(element, attribs) {

            /* for now, disable global font family selection so it's no longer synched between title+x+y
            if (element === 'Title' || element === 'XAxis' || element === 'YAxis') {
                if (attribs.fontFamily) {
                    formatChartX.fontFamily = formatChartY.fontFamily = formatChartTitle.fontFamily = attribs.fontFamily;
                }
            }
            */

            if (element === 'Title') {
                if (attribs.color) {
                    formatChartTitle.color = attribs.color;
                }
                if (attribs.fontWeight) {
                    formatChartTitle.fontWeight = attribs.fontWeight;
                }
                if (attribs.fontSize) {
                    formatChartTitle.fontSize = attribs.fontSize;
                }
                if (attribs.fontStyle) {
                    formatChartTitle.fontStyle = attribs.fontStyle;
                }
                if (attribs.fontFamily) {
                    formatChartTitle.fontFamily = attribs.fontFamily;
                }
                if (attribs.visible) {
                    formatChartTitle.visible = attribs.visible;
                }
            }
            if (element === 'XAxis' || element === 'YAxis') {
                if (attribs.color) {
                    formatChartX.color = formatChartY.color = attribs.color;
                }
                if (attribs.fontWeight) {
                    formatChartX.fontWeight = formatChartY.fontWeight = attribs.fontWeight;
                }
                if (attribs.fontSize) {
                    formatChartX.fontSize = formatChartY.fontSize = attribs.fontSize;
                }
                if (attribs.fontStyle) {
                    formatChartX.fontStyle = formatChartY.fontStyle = attribs.fontStyle;
                }
            }
            if (element === 'XAxis') {
                if (attribs.visible) {
                    formatChartX.visible = attribs.visible;
                }
            }
            if (element === 'YAxis') {
                if (attribs.visible) {
                    formatChartY.visible = attribs.visible;
                }
            }
            if (element === 'Legend') {
                return formatChartLegend;
            }
        },


        //called from layout.js, pass in DOM element and its "this" an whatever CSS styles are in "this" will be applied to element's CSS and saved in model
        applyFormattingfromEditor: function(element, caption, editor, typeOfelement) {
            if (!editor) {
                return;
            }
            var styleObject = {
                'color': editor.style.color,
                'fontWeight': editor.style.fontWeight,
                'fontSize': editor.style.fontSize,
                'fontFamily': editor.style.fontFamily,
                'fontStyle': editor.style.fontStyle
            };
            element.text(caption);
            element.css(styleObject);
            this.setChartFormat(typeOfelement, styleObject);
            return element;
        },


        saveOriginalCaption: function(whichElement, caption, index) {
            switch (whichElement) {
                case 'Title':
                    formatChartTitle.originalCaption = caption;
                    break;
                case 'XAxis':
                    formatChartX.originalCaption = caption;
                    break;
                case 'YAxis':
                    formatChartY.originalCaption[index] = caption;
                    break;
            }

        },

        revertOriginalCaption: function(whichElement, index) {
            switch (whichElement) {
                case 'Title':
                    return (formatChartTitle.originalCaption);
                case 'XAxis':
                    return (formatChartX.originalCaption);
                case 'YAxis':
                    return (formatChartY.originalCaption[index]);
            }

        },

        //when user changes X axis label, the staging element represented in the X axis needs to be renamed as well
        renameColumnInStaging: function(whichElement, newColumnName, stageScope) {
            switch (whichElement) {
                case 'XAxis':
                    stageScope.Category.name = newColumnName;
                    break;
                case 'YAxis':
                    break;
            }
        },

        getPreviousDimensionValues: function() {
            return previousDimensionValues;
        },

        setPreviousDimensionValues: function(val) {
            previousDimensionValues = val;
        },

        resetPreviousDimensionValues: function() {
            previousDimensionValues = {};
        },

        reportExpressionId: function() {
            var previousIds = _.pluck(this.reportExpressions, 'reportExpressionId');
            previousIds.push(1);
            return _.max(previousIds) + 1;
        }

    };
} );
