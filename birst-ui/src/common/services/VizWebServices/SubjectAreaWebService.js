/* globals localStorage */
/* jshint bitwise:false */
angular.module('visualizer')

.factory('subjectAreaWebService', function( $http, $q ) {
	'use strict';
    function toJSON2(xmlString) {
        // Faster version of toJSON. Leaving toJSON in as reference implementation, because this code is less straightforward.

        var stack = [{
            children: []
        }];
        var nextTag = "n";
        var nodes = 0;

        function transformAttributes(attrs) {
            var ret = {};
            ret.type = attrs.t;

            if (ret.type === 'm') {
                ret.type = 'measure';
            } else if (ret.type === 'a') {
                ret.type = 'attribute';
            } else if (ret.type === 'f') {
                ret.type = 'folder';
            } else if (ret.type === 's') {
                ret.type = 'saved expression';
            }

            ret.label = attrs.l || attrs.v;
            ret.value = attrs.v;
            ret.valid = attrs.vl;
            ret.visible = attrs.visible;
            ret.reportLabel = attrs.rl;
            ret.description = attrs.d;

            if (ret.visible === undefined) {
                ret.visible = 'true';
            }

            return ret;
        }

        function saxParse(str, onOpenTag, onText, onCloseTag) {
            var where = 0,
                inTag = false;

            function attrStringMap(attrString) {
                var parts = attrString.split("=");
                var leftPart = parts[0];
                var rightPart = parts.slice(1).join("=");
                rightPart = rightPart.replace(/^\s*['"]?|['"]?\s*$/g, "");
                rightPart = ~rightPart.indexOf('&') ? _.htmlUnescape(rightPart) : rightPart;
                attrMap[leftPart.replace(/^\s*|\s*$/g, "")] = rightPart;
            }

            while (true) {
                if (where === str.length) {
                    break;
                }
                if (!inTag) {
                    var next = str.indexOf("<", where);
                    if (next === -1) {
                        next = str.length;
                    }
                    onText(str.substring(where, next));
                    where = next;
                    inTag = true;
                } else {
                    var close = str.indexOf(">", where);
                    var tag = str.substring(where, close + 1);
                    var tagName = tag.match(/^<\s*\/?\s*([a-zA-Z0-9]+)/)[1];
                    var attrs = tag.match(/[a-zA-Z0-9]+=('[^']*'|"[^"]*"|\w*)/g);
                    var attrMap = {};
                    if (attrs) {
                        attrs.map(attrStringMap);
                    }
                    if (tag.match(/SavedExpression/)) {
                        //console.log(tag, attrs, attrMap);
                    }
                    var isSelfClosing = tag.match(/\/\s*>$/);
                    var isClosing = tag.match(/^<\s*\//);
                    if (isClosing) {
                        onCloseTag(tagName);
                    } else {
                        onOpenTag(tagName, attrMap, tag);
                        if (isSelfClosing) {
                            onText("");
                            onCloseTag(tagName);
                        }
                    }
                    where = close + 1;
                    inTag = false;
                }
            }
        }

        saxParse(xmlString, function(tagName, attrs) {
            if (tagName === nextTag) {
                if (tagName === 'c') {
                    nextTag = "n";
                    ++nodes;
                } else {
                    nextTag = "c";
                    var obj = transformAttributes(attrs);
                    obj.children = [];
                    stack[stack.length - 1].children.push(obj);
                    stack.push(obj);
                }
            }
        }, function() {}, function(tagName) {
            if (tagName !== nextTag) {
                if (tagName === 'n') {
                    nextTag = "n";
                    stack.pop();
                } else if (tagName === 'c') {
                    nextTag = "c";
                }
            }
        });

        return stack[0].children;
    }

    function SubjectAreaWebService($http, $q) {
        this.$http = $http;
        this.$q = $q;

        if (this.$http === undefined && angular && angular.injector) {
            this.$http = angular.injector(['ng']).get('$http');
        }

        this.keepAlive = false;

        this.cachedSubjectAreaResponse = null;
    }


    //SubjectAreaWebService.prototype.baseUrl = globalProtocol + '://' + globalURL + ':' + globalPort + '/SMIWeb/services/SubjectArea.SubjectAreaHttpSoap11Endpoint/';
    SubjectAreaWebService.prototype.baseUrl = '/SMIWeb/services/SubjectArea.SubjectAreaHttpSoap11Endpoint/';


    SubjectAreaWebService.prototype.post = function(urn, data, success, error) {
        var $http = this.$http;

        var noOp = function() {};

        //console.log('CSRF token = ', localStorage.getItem('csrfToken'));

        if ($http) {
            delete this.$http.defaults.headers.common['X-Requested-With'];

            $http({
                method: 'POST',
                url: this.baseUrl,
                data: data,
                dataType: 'xml',
                headers: {
                    'Accept': '*/*',
                    'Content-Type': 'text/xml; charset=utf-8',
                    'SOAPAction': urn, //'"urn:getSubjectArea"',
                    'X-Anti-CSRF': localStorage.getItem('csrfToken')
                }
            }).success(success || noOp).error(error || noOp);
        } else if ($) {
            $.ajax({
                type: 'POST',
                url: this.baseUrl,
                data: data,
                dataType: 'xml',
                contentType: "text/xml",
                success: success,
                error: error
            });
        } else {
            error();
        }
    }; //getVisualizerUpdatedSubjectArea

    SubjectAreaWebService.prototype.updatedSubjectAreaSuccess = function(xml, success, error) {
        var delimiter = '\\|~%#';

        var $xml = $(xml);
        var result = $xml.find('Result'); // ns: removed because of http://stackoverflow.com/questions/128580/jquery-find-problem
        // apparently couldnt use getSubjectAreaResponse without ns either
        //console.log('result', result);
        //console.log('result.length', result.length);
        //console.log('xml', xml);

        try {
            if (result.length === 0) {
                var $faultcode = $xml.find('faultcode').text();
                var $faultstring = $xml.find('faultstring').text();

                if ($faultcode) {
                    error('error (' + $faultcode + '): ' + $faultstring);
                } else {
                    error('unknown error');
                }
            } else {
                if (result.text().length > 0) {
                    var formattedResult = result.text().replace(/&lt;/g, '<');

                    //console.log(formattedResult);

                    var $formattedResult = $('<Result></Result>').append(formattedResult);

                    // first parse the trees, then split the delimiters <SA name=""><n><v>PATH</v><vl>BOOL</vl><av>BOOL for unknown purpose</av></SA>

                    var subjectAreas = {};

                    _.each($formattedResult.find('SA'), function(subjectArea) {
                        var $subjectArea = $(subjectArea);

                        var subjectAreaName = $subjectArea.attr('name');

                        if (subjectAreaName === '') {
                            subjectAreaName = 'Default Subject Area';
                        }

                        var nodes = [];

                        _.each($subjectArea.find('n'), function(node) {
                            var jsonNode = {};

                            var $node = $(node);

                            var path = $node.find('v').text();

                            jsonNode.path = path.split(window.RegExp(delimiter, 'g')).slice(1);

                            jsonNode.valid = $node.find('vl').text() === 'true';

                            nodes.push(jsonNode);
                        });

                        subjectAreas[subjectAreaName] = nodes;
                    });

                    success(subjectAreas);
                } else {
                    error('couldnt parse response');
                }
            }
        } catch (e) {
            //console.log(e);
            error(e);
        }
    };

    SubjectAreaWebService.prototype.getVisualizerUpdatedSubjectArea = function(query, success, error) {
        var that = this;

        this.post(
            '"urn:getVisualizerUpdatedSubjectArea"',

            '<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">' +
            '<SOAP-ENV:Body>' +
            '<ns:getVisualizerUpdatedSubjectArea xmlns:ns="http://SubjectArea.WebServices.successmetricsinc.com">' +
            '<ns:query>' + _.htmlEscape(query) + '</ns:query>' +
            '</ns:getVisualizerUpdatedSubjectArea>' +
            '</SOAP-ENV:Body>' +
            '</SOAP-ENV:Envelope>',

            function(xml) {
                that.updatedSubjectAreaSuccess(xml, success, error);
            },

            error
        );
    };


    SubjectAreaWebService.prototype.listSubjectAreas = function(success, error) {
        var that = this;

        function successWrapper(xml) {
            that.cachedSubjectAreaResponse = xml;

            var $xml = $(xml);
            var result = $xml.find('Result'); // ns: removed because of http://stackoverflow.com/questions/128580/jquery-find-problem
            // apparently couldnt use getSubjectAreaResponse without ns either
            //console.log('result', result);
            //console.log('result.length', result.length);
            //console.log('xml', xml);

            if (result.length === 0) {
                var $faultcode = $xml.find('faultcode').text();
                var $faultstring = $xml.find('faultstring').text();

                if ($faultcode) {
                    error('error (' + $faultcode + '): ' + $faultstring);
                } else {
                    error('unknown error');
                }
            } else {
                var resultText = result.text();

                if (resultText.length === 0) {
                    /*var resultStart = xml.indexOf('<Result>');
                    var resultEnd   = xml.indexOf('</Result>');

                    resultText = xml.substring(resultStart + resultStart.length, resultEnd - resultEnd.length + 1);
                    //console.log('resultText', resultText);*/

                    resultText = xml;
                    //console.log(resultText);
                }

                that.jsonResult = toJSON2(resultText);
                var jsonResult = that.jsonResult;
                var subjectAreas = [];

                if (jsonResult && jsonResult.length > 0) {
                    if (jsonResult[0].label === 'Subject Areas') {
                        subjectAreas = jsonResult[0].children || [];
                    } else {
                        subjectAreas = jsonResult;
                    }
                }

                var subjectAreaNames = _.map(subjectAreas, function(subjectArea) {
                    return subjectArea.label;
                });

                success(subjectAreaNames);
            }
        }

        if (this.cachedSubjectAreaResponse) {
            //console.log('using cached subject area response');
            successWrapper(this.cachedSubjectAreaResponse);
            return;
        }

        this.post(
            '"urn:getSubjectArea"',
            '<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">' +
            '<SOAP-ENV:Body/>' +
            '</SOAP-ENV:Envelope>',
            successWrapper,

            error
        );
    };

    SubjectAreaWebService.prototype.getSubjectAreaContentRaw = function(subjectAreaName, success, error) {
        var that = this;

        function successWrapper(xml) {
            that.cachedSubjectAreaResponse = xml;

            var $xml = $(xml);
            var result = $xml.find('Result'); // ns: removed because of http://stackoverflow.com/questions/128580/jquery-find-problem
            // apparently couldnt use getSubjectAreaResponse without ns either
            //console.log('result', result);
            //console.log('result.length', result.length);
            //console.log('xml', xml);

            if (result.length === 0) {
                var $faultcode = $xml.find('faultcode').text();
                var $faultstring = $xml.find('faultstring').text();

                if ($faultcode) {
                    error('error (' + $faultcode + '): ' + $faultstring);
                } else {
                    error('unknown error');
                }
            } else {
                var resultText = result.text();

                if (resultText.length === 0) {
                    //var resultStart = xml.indexOf('<Result>');
                    //var resultEnd   = xml.indexOf('</Result>');

                    resultText = xml;
                }

                success(resultText);
            }
        }

        if (this.jsonResult) {
            //console.log('using cached subject area response');
            success(this.jsonResult);
            return;
        } else if (this.cachedSubjectAreaResponse) {
            //console.log('using cached subject area response');
            successWrapper(this.cachedSubjectAreaResponse);
            return;
        }

        //console.log('not using mocked SAS getSubjectAreaContent response');
        this.post(
            '"urn:getSubjectArea"',
            '<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">' +
            '<SOAP-ENV:Body/>' +
            '</SOAP-ENV:Envelope>',
            successWrapper,

            error
        );
    };

    SubjectAreaWebService.prototype.getSubjectAreaContent = function(subjectAreaName, success, error) {
        this.getSubjectAreaContentRaw(
            subjectAreaName,

            function(nodes) {
                var jsonResult;
                if (typeof nodes !== 'object') {
                    jsonResult = toJSON2(nodes);
                } else {
                    jsonResult = nodes;
                }

                var subjectAreas = [];

                if (jsonResult && jsonResult.length > 0) {
                    if (jsonResult[0].label === 'Subject Areas') {
                        subjectAreas = jsonResult[0].children || [];
                    } else {
                        subjectAreas = jsonResult;
                    }
                }

                var subjectArea = _.find(subjectAreas, function(subjectArea) {
                    return subjectArea.label === subjectAreaName || subjectArea.label == null && subjectAreaName === 'Default Subject Area';
                });

                // trying to match the return value from CWS to maintain consistency

                success({
                    'root': 'SubjectAreaContent',
                    'children': [{
                        label: 'Subject Areas',
                        type: 'folder',
                        visible: 'true',
                        children: [subjectArea]
                    }]
                });
            },

            error
        );
    };

    visualizer.webservices.SubjectAreaWebService = SubjectAreaWebService;

    return new SubjectAreaWebService();

});

