describe( 'math service', function() {
	'use strict';

	var math;

	beforeEach( function() {
		module( 'services.math' );
		inject( function( _math_ ) {
			math = _math_;
		} );
	} );

	it( 'should create a percent from a part of a whole', function() {
		expect( math.percent( 50, 100 ) ).toBe( 50 );
	} );

} );
