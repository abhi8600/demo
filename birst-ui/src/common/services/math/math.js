angular.module( 'services.math', [] )

.factory( 'math', function() {
	'use strict';

	return {
		percent : function( part, whole ) {
			return ( part / whole ) * 100;
		},
	};

} );