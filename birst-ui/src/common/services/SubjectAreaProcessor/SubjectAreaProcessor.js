angular.module('visualizer')
.service('SubjectAreaProcessor', ['AdhocWebService', function(AdhocWebService) {
	'use strict';
    function SubjectAreaProcessor() {
        this.attributes = {};
        this.attributesByFolder = {}; //almost same as attributes, re-ordered so can display as accordion in SA
        this.measures = {};
        this.uncategorized = {};
        this.measureBQLMap = {};
        this.timeSeries = [];
        this.disallowedChars = ['[', ']', '.']; //list of chars that are not allowed to be in names, esp. 'uncategorized', so reject them before adding to subject area
    }

    // intended to be private

    SubjectAreaProcessor.prototype.reset = function() {
        this.attributes = {};
        this.attributesByFolder = {}; //almost same as attributes, re-ordered so can display as accordion in SA
        this.measures = {};
        this.uncategorized = {};
        this.measureBQLMap = {};
        this.timeSeries = [];
        this.disallowedChars = ['[', ']', '.'];
    };

    SubjectAreaProcessor.prototype.buildPath = function(parents, lastPart) {
        var path;

        if (lastPart) {
            path = [lastPart].concat(parents.map(function(p) {
                return p.label;
            })).reverse();
        } else {
            path = parents.map(function(p) {
                return p.label;
            }).reverse();
        }

        return path;
    };

    // getters exposed for testing purposes only

    SubjectAreaProcessor.prototype.getMeasures = function() {
        return this.measures;
    };

    SubjectAreaProcessor.prototype.getAttributes = function() {
        return this.attributes;
    };

    SubjectAreaProcessor.prototype.getUncategorized = function() {
        return this.uncategorized;
    };

    SubjectAreaProcessor.prototype.addToAttributes = function(tree, parents) {
        var path = this.buildPath(parents, tree.value);

        // discard Attributes folder and any folders above it in path (in the case of a top-level orphan expression, wouldn't need to do this)
        // may want to discard top N anyway that are not relevant

        path = path.slice(path.indexOf("Attributes") + 1);

        //gj-1/2   -- in order to handle multiple folder structures (e.g. custom subj area), seems like it's safe to take the 2 last vars in path
        //in "normal" data sets with no custom subject area, there are only 2 vars [[key],[value]], so nothing has changed
        //in custom subject area, path is like [null,"blah,"blah",[key],[value]] so we just need the key/value so extracting the last 2 seems to work with spaces we have

        var pathLength = path.length;
        path = [path[pathLength - 2], path[pathLength - 1]];

        var key = JSON.stringify(path);

        this.attributes[key] = this.attributes[key] || {
            path: path,
            displayName: tree.label ? _.last(tree.label.split('.')) : path[path.length - 1],
            label: path[path.length - 1], // possibly === tree.value, check with unit test
            type: 'attribute'
        };

        return this.attributes[key];
    };

    SubjectAreaProcessor.prototype.addToUncategorized = function(treeNode, ancestorList, subtype) {
        /*
                case "saved expression":
            return "" + this.head.value.replace(/&apos;/g, '\'');
        */

        var label = treeNode.value.replace(/&apos;/g, '\'');
        label = label.substring("SavedExpression('".length, label.length - 2);

        //console.log('found uncategorized with ancestorList', ancestorList);

        var path = this.buildPath(ancestorList, label);

        if (subtype === 'attribute') {
            path = path.slice(path.indexOf("Attributes") + 1);
        }

        //console.log('path = ', path);

        // discard Attributes folder and any folders above it in path (in the case of a top-level orphan expression, wouldn't need to do this)
        // may want to discard top N anyway that are not relevant

        // path = path.slice(path.indexOf("Attributes") + 1);

        var key = JSON.stringify(path);

        var node = {
            path: path,
            displayName: treeNode.label ? _.last(treeNode.label.split('.')) : label,
            label: label, // possibly === tree.value, check with unit test
            value: treeNode.value,
            type: 'saved expression',
            subtype: subtype
        };

        if (!node.subtype) {
            AdhocWebService.getColumnDefinition(node.value, function(columnDefinition) {
                node.subtype = columnDefinition.isMeasureExpression ? 'measure' : 'attribute';
                return;
            });
        }

        var shouldSkip = false;

        //reject from subject area expressions whose names contain disallowed characters
        _.each(this.disallowedChars, function(char) {
            if (_.indexOf(node.label, char) > -1) {
                shouldSkip = true;
            }
        });

        if (!shouldSkip) {
            if (subtype === 'measure') {
                this.measures[key] = this.measures[key] || node;
                return this.measures[key];
            } else if (subtype === 'attribute') {
                this.attributes[key] = this.attributes[key] || node;
                return this.attributes[key];
            } else {
                this.uncategorized[key] = this.uncategorized[key] || node;
                return this.uncategorized[key];
            }
        }

    };

    SubjectAreaProcessor.prototype.addToMeasures = function(tree, parents, errorHandler) {
        //console.log('parents', parents)
        var aggregation = tree.label.replace(/^\s*/, '');
        var dateType = null;

        // TODO: This regex won't be completely reliable, should get the information about whether this folder is a date type more explicitly.

        if (parents[0].label.match(/^By /)) {
            dateType = parents.shift().label;
        }

        var path = this.buildPath(parents);

        var timeSeriesLabel;

        var tsIndex = path.indexOf('Time Series Measures');

        if (tsIndex !== -1) {
            try {
                timeSeriesLabel = tree.label.match(/(.*?) ?(SUM|COUNT|COUNT DISTINCT|AVG|MIN|MAX)/i)[1];
            } catch (unknown_error) {
                timeSeriesLabel = '';
                errorHandler({
                    basicText: visualizer.misc.errors.ERR_SA_PROCESS_BASIC,
                    moreInfoText: visualizer.misc.errors.ERR_SA_PROCESS_MORE
                });
                //console.log('Unknown error while processing subject area.', tree);
            }

            this.addUnique(this.timeSeries, timeSeriesLabel);
            this.measureBQLMap[JSON.stringify([path[path.length - 1], aggregation.match(/sum|count|count distinct|avg|min|max/i)[0], dateType, timeSeriesLabel])] = tree.value;
            return;
        }

        //GJ hack in case no folders  under Measures in Pinterest
        var pathX = path;

        path = path.slice(path.indexOf("Measures") + 1);

        if (path.length < 1) {
            path = pathX.slice(pathX.indexOf("Measures"));
        }

        var key = JSON.stringify(path);

        if (!this.measures[key]) {
            this.measures[key] = {
                path: path,
                displayName: path[path.length - 1].replace(/_/g, ' '),
                bqlString: _.bind(this.bqlString, this),
                aggregations: [aggregation],
                dateTypes: dateType ? [dateType] : [],
                timeSeriesTypes: this.timeSeries,
                label: path[path.length - 1].replace(/_/g, ' '),
                type: 'measure'
            };
        }

        this.addUnique(this.measures[key].aggregations, aggregation);

        if (dateType) {
            this.addUnique(this.measures[key].dateTypes, dateType);
        }

        this.measureBQLMap[JSON.stringify([path[path.length - 1], aggregation, dateType, null])] = tree.value;

        return this.measures[key];
    };

    // returns true if element was unique and pushed

    SubjectAreaProcessor.prototype.addUnique = function(list, elem) {
        if (list.indexOf(elem) === -1) {
            list.push(elem);
            return true;
        } else {
            return false;
        }
    };

    //gj->return something like attributes but with folders for accordion in SA

    SubjectAreaProcessor.prototype.returnAttrByFolders = function() {
        var that = this;

        var attr = Object.keys(this.attributes).map(function(key) {
            return that.attributes[key];
        });

        var groups = {};

        _.each(attr, function(item) {
            var top = item.path[0];

            if (groups[top] === undefined) {
                groups[top] = [item];
            } else {
                groups[top].push(item);
            }
        });

        return _.sortBy(_.map(groups, function(group) {
            return _.sortBy(group, function(a) {
                // first sort the last component of the attribute (should be column name)
                return _.last(a.label.split('.'));
            });
        }), function(group) {
            // second sort the first component of the attribute (should be folder name)
            return _.first(group[0].label);
        });
    };

    SubjectAreaProcessor.prototype.bqlString = function(measureName, aggregation, dateType, timeSeriesLabel) {
        return this.measureBQLMap[JSON.stringify([measureName, aggregation, dateType, timeSeriesLabel])];
    };

    SubjectAreaProcessor.prototype.processNode = function(treeNode, ancestorList, topFolder, errorHandler) {
        var that = this;

        if (!treeNode) {
            //console.log('Empty Subject Area');
            return;
        }

        if (treeNode.children && treeNode.children.length) {
            if (!topFolder && (treeNode.label === 'Measures' || treeNode.label === 'Attributes')) {
                topFolder = treeNode.label;
            }

            treeNode.children.map(function(childNode) {
                that.processNode(childNode, [treeNode].concat(ancestorList), topFolder, errorHandler);
            });
        } else if (treeNode.type === 'measure') {
            that.addToMeasures(treeNode, ancestorList, errorHandler);
        } else if (treeNode.type === 'attribute') {
            that.addToAttributes(treeNode, ancestorList);
        } else if (treeNode.type === 'saved expression') {
            //console.log('found saved expression');
            if (topFolder === 'Measures') {
                that.addToUncategorized(treeNode, ancestorList, 'measure');
            } else if (topFolder === 'Attributes') {
                that.addToUncategorized(treeNode, ancestorList, 'attribute');
            } else {
                that.addToUncategorized(treeNode, ancestorList);
            }
        }
    };

    SubjectAreaProcessor.prototype.process = function(subjectArea, name, scope) {
        //subjectArea.children[0].children[0].children.push({'type':'folder','description':'Custom Expressions','label':'Custom-Expressions',visible:'true', 'reportLabel': null, 'valid': null, 'value':null, 'children':null})
        var that = this;

        //console.log('processing nodes');

        this.processNode(subjectArea, [], null, scope.errorHandler);

        // want to do something similar to this

        //call service to read custom expression
        //scope.customExpressionObject = customExpressionIO.load(Object.keys(measures).map(function (key) { return measures[key]; }));

        //console.log('uncat', that.uncategorized);

        var processed = {
            name: name,

            attributes: Object.keys(that.attributes).map(function(key) {
                return that.attributes[key];
            }),

            attributesByFolder: that.returnAttrByFolders(),

            measures: _.sortBy(Object.keys(that.measures).map(function(key) {
                return that.measures[key];
            }), function(a) {
                return a.label;
            }),

            uncategorized: Object.keys(that.uncategorized).map(function(key) {
                return that.uncategorized[key];
            }),

            timeSeries: that.timeSeries,
            //customExpressions: scope.customExpressionObject,
            measureBQLMap: that.measureBQLMap,
            measureBQL: _.bind(that.bqlString, that)
        };
        processed.bqlMeasureMap = {};

        _.each(processed.measureBQLMap, function(val, key) {
            var parts = JSON.parse(key);
            processed.bqlMeasureMap[ val ] = {
                measure: processed.measures.filter(function(measure) {
                    return _.last(measure.path) === parts[0];
                })[0],
                aggregation: parts[1],
                dateType: parts[2],
                timeSeriesType: parts[3]
            };
        });

        return processed;
    };

    return new SubjectAreaProcessor();

}]);

