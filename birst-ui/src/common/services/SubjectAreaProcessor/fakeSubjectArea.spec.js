
function fakeAttribute(folder, name) {
    'use strict';
    return ('<n t="f" l="{{folder}}">'+
              '<c>'+
                '<n t="a" v="{{name}}">'+
                '</n>'+
              '</c>'+
           '</n>').replace(/{{folder}}/g, folder).replace(/{{name}}/g, name);
}

function fakeMeasure(name, AGO, byDates, aggregations) {
    'use strict';

    function fakeByAggregation(agg) {
        return '<n t="m" v="{{AGO}}{{byDate}}: {{aggregation}}: {{name}}" l="{{aggregation}}" rl="{{aggregation}} {{name}}"></n>'.replace(/{{aggregation}}/g, agg).replace(/{{AGO}}/g, AGO);
        
    }

    function fakeByDate(byDate) {
        return ('<n t="f" l="By {{byDate}}">'+
                 '<c>'+
                    aggregations.map(fakeByAggregation).join("") +
                 '</c>'+
               '</n>').replace(/{{byDate}}/g, byDate);
    }
    
    return ('<n t="f" l="{{name}}">' +
             '<c>' +
               byDates.map(fakeByDate).join("") +
             '</c>' +
           '</n>').replace(/{{name}}/g, name);
}

function fakeSA(attributes, measures, timeSeries) {
    'use strict';

    function fakeAttributes() {
        return attributes.map(function(att) { return fakeAttribute(att.folder, att.name); }).join("");
    }

    function fakeMeasures(AGO) {
        return measures.map(function(m) { return fakeMeasure(m.name, AGO, m.byDates, m.aggregations); }).join("");
    }

    function fakeTimeSeriesMeasures() {
        return timeSeries.map(function(ts) {
          return ('<n t="f" l="{{timeSeriesLabel}}">' +
                   '<c>' +
                     timeSeries.map(function(ts) { return fakeMeasures(ts.name + " "); }).join("") +
                   '</c>' +
                '</n>').replace(/{{timeSeriesLabel}}/g, ts.label);
        }).join("");
    }

    return ('<soapenv:Envelope>' +
             '<soapenv:Body>' +
               '<ns:getSubjectAreaResponse>' +
                 '<ns:return>' +
                   '<com.successmetricsinc.WebServices.BirstWebServiceResult>' +
                     '<ErrorCode>0</ErrorCode>' +
                     '<ErrorMessage/>' +
                     '<Result>' +
                       '<n t="f" l="Default Subject Area" isRootSA="true">' +
                         '<c>' +
                           '<n t="f" l="Attributes">' +
                             '<c>' +
                               '{{attributes}}' +
                             '</c>' +
                           '</n>' +
                           '<n t="f" l="Measures">' +
                             '<c>' +
                               '{{measures}}' +
                             '</c>' +
                           '</n>' +
                           '<n t="f" l="Time Series Measures">' +
                             '<c>' +
                               '{{timeSeriesMeasures}}' +
                             '</c>' +
                           '</n>' +
                         '</c>' +
                       '</n>' +
                     '</Result>' +
                   '</com.successmetricsinc.WebServices.BirstWebServiceResult>' +
                 '</ns:return>' +
               '</ns:getSubjectAreaResponse>' +
             '</soapenv:Body>' +
           '</soapenv:Envelope>').replace(/{{attributes}}/g, fakeAttributes()).replace(/{{measures}}/g, fakeMeasures("")).replace(/{{timeSeriesMeasures}}/g, fakeTimeSeriesMeasures());
}

function fakeResultSet(columns, rows) {
    'use strict';

    function fakeRows() {
        return '<rows>' +
            rows.map(function(row) {
                return '<ArrayOfString>' +
                    row.map(function(val) {
                        return '<string>' + val + '</string>';
                    }).join("") + '</ArrayOfString>';
            }).join("") +
            '</rows>';
    }

    function fakeColumns() {
        return '<columnNames>' + columns.map(function(col) { return "<string>" + col.name + "</string>"; }).join("") + '</columnNames>' +
            '<displayNames>' + columns.map(function(col) { return "<string>" + col.name + "</string>"; }).join("") + '</displayNames>' +
            '<dataTypes>' + columns.map(function(col) { return "<int>" + (col.dataType || "0") + "</int>"; }).join("") + '</dataTypes>' +
            '<warehouseColumnDataTypes>' + columns.map(function(col) { return "<string>" + (col.warehouseColumnDataType || "") + "</string>"; }).join("") + '</warehouseColumnDataTypes>' +
            '<columnFormats>' + columns.map(function(col) { return "<string>" + (col.columnFormat || "") + "</string>"; }).join("") + '</columnFormats>';
    }

    return '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">'+
        '<soapenv:Body>' +
        '<ns:executeQueryResponse xmlns:ns="http://adhoc.WebServices.successmetricsinc.com">' +
        '<ns:return>' +
        '<com.successmetricsinc.WebServices.BirstWebServiceResult>' +
        '<ErrorCode>0</ErrorCode>' +
        '<ErrorMessage></ErrorMessage>' +
        '<Result>' +
        '<Result>' +
        fakeRows() +
        fakeColumns() +
        '<numRowsReturned>' + rows.length + '</numRowsReturned>' +
        '<hasMoreRows>false</hasMoreRows>' +
        '</Result>' +
        '</Result>' +
        '</com.successmetricsinc.WebServices.BirstWebServiceResult>' +
        '</ns:return>' +
        '</ns:executeQueryResponse>' +
        '</soapenv:Body>' +
        '</soapenv:Envelope>';
}


function fakeServices($httpBackend, attribs, measures, rows) {
    'use strict';

    function resultSetForColumns(cols) {
        var indices = [];
        cols.map(function(col) {
            for(var ii = 0; ii < attribs.length; ii++) {
                if (attribs[ii].name === col) {
                    indices.push(ii);
                }
            }
            for(var jj = 0; jj < measures.length; jj++) {
                if (measures[jj].name === col) {
                    indices.push(attribs.length + jj);
                }
            }
        });
        var columns = indices.map(function(index) { 
            if(index < attribs.length) {
                return attribs[index]; 
            } else {
                return measures[index - attribs.length];
            }
        });

        var byKey = {};
        var resultRows = [];
        rows.map(function(row) {
            var key = JSON.stringify(indices.filter(function(idx) { return idx < attribs.length; }).map(function(idx) { return row[idx]; }));
            if(!byKey[key]) {
                byKey[key] = [];
                resultRows.push(byKey[key]);
            }
            byKey[key].push(row);
        });
        resultRows = resultRows.map(function(keyRows) {
            return indices.map(function(idx) { 
                if(idx < attribs.length) {
                    return keyRows[0][idx]; 
                } else {
                    var measureVals = [];
                    keyRows.map(function(kr) {
                        measureVals.push.apply(measureVals, kr[idx]);
                    });
                    return measureVals.reduce(function(x,y) { return x + y; }, 0);
                }
            });
        });

        return fakeResultSet(columns, resultRows);
    }

    function fakeQueryHandler(method, url, data, headers) {
        var queryString = data.match(/<ns:queryString>([^<]*)<\/ns:queryString>/);
        if(!queryString) {
            return [500, "couldn't find queryString", {}, "couldn't find queryString"];
        }
        queryString = queryString[1];
        
        var projectionList = queryString.match(/select using outer join (.*) from/i)[1].split(',').map(function(col) {
            return col.replace(/^(.*) '[^']*'/, "$1").replace(/^\s*\[\s*|\s*\]\s*$/g, "");
        });

        // for now, ignore aggregation and datetype
        projectionList = projectionList.map(function(col) {
            return col.replace(/^.*:([^:]*)$/, "$1").replace(/^\s*|\s*$/g, "");
        });
        
        var resultSet = resultSetForColumns(projectionList);
        return [200, resultSet, {}, ""];
    }
    
    $httpBackend
        .when('POST', '/SMIWeb/services/Adhoc.AdhocHttpSoap11Endpoint/', /.*/, function(headers) { 
            return headers.SOAPAction === '"urn:executeQuery"';
        })
        .respond(fakeQueryHandler);

    var fakeSubjectArea = fakeSA(attribs, measures, []);

    $httpBackend.when('POST', '/SMIWeb/services/SubjectArea.SubjectAreaHttpSoap11Endpoint/').respond(fakeSubjectArea);
}

/*
console.log(fakeSA([{folder: 'Time', name: 'Time.Year'}], [{name: 'Revenue', byDates: ['LoadDate', 'OrderDate'], aggregations: ['Sum', 'Avg']}], [{name: 'MAGO', label: 'Month Ago'}]));
console.log(fakeResultSet([{name: 'Time.Year'}, {name: 'Revenue'}], [["2011", 100],["2012", 200]]));
*/
