/*jshint bitwise: false*/
angular.module( 'services.color', [] )

.factory( 'color', function() {
	'use strict';
	return {
		linearScale: function( srcDict, dstDict, src, dstStepCount ) {
			var minSrc = srcDict.min;
			var maxSrc = srcDict.max;
			var minDst = dstDict.min;
			var maxDst = dstDict.max;
			var srcNorm = src;
			var dst;
			if ( minSrc === maxSrc ) {
				dst = minDst;
			} else {
				dst = minDst + ( ( srcNorm - minSrc ) / ( maxSrc - minSrc ) * ( maxDst - minDst ) );
			}
			var result = NaN;
			if ( minDst < maxDst ) {
				result = Math.min( Math.max( minDst, dst ), maxDst );
			} else {
				result = Math.max( Math.min( minDst, dst ), maxDst );
			}
			if ( dstStepCount === 0 ) {
				return result;
			} else if ( dstStepCount < 0 ) {
				//throw exception
			} else {
				var step = ( maxDst - minDst ) / ( dstStepCount + 0.0 );
				var stepNum = Math.round( this.linearScale( dstDict, { min : 0, max : dstStepCount }, result ) );
				return minDst + stepNum * step;
			}
		},

		rgbToInt: function(r, g, b) {
			return r << 16 | g << 8 | b << 0;
		},

		rgbString2int : function( str ){
			if ( !str ) { return -1; }
			if ( str.length > 1 ){
				var startIdx = str.indexOf( 'Rgb(' );
				if ( startIdx === 0 ){
					var endIdx = str.indexOf( ')', 4 );
					if( endIdx > 0 ){
						var rgbString = str.substring( startIdx + 4, endIdx );
						var rgbValues = rgbString.split( ',' );
						if( rgbValues.length === 3 ){
							var r = parseInt( rgbValues[0], 10 );
							var g = parseInt( rgbValues[1], 10 );
							var b = parseInt( rgbValues[2], 10 );
							return r << 16 | g << 8 | b << 0;
						}
					}
				}
			}
			return parseInt( str, 10 );
		},

		rgbString2Array : function( str ) {
			if ( !str ) { return -1; }
			if ( str.length > 1 ){
				var startIdx = str.indexOf( 'Rgb(' );
				if ( startIdx === 0){
					var endIdx = str.indexOf( ')', 4 );
					if( endIdx > 0 ){
						var rgbString = str.substring( startIdx + 4, endIdx );
						var rgbValues = rgbString.split( ',' );
						if( rgbValues.length === 3 ) {
							var r = parseInt( rgbValues[0], 10 );
							var g = parseInt( rgbValues[1], 10 );
							var b = parseInt( rgbValues[2], 10 );
							return [ r, g, b ];
						}
					}
				}
			}
			return null;
		}
	};
} );
