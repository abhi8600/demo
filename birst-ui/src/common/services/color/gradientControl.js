angular.module( 'services.color.gradientControl', [ 'services.color.valueColorGradient' ] )

.factory( 'GradientControl', function( ValueColorGradient ) {
	'use strict';
	var GradientControl = function( flatGradientList ) {
		ValueColorGradient.apply( this, arguments );
		this.flatGradientList = flatGradientList;
	};

	_.extend( GradientControl.prototype, ValueColorGradient.prototype, {

		numFlatGradients : function() {
			return this.flatGradientList.length;
		},

		// Return the gradient at the specified index
		flatGradientAt : function( i ) {
			return this.flatGradientList[ i ];
		},

		/**
		* Find the gradient into whose range the specified value falls
		* and use it to calculate the color.
		*
		* @param value A number to translate into a color.
		*/
		colorForValue : function( value ) {
			for ( var i = 0; i< this.numFlatGradients(); i++ ) {
				var gradient = this.flatGradientAt( i );
				if ( gradient.containsValue( value ) ) {
					return gradient.colorForValue( value );
				}
			}
			// The value is greater than last gradient's max
			// - delegate to last gradient.
			var lastGradient = this.flatGradientAt( this.numFlatGradients() - 1 );
			return lastGradient.colorForValue( value );
		},

		rgbColorForValue: function(value){
			for ( var i = 0; i< this.numFlatGradients(); i++ ) {
				var gradient = this.flatGradientAt( i );
				if ( gradient.containsValue( value ) ) {
					return gradient.rgbColorForValue( value );
				}
			}
			// The value is greater than last gradient's max
			// - delegate to last gradient.
			var lastGradient = this.flatGradientAt( this.numFlatGradients() - 1 );
			return lastGradient.rgbColorForValue( value );
		}

	} );

	return GradientControl;

} );
