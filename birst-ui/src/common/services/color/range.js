/*jshint bitwise: false*/
angular.module( 'services.color.range', [] )

.constant( 'RANGE_CONSTANTS', {
	'MIN_PERCENT' : 0,
	'MAX_PERCENT' : 100
} )

.factory( 'Range', function() {
	'use strict';

	var Range = function() {
		this.minValue = NaN;
		this.maxValue = NaN;
		this.minColor = NaN;
		this.maxColor = NaN;
		this.minPercent = NaN;
		this.maxPercent = NaN;
	};

	_.extend( Range.prototype, {
		setMinPercent : function( value ) {
			this._minPercent = value;
		},

		setMaxPercent : function( value ) {
			this._maxPercent = value;
		},

		getValueDict : function() {
			return {
				'min': this.minValue,
				'max': this.maxValue
			};
		},

		getRgbColorDict : function() {
			var result = {};
			result.r = {
				'min': this.rChannel( this.minColor ),
				'max': this.rChannel( this.maxColor )
			};
			result.g = {
				'min': this.gChannel( this.minColor ),
				'max': this.gChannel( this.maxColor )
			};
			result.b = {
				'min': this.bChannel( this.minColor ),
				'max': this.bChannel( this.maxColor )
			};
			return result;
		},

		rChannel : function( value ){
			return value >> 16 & 0xFF;
		},

		gChannel : function( value ){
			return value >> 8 & 0xFF;
		},

		bChannel : function( value ) {
			return value >> 0 & 0xFF;
		}
	} );

	return Range;

} );

