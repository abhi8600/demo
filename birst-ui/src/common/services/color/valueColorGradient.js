angular.module( 'services.color.valueColorGradient', [ 'services.color.range', 'services.color' ] )

.factory( 'ValueColorGradient', function( Range, color ) {
	'use strict';
	var ValueColorGradient = function() {
		Range.apply( this, arguments );
	};

	_.extend( ValueColorGradient.prototype, Range.prototype, {

		/**
		* Apply the scaling calculations to translate the specfied number
		* into a color within this object's range.
		*/
		colorForValue : function( someValue ){
			var valueDict = this.getValueDict();
			var rgbColorDict = this.getRgbColorDict();
			var r = color.linearScale( valueDict, rgbColorDict.r, someValue, 0 );
			var g = color.linearScale( valueDict, rgbColorDict.g, someValue, 0 );
			var b = color.linearScale( valueDict, rgbColorDict.b, someValue, 0 );
			return color.rgbToInt( r, g, b );
		},

		rgbColorForValue : function( someValue ){
			var valueDict = this.getValueDict();
			var rgbColorDict = this.getRgbColorDict();
			var r = color.linearScale( valueDict, rgbColorDict.r, someValue, 0 );
			var g = color.linearScale( valueDict, rgbColorDict.g, someValue, 0 );
			var b = color.linearScale( valueDict, rgbColorDict.b, someValue, 0 );
			r = Math.floor( r );
			g = Math.floor( g );
			b = Math.floor( b );
			return { r: r, g: g, b: b};
		},

		containsValue: function( someValue ){
			return someValue < this.maxValue;
		},

		numFlatGradients: function() {
			return 1;
		},

		flatGradientAt: function( i ){
			return this;
		}

	} );

	return ValueColorGradient;

} );
