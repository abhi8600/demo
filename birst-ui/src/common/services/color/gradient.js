angular.module( 'services.color.gradient', [ 'services.color.range', 'services.color', 'services.color.valueColorGradient', 'services.color.gradientControl' ] )

.factory( 'Gradient', [ 'RANGE_CONSTANTS', 'Range', 'color', 'ValueColorGradient', 'GradientControl', function( rc, Range, color, ValueColorGradient, GradientControl ) {
	'use strict';
	var Gradient = function() {
		Range.apply( this, arguments );
	};

	_.extend( Gradient.prototype, Range.prototype, {

		setMinPercent : function( value ) {
			value = ( value < rc.MIN_PERCENT ) ? rc.MIN_PERCENT : value;
			this._minPercent = value;
		},

		// End of a percentage range. Values are forced to be less than MAX_PERCENT.
		setMaxPercent : function( value ) {
			value = ( value > rc.MAX_PERCENT ) ? rc.MAX_PERCENT : value;
			this._maxPercent = value;
		},

		arrayMinMax : function( values ) {
			var ln = values.length;
			if( ln === 0 ) { return []; }
			var overallMinValue = values[0];
			var overallMaxValue = values[0];
			for( var i = 0; i < ln; i++ ) {
				var v = values[ i ];
				if( v < overallMinValue ) {
					overallMinValue = v;
				}
				if( v > overallMaxValue ) {
					overallMaxValue = v;
				}
			}

			return [ overallMinValue, overallMaxValue ];
		},

		// *
		// * Create a new instance of this class from an XML config
		// *
		// * @param dataValues An array of numbers
		// *
		// * @return A new GradientControl objects with this object's
		// * gradient config applied to the specified values

		applyGradientToValueList : function( dataValues ) {
			var arrayMinMax = this.arrayMinMax( dataValues );
			if( arrayMinMax.length === 0 ) {
				return null;
			}
			this.minValue = arrayMinMax[0];
			this.maxValue = arrayMinMax[1];

			// TODO: should these be called twice?

			// Try reading values from children
			this.fillValueColorFromChildren();
			// If still unfilled, try reading values from the siblings
			this.fillValueColorFromSiblings();
			// If still unfilled, try reading values from the parent
			this.fillValueColorFromParent( this );

			//TODO: PORT GradientControl
			var gradients = this.createValueColorGradients( this );
			return new GradientControl( gradients );
		},

		fillValueColorFromChildren : function(){
		},

		fillValueColorFromSiblings : function(){
		},

		fillValueColorFromParent : function( parent ) {
		},

		// If min or max absolute values are not specified,
		// calculate them based on the percentage values
		// and on the min/max values taken from the parent.
		createValueColorGradients : function( parent ){
			var percentRangeDict = {
				min : 0,
				max : 100
			};
			var flatGradient = this.createFlatGradient();
			var dstDict = {
				min : parent.minValue,
				max : parent.maxValue
			};
			if( isNaN( flatGradient.minValue ) ) {
				flatGradient.minValue = color.linearScale( percentRangeDict, dstDict, this.minPercent, 0 );
			}
			if( isNaN( flatGradient.maxValue ) ) {
				flatGradient.maxValue = color.linearScale( percentRangeDict, dstDict, this.maxPercent, 0 );
			}

			return [ flatGradient ];
		},

		// Create a gradient with absolute values for use in a GradientControl.
		createFlatGradient : function() {
			var result = new ValueColorGradient();
			result.minValue = this.minValue;
			result.maxValue = this.maxValue;
			result.minColor = this.minColor;
			result.maxColor = this.maxColor;
			return result;
		},

		// Duplicate the min values of another GradientRule object
		fillMin : function( other ){
			if( isNaN( this.minValue ) ) {
				this.minValue = other.minValue;
			}
			if( isNaN( this.minColor ) ) {
				this.minColor = other.minColor;
			}
			// Do not transfer percentage between levels
		},

		// Duplicate the max values of another GradientRule object
		fillMax : function( other ){
			if( isNaN( this.maxValue ) ) {
				this.maxValue = other.maxValue;
			}
			if( isNaN( this.maxColor ) ) {
				this.maxColor = other.maxColor;
			}
			// Do not transfer percentage between levels
		},

		// Duplicate the max values of another GradientRule object
		// as the min values for this object
		fillFromPrev : function( prev, fillPercent ) {
			if( isNaN( this.minPercent ) && fillPercent ) {
				//minValue won't be overriden by this
				this.minPercent = prev.maxPercent;
			}
			if( isNaN( this.minValue ) ) {
				this.minValue = prev.maxValue;
			}
			if( isNaN( this.minColor ) ) {
				this.minColor = prev.maxColor;
			}
		},

		// Duplicate the min values of another GradientRule object
		// as the max values for this object
		fillFromNext: function( next, fillPercent ){
			if( isNaN( this.maxPercent ) && fillPercent ) {
				//maxValue won't be overriden by this
				this.maxPercent = next.minPercent;
			}
			if( isNaN( this.maxValue ) ) {
				this.maxValue = next.minValue;
			}

			if( isNaN(this.maxColor ) ) {
				this.maxColor = next.minColor;
			}
		}
	} );

	return Gradient;

} ] );
