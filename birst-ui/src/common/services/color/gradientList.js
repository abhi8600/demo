angular.module( 'services.color.gradientList', [ 'services.color.gradient' ] )

.factory( 'GradientList', [ 'Gradient', 'RANGE_CONSTANTS', function( Gradient, rc ) {
	'use strict';
	var GradientList = function( ruleList ) {
		Gradient.apply( this, arguments );
		this.ruleList = ruleList;
	};

	_.extend( GradientList.prototype, Gradient.prototype, {

		fillValueColorFromChildren : function() {
			var ln = this.ruleList.length;
			// Depth first.
			for( var i = 0; i < ln; i++ ){
				var g = this.ruleList[ i ];
				g.fillValueColorFromChildren();
			}

			if ( ln ) {
				this.fillMin( this.ruleList[0] );
				this.fillMax( this.ruleList[ ln - 1 ] );
			}
		},

		// Read value and color borders from the parent
		fillValueColorFromParent : function( parent ) {
			Gradient.prototype.fillValueColorFromParent.apply( parent, arguments );
			if (this.ruleList.length === 0) {
				return;
			}
			// Breadth first
			this.ruleList[0].fillMin( this );
			this.ruleList[ this.ruleList.length - 1 ].fillMax( this );

			for( var i = 0; i < this.ruleList.length; i++ ) {
				var g = this.ruleList[ i ];
				g.fillValueColorFromParent( this );
			}
		},

		// Read value and color borders from the neighboring gradients.
		// Use 0% and 100% for the start and end of the ranges if not specified.
		fillValueColorFromSiblings : function() {
			var fantomFirstEntry = new Gradient();
			fantomFirstEntry.maxPercent = rc.MIN_PERCENT;

			var fantomLastEntry = new Gradient();
			fantomLastEntry.minPercent = rc.MAX_PERCENT;

			var paddedGradientList = [ fantomFirstEntry ].concat( this.ruleList ).concat( [ fantomLastEntry ] );

			for ( var i = 1; i< paddedGradientList.length - 1; i++ ) {
				var g = paddedGradientList[ i ];

				var prev  = paddedGradientList[ i - 1 ];
				var next = paddedGradientList[ i + 1 ];

				g.fillFromPrev( prev, true );
				g.fillFromNext( next, true );

				g.fillValueColorFromSiblings();
			}
		},

		createValueColorGradients: function( parent ) {
			var oneGradientList = Gradient.prototype.createValueColorGradients.apply( parent, arguments );
			var flatGradient = oneGradientList[0];
			var flatGradientList = [];
			for( var i = 0; i < this.ruleList.length; i++ ){
				var g = this.ruleList[ i ];
				var childFlatGradients = g.createValueColorGradients( flatGradient );
				flatGradientList = flatGradientList.concat( childFlatGradients );
			}
			return flatGradientList;
		}

	} );

	return GradientList;
} ] );

