/* unit test functions in globalStateService.js */

describe('appState', function() {
	'use strict';
	var globalStateService;
	beforeEach(function() {
		module('visualizer');
	});
	beforeEach( inject( function( _globalStateService_ ){
		globalStateService = _globalStateService_;
	} ) );

	it('product launch state should be one of: dev, pivot, standalone, reportView and editing should be false for pivot+standalone+reportView for now', inject(function(globalStateService) {

		globalStateService.setProductLaunchedAs('pivot');

		expect(globalStateService.getProductLaunchedAs()).toBe('pivot');

		expect(globalStateService.isTitleAndEditingEnabled()).toBe(false);


		globalStateService.setProductLaunchedAs('standalone');

		expect(globalStateService.getProductLaunchedAs()).toBe('standalone');

		expect(globalStateService.isTitleAndEditingEnabled()).toBe(false);



		globalStateService.setProductLaunchedAs('reportView');

		expect(globalStateService.getProductLaunchedAs()).toBe('reportView');

		expect(globalStateService.isTitleAndEditingEnabled()).toBe(false);


	}));

	it ('get my name from server when launched as pivot and display my name in toolbar ', inject(function(globalStateService) {

		globalStateService.setMyName('Gabe');

		expect(globalStateService.whatsMyName()).toBe('Gabe');

	}));



	it ('make sure app aware if running for the first time', inject(function(globalStateService) {
		expect(globalStateService.runningFirstTime()).toBe(true);

		expect(globalStateService.runningFirstTime()).toBe(false);
	}));


	it ('make sure sample data returned from report (i.e. passed in Pivot mode) is saved and returned right', inject (function(globalStateService){

		var data = [{"name":"Year/Month","columnType":"Dimension","isMeasureExpression":false,"bql":"[Time.Year/Month]","displayExpression":"[Time.Year/Month]","reportIndex":"1","type":"visualized","isFilterOn":true},{"name":"UnitPrice","columnType":"Measure","isMeasureExpression":true,"bql":"[OrderDate: Sum: UnitPrice]","displayExpression":"[OrderDate: Sum: UnitPrice]","reportIndex":"3","type":"visualized","isFilterOn":true}];

		globalStateService.setReportData(data);

		expect(globalStateService.getReportData()).toBe(data);

		var isObject = _.isObject(data);

		expect(isObject).toBe(true);
	}));


});
