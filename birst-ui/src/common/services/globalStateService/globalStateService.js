/**
 * Created by gjakobson on 12/5/13
 *
 * Hold global state of prod, e.g. what prod was launched as (standalone, pivot control), using CWS vs. Internal, etc
 *
 *
 *
 */

 /* globals localStorage, location */
angular.module('visualizer')

.factory('globalStateService', function() {
	'use strict';
    /* holds value of what the product was launched as,
     default is 'dev'
     other choices are 'pivot' and 'standalone'; 'reportView' is when displaying saved report from dashboard
     */

    var validLaunchStates = ['dev', 'pivot', 'standalone', 'reportView', 'standalone_trial'];

    var productLaunchedAs = 'dev';

    var reportData = null;

    var myName = null; //name of current user as returned by server in standalone mode

    var enableTitleAndEditing = true; //chart title and editing enabled by default unless turned off for "pivot" and "standalone" modes

    var enabledForSaving = true;

    var enabledForBQL = true;

    var isFirstTimeRun = true; //true when prod first runs, false thereafter

    var keepAliveTimer = null;

    return {
		setProductLaunchedAs: function(value) {
            var that = this;

            if (_.contains(validLaunchStates, value)) {
                productLaunchedAs = value;


                //set special rules if product launched in various states

                switch (value) {
                case "reportView":
                    enableTitleAndEditing = false;
                    enabledForSaving = false;
                    enabledForBQL = false;
                    break;
                case "pivot":
                    enableTitleAndEditing = false;
                    enabledForSaving = false;
                    enabledForBQL = false;
                    break;
                case "dev":
                    if (keepAliveTimer === null) {
                        keepAliveTimer = setInterval(function() {
                            that.keepAlive();
                        }, 10 * 1000 * 8);
                    }
                    break;
                case "standalone":
                    if (keepAliveTimer === null) {
                        keepAliveTimer = setInterval(function() {
                            that.keepAlive();
                        }, 60 * 1000 * 8);
                    }
                    break;
                case "standalone_trial":
                    enabledForSaving = false;
                    enabledForBQL = false;
                    if (keepAliveTimer === null) {
                        keepAliveTimer = setInterval(function() {
                            that.keepAlive();
                        }, 60 * 1000 * 8);
                    }
                    break;
                }


            } else {
                //console.log('error, product cannot be launched in state: ', value);
            }

        },

        determineLaunchState: function() {
            var urlToken = location.href;
            if (urlToken.indexOf('pivot') > 0) {
                productLaunchedAs = 'pivot';
            } else if (urlToken.indexOf('standalone') > 0) {
                productLaunchedAs = 'standalone';
            } else {
                productLaunchedAs = 'dev';
            }


            this.setProductLaunchedAs(productLaunchedAs);
            return productLaunchedAs;
        },


        getProductLaunchedAs: function() {
            return productLaunchedAs;
        },

        useInternalWebServices: function() {
            return localStorage.getItem('CWS') !== 'true';
        },

        setReportData: function(data) {
            reportData = data;
        },

        getReportData: function() {
            return reportData;
        },

        setMyName: function(userName) {
            myName = userName;
        },

        whatsMyName: function() {
            return myName;
        },

        isTitleAndEditingEnabled: function() {
            return enableTitleAndEditing;
        },

        runningFirstTime: function() {
            var originalState = isFirstTimeRun;
            isFirstTimeRun = false;
            return originalState;
        },

        hasSavePermissions: function() {
            return enabledForSaving;
        },

        hasBQLEnabled: function() {
            return enabledForBQL;
        },

        //called via timer every 8 minutes to ping the server and keep the session alive, only in Standalone mode
        keepAlive: function() {

            var _dc = Math.floor(Math.random() * 10000000000000001);

            $.get("/PollSession.aspx?_dc=" + _dc, function() {
                //console.log ('keeping session alive');
            });
        }

    };

} );
