// include filters module as a dependency and get all the filters at once.
angular.module( 'filters', [
	'filters.highlight',
	'filters.groupsOf'
] );
