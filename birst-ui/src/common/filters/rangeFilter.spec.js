/* jshint multistr: true */
describe('range filter', function() {
	'use strict';
    beforeEach(function() {
        module('visualizer');
    });

    it('filters by range', function() {
        inject(function($compile, $rootScope) {
            var scope = $rootScope.$new();
            var template = '<div>\
                    <div id="repeat-test"><div ng-repeat="item in [1,2,3,4,5,6]">{{item}}</div></div>\
                    <div id="range-test"><div ng-repeat="item in [1,2,3,4,5,6] | range : 2 : 5 : true">{{item}}</div></div>\
                </div>';
            var partialDOM = angular.element(template);

            $compile(partialDOM)(scope);
            $rootScope.$digest();

            expect(partialDOM.find('#repeat-test').children().length).toBe(6); // test that the basic infrastructure of the test is working

            expect(partialDOM.find('#range-test').children().length).toBe(4);
        });
    });
});
