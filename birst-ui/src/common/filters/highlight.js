// originally from https://github.com/angular-ui/ui-utils/blob/master/modules/highlight/highlight.js
// temporarily using this implementation to ensure it works with special characters
angular.module( 'filters.highlight', [] )

.filter('highlight', function () {
	'use strict';

	return function (text, search, caseSensitive) {
		if ( search || angular.isNumber( search ) ) {
			text = text.toString();
			search = search.toString();
			search = search.replace( /\**/g, '' );
			if ( search === '' ) {
				return text;
			} else if ( caseSensitive ) {
				return text.split( search ).join( '<span class="ui-match">' + search + '</span>' );
			} else {
				return text.replace( new RegExp( search, 'gi' ), '<span class="ui-match">$&</span>' );
			}
		} else {
			return text;
		}
	};
} );
