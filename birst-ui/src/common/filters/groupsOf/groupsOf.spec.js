describe( 'groupsOf filter', function() {
	'use strict';

	var groupsOfFilter;

	beforeEach( module( 'filters.groupsOf' ) );

	beforeEach( inject( function( _groupsOfFilter_ ) {
		groupsOfFilter = _groupsOfFilter_;
	} ) );

	it( 'should filter a single element array into groups of 5', function() {
		expect( groupsOfFilter( [ 0 ], 5 ) ).toEqual( [ [ 0 ] ] );
	} );

	it( 'should filter a zero element array into groups of 5', function() {
		expect( groupsOfFilter( [], 5 ) ).toEqual( [] );
	} );

	it( 'should filter a 6 elment array into groups of 5', function() {
		expect( groupsOfFilter( [ 0, 1, 2, 3, 4, 5 ], 5 ) ).toEqual( [ [ 0, 1, 2, 3, 4 ], [ 5 ] ] );
	} );

	it( 'should filter a 6 elment array into groups of 5', function() {
		expect( groupsOfFilter( [ 0, 1, 2, 3, 4, 5, 6 ], 5 ) ).toEqual( [ [ 0, 1, 2, 3, 4 ], [ 5, 6 ] ] );
	} );

	it( 'should delete elements that are orphaned in cache', function() {
		var cache = groupsOfFilter( [ 0, 1, 2, 3, 4, 5, 6 ], 5 );
		expect( groupsOfFilter( [ 0, 1, 2, 3, 4 ], 5, cache ) ).toEqual( [ [ 0, 1, 2, 3, 4 ] ] );
	} );

	it( 'should delete elements that are orphaned in cache of long array', function() {
		var cache = groupsOfFilter( [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 ], 5 );
		expect( groupsOfFilter( [ 0, 1, 2, 3, 4 ], 5, cache ) ).toEqual( [ [ 0, 1, 2, 3, 4 ] ] );
	} );

} );
