/**
 * @name groupsOfFilter
 * @author Ryan McGinty
 * @param {Array} collection The flat collection to group
 * @param {Number} groupSize The group size to use
 * @param {Array} originalGroups A reference to an already grouped object
 * @returns {Array} Two dimensional grouped
 *
 * @description groupsOf filter will accept an array and split it into groups of the number specified
 * if you pass a reference to an existing two dimensional array it will only change the items
 * needed instead of re-creating the entire array. This allows for the items to maintain
 * the same references ( ie $$hashKey ) so repeaters don't redraw every object in an array.
 */

 /** @example
  * groupsOfFilter( [ 1, 2, 3, 4, 5, 6 ], 3 );
  * returns [ [ 1, 2, 3 ], [ 4, 5, 6 ] ]
 */

angular.module( 'filters.groupsOf', [] )

.filter( 'groupsOf', function() {
	'use strict';
	return function( collection, groupSize, originalGroups ) {
		var ln = collection.length;
		var numGroups = window.Math.ceil( ln / groupSize );
		var groups = originalGroups || [];
		if( ln ) {
			for( var i = 0; i < numGroups; i++ ) {
				var top = i * 5 + 5;
				top = top > ln ? ln : top;
				var slice = collection.slice( i * groupSize, top );
				if( groups[ i ] ) {
					// If equal don't change
					if( !_.isEqual( slice, groups[i] ) ) {
						for( var n = 0, len = slice.length; n < len; n++ ) {
							// Test index for equality before changing
							if( !groups[i][n] || !_.isEqual( groups[i][n], slice[n] ) ) {
								groups[i][n] = slice[n];
							}
						}
						// Delete Extras
						var sLen = slice.length;
						var gLen = groups[i].length;
						if( sLen < gLen ) {
							groups[i].splice( sLen, ( gLen - sLen ) );
						}
					}
				} else {
					groups[ i ] = slice;
				}
			}
			while( numGroups < groups.length ) {
				groups.pop();
			}
		} else {
			groups = [];
		}
		return groups;
	};

} );
