describe('characters filter', function() {
	'use strict';
    beforeEach(function() {
        module('visualizer');
    });

    it('truncates input to be no more than <chars> characters long', function() {
        inject(function($compile, $rootScope) {
            var scope = $rootScope.$new();
            var template = '<div>'+
                '<div id="no-argument">{{"1234567890123456789012345" | characters}}</div>'+
                '<div id="blank">{{"" | characters}}</div>'+
                '<div id="char-gibberish">{{"1234567890123456789012345" | characters:"random"}}</div>'+
                '<div id="chars-10">{{"12345678901234567890" | characters:10}}</div>'+
                '<div id="chars-30">{{"12345678901234567890" | characters:30}}</div>'+
                '</div>';
            var partialDOM = angular.element(template);

            $compile(partialDOM)(scope);
            $rootScope.$digest();

            expect(partialDOM.find('#no-argument').html().length).toBe(25);
            expect(partialDOM.find('#blank').html().length).toBe(0);
            expect(partialDOM.find('#char-gibberish').html().length).toBe(25);
            expect(partialDOM.find('#chars-10').html().length).toBe(12);
            expect(partialDOM.find('#chars-10').html()).toBe("123456789...");
            expect(partialDOM.find('#chars-30').html().length).toBe(20);
            expect(partialDOM.find('#chars-30').html()).toBe("12345678901234567890");
        });
    });
});
