'use strict';

/* Filters */
angular.module('visualizer').

filter('subjectAreaDisplayName', function() {
    return function(columnText) {
        if (typeof columnText === 'undefined') {
            return '';
        }
        return columnText.replace(/\./g, ' ').replace(/_/g, ' ');
    };
}).
filter('subjectAreaSearchName',function() {
    return function(columnText) {
        return columnText.split('.')[columnText.split('.').length - 1].replace(/_/g, ' ');
    };
}).
filter('popoverThreshold', function() {
    // This filter allows for popovers to only be shown if the displayName
    // is over a certain number of characters (threshold).
    // If threshold is set to the string 'omit', then the popover will
    // always be suppressed.
    return function(displayName, threshold) {
        if (displayName === null) {
            return '';
        }
        var thresh = 25; // by default, hide all tooltips of length less than this
        if (threshold === 'report expression') {
            thresh = 16; // report expressions have the +/- toggle, thus lower threshold
        } else if (!isNaN(threshold)) {
            thresh = threshold;
        }
        if (typeof displayName === 'undefined' || threshold === 'omit') {
            return '';
        }
        return displayName.length > thresh ? displayName : '';
    };
}).
filter('characters', function() {
    // Truncates and adds ellipsis to long inputs.  If `input` is greater
    // than `chars` characters long, returns the first `chars - 1` characters
    // plus three periods.
    return function(input, chars) {
        if (isNaN(chars)) {
            return input;
        }
        if (chars <= 0) {
            return '';
        }
        if (input && input.length > chars) {
            return input.substring(0, chars - 1) + '...';
        }
        return input;
    };
}).
filter('timeFromNow', function () {
    return function (date) {
        return window.moment(date).fromNow();
    };
}).
filter('highlightPartial', function() {
    return function(partial, search) {
        var regex_search = new RegExp(_.regexEscape(search), 'i');
        var result = regex_search.exec(partial);
        if (!result) {
            return _.htmlEscape(partial);
        }
        return [
            _.htmlEscape(partial.substring(0, result.index)),
            "<b>" + _.htmlEscape(partial.substring(result.index, result.index + search.length)) + "</b>",
            _.htmlEscape(partial.substring(result.index + search.length))
        ].join("");
    };
}).
filter('stagingAreaDisplayName', function() {
    return function(columnText) {
        return columnText.slice(columnText.indexOf('.') + 1).replace(/_/g, ' ');
    };
}).
filter('tableDisplayName', function() {
    return function(columnText) {
        return _.last(columnText.split(': ')).replace(/_/g, ' ');
    };
}).
filter('formatNumberWithCommas', function() {
    return function(number) {
        return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    };
}).
filter('displayRoundNumber', function() {
    return function(number) {
        return Math.floor(number);
    };
}).
filter('dataFormat', function() {
    return function(number, symbol, precision) {
        return _.dataFormat(number, symbol, precision);
    };
}).
filter('setDecimals', function() {
    return function(number, variable) {
        if (_.isNumber(number)) {
            variable = variable === undefined ? 2 : variable;
            var parts = number.toFixed(variable).toString().split(".");
            parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            number = parts.join(".");
        }
        return number;
    };
}).
filter('capitalizeFirstLetter', function() {
    return function(string) {
        return string.charAt(0).toUpperCase() + string.slice(1);
    };
}).
filter('lowerCaseFirstLetter', function() {
    return function(string) {
        return string.charAt(0).toLowerCase() + string.slice(1);
    };
}).
filter('range', function() {
    var MAX_ONLY = 'max';
    var MIN_ONLY = 'min';
    var MIN_TO_MAX = true;
    var OUTER = 'outer';

    return function(list, min, max, type) {
        var newList = [];
        var item, i;

        type = type || true;

        function testMin(item) {
            if (min === undefined) {
                return true;
            } else {
                return item >= min;
            }
        }

        function testMax(item) {
            if (max === undefined) {
                return true;
            } else {
                return item <= max;
            }
        }

        for (i = 0; i < list.length; ++i) {
            item = list[i];

            if (type === MIN_TO_MAX) {
                if (testMin(item) && testMax(item)) {
                    newList.push(item);
                }
            } else if (type === MIN_ONLY) {
                if (!testMin(item)) {
                    newList.push(item);
                }
            } else if (type === MAX_ONLY) {
                if (!testMax(item)) {
                    newList.push(item);
                }
            } else if (type === OUTER) {
                if (!(testMin(item) && testMax(item))) {
                    newList.push(item);
                }
            }
        }

        return newList;
    };
}).
filter('cssClass', function() {
    return function(text) {
        return text.toLowerCase().replace(/\s/g, '-').replace(/\(|\)/g, '');
    };
}).
filter('orderObjectBy', function() {
    return function(items, field, reverse) {
        var filtered = [];
        angular.forEach(items, function(item) {
            filtered.push(item);
        });
        filtered.sort(function(a, b) {
            return (a[field] > b[field]);
        });
        if (reverse) {
            filtered.reverse();
        }
        return filtered;
    };
});
