/* Controllers */
angular.module('visualizer')

.directive('modalDialog', function() {
    'use strict';
    return {
        transclude: true,
        replace: true,
        scope: {visible: '=modalDialog'},
        template: '<div></div>',
        link: function(scope, elem, attrs) {
            scope.className = attrs.className ? attrs.className : '';
        },
        controller: ['$transclude', '$scope', function($transclude, $scope) {
            var dialog, modalDialog, backdrop;
            $scope.$on('$destroy', function() {
                if (dialog) {
                    dialog.remove();
                }
                if (backdrop) {
                    backdrop.remove();
                }
            });
            $scope.$watch('visible', function(val) {
                if(val) {
                    $transclude(function(clone) {
                        dialog = $("<div>").addClass("modal " + $scope.className).appendTo($("body"));
                        modalDialog = $("<div>").addClass("modal-dialog").append(clone).appendTo(dialog);
                        backdrop = $("<div>").addClass("modal-backdrop").appendTo($("body"));
                        backdrop.on('click', function() {
                            $scope.$apply(function() {
                                $scope.visible = false;
                            });
                        });
                    });
                    dialog.addClass("fade in");
                    backdrop.addClass("fade in");
                } else {
                    if (dialog) {
                        dialog.remove();
                    }
                    if (backdrop) {
                        backdrop.remove();
                    }
                }
            });
        }]
    };
} );
