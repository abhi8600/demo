angular.module( 'plugins.visualizer', [] )

.factory( 'viz', function() {
	'use strict';
	return function( vizFile, dashletManager ) {
		var vizJson = JSON.parse( vizFile );
		return {
			render : function( el, prompts ) {
				var that = this;
				var iframe = $( '<iframe frameborder="0" src="/Visualizer/index.html#/reportView"></iframe>' );

				function sendReport( json ) {
					var iframeWindow = iframe[0].contentWindow;
					iframeWindow.postMessage( JSON.stringify( { report: json, prompts: prompts } ), '*' );
				}

				iframe.on('load', function() {
					sendReport( vizJson );
				} );

				el.html( iframe );

				// TODO: Is this necessary?
				//var trySendingReport = setInterval( function() {
				//sendReport( vizJson );
				//}, 1000 );

				// setTimeout( function() {
				// 	clearInterval( trySendingReport );
				// }, 10000 );
			}
		};
	};

} );
