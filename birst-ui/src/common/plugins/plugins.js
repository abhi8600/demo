angular.module( 'plugins', [ 'plugins.visualizer' ] )

.factory( 'plugins', function( viz ) {
	'use strict';
	return {
		viz : viz
	};

} );
