angular.module( 'models.chartSelector', [ 'services.xml' ] )

.factory( 'chartSelector', function( xml, lm ) {
	'use strict';
	function parseChartSelector( xmlDoc ) {
		var chartType, value, viewSelectorTypes = [], model = {};
		var chartTypes = [ 'Pie', 'Bar', 'Column', 'Line', 'default' ];
		if( xmlDoc.length ) {
			if ( xml.getXmlChildValue( xmlDoc[0], 'IncludeChartSelector' ) === 'true' ) {
				chartType = xml.getXmlChildValue( xmlDoc[0], 'ChartType' );
				value = chartType;
				if ( chartType.indexOf( chartType ) !== -1 ) {
					viewSelectorTypes.push( { label : lm.get( 'CHART_PIE' ), value : 'Pie' } );
					viewSelectorTypes.push( { label : lm.get( 'CHART_BAR' ), value : 'Bar' } );
					viewSelectorTypes.push( { label : lm.get( 'CHART_COLUMN' ), value : 'Column' } );
					viewSelectorTypes.push( { label : lm.get( 'CHART_LINE' ), value : 'Line' } );
				} else {
					// composite or any other type not in the default group
					if ( !chartType ) {
						value = 'table';
					} else {
						value = 'chart';
					}

					viewSelectorTypes.push( { label : lm.get( 'CHART_CHART' ), value : 'chart' } );
				}
				viewSelectorTypes.push( { label : lm.get( 'CHART_TABLE' ), value : 'table' } );
			}
		}
		model.selectorTypes = viewSelectorTypes;
		model.selectedValue = value;
		return model;
	}

	return function( xmlDoc ) {
		return parseChartSelector( xmlDoc );
	};

} );
