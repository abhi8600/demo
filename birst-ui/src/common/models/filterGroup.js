angular.module( 'models.filterGroup', [] )

.factory( 'FilterGroup', function() {
	'use strict';

	var FILTER_PREVIEW_SIZE = 5;
	var MAX_FILTERS_PER_GROUP = 10000;

	var FilterGroup = function( properties ) {
		_.extend( this, properties );
	};

	FilterGroup.prototype = {
		isOpen : true,
		limit : FILTER_PREVIEW_SIZE,
		multiSelectable : true,
		filters : [],
		selectedFilters : [],
		hasFilters : function() {
			return !_.isEmpty( this.filters );
		},
		hasSelectedFilters : function() {
			return !_.isEmpty( this.selectedFilters );
		},
		removeLimit : function() {
			this.limit = MAX_FILTERS_PER_GROUP;
		}
	};

	return FilterGroup;

} );
