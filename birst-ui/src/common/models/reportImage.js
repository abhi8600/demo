angular.module( 'models.reportImage', [ 'services.xml' ] )

.factory( 'ReportImage', function( xml ) {
	'use strict';
	var ReportImage = function( opts ) {
		var defaults = {
			xOrigin : 0,
			yOrigin : 0,
			xml : null,
			type : 'image'
		};
		_.extend( this, defaults, opts );
		this.parse();
	};

	ReportImage.prototype = {
		parse : function(){
			this.x = parseInt( this.xml.getAttribute( 'x' ), 10 );
			this.y = parseInt( this.xml.getAttribute( 'y' ), 10 );
			this.width  = parseInt( this.xml.getAttribute( 'width' ), 10 );
			this.height = parseInt( this.xml.getAttribute( 'height' ), 10 );
			this.backgroundColor = this.xml.getAttribute( 'backcolor' ) || 'transparent';
			this.link = this.xml.getAttribute( 'link' );
			this.src = '/SMIWeb/' + xml.getXmlValue( this.xml );
			this.setStyle();
			this.setAttributes();
			return this;
		},
		setStyle : function() {
			this.style = {
				backgroundColor : this.backgroundColor,
				width : this.width,
				height : this.height,
				left : this.x,
				top : this.y,
				position: 'absolute'
			};
		},
		setAttributes : function() {
			this.attrs = {
				src : this.src,
			};
		},
		_dataType : 'ChartImage'
	};

	return ReportImage;
} );
