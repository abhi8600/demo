angular.module( 'models.hashMap', [] )

.factory( 'HashMap', function() {
	'use strict';

	var HashMap = function( properties ) {
		this.hashes = {};
	};

	var hash = function( key ) {
		return _.isObject( key ) ? key.$$hashKey : key;
	};

	HashMap.prototype = {
		get : function( key ) {
			return this.hashes[ hash( key ) ];
		},
		set : function( key, value ) {
			this.hashes[ hash( key ) ] = value;
		},
		remove : function( key ) {
			delete this.hashes[ hash( key ) ];
		},
		clear : function() {
			this.hashes = {};
		}
	};

	return HashMap;

} );
