/* global Microsoft */
angular.module( 'models.BingMap', [ 'services.color', 'services.xml', 'services.color.gradient', 'services.color.gradientList' ] )

.factory( 'BingMap', function( color, xml, Gradient, GradientList ) {
	'use strict';

	var GREEN = 'green';
	var COLOR = 'color';
	var UMAP = 'umap';
	var BUBBLE = 'Bubble';
	var MARKER = 'Marker';
	var NAME = 'name';
	var TYPE = 'type';
	var POINT = 'point';
	var X = 'x';
	var Y = 'y';
	var SIZE = 'size';
	var ATTRIBUTES = 'attributes';
	var ATTRIBUTE = 'attribute';

	var defaults = {
		bounds : [],
		canvasIdNumber : 0,
		canvasLayer : {},
		colors : [],
		customPolyColors : {},
		data : null,
		divId : '',
		markers : [],
		nameAttributes : {},
		nameDrill : {},
		nameY : {},
		nameYToolTip : {},
		polygons : [],
//		renderer : null,
		selectStartLoc : null,
		selectEndLoc : null,
		selectPolygon : null,
		visualSelect : false,
		type : 'bingMap'
	};

	var BingMap = function( data ) {
		angular.extend( this, defaults, data );
		this.parseColorSettings();
	};

	_.extend( BingMap.prototype, {

		parseColorSettings : function() {
			if( this.data ){
				var clrs = [];
				var plts = this.data.getElementsByTagName( 'palettes' );
				if( plts.length > 0 ){
					var plt = plts[0].getElementsByTagName( 'palette' );
					if( plt.length > 0 ){
						var grad = plt[0].getElementsByTagName( 'gradient' );
						if( grad.length > 0 ){
							clrs = grad[0].getElementsByTagName( 'key' );
						}
					}
				}

				var defaultColors = false;
				for( var i = 0; !defaultColors && i < clrs.length; i++ ){
					var key = clrs[ i ];
					var colorStr = key.getAttribute( COLOR );
					if( colorStr === GREEN ){
						//hack here - green indicates default values, ie no user defined color palette
						defaultColors = true;
						continue;
					}
					var rgbValue = color.rgbString2int( colorStr );
					this.colors.push( rgbValue );
				}
			}
		},

		parseBubbleScatterMarkers: function(){
			if( this.data ) {

				//Bubble vars
				var points = [];
				var minRadius = 1;
				var maxRadius = 10;
				var minSize = 1;
				var maxSize = 2;
				var values = [];
				var series = [];
				var dt = this.data.getElementsByTagName( 'data' );

				if( dt.length > 0 ){
					series = dt[0].getElementsByTagName( 'series' );
				}

				for ( var i = 0, ln = series.length; i < ln; i++ ){
					var s = series[ i ];
					var seriesName = s.getAttribute( NAME );
					var type = s.getAttribute( TYPE );

					if( type === MARKER ){
						var markers = s.getElementsByTagName( POINT );
						for( var x = 0, xln = markers.length; x < xln; x++ ) {
							this.createMarker( markers[x], seriesName );
						}
					} else if( type === BUBBLE ) {
						var point = s.getElementsByTagName( POINT );
						if( point.length > 0 ){
							var pt = point[0];
							var lat = parseFloat( pt.getAttribute( Y ) );
							var lon = parseFloat( pt.getAttribute( X ) );
							var latLon = new Microsoft.Maps.Location( lat , lon );
							var rgbAttr = pt.getAttribute( COLOR );
							var size = parseFloat( pt.getAttribute( SIZE ) );
							if( i === 0 ) {
								minSize = maxSize = size;
							}
							if( size > maxSize ){
								maxSize = size;
							} else if ( size < minSize ) {
								minSize = size;
							}
							var obj = { name : seriesName, ll : latLon, sz : size, pt : pt, color : rgbAttr };
							values.push( size );
							points.push( obj );
						}
					}
				}

				if( points.length === 1 ) {
					this.addCircle( points[0], minRadius );
				} else {
					var range = maxSize - minSize;
					points.sort( function( a, b ){ return b.sz - a.sz; } );
					for( var y = 0, yln = points.length; y < yln; y++ ) {
						var data = points[ y ];
						var ratio = ( ( parseFloat( data.sz ) - minSize ) / range );
						var radiusSize = ( data.sz <= 0 ) ? minSize :  ratio * maxRadius;
						if( radiusSize < minRadius ){
							radiusSize = minRadius;
						}
						radiusSize = Math.round( radiusSize );
						this.addCircle( data, radiusSize );
					}

				}

				this.centerOnBounds();
				if( this.bounds.length === 1 ){
					this.map.setView( { zoom : 17 } );
				}
			}
		},

		centerOnBounds : function(){
			if( this.bounds.length ){
				this.map.setView( { bounds :  Microsoft.Maps.LocationRect.fromLocations( this.bounds ) } );
			}
		},

		addLineBreaks : function( str ) {
			return str.replace( /\n/g, '<br/>' );
		},

		addCircle : function( data, radiusSize ) {
			var dim = ( radiusSize * 10 ) + 28;
			var loc = data.ll;
			var anchorPt = new Microsoft.Maps.Point( 0, 10 );
			var marker = null;
			if( data.color ) {
				var radius = radiusSize * 2.3;
				marker = this.augmentColorIcon( data.color, loc, { width : dim, height : dim, anchor : anchorPt }, data.name, radius, radius * 3 );
				this.canvasLayer.push( marker );
			} else {
				var options = { icon : '/html/resources/images/pin_blue_' + radiusSize + '.png', width : dim, height : dim, anchor : new Microsoft.Maps.Point( 30, 30 ) };
				marker = new Microsoft.Maps.Pushpin( loc, options );
				this.map.entities.push( marker );

				var title = this.addLineBreaks( data.name );
				var info = new Microsoft.Maps.Infobox( loc, { title : title, description: '', pushpin : marker } );
				this.map.entities.push( info );
			}

			this.parseDrillAttributes( data.pt, data.name );
			marker.drillKey = data.name;

			this.markers.push( marker );
			this.bounds.push( loc );
		},

		createMarker : function( markerXml, seriesName ) {
			var options = { anchor : new Microsoft.Maps.Point( 0, 0 ) };
			var lat = parseFloat( markerXml.getAttribute( 'y' ) );
			var lon = parseFloat( markerXml.getAttribute( 'x' ) );
			var loc = new Microsoft.Maps.Location( lat , lon );
			var title = this.addLineBreaks( seriesName );

			var marker = this.createCustomColoredMarkerIcon( markerXml, loc, options, seriesName );
			if ( !marker ){
				marker = new Microsoft.Maps.Pushpin( loc, options );
				this.map.entities.push( marker );
				var info = new Microsoft.Maps.Infobox( loc, { title : title, description : '', pushpin : marker } );
				this.map.entities.push( info );
			}

			this.parseDrillAttributes( markerXml, seriesName );
			marker.drillKey = seriesName;

			this.markers.push( marker );
			this.bounds.push( loc );
		},

		createCustomColoredMarkerIcon : function( markerXml, loc, options, seriesName ) {
			var rgbAttr = markerXml.getAttribute( 'color' );
			var marker = null;
			if ( rgbAttr ) {
				marker = this.augmentColorIcon( rgbAttr, loc, options, seriesName );
				this.canvasLayer.push( marker );
			}
			return marker;
		},

		augmentColorIcon: function( color, loc, options, tooltip, radius, dim ) {
			var uniqueId = _.uniqueId( 'canvasElm' );
			dim = dim || 24;
			radius = radius || 12;

			var tool = tooltip.replace( /\n/g, '&#xa;' );

			options.htmlContent = '<div data-tip="' + tooltip + '"><canvas id="' + uniqueId + '" width="' + dim + '" height="' + dim + '" class="colorMarker"></canvas></div>';
			options.icon = '  ';

			var pushpin = new Microsoft.Maps.Pushpin( loc, options );
			pushpin._canvasID = uniqueId;
			pushpin.renderCanvas = function(){
				var c = document.getElementById( uniqueId );
				if( c ){
					//Draw circular icon
					var context = c.getContext( '2d' );
					context.clearRect( 0, 0, c.width, c.height );
					var centerX = c.width / 2;
					var centerY = c.height / 2;
					context.beginPath();
					context.arc( centerX, centerY, radius, 0, 2 * Math.PI, false );
					context.fillStyle = color;
					context.fill();
					context.lineWidth = 2;
					context.strokeStyle = '#FFFFFF';
					context.stroke();

				}
			};

			Microsoft.Maps.Events.addHandler( pushpin, 'mouseover', function( evt ) {
				throw 'StopBingThemeMouseOverEvent';
			} );

			return pushpin;
		},

		parseDrillAttributes : function( p, name ) {
			var hasDrill = false;
			var attrs = p.getElementsByTagName( ATTRIBUTES );
			for( var z = 0, ln = attrs.length;  z < ln; z++ ){
				var a = attrs[ z ].getElementsByTagName( ATTRIBUTE );
				var nameAttrVals = {};
				var drillCfg = {};
				for( var i = 1, iln = a.length; i < iln; i++ ){
					var ab = a[ i ];
					var attrName = ab.getAttribute( NAME );
					var attrVal = xml.getXmlValue( ab );
					if ( attrName === 'DrillType' ) {
						if( attrVal === 'Drill To Dashboard' ) {
							drillCfg[ attrName ] = attrVal;
						}
					} else if ( attrName === 'targetURL' || attrName === 'filters'){
						drillCfg[ attrName ] = attrVal;
					}
					nameAttrVals[ attrName ] = attrVal;
				}

				this.nameAttributes[ name ] = nameAttrVals;

				if( drillCfg.DrillType ) {
					this.nameDrill[ name ] = drillCfg;
					hasDrill = true;
				}
			}
			return hasDrill;
		},

		parseMarkersGetPlacemarks : function() {
			if( this.data ) {
				var mapSeries = null;
				var dataPlotSettings = this.data.getElementsByTagName( 'data_plot_settings' );
				if( dataPlotSettings && dataPlotSettings.length > 0 ) {
					mapSeries = dataPlotSettings[0].getElementsByTagName( 'map_series' );
					mapSeries = mapSeries && mapSeries.length ? mapSeries[0] : null;
				}

				if( !mapSeries ){
					return;
				}

				var source = mapSeries.getAttribute( 'source' );

				var polygonKeys = [];
				var dt = this.data.getElementsByTagName( 'data' );
				if( dt && dt.length > 0 ) {
					var series = dt[0].getElementsByTagName( 'series' );
					for( var x = 0, ln = series.length; x < ln; x++ ) {
						var type = series[x].getAttribute( 'type' );
						if( type === 'Bar' ) {
							polygonKeys.push( series[x] );
						}
					}
				}

				var doPolyRangeColors = true;
				var polySize = polygonKeys.length;
				if( source !== 'none' && polySize > 0 ) {
					for ( var i = 0; i < polySize; i++ ) {
						var s = polygonKeys[ i ];
						var points = s.getElementsByTagName( POINT );
						var count = points ? points.length : 0;
						if( count > 0 ) {
							for( var y = 0; y < count; y++ ) {
								var p = points[ y ];
								var name = p.getAttribute( NAME );
								if( !this.nameY[ name ] ){
									this.nameY[ name ] = p.getAttribute( Y );
									var attrs = p.getElementsByTagName( ATTRIBUTES );
									for( var z = 0, zln = attrs.length;  z < zln; z++ ) {
										var a = attrs[ z ].getElementsByTagName( ATTRIBUTE );
										if( a.length > 0 ) {
											if( a[0].getAttribute( NAME ) === UMAP ) {
												var attr = xml.getXmlValue( a[0] );
												this.nameYToolTip[ name ] = attr;
												this.parseDrillAttributes( p, name );
											}
										}
									}
								}

								//Custom column colors from a column?
								var rgbAttr = p.getAttribute( COLOR );
								if ( rgbAttr ){
									var rgbColor = color.rgbString2Array( rgbAttr );
									if(rgbColor){
										this.customPolyColors[ name ] = rgbColor;
										doPolyRangeColors = false;
									}
								}
							}
						}
					}
					this.parsePlacemarks( doPolyRangeColors );
				} else {
					this.parseBubbleScatterMarkers();
				}
				this.centerOnBounds();
			}
		},

		parsePlacemarks : function( doPolyRangeColors ) {
			var placemarks = this.data.getElementsByTagName( 'placemarks' );
			if( placemarks.length ) {
				var minValue = 0;
				var maxValue = 0;
				var values = [];
				var polys = placemarks[0].getElementsByTagName( 'pm' );
				for( var i = 0, ln = polys.length; i < ln; i++ ) {
					var pol = polys[ i ];
					var key = pol.getAttribute( 'id' );
					var coords = pol.getElementsByTagName( 'coords' );
					for( var y = 0, yln = coords.length; y < yln; y++ ) {
						var latlngs = [];
						var cordsStr = xml.getXmlValue( coords[ y ] );
						var cordPairs = cordsStr.split( ' ' );
						for( var z = 0, zln = cordPairs.length; z < zln; z++ ) {
							var cordPairsStr = cordPairs[ z ];
							var latlon = cordPairsStr.split( ',' );
							var lat = parseFloat( latlon[0] );
							var lon = parseFloat( latlon[1] );
							var latlng = new Microsoft.Maps.Location( lat, lon );
							latlngs[z] = latlng;
							this.bounds.push( latlng );
						}
						var value = this.nameY[ key ];
						var numValue = parseFloat( value );
						if( numValue ) {
							if( i === 0 ) {
								minValue = maxValue = numValue;
							} else {
								if( numValue < minValue ){
									minValue = numValue;
								}
								if( numValue > maxValue ){
									maxValue = numValue;
								}
							}
							values.push( numValue );
						}
						this.createPolygon( key, latlngs, numValue );
					}
				}

				if( doPolyRangeColors !== false ) {
					this.calculateGradientFromMinMax( minValue, maxValue );
					this.colorRangePolygons( values );
				}

				this.parseBubbleScatterMarkers();
			}
		},

		calculateGradientFromMinMax : function( minValue, maxValue ) {
			var cLen = this.colors.length;
			if( cLen >= 2 ) {
				var rules = [];
				var prev;
				var rangeIncrement = ( maxValue - minValue ) / cLen;
				var borderValues = [];
				borderValues.push(0);
				var rangeValue = minValue;

				for( var i = 0; i < cLen; i++ ) {
					var color = this.colors[ i ];
					//last one
					if( i === cLen - 1 ){
						prev.maxColor = color;
						prev.maxValue = maxValue;
						borderValues.push( maxValue );
						continue;
					}

					var g = new Gradient();
					g.minColor = color;
					if(i === 0){
						g.minValue = minValue;
					}
					if(prev){
						prev.maxColor = color;
					}
					g.maxValue = rangeValue + rangeIncrement;
					prev = g;
					rules.push(g);

					borderValues.push(rangeValue);
					rangeValue += rangeIncrement;
				}
				this.gradientRuleList = new GradientList( rules );

				// if(this.gradientBar){
				// 	this.gradientBar.borders = borderValues;
				// 	this.gradientBar.drawGradientRule(this.gradientRuleList);
				// }
			} else {
				var range = maxValue - minValue;
				var n1 = minValue + ( range / 4 );
				var n2 = n1 + ( range / 4 );
				this.calculateGradient( minValue, n1, n2, maxValue );
			}
		},

		createPolygon : function( key, latlngs, value ) {
			var color = this.customPolyColors[ key ];
			var fillColor = null;
			if( color ) {
				fillColor = new Microsoft.Maps.Color( 100, color[0], color[1], color[2] );
			} else {
				fillColor = new Microsoft.Maps.Color( 100, 100, 0, 100 );
			}
			var options = {
				fillColor : fillColor,
				strokeColor : new Microsoft.Maps.Color( 100, 0, 100, 100 ),
				strokeThickness : 1
			};
			var polygon = new Microsoft.Maps.Polygon( latlngs, options );
			polygon.amount = value;
			polygon.drillKey = key;

			var obj = this;
			Microsoft.Maps.Events.addHandler( polygon, 'mouseover', function( evt ) {
				obj.infoBoxVisibility( evt, true );
			} );
			Microsoft.Maps.Events.addHandler( polygon, 'mouseout', function( evt ) {
				obj.infoBoxVisibility( evt, false );
			} );

			var tt = key;
			var ttvalue = this.nameYToolTip[ key ];
			value = this.nameY[ key ];

			if( ttvalue ) {
				tt += ': ' + ttvalue;
			} else if(value) {
				tt += ': ' + value;
			}

			var pos = latlngs[0];
			var infoboxOffset = new Microsoft.Maps.Point( 0, -50 );
			var infobox = new Microsoft.Maps.Infobox( new Microsoft.Maps.Location( pos.latitude, pos.longitude ),
				{ title : tt, description : '', polygon : polygon, offset : infoboxOffset } );
			this.map.entities.push( infobox );
			this.map.entities.push( polygon );
			this.polygons.push( polygon );
		},

		infoBoxVisibility : function( evt, visibility ) {
			var polygon = evt.target;

			var infoBox = polygon.getInfobox();
			if( infoBox ) {
				if( visibility ) {
					var point = new Microsoft.Maps.Point( evt.getX(), evt.getY() );
					var location = this.map.tryPixelToLocation( point );
					infoBox.setLocation( location );
				}
				infoBox.setOptions( { visible : visibility } );
			}
		},

		calculateGradient : function( n0, n1, n2, n3 ) {
			var g1 = new Gradient();
			g1.minValue = n0;
			g1.maxValue = n1;
			//g1.minColor = 0x008000;
			g1.minColor = 0x00cc99;
			g1.maxColor = 0xffff00;

			var g2 = new Gradient();
			g2.maxColor = 0xff7700;
			g2.maxValue = n2;

			var g3 = new Gradient();
			g3.maxValue = n3;
			g3.maxColor = 0xff0000;

			this.gradientRuleList =  new GradientList( [ g1, g2, g3 ] );
		},

		colorRangePolygons : function( values ) {
			if( values.length > 0 ) {
				var gradientControl = this.gradientRuleList.applyGradientToValueList( values );
				for( var i = 0; i < this.polygons.length; i++ ) {
					var p = this.polygons[ i ];
					if ( !isNaN( p.amount ) ) {
						var colorRgb = gradientControl.rgbColorForValue( p.amount );
						p.setOptions( { fillColor: new Microsoft.Maps.Color( 100, colorRgb.r, colorRgb.g, colorRgb.b ) } );
					}
				}
			}
		}

	} );
	return BingMap;

} );
