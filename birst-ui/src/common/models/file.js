/*
 * File Soap Endpoint
 */

angular.module( 'models.file', [ 'services.soap', 'services.xml', 'services.iconClass' ] )

.factory( 'File', function( soap, xml, iconClass, $http, APP_CONFIG, lm ) {
	'use strict';

	var wsParams = {
		writeable : 'false'
	};

	var LAST_MODIFIED = lm.get( 'LAST_MODIFIED' ) + ': ';
	var GENERIC_CHART_TYPE = 'generic';
	var SEARCH_VIZ_TYPE = 'viz_dashlet';
	var SEARCH_ADHOC_TYPE = 'adhocReport';
	var VISUALIZER_EXT = 'viz.dashlet';
	var ADHOC_EXT = 'AdhocReport';

	var File = soap( APP_CONFIG.fileEndpoint, APP_CONFIG.fileNamespace );

	_.extend( File.prototype, {
		getIconType : function() {
			if( !this.fileType ) {
				this.setFileType();
			}
			return this.iconType;
		},
		setPlugin : function() {
			var tmp = this.fullPath.split( '/' );
			tmp = tmp[ tmp.length - 1 ];
			tmp = tmp.replace( this.reportName + '.', '' ).split( '.' );
			if( tmp.length > 1 ) {
				this.plugin = tmp[0];
			} else {
				this.plugin = false;
			}
		},
		getPlugin : function() {
			if( this.plugin || this.plugin === false ) {
				return this.plugin;
			}
			this.setPlugin();
			return this.plugin;
		},
		setFileType : function() {
			if( this.type === 'file' ) {
				if( this.isStyle() ) {
					this.fileType = 'style';
					this.iconType = 'icon-style';
				} else if( this.isVisualizer() ) {
					this.fileType = 'visualizer';
					this.iconType = 'visualizer-icon';
					this.extension = '.viz.dashlet';
				} else if( this.isAdhoc() ) {
					this.fileType = 'designer';
					this.iconType = 'designer-icon';
					this.extension = '.AdhocReport';
				} else if( this.isPage() ) {
					this.fileType = 'page';
					this.iconType = 'icon-page';
					this.extension = '.page';
				} else if( this.isImage() ) {
					this.fileType = 'image';
					this.iconType = 'icon-image';
					this.extension = this.fileName.match( /\.[0-9a-z]+$/i );
					this.extension = this.extension.length ? this.extension[0] : '';
				} else {
					this.fileType = 'other';
					this.iconType = 'icon-other';
				}
			} else {
				this.fileType = 'folder';
				this.iconType = 'folder-icon';
			}
		},
		isFolder : function() {
			return this.type === 'folder';
		},
		isFile : function() {
			return this.type === 'file';
		},
		isAdhoc : function() {
			return ( /\.AdhocReport$/i ).test( this.fileName );
		},
		isVisualizer : function() {
			return ( /\.viz\.dashlet$/i ).test( this.fileName );
		},
		isPage : function() {
			return ( /\.page$/ ).test( this.fileName );
		},
		isImage : function() {
			return ( /\.png$|\.jpg$|\.gif$|\.jpeg$/i ).test( this.fileName );
		},
		isStyle : function() {
			return ( /^\./ ).test( this.fileName );
		},
		containsActive : function() {
			var test = _.searchRecursive( this.children, { open : true } );
			console.log( test );
			return test;
		},
		walkFolderTree : function( activeDirectory, folderStructure ) {
			var open = false;
			folderStructure = folderStructure ||  this;
			_.each( folderStructure.children, function( child ) {
				//Is this active?
				if( child.path === activeDirectory ) {
					child.open = true;
					open = true;
					//Close the children
					this.walkFolderTree( activeDirectory, child );
				// Are it's children open?
				} else if( child.children.length ) {
					if( !open ){
						//To find if children are open
						open = this.walkFolderTree( activeDirectory, child );
						child.open = open;
					} else {
						child.open = this.walkFolderTree( activeDirectory, child );
					}
				// Otherwise, make sure it is closed
				} else {
					child.open = false;
				}
			}, this );
			return open;
		},
		fileNameCheck : function() {
			if ( this.fileName === undefined || !this.fileName.length ) {
				return 'Invalid report name';
			} else if ( this.fileName.match( /[\/\\\?\%\*\:\"<\>\.\|]/ ) ) {
				return 'Invalid report name';
			} else if ( this.existingFileNameInCurrentDirectory( this.fileName ) ) {
				return 'Report already exists';
			} else if ( !this.fileName.match( /[0-9a-zA-Z]/ ) ) {
				return 'Report name must contain at least one letter or number';
			} else {
				return true;
			}
		},
		existingFileInCurrentDirectory : function( file ) {
			return _.find( this.parent, function( fileNode ) {
				return fileNode.type === 'file' && fileNode.reportName === file.fileName && fileNode !== file;
			} );
		},
		incrementFileName : function() {
			var fileNameArray,
				match,
				counter;

			fileNameArray = this.fileName.split( '.' );
			match = fileNameArray[0].match( /\s\([0-9]+\)/ );

			if (match) {
				counter = Number( match[0].match( /[0-9]+/ )[0] );
				fileNameArray[0] = fileNameArray[0].replace( /\s\([0-9]+\)/, '' );
			} else {
				counter = 0;
			}

			fileNameArray[0] = fileNameArray[0] + ' (' + ( ++counter ) + ')';
			return fileNameArray.join( '.' );
		},
		getUniqueFileName : function( fileName ) {
			if ( typeof fileName === 'object' ) {
				var file = fileName;
				fileName = file.fileName;
				while ( this.existingFileInCurrentDirectory( fileName ) ) {
					fileName = this.incrementFileName( fileName.fileName );
				}
			} else {
				while ( this.existingFileNameInCurrentDirectory( fileName ) ) {
					fileName = this.incrementFileName( fileName );
				}
			}
			return fileName;
		},
		getChartType : function() {
			var chartType;
			if ( this.charts.length === 1 ) {
				chartType = this.charts[0];
			} else if ( this.charts.length > 1 ) {
				chartType = GENERIC_CHART_TYPE;
			}
			return iconClass.get( chartType );
		},
		getReadableDimensions : function() {
			return shortenColumns( this.dimensions );
		},
		getReadableMeasures : function() {
			return shortenColumns( this.measures );
		},
		getReadableExpressions : function() {
			return shortenColumns( this.expressions );
		},
		getReadableColumns : function() {
			return _.union( this.getReadableDimensions(), this.getReadableMeasures(), this.getReadableExpressions() );
		},
		getSortedColumnsAsString : function( query ) {
			var safeQuery = query.replace(/\**/g, '');
			var columns = this.getReadableColumns();

			if ( safeQuery !== '' ) {
				var queryRegExp = new RegExp( safeQuery, 'i' );
				var queryMatchIndex;
				columns = _.sortBy( columns, function( column ) {
					queryMatchIndex = column.search( queryRegExp );
					return queryMatchIndex === -1 ? Infinity : queryMatchIndex;
				} );
			}

			return columns.join(', ');
		}
	} );

	var getFinalParams = function( params ) {
		return angular.extend( {}, wsParams, params );
	};

	var transformResponse = function( data, header ) {
		var files = [];
		data = xml.str2Xml( data );
		_.each( data.querySelectorAll( 'FileNode' ), function( fileNode ) {
			if ( isReportXml( fileNode ) ) {
				var file = {};
				_.each( fileNode.children, function( childNode ) {
					file[ childNode.tagName ] = xml.getXmlValue( childNode );
				}, this );
				file.meta = LAST_MODIFIED + file.modifiedDate;
				files.push( file );
			}
		}, this );
		return files;
	};

	var transformGetDirectory = function( xmlStr, header ) {
		var data = xml.str2Xml( xmlStr );
		var fileNodes = $( data ).find( 'ch > FileNode' );
		var directory = [];
		_.each( fileNodes, function( node ) {
			var $node = $( node );
			//Format for date returned: 2014-04-02 13:50:24 needs to be -> YYYY-MM-DDTHH:mm:ss.sssZ
			var fileNode = new File( {
				fileName : $node.attr( 'n' ),
				reportName : $node.attr( 'l' ),
				type : $node.attr( 'dir' ) === 'true' ? 'folder' : 'file',
				owner : $node.attr( 'CreatedBy' ),
				modified : new Date( $node.attr( 'm' ).replace( ' ', 'T' ) + 'Z' ),
				dashboard : $node.attr( 'dash' ),
				write : $node.attr( 'w' ),
				editing : false
			} );
			fileNode.setFileType();

			directory.push( fileNode );
		} );

		return directory;
	};

	var addDirectories = function( parent, directory ) {
		parent.children = parent.children || [ ];
		_.each( directory.children( 'DirectoryNode' ).get(), function( directoryNode ) {
			var $directoryNode = $( directoryNode );

			var directory = {
				name : $directoryNode.attr('n'),
				path : $directoryNode.attr('p'),
				open : false,
				level : typeof parent.level !== 'undefined' ? parent.level + 1 : -1,
				parent : parent
			};
			Object.defineProperty( directory, 'parent', {
				enumerable : false,
				writable : true
			} );
			addDirectories( directory, $directoryNode.children('ch') );

			parent.children.push( directory );
		});
	};

	var transformVisibleFolderStructure = function( xmlStr ) {
		var data = xml.str2Xml( xmlStr );
		var top = $( data ).find( 'Result' );

		var json = {};

		addDirectories( json, top );

		json = json.children[0];
		json.name = 'My Reports';
		json.path = '/';
		json.open = true;
		json.level = 0;

		return json;
	};

	var getFilePath = function( obj ) {
		return { relativeFilePaths : obj.directory + '/' + obj.fileName };
	};

	var transformSave = function( xmlStr ) {
		return xml.str2Xml( xmlStr );
	};

	var isReportXml = function( node ) {
		return xml.getXmlChildValue( node, 'name' ).indexOf( '.AdhocReport' ) > -1;
	};

	var shortenColumns = function( columns ) {
		return _.map( columns, function( element ) {
			return element.split('.').pop();
		} );
	};

	File.all = function( params ) {
		return File.request(
			'getVisibleFileSystem',
			getFinalParams( params ),
			{ transformResponse : transformResponse }
		);
	};

	var config = {
		url : '/SMIWeb/Dashboards/reports/searchAggregate',
		method : 'GET',
		xsrfHeaderName : 'X-XSRF-TOKEN',
		xsrfCookieName : 'XSRF-TOKEN',
		timeout : 300000,
		transformResponse : function( data ) {
			data = JSON.parse( data );
			var hits = [];
			// create the file instances
			var file, name, fileName, fileExtension;
			_.each( data.hits.hits, function( hit, index ) {
				if ( hit._type !== 'Info') {
					if ( hit._type === SEARCH_VIZ_TYPE ) {
						fileExtension = VISUALIZER_EXT;
					} else if ( hit._type === SEARCH_ADHOC_TYPE ) {
						fileExtension = ADHOC_EXT;
					} else {
						fileExtension = hit._type;
					}
					name = hit.fields.filename[0];
					fileName = name + '.' + fileExtension;
					file = new File( {
						fileName : fileName,
						directory : hit.fields.dir[0],
						fullPath : hit.fields.dir[0] + '/' + fileName,
						reportName : name,
						type : 'file',
						owner : hit.fields.creator ? hit.fields.creator[0] : '',
						modified : new Date( hit.fields.lastModified[0] ),
						dimensions : hit.fields.dimensions || [],
						measures : hit.fields.measures || [],
						expressions : hit.fields.expressions || [],
						charts : hit.fields.charts || [],
						editing : false
					} );
					// TODO get organized about what files we show and dont show
					if ( !file.isStyle() ) {
						hits.push( file );
					}
				}
			} );
			data.hits = hits;
			return data;
		}
	};

	File.search = function( params ) {
		var requestConfig = angular.copy( config );
		// TODO see if we can get the server to accept a colon without url encoding
		if ( params ) {
			requestConfig.url += '?' + $.param( params );
		}
		return File.thenFactoryMethod( $http( requestConfig ) );
	};

	// @param: {{ dir : string, writeable : boolean }}
	File.getDirectory = function( params ) {
		return File.request(
			'getDirectory',
			params,
			{ transformResponse : transformGetDirectory }
		);
	};

	// @param: {{ writeable : boolean }}
	File.getVisibleFolderStructure = function( params ) {
		return File.request(
			'getVisibleFolderStructure',
			getFinalParams( params ),
			{ transformResponse : transformVisibleFolderStructure }
		);
	};

	// @param: {{ dirName : string, directory : string }}
	// @param: function(){}
	File.createDirectory = function( params ) {
		return File.request(
			'createDirectory',
			params,
			{ transformResponse : transformSave }
		);
	};

	//@param: {{ directory : string, fileName : string }}
	File.deleteFile = function( params ) {
		return File.request(
			'deleteFiles',
			getFilePath( params ),
			{ transformResponse : transformSave }
		);
	};

	//@param: {{ directory : string, oldName : string, newName : string}}
	File.renameFile = function( params ) {
		var oldFilePath = getFilePath( { directory : params.directory,
			fileName : params.oldName }
		);
		return File.request(
			'renameFile',
			{ newName : params.newName,
				oldRelativeFilePath : oldFilePath },
			{ transformResponse : transformSave }
		);
	};

	return File;
} );
