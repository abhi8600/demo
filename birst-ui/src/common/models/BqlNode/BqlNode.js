angular.module('visualizer').service('BqlNode', function() {
    'use strict';
    function simpleBqlNodeConstructor(type) {
        return function(val) {
            return new BqlNode({
                type: type,
                value: val
            }, []);
        };
    }

    var BqlNode = function(head, children) {
        this.head = head;
        this.children = children;
    };

    BqlNode.parseFromString = function(expressionString, subjectArea, isMeasureExpression) {
        var bqlMeasureMap = subjectArea && subjectArea.bqlMeasureMap;
        if (bqlMeasureMap) {
            // Grab everything between [ ]
            var columnPart = expressionString.match(/^\[([^\]]*)\]$/);

            if (columnPart) {
                columnPart = columnPart[1];

                if (bqlMeasureMap[columnPart]) {
                    var measureParts = bqlMeasureMap[columnPart];

                    return BqlNode.fromMeasure(measureParts.measure, measureParts.aggregation, measureParts.dateType, measureParts.timeSeriesType);
                } else {
                    var attribute = _.filter(subjectArea.attributes, function(attribute) {
                        return _.last(attribute.path) === columnPart;
                    })[0];

                    if (attribute) {
                        return BqlNode.fromAttribute(attribute);
                    }
                }
            }
        }

        if (typeof isMeasureExpression !== 'undefined') {

            if (isMeasureExpression) {
                return new BqlNode({
                    type: 'expression',
                    subtype: 'measure',
                    value: expressionString
                }, []);
            } else {
                return new BqlNode({
                    type: 'expression',
                    subtype: 'attribute',
                    value: expressionString
                }, []);
            }
        }

        return new BqlNode({
            type: 'external',
            value: expressionString
        }, []);
    };

    BqlNode.fromInt = simpleBqlNodeConstructor("int");
    BqlNode.fromFloat = simpleBqlNodeConstructor("float");
    BqlNode.fromString = simpleBqlNodeConstructor("string");
    BqlNode.fromAttribute = simpleBqlNodeConstructor("attribute");
    BqlNode.fromFromClause = simpleBqlNodeConstructor("fromClause");
    BqlNode.fromParameter = simpleBqlNodeConstructor("parameter");
    BqlNode.fromDateTime = simpleBqlNodeConstructor("datetime");

    BqlNode.fromRankBy = function(exp, rankedExps) {
        return new BqlNode({
            type: 'rankBy'
        }, [exp].concat(rankedExps));
    };

    BqlNode.fromWhereClause = function(exp) {
        return new BqlNode({
            type: 'whereClause'
        }, exp ? [exp] : []);
    };

    BqlNode.fromDisplayWhereClause = function(exp) {
        return new BqlNode({
            type: 'displayWhereClause'
        }, exp ? [exp] : []);
    };

    BqlNode.fromProjectionList = function(elems) {
        return new BqlNode({
            type: 'projectionList'
        }, elems);
    };

    BqlNode.fromQuery = function(projectionList, fromClause, whereClause, displayWhereClause) {
        return new BqlNode({
            type: 'query'
        }, [projectionList, fromClause, whereClause, displayWhereClause]);
    };

    BqlNode.defaultAggregation = function(measure) {
        return _.contains(measure.aggregations, "Sum") ? "Sum" : measure.aggregations[0];
    };

    BqlNode.defaultDateType = function(measure) {
        var dateType = measure.dateTypes[0];

        // Don't use Load Date if we can help it
        if (dateType === "By Load Date" && measure.dateTypes.length > 1) {
            dateType = measure.dateTypes[1];
        }

        // Quick hack to try to add measures with a date type which makes sense.
        _.each(["Forecast", "Inventory"], function(keyword) {
            if (_.contains(measure.label, keyword)) {
                dateType = _.filter(measure.dateTypes, function(dateType) {
                    return _.contains(dateType, keyword);
                })[0] || dateType;
            }
        });

        return dateType;
    };

    BqlNode.fromMeasure = function(measure, aggregation, dateType, timeSeries) {
        if (!aggregation) {
            aggregation = BqlNode.defaultAggregation(measure);
        }

        if (!dateType) {
            dateType = BqlNode.defaultDateType(measure);
        }
        return new BqlNode({
            type: 'measure',
            label: measure.label,
            aggregation: aggregation,
            dateType: dateType,
            timeSeriesType: timeSeries || null,
            bql: measure.bqlString(_.last(measure.path), aggregation, dateType, timeSeries || null)
        }, []);
    };

    BqlNode.blank = function(type) {
        if (!type) {
            type = ["bool", "string", "int", "float", "date", "datetime"];
        }

        return new BqlNode({
            type: "function",
            value: {
                label: 'blank',
                type: type,
                args: []
            }
        }, []);
    };

    // Translate from v1.0.0 persistence format
    BqlNode.fromBqlExportColumn = function(col) {
        if (col.expression.head.type === 'measure') {
            var exp = col.expression;
            return new BqlNode({
                type: 'measure',
                label: exp.head.label,
                aggregation: exp.children[0].head.value,
                dateType: exp.children[1].head.value,
                timeSeriesType: exp.children[2].head.value,
                bql: col.export.bqlExport.replace(/^\[|\]$/g, "")
            }, []);
        } else if (col.expression.head.type === 'attribute') {
            return BqlNode.fromAttribute(col.expression.head.value);
        }
    };

    BqlNode.fromJSON = function(obj) {
        return new BqlNode(obj.head, obj.children);
    };

    BqlNode.prototype = {

        expressionToString: function() {
            var substrings;
            switch (this.head.type) {
                case "measure":
                    return "[" + this.head.bql + "]";
                case "attribute":
                    return "[" + _.last(this.head.value.path) + "]";
                case "function":
                    substrings = _.map(this.children, function(child) {
                        return child.expressionToString();
                    });

                    return this.head.value + "(" + substrings.join(",") + ")";
                case "operator":
                    substrings = _.map(this.children, function(child) {
                        return child.expressionToString();
                    });
                    return "(" + substrings[0] + " " + this.head.value + " " + substrings[1] + ")";
                case "suffix":
                    // e.g. IS NULL
                    return this.children[0].expressionToString() + " " + this.head.value;
                case "datetime":
                    return this.head.value;
                case "projectionList":
                    substrings = _.map(this.children, function(child) {
                        return child.expressionToString();
                    });
                    return substrings.join(", ");
                case "whereClause":
                    if (this.children.length) {
                        return " WHERE " + this.children[0].expressionToString();
                    } else {
                        return "";
                    }
                    break;
                case "displayWhereClause":
                    if (this.children.length) {
                        return " DISPLAY WHERE " + this.children[0].expressionToString();
                    } else {
                        return "";
                    }
                    break;
                case "rankBy":
                    return this.children[0].expressionToString() +
                        " by " +
                        this.children.slice(1).map(function(child) {
                            return child.expressionToString();
                        }).join(",");
                case "query":
                    return "SELECT " + this.children[0].expressionToString() + " FROM " + this.children[1].expressionToString() + this.children[2].expressionToString() + this.children[3].expressionToString();
                case "string":
                    return "'" + this.head.value.replace(/\\/g, "\\\\").replace(/'/g, "\\'") + "'";
                case "datepart":
                case "statType":
                case "datetimepart":
                case "datetime":
                case "parameter":
                case "fromClause":
                    // Number is kept for backward compatibility with old report files, but should never be
                    // created any more - BQL distinguishes float from int.
                case "number":
                case "int":
                    return "" + this.head.value;
                case "float":
                    var ret = "" + this.head.value;
                    if (ret.indexOf(".") === -1) {
                        return ret + ".0";
                    } else {
                        return ret;
                    }
                    break;
                case "saved expression": //SavedExpression(&amp;apos;Marigin &amp;#37;&amp;apos;) &'Marigin%&'
                    return "" + this.head.value.replace(/&apos;/g, '\'');
                case "external":
                    return "" + this.head.value;
                case "expression":
                    return "" + this.head.value;
                case "placeholder":
                    throw "BqlNode: placeholder";
                default:
                    return "{Unhandled Expression Type}";
            }
        },

        getBasicType: function() {
            if (this.isSavedExpression() || this.isExpression()) {
                return this.head.subtype;
            } else {
                return (this.isMeasure()) ? 'measure' : 'attribute';
            }
        },

        getType: function() {
            return (this.isMeasure()) ? 'measure' : 'attribute';
        },

        getValue: function() {
            return this.head.value;
        },

        getLabel: function() {
            return this.head.value.label;
        },

        isTimeColumn: function() {

            if (this.head.value.path[0] === 'Time') {
                return true;
            }

            return false; //return true;
        },

        isSavedExpression: function() {
            return this.head.type === 'saved expression';
        },

        isExpression: function() {
            return this.head.type === 'expression' || this.isSavedExpression();
        },

        isMeasureExpression: function() {
            return this.isExpression() && this.head.subtype === 'measure';
        },

        isAttributeExpression: function() {
            return this.isExpression() && this.head.subtype === 'attribute';
        },

        isSavedMeasure: function() {
            return this.isSavedExpression() && this.head.subtype === 'measure';
        },

        isSavedAttribute: function() {
            return this.isSavedExpression() && this.head.subtype === 'attribute';
        },

        getPath: function() {
            return this.head.value.path;
        },

        isMeasure: function() {
            return this.head.type === 'measure' || this.isSavedMeasure() || this.isMeasureExpression();
        },

        isAttribute: function() {
            return this.head.type === 'attribute' || this.isSavedAttribute() || this.isAttributeExpression();
        },

        isNotBasicColumn: function() {
            return this.head.type !== 'attribute' && this.head.type !== 'measure';
        },

        isAttributeOrCustomExpression: function() {
            return !this.isMeasure();
        },

        isMeasureOrCustomExpression: function() {
            return !this.isAttribute();
        },

        isBlank: function() {
            return this.head.type === 'function' && this.head.value.label === 'blank';
        },

        isValid: function() {
            if (this.isBlank()) {
                return false;
            }

            return _.every(this.children, function(child) {
                return child.isValid();
            });
        },

        toJSON: function() {
            return {
                head: this.head,
                children: this.children,
                _class: "BqlNode"
            };
        },

        clone: function() {
            return new BqlNode(angular.copy(this.head), _.map(this.children, function(child) {
                return child.clone();
            }));
        },

        clientAggregation: function() {
            if (this.head.type !== 'measure') {
                return null;
            }
            switch (this.getAggregation()) {
                case "Count":
                case "Sum":
                    return function(x, y) {
                        return Number(x) + Number(y);
                    };
                case "Max":
                    return Math.max;
                case "Min":
                    return Math.min;
                default:
                    return null;
            }
        },

        getAggregations: function(subjectArea) {
            if (subjectArea) {
                return subjectArea.bqlMeasureMap[this.head.bql].measure.aggregations;
            }
        },
        getDateTypes: function(subjectArea) {
            if (subjectArea) {
                return subjectArea.bqlMeasureMap[this.head.bql].measure.dateTypes;
            }
        },
        getDateTypesWithoutLoadDate: function(subjectArea) {
            if (subjectArea) {
                var dateTypes = this.getDateTypes(subjectArea);
                if (dateTypes.length > 1 && this.getDateType() !== "By Load Date") {
                    return _.without(dateTypes, "By Load Date");
                } else {
                    // only one date type, or the measure is already by load
                    // date for some reason (e.g. external measure). Show all
                    // date types.
                    return dateTypes;
                }
            }
        },
        getTimeSeriesTypes: function(subjectArea) {
            return subjectArea.bqlMeasureMap[this.head.bql].measure.timeSeriesTypes;
        },

        getAggregation: function() {
            return this.head.aggregation;
        },
        getDateType: function() {
            return this.head.dateType;
        },
        getTimeSeriesType: function() {
            return this.head.timeSeriesType;
        },

        setAggregation: function(subjectArea, x) {
            var measure = subjectArea.bqlMeasureMap[this.head.bql].measure;
            this.head.aggregation = x;
            this.head.bql = BqlNode.fromMeasure(measure, this.head.aggregation, this.head.dateType, this.head.timeSeriesType).head.bql;
        },
        setDateType: function(subjectArea, x) {
            var measure = subjectArea.bqlMeasureMap[this.head.bql].measure;
            this.head.dateType = x;
            this.head.bql = BqlNode.fromMeasure(measure, this.head.aggregation, this.head.dateType, this.head.timeSeriesType).head.bql;
        },
        setTimeSeriesType: function(subjectArea, x) {
            var measure = subjectArea.bqlMeasureMap[this.head.bql].measure;
            this.head.timeSeriesType = x;
            this.head.bql = BqlNode.fromMeasure(measure, this.head.aggregation, this.head.dateType, this.head.timeSeriesType).head.bql;
        }
    };

    return BqlNode;
});
