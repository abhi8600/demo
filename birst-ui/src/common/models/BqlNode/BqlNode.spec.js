/* global BqlNode */

describe('BqlNode', function() {
	'use strict';
	var BqlNode;
	var savedExpression = {"type":"saved expression","label":"Uniquely Named Expression","value":"SavedExpression(&apos;Uniquely Named Expression&apos;)","visible":"true","reportLabel":"SavedExpression(&apos;Uniquely Named Expression&apos;)","children":[]};

	beforeEach(module('visualizer'));

	beforeEach( inject( function( _BqlNode_ ) {
		BqlNode = _BqlNode_;
	} ) );

	it('should be able to produce the correct bql for a saved expression', function() {
		var bqlNode = new BqlNode(savedExpression, []);

		expect(bqlNode.expressionToString()).toEqual("SavedExpression('Uniquely Named Expression')"); //"SavedExpression('Uniquely Named Expression')");
	});
});
