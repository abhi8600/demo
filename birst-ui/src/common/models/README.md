# The `src/models/` Directory

The `src/models/` directory houses business objects. These can be either plain
business objects as well as "active directory" type objects that now how to
persist themselves to SOAP or REST services.

Each model resides in its own directory. The build system will read all `*.js`
files that do not end in `.spec.js` as source files to be included in the final 
build, all `*.spec.js` files as unit tests to be executed, there should be no 
template files in the models directory. It should mainly consist of models and 
tests.

```
src/
  |- common/
  |  |- page/
```

- `page` - a dashboard page model that can persist itself to the database. It 
contains both the object definition as well as a persistence strategy.

Every model contained here should be drag-and-drop reusable in any other 
project; they should depend on no other components that aren't similarly 
drag-and-drop reusable.
