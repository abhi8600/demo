angular.module( 'models.reportText', [] )

.factory( 'ReportText', function() {
	'use strict';

	var defaults = {
		xml : null,
		xOrigin : 0,
		yOrigin : 0,
		maxY : 0,
		maxX : 0,
		style : {},
		type : 'text'
	};

	var ReportText = function( opts ){
		angular.extend( this, defaults, opts );
		this.parse();
	};

	ReportText.prototype = {
		parse : function() {
			this.setXY();
			this.backgroundColor = this.xml.getAttribute( 'transparent' ) ? 'transparent' : this.xml.getAttribute( 'backcolor' );
			this.color = this.xml.getAttribute( 'color' );
			this.fontFamily = this.xml.getAttribute( 'face' );
			this.fontWeight = this.xml.getAttribute( 'bold' ) === 'true' ? 'bold' : 'normal';
			this.fontStyle = this.xml.getAttribute( 'italic' ) === 'true' ? 'italic' : 'normal';
			this.fontSize = this.xml.getAttribute( 'size' ) + 'px';
			this.textDecoration = this.xml.getAttribute( 'underline' ) === 'true' ? 'underline' : 'none';
			this.setText();
			this.setAlignment();
			this.setStyle();
			this.link = this.xml.getAttribute( 'link' );
			return this;
		},

		setAlignment : function() {
			this.alignment = this.xml.getAttribute( 'alignment' );
			this.textX = this.x;
			if ( this.alignment === 'left' ) {
				this.anchor = 'start';
			} else if ( this.alignment === 'right' ) {
				this.anchor = 'end';
				this.textX = parseInt( this.x, 10 ) + parseInt( this.width, 10 );
			} else {
				this.anchor = 'middle';
				this.textX = parseInt( this.x, 10 ) + ( parseInt( this.width, 10) / 2 );
			}
		},

		getBorders : function() {
			var borderValues = {},
				borderAttribs = [ 'topBorderColor', 'topBorderWidth', 'topBorderStyle', 'bottomBorderColor', 'bottomBorderWidth', 'bottomBorderStyle', 'leftBorderColor', 'leftBorderWidth', 'leftBorderStyle', 'rightBorderColor', 'rightBorderWidth', 'rightBorderStyle' ];
			var borderStyleNames = [ 'borderTopColor', 'borderTopWidth', 'borderTopStyle', 'borderBottomColor', 'borderBottomWidth', 'borderBottomStyle', 'borderLeftColor', 'borderLeftWidth', 'borderLeftStyle', 'borderRightColor', 'borderRightWidth', 'borderRightStyle' ];
			var temp;
			_.each( borderAttribs, function( attrib, index ) {
				if( attrib.indexOf( 'Color' ) > 0 ) {
					borderValues[ borderStyleNames[ index ] ] = this.xml.getAttribute( attrib ) || 'transparent';
				} else if( attrib.indexOf( 'Width' ) > 0 ) {
					var temp = this.xml.getAttribute( attrib );
					temp = ( !_.isEmpty( temp ) ) ? parseInt( temp, 10 ) + 'px' : 0;
					borderValues[ borderStyleNames[ index ] ] =  temp;
				} else if ( attrib.indexOf( 'Style' ) > 0 ) {
					borderValues[ borderStyleNames[ index ] ] = this.xml.getAttribute( attrib ) || 'none';
				}
			}, this );
			return borderValues;
		},

		setStyle : function() {
			this.style = {
				backgroundColor : this.backgroundColor,
				color : this.color,
				display : 'block',
				fontFamily : this.fontFamily,
				fontWeight : this.fontWeight,
				fontStyle : this.fontStyle,
				fontSize : this.fontSize,
				height : this.height,
				left : this.x,
				position : 'absolute',
				textAlign : this.alignment,
				textDecoration : this.textDecoration,
				top : this.y,
				width : this.width
			};

			_.extend( this.style, this.getBorders() );
		},

		setXY : function() {
			this.width = parseInt( this.xml.getAttribute( 'width' ), 10 );
			this.height = parseInt( this.xml.getAttribute( 'height' ), 10 );
			this.x = this.xml.getAttribute( 'x' );
			this.y = this.xml.getAttribute( 'y' );
			this.x = parseInt(this.x, 10) + this.xOrigin;
			this.y = parseInt(this.y, 10) + this.yOrigin;
			this.maxX = Math.max( this.maxX, this.width + this.x );
			this.maxY = Math.max( this.maxY, this.height + this.y );
		},

		setText : function() {
			this.text = this.xml.getAttribute( 'value' );
		},

		_dataType : 'ReportText'

	};

	return ReportText;

} );
