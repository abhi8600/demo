angular.module( 'models.dashlet', [ 'models.file' ] )

.factory( 'Dashlet', function( xml, File ) {
	'use strict';

	var defaults = {
		EnableExcel : false,
		EnableAdhoc : false,
		EnableSelfSchedule : false,
		EnableCSV : false,
		EnableViewSelector : true,
		ApplyAllPrompts : true,
		ignorePromptList : false,
		EnableSelectorsCtrl : true,
		EnablePPT : false,
		EnablePivotCtrl : false,
		EnablePDF : false,
		plugin : false
	};

	var Dashlet = function( options ) {
		_.extend( this, defaults, options );
	};

	Dashlet.getLabelFromPath = function( pathStr ) {
		return pathStr.split( '/' ).pop();
	};

	angular.extend( Dashlet.prototype, {
		referenceReport : function( file ) {
			this.file = file;
		}
	} );

	return Dashlet;

} );
