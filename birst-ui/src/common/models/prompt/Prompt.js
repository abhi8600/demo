angular.module( 'models.Prompt', [ 'services.genericResource' ] )

.factory( 'Prompt', function( genericResource, APP_CONFIG ) {
	'use strict';

	var defaults = {
		addNoSelectionEntry : true,
		ColumnName : '',
		DefaultOption : '',
		FilterType : 'DATA',
		Invisible : false,
		IsExpression : false,
		multiSelectType : 'OR',
		Operator : '=',
		ParameterName : '',
		PromptType : 'Query',
		Query : '',
		ReadOnly : false,
		selected : false,
		text : 'All',
		ToolTip : '',
		variableToUpdate : false,
		VisibleName : ''
	};

	var Prompt = function( options ) {
		angular.extend( this, defaults, options );
	};

	Prompt = genericResource( Prompt );
	Prompt.setEndpoint( APP_CONFIG.promptEndpoint );
	Prompt.setBlacklist( 'selected', 'active' );

	angular.extend( Prompt.prototype, {

		hasChildPrompts : function() {
			return this.childPrompts.length > 0;
		},

		addChildPrompt : function( prompt ) {
			this.childPrompts.push( prompt );
		},

		getFilledOptions : function() {
			//TODO: Implement new restful service when available
		}

	} );

	return Prompt;
} );

	// var testPrompt = {
  //       '@isInDashlet' : true,
 //        '@ref' : 'PromptControl5924',
 //        addNoSelectionEntry : true,
 //        Class : 'Varchar',
 //        ColumnFormat : '',
 //        ColumnName : 'Time.Year/Month',
 //        DefaultOption : '2018/ 1',
 //        doNotFillIfParentNoSelection : false,
 //        ExpandIntoButtons : false,
 //        FilterType : 'DATA',
 //        hideIfOnlyOneRow : false,
 //        hideLabel : false,
 //        imagePath : '',
 //        Invisible : false,
 //        IsExpression : false,
 //        isInDashlet : true,
 //        Multiline : false,
 //        multiSelectType : 'OR',
 //        operationName : '',
 //        Operator : '=',
 //        OperatorParameterName : '',
 //        Options : '',
 //        ParameterName : 'Time.Year/Month',
 //        Parent : ['Time.Year', 'Time.Year', 'Time.Year', 'Time.Year'],
 //        Parents : ['', '', '', {
 //            Parent : 'Time.Year'
 //        }],
 //        PromptType : 'Query',
 //        Query : 'SELECT [Time.Year/Month] FROM [ALL] ORDER BY [Time.Year/Month] ASC',
 //        QueryColName : '',
 //        ReadOnly : false,
 //        row : 0,
 //        selectedDate : '',
 //        SliderDefaultEnd : '',
 //        SliderDefaultStart : '',
 //        SliderDispLabels : 'Ends',
 //        SliderEndPt : 10,
 //        SliderEndPts : 'Query-Based',
 //        SliderIncrement : 1,
 //        SliderStartPt : 0,
 //        SliderType : 'Point',
 //        SliderWidth : 100,
 //        targetDashboardName : '',
 //        text : 'All',
 //        ToolTip : '',
 //        TreeControl : false,
 //        UseDrillPath : false,
 //        validationMessage : '',
 //        variableToUpdate : false,
 //        Version : 2,
 //        VisibleName : 'Year/Month',

 //    };
