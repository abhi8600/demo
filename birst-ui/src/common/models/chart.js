angular.module( 'models.Chart', [ 'services.xml', 'models.BingMap' ] )

.factory( 'Chart', function( xml, BingMap ) {
	'use strict';

	var defaults = {
		xml : null,
		xOrigin : 0,
		yOrigin : 0,
		static : false,
		height : 0,
		width : 0
	};

	var Chart = function( opts ) {
		_.extend( this, defaults, opts );
		this.parse();
	};

	angular.extend( Chart.prototype, {
		parse : function() {
			var sprite,
				chartTypeXml;

			this.anychartXml = this.xml.childNodes[0];
			if( this.anychartXml && this.anychartXml.tagName === 'anychart' ) {
				if( this.static ) {
					this.width = this.xml.getAttribute( 'width' );
					this.height = this.xml.getAttribute( 'height' );
				}
				this.x = parseInt( this.xml.getAttribute( 'x' ), 10 ) + this.xOrigin;
				this.y = parseInt( this.xml.getAttribute( 'y' ), 10 ) + this.yOrigin;

				//Fixup for DASH-368. Client side html5 only for now. Could be server side if Flex is ok
				//with the additional parameter
				var series = this.xml.getElementsByTagName( 'series' );
				for( var i = 0, ln = series.length; series && i < ln; i++ ){
					var se = series[i];
					se.setAttribute( 'missing_points', 'no_paint' );
				}

				this.chartType = 'gauge';
				if ( this.xml.nodeName !== 'gauge' ) {
					chartTypeXml = this.xml.getElementsByTagName( 'chart' );
					this.chartType = chartTypeXml[0].getAttribute( 'plot_type' );
				}
				if ( this.chartType === 'Scatter' ) {
					// do more to determine if its a bubble chart
					var seriesList = this.xml.getElementsByTagName( 'series' );
					if ( seriesList && seriesList.length > 0 ) {
						series = seriesList[0];
						var isBubble = false;
						while( series && !isBubble ) {
							isBubble = ( series.getAttribute( 'type' ) === 'Bubble' );
							series = series.nextSibling;
						}

						if ( isBubble ) {
							this.chartType = 'Bubble';
						}
					}
				}
				if ( this.chartType === 'Radar' || this.chartType === 'Map' ) {
					/*sprite = this.createChartContainer($surface, x, y, this.width, this.height);
					var label = Ext.create('Ext.form.Label', {
						text: Ext.String.format(Birst.LM.Locale.get('CHART_NOTSUPPORTED'),chartType),
						cls: 'chartError',
						y: '50%',
						anchor: '100%'
					});
					sprite.add(label);*/
				} else if( this.chartType === 'UMap' ){
					this.bingMap = new BingMap( { data : chartTypeXml[0], width : this.width, height : this.height, static : this.static } );
					/*sprite = this.createChartContainer( $surface, x, y, this.width, this.height );
					var bingMaps = Ext.create('Birst.dashboards.BingMaps', sprite.body.dom.id, chartTypeXml[0], this);
					bingMaps.render();*/
				} else {
					this.anyChart = {};
					this.anyChart.chartData = xml.xml2Str( this.anychartXml );
					this.anyChart.renderingType = anychart.RenderingType.SVG_ONLY;
					this.anyChart.enabledChartMouseEvents = true;
					this.anyChart.width = this.width;
					this.anyChart.height = this.height;
					this.maxX = Math.max( this.maxX, this.anyChart.width + this.x );
					this.maxY = Math.max( this.maxY, this.anyChart.height + this.y );

					//chart.addEventListener('pointClick', this.handleChartClick);
					//chart.addEventListener('pointSelect', this.handleChartClick);
					//var obj = this;
					/*if ( this.isVisualFilter() ) {
						chart.addEventListener( 'multiplePointsSelect', function( e ) {
							obj.handleSelectionChange( e, chart );
						} );
					}
					chart.write( $surface.attr( 'id' ) );*/

					//We add the chart's location for each renderer so we can check if we are clicking
					//on a chart object and disable our dragtracker visual filtering which interferes with AnyChart
					//visual filtering.
					//TEMPOMIT: Birst.core.AnyChartTracker.add(chart.id, this,  { x: x, y: y, width: this.width, height: this.height} );
				}

			}

			return this;
		}
	} );

	return Chart;

} );
