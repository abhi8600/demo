angular.module( 'services.chartCollection', [ 'models.reportText', 'models.reportButton', 'services.xml', 'models.reportImage' ] )

.factory( 'chartCollection', function( ReportText, ReportButton, xml, ReportImage, $log ) {
	'use strict';

	var X_CHAR = 'x';
	var Y_CHAR = 'y';
	var CHART = 'chart';
	var JASPER = 'net.sf.jasperreports.crosstab.cell.type';
	var FRAME = 'frame';
	var IMAGE = 'image';
	var GAUGE = 'gauge';
	var TEXT = 'text';
	var BTN = 'adhocButton';
	var CROSSTAB = 'CrosstabHeader';
	var JASP_TYPES = [ 'Data', 'RowHeader', CROSSTAB, 'ColumnHeader' ];
	var WIDTH = 'width';
	var HEIGHT = 'height';
	var NORMAL = 'normal';

	var ChartCollection = function( xmlDoc ) {
		this.xmlDoc = xmlDoc;
		this.width = 0;
		this.height = 0;
		this.static = true;
		this.itemCollection = [];
		this.chartCollection = [];
		this.frameCollection = [];
	};

	ChartCollection.prototype = {
		//TODO: Is this needed anyore?
		calcMaxDims : function( item, xOffset, yOffset ) {
			var frameX = xml.getAttributeIntValue( item, X_CHAR );
			var frameY = xml.getAttributeIntValue( item, Y_CHAR );
			if ( item.tagName === 'frame' ) {
				var child = item.firstChild;
				while( child ){
					this.calcMaxDims( child, frameX + xOffset, frameY + yOffset );
					child = child.nextSibling;
				}
			} else {
				var x = frameX + xOffset;
				var y = frameY + yOffset;
				var width  = xml.getAttributeIntValue( item, WIDTH );
				var height = xml.getAttributeIntValue( item, HEIGHT );
				if ( isFinite( width ) && isFinite( x ) ) {
					this.width = Math.max( this.width, width + x );
				}
				if ( isFinite( height ) && isFinite( y ) ) {
					this.height = Math.max( this.height, height + y );
				}
			}
		},

		createTextObject : function( opts ) {
			var txt = new ReportText( opts );
			this.itemCollection.push( txt );
			return txt;
		},

		parseFrame : function( frameXml, xOrigin, yOrigin ) {
			var frameX = xml.getAttributeIntValue( frameXml, X_CHAR );
			var frameY = xml.getAttributeIntValue( frameXml, Y_CHAR );
			var child = frameXml.firstChild;
			this.directChild( child, frameX, frameY );
		},

		directChild : function( child, xOrigin, yOrigin ) {
			while( child ) {
				if( child.tagName === CHART || child.tagName === GAUGE ) {
					this.chartCollection.push( { xml : child, scale : false, type : NORMAL, x : xOrigin, y : yOrigin } );
				} else if (child.tagName === FRAME) {
					this.parseFrame( child, xOrigin, yOrigin );
				}
				child = child.nextSibling;
			}
		},

		processElement : function( child, xOrigin, yOrigin ) {
			while( child ){
				//TODO: What is this? this.calcMaxDims( child, 0, 0 );
				var cTag = child.tagName;
				// Is text element?
				if( cTag === TEXT ){
					this.createTextObject( { xml : child } );
				//Is Frame element
				} else if ( cTag === FRAME ) {
					var grandChild = child.firstChild;
					var label, cellType;
					var childCounts = 0;
					xOrigin = xml.getAttributeIntValue( child, X_CHAR );
					yOrigin = xml.getAttributeIntValue( child, Y_CHAR );

					while ( grandChild ) {
						var tag = grandChild.tagName;
						if( tag === TEXT ) {
							label = this.createTextObject( { xml : grandChild, xOrigin : xOrigin, yOrigin : yOrigin } );
							childCounts++;
						} else if( tag === BTN ) {
							this.itemCollection.push( new ReportButton( { xml : grandChild, xOrigin : xOrigin, yOrigin : yOrigin } ) );
						} else if( tag === JASPER ) {
							cellType = xml.getXmlValue( grandChild );
						} else if( tag === IMAGE ) {
							this.itemCollection.push( new ReportImage( { xml : grandChild } ) );
						}
						grandChild = grandChild.nextSibling;
					}
					if ( label && ( _.indexOf( JASP_TYPES, cellType ) > 0 ) ) {
						var textWidth =  xml.getAttributeIntValue( child, WIDTH );
						var textHeight = Math.max( xml.getAttributeIntValue( child, HEIGHT ), label.height );
						if (childCounts > 1) {
							textWidth = label.width + ( textWidth - ( (label.x - xOrigin ) + label.width ) );
						}

						if ( cellType === CROSSTAB ) {
							textHeight = label.height;
						} else if ( childCounts > 1 ) {
							textHeight = label.height + xml.getAttributeIntValue( child, HEIGHT ) - ( ( label.y - yOrigin ) + label.height );
						}

						label.width = Math.min( label.width, textWidth );
						label.height = textHeight;
					}
				} else if ( cTag === BTN ) {
					this.itemCollection.push( new ReportButton( { xml : child, xOrigin : 0, yOrigin : 0 } ) );
				} else if ( cTag === IMAGE ) {
					this.itemCollection.push( new ReportImage( { xml : child, xOrigin : 0, yOrigin : 0 } ) );
				}
				child = child.nextSibling;
			}
		},

		parse : function() {
			var child;
			if( this.xmlDoc.length ) {
				this.xmlDoc = this.xmlDoc[0];
			}
			try {
				child = this.xmlDoc.firstChild;
				this.processElement( child );
			} catch( err ) {
				$log.error( 'An error has occured parsing the Adhoc response.' );
				$log.error( err );
			}

			child = this.xmlDoc.firstChild;
			this.directChild( child );

			if( this.itemCollection.length === 0 && this.chartCollection.length === 1 ) {
				this.static = false;
			}

			return this;
		}
	};

	return function( reportXml ) {
		var charts = new ChartCollection( reportXml );
		return charts.parse();
	};

} );
