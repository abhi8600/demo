angular.module( 'models.searchSession', [ 'models.filterGroup', 'services.iconClass' ] )

.factory( 'SearchSession', function( FilterGroup, iconClass, lm ) {
	'use strict';

	var DATE_RANGE_KEYS = [
		'OVER_A_YEAR',
		'LAST_YEAR',
		'LAST_MONTH',
		'LAST_WEEK',
		'TODAY'
	];
	var QUERY_EXTENSION = '.raw';
	var CREATOR = 'creator';
	var CREATOR_QUERY_NAME = CREATOR + QUERY_EXTENSION;
	var CHARTS = 'charts';
	var CHARTS_QUERY_NAME = CHARTS + QUERY_EXTENSION;
	var RANGE = 'range';
	var RANGE_QUERY_NAME = 'lastModified';
	var CHART_LM_PREFIX = 'CHART_';
	var CREATOR_INDEX = 0;
	var CHARTS_INDEX = 1;
	var DATE_INDEX = 2;
	var DOC_COUNT = 'doc_count'; //TODO get see neng to change this property to camelCased version
	var FILTER_PREVIEW_SIZE = 5;
	var FILTER_LIMIT_PER_GROUP = 10000;
	var FILTER_NAME_KEY = 'key';

	var updateFilterGroups = function( searchResponse ) {
		var that = this;
		var aggregations = searchResponse.aggregations;
		var emptyRangeFilters = [];

		_.each( this.filterGroups, function( filterGroup ) {
			if ( filterGroup !== that.lastSelectedGroup ) {
				filterGroup.filters = aggregations[ filterGroup.name ].buckets;

				_.each( filterGroup.filters, function( filter, index ) {
					if ( filterGroup.name === CREATOR ) {
						filter.copyName = filter.key;
					} else if ( filterGroup.name === CHARTS ) {
						filter.copyName = lm.get( CHART_LM_PREFIX + filter.key.toUpperCase() );
						filter.icon = iconClass.get( filter.key );
					} else {
						if ( filter[DOC_COUNT] === 0 ) {
							emptyRangeFilters.push( filter );
						} else {
							filter.copyName = lm.get( DATE_RANGE_KEYS[ index ] );
							var from = filter.from || '*';
							filter.key = '[' + from + ' TO ' + filter.to + ']';
						}
					}
					filter.queryName = filterGroup.queryName;
					if ( _.some( filterGroup.selectedFilters, _.pick( filter, 'queryName', 'copyName') ) ) {
						console.log('selected:', filter.key);
						filter.selected = true;
					}
				} );
				// sort each filter group
				if ( filterGroup.name === RANGE ) {
					_.each( emptyRangeFilters , function( filter ) {
						_.pull( filterGroup.filters, filter );
					} );
					filterGroup.filters.reverse(); // server giving this in reverse order desired
				} else {
					filterGroup.filters = _.sortBy( filterGroup.filters, FILTER_NAME_KEY );
				}
			}
		});
	};

	var SearchSession = function( SearchClass ) {
		var that = this;

		this.filterGroups = [
			new FilterGroup( {
				name : CREATOR,
				copyName : 'Owner',
				queryName : CREATOR_QUERY_NAME,
			} ),
			new FilterGroup( {
				name : CHARTS,
				copyName : 'Chart type',
				queryName : CHARTS_QUERY_NAME,
			} ),
			new FilterGroup( {
				name : RANGE,
				copyName : 'Modified date',
				queryName : RANGE_QUERY_NAME,
			} )
		];

		this.search = function() {
			this.waiting = true;
			var fullQuery = this.generateQuery();
			return SearchClass.search( { q : fullQuery } ).then( function( searchResponse ) {
				that.hits = searchResponse.hits;
				updateFilterGroups.call( that, searchResponse );
				that.waiting = false;
			} );
		};
	};

	SearchSession.prototype = {
		query : '',
		hits : [],
		waiting : false,
		generateQuery : function() {
			var groupsWithSelectedFilters = _.filter( this.filterGroups, function( filterGroup ) {
				return !_.isEmpty( filterGroup.selectedFilters );
			} );
			var queries = _.map( groupsWithSelectedFilters, function( filterGroup ) {
				var keys = _.pluck( filterGroup.selectedFilters, 'key' );
				return filterGroup.queryName + ':(' + keys.join( ' ' ) + ')';
			});
			queries.push( this.query );
			return queries.join( ' AND ' );
		},
		clearSelectedFilters : function( options ) {
			options = options || {};
			_.each( this.filterGroups, function( filterGroup ) {
				filterGroup.limit = FILTER_PREVIEW_SIZE;
				_.each( filterGroup.selectedFilters, function( filter ) {
					filter.selected = false;
				} );
				_.each( filterGroup.filters, function( filter ) {
					filter.selected = false;
				} );
				filterGroup.selectedFilters = [];
			} );
			this.lastSelectedGroup = undefined;
			if ( !options.preventSearch ) {
				this.search();
			}
		},
		clearFilters : function() {
			_.each( this.filterGroups, function( filterGroup ) {
				filterGroup.filters = [];
			} );
			this.clearSelectedFilters( {
				preventSearch : true
			} );
		},
		removeSelectedFilter : function( filter ) {
			filter.selected = false;
			var relevantFilterGroup = _.find( this.filterGroups, {queryName : filter.queryName});
			_.remove( relevantFilterGroup.selectedFilters, _.pick( filter, 'queryName', 'copyName') );
			this.search();
		},
		toggleFilter : function ( filter ) {
			var relevantFilterGroup = _.find( this.filterGroups, {queryName : filter.queryName});
			this.lastSelectedGroup = relevantFilterGroup;

			if ( _.some( relevantFilterGroup.selectedFilters, _.pick( filter, 'queryName', 'copyName') ) ) {
				this.removeSelectedFilter( filter );
			} else {
				if ( !relevantFilterGroup.multiSelectable ) {
					_.each( relevantFilterGroup.selectedFilters, function( filter ) {
						filter.selected = false;
					} );
					relevantFilterGroup.selectedFilters = [];
				}
				relevantFilterGroup.selectedFilters.push( filter );
				console.log('adding filter:', filter);
				this.search();
			}
		},
		reset : function() {
			this.query = '';
			this.clearFilters();
			this.hits = [];
		},
		hasFilters : function() {
			return _.some( this.filterGroups, function( filterGroup ) {
				return filterGroup.hasFilters();
			} );
		},
		hasSelectedFilters : function() {
			return _.some( this.filterGroups, function( filterGroup ) {
				return filterGroup.hasSelectedFilters();
			} );
		},
		getSelectedFilters : function() {
			return _.flatten( _.pluck( this.filterGroups, 'selectedFilters' ) );
		},
		getNonEmptyFilterGroups : function() {
			return _.filter( this.filterGroups, function( filterGroup ) {
				return !_.isEmpty( filterGroup.filters );
			});
		}
	};

	return SearchSession;

} );
