angular.module( 'models.reportButton', [ 'models.reportText' ] )

.factory( 'ReportButton', function( ReportText ) {
	'use strict';

	var ReportButton = function( opts ) {
		var defaults = {
			xml : null,
			xOrigin : 0,
			yOrigin : 0,
			fontFamily : null,
			fontSize : null,
			fontStyle : null,
			fontWeight : null,
			type: 'button'
		};

		_.extend( this, defaults, opts );
		ReportText.prototype.parse.call( this );

	};

	var setStyle = function() {
		this.style = {
			color : this.color,
			fontFamily : this.fontFamily,
			fontWeight : this.fontWeight,
			fontStyle : this.fontStyle,
			fontSize : this.fontSize,
			height : this.height,
			left : this.x,
			position : 'absolute',
			textAlign : this.alignment,
			textDecoration : this.textDecoration,
			top : this.y,
			width : this.width
		};
		_.extend( this.style, this.getBorders() );
	};

	ReportButton.prototype = _.extend( {}, ReportText.prototype, {
		_dataType : 'ReportButton',
		setStyle : function() {
			this.style = {
				color : this.color,
				fontFamily : this.fontFamily,
				fontWeight : this.fontWeight,
				fontStyle : this.fontStyle,
				fontSize : this.fontSize,
				height : this.height,
				left : this.x,
				position : 'absolute',
				textAlign : this.alignment,
				textDecoration : this.textDecoration,
				top : this.y,
				width : this.width
			};
		}
	} );

	return ReportButton;

} );
