angular.module( 'models.pageItem', [ 'models.dashlet', 'services.boundary' ] )

.factory( 'PageItem', function( Dashlet, boundary ) {
	'use strict';

	var PERCENT_LEFT = 'percentLeft';
	var PERCENT_TOP = 'percentTop';
	var PERCENT_WIDTH = 'percentWidth';
	var PERCENT_HEIGHT = 'percentHeight';
	var Z_INDEX = 'zIndex';
	var DASHLET = 'dashlet';

	var defaults = {
		percentLeft : 0,
		percentTop : 0,
		percentWidth : 49,
		percentHeight : 49,
		zIndex : 1
	};

	var PageItem = function( options ) {
		_.extend( this, defaults, options );
		this.dashlet = new Dashlet( this.dashlet );
	};

	angular.extend( PageItem.prototype, {
		// sets the position based on center point coordinates
		setCenter : function( coordinates ) {
			this.setProperties( {
				percentLeft : coordinates.x - this.percentWidth / 2,
				percentTop : coordinates.y - this.percentHeight / 2
			} );
		},

		setProperties : function( properties, options ) {
			options = options || {};
			var originalState = _.pick( this, PERCENT_LEFT, PERCENT_TOP, PERCENT_WIDTH, PERCENT_HEIGHT );
			var newState = _.extend( {}, originalState, properties );
			newState = options.resize ? boundary.boundedResize( newState, options.axis ) : boundary.boundedProperties( newState );
			newState = boundary.minSizedProperties( newState );

			if ( options.resize && options.axis ) {
				newState = boundary.gridAlignedResize( newState, this, options.axis );
			} else {
				newState = boundary.gridAlignedProperties( newState, this );
			}

			angular.extend( this, newState );
		},

		setState : function( state ) {
			_.extend( this, state );
		},

		asPersisted : function() {
			return _.pick( this, PERCENT_LEFT, PERCENT_TOP, PERCENT_WIDTH, PERCENT_HEIGHT, Z_INDEX, DASHLET );
		}
	} );

	return PageItem;

} );
