angular.module( 'models.selectionGroup', [ 'services.boundary' ] )

.factory( 'SelectionGroup', function( boundary ) {
	'use strict';

	var PERCENT_LEFT = 'percentLeft';
	var PERCENT_TOP = 'percentTop';
	var PERCENT_WIDTH = 'percentWidth';
	var PERCENT_HEIGHT = 'percentHeight';


	var updateProperties = function( selectionGroup ) {
		var minX = Infinity;
		var minY = Infinity;
		var maxX = -Infinity;
		var maxY = -Infinity;

		_.each( selectionGroup.elements, function( element ) {
			minX = Math.min( element.percentLeft, minX );
			minY = Math.min( element.percentTop, minY );
			maxX = Math.max( element.percentLeft + element.percentWidth, maxX );
			maxY = Math.max( element.percentTop + element.percentHeight, maxY );
		} );

		selectionGroup.percentLeft = minX;
		selectionGroup.percentTop = minY;
		selectionGroup.percentWidth = maxX - minX;
		selectionGroup.percentHeight = maxY - minY;
	};

	var SelectionGroup = function() {
		this.elements = [];
		Object.defineProperty( this, 'elements', {
			enumerable : false,
			writable : true
		} );
		this.size = 0;
		this.percentLeft = 0;
		this.percentTop = 0;
		this.percentWidth = 0;
		this.percentHeight = 0;
	};

	SelectionGroup.prototype = {

		contains : function( element ) {
			return _.contains( this.elements, element );
		},
		
		add : function( element ) {
			if ( !_.contains( this.elements, element ) ) {
				this.elements.push( element );
				element.selected = true;
				element.selectionGroup = this;
				updateProperties( this );
				this.size++;
			}
		},

		remove : function( element ) {
			var elementIndex = _.indexOf( this.elements, element );
			if ( elementIndex > -1 ) {
				this.elements.splice(elementIndex, 1);
				element.selected = false;
				element.selectionGroup = undefined;
				updateProperties( this );
				this.size--;
			}
		},

		clear : function() {
			_.each( this.elements, function( element ) {
				element.selected = false;
				element.selectionGroup = undefined;
			} );
			this.elements = [];
			updateProperties( this );
			this.size = 0;
		},

		setProperties : function( properties, options ) {
			options = options || {};
			var originalState = _.pick( this, PERCENT_LEFT, PERCENT_TOP, PERCENT_WIDTH, PERCENT_HEIGHT );
			var newState = _.extend( {}, originalState, properties );
			newState = options.resize ? boundary.boundedResize( newState, options.axis ) : boundary.boundedProperties( newState );
			newState = boundary.minSizedProperties( newState );

			if ( options.resize && options.axis ) {
				newState = boundary.gridAlignedResize( newState, this, options.axis );
			} else {
				newState = boundary.gridAlignedProperties( newState, this );
			}
			if ( !_.isEqual( originalState, newState ) ) {
				_.each( this.elements, function( element ) {
					element.setProperties( {
						percentLeft : element.percentLeft + newState.percentLeft - originalState.percentLeft,
						percentTop : element.percentTop + newState.percentTop - originalState.percentTop,
						percentWidth : element.percentWidth + newState.percentWidth - originalState.percentWidth,
						percentHeight : element.percentHeight + newState.percentHeight - originalState.percentHeight
					}, options );
				} );
				_.extend( this, newState );
			}
		},

		setState : function( state ) {
			_.extend( this, state );
			_.each( this.elements, function( element ) {
				element.setState( state );
			} );
		}
	};

	return SelectionGroup;

} );
