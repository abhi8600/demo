describe( 'SelectionGroup', function() {
	'use strict';

	var SelectionGroup, selectionGroup, pageItem, anotherPageItem;

	var PageItem = function(left, top, width, height, zIndex) {
		this.percentLeft = left || 0;
		this.percentTop = top || 0;
		this.percentWidth = width || 50;
		this.percentHeight = height || 50;
		this.zIndex = zIndex || 1;
	};

	beforeEach( module( 'models.selectionGroup' ) );

	beforeEach( inject( function( $injector ) {
		SelectionGroup = $injector.get( 'SelectionGroup' );
		selectionGroup = new SelectionGroup();
		pageItem = new PageItem();
		anotherPageItem = new PageItem();
	} ) );

	describe( 'after instantiation', function() {
		it( 'should have a size of zero', function() {
			expect( selectionGroup.size ).toBe( 0 );
		} );

		it( 'should have zeroed out position and dimension properties', function() {
			expect( selectionGroup.percentLeft ).toBe( 0 );
			expect( selectionGroup.percentTop ).toBe( 0 );
			expect( selectionGroup.percentWidth ).toBe( 0 );
			expect( selectionGroup.percentHeight ).toBe( 0 );
		} );
	} );

	describe( '#contains()', function() {
		it( 'should return true if an element is in the group', function() {
			selectionGroup.add( pageItem );
			expect( selectionGroup.contains( pageItem ) ).toBe( true );
		} );

		it( 'should return false if an element is NOT in the group', function() {
			expect( selectionGroup.contains( pageItem ) ).toBe( false );
		} );
	} );

	describe( '#add()', function() {
		it( 'should add a PageItem', function() {
			selectionGroup.add( pageItem );
			expect( selectionGroup.elements[0] ).toBe( pageItem );
		} );

		it( 'should not allow duplicate elements', function() {
			selectionGroup.add( pageItem );
			selectionGroup.add( pageItem );
			expect( selectionGroup.size ).toBe( 1 );
		} );

		it( 'should increase the size count', function() {
			selectionGroup.add( pageItem );
			expect( selectionGroup.size ).toBe( 1 );
			selectionGroup.add( anotherPageItem );
			expect( selectionGroup.size ).toBe( 2 );
		} );

		it( 'should update the position and dimension properties of the selection group', function() {
			var value = 40;
			selectionGroup.add( new PageItem( value, value, value, value ) );
			expect( selectionGroup.percentLeft ).toBe( value );
			expect( selectionGroup.percentTop ).toBe( value );
			expect( selectionGroup.percentWidth ).toBe( value );
			expect( selectionGroup.percentHeight ).toBe( value );

			selectionGroup.add( pageItem );
			expect( selectionGroup.percentLeft ).toBe( 0 );
			expect( selectionGroup.percentTop ).toBe( 0 );
			expect( selectionGroup.percentWidth ).toBe( 80 );
			expect( selectionGroup.percentHeight ).toBe( 80 );
		} );
	} );

	describe( '#remove()', function() {
		it( 'should not modify the size when trying to remove an element not in the group', function() {
			selectionGroup.add( pageItem );
			selectionGroup.remove( anotherPageItem );
			expect( selectionGroup.size ).toBe( 1 );
		} );

		it( 'should only remove the element passed', function() {
			selectionGroup.add( pageItem );
			selectionGroup.add( anotherPageItem );
			selectionGroup.remove( pageItem );
			expect( selectionGroup.elements[0] ).toBe( anotherPageItem );
		} );

		it( 'should decrement the size count', function() {
			selectionGroup.add( pageItem );
			selectionGroup.remove( pageItem );
			expect( selectionGroup.size ).toBe( 0 );
		} );

		it( 'should update the position and dimension properties of the selection group', function() {
			selectionGroup.add( pageItem );
			var value = 40;
			selectionGroup.add( new PageItem( value, value, value, value ) );
			expect( selectionGroup.percentLeft ).toBe( 0 );
			expect( selectionGroup.percentTop ).toBe( 0 );
			expect( selectionGroup.percentWidth ).toBe( 80 );
			expect( selectionGroup.percentHeight ).toBe( 80 );

			selectionGroup.remove( pageItem );
			expect( selectionGroup.percentLeft ).toBe( value );
			expect( selectionGroup.percentTop ).toBe( value );
			expect( selectionGroup.percentWidth ).toBe( value );
			expect( selectionGroup.percentHeight ).toBe( value );
		} );
	} );

	describe( '#clear()', function() {
		it( 'should work even when the group is empty', function() {
			expect( selectionGroup.size ).toBe( 0 );
			selectionGroup.clear();
			expect( selectionGroup.size ).toBe( 0 );
		} );

		it( 'should remove all elements in the group', function() {
			selectionGroup.add( pageItem );
			selectionGroup.add( anotherPageItem );
			selectionGroup.clear();
			expect( selectionGroup.elements.length ).toBe( 0 );
		} );

		it( 'should change size to be zero', function() {
			selectionGroup.add( pageItem );
			selectionGroup.add( anotherPageItem );
			selectionGroup.clear();
			expect( selectionGroup.size ).toBe( 0 );
		} );
	} );

});
