angular.module( 'models.report', [ 'services.soap', 'services.xml', 'models.chartSelector', 'services.chartCollection' ] )

.factory( 'Report', function( soap, xml, $http, APP_CONFIG, chartSelector, chartCollection, lm ) {
	'use strict';

	var wsParams = {
		name : '/',
		prompts : '<Prompts></Prompts>',
		page : '0',
		applyAllPrompts : 'true',
		pagePath : 'NO_LOG',
		guid : '0_NO_LOG',
		isDashletEdit : 'true'
	};

	var wsParamsOrder = [
		'name',
		'prompts',
		'page',
		'applyAllPrompts',
		'pagePath',
		'guid'
	];

	var Report = soap( APP_CONFIG.adhocEndpoint, APP_CONFIG.adhocNamespace );

	var transformResponse = function( data, header ) {
		var resp = {};
		data = $.parseXML( data );
		var $data = $( data );
		resp.adhocXml = getAdhocReportXml( data );
		resp.report = chartCollection( $data.find( 'report' ) );
		resp.selectors = chartSelector( $data.find( 'Selectors' ) );
		resp.renderer = $data.find( 'reportRenderer' );
		resp.pager = getPagerObject( resp.renderer );
		resp.errorCode = $data.find( 'ErrorCode' ).text();
		resp.errorMessageReason = $data.find( 'ErrorMessage' ).text();
		resp.errorMessage = lm.get( resp.errorCode ) || '';
		return resp;
	};

	var transformPluginResponse = function( data, header ) {
		var resp = {};
		data = $( $.parseXML( data ) );
		resp.pluginData = data.find( 'data' ).text();
		return resp;
	};

	var getPagerObject = function( reportRenderers) {
		var pager = {};
		if( reportRenderers && reportRenderers.length > 0 ) {
			var numPages = xml.getXmlChildValue( reportRenderers[0], 'numberOfPages' );
			var currentPage = xml.getXmlChildValue( reportRenderers[0], 'page' );
			try {
				numPages = parseInt( numPages, 10 );
			} catch( err ) {
				numPages = 1;
			}
			pager.count = numPages;

			try {
				currentPage = parseInt( currentPage, 10 );
			} catch( err ) {
				currentPage = 0;
			}
			pager.currentPage = currentPage;
		}
		return pager;
	};

	var getAdhocXml = function( xmlResponse ) {
		return xmlResponse.getElementsByTagName( 'com.successmetricsinc.adhoc.AdhocReport' );
	};

	var getAdhocReportXml = function( xmlResponse ) {
		var adhocXml = getAdhocXml( xmlResponse );
		var adhocReportXml =  null;
		_.each( adhocXml, function( el, idx ) {
			var createdBy = el.getAttribute( 'createdBy' );
			if ( !_.isEmpty( createdBy ) ) {
				adhocReportXml = el;
				return adhocReportXml;
			}
			if( idx === adhocXml.length ) {
				adhocReportXml = adhocXml[0];
			}
		} );
		return adhocReportXml;
	};

	Report.getRenderedReport = function( params ) {
		params = angular.extend( {}, wsParams, params );
		return Report.request( 'getDashboardRenderedReport', params, { transformResponse : transformResponse }, wsParamsOrder );
	};

	Report.getPluginReport = function( params ) {
		return Report.request( 'getDashletPluginReportFile', params, { transformResponse : transformPluginResponse }, wsParamsOrder );
	};

	Report.saveFile = function( params ) {};

	Report.readFile = function( params ) {};

	return Report;
} );
