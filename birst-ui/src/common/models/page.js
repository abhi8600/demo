angular.module( 'models.page', [ 'models.pageItem', 'services.genericResource' ] )

.factory( 'Page', function( PageItem, $http, $log, genericResource, Prompt, $timeout ) {
	'use strict';

	var config = {
		url : '/SMIWeb/rest/dashboards/page',
		method : 'POST',
		xsrfHeaderName : 'X-XSRF-TOKEN',
		xsrfCookieName : 'XSRF-TOKEN',
		timeout : 300000
	};

	var defaults = {
		prompts : []
	};

	var PageConstructor = function( options ) {
		angular.extend( this, defaults, options );
	};

	var Page = genericResource( PageConstructor );

	Page.get = function( uuid, scb, ecb ) {
		var reqConf = angular.extend( {}, config, { url : config.url + '/' + uuid, transformResponse : transformResponse, method : 'GET' } );
		return Page.thenFactoryMethod( $http( reqConf, scb, ecb ) );
	};

	var transformResponse = function( data ) {
		try {
			data = JSON.parse( data );
			_.forEach( data.LayoutV2, function( pItem, i, collection ) {
				data.LayoutV2[i] = new PageItem( pItem );
			} );
			if( !_.isArray( data.prompts ) ) {
				// server sends object if only one prompt is saved
				data.prompts = [ data.prompts ];
			}
		} catch( error ) {
			$log.error( 'The server returned an invalid JSON string: ' + data );
		}
		return data;
	};

	angular.extend( Page.prototype, {

		arrangeLayout : function( numOfColumns ) {
			// determine pageItem order based on top/left positioning
			var posOrderPageItems = _.sortBy( this.LayoutV2, ['percentTop', 'percentLeft'] );
			var left = 0;
			var top = 0;
			var height = 49;
			var verticalMargin = 2;
			var n = 0;
			var width, horizontalMargin, w;

			// update properties of pageItems
			switch ( numOfColumns ) {
				case 1:
					width = 100;
					horizontalMargin = 0;
					break;
				case 2:
					width = 49;
					horizontalMargin = 2;
					break;
				case 3:
					width = 32;
					horizontalMargin = 2;
					break;
				case 4:
					width = [ 24, 23, 23, 24 ];
					horizontalMargin = 2;
					break;
			}

			_.each( posOrderPageItems, function( pageItem, index ) {
				if ( index > 0 && index % numOfColumns === 0 ) {
					left = 0;
					top += height + verticalMargin;
				}
				if( typeof width === 'number' ) {
					w = width;
				} else {
					n = n === width.length ? 0 : n;
					w = width[ n ];
					n++;
				}
				pageItem.setProperties( {
					percentLeft : left,
					percentTop : top,
					percentWidth : w,
					percentHeight : height
				} );
				left += w + horizontalMargin;
			} );
		},

		// return stringified object without $$hashKey's
		toCleanJsonString : function() {
			var cleanObj = _.omit( this, 'LayoutV2' );
			cleanObj.LayoutV2 = [];
			_.each( this.LayoutV2, function( pageItem ) {
				cleanObj.LayoutV2.push( pageItem.asPersisted() );
			} );
			return cleanObj;
		},

		post : function() {
			var that = this;
			var reqConf = _.extend( {}, config, { data : this.toCleanJsonString(), transformResponse : transformResponse } );
			return $http( reqConf ).then( function( resp ) {
				angular.extend( that, resp.data );
			} );
		}
	} );

	return Page;

} );
