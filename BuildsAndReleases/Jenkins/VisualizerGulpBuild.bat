echo on
REM
REM  Simple script to build/minimize the Visualizer code with Gulp.  
REM This is really a workaround for Grunt since it completely exits the script that executed it when it is done.
REM Any other commands in the script are bypassed.  
REM
set clean_dir=%1
REM set clean_dir=c:\workspace_git\birst

set PATH=C:\Users\jfong\AppData\Roaming\npm;%PATH%

cd %clean_dir%\Designer2.0

:NPM_UPDATE_DONE

gulp build

goto THE_END:

:BADEXIT
echo ------------------ Bad Exit -------------------------------------------
echo %VarMailText%
blat -to %VarNotify% -subject "%VarMailSubject%" -body "%VarMailText%"
rem *** if exist %VarCustProp% del %VarCustProp%
echo "This batch file failed."
exit 1

:THE_END

