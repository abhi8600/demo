ECHO ON

set VarNotify=jfong@birst.com
set VarMailText=Text message as part of the Mail
set VarMailSubject=Build Acorn
set FTP_files=0
set jfong_share="c:\shared\Builds"

:PROCESS_INPUT

if "%1"=="-n" goto SET_NOTIFICATION:
if "%1"=="-b" goto SET_BRANCH:
if "%1"=="-c" goto SET_CLEAN_DIR:
if "%1"=="-f" goto SET_FLAG:
if "%1"=="-t" goto SET_TIMESTAMP:
if "%1"=="-w" goto SET_WORKSPACE:
if "%1"=="" goto PROCESS_INPUT_DONE:
set temp=%1
if NOT "%temp:~0,1%"=="-" goto ADD_NOTIFICATION:

goto PROCESS_INPUT_ERROR:

:SET_TIMESTAMP
shift
set TimeStamp=%1
goto PROCESS_INPUT_LOOP:

:SET_BRANCH
shift
set Branch_name=%1
goto PROCESS_INPUT_LOOP:

:SET_FLAG
shift
set FTP_files=%1
echo %FTP_files%
goto PROCESS_INPUT_LOOP:

:SET_CLEAN_DIR
shift
set clean_dir=%1
goto PROCESS_INPUT_LOOP:

:SET_NOTIFICATION
shift
set VarNotify=%1
goto PROCESS_INPUT_LOOP:

:ADD_NOTIFICATION
set VarNotify="%VarNotify%,%temp%"
echo %VarNotify%
goto PROCESS_INPUT_LOOP:

:SET_WORKSPACE
shift
set Git_workspace=%1
goto PROCESS_INPUT_LOOP:

:PROCESS_INPUT_LOOP

shift
goto PROCESS_INPUT:

:PROCESS_INPUT_ERROR

echo ERROR: invalid input parameter
echo usage: DataConductorBuild.bat [-options]
echo where options include:
echo.
echo  -t ^<Time Stamp^> (optional)
echo 		Enter the Time Stamp for identifying the deployment
echo		the format is YYYYMMDDHHMM, or one will be generated.		
echo  -b ^<CVS Branch^> 
echo 		enter the cvs branch for the release e.g. HEAD, Birst_3_1_5_SP
echo  -n ^<email notification list^> comma separated (optional)
echo  		default=jfong@birst.com,4157301327@vtext.com
echo 		NOTE - multiple email addresses need to be separated by 
echo 			commas and enclosed in double quote
echo.

set VarMailText=Invalid arguement(s) provided. 
set VarMailSubject=DataConductorBuild:Process Arguements
goto BADEXIT:

:PROCESS_INPUT_DONE

REM
REM We need a timestamp.  If it is not provided, we should create our own.
REM

rem	cd %clean_dir%

rem	echo -%clean_dir%-
rem	for /l %%a in (1,1,31) do if "%clean_dir:~-1%"==" " set clean_dir=%clean_dir:~0,-1%
echo -%clean_dir%-
cd %clean_dir%
dir

REM
REM We need a timestamp.  If it is not provided, we should create our own.
REM

if "%TimeStamp%" neq "" ( goto TIMESTAMP_DEFINED: ) 

for /f "tokens=1-3 delims=:." %%a in ("%time%") do set hh_mm_stamp=%%a%%b
echo %hh_mm_stamp%
for /f "tokens=1-3 delims=\ " %%a in ("%date%") do set date_only=%%b
for /f "tokens=1-3 delims=/." %%a in ("%date_only%") do set teststamp=%%c%%a%%b
echo %teststamp%
set TimeStamp="%teststamp%%hh_mm_stamp%"
	
:TIMESTAMP_DEFINED
echo TimeStamp: %TimeStamp%

echo -%TimeStamp%-
for /l %%a in (1,1,31) do if "%TimeStamp:~-1%"==" " set TimeStamp=%TimeStamp:~0,-1%
echo -%TimeStamp%-

REM set TimeStamp=%TimeStamp:~0,14%
REM echo TimeStamp: -%TimeStamp%-

REM
REM Build Acorn
REM

cd %clean_dir%
dir

:BUILD_ACORN

cd %clean_dir%\Acorn
dir

cd %clean_dir%\Acorn\Acorn
if exist Acorn.csproj goto IIS_SED: 
set VarMailText=Something is wrong, I can't find the Acorn.csproj file. \n The CVS update or something else did not work
set VarMailSubject=Acorn_build: CVS Acorn.csproj file error
goto BADEXIT:

:IIS_SED
echo "Acorn.csproj exist"
copy Acorn.csproj Acorn.temp
type Acorn.temp | c:\cygwin\bin\sed.exe "s^<UseIIS>True</UseIIS>^<UseIIS>False</UseIIS>^" >Acorn.csproj
del Acorn.temp		# we've update the file, we can delete the older version.

cd %clean_dir%\Acorn

rem devenv acorn.sln /clean
IF %ERRORLEVEL% LSS 1 goto CLEAN_DONE:
set VarMailText=Build had problems cleaing out build area. 
set VarMailSubject=Acorn_build:clean errors
goto BADEXIT:

:CLEAN_DONE
devenv acorn.sln /build 
IF %ERRORLEVEL% LSS 1 goto BUILD_DONE:
set VarMailText=Build encountered problems.
set VarMailSubject=Acorn_build:build problems. 
goto BADEXIT:

:BUILD_DONE

rem 
rem create the deploy package
rem

rem	mkdir "C:\shared\Builds\%TimeStamp%"
rem	IF %ERRORLEVEL% LSS 1 goto DO_ZIP:
rem	set VarMailText=Could not build the "C:\shared\Builds\%TimeStamp%" directory. 
rem	set VarMailSubject=Acorn_build:mkdir zip directory
rem	goto BADEXIT:

:DO_ZIP
REM
REM We don't truely support the other languages besides English.
REM However, the code is internationalized so we have localized
REM spanish files.  ZIP them up and put them in the Acorn\swf 
REM directory to make the language accessible.
REM
cd %clean_dir%\SourceAdmin\build\swf
"c:\Program Files (x86)\WinZip\WZZIP.EXE" -ex -r -p -a C:\shared\Builds\%TimeStamp%\I18N.zip es_*
if %ERRORLEVEL% GEQ 1 ( if %ERRORLEVEL% NEQ 12 ( echo "Error attempting to zip up the I18N files" ))
cd %clean_dir%\FlexAdhoc\build\swf
"c:\Program Files (x86)\WinZip\WZZIP.EXE" -ex -r -p -a C:\shared\Builds\%TimeStamp%\I18N.zip es_*
if %ERRORLEVEL% GEQ 1 ( if %ERRORLEVEL% NEQ 12 ( echo "Error attempting to zip up the I18N files" ))

REM Delete the Spanish translation files since they are not approved and may cause weird UI issues
REM We can delete this section when have real translation files checked in.
REM
del /Q /S %clean_dir%\Acorn\Acorn\App_GlobalResources\HeaderLabels.es.*
del /Q /S %clean_dir%\Acorn\Acorn\swf\es_*

rem
rem Package the DataConductor 
rem 

copy /V /Y %clean_dir%\DataConductor\DataConductor\BirstConnect.zip C:\shared\Builds\%TimeStamp%
IF %ERRORLEVEL% LSS 1 goto COPY_BC_TO_DOWNLOADS:
set VarMailText=Could not copy BirstConnect.zip to the "C:\shared\Builds\%TimeStamp%" directory. 
set VarMailSubject=Acorn_build:copy BC zip error
goto BADEXIT:

REM
REM Copy to the download directory too
REM
:COPY_BC_TO_DOWNLOADS
copy /V /Y %clean_dir%\DataConductor\DataConductor\BirstConnect.zip %clean_dir%\Acorn\Acorn\downloads
IF %ERRORLEVEL% LSS 1 goto COPY_MAPS:
set VarMailText=Could not copy BirstConnect.zip to the "C:\shared\Builds\%TimeStamp%" directory. 
set VarMailSubject=Acorn_build:copy BC zip error
goto BADEXIT:

:COPY_MAPS
REM
REM	To save confusion when we deploy, Copy the Map files to the swf subdirectory
REM	Saves a step in the deployment script.
REM
cd %clean_dir%\Acorn\Acorn
IF NOT EXIST swf\maps goto NO_MAP_DIR:
cd swf
rmdir /Q /S maps
cd %clean_dir%\Acorn\Acorn

:NO_MAP_DIR
xcopy maps swf\maps /I /E
IF %ERRORLEVEL% LSS 1 goto MAPS_COPIED:
set VarMailText=Could not copy the map files to the swf directory. 
set VarMailSubject=Acorn_build:map copy error
goto BADEXIT:

:MAPS_COPIED
rem
rem	Count the number for files copied and compare the count with the 
rem	original tree, they should be equal.
rem
setlocal  enableextensions
set /a count1=0
set /a count2=0
for /r %clean_dir%\Acorn\Acorn\maps %%x in (*) do ( set /a count1+=1 )
for /r %clean_dir%\Acorn\Acorn\swf\maps %%x in (*) do ( set /a count2+=1 )
echo %count1% %count2%
endlocal & set /a count_diff=%count1%-%count2%

IF %count_diff% EQU 0 goto ALL_MAPS_COPIED:
set VarMailText=Copy of Map files incomplete %count_diff%, +0 missing destination files, -0 missing original files. 
set VarMailSubject=Acorn_build:Map copy incomplete
goto BADEXIT:

:ALL_MAPS_COPIED


REM
REM	Grab the birst-ui build from the new Linux server
REM

cd %clean_dir%\birst-ui

dir %clean_dir%\birst-ui\temp
if %ERRORLEVEL% EQU 0 ( del /F /Q %clean_dir%\birst-ui\temp\* 
	) else (
	mkdir %clean_dir%\birst-ui\temp )
	
cd %clean_dir%\birst-ui\temp
IF %ERRORLEVEL% LSS 1 goto BIRSTUI_TEMP_CREATED:
set VarMailText=Creation of the birst-ui temp directory did not work.
set VarMailSubject=Acorn_build:birst-ui create temp error 
goto BADEXIT:

:BIRSTUI_TEMP_CREATED

c:\cygwin\bin\wget.exe  --auth-no-challenge  http://jenkins:8080/job/Build_JS/lastSuccessfulBuild/artifact/*zip*/archive.zip
IF %ERRORLEVEL% LSS 1 goto BIRSTUI_WGET_WORKED:
set VarMailText=wget of the birst-ui archive from Jenkins did not work.
set VarMailSubject=Acorn_build:birst-ui wget error 
goto BADEXIT:

:BIRSTUI_WGET_WORKED
"c:\Program Files (x86)\WinZip\WZUNZIP.EXE" -d -o archive.zip .
IF %ERRORLEVEL% LSS 1 goto BIRSTUI_UNZIP_WORKED:
set VarMailText=Unzipping of the birst-ui Archive did not work.
set VarMailSubject=Acorn_build:birst-ui unzip error 
goto BADEXIT:

:BIRSTUI_UNZIP_WORKED
cd %clean_dir%\birst-ui\temp\archive\birst-ui\bin
IF %ERRORLEVEL% LSS 1 goto BIRSTUI_BIN_FOUND:
set VarMailText=Archive of the birst-ui is missing the bin directory.
set VarMailSubject=Acorn_build:birst-ui bin directory error 
goto BADEXIT:

:BIRSTUI_BIN_FOUND
xcopy * %clean_dir%\Acorn\Acorn\Visualizer /I /E /Y
IF %ERRORLEVEL% LSS 1 goto DASHBOARD_COPIED:
set VarMailText=Could not copy the birst-ui release files to the birst-ui directory. 
set VarMailSubject=Acorn_build:birst-ui copy error 
goto BADEXIT:

:DASHBOARD_COPIED

rem	
rem Package Acorn.zip
rem

set /a error_cnt=0
cd %clean_dir%\Acorn\Acorn

"c:\Program Files (x86)\WinZip\WZZIP.EXE" -ex -r -p -a C:\shared\Builds\%TimeStamp%\Acorn.zip *.aspx
if %ERRORLEVEL% GEQ 1 ( if %ERRORLEVEL% NEQ 12 ( goto WinZip_ERROR: ))
"c:\Program Files (x86)\WinZip\WZZIP.EXE" -ex -r -p -a C:\shared\Builds\%TimeStamp%\Acorn.zip *.ascx
if %ERRORLEVEL% GEQ 1 ( if %ERRORLEVEL% NEQ 12 ( goto WinZip_ERROR: ))
"c:\Program Files (x86)\WinZip\WZZIP.EXE" -ex -r -p -a C:\shared\Builds\%TimeStamp%\Acorn.zip *.asmx
if %ERRORLEVEL% GEQ 1 ( if %ERRORLEVEL% NEQ 12 ( goto WinZip_ERROR: ))
"c:\Program Files (x86)\WinZip\WZZIP.EXE" -ex -r -p -a C:\shared\Builds\%TimeStamp%\Acorn.zip *.smi
if %ERRORLEVEL% GEQ 1 ( if %ERRORLEVEL% NEQ 12 ( goto WinZip_ERROR: ))
"c:\Program Files (x86)\WinZip\WZZIP.EXE" -ex -r -p -a -x@..\..\BuildsAndReleases\builds\ExcludeFiles.txt C:\shared\Builds\%TimeStamp%\Acorn.zip *.xml
if %ERRORLEVEL% GEQ 1 ( if %ERRORLEVEL% NEQ 12 ( goto WinZip_ERROR: ))
"c:\Program Files (x86)\WinZip\WZZIP.EXE" -ex -r -p -a C:\shared\Builds\%TimeStamp%\Acorn.zip *.htm
if %ERRORLEVEL% GEQ 1 ( if %ERRORLEVEL% NEQ 12 ( goto WinZip_ERROR: ))
"c:\Program Files (x86)\WinZip\WZZIP.EXE" -ex -r -p -a C:\shared\Builds\%TimeStamp%\Acorn.zip *.html
if %ERRORLEVEL% GEQ 1 ( if %ERRORLEVEL% NEQ 12 ( goto WinZip_ERROR: ))
"c:\Program Files (x86)\WinZip\WZZIP.EXE" -ex -r -p -a C:\shared\Builds\%TimeStamp%\Acorn.zip *.config
if %ERRORLEVEL% GEQ 1 ( if %ERRORLEVEL% NEQ 12 ( goto WinZip_ERROR: ))
"c:\Program Files (x86)\WinZip\WZZIP.EXE" -ex -r -p -a C:\shared\Builds\%TimeStamp%\Acorn.zip *.style
if %ERRORLEVEL% GEQ 1 ( if %ERRORLEVEL% NEQ 12 ( goto WinZip_ERROR: ))
"c:\Program Files (x86)\WinZip\WZZIP.EXE" -ex -r -p -a C:\shared\Builds\%TimeStamp%\Acorn.zip *.AdhocReport
if %ERRORLEVEL% GEQ 1 ( if %ERRORLEVEL% NEQ 12 ( goto WinZip_ERROR: ))
"c:\Program Files (x86)\WinZip\WZZIP.EXE" -ex -r -p -a C:\shared\Builds\%TimeStamp%\Acorn.zip *.css
if %ERRORLEVEL% GEQ 1 ( if %ERRORLEVEL% NEQ 12 ( goto WinZip_ERROR: ))
"c:\Program Files (x86)\WinZip\WZZIP.EXE" -ex -r -p -a C:\shared\Builds\%TimeStamp%\Acorn.zip *.Master
if %ERRORLEVEL% GEQ 1 ( if %ERRORLEVEL% NEQ 12 ( goto WinZip_ERROR: ))
"c:\Program Files (x86)\WinZip\WZZIP.EXE" -ex -r -p -a C:\shared\Builds\%TimeStamp%\Acorn.zip *.ashx
if %ERRORLEVEL% GEQ 1 ( if %ERRORLEVEL% NEQ 12 ( goto WinZip_ERROR: ))
"c:\Program Files (x86)\WinZip\WZZIP.EXE" -ex -r -p -a C:\shared\Builds\%TimeStamp%\Acorn.zip *.asax
if %ERRORLEVEL% GEQ 1 ( if %ERRORLEVEL% NEQ 12 ( goto WinZip_ERROR: ))
"c:\Program Files (x86)\WinZip\WZZIP.EXE" -ex -r -p -a C:\shared\Builds\%TimeStamp%\Acorn.zip *.js
if %ERRORLEVEL% GEQ 1 ( if %ERRORLEVEL% NEQ 12 ( goto WinZip_ERROR: ))
"c:\Program Files (x86)\WinZip\WZZIP.EXE" -ex -r -p -a C:\shared\Builds\%TimeStamp%\Acorn.zip *.jar
if %ERRORLEVEL% GEQ 1 ( if %ERRORLEVEL% NEQ 12 ( goto WinZip_ERROR: ))
"c:\Program Files (x86)\WinZip\WZZIP.EXE" -ex -r -P -a C:\shared\Builds\%TimeStamp%\Acorn.zip *.jnlp
if %ERRORLEVEL% GEQ 1 ( if %ERRORLEVEL% NEQ 12 ( goto WinZip_ERROR: ))
"c:\Program Files (x86)\WinZip\WZZIP.EXE" -ex -r -P -a C:\shared\Builds\%TimeStamp%\Acorn.zip *.swf
if %ERRORLEVEL% GEQ 1 ( if %ERRORLEVEL% NEQ 12 ( goto WinZip_ERROR: ))
"c:\Program Files (x86)\WinZip\WZZIP.EXE" -ex -r -P -a C:\shared\Builds\%TimeStamp%\Acorn.zip *.swz
if %ERRORLEVEL% GEQ 1 ( if %ERRORLEVEL% NEQ 12 ( goto WinZip_ERROR: ))
rem	"c:\Program Files (x86)\WinZip\WZZIP.EXE" -ex -r -P -a C:\shared\Builds\%TimeStamp%\Acorn.zip *.xml
rem	if %ERRORLEVEL% GEQ 1 ( if %ERRORLEVEL% NEQ 12 ( goto WinZip_ERROR: ))
"c:\Program Files (x86)\WinZip\WZZIP.EXE" -ex -r -P -a C:\shared\Builds\%TimeStamp%\Acorn.zip *.txt
if %ERRORLEVEL% GEQ 1 ( if %ERRORLEVEL% NEQ 12 ( goto WinZip_ERROR: ))
"c:\Program Files (x86)\WinZip\WZZIP.EXE" -ex -r -P -a C:\shared\Builds\%TimeStamp%\Acorn.zip bin
if %ERRORLEVEL% GEQ 1 ( if %ERRORLEVEL% NEQ 12 ( goto WinZip_ERROR: ))
"c:\Program Files (x86)\WinZip\WZZIP.EXE" -ex -r -P -a C:\shared\Builds\%TimeStamp%\Acorn.zip images
if %ERRORLEVEL% GEQ 1 ( if %ERRORLEVEL% NEQ 12 ( goto WinZip_ERROR: ))
"c:\Program Files (x86)\WinZip\WZZIP.EXE" -ex -r -P -a C:\shared\Builds\%TimeStamp%\Acorn.zip lib
if %ERRORLEVEL% GEQ 1 ( if %ERRORLEVEL% NEQ 12 ( goto WinZip_ERROR: ))
"c:\Program Files (x86)\WinZip\WZZIP.EXE" -ex -r -P -a C:\shared\Builds\%TimeStamp%\Acorn.zip common
if %ERRORLEVEL% GEQ 1 ( if %ERRORLEVEL% NEQ 12 ( goto WinZip_ERROR: ))
"c:\Program Files (x86)\WinZip\WZZIP.EXE" -ex -r -P -a C:\shared\Builds\%TimeStamp%\Acorn.zip history
if %ERRORLEVEL% GEQ 1 ( if %ERRORLEVEL% NEQ 12 ( goto WinZip_ERROR: ))
"c:\Program Files (x86)\WinZip\WZZIP.EXE" -ex -r -P -a -x@..\..\BuildsAndReleases\builds\ExcludeFiles.txt C:\shared\Builds\%TimeStamp%\Acorn.zip App_Data
if %ERRORLEVEL% GEQ 1 ( if %ERRORLEVEL% NEQ 12 ( goto WinZip_ERROR: ))
"c:\Program Files (x86)\WinZip\WZZIP.EXE" -ex -r -P -a C:\shared\Builds\%TimeStamp%\Acorn.zip Training
if %ERRORLEVEL% GEQ 1 ( if %ERRORLEVEL% NEQ 12 ( goto WinZip_ERROR: ))
"c:\Program Files (x86)\WinZip\WZZIP.EXE" -ex -r -P -a C:\shared\Builds\%TimeStamp%\Acorn.zip maps
if %ERRORLEVEL% GEQ 1 ( if %ERRORLEVEL% NEQ 12 ( goto WinZip_ERROR: ))
"c:\Program Files (x86)\WinZip\WZZIP.EXE" -ex -r -P -a C:\shared\Builds\%TimeStamp%\Acorn.zip *.kml
if %ERRORLEVEL% GEQ 1 ( if %ERRORLEVEL% NEQ 12 ( goto WinZip_ERROR: ))
"c:\Program Files (x86)\WinZip\WZZIP.EXE" -ex -r -P -a C:\shared\Builds\%TimeStamp%\Acorn.zip App_GlobalResources
if %ERRORLEVEL% GEQ 1 ( if %ERRORLEVEL% NEQ 12 ( goto WinZip_ERROR: ))
REM	"c:\Program Files (x86)\WinZip\WZZIP.EXE" -ex -r -P -a C:\shared\Builds\%TimeStamp%\Acorn.zip Help
REM	if %ERRORLEVEL% GEQ 1 ( if %ERRORLEVEL% NEQ 12 ( goto WinZip_ERROR: ))
"c:\Program Files (x86)\WinZip\WZZIP.EXE" -ex -r -P -a C:\shared\Builds\%TimeStamp%\Acorn.zip *.properties
if %ERRORLEVEL% GEQ 1 ( if %ERRORLEVEL% NEQ 12 ( goto WinZip_ERROR: ))
"c:\Program Files (x86)\WinZip\WZZIP.EXE" -ex -r -P -a C:\shared\Builds\%TimeStamp%\Acorn.zip swf\maps
if %ERRORLEVEL% GEQ 1 ( if %ERRORLEVEL% NEQ 12 ( goto WinZip_ERROR: ))
"c:\Program Files (x86)\WinZip\WZZIP.EXE" -ex -r -P -a C:\shared\Builds\%TimeStamp%\Acorn.zip BirstConnect.zip
if %ERRORLEVEL% GEQ 1 ( if %ERRORLEVEL% NEQ 12 ( goto WinZip_ERROR: ))
"c:\Program Files (x86)\WinZip\WZZIP.EXE" -ex -r -P -a C:\shared\Builds\%TimeStamp%\Acorn.zip Visualizer
if %ERRORLEVEL% GEQ 1 ( if %ERRORLEVEL% NEQ 12 ( goto WinZip_ERROR: ))
"c:\Program Files (x86)\WinZip\WZZIP.EXE" -ex -r -P -a C:\shared\Builds\%TimeStamp%\Acorn.zip Dashboards2.0
if %ERRORLEVEL% GEQ 1 ( if %ERRORLEVEL% NEQ 12 ( goto WinZip_ERROR: ))
"c:\Program Files (x86)\WinZip\WZZIP.EXE" -ex -r -P -a C:\shared\Builds\%TimeStamp%\Acorn.zip *.key
if %ERRORLEVEL% GEQ 1 ( if %ERRORLEVEL% NEQ 12 ( goto WinZip_ERROR: ))
"c:\Program Files (x86)\WinZip\WZZIP.EXE" -ex -r -P -a C:\shared\Builds\%TimeStamp%\Acorn.zip *.cert
if %ERRORLEVEL% GEQ 1 ( if %ERRORLEVEL% NEQ 12 ( goto WinZip_ERROR: ))
"c:\Program Files (x86)\WinZip\WZZIP.EXE" -ex -r -P -a C:\shared\Builds\%TimeStamp%\Acorn.zip *.pfx
if %ERRORLEVEL% GEQ 1 ( if %ERRORLEVEL% NEQ 12 ( goto WinZip_ERROR: ))

goto PACKAGE_DONE:

:WinZip_Error
set VarMailText=There were problems Zipping the files into a zip file. 
set VarMailSubject=Acorn_build:WinZip error
goto BADEXIT:

:PACKAGE_DONE

:BUILD_ACORNTEST_ACORN

REM
REM Build the AcornTestConsole
REM

cd %clean_dir%\AcornTestConsole
IF %ERRORLEVEL% LSS 1 goto START_ACORNTEST_BUILD:

rem	IF not Branch_name == "HEAD"  goto BUILD_ACORNTEST_DONE:
rem	set VarMailText=There were problems with AcornTestConsole, no directory. 
rem	set VarMailSubject=Acorn_build:AcornTestConsole
rem	goto BADEXIT:

:START_ACORNTEST_BUILD
dir

devenv AcornTestConsole.sln /clean
IF %ERRORLEVEL% LSS 1 goto CLEAN_ACORNTEST_DONE:
set VarMailText=Build had problems cleaing out build area. 
set VarMailSubject=AcornTestConsole_build:clean errors
goto BADEXIT:

:CLEAN_ACORNTEST_DONE
devenv AcornTestConsole.sln /build
IF %ERRORLEVEL% LSS 1 goto BUILD_ACORNTEST_DONE:
set VarMailText=AcornTestConsole Build encountered problems.
set VarMailSubject=AcornTestConsole_build:build problems. 
goto BADEXIT:

:BUILD_ACORNTEST_DONE

cd %clean_dir%\AcornTestConsole\AcornTestConsole\bin\Debug

"c:\Program Files (x86)\WinZip\WZZIP.EXE" -ex -r -P -a C:\shared\Builds\%TimeStamp%\AcornTestConsole.zip .\AcornTestConsole.exe
if %ERRORLEVEL% GEQ 1 ( if %ERRORLEVEL% NEQ 12 ( goto WinZip_ERROR: ))
"c:\Program Files (x86)\WinZip\WZZIP.EXE" -ex -r -P -a C:\shared\Builds\%TimeStamp%\AcornTestConsole.zip .\AcornTestConsole.pdb
if %ERRORLEVEL% GEQ 1 ( if %ERRORLEVEL% NEQ 12 ( goto WinZip_ERROR: ))
"c:\Program Files (x86)\WinZip\WZZIP.EXE" -ex -r -P -a C:\shared\Builds\%TimeStamp%\AcornTestConsole.zip .\TestApiCore.dll
if %ERRORLEVEL% GEQ 1 ( if %ERRORLEVEL% NEQ 12 ( goto WinZip_ERROR: ))
"c:\Program Files (x86)\WinZip\WZZIP.EXE" -ex -r -P -a C:\shared\Builds\%TimeStamp%\AcornTestConsole.zip .\TestApiCore.pdb
if %ERRORLEVEL% GEQ 1 ( if %ERRORLEVEL% NEQ 12 ( goto WinZip_ERROR: ))
"c:\Program Files (x86)\WinZip\WZZIP.EXE" -ex -r -P -a C:\shared\Builds\%TimeStamp%\AcornTestConsole.zip .\log4net.dll
if %ERRORLEVEL% GEQ 1 ( if %ERRORLEVEL% NEQ 12 ( goto WinZip_ERROR: ))


cd %clean_dir%\AcornTestConsole\AcornTestConsole

"c:\Program Files (x86)\WinZip\WZZIP.EXE" -ex -r -P -a C:\shared\Builds\%TimeStamp%\AcornTestConsole.zip .\AcornTestConsole.exe.config
if %ERRORLEVEL% GEQ 1 ( if %ERRORLEVEL% NEQ 12 ( goto WinZip_ERROR: ))
"c:\Program Files (x86)\WinZip\WZZIP.EXE" -ex -r -P -a C:\shared\Builds\%TimeStamp%\AcornTestConsole.zip .\runAcornTestConsole.bat
if %ERRORLEVEL% GEQ 1 ( if %ERRORLEVEL% NEQ 12 ( goto WinZip_ERROR: ))
"c:\Program Files (x86)\WinZip\WZZIP.EXE" -ex -r -P -a C:\shared\Builds\%TimeStamp%\AcornTestConsole.zip .\dcconfig_template.xml
if %ERRORLEVEL% GEQ 1 ( if %ERRORLEVEL% NEQ 12 ( goto WinZip_ERROR: ))
"c:\Program Files (x86)\WinZip\WZZIP.EXE" -ex -r -P -a C:\shared\Builds\%TimeStamp%\AcornTestConsole.zip .\log4j.xml
if %ERRORLEVEL% GEQ 1 ( if %ERRORLEVEL% NEQ 12 ( goto WinZip_ERROR: ))
"c:\Program Files (x86)\WinZip\WZZIP.EXE" -ex -r -P -a C:\shared\Builds\%TimeStamp%\AcornTestConsole.zip .\log4net.config
if %ERRORLEVEL% GEQ 1 ( if %ERRORLEVEL% NEQ 12 ( goto WinZip_ERROR: ))

cd %clean_dir%\Acorn\Acorn\bin

"c:\Program Files (x86)\WinZip\WZZIP.EXE" -ex -r -P -a C:\shared\Builds\%TimeStamp%\AcornTestConsole.zip .\Acorn.dll
if %ERRORLEVEL% GEQ 1 ( if %ERRORLEVEL% NEQ 12 ( goto WinZip_ERROR: ))
"c:\Program Files (x86)\WinZip\WZZIP.EXE" -ex -r -P -a C:\shared\Builds\%TimeStamp%\AcornTestConsole.zip .\Acorn.pdb
if %ERRORLEVEL% GEQ 1 ( if %ERRORLEVEL% NEQ 12 ( goto WinZip_ERROR: ))
"c:\Program Files (x86)\WinZip\WZZIP.EXE" -ex -r -P -a C:\shared\Builds\%TimeStamp%\AcornTestConsole.zip .\Antlr3.Runtime.dll
if %ERRORLEVEL% GEQ 1 ( if %ERRORLEVEL% NEQ 12 ( goto WinZip_ERROR: ))
"c:\Program Files (x86)\WinZip\WZZIP.EXE" -ex -r -P -a C:\shared\Builds\%TimeStamp%\AcornTestConsole.zip .\ICSharpCode.SharpZipLib.dll
if %ERRORLEVEL% GEQ 1 ( if %ERRORLEVEL% NEQ 12 ( goto WinZip_ERROR: ))
"c:\Program Files (x86)\WinZip\WZZIP.EXE" -ex -r -P -a C:\shared\Builds\%TimeStamp%\AcornTestConsole.zip .\Newtonsoft.Json.dll
if %ERRORLEVEL% GEQ 1 ( if %ERRORLEVEL% NEQ 12 ( goto WinZip_ERROR: ))
"c:\Program Files (x86)\WinZip\WZZIP.EXE" -ex -r -P -a C:\shared\Builds\%TimeStamp%\AcornTestConsole.zip .\"Birst Administration.exe"
if %ERRORLEVEL% GEQ 1 ( if %ERRORLEVEL% NEQ 12 ( goto WinZip_ERROR: ))
"c:\Program Files (x86)\WinZip\WZZIP.EXE" -ex -r -P -a C:\shared\Builds\%TimeStamp%\AcornTestConsole.zip .\"Birst Administration.pdb"
if %ERRORLEVEL% GEQ 1 ( if %ERRORLEVEL% NEQ 12 ( goto WinZip_ERROR: ))
"c:\Program Files (x86)\WinZip\WZZIP.EXE" -ex -r -P -a C:\shared\Builds\%TimeStamp%\AcornTestConsole.zip .\LogicalExpression.dll
if %ERRORLEVEL% GEQ 1 ( if %ERRORLEVEL% NEQ 12 ( goto WinZip_ERROR: ))
"c:\Program Files (x86)\WinZip\WZZIP.EXE" -ex -r -P -a C:\shared\Builds\%TimeStamp%\AcornTestConsole.zip .\LogicalExpression.pdb
if %ERRORLEVEL% GEQ 1 ( if %ERRORLEVEL% NEQ 12 ( goto WinZip_ERROR: ))

cd %clean_dir%\ATCPreReqValidator\deployable_units

"c:\Program Files (x86)\WinZip\WZZIP.EXE" -ex -r -P -a C:\shared\Builds\%TimeStamp%\AcornTestConsole.zip .\*
if %ERRORLEVEL% GEQ 1 ( if %ERRORLEVEL% NEQ 12 ( goto WinZip_ERROR: ))

cd %clean_dir%\SpacesCleanUpUtility\deployable_units

"c:\Program Files (x86)\WinZip\WZZIP.EXE" -ex -r -P -a C:\shared\Builds\%TimeStamp%\AcornTestConsole.zip .\*
if %ERRORLEVEL% GEQ 1 ( if %ERRORLEVEL% NEQ 12 ( goto WinZip_ERROR: ))

set VarMailText=Acorn Build was successful. 
set VarMailSubject=Acorn_build:Done
blat -to %VarNotify% -subject "%VarMailSubject%" -body "%VarMailText%"

goto THE_END:


:BADEXIT
echo ------------------ Bad Exit -------------------------------------------
echo %VarMailText%
blat -to %VarNotify% -subject "%VarMailSubject%" -body "%VarMailText%"
rem *** if exist %VarCustProp% del %VarCustProp%
echo "This batch file failed."
exit 1

:THE_END

exit 0
