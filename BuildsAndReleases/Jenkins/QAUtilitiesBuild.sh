﻿#!/usr/bin/bash
#
ANT_OPTS="-Xms512m -Xmx512m -XX:MaxPermSize=512m";	export ANT_OPTS

function Parameter_Help
{
echo "usage: QAUtilitesBuild [-options]"
			echo "where options include:"
			echo " -c <clean working directory>"
			echo "		This is the clean area we will be exporting "
			echo "		and building from.  Everything will be deleted "
			echo "		from this directory tree before we start"
			echo "		e.g. \c\clean_build_SMI_2_2_0_SP"
			echo " -n <email notification list> comma separated"
			echo " 		default=jfong@birst.com,4157301327@vtext.com"
			echo " -t <build tag> "
			echo " 		Usually, this is the date and time  with or without a "
			echo " 		release number.  For example, 200802140805 "
			echo " 		is Feb 14th, 2008 8:05 a.m.  Or if it for 2.2.0, "
			echo "		the tag would be 200802140805_220"
}
# Define variables
# 	and clear them out first. 
#
jfong_share=/cygdrive/c/shared/Builds;				export jfong_share
jfong_clean="";								export jfong_clean
Notify="jfong@birst.com"; 		export Notify		#email list
declare -i error_count=0
build_tag=""
Branch_Name=""
CVS_workspace=""
#JAVA_HOME=`cygpath -u "C:\Program Files (x86)\Java\jre6"`
JAVA_HOME=`cygpath -u "C:\Program Files\Java\jdk1.7.0"`
echo $JAVA_HOME

while getopts ":c:n:t:" opts; do
        case "$opts" in
		c ) echo "-c $OPTARG"
			jfong_clean="$(cygpath -u -p -a "$OPTARG")"
			echo $jfong_clean
			;;
		n ) echo "-n $OPTARG" 
			Notify=$OPTARG
			export Notify 
			;;
		t ) echo "-t $OPTARG"			# Since I'm creating the build, I am generating the build tag.  
			build_tag=$OPTARG;	
			export build_tag
			;;
		*  ) Parameter_Help 
			exit 1 ;;
	esac
	echo one value $1
done

echo "workspace = " $jfong_clean

if [ -z $build_tag ] || [ "$jfong_clean" == "" ];	#  Make sure we have our parameters
then
	Parameter_Help
	exit 1
fi
	
cd "$jfong_clean"/ATCPreReqValidator
pwd

ant clean
error_count=$?
echo ATCPreReqValidator clean exit status $error_count

if [ $error_count == 0 ]; 
then
	echo "ATCPreReqValidator ant clean worked"
else
	blat -subject "QAUtilitiesBuild: ant clean" -body "ATCPreReqValidator ant clean had errors" -to $Notify
	exit -1
fi


ant 
error_count=$?
echo ATCPreReqValidator build exit status $error_count

if [ $error_count == 0 ]; 
then
	echo "ATCPreReqValidator ant all worked"
else
	blat -subject "QAUtilitiesBuild: ant all" -body "ATCPreReqValidator ant all had errors" -to $Notify
	exit -1
fi
error_count=0

#
# ATCPreReqValidator is built, have AcornBuild.bat will zip up the deployable_units into AcornTestConsole.zip file 
#

#
# Build the SpacesCleanUpUtility
#

cd "$jfong_clean"/SpacesCleanUpUtility
pwd

ant clean
error_count=$?
echo SpacesCleanUpUtility clean exit status $error_count

if [ $error_count == 0 ]; 
then
	echo "SpacesCleanUpUtility ant clean worked"
else
	blat -subject "QAUtilitiesBuild: ant clean" -body "SpacesCleanUpUtility ant clean had errors" -to $Notify
	exit -1
fi


ant 
error_count=$?
echo SpacesCleanUpUtility build exit status $error_count

if [ $error_count == 0 ]; 
then
	echo "SpacesCleanUpUtility ant all worked"
else
	blat -subject "QAUtilitiesBuild: ant all" -body "SpacesCleanUpUtility ant all had errors" -to $Notify
	exit -1
fi
error_count=0

#
# SpacesCleanUpUtility is built, have AcornBuild.bat will zip up the deployable_units into AcornTestConsole.zip file 
#
exit 0
