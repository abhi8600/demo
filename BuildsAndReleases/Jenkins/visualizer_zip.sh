﻿#!/usr/bin/bash

whoami
timestamp=""
echo "RunningTime Start zip `date`"

function Parameter_Help
{
echo "usage: visualizer_zip [-options]"
			echo "where options include:"
			echo " -n <email notification list> comma separated"
			echo " 		default=jfong@birst.com,4157301327@vtext.com"
			echo " -t <build tag> "
			echo " 		Usually, this is the date and time  with or without a "
			echo " 		release number.  For example, 200802140805 "
			echo " 		is Feb 14th, 2008 8:05 a.m.  Or if it for 2.2.0, "
			echo "		the tag would be 200802140805_220"
}

while getopts ":c:n:t:" opts; do
        case "$opts" in
		c ) echo "-c $OPTARG"
			jfong_clean="$(cygpath -u -p -a "$OPTARG")"
			echo $jfong_clean
			;;
		n ) echo "-n $OPTARG" 
			Notify=$OPTARG
			export Notify 
			;;
		t ) echo "-t $OPTARG"			# Since I'm creating the build, I am generating the build tag.  
			timestamp="$OPTARG"_visualizer;	
			export build_tag
			;;
		*  ) Parameter_Help 
			exit 1 ;;
	esac
	echo one value $1
done

 
echo "RunningTime Start Build `date`"
setlocal 
echo on 
alias autoexec=autoexec.bat
if [ "$timestamp" == "" ]; 
then 
	timestamp=`/usr/bin/date +%Y%m%d%H%M_visualizer`;
	#	export timestamp
fi 

#	timestamp=`date +%Y%m%d%H%M`;			export timestamp
echo $timestamp

env

Notify_list="jfong@birst.com"
Notify_wrapper_list="jfong@birst.com"
#	export Notify_list
#	export Notify_wrapper_list
echo $Notify_list

Current_Build_Number="$(cygpath -u -p -a '\\repo_dev\Dev\Builds\Current_Build_Number')"

jfong_share=/cygdrive/c/shared/Builds;
jfong_win_share=c:\\shared\\Builds;
WinZip_Home="$(cygpath -u -p -a 'C:\Program Files (x86)\WinZip')"
Builds_Dir="$(cygpath -u '\\repo_dev\dev\Builds')"

which blat
blat -profile 
ping 192.168.85.11

#	blat -superdebugT -s "Wrapper_Birst_$Working_Branch : Daily build start" -body "Started building the $timestamp build" -to "$Notify_wrapper_list"

echo $jfong_clean
mkdir $jfong_share/$timestamp

# This moves the minized files to replace the development ones.
# This is a tempory change in order to test min javascript until
# the min'ing can be done as part of the build itself.
rm -rf "$jfong_clean/Designer2.0/app"
mv "$jfong_clean/Designer2.0/release" "$jfong_clean/Designer2.0/app"

cp "$jfong_clean/Acorn/Acorn/Visualizer/Visualizer.aspx" "$jfong_clean/Designer2.0/app"
if [ -e  "$jfong_clean/Designer2.0/app/Visualizer.aspx" ]; 
then
	echo "Visualizer_zip: copy of Visualizer.aspx worked"
else
	blat -subject "Visualizer_zip: copying Visualizer.aspx had problems" \
	-body "Errors copying Visualizer.aspx, either it does not exist in Acorn or Visualizer directory had issues" \
	-to $Notify
	exit -1
fi


cd "$jfong_clean/Designer2.0"
if [ $? == 0 ]; 
then
	echo "Visualizer_zip: Zip of Visualizer worked"
else
	blat -subject "Visualizer_zip: cd to Visualizer had problems" \
	-body "Errors changing to the Visualizer directory" \
	-to $Notify
	exit -1
fi

"$WinZip_Home/WZZIP.EXE" -ex -r -P -a "`cygpath -w "$jfong_share/$timestamp/Visualizer.zip"`" app
if [ $? == 0 ]; 
then
	echo "Visualizer_zip: change directory to Visualizer worked"
else
	blat -subject "Visualizer_zip: Zip had problems" \
	-body "Errors were generated when we attempted to Zip of Visualizer package" \
	-to $Notify
	exit -1
fi


cp -R -v $jfong_share/$timestamp  $Builds_Dir/Nightly
if [ $? != 0 ]; 
then
	blat -subject "Visualizer_zip: Unable to copy the files to Repo_dev" \
	-body "Exiting since Copy was not successful in transfering the zipped up files." \
	-to $Notify
	exit -1
fi

# Write out the latest build number for deployment to pick up.
echo "build_number=$timestamp" >$Current_Build_Number/visualizer/build_number.txt

echo "RunningTime End zip `date`"

set endlocal

exit 0
