﻿#!/usr/bin/bash

whoami
  
PATH="/usr/local/bin:/usr/bin:/cygdrive/c/Program Files/nodejs:/cygdrive/c/Ruby193/bin:/cygdrive/c/Program Files (x86)/JavaFX/javafx-sdk1.3/bin:/cygdrive/c/Program Files (x86)/JavaFX/javafx-sdk1.3/emulator/bin:/cygdrive/c/Windows:/cygdrive/c/Windows/system32:/cygdrive/c/Windows/System32/Wbem:/cygdrive/c/Program Files/Intel/DMIX:/cygdrive/c/Windows/System32/WindowsPowerShell/v1.0:/cygdrive/c/Program Files (x86)/Microsoft SQL Server/80/Tools/Binn:/cygdrive/c/Program Files/Microsoft SQL Server/90/DTS/Binn:/cygdrive/c/Program Files/Microsoft SQL Server/90/Tools/binn:/cygdrive/c/Program Files/Microsoft SQL Server/100/DTS/Binn:/cygdrive/c/Program Files/Microsoft SQL Server/100/Tools/Binn:/cygdrive/c/Program Files (x86)/Microsoft SQL Server/90/DTS/Binn:/cygdrive/c/Program Files (x86)/Microsoft SQL Server/90/Tools/binn:/cygdrive/c/Program Files (x86)/Microsoft SQL Server/90/Tools/Binn/VSShell/Common7/IDE:/cygdrive/c/Program Files (x86)/Microsoft SQL Server/100/DTS/Binn:/cygdrive/c/Program Files (x86)/Microsoft SQL Server/100/Tools/Binn:/cygdrive/c/Program Files (x86)/Microsoft SQL Server/100/Tools/Binn/VSShell/Common7/IDE:/cygdrive/c/Program Files (x86)/Microsoft Visual Studio 8/Common7/IDE/PrivateAssemblies:/usr/bin:/cygdrive/c/Program Files (x86)/Microsoft Visual Studio 9.0/Common7/IDE/PrivateAssemblies:/cygdrive/c/program files (x86)/Microsoft Visual Studio 10.0/Common7/IDE:/cygdrive/c/Program Files/MySQL/MySQL Server 5.1/bin:/cygdrive/c/Program Files/Java/jdk1.7.0/bin:/cygdrive/c/Program Files/Java/jdk1.7.0/jre/bin:/cygdrive/c/Program Files (x86)/apache-ant-1.8.2/bin:/cygdrive/c/Program Files (x86)/Git/cmd:/cygdrive/c/Program Files (x86)/Git/bin:/cygdrive/c/Program Files (x86)/apache-ant-1.8.2/bin:/cygdrive/c/jruby-1.7.2/bin:/cygdrive/c/Program Files/apache-maven-3.2.1/bin"

JAVA_HOME=/cygdrive/c/Program\ Files/Java/jdk1.6.0_27
export JAVA_HOME
/usr/bin/env 

function Parameter_Help
{
echo "usage: wrapper_*.sh [-options]"
			echo "where options include:"
			echo " -n <email notification list> comma separated"
			echo " 		default=jfong@birst.com,4157301327@vtext.com"
			echo " -z <password> "
			echo " 		CVS password for exporting code "
			echo ""
}

cd ~; source ./.profile
cd /cygdrive/c/CustomerInstalls/NW/NWTest; source ./.profile

echo "RunningTime Start Build `date`"
 
echo on 
Working_Branch="dev"
alias autoexec=autoexec.bat
timestamp=`/usr/bin/date +%Y%m%d%H%M_5122`;		export timestamp
#	timestamp=`date +%Y%m%d%H%M`;			export timestamp
echo $timestamp

env

Notify_list="jfong@birst.com"
Notify_wrapper_list="jfong@birst.com"
#	,ricks@birst.com
#	Notify_list="ricks@birst.com"
export Notify_list
export Notify_wrapper_list
echo $Notify_list
Current_Build_Number="$(cygpath -u -p -a '\\repo_dev\Dev\Builds\Current_Build_Number')"
Git_workspace="C:\\Program Files (x86)\\Jenkins\\jobs\\$Working_Branch\\workspace"
Git_unix_workspace="$(cygpath -u -p -a "C:\\Program Files (x86)\\Jenkins\\jobs\\$Working_Branch\\workspace")"
cvs_password=""
jfong_clean="$(cygpath -u -p -a "C:\\Program Files (x86)\\Jenkins\\jobs\\$Working_Branch\\workspace")"
jfong_win_clean="C:\\Program Files (x86)\\Jenkins\\jobs\\$Working_Branch\\workspace"
jfong_share=/cygdrive/c/shared/Builds;
jfong_win_share=c:\\shared\\Builds;
WinZip_Home="$(cygpath -u -p -a 'C:\Program Files (x86)\WinZip')"

while getopts ":n:b:" opts; do
        case "$opts" in
		b ) echo "-b $OPTARG" 
			Working_Branch=$OPTARG
			echo $Working_Branch
			;;
		n ) echo "-n $OPTARG" 
			Notify=$OPTARG
			export Notify 
			;;
		*  ) Parameter_Help 
			exit 1 ;;
	esac
	echo one value $1
done

#if [ -z $Working_Branch ] ;	#  Make sure we have our parameters
#then
#	Parameter_Help
#	exit 1
#fi

which blat
blat -profile 
ping 192.168.85.11

#	blat -superdebugT -s "Wrapper_Birst_$Working_Branch : Daily build start" -body "Started building the $timestamp build" -to "$Notify_wrapper_list"

echo $jfong_clean
mkdir $jfong_share/$timestamp
mkdir $jfong_share/$timestamp/connectors

#
# Switch to the right branch
#
which git

cd "$Git_unix_workspace"
#	git checkout $Working_Branch

if [ $? == 0 ]; 
then
	echo "Git checkout was successful"  
else
	blat -subject "Wrapper_Birst_$Working_Branch : Build ended with Git Checkout error" \
	-body "Git had problems switching to the requested Branch" \
	-to $Notify_wrapper_list
	exit -1
fi

echo "RunningTime Start CVS `date`"

/usr/bin/bash -x "$Git_unix_workspace"/BuildsAndReleases/Jenkins/Git_sync2.sh -k 0 -c "$jfong_clean" -b $Working_Branch -t "$timestamp" -w "$Git_unix_workspace" 
if [ $? == 0 ]; 
then
	echo "Git_sync was successful"  
else
	blat -subject "Wrapper_Birst_$Working_Branch : Build ended with Git error" \
	-body "CVS had problems with tagging or exporting " \
	-to $Notify_wrapper_list
	exit -1
fi

echo "RunningTime Stop CVS `date`"

cd "$jfong_clean"
chmod -R 755 *



#
# Build MemDB 
#

echo "RunningTime Start MemDB `date`"

/usr/bin/bash -x  "$Git_unix_workspace"/BuildsAndReleases/Jenkins/MemDBBuild.sh  -c "$jfong_clean" -t "$timestamp" -n "$Notify_list" 

if [ $? == 0 ]; 
then
	echo "MemDB was successful"  
else
	blat -subject "Wrapper_Birst_$Working_Branch : Build ended with MemDB error" \
	-body "MemDB had problems with building " \
	-to $Notify_wrapper_list
	exit -1
fi

echo "RunningTime Stop MemDB `date`"

#
# Build FlexAdhoc 
#
echo $jfong_clean
echo "RunningTime Start FlexAdhoc `date`"

/usr/bin/bash -x  "$Git_unix_workspace"/BuildsAndReleases/Jenkins/FlexAdhocBuild.sh  -c "$jfong_clean" -t "$timestamp" -n "$Notify_list" 

if [ $? == 0 ]; 
then
	echo "FlexAdhoc was successful"  
else
	blat -subject "Wrapper_Birst_$Working_Branch : Build ended with FlexAdhoc error" \
	-body "FlexAdhoc had problems with building " \
	-to $Notify_wrapper_list
	exit -1
fi
echo $JAVA_HOME

echo "RunningTime Stop FlexAdhoc `date`"

#
# Build DashboardApp 
#

echo "RunningTime Start DashboardApp `date`"

/usr/bin/bash -x  "$Git_unix_workspace"/BuildsAndReleases/Jenkins/DashboardAppBuild.sh -c "$jfong_clean" -t "$timestamp" -n "$Notify_list" 

if [ $? == 0 ]; 
then
	echo "DashboardApp was successful"  
else
	blat -subject "Wrapper_Birst_$Working_Branch : Build ended with DashboardApp error" \
	-body "DashboardApp had problems with building " \
	-to $Notify_wrapper_list
	exit -1
fi

echo "RunningTime Stop DashboardApp `date`"
#
# Build ChartRenderer 
#
echo $jfong_clean
echo "RunningTime Start ChartRenderer `date`"

/usr/bin/bash -x  "$Git_unix_workspace"/BuildsAndReleases/Jenkins/ChartRendererBuild.sh  -c "$jfong_clean" -t "$timestamp" -n "$Notify_list" 

if [ $? == 0 ]; 
then
	echo "ChartRenderer was successful"  
else
	blat -subject "Wrapper_Birst_$Working_Branch : Build ended with ChartRenderer error" \
	-body "ChartRenderer had problems with building " \
	-to $Notify_wrapper_list
	exit -1
fi

echo "RunningTime Stop ChartRenderer `date`"

#
# Build Touch 
#

echo "RunningTime Start Touch `date`"
cd "$Git_unix_workspace"/BuildsAndReleases/Jenkins
pwd
echo $PATH
which cmd
cmd /c TouchBuild.bat -c "$jfong_win_clean" -t "$timestamp" -n "$Notify_list" 

if [ $? == 0 ]; 
then
	echo "Touch was successful"  
else
	blat -subject "Wrapper_Birst_$Working_Branch : Build ended with Touch error" \
	-body "Touch had problems with building " \
	-to $Notify_wrapper_list
	exit -1
fi

echo "RunningTime Stop Touch `date`"

#
# Build DataConductor 
#

echo "RunningTime Start DataConductor_jar `date`"

/usr/bin/bash -x "$Git_unix_workspace"/BuildsAndReleases/Jenkins/DataConductorBuild_jar.sh -c "$jfong_clean" -t "$timestamp" -n "$Notify_list" 

if [ $? == 0 ]; 
then
	echo "DataConductor was successful"  
else
	blat -subject "Wrapper_Birst_$Working_Branch : Build ended with DataConductor error" \
	-body "DataConductor had problems with building " \
	-to $Notify_wrapper_list
	exit -1
fi

echo "RunningTime Stop DataConductor_jar `date`"

#
# Build SMI
#

echo "RunningTime Start SMI `date`"

/usr/bin/bash -x  "$Git_unix_workspace"/BuildsAndReleases/Jenkins/SMIBuilds.sh \
	-h /cygdrive/c/CustomerInstalls/NW/NWTest	\
	-c "$jfong_clean"	\
	-p "7120"					\
	-f 0						\
	-t "$timestamp"					\
	-n "$Notify_list"

if [ $? == 0 ]; 
then
	echo "SMI was successful"  
else
	blat -subject "Wrapper_Birst_$Working_Branch : Build ended with SMI error" \
	-body "SMI had problems with building " \
	-to $Notify_wrapper_list
	exit -1
fi

echo "RunningTime Stop SMI `date`"

#
# Build QueueConsumer 
#

echo "RunningTime Start QueueConsumer `date`"

/usr/bin/bash -x  "$Git_unix_workspace"/BuildsAndReleases/Jenkins/QueueConsumerBuild.sh -c "$jfong_clean" -t "$timestamp" -n "$Notify_list" 

if [ $? == 0 ]; 
then
	echo "QueueConsumer was successful"  
else
	blat -subject "Wrapper_Birst_$Working_Branch : Build ended with QueueConsumer error" \
	-body "QueueConsumer had problems with building " \
	-to $Notify_wrapper_list
	exit -1
fi

cp "$jfong_clean"/QueueConsumer/deployable_units/ConsumerApp.war "$jfong_share"/$timestamp
if [ -s $jfong_share/$timestamp/ConsumerApp.war ];
then
	echo "copy of QueueConsumer was successful"
else
	blat -subject "Wrapper_Birst_$Working_Branch : Build ended with QueueConsumer error" \
	-body "QueueConsumer had problems with copying the ConsumerApp.war to the deployable_units location " \
	-to $Notify_wrapper_list
	exit -1
fi
	
echo "RunningTime Stop QueueConsumer `date`"


#
# Build LogicalExpression 
#

echo "RunningTime Start LogicalExpression `date`"
cd "$Git_workspace"/BuildsAndReleases/Jenkins
cmd /c LogicalExpressionBuild.bat -c "$jfong_win_clean" -t "$timestamp" -n "$Notify_list" 

if [ $? == 0 ]; 
then
	echo "LogicalExpression was successful"  
else
	blat -subject "Wrapper_Birst_$Working_Branch : Build ended with LogicalExpression error" \
	-body "LogicalExpression had problems with building " \
	-to $Notify_wrapper_list
	exit -1
fi

echo "RunningTime Stop LogicalExpression `date`"

#
# Build BirstAdmin 
#

echo "RunningTime Start BirstAdmin `date`"
cd "$Git_workspace"/BuildsAndReleases/Jenkins
cmd /c BirstAdminBuild.bat -c "$jfong_win_clean" -t "$timestamp" -f 0 -n "$Notify_list" 
 
if [ $? == 0 ]; 
then
	echo "BirstAdmin was successful"  
else
	blat -subject "Wrapper_Birst_$Working_Branch : Build ended with BirstAdmin error" \
	-body "BirstAdmin had problems with building " \
	-to $Notify_wrapper_list
	exit -1
fi
cp "$jfong_clean"/Performance\ Optimizer\ Administration/bin/Release/log4net.dll $jfong_share/$timestamp
cp "$jfong_clean"/Performance\ Optimizer\ Administration/bin/Release/Birst\ Administration.exe "$jfong_clean"/Acorn/Acorn/bin/Birst\ Administration.exe
"$WinZip_Home/WZZIP.EXE" -ex -a "$jfong_win_share\\$timestamp\\BirstAdmin.zip" \
	"$jfong_win_clean\\Performance Optimizer Administration\\bin\\Debug\\Birst Administration.exe" \
	"$jfong_win_clean\\LogicalExpression\\LogicalExpression\\bin\\Debug\\LogicalExpression.dll" \
	"$jfong_win_clean\\Performance Optimizer Administration\\bin\\Debug\\Log4net.dll" \
	"$jfong_win_clean\\LogicalExpression\\LogicalExpression\\bin\\Debug\\Antlr3.Runtime.dll"

echo "RunningTime Stop BirstAdmin `date`"

#
# Build DataConductor 
#

echo "RunningTime Start DataConductor_BC `date`"

/usr/bin/bash -x "$Git_unix_workspace"/BuildsAndReleases/Jenkins/DataConductorBuild_BC.sh -c "$jfong_clean" -t "$timestamp" -n "$Notify_list"

if [ $? == 0 ]; 
then
	echo "DataConductor was successful"  
else
	blat -subject "Wrapper_Birst_$Working_Branch : Build ended with DataConductor error" \
	-body "DataConductor had problems with building " \
	-to $Notify_wrapper_list
	exit -1
fi

echo "RunningTime Stop DataConductor_BC `date`"

#
# Build SourceAdmin 
#
echo $jfong_clean
echo "RunningTime Start SourceAdmin `date`"

/usr/bin/bash -x  "$Git_unix_workspace"/BuildsAndReleases/Jenkins/SourceAdminBuild.sh  -c "$jfong_clean" -t "$timestamp" -n "$Notify_list" 

if [ $? == 0 ]; 
then
	echo "SourceAdmin was successful"  
else
	blat -subject "Wrapper_Birst_$Working_Branch : Build ended with SourceAdmin error" \
	-body "SourceAdmin had problems with building " \
	-to $Notify_wrapper_list
	exit -1
fi
echo $JAVA_HOME

echo "RunningTime Stop SourceAdmin `date`"

#
# Build QAUtilities 
#
echo $jfong_clean
echo "RunningTime Start QAUtilities `date`"

/usr/bin/bash -x  "$Git_unix_workspace"/BuildsAndReleases/Jenkins/QAUtilitiesBuild.sh  -c "$jfong_clean" -t "$timestamp" -n "$Notify_list" 

if [ $? == 0 ]; 
then
	echo "QAUtilities was successful"  
else
	blat -subject "Wrapper_Birst_$Working_Branch : Build ended with QAUtilities error" \
	-body "QAUtilities had problems with building " \
	-to $Notify_wrapper_list
	exit -1
fi
echo $JAVA_HOME

echo "RunningTime Stop QAUtilities `date`"

#
# Build Acorn 
#

echo "RunningTime Start Acorn `date`"
cd "$Git_workspace"/BuildsAndReleases/Jenkins
cmd /c AcornBuild.bat -c "$jfong_win_clean" -w "$Git_workspace" -t "$timestamp" -f 0 -n "$Notify_list" 
 
if [ $? == 0 ]; 
then
	echo "Acorn was successful"  
else
	blat -subject "Wrapper_Birst_$Working_Branch : Build ended with Acorn error" \
	-body "Acorn had problems with building " \
	-to $Notify_wrapper_list
	exit -1
fi

echo "RunningTime Stop Acorn `date`"

#
# Build ChartWebService 
#

echo "RunningTime Start ChartWebService `date`"

/usr/bin/bash -x  "$Git_unix_workspace"/BuildsAndReleases/Jenkins/ChartWebServiceBuild.sh -c "$jfong_clean" -t "$timestamp" -n "$Notify_list" 

if [ $? == 0 ]; 
then
	echo "ChartWebService was successful"  
else
	blat -subject "Wrapper_Birst_$Working_Branch : Build ended with ChartWebService error" \
	-body "ChartWebService had problems with building " \
	-to $Notify_wrapper_list
	exit -1
fi

cp "$jfong_clean"/ChartWebService/deployable_units/SMIChartServer.war "$jfong_share"/$timestamp
if [ -s $jfong_share/$timestamp/SMIChartServer.war ];
then
	echo "copy of SMIChartServer.war was successful"
else
	blat -subject "Wrapper_Birst_$Working_Branch : Build ended with ChartWebService error" \
	-body "ChartWebService had problems with copying the ChartWebService.jar to the release location " \
	-to $Notify_wrapper_list
	exit -1
fi
	
echo "RunningTime Stop ChartWebService `date`"

#
# Build Dashboards2.0 
#
#	
#	echo "RunningTime Start Dashboards2.0 zip `date`"
#	
#	cd "$Git_workspace"/BuildsAndReleases/Jenkins
#	cmd /c Dashboards2.0Build.bat -c "$jfong_win_clean" -t "$timestamp" -b $Working_Branch -f 0 -n "$Notify_list" 
#	 
#	if [ $? == 0 ]; 
#	then
#		echo "Dashboards2.0 was successful"  
#	else
#		blat -subject "Wrapper_Birst_$Working_Branch : Build ended with Dashboards2.0 error" \
#		-body "Dashboards2.0 had problems with building." \
#		-to $Notify_wrapper_list
#		exit -1
#	fi
#	
#	echo "RunningTime Stop Dashboards2.0 `date`"

#
# Build BirstIndexerPlugin 
#

echo "RunningTime Start BirstIndexerPluginBuild `date`"

cd "$Git_workspace"/BuildsAndReleases/Jenkins
cmd /c BirstIndexerPluginBuild.bat -c "$jfong_win_clean" -t "$timestamp" -b $Working_Branch -f 0 -n "$Notify_list" 

cp "$jfong_clean"/BirstIndexerPlugin/target/releases/SpaceIndexerPlugin-1.0-SNAPSHOT-plugin.zip "$jfong_share"/$timestamp
if [ -s $jfong_share/$timestamp/SMIChartServer.war ];
then
	echo "copy of "BirstIndexerPlugin was successful""
else
	blat -subject "Wrapper_Birst_$Working_Branch : Build ended with BirstIndexerPlugin error" \
	-body "BirstIndexerPlugin had problems with copying the SpaceIndexerPlugin-1.0-SNAPSHOT-plugin.zip to the release location " \
	-to $Notify_wrapper_list
	exit -1
fi

echo "RunningTime Stop BirstIndexerPluginBuild `date`"

#
# Build Connector 
#
echo $jfong_clean
echo "RunningTime Start Connector `date`"

/usr/bin/bash -x  "$Git_unix_workspace"/BuildsAndReleases/Jenkins/ConnectorBuild.sh  -c "$jfong_clean" -b $Working_Branch -t "$timestamp" -n "$Notify_list" 

if [ $? == 0 ]; 
then
	echo "Connector was successful"  
else
	blat -subject "Wrapper_Birst_$Working_Branch : Build ended with Connector error" \
	-body "Connector had problems with building " \
	-to $Notify_wrapper_list
	exit -1
fi

#
#  Copy the files to the package directory
#

#	cp "$jfong_clean"/ConnectorController/deployable_units/Connector_configs.zip $jfong_share/$timestamp
#	cp "$jfong_clean"/ConnectorController/deployable_units/ConnectorController.war $jfong_share/$timestamp
#	cp "$jfong_clean"/SalesforceConnector_26_0/deployable_units/SalesforceConnector_26_0.war $jfong_share/$timestamp
#	cp "$jfong_clean"/SalesforceConnector_26_0/deployable_units/SalesforceConnector_26_0.war $jfong_share/$timestamp

#
#	figure out where the latest connectors are, then copy them into the current build
#

Build_Number=`grep build_number //repo_dev/dev/Builds/Current_Build_Number/Everest_master/build_number.txt | sed s^build_number=^^`
echo $Build_Number

for connector in NetSuiteConnector_2012_2 NetSuiteConnector_2013_2 MarketoConnector_2_1 GoogleAnalyticsConnector_3_0 OmnitureSiteCatalystConnector_1_4 NetSuiteJDBCConnector 
do
	echo "Copying $connector for the current build"
	cp /cygdrive/c/shared/Builds/$Build_Number/$connector.war $jfong_share/$timestamp/connectors
done

echo "RunningTime Stop Connector `date`"

#
#
# Build MoveSpace 
#

echo "RunningTime Start MoveSpace `date`"
cd "$Git_workspace"/BuildsAndReleases/Jenkins
cmd /c MoveSpaceBuild.bat -c "$jfong_win_clean" -t "$timestamp" -f 0 -n "$Notify_list" 
 
if [ $? == 0 ]; 
then
	echo "MoveSpace was successful"  
else
	blat -subject "Wrapper_Birst_$Working_Branch : Build ended with MoveSpace error" \
	-body "MoveSpace had problems with building " \
	-to $Notify_wrapper_list
	exit -1
fi

echo "RunningTime Stop MoveSpace `date`"

#
# Build AcornExecute 
#

mkdir "$jfong_clean"/AcornExecute/AcornExecute/bin
cp "$jfong_clean"/Performance\ Optimizer\ Administration/bin/Release/Birst\ Administration.exe "$jfong_clean"/AcornExecute/AcornExecute/bin/Birst\ Administration.exe
echo "RunningTime Start AcornExecute `date`"
cd "$Git_workspace"/BuildsAndReleases/Jenkins
cmd /c AcornExecuteBuild.bat -c "$jfong_win_clean" -t "$timestamp" -f 0 -n "$Notify_list" 
 
if [ $? == 0 ]; 
then
	echo "AcornExecute was successful"  
else
	blat -subject "Wrapper_Birst_$Working_Branch : Build ended with AcornExecute error" \
	-body "AcornExecute had problems with building " \
	-to $Notify_wrapper_list
	exit -1
fi

echo "RunningTime Stop AcornExecute `date`"

#
# Build AcornUploader 
#

mkdir "$jfong_clean"/AcornUploader/AcornUploader/bin
#	cp "$jfong_clean"/Performance\ Optimizer\ Administration/bin/Release/Birst\ Administration.exe "$jfong_clean"/AcornUploader/AcornUploader/bin/Birst\ Administration.exe
echo "RunningTime Start AcornUploader `date`"
cd "$Git_workspace"/BuildsAndReleases/Jenkins
cmd /c AcornUploaderBuild.bat -c "$jfong_win_clean" -t "$timestamp" -f 0 -n "$Notify_list" 
 
if [ $? == 0 ]; 
then
	echo "AcornUploader was successful"  
else
	blat -subject "Wrapper_Birst_$Working_Branch : Build ended with AcornUploader error" \
	-body "AcornUploader had problems with building " \
	-to $Notify_wrapper_list
	exit -1
fi

echo "RunningTime Stop AcornUploader `date`"

#
# Build Scheduler 
#
echo "$jfong_clean"
echo "RunningTime Start Scheduler `date`"

/usr/bin/bash -x  "$Git_unix_workspace"/BuildsAndReleases/Jenkins/SchedulerBuild.sh  -c "$jfong_clean" -t "$timestamp" -n "$Notify_list" 

if [ $? == 0 ]; 
then
	echo "Scheduler was successful"  
else
	blat -subject "Wrapper_Birst_$Working_Branch : Build ended with Scheduler error" \
	-body "Scheduler had problems with building " \
	-to $Notify_wrapper_list
	exit -1
fi

echo "RunningTime Stop Scheduler `date`"

#
# Package Help 
#

echo "RunningTime Start Help `date`"
cd "$Git_workspace"/BuildsAndReleases/Jenkins
cmd /c PackageHelp.bat -c "$jfong_win_clean" -t "$timestamp" -f 0 -n "$Notify_list" 
 
if [ $? == 0 ]; 
then
	echo "PackageHelp was successful"  
else
	blat -subject "Wrapper_Birst_$Working_Branch : Package Help ended with an error" \
	-body "Help had problems with the packaging " \
	-to $Notify_wrapper_list
	exit -1
fi

echo "RunningTime Stop Help `date`"

#
#	Sign Jars
#

echo "RunningTime Start SignJars `date`"

/usr/bin/bash -x  "$Git_unix_workspace"/BuildsAndReleases/Jenkins/Sign_jars.sh -t "$timestamp" -b $Working_Branch -n "$Notify_list" 
if [ $? == 0 ]; 
then
	echo "SMI was successful"  
else
	blat -subject "Wrapper_Birst_$Working_Branch : Build ended with Sign_jar error" \
	-body "Sign_jar had problems encrypting files " \
	-to $Notify_wrapper_list
	exit -1
fi

echo "RunningTime Stop SignJars `date`"

#
# FTP files
#

echo "RunningTime Start FTPfiles `date`"

/usr/bin/bash -x  "$Git_unix_workspace"/BuildsAndReleases/Jenkins/ftp_files.sh \
	-h /cygdrive/c/CustomerInstalls/NW/NWTest	\
	-c "$jfong_clean"	\
	-p "7120"					\
	-f 0						\
	-t "$timestamp"					\
	-n "$Notify_list"				

if [ $? == 0 ]; 
then
	echo "FTP was successful"  
else
	blat -subject "Wrapper_Birst_$Working_Branch : FTP error" \
	-body "FTP had problems with transfering files " \
	-to $Notify_wrapper_list
	exit -1
fi

echo "RunningTime Stop FTPfiles `date`"

# Write out the latest build number for deployment to pick up.
echo "build_number=$timestamp" >$Current_Build_Number/$Working_Branch/build_number.txt

blat -superdebugT -subject "Wrapper_Birst_$Working_Branch : Daily build completed without errors" \
	-body "$timestamp build is now available" \
	-to "$Notify_wrapper_list" 

#/cygdrive/c/Program\ Files\ \(x86\)/sendmail/sendEmail.exe -s 192.168.85.11 -f jfong@birst.com  \
#	-t "$Notify_wrapper_list" \
#	-u "Wrapper_Birst_$Working_Branch : Daily build completed without errors"	\
#	-m "$timestamp build is now available"

echo "RunningTime End Build `date`"

exit 0
