echo on 
echo "RunningTime Start Dashboards2.0 `date`"

ECHO ON

set VarNotify=jfong@birst.com
set VarMailText=Text message as part of the Mail
set VarMailSubject=Dashboards2.0
set FTP_files=0

:PROCESS_INPUT

if "%1"=="-n" goto SET_NOTIFICATION:
if "%1"=="-b" goto SET_BRANCH:
if "%1"=="-c" goto SET_CLEAN_DIR:
if "%1"=="-f" goto SET_FLAG:
if "%1"=="-t" goto SET_TIMESTAMP:
if "%1"=="" goto PROCESS_INPUT_DONE:
set temp=%1
if NOT "%temp:~0,1%"=="-" goto ADD_NOTIFICATION:

goto PROCESS_INPUT_ERROR:

:SET_TIMESTAMP
shift
set TimeStamp=%1
goto PROCESS_INPUT_LOOP:

:SET_BRANCH
shift
set Working_Branch=%1
goto PROCESS_INPUT_LOOP:

:SET_CLEAN_DIR
shift
set clean_dir=%1
goto PROCESS_INPUT_LOOP:

:SET_FLAG
shift
set FTP_files=%1
echo %FTP_files%
goto PROCESS_INPUT_LOOP:

:SET_NOTIFICATION
shift
set VarNotify=%1
goto PROCESS_INPUT_LOOP:

:ADD_NOTIFICATION
set VarNotify="%VarNotify%,%temp%"
echo %VarNotify%
goto PROCESS_INPUT_LOOP:

:PROCESS_INPUT_LOOP

shift
goto PROCESS_INPUT:

:PROCESS_INPUT_ERROR

echo ERROR: invalid input parameter
echo usage: MoveSpaceBuild.bat [-options]
echo where options include:
echo.
echo  -t ^<Time Stamp^> (optional)
echo 		Enter the Time Stamp for identifying the deployment
echo		the format is YYYYMMDDHHMM, or one will be generated.		
echo  -b ^<CVS Branch^> 
echo 		enter the cvs branch for the release e.g. HEAD, Birst_3_1_5_SP
echo  -n ^<email notification list^> comma separated (optional)
echo  		default=jfong@birst.com,4157301327@vtext.com
echo 		NOTE - multiple email addresses need to be separated by 
echo 			commas and enclosed in double quote
echo.

set VarMailText=Invalid arguement(s) provided. 
set VarMailSubject=MoveSpaceBuild:Process Arguements
goto BADEXIT

:PROCESS_INPUT_DONE
REM
REM We need a timestamp.  If it is not provided, we should create our own.
REM

if "%TimeStamp%" neq "" ( goto TIMESTAMP_DEFINED: ) 

for /f "tokens=1-3 delims=:." %%a in ("%time%") do set hh_mm_stamp=%%a%%b
echo %hh_mm_stamp%
for /f "tokens=1-3 delims=\ " %%a in ("%date%") do set date_only=%%b
for /f "tokens=1-3 delims=/." %%a in ("%date_only%") do set teststamp=%%c%%a%%b
echo %teststamp%
set TimeStamp="%teststamp%%hh_mm_stamp%"
	
:TIMESTAMP_DEFINED
echo TimeStamp: %TimeStamp%

echo -%TimeStamp%-
call :dequote %TimeStamp%
set TimeStamp=%ret%
set TimeStamp=%TimeStamp: =%
echo -%TimeStamp%-

set Timestamp=%TimeStamp%_dash
echo %Timestamp%

set clean_dir="C:\Program Files (x86)\Jenkins\jobs\%Working_Branch%\workspace"
set jfong_clean="C:\Program Files (x86)\Jenkins\jobs\%Working_Branch%\\workspace")"
set jfong_share="c:\shared\Builds"
set Current_Build_Number="\\repo_dev\Dev\Builds\Current_Build_Number"
set Builds_Dir="\\repo_dev\Dev\Builds"
set WinZip_Home="C:\Program Files (x86)\WinZip"

rem
rem	Run the Dashboard 2.0 Grunt tasks.  Spawn a child process since grunt ends the batch job..
rem

cd %clean_dir%\BuildsAndReleases\Jenkins

cmd /c Dashboards2.0Grunt.bat %clean_dir%
IF %ERRORLEVEL% LSS 1 goto DASHBOARD_BUILD_DONE:
set VarMailText=Could not build the Dashboard 2.0 Minimized release files. Grunt error, please fix before proceeding. 
set VarMailSubject=Dashboard 2.0:Dashboard 2.0 build error 
goto BADEXIT:

:DASHBOARD_BUILD_DONE
set jfong_share="c:\shared\Builds"
IF %ERRORLEVEL% LSS 1 goto DASHBOARD_PACKAGE_FILES:
set VarMailText=Could not create the shared directory, please fix before proceeding. 
set VarMailSubject=Dashboard 2.0:Dashboard 2.0 build error 
goto BADEXIT:

:DASHBOARD_PACKAGE_FILES
cd "%clean_dir%\ng-dashboards2.0\bin"

"c:\Program Files (x86)\WinZip\WZZIP.EXE" -ex -r -p -a %jfong_share%\%TimeStamp%\Dashboards2.0.zip .
IF %ERRORLEVEL% LSS 1 goto DASHBOARD_COPIED:
set VarMailText=Could not copy the Dashboard 2.0 release files to the Dashboard 2.0 directory. 
set VarMailSubject=Dashboard 2.0:Dashboard 2.0 copy error 
goto BADEXIT:

:DASHBOARD_COPIED

xcopy /I /E /Q /V %jfong_share%\%TimeStamp%  %Builds_Dir%\Nightly\%TimeStamp%
IF %ERRORLEVEL% LSS 1 goto DASHBOARD_ON_REPO:
set VarMailText=Could not copy the Dashboard 2.0 release files to the Dashboard 2.0 directory. 
set VarMailSubject=Dashboard 2.0:Dashboard 2.0 copy error 
goto BADEXIT:

:DASHBOARD_ON_REPO

REM	echo build_number=%Timestamp%>%Current_Build_Number%\%Working_Branch%\build_number.txt
goto THE_END:

:BADEXIT
echo ------------------ Bad Exit -------------------------------------------
echo %VarMailText%
blat -to %VarNotify% -subject "%VarMailSubject%" -body "%VarMailText%"
rem *** if exist %VarCustProp% del %VarCustProp%
echo "This batch file failed."
exit 1

:THE_END

exit 0

:dequote
setlocal
rem The tilde in the next line is the really important bit.
set thestring=%~1
endlocal&set ret=%thestring%
goto :EOF

:EOF