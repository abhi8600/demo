echo on
REM
REM  Simple script to build/minimize the birst-ui code.  
REM This is really a workaround for Grunt since it completely exits the script that executed it when it is done.
REM Any other commands in the script are bypassed.  
REM
set clean_dir=%1
REM set clean_dir=c:\workspace_git\birst

set PATH=C:\Users\jfong\AppData\Roaming\npm;%PATH%
if not exist %clean_dir%\birst-ui\bower ( mklink /d %clean_dir%\birst-ui\bower "c:\Program Files\bower")

cd "%clean_dir%\birst-ui"

cmd /c npm prune
IF %ERRORLEVEL% LSS 1 goto NPM_PRUNE_DONE:
set VarMailText=NPM Install had issues,please fix before proceeding. 
set VarMailSubject=birst-ui:birst-ui build error 
blat -to %VarNotify% -subject "%VarMailSubject%" -body "%VarMailText%"

:NPM_PRUNE_DONE
cmd /c npm install
IF %ERRORLEVEL% LSS 1 goto NPM_INSTALL_DONE:
set VarMailText=NPM Install had issues,please fix before proceeding. 
set VarMailSubject=birst-ui:birst-ui build error 
goto BADEXIT:

:NPM_INSTALL_DONE
rem cmd /c npm update
IF %ERRORLEVEL% LSS 1 goto NPM_UPDATE_DONE:
set VarMailText=NPM Update had issue, please fix before proceeding. 
set VarMailSubject=birst-ui:birst-ui build error 
goto BADEXIT:

:NPM_UPDATE_DONE

grunt 

goto THE_END:

:BADEXIT
echo ------------------ Bad Exit -------------------------------------------
echo %VarMailText%
blat -to %VarNotify% -subject "%VarMailSubject%" -body "%VarMailText%"
rem *** if exist %VarCustProp% del %VarCustProp%
echo "This batch file failed."
exit 1

:THE_END

