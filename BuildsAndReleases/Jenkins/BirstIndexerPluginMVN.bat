echo on
REM
REM  Simple script to build/minimize the Visualizer code.  
REM This is really a workaround for Grunt since it completely exits the script that executed it when it is done.
REM Any other commands in the script are bypassed.  
REM
set PATH=%PATH%;"C:\Program Files\apache-maven-3.2.1\bin"
set JAVA_HOME="C:\Progra~2\Java\jdk1.7.0"
set clean_dir=%1

cd "%clean_dir%\BirstIndexerPlugin"

mvn package 

goto THE_END:

:BADEXIT
echo ------------------ Bad Exit -------------------------------------------
echo %VarMailText%
blat -to %VarNotify% -subject "%VarMailSubject%" -body "%VarMailText%"
rem *** if exist %VarCustProp% del %VarCustProp%
echo "This batch file failed."
exit 1

:THE_END

