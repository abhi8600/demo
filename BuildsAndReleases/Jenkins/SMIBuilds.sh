﻿#! /usr/bin/bash	
#
# Nightly Build and Test
#echo on

source /cygdrive/c/Documents\ and\ Settings/jfong.SUCCESSMETRICS/.profile

# source /cygdrive/c/CustomerInstalls/Demos/Southwind/.profile 

function Parameter_Help
{
	echo "usage: deploy [-options]"
	echo "where options include:"
	echo " -b <branch> "
	echo "		Enter the branch that we will be tagging on."
	echo " 		If not branch is supplied, we default to HEAD"
	echo " -h <smi_home> "
	echo "		Enter the home directory of the instance"
	echo " 		e.g./cygdrive/c/CustomerInstalls/RBC/RBCWeb"
	echo " -n <email notification list> comma separated"
	echo " 		default=jfong@birst.com,4157301327@vtext.com"
	echo " -p <port number>"
	echo "		Enter 4 digit port number for the installation"	
	echo " 		Installation will use port number +2"
	echo "		e.g. 9300, 9301, 9302"
	#	echo " -r <release directory> "
	#	echo " 		Which do you want to get the release from?"
	#	echo "		Nightly for nightly builds"
	#	echo "		Releases/Platform for version releases"
	echo " -t <build tag> "
	echo " 		If you are getting files from Releases/Platform, enter a Release Number e.g. 2.0.0"
	echo "		If you are getting files from Nightly, enter a date and time e.g. 200705230130"
}

nightly_bin=/cygdrive/c/workspace_HEAD/BuildsAndReleases/builds; 		export nightly_bin
jfong_share=/cygdrive/c/shared/Builds;				export jfong_share
jfong_clean="";								export jfong_clean
Notify="jfong@birst.com"; 		export Notify		#email list
declare -i error_count=0

#--------------------------------------------------------------------------------------------------------------------------
ANT_OPTS="-Xms768m -Xmx1024m -XX:MaxPermSize=768m";				export ANT_OPTS
DEBUG=0;					export DEBUG			#debug=1, not debug=0
FTP_files=0;					export FTP_files
SMI_HOME="";					export SMI_HOME
Https_Port=""; 					export Https_Port
Release_Dir="";					export Release_Dir
Period_Id="";					export Period_Id
build_tag="";					export build_tag
SMI_Branch="";					export SMI_Branch
SMI_workspace=""				export SMI_workspace
jfong_clean=""					export jfong_clean
env
#  Notify=jfong@birst.com;		export Notify
echo done clearing stuff

while getopts ":d:h:b:c:n:p:r:t:f:" opts; do
        case "$opts" in
		d ) echo "-d $OPTARG"
			DEBUG=$OPTARG
		 	export DEBUG
			;;
		b ) echo "-b $OPTARG" 
			SMI_Branch=$OPTARG
			export SMI_Branch
			echo $SMI_Branch
			;;
		c ) echo "-c $OPTARG"
			jfong_clean="$(cygpath -u -p -a "$OPTARG")"
			SMI_workspace="$(cygpath -u -p -a "$OPTARG")"
			echo $jfong_clean
			;;
		f ) echo "-f $OPTARG" 
			FTP_files=$OPTARG
			export FTP_files
			echo $FTP_files
			;;
		h ) echo "-h $OPTARG" 
			SMI_HOME=$OPTARG
			export SMI_HOME				# expect SMI_HOME to be in the following format
			Cust_Type=${SMI_HOME##*/}		# 	/cygdrive/c/CustomerInstalls/RBC/RBCWeb
			Customer_Home=${SMI_HOME%/*}		# so    ----------------------------
			export Customer_Home			#            Install Root            ---
			Customer_Name=${Customer_Home##*/}	#                          Customer Name ------
			export Customer_Name			#                                        Customer's installation type
			Cust_Root=${Customer_Home%/*}		#SMI_HOME=$Install_Root+$Customer_Name+Cust_Type
			export Cust_Root
			Install_Root=${Customer_Home%/*}
			export Install_Root

			echo $Install_Root
			echo $Customer_Home
			echo $Customer_Name
			echo $Cust_Type
			;;
		n ) echo "-n $OPTARG" 
			Notify="$Notify,$OPTARG"
			export Notify 
			;;
		p ) echo "-p $OPTARG"
			echo $OPTARG | grep [^0-9] >/dev/null 2>&1
			if [ "$?"  -eq "0" ]; then
				echo $OPTARG "is not a number.  The Port Number needs to be numeric"  
				exit 1
			else
				Https_Port=$OPTARG;  export Https_Port 
			fi
			;;
		r ) echo "-r $OPTARG" 
			if [ $OPTARG == "Nightly" ] || [ $OPTARG == "Releases/Platform" ]; 
				then 
				Release_Dir=$OPTARG
				export Release_Dir
			else
				echo "Release Build directory, $OPTARG, does not exist, please fix"
				exit 1
			fi	
			;;
		t ) echo "-t $OPTARG"			# Since I'm creating the build, I am generating the build tag.  
			build_tag=$OPTARG;	
			export build_tag
			;;
		*  ) Parameter_Help
			exit 1 ;;
	esac
	echo one value $1
done

echo Is build tag defined? $build_tag
if [ -z $build_tag ]; then
	build_tag=`date +%Y%m%d%H%M`
	export build_tag
fi
date_range1=`date --date=yesterday +%Y-%m-%d\ %H\:%M`;	export date_range1
date_range2=`date +%Y-%m-%d\ %H\:%M`;			export date_range2

#
# figure out which branch we are using.
#
# jfong_clean="/cygdrive/c/clean_builds_"$SMI_Branch

echo "clean     is " $jfong_clean
echo "workspace is " $SMI_workspace

if [ -z $SMI_HOME ] || [ -z $Https_Port ]  || [ -z $build_tag ]; 	#	|| [ -z $Release_Dir ] || [ -z $Period_Id ]; 
then
	echo "One or more of the required parameters was not provided."
	echo "Please provide all the required paramaters:" 
	Parameter_Help
	exit 1 
fi 

#-----------------------------------------------------------------------
CATALINA_HOME=$SMI_HOME/tomcat;				export CATALINA_HOME
CATALINA_BASE=$SMI_HOME/tomcat;				export CATALINA_BASE

JAVA_HOME=`cygpath -w "C:\Program Files\Java\jdk1.7.0"` ;		export JAVA_HOME

echo $DEBUG
echo $SMI_HOME 
echo $build_tag
echo $JAVA_HOME
echo $CATALINA_BASE
echo $CATALINA_HOME
echo "who are you?"
id
echo ""
echo Date range is $date_range1 and $date_range2

#
#	Make sure all the localization is setup 
#	several scripts need to be modified to work in my environment.
#	(e.g. build.properties needs to know about the local tree)
#
#	First, make sure the build.properties file is setup for my local 
#	installation.  
#		The build.properties files expects Apache to be installed 
#		on your computer.  This is a hack to allow me to build 
#		off the Brokerage demo.
#

cd "$jfong_clean"/SMI/staging/minnesota
if [ -a build.properties ]; 
then
	echo "build.properties exist"
	cp ./build.properties ./build.temp
else
	blat -subject "SMIBuild: CVS properties file error" \
	-body "Something is wrong, I can't find the build.properties file. \n The CVS update or something else did not work" \
	-to $Notify
	exit -1
fi

# C:\/apache-tomcat-6.0.32\/lib

sed '1,$ s^c\:\/dev\/main\/tomcat^C\:\/Program\ Files\ \(x86\)\/Apache\ Software\ Foundation\/Tomcat\ 6.0^' <./build.temp >./build.properties
rm ./build.temp		# we've update the file, we can delete the older version.

cp ./build.properties ./build.temp
sed '1,$ s^Program Files (x86)\/Apache Software Foundation\/apache-tomcat-5.5.20^C\:\/Program\ Files\ \(x86\)\/Apache\ Software\ Foundation\/Tomcat\ 6.0^' <./build.temp >./build.properties
rm ./build.temp		# we've update the file, we can delete the older version.

# handle the 64 bit definitions.  Make them 32 bits again. 
cp ./build.properties ./build.temp
sed '1,$ s^E\:\/CustomerInstalls\/RightNow\/RightNowWeb\/tomcat^C\:\/Program\ Files\ \(x86\)\/Apache\ Software\ Foundation\/Tomcat\ 6.0^' <./build.temp >./build.properties
rm ./build.temp		# we've update the file, we can delete the older version.


# handle the 64 bit definitions.  Make them 32 bits again. 
#	cp ./build.properties ./build.temp
#	sed '1,$ s^Program Files (x86)\/^Program Files\/^' <./build.temp >./build.properties
#	rm ./build.temp		# we've update the file, we can delete the older version.

#
# Verify that our substitution/hack worked
#
grep -q "Tomcat\ 6.0" ./build.properties
if [ $? == 0 ];
then
        echo "substitution worked"
else
	blat -subject "SMIBuild: Localize builds.properties failed" \
        -body "Substitution for local environment did not work. \\n Either CVS did not update or someone change the file.\n exiting so we can figure it out" \
        -to $Notify
        exit -1
fi

#
# TEMPORARY change to handle the hard coded requirement of where CATALINA_HOME has to be.  
#
	
cd "$jfong_clean"/SMI/SMIWeb/minnesota
if [ -a build.xml ]; 
then
	echo "build.xml exist"
	cp ./build.xml ./build.temp
else
	blat -subject "SMIBuild: CVS properties file error" \
	-body "Something is wrong, I can't find the build.xml file. \n The CVS update or something else did not work" \
	-to $Notify
	exit -1
fi
sed '1,$ s^c:\/dev\/Main\/tomcat^C\:\/Program\ Files\ \(x86\)\/Apache\ Software\ Foundation\/Tomcat\ 6.0^' <./build.temp >./build.xml
rm ./build.temp		# we've update the file, we can delete the older version.

#
#	Put the current build number in the release directoy
#

mkdir ""$jfong_share""/$build_tag
cp "$jfong_clean"/SMI/staging/minnesota/build.number "$jfong_share"/$build_tag

#
#
#	Goto the staging directory
#		- ant clean to clean out old junk
#		- ant all to build all
#
ANT_OPTS=-Xmx1024M				export ANT_OPTS
env

echo $JAVA_HOME
JAVA_HOME='/cygdrive/c/Program Files/Java/jdk1.7.0'; export JAVA_HOME
echo $JAVA_HOME
#
# Build the rest of SMI
#

cd "$jfong_clean"/SMI/staging/minnesota

echo "Clean out everything"
ant clean
if [ $? != 0 ];
then
	blat -subject "SMIBuild: clean env had errors" \
	-body "Exiting nightly build since there was clean error with SMI" \
	-to $Notify
	exit -1
fi

echo "Build everything"
ant -verbose  nightly
if [ $? != 0 ];
then
	blat -subject "SMIBuild: Build all had an error " \
	-body "Exiting nightly build since there was a build error with ant" \
	-to $Notify
	exit -1
fi

echo "creating the difference log" 

#if [ $DEBUG -eq 0 ]; 
#then
#	/cygdrive/c/Program\ Files/CVSNT/cvs -d :pserver:jfong:jfong123@SMISRC:/CVS rlog -S -N -d "$date_range1<=$date_range2" SMI  >"$jfong_share"/$build_tag/changes.log
#fi

cd "$jfong_clean"/Performance\ Optimizer\ Administration/bin/Release

#
#  Standalone Birst Admin.
#
#if [ -e log4net.dll ]; 
#then
#	echo "log4net.dll exists, we  will run ILMerge"
#	/c/Program\ Files/Microsoft/ILMerge/ILMerge.exe     /t:winexe /out:Birst\ Administration.exe  Birst\ Administration.exe log4net.dll
#	if [ $? != 0 ];
#	then
#		blat -subject "SMIBuild: ILMerge had problems  " \
#		-body "Exiting nightly build since there was a problem with the ILMerge." \
#		-to $Notify
#		exit -1
#	fi
#else
#	echo "There is no lo4net.dll available.  Bypass the ILMerge process"
#fi


#
#	We should now have the SMIWeb.war, the PerformanceEngine.zip and the SMI Adminstrator.exe files
#	Copying them over to the shared drive for safe keeping

echo "copy over the files to the shared drive"

cp "$jfong_clean"/SMI/SMIWeb/minnesota/deployable_units/SMIWeb.war "$jfong_share"/$build_tag
cp "$jfong_clean"/SMI/PerformanceEngine/minnesota/deployable_units/PerformanceEngine.zip "$jfong_share"/$build_tag
#	cp "$jfong_clean"/Performance\ Optimizer\ Administration/SMI\ Administration\ Installer.exe  "$jfong_share"/$build_tag
#	cp "$jfong_clean"/Performance\ Optimizer\ Administration/bin/Release/Birst\ Administration.exe "$jfong_share"/$build_tag
#	cp "$jfong_clean"/Performance\ Optimizer\ Administration/bin/Release/log4net.dll "$jfong_share"/$build_tag
#	cp "$jfong_clean"/LogicalExpression/LogicalExpression/bin/Debug/LogicalExpression.dll "$jfong_share"/$build_tag
#	cp "$jfong_clean"/LogicalExpression/LogicalExpression/bin/Debug/Antlr3.Runtime.dll "$jfong_share"/$build_tag
#	cp "$jfong_clean"/SMI/staging/minnesota/build.number "$jfong_share"/$build_tag
#	cp "$jfong_clean"/ChartService/console-chart-server/target/ChartService.jar "$jfong_share"/$build_tag
####################################################################################
#
# This is a temporary fix for 3.1.5
#
##########################################################################

if [ ! -d "$jfong_clean"/Acorn/Acorn/bin ];
then
	mkdir "$jfong_clean"/Acorn/Acorn/bin
	if [ $? != 0 ];
	then
		echo "Problem creating the Acorn Bin subdirectory." $Notify
		exit -1
	fi
fi
#ls -la ""$jfong_clean""/Acorn/Acorn/bin
#cp "$jfong_clean"/Performance\ Optimizer\ Administration/bin/Release/Birst\ Administration.exe $jfong_clean/Acorn/Acorn/bin/Birst\ Administration.exe

#
#
#
# Package up the Demo for testing
# first copy it over from /c/clean_builds_4
#
# NOTE *** this is a total hack to get a demo file for me to test with
#
#
#

# cp  -r /c/clean_builds_4/SMI/Customers/Demo /c/clean_builds/SMI/Customers

# 
# clean up the customer area before retrieving the data
#

cd "$jfong_clean"
rm -rf SMI_Customers
ls -la 

#
# Package up the customer specific files.  
#	1. add the configuration files into SMIWeb.war
#	2. zip up all the customer specific files (repository.xml, dashboards, reports, etc.)
#
#

#/cygdrive/c/Program\ Files/CVSNT/cvs -d :pserver:jfong:jfong123@SMISRC:/CVS export -r HEAD  SMI_Customers

#stat=$?
#echo CVS exit status $stat

#if [ $stat == 0 ]; 
#then
#	echo "CVS export for SMI_Customers worked"
#else
#	echo "CVS export for SMI_Customers had errors"
#	exit -1
#fi

#
#  steps 2 & 3
#
if [ 1 == 0 ];  
then 
	for Cust_Name in `ls -1 "$jfong_clean"/SMI_Customers`
	do
		echo $Cust_Name
		if [ $Cust_Name != "Operation" ];
		then
			if [ ! -d "$jfong_share"/$build_tag/$Cust_Name ];
			then
				mkdir "$jfong_share"/$build_tag/$Cust_Name
				if [ $? != 0 ];
				then
					blat -subject "Package_Cust: Error creating customer subdirectory." \
					-body "Problem creating the Customer specific subdirectory" \
					-to $Notify
				fi
			fi

			#
			# zip up the files into the proper directory structure.  
			# 1. SMIWeb.war
			cd "$jfong_clean"/SMI_Customers/$Cust_Name/Code/WebApp
			# ls -la 

			#
			# First, check to make sure the WEB-INF directory exists since 
			# starting with 2.2.0, all customizations are stored in files 
			# in the $SMI_HOME/conf directory.  
			# zip up the WEB-INF customizations, but exclude the web.xml.  
			# All web customization are done in customer.properties now
			#

			if [ -s ./conf/customer.properties ] && [ -s ./web/WEB-INF/classes/customer.properties ];
			then
				rm -rf ./web/WEB-INF/classes/customer.properties
				error_count=error_count+$?
				echo "removed customer.properties from classes directory, error status is " $error_count
			else
				if [ -s ./web/WEB-INF/classes/customer.properties ];
				then
					mv ./web/WEB-INF/classes/customer.properties ./conf
					error_count=error_count+$?
					echo "copied customer.properties to conf directory, error status is " $error_count
				else
					echo "nothing to do, no customer.properties file to move to the conf directory"
				fi
			fi

			if [ -s ./conf/customer-dev.properties ] && [ -s ./web/WEB-INF/classes/customer-dev.properties ];
			then
				rm -rf ./web/WEB-INF/classes/customer-dev.properties
				error_count=error_count+$?
				echo "removed customer-dev.properties from classes directory, error status is " $error_count
			else
				if [ -s ./web/WEB-INF/classes/customer-dev.properties ];
				then
					mv ./web/WEB-INF/classes/customer-dev.properties ./conf
					error_count=error_count+$?
					echo "copied customer-dev.properties to conf directory, error status is " $error_count
				else
					echo "nothing to do, no customer-dev.properties file to move to the conf directory"
				fi
			fi



			# zip up the logo file
			cd ./web
			if [ -d ./images ]; 
			then
				zip -r "$jfong_share"/$build_tag/$Cust_Name/$Cust_Name.zip ./images
				error_count=error_count+$?
				echo $error_count
			else
				echo "no images to save"
			fi

			# zip up the customer.css.  
			# under the style directory.
			if [ -s ./style/customer.css ];
			then
				zip "$jfong_share"/$build_tag/$Cust_Name/$Cust_Name.zip ./style/customer.css
				error_count=error_count+$?
				echo $error_count
			fi

			cd "$jfong_clean"/SMI_Customer/$Cust_Name/Code/WebApp/web 
			if [ `ls . | wc -l` -gt 0 ]; 
			then
			    zip -r "$jfong_share"/$build_tag/$Cust_Name/$Cust_Name.zip .
			    error_count=error_count+$?
			    echo $error_count
			fi

			if [ $error_count -ne 0 ];
			then
				blat -subject "Package_Cust: Problems adding " $Cust_Name " files SMIWeb.war." \
				-body "Either zip had problems adding files to the SMIWeb.war file or I'm missing some directories" \
				-to $Notify
				exit 1
			fi
			#
			# 2. <Cust_Name>.zip
			#
			error_count=0

			cd "$jfong_clean"/SMI_Customers/$Cust_Name/Code/WebApp
			error_count=error_count+$?
			echo $error_count

			pwd
			zip -r "$jfong_share"/$build_tag/$Cust_Name/$Cust_Name.zip ./catalog
			error_count=error_count+$?
			echo $error_count

			pwd
			zip -r "$jfong_share"/$build_tag/$Cust_Name/$Cust_Name.zip ./conf
			error_count=error_count+$?
			echo $error_count

			if [ $error_count -ne 0 ];
			then
				blat -subject "Package_Cust: Problems creating the " $Cust_Name".zip file." \
				-body "Either we had problems creating the zip file or we do not have privilege t create the file" \
				-to $Notify
				exit
			fi
		fi
	done
fi

#
# Create the Decrypt files
#

cd "$jfong_clean"/Decrypt

echo "Clean out everything for Decrypt"
ant clean
if [ $? != 0 ];
then
	blat -subject "SMIBuild: clean env for Decrypt had errors" \
	-body "Exiting nightly build since there was clean error with ant Decrypt" \
	-to $Notify
	exit -1
fi

echo "Build everything for Decrypt"
ant  
if [ $? != 0 ];
then
	blat -subject "SMIBuild: Build all for Decrypt had an error " \
	-body "Exiting nightly build since there was a build error with ant Decrypt" \
	-to $Notify
	exit -1
fi

echo "copy over the files to the shared drive"

cp "$jfong_clean"/Decrypt/decrypt.war "$jfong_share"/$build_tag


#
# add to the change log.  
#
#if [ $DEBUG -eq 0 ]; 
#then
#	/cygdrive/c/Program\ Files/CVSNT/cvs -d :pserver:jfong:jfong123@SMISRC:/CVS rlog -S -N -d "$date_range1<=$date_range2" Decrypt  >>"$jfong_share"/$build_tag/changes.log
#fi


blat -subject "SMIBuild: completed without errors." \
-body "Build $build_tag finished without errors"  \
-to $Notify

exit 0
#################################################

List to todos

- attempt to persist the aggregate (temp) tables and delete them afterwards

- create aggegates

- seed the cache

- Break up sections to sub problems so I can run them separately

- parameterize the environment variables so we can change environments (build from a tag or branch)

- cleanup startup and shutdown scripts to generate errors

- generate a list of diffs.
	cvs -d :pserver:jfong:jfong0p;@SMISRC:/CVS log -N -S -rSMI_Nightly_200703160128:SMI_Nightly_20070315012801
	
- generate a list of differences between tags
	$ /cygdrive/c/Program\ Files/CVSNT/cvs -d :pserver:jfong:jfong123@SMISRC:/CVS diff -r Root_SMI_1_5_0_SP  -r SMI_1_5_1 ./SMI_1_5/  >cvs_diff2.log
