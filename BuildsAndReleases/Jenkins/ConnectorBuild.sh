﻿#!/usr/bin/bash
#
ANT_OPTS="-Xms512m -Xmx512m -XX:MaxPermSize=512m -XX:ReservedCodeCacheSize=96m";	export ANT_OPTS

function Parameter_Help
{
echo "usage: Connectorbuild [-options]"
			echo "where options include:"
			echo " -c <clean working directory>"
			echo "		This is the clean area we will be exporting "
			echo "		and building from.  Everything will be deleted "
			echo "		from this directory tree before we start"
			echo "		e.g. \c\clean_build_SMI_2_2_0_SP"
			echo " -n <email notification list> comma separated"
			echo " 		default=jfong@birst.com,4157301327@vtext.com"
			echo " -t <build tag> "
			echo " 		Usually, this is the date and time  with or without a "
			echo " 		release number.  For example, 200802140805 "
			echo " 		is Feb 14th, 2008 8:05 a.m.  Or if it for 2.2.0, "
			echo "		the tag would be 200802140805_220"
}
# Define variables
# 	and clear them out first. 
#
Notify=jfong@birst.com
jfong_share=/cygdrive/c/shared/Builds;
jfong_win_share=c:\\shared\\Builds;
build_tag=""
Branch_Name=""
jfong_clean=""
CVS_workspace=""
declare -i error_count

while getopts ":b:c:n:t:" opts; do
        case "$opts" in
		b ) echo "-b $OPTARG" 
			Branch_Name=$OPTARG
			echo $Branch_Name
			;;
		c ) echo "-c $OPTARG"
			jfong_clean="$(cygpath -u -p -a "$OPTARG")"
			echo $jfong_clean
			;;
		n ) echo "-n $OPTARG" 
			Notify=$OPTARG
			export Notify 
			;;
		t ) echo "-t $OPTARG"			# Since I'm creating the build, I am generating the build tag.  
			build_tag=$OPTARG;	
			export build_tag
			;;
		*  ) Parameter_Help 
			exit 1 ;;
	esac
	echo one value $1
done

echo "workspace = " $jfong_clean

if [ -z $build_tag ] || [ "$jfong_clean" == "" ] || [ "$Branch_Name" == "" ];	#  Make sure we have our parameters
then
	Parameter_Help
	exit 1
fi
	
#JAVA_HOME=`cygpath -u "C:\Program Files (x86)\Java\jdk1.7.0"`
JAVA_HOME=`cygpath -u "C:\Program Files\Java\jdk1.7.0"`
echo $JAVA_HOME

#
# Build the BirstConnectorAPI
# 

cd "$jfong_clean"/BirstConnectorAPI
pwd

ant clean
stat=$?
echo BirstConnectorAPI clean exit status $stat

if [ $stat == 0 ]; 
then
	echo "BirstConnectorAPI ant clean worked"
else
	blat -subject "BirstConnectorAPI: ant clean" -body "BirstConnectorAPI ant clean had errors" -to $Notify
	exit -1
fi


ant -verbose 
stat=$?
echo BirstConnectorAPI build exit status $stat

if [ $stat == 0 ]; 
then
	echo "BirstConnectorAPI ant all worked"
else
	blat -subject "BirstConnectorAPI: ant all" -body "BirstConnectorAPI ant all had errors" -to $Notify
	exit -1
fi

#
# Build the IConnectorWeb
# 

cd "$jfong_clean"/IConnectorWeb
pwd

ant clean
stat=$?
echo IConnectorWeb clean exit status $stat

if [ $stat == 0 ]; 
then
	echo "IConnectorWeb ant clean worked"
else
	blat -subject "IConnectorWeb: ant clean" -body "IConnectorWeb ant clean had errors" -to $Notify
	exit -1
fi


ant -verbose 
stat=$?
echo IConnectorWeb build exit status $stat

if [ $stat == 0 ]; 
then
	echo "IConnectorWeb ant all worked"
else
	blat -subject "IConnectorWeb: ant all" -body "IConnectorWeb ant all had errors" -to $Notify
	exit -1
fi


#
# Build the war file for the connectors. 
# 
Connector_version[1]="ConnectorController"
Connector_version[2]="SalesforceConnector_26_0"
Connector_version[3]="SalesforceConnector_28_0"

for ((index=1; index<=3; index++))
do
	printf "     %s\n" "${Connector_version[index]}"
	error_count=0


	cd "$jfong_clean"/${Connector_version[index]} 
	pwd

	ant clean
	stat=$?
	echo ${Connector_version[index]}  clean exit status $stat

	if [ $stat == 0 ]; 
	then
		echo "${Connector_version[index]}  ant clean worked"
	else
		blat -subject "${Connector_version[index]} : ant clean" -body "${Connector_version[index]}  ant clean had errors" -to $Notify
		exit -1
	fi


	ant -verbose 
	stat=$?
	echo ${Connector_version[index]}  build exit status $stat

	if [ $stat == 0 ]; 
	then
		echo "${Connector_version[index]}  ant all worked"
	else
		blat -subject "${Connector_version[index]} : ant all" -body "${Connector_version[index]}  ant all had errors" -to $Notify
		exit -1
	fi

	if [ -e "$jfong_clean"/${Connector_version[index]}/deployable_units/${Connector_version[index]}.war ]; 
	then
		echo "${Connector_version[index]}.war was created successfully"
	else
		blat -subject "${Connector_version[index]}.war" -body "${Connector_version[index]}.war does not exist in the deployable_units directory." -to $Notify
		exit -1
	fi

	cp "$jfong_clean"/${Connector_version[index]}/deployable_units/${Connector_version[index]}.war "$jfong_share"/$build_tag/connectors
	if [ -e "$jfong_share"/$build_tag/connectors/${Connector_version[index]}.war ]; 
	then
		echo "${Connector_version[index]}.war was copied successfully"
	else
		blat -subject "${Connector_version[index]}.war" -body "Copy error, ${Connector_version[index]}.war does not exist in the distribution directory." -to $Notify
		exit -1
	fi


done

#
# 	Package the config files into Connector_confs.zip
#

cd "$jfong_clean"/ConnectorController/conf

/cygdrive/c//Program\ Files\ \(x86\)/WinZip/WZZIP.EXE -ex -r -P -a "`cygpath -w "$jfong_clean/ConnectorController/deployable_units/Connector_configs.zip"`" *
if [ $? != 0 ];
then

	blat -subject "ConnectorBuild: Package conf files failed  " \
	-body "There were issues with winzip packaging up the Connector configuration files." \
	-to $Notify
	exit -1
fi

cp "$jfong_clean"/ConnectorController/deployable_units/Connector_configs.zip "$jfong_share"/$build_tag/connectors
if [ -e "$jfong_share"/$build_tag/connectors/Connector_configs.zip ]; 
then
	echo "Connector_configs.zip was copied successfully"
else
	blat -subject "Connector_configs.zip" -body "Copy error, Connector_configs.zip does not exist in the distribution directory." -to $Notify
	exit -1
fi

#
# copy the Jars to the Everest Project
# 

error_count=0
cp "$jfong_clean"/BirstConnectorAPI/deployable_units/birst-connector-api-1.0.0.jar "$jfong_clean"/../../ConnectorJars/$Branch_Name/
error_count=$error_count+$?
cp "$jfong_clean"/BirstConnectorAPI/deployable_units/birst-connector-api-1.0.0.jar "$jfong_clean"/../../ConnectorJars/master/
error_count=$error_count+$?

cp "$jfong_clean"/BirstConnectorAPI/deployable_units/birst-connector-api-1.0.0-javadoc.jar "$jfong_clean"/../../ConnectorJars/$Branch_Name/
error_count=$error_count+$?
cp "$jfong_clean"/BirstConnectorAPI/deployable_units/birst-connector-api-1.0.0-javadoc.jar "$jfong_clean"/../../ConnectorJars/master/
error_count=$error_count+$?

cp "$jfong_clean"/IConnectorWeb/deployable_units/IConnectorWeb.jar "$jfong_clean"/../../ConnectorJars/$Branch_Name/
error_count=$error_count+$?
cp "$jfong_clean"/IConnectorWeb/deployable_units/IConnectorWeb.jar "$jfong_clean"/../../ConnectorJars/master/
error_count=$error_count+$?

cp "$jfong_clean"/IConnectorWeb/deployable_units/IConnectorWeb-javadoc.jar "$jfong_clean"/../../ConnectorJars/$Branch_Name/
error_count=$error_count+$?
cp "$jfong_clean"/IConnectorWeb/deployable_units/IConnectorWeb-javadoc.jar "$jfong_clean"/../../ConnectorJars/master/
error_count=$error_count+$?

if [ $error_count != 0 ];
then

	blat -subject "ConnectorBuild: Copying Jar files encountered problem  " 
	-body "Exiting ConnectorBuild we had issues copying the jar files over to the temporary holding area." \
	-to $Notify
	exit -1
fi

exit 0

