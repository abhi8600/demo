#!/bin/bash.exe
declare -i error_count
error_count=0

function Parameter_Help
{
echo "usage: deploy [-options]"
			echo "where options include:"
			echo " -r <release directory>"
			echo "		This is the release area we will be deploying "
			echo "		to this directory tree "
			echo "		e.g. \c\BIRST_HOME\BIRST"
			echo " -i <Instance you are deploying^>"
			echo "		Instance you are deploying to (e.g sfoqa1, sde, prod)"
			echo " -n <email notification list> comma separated"
			echo " 		default=jfong@birst.com,4157301327@vtext.com"
			echo " -t <build tag> "
			echo " 		Usually, this is the date and time  with or without a "
			echo " 		release number.  For example, 200802140805 "
			echo " 		is Feb 14th, 2008 8:05 a.m.  Or if it for 2.2.0, "
			echo "		the tag would be 200802140805_220"
}
# Define variables
# 	and clear them out first. 
#
Notify=jfong@birst.com
build_tag=""
Branch_Name=""
release_dir=""
Instance=""
CVS_workspace=""
delete_me="/cygdrive/d/delete_me/No_ftp"
 TimeStamp=`date +"%Y%m%e%H%M"`

SevenZip=""
if [ -f "/cygdrive/c/Program Files/7-Zip/7z.exe" ]
then
	SevenZip="/cygdrive/c/Program Files/7-Zip"
else if [ -f "/cygdrive/c/Program Files (x86)/7-Zip/7z.exe" ] 
	then
		set SevenZip="/cygdrive/c/Program Files (x86)/7-Zip"
	fi 
fi 

echo $SevenZip
if [ "$SevenZip" == "" ]
then
	blat -subject "Deploy_qa.sh on $COMPUTERNAME: 7Zip error " -body "Missing 7zip. Cannot file 7Zip in the default 32 and 64 bit configurations. " -to $Notify
	exit -1
fi
 
while getopts "r:i:n:t:" opts; do
		echo "$opts"
        case "$opts" in
		r ) echo "-r $OPTARG"
			release_dir="$(cygpath -u -p -a "$OPTARG")"
			echo $release_dir
			;;
		i ) echo "-i $OPTARG" 
			Instance=$OPTARG
			export Instance 
			;;
		n ) echo "-n $OPTARG" 
			Notify=$OPTARG
			export Notify 
			;;
		t ) echo "-t $OPTARG"			# Since I'm creating the build, I am generating the build tag.  
			build_tag=$OPTARG;	
			export build_tag
			;;
		*  ) Parameter_Help 
			exit 1 ;;
	esac
	echo one value $1
done

echo "workspace = " $release_dir

if [ -z $build_tag ] || [ "$release_dir" == "" ] || [ "$Instance" == "" ];	#  Make sure we have our parameters
then
	Parameter_Help
	exit 1
fi
	
if [  -f "$delete_me/QA.zip" ]; 
then
	echo QA.zip file is available
else
	blat -subject "Deploy_qa.sh on $COMPUTERNAME: QA.zip" -body "Missing QA.zip file, please verify the file is available." -to $Notify
	exit -1
fi
	
#
# Update the QA files
#

cd "$release_dir/ATC"

"$SevenZip/7z.exe" x -y -r "d:\delete_me\No_ftp\QA.zip"

#
# update the AcornTestConsole.exe.config
#

cd  $release_dir/ATC/QA/bin
/bin/dos2unix.exe ./AcornTestConsole.exe.config

sed '1,$s^key=\"ConfigLocation\" value=\"c\:\\dev\\Main\\Acorn\"^key=\"ConfigLocation\"\ value=\"D\:\\BIRST_HOME\\BIRST\\Acorn\"^' <./AcornTestConsole.exe.config >./AcornTestConsole.exe.config.temp
diff ./AcornTestConsole.exe.config ./AcornTestConsole.exe.config.temp
error_count=$error_count+$?-1

sed '1,$s^key=\"TestUser\" value=\"mpandit@birst.com\"^key=\"TestUser\" value=\"atc@birst.com\"^' <./AcornTestConsole.exe.config.temp >./AcornTestConsole.exe.config.temp1
diff ./AcornTestConsole.exe.config.temp ./AcornTestConsole.exe.config.temp1
error_count=$error_count+$?-1

sed '1,$s^key=\"DCConfigLocation\" value=\"c\:\\dev\\Main\\QA\\bin\"^key=\"DCConfigLocation\" value=\"D\:\\BIRST_HOME\\BIRST\\ATC\\QA\\bin\"^' <./AcornTestConsole.exe.config.temp1 >./AcornTestConsole.exe.config.temp2
diff ./AcornTestConsole.exe.config.temp1 ./AcornTestConsole.exe.config.temp2
error_count=$error_count+$?-1

sed '1,$s^key=\"ATCWebDAVURL\" value=\"amdmpandit\:57747\"^key=\"ATCWebDAVURL\" value=\"localhost\:6105\"^' <./AcornTestConsole.exe.config.temp2 >./AcornTestConsole.exe.config.temp3
diff ./AcornTestConsole.exe.config.temp2 ./AcornTestConsole.exe.config.temp3
error_count=$error_count+$?-1

sed '1,$s^key=\"RunQueryCommand\" value=\"C\:\\SMIPerfEngine\\Main\\bin\\run.bat\"^key=\"RunQueryCommand\" value=\"D\:\\BIRST_HOME\\BIRST\\PerfEngines\\default\\bin\\run.bat\"^' <./AcornTestConsole.exe.config.temp3 >./AcornTestConsole.exe.config.temp4
diff ./AcornTestConsole.exe.config.temp3 ./AcornTestConsole.exe.config.temp4
error_count=$error_count+$?-1

sed '1,$s^key=\"ATC_TOMCAT_TEMP\" value=\"C\:\\dev\\Main\\tomcat\\temp\\ATC\"^key=\"ATC_TOMCAT_TEMP\" value=\"D\:\\BIRST_HOME\\BIRST\\SMIWeb\\tomcat\\temp\\ATC\"^' <./AcornTestConsole.exe.config.temp4 >./AcornTestConsole.exe.config.temp5
diff ./AcornTestConsole.exe.config.temp4 ./AcornTestConsole.exe.config.temp5
error_count=$error_count+$?-1

sed '1,$s^key=\"BirstWebService_BaseURL\" value=\"http\://amdmpandit\:57747\"^key=\"BirstWebService_BaseURL\" value=\"http\://localhost\:6015\"^' <./AcornTestConsole.exe.config.temp5 >./AcornTestConsole.exe.config.temp6
diff ./AcornTestConsole.exe.config.temp5 ./AcornTestConsole.exe.config.temp6
error_count=$error_count+$?-1

cp ./AcornTestConsole.exe.config.temp6 ./AcornTestConsole.exe.config
diff ./AcornTestConsole.exe.config ./AcornTestConsole.exe.config.temp5
error_count=$error_count+$?-1

if [ $error_count == 0 ]; 
then
	echo "Customization of the AcornTestConsole.exe.config worked"
	rm ./AcornTestConsole.exe.config.temp*
else
	blat -subject "Deploy_qa.sh on $COMPUTERNAME: Cusomization of AcornTestConsole.exe.config" -body "Customization of the AcornTestConsole.exe.config had generated errors, cusomtization failed." -to $Notify
	exit -1
fi

cd  $release_dir/ATC/QA/bin

/bin/dos2unix.exe ./QA_Master1.bat
sed '1,$s^ATC_HOME=E\:\\Birst\\Main^ATC_HOME=D\:\\BIRST_HOME\\BIRST\\ATC^' <./QA_Master1.bat >./QA_Master1.bat.temp1
diff ./QA_Master1.bat ./QA_Master1.bat.temp1
error_count=$error_count+$?-1

sed '1,$s^SPACE_DIR=E\:\\Birst\\Main\\Data^SPACE_DIR=\\\\BirstMetaData\\Repositories\\data^' <./QA_Master1.bat.temp1 >./QA_Master1.bat.temp2
diff ./QA_Master1.bat.temp1 ./QA_Master1.bat.temp2
error_count=$error_count+$?-1

sed '1,$s^JAVA_HOME=C\:\\Program\ Files\\Java\\jdk1\.7\.0\_01^JAVA_HOME=D\:\\BIRST_HOME\\BIRST\\SMIWeb\\jdk^' <./QA_Master1.bat.temp2 >./QA_Master1.bat.temp3
diff ./QA_Master1.bat.temp2 ./QA_Master1.bat.temp3
error_count=$error_count+$?-1

sed '1,$s^SMI_HOME=E\:\\Birst\\Main\\PerfEngine^SMI_HOME=D\:\\BIRST_HOME\\BIRST\\PerfEngines\\default^' <./QA_Master1.bat.temp3 >./QA_Master1.bat.temp4
diff ./QA_Master1.bat.temp3 ./QA_Master1.bat.temp4
error_count=$error_count+$?-1

sed '1,$s^INSTALL_DIR=E\:\\Birst\\Main\\PerfEngine^INSTALL_DIR=D\:\\BIRST_HOME\\BIRST\\PerfEngines\\default^' <./QA_Master1.bat.temp4 >./QA_Master1.bat.temp5
diff ./QA_Master1.bat.temp4 ./QA_Master1.bat.temp5
error_count=$error_count+$?-1

echo %$error_count
cp ./QA_Master1.bat.temp5 ./QA_Master1.bat
diff ./QA_Master1.bat ./QA_Master1.bat.temp4
error_count=$error_count+$?-1

if [ $error_count == 0 ]; 
then
	echo "Customization of the QA_Master1.bat worked"
	rm ./QA_Master1.bat.temp*
else
	blat -subject "Deploy_qa.sh on $COMPUTERNAME: Cusomization of QA_Master1.bat" -body "Customization of the QA_Master1.bat had generated errors, cusomtization failed." -to $Notify
	exit -1
fi

exit



