ECHO ON
REM 
REM These should be input parameters in the future
REM
echo %1% %2% %3% %4%
set Scheduler_Root=""
set Release_Dir=""
set Build_Tag=""
set Scheduler_Port=""
set TimeStamp=""
set Instance=""
set CurrentDir=%CD%

REM 
REM Setup parameters for notification
REM 

set VarNotify=jfong@successmetricsinc.com
set VarMailText=Setup email message
set VarMailSubject=Deploy SCHEDULER

:PROCESS_INPUT

if "%1"=="-a" goto SET_SCHEDULER:
if "%1"=="-n" goto SET_NOTIFICATION:
if "%1"=="-p" goto SET_SCHEDULER_PORT:
if "%1"=="-k" goto SET_CLEAN_DIR:
if "%1"=="-r" goto SET_RELEASE_DIR:
if "%1"=="-s" goto SET_TIMESTAMP:
if "%1"=="-t" goto SET_BUILD_TAG:
if "%1"=="-i" goto SET_INSTANCE:
if "%1"=="" goto PROCESS_INPUT_DONE:

goto PROCESS_INPUT_ERROR:

:SET_TIMESTAMP
shift
set TimeStamp=%1
goto PROCESS_INPUT_LOOP:

:SET_SCHEDULER
shift
set Scheduler_Root=%1
goto PROCESS_INPUT_LOOP:

:SET_RELEASE_DIR
shift
set Release_Dir=%1
goto PROCESS_INPUT_LOOP:

:SET_INSTANCE
shift
set Instance=%1
goto PROCESS_INPUT_LOOP:

:SET_BUILD_TAG
shift
set Build_Tag=%1
goto PROCESS_INPUT_LOOP:

:SET_SCHEDULER_PORT
shift
set Scheduler_Port=%1
goto PROCESS_INPUT_LOOP:

:SET_NOTIFICATION
shift
set VarNotify=%1
goto PROCESS_INPUT_LOOP:

:PROCESS_INPUT_LOOP

shift
goto PROCESS_INPUT:

:PROCESS_INPUT_ERROR

echo ERROR: invalid input parameter
echo usage: deploy_scheduler [-options]
echo where options include:
echo.
echo  -s ^<Time Stamp^> (optional)
echo 		Enter the Time Stamp for identifying the deployment
echo		the format is YYYYMMDDHHMM, or one will be generated.		
echo  -a ^<scheduler root^>
echo 		Enter the home directory of the instance, the
echo 		default is usually  c:\SMI
echo 		This is the parent directory of Acorn, SMIWeb and Scheduler
echo  -p ^<port number^>
echo 		Enter 4 digit port number for the installation	
echo  		Installation will use port number +2
echo 		e.g. 9300, 9301, 9302
echo  -r ^<Release directory^> 
echo  		Which do you want to get the release from?
echo 		Nightly for nightly builds
echo 		Releases/Platform for version releases
echo  -i ^<Instance you are deploying^>
echo            Instance you are deploying to (e.g sfoqa1, sde, prod)
echo  -t ^<Build tag^> 
echo  		If you are getting files from Releases/Platform,
echo 		enter a Release Number e.g. 2.0.0
echo 		If you are getting files from Nightly, 
echo 		enter a date and time e.g. 200705230130
echo  -n ^<email notification list^> comma separated (optional)
echo  		default=jfong@successmetricsinc.com,4157301327@vtext.com
echo 		NOTE - mulitple email addresses need to be separated by 
echo 			commas and inclosed in double quote
echo.
echo Note: the following files needs to be in the Saved_stuff\config_base directory
echo 	chart-service.xml
echo 	customer.properties
echo 	NewUserSpaces.config
echo 	Products.config
echo 	securityfilter-config.xml
echo 	Spaces.config
echo 	Web.config
echo 	web.xml
echo 	

set VarMailText=Invalid arguement(s) provided. 
set VarMailSubject=Scheduler_Deploy:Process Arguements
goto BADEXIT

:PROCESS_INPUT_DONE

for /f "delims=:, tokens=1,2" %%A in ("%Scheduler_Root%") do ( set DriveLetter=%%A )
set DriveLetter=%DriveLetter: =%

for /f "delims=\, tokens=1-4" %%A in ("%Scheduler_Root%") do ( 
	echo %%A-%%B-%%C-%%D 
	set temp=%%B
	)

echo Install_name=%Install_Name%-

set Install_Name=%temp: =%
set IIS_NAME=%Install_Name%
if %Install_Name%==SMI ( set Install_Name=Scheduler )
if %Install_Name%==SMI1 ( set Install_Name=Scheduler )
set Install_Name=%Install_Name: =%


echo Install_Name=%Install_Name%-
echo IIS_Name=%IIS_Name%-

echo TimeStamp		= %TimeStamp%
echo VarNotify		= %VarNotify%
echo Scheduler_Root	= %Scheduler_Root%
echo DriveLetter	= %DriveLetter%
echo Release_Dir	= %Release_Dir%
echo Build_Tag		= %Build_Tag%
echo Scheduler_Port	= %Scheduler_Port%

if %Scheduler_Root%=="" ( goto PROCESS_INPUT_ERROR )
if %Release_Dir%=="" ( goto PROCESS_INPUT_ERROR )
if %Build_Tag%=="" ( goto PROCESS_INPUT_ERROR )
if %Scheduler_Port%=="" ( goto PROCESS_INPUT_ERROR )
if %Instance%=="" ( goto PROCESS_INPUT_ERROR )

set SCHEDULER_HOME="%Scheduler_Root%\Scheduler"
echo %SCHEDULER_HOME%

REM
REM We need a timestamp.  If it is not provided, we should create our own.
REM

if %TimeStamp% neq "" ( goto TIMESTAMP_DEFINED: ) 
	
for /f "tokens=1-3 delims=:." %%a in ("%time%") do set hh_mm_stamp=%%a%%b
echo %hh_mm_stamp%
echo %hh_mm_stamp:~0,1%
echo %hh_mm_stamp:~1,3%
if "%hh_mm_stamp:~0,1%" == " " ( set hh_mm_stamp=0%hh_mm_stamp:~1,3%)
echo %hh_mm_stamp%
for /f "tokens=1-3 delims=\ " %%a in ("%date%") do set date_only=%%b
for /f "tokens=1-3 delims=/." %%a in ("%date_only%") do set teststamp=%%c%%a%%b
echo %teststamp%
set TimeStamp=%teststamp%%hh_mm_stamp%
	
:TIMESTAMP_DEFINED
echo TimeStamp: -%TimeStamp%-

set DeleteMeDir=%DriveLetter%:\delete_me\%TimeStamp%
echo DeleteMeDir: -%DeleteMeDir%-

REM
REM Goto the same drive as the Scheduler installation
REM

%DriveLetter%:

REM 
REM Setup the delete_me directory
REM 

dir %DeleteMeDir%
set /a Compare_Error_Cnt=%ERRORLEVEL% 
if %Compare_Error_Cnt% EQU 0 ( del /F /Q %DeleteMeDir%\* 
	) else (
	mkdir %DeleteMeDir% )

REM
REM where is winzip?
REM This handles the standard 32 and 64 bit locations
REM

set WinZip_Home=""
if exist "C:\Program Files\WinZip" (set WinZip_Home="c:\Program Files\WinZip")
if exist "C:\Program Files (x86)\WinZip" (set WinZip_Home="C:\Program Files (x86)\WinZip")
if %WinZip_Home% neq "" (goto WinZip_Found:)
echo We can't find the WinZip directory"
set VarMailText=Cannot file WinZip in the default 32 and 64 bit configurations. 
set VarMailSubject=Scheduler_Deploy:WinZip directory missing
goto BADEXIT


:WinZip_Found
REM
REM 
REM --------------------------------------------------------------------------------------------
REM Backup the configuration files
REM
set /a Compare_Error_Cnt=0
set /a Compare_Error_Cnt=%Compare_Error_Cnt%+%ERRORLEVEL%

mkdir "%Scheduler_Root%\Saved_stuff\%TimeStamp%"
IF %ERRORLEVEL% LSS 1 goto CREATED_ARCHIVE_DIR:
set VarMailText=There were problems creating the archive directory. 
set VarMailSubject=Scheduler_Deploy:Config archive problems
goto BADEXIT

:CREATED_ARCHIVE_DIR
cd "%Scheduler_Root%\Saved_stuff\%TimeStamp%"
IF %ERRORLEVEL% LSS 1 goto CD_TO_ARCHIVE_DIR:
set VarMailText=There were problems changing to the archive directory. 
set VarMailSubject=Scheduler_Deploy:Config archive problems
goto BADEXIT

:CD_TO_ARCHIVE_DIR
copy /V /Y %SCHEDULER_HOME%\config\*.properties .
set /a Compare_Error_Cnt=%Compare_Error_Cnt%+%ERRORLEVEL%

copy /V /Y %SCHEDULER_HOME%\tomcat\webapps\SCHEDULER\WEB-INF\web.xml .
set /a Compare_Error_Cnt=%Compare_Error_Cnt%+%ERRORLEVEL%

copy /V /Y %SCHEDULER_HOME%\tomcat\webapps\SCHEDULER\WEB-INF\axis2.xml .
set /a Compare_Error_Cnt=%Compare_Error_Cnt%+%ERRORLEVEL%

copy /V /Y %SCHEDULER_HOME%\tomcat\webapps\SCHEDULER\WEB-INF\classes\log4j.xml .
set /a Compare_Error_Cnt=%Compare_Error_Cnt%+%ERRORLEVEL%

copy /V /Y %SCHEDULER_HOME%\tomcat\webapps\SCHEDULER\WEB-INF\classes\schedulerQuartz.properties .
set /a Compare_Error_Cnt=%Compare_Error_Cnt%+%ERRORLEVEL%

IF %Compare_Error_Cnt%  LSS 1 goto CONFIG_ARCHIVED
set VarMailSubject="Scheduler_Deploy:Config copy had issues, manual intervention needed"
set VarMailText="Manual review of deployment necessary since we had problems copying config files."
got BADEXIT

:CONFIG_ARCHIVED

REM
REM Get the data files
REM

cd %DeleteMeDir%
copy %DriveLetter%:\delete_me\No_ftp\*  %DeleteMeDir%
dir

REM echo open builds >download.ftp
REM echo smibuilds>>download.ftp
REM echo 5771builds!>>download.ftp
REM echo verbose >>download.ftp
REM echo cd   %Release_Dir%/%Build_Tag% >>download.ftp
REM echo prompt >>download.ftp
REM echo get Scheduler.zip >>download.ftp
REM echo quit >>download.ftp

REM ftp -s:download.ftp   > download.log 2>&1

REM findstr /C:"Connection closed by remote host." download.log
REM IF %ERRORLEVEL% EQU 1 goto CONNECTION_NOT_CLOSED:
REM set VarMailText=FTP connection closed by remote host. 
REM set VarMailSubject=Scheduler_Deploy:FTP
REM goto BADEXIT

REM :CONNECTION_NOT_CLOSED
REM findstr /C:"Not connected" download.log
REM IF %ERRORLEVEL% EQU 1 goto NOT_CONNECTION:
REM set VarMailText=FTP not connected anymore.
REM set VarMailSubject=Scheduler_Deploy:FTP
REM goto BADEXIT

:NOT_CONNECTION
REM echo NULL >download.ftp
REM rm download.ftp

if not exist Scheduler.zip (goto FTP_Problems:)
goto Start_Upgrade:

:FTP_Problems
set VarMailText=One or more of the FTP files were not recieved.  Please investigate the problem.  
set VarMailSubject=Scheduler_Deploy:FTP issues
goto BADEXIT


:Start_Upgrade


REM 
REM --------------------------------------------------------------------------------------------
REM upgrade SCHEDULER
REM
  Adding \css\classic\datasourcetest.css....    ..  


cd %SCHEDULER_HOME%\tomcat
%WinZip_Home%\WZZIP.EXE -ex -r -P -a -x.\logs\*.* -x.\tomcat\logs\*.* -x.\tomcat\webapps\probe\*.* %Scheduler_Root%\Saved_stuff\%TimeStamp%_Scheduler_platform.zip .
IF %ERRORLEVEL% LSS 1 goto BACKUP_SCHEDULER:
set VarMailText=Backup of the SCHEDULER environment failed, Please correct problem before proceeding. 
set VarMailSubject=Scheduler_Deploy:Scheduler Backup
goto BADEXIT

:BACKUP_SCHEDULER

echo "shutdown the server"
net stop %Install_Name%%Scheduler_Port%
if %ERRORLEVEL% == 2 (
	goto SHUTDOWN_DONE: 
	) else (
	if %ERRORLEVEL% == 0 ( goto SHUTDOWN_DONE: ))
set VarMailText=Shutdown of the SMIScheduler server did not complete normally. 
set VarMailSubject=Scheduler_Deploy:Shutdown did not complete
goto BADEXIT

:SHUTDOWN_DONE
REM sleep for 30 seconds
ping -n 30 127.0.0.1 > NUL 2>&1
 

:Update_SCHEDULER
cd %SCHEDULER_HOME%\tomcat\webapps
rmdir /S /Q %SCHEDULER_HOME%\tomcat\webapps\SCHEDULER
IF %ERRORLEVEL% LSS 1 goto SCHEDULER_RMDIR_DONE:
set VarMailText=SCHEDULER had problems removing SCHEDULER, Please correct problem before proceeding. 
set VarMailSubject=Scheduler_Deploy:SCHEDULER rmdir
goto BADEXIT

:SCHEDULER_RMDIR_DONE
del %SCHEDULER_HOME%\tomcat\webapps\SCHEDULER.war
IF %ERRORLEVEL% LSS 1 goto SCHEDULER_DEL_DONE:
set VarMailText=SCHEDULER had problems deleting SCHEDULER.war, Please correct problem before proceeding. 
set VarMailSubject=Scheduler_Deploy:SCHEDULER Del
goto BADEXIT

:SCHEDULER_DEL_DONE
dir

cd %SCHEDULER_HOME%

%WinZip_Home%\WZUNZIP.EXE -d -ybc -o -yo "%DeleteMeDir%\Scheduler.zip" %SCHEDULER_HOME%
IF %ERRORLEVEL% LSS 1 goto SCHEDULER_UNZIP_DONE:
set VarMailText=SCHEDULER had problems Unzipping, Please correct problem before proceeding. 
set VarMailSubject=Scheduler_Deploy:SCHEDULER unzip
goto BADEXIT

:SCHEDULER_UNZIP_DONE

echo "startup the server"
net start %Install_Name%%Scheduler_Port%
if %ERRORLEVEL% == 0  ( goto STARTUP_DONE:)
REM sleep for 30 seconds
ping -n 30 127.0.0.1 > NUL 2>&1
for /f "tokens=2-4" %%a in ('sc query %Install_Name%%Scheduler_Port% ^| findstr RUNNING') do if %%c==RUNNING ( goto STARTUP_DONE:)
set VarMailText=Startup of the SCHEDULER server did not startup normally. 
set VarMailSubject=Scheduler_Deploy:SCHEDULER startup did not complete
goto BADEXIT

:STARTUP_DONE
REM sleep for 30 seconds
 ping -n 30 127.0.0.1 > NUL 2>&1
 
REM
REM Save the most recent configuration files.
REM
if not exist "%Scheduler_Root%\SetupTools\most_current\Scheduler" (mkdir "%Scheduler_Root%\SetupTools\most_current\Scheduler")
copy /V /Y %SCHEDULER_HOME%\config\overrides.config %Scheduler_Root%\SetupTools\most_current\Scheduler\
copy /V /Y %SCHEDULER_HOME%\tomcat\webapps\SCHEDULER\WEB-INF\web.xml %Scheduler_Root%\SetupTools\most_current\Scheduler\
copy /V /Y %SCHEDULER_HOME%\tomcat\webapps\SCHEDULER\WEB-INF\axis2.xml %Scheduler_Root%\SetupTools\most_current\
copy /V /Y %SCHEDULER_HOME%\tomcat\webapps\SCHEDULER\WEB-INF\classes\log4j.xml %Scheduler_Root%\SetupTools\most_current\Scheduler\
copy /V /Y %SCHEDULER_HOME%\tomcat\webapps\SCHEDULER\WEB-INF\classes\schedulerQuartz.properties %Scheduler_Root%\SetupTools\most_current\Scheduler\

:SCHEDULER_RESTARTED

REM
REM ----------------------------------------------------------------------------------------------
REM
REM Stop the servers so we can copy the configuration files to their proper place
REM


net stop %Install_Name%%Scheduler_Port%
if %ERRORLEVEL% == 2 (
	goto RESHUTDOWN_DONE: 
	) else (
	if %ERRORLEVEL% == 0 ( goto RESHUTDOWN_DONE: ))
set VarMailText=Restart/shutdown of the SCHEDULER server did not complete normally. 
set VarMailSubject=Scheduler_Deploy:SCHEDULER restart shutdown did not complete
goto BADEXIT

:RESHUTDOWN_DONE

REM sleep for 30 seconds
ping -n 30 127.0.0.1 > NUL 2>&1
 
REM
REM -----------------------------------------------------------------------------------------------
REM
REM Copy the custom configurations files into place
REM
REM Restore the old config/xml files.
REM

set /a Compare_Error_Cnt=0

cd %Scheduler_Root%\Saved_stuff\%TimeStamp%
set /a Compare_Error_Cnt=%Compare_Error_Cnt%+%ERRORLEVEL%

cd %SCHEDULER_HOME%\config
set /a Compare_Error_Cnt=%Compare_Error_Cnt%+%ERRORLEVEL%

copy /v /y %Instance%\*.properties .
set /a Compare_Error_Cnt=%Compare_Error_Cnt%+%ERRORLEVEL%

REM Put the proper Scheduler Service configuration in place for the specific server
REM It the configuration exists in the config file, use that, if not used our saved copy.

REM Put the proper axis2.xml for SCHEDULER in place for specific server.
if EXIST %Instance%\axis2.xml (
	copy /v /y  %Instance%\axis2.xml %SCHEDULER_HOME%\tomcat\webapps\SCHEDULER\WEB-INF\axis2.xml
	set /a Compare_Error_Cnt=%Compare_Error_Cnt%+%ERRORLEVEL% )

REM Put the proper web.xml for SCHEDULER in place for specific server.
if EXIST %Instance%\web.xml (
	copy /v /y  %Instance%\web.xml %SCHEDULER_HOME%\tomcat\webapps\SCHEDULER\WEB-INF\web.xml
	set /a Compare_Error_Cnt=%Compare_Error_Cnt%+%ERRORLEVEL% )

REM Put the proper schedulerQuartz.properties file in place for the specific server
if EXIST %Instance%\schedulerQuartz.properties (
	copy /v /y  %Instance%\schedulerQuartz.properties %SCHEDULER_HOME%\tomcat\webapps\SCHEDULER\WEB-INF\classes\schedulerQuartz.properties
	set /a Compare_Error_Cnt=%Compare_Error_Cnt%+%ERRORLEVEL% )

IF %Compare_Error_Cnt%  EQU 0 ( 
	set VarMailSubject="Scheduler_Deploy:Config copy successful" & set VarMailText="Copy of configuration files was successful." 
	) else (
	set VarMailSubject="Scheduler_Deploy:Config copy had issues, manual intervention needed" & set VarMailText="Manual review of deployment necessary since we had problems copying config files." ) 



REM
REM -----------------------------------------------------------------------------------------------
REM
REM Since we copied over files, restart the servers.
REM
:CHART_RESTART

net start %Install_Name%%Scheduler_Port%
if %ERRORLEVEL% == 0  ( goto RESTARTUP_DONE:)
REM sleep for 30 seconds
ping -n 30 127.0.0.1 > NUL 2>&1
for /f "tokens=2-4" %%a in ('sc query %Install_Name%%Scheduler_Port% ^| findstr RUNNING') do if %%c==RUNNING ( goto RESTARTUP_DONE:)
set VarMailText=Restart/startup of the SCHEDULER server did not startup normally. 
set VarMailSubject=Scheduler_Deploy:SCHEDULER restart startup did not complete
goto BADEXIT

:RESTARTUP_DONE
REM sleep for 30 seconds
ping -n 30 127.0.0.1 > NUL 2>&1
 
goto THE_END:

:BADEXIT
echo ------------------ Bad Exit -------------------------------------------
echo %VarMailText%
blat -to %VarNotify% -subject "%VarMailSubject%" -body "%VarMailText%"
rem *** if exist %VarCustProp% del %VarCustProp%
echo "Scheduler Deployment failed."

REM
REM Clean up the directory stack.
REM
popd
popd
popd

exit /B 1

:THE_END

exit /B 0


