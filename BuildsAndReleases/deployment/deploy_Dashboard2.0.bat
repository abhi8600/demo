echo on
echo %1% %2% %3% %4%
set TimeStamp=""
set VarNotify="jfong@birst.com"
set Install_Name=Dashboards2.0
set Port_Num=6205
set Working_Branch=feature_freeform_dashboards
set Current_Build_Number=\\repo_dev\Dev\Builds\Current_Build_Number
set Acorn_Root=D:\BIRST_HOME\BIRST

REM
REM We need a timestamp.  If it is not provided, we should create our own.
REM

if %TimeStamp% neq "" ( goto TIMESTAMP_DEFINED: ) 
	
for /f "tokens=1-3 delims=:." %%a in ("%time%") do set hh_mm_stamp=%%a%%b
echo %hh_mm_stamp%
echo %hh_mm_stamp:~0,1%
echo %hh_mm_stamp:~1,3%
if "%hh_mm_stamp:~0,1%" == " " ( set hh_mm_stamp=0%hh_mm_stamp:~1,3%)
echo %hh_mm_stamp%
for /f "tokens=1-3 delims=\ " %%a in ("%date%") do set date_only=%%b
for /f "tokens=1-3 delims=/." %%a in ("%date_only%") do set teststamp=%%c%%a%%b
echo %teststamp%
set TimeStamp=%teststamp%%hh_mm_stamp%
	
:TIMESTAMP_DEFINED
echo TimeStamp: -%TimeStamp%-

REM
REM where is 7-zip 
REM This handles the standard 32 and 64 bit locations
REM

set SevenZip_Home=""
if exist "c:\Program Files\7-Zip" ( set SevenZip_Home="c:\Program Files\7-Zip")
echo %SevenZip_Home%
if exist "C:\Program Files (x86)\7-Zip" ( set SevenZip_Home="C:\Program Files (x86)\7-Zip")
echo %SevenZip_Home%
if %SevenZip_Home% neq "" ( goto 7Zip_Found: )
echo We can't find the 7Zip directory"
set VarMailText=Cannot file 7Zip in the default 32 and 64 bit configurations. 
set VarMailSubject=Deploy_Dashboards2.0 on %COMPUTERNAME%:7Zip directory missing
goto BADEXIT


:7Zip_Found

REM
REM Figure out which build to use
REM
FOR /F "tokens=1,2,3 delims==" %%A IN (%Current_Build_Number%\%Working_Branch%\build_number.txt) DO if "%%A" == "build_number" (set build_number=%%B) else (ECHO "No build Number "%%A"-" %%B"-")
echo -%build_number%-

REM
REM Copy the files to the local system
REM
if exist D:\delete_me\No_ftp (goto NO_FTP_EXISTS:)
set VarMailSubject="Deploy_Dashboards2.0 of %Build_Tag% on %COMPUTERNAME%:No FTP directory"
set VarMailText="There is no default No_FTP directory, please create." ) 
goto BADEXIT:)

:NO_FTP_EXISTS
chdir /d d:\delete_me\No_ftp
REM	del /F /Q /S *
xcopy /Y /V  \\repo_dev\dev\Builds\Nightly\%build_number%\Dashboards2.0.zip .

:CLEAN_Dashboards2.0
chdir /D %ACORN_ROOT%\Acorn

REM
REM Delete the old saved directory
REM
:Dashboards2.0_SAVED
if not exist Dashboards2.0_old goto NO_Dashboards2.0_OLD:
del /F /S /Q Dashboards2.0_old
dir /S Dashboards2.0_old

if not exist Dashboards2.0_old goto NO_Dashboards2.0_OLD:
rmdir /S /Q Dashboards2.0_old

if not exist Dashboards2.0_old goto NO_Dashboards2.0_OLD:
dir
set VarMailText=Problems with removing the Dashboards2.0_old directory, Please correct problem before proceeding. 
set VarMailSubject=Deploy_Dashboards2.0 on %COMPUTERNAME%:Dashboards2.0 Backup
goto BADEXIT

:NO_Dashboards2.0_OLD

xcopy /V /E /I /H /Y Dashboards2.0 Dashboards2.0_old
IF %ERRORLEVEL% LSS 1 goto CREATE_Dashboards2.0_DIR:
set VarMailText=Fail to xcopy Dashboards2.0 to Dashboards2.0_old, Please correct problem before proceeding. 
set VarMailSubject=Deploy_Dashboards2.0 on %COMPUTERNAME%:Dashboards2.0 Backup
goto BADEXIT

:CREATE_Dashboards2.0_DIR

del /F /S /Q Dashboards2.0
dir /S Dashboards2.0

if not exist Dashboards2.0 goto RELOAD_Dashboards2.0:
del /F /S /Q Dashboards2.0
dir /S Dashboards2.0

if not exist Dashboards2.0 goto RELOAD_Dashboards2.0:
rmdir /S /Q Dashboards2.0

if not exist Dashboards2.0 goto RELOAD_Dashboards2.0:
rmdir /S /Q Dashboards2.0

IF not exist Dashboards2.0 goto RELOAD_Dashboards2.0:
dir
set VarMailText=Fail to delete the  Dashboards2.0 directory, Please correct problem before proceeding. 
set VarMailSubject=Deploy_Dashboards2.0 on %COMPUTERNAME%:Dashboards2.0 Backup
goto BADEXIT

:RELOAD_Dashboards2.0

chdir /D %ACORN_ROOT%\Acorn

if exist %ACORN_ROOT%\Acorn\Dashboards2.0 goto DASHBOARD_DIR_CREATED:
mkdir %ACORN_ROOT%\Acorn\Dashboards2.0
IF %ERRORLEVEL% LSS 1 goto DASHBOARD_DIR_CREATED:
set VarMailText=Could not build the Dashboard 2.0 Minimized release files. Grunt error, please fix before proceeding. 
set VarMailSubject=Dashboard 2.0:Dashboard 2.0 build error 
goto BADEXIT:

:DASHBOARD_DIR_CREATED
cd %ACORN_ROOT%\Acorn\Dashboards2.0

%SevenZip_Home%\7Z.EXE x -r -y -tzip d:\delete_me\No_ftp\Dashboards2.0.zip -o%ACORN_ROOT%\Acorn\Dashboards2.0
IF %ERRORLEVEL% LSS 1 goto Dashboards2.0_UNZIP_DONE:
set VarMailText=ACORN had problems Unzipping Dashboards2.0, Please correct problem before proceeding. 
set VarMailSubject=Deploy_Dashboards2.0 on %COMPUTERNAME%:Dashboards2.0 unzip
goto BADEXIT

:Dashboards2.0_UNZIP_DONE

:RENAME_DONE
set VarMailSubject="Deploy_Dashboards2.0 of %build_number% on %COMPUTERNAME%: Is Done"
set VarMailText="Birst updated and is now ready for use." 

blat -to "jfong@birst.com" -subject %VarMailSubject% -body %VarMailText%

goto THEEND:

:BADEXIT
echo ------------------ Bad Exit -------------------------------------------
echo %VarMailText%
blat -to %VarNotify% -subject "%VarMailSubject%" -body "%VarMailText%"
rem *** if exist %VarCustProp% del %VarCustProp%
echo "Acorn Deployment failed."
exit /B 1
:THEEND

exit /B 0