REM
REM Update the Birst SpaceIndexerPlugin
REM
echo on
set ElasticSearch=c:\ElasticSearch\elasticsearch-1.1.1
set VarNotify=jfong@birst.com

FOR /F "tokens=1,2,3 delims==" %%A IN (\\repo_dev\dev\Builds\Current_Build_Number\release\build_number.txt) DO if "%%A" == "build_number" (set build_number=%%B) else (ECHO "No build Number "%%A"-" %%B"-")
echo -%build_number%-
if "%build_number%" == "" (set build_number=201208210700)
echo -%build_number%-

dir \\repo_dev\dev\Builds\Nightly\%build_number%

copy \\repo_dev\dev\Builds\Nightly\%build_number%\SpaceIndexerPlugin*.zip c:\delete_me

REM
REM First shutdown the ElasticSearch service
REM 

echo "shutdown the server"
net stop elasticsearch-service-x64
if %ERRORLEVEL% == 2 (
	goto SHUTDOWN_DONE: 
	) else (
	if %ERRORLEVEL% == 0 ( goto SHUTDOWN_DONE: ))
set VarMailText=Shutdown of the SpaceIndexerPlugin server did not complete normally. 
set VarMailSubject=Deploy_SpaceIndexerPlugin on %COMPUTERNAME%, elasticsearch-service-x64 shutdown did not complete
goto BADEXIT

:SHUTDOWN_DONE
REM
REM Remove the current Plugin
REM

cd %ElasticSearch%\bin
cmd /c plugin -r SpaceIndexerPlugin
IF %ERRORLEVEL% LSS 1 goto PLUGIN_REMOVED:
set VarMailText=There were problems removing the new Plugin zip file. 
set VarMailSubject=Deploy_SpaceIndexerPlugin on %COMPUTERNAME%:Remove problems
goto BADEXIT

:PLUGIN_REMOVED
REM
REM Copy the new plugin zip file to the proper location
REM
dir /R %ElasticSearch%\plugins
del %ElasticSearch%\..\SpaceIndexerPlugin*.zip 
copy c:\delete_me\SpaceIndexerPlugin*.zip %ElasticSearch%\..

REM
REM Install the new plugin
REM

cd %ElasticSearch%\bin
cmd /c plugin -v --url file://localhost/C:/ElasticSearch/SpaceIndexerPlugin-1.0-SNAPSHOT-plugin.zip install SpaceIndexerPlugin
IF %ERRORLEVEL% LSS 1 goto PLUGIN_INSTALLED:
set VarMailText=There were problems installing the new Plugin zip file. 
set VarMailSubject=Deploy_SpaceIndexerPlugin on %COMPUTERNAME%:Install problems
goto BADEXIT

:PLUGIN_INSTALLED
REM
REM Start the service again
REM

echo "startup the server"
net start elasticsearch-service-x64
if %ERRORLEVEL% == 0  ( goto STARTUP_DONE:)
REM sleep for 30 seconds
ping -n 30 127.0.0.1 > NUL 2>&1
for /f "tokens=2-4" %%a in ('sc query elasticsearch-service-x64 ^| findstr RUNNING') do if %%c==RUNNING ( goto STARTUP_DONE:)
set VarMailText=Startup of the elasticsearch-service-x64 service did not startup normally. 
set VarMailSubject=Deploy_SpaceIndexerPlugin on %COMPUTERNAME%, elasticsearch-service-x64 startup did not complete
goto BADEXIT

:STARTUP_DONE
set VarMailText=update of the elasticsearch-service-x64 service completed successfully 
set VarMailSubject=Deploy_SpaceIndexerPlugin on %COMPUTERNAME%, is done
blat -to %VarNotify% -subject "%VarMailSubject%" -body "%VarMailText%"

goto THEEND:

:BADEXIT
echo ------------------ Bad Exit -------------------------------------------
echo %VarMailText%
blat -to %VarNotify% -subject "%VarMailSubject%" -body "%VarMailText%"
rem *** if exist %VarCustProp% del %VarCustProp%
echo "SpaceIndexerPlugin deployment failed."
exit /B 1
:THEEND

exit /B 0