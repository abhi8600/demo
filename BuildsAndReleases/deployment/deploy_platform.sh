#!/usr/bin/bash	
#
# Deploy the latest build 
# This script will deploy the defined build in the designated area.
# This script will preform a backup of the entire instance before it starts
# the deployment process.  It will also move specific directory contents to 
# the backup directroy for review. 
#
# If a Period Id is provided. The script will look for a base seed script to 
# execute with the Period Id.
# 
# echo on

echo \e
echo \t "**********************************************************************"
echo \t "     Deploy Platform Package for SMIWeb version 2.1.0 and above.      "
echo \t "**********************************************************************"
echo \e

script_loc=${0%/*}
echo $script_loc

#
# Help for parameters
#

function Parameter_Help
{
	echo "usage: deploy_platform [-options]"
	echo "where options include:"
	echo " -h <smi_home> "
	echo "		Enter the home directory of the instance"
	echo " 		e.g./cygdrive/c/CustomerInstalls/RBC/RBCWeb"
	echo "		*** NOTE: if you enter a DOS path name,it needs "
	echo "		to be double quoted or the backslashes will "
	echo " 		be striped.  \"c:\\CustomerInstalls\\RBC\\RBCWeb\" "
	echo " -p <port number>"
	echo "		Enter 4 digit port number for the installation"	
	echo " 		Installation will use port number +2"
	echo "		e.g. 9300, 9301, 9302"
	echo " -e <prod|test|dev> (optional)"
	echo "		define which environment you are deploying to"
	echo "		prod is for a production environment"
	echo "		dev is for a development environment"
	echo "		test is for a test environment "
	echo " -c (optional)"
	echo "		-c flag identifies if the script should retain the "
	echo "		cache files instead of initializing them.  This is useful"
	echo "		minor platfrom deployments that do not affect the user cache"
	echo "		files"
	echo " -u (optional)"
	echo "		-u flag identifies the use of a customized URL  "
	echo "		based on the customer's installation type.  "
	echo " 		For example: if SMI home is "
	echo "		/c/CustomerInstalls/RBC/RBCDev, "
	echo "		the URL will be https://.../RBCDev/Login"
	echo "		Default is no, use SMIWeb"
	echo " -n <email notification list> comma separated (optional)"
	echo " 		default=jfong@successmetricsinc.com,4157301327@vtext.com"
	echo " -r <release directory> "
	echo " 		Which do you want to get the release from?"
	echo "		Nightly for nightly builds"
	echo "		Releases/Platform for version releases"
	echo " -t <build tag> "
	echo " 		If you are getting files from Releases/Platform,"
	echo "		enter a Release Number e.g. 2.0.0"
	echo "		If you are getting files from Nightly, "
	echo "		enter a date and time e.g. 200705230130"
}		# end of the Parameter_Help function


#
# end of Functions
#---------------------------------------------------------------------------------------------------
# 

#
# setup defaults
#
DEBUG=0;					export DEBUG			#debug=1, not debug=0
timestamp=`date +%Y%m%d%H%M`
SMI_HOME="";					export SMI_HOME
Https_Port=""; 					export Https_Port
Release_Dir="";					export Release_Dir
Build_Tag="";					export Build_Tag
Two_One="false"
Notify=jfong@successmetricsinc.com;		export Notify
Deployment_env="prod"				# setting default environment to be production
user_defined_URL="false"			# setting default for URL rename.
retain_cache="false"
echo done clearing stuff

while getopts ":d:e:h:ucn:p:r:t:" opts; do
        case "$opts" in
		d ) echo "-d $OPTARG"
			DEBUG=$OPTARG
		 	export DEBUG
			;;
		e ) echo "-e $OPTARG"
			case "$OPTARG" in
				"prod" | "test"| "dev") Deployment_env=$OPTARG
					echo "Deployment_env is " $Deployment_env
					;;
				* )	echo "-e was entered with an invalid value"
					echo "   the only values accepted are "
					echo "   prod, test and dev"
					echo "   please correct before proceeding"
					echo "   or leave a the default which is prod"
					exit -1
					;;
			esac
			;;
		u ) echo "-u  flag is set, using installation type in URL"
			user_defined_URL="true"
			;;			
		c ) echo "-c  flag is set, using installation type in URL"
			retain_cache="true"
			;;			
		h ) echo "-h $OPTARG" 
			SMI_HOME="$(cygpath -u -p -a "$OPTARG")"
			SMI_HOME="${SMI_HOME%/}"
			export SMI_HOME				# expect SMI_HOME to be in the following format
			Cust_Type=${SMI_HOME##*/}		# 	/cygdrive/c/CustomerInstalls/RBC/RBCWeb
			Customer_Home=${SMI_HOME%/*}		# so    ----------------------------
			export Customer_Home			#            Install Root            ---
			Customer_Name=${Customer_Home##*/}	#                          Customer Name ------
			export Customer_Name			#                                        Customer's installation type
			Cust_Root=${Customer_Home%/*}		# SMI_HOME=$Install_Root+$Customer_Name+Cust_Type
			export Cust_Root
			Install_Root=${Customer_Home%/*}
			export Install_Root

			echo "$Install_Root"
			echo "$Customer_Home"
			echo "$Customer_Name"
			echo "$Cust_Type"
			;;
		n ) echo "-n $OPTARG" 
			Notify=$OPTARG
			export Notify 
			;;
		p ) echo "-p $OPTARG"
			echo $OPTARG | grep [^0-9] >/dev/null 2>&1
			if [ "$?"  -eq "0" ]; then
				echo $OPTARG "is not a number.  The Port Number needs to be numeric"  
				exit 1
			else
				Https_Port=$OPTARG;  export Https_Port 
			fi
			;;
		r ) echo "-r $OPTARG" 
			if [ $OPTARG == "Nightly" ] || [ $OPTARG == "Releases/Platform" ]; 
				then 
				Release_Dir=$OPTARG
				export Release_Dir
			else
				echo "Release Build directory, $OPTARG, does not exist, please fix"
				exit 1
			fi	
			;;
		t ) echo "-t $OPTARG" 
			Build_Tag=$OPTARG;	
			export Build_Tag
			;;
		*  ) Parameter_Help
			exit 1 ;;
	esac
	echo one value $1
done


echo  SMI_HOME "$SMI_HOME"
echo  Https_Port $Https_Port 
echo  Release_Dir $Release_Dir
echo  Build_Tag $Build_Tag
echo Deployment_env $Deployment_env

if [ -z $SMI_HOME ] || [ -z $Https_Port ] || [ -z $Release_Dir ] || [ -z $Build_Tag ]; 
then
	echo "One or more of the required parameters was not provided.  "
	echo "Please provide all required paramaters:"
	Parameter_Help
	exit 1 
fi 

if [ $Release_Dir == "Releases/Platform" ] && [[ "$Build_Tag" == 2.1.* ]];
then
	Two_One="True"
else
	Two_One="False"
fi
echo "Is this version 2.1.x?  $Two_One"

#
# Current Period's Drop, Last period's Data
# Customer directorise will contain catalog (shared directory), repository.xml, 
#	base.seed.1288.txt (for period 1288), customer.css and abbreviated web.xml

# Things I need to know to get started:

#
Http_Port=`expr $Https_Port + 1`; 			export Http_Port
Shutdown_Port=`expr $Https_Port + 2`;			export Shutdown_Port
CATALINA_HOME="$SMI_HOME"/tomcat; 			export CATALINA_HOME
CATALINA_BASE="$SMI_HOME"/tomcat; 			export CATALINA_BASE
Cust_Backup="$Customer_Home"/$Cust_Type"-Platform_"$timestamp
declare -i error_count=0

#
# if the -u flag is used, then, change the SMIWeb.war file,
# then, change the file to match the Customer Type .
#
if [ $user_defined_URL = "true" ];
then
	War_Name="$Cust_Type"
else
	War_Name="SMIWeb"
fi
echo War_Name "$War_Name"
#
#  First, check to see if the backup directory exists.  
#  If it exists, then check to see if we have files with the period id
#  	If there are files with the same period id, exit since we don't want to overwrite an archive
#  	else  delete everything in the directory since it is old
#  else create the backup directory.  
#
#
# Clear out last months files and backup the entire instance first
# 

if [ -d "$Cust_Backup" ];	# if backup exists, check to make sure I'm not using the same Period ID
then
	echo "Reusing the same build time stamp $timestamp, exiting since we do not want to overwrite your previous backup.  Please manully correct" \
			| mutt -s "Deploy_platform: --- Error --- Reusing a Time Stamp" $Notify
fi

$script_loc/purge_dir.sh -s "$Customer_Home"/$Cust_Type"-Platform_" -x 5
if [ $? != 0 ];
then
	echo "Purge script had problems or was not able to delete older directory" \
		| mutt -s "Deploy_platform: Purge was unsuccessful" $Notify
fi 

mkdir "$Cust_Backup"
if [ $? != 0 ];
then
	echo "Exiting since I was not successful in creating last period's directory.  " \
		| mutt -s "Deploy_platform: Cannot create backup directory" $Notify
	exit -1
fi

#
# Shutdown the server
#

echo "shutdown the server"
net stop $Cust_Type$Https_Port
status=$?
if [ $status == 0 ] || [ $status == 2 ]; 
then
	echo "Service has been stopped or is already down"  
else
	echo "Not sure what state the Tomcat service is in.  The service is not down, but stopping the service doesn't seem to stop it." \
		| mutt -s "Deploy_platform: Having problems stopping service" $Notify
	exit -1
fi

# 
# Backup the entire system
#

cd "$SMI_HOME"
zip -r "$Cust_Backup"/Backup_Platform.zip *
if [ $? == 0 ]; 
then
	echo "Backup of instance was successful"  
else
	echo "Exiting since I was not successful in creating a backup of the entire instance.  " \
		| mutt -s "Deploy_platform: Unable to backup instance" $Notify
	exit -1
fi
	
#
#Create working temp directory (with stupid name which should not exist)
#

Temp_Dir="/cygdrive/c/delete_me";			export Temp_Dir
echo "$Temp_Dir"
if [ -d "$Temp_Dir" ];
then
	rm -rf "$Temp_Dir"/*				# clean up the directory
else
	mkdir "$Temp_Dir"
	if [ $? != 0 ]; 
	then
		echo "Error trying to create my temporary working directory"
		echo "Please fix and try again"
		exit -1
	fi
fi

#


echo "*******************************"
echo
echo "Let us ftp our files and deploy them."
echo
echo "*******************************"

#
# 1 - FTP the Platform Build Directory and Customer Build Directory to OpSource (10.100.100.21), making sure to label them appropriately; the Platform Build Directory could already be out there for another customer.
# 	use the FTP script to login and retrieve the files.  

cd "$Temp_Dir"
ls -la "$Temp_Dir"


if [ $DEBUG -eq 0 ]; then 
	echo "open builds" >download.ftp
	echo "smibuilds" >>download.ftp
	echo "5771builds!" >>download.ftp
	echo "verbose" >>download.ftp
	echo "cd " $Release_Dir/$Build_Tag >>download.ftp
	echo "prompt" >>download.ftp
	echo "mget *.*" >>download.ftp
	echo "cd " $Customer_Name >>download.ftp
	echo "mget *" >>download.ftp
	echo "quit" >>download.ftp

	ftp -s:download.ftp  

	echo NULL >download.ftp
	rm download.ftp
else
	echo "copy from local build area"
	cp -r /cygdrive/c/SFOJFONG/Builds/$Build_Tag/*  .
fi

ls -la "$Temp_Dir"
if [ -s ./PerformanceEngine.Zip ] && [ -s ./Birst\ Administration.exe ] && [ -s ./SMIWeb.war ]; 
then
	echo "FTP successfully copied files over to temp directory."
else
	echo "One or more required files did not get transfered properly or was not found.  Cannot proceed with upgrade preparations" \
		| mutt -s "Deploy_platform: FTP had problems" $Notify
	exit -1
fi 

#
# *** NOTE ***  This section is a legacy setup for version 2.1.0.  The customer.properties is now in conf, not under SMIWeb
# Configure the customer.properties file  
# If setup is for "prod", then customer.properties is left alone, and customer-dev.properties is deleted
# If setup is for "dev", then customer.properties is deleted and customer-dev.properties is renamed to customer.properties.  
#
if [ $Deployment_env = "dev" ] ||  [ $Deployment_env = "test" ];
then
	error_count=0
	unzip -C SMIWeb.war  WEB-INF/classes/customer-$Deployment_env.properties
	if [ -e WEB-INF/classes/customer-$Deployment_env.properties ];
	then 
		mv WEB-INF/classes/customer-$Deployment_env.properties WEB-INF/classes/customer.properties
		error_count=error_count+$?
		zip -r SMIWeb.war  WEB-INF/classes/customer.properties
		error_count=error_count+$?
		if [ $error_count -ne 0 ];
		then
			echo "The renaming of customer-$Deployment_env.properties. Not sure if it is zip, unzip or mv" \
			| mutt -s "deploy_platform had problems setting the customer.properties file." $Notify
			exit 1
		fi
	else
		echo "No customer-$Deployment_env.properties files in customer build"
	fi
fi	

#
# Remove the old log files 
#
stat "$SMI_HOME"/logs >/dev/null 2>&1
if [ $? = 0 ]; 
then
	rm -r "$SMI_HOME"/logs 
	if [ $? != 0 ]; 
	then
		echo "Log file or log directory is still being accessed, cannot delete it." \
		| mutt -s "deploy_platform had problems deleting the log directory." $Notify
		exit -1
	fi
fi
mkdir "$SMI_HOME"/logs 
chmod 777 "$SMI_HOME"/logs

#
# Clean up the webapps directory too.
#	Assume that SMIWeb.war has to exist before we can have the SMIWeb directory.  

cd "$SMI_HOME"/tomcat/webapps
if [ -d ./"$War_Name" ];
then
	chmod 757 ./$War_Name
fi

if [ -e ./"$War_Name".war ];
then 
	rm ./"$War_Name".war
	if [ $? != 0 ];
	then
		echo "Exiting deployment script since the delete of the SMIWeb.war file was unsuccessful" \
			| mutt -s "Deploy_platform: Delete old SMIWeb.war had problems." $Notify
		exit -1
	fi
	echo SMIWeb.war was removed without a problem.  
fi

if [ -d ./"$War_Name" ];
then
	rm -rf ./SMIWeb
fi

del_stat=$?
echo "remove SMIWeb directory, status=" $del_stat 
if [ -d ./"$War_Name" ] || [ $del_stat -ne 0 ] ;
then
	echo "First delete of SMIWeb did not work, try again"
	sleep 60
	ls -la ./"$War_Name"
	rm -rf ./"$War_Name"
	if [ $? != 0 ];
	then
		echo "Exiting deployment script since the delete of the SMIWeb directory was unsuccessful" \
			| mutt -s "Deploy_platform: Deleting the SMIWeb directory had errors" $Notify
		exit -1
	fi
fi
echo Deleting the old SMIWeb directory was successful.

#
# handle the case where we started with SMIWeb, but switched over to a
# customizes URL based on the instance type
#
if [ -d ./SMIWeb ] || [ -e SMIWeb.war ];
then
	error_count=0
	chmod 757 ./SMIWeb
	error_count=error_count+$?
	rm ./SMIWeb.war
	error_count=error_count+$?
	rm -rf ./SMIWeb
	error_count=error_count+$?
	if [ $error_count -ne 0 ];
		then
			echo "Assume we are using customize URL, but deleting old SMIWeb.war encounter problems.  Manul fix required." \
			| mutt -s "deploy_platform had problems deleting duplicate SMIWeb.war" $Notify
			exit 1
	fi
fi
#
# Copy the new SMIWeb.war from our FTP directory.  
# Known customers will get a customer specific version of the SMIWeb.war
# with their own WEB-INF, images and customer.css files.  The customer 
# specific SMIWeb.war is retrieved and switched in the FTP process.  
#

cp "$Temp_Dir"/SMIWeb.war ./"$War_Name".war
if [ $? != 0 ];
then
	echo "Exiting deployment script since the copy of the SMIWeb.war file was unsuccessful" \
		| mutt -s "Deploy_platform: Problems copy over SMIWeb.war" $Notify
	exit -1
fi
echo copy of the new SMIWeb.war was successful
chmod 755 "$War_Name".war


# 
# Don't remove the old bin directory, only delete the run.bat files
#

if [ -e "$SMI_HOME"/bin/run.bat ];
then
	rm "$SMI_HOME"/bin/run.bat	# Need to delete the old version so unzip can update file from
					# the new PerformanceEngine.zip file.  
fi

#
# backup the lib directory and delete it to insure we are getting updated files.
#

if [ -d "$SMI_HOME"/lib ];
then
	rm -rf "$SMI_HOME"/lib/*
	if [ $? != 0 ];
	then
		echo "Exiting deployment script since we were not able to remove the old lib directory" \
			| mutt -s "Deploy_platform: Delete lib had errors" $Notify
		exit -1
	fi

	#
	# move the old log4j.properties over to the backup directory (this essentially removes it)
	#

	stat "$SMI_HOME"/log4j.properties >/dev/null 2>&1
	if [ $? -eq 0 ]; then
		echo delete the log4j.properties file.
		rm -r "$SMI_HOME"/log4j.properties
		if [ $? != 0 ];
		then
			echo "Exiting deployment script since we had problems deleting the log4j.properties files.  ." \
				| mutt -s "Deploy_platform: Error deleting log4j.property file" $Notify
		exit -1
		fi
	else
		echo "Bypass delete since there files was not log4j.perperties file to delete"
	fi
	echo Removed the old files so we can start with an updated log4j.properties file.
fi

#
# Make sure we are in the right directory and then install the Performance Engine.
#	unzip the PerformanceEngine.zip to the Brokerage SMI directory and subdirectories
#

cd "$SMI_HOME"

unzip -o "$Temp_Dir"/PerformanceEngine.zip -d "$SMI_HOME"
if [ $? != 0 ];
then
	echo "Exiting deployment script since the unzip of the PerformanceEngine.zip was unsuccessful" \
		| mutt -s "Deploy_platform: Unzip PerformanceEngine had errors" $Notify
	exit -1
fi
echo Unzip of the PerformanceEngine.zip file was successful

#
# Update the Birst Administration tool
#

cp "$Temp_Dir"/Birst\ Administration.exe "$SMI_HOME"/bin/Birst\ Administration.exe
if [ $? != 0 ];
then
	echo "Exiting deployment script since there were problems copying over the new SMI Admin" \
		| mutt -s "Deploy_platform: Copy SMI Admin to local env had errors" $Notify
	exit -1
fi
echo Copy the new SMI Adminstration.exe file was successful

#
# Start up the server so we can test it.  
# and clean out the cache files
#

stat "$SMI_HOME"/cache/* >/dev/null 2>&1
if [ $? -eq 0 ]; then
        echo there are cache files to handle
        if [ $retain_cache = "true" ]; 
        then
        	echo "Request was to save the old cache files,  No cache files were deleted"
        else 
		rm -r "$SMI_HOME"/cache/*
		if [ $? != 0 ];
		then
			echo "Exiting deployment script since we had problems clearing out all the old cache files." \
				| mutt -s "Deploy_platform: Error deleting Cache files" $Notify
		exit -1
		fi
		echo We can start with a fresh cache for normal transactions.
	fi
else
        echo "Bypass delete since there were no files in cache to delete"
fi

rm -rf "$SMI_HOME"/tomcat/logs/*
rm -rf "$SMI_HOME"/tomcat/temp/*
rm -rf "$SMI_HOME"/tomcat/work/*
#
# Change the privileges to make things work.
#
chmod 777 "$SMI_HOME"
chmod -R g=rwx "$SMI_HOME"
chmod -R 755 "$SMI_HOME"/jdk
chmod 777 "$SMI_HOME"/bin/*.bat
chmod 777 "$SMI_HOME"/logs
chmod -R 777 "$SMI_HOME"/catalog
chmod -R 777 "$SMI_HOME"/cache
chmod 755 "$SMI_HOME"/tomcat/bin/tomcat*
chmod 777 "$SMI_HOME"/tomcat/conf
chmod 777 "$SMI_HOME"/tomcat/work
chmod 777 "$SMI_HOME"/tomcat/logs
chmod 777 "$SMI_HOME"/tomcat/webapps

#
# install the server as a service (if needed) and then start it up.
#
# We should not need to remove the service if we didn't make significant changes, we normally would not
# need to re-install the service.  Commenting out the steps just in case I am wrong.
# if [ `net start | grep -c $Cust_Type$Https_Port` == 0 ];
# then
# 	cd "$SMI_HOME"/tomcat/bin
# 	SMI_HOME=`cygpath --absolute --windows "$SMI_HOME"`; export SMI_HOME
# 	CATALINA_HOME=`cygpath --absolute --windows "$CATALINA_HOME"`; export CATALINA_HOME
# 	CATALINA_BASE=`cygpath --absolute --windows "$CATALINA_BASE"`; export CATALINA_BASE
# 	cmd /c service.bat install  $Cust_Type$Https_Port
# fi

net start $Cust_Type$Https_Port
if [ $? == 0 ];
then
	echo "started the "$Cust_Type$Https_Port" service"
else
	echo "Had problems starting the $Cust_Type$Https_Port service." \
		| mutt -s "Deploy_platform: Unable to start service" $Notify
        exit -1
fi 

#
# Remove the customer-dev.properties file since it is no longer needed.  
#
sleep 30 

#
# Restore the SMI customer customizations 
#	the customer.css and the image directory should have been 
#	restored back to files from the last customer deployment. 
#

cd "$SMI_HOME"/tomcat/webapps/"$War_Name"

#  
# ********************************************************************************
#
# Missing the login page customizations logic, I didn't save anything yet
# so there is nothing to copy.  
# Need to fix for Fidelity.
#
#if [ -d "$SMI_HOME"/smi_save ] && [ $Two_One = "False" ];
#then
#	cp -r "$SMI_HOME"/smi_save/*  .
#	if [ $? != 0 ];
#	then
#		echo "Platform deployment had problems restoring the SMIWeb customization files from smi_save.  Please restore manually" \
#			| mutt -s "Deploy_platform: Problems restoring customer customization files" $Notify
#	fi
#fi
#  ********************************************************************************

#
#  If there is a Messages_en.properties files, restart the server
#  For all customizations except for changes in the Messages files.  
#  Currently if you want to customize messages for the customer  
#  (e.g. change the login page message of "Welcome ot SMI Brokerage Application"
#   to something that has the customer name), you would need to add a 
#  Messages_en.properties file.  This section will restart the server for 
#  you to pick up the changes after the file has been put in place.  
#

stat "$SMI_HOME"/tomcat/webapps/"$War_Name/WEB-INF/classes/com/successmetricsinc/UI/bundle/Messages_en.properties" >/dev/null 2>&1

if [ $? = 0 ];
then
	echo "Restart Server to we pickup the Messages_en.properties"
	net stop $Cust_Type$Https_Port
	status=$?
	if [ $status == 0 ] || [ $status == 2 ]; 
	then
		echo "Service has been stopped or is already down"  
	else
		echo "Not sure what state the Tomcat service is in.  The service is not down, but stopping the service doesn't seem to stop it." \
			| mutt -s "Deploy_platform: Having problems stopping service" $Notify
		exit -1
	fi
	
	sleep 15

	net start $Cust_Type$Https_Port
	if [ $? == 0 ];
	then
		echo "started the "$Cust_Type$Https_Port" service"
	else
		echo "Had problems starting the $Cust_Type$Https_Port service. to pick up customer files" \
			| mutt -s "Deploy_platform: Unable to start service" $Notify
		exit -1
	fi 
fi 

cd "$SMI_HOME"/tomcat/webapps/"$War_Name"/WEB-INF/classes

if [ -e ./customer-dev.properties ];
then
	rm -rf ./customer-dev.properties
	if [ $? != 0 ];
	then
		echo "Production deployment had problems deleting the customer-dev.properties files.  Please delete manually" \
			| mutt -s "Deploy_platform: Problems deleting SMIWeb customer-dev.properties" $Notify
	fi
fi	

if [ -e ./customer-test.properties ];
then
	rm -rf ./customer-test.properties
	if [ $? != 0 ];
	then
		echo "Production deployment had problems deleting the customer-test.properties files.  Please delete manually" \
			| mutt -s "Deploy_platform: Problems deleting SMIWeb customer-dev.properties" $Notify
	fi
fi	

cd "$SMI_HOME"/conf

if [ -e ./customer-dev.properties ];
then
	rm -rf ./customer-dev.properties
	if [ $? != 0 ];
	then
		echo "Production deployment had problems deleting the customer-dev.properties files.  Please delete manually" \
			| mutt -s "Deploy_platform: Problems deleting conf/customer-dev.properties" $Notify
	fi
fi	

if [ -e ./customer-test.properties ];
then
	rm -rf ./customer-test.properties
	if [ $? != 0 ];
	then
		echo "Production deployment had problems deleting the customer-test.properties files.  Please delete manually" \
			| mutt -s "Deploy_platform: Problems deleting conf/customer-test.properties" $Notify
	fi
fi	

cd "$SMI_HOME"/tomcat/webapps

# Delete the temp  directory
rm -rf "$Temp_Dir"				

exit
