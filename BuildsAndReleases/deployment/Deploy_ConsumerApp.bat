echo on
echo %1% %2% %3% %4%
set TimeStamp=""
set VarNotify="jfong@birst.com"
set Current_Branch=dev
set Install_Name=QueueConsumer
set CONSUMERAPP_HOME=C:\QueueConsumer

REM
REM We need a timestamp.  If it is not provided, we should create our own.
REM

if %TimeStamp% neq "" ( goto TIMESTAMP_DEFINED: ) 
	
for /f "tokens=1-3 delims=:." %%a in ("%time%") do set hh_mm_stamp=%%a%%b
echo %hh_mm_stamp%
echo %hh_mm_stamp:~0,1%
echo %hh_mm_stamp:~1,3%
if "%hh_mm_stamp:~0,1%" == " " ( set hh_mm_stamp=0%hh_mm_stamp:~1,3%)
echo %hh_mm_stamp%
for /f "tokens=1-3 delims=\ " %%a in ("%date%") do set date_only=%%b
for /f "tokens=1-3 delims=/." %%a in ("%date_only%") do set teststamp=%%c%%a%%b
echo %teststamp%
set TimeStamp=%teststamp%%hh_mm_stamp%
set DeleteMeDir=c:\delete_me\%TimeStamp%

:TIMESTAMP_DEFINED
echo TimeStamp: -%TimeStamp%-

FOR /F "tokens=1,2,3 delims==" %%A IN (\\repo_dev\dev\Builds\Current_Build_Number\%Current_Branch%\build_number.txt) DO if "%%A" == "build_number" (set build_number=%%B) else (ECHO "No build Number "%%A"-" %%B"-")
echo -%build_number%-
if "%build_number%" == "" (set build_number=201208210700)
echo -%build_number%-

if exist c:\delete_me\No_ftp (goto NO_FTP_EXISTS:)
set VarMailSubject="Deploy_ConsumerApp of %Build_Tag% on %COMPUTERNAME%:No FTP directory"
set VarMailText="There is no default No_FTP directory, please create." ) 
goto BADEXIT:)

:NO_FTP_EXISTS
chdir /d c:\delete_me\No_ftp
del /F /Q /S *
xcopy /E \\repo_dev\dev\Builds\Nightly\%build_number%\ConsumerApp.war .


chdir /d %CONSUMERAPP_HOME%\SetupTools

REM 
REM --------------------------------------------------------------------------------------------
REM upgrade ConsumerApp
REM

echo "shutdown the server"
net stop %Install_Name%
if %ERRORLEVEL% == 2 (
	goto SHUTDOWN_DONE: 
	) else (
	if %ERRORLEVEL% == 0 ( goto SHUTDOWN_DONE: ))
set VarMailText=Shutdown of the ConsumerApp server did not complete normally. 
set VarMailSubject=Deploy_ConsumerApp on %COMPUTERNAME%:Shutdown did not complete
goto BADEXIT

:SHUTDOWN_DONE
REM sleep for 30 seconds
ping -n 30 127.0.0.1 > NUL 2>&1
 
REM
REM not sure if we need to clean the temp directory, commenting out for now
REM
REM	if %Clean_Flag% equ 0 ( goto Update_CONSUMERAPP: )
REM	cd %CONSUMERAPP_HOME%\tomcat
REM	dir "temp"
REM	if %ERRORLEVEL% NEQ 0 goto Update_CONSUMERAPP:
REM	del /Q /S temp\*

:Update_CONSUMERAPP
cd %CONSUMERAPP_HOME%\tomcat\webapps
rmdir /S /Q %CONSUMERAPP_HOME%\tomcat\webapps\ConsumerApp
IF %ERRORLEVEL% LSS 1 goto CONSUMERAPP_RMDIR_DONE:
set VarMailText=ConsumerApp had problems removing ConsumerApp, Please correct problem before proceeding. 
set VarMailSubject=Deploy_ConsumerApp on %COMPUTERNAME%:ConsumerApp rmdir
goto BADEXIT

:CONSUMERAPP_RMDIR_DONE
del %CONSUMERAPP_HOME%\tomcat\webapps\ConsumerApp.war
IF %ERRORLEVEL% LSS 1 goto CONSUMERAPP_DEL_DONE:
set VarMailText=ConsumerApp had problems deleting ConsumerApp.war, Please correct problem before proceeding. 
set VarMailSubject=Deploy_ConsumerApp on %COMPUTERNAME%:ConsumerApp Del
goto BADEXIT

:CONSUMERAPP_DEL_DONE
dir

copy c:\delete_me\No_ftp\ConsumerApp.war %CONSUMERAPP_HOME%\tomcat\webapps
IF %ERRORLEVEL% LSS 1 goto CONSUMERAPP_COPY_DONE:
set VarMailText=ConsumerApp had problems copying new ConsumerApp.war, Please correct problem before proceeding. 
set VarMailSubject=Deploy_ConsumerApp on %COMPUTERNAME%:ConsumerApp Copy
goto BADEXIT

:CONSUMERAPP_COPY_DONE
echo "startup the server"
net start %Install_Name%
if %ERRORLEVEL% == 0  ( goto CONSUMERAPP_STARTUP_DONE:)
REM sleep for 30 seconds
ping -n 30 127.0.0.1 > NUL 2>&1
for /f "tokens=2-4" %%a in ('sc query %Install_Name% ^| findstr RUNNING') do if %%c==RUNNING ( goto CONSUMERAPP_STARTUP_DONE:)
set VarMailText=Startup of the ConsumerApp server did not startup normally. 
set VarMailSubject=Deploy_ConsumerApp on %COMPUTERNAME%:ConsumerApp startup did not complete
goto BADEXIT

:CONSUMERAPP_STARTUP_DONE
REM sleep for 30 seconds
 ping -n 30 127.0.0.1 > NUL 2>&1

goto THEEND:

:BADEXIT
echo ------------------ Bad Exit -------------------------------------------
echo %VarMailText%
blat -to %VarNotify% -subject "%VarMailSubject%" -body "%VarMailText%"
rem *** if exist %VarCustProp% del %VarCustProp%
echo "Acorn Deployment failed."
exit /B 1
:THEEND
rem rmdir /S /Q %DeleteMeDir%

exit /B 0