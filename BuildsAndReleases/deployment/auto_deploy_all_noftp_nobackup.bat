echo on
echo %1% %2% %3% %4%
set TimeStamp=""
set VarNotify="jfong@birst.com,pmalshe@birst.com,ricks@birst.com"

REM
REM We need a timestamp.  If it is not provided, we should create our own.
REM

if %TimeStamp% neq "" ( goto TIMESTAMP_DEFINED: ) 
	
for /f "tokens=1-3 delims=:." %%a in ("%time%") do set hh_mm_stamp=%%a%%b
echo %hh_mm_stamp%
echo %hh_mm_stamp:~0,1%
echo %hh_mm_stamp:~1,3%
if "%hh_mm_stamp:~0,1%" == " " ( set hh_mm_stamp=0%hh_mm_stamp:~1,3%)
echo %hh_mm_stamp%
for /f "tokens=1-3 delims=\ " %%a in ("%date%") do set date_only=%%b
for /f "tokens=1-3 delims=/." %%a in ("%date_only%") do set teststamp=%%c%%a%%b
echo %teststamp%
set TimeStamp=%teststamp%%hh_mm_stamp%
	
:TIMESTAMP_DEFINED
echo TimeStamp: -%TimeStamp%-

FOR /F "tokens=1,2,3 delims==" %%A IN (\\repo_dev\dev\Builds\Current_Build_Number\release\build_number.txt) DO if "%%A" == "build_number" (set build_number=%%B) else (ECHO "No build Number "%%A"-" %%B"-")
echo -%build_number%-
if "%build_number%" == "" (set build_number=201208210700)
echo -%build_number%-

if exist D:\delete_me\No_ftp (goto NO_FTP_EXISTS:)
set VarMailSubject="Auto_Deploy_All_noftp_nobackup of %Build_Tag% on %COMPUTERNAME%:No FTP directory"
set VarMailText="There is no default No_FTP directory, please create." ) 
goto BADEXIT:)

:NO_FTP_EXISTS
chdir /d d:\delete_me\No_ftp
del /F /Q /S *
xcopy /E \\repo_dev\dev\Builds\Nightly\%build_number% .
rem 	xcopy \\smi\builds\Nightly\%build_number%_acorn .

chdir /d d:\BIRST_HOME\BIRST\SetupTools

\\%COMPUTERNAME%\BIRST_HOME\BIRST\SetupTools\deploy_all.bat -a D:\BIRST_HOME\BIRST -p 6100 -r Nightly -t %build_number% -i 5_11 -f 1 -g 1 > deploy.log 2>&1

goto THEEND:

:BADEXIT
echo ------------------ Bad Exit -------------------------------------------
echo %VarMailText%
blat -to %VarNotify% -subject "%VarMailSubject%" -body "%VarMailText%"
rem *** if exist %VarCustProp% del %VarCustProp%
echo "Acorn Deployment failed."
exit /B 1
:THEEND

exit /B 0