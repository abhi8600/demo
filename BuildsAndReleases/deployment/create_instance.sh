#!/usr/bin/bash
#
# Create a new instance of the customer installation
#
# Created from Rick's instructions
#
# Setup the tomcat and get it ready for SMI software deployment:
#
# Things I need to know to get started:

# clear out my parameters first and set defaults if necessary

function Parameter_Help
{
	echo "usage: create_instance [-options]"
	echo "where options include:"
	echo " -h <smi_home> "
	echo "		Enter the home directory of the instance"
	echo " 		e.g./cygdrive/c/CustomerInstalls/RBC/RBCWeb"
	echo "		*** NOTE: if you enter a DOS path name,it needs "
	echo "		to be double quoted or the backslashes will "
	echo " 		be striped.  \"c:\\CustomerInstalls\\RBC\\RBCWeb\" "
	echo " -n <email notification list> comma separated (optional)"
	echo " 		default=jfong@successmetricsinc.com,4157301327@vtext.com"
	echo " -p <port number>"
	echo "		Enter 4 digit port number for the installation"	
	echo " 		Installation will use port number +2"
	echo "		e.g. 9300, 9301, 9302"
	echo " -e <prod|dev> (optional)"
	echo "		define which environment you are deploying to"
	echo "		prod is for a production environment"
	echo "		dev is for a development environment"
	echo " -a <manual|auto> (optional)"
	echo "		should the service be started manually or automatically"
	echo "		manual will require the server be started manually."
	echo "		auto will start the service if the system is restarted (default)."
	echo " -t <build tag> "
	echo " 		If you are getting files from Releases/Platform,"
	echo "		enter the base Release Number e.g. base_2_2_0"
	echo ""
	echo " **************************************************************  "
	echo " Note:"
	echo " "
	echo " If you are re-creating or re-using a port that has an "
	echo " associated service, you will need to remove the service"
	echo " before you run create_instance.  The easiest way to remove"
	echo " a service is with the sc command.  The format is:  "
	echo " 	sc delete 'service_name'"
	echo " For example, if my service was DemoWeb8080, the command is:"
	echo " 	sc delete DemoWeb8080"
	echo " sc will return a status when it is dones"
	echo " "
	echo " Also, if you are using the script with a -e prod flag "
	echo " to create a local non-secure instance (http), you will need "
	echo " to remove the scheme=https parameter from your server "
	echo " definition.  The scheme-https was added since the production"
	echo " server have a secure load balancer in front of them.  This "
	echo " is not necessary for local http deployments.  You will probably"
	echo " cannot display this page error message. "
	echo ""
}

SMI_HOME="";					export SMI_HOME
Https_Port=""; 					export Https_Port
Release_Dir="";					export Release_Dir
Build_Tag="";					export Build_Tag
Notify=jfong@successmetricsinc.com;		export Notify
startup_opt="auto"				# default service startup to be automatic
Deployment_env="prod"				# setting default environment to be production

while getopts ":a:d:h:n:p:t:e:" opts; do
        case "$opts" in
		d ) echo "-d $OPTARG"
			DEBUG=$OPTARG
		 	export DEBUG
			;;
		a ) echo "-a $OPTARG"
			case "$OPTARG" in
				"manual" | "auto") startup_opt=$OPTARG
					echo "Deployment_env is " $Deployment_env
					;;
				* )	echo "-a was entered with an invalid value"
					echo "   the only values accepted are "
					echo "   manaul and auto"
					echo "   please correct before proceeding"
					echo "   or exclude to use the default which is auto"
					exit -1
					;;
			esac
			;;
		e ) echo "-e $OPTARG"
			case "$OPTARG" in
				"prod" | "dev") Deployment_env=$OPTARG
					echo "Deployment_env is " $Deployment_env
					;;
				* )	echo "-e was entered with an invalid value"
					echo "   the only values accepted are "
					echo "   prod and dev"
					echo "   please correct before proceeding"
					echo "   or exclude to use the default which is prod"
					exit -1
					;;
			esac
			;;
		h ) echo "-h $OPTARG" 
			SMI_HOME="$(cygpath -u -p -a "$OPTARG")"
			SMI_HOME="${SMI_HOME%/}"
			export SMI_HOME				# expect SMI_HOME to be in the following format
			Cust_Type=${SMI_HOME##*/}		# 	/cygdrive/c/CustomerInstalls/RBC/RBCWeb
			Customer_Home=${SMI_HOME%/*}		# so    ----------------------------
			export Customer_Home			#            Install Root            ---
			Customer_Name=${Customer_Home##*/}	#                          Customer Name ------
			export Customer_Name			#                                        Customer's installation type
			Cust_Root=${Customer_Home%/*}		#SMI_HOME=$Install_Root+$Customer_Name+Cust_Type
			export Cust_Root
			Install_Root=${Customer_Home%/*}
			export Install_Root
								#
								# if SMI_HOME is /c/CustomerInstalls/RBC/RBCWeb
								# we get the following breakdown
			echo $Install_Root			#  /c/CustomerInstalls
			echo $Customer_Home			#  /c/CustomerInstalls/RBC
			echo $Customer_Name			#  RBC
			echo $Cust_Type				#  RBCWeb
			;;
		n ) echo "-n $OPTARG" 
			Notify=$OPTARG
			export Notify 
			;;
		p ) echo "-p $OPTARG"
			if [ ! $(echo $OPTARG | grep '[0-9][0-9][0-9][0-9]') ]; then
				echo $OPTARG "is not a 4 digit port number"  
				exit 1
			else
				Https_Port=$OPTARG;  export Https_Port 
			fi
			;;
		t ) echo "-t $OPTARG" 
			Build_Tag=$OPTARG;	
			export Build_Tag
			;;
		*  ) Parameter_Help
			exit 1 ;;
	esac
	echo one value $1
done


if [ -z "$SMI_HOME" ] || [ -z $Https_Port ] || [ -z $Build_Tag ]; 
then
	echo "One or more of the required parameters was not provided.  "
	echo "Please provide all required paramaters:"
	Parameter_Help
	exit 1 
fi 
	
#
#Create working temp directory (with stupid name which should not exist)
#

Temp_Dir="/cygdrive/c/delete_me";			export Temp_Dir
echo $Temp_Dir
if [ -d $Temp_Dir ];
then
	rm -rf $Temp_Dir/*				# clean up the directory
else
	mkdir $Temp_Dir
	if [ $? != 0 ]; 
	then
		echo "Error trying to create my temporary working directory"
		echo "Please fix and try again"
		exit 1
	fi
fi

#
if [ $Deployment_env = "prod" ] 
then
	Http_Port=`expr $Https_Port`;	 			export Http_Port
	Https_Port=`expr $Http_Port + 1`; 			export Https_Port
	Shutdown_Port=`expr $Http_Port + 2`; 			export Shutdown_Port
	Internal_Port=`expr $Http_Port + 3`; 			export Internal_Port
else 
	Http_Port=`expr $Https_Port + 1`; 			export Http_Port
	Shutdown_Port=`expr $Https_Port + 2`; 			export Shutdown_Port
	Internal_Port=`expr $Https_Port + 3`; 			export Internal_Port
fi

CATALINA_HOME="$SMI_HOME"/tomcat; 				export CATALINA_HOME
CATALINA_BASE="$SMI_HOME"/tomcat; 				export CATALINA_BASE

#
#  1) Until we find a better place, grab it from \\smi\Builds\Releases\Platform\Base
#

cd $Temp_Dir

echo "open builds" >download.ftp
echo "smibuilds" >>download.ftp
echo "5771builds!" >>download.ftp
echo "verbose" >>download.ftp
echo "cd Releases/Platform/$Build_Tag" >>download.ftp
echo "prompt" >>download.ftp
echo "get base.zip" >>download.ftp
echo "quit" >>download.ftp

ftp -s:download.ftp  

echo NULL >download.ftp
rm download.ftp

if [ -s base.zip ]; 
then
	echo "FTP successfully copied files over to temp directory."
else
	echo "One or more required files did not get transfered properly or was not found.  Cannot proceed with upgrade preparations" \
		| mutt -s "Upgrade_prep: FTP had problems" $Notify
	exit -1
fi 

#
#  2) unzip it to somewhere
#

mkdir -p $Customer_Home
if [ $? != 0 ]; 
then
	echo "Customer already exist on the system.  Assume we are creating a new installation type for the customer"
fi

#
# Make sure the installation does not exist already.  
# We need to make sure the customer's installation type is not there 
# for example, If we are creating RBCWeb, the /CustomerInstalls/RBC diretory can exist
# and other installation types like /CustomerInstalls/RBC/RBCDev is alright.  We test
# to see that under /CustomerInstalls/RBC, there is no RBCDev.  We also check to see if
# there is a base directory.  If base exists, someone did not complete the creation of
# a new instance (base.zip creates the base directory).  Issue a warning so we clean it 
# up first.  
cd $Customer_Home
if [ -d base ]  || [ -d $Cust_Type ]; 
then
	echo "Something is wrong, the Base or Customer directory exist for this customer. \n Do we have the right customer or do we need to do some clean up?"
	echo "We are attempting to install Cust Type : $Cust_Type  for  $Customer_Name  with the install root of  $Install_Root "
	echo "Something is wrong, Do we have the right customer or do we need to do some clean up?" \
		| mutt -s "Create Instance: Base or Customer directory already exists" $Notify
	exit -1
fi
 
unzip $Temp_Dir/base.zip -d .
mv ./base ./$Cust_Type

#
# Change the privileges to make things work.
#
chmod 777 "$SMI_HOME"
chmod -R 755 "$SMI_HOME"/jdk
chmod 777 "$SMI_HOME"/logs
chmod -R 777 "$SMI_HOME"/catalog
chmod -R 777 "$SMI_HOME"/cache
chmod 755 "$SMI_HOME"/tomcat/bin/tomcat*
chmod 777 "$SMI_HOME"/tomcat/conf
chmod 777 "$SMI_HOME"/tomcat/work
chmod 777 "$SMI_HOME"/tomcat/logs
chmod 777 "$SMI_HOME"/tomcat/webapps
chmod 777 "$SMI_HOME"/tomcat/temp
#
# 3) cd somewhere\tomcat\conf
#
 
cd "$SMI_HOME"/tomcat/conf

#
# 4) edit server.xml
# 

if [ ! -s ./server.xml ]; 
then
	echo "The server.xml file did not populate properly." \
		| mutt -s "Create Instance: server.xml file is missing or empty" $Notify
	exit -1
fi

cp ./server.xml ./temp.xml

#     change 8005 to the customer port # for shutdown - if this is the only server on the machine and if not for customer use, you can leave the port as is
#     change  8080 to the customer port # for connections - ditto
#     change  8443 to the customer port # for ssl connections - ditto
#     change Zort to the customer web app name (for example, FMR would be FMRWeb, RBC would be RBCWeb)
#                - this allows you to access the web app as http://host:port/Zort/Login rather than http://host:port/SMIWeb/Login
#     ADVANCED: possibly change the scheme for the http connection connection (for production deployment when the server is behind something that is terminating the SSL connection)
#     ADVANCED: configure the https (for development sites that need SSL for customer access, not recommended for internal test sites; this requires the generation of a local certificate and the creation of a keystore)


cat ./temp.xml | \
	sed '1,$ s^8005^'$Shutdown_Port'^' | \
	sed '1,$ s^8080^'$Http_Port'" scheme="https^' | \
	sed '1,$ s^8443^'$Https_Port'^' | \
	sed '1,$ s^8081^'$Internal_Port'^' |\
	sed '1,$ s^Zort^'$Cust_Type'^' \
	 >./server.xml

if [ $Deployment_env = "prod" ] 
then
	cp ./server.xml ./temp.xml
	cat ./temp.xml | \
		sed '1,$ s^<Connector port="'$Https_Port'"^<!--  <Connector port="'$Https_Port'"^' | \
		sed '1,$ s^keystoreFile="conf/tomcat.keystore" />^keystoreFile="conf/tomcat.keystore" /> -->^' \
		 >./server.xml
fi

rm ./temp.xml

#
# 5) cd somewhere\tomcat\bin
#
 
cd "$SMI_HOME"/tomcat/bin
 
#
# 6) service install ServiceName
#      - normally ServiceName will be the customer designation (FMR, RBC, ...) with the customer connection port #, for example RBC7314
#      - the service will appear in the service control panel as 'SMI ServiceName'
#      - the service will not be started and it will be marked as 'Manual'.  You should change it to 'Automatic' and start it.

CATALINA_HOME=`cygpath --absolute --windows "$CATALINA_HOME"`; export CATALINA_HOME
CATALINA_BASE=`cygpath --absolute --windows "$CATALINA_BASE"`; export CATALINA_BASE
JAVA_HOME=`cygpath --absolute --windows "$SMI_HOME\jdk"`; export JAVA_HOME

if [ $startup_opt = "auto" ];
then
	cat ./service.bat | \
		sed '1,$ s^--StartParams^--Startup auto --StartParams^' \
		> service2.bat
	mv ./service2.bat ./service.bat
fi 

if [ $Deployment_env = "prod" ] 
then
	cmd /c service.bat install  $Cust_Type$Http_Port
	net start $Cust_Type$Http_Port
else
	cmd /c service.bat install  $Cust_Type$Https_Port
	net start $Cust_Type$Https_Port
fi

#net start $Cust_Type$Https_Port
if [ $? == 0 ];
then
	echo started then $Cust_Type$Https_Port service
else
	echo "Had problems starting the $Cust_Type$Https_Port service." \
		| mutt -s "Create Instance: Unable to start service" $Notify
        exit -1
fi 



# 7) you should be able to access the server via:
#	https://hosts:port 
#	    - a default tomcat page should appear
#
#	Not sure how to do this step
 
#
#	Done with creating the instance
#

rm -rf $Temp_Dir

exit 

# 8) you should now be ready to start deploying our software (repository.xml into conf, catalog into catalog, SMIWeb.war into tomcat\webapps).  That script is coming....
#
# 9) you can start and stop the services with the following commands:
#
#                net start �SMI servicename�
#
#                net stop �SMI servicename�
#
# 10) you can remove the service with the following command:
#
#                service remove ServiceName

