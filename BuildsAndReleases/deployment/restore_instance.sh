#
# Help for parameters
#

function Parameter_Help
{
	echo "usage: deploy_customer [-options]"
	echo "where options include:"
	echo " -h <smi_home> "
	echo "		Enter the home directory of the instance"
	echo " 		e.g./cygdrive/c/CustomerInstalls/RBC/RBCWeb"
	echo "		*** NOTE: if you enter a DOS path name,it needs "
	echo "		to be double quoted or the backslashes will "
	echo " 		be striped.  \"c:\\CustomerInstalls\\RBC\\RBCWeb\" "
	echo " -b <Backup Directory Name>"
	echo "		Enter name of the backup directory you want to use for the restore"
	echo "		e.g. RBCWeb-Customer_200803041620"
	echo "		You will find the backup sub-directories under the main customer"
	echo "		directory.  They are indentified by Customer installation type, "
	echo "		type of deployment (platform or customer) and a timestamp"
	echo " -n <email notification list> comma separated (optional)"
	echo " 		default=jfong@successmetricsinc.com,4157301327@vtext.com"
	echo " -p <port number>"
	echo "		Enter 4 digit port number for the installation"	
	echo " 		Installation will use port number +2"
	echo "		e.g. 9300, 9301, 9302"
}		# end of the Parameter_Help function



#
# end of Functions
#---------------------------------------------------------------------------------------------------
# 
#
# setup defaults
#
DEBUG=0;					export DEBUG			#debug=1, not debug=0
timestamp=`date +%Y%m%d%H%M`
SMI_HOME="";					export SMI_HOME
Https_Port=""; 					export Https_Port
Backup_Name="";					export Backup_Name
Notify=jfong@successmetricsinc.com;		export Notify
user_defined_URL="false"			# setting default for URL rename.
retain_cache="false"
Two_One="false"
echo done clearing stuff

while getopts ":b:d:h:n:p:" opts; do
        case "$opts" in
		d ) echo "-d $OPTARG"
			DEBUG=$OPTARG
		 	export DEBUG
			;;
		h ) echo "-h $OPTARG" 
			SMI_HOME="$(cygpath -u -p -a "$OPTARG")"
			SMI_HOME="${SMI_HOME%/}"
			export SMI_HOME				# expect SMI_HOME to be in the following format
			Cust_Type=${SMI_HOME##*/}		# 	/cygdrive/c/CustomerInstalls/RBC/RBCWeb
			Customer_Home=${SMI_HOME%/*}		# so    ----------------------------
			export Customer_Home			#            Install Root            ---
			Customer_Name=${Customer_Home##*/}	#                          Customer Name ------
			export Customer_Name			#                                        Customer's installation type
			Cust_Root=${Customer_Home%/*}		#SMI_HOME=$Install_Root+$Customer_Name+Cust_Type
			export Cust_Root
			Install_Root=${Customer_Home%/*}
			export Install_Root

			echo "$SMI_HOME"
			echo "$Install_Root"
			echo "$Customer_Home"
			echo "$Customer_Name"
			echo "$Cust_Type"
			;;
		b ) echo "-b $OPTARG" 
			Backup_Name=$OPTARG
			export Notify 
			;;
		n ) echo "-n $OPTARG" 
			Notify=$OPTARG
			export Notify 
			;;
		p ) echo "-p $OPTARG"
			echo $OPTARG | grep [^0-9] >/dev/null 2>&1
			if [ "$?"  -eq "0" ]; then
				echo $OPTARG "is not a number.  The Port Number needs to be numeric"  
				exit 1
			else
				Https_Port=$OPTARG;  export Https_Port 
			fi
			;;
		*  ) Parameter_Help
			exit 1 ;;
	esac
	echo one value $1
done
shift $(($OPTIND - 1))

echo  SMI_HOME "$SMI_HOME"
echo  Https_Port $Https_Port 
echo  Backup Filename $Backup_Name


if [ -z $SMI_HOME ] || [ -z $Https_Port ] || [ -z $Backup_Name ]; 
then
	echo "One or more of the required parameters was not provided.  "
	echo "Please provide all required paramaters:"
	Parameter_Help
	exit 1 
fi 

temp1=${Backup_Name#$Cust_Type-}
Backup_Type=${temp1%%_*}
Cust_Backup="$Customer_Home/$Backup_Name/Backup_$Backup_Type.zip"

if [ -d $Customer_Home/$Backup_name ] && [ -e $Cust_Backup ];
then
	echo "The backup directory provide exists"
else
	echo "The backup directory does not exist, Please verify directory name"
	exit -1
fi 

#
# Shutdown the server
#

echo "shutdown the server"
net stop $Cust_Type$Https_Port
status=$?
if [ $status == 0 ] || [ $status == 2 ]; 
then
	echo "Service has been stopped or is already down"  
else
	echo "Not sure what state the Tomcat service is in.  Please make sure service is down before rerunning script." \
		| mutt -s "Deploy_customer: Having problems stopping service" $Notify
	exit -1
fi


#
# Move the current directory
#

if [ -d "$SMI_HOME"_backup ];
then
	rm -rf "$SMI_HOME"_Backup
	if [ $? = 0 ];
	then
		echo "Backup directory removed without problems"
	else
		echo "Backup directory could not be removed completely, please fix manuall"
		exit  -1 
	fi
fi

mv "$SMI_HOME" "$SMI_HOME"_backup
if [ $? = 0 ];
then
	echo "SMI_HOME moved without problems"
else
	echo "Problems moving SMI_HOME,  Please fix and rerun"
	exit -1 
fi

mkdir "$SMI_HOME"
chmod 777 "$SMI_HOME"
cd "$SMI_HOME"

unzip -uo "$Cust_Backup" 
if [ $? == 0 ]; 
then
	echo "Backup of instance was successful"  
else
	echo "Exiting since restore was not successful in unzipping the backup, manual restore required. " \
		| mutt -s "Deploy_customer: Unable to restore instance" $Notify
	exit -1
fi
#
# Change the privileges to make things work.
#
chmod -R g=rwx "$SMI_HOME"
chmod -R 755 "$SMI_HOME"/jdk
chmod 777 "$SMI_HOME"/logs
chmod -R 777 "$SMI_HOME"/catalog
chmod -R 777 "$SMI_HOME"/cache
chmod 755 "$SMI_HOME"/tomcat/bin/tomcat*
chmod 777 "$SMI_HOME"/tomcat/conf
chmod 777 "$SMI_HOME"/tomcat/work
chmod 777 "$SMI_HOME"/tomcat/logs
chmod 777 "$SMI_HOME"/tomcat/webapps

net start $Cust_Type$Https_Port
if [ $? == 0 ];
then
	echo "started the "$Cust_Type$Https_Port" service"
else
	echo "Had problems starting " $Cust_Type$Https_Port service." Manual help required"  \
		| mutt -s "Deploy_Customer: Restore is unable to start service" $Notify
	exit -1
fi 

#
# end of the Restore_Customer function
#
