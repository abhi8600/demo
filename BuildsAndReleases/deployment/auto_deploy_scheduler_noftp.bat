echo on
echo %1% %2% %3% %4%
set TimeStamp=""
s
REM
REM We need a timestamp.  If it is not provided, we should create our own.
REM

if %TimeStamp% neq "" ( goto TIMESTAMP_DEFINED: ) 
	
for /f "tokens=1-3 delims=:." %%a in ("%time%") do set hh_mm_stamp=%%a%%b
echo %hh_mm_stamp%
echo %hh_mm_stamp:~0,1%
echo %hh_mm_stamp:~1,3%
if "%hh_mm_stamp:~0,1%" == " " ( set hh_mm_stamp=0%hh_mm_stamp:~1,3%)
echo %hh_mm_stamp%
for /f "tokens=1-3 delims=\ " %%a in ("%date%") do set date_only=%%b
for /f "tokens=1-3 delims=/." %%a in ("%date_only%") do set teststamp=%%c%%a%%b
echo %teststamp%
set TimeStamp=%teststamp%%hh_mm_stamp%
	
:TIMESTAMP_DEFINED
echo TimeStamp: -%TimeStamp%-

FOR /F "tokens=1,2,3 delims==" %%A IN (\\sfojfong\Current_Build_Number\HEAD\build_number.txt) DO if "%%A" == "build_number" (set build_number=%%B) else (ECHO "No build Number "%%A"-" %%B"-")
echo -%build_number%-

cd c:\delete_me\No_ftp
del /F /Q /S *
xcopy \\smi\builds\Nightly\%build_number% .

cd C:\SMI\SetupTools

\\%COMPUTERNAME%\SMI\SetupTools\deploy_scheduler.bat -a c:\SMI -p 6100 -r Nightly -t %build_number% -i sfoqa1 -f 1 > scheduler.log 2>&1

exit /B 0