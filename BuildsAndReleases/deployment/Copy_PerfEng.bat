ECHO ON

set VarNotify=jfong@successmetricsinc.com
set VarMailText=Text message as part of the Mail
set VarMailSubject=Copy_PerfEng
set Branch_name=""
set Current_Server=""
set /A error_count=0

:PROCESS_INPUT

if "%1"=="-n" goto SET_NOTIFICATION:
if "%1"=="-b" goto SET_BRANCH:
if "%1"=="-s" goto SET_CURRENTSERVER:
if "%1"=="-t" goto SET_TIMESTAMP:
if "%1"=="" goto PROCESS_INPUT_DONE:

goto PROCESS_INPUT_ERROR:

:SET_TIMESTAMP
shift
set TimeStamp=%1
goto PROCESS_INPUT_LOOP:

:SET_BRANCH
shift
set Branch_name=%1
goto PROCESS_INPUT_LOOP:

:SET_CURRENTSERVER
shift
set Current_Server=%1
echo %Current_Server%
goto PROCESS_INPUT_LOOP:


:SET_NOTIFICATION
shift
set VarNotify=%1
goto PROCESS_INPUT_LOOP:

:PROCESS_INPUT_LOOP

shift
goto PROCESS_INPUT:

:PROCESS_INPUT_ERROR

echo ERROR: invalid input parameter
echo usage: DataConductorBuild.bat [-options]
echo where options include:
echo.
echo  -b ^<Branch^> 
echo 		enter the branch for the Perf Eng e.g. 5.1.0, 5.1.1, 5.2.0
echo  -s ^<Current Server^>
echo            the server you are running from
echo  -n ^<email notification list^> comma separated (optional)
echo  		default=jfong@successmetricsinc.com,4157301327@vtext.com
echo 		NOTE - multiple email addresses need to be separated by 
echo 			commas and enclosed in double quote
echo.

set VarMailText=Invalid arguement(s) provided. 
set VarMailSubject=DataConductorBuild:Process Arguements
goto BADEXIT

:PROCESS_INPUT_DONE

if %Branch_name% == "" goto PROCESS_INPUT_ERROR:
if %Current_Server% == "" goto PROCESS_INPUT_ERROR:

dir c:\Birst\%Branch_name% 
IF %ERRORLEVEL% LSS 1 goto START_COPY:
set VarMailText=The Performance Engine version doesn't exist
set VarMailSubject=Copy_PerfEng:No Perf Engine
goto BADEXIT

:START_COPY
rem	setlocal EnableDelayedExpansion

for  /f "tokens=1" %%A in (D:\BIRST_HOME\BIRST\SetupTools\Copy_PerfEng_Servers.lst) do (
	echo %%A
	if %Current_Server% NEQ %%A  ( call :DO_COPY %%A %Branch_name% %Current_Server% ) 
	)
	
goto THE_END:


:BADEXIT
echo ------------------ Bad Exit -------------------------------------------
echo %VarMailText%
blat -to %VarNotify% -subject "%VarMailSubject%" -body "%VarMailText%"
rem *** if exist %VarCustProp% del %VarCustProp%
echo "This batch file failed."
rem	exit 1

:THE_END

rem	exit 0
goto EOF:



:DO_COPY
set t_server=%1%
set t_branch=%2%
set t_current= %3%
echo %t_server% %t_branch% %t_current%

rem dir \\%t_server%\Birst\%t_branch%
xcopy /Y  /E /I /V c:\Birst\%t_branch% \\%t_server%\Birst\%t_branch% 
IF %ERRORLEVEL% LSS 1 goto EOF:
set VarMailText=There were problems copying %t_branch% from %t_current% to %t_server%
set VarMailSubject=Copy_PerfEng:Error
blat -to jfong@birst.com -subject "%VarMailSubject%" -body "%VarMailText%"

goto EOF:

:EOF
