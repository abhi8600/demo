ECHO OFF
REM 
REM These should be input parameters in the future
REM
echo %1% %2% %3% %4%
set SMI_HOME=""
set Release_Dir=""
set Build_Tag=""
set SMI_Port=""
set TimeStamp=""
set Env_Name=""
set CurrentDir=%CD%
set Clear_Cache=""
set Clean_Flag=1

REM 
REM Setup parameters for notification
REM 

set VarNotify=jfong@successmetricsinc.com
set VarMailText=Setup email message
set VarMailSubject=Deploy Acorn

:PROCESS_INPUT

if "%1"=="-h" goto SET_SMI_HOME:
if "%1"=="-n" goto SET_NOTIFICATION:
if "%1"=="-p" goto SET_SMI_PORT:
if "%1"=="-k" goto SET_CLEAN_DIR:
if "%1"=="-s" goto SET_TIMESTAMP:
if "%1"=="-t" goto SET_BUILD_TAG:
if "%1"=="-e" goto SET_ENV_NAME:
if "%1"=="-c" goto SET_CLEAR_CACHE:
if "%1"=="" goto PROCESS_INPUT_DONE:

goto PROCESS_INPUT_ERROR:

:SET_TIMESTAMP
shift
set TimeStamp=%1
goto PROCESS_INPUT_LOOP:

:SET_SMI_HOME
shift
set SMI_HOME=%1
goto PROCESS_INPUT_LOOP:

:SET_CLEAN_DIR
shift
set Clean_Flag=%1
goto PROCESS_INPUT_LOOP:

:SET_BUILD_TAG
shift
set Build_Tag=%1
goto PROCESS_INPUT_LOOP:

:SET_ENV_NAME
shift
set Env_Name=%1
goto PROCESS_INPUT_LOOP:

:SET_SMI_PORT
shift
set SMI_Port=%1
goto PROCESS_INPUT_LOOP:

:SET_NOTIFICATION
shift
set VarNotify=%1
goto PROCESS_INPUT_LOOP:

:SET_CLEAR_CACHE
shift
set Clear_Cache=%1
goto PROCESS_INPUT_LOOP:

:PROCESS_INPUT_LOOP

shift
goto PROCESS_INPUT:

:PROCESS_INPUT_ERROR

echo ERROR: invalid input parameter
echo usage: deploy_acorn [-options]
echo where options include:
echo.
echo  -s ^<Time Stamp^> (optional)
echo 		Enter the Time Stamp for identifying the deployment
echo		the format is YYYYMMDDHHMM, or one will be generated.		
echo  -h ^<SMI_HOME^>
echo 		Enter the home directory of the instance, the
echo 		default is usually  c:\SMI
echo 		This is the parent directory of Acorn and SMIWeb
echo  -p ^<port number^>
echo 		Enter 4 digit port number for the installation	
echo  		Installation will use port number +2
echo 		e.g. 9300, 9301, 9302
echo  -k ^<clean_flag^> (optional)
echo 		Any non-zero value for this flag will clean temp directories
echo 		Default is 1 for clean temp directories.
echo  -t ^<Build tag^> 
echo  		If you are getting files from Releases/Platform,
echo 		enter the release tag e.g. FMR_081127, RBC_081204
echo  -e ^<Environment^>
echo 		The environment you want to deploy to 
echo 		e.g. dev, test, prod, prod_sso
echo  -n ^<email notification list^> comma separated (optional)
echo  		default=jfong@successmetricsinc.com,4157301327@vtext.com
echo 		NOTE - mulitple email addresses need to be separated by 
echo 			commas and inclosed in double quote
echo.

set VarMailText=Invalid arguement(s) provided. 
set VarMailSubject=Deploy_Customer:Process Arguements
goto BADEXIT

:PROCESS_INPUT_DONE

REM
REM We need a timestamp.  If it is not provided, we should create our own.
REM

if %TimeStamp% neq "" ( goto TIMESTAMP_DEFINED: ) 
	
for /f "tokens=1-3 delims=:." %%a in ("%time%") do set hh_mm_stamp=%%a%%b
echo %hh_mm_stamp%
echo %hh_mm_stamp:~0,1%
echo %hh_mm_stamp:~1,3%
if "%hh_mm_stamp:~0,1%" == " " ( set hh_mm_stamp=0%hh_mm_stamp:~1,3%)
echo %hh_mm_stamp%
for /f "tokens=1-3 delims=\ " %%a in ("%date%") do set date_only=%%b
for /f "tokens=1-3 delims=/." %%a in ("%date_only%") do set teststamp=%%c%%a%%b
echo %teststamp%
set TimeStamp=%teststamp%%hh_mm_stamp%
	
:TIMESTAMP_DEFINED
echo TimeStamp: -%TimeStamp%-

REM 
REM Break apart SMI_HOME to figure out :
REM	drive letter
REM	Install_Root
REM	Customer_Name
REM	Customer_Type

for /f "delims=:, tokens=1,2" %%A in ("%SMI_HOME%") do ( set DriveLetter=%%A )
set DriveLetter=%DriveLetter: =%

for /f "delims=\, tokens=1-4" %%A in ("%SMI_HOME%") do ( 
	echo %%A-%%B-%%C-%%D 
	set Install_Root=%%A\%%B
	set Customer_Name=%%C
	set Customer_Type=%%D
	)

set Service_Name=%Customer_Type%%SMI_Port%
set Cust_Backup=%Install_Root%\%Customer_Name%\%Customer_Type%-Customer_%TimeStamp%
set Rel_Dir=%Customer_Name%_%Build_Tag%_%Env_Name%

echo SMI_HOME		= %Install_Root%\%Customer_Name%\%Customer_Type%
echo TimeStamp		= %TimeStamp%
echo VarNotify		= %VarNotify%
echo SMI_HOME		= %SMI_HOME%
echo DriveLetter	= %DriveLetter%
echo Build_Tag		= %Build_Tag%
echo SMI_Port		= %SMI_Port%
echo Clean_Flag		= %Clean_Flag%

if %SMI_HOME%=="" ( goto PROCESS_INPUT_ERROR )
if %Build_Tag%=="" ( goto PROCESS_INPUT_ERROR )
if %SMI_Port%=="" ( goto PROCESS_INPUT_ERROR )
if %Env_Name%=="" ( goto PROCESS_INPUT_ERROR )

set DeleteMeDir=%DriveLetter%:\delete_me\%TimeStamp%
echo DeleteMeDir: -%DeleteMeDir%-

REM
REM Goto the same drive as the Acorn installation
REM

%DriveLetter%:

REM 
REM Setup the delete_me directory
REM 

dir %DeleteMeDir%
set /a Compare_Error_Cnt=%ERRORLEVEL% 
if %Compare_Error_Cnt% EQU 0 ( del /F /Q %DeleteMeDir%\* 
	) else (
	mkdir %DeleteMeDir% )

REM
REM where is winzip?
REM This handles the standard 32 and 64 bit locations
REM

set WinZip_Home=""
if exist "C:\Program Files\WinZip" (set WinZip_Home="c:\Program Files\WinZip")
if exist "C:\Program Files (x86)\WinZip" (set WinZip_Home="C:\Program Files (x86)\WinZip")
if %WinZip_Home% neq "" (goto WinZip_Found:)
echo We can't find the WinZip directory"
set VarMailText=Cannot file WinZip in the default 32 and 64 bit configurations. 
set VarMailSubject=Deploy_Customer:WinZip directory missing
goto BADEXIT

:WinZip_Found
REM
REM --------------------------------------------------------------------------------------------
REM  Shutdown the server
REM 

net stop %Service_Name%
if %ERRORLEVEL% == 0 ( goto SHUTDOWN_DONE:)
IF %ERRORLEVEL% == 2  ( goto SHUTDOWN_DONE:)
set VarMailText=Stop/Shutdown of the SMIWEB server did not shutdown normally. 
set VarMailSubject=Deploy_Customer:SMIWeb Shutdown 
goto BADEXIT

:SHUTDOWN_DONE


REM 
REM --------------------------------------------------------------------------------------------
REM Backup the configuration files
REM

mkdir %Cust_Backup% 
IF %ERRORLEVEL% LSS 1 goto BACKUP_DIR_CREATED:
set VarMailText=Backup of the SMI environment failed, could not create backup directory %Cust_Backup%. 
set VarMailSubject=Deploy_Customer:SMI Backup
goto BADEXIT:

:BACKUP_DIR_CREATED
cd %SMI_HOME%
%WinZip_Home%\WZZIP.EXE -ex -r -P -a %Cust_Backup%\Backup_Customer.zip .
IF %ERRORLEVEL% LSS 1 goto BACKUP_CUST:
set VarMailText=Backup of the SMI environment failed, Please correct problem before proceeding. 
set VarMailSubject=Deploy_Customer:SMI Backup
goto BADEXIT

:BACKUP_CUST
cd %SMI_HOME%\catalog
%WinZip_Home%\WZZIP.EXE -ex -r -P -a %Cust_Backup%\%Customer_Type%_Catalog.zip .
IF %ERRORLEVEL% LSS 1 goto BACKUP_CATALOG:
set VarMailText=Backup of the SMI environment failed, Please correct problem before proceeding. 
set VarMailSubject=Deploy_Customer:SMI Backup
goto BADEXIT

:BACKUP_CATALOG

REM
REM Get the data files
REM

cd %DeleteMeDir%

echo open builds >download.ftp
echo smibuilds>>download.ftp
echo 5771builds!>>download.ftp
echo verbose >>download.ftp
echo cd   Releases/Customers/%Customer_name%/%Rel_Dir% >>download.ftp
echo prompt >>download.ftp
echo get SMIWeb.war >>download.ftp
echo get Customer.zip >>download.ftp
echo quit >>download.ftp

ftp -s:download.ftp  

REM echo NULL >download.ftp
REM rm download.ftp

if not exist SMIWeb.war (goto FTP_Problems:)
if not exist Customer.zip (goto FTP_Problems:)
goto Upgrade_SMI:

:FTP_Problems
set VarMailText=One or more of the FTP files were not recieved.  Please investigate the problem.  
set VarMailSubject=Deploy_Customer:FTP issues
goto BADEXIT



:Upgrade_SMI
REM 
REM --------------------------------------------------------------------------------------------
REM upgrade SMI
REM

cd %SMI_HOME%
dir C:\workspace\BuildsAndReleases\deployment\test\*.* /b /a | find /v "NoTlIkElY" >nul&& echo NOT empty || echo Empty

echo "shutdown the server"
net stop %Service_Name%
if %ERRORLEVEL% == 2 (
	goto SHUTDOWN_DONE: 
	) else (
	if %ERRORLEVEL% == 0 ( goto SHUTDOWN_DONE: ))
set VarMailText=Shutdown of the SMIWEB server did not complete normally. 
set VarMailSubject=Deploy_Customer:Shutdown did not complete
goto BADEXIT

:SHUTDOWN_DONE

REM
REM Clean up before updating the customer files. 
REM

if %Clean_Flag% equ 0 ( goto Update_SMIWeb: )
cd %SMI_HOME%\tomcat\temp
del /Q /S *

dir %SMI_HOME%\conf\*.* /b /a | find /v "NoTlIkElY" >nul&& goto CLEAN_CONF: || goto CLEANED_CONF:
:CLEAN_CONF
del /F /S /Q %SMI_HOME%\conf\* 
IF %ERRORLEVEL% LSS 1 goto CLEANED_CONF:
set VarMailText=SMI had problems cleaning the conf directory, Please correct problem before proceeding. 
set VarMailSubject=Deploy_Customer:Del conf
goto BADEXIT

:CLEANED_CONF

dir %SMI_HOME%\catalog\shared\Dashboards\*.* /b /a | find /v "NoTlIkElY" >nul&& goto CLEAN_DASHBOARD: || goto CLEANED_DASHBOARD:
:CLEAN_DASHBOARD
del /F /S /Q %SMI_HOME%\catalog\shared\Dashboards\* 
IF %ERRORLEVEL% LSS 1 goto CLEANED_DASHBOARD:
set VarMailText=SMI had problems cleaning the shared dashboard directory, Please correct problem before proceeding. 
set VarMailSubject=Deploy_Customer:Del dashboard
goto BADEXIT

:CLEANED_DASHBOARD

dir %SMI_HOME%\catalog\shared\Images\*.* /b /a | find /v "NoTlIkElY" >nul&& goto CLEAN_IMAGES: || goto CLEANED_IMAGES:
:CLEAN_IMAGES
del /F /S /Q %SMI_HOME%\catalog\shared\Images\* 
IF %ERRORLEVEL% LSS 1 goto CLEANED_IMAGES:
set VarMailText=SMI had problems cleaning the shared Images directory, Please correct problem before proceeding. 
set VarMailSubject=Deploy_Customer:Del Images
goto BADEXIT

:CLEANED_IMAGES

dir %SMI_HOME%\catalog\shared\Reports\*.* /b /a | find /v "NoTlIkElY" >nul&& goto CLEAN_REPORTS: || goto CLEANED_REPORTS:
:CLEAN_REPORTS
del /F /S /Q %SMI_HOME%\catalog\shared\Reports\* 
IF %ERRORLEVEL% LSS 1 goto CLEANED_REPORTS:
set VarMailText=SMI had problems cleaning the shared reports directory, Please correct problem before proceeding. 
set VarMailSubject=Deploy_Customer:Del reports
goto BADEXIT

:CLEANED_REPORTS

dir %SMI_HOME%\catalog\shared\dir.xml /b /a | find /v "NoTlIkElY" >nul&& goto CLEAN_DIR_XML: || goto CLEANED_DIR_XML:
:CLEAN_DIR_XML
del /F /S /Q %SMI_HOME%\catalog\shared\dir.xml 
IF %ERRORLEVEL% LSS 1 goto CLEANED_DIR_XML:
set VarMailText=SMI had problems cleaning the shared dir.xml file, Please correct problem before proceeding. 
set VarMailSubject=Deploy_Customer:Del dir.xml
goto BADEXIT

:CLEANED_DIR_XML

REM
REM Update the Customer files
REM

%WinZip_Home%\WZUNZIP.EXE -d -ybc -o -yo "%DeleteMeDir%\Customer.zip" conf/* "%SMI_HOME%"
set /A error_count=%ERRORLEVEL%

%WinZip_Home%\WZUNZIP.EXE -d -ybc -o -yo "%DeleteMeDir%\Customer.zip" catalog/* "%SMI_HOME%"
set /A error_count=%error_count%+%ERRORLEVEL%
IF %error_count% LSS 1 goto SMI_UNZIP_DONE:
set VarMailText=SMI had problems Unzipping Customer.zip, Please correct problem before proceeding. 
set VarMailSubject=Deploy_Customer:Customer unzip
goto BADEXIT

:SMI_UNZIP_DONE
cd %SMI_HOME%\tomcat\webapps
rmdir /S /Q %SMI_HOME%\tomcat\webapps\SMIWeb
IF %ERRORLEVEL% LSS 1 goto SMI_RMDIR_DONE:
set VarMailText=SMI had problems removing SMIWeb, Please correct problem before proceeding. 
set VarMailSubject=Deploy_Customer:SMI rmdir
goto BADEXIT

:SMI_RMDIR_DONE
del %SMI_HOME%\tomcat\webapps\SMIWeb.war
IF %ERRORLEVEL% LSS 1 goto SMI_DEL_DONE:
set VarMailText=SMI had problems deleting SMIWeb.war, Please correct problem before proceeding. 
set VarMailSubject=Deploy_Customer:SMI Del
goto BADEXIT

:SMI_DEL_DONE
dir

copy %DeleteMeDir%\SMIWeb.war %SMI_HOME%\tomcat\webapps
IF %ERRORLEVEL% LSS 1 goto SMI_COPY_DONE:
set VarMailText=SMI had problems copying new SMIWeb.war, Please correct problem before proceeding. 
set VarMailSubject=Deploy_Customer:SMI Copy
goto BADEXIT

:SMI_COPY_DONE
cd %SMI_HOME%

echo "startup the server"
net start %Service_Name%
if %ERRORLEVEL% == 0  ( goto STARTUP_DONE:)
set VarMailText=Startup of the SMIWEB server did not startup normally. 
set VarMailSubject=Deploy_Customer:SMI startup did not complete
goto BADEXIT

:STARTUP_DONE

REM sleep for 30 seconds
ping -n 30 127.0.0.1 > NUL 2>&1

goto THE_END:


:BADEXIT
echo ------------------ Bad Exit -------------------------------------------
echo %VarMailText%
blat -to %VarNotify% -subject "%VarMailSubject%" -body "%VarMailText%"
rem *** if exist %VarCustProp% del %VarCustProp%
echo "Acorn Deployment failed."
exit /B 1

:THE_END

exit /B 0


