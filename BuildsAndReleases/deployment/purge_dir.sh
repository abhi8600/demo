#!/usr/bin/bash
 
declare -i file_count=0
declare -i max_count=0
Notify=jfong@successmetricsinc.com;		export Notify
dir_file=""


#
# Help for parameters
#

function Parameter_Help
{
	echo "usage: purge_dir [-options]"
	echo "where options include:"
	echo " -s <director and prefix of directory> "
	echo "		Enter the home directory and the prefix of the files "
	echo "		you want to purge:"
	echo " 		e.g./cygdrive/c/CustomerInstalls/RBC/RBCWeb-Platform"
	echo "		This will purge backup directories with that start with"
	echo "		RBCWeb-Platform like RBCWeb-Platform-200802220805 "
	echo "		or RBCWeb-Platform-PreviousPeriod."
	echo "		*** NOTE: if you enter a DOS path name,it needs "
	echo "		to be double quoted or the backslashes will "
	echo " 		be striped.  \"c:\\CustomerInstalls\\RBC\\RBCWeb\" "
	echo " -x <number of backups you want to save>"
	echo "		define the number of backups you want to save on"
	echo "		your local machine.  For example, 4 will save you "
	echo "		the last 4 backups of the system"
	echo " -n <email notification list> comma separated (optional)"
	echo " 		default=jfong@successmetricsinc.com,4157301327@vtext.com"
}		# end of the Parameter_Help function


while getopts ":d:s:x:n:" opts; do
        case "$opts" in
		d ) echo "-d $OPTARG"
			DEBUG=$OPTARG
		 	export DEBUG
			;;
		s ) echo "-s $OPTARG"
			dir_file="$(cygpath -u -p -a "$OPTARG")"
			dir_file="$dir_file*"
			stat $dir_file >/dev/null 2>&1
			if [ "$?"  -ne "0" ]; then
				echo "There are not directory matching $dir_file, please re-enter"  
				exit 1
			fi
			;;
		n ) echo "-n $OPTARG" 
			Notify=$OPTARG
			export Notify 
			;;
		x ) echo "-x $OPTARG"
			echo $OPTARG | grep [^0-9] >/dev/null 2>&1
			if [ "$?"  -eq "0" ]; then
				echo $OPTARG "is not a number.  The file retention value has to be a number"  
				exit 1
			else
				max_count=$OPTARG
			fi
			;;
		*  ) Parameter_Help
			exit 1 ;;
	esac
	echo one value $OPTARG
done

if [ $max_count -eq 0 ] || [ $dir_file = "" ];
then
	Parameter_Help
	exit 1
fi

ls -p1dt $dir_file | \
while read dirname;
do 
	echo $dirname
	if [ $file_count -ge $max_count ];
	then
		echo "Deleted $dirname"
		rm -r $dirname
	fi
	file_count=file_count+1
done 

