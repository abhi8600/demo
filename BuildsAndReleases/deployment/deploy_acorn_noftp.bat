ECHO ON
REM 
REM These should be input parameters in the future
REM
echo %1% %2% %3% %4%
set Acorn_Root=""
set Release_Dir=""
set Build_Tag=""
set SMI_Port=""
set TimeStamp=""
set Instance=""
set CurrentDir=%CD%
set Clean_Flag=1
set Archive_Flag=0

REM 
REM Setup parameters for notification
REM 

set VarNotify=jfong@successmetricsinc.com
set VarMailText=Setup email message
set VarMailSubject=Deploy Acorn

:PROCESS_INPUT

if "%1"=="-a" goto SET_ACORN:
if "%1"=="-n" goto SET_NOTIFICATION:
if "%1"=="-p" goto SET_SMI_PORT:
if "%1"=="-k" goto SET_CLEAN_DIR:
if "%1"=="-r" goto SET_RELEASE_DIR:
if "%1"=="-s" goto SET_TIMESTAMP:
if "%1"=="-t" goto SET_BUILD_TAG:
if "%1"=="-i" goto SET_INSTANCE:
if "%1"=="-g" goto SET_ARCHIVE:
if "%1"=="" goto PROCESS_INPUT_DONE:

goto PROCESS_INPUT_ERROR:

:SET_TIMESTAMP
shift
set TimeStamp=%1
goto PROCESS_INPUT_LOOP:

:SET_ACORN
shift
set Acorn_Root=%1
goto PROCESS_INPUT_LOOP:

:SET_CLEAN_DIR
shift
set Clean_Flag=%1
goto PROCESS_INPUT_LOOP:

:SET_RELEASE_DIR
shift
set Release_Dir=%1
goto PROCESS_INPUT_LOOP:

:SET_INSTANCE
shift
set Instance=%1
goto PROCESS_INPUT_LOOP:

:SET_BUILD_TAG
shift
set Build_Tag=%1
goto PROCESS_INPUT_LOOP:

:SET_SMI_PORT
shift
set SMI_Port=%1
goto PROCESS_INPUT_LOOP:

:SET_NOTIFICATION
shift
set VarNotify=%1
goto PROCESS_INPUT_LOOP:

:SET_ARCHIVE
shift
set Archive_Flag=%1
goto PROCESS_INPUT_LOOP:

:PROCESS_INPUT_LOOP

shift
goto PROCESS_INPUT:

:PROCESS_INPUT_ERROR

echo ERROR: invalid input parameter
echo usage: deploy_acorn [-options]
echo where options include:
echo.
echo  -s ^<Time Stamp^> (optional)
echo 		Enter the Time Stamp for identifying the deployment
echo		the format is YYYYMMDDHHMM, or one will be generated.		
echo  -a ^<acorn root^>
echo 		Enter the home directory of the instance, the
echo 		default is usually  c:\SMI
echo 		This is the parent directory of Acorn and SMIWeb
echo  -p ^<port number^>
echo 		Enter 4 digit port number for the installation	
echo  		Installation will use port number +2
echo 		e.g. 9300, 9301, 9302
echo  -k ^<clean_flag^>
echo 		Any non-zero value for this flag will clean temp directories
echo  -r ^<Release directory^> 
echo  		Which do you want to get the release from?
echo 		Nightly for nightly builds
echo 		Releases/Platform for version releases
echo  -i ^<Instance you are deploying^>
echo            Instance you are deploying to (e.g sfoqa1, sde, prod)
echo  -t ^<Build tag^> 
echo  		If you are getting files from Releases/Platform,
echo 		enter a Release Number e.g. 2.0.0
echo 		If you are getting files from Nightly, 
echo 		enter a date and time e.g. 200705230130
echo  -g ^<1^> (optional)
echo 		Enter the 1 (one) if you want to bypass backing up the instance.
echo		The value of 0 (zero) or undefined will perform a backup of the instance.
echo  -n ^<email notification list^> comma separated (optional)
echo  		default=jfong@successmetricsinc.com,4157301327@vtext.com
echo 		NOTE - mulitple email addresses need to be separated by 
echo 			commas and inclosed in double quote
echo.
echo Note: the following files needs to be in the Saved_stuff\config_base directory
echo 	chart-service.xml
echo 	customer.properties
echo 	NewUserSpaces.config
echo 	Products.config
echo 	securityfilter-config.xml
echo 	Spaces.config
echo 	Web.config
echo 	web.xml
echo 	

set VarMailText=Invalid arguement(s) provided. 
set VarMailSubject=Acorn_Deploy:Process Arguements
goto BADEXIT

:PROCESS_INPUT_DONE

for /f "delims=:, tokens=1,2" %%A in ("%Acorn_Root%") do ( set DriveLetter=%%A )
set DriveLetter=%DriveLetter: =%

for /f "delims=\, tokens=1-4" %%A in ("%Acorn_Root%") do ( 
	echo %%A-%%B-%%C-%%D 
	set temp=%%B
	)

echo Install_name=%Install_Name%-

set Install_Name=%temp: =%
set IIS_NAME=%Install_Name%
if %Install_Name%==SMI ( set Install_Name=SMIWeb & set IIS_Name=Acorn )
if %Install_Name%==SMI1 ( set Install_Name=SMIWeb & set IIS_Name=AcornSMI1 )
set Install_Name=%Install_Name: =%
set IIS_NAME=%IIS_Name: =%

echo Install_Name=%Install_Name%-
echo IIS_Name=%IIS_Name%-

echo TimeStamp		= %TimeStamp%
echo VarNotify		= %VarNotify%
echo Acorn_Root		= %Acorn_Root%
echo DriveLetter	= %DriveLetter%
echo Release_Dir	= %Release_Dir%
echo Build_Tag		= %Build_Tag%
echo SMI_Port		= %SMI_Port%
echo Clean_Flag		= %Clean_Flag%

if %Acorn_Root%=="" ( goto PROCESS_INPUT_ERROR )
if %Release_Dir%=="" ( goto PROCESS_INPUT_ERROR )
if %Build_Tag%=="" ( goto PROCESS_INPUT_ERROR )
if %SMI_Port%=="" ( goto PROCESS_INPUT_ERROR )
if %Instance%=="" ( goto PROCESS_INPUT_ERROR )

set ACORN_HOME="%Acorn_Root%\Acorn"
echo %ACORN_HOME%

set SMI_HOME="%Acorn_Root%\SMIWeb"
echo %SMI_HOME%

set CHARTSERVER_HOME="%Acorn_Root%\ChartServer"
echo %CHARTSERVER_HOME%

REM
REM We need a timestamp.  If it is not provided, we should create our own.
REM

if %TimeStamp% neq "" ( goto TIMESTAMP_DEFINED: ) 
	
for /f "tokens=1-3 delims=:." %%a in ("%time%") do set hh_mm_stamp=%%a%%b
echo %hh_mm_stamp%
echo %hh_mm_stamp:~0,1%
echo %hh_mm_stamp:~1,3%
if "%hh_mm_stamp:~0,1%" == " " ( set hh_mm_stamp=0%hh_mm_stamp:~1,3%)
echo %hh_mm_stamp%
for /f "tokens=1-3 delims=\ " %%a in ("%date%") do set date_only=%%b
for /f "tokens=1-3 delims=/." %%a in ("%date_only%") do set teststamp=%%c%%a%%b
echo %teststamp%
set TimeStamp=%teststamp%%hh_mm_stamp%
	
:TIMESTAMP_DEFINED
echo TimeStamp: -%TimeStamp%-

set DeleteMeDir=%DriveLetter%:\delete_me\%TimeStamp%
echo DeleteMeDir: -%DeleteMeDir%-

REM
REM Goto the same drive as the Acorn installation
REM

%DriveLetter%:

REM 
REM Setup the delete_me directory
REM 

dir %DeleteMeDir%
set /a Compare_Error_Cnt=%ERRORLEVEL% 
if %Compare_Error_Cnt% EQU 0 ( del /F /Q %DeleteMeDir%\* 
	) else (
	mkdir %DeleteMeDir% )

REM
REM where is winzip?
REM This handles the standard 32 and 64 bit locations
REM

set WinZip_Home=""
if exist "C:\Program Files\WinZip" (set WinZip_Home="c:\Program Files\WinZip")
if exist "C:\Program Files (x86)\WinZip" (set WinZip_Home="C:\Program Files (x86)\WinZip")
if %WinZip_Home% neq "" (goto WinZip_Found:)
echo We can't find the WinZip directory"
set VarMailText=Cannot file WinZip in the default 32 and 64 bit configurations. 
set VarMailSubject=Acorn_Deploy:WinZip directory missing
goto BADEXIT


:WinZip_Found
REM
REM 
REM --------------------------------------------------------------------------------------------
REM Backup the configuration files
REM

cd "%CurrentDir%"
call "%CurrentDir%\archive_configs.bat" -s %TimeStamp% -a %Acorn_Root%
IF %ERRORLEVEL% LSS 1 goto CONFIG_ARCHIVED:
set VarMailText=There were problems archiving the config files. 
set VarMailSubject=Acorn_Deploy:Config archive problems
goto BADEXIT

:CONFIG_ARCHIVED

REM
REM Get the data files
REM

cd %DeleteMeDir%
copy %DriveLetter%:\delete_me\No_ftp\*  %DeleteMeDir%
dir

REM echo open builds >download.ftp
REM echo smibuilds>>download.ftp
REM echo 5771builds!>>download.ftp
REM echo verbose >>download.ftp
REM echo cd   %Release_Dir%/%Build_Tag% >>download.ftp
REM echo prompt >>download.ftp
REM echo get PerformanceEngine.zip >>download.ftp
REM echo get SMIWeb.war >>download.ftp
REM echo get build.number >>download.ftp
REM echo get "Birst Administration.exe" >>download.ftp
REM echo get Antlr3.Runtime.dll >>download.ftp
REM echo get SMIChartServer.war >>download.ftp
REM echo cd ..\%Build_Tag%_Acorn >>download.ftp
REM echo get Acorn.zip >>download.ftp
REM echo get MoveSpace.zip >>download.ftp
REM echo get BirstConnect.zip >>download.ftp
REM	echo get AccountMigrationTool.zip >>download.ftp
REM if %COMPUTERNAME% == "SFOQA1" (echo get I18N.zip >>download.ftp)
REM if %COMPUTERNAME% == "SFOQA2" (echo get I18N.zip >>download.ftp)
REM echo quit >>download.ftp

REM ftp -s:download.ftp   > download.log 2>&1

REM findstr /C:"Connection closed by remote host." download.log
REM IF %ERRORLEVEL% EQU 1 goto CONNECTION_NOT_CLOSED:
REM set VarMailText=FTP connection closed by remote host. 
REM set VarMailSubject=Acorn_Deploy:FTP
REM goto BADEXIT

REM :CONNECTION_NOT_CLOSED
REM findstr /C:"Not connected" download.log
REM IF %ERRORLEVEL% EQU 1 goto NOT_CONNECTION:
REM set VarMailText=FTP not connected anymore.
REM set VarMailSubject=Acorn_Deploy:FTP
REM goto BADEXIT

REM :NOT_CONNECTION
REM findstr /C:"Software caused connection abort" download.log
REM IF %ERRORLEVEL% EQU 1 goto NOT_ABORTED:
REM set VarMailText=Software caused connection abort.
REM set VarMailSubject=Acorn_Deploy:FTP
REM goto BADEXIT

:NOT_ABORTED
REM echo NULL >download.ftp
REM rm download.ftp

if not exist PerformanceEngine.zip (goto FTP_Problems:)
if not exist SMIWeb.war (goto FTP_Problems:)
if not exist Acorn.zip (goto FTP_Problems:)
if not exist "BirstAdmin.zip" (goto FTP_Problems:)
if not exist build.number (goto FTP_Problems:)
if not exist SMIChartServer.war (goto FTP_Problems:)
if not exist BirstConnect.zip (goto FTP_Problems:)
goto Start_Upgrade:

:FTP_Problems
set VarMailText=One or more of the FTP files were not recieved.  Please investigate the problem.  
set VarMailSubject=Acorn_Deploy:FTP issues
goto BADEXIT


:Start_Upgrade

REM
REM --------------------------------------------------------------------------------------------
REM Upgrade the LoadWarehouse
REM 
REM 
REM  Figure out where to get the LOADWAREHOUSE_HOME

set LOADWAREHOUSE_HOME=""
set version_number=""
set spaces_config=%Acorn_Root%\Acorn\spaces.config

if not exist %spaces_config% (set spaces_config=%Acorn_Root%\Acorn\config\spaces.config)
if exist %spaces_config% (goto SPACES_CONFIG_EXISTS:)
set VarMailText=Cannot find spaces.config in standand locations. Please correct problem before proceeding. 
set VarMailSubject=Acorn_Deploy:Spaces config not found.
goto BADEXIT

:SPACES_CONFIG_EXISTS
FOR /F "tokens=1,2,3,4,5,6,7,8,9,10,11 delims=	" %%A IN (%spaces_config%) DO if "%%A" == "0" (set ltemp1=%%J) else (ECHO %%A"-" %%B"-" %%C"-" %%D"-" %%E"-" %%F"-" %%G"-" %%H"-" %%I"-" %%K"-" %%K)
echo %ltemp1%

set LOADWAREHOUSE_HOME=%ltemp1:\bin\load_warehouse.bat=%\..

FOR /F "tokens=1,2,3,4 delims=." %%A IN (%DeleteMeDir%\build.number) DO if "%%A" == "release" (set vtemp1=%%B.%%C.%%D) else (ECHO %%A"-" %%B"-" %%C"-" %%D"-")
echo %vtemp1%
echo "Next"
FOR /F "tokens=1,2,3 delims= " %%A IN ("%vtemp1%") DO (set vtemp2=%%A)
echo %vtemp2%

FOR /F "tokens=1,2,3,4 delims==" %%A IN ("%vtemp2%") DO (set version_number=%%B)
echo %version_number%

IF not (%version_number% == "") ( goto LOADWAREHOUSE_START: )
set VarMailText=LoadWarehouse was not able to get a version number.  Please correct problem before proceeding. 
set VarMailSubject=Acorn_Deploy:LoadWarehouse version number.
goto BADEXIT

:LOADWAREHOUSE_START

REM
REM Note: using pushd instead of cd since CMD does not recognize UNC path, but pushd does.
REM 
pushd %LOADWAREHOUSE_HOME%
IF %ERRORLEVEL% LSS 1 goto PUSHD1:
set VarMailText=Pushd to %LOADWAREHOUSE_HOME% generated an error, does directory exist? Please correct problem before proceeding. 
set VarMailSubject=Acorn_Deploy:Pushd 1
goto BADEXIT

:PUSHD1
if exist %version_number% goto LOADWAREHOUSE_ZIP:
mkdir %version_number%
IF %ERRORLEVEL% LSS 1 goto LOADWAREHOUSE_ZIP:
set VarMailText=Create version number directory failed, Please correct problem before proceeding. 
set VarMailSubject=Acorn_Deploy:create dir
goto BADEXIT

:LOADWAREHOUSE_ZIP
pushd %LOADWAREHOUSE_HOME%
IF %ERRORLEVEL% LSS 1 goto PUSHD2:
set VarMailText=Pushd to %LOADWAREHOUSE_HOME% generated an error, does directory exist? Please correct problem before proceeding. 
set VarMailSubject=Acorn_Deploy:Pushd 2
goto BADEXIT

:PUSHD2

REM Skip archive if archive flag is true
IF %Archive_Flag% NEQ 0 goto BYPASS_BACKUP_LOADWAREHOUSE:

%WinZip_Home%\WZZIP.EXE -ex -r -P -a %Acorn_Root%\Saved_stuff\%TimeStamp%_LoadWarehouse.zip %LOADWAREHOUSE_HOME%
IF %ERRORLEVEL% LSS 1 goto BYPASS_BACKUP_LOADWAREHOUSE:
set VarMailText=Backup of the LoadWarehouse environment failed, Please correct problem before proceeding. 
set VarMailSubject=Acorn_Deploy:Loadwarehouse Backup
goto BADEXIT

:BYPASS_BACKUP_LOADWAREHOUSE

REM if not exist %version_number%_old goto NO_LOADWAREHOUSE_OLD:
REM 
REM rmdir /S /Q %version_number%_old
REM if not exist %version_number%_old goto NO_LOADWAREHOUSE_OLD:
REM set VarMailText=Problems with removing the LoadWarehouseService_old directory, Please correct problem before proceeding. 
REM set VarMailSubject=Acorn_Deploy:LOADWAREHOUSE remove directory
REM goto BADEXIT
REM 
REM :NO_LOADWAREHOUSE_OLD
REM 
REM xcopy /V /E /I /H  %version_number% %version_number%_old
REM IF %ERRORLEVEL% LSS 1 goto CREATE_LOADWAREHOUSE_DIR:
REM set VarMailText=Fail to xcopy %version_number% to %version_number%_old, Please correct problem before proceeding. 
REM set VarMailSubject=Acorn_Deploy:LOADWAREHOUSE copy
REM goto BADEXIT
REM 
REM :CREATE_LOADWAREHOUSE_DIR

del /F /S /Q %version_number%
IF %ERRORLEVEL% LSS 1 goto RELOAD_LOADWAREHOUSE:
set VarMailText=Fail to delete the  LoadWarehouseService directory, Please correct problem before proceeding. 
set VarMailSubject=Acorn_Deploy:LOADWAREHOUSE Backup
goto BADEXIT

:RELOAD_LOADWAREHOUSE

pushd %LOADWAREHOUSE_HOME%
IF %ERRORLEVEL% LSS 1 goto PUSHD3:
set VarMailText=Pushd to %LOADWAREHOUSE_HOME% generated an error, does directory exist? Please correct problem before proceeding. 
set VarMailSubject=Acorn_Deploy:Pushd 3
goto BADEXIT

:PUSHD3

%WinZip_Home%\WZUNZIP.EXE -d -ybc -o -yo "%DeleteMeDir%\PerformanceEngine.zip" "%version_number%"
IF %ERRORLEVEL% LSS 1 goto LOADWAREHOUSE_UNZIP_DONE:
set VarMailText=ACORN had problems Unzipping Perfengine, Please correct problem before proceeding. 
set VarMailSubject=Acorn_Deploy:LOADWAREHOUSE unzip
goto BADEXIT

:LOADWAREHOUSE_UNZIP_DONE

REM 
REM copy jdk to new directory
REM
xcopy /V /E /I /H  %SMI_HOME%\jdk %version_number%\jdk 
IF %ERRORLEVEL% LSS 1 goto LOADWAREHOUSE_JDK_COPIED:
set VarMailText=ACORN had problems copying the jdk directory, Please correct problem before proceeding. 
set VarMailSubject=Acorn_Deploy:LOADWAREHOUSE copy jdk
goto BADEXIT

:LOADWAREHOUSE_JDK_COPIED

copy "%DeleteMeDir%\build.number" %version_number%
IF %ERRORLEVEL% LSS 1 goto LOADWAREHOUSE_SETUP_DONE:
set VarMailText=SMI had problems copying the build.number file, Please correct problem before proceeding. 
set VarMailSubject=Acorn_Deploy:Performance Engine build.number
goto BADEXIT

:LOADWAREHOUSE_SETUP_DONE

REM
REM Clean up the directory stack.
REM
popd
popd
popd

REM 
REM --------------------------------------------------------------------------------------------
REM upgrade SMI
REM

REM Skip archive if archive flag is true
IF %Archive_Flag% NEQ 0 goto BYPASS_BACKUP_SMI:

cd %SMI_HOME%
%WinZip_Home%\WZZIP.EXE -ex -r -P -a -x.\logs\*.* -x.\tomcat\logs\*.* %Acorn_Root%\Saved_stuff\%TimeStamp%_SMI_platform.zip .
IF %ERRORLEVEL% LSS 1 goto BYPASS_BACKUP_SMI:
set VarMailText=Backup of the SMI environment failed, Please correct problem before proceeding. 
set VarMailSubject=Acorn_Deploy:SMI Backup
goto BADEXIT

:BYPASS_BACKUP_SMI

echo "shutdown the server"
net stop %Install_Name%%SMI_Port%
if %ERRORLEVEL% == 2 (
	goto SHUTDOWN_DONE: 
	) else (
	if %ERRORLEVEL% == 0 ( goto SHUTDOWN_DONE: ))
set VarMailText=Shutdown of the SMIWEB server did not complete normally. 
set VarMailSubject=Acorn_Deploy:Shutdown did not complete
goto BADEXIT

:SHUTDOWN_DONE
REM sleep for 30 seconds
ping -n 30 127.0.0.1 > NUL 2>&1
 

if %Clean_Flag% equ 0 ( goto Update_SMIWeb: )
cd %SMI_HOME%\tomcat\temp
del /Q /S *

:Update_SMIWeb
cd %SMI_HOME%\tomcat\webapps
rmdir /S /Q %SMI_HOME%\tomcat\webapps\SMIWeb
IF %ERRORLEVEL% LSS 1 goto SMI_RMDIR_DONE:
set VarMailText=SMI had problems removing SMIWeb, Please correct problem before proceeding. 
set VarMailSubject=Acorn_Deploy:SMI rmdir
goto BADEXIT

:SMI_RMDIR_DONE
del %SMI_HOME%\tomcat\webapps\SMIWeb.war
IF %ERRORLEVEL% LSS 1 goto SMI_DEL_DONE:
set VarMailText=SMI had problems deleting SMIWeb.war, Please correct problem before proceeding. 
set VarMailSubject=Acorn_Deploy:SMI Del
goto BADEXIT

:SMI_DEL_DONE
dir

copy %DeleteMeDir%\SMIWeb.war %SMI_HOME%\tomcat\webapps
IF %ERRORLEVEL% LSS 1 goto SMI_COPY_DONE:
set VarMailText=SMI had problems copying new SMIWeb.war, Please correct problem before proceeding. 
set VarMailSubject=Acorn_Deploy:SMI Copy
goto BADEXIT

:SMI_COPY_DONE
cd %SMI_HOME%

%WinZip_Home%\WZUNZIP.EXE -d -ybc -o -yo "%DeleteMeDir%\PerformanceEngine.zip" "%SMI_HOME%"
IF %ERRORLEVEL% LSS 1 goto SMI_UNZIP_DONE:
set VarMailText=SMI had problems Unzipping Perfengine, Please correct problem before proceeding. 
set VarMailSubject=Acorn_Deploy:SMI unzip
goto BADEXIT

:SMI_UNZIP_DONE
%WinZip_Home%\WZUNZIP.EXE -e -ybc -o -yo @"%Acorn_Root%\SetupTools\BirstAdminExtract.txt" "%DeleteMeDir%\BirstAdmin.zip" "%SMI_HOME%\bin"
IF %ERRORLEVEL% LSS 1 goto UNZIP_BIRSTADMIN_DONE:
set VarMailText=SMI had problems Unzipping Perfengine, Please correct problem before proceeding. 
set VarMailSubject=Acorn_Deploy:SMI copy Birst Administration.exe
goto BADEXIT

:UNZIP_BIRSTADMIN_DONE
copy "%DeleteMeDir%\build.number" "%SMI_HOME%\bin"
IF %ERRORLEVEL% LSS 1 goto BUILDNUMBER_COPIED:
set VarMailText=SMI had problems Unzipping Perfengine, Please correct problem before proceeding. 
set VarMailSubject=Acorn_Deploy:SMI build.number
goto BADEXIT

:BUILDNUMBER_COPIED
echo "startup the server"
net start %Install_Name%%SMI_Port%
if %ERRORLEVEL% == 0  ( goto STARTUP_DONE:)
set VarMailText=Startup of the SMIWEB server did not startup normally. 
set VarMailSubject=Acorn_Deploy:SMI startup did not complete
goto BADEXIT

:STARTUP_DONE
REM sleep for 30 seconds
 ping -n 30 127.0.0.1 > NUL 2>&1
 
REM
REM Save the most recent configuration files.
REM

copy /V /Y %SMI_HOME%\conf\customer.properties %Acorn_Root%\SetupTools\most_current\
copy /V /Y %SMI_HOME%\tomcat\webapps\SMIWeb\WEB-INF\web.xml %Acorn_Root%\SetupTools\most_current\
copy /V /Y %SMI_HOME%\tomcat\webapps\SMIWeb\WEB-INF\securityfilter-config.xml %Acorn_Root%\SetupTools\most_current\
copy /V /Y %SMI_HOME%\tomcat\webapps\SMIWeb\WEB-INF\classes\log4j.xml %Acorn_Root%\SetupTools\most_current\

REM 
REM --------------------------------------------------------------------------------------------
REM Upgrade Acorn
REM


REM
REM Backup the Acorn directory tree
REM

REM Skip archive if archive flag is true
IF %Archive_Flag% NEQ 0 goto BYPASS_BACKUP_ACORN:

cd %ACORN_HOME%
%WinZip_Home%\WZZIP.EXE -ex -r -P -a %Acorn_Root%\Saved_stuff\%TimeStamp%_ACORN_platform.zip .
IF %ERRORLEVEL% LSS 1 goto BYPASS_BACKUP_ACORN:
set VarMailText=Backup of the ACORN environment failed, Please correct problem before proceeding. 
set VarMailSubject=Acorn_Deploy:ACORN Backup
goto BADEXIT

:BYPASS_BACKUP_ACORN
REM
REM First move the old Acorn Directory to Acorn_old, the create a new Acorn Directory to 
REM unzip the files.  
REM
cd %Acorn_Root%

systeminfo /FO LIST | find "Microsoft Windows XP Professional"
IF %ERRORLEVEL% neq 0 ( goto OS_Not_XP1: )
Iisreset /stop
IF %ERRORLEVEL% LSS 1 ( goto ACORN_STOPPED: )
set VarMailText=IIS was not able to restart correctly.  Please correct problem before proceeding. 
set VarMailSubject=Acorn_Deploy:ACORN not restarted
goto BADEXIT

:OS_Not_XP1
systeminfo /FO LIST | find "Microsoft(R) Windows(R) Server 2003 Standard x64 Edition"
IF %ERRORLEVEL% neq 0 (goto OS_Unknown: )
cscript c:\WINDOWS\system32\iisweb.vbs /stop %IIS_Name%
IF %ERRORLEVEL% LSS 1 ( goto ACORN_STOPPED: )
set VarMailText=IIS was not able to restart correctly.  Please correct problem before proceeding. 
set VarMailSubject=Acorn_Deploy:ACORN not restarted
goto BADEXIT

:ACORN_STOPPED

if not exist Acorn_old goto NO_ACORN_OLD:

rmdir /S /Q Acorn_old
if not exist Acorn_old goto NO_ACORN_OLD:

set VarMailText=Problems with removing the ACORN_old directory, Please correct problem before proceeding. 
set VarMailSubject=Acorn_Deploy:ACORN Backup
goto BADEXIT

:NO_ACORN_OLD

xcopy /V /E /I /H  Acorn Acorn_old
IF %ERRORLEVEL% LSS 1 goto CREATE_ACORN_DIR:
set VarMailText=Fail to xcopy Acorn to Acorn_old, Please correct problem before proceeding. 
set VarMailSubject=Acorn_Deploy:ACORN Backup
goto BADEXIT

:CREATE_ACORN_DIR

del /F /S /Q Acorn
IF %ERRORLEVEL% LSS 1 goto RELOAD_ACORN:
set VarMailText=Fail to delete the  Acorn directory, Please correct problem before proceeding. 
set VarMailSubject=Acorn_Deploy:ACORN Backup
goto BADEXIT

:RELOAD_ACORN

cd %ACORN_HOME%

%WinZip_Home%\WZUNZIP.EXE -d -ybc -o -yo "%DeleteMeDir%\Acorn.zip" "%ACORN_HOME%"
IF %ERRORLEVEL% LSS 1 goto ACORN_UNZIP_DONE:
set VarMailText=ACORN had problems Unzipping Perfengine, Please correct problem before proceeding. 
set VarMailSubject=Acorn_Deploy:ACORN unzip
goto BADEXIT

:ACORN_UNZIP_DONE

REM
REM move the map files from ACORN_HOME\maps to ACORN_HOME\swf\maps
REM

if exist %ACORN_HOME%\swf\maps goto START_MAP_COPY:
mkdir %ACORN_HOME%\swf\maps
IF %ERRORLEVEL% LSS 1 goto START_MAP_COPY:
set VarMailText=Create %ACORN_HOME%\swf\maps directory failed, Please correct problem before proceeding. 
set VarMailSubject=Acorn_Deploy:create map dir
goto BADEXIT

:START_MAP_COPY
xcopy /s /e /y %ACORN_HOME%\maps %ACORN_HOME%\swf\maps
IF %ERRORLEVEL% LSS 1 goto COPIED_MAPS:
set VarMailText=ACORN had problems copying the \bin\lib to \lib for the Data Conductor. 
set VarMailSubject=Acorn_Deploy: copy lib problem
goto BADEXIT

:COPIED_MAPS
REM
REM move the launch.jnlp files from ACORN_HOME\bin\lib to ACORN_HOME\lib
REM

REM copy /V /Y %ACORN_HOME%\bin\launch.jnlp %ACORN_HOME%\launch.jnlp
REM IF %ERRORLEVEL% LSS 1 goto COPIED_JNLP:
REM set VarMailText=ACORN had problems copying the \bin\launch.jnlp to Acorn_home for the Data Conductor. 
REM set VarMailSubject=Acorn_Deploy: copy jnlp problem
REM goto BADEXIT

:COPIED_JNLP
REM
REM	Copy the BirstConnect.zip file to the downloads directory
REM

cd %ACORN_HOME%

if exist %ACORN_HOME%\downloads goto DOWNLOADS_CREATED:
mkdir %ACORN_HOME%\downloads 
IF %ERRORLEVEL% LSS 1 goto DOWNLOADS_CREATED:
set VarMailText=Had problems creating the downloads directory, Please correct problem before proceeding. 
set VarMailSubject=Acorn_Deploy: download create
goto BADEXIT

:DOWNLOADS_CREATED
cd %ACORN_HOME%\downloads
copy %DeleteMeDir%\BirstConnect.zip %ACORN_HOME%\downloads
IF %ERRORLEVEL% LSS 1 goto BIRSTCONNECT_COPY_DONE:
set VarMailText=SMI had problems copying new BirstConnect.zip, Please correct problem before proceeding. 
set VarMailSubject=Acorn_Deploy:BirstConnect
goto BADEXIT

:BIRSTCONNECT_COPY_DONE
REM
REM Save the most recent configuration files.
REM

copy /V /Y %ACORN_HOME%\web.config %Acorn_Root%\SetupTools\most_current\
copy /V /Y %ACORN_HOME%\Spaces.config %Acorn_Root%\SetupTools\most_current\
copy /V /Y %ACORN_HOME%\NewUserSpaces.config %Acorn_Root%\SetupTools\most_current\
copy /V /Y %ACORN_HOME%\Products.config %Acorn_Root%\SetupTools\most_current\

systeminfo /FO LIST | find "Microsoft Windows XP Professional"
IF %ERRORLEVEL% neq 0 ( goto OS_Not_XP: )
Iisreset /start
IF %ERRORLEVEL% LSS 1 ( goto ACORN_RESTARTED: )
set VarMailText=IIS was not able to restart correctly.  Please correct problem before proceeding. 
set VarMailSubject=Acorn_Deploy:ACORN not restarted
goto BADEXIT

:OS_Not_XP
systeminfo /FO LIST | find "Microsoft(R) Windows(R) Server 2003 Standard x64 Edition"
IF %ERRORLEVEL% neq 0 (goto OS_Unknown: )

cscript c:\WINDOWS\system32\iisweb.vbs /start %IIS_Name%
IF %ERRORLEVEL% LSS 1 ( goto ACORN_RESTARTED: )
set VarMailText=IIS was not able to restart correctly.  Please correct problem before proceeding. 
set VarMailSubject=Acorn_Deploy:ACORN not restarted
goto BADEXIT

:OS_Unknown
set VarMailText=Couldn't recognize  which OS we were running on.  Did not know how to restart IIS. 
set VarMailSubject=Acorn_Deploy:Could not restart the webserver.
goto BADEXIT

:ACORN_RESTARTED

REM
REM --------------------------------------------------------------------------------------------
REM Upgrade the Chart Service
REM 

REM Skip archive if archive flag is true
IF %Archive_Flag% NEQ 0 goto BYPASS_BACKUP_CHARTSERVER:

cd %CHARTSERVER_HOME%
%WinZip_Home%\WZZIP.EXE -ex -r -P -a -x.\logs\*.* -x.\tomcat\logs\*.* %Acorn_Root%\Saved_stuff\%TimeStamp%_CHARTSERVER.zip .
IF %ERRORLEVEL% LSS 1 goto BYPASS_BACKUP_CHARTSERVER:
set VarMailText=Backup of the CHARTSERVER environment failed, Please correct problem before proceeding. 
set VarMailSubject=Acorn_Deploy:CHARTSERVER Backup
goto BADEXIT

:BYPASS_BACKUP_CHARTSERVER

echo "shutdown the server"
net stop ChartServer%SMI_Port%
if %ERRORLEVEL% == 2 (
	goto SHUTDOWN_DONE: 
	) else (
	if %ERRORLEVEL% == 0 ( goto SHUTDOWN_DONE: ))
set VarMailText=Shutdown of the ChartServer%SMI_Port% server did not complete normally. 
set VarMailSubject=Acorn_Deploy:Shutdown did not complete
goto BADEXIT

:SHUTDOWN_DONE
REM sleep for 30 seconds
ping -n 30 127.0.0.1 > NUL 2>&1
 

if %Clean_Flag% equ 0 ( goto Update_SMIChartServer: )
cd %CHARTSERVER_HOME%\tomcat\temp
del /Q /S *

:Update_SMIChartServer
cd %CHARTSERVER_HOME%\tomcat\webapps
rmdir /S /Q %CHARTSERVER_HOME%\tomcat\webapps\SMIChartServer
IF %ERRORLEVEL% LSS 1 goto CHARTSERVER_RMDIR_DONE:
set VarMailText=CHARTSERVER had problems removing SMIChartServer, Please correct problem before proceeding. 
set VarMailSubject=Acorn_Deploy:CHARTSERVER rmdir
goto BADEXIT

:CHARTSERVER_RMDIR_DONE
del %CHARTSERVER_HOME%\tomcat\webapps\SMIChartServer.war
IF %ERRORLEVEL% LSS 1 goto CHARTSERVER_DEL_DONE:
set VarMailText=CHARTSERVER had problems deleting SMIChartServer.war, Please correct problem before proceeding. 
set VarMailSubject=Acorn_Deploy:SMIChartServer Del
goto BADEXIT

:CHARTSERVER_DEL_DONE
dir

copy %DeleteMeDir%\SMIChartServer.war %CHARTSERVER_HOME%\tomcat\webapps
IF %ERRORLEVEL% LSS 1 goto CHARTSERVER_COPY_DONE:
set VarMailText=CHARTSERVER had problems copying new SMIChartServer.war, Please correct problem before proceeding. 
set VarMailSubject=Acorn_Deploy:SMIChartServer Copy
goto BADEXIT

:CHARTSERVER_COPY_DONE

echo "startup the server"
net start ChartServer%SMI_Port%
if %ERRORLEVEL% == 0  ( goto STARTUP_DONE:)
REM sleep for 30 seconds
ping -n 30 127.0.0.1 > NUL 2>&1
for /f "tokens=2-4" %%a in ('sc query ChartServer%SMI_Port% ^| findstr RUNNING') do if %%c==RUNNING ( goto STARTUP_DONE:)
set VarMailText=Startup of the SMIChartServer server did not startup normally. 
set VarMailSubject=Acorn_Deploy:CHARTSERVER startup did not complete
goto BADEXIT

:STARTUP_DONE
REM sleep for 30 seconds
 ping -n 30 127.0.0.1 > NUL 2>&1
 
REM
REM Save the most recent configuration files.
REM

REM	copy /V /Y %CHARTSERVER_HOME%\conf\customer.properties %Acorn_Root%\SetupTools\most_current\
REM	copy /V /Y %CHARTSERVER_HOME%\tomcat\webapps\SMIChartServer\WEB-INF\web.xml %Acorn_Root%\SetupTools\most_current\
REM	copy /V /Y %CHARTSERVER_HOME%\tomcat\webapps\SMIChartServer\WEB-INF\securityfilter-config.xml %Acorn_Root%\SetupTools\most_current\
copy /V /Y %CHARTSERVER_HOME%\tomcat\webapps\SMIChartServer\WEB-INF\classes\log4j.xml %Acorn_Root%\SetupTools\most_current\chartserver_log4j.xml

REM
REM ----------------------------------------------------------------------------------------------
REM
REM update MoveSpace to insure we have the most current movespace for this instance
REM

:UPDATE_MOVESPACE

cd %Acorn_Root%
dir "%Acorn_Root%\Movespace"
if %ERRORLEVEL% EQU 0 goto CLEAN_MOVESPACE: else CREATE_MOVESPACE:

:CLEAN_MOVESPACE
del /F /Q %Acorn_Root%\Movespace\* 
set /a file_cnt=0
dir %Acorn_Root%\Movespace | find /C "." >%Acorn_Root%\file_cnt.tmp
set /P file_cnt=<%Acorn_Root%\file_cnt.tmp
IF %file_cnt% LEQ 3 goto MOVESPACE_CREATED:
set VarMailText=Had problems clearing the Movespace directory, Please correct problem before proceeding. 
set VarMailSubject=Acorn_Deploy:Movespace unzip
goto BADEXIT

:CREATE_MOVESPACE
mkdir %Acorn_Root%\Movespace 
IF %ERRORLEVEL% LSS 1 goto MOVESPACE_CREATED:
set VarMailText=Had problems creating the Movespace directory, Please correct problem before proceeding. 
set VarMailSubject=Acorn_Deploy:Movespace unzip
goto BADEXIT

:MOVESPACE_CREATED
%WinZip_Home%\WZUNZIP.EXE -d -ybc -o -yo "%DeleteMeDir%\MoveSpace.zip" "%Acorn_Root%\Movespace"
IF %ERRORLEVEL% LSS 1 goto MOVESPACE_UNZIP_DONE:
set VarMailText=ACORN had problems Unzipping Movespace, Please correct problem before proceeding. 
set VarMailSubject=Acorn_Deploy:Movespace unzip
goto BADEXIT

:MOVESPACE_UNZIP_DONE

REM
REM -----------------------------------------------------------------------------------------------
REM
REM update AccountMigrationTool to insure we have the most current AccountMigrationTool for this instance
REM
REM
REM cd %ACORN_ROOT%
REM dir "%ACORN_ROOT%\SetupTools\AccountMigrationTool.exe"
REM if %ERRORLEVEL% EQU 0 ( goto CLEAN_ACCOUNTMIGRATIONTOOL: ) else ( goto ACCOUNTMIGRATIONTOOL_UNZIP: )
REM 
REM :CLEAN_ACCOUNTMIGRATIONTOOL
REM del /F /Q %ACORN_ROOT%\SetupTools\AccountMigrationTool.exe
REM del /F /Q %ACORN_ROOT%\SetupTools\log4net.dll
REM set /a file_cnt=0
REM dir %ACORN_ROOT%\SetupTools\AccountMigrationTool.exe | find /C "." >%ACORN_ROOT%\file_cnt.tmp
REM set /P file_cnt=<%ACORN_ROOT%\file_cnt.tmp
REM IF %file_cnt% GTR 3 goto ACCOUNTMIGRATIONTOOL_CLEAN_ERR:
REM set /a file_cnt=0
REM dir %ACORN_ROOT%\SetupTools\log4net.dll | find /C "." >%ACORN_ROOT%\file_cnt.tmp
REM set /P file_cnt=<%ACORN_ROOT%\file_cnt.tmp
REM IF %file_cnt% LEQ 3 goto ACCOUNTMIGRATIONTOOL_UNZIP:
REM :ACCOUNTMIGRATIONTOOL_CLEAN_ERR
REM set VarMailText=Had problems clearing the AccountMigrationTool file, Please correct problem before proceeding. 
REM set VarMailSubject=Acorn_Deploy:AccountMigrationTool cleanup
REM goto BADEXIT
REM
REM :ACCOUNTMIGRATIONTOOL_UNZIP
REM %WinZip_Home%\WZUNZIP.EXE -d -ybc -o -yo "%DeleteMeDir%\AccountMigrationTool.zip" "%ACORN_ROOT%\SetupTools"
REM IF %ERRORLEVEL% LSS 1 goto ACCOUNTMIGRATIONTOOL_UNZIP_DONE:
REM set VarMailText=ACORN had problems Unzipping AccountMigrationTool, Please correct problem before proceeding. 
REM set VarMailSubject=Acorn_Deploy:AccountMigrationTool unzip
REM goto BADEXIT
REM
REM :ACCOUNTMIGRATIONTOOL_UNZIP_DONE
REM 
REM
REM On SFOQA1 only, unzip the Spanish files
REM

if %COMPUTERNAME% == "SFOQA1" ( %WinZip_Home%\WZUNZIP.EXE -d -ybc -o -yo "%DeleteMeDir%\I18N.zip" "%ACORN_ROOT%\Acorn\swf" )
if %COMPUTERNAME% == "SFOQA2" ( %WinZip_Home%\WZUNZIP.EXE -d -ybc -o -yo "%DeleteMeDir%\I18N.zip" "%ACORN_ROOT%\Acorn\swf" )

REM
REM ----------------------------------------------------------------------------------------------
REM
REM Stop the servers so we can copy the configuration files to their proper place
REM

REM  Chart service was not restarted to I don't need to restart it.  
net stop ChartServer%SMI_Port%
if %ERRORLEVEL% == 2 (
	goto CHART_SHUTDOWN: 
	) else (
	if %ERRORLEVEL% == 0 ( goto CHART_SHUTDOWN: ))
set VarMailText=Shutdown of the ChartServer%SMI_Port% server did not complete normally. 
set VarMailSubject=Acorn_Deploy:Shutdown did not complete
goto BADEXIT

:CHART_SHUTDOWN

net stop %Install_Name%%SMI_Port%
if %ERRORLEVEL% == 2 (
	goto RESHUTDOWN_DONE: 
	) else (
	if %ERRORLEVEL% == 0 ( goto RESHUTDOWN_DONE: ))
set VarMailText=Restart/shutdown of the SMIWEB server did not complete normally. 
set VarMailSubject=Acorn_Deploy:SMIWeb restart shutdown did not complete
goto BADEXIT

:RESHUTDOWN_DONE

systeminfo /FO LIST | find "Microsoft Windows XP Professional"
IF %ERRORLEVEL% neq 0 ( goto OS_Not_XP2: )
Iisreset /stop /noforce
IF %ERRORLEVEL% LSS 1 ( goto IIS_STOP_DONE: )
set VarMailText=IIS was not able to restart correctly.  Please correct problem before proceeding. 
set VarMailSubject=Acorn_Deploy:ACORN not restarted 2
goto BADEXIT

:OS_Not_XP2
systeminfo /FO LIST | find "Microsoft(R) Windows(R) Server 2003 Standard x64 Edition"
IF %ERRORLEVEL% neq 0 (goto OS_Unknown2: )
cscript c:\WINDOWS\system32\iisweb.vbs /stop %IIS_Name%

IF %ERRORLEVEL% LSS 1 ( goto IIS_STOP_DONE: )
set VarMailText=IIS was not able to restart correctly.  Please correct problem before proceeding. 
set VarMailSubject=Acorn_Deploy:ACORN not restarted 2
goto BADEXIT

:OS_Unknown2
set VarMailText=Couldn't recognize  which OS we were running on.  Did not know how to restart IIS. 
set VarMailSubject=Acorn_Deploy:Could not restart the webserver 2.
goto BADEXIT

:IIS_STOP_DONE

REM sleep for 30 seconds
ping -n 30 127.0.0.1 > NUL 2>&1
 
REM
REM -----------------------------------------------------------------------------------------------
REM
REM Copy the custom configurations files into place
REM
REM Restore the old config/xml files.
REM

set /a Compare_Error_Cnt=0

cd %Acorn_Root%\Saved_stuff\%TimeStamp%
set /a Compare_Error_Cnt=%Compare_Error_Cnt%+%ERRORLEVEL%

cd %ACORN_HOME%\config
set /a Compare_Error_Cnt=%Compare_Error_Cnt%+%ERRORLEVEL%

copy /v /y %Instance%\*.config .
set /a Compare_Error_Cnt=%Compare_Error_Cnt%+%ERRORLEVEL%

REM Put the proper Customer.properties for SMIWeb in place for specific server.
copy /v /y  %Instance%\customer.properties %SMI_HOME%\Conf\Customer.properties
set /a Compare_Error_Cnt=%Compare_Error_Cnt%+%ERRORLEVEL%

REM Put the proper log4j file for Audit logging in place for the specific server
copy /v /y  %Instance%\log4j.xml %SMI_HOME%\tomcat\webapps\SMIWeb\WEB-INF\classes\log4j.xml
set /a Compare_Error_Cnt=%Compare_Error_Cnt%+%ERRORLEVEL%

REM Put the proper Chart Service configuration in place for the specific server
REM It the configuration exists in the config file, use that, if not used our saved copy.

REM
REM -----------------------------------------------------------------------------------------------
REM
REM Since we copied over files, restart the servers.
REM

net start ChartServer%SMI_Port%
if %ERRORLEVEL% == 0  ( goto CHART_RESTART:)
REM sleep for 30 seconds
ping -n 30 127.0.0.1 > NUL 2>&1
for /f "tokens=2-4" %%a in ('sc query ChartServer%SMI_Port% ^| findstr RUNNING') do if %%c==RUNNING ( goto CHART_RESTART:)
set VarMailText=Startup of the SMIChartServer server did not startup normally. 
set VarMailSubject=Acorn_Deploy:CHARTSERVER startup did not complete
goto BADEXIT

:CHART_RESTART

net start %Install_Name%%SMI_Port%
if %ERRORLEVEL% == 0  ( goto RESTARTUP_DONE:)
set VarMailText=Restart/startup of the SMIWEB server did not startup normally. 
set VarMailSubject=Acorn_Deploy:SMIWeb restart startup did not complete
goto BADEXIT

:RESTARTUP_DONE
REM sleep for 30 seconds
ping -n 30 127.0.0.1 > NUL 2>&1
 
systeminfo /FO LIST | find "Microsoft Windows XP Professional"
IF %ERRORLEVEL% neq 0 ( goto OS_Not_XP22: )
Iisreset /start /noforce
IF %ERRORLEVEL% LSS 1 ( goto CONFIGS_DONE: )
set VarMailText=IIS was not able to restart correctly.  Please correct problem before proceeding. 
set VarMailSubject=Acorn_Deploy:ACORN not restarted 2
goto BADEXIT

:OS_Not_XP22
systeminfo /FO LIST | find "Microsoft(R) Windows(R) Server 2003 Standard x64 Edition"
IF %ERRORLEVEL% neq 0 (goto OS_Unknown2: )
cscript c:\WINDOWS\system32\iisweb.vbs /start %IIS_Name%

IF %ERRORLEVEL% LSS 1 ( goto CONFIGS_DONE: )
set VarMailText=IIS was not able to restart correctly.  Please correct problem before proceeding. 
set VarMailSubject=Acorn_Deploy:ACORN not restarted 2
goto BADEXIT

:OS_Unknown2
set VarMailText=Couldn't recognize  which OS we were running on.  Did not know how to restart IIS. 
set VarMailSubject=Acorn_Deploy:Could not restart the webserver 2.
goto BADEXIT

:CONFIGS_DONE

IF %Compare_Error_Cnt%  EQU 0 ( set VarMailSubject="Acorn_Deploy:Done" & set VarMailText="No changes to the configuration files." 
	) else (
	set VarMailSubject="Acorn_Deploy:Done, but manual intervention needed" & set VarMailText="Manual review of deployment necessary since we had problems copying config files." ) 

blat -to %VarNotify% -subject %VarMailSubject% -body %VarMailText%

goto THE_END:


:BADEXIT
echo ------------------ Bad Exit -------------------------------------------
echo %VarMailText%
blat -to %VarNotify% -subject "%VarMailSubject%" -body "%VarMailText%"
rem *** if exist %VarCustProp% del %VarCustProp%
echo "Acorn Deployment failed."

REM
REM Clean up the directory stack.
REM
popd
popd
popd

exit /B 1

:THE_END

exit /B 0


