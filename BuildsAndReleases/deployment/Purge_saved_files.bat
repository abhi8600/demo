rem
rem purge older files so we don't run out of space.
rem
rem FORFILES /S /D -10 /C "cmd /c IF @isdir == TRUE rd /S /Q @path"

echo on
date /T
time /T
echo %1 %2 %3 %4
set TimeStamp=""
set Acorn_Root=""
set DriverLetter=""

set VarNotify="jfong@birst.com,pmalshe@birst.biz"
set VarMailText=Setup email message
set VarMailSubject=Deploy_MemDB

:PROCESS_INPUT

if "%1"=="-a" goto SET_ACORN:
if "%1"=="-n" goto SET_NOTIFICATION:
if "%1"=="" goto PROCESS_INPUT_DONE:

goto PROCESS_INPUT_ERROR:

:SET_ACORN
shift
set Acorn_Root=%1
goto PROCESS_INPUT_LOOP:

:SET_NOTIFICATION
shift
set VarNotify=%1
goto PROCESS_INPUT_LOOP:

:PROCESS_INPUT_LOOP

shift
goto PROCESS_INPUT:

:PROCESS_INPUT_ERROR

echo ERROR: invalid input parameter
echo usage: deploy_memdb [-options]
echo where options include:
echo.
echo  -a ^<acorn root^>
echo 		Enter the home directory of the instance, the
echo 		default is usually  c:\SMI
echo 		This is the parent directory of Acorn and SMIWeb
echo  -n ^<email notification list^> comma separated (optional)
echo  		default=jfong@successmetricsinc.com,4157301327@vtext.com
echo 		NOTE - mulitple email addresses need to be separated by 
echo 			commas and inclosed in double quote
echo.

set VarMailText=Invalid arguement(s) provided. 
set VarMailSubject=Acorn_Deploy:Process Arguements
goto BADEXIT

:PROCESS_INPUT_DONE

if %Acorn_Root%=="" ( goto PROCESS_INPUT_ERROR )

for /f "delims=:, tokens=1,2" %%A in ("%Acorn_Root%") do ( set DriveLetter=%%A )
set DriveLetter=%DriveLetter: =%


if exist %DriveLetter%:\delete_me\FilesToDelete*.txt (del %DriveLetter%:\delete_me\FilesToDelete*.txt)

echo on
REM
REM Clean up Saved_Stuff directory
REM

chdir /d %Acorn_Root%\Saved_stuff
setlocal DisableDelayedExpansion
FORFILES /P "%Acorn_Root%\Saved_stuff" /D -5 /C "cmd /c IF @isdir == TRUE echo @path" > %DriveLetter%:\delete_me\FilesToDelete.txt 
IF %ERRORLEVEL% NEQ 0 (goto REMOVED_SAVED_STUFF_DIR:)
for /f %%A in (%DriveLetter%:\delete_me\FilesToDelete.txt) do (call :REMOVE_SPACES %%~A )
IF not exist %DriveLetter%:\delete_me\FilesToDelete2.txt (goto REMOVED_SAVED_STUFF_DIR:)
for /f "delims=\ tokens=1-6" %%A in (%DriveLetter%:\delete_me\FilesToDelete2.txt) do ( 
	echo -%%A- -%%B- -%%C- -%%D- -%%E- -%%F-
	if "%%D" NEQ "config_base"  (rd /S /Q  %%A\%%B\%%C\%%D) else (echo Bypass config_base we need it, %%A\%%B\%%C\%%D )
	)

:REMOVED_SAVED_STUFF_DIR
if exist %DriveLetter%:\delete_me\FilesToDelete*.txt (del %DriveLetter%:\delete_me\FilesToDelete*.txt)

forfiles /P "%Acorn_Root%\Saved_stuff" /D -5  /C "cmd /c if @isdir EQU FALSE ( echo @path )" > %DriveLetter%:\delete_me\FilesToDelete.txt 
IF %ERRORLEVEL% NEQ 0  (goto REMOVED_SAVED_STUFF_FILES:)
for /f %%A in (%DriveLetter%:\delete_me\FilesToDelete.txt) do (call :REMOVE_SPACES %%~A )
IF not exist %DriveLetter%:\delete_me\FilesToDelete2.txt (goto REMOVED_SAVED_STUFF_FILES:)
for /f "delims=\ tokens=1-6" %%A in (%DriveLetter%:\delete_me\FilesToDelete2.txt) do ( 
	echo %%A %%B %%C %%D %%E %%F 
	if "%%D" NEQ "FilesToDelete2.txt"  (del /S /F %%A\%%B\%%C\%%D) else (echo Bypass FilesToDelete2 since we need it %%A\%%B\%%C\%%D\%%E\%%F)
	)

:REMOVED_SAVED_STUFF_FILES
REM	
REM Clean up No_ftp directory
REM
if exist %DriveLetter%:\delete_me\FilesToDelete*.txt (del %DriveLetter%:\delete_me\FilesToDelete*.txt)

chdir /d %DriveLetter%:\delete_me
setlocal DisableDelayedExpansion
FORFILES /P "%DriveLetter%:\delete_me" /D -5 /C "cmd /c IF @isdir == TRUE echo @path" > %DriveLetter%:\delete_me\FilesToDelete.txt 
IF %ERRORLEVEL% NEQ 0  (goto REMOVED_DELETE_ME_DIR:)
for /f %%A in (%DriveLetter%:\delete_me\FilesToDelete.txt) do (call :REMOVE_SPACES %%~A )
IF not exist %DriveLetter%:\delete_me\FilesToDelete2.txt (goto REMOVED_DELETE_ME_DIR:)
for /f "delims=\ tokens=1-6" %%A in (%DriveLetter%:\delete_me\FilesToDelete2.txt) do ( 
	echo -%%A- -%%B- -%%C- -%%D- -%%E- -%%F-
	if "%%C" NEQ "No_ftp"  (rd /S /Q %%A\%%B\%%C) else (echo Bypass No_ftp since we need it %%A\%%B\%%C\%%D )
	)

:REMOVED_DELETE_ME_DIR
if exist %DriveLetter%:\delete_me\FilesToDelete*.txt (del %DriveLetter%:\delete_me\FilesToDelete*.txt)

forfiles /P "%DriveLetter%:\delete_me" /D -5  /C "cmd /c if @isdir EQU FALSE ( echo @path )" > %DriveLetter%:\delete_me\FilesToDelete.txt 
IF %ERRORLEVEL% NEQ 0 (goto REMOVED_DELETE_ME_FILES:)
for /f %%A in (%DriveLetter%:\delete_me\FilesToDelete.txt) do (call :REMOVE_SPACES %%~A )
IF not exist %DriveLetter%:\delete_me\FilesToDelete2.txt (goto REMOVED_DELETE_ME_FILES:)
for /f "delims=\ tokens=1-6" %%A in (%DriveLetter%:\delete_me\FilesToDelete2.txt) do ( 
	echo %%A %%B %%C %%D %%E %%F 
	if "%%C" NEQ "FilesToDelete2.txt"  (del /S /F %%A\%%B\%%C) else (echo Bypass FilesToDelete2 since we need %%A\%%B\%%C\%%D)
	)

:REMOVED_DELETE_ME_FILES

REM
REM  Double check to make sure we have space on the server.  
REM

dir d:\ > dir.lst

FOR /F "tokens=1-5 delims= " %%A IN (dir.lst) DO if "%%D"=="bytes" (if "%%E"=="free" ( set free_space=%%C)) 

echo %free_space%

REM
REM	Parse out the Free Space value and divide by 1000 to make sure the values fits in a 32-bit integer
REM
FOR /F "tokens=1-5 delims=," %%A IN ("%free_space%") DO (set /A free_space2=%%A%%B%%C%)

echo %free_space2% 
if %free_space2% GEQ 5368709 (goto THE_END:)

set VarMailText=%COMPUTERNAME% is still low on disk space after purging files, only about %free_space2% KB free.   Need to manually purge files.
set VarMailSubject=Purge Save files: Server still low on space!!!
goto BADEXIT:

:THE_END

rem	exit 0
goto EOF:

:BADEXIT
echo ------------------ Bad Exit -------------------------------------------
echo %VarMailText%
blat -to %VarNotify% -subject "%VarMailSubject%" -body "%VarMailText%"
rem *** if exist %VarCustProp% del %VarCustProp%
echo "Acorn Deployment failed."

REM
REM Clean up the directory stack.
REM
popd
popd
popd

exit /B 1

REM
REM  ---------------- Remove trailing spaces Subroutine --------------------
REM

:REMOVE_SPACES
set FILE_AND_PATH=%1%

echo -%FILE_AND_PATH%-

set NO_SPACES_FILENAME=%FILE_AND_PATH: =%
IF %ERRORLEVEL% LSS 1 goto ECHO_FILENAME:
set VarMailText=There were problems removing spaces from the file_name %FILE_AND_PATH%
set VarMailSubject=Purge Save files:Error
blat -to jfong@birst.com -subject "%VarMailSubject%" -body "%VarMailText%"

goto EOF:

:ECHO_FILENAME
echo -%NO_SPACES_FILENAME%-
echo %NO_SPACES_FILENAME%>> %DriveLetter%:\delete_me\FilesToDelete2.txt

:EOF


