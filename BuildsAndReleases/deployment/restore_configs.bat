ECHO ON
SETLOCAL 
REM Simple script to save configurations files for me.

set VarNotify=jfong@successmetricsinc.com
set VarMailText=Setup email message
set VarMailSubject=Archive Config Files

REM
REM Process the input variables, we have to have them or we can't run. 
REM

:PROCESS_INPUT

if "%1"=="-s" goto SET_TIMESTAMP:
if "%1"=="-a" goto SET_ACORN:
if "%1"=="-n" goto SET_NOTIFICATION:
if "%1"=="" goto PROCESS_INPUT_DONE:

goto PROCESS_INPUT_ERROR:

:SET_TIMESTAMP
shift
set TimeStamp=%1
goto PROCESS_INPUT_LOOP:

:SET_ACORN
shift
set Acorn_Root=%1
goto PROCESS_INPUT_LOOP:

:SET_NOTIFICATION
shift
set VarNotify=%1
goto PROCESS_INPUT_LOOP:

:PROCESS_INPUT_LOOP

shift
goto PROCESS_INPUT:

:PROCESS_INPUT_ERROR

echo restore_configs.bat will restore working configuration files from 
echo	the working system before the upgrade.  This is useful for
echo	upgrades that fail after a partial deployment (e.g. locked files).
echo 	restore_configs.bat will restore the previous working config files.
echo ERROR: invalid input parameter
echo usage: restore_comfigs.bat [-options]
echo where options include:
echo.
echo  -s ^<Time Stamp^(optional)
echo 		Enter the Time Stamp from the last deployment.  You can 
echo		look for the most recent timestamp under
echo		...\Saved_Stuff\YYYYMMDDHHMM.  The directory name of 
echo		YYYYMMDDHHMM, lets the script know which files to use.		
echo  -a ^<acorn root^>
echo 		Enter the home directory of the instance, the
echo  		default is usually  c:\SMI
echo		This is the parent directory of Acorn and SMIWeb
echo  -n ^<email notification list^comma separated (optional)
echo  		default=jfong@successmetricsinc.com,4157301327@vtext.com
echo 		NOTE - mulitple email addresses need to be separated by 
echo 			commas and inclosed in double quote
echo.

set VarMailText=Invalid arguement(s) provided. 
set VarMailSubject=Acorn_Deploy:Process Arguements
goto BADEXIT

:PROCESS_INPUT_DONE

set Acorn_Home=%Acorn_Root%\Acorn
set SMI_Home=%Acorn_Root%\SMIWeb
set CHART_HOME="%Acorn_Root%\ChartService"
		
echo TimeStamp: -%TimeStamp%-
echo Acorn_Root: -%Acorn_Root%-
echo Acorn_Home:%Acorn_Home%
echo SMI_Home:%SMI_Home%
echo ChartService %CHART_HOME%

cd "%Acorn_Root%\Saved_stuff\%TimeStamp%"
IF %ERRORLEVEL% LSS 1 goto DIR_EXISTS:
set VarMailText=The restored directory does not exists.  We need a valid directory.  . 
set VarMailSubject=Restore_Configs: Restore Failed.
goto BADEXIT

:DIR_EXISTS
set /a Copy_Error_Cnt=0
copy /V customer.properties %SMI_Home%\conf\
set /a Copy_Error_Cnt=%Copy_Error_Cnt%+%ERRORLEVEL%

copy /V web.xml %SMI_Home%\tomcat\webapps\SMIWeb\WEB-INF\
set /a Copy_Error_Cnt=%Copy_Error_Cnt%+%ERRORLEVEL%

copy /V securityfilter-config.xml %SMI_Home%\tomcat\webapps\SMIWeb\WEB-INF\
set /a Copy_Error_Cnt=%Copy_Error_Cnt%+%ERRORLEVEL%

copy /V log4j.xml %SMI_Home%\tomcat\webapps\SMIWeb\WEB-INF\classes\
set /a Copy_Error_Cnt=%Copy_Error_Cnt%+%ERRORLEVEL%

copy /V Web.config %Acorn_Home%\
set /a Copy_Error_Cnt=%Copy_Error_Cnt%+%ERRORLEVEL%

copy /V Spaces.config %Acorn_Home%\
set /a Copy_Error_Cnt=%Copy_Error_Cnt%+%ERRORLEVEL%

copy /V NewUserSpaces.config %Acorn_Home%\
set /a Copy_Error_Cnt=%Copy_Error_Cnt%+%ERRORLEVEL%

copy /V Products.config %Acorn_Home%\
set /a Copy_Error_Cnt=%Copy_Error_Cnt%+%ERRORLEVEL%

copy /V chart-service.xml %CHART_HOME%\bin\
set /a Copy_Error_Cnt=%Copy_Error_Cnt%+%ERRORLEVEL%

IF %Copy_Error_Cnt%  EQU 0 ( goto RESTORE_COMPLETE: )
set VarMailText=The restored had problems with one or more file copy.  .  . 
set VarMailSubject=Restore_Configs: Restore file had an error.
goto BADEXIT

:RESTORE_COMPLETE

set VarMailText=The restored directory completed without an error. 
set VarMailSubject=Restore_Configs: Restore was successful.
blat -to %VarNotify% -subject %VarMailSubject% -body %VarMailText%

goto THE_END:


:BADEXIT
echo ------------------ Bad Exit -------------------------------------------
echo %VarMailText%
blat -to %VarNotify% -subject "%VarMailSubject%" -body "%VarMailText%"
rem *** if exist %VarCustProp% del %VarCustProp%
echo "Acorn Deployment failed."
ENDLOCAL
exit /B 1

:THE_END

ENDLOCAL
exit /B 0






