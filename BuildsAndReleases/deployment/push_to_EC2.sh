#!/usr/bin/bash
#
#this takes too long, over 3 hours, sixth try 
PATH="/usr/local/bin:/usr/bin:/cygdrive/c/Program Files/nodejs:/cygdrive/c/Ruby193/bin:/cygdrive/c/Program Files (x86)/JavaFX/javafx-sdk1.3/bin:/cygdrive/c/Program Files (x86)/JavaFX/javafx-sdk1.3/emulator/bin:/cygdrive/c/Windows:/cygdrive/c/Windows/system32:/cygdrive/c/Windows/System32/Wbem:/cygdrive/c/Program Files/Intel/DMIX:/cygdrive/c/Windows/System32/WindowsPowerShell/v1.0:/cygdrive/c/Program Files (x86)/Microsoft SQL Server/80/Tools/Binn:/cygdrive/c/Program Files/Microsoft SQL Server/90/DTS/Binn:/cygdrive/c/Program Files/Microsoft SQL Server/90/Tools/binn:/cygdrive/c/Program Files/Microsoft SQL Server/100/DTS/Binn:/cygdrive/c/Program Files/Microsoft SQL Server/100/Tools/Binn:/cygdrive/c/Program Files (x86)/Microsoft SQL Server/90/DTS/Binn:/cygdrive/c/Program Files (x86)/Microsoft SQL Server/90/Tools/binn:/cygdrive/c/Program Files (x86)/Microsoft SQL Server/90/Tools/Binn/VSShell/Common7/IDE:/cygdrive/c/Program Files (x86)/Microsoft SQL Server/100/DTS/Binn:/cygdrive/c/Program Files (x86)/Microsoft SQL Server/100/Tools/Binn:/cygdrive/c/Program Files (x86)/Microsoft SQL Server/100/Tools/Binn/VSShell/Common7/IDE:/cygdrive/c/Program Files (x86)/Microsoft Visual Studio 8/Common7/IDE/PrivateAssemblies:/usr/bin:/cygdrive/c/Program Files (x86)/Microsoft Visual Studio 9.0/Common7/IDE/PrivateAssemblies:/cygdrive/c/program files (x86)/Microsoft Visual Studio 10.0/Common7/IDE:/cygdrive/c/Program Files/MySQL/MySQL Server 5.1/bin:/cygdrive/c/Program Files/Java/jdk1.7.0/bin:/cygdrive/c/Program Files/Java/jdk1.7.0/jre/bin:/cygdrive/c/Program Files (x86)/apache-ant-1.8.2/bin:/cygdrive/c/Program Files (x86)/Git/cmd:/cygdrive/c/Program Files (x86)/Git/bin:/cygdrive/c/Program Files (x86)/apache-ant-1.8.2/bin:/cygdrive/c/jruby-1.7.2/bin"

JAVA_HOME=/cygdrive/c/Program\ Files/Java/jdk1.6.0_27
export JAVA_HOME
/usr/bin/env 

Notify=jfong@birst.com
build_timestamp=""
QA_timestamp=""

if [ $# -eq 1 ]; 
then
	echo "UpdateEC2: pem was file passed"
else
	blat -subject "UpdateEC2: No pem file  error" -body "We need a pem file to connect to EC2 instance, please pass as first parameter" -to $Notify
	exit -1
fi

pem_file="$1"
echo "$pem_file"

if [ -f $pem_file_name ];
then
	echo "UpdateEC2: pem file exists"
else
	blat -subject "UpdateEC2: No pem file  error" -body "We could not find a valid file, please pass a pem file as first parameter" -to $Notify
	exit -1
fi

while read -r line
do
  line=${line##*/}
  echo "${line%\"}"
    build_timestamp=${line:13:${#line}}
    echo $build_timestamp
done < //repo_dev/dev/Builds/Current_Build_Number/release/build_number.txt

echo Build found
if [ "$build_timestamp" == "" ];
then
	blat -subject "UpdateEC2: Build_number error" \
	-body "There were problems extracting the current release build number.  Please resolve to continue EC2 updates" \
	-to $Notify
	exit -1
fi

while read -r line
do
  line=${line##*/}
  echo "${line%\"}"
    QA_timestamp=${line:13:${#line}}
    echo $QA_timestamp
done < //repo_dev/dev/Builds/Current_Build_Number/QA_release/build_number.txt

echo QA_buid found
if [ "$QA_timestamp" == "" ];
then
	blat -subject "UpdateEC2: Build_number error" \
	-body "There were problems extracting the current QA build number.  Please resolve to continue EC2 updates" \
	-to $Notify
	exit -1
fi

date

EC2_Server[1]="54.235.211.101"		#	Comparison_ATC_IB
EC2_Server[2]="54.243.126.174"		#	Comparison_ATC_SQL

for ((index=1; index<=2; index++))
do
	printf "     %s\n" "${EC2_Server[index]}"
	error_count=0
	
	date
#	Transfor the build to the specified EC2 Server
	scp -r -i $pem_file //repo_dev/dev/Builds/Nightly/$build_timestamp/* ec2-user@${EC2_Server[index]}:/cygdrive/d/delete_me/No_ftp

	if [ $? != 0 ];
	then
		blat -subject "UpdateEC2: ${EC2_Server[index]} update of build files failed" \
		-body "scp to ${EC2_Server[index]} reported errors, please fix the scp error before trying again" \
		-to $Notify
		exit -1
	fi
	
	date
#	Transfer the QA build to the specified EC2 Server
	scp -r -i $pem_file //repo_dev/dev/Builds/Nightly/$QA_timestamp/* ec2-user@${EC2_Server[index]}:/cygdrive/d/delete_me/No_ftp

	if [ $? != 0 ];
	then
		blat -subject "UpdateEC2: ${EC2_Server[index]} update of QA build files failed" \
		-body "scp to ${EC2_Server[index]} reported errors, please fix the scp error before trying again" \
		-to $Notify
		exit -1
	fi

done

date
blat -subject "UpdateEC2: Update of EC2 instances are done" \
	-body "scp to the EC2 macines did not generate any errors" \
	-to $Notify

exit 0
