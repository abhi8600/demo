echo on
echo %1% %2% %3% %4%
set TimeStamp=""
set VarNotify="jfong@birst.com,pmalshe@birst.com"
set Install_Name=Visualizer
set Port_Num=6205
set Working_Branch=visualizer
set Current_Build_Number=\\repo_dev\Dev\Builds\Current_Build_Number
set Acorn_Root=D:\BIRST_HOME\BIRST

REM
REM We need a timestamp.  If it is not provided, we should create our own.
REM

if %TimeStamp% neq "" ( goto TIMESTAMP_DEFINED: ) 
	
for /f "tokens=1-3 delims=:." %%a in ("%time%") do set hh_mm_stamp=%%a%%b
echo %hh_mm_stamp%
echo %hh_mm_stamp:~0,1%
echo %hh_mm_stamp:~1,3%
if "%hh_mm_stamp:~0,1%" == " " ( set hh_mm_stamp=0%hh_mm_stamp:~1,3%)
echo %hh_mm_stamp%
for /f "tokens=1-3 delims=\ " %%a in ("%date%") do set date_only=%%b
for /f "tokens=1-3 delims=/." %%a in ("%date_only%") do set teststamp=%%c%%a%%b
echo %teststamp%
set TimeStamp=%teststamp%%hh_mm_stamp%
	
:TIMESTAMP_DEFINED
echo TimeStamp: -%TimeStamp%-

REM
REM where is 7-zip 
REM This handles the standard 32 and 64 bit locations
REM

set SevenZip_Home=""
if exist "c:\Program Files\7-Zip" ( set SevenZip_Home="c:\Program Files\7-Zip")
echo %SevenZip_Home%
if exist "C:\Program Files (x86)\7-Zip" ( set SevenZip_Home="C:\Program Files (x86)\7-Zip")
echo %SevenZip_Home%
if %SevenZip_Home% neq "" ( goto 7Zip_Found: )
echo We can't find the 7Zip directory"
set VarMailText=Cannot file 7Zip in the default 32 and 64 bit configurations. 
set VarMailSubject=Deploy_Visualizer on %COMPUTERNAME%:7Zip directory missing
goto BADEXIT


:7Zip_Found

REM
REM Figure out which build to use
REM
FOR /F "tokens=1,2,3 delims==" %%A IN (%Current_Build_Number%\%Working_Branch%\build_number.txt) DO if "%%A" == "build_number" (set build_number=%%B) else (ECHO "No build Number "%%A"-" %%B"-")
echo -%build_number%-
if "%build_number%" == "" (set build_number=201208141600)
echo -%build_number%-

REM
REM Copy the files to the local system
REM
if exist D:\delete_me\No_ftp (goto NO_FTP_EXISTS:)
set VarMailSubject="Deploy_Visualizer of %Build_Tag% on %COMPUTERNAME%:No FTP directory"
set VarMailText="There is no default No_FTP directory, please create." ) 
goto BADEXIT:)

:NO_FTP_EXISTS
chdir /d d:\delete_me\No_ftp
REM	del /F /Q /S *
xcopy /Y /V  \\repo_dev\dev\Builds\Nightly\%build_number%\Visualizer.zip .

:CLEAN_VISUALIZER
chdir /D %ACORN_ROOT%\Acorn

REM
REM Copy the Visualizer.aspx file 
REM

copy Visualizer\Visualizer.aspx  .
IF %ERRORLEVEL% LSS 1 goto VISUALIZER_SAVED:
set VarMailText=Visualizer had problems copying Visualizer.aspx file to save area, Please correct problem before proceeding. 
set VarMailSubject=Deploy_Visualizer on %COMPUTERNAME%:Visualizer.asxp save
goto BADEXIT

REM
REM Delete the old saved directory
REM
:VISUALIZER_SAVED
if not exist Visualizer_old goto NO_VISUALIZER_OLD:
del /F /S /Q Visualizer_old
dir /S Visualizer_old

if not exist Visualizer_old goto NO_VISUALIZER_OLD:
rmdir /S /Q Visualizer_old

if not exist Visualizer_old goto NO_VISUALIZER_OLD:
dir
set VarMailText=Problems with removing the Visualizer_old directory, Please correct problem before proceeding. 
set VarMailSubject=Deploy_Visualizer on %COMPUTERNAME%:VISUALIZER Backup
goto BADEXIT

:NO_VISUALIZER_OLD

xcopy /V /E /I /H /Y Visualizer Visualizer_old
IF %ERRORLEVEL% LSS 1 goto CREATE_VISUALIZER_DIR:
set VarMailText=Fail to xcopy Visualizer to Visualizer_old, Please correct problem before proceeding. 
set VarMailSubject=Deploy_Visualizer on %COMPUTERNAME%:VISUALIZER Backup
goto BADEXIT

:CREATE_VISUALIZER_DIR

del /F /S /Q Visualizer
dir /S Visualizer

if not exist Visualizer goto RELOAD_VISUALIZER:
del /F /S /Q Visualizer
dir /S Visualizer

if not exist Visualizer goto RELOAD_VISUALIZER:
rmdir /S /Q Visualizer

if not exist Visualizer goto RELOAD_VISUALIZER:
rmdir /S /Q Visualizer

IF not exist Visualizer goto RELOAD_VISUALIZER:
dir
set VarMailText=Fail to delete the  Visualizer directory, Please correct problem before proceeding. 
set VarMailSubject=Deploy_Visualizer on %COMPUTERNAME%:VISUALIZER Backup
goto BADEXIT

:RELOAD_VISUALIZER

%SevenZip_Home%\7Z.EXE x -r -y -tzip d:\delete_me\No_ftp\Visualizer.zip -o%ACORN_ROOT%\Acorn
IF %ERRORLEVEL% LSS 1 goto VISUALIZER_UNZIP_DONE:
set VarMailText=ACORN had problems Unzipping Visualizer, Please correct problem before proceeding. 
set VarMailSubject=Deploy_Visualizer on %COMPUTERNAME%:Visualizer unzip
goto BADEXIT

:VISUALIZER_UNZIP_DONE

dir app
rename app Visualizer
IF %ERRORLEVEL% LSS 1 goto RENAME_DONE:
set VarMailText=ACORN had problems creating Visualizer directory, the rename failed.  Please correct problem before proceeding. 
set VarMailSubject=Deploy_Visualizer on %COMPUTERNAME%:Visualizer rename
goto BADEXIT

:RENAME_DONE
REM
REM Restore the Visualizer.aspx file
REM
copy /Y Visualizer Visualizer\Visualizer.aspx  
IF %ERRORLEVEL% LSS 1 goto VISUALIZER_RESTORED:
set VarMailText=Visualizer had problems copying  Visualizer.aspx file BACK, Please correct problem before proceeding. 
set VarMailSubject=Deploy_Visualizer on %COMPUTERNAME%:Visualizer.asxp restore
goto BADEXIT

:VISUALIZER_RESTORED
del /Q Visualizer.aspx
dir Visualizer
set VarMailSubject="Deploy_Visualizer of %build_number% on %COMPUTERNAME%: Is Done"
set VarMailText="Birst updated and is now ready for use." 

blat -to "jfong@birst.com" -subject %VarMailSubject% -body %VarMailText%

goto THEEND:

:BADEXIT
echo ------------------ Bad Exit -------------------------------------------
echo %VarMailText%
blat -to %VarNotify% -subject "%VarMailSubject%" -body "%VarMailText%"
rem *** if exist %VarCustProp% del %VarCustProp%
echo "Acorn Deployment failed."
exit /B 1
:THEEND

exit /B 0