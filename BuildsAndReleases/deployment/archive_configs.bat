ECHO ON
SETLOCAL 
REM Simple script to save configurations files for me.

set /a Compare_Error_Cnt=0
set /a Compare_Error_Cnt=%Compare_Error_Cnt%+%ERRORLEVEL%
set VarNotify=jfong@successmetricsinc.com
set VarMailText=Setup email message
set VarMailSubject=Archive Config Files

REM
REM Process the input variables, we have to have them or we can't run. 
REM

:PROCESS_INPUT

if "%1"=="-s" goto SET_TIMESTAMP:
if "%1"=="-a" goto SET_ACORN:
if "%1"=="-n" goto SET_NOTIFICATION:
if "%1"=="" goto PROCESS_INPUT_DONE:

goto PROCESS_INPUT_ERROR:

:SET_TIMESTAMP
shift
set TimeStamp=%1
goto PROCESS_INPUT_LOOP:

:SET_ACORN
shift
set Acorn_Root=%1
goto PROCESS_INPUT_LOOP:

:SET_NOTIFICATION
shift
set VarNotify=%1
goto PROCESS_INPUT_LOOP:

:PROCESS_INPUT_LOOP

shift
goto PROCESS_INPUT:

:PROCESS_INPUT_ERROR

echo ERROR: invalid input parameter
echo usage: deploy_acorn [-options]
echo where options include:
echo.
echo  -s ^<Time Stamp^(optional)
echo 		Enter the Time Stamp for identifying the deployment
echo		the format is YYYYMMDDHHMM, or one will be generated.		
echo  -a ^<acorn root^>
echo 		Enter the home directory of the instance, the
echo  		default is usually  c:\SMI
echo		This is the parent directory of Acorn and SMIWeb
echo  -n ^<email notification list^comma separated (optional)
echo  		default=jfong@successmetricsinc.com,4157301327@vtext.com
echo 		NOTE - mulitple email addresses need to be separated by 
echo 			commas and inclosed in double quote
echo.

set VarMailText=Invalid arguement(s) provided. 
set VarMailSubject=Acorn_Deploy:Process Arguements
goto BADEXIT

:PROCESS_INPUT_DONE

set Acorn_Home=%Acorn_Root%\Acorn
set SMI_Home=%Acorn_Root%\SMIWeb
set CHARTSERVER_HOME="%Acorn_Root%\ChartServer"
set SCHEDULER_HOME="%Acorn_Root%\Scheduler"
		
echo TimeStamp: -%TimeStamp%-
echo Acorn_Root: -%Acorn_Root%-
echo Acorn_Home:%Acorn_Home%
echo SMI_Home:%SMI_Home%
echo ChartService %CHARTSERVER_HOME%
echo SCHEDULER_HOME %SCHEDULER_HOME%


mkdir "%Acorn_Root%\Saved_stuff\%TimeStamp%"
set /a Compare_Error_Cnt=%Compare_Error_Cnt%+%ERRORLEVEL%

cd "%Acorn_Root%\Saved_stuff\%TimeStamp%"
set /a Compare_Error_Cnt=%Compare_Error_Cnt%+%ERRORLEVEL%

mkdir Config
set /a Compare_Error_Cnt=%Compare_Error_Cnt%+%ERRORLEVEL%

copy %SMI_Home%\conf\customer.properties .
set /a Compare_Error_Cnt=%Compare_Error_Cnt%+%ERRORLEVEL%

copy %SMI_Home%\tomcat\webapps\SMIWeb\WEB-INF\web.xml .
set /a Compare_Error_Cnt=%Compare_Error_Cnt%+%ERRORLEVEL%

copy %SMI_Home%\tomcat\webapps\SMIWeb\WEB-INF\securityfilter-config.xml .
set /a Compare_Error_Cnt=%Compare_Error_Cnt%+%ERRORLEVEL%

copy %SMI_Home%\tomcat\webapps\SMIWeb\WEB-INF\classes\log4j.xml .
set /a Compare_Error_Cnt=%Compare_Error_Cnt%+%ERRORLEVEL%

copy %Acorn_Home%\Web.config .
set /a Compare_Error_Cnt=%Compare_Error_Cnt%+%ERRORLEVEL%

copy %Acorn_Home%\Config\* Config 
set /a Compare_Error_Cnt=%Compare_Error_Cnt%+%ERRORLEVEL%

copy /V /Y %SCHEDULER_HOME%\config\*.properties .
set /a Compare_Error_Cnt=%Compare_Error_Cnt%+%ERRORLEVEL%

copy /V /Y %SCHEDULER_HOME%\tomcat\webapps\SCHEDULER\WEB-INF\web.xml .
set /a Compare_Error_Cnt=%Compare_Error_Cnt%+%ERRORLEVEL%

copy /V /Y %SCHEDULER_HOME%\tomcat\webapps\SCHEDULER\WEB-INF\axis2.xml .
set /a Compare_Error_Cnt=%Compare_Error_Cnt%+%ERRORLEVEL%

copy /V /Y %SCHEDULER_HOME%\tomcat\webapps\SCHEDULER\WEB-INF\classes\log4j.xml .
set /a Compare_Error_Cnt=%Compare_Error_Cnt%+%ERRORLEVEL%

copy /V /Y %SCHEDULER_HOME%\tomcat\webapps\SCHEDULER\WEB-INF\classes\schedulerQuartz.properties .
set /a Compare_Error_Cnt=%Compare_Error_Cnt%+%ERRORLEVEL%

IF %Compare_Error_Cnt%  LSS 1 goto COPY_DONE:
set VarMailSubject="Deploy_all on %COMPUTERNAME%:Config copy had issues, manual intervention needed"
set VarMailText="Manual review of deployment necessary since we had problems copying config files."
goto BADEXIT

:COPY_DONE


ENDLOCAL


