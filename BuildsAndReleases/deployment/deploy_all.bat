ECHO ON
REM 
REM These should be input parameters in the future
REM
echo %1% %2% %3% %4%
set Acorn_Root=""
set Release_Dir=""
set Build_Tag=""
set SMI_Port=""
set Scheduler_Root=""
set Scheduler_Port=""
set TimeStamp=""
set Instance=""
set CurrentDir=%CD%
set Clean_Flag=1
set Archive_Flag=0
set NoFTP_Flag=0

REM 
REM Setup parameters for notification
REM 

set VarNotify="jfong@birst.com"
REM 		,pmalshe@birst.com,ricks@birst.com"
set VarMailText=Setup email message
set VarMailSubject=Deploy Acorn

set VarMailSubject="Deploy_All started on %COMPUTERNAME%"
set VarMailText="Updating Birst on %COMPUTERNAME% is in progress" 
blat -to "jfong@birst.com,pmalshe@birst.com,ricks@birst.com" -subject %VarMailSubject% -body %VarMailText%

REM	set WinZip_Home=""
REM	if exist "C:\Program Files\WinZip" (set WinZip_Home="c:\Program Files\WinZip")
REM	if exist "C:\Program Files (x86)\WinZip" (set WinZip_Home="C:\Program Files (x86)\WinZip")        
REM	if %WinZip_Home% neq "" (goto PROCESS_INPUT:)
REM	echo We can't find the WinZip directory"
REM	set VarMailText=Cannot file WinZip in the default 32 and 64 bit configurations. 
REM	set VarMailSubject=Acorn_Deploy:WinZip directory missing
REM	goto BADEXIT

:PROCESS_INPUT

if "%1"=="-a" goto SET_ACORN:
if "%1"=="-n" goto SET_NOTIFICATION:
if "%1"=="-p" goto SET_SMI_PORT:
if "%1"=="-k" goto SET_CLEAN_DIR:
if "%1"=="-r" goto SET_RELEASE_DIR:
if "%1"=="-s" goto SET_TIMESTAMP:
if "%1"=="-t" goto SET_BUILD_TAG:
if "%1"=="-i" goto SET_INSTANCE:
if "%1"=="-g" goto SET_ARCHIVE:
if "%1"=="-f" goto SET_NOFTP:
if "%1"=="" goto PROCESS_INPUT_DONE:

goto PROCESS_INPUT_ERROR:

:SET_TIMESTAMP
shift
set TimeStamp=%1
goto PROCESS_INPUT_LOOP:

:SET_ACORN
shift
set Acorn_Root=%1
set Scheduler_Root=%1
goto PROCESS_INPUT_LOOP:

:SET_CLEAN_DIR
shift
set Clean_Flag=%1
goto PROCESS_INPUT_LOOP:

:SET_RELEASE_DIR
shift
set Release_Dir=%1
goto PROCESS_INPUT_LOOP:

:SET_INSTANCE
shift
set Instance=%1
set aws_type=%Instance:~0,3%
goto PROCESS_INPUT_LOOP:

:SET_BUILD_TAG
shift
set Build_Tag=%1
goto PROCESS_INPUT_LOOP:

:SET_SMI_PORT
shift
set SMI_Port=%1
set Scheduler_Port=%1
set CONNECTOR_Port=%1
goto PROCESS_INPUT_LOOP:

:SET_NOTIFICATION
shift
set VarNotify=%1
goto PROCESS_INPUT_LOOP:

:SET_ARCHIVE
shift
set Archive_Flag=%1
goto PROCESS_INPUT_LOOP:

:SET_NOFTP
shift
set NoFTP_Flag=%1
goto PROCESS_INPUT_LOOP:

:PROCESS_INPUT_LOOP

shift
goto PROCESS_INPUT:

:PROCESS_INPUT_ERROR

echo ERROR: invalid input parameter
echo usage: deploy_acorn [-options]
echo where options include:
echo.
echo  -s ^<Time Stamp^> (optional)
echo 		Enter the Time Stamp for identifying the deployment
echo		the format is YYYYMMDDHHMM, or one will be generated.		
echo  -a ^<acorn root^>
echo 		Enter the home directory of the instance, the
echo 		default is usually  c:\SMI
echo 		This is the parent directory of Acorn and SMIWeb
echo  -p ^<port number^>
echo 		Enter 4 digit port number for the installation	
echo  		Installation will use port number +2
echo 		e.g. 9300, 9301, 9302
echo  -k ^<clean_flag^>
echo 		Any non-zero value for this flag will clean temp directories
echo  -r ^<Release directory^> 
echo  		Which do you want to get the release from?
echo 		Nightly for nightly builds
echo 		Releases/Platform for version releases
echo  -i ^<Instance you are deploying^>
echo            Instance you are deploying to (e.g sfoqa1, sde, prod)
echo  -t ^<Build tag^> 
echo  		If you are getting files from Releases/Platform,
echo 		enter a Release Number e.g. 2.0.0
echo 		If you are getting files from Nightly, 
echo 		enter a date and time e.g. 200705230130
echo  -g ^<1^> (optional)
echo 		Enter the 1 (one) if you want to bypass backing up the instance.
echo		The value of 0 (zero) or undefined will perform a backup of the instance.
echo  -f ^<1^> (optional)
echo 		Enter the 1 (one) if you want to bypass FTP.  If we bypass FTP, the system
echo		expect the files to be in {current directory}:\delete_me\No_ftp 
echo		(e.g. the D:\delete_me\No_ftp directory for most of the production machines).
echo		The value of 0 (zero) or undefined will cause the script to FTP the required
echo		files from the FTP server..
echo  -n ^<email notification list^> comma separated (optional)
echo  		default=jfong@successmetricsinc.com,4157301327@vtext.com
echo 		NOTE - mulitple email addresses need to be separated by 
echo 			commas and inclosed in double quote
echo.
echo Note: the following files needs to be in the Saved_stuff\config_base directory
echo 	chart-service.xml
echo 	customer.properties
echo 	NewUserSpaces.config
echo 	Products.config
echo 	securityfilter-config.xml
echo 	Spaces.config
echo 	Web.config
echo 	web.xml
echo 	

set VarMailText=Invalid arguement(s) provided. 
set VarMailSubject=Deploy_All:Process Arguements
goto BADEXIT

:PROCESS_INPUT_DONE

for /f "delims=:, tokens=1,2" %%A in ("%Acorn_Root%") do ( set DriveLetter=%%A )
set DriveLetter=%DriveLetter: =%

for /f "delims=\, tokens=1-4" %%A in ("%Acorn_Root%") do ( 
	echo %%A-%%B-%%C-%%D 
	set temp=%%B
	)

echo Install_name=%Install_Name%-

set Install_Name=SMI
set IIS_NAME=%Install_Name%
if %Install_Name%==SMI ( set Install_Name=SMIWeb & set IIS_Name=Acorn )
if %Install_Name%==SMI1 ( set Install_Name=SMIWeb & set IIS_Name=AcornSMI1 )
set Install_Name=%Install_Name: =%
set IIS_NAME=%IIS_Name: =%
set Scheduler_Name=Scheduler 
set Scheduler_Name=%Scheduler_Name: =%
set Connector_Name=Connector

echo Install_Name=%Install_Name%-
echo IIS_Name=%IIS_Name%-
echo Scheduler_Name=%Scheduler_Name%-
echo TimeStamp		= %TimeStamp%
echo VarNotify		= %VarNotify%
echo Acorn_Root		= %Acorn_Root%
echo DriveLetter	= %DriveLetter%
echo Release_Dir	= %Release_Dir%
echo Build_Tag		= %Build_Tag%
echo SMI_Port		= %SMI_Port%
echo Clean_Flag		= %Clean_Flag%
echo NoFTP_Flag		= %NoFTP_Flag%

if %Acorn_Root%=="" ( goto PROCESS_INPUT_ERROR )
if %Release_Dir%=="" ( goto PROCESS_INPUT_ERROR )
if %Build_Tag%=="" ( goto PROCESS_INPUT_ERROR )
if %SMI_Port%=="" ( goto PROCESS_INPUT_ERROR )
if %Instance%=="" ( goto PROCESS_INPUT_ERROR )
if %Scheduler_Root%=="" ( goto PROCESS_INPUT_ERROR )
if %Scheduler_Port%=="" ( goto PROCESS_INPUT_ERROR )

set ACORN_HOME="%Acorn_Root%\Acorn"
echo %ACORN_HOME%

set SMI_HOME="%Acorn_Root%\SMIWeb"
echo %SMI_HOME%

set CHARTSERVER_HOME="%Acorn_Root%\ChartServer"
echo %CHARTSERVER_HOME%

set SCHEDULER_HOME="%Acorn_Root%\Scheduler"
echo %SCHEDULER_HOME%

set CONNECTOR_HOME="%Acorn_Root%\Connectors"
echo %CONNECTOR_HOME%

set PERFENGINES_HOME="%Acorn_Root%\PerfEngines"
echo %PERFENGINES_HOME%

REM
REM We need a timestamp.  If it is not provided, we should create our own.
REM

if %TimeStamp% neq "" ( goto TIMESTAMP_DEFINED: ) 
	
for /f "tokens=1-3 delims=:." %%a in ("%time%") do set hh_mm_stamp=%%a%%b
echo %hh_mm_stamp%
echo %hh_mm_stamp:~0,1%
echo %hh_mm_stamp:~1,3%
if "%hh_mm_stamp:~0,1%" == " " ( set hh_mm_stamp=0%hh_mm_stamp:~1,3%)
echo %hh_mm_stamp%
for /f "tokens=1-3 delims=\ " %%a in ("%date%") do set date_only=%%b
for /f "tokens=1-3 delims=/." %%a in ("%date_only%") do set teststamp=%%c%%a%%b
echo %teststamp%
set TimeStamp=%teststamp%%hh_mm_stamp%
	
:TIMESTAMP_DEFINED
echo TimeStamp: -%TimeStamp%-

set DeleteMeDir=%DriveLetter%:\delete_me\%TimeStamp%
echo DeleteMeDir: -%DeleteMeDir%-

REM
REM Goto the same drive as the Acorn installation
REM

%DriveLetter%:

REM 
REM Setup the delete_me directory
REM 

dir %DeleteMeDir%
set /a Compare_Error_Cnt=%ERRORLEVEL% 
if %Compare_Error_Cnt% EQU 0 ( del /F /Q %DeleteMeDir%\* 
	) else (
	mkdir %DeleteMeDir% )

REM
REM where is 7-zip 
REM This handles the standard 32 and 64 bit locations
REM

set SevenZip_Home=""
if exist "c:\Program Files\7-Zip" ( set SevenZip_Home="c:\Program Files\7-Zip")
echo %SevenZip_Home%
if exist "C:\Program Files (x86)\7-Zip" ( set SevenZip_Home="C:\Program Files (x86)\7-Zip")
echo %SevenZip_Home%
if %SevenZip_Home% neq "" ( goto 7Zip_Found: )
echo We can't find the 7Zip directory"
set VarMailText=Cannot file 7Zip in the default 32 and 64 bit configurations. 
set VarMailSubject=Acorn_Deploy on %COMPUTERNAME%:7Zip directory missing
goto BADEXIT


:7Zip_Found

REM
REM 
REM --------------------------------------------------------------------------------------------
REM Backup the configuration files
REM

cd "%CurrentDir%"
call "%CurrentDir%\archive_configs.bat" -s %TimeStamp% -a %Acorn_Root%
IF %ERRORLEVEL% LSS 1 goto CONFIG_ARCHIVED:
set VarMailText=There were problems archiving the config files. 
set VarMailSubject=Deploy_All on %COMPUTERNAME%:Config archive problems
goto BADEXIT

:CONFIG_ARCHIVED

REM
REM Get the data files
REM

cd %DeleteMeDir%

If %NoFTP_Flag% EQU 1 goto COPY_LOCAL_FILES:

rem			echo open builds >download.ftp
rem			echo smibuilds>>download.ftp
rem			echo 5771builds!>>download.ftp
rem			echo verbose >>download.ftp
rem			echo cd   %Release_Dir%/%Build_Tag% >>download.ftp
rem			echo prompt >>download.ftp
rem			echo get PerformanceEngine.zip >>download.ftp
rem			echo get SMIWeb.war >>download.ftp
rem			echo get build.number >>download.ftp
rem			echo get "BirstAdmin.zip" >>download.ftp
rem			echo get SMIChartServer.war >>download.ftp
rem			echo get Scheduler.zip >>download.ftp
rem			echo cd ..\%Build_Tag%_Acorn >>download.ftp
rem			echo get Acorn.zip >>download.ftp
rem			echo get MoveSpace.zip >>download.ftp
rem			echo get BirstConnect.zip >>download.ftp
rem			echo get Help.zip >>download.ftp
rem			if %COMPUTERNAME% == SFOQA1 (echo get I18N.zip >>download.ftp)
rem			if %COMPUTERNAME% == SFOQA2 (echo get I18N.zip >>download.ftp)
rem			if %COMPUTERNAME% == SFOQA3 (echo get I18N.zip >>download.ftp)
rem			if %COMPUTERNAME% == SFOQA4 (echo get I18N.zip >>download.ftp)
rem			if %COMPUTERNAME% == SFOQA5 (echo get I18N.zip >>download.ftp)
rem			if %COMPUTERNAME% == SFOQA6 (echo get I18N.zip >>download.ftp)
rem			echo quit >>download.ftp
rem			
rem			ftp -s:download.ftp   > download.log 2>&1
rem			
rem			findstr /C:"Connection closed by remote host." download.log
rem			IF %ERRORLEVEL% EQU 1 goto CONNECTION_NOT_CLOSED:
rem			set VarMailText=FTP connection closed by remote host. 
rem			set VarMailSubject=Deploy_All on %COMPUTERNAME%:FTP
rem			goto BADEXIT
rem			
rem			:CONNECTION_NOT_CLOSED
rem			findstr /C:"Not connected" download.log
rem			IF %ERRORLEVEL% EQU 1 goto NOT_CONNECTION:
rem			set VarMailText=FTP not connected anymore.
rem			set VarMailSubject=Deploy_All on %COMPUTERNAME%:FTP
rem			goto BADEXIT
rem			
rem			:NOT_CONNECTION
rem			findstr /C:"Software caused connection abort" download.log
rem			IF %ERRORLEVEL% EQU 1 goto NOT_ABORTED:
rem			set VarMailText=Software caused connection abort.
rem			set VarMailSubject=Deploy_All on %COMPUTERNAME%:FTP
rem			goto BADEXIT

xcopy \\repo_dev\dev\Builds\%Release_Dir%\%build_number% .
goto NOT_ABORTED:

:COPY_LOCAL_FILES
xcopy /E %DriveLetter%:\delete_me\No_ftp\*  %DeleteMeDir%
dir

:NOT_ABORTED
REM echo NULL >download.ftp
REM rm download.ftp

if not exist PerformanceEngine.zip (goto FTP_Problems:)
if not exist SMIWeb.war (goto FTP_Problems:)
if not exist Acorn.zip (goto FTP_Problems:)
if not exist "BirstAdmin.zip" (goto FTP_Problems:)
if not exist build.number (goto FTP_Problems:)
if not exist SMIChartServer.war (goto FTP_Problems:)
if not exist BirstConnect.zip (goto FTP_Problems:)
if not exist Help.zip (goto FTP_Problems:)
if not exist Scheduler.zip (goto FTP_Problems:)
if not exist Scheduler.war (goto FTP_Problems:)
if not exist DashboardApp.zip (goto FTP_Problems:)
goto Start_Upgrade:

:FTP_Problems
set VarMailText=One or more of the FTP files were not recieved.  Please investigate the problem.  
set VarMailSubject=Deploy_All on %COMPUTERNAME%:FTP issues
goto BADEXIT


:Start_Upgrade

REM
REM --------------------------------------------------------------------------------------------
REM Upgrade the Performance Engine
REM 
REM 
REM  Figure out where to get the PERFENGINES_HOME

set PERFENGINES_HOME=""
set version_number=""
set spaces_config=%Acorn_Root%\Acorn\spaces.config

if not exist %spaces_config% (set spaces_config=%Acorn_Root%\Acorn\config\spaces.config)
if exist %spaces_config% (goto SPACES_CONFIG_EXISTS:)
set VarMailText=Cannot find spaces.config in standand locations. Please correct problem before proceeding. 
set VarMailSubject=Deploy_All on %COMPUTERNAME%:Spaces config not found.
goto BADEXIT

:SPACES_CONFIG_EXISTS
FOR /F "tokens=1,2,3,4,5,6,7,8,9,10,11 delims=	" %%A IN (%spaces_config%) DO if "%%A" == "0" (set ltemp1=%%J) else (ECHO %%A"-" %%B"-" %%C"-" %%D"-" %%E"-" %%F"-" %%G"-" %%H"-" %%I"-" %%K"-" %%K)
echo %ltemp1%

set PERFENGINES_HOME=%ltemp1:\bin\load_warehouse.bat=%\..

FOR /F "tokens=1,2,3,4 delims=." %%A IN (%DeleteMeDir%\build.number) DO if "%%A" == "release" (set vtemp1=%%B.%%C.%%D) else (ECHO %%A"-" %%B"-" %%C"-" %%D"-")
echo %vtemp1%
echo "Next"
FOR /F "tokens=1,2,3 delims= " %%A IN ("%vtemp1%") DO (set vtemp2=%%A)
echo %vtemp2%

FOR /F "tokens=1,2,3,4 delims==" %%A IN ("%vtemp2%") DO (set version_number=%%B)
echo %version_number%

IF not (%version_number% == "") ( goto PERFENGINES_START: )
set VarMailText=PerfEngine was not able to get a version number.  Please correct problem before proceeding. 
set VarMailSubject=Deploy_All on %COMPUTERNAME%:PerfEngine version number.
goto BADEXIT

:PERFENGINES_START

REM
REM Note: using pushd instead of cd since CMD does not recognize UNC path, but pushd does.
REM 
pushd %PERFENGINES_HOME%
IF %ERRORLEVEL% LSS 1 goto PUSHD1:
set VarMailText=Pushd to %PERFENGINES_HOME% generated an error, does directory exist? Please correct problem before proceeding. 
set VarMailSubject=Deploy_All on %COMPUTERNAME%:Pushd 1
goto BADEXIT

:PUSHD1
if exist %version_number% goto PERFENGINES_ZIP:
mkdir %version_number%
IF %ERRORLEVEL% LSS 1 goto PERFENGINES_ZIP:
set VarMailText=Create version number directory failed, Please correct problem before proceeding. 
set VarMailSubject=Deploy_All on %COMPUTERNAME%:create dir
goto BADEXIT

:PERFENGINES_ZIP
pushd %PERFENGINES_HOME%
IF %ERRORLEVEL% LSS 1 goto PUSHD2:
set VarMailText=Pushd to %PERFENGINES_HOME% generated an error, does directory exist? Please correct problem before proceeding. 
set VarMailSubject=Deploy_All on %COMPUTERNAME%:Pushd 2
goto BADEXIT

:PUSHD2

REM Skip archive if archive flag is true
IF %Archive_Flag% NEQ 0 goto BYPASS_BACKUP_PERFENGINES:

%SevenZip_Home%\7Z.EXE a -mx=5 -r -y -tzip %Acorn_Root%\Saved_stuff\%TimeStamp%_%version_number%_LoadWarehouse.zip %version_number%\*
IF %ERRORLEVEL% LSS 1 goto BYPASS_BACKUP_PERFENGINES:
set VarMailText=Backup of the PerfEngine environment failed, Please correct problem before proceeding. 
set VarMailSubject=Deploy_All on %COMPUTERNAME%:PerfEngine Backup
goto BADEXIT

:BYPASS_BACKUP_PERFENGINES

REM if not exist %version_number%_old goto NO_PERFENGINES_OLD:
REM 
REM rmdir /S /Q %version_number%_old
REM if not exist %version_number%_old goto NO_PERFENGINES_OLD:
REM set VarMailText=Problems with removing the PerfEngineService_old directory, Please correct problem before proceeding. 
REM set VarMailSubject=Deploy_All on %COMPUTERNAME%:PERFENGINES remove directory
REM goto BADEXIT
REM 
REM :NO_PERFENGINES_OLD
REM 
REM xcopy /V /E /I /H  %version_number% %version_number%_old
REM IF %ERRORLEVEL% LSS 1 goto CREATE_PERFENGINES_DIR:
REM set VarMailText=Fail to xcopy %version_number% to %version_number%_old, Please correct problem before proceeding. 
REM set VarMailSubject=Deploy_All on %COMPUTERNAME%:PERFENGINES copy
REM goto BADEXIT
REM 
REM :CREATE_PERFENGINES_DIR

del /F /S /Q %version_number%
IF %ERRORLEVEL% LSS 1 goto RELOAD_PERFENGINES:
set VarMailText=Fail to delete the  PerfEngineService directory, Please correct problem before proceeding. 
set VarMailSubject=Deploy_All on %COMPUTERNAME%:PERFENGINES Backup
goto BADEXIT

:RELOAD_PERFENGINES

pushd %PERFENGINES_HOME%
IF %ERRORLEVEL% LSS 1 goto PUSHD3:
set VarMailText=Pushd to %PERFENGINES_HOME% generated an error, does directory exist? Please correct problem before proceeding. 
set VarMailSubject=Deploy_All on %COMPUTERNAME%:Pushd 3
goto BADEXIT

:PUSHD3

%SevenZip_Home%\7Z.EXE x -r -y -tzip %DeleteMeDir%\PerformanceEngine.zip -o%version_number%
IF %ERRORLEVEL% LSS 1 goto PERFENGINES_UNZIP_DONE:
set VarMailText=ACORN had problems Unzipping Perfengine, Please correct problem before proceeding. 
set VarMailSubject=Deploy_All on %COMPUTERNAME%:PERFENGINES unzip
goto BADEXIT

:PERFENGINES_UNZIP_DONE

REM 
REM copy jdk to new directory
REM
REM	xcopy /V /Y /E /I /H  %SMI_HOME%\jdk %version_number%\jdk 

dir %PERFENGINES_HOME%\%version_number%\jdk
if exist "%PERFENGINES_HOME%\%version_number%\jdk" goto PERFENGINES_JDK_COPIED:
mklink /D D:\BIRST_HOME\BIRST\PerfEngines\%version_number%\jdk  D:\BIRST_HOME\BIRST\Java\jdk1.7.0_02
IF %ERRORLEVEL% LSS 1 goto PERFENGINES_JDK_COPIED:
set VarMailText=ACORN had problems copying the jdk directory, Please correct problem before proceeding. 
set VarMailSubject=Deploy_All on %COMPUTERNAME%:PERFENGINES copy jdk
goto BADEXIT

:PERFENGINES_JDK_COPIED

copy "%DeleteMeDir%\build.number" %version_number%
IF %ERRORLEVEL% LSS 1 goto PERFENGINES_SETUP_DONE:
set VarMailText=SMI had problems copying the build.number file, Please correct problem before proceeding. 
set VarMailSubject=Deploy_All on %COMPUTERNAME%:Performance Engine build.number
goto BADEXIT

:PERFENGINES_SETUP_DONE

REM
REM Clean up the directory stack.
REM
popd
popd
popd

REM 
REM --------------------------------------------------------------------------------------------
REM upgrade SMI
REM

REM Skip archive if archive flag is true
IF %Archive_Flag% NEQ 0 goto BYPASS_BACKUP_SMI:

cd %SMI_HOME%
%SevenZip_Home%\7Z.EXE a -mx=5 -r -y -tzip -x!logs\*  -x!tomcat\logs\*  %Acorn_Root%\Saved_stuff\%TimeStamp%_SMI_platform.zip * 
IF %ERRORLEVEL% LSS 1 goto BYPASS_BACKUP_SMI:
set VarMailText=Backup of the SMI environment failed, Please correct problem before proceeding. 
set VarMailSubject=Deploy_All on %COMPUTERNAME%:SMI Backup
goto BADEXIT

:BYPASS_BACKUP_SMI

echo "shutdown the server"
net stop %Install_Name%%SMI_Port%
if %ERRORLEVEL% == 2 (
	goto SHUTDOWN_DONE: 
	) else (
	if %ERRORLEVEL% == 0 ( goto SHUTDOWN_DONE: ))
set VarMailText=Shutdown of the SMIWEB server did not complete normally. 
set VarMailSubject=Deploy_All on %COMPUTERNAME%:Shutdown did not complete
goto BADEXIT

:SHUTDOWN_DONE
REM sleep for 30 seconds
ping -n 30 127.0.0.1 > NUL 2>&1
 

if %Clean_Flag% equ 0 ( goto Update_SMIWeb: )
cd %SMI_HOME%\tomcat
dir "temp"
if %ERRORLEVEL% NEQ 0 goto Update_SMIWeb:

del /Q /S temp\*

:Update_SMIWeb
cd %SMI_HOME%\tomcat\webapps
rmdir /S /Q %SMI_HOME%\tomcat\webapps\SMIWeb
IF %ERRORLEVEL% LSS 1 goto SMI_RMDIR_DONE:
set VarMailText=SMI had problems removing SMIWeb, Please correct problem before proceeding. 
set VarMailSubject=Deploy_All on %COMPUTERNAME%:SMI rmdir
goto BADEXIT

:SMI_RMDIR_DONE
del %SMI_HOME%\tomcat\webapps\SMIWeb.war
IF %ERRORLEVEL% LSS 1 goto SMI_DEL_DONE:
set VarMailText=SMI had problems deleting SMIWeb.war, Please correct problem before proceeding. 
set VarMailSubject=Deploy_All on %COMPUTERNAME%:SMI Del
goto BADEXIT

:SMI_DEL_DONE
dir

copy %DeleteMeDir%\SMIWeb.war %SMI_HOME%\tomcat\webapps
IF %ERRORLEVEL% LSS 1 goto SMI_COPY_DONE:
set VarMailText=SMI had problems copying new SMIWeb.war, Please correct problem before proceeding. 
set VarMailSubject=Deploy_All on %COMPUTERNAME%:SMI Copy
goto BADEXIT

:SMI_COPY_DONE

cd %SMI_HOME%

%SevenZip_Home%\7Z.EXE x -r -y -tzip %DeleteMeDir%\PerformanceEngine.zip -o%SMI_HOME%
IF %ERRORLEVEL% LSS 1 goto SMI_UNZIP_DONE:
set VarMailText=SMI had problems Unzipping Perfengine, Please correct problem before proceeding. 
set VarMailSubject=Acorn_Deploy on %COMPUTERNAME%:SMI unzip
goto BADEXIT

:SMI_UNZIP_DONE
%SevenZip_Home%\7Z.EXE x -r -y -tzip "%DeleteMeDir%\BirstAdmin.zip" -o%SMI_HOME%\bin @%Acorn_Root%\SetupTools\BirstAdminExtract.txt
IF %ERRORLEVEL% LSS 1 goto UNZIP_BIRSTADMIN_DONE:
set VarMailText=SMI had problems Unzipping Perfengine, Please correct problem before proceeding. 
set VarMailSubject=Deploy_All on %COMPUTERNAME%:SMI copy Birst Administration.exe
goto BADEXIT

:UNZIP_BIRSTADMIN_DONE
copy "%DeleteMeDir%\build.number" "%SMI_HOME%\bin"
IF %ERRORLEVEL% LSS 1 goto BUILDNUMBER_COPIED:
set VarMailText=SMI had problems Unzipping Perfengine, Please correct problem before proceeding. 
set VarMailSubject=Deploy_All on %COMPUTERNAME%:SMI build.number
goto BADEXIT

:BUILDNUMBER_COPIED
echo "startup the server"
net start %Install_Name%%SMI_Port%
if %ERRORLEVEL% == 0  ( goto STARTUP_DONE:)
REM sleep for 30 seconds
ping -n 30 127.0.0.1 > NUL 2>&1
for /f "tokens=2-4" %%a in ('sc query %Install_Name%%SMI_Port% ^| findstr RUNNING') do if %%c==RUNNING ( goto STARTUP_DONE:)
set VarMailText=Startup of the SMIWEB server did not startup normally. 
set VarMailSubject=Deploy_All on %COMPUTERNAME%:SMI startup did not complete
goto BADEXIT

:STARTUP_DONE
REM sleep for 30 seconds
 ping -n 30 127.0.0.1 > NUL 2>&1
 
REM
REM Save the most recent configuration files.
REM

copy /V /Y %SMI_HOME%\conf\customer.properties %Acorn_Root%\SetupTools\most_current\
copy /V /Y %SMI_HOME%\tomcat\webapps\SMIWeb\WEB-INF\web.xml %Acorn_Root%\SetupTools\most_current\
copy /V /Y %SMI_HOME%\tomcat\webapps\SMIWeb\WEB-INF\securityfilter-config.xml %Acorn_Root%\SetupTools\most_current\
copy /V /Y %SMI_HOME%\tomcat\webapps\SMIWeb\WEB-INF\classes\log4j.xml %Acorn_Root%\SetupTools\most_current\

REM 
REM --------------------------------------------------------------------------------------------
REM Upgrade Acorn
REM


REM
REM Backup the Acorn directory tree
REM

REM Skip archive if archive flag is true
IF %Archive_Flag% NEQ 0 goto BYPASS_BACKUP_ACORN:

cd %ACORN_HOME%
%SevenZip_Home%\7Z.EXE a -mx=5 -r -y -tzip %Acorn_Root%\Saved_stuff\%TimeStamp%_ACORN_platform.zip * 
IF %ERRORLEVEL% LSS 1 goto BYPASS_BACKUP_ACORN:
set VarMailText=Backup of the ACORN environment failed, Please correct problem before proceeding. 
set VarMailSubject=Deploy_All on %COMPUTERNAME%:ACORN Backup
goto BADEXIT

:BYPASS_BACKUP_ACORN
REM
REM First move the old Acorn Directory to Acorn_old, the create a new Acorn Directory to 
REM unzip the files.  
REM
cd %Acorn_Root%

systeminfo /FO LIST | find "Microsoft Windows XP Professional"
IF %ERRORLEVEL% neq 0 ( goto OS_Not_XP1: )
Iisreset /stop
IF %ERRORLEVEL% LSS 1 ( goto ACORN_STOPPED: )
set VarMailText=IIS was not able to stop correctly.  Please correct problem before proceeding. 
set VarMailSubject=Deploy_All on %COMPUTERNAME%:ACORN not restarted
goto BADEXIT

:OS_Not_XP1
systeminfo /FO LIST | find "Microsoft(R) Windows(R) Server 2003 Standard x64 Edition"
IF %ERRORLEVEL% neq 0 (goto OS_Not_S2003_1: )
cscript c:\WINDOWS\system32\iisweb.vbs /stop %IIS_Name%
IF %ERRORLEVEL% LSS 1 ( goto ACORN_STOPPED: )
set VarMailText=IIS was not able to stop correctly.  Please correct problem before proceeding. 
set VarMailSubject=Deploy_All on %COMPUTERNAME%:ACORN not restarted
goto BADEXIT

:OS_Not_S2003_1
systeminfo /FO LIST | find "Microsoft Windows Server 2008"
IF %ERRORLEVEL% neq 0 (goto OS_Unknown: )
%windir%\system32\inetsrv\appcmd STOP apppool /apppool.name:%IIS_Name%_pools
IF %ERRORLEVEL% LSS 1 ( goto ACORN_STOPPED: )
set VarMailText=IIS7 was not able to stop correctly.  Please correct problem before proceeding. 
set VarMailSubject=Deploy_All on %COMPUTERNAME%:ACORN not restarted
goto BADEXIT

:ACORN_STOPPED

if not exist Acorn_old goto NO_ACORN_OLD:

rmdir /S /Q Acorn_old
if not exist Acorn_old goto NO_ACORN_OLD:

set VarMailText=Problems with removing the ACORN_old directory, Please correct problem before proceeding. 
set VarMailSubject=Deploy_All on %COMPUTERNAME%:ACORN Backup
goto BADEXIT

:NO_ACORN_OLD

xcopy /V /E /I /H  Acorn Acorn_old
IF %ERRORLEVEL% LSS 1 goto CREATE_ACORN_DIR:
set VarMailText=Fail to xcopy Acorn to Acorn_old, Please correct problem before proceeding. 
set VarMailSubject=Deploy_All on %COMPUTERNAME%:ACORN Backup
goto BADEXIT

:CREATE_ACORN_DIR

del /F /S /Q Acorn
IF %ERRORLEVEL% LSS 1 goto RELOAD_ACORN:
set VarMailText=Fail to delete the  Acorn directory, Please correct problem before proceeding. 
set VarMailSubject=Deploy_All on %COMPUTERNAME%:ACORN Backup
goto BADEXIT

:RELOAD_ACORN

cd %ACORN_HOME%

%SevenZip_Home%\7Z.EXE x -r -y -tzip "%DeleteMeDir%\Acorn.zip" -o%ACORN_HOME%
IF %ERRORLEVEL% LSS 1 goto ACORN_UNZIP_DONE:
set VarMailText=ACORN had problems Unzipping Perfengine, Please correct problem before proceeding. 
set VarMailSubject=Deploy_All on %COMPUTERNAME%:ACORN unzip
goto BADEXIT

:ACORN_UNZIP_DONE

copy /V /Y %ACORN_HOME%\web.config %Acorn_Root%\SetupTools\most_current\
copy /V /Y %ACORN_HOME%\Spaces.config %Acorn_Root%\SetupTools\most_current\
copy /V /Y %ACORN_HOME%\NewUserSpaces.config %Acorn_Root%\SetupTools\most_current\
copy /V /Y %ACORN_HOME%\Products.config %Acorn_Root%\SetupTools\most_current\

%SevenZip_Home%\7Z.EXE x -r -y -tzip %DeleteMeDir%\DashboardApp.zip -o%ACORN_HOME%
IF %ERRORLEVEL% LSS 1 goto HTML_UNZIP_DONE:
set VarMailText=SMI had problems Unzipping DashboardApp, Please correct problem before proceeding. 
set VarMailSubject=Acorn_Deploy on %COMPUTERNAME%:DashboardApp unzip
goto BADEXIT

:HTML_UNZIP_DONE

REM
REM Hack to get Nima's new login page deployed for testing.
REM

if not exist C:\Users\jfong\Desktop\NewSSOLogin\LoginSSO.zip (goto UNZIP_NEW_LOGIN_DONE:)
cd $ACORN_HOME%
%SevenZip_Home%\7Z.EXE x -r -y -tzip C:\Users\jfong\Desktop\NewSSOLogin\LoginSSO.zip -o%ACORN_HOME%
IF %ERRORLEVEL% LSS 1 goto UNZIP_NEW_LOGIN_DONE:
set VarMailText=SMI had problems Unzipping loginSSO.zip, Please correct problem before proceeding. 
set VarMailSubject=Acorn_Deploy on %COMPUTERNAME%:loginSSO unzip
goto BADEXIT

:UNZIP_NEW_LOGIN_DONE

systeminfo /FO LIST | find "Microsoft Windows XP Professional"
IF %ERRORLEVEL% neq 0 ( goto OS_Not_XP: )
Iisreset /start
IF %ERRORLEVEL% LSS 1 ( goto ACORN_RESTARTED: )
set VarMailText=IIS was not able to restart correctly.  Please correct problem before proceeding. 
set VarMailSubject=Deploy_All on %COMPUTERNAME%:ACORN not restarted
goto BADEXIT

:OS_Not_XP
systeminfo /FO LIST | find "Microsoft(R) Windows(R) Server 2003 Standard x64 Edition"
IF %ERRORLEVEL% neq 0 (goto OS_Not_S2003: )
cscript c:\WINDOWS\system32\iisweb.vbs /start %IIS_Name%
IF %ERRORLEVEL% LSS 1 ( goto ACORN_RESTARTED: )
set VarMailText=IIS was not able to restart correctly.  Please correct problem before proceeding. 
set VarMailSubject=Deploy_All on %COMPUTERNAME%:ACORN not restarted
goto BADEXIT

:OS_Not_S2003
systeminfo /FO LIST | find "Microsoft Windows Server 2008"
IF %ERRORLEVEL% neq 0 (goto OS_Unknown: )
%windir%\system32\inetsrv\appcmd START apppool /apppool.name:%IIS_Name%_pools
IF %ERRORLEVEL% LSS 1 ( goto ACORN_RESTARTED: )
set VarMailText=IIS was not able to restart correctly.  Please correct problem before proceeding. 
set VarMailSubject=Deploy_All on %COMPUTERNAME%:ACORN not restarted
goto BADEXIT

:OS_Unknown
set VarMailText=Couldn't recognize  which OS we were running on.  Did not know how to restart IIS. 
set VarMailSubject=Deploy_All on %COMPUTERNAME%:Could not restart the webserver.
goto BADEXIT

:ACORN_RESTARTED

REM
REM --------------------------------------------------------------------------------------------
REM Upgrade the Chart Service
REM 

REM Skip archive if archive flag is true
IF %Archive_Flag% NEQ 0 goto BYPASS_BACKUP_CHARTSERVER:

cd %CHARTSERVER_HOME%
%SevenZip_Home%\7Z.EXE a -mx=5 -r -y -x!logs\*  -x!tomcat\logs\* -tzip %Acorn_Root%\Saved_stuff\%TimeStamp%_CHARTSERVER.zip * 
IF %ERRORLEVEL% LSS 1 goto BYPASS_BACKUP_CHARTSERVER:
set VarMailText=Backup of the CHARTSERVER environment failed, Please correct problem before proceeding. 
set VarMailSubject=Deploy_All on %COMPUTERNAME%:CHARTSERVER Backup
goto BADEXIT

:BYPASS_BACKUP_CHARTSERVER

echo "shutdown the server"
net stop ChartServer%SMI_Port%
if %ERRORLEVEL% == 2 (
	goto SHUTDOWN_DONE: 
	) else (
	if %ERRORLEVEL% == 0 ( goto SHUTDOWN_DONE: ))
set VarMailText=Shutdown of the ChartServer%SMI_Port% server did not complete normally. 
set VarMailSubject=Deploy_All on %COMPUTERNAME%:Shutdown did not complete
goto BADEXIT

:SHUTDOWN_DONE
REM sleep for 30 seconds
ping -n 30 127.0.0.1 > NUL 2>&1
 

if %Clean_Flag% equ 0 ( goto Update_SMIChartServer: )
cd %CHARTSERVER_HOME%\tomcat
dir "temp"
if %ERRORLEVEL% NEQ 0 goto Update_SMIChartServer:

del /Q /S temp\*

:Update_SMIChartServer
cd %CHARTSERVER_HOME%\tomcat\webapps
rmdir /S /Q %CHARTSERVER_HOME%\tomcat\webapps\SMIChartServer
IF %ERRORLEVEL% LSS 1 goto CHARTSERVER_RMDIR_DONE:
set VarMailText=CHARTSERVER had problems removing SMIChartServer, Please correct problem before proceeding. 
set VarMailSubject=Deploy_All on %COMPUTERNAME%:CHARTSERVER rmdir
goto BADEXIT

:CHARTSERVER_RMDIR_DONE
del %CHARTSERVER_HOME%\tomcat\webapps\SMIChartServer.war
IF %ERRORLEVEL% LSS 1 goto CHARTSERVER_DEL_DONE:
set VarMailText=CHARTSERVER had problems deleting SMIChartServer.war, Please correct problem before proceeding. 
set VarMailSubject=Deploy_All on %COMPUTERNAME%:SMIChartServer Del
goto BADEXIT

:CHARTSERVER_DEL_DONE
dir

copy %DeleteMeDir%\SMIChartServer.war %CHARTSERVER_HOME%\tomcat\webapps
IF %ERRORLEVEL% LSS 1 goto CHARTSERVER_COPY_DONE:
set VarMailText=CHARTSERVER had problems copying new SMIChartServer.war, Please correct problem before proceeding. 
set VarMailSubject=Deploy_All on %COMPUTERNAME%:SMIChartServer Copy
goto BADEXIT

:CHARTSERVER_COPY_DONE

echo "startup the server"
net start ChartServer%SMI_Port%
if %ERRORLEVEL% == 0  ( goto STARTUP_DONE:)
REM sleep for 30 seconds
ping -n 30 127.0.0.1 > NUL 2>&1
for /f "tokens=2-4" %%a in ('sc query ChartServer%SMI_Port% ^| findstr RUNNING') do if %%c==RUNNING ( goto STARTUP_DONE:)
set VarMailText=Startup of the SMIChartServer server did not startup normally. 
set VarMailSubject=Deploy_All on %COMPUTERNAME%:CHARTSERVER startup did not complete
goto BADEXIT

:STARTUP_DONE
REM sleep for 30 seconds
 ping -n 30 127.0.0.1 > NUL 2>&1
 
REM
REM Save the most recent configuration files.
REM

REM	copy /V /Y %CHARTSERVER_HOME%\conf\customer.properties %Acorn_Root%\SetupTools\most_current\
REM	copy /V /Y %CHARTSERVER_HOME%\tomcat\webapps\SMIChartServer\WEB-INF\web.xml %Acorn_Root%\SetupTools\most_current\
REM	copy /V /Y %CHARTSERVER_HOME%\tomcat\webapps\SMIChartServer\WEB-INF\securityfilter-config.xml %Acorn_Root%\SetupTools\most_current\
copy /V /Y %CHARTSERVER_HOME%\tomcat\webapps\SMIChartServer\WEB-INF\classes\log4j.xml %Acorn_Root%\SetupTools\most_current\chartserver_log4j.xml


REM 
REM --------------------------------------------------------------------------------------------
REM upgrade SCHEDULER
REM
REM   Adding \css\classic\datasourcetest.css......  

REM Skip archive if archive flag is true
IF %Archive_Flag% NEQ 0 goto BYPASS_BACKUP_SCHEDULER:

cd %SCHEDULER_HOME%
REM 	%WinZip_Home%\WZZIP.EXE -ex -r -P -a -x.\logs\*.* -x.\tomcat\logs\*.* -x.\jdk*\*.* -x.\tomcat\webapps\probe\*.* %Scheduler_Root%\Saved_stuff\%TimeStamp%_Scheduler_platform.zip .
%SevenZip_Home%\7Z.EXE a -mx=5 -r -y -x!logs\*  -x!tomcat\logs\* -x!jdk*\* -x!tomcat\webapps\probe\* -tzip %Scheduler_Root%\Saved_stuff\%TimeStamp%_Scheduler_platform.zip .

IF %ERRORLEVEL% LSS 1 goto BYPASS_BACKUP_SCHEDULER:
set VarMailText=Backup of the SCHEDULER environment failed, Please correct problem before proceeding. 
set VarMailSubject=Deploy_All on %COMPUTERNAME%:Scheduler Backup
goto BADEXIT

:BYPASS_BACKUP_SCHEDULER

echo "shutdown the server"
net stop %Scheduler_Name%%Scheduler_Port%
if %ERRORLEVEL% == 2 (
	goto SHUTDOWN_DONE: 
	) else (
	if %ERRORLEVEL% == 0 ( goto SHUTDOWN_DONE: ))
set VarMailText=Shutdown of the Scheduler server did not complete normally. 
set VarMailSubject=Deploy_All on %COMPUTERNAME%:Shutdown did not complete
goto BADEXIT

:SHUTDOWN_DONE
REM sleep for 30 seconds
ping -n 30 127.0.0.1 > NUL 2>&1
 
cd %SCHEDULER_HOME%\tomcat
dir "temp"
if %ERRORLEVEL% NEQ 0 goto Update_SCHEDULER:

del /Q /S temp\*

:Update_SCHEDULER
cd %SCHEDULER_HOME%\tomcat\webapps
rmdir /S /Q %SCHEDULER_HOME%\tomcat\webapps\SCHEDULER
IF %ERRORLEVEL% LSS 1 goto SCHEDULER_RMDIR_DONE:
set VarMailText=SCHEDULER had problems removing SCHEDULER, Please correct problem before proceeding. 
set VarMailSubject=Deploy_All on %COMPUTERNAME%:SCHEDULER rmdir
goto BADEXIT

:SCHEDULER_RMDIR_DONE
del %SCHEDULER_HOME%\tomcat\webapps\SCHEDULER.war
IF %ERRORLEVEL% LSS 1 goto SCHEDULER_DEL_DONE:
set VarMailText=SCHEDULER had problems deleting SCHEDULER.war, Please correct problem before proceeding. 
set VarMailSubject=Deploy_All on %COMPUTERNAME%:SCHEDULER Del
goto BADEXIT

:SCHEDULER_DEL_DONE
cd %SCHEDULER_HOME%\config
del /S /Q /F %SCHEDULER_HOME%\config
IF %ERRORLEVEL% LSS 1 goto SCHEDULER_RM_CONFIG_DONE:
set VarMailText=SCHEDULER had problems removing Config directory, Please correct problem before proceeding. 
set VarMailSubject=Deploy_All on %COMPUTERNAME%:SCHEDULER/config rmdir
goto BADEXIT

:SCHEDULER_RM_CONFIG_DONE
cd %SCHEDULER_HOME%
set /a loop_count=1
set /a error_code=0

:LOOP_START
echo %loop_count% 
REM	%WinZip_Home%\WZUNZIP.EXE -d -ybc -o -yo "%DeleteMeDir%\Scheduler.zip" %SCHEDULER_HOME%
%SevenZip_Home%\7Z.EXE x -r -y -tzip "%DeleteMeDir%\Scheduler.zip" -o%SCHEDULER_HOME%
set /a error_code=%ERRORLEVEL%
IF %error_code% EQU 0 goto SCHEDULER_UNZIP_DONE:
echo "SCHEDULER Unzip failed %loop_count%"
set /a loop_count=%loop_count%+1
IF %loop_count% LSS 3 goto LOOP_START:

IF %error_code% LSS 1 goto SCHEDULER_UNZIP_DONE:
set VarMailText=SCHEDULER had problems Unzipping, Please correct problem before proceeding. 
set VarMailSubject=Deploy_All on %COMPUTERNAME%:SCHEDULER unzip
goto BADEXIT

:SCHEDULER_UNZIP_DONE
copy %DeleteMeDir%\Scheduler.war %SCHEDULER_HOME%\tomcat\webapps
IF %ERRORLEVEL% LSS 1 goto SCHEDULER_COPY_DONE:
set VarMailText=Had problems copying new Scheduler.war, Please correct problem before proceeding. 
set VarMailSubject=Deploy_All on %COMPUTERNAME%:Scheduler Copy
goto BADEXIT


:SCHEDULER_COPY_DONE

echo "startup the server"
net start %Scheduler_Name%%Scheduler_Port%
if %ERRORLEVEL% == 0  ( goto STARTUP_DONE:)
REM sleep for 30 seconds
ping -n 30 127.0.0.1 > NUL 2>&1
for /f "tokens=2-4" %%a in ('sc query %Scheduler_Name%%Scheduler_Port% ^| findstr RUNNING') do if %%c==RUNNING ( goto STARTUP_DONE:)
set VarMailText=Startup of the SCHEDULER server did not startup normally. 
set VarMailSubject=Deploy_All on %COMPUTERNAME%:SCHEDULER startup did not complete
goto BADEXIT

:STARTUP_DONE
REM sleep for 30 seconds
 ping -n 30 127.0.0.1 > NUL 2>&1
 
REM
REM Save the most recent configuration files.
REM
if not exist "%Scheduler_Root%\SetupTools\most_current\Scheduler" (mkdir "%Scheduler_Root%\SetupTools\most_current\Scheduler")
copy /V /Y %SCHEDULER_HOME%\config\overrides.config %Scheduler_Root%\SetupTools\most_current\Scheduler\
copy /V /Y %SCHEDULER_HOME%\tomcat\webapps\SCHEDULER\WEB-INF\web.xml %Scheduler_Root%\SetupTools\most_current\Scheduler\
copy /V /Y %SCHEDULER_HOME%\tomcat\webapps\SCHEDULER\WEB-INF\axis2.xml %Scheduler_Root%\SetupTools\most_current\
copy /V /Y %SCHEDULER_HOME%\tomcat\webapps\SCHEDULER\WEB-INF\classes\log4j.xml %Scheduler_Root%\SetupTools\most_current\Scheduler\
copy /V /Y %SCHEDULER_HOME%\tomcat\webapps\SCHEDULER\WEB-INF\classes\schedulerQuartz.properties %Scheduler_Root%\SetupTools\most_current\Scheduler\


REM
REM ----------------------------------------------------------------------------------------------
REM
REM update AcornUploader to insure we have the most current AcornUploader for this instance
REM

:UPDATE_ACORNUPLOADER

cd %Acorn_Root%
dir "%Acorn_Root%\AcornUploader"
if %ERRORLEVEL% EQU 0 goto CLEAN_ACORNUPLOADER: else CREATE_ACORNUPLOADER:

:CLEAN_ACORNUPLOADER
del /F /Q %Acorn_Root%\AcornUploader\* 
set /a file_cnt=0
dir %Acorn_Root%\AcornUploader | find /C "." >%Acorn_Root%\file_cnt.tmp
set /P file_cnt=<%Acorn_Root%\file_cnt.tmp
IF %file_cnt% LEQ 3 goto ACORNUPLOADER_CREATED:
set VarMailText=Had problems clearing the AcornUploader directory, Please correct problem before proceeding. 
set VarMailSubject=Deploy_All on %COMPUTERNAME%:AcornUploader unzip
goto BADEXIT

:CREATE_ACORNUPLOADER
mkdir %Acorn_Root%\AcornUploader 
IF %ERRORLEVEL% LSS 1 goto ACORNUPLOADER_CREATED:
set VarMailText=Had problems creating the AcornUploader directory, Please correct problem before proceeding. 
set VarMailSubject=Deploy_All on %COMPUTERNAME%:AcornUploader unzip
goto BADEXIT

:ACORNUPLOADER_CREATED
%SevenZip_Home%\7Z.EXE x -r -y -tzip %DeleteMeDir%\AcornUploader.zip -o%Acorn_Root%\AcornUploader
IF %ERRORLEVEL% LSS 1 goto ACORNUPLOADER_UNZIP_DONE:
set VarMailText=ACORN had problems Unzipping AcornUploader, Please correct problem before proceeding. 
set VarMailSubject=Deploy_All on %COMPUTERNAME%:AcornUploader unzip
goto BADEXIT

:ACORNUPLOADER_UNZIP_DONE

REM 
REM --------------------------------------------------------------------------------------------
REM upgrade CONNECTOR
REM

:UPGRADE_CONNECTOR_TMP

REM	goto UPDATE_MOVESPACE:
REM Skip archive if archive flag is true
IF %Archive_Flag% NEQ 0 goto BYPASS_BACKUP_CONNECTOR:

cd %CONNECTOR_HOME%
%SevenZip_Home%\7Z.EXE a -mx=5 -r -y -tzip -x!logs\*  -x!tomcat\logs\*  %Acorn_Root%\Saved_stuff\%TimeStamp%_Connector_platform.zip * 
IF %ERRORLEVEL% LSS 1 goto BYPASS_BACKUP_CONNECTOR:
set VarMailText=Backup of the CONNECTOR environment failed, Please correct problem before proceeding. 
set VarMailSubject=Deploy_All on %COMPUTERNAME%:CONNECTOR Backup
goto BADEXIT

:BYPASS_BACKUP_CONNECTOR

echo "shutdown the server"
net stop %Connector_Name%%CONNECTOR_Port%
if %ERRORLEVEL% == 2 (
	goto SHUTDOWN_DONE: 
	) else (
	if %ERRORLEVEL% == 0 ( goto SHUTDOWN_DONE: ))
set VarMailText=Shutdown of the SMIWEB server did not complete normally. 
set VarMailSubject=Deploy_All on %COMPUTERNAME%:Shutdown did not complete
goto BADEXIT

:SHUTDOWN_DONE
REM sleep for 30 seconds
ping -n 30 127.0.0.1 > NUL 2>&1
 

if %Clean_Flag% equ 0 ( goto Update_Connector: )
cd %CONNECTOR_HOME%\tomcat
dir "temp"
if %ERRORLEVEL% NEQ 0 else goto Update_Connector:

del /Q /S temp\*

:Update_Connector
REM
REM	Update the Connectors Now
REM

for %%d in (ConnectorController SalesforceConnector_26_0 SalesforceConnector_28_0 NetSuiteConnector_2012_2 NetSuiteConnector_2013_2 MarketoConnector_2_1 GoogleAnalyticsConnector_3_0 OmnitureSiteCatalystConnector_1_4 NetSuiteJDBCConnector ) do ( 
call:UPDATE_CONNECTOR_FUNC %%d
)

:UPDATE_CONNECTOR_DONE

cd %CONNECTOR_HOME%/conf

%SevenZip_Home%\7Z.EXE x -r -y -tzip "%DeleteMeDir%\connectors\Connector_configs.zip" -o%CONNECTOR_HOME%/conf
IF %ERRORLEVEL% LSS 1 goto CONNECTOR_CONFIG_UNZIP_DONE:
set VarMailText=Connector_configs had problems Unzipping Perfengine, Please correct problem before proceeding. 
set VarMailSubject=Deploy_All on %COMPUTERNAME%:Connector_configs unzip
goto BADEXIT

:CONNECTOR_CONFIG_UNZIP_DONE
chdir /d  %CONNECTOR_HOME%/conf
IF %ERRORLEVEL% LSS 1 goto CONNECTOR_CONFIG_GOTO_CONFIG:
set VarMailText=CONNECTOR_CONFIG had problems changing directory to \conf. 
set VarMailSubject=Deploy_All on %COMPUTERNAME%:CONNECTOR_CONFIG .\conf
goto BADEXIT

:CONNECTOR_CONFIG_GOTO_CONFIG
dir 

copy /v /y %Instance%\* .
IF %ERRORLEVEL% LSS 1 goto UPDATE_CONNECTOR_CONFIG_DONE:
set VarMailText=CONNECTOR_CONFIG had problems copying the right config files in place.  
set VarMailSubject=Deploy_All on %COMPUTERNAME%:CONNECTOR_CONFIG copy config
goto BADEXIT

:UPDATE_CONNECTOR_CONFIG_DONE

cd %CONNECTOR_HOME%

echo "startup the server"
net start %Connector_Name%%CONNECTOR_Port%
if %ERRORLEVEL% == 0  ( goto STARTUP_DONE:)
REM sleep for 30 seconds
ping -n 30 127.0.0.1 > NUL 2>&1
for /f "tokens=2-4" %%a in ('sc query %Connector_Name%%CONNECTOR_Port% ^| findstr RUNNING') do if %%c==RUNNING ( goto STARTUP_DONE:)
set VarMailText=Startup of the SMIWEB server did not startup normally. 
set VarMailSubject=Deploy_All on %COMPUTERNAME%:CONNECTOR startup did not complete
goto BADEXIT

:STARTUP_DONE
REM sleep for 30 seconds
 ping -n 30 127.0.0.1 > NUL 2>&1

REM
REM ----------------------------------------------------------------------------------------------
REM
REM update MoveSpace to insure we have the most current movespace for this instance
REM

:UPDATE_MOVESPACE

cd %Acorn_Root%
dir "%Acorn_Root%\Movespace"
if %ERRORLEVEL% EQU 0 goto CLEAN_MOVESPACE: else CREATE_MOVESPACE:

:CLEAN_MOVESPACE
del /F /Q %Acorn_Root%\Movespace\* 
set /a file_cnt=0
dir %Acorn_Root%\Movespace | find /C "." >%Acorn_Root%\file_cnt.tmp
set /P file_cnt=<%Acorn_Root%\file_cnt.tmp
IF %file_cnt% LEQ 3 goto MOVESPACE_CREATED:
set VarMailText=Had problems clearing the Movespace directory, Please correct problem before proceeding. 
set VarMailSubject=Deploy_All on %COMPUTERNAME%:Movespace unzip
goto BADEXIT

:CREATE_MOVESPACE
mkdir %Acorn_Root%\Movespace 
IF %ERRORLEVEL% LSS 1 goto MOVESPACE_CREATED:
set VarMailText=Had problems creating the Movespace directory, Please correct problem before proceeding. 
set VarMailSubject=Deploy_All on %COMPUTERNAME%:Movespace unzip
goto BADEXIT

:MOVESPACE_CREATED
%SevenZip_Home%\7Z.EXE x -r -y -tzip %DeleteMeDir%\MoveSpace.zip -o%Acorn_Root%\Movespace
IF %ERRORLEVEL% LSS 1 goto MOVESPACE_UNZIP_DONE:
set VarMailText=ACORN had problems Unzipping Movespace, Please correct problem before proceeding. 
set VarMailSubject=Deploy_All on %COMPUTERNAME%:Movespace unzip
goto BADEXIT

:MOVESPACE_UNZIP_DONE

REM
REM  Update the Help files
REM

cd %Acorn_Root%
%SevenZip_Home%\7Z.EXE x -r -y -tzip %DeleteMeDir%\Help.zip -o%Acorn_Root%\Acorn\Help

IF %ERRORLEVEL% LSS 1 goto HELP_UNZIP_DONE:
set VarMailText=ACORN had problems Unzipping Help, Please correct problem before proceeding. 
set VarMailSubject=Deploy_All on %COMPUTERNAME%:Help unzip
goto BADEXIT

:HELP_UNZIP_DONE


REM
REM On SFOQA1 only, unzip the Spanish files
REM
if %COMPUTERNAME% == SFOQA1 ( %SevenZip_Home%\7Z.EXE x -r -y -tzip %DeleteMeDir%\I18N.zip -o%Acorn_Root%\Acorn\swf )
if %COMPUTERNAME% == SFOQA2 ( %SevenZip_Home%\7Z.EXE x -r -y -tzip %DeleteMeDir%\I18N.zip -o%Acorn_Root%\Acorn\swf )
if %COMPUTERNAME% == SFOQA3 ( %SevenZip_Home%\7Z.EXE x -r -y -tzip %DeleteMeDir%\I18N.zip -o%Acorn_Root%\Acorn\swf )
if %COMPUTERNAME% == SFOQA4 ( %SevenZip_Home%\7Z.EXE x -r -y -tzip %DeleteMeDir%\I18N.zip -o%Acorn_Root%\Acorn\swf )
if %COMPUTERNAME% == SFOQA5 ( %SevenZip_Home%\7Z.EXE x -r -y -tzip %DeleteMeDir%\I18N.zip -o%Acorn_Root%\Acorn\swf )
if %COMPUTERNAME% == SFOQA6 ( %SevenZip_Home%\7Z.EXE x -r -y -tzip %DeleteMeDir%\I18N.zip -o%Acorn_Root%\Acorn\swf )
if %COMPUTERNAME% == QAAPP-001 ( %SevenZip_Home%\7Z.EXE x -r -y -tzip %DeleteMeDir%\I18N.zip -o%Acorn_Root%\Acorn\swf )
if %COMPUTERNAME% == QAAPP-002	 ( %SevenZip_Home%\7Z.EXE x -r -y -tzip %DeleteMeDir%\I18N.zip -o%Acorn_Root%\Acorn\swf )


REM
REM ----------------------------------------------------------------------------------------------
REM
REM Stop the servers so we can copy the configuration files to their proper place
REM

net stop %Scheduler_Name%%Scheduler_Port%
if %ERRORLEVEL% == 2 (
	goto RESHUTDOWN_DONE: 
	) else (
	if %ERRORLEVEL% == 0 ( goto SCHEDULER_SHUTDOWN_DONE: ))
set VarMailText=Restart/shutdown of the SCHEDULER server did not complete normally. 
set VarMailSubject=Deploy_All on %COMPUTERNAME%:SCHEDULER restart shutdown did not complete
goto BADEXIT

:SCHEDULER_SHUTDOWN_DONE

REM  Chart service was not restarted to I don't need to restart it.  
net stop ChartServer%SMI_Port%
if %ERRORLEVEL% == 2 (
	goto CHART_SHUTDOWN: 
	) else (
	if %ERRORLEVEL% == 0 ( goto CHART_SHUTDOWN: ))
set VarMailText=Shutdown of the ChartServer%SMI_Port% server did not complete normally. 
set VarMailSubject=Deploy_All on %COMPUTERNAME%:Shutdown did not complete
goto BADEXIT

:CHART_SHUTDOWN

net stop %Install_Name%%SMI_Port%
if %ERRORLEVEL% == 2 (
	goto RESHUTDOWN_DONE: 
	) else (
	if %ERRORLEVEL% == 0 ( goto RESHUTDOWN_DONE: ))
set VarMailText=Restart/shutdown of the SMIWEB server did not complete normally. 
set VarMailSubject=Deploy_All on %COMPUTERNAME%:SMIWeb restart shutdown did not complete
goto BADEXIT

:RESHUTDOWN_DONE

systeminfo /FO LIST | find "Microsoft Windows XP Professional"
IF %ERRORLEVEL% neq 0 ( goto OS_Not_XP2: )
Iisreset /stop /noforce
IF %ERRORLEVEL% LSS 1 ( goto IIS_STOP_DONE: )
set VarMailText=IIS was not able to stop correctly.  Please correct problem before proceeding. 
set VarMailSubject=Deploy_All on %COMPUTERNAME%:ACORN not restarted 2
goto BADEXIT

:OS_Not_XP2
systeminfo /FO LIST | find "Microsoft(R) Windows(R) Server 2003 Standard x64 Edition"
IF %ERRORLEVEL% neq 0 (goto OS_Not_S2003_2: )
cscript c:\WINDOWS\system32\iisweb.vbs /stop %IIS_Name%
IF %ERRORLEVEL% LSS 1 ( goto IIS_STOP_DONE: )
set VarMailText=IIS was not able to stop correctly.  Please correct problem before proceeding. 
set VarMailSubject=Deploy_All on %COMPUTERNAME%:ACORN not restarted 2
goto BADEXIT

:OS_Not_S2003_2
systeminfo /FO LIST | find "Microsoft Windows Server 2008"
IF %ERRORLEVEL% neq 0 (goto OS_Unknown2: )
%windir%\system32\inetsrv\appcmd STOP apppool /apppool.name:%IIS_Name%_pools
IF %ERRORLEVEL% LSS 1 ( goto IIS_STOP_DONE: )
set VarMailText=IIS was not able to stop correctly.  Please correct problem before proceeding. 
set VarMailSubject=Deploy_All on %COMPUTERNAME%:ACORN not restarted 2
goto BADEXIT

:IIS_STOP_DONE

REM sleep for 30 seconds
ping -n 30 127.0.0.1 > NUL 2>&1
 
REM
REM -----------------------------------------------------------------------------------------------
REM
REM Copy the custom configurations files into place
REM
REM Restore the old config/xml files.
REM

set /a Compare_Error_Cnt=0

cd %Acorn_Root%\Saved_stuff\%TimeStamp%
set /a Compare_Error_Cnt=%Compare_Error_Cnt%+%ERRORLEVEL%

cd %ACORN_HOME%\config
set /a Compare_Error_Cnt=%Compare_Error_Cnt%+%ERRORLEVEL%

copy /v /y %Instance%\*.config .
set /a Compare_Error_Cnt=%Compare_Error_Cnt%+%ERRORLEVEL%

REM Put the proper Customer.properties for SMIWeb in place for specific server.
copy /v /y  %Instance%\customer.properties %SMI_HOME%\Conf\Customer.properties
set /a Compare_Error_Cnt=%Compare_Error_Cnt%+%ERRORLEVEL%

REM Put the proper log4j file for Audit logging in place for the specific server
copy /v /y  %Instance%\log4j.xml %SMI_HOME%\Conf\log4j.xml
set /a Compare_Error_Cnt=%Compare_Error_Cnt%+%ERRORLEVEL%

REM Put the proper log4j file for Audit logging in place for the specific server
copy /v /y  %Instance%\log4j.xml %SMI_HOME%\tomcat\webapps\SMIWeb\WEB-INF\classes\log4j.xml
set /a Compare_Error_Cnt=%Compare_Error_Cnt%+%ERRORLEVEL%

REM
REM Copy the custom configurations files into place
REM
REM Restore the old config/xml files.
REM

set /a Compare_Error_Cnt=0

cd %Scheduler_Root%\Saved_stuff\%TimeStamp%
set /a Compare_Error_Cnt=%Compare_Error_Cnt%+%ERRORLEVEL%

cd %SCHEDULER_HOME%\config
set /a Compare_Error_Cnt=%Compare_Error_Cnt%+%ERRORLEVEL%

copy /v /y %Instance%\*.properties .
set /a Compare_Error_Cnt=%Compare_Error_Cnt%+%ERRORLEVEL%

REM Put the proper Scheduler Service configuration in place for the specific server
REM It the configuration exists in the config file, use that, if not used our saved copy.

REM Put the proper axis2.xml for SCHEDULER in place for specific server.
if EXIST %Instance%\axis2.xml (
	copy /v /y  %Instance%\axis2.xml %SCHEDULER_HOME%\tomcat\webapps\SCHEDULER\WEB-INF\axis2.xml
	set /a Compare_Error_Cnt=%Compare_Error_Cnt%+%ERRORLEVEL% )

REM Put the proper web.xml for SCHEDULER in place for specific server.
if EXIST %Instance%\web.xml (
	copy /v /y  %Instance%\web.xml %SCHEDULER_HOME%\tomcat\webapps\SCHEDULER\WEB-INF\web.xml
	set /a Compare_Error_Cnt=%Compare_Error_Cnt%+%ERRORLEVEL% )

REM Put the proper schedulerQuartz.properties file in place for the specific server
if EXIST %Instance%\schedulerQuartz.properties (
	copy /v /y  %Instance%\schedulerQuartz.properties %SCHEDULER_HOME%\tomcat\webapps\SCHEDULER\WEB-INF\classes\schedulerQuartz.properties
	set /a Compare_Error_Cnt=%Compare_Error_Cnt%+%ERRORLEVEL% )

REM Put the proper log4j.xml for SCHEDULER in place for specific server.
if EXIST %Instance%\log4j.xml (
	copy /v /y  %Instance%\log4j.xml %SCHEDULER_HOME%\tomcat\webapps\SCHEDULER\WEB-INF\classes\log4j.xml
	set /a Compare_Error_Cnt=%Compare_Error_Cnt%+%ERRORLEVEL% )

IF %Compare_Error_Cnt%  EQU 0 ( 
	set VarMailSubject="Deploy_All on %COMPUTERNAME%:Config copy successful" & set VarMailText="Copy of configuration files was successful." 
	) else (
	set VarMailSubject="Deploy_All on %COMPUTERNAME%:Config copy had issues, manual intervention needed" & set VarMailText="Manual review of deployment necessary since we had problems copying config files." ) 

REM
REM -----------------------------------------------------------------------------------------------
REM
REM Before we restart the servers, check to see what kind of install this is.  
REM If it is a ATC/standalone Instance, we want to update the default processing engine to
REM point to the latest version of the preformance engine.  
REM

if not %aws_type%==atc (goto NOT_ATC:) 

if not exist %PERFENGINES_HOME%\default ( goto NO_DEFAULT_DIR: )
cd %PERFENGINES_HOME%
dir 

rmdir /Q .\default
IF %ERRORLEVEL% LSS 1 ( goto NO_DEFAULT_DIR: )
set VarMailText=Had problems deleting the PerfEngines/default directory. 
set VarMailSubject=Deploy_All on %COMPUTERNAME%: Set up for ATC
goto BADEXIT

:NO_DEFAULT_DIR
mklink /D %PERFENGINES_HOME%\default %PERFENGINES_HOME%\%version_number%
IF %ERRORLEVEL% LSS 1 ( goto NOT_ATC: )
set VarMailText=Had problems creating the symbolic link for PerfEngines/default. 
set VarMailSubject=Deploy_All on %COMPUTERNAME%: Set up for ATC
goto BADEXIT

:NOT_ATC

REM
REM -----------------------------------------------------------------------------------------------
REM
REM Since we copied over files, restart the servers.
REM


net start %Scheduler_Name%%Scheduler_Port%
if %ERRORLEVEL% == 0  ( goto RESTART_SCHEDULER_DONE:)
REM sleep for 30 seconds
ping -n 30 127.0.0.1 > NUL 2>&1
for /f "tokens=2-4" %%a in ('sc query %Scheduler_Name%%Scheduler_Port% ^| findstr RUNNING') do if %%c==RUNNING ( goto RESTARTUP_DONE:)
set VarMailText=Restart/startup of the SCHEDULER server did not startup normally. 
set VarMailSubject=Deploy_All on %COMPUTERNAME%:SCHEDULER restart startup did not complete
goto BADEXIT

:RESTART_SCHEDULER_DONE

net start ChartServer%SMI_Port%
if %ERRORLEVEL% == 0  ( goto CHART_RESTART:)
REM sleep for 30 seconds
ping -n 30 127.0.0.1 > NUL 2>&1
for /f "tokens=2-4" %%a in ('sc query ChartServer%SMI_Port% ^| findstr RUNNING') do if %%c==RUNNING ( goto CHART_RESTART:)
set VarMailText=Startup of the SMIChartServer server did not startup normally. 
set VarMailSubject=Deploy_All on %COMPUTERNAME%:CHARTSERVER startup did not complete
goto BADEXIT

:CHART_RESTART

net start %Install_Name%%SMI_Port%
if %ERRORLEVEL% == 0  ( goto RESTARTUP_DONE:)
REM sleep for 30 seconds
ping -n 30 127.0.0.1 > NUL 2>&1
for /f "tokens=2-4" %%a in ('sc query %Install_Name%%SMI_Port% ^| findstr RUNNING') do if %%c==RUNNING ( goto RESTARTUP_DONE:)
set VarMailText=Restart/startup of the SMIWEB server did not startup normally. 
set VarMailSubject=Deploy_All on %COMPUTERNAME%:SMIWeb restart startup did not complete
goto BADEXIT

:RESTARTUP_DONE
REM sleep for 30 seconds
ping -n 30 127.0.0.1 > NUL 2>&1
 
systeminfo /FO LIST | find "Microsoft Windows XP Professional"
IF %ERRORLEVEL% neq 0 ( goto OS_Not_XP3: )
Iisreset /start /noforce
IF %ERRORLEVEL% LSS 1 ( goto CONFIGS_DONE: )
set VarMailText=IIS was not able to restart correctly.  Please correct problem before proceeding. 
set VarMailSubject=Deploy_All on %COMPUTERNAME%:ACORN not restarted 2
goto BADEXIT

:OS_Not_XP3
systeminfo /FO LIST | find "Microsoft(R) Windows(R) Server 2003 Standard x64 Edition"
IF %ERRORLEVEL% neq 0 (goto OS_Not_S2003_3: )
cscript c:\WINDOWS\system32\iisweb.vbs /start %IIS_Name%
IF %ERRORLEVEL% LSS 1 ( goto CONFIGS_DONE: )
set VarMailText=IIS was not able to restart correctly.  Please correct problem before proceeding. 
set VarMailSubject=Deploy_All on %COMPUTERNAME%:ACORN not restarted 2
goto BADEXIT

:OS_Not_S2003_3
systeminfo /FO LIST | find "Microsoft Windows Server 2008"
IF %ERRORLEVEL% neq 0 (goto OS_Unknown2: )
%windir%\system32\inetsrv\appcmd START apppool /apppool.name:%IIS_Name%_pools
IF %ERRORLEVEL% LSS 1 ( goto CONFIGS_DONE: )
set VarMailText=IIS was not able to restart correctly.  Please correct problem before proceeding. 
set VarMailSubject=Deploy_All on %COMPUTERNAME%:ACORN not restarted 2
goto BADEXIT

:OS_Unknown2
set VarMailText=Couldn't recognize  which OS we were running on.  Did not know how to restart IIS. 
set VarMailSubject=Deploy_All on %COMPUTERNAME%:Could not restart the webserver 2.
goto BADEXIT

:CONFIGS_DONE

IF %Compare_Error_Cnt%  EQU 0 ( set VarMailSubject="Deploy_All of %Build_Tag% on %COMPUTERNAME%: Is Done" & set VarMailText="Birst updated and is now ready for use." 
	) else (
	set VarMailSubject="Deploy_All of %Build_Tag% on %COMPUTERNAME%:Done, but manual intervention needed" & set VarMailText="Manual review of deployment necessary since we had problems copying config files." ) 

blat -to "jfong@birst.com,QA@birst.com,ricks@birst.com" -subject %VarMailSubject% -body %VarMailText%

goto THE_END:


:BADEXIT
echo ------------------ Bad Exit -------------------------------------------
echo %VarMailText%
blat -to %VarNotify% -subject "%VarMailSubject%" -body "%VarMailText%"
rem *** if exist %VarCustProp% del %VarCustProp%
echo "Acorn Deployment failed."

REM
REM Clean up the directory stack.
REM
popd
popd
popd
rmdir /S /Q %DeleteMeDir%

exit /B 1

:THE_END
rmdir /S /Q %DeleteMeDir%

exit /B 0


REM
REM	Function to update Connectors
REM

:UPDATE_CONNECTOR_FUNC

set current_connector=%1%

if NOT EXIST %DeleteMeDir%\connectors\%current_connector%.war (
	set VarMailText=CONNECTOR had problems copying %current_connector%,war Please correct problem before proceeding. 
	set VarMailSubject=Deploy_All on %COMPUTERNAME%:%current_connector%.war is missing
	blat -to %VarNotify% -subject "%VarMailSubject%" -body "%VarMailText%"
	goto CONNECTOR_COPY_DONE:
	)

cd %CONNECTOR_HOME%\tomcat\webapps
IF not exist %CONNECTOR_HOME%\tomcat\webapps\%current_connector% goto :CONNECTOR_RMDIR_DONE
rmdir /S /Q %CONNECTOR_HOME%\tomcat\webapps\%current_connector%
IF %ERRORLEVEL% LSS 1 goto CONNECTOR_RMDIR_DONE:
set VarMailText=CONNECTOR had problems removing %current_connector%, Please correct problem before proceeding. 
set VarMailSubject=Deploy_All on %COMPUTERNAME%:%current_connector% rmdir
goto BADEXIT

:CONNECTOR_RMDIR_DONE
IF not exist %CONNECTOR_HOME%\tomcat\webapps\%current_connector%.war goto :CONNECTOR_DEL_DONE
del %CONNECTOR_HOME%\tomcat\webapps\%current_connector%.war
IF %ERRORLEVEL% LSS 1 goto CONNECTOR_DEL_DONE:
set VarMailText=CONNECTOR had problems deleting %current_connector%.war, Please correct problem before proceeding. 
set VarMailSubject=Deploy_All on %COMPUTERNAME%:%current_connector% Del
goto BADEXIT

:CONNECTOR_DEL_DONE
copy %DeleteMeDir%\connectors\%current_connector%.war %CONNECTOR_HOME%\tomcat\webapps
IF %ERRORLEVEL% LSS 1 goto CONNECTOR_COPY_DONE:
set VarMailText=CONNECTOR had problems copying new %current_connector%.war, Please correct problem before proceeding. 
set VarMailSubject=Deploy_All on %COMPUTERNAME%:%current_connector% Copy
goto BADEXIT

:CONNECTOR_COPY_DONE
dir

goto:eof