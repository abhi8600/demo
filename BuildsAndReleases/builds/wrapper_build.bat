REM
REM  Wrapper script to kick off the Bash script to build everything.
REM

set jfong_win_share=c:\\shared\\Builds\\logs

for /f "tokens=1-3 delims=:." %%a in ("%time%") do set hh_mm_stamp=%%a%%b
echo %hh_mm_stamp%
echo %hh_mm_stamp:~0,1%
echo %hh_mm_stamp:~1,3%
if "%hh_mm_stamp:~0,1%" == " " ( set hh_mm_stamp=0%hh_mm_stamp:~1,3%)
echo %hh_mm_stamp%
for /f "tokens=1-3 delims=\ " %%a in ("%date%") do set date_only=%%b
for /f "tokens=1-3 delims=/." %%a in ("%date_only%") do set teststamp=%%c%%a%%b
echo %teststamp%
set TimeStamp=%teststamp%%hh_mm_stamp%
	
:TIMESTAMP_DEFINED
echo TimeStamp: -%TimeStamp%-

cd c:\workspace_HEAD\BuildsAndReleases\builds
C:\cygwin\bin\bash.exe -x -v wrapper_head.sh -z 153KearnySt > daily_build.log 2>&1

IF %ERRORLEVEL% EQU 0 goto GET_TIMING:
exit -1

:GET_TIMING
c:\cygwin\bin\grep.exe RunningTime daily_build.log | c:\cygwin\bin\grep.exe -v "echo" > %jfong_win_share%\\RunningTime_%TimeStamp%.txt
