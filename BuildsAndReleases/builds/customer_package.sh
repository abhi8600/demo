#!/usr/bin/bash

# Quick script to package the user files for 2.1.x   
# There was a problem with how we were creating our 2.1.x packages.  Some of the 
# customer files was included in the platform package.   This created a situation 
# where we had to repackage the platform package at a later time when the customer 
# files were ready for release.  
#
# This script takes an existing platform build and recreates the Customer specific 
# package with the update customer files.  
# 
# NOTE - this uses Jerome's default build areas for create the new package.  
#	Look at the definition for jfong_share and jfong_clean.
#

DEBUG=0;					export DEBUG
nightly_bin=/cygdrive/c/workspace_HEAD/BuildsAndReleases/builds; 		export nightly_bin
jfong_share=/cygdrive/c/shared/Builds;		export jfong_share
jfong_clean=/cygdrive/c/clean_builds_2			export jfong_clean
Notify="jfong@birst.com" export Notify		#email list
declare -i error_count=0
build_tag=""
Cust_Name=""
CVS_Tag=""
New_Rel_Number=""

while getopts ":d:i:s:n:v:t:" opts; do
        case "$opts" in
		d ) echo "-d $OPTARG"
			DEBUG=$OPTARG
		 	export DEBUG
			;;
		i ) echo "-i $OPTARG" 
			New_Rel_Number=$OPTARG
			export New_Rel_Number
			;;
		s ) echo "-s $OPTARG" 
			Cust_Name=$OPTARG
			export Cust_Home
			;;
		v ) echo "-v $OPTARG" 
			CVS_Tag=$OPTARG
			export CVS_Tag 
			;;
		t ) echo "-t $OPTARG"			# Since I'm creating the build, I am generating the build tag.  
			build_tag=$OPTARG;	
			export build_tag
			;;
		n ) echo "-n $OPTARG" 
			Notify=$OPTARG
			export Notify 
			;;
		*  ) echo "usage: deploy [-options]"
			echo "where options include:"
			echo " -s <customer name> "
			echo "		Enter the ID of the customer that we want build customized files for."
			echo " 		e.g. RBC for RBC DainRauscher, FMR for Fidelity, etc."
			echo " -v <cvs tag>"
			echo " 		Enter the CVS tag to be used for pulling down the Customer files."
			echo " -n <email notification list> comma separated"
			echo " 		default=jfong@birst.com,4157301327@vtext.com"
			echo " -t <build tag> "
			echo "		We are looking for the Nightly,timestampe,the date and time "
			echo "		e.g. 200705230130, or 200806190705_230 for  a 2.3.0 build"
			echo " -i <release number>"
			echo " 		This is the release number for the new releases of customer files.  "
			echo " 		FMR has weekly releases, for every new push to production, we assign "
			echo " 		a new release number to the release.  The majority of time, the"
			echo " 		point release only has FMR files, but it could have mulitple customers"
			echo " 		if they are all releasing the same day"
			echo " 	For example, we have a FMR tag called FMR5659, we want to put the files in "
			echo " 	release number 2.3.2.1, the command would be"
			echo " bash -x customer_package.sh -s FMR -v FMR5659 -t 200805201013_215 -i 2.3.2.1 "
			exit 1 ;;
	esac
	echo one value $1
done

if [ -z $Cust_Name ] || [ -z $CVS_Tag ]  || [ -z $build_tag ] || [ -z $New_Rel_Number ] #	|| [ -z $Period_Id ]; 
then
	echo "usage: 21x_customer_package.sh [-options]"
	echo "where options include:"
	echo " -s <customer name> "
	echo "		Enter the ID of the customer that we want build customized files for."
	echo " 		e.g. RBC for RBC DainRauscher, FMR for Fidelity, etc."
	echo " -v <cvs tag>"
	echo " 		Enter the CVS tag to be used for pulling down the Customer files."
	echo " -n <email notification list> comma separated"
	echo " 		default=jfong@birst.com,4157301327@vtext.com"
	echo " -t <build tag> "
	echo "		We are looking for the Nightly,timestampe,the date and time "
	echo "		e.g. 200705230130, or 200806190705_230 for  a 2.3.0 build"
	echo " -i <release number>"
	echo " 		This is the release number for the new releases of customer files.  "
	echo " 		FMR has weekly releases, for every new push to production, we assign "
	echo " 		a new release number to the release.  The majority of time, the"
	echo " 		point release only has FMR files, but it could have mulitple customers"
	echo " 		if they are all releasing the same day"
	echo " 	For example, we have a FMR tag called FMR5659, we want to put the files in "
	echo " 	release number 2.3.2.1, the command would be"
	echo " bash -x 21x_customer_package.sh -r FMR -v FMR5659 -t 200805201013_215 -i 2.3.2.1 " 
	exit 1
fi 

#
#	Check to make sure the directory doesn't exist first.
#
if [ -d /cygdrive/b/Releases/Platform/$New_Rel_Number ];
then
	echo "The release already exist, you cannot overwrite an existing release" \
	| mutt -s "Customer_package: release version exists" $Notify
	exit -1
fi 

#
# Now Export my tag to create my build.
# 

if [ $DEBUG -eq 0 ]; then 
	cd $jfong_clean				# goto my clean build area
	rm -rf SMI_Customers		# clean out the old files.  

	/cygdrive/c/Program\ Files/CVSNT/cvs -d :pserver:jfong:jfong123@SMISRC:/CVS export -r $CVS_Tag  SMI_Customers

	stat=$?
	echo CVS exit status $stat

	if [ $stat == 0 ]; 
	then
		echo "CVS export for SMI_Customers worked"
	else
		echo "CVS export for SMI_Customers had errors"
		exit -1
	fi
fi

#
# Package up the customer specific files.  
#	1. zip up all the customer specific files (repository.xml, dashboards, reports, etc.)
#	2. FTP the files up to the local, colo and Rackspace machines.  
#
echo $Cust_Name
if [ ! -d $jfong_share/$build_tag/$Cust_Name ];
then
	mkdir $jfong_share/$build_tag/$Cust_Name
	if [ $? != 0 ];
	then
		echo "Problem creating the Customer specific subdirectory" \
		| mutt -s "Customer_package: Error creating customer subdirectory." $Notify
	fi
fi
#
# 2. <Cust_Name>.zip
#
error_count=0

cd $jfong_clean/SMI_Customers/$Cust_Name/Code/WebApp
error_count=error_count+$?
echo $error_count

pwd
if [ `ls ./catalog | wc -l` -gt 0 ]; 
then
    zip -r $jfong_share/$build_tag/$Cust_Name/$Cust_Name.zip ./catalog
    error_count=error_count+$?
    echo $error_count
fi

pwd
if [ `ls ./conf | wc -l` -gt 0 ]; 
then
    zip -r $jfong_share/$build_tag/$Cust_Name/$Cust_Name.zip ./conf
    error_count=error_count+$?
    echo $error_count
fi

if [ $error_count -ne 0 ];
then
	echo "Either we had problems creating the zip file or we do not have privilege t create the file" \
	| mutt -s "Customer_package: Problems creating the " $Cust_Name".zip file." $Notify
	exit
fi

if [ $DEBUG -eq 0 ]; then
	echo "copy over the files to the shared /smi/builds/"
	
	if [ -e "$jfong_clean"/upload.ftp ];
	then
		rm -rf  "$jfong_clean"/upload.ftp
	fi 

	#
	# FTP build to the //smi/builds directory
	#
	cd $jfong_clean
	echo "open builds" >upload.ftp
	echo "smibuilds" >>upload.ftp
	echo "5771builds!" >>upload.ftp
	echo "verbose" >>upload.ftp
	echo "prompt" >>upload.ftp
	echo "lcd C:\SFOJFONG\builds\\"$build_tag >>upload.ftp
	echo "cd Releases\Platform" >>upload.ftp
	echo "mkdir " $New_Rel_Number >>upload.ftp
	echo "cd " $New_Rel_Number >>upload.ftp
	echo "mkdir " $Cust_Name >>upload.ftp
	echo "cd " $Cust_Name >>upload.ftp
	echo "put "$Cust_Name"/"$Cust_Name".zip" >>upload.ftp
	echo "quit" >>upload.ftp
	
	ftp -s:upload.ftp  
	if [ $? == 0 ]; 
	then
		echo "upload of log files was successful"  
	else
		echo "Exiting since FTP was not successful in transfering the zipped up files.  " \
			| mutt -s "Customer_package: Unable to ftp the files to SMI" $Notify
		exit -1
	fi
	
	rm -rf  "$jfong_clean"/upload.ftp
	
	
	#
	# FTP build to the Colo1 build directory
	#
	cd $jfong_clean
	echo "open 192.168.1.161" >upload.ftp
	echo "smibuilds" >>upload.ftp
	echo "5771builds!" >>upload.ftp
	echo "verbose" >>upload.ftp
	echo "prompt" >>upload.ftp
	echo "lcd C:\SFOJFONG\builds\\"$build_tag >>upload.ftp
	echo "cd Releases\Platform" >>upload.ftp
	echo "mkdir " $New_Rel_Number >>upload.ftp
	echo "cd " $New_Rel_Number >>upload.ftp
	echo "mkdir " $Cust_Name >>upload.ftp
	echo "cd " $Cust_Name >>upload.ftp
	echo "put "$Cust_Name"/"$Cust_Name".zip" >>upload.ftp
	echo "quit" >>upload.ftp
	
	ftp -s:upload.ftp  
	if [ $? == 0 ]; 
	then
		echo "upload of log files was successful"  
	else
		echo "Exiting since FTP was not successful in transfering the zipped up files.  " \
			| mutt -s "Customer_package: Unable to ftp the files to colo1" $Notify
		exit -1
	fi
	
	rm -rf  "$jfong_clean"/upload.ftp

	#
	# FTP build to the FTP1 build directory
	#
	cd $jfong_clean
	echo "open rsbuilds" >upload.ftp
	echo "smibuilds" >>upload.ftp
	echo "5771builds!" >>upload.ftp
	echo "verbose" >>upload.ftp
	echo "prompt" >>upload.ftp
	echo "lcd C:\SFOJFONG\builds\\"$build_tag >>upload.ftp
	echo "cd Releases\Platform" >>upload.ftp
	echo "mkdir " $New_Rel_Number >>upload.ftp
	echo "cd " $New_Rel_Number >>upload.ftp
	echo "mkdir " $Cust_Name >>upload.ftp
	echo "cd " $Cust_Name >>upload.ftp
	echo "put "$Cust_Name"/"$Cust_Name".zip" >>upload.ftp
	echo "quit" >>upload.ftp
	
	ftp -s:upload.ftp  
	if [ $? == 0 ]; 
	then
		echo "upload of log files was successful"  
	else
		echo "Exiting since FTP was not successful in transfering the zipped up files.  " \
			| mutt -s "Customer_package: Unable to ftp the files to colo1" $Notify
		exit -1
	fi
	
#	rm -rf  "$jfong_clean"/upload.ftp
		
fi

cd $jfong_share/$build_tag
mv $Cust_Name $CVS_Tag

	