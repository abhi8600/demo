#! /usr/bin/bash	
#
# Nightly Build and Test
#echo on

source /cygdrive/c/Users/jfong/cygwin/jfong/.profile
PATH=.:/usr/local/bin:/usr/bin:/cygdrive/c/Windows/system32:/cygdrive/c/Windows:/cygdrive/c/Windows/System32/Wbem:/cygdrive/c/Windows/System32/WindowsPowerShell/v1.0:/cygdrive/c/Program\ Files\ \(x86\)/blat307/full
export PATH

env 

jfong_chg_dir=/cygdrive/c/Users/jfong/chg_dir;	export jfong_chg_dir
CVS_exe=/cygdrive/c/cygwin/bin/cvs.exe

projects=( "Acorn" \
"AcornExecute" \
"AcornTestConsole" \
"ChartRenderer" \
"ChartWebService" \
"DataConductor" \
"Decrypt" \
"FlexAdhoc" \
"Help" \
"LogicalExpression" \
"Maps" \
"MemDB" \
"MoveSpace" \
"Performance Optimizer Administration" \
"SMI" \
"Scheduler" \
"SourceAdmin" \
"Touch" )


date_range1=`date --date=yesterday +%Y-%m-%d\ %H\:%M`;	export date_range1
date_range2=`date +%Y-%m-%d\ %H\:%M`;			export date_range2

if [ -e $jfong_chg_dir/changes.log ]; then
	rm -f -v $jfong_chg_dir/changes.log
fi

touch $jfong_chg_dir/changes.log

for var in "${projects[@]}"
do
	echo "${var}"
	$CVS_exe -d :pserver:birst\\jfong:153KearnySt@SMISRC:/CVS rlog -S -N -d "$date_range1<=$date_range2" ${var} >>$jfong_chg_dir/changes.log  
done


/usr/bin/grep -A 1 -B 2 "bpeters" $jfong_chg_dir/changes.log >$jfong_chg_dir/bpeters.log
if [ $? != 0 ];
then
	blat -subject "Brad's changes for the past 24 hours." \
		-to jfong@birst.com,kfaulkner@birst.com,ricks@birst.com \
		-body "There were no changes from Brad in the build"
else 
	blat -subject "Brad's changes for the past 24 hours." \
		-to jfong@birst.com,kfaulkner@birst.com,ricks@birst.com \
		< $jfong_chg_dir/bpeters.log
fi

## /usr/bin/cvs -d :pserver:birst\\jfong:153KearnySt@SMISRC:/CVS rlog -S -N -d "$date_range1<=$date_range2" Acorn >>$jfong_chg_dir/changes.log
## $CVS_exe -d :pserver:birst\\jfong:153KearnySt@SMISRC:/CVS rlog -S -N -d "$date_range1<=$date_range2" Acorn >>$jfong_chg_dir/changes.log