#!/usr/bin/bash
#
ANT_OPTS="-Xms512m -Xmx512m -XX:MaxPermSize=512m";	export ANT_OPTS

function Parameter_Help
{
echo "usage: deploy [-options]"
			echo "where options include:"
			echo " -c <clean working directory>"
			echo "		This is the clean area we will be exporting "
			echo "		and building from.  Everything will be deleted "
			echo "		from this directory tree before we start"
			echo "		e.g. \c\clean_build_SMI_2_2_0_SP"
			echo " -n <email notification list> comma separated"
			echo " 		default=jfong@birst.com,4157301327@vtext.com"
			echo " -t <build tag> "
			echo " 		Usually, this is the date and time  with or without a "
			echo " 		release number.  For example, 200802140805 "
			echo " 		is Feb 14th, 2008 8:05 a.m.  Or if it for 2.2.0, "
			echo "		the tag would be 200802140805_220"
}
# Define variables
# 	and clear them out first. 
#
Notify=jfong@birst.com
build_tag=""
Branch_Name=""
jfong_clean=""
CVS_workspace=""
JAVA_HOME=/cygdrive/c/Program\ Files/Java/jdk1.6.0_27
#	JAVA_HOME='/cygdrive/c/Program Files/Java/jdk1.7.0'; export JAVA_HOME
echo $JAVA_HOME

while getopts ":c:n:t:" opts; do
        case "$opts" in
		c ) echo "-c $OPTARG"
			jfong_clean="$(cygpath -u -p -a "$OPTARG")"
			echo $jfong_clean
			;;
		n ) echo "-n $OPTARG" 
			Notify=$OPTARG
			export Notify 
			;;
		t ) echo "-t $OPTARG"			# Since I'm creating the build, I am generating the build tag.  
			build_tag=$OPTARG;	
			export build_tag
			;;
		*  ) Parameter_Help 
			exit 1 ;;
	esac
	echo one value $1
done

echo "workspace = " $jfong_clean

if [ -z $build_tag ] || [ -z $jfong_clean ];	#  Make sure we have our parameters
then
	Parameter_Help
	exit 1
fi
	
cd $jfong_clean/ChartRenderer/build
pwd

ant -verbose clean
stat=$?
echo ChartRenderer clean exit status $stat

if [ $stat == 0 ]; 
then
	echo "ChartRenderer ant clean worked"
else
	blat -subject "ChartRenderer: ant clean" -body "ChartRenderer ant clean had errors" -to $Notify
	exit -1
fi


ant -verbose all
stat=$?
echo ChartRenderer build exit status $stat

if [ $stat == 0 ]; 
then
	echo "ChartRenderer ant all worked"
else
	blat -subject "ChartRenderer: ant all" -body "ChartRenderer ant all had errors" -to $Notify
	exit -1
fi

exit 0
