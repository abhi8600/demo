echo on
REM example: 
REM C:\workspace\BuildsAndReleases\builds>customer_package.bat FMR MDS_Phones FMR_20090114 Releases\Platform\2.3.11 > customer_package.log 2>&1

REM
REM predefined variables
REM

set VarNotify=jfong@birst.com
rem 	,4157301327@vtext.com
set VarMailText=Text message as part of the Mail
set VarMailSubject=Customer_Package.bat
set customer=""
set cust_env=""
set tag=""
set temp=""

REM
REM Package up the customer files by release
REM

REM
REM get the input parameters and attempt to verify they work.  
REM


if "%1" neq "" ( set temp=%1 && goto CUST_DEFINED: ) 
goto LIST_PARAM:

:CUST_DEFINED
set customer=%temp: =%
if "%2" neq "" ( set temp=%2 && goto CUST_ENV_DEFINED: ) 
goto LIST_PARAM:

:CUST_ENV_DEFINED
set cust_env=%temp: =%
if "%3" neq "" ( set temp=%3 && goto TAG_DEFINED: ) 
goto LIST_PARAM:

:TAG_DEFINED
set tag=%temp: =%
if "%4" neq "" ( set temp=%4 && goto ALL_PARAM_DEFINED: ) 

:LIST_PARAM
echo "Customer_Package.bat usages is"
echo "Customer_Package.bat %1 %2 %3 %4"
echo "	where %1 = customer name"
echo "	where %2 = customer deploy environment"
echo "	where %3 = CVS Tag of the files to be used"
echo "	where %4 = Release to use (e.g. Releases\Platform\2.3.1 or Nightly\200812310705)"
echo ""
set VarMailText=Not all required parameters were entered.   
set VarMailSubject=Customer_Package:Bad parameters
goto BADEXIT:



:ALL_PARAM_DEFINED
set rel_num=%temp: =%
echo %customer%
echo %cust_env%
echo %tag%
echo %rel_num%

REM
REM figure out the next release number
REM

set clean_dir=C:\clean_builds_2
set clean_old_war=C:\clean_old_war
set cvs_command="-d :pserver:jfong:jfong123@SMISRC:/CVS export -r %tag% SMI_Customers"
set temp=%customer%_%tag%_%cust_env%
set new_label=%temp: =%
set new_rel_dir=%clean_dir%\%new_label%
mkdir %new_rel_dir%
set env_path=%clean_dir%\SMI_Customers\%customer%\Code\Environments\%cust_env%
set base_path=%clean_dir%\SMI_Customers\%customer%\Code\Webapp

echo %tag%
echo %rel_num%
echo %clean_dir%
echo %cvs_command%
echo %target_war%
echo %new_rel_path%

REM 
REM Setup the delete_me directory
REM 

dir %clean_old_war%
set /a Compare_Error_Cnt=%ERRORLEVEL% 
if %Compare_Error_Cnt% EQU 0 ( del /F /Q %clean_old_war%\* 
	) else (
	mkdir %clean_old_war% )
rem 
rem  Use FTP to get the SMIWeb.war file from //smi/builds directory
rem 

cd %clean_old_war%
echo open builds>upload.ftp
echo smibuilds>>upload.ftp
echo 5771builds!>>upload.ftp
echo verbose>>upload.ftp
echo prompt>>upload.ftp
echo cd %rel_num% >>upload.ftp
echo get SMIWeb.war>>upload.ftp
echo delete upload.ftp>>upload.ftp
echo prompt>>upload.ftp
echo quit>>upload.ftp	

ftp -s:upload.ftp  

dir SMIWeb.war
IF %ERRORLEVEL% LSS 1 goto FTP_GET_DONE:
set VarMailText=There were problems FTPing SMIWeb.war over from smibuilds. 
set VarMailSubject=Acorn_build:FTP GET error
goto BADEXIT

:FTP_GET_DONE

cd %clean_dir%

call ant -verbose -file c:\workspace_HEAD\BuildsAndReleases\Builds\customer.xml -Dcustomer=%customer% -Dclean_dir=%clean_dir% -Dclean_old_war=%clean_old_war% -Dcvs_command=%cvs_command% -Denv_path=%env_path% -Dbase_path=%base_path% -Dnew_rel_dir=%new_rel_dir% all

echo on

rem 
rem  FTP build to the //smi/builds directory
rem 

cd %new_rel_dir%
echo open builds>upload.ftp
echo smibuilds>>upload.ftp
echo 5771builds!>>upload.ftp
echo verbose>>upload.ftp
echo prompt>>upload.ftp
echo mkdir Releases/Customers/%customer% >>upload.ftp
echo cd Releases/Customers/%customer% >>upload.ftp
echo mkdir  %new_label%>>upload.ftp
echo cd  %new_label%>>upload.ftp
echo mput *>>upload.ftp
echo delete upload.ftp>>upload.ftp
echo prompt>>upload.ftp
echo quit>>upload.ftp	

ftp -s:upload.ftp  
IF %ERRORLEVEL% LSS 1 goto FTP_DONE:
set VarMailText=There were problems FTPing the files over to smibuilds. 
set VarMailSubject=Acorn_build:FTP error
goto BADEXIT

:FTP_DONE

rem 
rem  FTP rsbuild to the //smi/builds directory
rem 

cd %new_rel_dir%
echo open rsbuilds>upload.ftp
echo smibuilds>>upload.ftp
echo 5771builds!>>upload.ftp
echo verbose>>upload.ftp
echo prompt>>upload.ftp
echo mkdir Releases/Customers/%customer% >>upload.ftp
echo cd Releases/Customers/%customer% >>upload.ftp
echo mkdir  %new_label%>>upload.ftp
echo cd  %new_label%>>upload.ftp
echo mput *>>upload.ftp
echo delete upload.ftp>>upload.ftp
echo prompt>>upload.ftp
echo quit>>upload.ftp	

ftp -s:upload.ftp  
IF %ERRORLEVEL% LSS 1 goto FTP_RSDONE:
set VarMailText=There were problems FTPing the files over to smibuilds. 
set VarMailSubject=Acorn_build:FTP error
goto BADEXIT

:FTP_RSDONE
goto THE_END:


:BADEXIT
echo ------------------ Bad Exit -------------------------------------------
echo %VarMailText%
blat -to %VarNotify% -subject "%VarMailSubject%" -body "%VarMailText%"
rem *** if exist %VarCustProp% del %VarCustProp%
echo "This batch file failed."
exit 1

:THE_END

exit 0
