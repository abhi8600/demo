ECHO ON

set VarNotify=jfong@birst.com
set VarMailText=Text message as part of the Mail
set VarMailSubject=Build Touch
set FTP_files=0

:PROCESS_INPUT

if "%1"=="-n" goto SET_NOTIFICATION:
if "%1"=="-b" goto SET_BRANCH:
if "%1"=="-c" goto SET_CLEAN_DIR:
if "%1"=="-f" goto SET_FLAG:
if "%1"=="-t" goto SET_TIMESTAMP:
if "%1"=="" goto PROCESS_INPUT_DONE:
set temp=%1
if NOT "%temp:~0,1%"=="-" goto ADD_NOTIFICATION:

goto PROCESS_INPUT_ERROR:

:SET_TIMESTAMP
shift
set TimeStamp=%1
goto PROCESS_INPUT_LOOP:

:SET_BRANCH
shift
set Branch_name=%1
goto PROCESS_INPUT_LOOP:

:SET_CLEAN_DIR
shift
set clean_dir=%1
goto PROCESS_INPUT_LOOP:

:SET_FLAG
shift
set FTP_files=%1
echo %FTP_files%
goto PROCESS_INPUT_LOOP:

:SET_NOTIFICATION
set VarNotify=%1
goto PROCESS_INPUT_LOOP:

:ADD_NOTIFICATION
set VarNotify="%VarNotify%,%temp%"
echo %VarNotify%
goto PROCESS_INPUT_LOOP:

:PROCESS_INPUT_LOOP

shift
goto PROCESS_INPUT:

:PROCESS_INPUT_ERROR

echo ERROR: invalid input parameter
echo usage: DataConductorBuild.bat [-options]
echo where options include:
echo.
echo  -t ^<Time Stamp^> (optional)
echo 		Enter the Time Stamp for identifying the deployment
echo		the format is YYYYMMDDHHMM, or one will be generated.		
echo  -b ^<CVS Branch^> 
echo 		enter the cvs branch for the release e.g. HEAD, Birst_3_1_5_SP
echo  -n ^<email notification list^> comma separated (optional)
echo  		default=jfong@birst.com,4157301327@vtext.com
echo 		NOTE - multiple email addresses need to be separated by 
echo 			commas and enclosed in double quote
echo.

set VarMailText=Invalid arguement(s) provided. 
set VarMailSubject=DataConductorBuild:Process Arguements
goto BADEXIT:

:PROCESS_INPUT_DONE

REM
REM We need a timestamp.  If it is not provided, we should create our own.
REM

cd %clean_dir%

echo -%clean_dir%-
for /l %%a in (1,1,31) do if "%clean_dir:~-1%"==" " set clean_dir=%clean_dir:~0,-1%
echo -%clean_dir%-
cd %clean_dir%
dir

REM
REM We need a timestamp.  If it is not provided, we should create our own.
REM

if "%TimeStamp%" neq "" ( goto TIMESTAMP_DEFINED: ) 

for /f "tokens=1-3 delims=:." %%a in ("%time%") do set hh_mm_stamp=%%a%%b
echo %hh_mm_stamp%
for /f "tokens=1-3 delims=\ " %%a in ("%date%") do set date_only=%%b
for /f "tokens=1-3 delims=/." %%a in ("%date_only%") do set teststamp=%%c%%a%%b
echo %teststamp%
set TimeStamp="%teststamp%%hh_mm_stamp%"
	
:TIMESTAMP_DEFINED
echo TimeStamp: %TimeStamp%

echo -%TimeStamp%-
for /l %%a in (1,1,31) do if "%TimeStamp:~-1%"==" " set TimeStamp=%TimeStamp:~0,-1%
echo -%TimeStamp%-

REM set TimeStamp=%TimeStamp:~0,14%
REM echo TimeStamp: -%TimeStamp%-

REM
REM Build Touch
REM

cd %clean_dir%
dir

:BUILD_Touch

cd %clean_dir%\Touch
dir

cd %clean_dir%\Touch\Touch
if exist Touch.csproj goto IIS_SED: 
set VarMailText=Something is wrong, I can't find the Touch.csproj file. \n The CVS update or something else did not work
set VarMailSubject=Touch_build: CVS Touch.csproj file error
goto BADEXIT:

:IIS_SED
echo "Touch.csproj exist"
copy Touch.csproj Touch.temp
type Touch.temp | c:\cygwin\bin\sed.exe "s^<UseIIS>True</UseIIS>^<UseIIS>False</UseIIS>^" >Touch.csproj
del Touch.temp		# we've update the file, we can delete the older version.

cd %clean_dir%\Touch\Touch

devenv Touch.sln /clean
IF %ERRORLEVEL% LSS 1 goto CLEAN_DONE:
set VarMailText=Build had problems cleaing out build area. 
set VarMailSubject=Touch_build:clean errors
goto BADEXIT:

:CLEAN_DONE
devenv Touch.sln /build
IF %ERRORLEVEL% LSS 1 goto BUILD_DONE:
set VarMailText=Build encountered problems.
set VarMailSubject=Touch_build:build problems. 
goto BADEXIT:

:BUILD_DONE

rem 
rem Copy Touch file to Performance Engine.
rem 

copy %clean_dir%\Touch\Touch\bin\Debug\Touch.exe %clean_dir%\SMI\PerformanceEngine\minnesota\bin 

:BUILD_TouchTEST_DONE

REM	set VarMailText=Touch Build was successful. 
REM	set VarMailSubject=Touch_build:Done
REM	blat -to %VarNotify% -subject "%VarMailSubject%" -body "%VarMailText%"

goto THE_END:


:BADEXIT
echo ------------------ Bad Exit -------------------------------------------
echo %VarMailText%
blat -to %VarNotify% -subject "%VarMailSubject%" -body "%VarMailText%"
rem *** if exist %VarCustProp% del %VarCustProp%
echo "This batch file failed."
exit 1

:THE_END

exit 0
