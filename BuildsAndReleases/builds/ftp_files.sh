﻿#! /usr/bin/bash	
#
# FTP files 
#echo on

source /cygdrive/c/Documents\ and\ Settings/jfong.SUCCESSMETRICS/.profile

# source /cygdrive/c/CustomerInstalls/Demos/Southwind/.profile 

function Parameter_Help
{
	echo "usage: deploy [-options]"
	echo "where options include:"
	echo " -b <branch> "
	echo "		Enter the branch that we will be tagging on."
	echo " 		If not branch is supplied, we default to HEAD"
	echo " -h <smi_home> "
	echo "		Enter the home directory of the instance"
	echo " 		e.g./cygdrive/c/CustomerInstalls/RBC/RBCWeb"
	echo " -n <email notification list> comma separated"
	echo " 		default=jfong@birst.com,4157301327@vtext.com"
	echo " -p <port number>"
	echo "		Enter 4 digit port number for the installation"	
	echo " 		Installation will use port number +2"
	echo "		e.g. 9300, 9301, 9302"
	#	echo " -r <release directory> "
	#	echo " 		Which do you want to get the release from?"
	#	echo "		Nightly for nightly builds"
	#	echo "		Releases/Platform for version releases"
	echo " -t <build tag> "
	echo " 		If you are getting files from Releases/Platform, enter a Release Number e.g. 2.0.0"
	echo "		If you are getting files from Nightly, enter a date and time e.g. 200705230130"
}

nightly_bin=/cygdrive/c/workspace_HEAD/BuildsAndReleases/builds; 		export nightly_bin
jfong_share=/cygdrive/c/shared/Builds;				export jfong_share
jfong_share_win="$(cygpath -w -p -a "$jfong_share")"				export jfong_share_win
jfong_clean="";								export jfong_clean
Notify="jfong@birst.com"; 		export Notify		#email list
declare -i error_count=0
Builds_Dir="$(cygpath -u '\\repo_dev\dev\Builds')"

#--------------------------------------------------------------------------------------------------------------------------
ANT_OPTS="-Xms768m -Xmx1024m -XX:MaxPermSize=768m";				export ANT_OPTS
DEBUG=0;					export DEBUG			#debug=1, not debug=0
FTP_files=0;					export FTP_files
SMI_HOME="";					export SMI_HOME
Https_Port=""; 					export Https_Port
Release_Dir="";					export Release_Dir
Period_Id="";					export Period_Id
build_tag="";					export build_tag
SMI_Branch="";					export SMI_Branch
SMI_workspace=""				export SMI_workspace
jfong_clean=""					export jfong_clean
env
#  Notify=jfong@birst.com;		export Notify
echo done clearing stuff

while getopts ":d:h:b:n:p:r:t:f:" opts; do
        case "$opts" in
		d ) echo "-d $OPTARG"
			DEBUG=$OPTARG
		 	export DEBUG
			;;
		b ) echo "-b $OPTARG" 
			SMI_Branch=$OPTARG
			export SMI_Branch
			echo $SMI_Branch
			;;
		f ) echo "-f $OPTARG" 
			FTP_files=0				# we do not need to ftp files to remote servers anymore.
			export FTP_files
			echo $FTP_files
			;;
		h ) echo "-h $OPTARG" 
			SMI_HOME=$OPTARG
			export SMI_HOME				# expect SMI_HOME to be in the following format
			Cust_Type=${SMI_HOME##*/}		# 	/cygdrive/c/CustomerInstalls/RBC/RBCWeb
			Customer_Home=${SMI_HOME%/*}		# so    ----------------------------
			export Customer_Home			#            Install Root            ---
			Customer_Name=${Customer_Home##*/}	#                          Customer Name ------
			export Customer_Name			#                                        Customer's installation type
			Cust_Root=${Customer_Home%/*}		#SMI_HOME=$Install_Root+$Customer_Name+Cust_Type
			export Cust_Root
			Install_Root=${Customer_Home%/*}
			export Install_Root

			echo $Install_Root
			echo $Customer_Home
			echo $Customer_Name
			echo $Cust_Type
			;;
		n ) echo "-n $OPTARG" 
			Notify="$Notify,$OPTARG"
			export Notify 
			;;
		p ) echo "-p $OPTARG"
			echo $OPTARG | grep [^0-9] >/dev/null 2>&1
			if [ "$?"  -eq "0" ]; then
				echo $OPTARG "is not a number.  The Port Number needs to be numeric"  
				exit 1
			else
				Https_Port=$OPTARG;  export Https_Port 
			fi
			;;
		r ) echo "-r $OPTARG" 
			if [ $OPTARG == "Nightly" ] || [ $OPTARG == "Releases/Platform" ]; 
				then 
				Release_Dir=$OPTARG
				export Release_Dir
			else
				echo "Release Build directory, $OPTARG, does not exist, please fix"
				exit 1
			fi	
			;;
		t ) echo "-t $OPTARG"			# Since I'm creating the build, I am generating the build tag.  
			build_tag=$OPTARG;	
			export build_tag
			;;
		*  ) Parameter_Help
			exit 1 ;;
	esac
	echo one value $1
done

#
# figure out which branch we are using.
#
jfong_clean="/cygdrive/c/clean_builds_"$SMI_Branch

if [ -z $SMI_Branch ] || [ $SMI_Branch = "HEAD" ];  #  Make sure we have our parameters
then
	SMI_workspace="/cygdrive/c/workspace_HEAD"
else
	SMI_workspace="/cygdrive/c/workspace_"$SMI_Branch
fi
echo "clean     is " $jfong_clean
echo "workspace is " $SMI_workspace

if [ -z $SMI_HOME ] || [ -z $Https_Port ]  || [ -z $build_tag ]; 	#	|| [ -z $Release_Dir ] || [ -z $Period_Id ]; 
then
	echo "One or more of the required parameters was not provided."
	echo "Please provide all the required paramaters:" 
	Parameter_Help
	exit 1 
fi 


#
# Copy everything over to the \\smi\Builds subdirectory system
#

if [ $DEBUG -eq 0 ]; then
	echo "copy over the files to the shared \\repo_dev\dev\Builds"
	
	if [ -e "$jfong_clean"/upload.ftp ];
	then
		rm -rf  "$jfong_clean"/upload.ftp
	fi 
	
	
	cd $jfong_clean
	
	cp -R -v $jfong_share/$build_tag  $Builds_Dir/Nightly
	if [ $? != 0 ]; 
	then
		blat -subject "ftp_files: Unable to copy the files to Repo_dev" -body "Exiting since Copy was not successful in transfering the zipped up files.  " -to $Notify
		exit -1
	fi
	
fi

#	blat -subject "FTP_files: completed without errors." -body "Build $build_tag finished without errors"  -to $Notify

exit 
