ECHO ON

set VarNotify=jfong@birst.com
set VarMailText=Text message as part of the Mail
set VarMailSubject=Movespace
set FTP_files=0

:PROCESS_INPUT

if "%1"=="-n" goto SET_NOTIFICATION:
if "%1"=="-b" goto SET_BRANCH:
if "%1"=="-c" goto SET_CLEAN_DIR:
if "%1"=="-f" goto SET_FLAG:
if "%1"=="-t" goto SET_TIMESTAMP:
if "%1"=="" goto PROCESS_INPUT_DONE:
set temp=%1
if NOT "%temp:~0,1%"=="-" goto ADD_NOTIFICATION:

goto PROCESS_INPUT_ERROR:

:SET_TIMESTAMP
shift
set TimeStamp=%1
goto PROCESS_INPUT_LOOP:

:SET_BRANCH
shift
set Branch_name=%1
goto PROCESS_INPUT_LOOP:

:SET_CLEAN_DIR
shift
set clean_dir=%1
goto PROCESS_INPUT_LOOP:

:SET_FLAG
shift
set FTP_files=%1
echo %FTP_files%
goto PROCESS_INPUT_LOOP:

:SET_NOTIFICATION
shift
set VarNotify=%1
goto PROCESS_INPUT_LOOP:

:ADD_NOTIFICATION
set VarNotify="%VarNotify%,%temp%"
echo %VarNotify%
goto PROCESS_INPUT_LOOP:

:PROCESS_INPUT_LOOP

shift
goto PROCESS_INPUT:

:PROCESS_INPUT_ERROR

echo ERROR: invalid input parameter
echo usage: MoveSpaceBuild.bat [-options]
echo where options include:
echo.
echo  -t ^<Time Stamp^> (optional)
echo 		Enter the Time Stamp for identifying the deployment
echo		the format is YYYYMMDDHHMM, or one will be generated.		
echo  -b ^<CVS Branch^> 
echo 		enter the cvs branch for the release e.g. HEAD, Birst_3_1_5_SP
echo  -n ^<email notification list^> comma separated (optional)
echo  		default=jfong@birst.com,4157301327@vtext.com
echo 		NOTE - multiple email addresses need to be separated by 
echo 			commas and enclosed in double quote
echo.

set VarMailText=Invalid arguement(s) provided. 
set VarMailSubject=MoveSpaceBuild:Process Arguements
goto BADEXIT

:PROCESS_INPUT_DONE

REM
REM We need a timestamp.  If it is not provided, we should create our own.
REM

cd %clean_dir%

echo -%clean_dir%-
for /l %%a in (1,1,31) do if "%clean_dir:~-1%"==" " set clean_dir=%clean_dir:~0,-1%
echo -%clean_dir%-
cd %clean_dir%
dir

REM
REM We need a timestamp.  If it is not provided, we should create our own.
REM

if "%TimeStamp%" neq "" ( goto TIMESTAMP_DEFINED: ) 

for /f "tokens=1-3 delims=:." %%a in ("%time%") do set hh_mm_stamp=%%a%%b
echo %hh_mm_stamp%
for /f "tokens=1-3 delims=\ " %%a in ("%date%") do set date_only=%%b
for /f "tokens=1-3 delims=/." %%a in ("%date_only%") do set teststamp=%%c%%a%%b
echo %teststamp%
set TimeStamp="%teststamp%%hh_mm_stamp%"
	
:TIMESTAMP_DEFINED
echo TimeStamp: %TimeStamp%

echo -%TimeStamp%-
for /l %%a in (1,1,31) do if "%TimeStamp:~-1%"==" " set TimeStamp=%TimeStamp:~0,-1%
echo -%TimeStamp%-

REM set TimeStamp=%TimeStamp:~0,14%
REM echo TimeStamp: -%TimeStamp%-


REM
REM Build Acorn
REM

cd %clean_dir%
dir

:BUILD_ACORN

cd %clean_dir%\MoveSpace
dir

cd %clean_dir%\MoveSpace

devenv MoveSpace.sln /clean
IF %ERRORLEVEL% LSS 1 goto CLEAN_DONE:
set VarMailText=Build had problems cleaing out build area. 
set VarMailSubject=MoveSpace_build:clean errors
goto BADEXIT

:CLEAN_DONE
devenv MoveSpace.sln /build
IF %ERRORLEVEL% LSS 1 goto BUILD_DONE:
set VarMailText=Build encountered problems.
set VarMailSubject=MoveSpace_build:build problems. 
goto BADEXIT

:BUILD_DONE

rem 
rem create the deploy package
rem

rem	mkdir "C:\shared\Builds\%TimeStamp%"
rem 	IF %ERRORLEVEL% LSS 1 goto COPY_MOVESPACE:
rem 	set VarMailText=Could not build the "C:\shared\Builds\%TimeStamp%" directory. 
rem	set VarMailSubject=MoveSpace_build:mkdir zip directory
rem	goto BADEXIT

:COPY_MOVESPACE

cd %clean_dir%\MoveSpace\MoveSpace\bin\Release
IF %ERRORLEVEL% EQU 0 goto START_COPY:

cd %clean_dir%\MoveSpace\MoveSpace\bin\Debug
IF %ERRORLEVEL% EQU 0 goto START_COPY:
set VarMailText=Could not find files under bin "..\MoveSpace\MoveSpace\bin" directory. 
set VarMailSubject=MoveSpace_build:No files to zip
goto BADEXIT

:START_COPY
"c:\Program Files (x86)\WinZip\WZZIP.EXE" -ex -r -p -a C:\shared\Builds\%TimeStamp%\MoveSpace.zip *.*
IF %ERRORLEVEL% LSS 1 goto FTP_DONE:
set VarMailText=Could not zip the "C:\shared\Builds\%TimeStamp%\MoveSpace" directory. 
set VarMailSubject=MoveSpace_build:zip directory
goto BADEXIT

rem 
rem Now that we have the package, FTP the package up to colo1
rem 

rem 
rem  FTP build to the //smi/builds directory
rem 

cd %clean_dir%
echo open builds>upload.ftp
echo smibuilds>>upload.ftp
echo 5771builds!>>upload.ftp
echo verbose>>upload.ftp
echo prompt>>upload.ftp
echo lcd C:\shared\Builds\%TimeStamp%>>upload.ftp
echo cd Nightly>>upload.ftp
echo mkdir  %TimeStamp%>>upload.ftp
echo cd  %TimeStamp%>>upload.ftp
echo mput *>>upload.ftp
echo delete upload.ftp>>upload.ftp
echo prompt>>upload.ftp
echo quit>>upload.ftp	

ftp -s:upload.ftp  
IF %ERRORLEVEL% LSS 1 goto FTP_COLO1:
set VarMailText=There were problems FTPing the files over to smibuilds. 
set VarMailSubject=MoveSpace_build:FTP error
goto BADEXIT


:FTP_COLO1
REM goto FTP_FTP1:
if %FTP_files%==0 goto BYPASS_FTP
cd %clean_dir%
echo open 192.168.1.161>upload.ftp
echo smibuilds>>upload.ftp
echo 5771builds!>>upload.ftp
echo verbose>>upload.ftp
echo prompt>>upload.ftp
echo lcd C:\shared\Builds\%TimeStamp%>>upload.ftp
echo cd Nightly>>upload.ftp
echo mkdir  %TimeStamp%>>upload.ftp
echo cd  %TimeStamp%>>upload.ftp
echo mput *>>upload.ftp
echo delete upload.ftp>>upload.ftp
echo prompt>>upload.ftp
echo quit>>upload.ftp	

ftp -s:upload.ftp  
IF %ERRORLEVEL% LSS 1 goto FTP_FTP1:
set VarMailText=There were problems FTPing the files over to colo1. 
set VarMailSubject=MoveSpace_build:FTP error
goto BADEXIT

:FTP_FTP1:
if %FTP_files%==0 goto BYPASS_FTP
cd %clean_dir%
echo open rsbuilds>upload.ftp
echo smibuilds>>upload.ftp
echo 5771builds!>>upload.ftp
echo verbose>>upload.ftp
echo prompt>>upload.ftp
echo lcd C:\shared\Builds\%TimeStamp%>>upload.ftp
echo cd Nightly>>upload.ftp
echo mkdir  %TimeStamp%>>upload.ftp
echo cd  %TimeStamp%>>upload.ftp
echo mput *>>upload.ftp
echo delete upload.ftp>>upload.ftp
echo prompt>>upload.ftp
echo quit>>upload.ftp	

ftp -s:upload.ftp  
IF %ERRORLEVEL% LSS 1 goto FTP_FTP2:
set VarMailText=There were problems FTPing the files over to FTP1. 
set VarMailSubject=MoveSpace_build:FTP error
goto BADEXIT

:FTP_FTP2:
if %FTP_files%==0 goto BYPASS_FTP
cd %clean_dir%
echo open 172.26.140.148>upload.ftp
echo smibuilds>>upload.ftp
echo 5771builds!>>upload.ftp
echo verbose>>upload.ftp
echo prompt>>upload.ftp
echo lcd C:\shared\Builds\%TimeStamp%>>upload.ftp
echo cd Nightly>>upload.ftp
echo mkdir  %TimeStamp%>>upload.ftp
echo cd  %TimeStamp%>>upload.ftp
echo mput *>>upload.ftp
echo delete upload.ftp>>upload.ftp
echo prompt>>upload.ftp
echo quit>>upload.ftp	

ftp -s:upload.ftp  
IF %ERRORLEVEL% LSS 1 goto FTP_DONE:
set VarMailText=There were problems FTPing the files over to FTP1. 
set VarMailSubject=Movespace:FTP error
goto BADEXIT

:FTP_DONE
:BYPASS_FTP

cd %clean_dir%
dir

set VarMailText=MoveSpace build was successful. 
set VarMailSubject=MoveSpace_build:Done
blat -to %VarNotify% -subject "%VarMailSubject%" -body "%VarMailText%" -server 192.168.85.11:25 -f jfong@birst.com

goto THE_END:


:BADEXIT
echo ------------------ Bad Exit -------------------------------------------
echo %VarMailText%
blat -to %VarNotify% -subject "%VarMailSubject%" -body "%VarMailText%"
rem *** if exist %VarCustProp% del %VarCustProp%
echo "This batch file failed."
exit 1

:THE_END

exit 0
