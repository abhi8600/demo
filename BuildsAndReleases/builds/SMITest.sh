#! /usr/bin/bash	
#
# Nightly Build and Test
#echo on

source /cygdrive/c/Documents\ and\ Settings/jfong.SUCCESSMETRICS/.profile

# source /cygdrive/c/CustomerInstalls/Demos/Southwind/.profile 

function Parameter_Help
{
	echo "usage: deploy [-options]"
	echo "where options include:"
	echo " -b <branch> "
	echo "		Enter the branch that we will be tagging on."
	echo " 		If not branch is supplied, we default to HEAD"
	echo " -h <smi_home> "
	echo "		Enter the home directory of the instance"
	echo " 		e.g./cygdrive/c/CustomerInstalls/RBC/RBCWeb"
	echo " -n <email notification list> comma separated"
	echo " 		default=jfong@birst.com,4157301327@vtext.com"
	echo " -p <port number>"
	echo "		Enter 4 digit port number for the installation"	
	echo " 		Installation will use port number +2"
	echo "		e.g. 9300, 9301, 9302"
	#	echo " -r <release directory> "
	#	echo " 		Which do you want to get the release from?"
	#	echo "		Nightly for nightly builds"
	#	echo "		Releases/Platform for version releases"
	echo " -t <build tag> "
	echo " 		If you are getting files from Releases/Platform, enter a Release Number e.g. 2.0.0"
	echo "		If you are getting files from Nightly, enter a date and time e.g. 200705230130"
}

nightly_bin=/cygdrive/c/workspace_HEAD/BuildsAndReleases/builds; 		export nightly_bin
jfong_share=/cygdrive/c/shared/Builds;				export jfong_share
jfong_clean="";								export jfong_clean
Notify="jfong@birst.com"; 		export Notify		#email list
declare -i error_count=0

#--------------------------------------------------------------------------------------------------------------------------
ANT_OPTS="-Xms768m -Xmx1024m -XX:MaxPermSize=768m";				export ANT_OPTS
DEBUG=0;					export DEBUG			#debug=1, not debug=0
FTP_files=0;					export FTP_files
SMI_HOME="";					export SMI_HOME
Https_Port=""; 					export Https_Port
Release_Dir="";					export Release_Dir
Period_Id="";					export Period_Id
build_tag="";					export build_tag
SMI_Branch="";					export SMI_Branch
SMI_workspace=""				export SMI_workspace
jfong_clean=""					export jfong_clean
env
#  Notify=jfong@birst.com;		export Notify
echo done clearing stuff

while getopts ":d:h:b:n:p:r:t:f:" opts; do
        case "$opts" in
		d ) echo "-d $OPTARG"
			DEBUG=$OPTARG
		 	export DEBUG
			;;
		b ) echo "-b $OPTARG" 
			SMI_Branch=$OPTARG
			export SMI_Branch
			echo $SMI_Branch
			;;
		f ) echo "-f $OPTARG" 
			FTP_files=$OPTARG
			export FTP_files
			echo $FTP_files
			;;
		h ) echo "-h $OPTARG" 
			SMI_HOME=$OPTARG
			export SMI_HOME				# expect SMI_HOME to be in the following format
			Cust_Type=${SMI_HOME##*/}		# 	/cygdrive/c/CustomerInstalls/RBC/RBCWeb
			Customer_Home=${SMI_HOME%/*}		# so    ----------------------------
			export Customer_Home			#            Install Root            ---
			Customer_Name=${Customer_Home##*/}	#                          Customer Name ------
			export Customer_Name			#                                        Customer's installation type
			Cust_Root=${Customer_Home%/*}		#SMI_HOME=$Install_Root+$Customer_Name+Cust_Type
			export Cust_Root
			Install_Root=${Customer_Home%/*}
			export Install_Root

			echo $Install_Root
			echo $Customer_Home
			echo $Customer_Name
			echo $Cust_Type
			;;
		n ) echo "-n $OPTARG" 
			Notify="$Notify,$OPTARG"
			export Notify 
			;;
		p ) echo "-p $OPTARG"
			echo $OPTARG | grep [^0-9] >/dev/null 2>&1
			if [ "$?"  -eq "0" ]; then
				echo $OPTARG "is not a number.  The Port Number needs to be numeric"  
				exit 1
			else
				Https_Port=$OPTARG;  export Https_Port 
			fi
			;;
		r ) echo "-r $OPTARG" 
			if [ $OPTARG == "Nightly" ] || [ $OPTARG == "Releases/Platform" ]; 
				then 
				Release_Dir=$OPTARG
				export Release_Dir
			else
				echo "Release Build directory, $OPTARG, does not exist, please fix"
				exit 1
			fi	
			;;
		t ) echo "-t $OPTARG"			# Since I'm creating the build, I am generating the build tag.  
			build_tag=$OPTARG;	
			export build_tag
			;;
		*  ) Parameter_Help
			exit 1 ;;
	esac
	echo one value $1
done

echo Is build tag defined? $build_tag
if [ -z $build_tag ]; then
	build_tag=`date +%Y%m%d%H%M`
	export build_tag
fi
date_range1=`date --date=yesterday +%Y-%m-%d\ %H\:%M`;	export date_range1
date_range2=`date +%Y-%m-%d\ %H\:%M`;			export date_range2

#
# figure out which branch we are using.
#
jfong_clean="/cygdrive/c/clean_builds_"$SMI_Branch

if [ -z $SMI_Branch ] || [ $SMI_Branch = "HEAD" ];  #  Make sure we have our parameters
then
	SMI_workspace="/cygdrive/c/workspace_HEAD"
else
	SMI_workspace="/cygdrive/c/workspace_"$SMI_Branch
fi
echo "clean     is " $jfong_clean
echo "workspace is " $SMI_workspace

if [ -z $SMI_HOME ] || [ -z $Https_Port ]  || [ -z $build_tag ]; 	#	|| [ -z $Release_Dir ] || [ -z $Period_Id ]; 
then
	echo "One or more of the required parameters was not provided."
	echo "Please provide all the required paramaters:" 
	Parameter_Help
	exit 1 
fi 

#-----------------------------------------------------------------------
CATALINA_HOME=$SMI_HOME/tomcat;				export CATALINA_HOME
CATALINA_BASE=$SMI_HOME/tomcat;				export CATALINA_BASE

JAVA_HOME=`cygpath -w "c:\Program Files\Java\jdk1.6.0_10"` ;		export JAVA_HOME

echo $DEBUG
echo $SMI_HOME 
echo $build_tag
echo $JAVA_HOME
echo $CATALINA_BASE
echo $CATALINA_HOME
echo "who are you?"
id
echo ""
echo Date range is $date_range1 and $date_range2

	
#
# Let us test our creation
#
# First, test the build.
# deploy_platform and deploy_customer requires the following paramaters:
#	 -h <smi_home>
#                Enter the home directory of the instance
#                e.g./cygdrive/c/CustomerInstalls/RBC/RBCWeb
#	 -p <port number>
#                Enter 4 digit port number for the installation
#                Installation will use port number +2
#                e.g. 9300, 9301, 9302
#	 -r <release directory>
#                Which do you want to get the release from?
#                Nightly for nightly builds
#                Releases/Platform for version releases
#	 -t <build tag>
#                If you are getting files from Releases/Platform, enter a release Number e.g. 2.0.0
#                If you are getting files from Nightly, enter a date and time e.g. 200705230130
#

/usr/bin/bash -x /cygdrive/c/workspace_HEAD/BuildsAndReleases/deployment/deploy_platform.sh \
	-d $DEBUG \
	-e dev \
	-h $SMI_HOME \
	-p $Https_Port \
	-r Nightly \
	-t $build_tag \
	1>/cygdrive/c/shared/Builds/Logs/deployment_$build_tag.log 2>&1

#
# install the customer files. If it exists.  Commented out for now since we are
# only dealing with Northwind.
#
if [ $? == 0 ];
then
	echo "Deploy was suceessful"
else
	blat -subject "NightlyBuild: Problems deploying the build." \
	-body "NightlyBuild.sh had problems deploying the Nightly Build." \
	-to $Notify
fi

#
# Then, run the basic qa queries
#
# move the files over to the test area since it isn't part of any release or package
#
cd $SMI_HOME/bin
if [ $SMI_Branch = "HEAD" ];  #  Make sure we have our parameters
then
	cp $SMI_HOME/../../Demos/smi_bin_save/4.0/qaresults-good.txt	$SMI_HOME/bin
	cp $SMI_HOME/../../Demos/smi_bin_save/4.0/testqueries.txt	$SMI_HOME/bin
	# cp $SMI_HOME/../../Demos/smi_bin_save/4.0/repositoryQA.xml	$SMI_HOME/bin
	cp $SMI_HOME/../../Demos/smi_bin_save/4.0/run_dev.sh		$SMI_HOME/bin
	cp $SMI_HOME/../../Demos/smi_bin_save/4.0/runqaqueries.sh	$SMI_HOME/bin
	cp $SMI_HOME/../../Demos/smi_bin_save/4.0/fmr_smi.txt		$SMI_HOME/bin
	cp $SMI_HOME/../../Demos/smi_bin_save/4.0/fmr_model-good.txt	$SMI_HOME/bin
	cp $SMI_HOME/../../Demos/smi_bin_save/4.0/repository_fmr.xml	$SMI_HOME/bin
	cp $SMI_HOME/../../Demos/smi_bin_save/4.0/runfmrmodel.sh	$SMI_HOME/bin
else
	cp $SMI_HOME/../../Demos/smi_bin_save/3.2/qaresults-good.txt	$SMI_HOME/bin
	cp $SMI_HOME/../../Demos/smi_bin_save/3.2/testqueries.txt	$SMI_HOME/bin
	# cp $SMI_HOME/../../Demos/smi_bin_save/3.2/repositoryQA.xml	$SMI_HOME/bin
	cp $SMI_HOME/../../Demos/smi_bin_save/3.2/run_dev.sh		$SMI_HOME/bin
	cp $SMI_HOME/../../Demos/smi_bin_save/3.2/runqaqueries.sh	$SMI_HOME/bin
	cp $SMI_HOME/../../Demos/smi_bin_save/3.2/fmr_smi.txt		$SMI_HOME/bin
	cp $SMI_HOME/../../Demos/smi_bin_save/3.2/fmr_model-good.txt	$SMI_HOME/bin
	cp $SMI_HOME/../../Demos/smi_bin_save/3.2/repository_fmr.xml	$SMI_HOME/bin
	cp $SMI_HOME/../../Demos/smi_bin_save/3.2/runfmrmodel.sh	$SMI_HOME/bin
fi

pwd 
ls -la
echo "I'm done with my copy"

#
# clean out my cache
#
rm -rf /cygdrive/c/cache/*
rm -rf $SMI_HOME/cache/*

#
# run the queries
#

./runqaqueries.sh 
if [ $? == 0 ];
then
        echo "QA Queries ran successfully"
else
	blat -subject "NightlyBuild: QA Queries did not run correctly." \
        -body "QA Queries did not run successfully." \
	-to $Notify
        exit -1
fi 

# do a diff to see if it worked
# but strip out the first 2 lines that has the version and build number
# pwd
# sed -e 1,2d ./qaresults.txt >./cleanresults.txt
# diff ./cleanresults.txt ./qaresults-good.txt

diff ./qaresults.txt ./qaresults-good.txt
if [ $? == 0 ];
then
        echo "QA Queries worked"
else
        blat -subject "NightlyBuild: QA Queries had problems." -body "QA Queries had problems." -to $Notify
        # exit -1
fi 

	#
	# run the model
	#
	#	rm -rf $SMI_HOME/Fidelity/*		# clean out old modeling files. 
	#
	#	./runfmrmodel.sh 
	#	if [ $? == 0 ];
	#	then
	#		echo "FMR model  ran successfully"
	#	else
	#		blat -subject "NightlyBuild: FMR Model did not run correctly." -body "FMR model did not run successfully." -to $Notify
	#		exit -1
	#	fi 
	#
	#	# do a diff to see if it worked
	#	# but strip out the first 2 lines that has the version and build number
	#	# pwd
	#	# sed -e 1,2d ./qaresults.txt >./cleanresults.txt
	#	# diff ./cleanresults.txt ./qaresults-good.txt
	#
	#	diff ./fmr_model.txt ./fmr_model-good.txt
	#	if [ $? == 0 ];
	#	then
	#		echo "FMR modeling worked"
	#	else
	#		blat -subject "NightlyBuild: FMR modeling had problems." -body "FMR modeling had problems." -to $Notify
	#		exit -1
	#	fi 
	#
#
# Check if Brad made changes without a bug number.
#

#grep "bpeters" $jfong_share/$build_tag/changes.log >./bpeters.log
#if [ $? != 0 ];
#then
#	echo "There were no changes from Brad in the build"
#else 
#	blat -subject "NightlyBuild: Brad made changes last night" jfong@birst.com <./bpeters.log
#fi
#
blat -subject "NightlyBuild: completed without errors." -body "Build $build_tag finished without errors" -to $Notify

exit 
#################################################

List to todos

- attempt to persist the aggregate (temp) tables and delete them afterwards

- create aggegates

- seed the cache

- Break up sections to sub problems so I can run them separately

- parameterize the environment variables so we can change environments (build from a tag or branch)

- cleanup startup and shutdown scripts to generate errors

- generate a list of diffs.
	cvs -d :pserver:jfong:jfong0p;@SMISRC:/CVS log -N -S -rSMI_Nightly_200703160128:SMI_Nightly_20070315012801
	
- generate a list of differences between tags
	$ /cygdrive/c/Program\ Files/CVSNT/cvs -d :pserver:jfong:jfong123@SMISRC:/CVS diff -r Root_SMI_1_5_0_SP  -r SMI_1_5_1 ./SMI_1_5/  >cvs_diff2.log
