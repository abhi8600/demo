﻿#!/usr/bin/bash

whoami


PATH=/usr/local/bin:/usr/bin:$PATH
 
JAVA_HOME=/cygdrive/c/Program\ Files/Java/jdk1.6.0_27
export JAVA_HOME
/usr/bin/env 

function Parameter_Help
{
echo "usage: wrapper_*.sh [-options]"
			echo "where options include:"
			echo " -n <email notification list> comma separated"
			echo " 		default=jfong@birst.com,4157301327@vtext.com"
			echo " -z <password> "
			echo " 		CVS password for exporting code "
			echo ""
}

cd ~; source ./.profile
cd /cygdrive/c/CustomerInstalls/NW/NWTest; source ./.profile

echo "RunningTime Start Build `date`"

echo on
alias autoexec=autoexec.bat
timestamp=`/usr/bin/date +%Y%m%d%H%M`;		export timestamp
#	timestamp=`date +%Y%m%d%H%M`;			export timestamp
echo $timestamp

env

Notify_list="jfong@birst.com"
Notify_wrapper_list="jfong@birst.com"
#	,ricks@birst.com
#	Notify_list="ricks@birst.com"
export Notify_list
export Notify_wrapper_list
echo $Notify_list
Current_Build_Number="$(cygpath -u -p -a '\\repo_dev\Dev\Builds\Current_Build_Number')"
Working_Branch=master
Git_workspace=c:\\workspace_git_master\\Birst
Git_unix_workspace=/cygdrive/c/workspace_git_master/Birst
cvs_password=""
jfong_clean="$(cygpath -u -p -a 'c:\clean_builds')"
jfong_win_clean="c:\\clean_builds"
jfong_share=/cygdrive/c/shared/Builds;
jfong_win_share=c:\\shared\\Builds;
WinZip_Home="$(cygpath -u -p -a 'C:\Program Files (x86)\WinZip')"

while getopts ":n:z:" opts; do
        case "$opts" in
		n ) echo "-n $OPTARG" 
			Notify=$OPTARG
			export Notify 
			;;
		*  ) Parameter_Help 
			exit 1 ;;
	esac
	echo one value $1
done

which blat
blat -profile 
ping 192.168.85.11

blat -superdebugT -s "Wrapper_master : Daily build start" -body "Started building the $timestamp build" -to "$Notify_wrapper_list"

echo $jfong_clean
mkdir $jfong_share/$timestamp

#
# Switch to the right branch
#
which git 

cd $Git_unix_workspace
#	git checkout $Working_Branch

if [ $? == 0 ]; 
then
	echo "Git checkout was successful"  
else
	blat -subject "Wrapper_master : Build ended with Git Checkout error" \
	-body "Git had problems switching to the requested Branch" \
	-to $Notify_wrapper_list
	exit -1
fi

echo "RunningTime Start CVS `date`"

/usr/bin/bash -x $Git_unix_workspace/BuildsAndReleases/builds/Git_sync.sh -k 0 -b $Working_Branch -c /cygdrive/c/clean_builds/$Working_Branch -t "$timestamp" -w $Git_unix_workspace > $jfong_share/Logs/Git_Sync_$timestamp.log 2>&1
if [ $? == 0 ]; 
then
	echo "Git_sync was successful"  
else
	blat -subject "Wrapper_master : Build ended with Git error" \
	-body "CVS had problems with tagging or exporting " \
	-to $Notify_wrapper_list
	exit -1
fi

echo "RunningTime Stop CVS `date`"

cd /cygdrive/c/clean_builds/$Working_Branch
chmod -R 755 *

#
# Build MemDB 
#

echo "RunningTime Start MemDB `date`"

/usr/bin/bash -x  $Git_unix_workspace/BuildsAndReleases/builds/MemDBBuild.sh  -c /cygdrive/c/clean_builds/$Working_Branch -t "$timestamp" -n "$Notify_list" 1>$jfong_share/Logs/MemDBBuild_$timestamp.log 2>&1

if [ $? == 0 ]; 
then
	echo "MemDB was successful"  
else
	blat -subject "Wrapper_master : Build ended with MemDB error" \
	-body "MemDB had problems with building " \
	-to $Notify_wrapper_list
	exit -1
fi

echo "RunningTime Stop MemDB `date`"

#
# Build FlexAdhoc 
#
echo $jfong_clean
echo "RunningTime Start FlexAdhoc `date`"

/usr/bin/bash -x  $Git_unix_workspace/BuildsAndReleases/builds/FlexAdhocBuild.sh  -c /cygdrive/c/clean_builds/$Working_Branch -t "$timestamp" -n "$Notify_list" 1>$jfong_share/Logs/FlexAdhoc_$timestamp.log 2>&1

if [ $? == 0 ]; 
then
	echo "FlexAdhoc was successful"  
else
	blat -subject "Wrapper_master : Build ended with FlexAdhoc error" \
	-body "FlexAdhoc had problems with building " \
	-to $Notify_wrapper_list
	exit -1
fi
echo $JAVA_HOME

echo "RunningTime Stop FlexAdhoc `date`"

#
# Build DashboardApp 
#

echo "RunningTime Start DashboardApp `date`"

/usr/bin/bash -x  $Git_unix_workspace/BuildsAndReleases/builds/DashboardAppBuild.sh -c /cygdrive/c/clean_builds/$Working_Branch -t "$timestamp" -n "$Notify_list" > "$jfong_share/Logs/DashboardApp_$timestamp.log" 2>&1

if [ $? == 0 ]; 
then
	echo "DashboardApp was successful"  
else
	blat -subject "Wrapper_master : Build ended with DashboardApp error" \
	-body "DashboardApp had problems with building " \
	-to $Notify_wrapper_list
	exit -1
fi

echo "RunningTime Stop DashboardApp `date`"
#
# Build ChartRenderer 
#
echo $jfong_clean
echo "RunningTime Start ChartRenderer `date`"

/usr/bin/bash -x  $Git_unix_workspace/BuildsAndReleases/builds/ChartRendererBuild.sh  -c /cygdrive/c/clean_builds/$Working_Branch -t "$timestamp" -n "$Notify_list" 1>$jfong_share/Logs/ChartRenderer_$timestamp.log 2>&1

if [ $? == 0 ]; 
then
	echo "ChartRenderer was successful"  
else
	blat -subject "Wrapper_master : Build ended with ChartRenderer error" \
	-body "ChartRenderer had problems with building " \
	-to $Notify_wrapper_list
	exit -1
fi

echo "RunningTime Stop ChartRenderer `date`"

#
# Build Touch 
#

echo "RunningTime Start Touch `date`"

cmd /c "$Git_workspace\BuildsAndReleases\builds\TouchBuild.bat" -c $jfong_win_clean\\$Working_Branch -b $Working_Branch -t "$timestamp" -n "$Notify_list" > "$jfong_win_share\Logs\Touch_$timestamp.log" 2>&1

if [ $? == 0 ]; 
then
	echo "Touch was successful"  
else
	blat -subject "Wrapper_master : Build ended with Touch error" \
	-body "Touch had problems with building " \
	-to $Notify_wrapper_list
	exit -1
fi

echo "RunningTime Stop Touch `date`"

#
# Build DataConductor 
#

echo "RunningTime Start DataConductor_jar `date`"

/usr/bin/bash -x $Git_unix_workspace/BuildsAndReleases/builds/DataConductorBuild_jar.sh -c /cygdrive/c/clean_builds/$Working_Branch -b $Working_Branch -t "$timestamp" -n "$Notify_list" > "$jfong_share/Logs/DataConductor_jar_$timestamp.log" 2>&1

if [ $? == 0 ]; 
then
	echo "DataConductor was successful"  
else
	blat -subject "Wrapper_master : Build ended with DataConductor error" \
	-body "DataConductor had problems with building " \
	-to $Notify_wrapper_list
	exit -1
fi

echo "RunningTime Stop DataConductor_jar `date`"

#
# Build SMI
#

echo "RunningTime Start SMI `date`"

/usr/bin/bash -x  $Git_unix_workspace/BuildsAndReleases/builds/SMIBuilds.sh \
	-h /cygdrive/c/CustomerInstalls/NW/NWTest	\
	-c /cygdrive/c/clean_builds/$Working_Branch	\
	-p "7120"					\
	-f 0						\
	-t "$timestamp"					\
	-b "$Working_Branch"					\
	-n "$Notify_list"				\
1>$jfong_share/Logs/nightly_$timestamp.log 2>&1

if [ $? == 0 ]; 
then
	echo "SMI was successful"  
else
	blat -subject "Wrapper_master : Build ended with SMI error" \
	-body "SMI had problems with building " \
	-to $Notify_wrapper_list
	exit -1
fi

echo "RunningTime Stop SMI `date`"

#
# Build FlexAdhoc 
#
#echo $jfong_clean
#echo "RunningTime Start FlexAdhoc `date`"

#/usr/bin/bash -x  $Git_unix_workspace/BuildsAndReleases/builds/FlexAdhocBuild.sh  -c /cygdrive/c/clean_builds/$Working_Branch -t "$timestamp" -n "$Notify_list" 1>$jfong_share/Logs/FlexAdhoc_$timestamp.log 2>&1

#if [ $? == 0 ]; 
#then
#	echo "FlexAdhoc was successful"  
#else
#	blat -subject "Wrapper_52x : Build ended with FlexAdhoc error" \
#	-body "FlexAdhoc had problems with building " \
#	-to $Notify_wrapper_list
#	exit -1
#fi
#echo $JAVA_HOME
#
#echo "RunningTime Stop FlexAdhoc `date`"

#
# Build LogicalExpression 
#

echo "RunningTime Start LogicalExpression `date`"

cmd /c "$Git_workspace\BuildsAndReleases\builds\LogicalExpressionBuild.bat" -c $jfong_win_clean\\$Working_Branch -b $Working_Branch -t "$timestamp" -n "$Notify_list" > "$jfong_win_share\Logs\LogicalExpression_$timestamp.log" 2>&1

if [ $? == 0 ]; 
then
	echo "LogicalExpression was successful"  
else
	blat -subject "Wrapper_master : Build ended with LogicalExpression error" \
	-body "LogicalExpression had problems with building " \
	-to $Notify_wrapper_list
	exit -1
fi

echo "RunningTime Stop LogicalExpression `date`"

#
# Build BirstAdmin 
#

echo "RunningTime Start BirstAdmin `date`"

cmd /c "$Git_workspace\BuildsAndReleases\builds\BirstAdminBuild.bat" -c $jfong_win_clean\\$Working_Branch -b $Working_Branch -t "$timestamp" -f 0 -n "$Notify_list" > "$jfong_win_share\Logs\BirstAdmin_$timestamp.log" 2>&1
 
if [ $? == 0 ]; 
then
	echo "BirstAdmin was successful"  
else
	blat -subject "Wrapper_master : Build ended with BirstAdmin error" \
	-body "BirstAdmin had problems with building " \
	-to $Notify_wrapper_list
	exit -1
fi
cp $jfong_clean/Performance\ Optimizer\ Administration/bin/Release/log4net.dll $jfong_share/$timestamp
cp $jfong_clean/Performance\ Optimizer\ Administration/bin/Release/Birst\ Administration.exe $jfong_clean/Acorn/Acorn/bin/Birst\ Administration.exe
"$WinZip_Home/WZZIP.EXE" -ex -a "$jfong_win_share\\$timestamp\\BirstAdmin.zip" \
	"$jfong_win_clean\\$Working_Branch\\Performance Optimizer Administration\\bin\\Release\\Birst Administration.exe" \
	"$jfong_win_clean\\$Working_Branch\\Performance Optimizer Administration\\bin\\Debug\\Birst Administration.exe" \
	"$jfong_win_clean\\$Working_Branch\\LogicalExpression\\LogicalExpression\\bin\\Debug\\LogicalExpression.dll" \
	"$jfong_win_clean\\$Working_Branch\\Performance Optimizer Administration\\bin\\Debug\\Log4net.dll" \
	"$jfong_win_clean\\$Working_Branch\\LogicalExpression\\LogicalExpression\\bin\\Debug\\Antlr3.Runtime.dll"

echo "RunningTime Stop BirstAdmin `date`"

#
# Build DataConductor 
#

echo "RunningTime Start DataConductor_BC `date`"

/usr/bin/bash -x $Git_unix_workspace/BuildsAndReleases/builds/DataConductorBuild_BC.sh -c /cygdrive/c/clean_builds/$Working_Branch -b $Working_Branch -t "$timestamp" -n "$Notify_list" > "$jfong_share/Logs/DataConductor_BC_$timestamp.log" 2>&1

if [ $? == 0 ]; 
then
	echo "DataConductor was successful"  
else
	blat -subject "Wrapper_master : Build ended with DataConductor error" \
	-body "DataConductor had problems with building " \
	-to $Notify_wrapper_list
	exit -1
fi

echo "RunningTime Stop DataConductor_BC `date`"

#
# Build SourceAdmin 
#
echo $jfong_clean
echo "RunningTime Start SourceAdmin `date`"

/usr/bin/bash -x  $Git_unix_workspace/BuildsAndReleases/builds/SourceAdminBuild.sh  -c /cygdrive/c/clean_builds/$Working_Branch -t "$timestamp" -n "$Notify_list" 1>$jfong_share/Logs/SourceAdmin_$timestamp.log 2>&1

if [ $? == 0 ]; 
then
	echo "SourceAdmin was successful"  
else
	blat -subject "Wrapper_master : Build ended with SourceAdmin error" \
	-body "SourceAdmin had problems with building " \
	-to $Notify_wrapper_list
	exit -1
fi
echo $JAVA_HOME

echo "RunningTime Stop SourceAdmin `date`"

#
# Build Acorn 
#

echo "RunningTime Start Acorn `date`"

cmd /c "$Git_workspace\BuildsAndReleases\builds\AcornBuild.bat" -c $jfong_win_clean\\$Working_Branch -w $Git_workspace -b $Working_Branch -t "$timestamp" -f 0 -n "$Notify_list" > "$jfong_win_share\Logs\Acorn_$timestamp.log" 2>&1
 
if [ $? == 0 ]; 
then
	echo "Acorn was successful"  
else
	blat -subject "Wrapper_master : Build ended with Acorn error" \
	-body "Acorn had problems with building " \
	-to $Notify_wrapper_list
	exit -1
fi

echo "RunningTime Stop Acorn `date`"

#
# Build ChartWebService 
#

echo "RunningTime Start ChartWebService `date`"

/usr/bin/bash -x  $Git_unix_workspace/BuildsAndReleases/builds/ChartWebServiceBuild.sh -c /cygdrive/c/clean_builds/$Working_Branch -t "$timestamp" -n "$Notify_list" > "$jfong_share/Logs/ChartWebService_$timestamp.log" 2>&1

if [ $? == 0 ]; 
then
	echo "ChartWebService was successful"  
else
	blat -subject "Wrapper_master : Build ended with ChartWebService error" \
	-body "ChartWebService had problems with building " \
	-to $Notify_wrapper_list
	exit -1
fi

cp $jfong_clean/$Working_Branch/ChartWebService/deployable_units/SMIChartServer.war $jfong_share/$timestamp
if [ -s $jfong_share/$timestamp/SMIChartServer.war ];
then
	echo "copy of SMIChartServer.war was successful"
else
	blat -subject "Wrapper_master : Build ended with ChartWebService error" \
	-body "ChartWebService had problems with copying the ChartWebService.jar to the release location " \
	-to $Notify_wrapper_list
	exit -1
fi
	
echo "RunningTime Stop ChartWebService `date`"

#
#
# Build MoveSpace 
#

echo "RunningTime Start MoveSpace `date`"

cmd /c "$Git_workspace\BuildsAndReleases\builds\MoveSpaceBuild.bat" -c $jfong_win_clean\\$Working_Branch -b $Working_Branch -t "$timestamp" -f 0 -n "$Notify_list" > "$jfong_win_share\Logs\MoveSpace_$timestamp.log" 2>&1
 
if [ $? == 0 ]; 
then
	echo "MoveSpace was successful"  
else
	blat -subject "Wrapper_master : Build ended with MoveSpace error" \
	-body "MoveSpace had problems with building " \
	-to $Notify_wrapper_list
	exit -1
fi

echo "RunningTime Stop MoveSpace `date`"

#
# Build AcornExecute 
#

mkdir $jfong_clean/AcornExecute/AcornExecute/bin
cp $jfong_clean/$Working_Branch/Performance\ Optimizer\ Administration/bin/Release/Birst\ Administration.exe $jfong_clean/AcornExecute/AcornExecute/bin/Birst\ Administration.exe
echo "RunningTime Start AcornExecute `date`"

cmd /c "$Git_workspace\BuildsAndReleases\builds\AcornExecuteBuild.bat" -c $jfong_win_clean\\$Working_Branch -b $Working_Branch -t "$timestamp" -f 0 -n "$Notify_list" > "$jfong_win_share\Logs\AcornExecute_$timestamp.log" 2>&1
 
if [ $? == 0 ]; 
then
	echo "AcornExecute was successful"  
else
	blat -subject "Wrapper_master : Build ended with AcornExecute error" \
	-body "AcornExecute had problems with building " \
	-to $Notify_wrapper_list
	exit -1
fi

echo "RunningTime Stop AcornExecute `date`"

#
# Build Scheduler 
#
echo $jfong_clean
echo "RunningTime Start Scheduler `date`"

/usr/bin/bash -x  $Git_unix_workspace/BuildsAndReleases/builds/SchedulerBuild.sh  -c /cygdrive/c/clean_builds/$Working_Branch -t "$timestamp" -n "$Notify_list" 1>$jfong_share/Logs/Scheduler_$timestamp.log 2>&1

if [ $? == 0 ]; 
then
	echo "Scheduler was successful"  
else
	blat -subject "Wrapper_master : Build ended with Scheduler error" \
	-body "Scheduler had problems with building " \
	-to $Notify_wrapper_list
	exit -1
fi

echo "RunningTime Stop Scheduler `date`"

#
# Package Help 
#

echo "RunningTime Start Help `date`"

cmd /c "$Git_workspace\BuildsAndReleases\builds\PackageHelp.bat" -c $jfong_win_clean\\$Working_Branch -b $Working_Branch -t "$timestamp" -f 0 -n "$Notify_list" > "$jfong_win_share\Logs\PackageHelp_$timestamp.log" 2>&1
 
if [ $? == 0 ]; 
then
	echo "PackageHelp was successful"  
else
	blat -subject "Wrapper_master : Package Help ended with an error" \
	-body "Help had problems with the packaging " \
	-to $Notify_wrapper_list
	exit -1
fi

echo "RunningTime Stop Help `date`"

#
# Build MigrateSchedules 
#
#
#echo "RunningTime Start MigrateSchedules `date`"
#
#cmd /c "$Git_workspace\BuildsAndReleases\builds\MigrateSchedulesBuild.bat" -c $jfong_win_clean\\$Working_Branch -b $Working_Branch -t "$timestamp" -f 0 -n "$Notify_list" > "$jfong_win_share\Logs\MigrateSchedules_$timestamp.log" 2>&1
# 
#if [ $? == 0 ]; 
#then
#	echo "MigrateSchedules was successful"  
#else
#	blat -subject "Wrapper_master : Build ended with MigrateSchedules error" \
#	-body "MigrateSchedules had problems with building " \
#	-to $Notify_wrapper_list
#	exit -1
#fi
#
#echo "RunningTime Stop MigrateSchedules `date`"
#
#
#	Sign Jars
#

echo "RunningTime Start SignJars `date`"

/usr/bin/bash -x  $Git_unix_workspace/BuildsAndReleases/builds/Sign_jars.sh -t "$timestamp" -n "$Notify_list" 1>$jfong_share/Logs/Sign_jar_$timestamp.log 2>&1 
if [ $? == 0 ]; 
then
	echo "SMI was successful"  
else
	blat -subject "Wrapper_master : Build ended with Sign_jar error" \
	-body "Sign_jar had problems encrypting files " \
	-to $Notify_wrapper_list
	exit -1
fi

echo "RunningTime Stop SignJars `date`"

#
# FTP files
#

echo "RunningTime Start FTPfiles `date`"

/usr/bin/bash -x  $Git_unix_workspace/BuildsAndReleases/builds/ftp_files.sh \
	-h /cygdrive/c/CustomerInstalls/NW/NWTest	\
	-p "7120"					\
	-f 0						\
	-t "$timestamp"					\
	-b "$Working_Branch"					\
	-n "$Notify_list"				\
1>$jfong_share/Logs/FTP_$timestamp.log 2>&1

if [ $? == 0 ]; 
then
	echo "FTP was successful"  
else
	blat -subject "Wrapper_master : FTP error" \
	-body "FTP had problems with transfering files " \
	-to $Notify_wrapper_list
	exit -1
fi

echo "RunningTime Stop FTPfiles `date`"

#
# Deploy  SMI
#

#	/usr/bin/bash -x  $Git_unix_workspace/BuildsAndReleases/builds/SMITest.sh \
#		-h /cygdrive/c/CustomerInstalls/NW/NWTest	\
#		-p "7120"					\
#		-f 0						\
#		-t "$timestamp"					\
#		-b "$Working_Branch"					\
#		-n "$Notify_list"				\
#	1>$jfong_share/Logs/SMITest_$timestamp.log 2>&1

if [ $? == 0 ]; 
then
	echo "SMITest was successful"  
else
	blat -subject "Wrapper_master : Test ended with SMI error" \
	-body "SMITest had problems with building " \
	-to $Notify_wrapper_list
	exit -1
fi

# Write out the latest build number for deployment to pick up.
echo "build_number=$timestamp" >$Current_Build_Number/$Working_Branch/build_number.txt

blat -superdebugT -subject "Wrapper_master : Daily build completed without errors" \
	-body "$timestamp build is now available" \
	-to "$Notify_wrapper_list" 

#/cygdrive/c/Program\ Files\ \(x86\)/sendmail/sendEmail.exe -s 192.168.85.11 -f jfong@birst.com  \
#	-t "$Notify_wrapper_list" \
#	-u "Wrapper_master : Daily build completed without errors"	\
#	-m "$timestamp build is now available"

echo "RunningTime End Build `date`"

exit 0