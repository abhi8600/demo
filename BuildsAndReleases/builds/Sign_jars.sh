﻿#!/usr/bin/bash
#
PATH=/cygdrive/c/eclipse/plugins/apache-ant-1.7.1/bin:$PATH;		export PATH
JAVA_HOME=`cygpath -u "C:\Program Files\Java\jdk1.7.0"` ;		export JAVA_HOME
CLASSPATH=`cygpath -u ".\ClientCodeSigner.jar"`; 			export CLASSPATH


function Parameter_Help
{
echo "usage: Sign_jar [-options]"
			echo "where options include:"
			echo " -n <email notification list> comma separated"
			echo " 		default=jfong@birst.com,4157301327@vtext.com"
			echo " -t <build tag> "
			echo " 		Usually, this is the date and time  with or without a "
			echo " 		release number.  For example, 200802140805 "
			echo " 		is Feb 14th, 2008 8:05 a.m.  Or if it for 2.2.0, "
			echo "		the tag would be 200802140805_220"
			echo ""
}
# Define variables
# 	and clear them out first. 
#
Notify=jfong@birst.com
build_tag=""
Branch_Name=""
jfong_clean="/cygdrive/c/clean_builds_"$Working_Branch
jfong_share=/cygdrive/c/shared/Builds;
declare -i error_number=0
jfong_clean=""
CVS_workspace=""

while getopts ":d:b:n:t:" opts; do
        case "$opts" in
		d ) echo "-d $OPTARG"
			DEBUG=$OPTARG
		 	export DEBUG
			;;
		n ) echo "-n $OPTARG" 
			Notify=$OPTARG
			export Notify 
			;;
		t ) echo "-t $OPTARG"			# Since I'm creating the build, I am generating the build tag.  
			build_tag=$OPTARG;	
			export build_tag
			;;
		*  ) Parameter_Help 
			exit 1 ;;
	esac
	echo one value $1
done

jfong_clean=/cygdrive/c/clean_builds_$Branch_Name
echo "Branch = " $Branch_Name "-"
echo "workspace = " $jfong_clean

if [ -z $build_tag ];	#  Make sure we have our parameters
then
	Parameter_Help
	exit 1
fi

env

# 
# 	Sign the jars
#

#	Sign Acorn.zip

cd `cygpath -u "C:\ClientCodeSigner"` 		# goto my clean build area
if [ $? == 0 ]; 
then
	echo "Wrapper Sign_jar: Changed directory worked"
else
	blat -subject "Wrapper Sign_jar: change directory had problems" \
	-body "Can get to the ClientCodeSigner directory, need to fix before proceeding." \
	-to $Notify
	exit -1
fi

mkdir ./temp
if [ $? == 0 ]; 
then
	echo "Wrapper Sign_jar: Create Temp directory worked"
else
	echo "temp directory exists"
	rm -rf ./temp
	if [ $? == 0 ]; 
	then
		echo "Wrapper Sign_jar: Removed old temp directory worked"
		mkdir ./temp
		if [ $? == 0 ]; 
		then
			echo "Wrapper Sign_jar: Create Temp directory worked"
		else
			blat -subject "Wrapper Sign_jar: create temp directory had problems" \
			-body "Errors were generated when we attempted create the temp directory for signing" \
			-to $Notify
			exit -1
		fi
	else
		blat -subject "Wrapper Sign_jar: create temp directory had problems" \
		-body "Errors were generated when we attempted create the temp directory for signing" \
		-to $Notify
		exit -1
	fi
fi

mv `cygpath -w "$jfong_share/$build_tag/Acorn.zip"` .

/cygdrive/c//Program\ Files\ \(x86\)/WinZip/WZUNZIP.EXE -d -ybc -o -yo ./Acorn.zip ./temp
if [ $? == 0 ]; 
then
	echo "Wrapper Sign_jar: Unzip Acorn.zip worked"
else
	blat -subject "Wrapper Sign_jar: Unzip Acorn.zip had problems" \
	-body "Errors were generated when we attempted to unzip the Acorn.zip package for signing" \
	-to $Notify
	exit -1
fi

for file in `/usr/bin/find ./temp -name "*.jar" -type f` ; 
do 
echo $file;  

"$JAVA_HOME"/bin/java.exe ClientCodeSigner $file;
jarsigner -verify -strict $file;
error_number=$?
if [ $error_number == 0 ] || [ $error_number == 6 ]; 
then
	echo "Wrapper Sign_jar: Signning $file worked"
else
	blat -subject "Wrapper Sign_jar: Signingproblems" \
	-body "Errors were generated when we attempted to sign $file" \
	-to $Notify
	exit -1
fi

done

cd `cygpath -u "C:\ClientCodeSigner\temp"` 		# goto my clean build area

/cygdrive/c//Program\ Files\ \(x86\)/WinZip/WZZIP.EXE -ex -r -P -a `cygpath -w "$jfong_share/$build_tag/Acorn.zip"` .
if [ $? == 0 ]; 
then
	echo "Wrapper Sign_jar: Zip Acorn.zip worked"
else
	blat -subject "Wrapper Sign_jar: Unzip Acorn.zip had problems" \
	-body "Errors were generated when we attempted to unzip the Acorn.zip package for signing" \
	-to $Notify
	exit -1
fi

cd `cygpath -u "C:\ClientCodeSigner"` 		# goto my clean build area
rm -rf ./temp
rm Acorn.zip




#	Sign BirstConnect.zip

cd `cygpath -u "C:\ClientCodeSigner"` 		# goto my clean build area
if [ $? == 0 ]; 
then
	echo "Wrapper Sign_jar: Changed directory worked"
else
	blat -subject "Wrapper Sign_jar: change directory had problems" \
	-body "Can get to the ClientCodeSigner directory, need to fix before proceeding." \
	-to $Notify
	exit -1
fi

mkdir ./temp
if [ $? == 0 ]; 
then
	echo "Wrapper Sign_jar: Create Temp directory worked"
else
	echo "temp directory exists"
	rm -rf ./temp
	if [ $? == 0 ]; 
	then
		echo "Wrapper Sign_jar: Removed old temp directory worked"
		mkdir ./temp
		if [ $? == 0 ]; 
		then
			echo "Wrapper Sign_jar: Create Temp directory worked"
		else
			blat -subject "Wrapper Sign_jar: create temp directory had problems" \
			-body "Errors were generated when we attempted create the temp directory for signing" \
			-to $Notify
			exit -1
		fi
	else
		blat -subject "Wrapper Sign_jar: create temp directory had problems" \
		-body "Errors were generated when we attempted create the temp directory for signing" \
		-to $Notify
		exit -1
	fi
fi

mv `cygpath -w "$jfong_share/$build_tag/BirstConnect.zip"` .

/cygdrive/c//Program\ Files\ \(x86\)/WinZip/WZUNZIP.EXE -d -ybc -o -yo ./BirstConnect.zip ./temp
if [ $? == 0 ]; 
then
	echo "Wrapper Sign_jar: Unzip BirstConnect.zip worked"
else
	blat -subject "Wrapper Sign_jar: Unzip BirstConnect.zip had problems" \
	-body "Errors were generated when we attempted to unzip the BirstConnect.zip package for signing" \
	-to $Notify
	exit -1
fi

for file in `/usr/bin/find ./temp -name "*.jar" -type f` ; 
do 
echo $file;  

"$JAVA_HOME"/bin/java.exe ClientCodeSigner $file;
jarsigner -verify -strict $file;
error_number=$?
if [ $error_number == 0 ] || [ $error_number == 6 ]; 
then
	echo "Wrapper Sign_jar: Signning $file worked"
else
	blat -subject "Wrapper Sign_jar: Signingproblems" \
	-body "Errors were generated when we attempted to sign $file" \
	-to $Notify
	exit -1
fi

done

cd `cygpath -u "C:\ClientCodeSigner\temp"` 		# goto my clean build area

/cygdrive/c//Program\ Files\ \(x86\)/WinZip/WZZIP.EXE -ex -r -P -a `cygpath -w "$jfong_share/$build_tag/BirstConnect.zip"` .
if [ $? == 0 ]; 
then
	echo "Wrapper Sign_jar: Unzip BirstConnect.zip worked"
else
	blat -subject "Wrapper Sign_jar: Unzip BirstConnect.zip had problems" \
	-body "Errors were generated when we attempted to unzip the BirstConnect.zip package for signing" \
	-to $Notify
	exit -1
fi

cd `cygpath -u "C:\ClientCodeSigner"` 		# goto my clean build area
rm -rf ./temp
rm BirstConnect.zip


