#!/usr/bin/bash

# Quick script to package the user files for 2.1.x   
# There was a problem with how we were creating our 2.1.x packages.  Some of the 
# customer files was included in the platform package.   This created a situation 
# where we had to repackage the platform package at a later time when the customer 
# files were ready for release.  
#
# This script takes an existing platform build and recreates the Customer specific 
# package with the update customer files.  
# 
# NOTE - this uses Jerome's default build areas for create the new package.  
#	Look at the definition for jfong_share and jfong_clean.
#

DEBUG=0;					export DEBUG
nightly_bin=/cygdrive/c/workspace_HEAD/BuildsAndReleases/builds; 		export nightly_bin
jfong_share=/cygdrive/c/shared/Builds;		export jfong_share
Notify="jfong@birst.com" export Notify		#email list
declare -i error_count=0
build_tag=""
Cust_Name=""
CVS_Tag=""
New_Rel_Number=""

while getopts ":d:i:n:t:" opts; do
        case "$opts" in
		d ) echo "-d $OPTARG"
			DEBUG=$OPTARG
		 	export DEBUG
			;;
		i ) echo "-i $OPTARG" 
			New_Rel_Number=$OPTARG
			export New_Rel_Number
			;;
		t ) echo "-t $OPTARG"			# Since I'm creating the build, I am generating the build tag.  
			build_tag=$OPTARG;	
			export build_tag
			;;
		n ) echo "-n $OPTARG" 
			Notify=$OPTARG
			export Notify 
			;;
		*  ) echo "usage: release_platform [-options]"
			echo "where options include:"
			echo " -n <email notification list> comma separated"
			echo " 		default=jfong@birst.com,4157301327@vtext.com"
			echo " -t <build tag> "
			echo "		We are looking for the Nightly,timestampe,the date and time "
			echo "		e.g. 200705230130, or 200806190705_230 for  a 2.3.0 build"
			echo " -i <release number>"
			echo " 		This is the release number for the new releases of customer files.  "
			echo " 		FMR has weekly releases, for every new push to production, we assign "
			echo " 		a new release number to the release.  The majority of time, the"
			echo " 		point release only has FMR files, but it could have mulitple customers"
			echo " 		if they are all releasing the same day"
			echo " 	For example, we have a FMR tag called FMR5659, we want to put the files in "
			echo " 	release number 2.3.2.1, the command would be"
			echo " bash -x release_platform.sh -t 200805201013_215 -i 2.1.5 "
			exit 1 ;;
	esac
	echo one value $1
done

if [ -z $build_tag ] || [ -z $New_Rel_Number ] #	|| [ -z $Period_Id ]; 
then
	echo "usage: 21x_customer_package.sh [-options]"
	echo "where options include:"
	echo " -n <email notification list> comma separated"
	echo " 		default=jfong@birst.com,4157301327@vtext.com"
	echo " -t <build tag> "
	echo "		We are looking for the Nightly,timestampe,the date and time "
	echo "		e.g. 200705230130, or 200806190705_230 for  a 2.3.0 build"
	echo " -i <release number>"
	echo " 		This is the release number for the new releases of customer files.  "
	echo " 		FMR has weekly releases, for every new push to production, we assign "
	echo " 		a new release number to the release.  The majority of time, the"
	echo " 		point release only has FMR files, but it could have mulitple customers"
	echo " 		if they are all releasing the same day"
	echo " 	For example, we have a FMR tag called FMR5659, we want to put the files in "
	echo " 	release number 2.3.2.1, the command would be"
	echo " bash -x release_platform.sh -t 200805201013_215 -i 2.1.5 " 
	exit 1
fi 


#
# Package up the customer specific files.  
#	1. find the files to release
#	2. FTP the files up to the local, colo and Rackspace machines.  
#

#
#	1. find the files to release
#
echo $Cust_Name
if [ ! -d $jfong_share/$build_tag ];
then
	mkdir $jfong_share/$build_tag/$Cust_Name
	if [ $? != 0 ];
	then
		echo "Problem creating the Customer specific subdirectory" \
		| mutt -s "Package_Cust: Error creating customer subdirectory." $Notify
	fi
fi

if [ -d /cygdrive/b/Releases/Platform/$New_Rel_Number ];
then
	echo "The release already exist, you cannot overwrite an existing release" \
	| mutt -s "Release_Platform: release version exists" $Notify
	exit -1
fi 

#
# 	2. FTP the files up to the local, colo and Rackspace machines.  
#

if [ $DEBUG -eq 0 ]; then
	echo "copy over the files to the shared /smi/builds/"
	
	if [ -e "$jfong_clean"/upload.ftp ];
	then
		rm -rf  "$jfong_clean"/upload.ftp
	fi 

	#
	# FTP build to the //smi/builds directory
	#
	cd $jfong_clean
	echo "open builds" >upload.ftp
	echo "smibuilds" >>upload.ftp
	echo "5771builds!" >>upload.ftp
	echo "verbose" >>upload.ftp
	echo "prompt" >>upload.ftp
	echo "lcd C:\SFOJFONG\builds\\"$build_tag >>upload.ftp
	echo "cd Releases\Platform" >>upload.ftp
	echo "mkdir " $New_Rel_Number >>upload.ftp
	echo "cd " $New_Rel_Number >>upload.ftp
	echo "pwd "  >>upload.ftp
	echo "ls -la"  >>upload.ftp
	echo "mput *.* " >>upload.ftp
	echo "ls -la" >>upload.ftp
	echo "quit" >>upload.ftp
	
	ftp -s:upload.ftp  
	if [ $? == 0 ]; 
	then
		echo "upload of log files was successful"  
	else
		echo "Exiting since FTP was not successful in transfering the zipped up files.  " \
			| mutt -s "Cust_package: Unable to ftp the files to SMI" $Notify
		exit -1
	fi
	
	rm -rf  "$jfong_clean"/upload.ftp
	
	
	#
	# FTP build to the Colo1 build directory
	#
	cd $jfong_clean
	echo "open 192.168.1.161" >upload.ftp
	echo "smibuilds" >>upload.ftp
	echo "5771builds!" >>upload.ftp
	echo "verbose" >>upload.ftp
	echo "prompt" >>upload.ftp
	echo "lcd C:\SFOJFONG\builds\\"$build_tag >>upload.ftp
	echo "cd Releases\Platform" >>upload.ftp
	echo "mkdir " $New_Rel_Number >>upload.ftp
	echo "cd " $New_Rel_Number >>upload.ftp
	echo "pwd "  >>upload.ftp
	echo "ls -la"  >>upload.ftp
	echo "mput *.* " >>upload.ftp
	echo "ls -la" >>upload.ftp
	echo "quit" >>upload.ftp
	
	ftp -s:upload.ftp  
	if [ $? == 0 ]; 
	then
		echo "upload of log files was successful"  
	else
		echo "Exiting since FTP was not successful in transfering the zipped up files.  " \
			| mutt -s "Cust_package: Unable to ftp the files to colo1" $Notify
		exit -1
	fi
	
	rm -rf  "$jfong_clean"/upload.ftp

	#
	# FTP build to the FTP1 build directory
	#
	cd $jfong_clean
	echo "open rsbuilds" >upload.ftp
	echo "smibuilds" >>upload.ftp
	echo "5771builds!" >>upload.ftp
	echo "verbose" >>upload.ftp
	echo "prompt" >>upload.ftp
	echo "lcd C:\SFOJFONG\builds\\"$build_tag >>upload.ftp
	echo "cd Releases\Platform" >>upload.ftp
	echo "mkdir " $New_Rel_Number >>upload.ftp
	echo "cd " $New_Rel_Number >>upload.ftp
	echo "pwd "  >>upload.ftp
	echo "ls -la"  >>upload.ftp
	echo "mput *.* " >>upload.ftp
	echo "ls -la " >>upload.ftp
	echo "quit" >>upload.ftp
	
	ftp -s:upload.ftp  
	if [ $? == 0 ]; 
	then
		echo "upload of log files was successful"  
	else
		echo "Exiting since FTP was not successful in transfering the zipped up files.  " \
			| mutt -s "Cust_package: Unable to ftp the files to colo1" $Notify
		exit -1
	fi
	
#	rm -rf  "$jfong_clean"/upload.ftp
		
fi



	