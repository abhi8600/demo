﻿#!/usr/bin/bash
#  
echo on

/usr/bin/env 

function Parameter_Help
{
echo "usage: deploy [-options]"
			echo "where options include:"
			echo " -b <branch> "
			echo "		Enter the branch that we will be tagging on."
			echo " 		If not branch is supplied, we default to HEAD"
			echo " -c <clean working directory>"
			echo "		This is the clean area we will be exporting "
			echo "		and building from.  Everything will be deleted "
			echo "		from this directory tree before we start"
			echo "		e.g. \c\clean_build_SMI_2_2_0_SP"
			echo " -n <email notification list> comma separated"
			echo " 		default=jfong@birst.com,4157301327@vtext.com"
			echo " -t <build tag> "
			echo " 		Usually, this is the date and time  with or without a "
			echo " 		release number.  For example, 200802140805 "
			echo " 		is Feb 14th, 2008 8:05 a.m.  Or if it for 2.2.0, "
			echo "		the tag would be 200802140805_220"
			echo " -w <working directory>"
			echo "		This is the working directory where the code resides on "
			echo "		the machine.  With Eclipse, the CVS trunk is usually  "
			echo "		c:\workspace and the branchesw will reside under "
			echo "		the branch name. \c\workspace_SMI_2_2_0_SP"
			echo ""
}
# Define variables
# 	and clear them out first. 
#

Notify=jfong@birst.com
CVS_exe=/cygdrive/c/cygwin/bin/cvs.exe
#Build_Number_Dir=/Build_Version_Numbers
Build_Number_Dir="$(cygpath -u -p -a '\\repo_dev\dev\Builds\Build_Version_Number')"
time_tag=""
Branch_Name=""
Tag_flag=0
jfong_clean=""
Git_workspace=""
cvs_password=""
declare -i error_count

ls -la $Build_Number_Dir

while getopts ":d:b:c:k:n:t:w:z:" opts; do
        case "$opts" in
		d ) echo "-d $OPTARG"
			DEBUG=$OPTARG
		 	export DEBUG
			;;
		b ) echo "-b $OPTARG" 
			Branch_Name=$OPTARG
			echo $Branch_Name
			;;
		c ) echo "-c $OPTARG"
			jfong_clean="$(cygpath -u -p -a "$OPTARG")"
			echo $jfong_clean
			;;
		k ) echo "-k $OPTARG" 
			Tag_flag=$OPTARG
			export Notify 
			;;
		n ) echo "-n $OPTARG" 
			Notify=$OPTARG
			export Notify 
			;;
		t ) echo "-t $OPTARG"			# Since I'm creating the build, I am generating the build tag.  
			time_tag=$OPTARG;	
			export time_tag
			;;
		z ) echo "-z $OPTARG"			
			cvs_password=$OPTARG;	
			export cvs_password
			;;
		w ) echo "-w $OPTARG"
			Git_workspace="$(cygpath -u -p -a "$OPTARG")"
			echo $Git_workspace
			;;
		*  ) Parameter_Help 
			exit 1 ;;
	esac
	echo one value $1
done

echo "Branch = " $Branch_Name "-"
echo "workspace = " $jfong_clean

if [ -z $jfong_clean ] || [ -z $Git_workspace ] || [ -z $time_tag ] || [ -z $Branch_Name ];	#  Make sure we have our parameters
then
	Parameter_Help
	exit 1
fi
	
if [ ! -d $jfong_clean ];
then
	mkdir -p $jfong_clean 
	chmod -R 777 $jfong_clean
else
	rm -rf $jfong_clean/*
	chmod -R 777 $jfong_clean
fi

cd $Git_workspace

#echo cd ${Git_workspace:9} > Git_update.txt
#echo git checkout $Branch_Name >> Git_update.txt
#echo git status >> Git_update.txt
#	echo git pull origin $Branch_Name >> Git_update.txt
#	echo git reset --hard HEAD >> Git_update.txt
#	echo git clean -fd >> Git_update.txt
#echo exit >> Git_update.txt

if [ $Tag_flag == 1 ] 
then
	Build_tag=Birst_$time_tag
else
	Build_tag=$Branch_Name

fi
export Build_tag


echo cvs sync smi_home  $Git_workspace
echo cvs sync time_tag $time_tag
echo cvs sync Build_tag $Build_tag
echo cvs sync Path $PATH

cd $Git_workspace
pwd

#	git checkout $Branch_Name
#	git pull origin $Branch_Name

#	/cygdrive/c/Program\ Files\ \(x86\)/Git/bin/sh.exe  -l < Git_update.txt
 
 
git archive $Branch_Name | tar -x -C $jfong_clean

# ------------------------------------------------------------------------------------------
# FlexAdhoc module.
# Copy custom build properties
#

cd $jfong_clean/FlexAdhoc/build

cp -v -f  $Git_workspace/BuildsAndReleases/builds/customBuild.properties  $jfong_clean/FlexAdhoc/build
if [ $? == 0 ]; 
then
	echo "CVS copy of customBuild.properties worked"
else
	blat -subject "CVS_sync: copy of CustomBuild error" -body "CVS copying customBuild.properties for FlexAdhoc had problem" -to $Notify
	exit -1
fi

# ------------------------------------------------------------------------------------------
# ChartRenderer module.
# Copy custom build properties
#

cd $jfong_clean/ChartRenderer/build

cp -v -f  $Git_workspace/BuildsAndReleases/builds/customBuild.properties  $jfong_clean/ChartRenderer/build
if [ $? == 0 ]; 
then
	echo "CVS copy of customBuild.properties worked"
else
	blat -subject "CVS_sync: copy of CustomBuild error" -body "CVS copying customBuild.properties for ChartRenderer had problem" -to $Notify
	exit -1
fi

# ------------------------------------------------------------------------------------------
# Maps module.
# Copy to the other required directories.
#

cd $jfong_clean	

mkdir -p $jfong_clean/Acorn/Acorn/maps 
if [ ! -d $jfong_clean/Acorn/Acorn/maps ];
then
	set VarMailSubject="CVS_sync of %Build_Tag% on %COMPUTERNAME%:Cannot create Map directory"
	set VarMailText="Cannot create Acorn Map directory, please correct issue and re-run."
	goto BADEXIT:
fi

cp -r -f ./Maps/*  $jfong_clean/Acorn/Acorn/maps
if [ $? == 0 ]; 
then
	echo "Copy of Maps to Acorn worked"
else
	blat -subject "CVS_sync: Maps copy failed" -body "CVS copy of Maps to Acorn had errors" -to $Notify
	exit -1
fi

mkdir -p $jfong_clean/ChartWebService/web/WEB-INF/classes/maps 
if [ ! -d $jfong_clean/ChartWebService/web/WEB-INF/classes/maps ];
then
	set VarMailSubject="CVS_sync of %Build_Tag% on %COMPUTERNAME%:Cannot create Map directory"
	set VarMailText="Cannot create Acorn Map directory, please correct issue and re-run."
	goto BADEXIT:
fi

cp -r -f ./Maps/* $jfong_clean/ChartWebService/web/WEB-INF/classes/maps
if [ $? == 0 ]; 
then
	echo "Copy of Maps to ChartWebServices worked"
else
	blat -subject "CVS_sync: Maps copy failed" -body "CVS copy of Maps to ChartWebServices had errors" -to $Notify
	exit -1
fi


# ------------------------------------------------------------------------------------------
# SourceAdmin module.
# Copy custom build properties
#

cd $jfong_clean/SourceAdmin/build

cp -v -f  $Git_workspace/BuildsAndReleases/builds/customBuild.properties  $jfong_clean/SourceAdmin/build
if [ $? == 0 ]; 
then
	echo "CVS copy of customBuild.properties worked"
else
	blat -subject "CVS_sync: copy of CustomBuild for SourceAdmin error" -body "CVS copying customBuild.properties for SourceAdmin had problem" -to $Notify
	exit -1
fi

# ------------------------------------------------------------------------------------------
# Now do the same for the MemDB module.
# create a label with the latest greatest code
#

cd $jfong_clean/MemDB/build

if [ ! -d $jfong_clean/MemDB/build ];
then
	mkdir -p $jfong_clean/MemDB/build 
	chmod -R 777 $jfong_clean/MemDB/build
else
	rm -rf $jfong_clean/MemDB/build/*
	chmod -R 777 $jfong_clean/MemDB/build
fi

cd $jfong_clean/MemDB/build

cp -v -f  $Git_workspace/BuildsAndReleases/builds/customBuild.properties  $jfong_clean/MemDB/build
if [ $? == 0 ]; 
then
	echo "CVS copy of customBuild.properties worked"
else
	blat -subject "CVS_sync: copy of CustomBuild for MemDB error" -body "CVS copying customBuild.properties for MemDB had problem" -to $Notify
	exit -1
fi


# ------------------------------------------------------------------------------------------
#
#	Make sure all the SMI localization is setup 
#	several scripts need to be modified to work in my environment.
#	(e.g. build.properties needs to know about the local tree)
#
#	First, make sure the build.properties file is setup for my local 
#	installation.  
#		The build.properties files expects Apache to be installed 
#		on your computer.  This is a hack to allow me to build 
#		off the Brokerage demo.
#

cd $jfong_clean/SMI/staging/minnesota
if [ -a build.properties ]; 
then
	echo "build.properties exist"
	cp ./build.properties ./build.temp
else
	blat -subject "NightlyBuild: CVS properties file error" -body "Something is wrong, I can't find the build.properties file. \n The CVS update or something else did not work" -to $Notify
	exit -1
fi

sed '1,$ s^c\:\/dev\/main\/tomcat^C\:\/apache-tomcat-6.0.32^' <./build.temp >./build.properties
diff build.properties build.temp         # > /dev/null
if [ $? == 0 ];
then
        blat -subject "NightlyBuild: Localize SMI tomcat builds.properties failed" \
	-body "Substitution for local environment did not work. \\n Either CVS did not update or someone change the file.\n exiting so we can figure it out" \
	-to $Notify
        exit -1
fi
rm ./build.temp		# we've update the file, we can delete the older version.

#
# TEMPORARY change to handle the hard coded requirement of where CATALINA_HOME has to be for the Scheduler.  
#
	
cd $jfong_clean/Scheduler
if [ -a build.properties ]; 
then
	echo "build.properties exist"
	cp ./build.properties ./build.temp
else
	blat -subject "NightlyBuild: CVS properties file error" -body "Something is wrong, I can't find the Scheduler build.properties file. \n The CVS update or something else did not work" -to $Notify
	exit -1
fi

sed '1,$ s^c\:\/dev\/main\/tomcat^C\:\/apache-tomcat-6.0.32^' <./build.temp >./build.properties
diff build.properties build.temp         # > /dev/null
if [ $? == 0 ];
then
        blat -subject "NightlyBuild: Localize Scheduler builds.properties failed" \
	-body "Substitution for local environment did not work. \\n Either CVS did not update or someone change the file.\n exiting so we can figure it out" \
	-to $Notify
        exit -1
fi
rm ./build.temp		# we've update the file, we can delete the older version.

#
# Verify that our substitution/hack worked
#
grep -q "apache-tomcat-6.0.32" ./build.properties
if [ $? == 0 ];
then
        echo "substitution worked"
else
        blat -subject "NightlyBuild: Localize builds.properties failed" \
	-body "Substitution for local environment did not work. \\n Either CVS did not update or someone change the Scheduler file.\n exiting so we can figure it out" \
	-to $Notify
        exit -1
fi

#
# TEMPORARY change to handle the hard coded requirement of where CATALINA_HOME has to be for the ChartWebService.  
#
	
cd $jfong_clean/ChartWebService
if [ -a build.properties ]; 
then
	echo "build.properties exist"
	cp ./build.properties ./build.temp
else
	blat -subject "NightlyBuild: CVS properties file error" -body "Something is wrong, I can't find the ChartWebService build.properties file. \n The CVS update or something else did not work" -to $Notify
	exit -1
fi

sed '1,$ s^c\:\/dev\/main\/tomcat^C\:\/apache-tomcat-6.0.32^' <./build.temp >./build.properties
diff build.properties build.temp         # > /dev/null
if [ $? == 0 ];
then
        blat -subject "NightlyBuild: ChartWebService builds.properties failed" \
	-body "Substitution for local environment did not work. \\n Either CVS did not update or someone change the file.\n exiting so we can figure it out" \
	-to $Notify
        exit -1
fi
rm ./build.temp		# we've update the file, we can delete the older version.

#
# Verify that our substitution/hack worked
#
grep -q "apache-tomcat-6.0.32" ./build.properties
if [ $? == 0 ];
then
        echo "substitution worked"
else
        blat -subject "NightlyBuild: Localize builds.properties failed" \
	-body "Substitution for local environment did not work. \\n Either CVS did not update or someone change the ChartWebService file.\n exiting so we can figure it out" \
	-to $Notify
        exit -1
fi

#
#	Update our own build number since we don't want anyone overriding our numbering consistency.
#

echo $Build_Number_Dir
echo $JAVA_HOME
echo $CLASSPATH
which ant

cd $Build_Number_Dir/$Branch_Name

ant buildnumber 
if [ $? != 0 ];
then
	blat -subject "CVS_Sync: Update build number problem  " \
	-body "Exiting nightly build since there was a problem with the build number copy." \
	-to $Notify
	exit -1
fi

#
#	We have our own build number since we don't want anyone overriding our numbering consistency.
#	copy build.number to the build directory.  
#
cp -v  $Build_Number_Dir/$Branch_Name/build.number \
	$jfong_clean/SMI/staging/minnesota/build.number

if [ $? != 0 ];
then
	
	blat -subject "NightlyBuild: Copy consistent build number problem  " \
	-body "Exiting nightly build since there was a problem with the build number copy." \
	-to $Notify
	exit -1
fi

#
#	copy build.number to the build directory for Scheduler.  
#
cp -v  $Build_Number_Dir/$Branch_Name/build.number \
	$jfong_clean/Scheduler/build.number

if [ $? != 0 ];
then
	
	blat -subject "NightlyBuild: Copy consistent build number problem  " \
	-body "Exiting nightly build since there was a problem with the build number copy." \
	-to $Notify
	exit -1
fi

#
#	copy build.number to the build directory for ChartWebService.  
#
cp -v  $Build_Number_Dir/$Branch_Name/build.number \
	$jfong_clean/ChartWebService/build.number

if [ $? != 0 ];
then
	
	blat -subject "NightlyBuild: Copy consistent build number problem  " \
	-body "Exiting nightly build since there was a problem with the build number copy." \
	-to $Notify
	exit -1
fi

#
#	Copy build.number to the build directory for MemDB.  
#
cp -v  $Build_Number_Dir/$Branch_Name/build.number \
	$jfong_clean/MemDB/build.number

if [ $? != 0 ];
then
	
	blat -subject "NightlyBuild: Copy consistent build number problem  " \
	-body "Exiting nightly build since there was a problem with the build number copy." \
	-to $Notify
	exit -1
fi


#
#	Update our own Acorn build number since we don't want anyone overriding our numbering consistency.
#

grep versionNumber $jfong_clean/Acorn/Acorn/Web.config
echo VersionNumber: $versionNumber
Build_Number=`grep build.number $Build_Number_Dir/$Branch_Name/build.number | sed s/build.number=//`
echo BuildNumber: $Build_Number
Release_Number=`grep release.number $Build_Number_Dir/$Branch_Name/build.number | sed s/release.number=//`
Release_Number=${Release_Number:0:5}
echo $Release_Number
copyRight=`grep "add\ key=\"copyRight\"" $jfong_clean/Acorn/Acorn/web.config | sed s^\<add\ key=\"copyRight\"\ value=\"^^ | sed s^\"\/\>^^ | sed s^\ \ \ \ ^^`
echo copyRight: $copyRight

#
# Update version numbers in the AssemblyInfo.cs files
#
#
# AssemblyVersion has the release version
# AssemblyFileVersion has the build sequence for the version.
#

Directory[1]="Acorn/Acorn"
Directory[2]="AcornTestConsole/AcornTestConsole"
Directory[3]="LogicalExpression/LogicalExpression"
Directory[4]="LogicalExpression/LogicalExpressionTest"
Directory[5]="MoveSpace/MoveSpace"
Directory[6]="AcornExecute/AcornExecute"
#	Directory[7]="MigrateSchedules/MigrateSchedules"


for ((index=1; index<=6; index++))
do
	printf "     %s\n" "${Directory[index]}"
	error_count=0
	cd $jfong_clean/${Directory[index]}/Properties
	error_count=$error_count+$?
	cp ./AssemblyInfo.cs ./AssemblyInfo.temp
	error_count=$error_count+$?
	sed '1,$s^assembly: AssemblyVersion("1.0.0.0")^assembly: AssemblyVersion("'$Release_Number'.'$Build_Number'")^' < ./AssemblyInfo.temp > ./AssemblyInfo.temp1
	error_count=$error_count+$?
	sed '1,$s^assembly: AssemblyFileVersion("1.0.0.0")^assembly: AssemblyFileVersion("'$Release_Number'.'$Build_Number'")^' < ./AssemblyInfo.temp1 > ./AssemblyInfo.temp2
	error_count=$error_count+$?
	sed '1,$s^AssemblyCopyright("Copyright \©  2009-2010")^AssemblyCopyright("'"$copyRight"'")^' < ./AssemblyInfo.temp2 > ./AssemblyInfo.cs
	error_count=$error_count+$?
	diff AssemblyInfo.cs AssemblyInfo.temp         # > /dev/null
	if [ $? == 0 ];
	then
		blat -subject "NightlyBuild: Modification of the ${Directory[index]} AssemblyInfo.cs failed" \
		-body "Substitution for local environment did not work. \\n Either CVS did not update or someone change the file.\n exiting so we can figure it out" \
		-to $Notify
		exit -1
	fi

	if [ $error_count != 0 ];
	then
		
		blat -subject "CVS_Sync: Updating AssemblyVersion encountered problem  " 
		-body "Exiting CVS_Sync since there was a problem updating a AssemblyInfo.cs file." \
		-to $Notify
		exit -1
	fi

done

cd "$jfong_clean/Performance Optimizer Administration"
error_count=$error_count+$?
cp ./AssemblyInfo.cs ./AssemblyInfo.temp
error_count=$error_count+$?
sed '1,$s^assembly: AssemblyVersion("1.0.0.0")^assembly: AssemblyVersion("'$Release_Number'.'$Build_Number'")^' < ./AssemblyInfo.temp > ./AssemblyInfo.temp1
error_count=$error_count+$?
sed '1,$s^assembly: AssemblyFileVersion("1.0.0.0")^assembly: AssemblyFileVersion("'$Release_Number'.'$Build_Number'")^' < ./AssemblyInfo.temp1 > ./AssemblyInfo.temp2
error_count=$error_count+$?
sed '1,$s^AssemblyCopyright("Copyright \©  2009-2010")^AssemblyCopyright("'"$copyRight"'")^' < ./AssemblyInfo.temp2 > ./AssemblyInfo.cs
error_count=$error_count+$?
diff AssemblyInfo.cs AssemblyInfo.temp         # > /dev/null
if [ $? == 0 ];
then
	blat -subject "NightlyBuild: Modification of the Performance Optimizer AssemblyInfo.cs failed" \
	-body "Substitution for local environment did not work. \\n Either CVS did not update or someone change the file.\n exiting so we can figure it out" \
	-to $Notify
	exit -1
fi

cp ./Version.cs ./Version.temp
error_count=$error_count+$?
sed '1,$s^public static int buildNumber = 1;^public static int buildNumber = '$Build_Number';^' < ./Version.temp > ./Version.temp1
error_count=$error_count+$?
sed '1,$s^public static string releaseNumber = "4.1";^public static string releaseNumber = "'$Release_Number'";^' < ./Version.temp1 > ./Version.cs
error_count=$error_count+$?
diff Version.cs Version.temp         # > /dev/null
if [ $? == 0 ];
then
	blat -subject "NightlyBuild: Modification of the Performance Optimizer Version.cs failed" \
	-body "Substitution for local environment did not work. \\n Either CVS did not update or someone change the file.\n exiting so we can figure it out" \
	-to $Notify
	exit -1
fi

if [ $error_count != 0 ];
then
	blat -subject "CVS_Sync: Copy consistent build number problem  " -body "Exiting CVS_Sync since there was a problem with the AssemblyInfo build number copy." -to $Notify
	exit -1
fi


cd "$jfong_clean/Acorn/Acorn"
error_count=$error_count+$?
cp ./Acorn.csproj ./Acorn.temp1
error_count=$error_count+$?
sed '1,$s^Birst Administration, Version=1.0.0.0^Birst Administration, Version='$Release_Number'.'$Build_Number'^' < ./Acorn.temp1 > ./Acorn.temp2
error_count=$error_count+$?
sed '1,$s^LogicalExpression, Version=1.0.0.0^LogicalExpression, Version='$Release_Number'.'$Build_Number'^' < ./Acorn.temp2 > ./Acorn.csproj
error_count=$error_count+$?
diff Acorn.csprog Acorn.temp1         # > /dev/null
if [ $? == 0 ];
then
	blat -subject "NightlyBuild: Modification of the Acorn Acorn.csprog failed" \
	-body "Substitution for local environment did not work. \\n Either CVS did not update or someone change the file.\n exiting so we can figure it out" \
	-to $Notify
	exit -1
fi

cp ./Version.cs ./Version.temp
error_count=$error_count+$?
sed '1,$s^public static int buildNumber = 1;^public static int buildNumber = '$Build_Number';^' < ./Version.temp > ./Version.temp1
error_count=$error_count+$?
sed '1,$s^public static string releaseNumber = "4.1";^public static string releaseNumber = "'$Release_Number'";^' < ./Version.temp1 > ./Version.cs
error_count=$error_count+$?
diff Version.cs Version.temp         # > /dev/null
if [ $? == 0 ];
then
	blat -subject "NightlyBuild: Modification of the Acorn Version.cs failed" \
	-body "Substitution for local environment did not work. \\n Either CVS did not update or someone change the file.\n exiting so we can figure it out" \
	-to $Notify
	exit -1
fi

if [ $error_count != 0 ];
then
	blat -subject "CVS_Sync: Copy consistent build number problem  " -body "Exiting CVS_Sync since there was a problem with the Acorn/Acorn.csproj build number copy." -to $Notify
	exit -1
fi

cd "$jfong_clean/AcornTestConsole/AcornTestConsole"
error_count=$error_count+$?
cp ./AcornTestConsole.csproj ./AcornTestConsole.temp1
error_count=$error_count+$?
sed '1,$s^Acorn, Version=1.0.0.0^Acorn, Version='$Release_Number'.'$Build_Number'^' < ./AcornTestConsole.temp1 > ./AcornTestConsole.temp2
error_count=$error_count+$?
sed '1,$s^LogicalExpression, Version=1.0.0.0^LogicalExpression, Version='$Release_Number'.'$Build_Number'^' < ./AcornTestConsole.temp2 > ./AcornTestConsole.csproj
error_count=$error_count+$?
diff AcornTestConsole.csprog AcornTestConsole.temp1         # > /dev/null
if [ $? == 0 ];
then
	blat -subject "NightlyBuild: Modification of the Acorn AcornTestConsole.csprog failed" \
	-body "Substitution for local environment did not work. \\n Either CVS did not update or someone change the file.\n exiting so we can figure it out" \
	-to $Notify
	exit -1
fi

if [ $error_count != 0 ];
then
	blat -subject "CVS_Sync: Copy consistent build number problem  " -body "Exiting CVS_Sync since there was a problem with the AcornTestConsole/AcornTestConsole.csproj build number copy." -to $Notify
	exit -1
fi

cd "$jfong_clean/MoveSpace/MoveSpace"
error_count=$error_count+$?
cp ./MoveSpace.csproj ./MoveSpace.temp1
error_count=$error_count+$?
sed '1,$s^Acorn, Version=1.0.0.0^Acorn, Version='$Release_Number'.'$Build_Number'^' < ./MoveSpace.temp1 > ./MoveSpace.temp2
error_count=$error_count+$?
sed '1,$s^Birst Administration, Version=2.0.3426.41490^Birst Administration, Version='$Release_Number'.'$Build_Number'^' < ./MoveSpace.temp2 > ./MoveSpace.csproj
error_count=$error_count+$?
diff MoveSpace.csprog MoveSpace.temp1         # > /dev/null
if [ $? == 0 ];
then
	blat -subject "NightlyBuild: Modification of the MoveSpace.csprog failed" \
	-body "Substitution for local environment did not work. \\n Either CVS did not update or someone change the file.\n exiting so we can figure it out" \
	-to $Notify
	exit -1
fi

if [ $error_count != 0 ];
then
	blat -subject "CVS_Sync: Copy consistent build number problem  " -body "Exiting CVS_Sync since there was a problem with the MoveSpace/MoveSpace.csproj build number copy." -to $Notify
	exit -1
fi

#
#	Update the Dataconductor properties version number
#

cd $jfong_clean/DataConductor/DataConductor/src/com/birst/dataconductor/resources
cp ./DataConductorApp.properties ./DataConductorApp.temp
sed '1,$s^Application.version = 5.1.0.1^Application.version = '$Release_Number.$Build_Number'^' < ./DataConductorApp.temp > ./DataConductorApp.properties
diff DataConductorApp.properties DataConductorApp.temp         # > /dev/null
if [ $? == 0 ];
then
	blat -subject "NightlyBuild: Modification of the DataConductorApp.properties failed" \
	-body "Substitution for local environment did not work. \\n Either CVS did not update or someone change the file.\n exiting so we can figure it out" \
	-to $Notify
	exit -1
fi

if [ $? != 0 ];
then
	blat -subject "CVS_Sync: Copy consistent build number problem  " -body "Exiting CVS_Sync since there was a problem with the DataConductorApp.properties build number copy." -to $Notify
	exit -1
fi

#
#	Update the BirstConnect version number
#

cd $jfong_clean/Acorn/Acorn
cp ./Web.config ./Web.temp
sed '1,$s^BirstConnectProductVersion" value="1.0.0.0^BirstConnectProductVersion" value="'$Release_Number.$Build_Number'^' < ./Web.temp > ./Web.config
diff ./Web.config ./Web.temp         # > /dev/null
if [ $? == 0 ];
then
	blat -subject "NightlyBuild: Modification of the Acorn Web.config failed" \
	-body "Substitution for local environment did not work. \\n Either CVS did not update or someone change the file.\n exiting so we can figure it out" \
	-to $Notify
	exit -1
fi

if [ $? != 0 ];
then
	blat -subject "CVS_Sync: Copy consistent build number problem  " -body "Exiting CVS_Sync since there was a problem with the BirsConnect build number copy." -to $Notify
	exit -1
fi

#
#	Update the DashboardApp version number
#
error_count=0
cd $jfong_clean/DashboardApp/homeApp/view
cp ./Viewport.js ./Viewport.temp
sed '1,$s^All rights reserved.   1.0.0.0^All rights reserved.   '$Release_Number.$Build_Number'^' < ./Viewport.temp > ./Viewport.temp2
error_count=$error_count+$?
sed '1,$s^Copyright (c) 2008-2012 Birst. All rights reserved. ^'"$copyRight"'^' < ./Viewport.temp2 > ./Viewport.js
error_count=$error_count+$?
diff ./Viewport.js ./Viewport.temp         # > /dev/null
if [ $? == 0 ] || [ $error_count != 0 ];
then
	blat -subject "NightlyBuild: Modification of the DashboardApp Viewport.js failed" \
	-body "Substitution of version and build number did not work. \\n Either CVS did not update or someone change the file.\n exiting so we can figure it out" \
	-to $Notify
	exit -1
fi

if [ $? != 0 ];
then
	blat -subject "CVS_Sync: Copy consistent build number problem  " -body "Exiting CVS_Sync since there was a problem with the BirsConnect build number copy." -to $Notify
	exit -1
fi

#
#  FlexAdhoc - need to change the MXMLC execution to failonerror="false"
#	per Rick.
#

cd $jfong_clean/FlexAdhoc/build

cp build.xml build.temp
sed '1,$s^failonerror=\"true\"^failonerror=\"false\"^' < ./build.temp > ./build.xml
diff build.xml build.temp         # > /dev/null
if [ $? == 0 ];
then
	blat -subject "NightlyBuild: Modification of the FlexAdhoc build.xml failed" \
	-body "Substitution for local environment did not work. \\n Either CVS did not update or someone change the file.\n exiting so we can figure it out" \
	-to $Notify
	exit -1
fi

if [ $? != 0 ];
then
	blat -subject "CVS_Sync: FlexAdhoc build.xml problem  " -body "there were problems updating the build.xml from failonerror to false." -to $Notify
	exit -1
fi
 
#
#  SourceAdmin - need to change the MXMLC execution to failonerror="false"
#	per Rick.
#

cd $jfong_clean/SourceAdmin/build

cp build.xml build.temp
sed '1,$s^failonerror=\"true\"^failonerror=\"false\"^' < ./build.temp > ./build.xml
diff build.xml build.temp         # > /dev/null
if [ $? == 0 ];
then
	blat -subject "CVS_sync: Modification of the SourceAdmin build.xml failed" \
	-body "Substitution for local environment did not work. \\n Either CVS did not update or someone change the file.\n exiting so we can figure it out" \
	-to $Notify
	exit -1
fi

if [ $? != 0 ];
then
	blat -subject "CVS_Sync: SourceAdmin build.xml problem  " -body "there were problems updating the build.xml from failonerror to false." -to $Notify
	exit -1
fi
 

#
#	Update Help version
#
#
#
#	Create Help temp directory
#
#
help_version=( "BXE,BXEHelp" \
		"Full,Help" )

cd $jfong_clean/Help

echo "start loop"
for var in "${help_version[@]}"
do
	echo var	    "${var}"
	str_length=${#var}
	comma_pos=`expr index "${var}" ','`
	Help_type=${var:0:$comma_pos-1}
	Help_file=${var:$comma_pos}

	if [ ! -d $jfong_clean/Help/dist/$Help_type ];
	then
		mkdir -p $jfong_clean/Help/dist/$Help_type 
		chmod -R 777 $jfong_clean/Help/dist/$Help_type
	else
		rm -rf $jfong_clean/Help/dist/$Help_type/*
		chmod -R 777 $jfong_clean/Help/dist/$Help_type
	fi


	if [ ! -d $jfong_clean/dist/$Help_type ];
	then
		set VarMailSubject="CVS_sync of %Build_Tag% on %COMPUTERNAME%:Cannot create dist/$Help_type directory"
		set VarMailText="Cannot create dist/$Help_type directory, please create."
		goto BADEXIT:
	fi


	cd $jfong_clean/Help

	/cygdrive/c//Program\ Files\ \(x86\)/WinZip/WZUNZIP.EXE -d -ybc -o -yo ./${Help_file}.zip ./dist/$Help_type
	if [ $? != 0 ];
	then

		blat -subject "CVS_Sync: Extract of welcome.htm file failed  " \
		-body "Exiting nightly build since there was a problem extracting the welcome.htm Help file." \
		-to $Notify
		exit -1
	fi

	cd $jfong_clean/Help/dist/$Help_type
	cp welcome.htm welcome.temp
	if [ $? != 0 ];
	then

		blat -subject "CVS_Sync: Extract of welcome.htm file failed  " \
		-body "Exiting nightly build since there was a problem extracting the welcome.htm Help file." \
		-to $Notify
		exit -1
	fi

	sed '1,$s^Version 1.0.0.0^Version '$Release_Number'.'$Build_Number'^' < ./welcome.temp > ./welcome.htm
	if [ $? != 0 ];
	then
		blat -subject "CVS_Sync: Help welcome.htm problem  " -body "there were problems updating the welcome.htm with the current release number." -to $Notify
		exit -1
	fi

	diff --strip-trailing-cr welcome.htm welcome.temp         # > /dev/null
	if [ $? == 0 ];
	then
		blat -subject "CVS_Sync: Modification of the $Help_type welcome.htm failed" \
		-body "Substitution for current build version did not work. \\n Either CVS did not update or someone change the file.\n exiting so we can figure it out" \
		-to $Notify
		exit -1
	fi


done


exit 0
