ECHO ON

set VarNotify=jfong@birst.com
set VarMailText=Text message as part of the Mail
set VarMailSubject=Build AccountMigrationTool
set FTP_files=0

:PROCESS_INPUT

if "%1"=="-n" goto SET_NOTIFICATION:
if "%1"=="-b" goto SET_BRANCH:
if "%1"=="-f" goto SET_FLAG:
if "%1"=="-t" goto SET_TIMESTAMP:
if "%1"=="" goto PROCESS_INPUT_DONE:
set temp=%1
if NOT "%temp:~0,1%"=="-" goto ADD_NOTIFICATION:

goto PROCESS_INPUT_ERROR:

:SET_TIMESTAMP
shift
set TimeStamp=%1
goto PROCESS_INPUT_LOOP:

:SET_BRANCH
shift
set Branch_name=%1
goto PROCESS_INPUT_LOOP:

:SET_FLAG
shift
set FTP_files=%1
echo %FTP_files%
goto PROCESS_INPUT_LOOP:


:SET_NOTIFICATION
shift
set VarNotify=%1
goto PROCESS_INPUT_LOOP:

:ADD_NOTIFICATION
set VarNotify="%VarNotify%,%temp%"
echo %VarNotify%
goto PROCESS_INPUT_LOOP:

:PROCESS_INPUT_LOOP

shift
goto PROCESS_INPUT:

:PROCESS_INPUT_ERROR

echo ERROR: invalid input parameter
echo usage: DataConductorBuild.bat [-options]
echo where options include:
echo.
echo  -t ^<Time Stamp^> (optional)
echo 		Enter the Time Stamp for identifying the deployment
echo		the format is YYYYMMDDHHMM, or one will be generated.		
echo  -b ^<CVS Branch^> 
echo 		enter the cvs branch for the release e.g. HEAD, Birst_3_1_5_SP
echo  -n ^<email notification list^> comma separated (optional)
echo  		default=jfong@birst.com,4157301327@vtext.com
echo 		NOTE - multiple email addresses need to be separated by 
echo 			commas and enclosed in double quote
echo.

set VarMailText=Invalid arguement(s) provided. 
set VarMailSubject=DataConductorBuild:Process Arguements
goto BADEXIT

:PROCESS_INPUT_DONE

REM
REM We need a timestamp.  If it is not provided, we should create our own.
REM

set clean_dir=C:\clean_builds_%Branch_name%
cd %clean_dir%

echo -%clean_dir%-
for /l %%a in (1,1,31) do if "%clean_dir:~-1%"==" " set clean_dir=%clean_dir:~0,-1%
echo -%clean_dir%-
cd %clean_dir%
dir

REM
REM We need a timestamp.  If it is not provided, we should create our own.
REM

if "%TimeStamp%" neq "" ( goto TIMESTAMP_DEFINED: ) 

for /f "tokens=1-3 delims=:." %%a in ("%time%") do set hh_mm_stamp=%%a%%b
echo %hh_mm_stamp%
for /f "tokens=1-3 delims=\ " %%a in ("%date%") do set date_only=%%b
for /f "tokens=1-3 delims=/." %%a in ("%date_only%") do set teststamp=%%c%%a%%b
echo %teststamp%
set TimeStamp="%teststamp%%hh_mm_stamp%"
	
:TIMESTAMP_DEFINED
echo TimeStamp: %TimeStamp%

echo -%TimeStamp%-
for /l %%a in (1,1,31) do if "%TimeStamp:~-1%"==" " set TimeStamp=%TimeStamp:~0,-1%
echo -%TimeStamp%-

REM set TimeStamp=%TimeStamp:~0,14%
REM echo TimeStamp: -%TimeStamp%-

REM
REM Build AccountMigrationTool
REM

cd %clean_dir%
dir

:BUILD_ACORN

cd %clean_dir%\AccountMigrationTool
dir

cd %clean_dir%\AccountMigrationTool\AccountMigrationTool
if exist AccountMigrationTool.csproj goto IIS_SED: 
set VarMailText=Something is wrong, I can't find the AccountMigrationTool.csproj file. \n The CVS update or something else did not work
set VarMailSubject=AccountMigrationTool_build: CVS AccountMigrationTool.csproj file error
goto BADEXIT:

:IIS_SED
echo "AccountMigrationTool.csproj exist"
copy AccountMigrationTool.csproj AccountMigrationTool.temp
type AccountMigrationTool.temp | c:\cygwin\bin\sed.exe "s^<UseIIS>True</UseIIS>^<UseIIS>False</UseIIS>^" >AccountMigrationTool.csproj
del AccountMigrationTool.temp		# we've update the file, we can delete the older version.

cd %clean_dir%\AccountMigrationTool

devenv AccountMigrationTool.sln /clean
IF %ERRORLEVEL% LSS 1 goto CLEAN_DONE:
set VarMailText=Build had problems cleaning out build area. 
set VarMailSubject=AccountMigrationTool_build:clean errors
goto BADEXIT

:CLEAN_DONE
devenv AccountMigrationTool.sln /build
IF %ERRORLEVEL% LSS 1 goto BUILD_DONE:
set VarMailText=Build encountered problems.
set VarMailSubject=AccountMigrationTool_build:build problems. 
goto BADEXIT

:BUILD_DONE

set /a error_cnt=0
cd %clean_dir%\AccountMigrationTool\AccountMigrationTool\bin

"c:\Program Files\WinZip\WZZIP.EXE" -ex -r -p -a c:\SFOJFONG\builds\%TimeStamp%_Acorn\AccountMigrationTool.zip AccountMigrationTool.exe
if %ERRORLEVEL% GEQ 1 ( if %ERRORLEVEL% NEQ 12 ( goto WinZip_ERROR: ))
"c:\Program Files\WinZip\WZZIP.EXE" -ex -r -p -a c:\SFOJFONG\builds\%TimeStamp%_Acorn\AccountMigrationTool.zip *.dll
if %ERRORLEVEL% GEQ 1 ( if %ERRORLEVEL% NEQ 12 ( goto WinZip_ERROR: ))

goto PACKAGE_DONE:

:WinZip_Error
set VarMailText=There were problems Zipping the files into a zip file. 
set VarMailSubject=AccountMigrationTool_build:WinZip error
goto BADEXIT

:PACKAGE_DONE

rem
rem Now that we have the package, FTP the package up to colo1
rem 

rem 
rem  FTP build to the //smi/builds directory
rem 

cd %clean_dir%
echo open builds>upload.ftp
echo smibuilds>>upload.ftp
echo 5771builds!>>upload.ftp
echo verbose>>upload.ftp
echo prompt>>upload.ftp
echo lcd C:\SFOJFONG\builds\%TimeStamp%_Acorn>>upload.ftp
echo cd Nightly>>upload.ftp
rem	echo mkdir  %TimeStamp%_Acorn>>upload.ftp
echo cd  %TimeStamp%_Acorn>>upload.ftp
echo put AccountMigrationTool.zip>>upload.ftp
echo delete upload.ftp>>upload.ftp
echo prompt>>upload.ftp
echo quit>>upload.ftp	

ftp -s:upload.ftp  
IF %ERRORLEVEL% LSS 1 goto FTP_COLO1:
set VarMailText=There were problems FTPing the files over to smibuilds. 
set VarMailSubject=AccountMigrationTool_build:FTP error
goto BADEXIT


:FTP_COLO1
REM goto FTP_FTP1:
if %FTP_files%==0 goto BYPASS_FTP
cd %clean_dir%
echo open 192.168.1.161>upload.ftp
echo smibuilds>>upload.ftp
echo 5771builds!>>upload.ftp
echo verbose>>upload.ftp
echo prompt>>upload.ftp
echo lcd C:\SFOJFONG\builds\%TimeStamp%_Acorn>>upload.ftp
echo cd Nightly>>upload.ftp
REM	echo mkdir  %TimeStamp%_Acorn>>upload.ftp
echo cd  %TimeStamp%_Acorn>>upload.ftp
echo put AccountMigrationTool.zip>>upload.ftp
echo delete upload.ftp>>upload.ftp
echo prompt>>upload.ftp
echo quit>>upload.ftp	

ftp -s:upload.ftp  
IF %ERRORLEVEL% LSS 1 goto FTP_FTP1:
set VarMailText=There were problems FTPing the files over to colo1. 
set VarMailSubject=AccountMigrationTool_build:FTP error
goto BADEXIT

:FTP_FTP1:
REM ftp to Rackspace Enterprise
if %FTP_files%==0 goto BYPASS_FTP
cd %clean_dir%
echo open rsbuilds>upload.ftp
echo smibuilds>>upload.ftp
echo 5771builds!>>upload.ftp
echo verbose>>upload.ftp
echo prompt>>upload.ftp
echo lcd C:\SFOJFONG\builds\%TimeStamp%_Acorn>>upload.ftp
echo cd Nightly>>upload.ftp
REM	echo mkdir  %TimeStamp%_Acorn>>upload.ftp
echo cd  %TimeStamp%_Acorn>>upload.ftp
echo put AccountMigrationTool.zip>>upload.ftp
echo delete upload.ftp>>upload.ftp
echo prompt>>upload.ftp
echo quit>>upload.ftp	

ftp -s:upload.ftp  
IF %ERRORLEVEL% LSS 1 goto FTP_FTP2:
set VarMailText=There were problems FTPing the files over to FTP1. 
set VarMailSubject=AccountMigrationTool_build:FTP error
goto BADEXIT

:FTP_FTP2:
REM ftp to Rackspace Birst
if %FTP_files%==0 goto BYPASS_FTP
cd %clean_dir%
echo open 172.26.140.148>upload.ftp
echo smibuilds>>upload.ftp
echo 5771builds!>>upload.ftp
echo verbose>>upload.ftp
echo prompt>>upload.ftp
echo lcd C:\SFOJFONG\builds\%TimeStamp%_Acorn>>upload.ftp
echo cd Nightly>>upload.ftp
REM	echo mkdir  %TimeStamp%_Acorn>>upload.ftp
echo cd  %TimeStamp%_Acorn>>upload.ftp
echo put AccountMigrationTool.zip>>upload.ftp
echo delete upload.ftp>>upload.ftp
echo prompt>>upload.ftp
echo quit>>upload.ftp	

ftp -s:upload.ftp  
IF %ERRORLEVEL% LSS 1 goto FTP_DONE:
set VarMailText=There were problems FTPing the files over to FTP1. 
set VarMailSubject=AccountMigrationTool_build:FTP error
goto BADEXIT

:FTP_DONE
:BYPASS_FTP

cd %clean_dir%
dir

goto THE_END:


:BADEXIT
echo ------------------ Bad Exit -------------------------------------------
echo %VarMailText%
blat -to %VarNotify% -subject "%VarMailSubject%" -body "%VarMailText%"
rem *** if exist %VarCustProp% del %VarCustProp%
echo "This batch file failed."
exit 1

:THE_END

exit 0
