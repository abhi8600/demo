ECHO ON

set VarNotify=jfong@birst.com
set VarMailText=Text message as part of the Mail
set VarMailSubject=Build Acorn
set Path=c:\eclipse\plugins\apache-ant-1.7.1\bin;%Path%
set

:PROCESS_INPUT

if "%1"=="-n" goto SET_NOTIFICATION:
if "%1"=="-b" goto SET_BRANCH:
if "%1"=="-c" goto SET_CLEAN_DIR:
if "%1"=="-t" goto SET_TIMESTAMP:
if "%1"=="" goto PROCESS_INPUT_DONE:
set temp=%1
if NOT "%temp:~0,1%"=="-" goto ADD_NOTIFICATION:

goto PROCESS_INPUT_ERROR:

:SET_TIMESTAMP
shift
set TimeStamp=%1
goto PROCESS_INPUT_LOOP:

:SET_BRANCH
shift
set Branch_name=%1
goto PROCESS_INPUT_LOOP:

:SET_CLEAN_DIR
shift
set clean_dir=%1
goto PROCESS_INPUT_LOOP:

:SET_NOTIFICATION
shift
set VarNotify=%1
goto PROCESS_INPUT_LOOP:

:ADD_NOTIFICATION
set VarNotify="%VarNotify%,%temp%"
echo %VarNotify%
goto PROCESS_INPUT_LOOP:

:PROCESS_INPUT_LOOP

shift
goto PROCESS_INPUT:

:PROCESS_INPUT_ERROR

echo ERROR: invalid input parameter
echo usage: DataConductorBuild.bat [-options]
echo where options include:
echo.
echo  -t ^<Time Stamp^> (optional)
echo 		Enter the Time Stamp for identifying the deployment
echo		the format is YYYYMMDDHHMM, or one will be generated.		
echo  -b ^<CVS Branch^> 
echo 		enter the cvs branch for the release e.g. HEAD, Birst_3_1_5_SP
echo  -n ^<email notification list^> comma separated (optional)
echo  		default=jfong@birst.com,4157301327@vtext.com
echo 		NOTE - multiple email addresses need to be separated by 
echo 			commas and enclosed in double quote
echo.

set VarMailText=Invalid arguement(s) provided. 
set VarMailSubject=DataConductorBuild:Process Arguements
goto BADEXIT

:PROCESS_INPUT_DONE

REM
REM We need a timestamp.  If it is not provided, we should create our own.
REM

if "%TimeStamp%" neq "" ( goto TIMESTAMP_DEFINED: ) 
	
for /f "tokens=1-3 delims=:." %%a in ("%time%") do set hh_mm_stamp=%%a%%b
echo %hh_mm_stamp%
for /f "tokens=1-3 delims=\ " %%a in ("%date%") do set date_only=%%b
for /f "tokens=1-3 delims=/." %%a in ("%date_only%") do set teststamp=%%c%%a%%b
echo %teststamp%
set TimeStamp="%teststamp%%hh_mm_stamp%"
	
:TIMESTAMP_DEFINED
echo TimeStamp: %TimeStamp%

echo -%TimeStamp%-
for /l %%a in (1,1,31) do if "%TimeStamp:~-1%"==" " set TimeStamp=%TimeStamp:~0,-1%
echo -%TimeStamp%-

REM set TimeStamp=%TimeStamp:~0,14%
REM echo TimeStamp: -%TimeStamp%-

REM
REM Build Acorn DataConductor
REM

echo %JAVA_HOME%
set JAVA_HOME=/cygdrive/c/Program\ Files/Java/jdk1.6.0_10
rem set JAVA_HOME="c:\Program Files\Java\jdk1.6.0_10"
echo %JAVA_HOME%
#dir %JAVA_HOME%\jarsigner.exe
cd %clean_dir%

dir

:BUILD_ACORNADMIN_ACORN

cd %clean_dir%\DataConductor\DataConductor
dir
echo start Clean.

cmd /c ant clean 
IF %ERRORLEVEL% LSS 1 goto CLEAN_ACORNADMIN_DONE:
echo DataConductor problems: %ERRORLEVEL%
set VarMailText=Build had problems cleaing out build area. 
set VarMailSubject=AcornAdministration_build:clean errors
goto BADEXIT

:CLEAN_ACORNADMIN_DONE
REM set JAVA_HOME="c:\Program Files\Java\jdk1.5.0_10\bin"

cd %clean_dir%\DataConductor\DataConductor
dir

echo Clean done, starting build.
cmd /c ant
IF %ERRORLEVEL% LSS 1 goto BUILD_ACORNADMIN_DONE:
set VarMailText=Build encountered problems.
set VarMailSubject=AcornAdministration_build:build problems. 
goto BADEXIT

:BUILD_ACORNADMIN_DONE
echo Build done.

goto THE_END:


:BADEXIT
echo ------------------ Bad Exit -------------------------------------------
echo %VarMailText%
blat -to %VarNotify% -subject "%VarMailSubject%" -body "%VarMailText%"
rem *** if exist %VarCustProp% del %VarCustProp%
echo "This batch file failed."
exit 1

:THE_END

exit 0
