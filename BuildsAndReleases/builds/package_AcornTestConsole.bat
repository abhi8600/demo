REM
REM Package up the AcornTestConsole executables
REM
ECHO ON

set VarNotify=jfong@birst.com
set VarMailText=Text message as part of the Mail
set VarMailSubject=Build Acorn

echo VarNotify: %VarNotify%
echo VarMailText: %VarMailText%
echo VarMailSubject: %VarMailSubject%

:PROCESS_INPUT

if "%1"=="-n" goto SET_NOTIFICATION:
if "%1"=="-b" goto SET_BRANCH:
if "%1"=="-c" goto SET_CLEAN_DIR:
if "%1"=="-t" goto SET_TIMESTAMP:
if "%1"=="" goto PROCESS_INPUT_DONE:
set temp=%1
if NOT "%temp:~0,1%"=="-" goto ADD_NOTIFICATION:

goto PROCESS_INPUT_ERROR:

:SET_TIMESTAMP
shift
set TimeStamp=%1
goto PROCESS_INPUT_LOOP:

:SET_BRANCH
shift
set Branch_name=%1
goto PROCESS_INPUT_LOOP:

:SET_CLEAN_DIR
shift
set clean_dir=%1
goto PROCESS_INPUT_LOOP:

:SET_NOTIFICATION
shift
set VarNotify=%1
goto PROCESS_INPUT_LOOP:

:ADD_NOTIFICATION
set VarNotify="%VarNotify%,%temp%"
echo %VarNotify%
goto PROCESS_INPUT_LOOP:

:PROCESS_INPUT_LOOP

shift
goto PROCESS_INPUT:

:PROCESS_INPUT_ERROR

echo ERROR: invalid input parameter
echo usage: DataConductorBuild.bat [-options]
echo where options include:
echo.
echo  -t ^<Time Stamp^> (optional)
echo 		Enter the Time Stamp for identifying the deployment
echo		the format is YYYYMMDDHHMM, or one will be generated.		
echo  -b ^<CVS Branch^> 
echo 		enter the cvs branch for the release e.g. HEAD, Birst_3_1_5_SP
echo  -n ^<email notification list^> comma separated (optional)
echo  		default=jfong@birst.com,4157301327@vtext.com
echo 		NOTE - multiple email addresses need to be separated by 
echo 			commas and enclosed in double quote
echo.

set VarMailText=Invalid arguement(s) provided. 
set VarMailSubject=DataConductorBuild:Process Arguements
goto BADEXIT

:PROCESS_INPUT_DONE

REM
REM We need a timestamp.  If it is not provided, we should create our own.
REM

cd %clean_dir%

echo -%clean_dir%-
for /l %%a in (1,1,31) do if "%clean_dir:~-1%"==" " set clean_dir=%clean_dir:~0,-1%
echo -%clean_dir%-
cd %clean_dir%

REM
REM We need a timestamp.  If it is not provided, we should create our own.
REM

if "%TimeStamp%" neq "" ( goto TIMESTAMP_DEFINED: ) 
	
for /f "tokens=1-3 delims=:." %%a in ("%time%") do set hh_mm_stamp=%%a%%b
echo %hh_mm_stamp%
for /f "tokens=1-3 delims=\ " %%a in ("%date%") do set date_only=%%b
for /f "tokens=1-3 delims=/." %%a in ("%date_only%") do set teststamp=%%c%%a%%b
echo %teststamp%
set TimeStamp="%teststamp%%hh_mm_stamp%"
	
:TIMESTAMP_DEFINED
echo TimeStamp: %TimeStamp%

cd %clean_dir%\AcornTestConsole\AcornTestConsole\bin\Debug

"c:\Program Files\WinZip\WZZIP.EXE" -ex -r -p -a C:\shared\Builds\%TimeStamp%_Acorn\AcornTestConsole.zip Acorn.dll AcornTestConsole.exe AjaxControlToolkit.dll EO.Web.dll Ionic.Utils.Zip.dll "SMI Administration.exe" log4net.dll log4net.xml

REM
REM Package up the command and results files
REM

cd %clean_dir%\AcornTestConsole

"c:\Program Files\WinZip\WZZIP.EXE" -ex -P -r -a C:\shared\Builds\%TimeStamp%_Acorn\AcornTestConsole_ref.zip TestInput* Test* TestRefOutput\*