﻿#!/usr/bin/bash
#
PATH=/cygdrive/c/eclipse/plugins/apache-ant-1.7.1/bin:$PATH; export PATH

function Parameter_Help
{
echo "usage: DataConductorBuild_jar [-options]"
			echo "where options include:"
			echo " -b <branch> "
			echo "		Enter the branch that we will be tagging on."
			echo " 		If not branch is supplied, we default to HEAD"
			echo " -n <email notification list> comma separated"
			echo " 		default=jfong@birst.com,4157301327@vtext.com"
			echo " -t <build tag> "
			echo " 		Usually, this is the date and time  with or without a "
			echo " 		release number.  For example, 200802140805 "
			echo " 		is Feb 14th, 2008 8:05 a.m.  Or if it for 2.2.0, "
			echo "		the tag would be 200802140805_220"
			echo ""
}
# Define variables
# 	and clear them out first. 
#
Notify=jfong@birst.com
build_tag=""
Branch_Name=""
jfong_clean=""
CVS_workspace=""
echo $JAVA_HOME
#	JAVA_HOME=/cygdrive/c/Program\ Files/Java/jdk1.6.0_27
JAVA_HOME=/cygdrive/c/Program\ Files/Java/jdk1.7.0
#	CLASSPATH=".;C:\Program Files\Java\jdk1.6.0_27\lib;C:\Program Files (x86)\Java\jre6\lib\ext\QTJava.zip"
CLASSPATH=".;C:\Program Files\Java\jdk1.7.0\lib;C:\Program Files (x86)\Java\jre6\lib\ext\QTJava.zip"
echo $JAVA_HOME
echo $CLASSPATH
echo $PATH

while getopts ":d:b:c:n:t:" opts; do
        case "$opts" in
		d ) echo "-d $OPTARG"
			DEBUG=$OPTARG
		 	export DEBUG
			;;
		b ) echo "-b $OPTARG" 
			Branch_Name=$OPTARG
			echo $Branch_Name
			;;
		c ) echo "-c $OPTARG"
			jfong_clean="$(cygpath -u -p -a "$OPTARG")"
			echo $jfong_clean
			;;
		n ) echo "-n $OPTARG" 
			Notify=$OPTARG
			export Notify 
			;;
		t ) echo "-t $OPTARG"			# Since I'm creating the build, I am generating the build tag.  
			build_tag=$OPTARG;	
			export build_tag
			;;
		*  ) Parameter_Help 
			exit 1 ;;
	esac
	echo one value $1
done

echo "Branch = " $Branch_Name "-"
echo "workspace = " $jfong_clean

if [ -z $build_tag ] || [ -z $Branch_Name ];	#  Make sure we have our parameters
then
	Parameter_Help
	exit 1
fi

env

#
# Now clean DataConductor
# 

cd $jfong_clean/DataConductor/DataConductor 		# goto my clean build area

#	ant -Dplatforms.JDK_1.6.home="c:\Program Files\Java\jdk1.6.0_27" -verbose  clean 
ant -Dplatforms.JDK_1.7.home="c:\Program Files\Java\jdk1.7.0" -verbose  clean 

if [ $? == 0 ]; 
then
	echo "DataConductor: Ant clean worked"
else
	blat -subject "DataConductor_jar: Ant clean had problems" \
	-body "Errors were generated when we attempted to do a clean" \
	-to $Notify
	exit -1
fi


#
# Now build DataConductor 
# 

cd $jfong_clean/DataConductor/DataConductor 		# goto my clean build area

#	ant -Dplatforms.JDK_1.6.home="c:\Program Files\Java\jdk1.6.0_27" -verbose jar
ant -Dplatforms.JDK_1.7.home="c:\Program Files\Java\jdk1.7.0" -verbose  jar 

if [ $? == 0 ]; 
then
	echo "DataConductor: Ant build worked"
else
	blat -subject "DataConductor_jar: Ant build had problems" \
	-body "Errors were generated when we attempted to do a build DataConductor" \
	-to $Notify
	exit -1
fi


exit 0
