#!/usr/bin/bash

echo on
#	timestamp=`date +%Y%m%d%H%M_220`;		export timestamp
timestamp=`date +%Y%m%d%H%M`;			export timestamp
nwtest

/usr/bin/bash -x  /cygdrive/c/workspace_HEAD/BuildsAndReleases/builds/NightlyBuilds.sh \
	-h /cygdrive/c/CustomerInstalls/NW/NWTest	\
	-p "7120"					\
	-t "$timestamp"					\
	-b "HEAD"					\
1>/cygdrive/c/SFOJFONG/Builds/Logs/nightly_$timestamp.log 2>&1

#
# Build Acorn also
#

cmd /c "c:\workspace_HEAD\BuildsAndReleases\builds\AcornBuild.bat" "$timestamp" > "c:\SFOJFONG\Builds\Logs\acorn_$timestamp.log" 2>&1

#
# Deploy Acorn ( we have a scheduler task for that now)
#
#
#cd /cygdrive/c/workspace_HEAD/BuildsAndReleases/deployment
#
# cmd /c "c:\workspace_HEAD\BuildsAndReleases\deployment\deploy_acorn.bat" $timestamp $timestamp > "c:\SFOJFONG\Builds\Logs\deploy_acorn_$timestamp.log" 2>&1
#
#cmd /c "c:\workspace_HEAD\BuildsAndReleases\deployment\deploy_acorn.bat" -s $timestamp -a "c:\SMI" -p 6100 -r Nightly -t $timestamp > "c:\SFOJFONG\Builds\Logs\deploy_acorn_$timestamp.log" 2>&1
