#!/usr/bin/bash

echo on
timestamp=`date +%Y%m%d%H%M_230`;		export timestamp
#	timestamp=`date +%Y%m%d%H%M`;			export timestamp
nwtest

/usr/bin/bash -x  /cygdrive/c/workspace/BuildsAndReleases/builds/NightlyBuilds.sh \
	-h /cygdrive/c/CustomerInstalls/NW/NWDev	\
	-p "7100"					\
	-t "$timestamp"					\
	-b "SMI_2_3_0_SP"					\
1>/cygdrive/c/SFOJFONG/Builds/logs/nightly_$timestamp.log 2>&1
