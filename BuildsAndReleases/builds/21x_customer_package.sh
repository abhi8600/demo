#!/usr/bin/bash

# Quick script to package the user files for 2.1.x   
# There was a problem with how we were creating our 2.1.x packages.  Some of the 
# customer files was included in the platform package.   This created a situation 
# where we had to repackage the platform package at a later time when the customer 
# files were ready for release.  
#
# This script takes an existing platform build and recreates the Customer specific 
# package with the update customer files.  
# 
# NOTE - this uses Jerome's default build areas for create the new package.  
#	Look at the definition for jfong_share and jfong_clean.
#

DEBUG=0;					export DEBUG
nightly_bin=/cygdrive/c/workspace_HEAD/BuildsAndReleases/builds; 		export nightly_bin
jfong_share=/cygdrive/c/shared/Builds;		export jfong_share
jfong_clean=/cygdrive/c/clean_builds_2			export jfong_clean
Notify="jfong@birst.com" export Notify		#email list
declare -i error_count=0
build_tag=""
Cust_Name=""
CVS_Tag=""

while getopts ":d:s:n:v:t:" opts; do
        case "$opts" in
		d ) echo "-d $OPTARG"
			DEBUG=$OPTARG
		 	export DEBUG
			;;
		s ) echo "-s $OPTARG" 
			Cust_Name=$OPTARG
			export Cust_Home
			;;
		v ) echo "-v $OPTARG" 
			CVS_Tag=$OPTARG
			export CVS_Tag 
			;;
		t ) echo "-t $OPTARG"			# Since I'm creating the build, I am generating the build tag.  
			build_tag=$OPTARG;	
			export build_tag
			;;
		n ) echo "-n $OPTARG" 
			Notify=$OPTARG
			export Notify 
			;;
		*  ) echo "usage: deploy [-options]"
			echo "where options include:"
			echo " -s <customer name> "
			echo "		Enter the ID of the customer that we want build customized files for."
			echo " 		e.g. RBC for RBC DainRauscher, FMR for Fidelity, etc."
			echo " -v <cvs tag>"
			echo " 		Enter the CVS tag to be used for pulling down the Customer files."
			echo " -n <email notification list> comma separated"
			echo " 		default=jfong@birst.com,4157301327@vtext.com"
			echo " -t <build tag> "
			echo " 		If you are getting files from Releases/Platform, enter a Release Number e.g. 2.0.0"
			echo "		If you are getting files from Nightly, enter a date and time e.g. 200705230130"
			exit 1 ;;
	esac
	echo one value $1
done

if [ -z $Cust_Name ] || [ -z $CVS_Tag ]  || [ -z $build_tag ] # 	|| [ -z $Release_Dir ] || [ -z $Period_Id ]; 
then
	echo "usage: 21x_customer_package.sh [-options]"
	echo "where options include:"
	echo " -s <customer name> "
	echo "		Enter the ID of the customer that we want build customized files for."
	echo " 		e.g. RBC for RBC DainRauscher, FMR for Fidelity, etc."
	echo " -v <cvs tag>"
	echo " 		Enter the CVS tag to be used for pulling down the Customer files."
	echo " -n <email notification list> comma separated"
	echo " 		default=jfong@birst.com,4157301327@vtext.com"
	echo " -t <build tag> "
	echo " 		If you are getting files from Releases/Platform, enter a Release Number e.g. 2.0.0"
	echo "		If you are getting files from Nightly, enter a date and time e.g. 200705230130"
	exit 1 
fi 



#
# Now Export my tag to create my build.
# 

if [ $DEBUG -eq 0 ]; then 
	cd $jfong_clean				# goto my clean build area
	rm -rf SMI				# clean out the old files.  

	/cygdrive/c/Program\ Files/CVSNT/cvs -d :pserver:jfong:jfong123@SMISRC:/CVS export -r $CVS_Tag  SMI 

	stat=$?
	echo CVS exit status $stat

	if [ $stat == 0 ]; 
	then
		echo "CVS export for SMI worked"
	else
		echo "CVS export for SMI had errors"
		exit -1
	fi
fi

#
# Package up the customer specific files.  
#	1. add the configuration files into SMIWeb.war
#	2. zip up all the customer specific files (repository.xml, dashboards, reports, etc.)
#
echo $Cust_Name
if [ ! -d $jfong_share/$build_tag/$Cust_Name ];
then
	mkdir $jfong_share/$build_tag/$Cust_Name
	if [ $? != 0 ];
	then
		echo "Problem creating the Customer specific subdirectory" \
		| mutt -s "Package_Cust: Error creating customer subdirectory." $Notify
	fi
fi
#
# Make a copy of the current SMIWeb.war file
#
cp $jfong_share/$build_tag/SMIWeb.war $jfong_share/$build_tag/$Cust_Name/SMIWeb.war
if [ $? == 0 ];
then
	echo "Create a new copy of the SMIWeb.war file"
else
	echo "Either SMIWeb.war does not exist or I am having problems copying to the new subdirectory." \
	| mutt -s "Package_Cust: Problems copying the SMIWeb.war file." $Notify
fi

#
# zip up the files into the proper directory structure.  
# 1. SMIWeb.war
cd $jfong_clean/SMI/Customers/$Cust_Name/Code/WebApp/web
ls -la 

#
# zip up the WEB-INF customizations, but exclude the web.xml.  
# All web customization are done in customer.properties now
#
rm -rf ./WEB-INF/web.xml
zip -r $jfong_share/$build_tag/$Cust_Name/SMIWeb.war ./WEB-INF
error_count=error_count+$?
echo $error_count

# zip up the logo file
zip -r $jfong_share/$build_tag/$Cust_Name/SMIWeb.war ./images
error_count=error_count+$?
echo $error_count

# zip upt he customer.css.  However it needs to be at the top level, 
# not under the style directory.
cd style/
zip $jfong_share/$build_tag/$Cust_Name/SMIWeb.war ./customer.css
error_count=error_count+$?
echo $error_count

if [ $error_count -ne 0 ];
then
	echo "Either zip had problems adding files to the SMIWeb.war file or I'm missing some directories" \
	| mutt -s "Package_Cust: Problems adding " $Cust_Name " files SMIWeb.war." $Notify
	exit 1
fi

#
# 2. <Cust_Name>.zip
#
error_count=0

cd $jfong_clean/SMI/Customers/$Cust_Name//Code/WebApp
error_count=error_count+$?
echo $error_count

pwd
zip -r $jfong_share/$build_tag/$Cust_Name/$Cust_Name.zip ./catalog
error_count=error_count+$?
echo $error_count

pwd
zip -r $jfong_share/$build_tag/$Cust_Name/$Cust_Name.zip ./conf
error_count=error_count+$?
echo $error_count

if [ $error_count -ne 0 ];
then
	echo "Either we had problems creating the zip file or we do not have privilege t create the file" \
	| mutt -s "Package_Cust: Problems creating the " $Cust_Name".zip file." $Notify
	exit
fi

