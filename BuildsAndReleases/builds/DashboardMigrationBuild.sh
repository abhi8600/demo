#!/usr/bin/bash
#
ANT_OPTS="-Xms768m -Xmx768m -XX:MaxPermSize=512m";	export ANT_OPTS

function Parameter_Help
{
echo "usage: deploy [-options]"
			echo "where options include:"
			echo " -c <clean working directory>"
			echo "		This is the clean area we will be exporting "
			echo "		and building from.  Everything will be deleted "
			echo "		from this directory tree before we start"
			echo "		e.g. \c\clean_build_SMI_2_2_0_SP"
			echo " -n <email notification list> comma separated"
			echo " 		default=jfong@birst.com,4157301327@vtext.com"
			echo " -t <build tag> "
			echo " 		Usually, this is the date and time  with or without a "
			echo " 		release number.  For example, 200802140805 "
			echo " 		is Feb 14th, 2008 8:05 a.m.  Or if it for 2.2.0, "
			echo "		the tag would be 200802140805_220"
}
# Define variables
# 	and clear them out first. 
#
Notify=jfong@birst.com
build_tag=""
Branch_Name=""
jfong_clean="/cygdrive/c/clean_builds_HEAD"
CVS_workspace=""
FTP_files=0;					export FTP_files

while getopts ":c:f:n:t:" opts; do
        case "$opts" in
		c ) echo "-c $OPTARG"
			jfong_clean="$(cygpath -u -p -a "$OPTARG")"
			echo $jfong_clean
			;;
		f ) echo "-f $OPTARG" 
			FTP_files=$OPTARG
			export FTP_files
			echo $FTP_files
			;;
		n ) echo "-n $OPTARG" 
			Notify=$OPTARG
			export Notify 
			;;
		t ) echo "-t $OPTARG"			# Since I'm creating the build, I am generating the build tag.  
			build_tag=$OPTARG;	
			export build_tag
			;;
		*  ) Parameter_Help 
			exit 1 ;;
	esac
	echo one value $1
done

echo "workspace = " $jfong_clean

if [ -z $build_tag ] || [ -z $jfong_clean ];	#  Make sure we have our parameters
then
	Parameter_Help
	exit 1
fi
	
cd $jfong_clean/DashboardMigration
pwd

cd $jfong_clean/DashboardMigration
if [ -a .classpath ]; 
then
	echo ".classpath exist"
	cp ./.classpath ./.classpath_temp
else
	echo "Something is wrong, I can't find the .classpath file. \n The CVS update or something else did not work" \
		| mutt -s "DashboarMigrationBuild: CVS .classpath file error" $Notify
	exit -1
fi

sed '1,$ s^src" path="\/SMI^src" path="c:\/clean_builds_HEAD\/SMI^' <./.classpath_temp >./.classpath_temp2

sed '1,$ s^\/SMI\/^c:\/clean_builds_HEAD\/SMI\/^g' <./.classpath_temp2 >./.classpath
rm ./.classpath_temp		# we've update the file, we can delete the older version.
rm ./.classpath_temp_2		# we've update the file, we can delete the older version.


ant
stat=$?
echo DashboardMigration build exit status $stat

if [ $stat == 0 ]; 
then
	echo "DashboardMigration ant all worked"
else
	echo "DashboardMigration ant all had errors" \
        | mutt -s "DashboardMigration: ant all" $Notify
	exit -1
fi

cd $jfong_clean/DashboardMigration
zip -r DashboardMigration.zip .
if [ $? != 0 ];
then
	echo "Either zip had problems adding files to the DashboardMigration.zip file or I'm missing some directories" \
	| mutt -s "DashboardMigration: Problems adding  files SMIWeb.war." $Notify
	exit 1
fi

#
# FTP build to the //smi/builds directory
#
cd $jfong_clean/DashboardMigration
echo "open builds" >upload.ftp
echo "smibuilds" >>upload.ftp
echo "5771builds!" >>upload.ftp
echo "verbose" >>upload.ftp
echo "prompt" >>upload.ftp
echo "cd Nightly" >>upload.ftp
echo "cd " $build_tag >>upload.ftp
echo "mput DashboardMigration.zip" >>upload.ftp
echo "delete upload.ftp" >>upload.ftp
echo "prompt" >>upload.ftp
echo "quit" >>upload.ftp

echo "Starting FTP to \smi\Builds"
ftp -s:upload.ftp  
if [ $? == 0 ]; 
then
	echo "upload of log files was successful"  
else
	echo "Exiting since FTP was not successful in transfering the zipped up files.  " \
		| mutt -s "NightlyBuild: Unable to ftp the files to SMI" $Notify
	exit -1
fi

echo "FTP to \smi\Builds is done"
rm -rf  upload.ftp

if [ $FTP_files -eq 1 ]; 
then	
	# bypass colo since most of the customer stuff is atRackspace now
	# FTP build to the Colo1 build directory
	# 
	cd $jfong_clean/DashboardMigration
	echo "open 192.168.1.161" >upload.ftp
	echo "smibuilds" >>upload.ftp
	echo "5771builds!" >>upload.ftp
	echo "verbose" >>upload.ftp
	echo "prompt" >>upload.ftp
	echo "cd Nightly" >>upload.ftp
	echo "cd " $build_tag >>upload.ftp
	echo "mput DashboardMigration.zip" >>upload.ftp
	echo "delete upload.ftp" >>upload.ftp
	echo "prompt" >>upload.ftp
	echo "quit" >>upload.ftp
	
	echo "Starting FTP to Colo"
	ftp -s:upload.ftp  
	if [ $? == 0 ]; 
	then
		echo "upload of log files was successful"  
	else
		echo "Exiting since FTP was not successful in transfering the zipped up files.  " \
			| mutt -s "Nightlybuild: Unable to ftp the files to colo1" $Notify
		exit -1
	fi

	echo "FTP to Colo is done"
	rm -rf  upload.ftp

	#
	# FTP build to the FTP1 build directory
	#
	nslookup rsbuilds 
	cd $jfong_clean/DashboardMigration
	echo "open rsbuilds" >upload.ftp
	echo "smibuilds" >>upload.ftp
	echo "5771builds!" >>upload.ftp
	echo "verbose" >>upload.ftp
	echo "prompt" >>upload.ftp
	echo "cd Nightly" >>upload.ftp
	echo "cd " $build_tag >>upload.ftp
	echo "mput DashboardMigration.zip" >>upload.ftp
	echo "delete upload.ftp" >>upload.ftp
	echo "prompt" >>upload.ftp
	echo "quit" >>upload.ftp

	echo "Starting FTP to RackSpace"
	ftp -s:upload.ftp  
	if [ $? == 0 ]; 
	then
		echo "upload of log files was successful"  
	else
		echo "Exiting since FTP was not successful in transfering the zipped up files.  " \
			| mutt -s "Nightlybuild: Unable to ftp the files to colo1" $Notify
		exit -1
	fi

	echo "FTP to RackSpace is done"
	rm -rf  upload.ftp

	#
	# FTP build to the Birst Rackspace build directory
	#
	cd $jfong_clean/DashboardMigration
	echo "open 174.143.72.148" >upload.ftp
	echo "smibuilds" >>upload.ftp
	echo "5771builds!" >>upload.ftp
	echo "verbose" >>upload.ftp
	echo "prompt" >>upload.ftp
	echo "cd Nightly" >>upload.ftp
	echo "cd " $build_tag >>upload.ftp
	echo "mput DashboardMigration.zip" >>upload.ftp
	echo "delete upload.ftp" >>upload.ftp
	echo "prompt" >>upload.ftp
	echo "quit" >>upload.ftp

	echo "Starting FTP to Birst RackSpace"
	ftp -s:upload.ftp  
	if [ $? == 0 ]; 
	then
		echo "upload of log files was successful"  
	else
		echo "Exiting since FTP was not successful in transfering the zipped up files.  " \
			| mutt -s "Nightlybuild: Unable to ftp the files to Birst RackSpace" $Notify
		exit -1
	fi

	echo "FTP to Birst RackSpace is done"
	rm -rf  /upload.ftp
fi

exit 0
