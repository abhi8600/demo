#!/usr/bin/bash
#
PATH=/cygdrive/c/eclipse/plugins/apache-ant-1.7.1/bin:$PATH; export PATH

function Parameter_Help
{
echo "usage: deploy [-options]"
			echo "where options include:"
			echo " -b <branch> "
			echo "		Enter the branch that we will be tagging on."
			echo " 		If not branch is supplied, we default to HEAD"
			echo " -n <email notification list> comma separated"
			echo " 		default=jfong@birst.com,4157301327@vtext.com"
			echo " -t <build tag> "
			echo " 		Usually, this is the date and time  with or without a "
			echo " 		release number.  For example, 200802140805 "
			echo " 		is Feb 14th, 2008 8:05 a.m.  Or if it for 2.2.0, "
			echo "		the tag would be 200802140805_220"
			echo ""
}
# Define variables
# 	and clear them out first. 
#
Notify=jfong@birst.com
build_tag=""
Branch_Name=""
jfong_clean=""
CVS_workspace=""

while getopts ":d:b:n:t:" opts; do
        case "$opts" in
		d ) echo "-d $OPTARG"
			DEBUG=$OPTARG
		 	export DEBUG
			;;
		b ) echo "-b $OPTARG" 
			Branch_Name=$OPTARG
			echo $Branch_Name
			;;
		n ) echo "-n $OPTARG" 
			Notify=$OPTARG
			export Notify 
			;;
		t ) echo "-t $OPTARG"			# Since I'm creating the build, I am generating the build tag.  
			build_tag=$OPTARG;	
			export build_tag
			;;
		*  ) Parameter_Help 
			exit 1 ;;
	esac
	echo one value $1
done

jfong_clean=/cygdrive/c/clean_builds_$Branch_Name
echo "Branch = " $Branch_Name "-"
echo "workspace = " $jfong_clean

if [ -z $build_tag ] || [ -z $Branch_Name ];	#  Make sure we have our parameters
then
	Parameter_Help
	exit 1
fi

env

#
# Now clean DataConductor
# 

cd $jfong_clean/DataConductor/DataConductor 		# goto my clean build area

ant clean 

if [ $? == 0 ]; 
then
	echo "DataConductor: Ant clean worked"
else
	echo "Errors were generated when we attempted to do a clean" \
        | mutt -s "DataConductor: Ant clean had problems" $Notify
	exit -1
fi


#
# Now build DataConductor 
# 

cd $jfong_clean/DataConductor/DataConductor 		# goto my clean build area

ant  

if [ $? == 0 ]; 
then
	echo "DataConductor: Ant build worked"
else
	echo "Errors were generated when we attempted to do a build DataConductor" \
        | mutt -s "DataConductor: Ant build had problems" $Notify
	exit -1
fi


exit 0
