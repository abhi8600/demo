#!/usr/bin/bash

whoami

echo "RunningTime Start `date`"

PATH=/usr/local/bin:/usr/bin:$PATH
 
cd /Current_Build_Number
ls -la
JAVA_HOME=/cygdrive/c/Program\ Files/Java/jdk1.6.0_27
export JAVA_HOME
/usr/bin/env 

function Parameter_Help
{
echo "usage: wrapper_*.sh [-options]"
			echo "where options include:"
			echo " -n <email notification list> comma separated"
			echo " 		default=jfong@birst.com,4157301327@vtext.com"
			echo " -z <password> "
			echo " 		CVS password for exporting code "
			echo ""
}

cd ~; source ./.profile
cd /cygdrive/c/CustomerInstalls/NW/NWTest; source ./.profile

echo on
alias autoexec=autoexec.bat
timestamp=`/usr/bin/date +%Y%m%d%H%M`;		export timestamp
#	timestamp=`date +%Y%m%d%H%M`;			export timestamp
echo $timestamp

env

Notify_list="jfong@birst.com"
Notify_wrapper_list="jfong@birst.com"  
#	,ricks@birst.com
#	Notify_list="ricks@birst.com"
export Notify_list
export Notify_wrapper_list
echo $Notify_list
Current_Build_Number=/Current_Build_Number
#Current_Build_Number="$(cygpath -u -p -a "C:\Temp\Current_Build_Number")"
Working_Branch=HEAD
CVS_workspace=c:\\workspace_$Working_Branch
CVS_unix_workspace=/cygdrive/c/workspace_$Working_Branch
cvs_password=""
jfong_clean="/cygdrive/c/clean_builds_"$Working_Branch
jfong_win_clean="c:\\clean_builds_"$Working_Branch
jfong_share=/cygdrive/c/shared/Builds;
jfong_win_share=c:\\shared\\Builds
WinZip_Home="/cygdrive/c/Program Files (x86)/WinZip"

while getopts ":n:z:" opts; do
        case "$opts" in
		n ) echo "-n $OPTARG" 
			Notify=$OPTARG
			export Notify 
			;;
		z ) echo "-z $OPTARG"			# Since I'm creating the build, I am generating the build tag.  
			cvs_password=$OPTARG;	
			export cvs_password
			;;
		*  ) Parameter_Help 
			exit 1 ;;
	esac
	echo one value $1
done

if [ -z $cvs_password ];		 	#	|| [ -z $Release_Dir ] || [ -z $Period_Id ]; 
then
	echo "One or more of the required parameters was not provided."
	echo "Please provide all the required paramaters:" 
	Parameter_Help
	exit 1 
fi 

which blat
ping 192.168.90.100

blat -superdebugT -s "Wrapper_HEAD : Daily build start" -body "Started building the $timestamp build" -to "$Notify_wrapper_list"


echo $jfong_clean
mkdir $jfong_share/$timestamp

echo "RunningTime Start CVS `date`"

/usr/bin/bash -x $CVS_unix_workspace/BuildsAndReleases/builds/CVS_sync_tag.sh -k 1 -b $Working_Branch -c /cygdrive/c/clean_builds_$Working_Branch -t "$timestamp" -w /cygdrive/c/workspace_$Working_Branch -z "$cvs_password" > $jfong_share/Logs/CVS_Sync_$timestamp.log 2>&1
if [ $? == 0 ]; 
then
	echo "CVS_sync was successful"  
else
	blat -subject "Wrapper_HEAD : Build ended with CVS error" \
	-body "CVS had problems with tagging or exporting " \
	-to $Notify_wrapper_list
	exit -1
fi

echo "RunningTime Stop CVS `date`"

#
# Build MemDB 
#

echo "RunningTime Start MemDB `date`"

/usr/bin/bash -x  $CVS_unix_workspace/BuildsAndReleases/builds/MemDBBuild.sh  -c /cygdrive/c/clean_builds_$Working_Branch -t "$timestamp" 1>$jfong_share/Logs/MemDBBuild_$timestamp.log 2>&1

if [ $? == 0 ]; 
then
	echo "MemDB was successful"  
else
	blat -subject "Wrapper_HEAD : Build ended with MemDB error" \
	-body "MemDB had problems with building " \
	-to $Notify_wrapper_list
	exit -1
fi

echo "RunningTime Stop MemDB `date`"

#
# Build ChartRenderer 
#
echo $jfong_clean
echo "RunningTime Start ChartRenderer `date`"

/usr/bin/bash -x  $CVS_unix_workspace/BuildsAndReleases/builds/ChartRendererBuild.sh  -c /cygdrive/c/clean_builds_$Working_Branch -t "$timestamp" 1>$jfong_share/Logs/ChartRenderer_$timestamp.log 2>&1

if [ $? == 0 ]; 
then
	echo "ChartRenderer was successful"  
else
	blat -subject "Wrapper_HEAD : Build ended with ChartRenderer error" \
	-body "ChartRenderer had problems with building " \
	-to $Notify_wrapper_list
	exit -1
fi

echo "RunningTime Stop ChartRenderer `date`"

#
# Build DataConductor 
#

echo "RunningTime Start DataConductor_jar `date`"

/usr/bin/bash -x $CVS_unix_workspace/BuildsAndReleases/builds/DataConductorBuild_jar.sh -b $Working_Branch -t "$timestamp" > "$jfong_share/Logs/DataConductor_jar_$timestamp.log" 2>&1

if [ $? == 0 ]; 
then
	echo "DataConductor was successful"  
else
	blat -subject "Wrapper_HEAD : Build ended with DataConductor error" \
	-body "DataConductor had problems with building " \
	-to $Notify_wrapper_list
	exit -1
fi

echo "RunningTime Stop DataConductor_jar `date`"

#
# Build SMI
#

echo "RunningTime Start SMI `date`"

/usr/bin/bash -x  $CVS_unix_workspace/BuildsAndReleases/builds/SMIBuilds.sh \
	-h /cygdrive/c/CustomerInstalls/NW/NWTest	\
	-p "7120"					\
	-f 0						\
	-t "$timestamp"					\
	-b "$Working_Branch"					\
	-n "$Notify_list"				\
1>$jfong_share/Logs/nightly_$timestamp.log 2>&1

if [ $? == 0 ]; 
then
	echo "SMI was successful"  
else
	blat -subject "Wrapper_HEAD : Build ended with SMI error" \
	-body "SMI had problems with building " \
	-to $Notify_wrapper_list
	exit -1
fi

echo "RunningTime Stop SMI `date`"

#
# Build FlexAdhoc 
#
echo $jfong_clean
echo "RunningTime Start FlexAdhoc `date`"

/usr/bin/bash -x  $CVS_unix_workspace/BuildsAndReleases/builds/FlexAdhocBuild.sh  -c /cygdrive/c/clean_builds_$Working_Branch -t "$timestamp" 1>$jfong_share/Logs/FlexAdhoc_$timestamp.log 2>&1

if [ $? == 0 ]; 
then
	echo "FlexAdhoc was successful"  
else
	blat -subject "Wrapper_HEAD : Build ended with FlexAdhoc error" \
	-body "FlexAdhoc had problems with building " \
	-to $Notify_wrapper_list
	exit -1
fi
echo $JAVA_HOME

echo "RunningTime Stop FlexAdhoc `date`"

#
# Build LogicalExpression 
#

echo "RunningTime Start LogicalExpression `date`"

cmd /c "$CVS_workspace\BuildsAndReleases\builds\LogicalExpressionBuild.bat" -b $Working_Branch -t "$timestamp" > "$jfong_win_share\Logs\LogicalExpression_$timestamp.log" 2>&1

if [ $? == 0 ]; 
then
	echo "LogicalExpression was successful"  
else
	blat -subject "Wrapper_HEAD : Build ended with LogicalExpression error" \
	-body "LogicalExpression had problems with building " \
	-to $Notify_wrapper_list
	exit -1
fi

echo "RunningTime Stop LogicalExpression `date`"

#
# Build BirstAdmin 
#

echo "RunningTime Start BirstAdmin `date`"

cmd /c "$CVS_workspace\BuildsAndReleases\builds\BirstAdminBuild.bat" -b $Working_Branch -t "$timestamp" -f 0 > "$jfong_win_share\Logs\BirstAdmin_$timestamp.log" 2>&1
 
if [ $? == 0 ]; 
then
	echo "BirstAdmin was successful"  
else
	blat -subject "Wrapper_HEAD : Build ended with BirstAdmin error" \
	-body "BirstAdmin had problems with building " \
	-to $Notify_wrapper_list
	exit -1
fi
cp $jfong_clean/Performance\ Optimizer\ Administration/bin/Release/log4net.dll $jfong_share/$timestamp
cp $jfong_clean/Performance\ Optimizer\ Administration/bin/Release/Birst\ Administration.exe $jfong_clean/Acorn/Acorn/bin/Birst\ Administration.exe
"$WinZip_Home/WZZIP.EXE" -ex -a "$jfong_win_share\\$timestamp\\BirstAdmin.zip" \
	"$jfong_win_clean\\Performance Optimizer Administration\\bin\\Release\\Birst Administration.exe" \
	"$jfong_win_clean\\LogicalExpression\\LogicalExpression\\bin\\Debug\\LogicalExpression.dll" \
	"$jfong_win_clean\\LogicalExpression\\LogicalExpression\\bin\\Debug\\Antlr3.Runtime.dll"

echo "RunningTime Stop BirstAdmin `date`"

#
# Build DataConductor 
#

echo "RunningTime Start DataConductor_BC `date`"

/usr/bin/bash -x $CVS_unix_workspace/BuildsAndReleases/builds/DataConductorBuild_BC.sh -b $Working_Branch -t "$timestamp" > "$jfong_share/Logs/DataConductor_BC_$timestamp.log" 2>&1

if [ $? == 0 ]; 
then
	echo "DataConductor was successful"  
else
	blat -subject "Wrapper_HEAD : Build ended with DataConductor error" \
	-body "DataConductor had problems with building " \
	-to $Notify_wrapper_list
	exit -1
fi

echo "RunningTime Stop DataConductor_BC `date`"

#
# Build SourceAdmin 
#
echo $jfong_clean
echo "RunningTime Start SourceAdmin `date`"

/usr/bin/bash -x  $CVS_unix_workspace/BuildsAndReleases/builds/SourceAdminBuild.sh  -c /cygdrive/c/clean_builds_$Working_Branch -t "$timestamp" 1>$jfong_share/Logs/SourceAdmin_$timestamp.log 2>&1

if [ $? == 0 ]; 
then
	echo "SourceAdmin was successful"  
else
	blat -subject "Wrapper_HEAD : Build ended with SourceAdmin error" \
	-body "SourceAdmin had problems with building " \
	-to $Notify_wrapper_list
	exit -1
fi
echo $JAVA_HOME

echo "RunningTime Stop SourceAdmin `date`"

#
# Build Acorn 
#

echo "RunningTime Start Acorn `date`"

cmd /c "$CVS_workspace\BuildsAndReleases\builds\AcornBuild.bat" -b $Working_Branch -t "$timestamp" -f 0 > "$jfong_win_share\Logs\Acorn_$timestamp.log" 2>&1
 
if [ $? == 0 ]; 
then
	echo "Acorn was successful"  
else
	blat -subject "Wrapper_HEAD : Build ended with Acorn error" \
	-body "Acorn had problems with building " \
	-to $Notify_wrapper_list
	exit -1
fi

echo "RunningTime Stop Acorn `date`"

#
# Build ChartWebService 
#

echo "RunningTime Start ChartWebService `date`"

/usr/bin/bash -x  $CVS_unix_workspace/BuildsAndReleases/builds/ChartWebServiceBuild.sh -c /cygdrive/c/clean_builds_$Working_Branch -t "$timestamp" > "$jfong_share/Logs/ChartWebService_$timestamp.log" 2>&1

if [ $? == 0 ]; 
then
	echo "ChartWebService was successful"  
else
	blat -subject "Wrapper_HEAD : Build ended with ChartWebService error" \
	-body "ChartWebService had problems with building " \
	-to $Notify_wrapper_list
	exit -1
fi

cp $jfong_clean/ChartWebService/deployable_units/SMIChartServer.war $jfong_share/$timestamp
if [ -s $jfong_share/$timestamp/SMIChartServer.war ];
then
	echo "copy of SMIChartServer.war was successful"
else
	blat -subject "Wrapper_HEAD : Build ended with ChartWebService error" \
	-body "ChartWebService had problems with copying the ChartWebService.jar to the release location " \
	-to $Notify_wrapper_list
	exit -1
fi
	
echo "RunningTime Stop ChartWebService `date`"

#
#
# Build MoveSpace 
#

echo "RunningTime Start MoveSpace `date`"

cmd /c "$CVS_workspace\BuildsAndReleases\builds\MoveSpaceBuild.bat" -b $Working_Branch -t "$timestamp" -f 0 -n "$Notify_list" > "$jfong_win_share\Logs\MoveSpace_$timestamp.log" 2>&1
 
if [ $? == 0 ]; 
then
	echo "MoveSpace was successful"  
else
	blat -subject "Wrapper_HEAD : Build ended with MoveSpace error" \
	-body "MoveSpace had problems with building " \
	-to $Notify_wrapper_list
	exit -1
fi

echo "RunningTime Stop MoveSpace `date`"

#
# Build AcornExecute 
#

mkdir $jfong_clean/AcornExecute/AcornExecute/bin
cp $jfong_clean/Performance\ Optimizer\ Administration/bin/Release/Birst\ Administration.exe $jfong_clean/AcornExecute/AcornExecute/bin/Birst\ Administration.exe
echo "RunningTime Start AcornExecute `date`"

cmd /c "$CVS_workspace\BuildsAndReleases\builds\AcornExecuteBuild.bat" -b $Working_Branch -t "$timestamp" -f 0 > "$jfong_win_share\Logs\AcornExecute_$timestamp.log" 2>&1
 
if [ $? == 0 ]; 
then
	echo "AcornExecute was successful"  
else
	blat -subject "Wrapper_HEAD : Build ended with AcornExecute error" \
	-body "AcornExecute had problems with building " \
	-to $Notify_wrapper_list
	exit -1
fi

echo "RunningTime Stop AcornExecute `date`"

#
# Build Scheduler 
#
echo $jfong_clean
echo "RunningTime Start Scheduler `date`"

/usr/bin/bash -x  $CVS_unix_workspace/BuildsAndReleases/builds/SchedulerBuild.sh  -c /cygdrive/c/clean_builds_$Working_Branch -t "$timestamp" 1>$jfong_share/Logs/Scheduler_$timestamp.log 2>&1

if [ $? == 0 ]; 
then
	echo "Scheduler was successful"  
else
	blat -subject "Wrapper_HEAD : Build ended with Scheduler error" \
	-body "Scheduler had problems with building " \
	-to $Notify_wrapper_list
	exit -1
fi

echo "RunningTime Stop Scheduler `date`"

#
# Package Help 
#

echo "RunningTime Start Help `date`"

cmd /c "$CVS_workspace\BuildsAndReleases\builds\PackageHelp.bat" -b $Working_Branch -t "$timestamp" -f 0 > "$jfong_win_share\Logs\PackageHelp_$timestamp.log" 2>&1
 
if [ $? == 0 ]; 
then
	echo "PackageHelp was successful"  
else
	blat -subject "Wrapper_HEAD : Package Help ended with an error" \
	-body "Help had problems with the packaging " \
	-to $Notify_wrapper_list
	exit -1
fi

echo "RunningTime Stop Help `date`"

#
# Build MigrateSchedules 
#
#
#echo "RunningTime Start MigrateSchedules `date`"
#
#cmd /c "$CVS_workspace\BuildsAndReleases\builds\MigrateSchedulesBuild.bat" -b $Working_Branch -t "$timestamp" -f 0 > "$jfong_win_share\Logs\MigrateSchedules_$timestamp.log" 2>&1
# 
#if [ $? == 0 ]; 
#then
#	echo "MigrateSchedules was successful"  
#else
#	blat -subject "Wrapper_HEAD : Build ended with MigrateSchedules error" \
#	-body "MigrateSchedules had problems with building " \
#	-to $Notify_wrapper_list
#	exit -1
#fi
#
#echo "RunningTime Stop MigrateSchedules `date`"
#
#
#	Sign Jars
#

echo "RunningTime Start SignJars`date`"

/usr/bin/bash -x  $CVS_unix_workspace/BuildsAndReleases/builds/Sign_jars.sh -t "$timestamp" 1>$jfong_share/Logs/Sign_jar_$timestamp.log 2>&1 
if [ $? == 0 ]; 
then
	echo "SMI was successful"  
else
	blat -subject "Wrapper_HEAD : Build ended with Sign_jar error" \
	-body "Sign_jar had problems encrypting files " \
	-to $Notify_wrapper_list
	exit -1
fi

echo "RunningTime Stop SignJars`date`"
#
# Build AcornExecute 
#

mkdir $jfong_clean/AcornExecute/AcornExecute/bin
cp $jfong_clean/Performance\ Optimizer\ Administration/bin/Release/Birst\ Administration.exe $jfong_clean/AcornExecute/AcornExecute/bin/Birst\ Administration.exe
cmd /c "$CVS_workspace\BuildsAndReleases\builds\AcornExecuteBuild.bat" -b $Working_Branch -t "$timestamp" -f 0 > "$jfong_win_share\Logs\AcornExecute_$timestamp.log" 2>&1
 
if [ $? == 0 ]; 
then
	echo "AcornExecute was successful"  
else
	blat -subject "Wrapper_HEAD : Build ended with AcornExecute error" \
	-body "AcornExecute had problems with building " \
	-to $Notify_wrapper_list
	exit -1
fi

#
# FTP files
#

echo "RunningTime Start FTPfiles `date`"

/usr/bin/bash -x  $CVS_unix_workspace/BuildsAndReleases/builds/ftp_files.sh \
	-h /cygdrive/c/CustomerInstalls/NW/NWTest	\
	-p "7120"					\
	-f 0						\
	-t "$timestamp"					\
	-b "$Working_Branch"					\
	-n "$Notify_list"				\
1>$jfong_share/Logs/FTP_$timestamp.log 2>&1

if [ $? == 0 ]; 
then
	echo "FTP was successful"  
else
	blat -subject "Wrapper_HEAD : FTP error" \
	-body "FTP had problems with transfering files " \
	-to $Notify_wrapper_list
	exit -1
fi

echo "RunningTime Stop FTPfiles `date`"

#
# Deploy  SMI
#

#	/usr/bin/bash -x  $CVS_unix_workspace/BuildsAndReleases/builds/SMITest.sh \
#		-h /cygdrive/c/CustomerInstalls/NW/NWTest	\
#		-p "7120"					\
#		-f 1						\
#		-t "$timestamp"					\
#		-b "$Working_Branch"					\
#		-n "$Notify_list"				\
#	1>$jfong_share/Logs/SMITest_$timestamp.log 2>&1

if [ $? == 0 ]; 
then
	echo "SMITest was successful"  
else
	blat -subject "Wrapper_HEAD : Test ended with SMI error" \
	-body "SMITest had problems with building " \
	-to $Notify_wrapper_list
	exit -1
fi

# Write out the latest build number for deployment to pick up.
echo "build_number=$timestamp" >$Current_Build_Number/$Working_Branch/build_number.txt

blat -superdebugT -subject "Wrapper_HEAD : Daily build completed without errors" \
	-body "$timestamp build is now available" \
	-to "$Notify_wrapper_list"

echo "RunningTime End `date`"
exit 0