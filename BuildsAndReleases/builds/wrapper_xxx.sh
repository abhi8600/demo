#!/usr/bin/bash

function Parameter_Help
{
echo "usage: wrapper_*.sh [-options]"
			echo "where options include:"
			echo " -n <email notification list> comma separated"
			echo " 		default=jfong@birst.com,4157301327@vtext.com"
			echo " -z <password> "
			echo " 		CVS password for exporting code "
			echo ""
}

echo on
alias autoexec=autoexec.bat
timestamp=`date +%Y%m%d%H%M_436`;		export timestamp
#	timestamp=`date +%Y%m%d%H%M`;			export timestamp
cd ~; source ./.profile
cd /cygdrive/c/CustomerInstalls/NW/NWTest; source ./.profile
env

Notify_list="jfong@birst.com"
#	Notify_list="ricks@birst.com"
export Notify_list
echo $Notify_list
Working_Branch=Birst_4_3_6_SP
CVS_workspace=c:\\workspace_$Working_Branch
CVS_unix_workspace=/cygdrive/c/workspace_$Working_Branch

while getopts ":n:z:" opts; do
        case "$opts" in
		n ) echo "-n $OPTARG" 
			Notify=$OPTARG
			export Notify 
			;;
		z ) echo "-z $OPTARG"			# Since I'm creating the build, I am generating the build tag.  
			cvs_password=$OPTARG;	
			export cvs_password
			;;
		*  ) Parameter_Help 
			exit 1 ;;
	esac
	echo one value $1
done

#
# Tag and export files from CVS.
#
#	if [ -z $SMI_Branch ]
#	then
#		bash -x $nightly_bin/CVS_sync.sh -b "HEAD" -c $jfong_clean -t $build_tag -w $SMI_workspace
#	else
#		bash -x $nightly_bin/CVS_sync.sh -b $SMI_Branch -c $jfong_clean -t $build_tag -w $SMI_workspace
#	fi


/usr/bin/bash -x $CVS_unix_workspace/BuildsAndReleases/builds/CVS_sync_tag.sh -k 0 -b $Working_Branch -c /cygdrive/c/clean_builds_$Working_Branch -t "$timestamp" -w /cygdrive/c/workspace_$Working_Branch -z "$cvs_password" > "c:\SFOJFONG\Builds\Logs\CVS_Sync_$timestamp.log" 2>&1
if [ $? == 0 ]; 
then
	echo "CVS_sync was successful"  
else
	echo "CVS had problems with tagging or exporting " \
		| mutt -s "Wrapper_HEAD : Build ended with CVS error" $Notify_list
	exit -1
fi

# chmod -R -v 777 /cygdrive/c/clean_builds_$Working_Branch/Performance\ Optimizer\ Administration
# cd /cygdrive/c/clean_builds_$Working_Branch/Acorn/Acorn
# chmod -R -v 777 *.dll
# cd /cygdrive/c/clean_builds_$Working_Branch/Acorn/Acorn/bin
# chmod -R -v 777 *.dll

#
# Build ChartRenderer 
#
echo $jfong_clean
/usr/bin/bash -x  $CVS_unix_workspace/BuildsAndReleases/builds/ChartRendererBuild.sh  -c /cygdrive/c/clean_builds_$Working_Branch -t "$timestamp" 1>/cygdrive/c/SFOJFONG/Builds/Logs/ChartRenderer_$timestamp.log 2>&1

if [ $? == 0 ]; 
then
	echo "ChartRenderer was successful"  
else
	echo "ChartRenderer had problems with building " \
		| mutt -s "Wrapper_HEAD : Build ended with ChartRenderer error" $Notify_list
	exit -1
fi

#
# Build DataConductor 
#

# cmd /c "$CVS_workspace\BuildsAndReleases\builds\Birst_head\DataConductorBuild.bat" -b $Working_Branch -t "$timestamp" > "c:\SFOJFONG\Builds\Logs\DataConductor_$timestamp.log" 2>&1
/usr/bin/bash -x $CVS_unix_workspace/BuildsAndReleases/builds/DataConductorBuild.sh -b $Working_Branch -t "$timestamp" > "/cygdrive/c/SFOJFONG/Builds/Logs/DataConductor_$timestamp.log" 2>&1

if [ $? == 0 ]; 
then
	echo "DataConductor was successful"  
else
	echo "DataConductor had problems with building " \
		| mutt -s "Wrapper_HEAD : Build ended with DataConductor error" $Notify_list
	exit -1
fi

#
# Build LogicalExpression 
#

cmd /c "$CVS_workspace\BuildsAndReleases\builds\LogicalExpressionBuild.bat" -b $Working_Branch -t "$timestamp" > "c:\SFOJFONG\Builds\Logs\LogicalExpression_$timestamp.log" 2>&1

if [ $? == 0 ]; 
then
	echo "LogicalExpression was successful"  
else
	echo "LogicalExpression had problems with building " \
		| mutt -s "Wrapper_HEAD : Build ended with LogicalExpression error" $Notify_list
	exit -1
fi

#
# Build FlexAdhoc 
#
echo $jfong_clean
/usr/bin/bash -x  $CVS_unix_workspace/BuildsAndReleases/builds/FlexAdhocBuild.sh  -c /cygdrive/c/clean_builds_$Working_Branch -t "$timestamp" 1>/cygdrive/c/SFOJFONG/Builds/Logs/FlexAdhoc_$timestamp.log 2>&1

if [ $? == 0 ]; 
then
	echo "FlexAdhoc was successful"  
else
	echo "FlexAdhoc had problems with building " \
		| mutt -s "Wrapper_HEAD : Build ended with FlexAdhoc error" $Notify_list
	exit -1
fi

#
# Build SourceAdmin 
#
echo $jfong_clean
/usr/bin/bash -x  $CVS_unix_workspace/BuildsAndReleases/builds/SourceAdminBuild.sh  -c /cygdrive/c/clean_builds_$Working_Branch -t "$timestamp" 1>/cygdrive/c/SFOJFONG/Builds/Logs/SourceAdmin_$timestamp.log 2>&1

if [ $? == 0 ]; 
then
	echo "SourceAdmin was successful"  
else
	echo "SourceAdmin had problems with building " \
		| mutt -s "Wrapper_HEAD : Build ended with SourceAdmin error" $Notify_list
	exit -1
fi

#
# Build BirstAdmin 
#

cmd /c "$CVS_workspace\BuildsAndReleases\builds\BirstAdminBuild.bat" -b $Working_Branch -t "$timestamp" -f 0 > "c:\SFOJFONG\Builds\Logs\BirstAdmin_$timestamp.log" 2>&1
 
if [ $? == 0 ]; 
then
	echo "BirstAdmin was successful"  
else
	echo "BirstAdmin had problems with building " \
		| mutt -s "Wrapper_HEAD : Build ended with BirstAdmin error" $Notify_list
	exit -1
fi

#
# Build ChartService 
#
#echo
#echo  SKIPPING THE ChartService BUILD
#echo 
#echo 

# cmd /c "$CVS_workspace\BuildsAndReleases\builds\Birst_head\ChartServiceBuild.bat" -b $Working_Branch -t "$timestamp" > "c:\SFOJFONG\Builds\Logs\ChartService_$timestamp.log" 2>&1
/usr/bin/bash -x $CVS_unix_workspace/BuildsAndReleases/builds/ChartServiceBuild.sh -b $Working_Branch -t "$timestamp" > "/cygdrive/c/SFOJFONG/Builds/Logs/ChartService_$timestamp.log" 2>&1

if [ $? == 0 ]; 
then
	echo "ChartService was successful"  
else
	echo "ChartService had problems with building " \
		| mutt -s "Wrapper_HEAD : Build ended with ChartService error" $Notify_list
	exit -1
fi

#
# Build SMI
#

/usr/bin/bash -x  $CVS_unix_workspace/BuildsAndReleases/builds/SMIBuilds.sh \
	-h /cygdrive/c/CustomerInstalls/NW/NWTest	\
	-p "7120"					\
	-f 0						\
	-t "$timestamp"					\
	-b "$Working_Branch"					\
	-n "$Notify_list"				\
1>/cygdrive/c/SFOJFONG/Builds/Logs/nightly_$timestamp.log 2>&1

if [ $? == 0 ]; 
then
	echo "SMI was successful"  
else
	echo "SMI had problems with building " \
		| mutt -s "Wrapper_HEAD : Build ended with SMI error" $Notify_list
	exit -1
fi

#
# Build Acorn 
#

cmd /c "$CVS_workspace\BuildsAndReleases\builds\AcornBuild.bat" -b $Working_Branch -t "$timestamp" -f 0 > "c:\SFOJFONG\Builds\Logs\acorn_$timestamp.log" 2>&1
 
if [ $? == 0 ]; 
then
	echo "Acorn was successful"  
else
	echo "Acorn had problems with building " \
		| mutt -s "Wrapper_HEAD : Build ended with Acorn error" $Notify_list
	exit -1
fi

#
# Build AccountMigrationTool 
#
#
#cmd /c "$CVS_workspace\BuildsAndReleases\builds\AccountMigrationToolbuild.bat" -b $Working_Branch -t "$timestamp" -f 1 > "c:\SFOJFONG\Builds\Logs\AccountMigrationTool_$timestamp.log" 2>&1
# 
#if [ $? == 0 ]; 
#then
#	echo "AccountMigrationTool was successful"  
#else
#	echo "AccountMigrationTool had problems with building " \
#		| mutt -s "Wrapper_HEAD : Build ended with Acorn error" $Notify_list
#	exit -1
#fi
#
#
# Build MoveSpace 
#

cmd /c "$CVS_workspace\BuildsAndReleases\builds\MoveSpaceBuild.bat" -b $Working_Branch -t "$timestamp" -f 0 -n "$Notify_list" > "c:\SFOJFONG\Builds\Logs\MoveSpace_$timestamp.log" 2>&1
 
if [ $? == 0 ]; 
then
	echo "MoveSpace was successful"  
else
	echo "MoveSpace had problems with building " \
		| mutt -s "Wrapper_HEAD : Build ended with MoveSpace error" $Notify_list
	exit -1
fi

#
# Build DashboardMigration
#
#
#/usr/bin/bash -x  $CVS_unix_workspace/BuildsAndReleases/builds/DashboardMigrationBuild.sh \
#	-f 0						\
#	-t "$timestamp"					\
#	-n "$Notify_list"				\
#1>/cygdrive/c/SFOJFONG/Builds/Logs/DashboardMigration_$timestamp.log 2>&1
#
#if [ $? == 0 ]; 
#then
#	echo "SMI was successful"  
#else
#	echo "SMI had problems with building " \
#		| mutt -s "Wrapper_HEAD : Build ended with SMI error" $Notify_list
#	exit -1
#fi

#
# Package up AcornTestConsole
#

#cmd /c "$CVS_workspace\BuildsAndReleases\builds\package_AcornTestConsole.bat" -b $Working_Branch -t "$timestamp" > "c:\SFOJFONG\Builds\Logs\acorntestconsole_$timestamp.log" 2>&1

#if [ $? == 0 ]; 
#then
#	echo "AcornTestConsole was successful"  
#else
#	echo "AcornTestConsole had problems with building " \
#		| mutt -s "Wrapper_HEAD : Build ended with AcornTestConsole error" $Notify_list
#	exit -1
#fi

#
# Deploy Acorn ( we have a scheduler task for that now)
#
#
#cd $CVS_unix_workspace/BuildsAndReleases/deployment
#
# cmd /c "$CVS_workspace\BuildsAndReleases\deployment\deploy_acorn.bat" $timestamp $timestamp > "c:\SFOJFONG\Builds\Logs\deploy_acorn_$timestamp.log" 2>&1
#
#cmd /c "$CVS_workspace\BuildsAndReleases\deployment\deploy_acorn.bat" -s $timestamp -a "c:\SMI" -p 6100 -r Nightly -t $timestamp > "c:\SFOJFONG\Builds\Logs\deploy_acorn_$timestamp.log" 2>&1


