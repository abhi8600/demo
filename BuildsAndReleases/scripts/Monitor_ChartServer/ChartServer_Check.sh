#!/usr/bin/bash
source /cygdrive/c/cygwin/home/jfong/.bashrc
PATH=.:/usr/bin:$PATH:"/cygdrive/c/Program Files (x86)/blat307/full"
echo on

declare -i  counter=0
for i in "$@"; do
	counter=$counter+1
	Input[$counter]="$i"
	echo "${Input[$counter]}"
	echo "${Input[1]}"
done

loop_cnt=1
while [[ $loop_cnt -le $counter ]]
do
	echo "$loop_cnt"

	status=`/cygdrive/c/Program\ Files/curl-7.36.0-win64/bin/curl -s0 --connect-timeout 30 http://${Input[$loop_cnt]}:6104/SMIChartServer/Health.jsp success |grep success|wc -l`
	if [ $status -ne 1 ]
	then
		sc \\\\${Input[$loop_cnt]} stop ChartServer6100
		sc \\\\${Input[$loop_cnt]} start ChartServer6100

		/cygdrive/c/Windows/system32/sc \\\\sfo0-qa-04 query ChartServer6100 | grep STATE | grep RUNNING 

		if [ $? -ne 1 ]
		then
			blat -subject "ChartServer: Restarted" -body "Restarting the ${Input[$loop_cnt]} ChartServer returned it to a running state" -to jfong@birst.com
		else
			blat -subject "ChartServer: Restart failed" -body "Restarting the ${Input[$loop_cnt]} ChartServer still did not return it to a running state" -to jfong@birst.com
		fi
	fi

	(( loop_cnt++ ))
done
