REM
REM Check the status of the ChartServer and restart if it is in a weird state
REM
set status=""
set VarNotify=jfong@birst.com
REM define the command to retrieve the status of the server
set command="curl -s0 --connect-timeout 30 http://%COMPUTERNAME%:6104/SMIChartServer/Health.jsp"

REM execute the command and save the result in status.
for /F %%i IN (' %command% ') DO SET status=%%i

REM if the results is not "success", restart the server and verify that it is running
if "%status%" EQU "success" goto CHARTSERVER_ALRIGHT:

sc \\%COMPUTERNAME% stop ChartServer6100
sc \\%COMPUTERNAME% start ChartServer6100

for /f "tokens=2-4" %%a in ('sc \\%COMPUTERNAME% query  ChartServer6100 ^| findstr RUNNING') do if %%c==RUNNING ( goto EMAIL_SERVER_RESTARTED:)
set VarMailText=Stopping and Starting of the ChartServer did not complete normally. 
set VarMailSubject=ChartServer_Check on %COMPUTERNAME%:ChartServer startup did not complete
goto BADEXIT

:EMAIL_SERVER_RESTARTED
set VarMailText=Successfully restarted the ChartServer since status was %status%. 
set VarMailSubject=ChartServer_Check on %COMPUTERNAME%:ChartServer restart
blat -to %VarNotify% -subject "%VarMailSubject%" -body "%VarMailText%"

:CHARTSERVER_ALRIGHT

goto THE_END:

:BADEXIT
echo ------------------ Bad Exit -------------------------------------------
echo %VarMailText%
blat -to %VarNotify% -subject "%VarMailSubject%" -body "%VarMailText%"
rem *** if exist %VarCustProp% del %VarCustProp%
echo "ChartServer restart failed."

exit /B 1

:THE_END

exit /B 0





