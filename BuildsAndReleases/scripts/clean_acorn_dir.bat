echo off

set VarNotify=jfong@successmetricsinc.com
set VarMailText=Processing
set VarMailSubject=Acorn Clean Dir: Started %DATE% %TIME%

echo  %1
if "%1"=="" (
	set VarMailText=A starting directory was not provided
	set VarMailSubject=Acorn Clean Dir: Failed
	echo No starting directory provide 
	echo clean_acorn_dir.bat {acorn data directory home}
	goto badexit:
)
if not exist %1 (
	set VarMailText=Directory provided does not exist
	set VarMailSubject=Acorn Clean Dir: Failed
	echo No starting directory provide 
	echo clean_acorn_dir.bat {acorn data directory home}
	goto badexit:
)

pushd %1
echo is this here?
for /d %%A in (%1\*) do (
	echo %%A 
	if exist %%A\deleted.lock ( 
		for /f "delims=\ tokens=4" %%a in ("%%A") do (	
			echo "deleting directory %%a"
			rmdir /Q /S /s %%a
			if exist %%a (
				set VarMailText=Need to check directory %%a since we were not able to delete the directory
				set VarMailSubject=Acorn Clean Dir: Delete directory error 
				goto badexit:
			)
		) 
	)
)

set VarMailText=Processing Done
set VarMailSubject=Acorn Clean Dir: completed for  %DATE% %TIME%

goto Done:

:badexit
echo ------------------ Bad Exit -------------------------------------------
echo %VarMailText%
blat -to %VarNotify% -subject "%VarMailSubject%" -body "%VarMailText%"
echo "This batch file failed."
popd
exit /B 1

:done
echo %VarMailText%
blat -to %VarNotify% -subject "%VarMailSubject%" -body "%VarMailText%"

:cleanup
echo -------------------Clean up and Exit --------------------------
	
popd

