echo on
REM
REM Get Acorn user file from db3 and email to people
REM

set VarNotify=jfong@successmetricsinc.com

cd C:\downloads\Acorn

ftp -s:C:\workspace\BuildsAndReleases\scripts\ftp_users.txt 

findstr LastName users.txt 
if %ERRORLEVEL% LSS 1 goto EmailPeople:
set VarMailText=The users.txt file was not transfered, suspect problem is on DB3 side. 
set VarMailSubject=FTP Acorn Users had FTP problems
goto badexit

:EmailPeople
rem
echo  Email People that we are done.
rem

set VarMailSubject=Registered Acorn Users
blat users.txt -to jfong@successmetricsinc.com,blewis@successmetricsinc.com,elan@successmetricsinc.com,pstaelin@successmetricsinc.com,sschmitz@successmetricsinc.com -subject "%VarMailSubject%" 
REM blat users.txt -to jfong@successmetricsinc.com -subject "%VarMailSubject%" 

goto Done:

:badexit
echo ------------------ Bad Exit -------------------------------------------
echo %VarMailText%
blat -to %VarNotify% -subject "%VarMailSubject%" -body "%VarMailText%"
rem *** if exist %VarCustProp% del %VarCustProp%
echo "This batch file failed."
exit /B 1

:done
REM echo %VarMailText%
REM blat -to %VarNotify% -subject "%VarMailSubject%" -body "%VarMailText%"

:cleanup
echo -------------------Clean up and Exit --------------------------
del users.txt