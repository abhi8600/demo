echo on
set VarNotify=jfong@birst.com

for /f "tokens=1-3 delims=\ " %%a in ("%date%") do set date_only=%%b
for /f "tokens=1-3 delims=/." %%a in ("%date_only%") do set datestamp=%%c%%a%%b
echo %DateStamp%

echo -%DateStamp%-
for /l %%a in (1,1,31) do if "%DateStamp:~-1%"==" " set DateStamp=%DateStamp:~0,-1%
echo -%DateStamp%-

del /F/S/Q C:\Questar\reports\smiengine.log
REM
REM Clean-up and Generate the reports
REM
if EXIST "c:\SMI\Data\fee6a220-e63d-41f7-845d-719e12d21d1c\catalog\shared\reports" ( goto DIR_CLEANUP: )
set VarMailText=Reports directory doesn't exist, please create it first.
set VarMailSubject=Questar_Reports:Directory doesn't exist. 
goto BADEXIT:

:DIR_CLEANUP
cd "c:\SMI\Data\fee6a220-e63d-41f7-845d-719e12d21d1c\catalog\shared\reports"
IF %ERRORLEVEL% LSS 1 goto DIR_EXIST:
set VarMailText=Cannot cd to reports directory to cleanup old reports.
set VarMailSubject=Questar_Reports:Directory doesn't exist. 
goto BADEXIT:

:DIR_EXIST

del /F/S/Q *.pdf 

cd c:\Questar\reports

cmd /c Wrapper_reports_all.bat
IF %ERRORLEVEL% LSS 1 goto REPORTS_GENERATED:
set VarMailText=Error ecountered generating the Questar reports.
set VarMailSubject=Questar_Reports:Report gen error. 
goto BADEXIT:

:REPORTS_GENERATED
echo Reports Generated

REM 
REM Zip up the reports
REM 

cd "c:\SMI\Data\fee6a220-e63d-41f7-845d-719e12d21d1c\catalog\shared\reports"

"c:\Program Files (x86)\7-Zip\7z.exe" a -tzip c:\Questar\weekly\Reports_generated_%DateStamp%.zip 

IF %ERRORLEVEL% LSS 1 goto ZIP_CREATED:
set VarMailText=7-Zip had problems generating the zip file.
set VarMailSubject=Questar_Reports:zip problems. 
goto BADEXIT:

:ZIP_CREATED
echo Zip file created.

REM
REM FTP the files over to FTP1
REM 

cd c:\Questar\reports

copy upload.template upload.ftp

REM echo del * >>upload.ftp 
echo put Reports_generated_%DateStamp%.zip >>upload.ftp 
echo prompt >>upload.ftp 
echo quit >>upload.ftp 

cd c:\Questar\weekly

ftp -s:c:\Questar\reports\upload.ftp 
IF %ERRORLEVEL% LSS 1 goto FTP_DONE:
set VarMailText=FTP had problems transfering the zip file.
set VarMailSubject=Questar_Reports:ftp problems. 
goto BADEXIT:

:FTP_DONE

goto THE_END:


:BADEXIT
echo ------------------ Bad Exit -------------------------------------------
echo %VarMailText%
blat -to %VarNotify% -subject "%VarMailSubject%" -body "%VarMailText%"
rem *** if exist %VarCustProp% del %VarCustProp%
echo "This batch file failed."
exit /B 1

:THE_END

exit /B 0
