REM
REM	Wrapper script to execute the report generator.
REM
echo on
set VarNotify=jfong@birst.com

set SMI_HOME=c:\SMI\Data\fee6a220-e63d-41f7-845d-719e12d21d1c
set JAVA_HOME=C:\SMI\SMIWeb\jdk
set INSTALL_DIR=C:\Questar

cd c:\SMI\Data\fee6a220-e63d-41f7-845d-719e12d21d1c\catalog\shared
IF %ERRORLEVEL% LSS 1 goto DIR_EXIST:
set VarMailText=Cannot cd to reports directory to cleanup old reports.
set VarMailSubject=Questar_Reports:Directory doesn't exist. 
goto BADEXIT:

:DIR_EXIST
c:\Questar\bin\run.bat -c C:\Questar\reports\Reports_all.txt -logfile c:\Questar\reports\smiengine.log

goto THE_END:


:BADEXIT
echo ------------------ Bad Exit -------------------------------------------
echo %VarMailText%
blat -to %VarNotify% -subject "%VarMailSubject%" -body "%VarMailText%"
rem *** if exist %VarCustProp% del %VarCustProp%
echo "This batch file failed."
exit /B 1

:THE_END

exit /B 0
