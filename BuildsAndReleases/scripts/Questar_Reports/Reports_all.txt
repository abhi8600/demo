repository "..\..\repository.xml"
clearcache
catalogdir "C:\SMI\Data\fee6a220-e63d-41f7-845d-719e12d21d1c\catalog"

exportreports pdf "District Report Master.jasper" reports/DistrictReport- query "DC{Alignment Hieararchy.DistrictNumber}DistrictNumber,DC{Fiscal Time Hierarchy.Fiscal Week ID}FiscalWeekID,FDC{Fiscal Time Hierarchy.Fiscal Week ID=V{CurrentFiscalWeekID}}"

exportreports pdf "Store Report Master.jasper" reports/StoreReport- query "DC{Alignment Hieararchy.AlignmentID}AlignmentID,DC{Fiscal Time Hierarchy.Fiscal Week ID}FiscalWeekID,FDC{Fiscal Time Hierarchy.Fiscal Week ID=V{CurrentFiscalWeekID}}"

exportreports pdf "Division Report Master.jasper" reports/DivisionReport- query "DC{Alignment Hieararchy.DivisionNumber}DivisionNumber,DC{Fiscal Time Hierarchy.Fiscal Week ID}FiscalWeekID,FDC{Fiscal Time Hierarchy.Fiscal Week ID=V{CurrentFiscalWeekID}}"

exportreports pdf "Region Report Master.jasper" reports/RegionReport- query "DC{Alignment Hieararchy.RegionNumber}RegionNumber,DC{Fiscal Time Hierarchy.Fiscal Week ID}FiscalWeekID,FDC{Fiscal Time Hierarchy.Fiscal Week ID=V{CurrentFiscalWeekID}}"

exportreports pdf "Brand Report Master.jasper" reports/BrandReport- query "DC{Alignment Hieararchy.BrandNumber}BrandNumber,DC{Fiscal Time Hierarchy.Fiscal Week ID}FiscalWeekID,FDC{Fiscal Time Hierarchy.Fiscal Week ID=V{CurrentFiscalWeekID}}"

exportreports pdf "DelhaizeCommentsReports/CommentsReport.jasper" reports/CommentsReport- query "DC{Alignment Hieararchy.AlignmentID}AlignmentID,DC{Fiscal Time Hierarchy.Fiscal Week ID}FiscalWeekID,FDC{Fiscal Time Hierarchy.Fiscal Week ID=V{CurrentFiscalWeekID}}"

