@echo on
setlocal
call :get_date
:: Strip leading zeros from possible octals and decrement the day
set /a mm=1%mm%-100, dd=1%dd%-101
if %dd% NEQ 0 goto :add_zeros
:: Today is the 1st of the month - decrement the month
:: and set leap year check (ignoring centuries)
set /a mm-=1,ly=yy%%4
:: If today is 1 Jan, set date to 31st Dec
if %mm% EQU 0 (set /a dd=31, mm=12, yy-=1) else (
  rem Calculate days in last month (by Frank Westlake)
  set /a "dd=5546>>mm&1,dd+=30"
  rem Special case for February
  if %mm% EQU 2 if %ly% EQU 0 (set dd=29) else (set dd=28)
)
:add_zeros
if %dd% LSS 10 set dd=0%dd%
if %mm% LSS 10 set mm=0%mm%
set yy=%yy:~-2%
echo yesterday was %yy%%mm%%dd%
exit /B %yy%%mm%%dd%
goto :eof

:: ------------------------------------------------------------------
:Get_Date
:: ------------------------------------------------------------------
:: Generic date parser
:: Sets %dd% (01-31), %mm% (01-12) & %yy% (4 digit)

if "%date%A" LSS "A" (set toks=1-3) else (set toks=2-4)
for /f "skip=1 tokens=2-4 delims=(-)" %%a in ('echo:^|date') do (
  for /f "tokens=%toks% delims=.-/ " %%d in ('date/t') do (
    set %%a=%%d
    set %%b=%%e
    set %%c=%%f
    set toks=
  )
)
if %yy% LSS 100 set yy=20%yy%
goto :eof 