echo on 
set VarNotify=jfong@successmetricsinc.com,4157301327@vtext.com

:Beginning
chdir /D E:\RightNow_processes\EA_combo\scripts

set /A loop_counter=26
set /A loop_end=28

:Loop_Begin
	if %loop_counter% GTR %loop_end% goto Loop_Done:
	echo %loop_counter%  
	call Process_EA_Data.bat 1011%loop_counter% > process_ea.log 2>&1
	IF %ERRORLEVEL% LSS 1 goto Check_Status:
	set VarMailText=There was problems loading 1011%loop_counter%. exit code %ERRORLEVEL%
	set VarMailSubject=EA_Catchup-Process load error 
	goto badexit
	
:Check_Status
	ping -n 600 localhost 
	"C:\Program Files\Java\jdk1.6.0_10\bin\java.exe" -cp "C:\BirstConnect\dist\DataConductor.jar;C:\BirstConnect\dist\lib\*" -Djnlp.username="rnt-ea@birst.com" -Djnlp.password="7s9kObpNP00+4lvF9tdAfZRnM0+4z6PE5H1fXttis0uRmX/phVY6tJtwAMV3bu82" -Djnlp.space="Electronic Arts" -Djnlp.url="https://webdav.birst.com" -Djnlp.realm="authenticate@birst.com" com.birst.dataconductor.DataConductorCommandLine -tasks -checkstatus
	REM For checkstatus command returns
	REM     LAST_PROCESS_SUCCESSFULLY_COMPELETED = 0
	REM     CURRENTLY_PROCESSING = 1
	REM     LAST_PROCESS_FAILED = 2
	REM     NO_SERVER_RESPONSE/ ANY ERROR = 3
	IF %ERRORLEVEL% LSS 2 goto Check_Running_Status:
	set VarMailText=Processing 1011%loop_counter% returned an error. exit code %ERRORLEVEL%
	set VarMailSubject=EA_Catchup-Check Status error 
	goto badexit

:Check_Running_Status					REM     CURRENTLY_PROCESSING = 1
	IF %ERRORLEVEL% LSS 1 goto Check_Done_Status:
	goto Check_Status:

:Check_Done_Status					REM     LAST_PROCESS_SUCCESSFULLY_COMPELETED = 0
	IF %ERRORLEVEL% EQU 0 goto Processing_Done:
	set VarMailText=There was problems processing 1011%loop_counter%. exit code %ERRORLEVEL%
	set VarMailSubject=EA_Catchup-Check Status error 
	goto badexit

:Processing_Done
	set VarMailText=BirstConnect said the  1011%loop_counter% load is done, Moving to the next day.
	set VarMailSubject=EA_Catchup-Processing next load.
	blat -to %VarNotify% -subject "%VarMailSubject%" -body "%VarMailText%"
	set /A loop_counter=%loop_counter%+1
	chdir /D E:\RightNow_processes\EA_combo\scripts
goto Loop_Begin:

:Loop_Done

goto Done:

:badexit
echo ------------------ Bad Exit -------------------------------------------
set VarMailText=Script Exedit. 
set VarMailSubject=EA-Catchup-Error

echo %VarMailText%
blat -to %VarNotify% -subject "%VarMailSubject%" -body "%VarMailText%"
rem *** if exist %VarCustProp% del %VarCustProp%
echo "This batch file failed."
goto THE_END:
REM exit /B 1

:Done

echo I am done
blat -to %VarNotify%  -subject "EA-Process RightNow files done" -body "We should be done" 

:THE_END