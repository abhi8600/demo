ECHO ON
rem 
rem Process the ea data for Rightnow
rem


REM 
REM These should be input parameters in the future
REM
echo %1% %2% %3% %4%
set SMI_HOME=""
set Release_Dir=""
set Build_Tag=""
set SMI_Port=""
set TimeStamp=""
set Env_Name=""
set CurrentDir=%CD%
set Clear_Cache=""
set Clean_Flag=1

REM 
REM Setup parameters for notification
REM 

set VarNotify=jfong@successmetricsinc.com
set VarMailText=Setup email message
set VarMailSubject=Deploy Acorn

:PROCESS_INPUT

if "%1"=="-c" goto SET_CURRDATE:
if "%1"=="-d" goto SET_DATADIR:
if "%1"=="-n" goto SET_NOTIFICATION:
if "%1"=="-s" goto SET_SYSTEMNAME
if "%1"=="-t" goto SET_TRANSFERDIR:
if "%1"=="" goto PROCESS_INPUT_DONE:

goto PROCESS_INPUT_ERROR:

:SET_CURRDATE
shift
set CurrDate=%1
goto PROCESS_INPUT_LOOP:

:SET_DATADIR
shift
set Data_Dir=%1
goto PROCESS_INPUT_LOOP:

:SET_NOTIFICATION
shift
set VarNotify=%1
goto PROCESS_INPUT_LOOP:

:SET_SYSTEMNAME
shift
set Sys_Name=%1
goto PROCESS_INPUT_LOOP:

:SET_TRANSFERDIR
shift
set Transfer_dir=%1
goto PROCESS_INPUT_LOOP:

:PROCESS_INPUT_LOOP

shift
goto PROCESS_INPUT:

:PROCESS_INPUT_ERROR

echo ERROR: invalid input parameter
echo usage: Process_file.bat [-options]
echo where options include:
echo.
echo  -c ^<Current Date^>
echo 		Enter the Current Date for identifying the processing date, 
echo		the format is YYMMDD, or one will be generated.		
echo  -s ^<System Name^>
echo 		Enter the name of the system you are loading, e.g. ea, elarts, ea_reports, etc.  
echo  -t ^<Transfer Directory^>
echo 		Directory the hold the raw data file.  Usually the ftp directory	
echo  -d ^<Data directory^>
echo 		Target direotory, where you want your files uncompressed and unpackaged. 
echo  -n ^<email notification list^> comma separated (optional)
echo  		default=jfong@successmetricsinc.com,4157301327@vtext.com
echo 		NOTE - mulitple email addresses need to be separated by 
echo 			commas and inclosed in double quote
echo.

set VarMailText=Invalid arguement(s) provided. 
set VarMailSubject=Process_File:Process Arguements
goto BADEXIT

:PROCESS_INPUT_DONE

if "%Sys_Name%"=="" goto PROCESS_INPUT_ERROR:
if "%Transfer_dir%"=="" goto PROCESS_INPUT_ERROR:
if "%Data_Dir%"=="" goto PROCESS_INPUT_ERROR:

REM
REM We need a timestamp.  If it is not provided, we should create our own.
REM

if %CurrDate% neq "" ( goto CURRDATE_DEFINED: ) 
	 
rem 
rem get date
rem 

for /F "tokens=2 delims=/ " %%A in ('Date /t') do set MM=%%A
for /F "tokens=3 delims=/ " %%A in ('Date /t') do set DD=%%A
for /F "tokens=4 delims=/ " %%A in ('Date /t') do set YY=%%A
set YY=%YY:~-2%

REM set CurrDate=%YY%%MM%%DD%
set CurrDate=100918

:CURRDATE_DEFINED
echo Today's date is: %YY%%MM%%DD% %CurrDate%

rem 
echo  Define the parameters
rem 
set VarPassfile="C:\Documents and Settings\812606-jfong\My Documents\passfile.txt"

set VarPGPF1=%Transfer_dir%\%Sys_Name%_%CurrDate%.tar.gpg
set VarPGPF2=%Data_Dir%\%Sys_Name%_%CurrDate%.tar.gpg
set VarRawFile=%Data_Dir%\%Sys_Name%_%CurrDate%.tar
set VarRawFile2=%Data_Dir%\%Sys_Name%_%CurrDate%
set VarDailyDirName=%CurrDate%_%Sys_name%
set VarDailyDir=%Data_Dir%\%CurrDate%_%Sys_name%
set LoadDir=%Data_Dir%\Load
set RightNow_Home=%Data_Dir%
set VarNotify=jfong@successmetricsinc.com

echo VarPassfile %VarPassfile%
echo VarPGPF1 %VarPGPF1%
echo VarPGPF2 %VarPGPF2%
echo VarRawFile %VarRawFile%
echo VarDailyDir %VarDailyDir%

rem
echo  Move the file from the ftp directory and decrypt
rem

echo How much do we need to do? See if PGP file has already been moved and/or decrypted
if exist %VarRawFile% goto UnZipFile:
if exist %VarPGPF2% goto Decrypt:
if exist %VarPGPF1% goto Move:
set VarMailText=The PGP file was either not sent or not recieved.  Exiting since we cannot proceed without the file.
set VarMailSubject=Process_File-PGP file %VarPGPF1% does not exist
goto badexit

:Move
echo Move PGP file to archive directory
echo Moving %VarPGPF1% to %VarPGPF2% ...
copy %VarPGPF1% %VarPGPF2%
if exist %VarPGPF2% goto decrypt
set VarMailText=Attempting to move the %VarPGPF1%  file was unsuccessful. 
set VarMailSubject=Process_File-PGP file %VarPGPF2% does not exist
goto badexit

:Decrypt
echo Decrypt PGP file

echo Decrypting %VarPGPF2% into %VarRawFile% ...
"C:\Program Files\GNU\GnuPG\gpg.exe" --passphrase-fd 0 <%VarPassfile% -r "Success Metrics" --output %VarRawFile% --decrypt %VarPGPF2%

if exist %VarRawFile% goto UnZipFile:
set VarMailText=There were problems decrypting the pgp file. 
set VarMailSubject=Process_File-Raw data file %VarRawFile% does not exist
goto badexit

:UnZipFile

dir
rem
echo Unzip the files, then rename the based directory from EA to today's date
rem
cd %RightNow_Home%
IF %ERRORLEVEL% LSS 1 goto UnTarStuff:
set VarMailText=Encounter exit code %ERRORLEVEL% when attempting goto %RightNow_Home%. 
set VarMailSubject=Process_File-Problems changing directory.
goto badexit

:UnTarStuff

dir
"C:\Program Files\7-Zip\7z.exe" x %VarRawFile%
IF %ERRORLEVEL% LSS 1 goto UnZipStuff:
set VarMailText=Encounter exit code %ERRORLEVEL% when attempting to to unzip %VarRawFile%. 
set VarMailSubject=Process_File-Could not untar the %Sys_Name% file.
goto badexit

:UnZipStuff
dir
"C:\Program Files\7-Zip\7z.exe" x %VarRawFile2%
IF %ERRORLEVEL% LSS 1 goto Dir_CleanUp:
set VarMailText=Encounter exit code %ERRORLEVEL% when attempting to to unzip %VarRawFile%. 
set VarMailSubject=Process_File-Could not unzip the %Sys_Name% file.
goto badexit

:Dir_CleanUp
dir %VarDailyDirName%
IF %ERRORLEVEL% NEQ 0 goto RenameEA:
rmdir /S /Q %VarDailyDirName%
IF %ERRORLEVEL% LSS 1 goto RenameEA:
set VarMailText=Encounter exit code %ERRORLEVEL% when attempting to to delete the %VarDailyDirName% directory. 
set VarMailSubject=Process_File-Could not delete the old %VarDailyDirName% dir.
goto badexit


:RenameEA
rename %Sys_name% %VarDailyDirName%
IF %ERRORLEVEL% LSS 1 goto ExpandFiles:
set VarMailText=Encounter exit code %ERRORLEVEL% when renaming EA directory to %CurrDate%. 
set VarMailSubject=Process_File-Could not rename the EA Directory
goto badexit

:ExpandFiles

rem
echo  Email Brad that we are done.
rem

set VarMailText=Successfully processed the %System_Name% files for %CurrDate%. 
set VarMailSubject=Process_File-Process %Sys_Name% files done

goto Done:

:badexit
echo ------------------ Bad Exit -------------------------------------------
echo %VarMailText%
blat -to %VarNotify% -subject "%VarMailSubject%" -body "%VarMailText%"
rem *** if exist %VarCustProp% del %VarCustProp%
echo "This batch file failed."
exit /B 1

:Done
echo %VarMailText%
blat -to %VarNotify% -subject "%VarMailSubject%" -body "%VarMailText%"

:cleanup
echo -------------------Clean up and Exit --------------------------

del /F /S /Q %VarRawFile%
del /F /S /Q %VarRawFile2%

exit /B 0