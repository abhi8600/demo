echo on
time /t
set CurrDate=
if "%1" EQU "" ( goto No_Date: )
set TempDate=%1
set CurrDate=%TempDate:~0,6%

:No_Date
set Sys_Name1=ea
set Sys_Name2=eacs
set FTPDir=E:\RNT\EA
set ProcessFileDir=E:\RightNow_processes\EA_combo\scripts
set ProcessFileScript=E:\RightNow_processes\EA_combo\scripts\Process_file.bat
set ProcessDataDir=E:\RightNow_processes\EA_combo\Data
set LoadDir=%ProcessDataDir%\Load
set Sys_1_Dir=%ProcessDataDir%\%CurrDate%_%Sys_name1%
set Sys_2_Dir=%ProcessDataDir%\%CurrDate%_%Sys_name2%
set VarNotify=rnt-ea@birst.com

REM
REM	Define Date for processing
REM

if "%CurrDate%" neq "" ( goto CURRDATE_DEFINED: ) 
	 
rem 
rem get date
rem 

for /F "tokens=2 delims=/ " %%A in ('Date /t') do set MM=%%A
for /F "tokens=3 delims=/ " %%A in ('Date /t') do set DD=%%A
for /F "tokens=4 delims=/ " %%A in ('Date /t') do set YY=%%A
set YY=%YY:~-2%

set CurrDate=%YY%%MM%%DD%
REM set CurrDate=110105

:CURRDATE_DEFINED
echo Today's date is: %YY%%MM%%DD% %CurrDate%

call yesterday.bat
set Yesterday=%ERRORLEVEL%
echo Yesterday date is %Yesterday%

set Sys_1_Dir=%ProcessDataDir%\%CurrDate%_%Sys_name1%
set Sys_2_Dir=%ProcessDataDir%\%CurrDate%_%Sys_name2%

REM
REM decrypt and unpack the data files
REM 

:ProcessFiles1

chdir /d %ProcessFileDir%

cmd /c %ProcessFileScript% -c %CurrDate% -s %Sys_Name1% -t %FTPDir% -d %ProcessDataDir% > Process.log 2>&1

IF %ERRORLEVEL% LSS 1 goto ProcessFiles2:
set VarMailText=Encounter exit code %ERRORLEVEL% Process_file.bat failed. 
set VarMailSubject=Process_EA_Data-Could not unpack %Sys_Name1%
goto badexit

:ProcessFiles2

chdir /d %ProcessFileDir%

cmd /c %ProcessFileScript% -c %CurrDate% -s %Sys_Name2% -t %FTPDir% -d %ProcessDataDir% > Process1.log 2>&1

IF %ERRORLEVEL% LSS 1 goto ProcessFilesDone:
set VarMailText=Encounter exit code %ERRORLEVEL% Process_file.bat failed.  
set VarMailSubject=Process_EA_Data-Could not unpack %Sys_Name2%
goto badexit

:ProcessFilesDone

REM 
REM	Rename the text files so we can identify which system they are from
REM

cd %Sys_1_Dir%
for %%B in ("*.txt") do (rename %%B 88_%%B)
IF %ERRORLEVEL% LSS 1 goto RenameDone:
set VarMailText=Encounter exit code %ERRORLEVEL% failed adding 88_ to the file names.  
set VarMailSubject=Process_EA_Data-Could not rename %Sys_Name2%
goto badexit

:RenameDone

rem
echo Create the Load directory
rem

rmdir /S /Q %LoadDir%
IF %ERRORLEVEL% LSS 1 goto MakeLoadDir:
set VarMailText=Encounter exit code %ERRORLEVEL% when deleting %LoadDir%. 
set VarMailSubject=Process_EA_Data-Could not delete the Load Directory
goto badexit

:MakeLoadDir
mkdir %LoadDir%
IF %ERRORLEVEL% LSS 1 goto PopulateLoad:
set VarMailText=Encounter exit code %ERRORLEVEL% when creating %LoadDir%. 
set VarMailSubject=Process_EA_Data-Could not create the Load Directory
goto badexit

:PopulateLoad
copy /Y %Sys_1_Dir%\*.txt %LoadDir%
IF %ERRORLEVEL% LSS 1 goto PopulateLoad2:
set VarMailText=Problems copy text files from %CurrDate%_%Sys_name1% to %LoadDir%. exit code %ERRORLEVEL%
set VarMailSubject=Process_EA_Data-Could not populate the Load Directory
goto badexit

:PopulateLoad2
copy /Y %Sys_2_Dir%\*.txt %LoadDir%
IF %ERRORLEVEL% LSS 1 goto PopulateLoad3:
set VarMailText=Problems copy text files from %CurrDate%_%Sys_name2% to %LoadDir%. exit code %ERRORLEVEL%
set VarMailSubject=Process_EA_Data-Could not populate the Load Directory
goto badexit

:PopulateLoad3
rem	copy /Y %ProcessFileDir%\*.xlsx %LoadDir%
rem	IF %ERRORLEVEL% LSS 1 goto PopulateLoad4:
rem	set VarMailText=Problems copy text files from %CurrDate% to %LoadDir%. exit code %ERRORLEVEL%
rem	set VarMailSubject=Process_EA_Data-Could not populate the Load Directory
rem	goto badexit

:PopulateLoad4
rem	copy /Y %ProcessFileDir%\*.xls %LoadDir%
rem	IF %ERRORLEVEL% LSS 1 goto CreateSeq:
rem	set VarMailText=Problems copy text files from %CurrDate% to %LoadDir%. exit code %ERRORLEVEL%
rem	set VarMailSubject=Process_EA_Data-Could not populate the Load Directory
rem	goto badexit

:CreateSeq
echo %CurrDate% > %LoadDir%/%CurrDate%
IF %ERRORLEVEL% LSS 1 goto DoBirstConnect:
set VarMailText=%LoadDir%/%CurrDate% was not created with %CurrDate%. exit code %ERRORLEVEL%
set VarMailSubject=Process_EA_Data-Could not create sequence file
goto badexit

:DoBirstConnect
echo "starting the dataconductor now" 

"C:\Program Files\Java\jdk1.6.0_10\bin\java.exe" -cp "C:\BirstConnect_5_0_0\dist\lib\*;C:\BirstConnect_5_0_0\dist\*" -Djnlp.file="E:\RightNow_processes\EA_combo\scripts\configuration_files\Electronic_Arts_20110818.jnlp" com.birst.dataconductor.DataConductorCommandLine -tasks "Upload"

REM For checkstatus command returns
REM     LAST_PROCESS_SUCCESSFULLY_COMPELETED = 0
REM     CURRENTLY_PROCESSING = 1
REM     LAST_PROCESS_FAILED = 2
REM     NO_SERVER_RESPONSE/ ANY ERROR = 3

IF %ERRORLEVEL% LSS 1 goto BirstConnectDone:
set VarMailText=BirstConnect had problems uploading the %CurrDate% files. exit code %ERRORLEVEL%
set VarMailSubject=Process_EA_Data-BirstConnect encountered errors
goto badexit

:BirstConnectDone
set VarMailText=BirstConnect uploaded the %CurrDate% files successfully for processing.
set VarMailSubject=Process_EA_Data-BirstConnect completed the upload.
blat -to %VarNotify% -subject "%VarMailSubject%" -body "%VarMailText%"
echo Birst Connect complete without error.

REM wait 60 seconds to make sure process starts
ping -n 60 localhost 

:Check_Status
	time /T
	"C:\Program Files\Java\jdk1.6.0_10\bin\java.exe" -cp "C:\BirstConnect_5_0_0\dist\lib\*;C:\BirstConnect_5_0_0\dist\*" -Djnlp.file="E:\RightNow_processes\EA_combo\scripts\configuration_files\Electronic_Arts_20110818.jnlp"  com.birst.dataconductor.DataConductorCommandLine -tasks -checkstatus
	REM For checkstatus command returns
	REM     LAST_PROCESS_SUCCESSFULLY_COMPELETED = 0
	REM     CURRENTLY_PROCESSING = 1
	REM     LAST_PROCESS_FAILED = 2
	REM     NO_SERVER_RESPONSE/ ANY ERROR = 3
	IF %ERRORLEVEL% LSS 2 goto Check_Running_Status:
	set VarMailText=Status check returned a value larger than 2. exit code %ERRORLEVEL%
	set VarMailSubject=Process_EA_Data-Check Status error 
	goto badexit

:Check_Running_Status					REM     CURRENTLY_PROCESSING = 1
	IF %ERRORLEVEL% LSS 1 goto Check_Done_Status:
	ping -n 600 localhost 
	time /T
	goto Check_Status:

:Check_Done_Status					REM Last_Process_Successfully_Completed = 0
	IF %ERRORLEVEL% EQU 0 goto Processing_Done:
	set VarMailText=Status check returned a value less than 0. exit code %ERRORLEVEL%
	set VarMailSubject=Process_EA_Data-Check Status error 
	goto badexit

:Processing_Done
	set VarMailText=BirstConnect said processing of the daily files are done.
	set VarMailSubject=Process_EA_Data-Daily files processed.
	blat -to %VarNotify% -subject "%VarMailSubject%" -body "%VarMailText%"

REM 
REM After processing the daily files, run the process group
REM 

:DoBirstConnectProcessGroup
echo "starting the dataconductor for Process Groups now" 

"C:\Program Files\Java\jdk1.6.0_10\bin\java.exe" -cp "C:\BirstConnect_5_0_0\dist\lib\*;C:\BirstConnect_5_0_0\dist\*" -Djnlp.file="E:\RightNow_processes\EA_combo\scripts\configuration_files\Electronic_Arts_20110818.jnlp" com.birst.dataconductor.DataConductorCommandLine -tasks "Post_Processing"

REM For checkstatus command returns
REM     LAST_PROCESS_SUCCESSFULLY_COMPELETED = 0
REM     CURRENTLY_PROCESSING = 1
REM     LAST_PROCESS_FAILED = 2
REM     NO_SERVER_RESPONSE/ ANY ERROR = 3

IF %ERRORLEVEL% LSS 1 goto BirstConnectProcessGroupDone:
set VarMailText=BirstConnect had problems initializing the post processing task. exit code %ERRORLEVEL%
set VarMailSubject=Process_EA_Data-BirstConnect encountered errors
goto badexit

:BirstConnectProcessGroupDone
set VarMailText=BirstConnect started Post Processing successfully.
set VarMailSubject=Process_EA_Data-BirstConnect started Post Processing.
echo Birst Connect complete without error.


goto Done:

:badexit
echo ------------------ Bad Exit -------------------------------------------
echo %VarMailText%
blat -to %VarNotify% -subject "%VarMailSubject%" -body "%VarMailText%"
rem *** if exist %VarCustProp% del %VarCustProp%
echo "This batch file failed."
time /t
exit /B 1

:Done
echo %VarMailText%
blat -to %VarNotify% -subject "%VarMailSubject%" -body "%VarMailText%"

:cleanup
echo -------------------Clean up and Exit --------------------------
time /t
exit /B 0