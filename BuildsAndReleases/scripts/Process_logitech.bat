rem 
rem Process the logitech data for Rightnow
rem

echo on 
rem 
rem get date
rem 

for /F "tokens=2 delims=/ " %%A in ('Date /t') do set MM=%%A
for /F "tokens=3 delims=/ " %%A in ('Date /t') do set DD=%%A
for /F "tokens=4 delims=/ " %%A in ('Date /t') do set YY=%%A
set YY=%YY:~-2%

set CurrDate=%YY%%MM%%DD%
	rem set CurrDate=080715
echo Today's date is: %YY%%MM%%DD% %CurrDate%

rem 
echo  Define the parameters
rem 
set VarPassfile="c:\Documents and Settings\jfong.COLO\My Documents\passfile.txt"

set VarPGPF1=c:\FileTransfer\localuser\rightnow\logitech_%CurrDate%.tgz.gpg
set VarPGPF2=c:\RightNow\logitech\logitech_%CurrDate%.tgz.gpg
set VarRawFile=c:\RightNow\logitech\logitech_%CurrDate%.tgz
set VarDailyDir=c:\RightNow\logitech\%CurrDate%
set LoadDir=c:\RightNow\logitech\Load
set RightNow_Home=c:\RightNow\logitech
set Colo6_DailyDir=\\smicolo6\e$\data\RightNow\logitech\%CurrDate%
set Colo6_LoadDir=\\smicolo6\e$\data\RightNow\logitech\Load
set VarNotify=jfong@successmetricsinc.com,jeromefong@yahoo.com

goto TestParams:
	set VarPGPF1=c:\jfong\Rightnow\logitech_%CurrDate%.tgz.gpg
	set VarPGPF2=c:\\jfong\RightNow\Logitech\logitech_%CurrDate%.tgz.gpg
	set VarRawFile=c:\jfong\RightNow\Logitech\logitech_%CurrDate%.tgz
	set VarDailyDir=c:\jfong\RightNow\Logitech\%CurrDate%
	set LoadDir=c:\jfong\RightNow\Logitech\Load
	set RightNow_Home=c:\jfong\RightNow\Logitech
	set Colo6_DailyDir=\\smicolo6\e$\data\jfong\logitech\%CurrDate%
	set Colo6_LoadDir=\\smicolo6\e$\data\jfong\logitech\Load
	set VarNotify=jfong@successmetricsinc.com,jeromefong@yahoo.com
:TestParams

echo VarPassfile %VarPassfile%
echo VarPGPF1 %VarPGPF1%
echo VarPGPF2 %VarPGPF2%
echo VarRawFile %VarRawFile%
echo VarDailyDir %VarDailyDir%

rem
echo  Move the file from the ftp directory and decrypt
rem

echo How much do we need to do? See if PGP file has already been moved and/or decrypted
if exist %VarRawFile% goto UnZipFile:
if exist %VarPGPF2% goto Decrypt:
if exist %VarPGPF1% goto Move:
set VarMailText=The PGP file was either not sent or not recieved.  Exiting since we cannot proceed without the file.
set VarMailSubject=RightNow-PGP file %VarPGPF1% does not exist
goto badexit

:Move
echo Move PGP file to archive directory
echo Moving %VarPGPF1% to %VarPGPF2% ...
move %VarPGPF1% %VarPGPF2%
if exist %VarPGPF2% goto decrypt
set VarMailText=Attempting to move the %VarPGPF1%  file was unsuccessful. 
set VarMailSubject=RightNow-PGP file %VarPGPF2% does not exist
goto badexit

:Decrypt
echo Decrypt PGP file

echo Decrypting %VarPGPF2% into %VarRawFile% ...
gpg --passphrase-fd 0 <%VarPassfile% -r "Success Metrics" --output %VarRawFile% --decrypt %VarPGPF2%

if exist %VarRawFile% goto UnZipFile:
set VarMailText=There were problems decrypting the pgp file. 
set VarMailSubject=RightNow-Raw data file %VarRawFile% does not exist
goto badexit

:UnZipFile
rem
echo Unzip the files, then rename the based directory from Logitech to today's date
rem

cd %RightNow_Home%
IF %ERRORLEVEL% LSS 1 goto GunzipStuff:
set VarMailText=Encounter exit code %ERRORLEVEL% when attempting goto %RightNow_Home%. 
set VarMailSubject=RightNow-Problems changing directory.
goto badexit

:GunzipStuff
c:\unixkit-tiny\bin\gunzip.exe < %VarRawFile% | c:\unixkit-tiny\bin\tar.exe xvf -
IF %ERRORLEVEL% LSS 1 goto RenameLogitech:
set VarMailText=Encounter exit code %ERRORLEVEL% when attempting to to unzip %VarRawFile%. 
set VarMailSubject=RightNow-Could not unzip the RightNow file.
goto badexit

:RenameLogitech
rename Logitech %CurrDate%
IF %ERRORLEVEL% LSS 1 goto ExpandFiles:
set VarMailText=Encounter exit code %ERRORLEVEL% when renaming Logitech directory to %CurrDate%. 
set VarMailSubject=RightNow-Could not rename the Logitech Directory
goto badexit

:ExpandFiles
rem
echo Create the Load directory
rem

rmdir /S /Q %LoadDir%
IF %ERRORLEVEL% LSS 1 goto MakeLoadDir:
set VarMailText=Encounter exit code %ERRORLEVEL% when deleting %LoadDir%. 
set VarMailSubject=RightNow-Could not delete the Load Directory
goto badexit

:MakeLoadDir
mkdir %LoadDir%
IF %ERRORLEVEL% LSS 1 goto PopulateLoad:
set VarMailText=Encounter exit code %ERRORLEVEL% when creating %LoadDir%. 
set VarMailSubject=RightNow-Could not create the Load Directory
goto badexit

:PopulateLoad
copy /Y %VarDailyDir%\*.txt %LoadDir%
IF %ERRORLEVEL% LSS 1 goto CreateSeq:
set VarMailText=Problems copy text files from %CurrDate% to %LoadDir%. 
set VarMailSubject=RightNow-Could not populate the Load Directory
goto badexit

:CreateSeq
echo %CurrDate% > %LoadDir%/%CurrDate%
IF %ERRORLEVEL% LSS 1 goto DoRoboCopy:
set VarMailText=%LoadDir%/%CurrDate% was not created with %CurrDate%. 
set VarMailSubject=RightNow-Could not create sequence file
goto badexit

:DoRoboCopy
rem 
echo RoboCopy the files over to smicolo6
rem

robocopy /Z %VarDailyDir% %Colo6_DailyDir%
IF %ERRORLEVEL% EQU 1 goto CopyLoadDir:
set VarMailText=There were problems copying the daily files to %Colo6_DailyDir%. 
set VarMailSubject=RightNow-Encountered problems with RoboCopy 1
goto badexit

:CopyLoadDir
robocopy /MIR /Z %LoadDir% %Colo6_LoadDir%
IF %ERRORLEVEL% EQU 1 goto EmailBrad:
set VarMailText=There were problems copying the daily files to %Colo6_LoadDir%. 
set VarMailSubject=RightNow-Encountered problems with RoboCopy 2
goto badexit

:EmailBrad
rem
echo  Email Brad that we are done.
rem

set VarMailText=Successfully processed the Logitech files for %CurrDate%. 
set VarMailSubject=RightNow-Process RightNow files done

goto Done:

:badexit
echo ------------------ Bad Exit -------------------------------------------
echo %VarMailText%
blat -to %VarNotify% -subject "%VarMailSubject%" -body "%VarMailText%"
rem *** if exist %VarCustProp% del %VarCustProp%
echo "This batch file failed."
exit /B 1

:done
echo %VarMailText%
blat -to %VarNotify% -subject "%VarMailSubject%" -body "%VarMailText%"

:cleanup
echo -------------------Clean up and Exit --------------------------

