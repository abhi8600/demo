ECHO ON

REM
REM We need a timestamp.  If it is not provided, we should create our own.
REM


if "%1"=="" goto TIMESTAMP_DEFINE: 
set TStamp=%1
goto TIMESTAMP_DEFINED:

:TIMESTAMP_DEFINE
for /f "tokens=1-3 delims=:." %%a in ("%time%") do set hh_mm_stamp=%%a%%b
echo %hh_mm_stamp%
for /f "tokens=1-3 delims=/-" %%a in ("%date%") do set teststamp=%%c%%b%%a
echo %teststamp%
set TStamp="%teststamp%%hh_mm_stamp%"

:TIMESTAMP_DEFINED
echo TimeStamp=%TStamp%

echo ------------------------------------------------------------
echo -------------Deploying Acorn to amdpjhala-------------------

call "F:\BirstEnv\Deploy_Projects.bat" "F:\BirstEnv\smi\SavedConfig\Pooja" -a "\\amdpjhala\main\Acorn"
IF %ERRORLEVEL% LSS 1 goto DEPLOY_BUILDS:
set VarMailText=Acorn deploy Problems
set VarMailSubject=Pooja_Deploy:Deployment Errors
goto BADEXIT:

:DEPLOY_BUILDS
echo -------------Deploying builds to amdpjhala------------------

call "F:\BirstEnv\Copy_Builds.bat" "\\amdpjhala\downloads\Builds" %TStamp%
IF %ERRORLEVEL% LSS 1 goto THE_END:
set VarMailText=Builds deploy Problems
set VarMailSubject=Pooja_Deploy:Deployment Errors
goto BADEXIT:


:BADEXIT
echo ------------------ Bad Exit --------------------------------
echo %VarNotify% 
echo %VarMailSubject%
echo %VarMailText%

echo "This batch file failed."
exit /B 1

:THE_END
echo "Successfully deployed builds to pooja's machine."
exit /B 0