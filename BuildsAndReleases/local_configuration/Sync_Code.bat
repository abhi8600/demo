ECHO ON

set TimeStamp=%1
	
:TIMESTAMP_DEFINED
echo TimeStamp: %TimeStamp%

cd "f:\Birst Workspace"
dir

rem  get code with the label ACORN_Nightly_%TimeStamp%, LogicalExpression_Nightly_%TimeStamp% and SMI_Nightly_%TimeStamp%

:GET_Acorn
if "%TimeStamp%"=="" goto Latest_Acron:
"C:\Program Files\CVSNT\cvs.exe" -d :pserver:mpandit@SMISRC:/CVS update -C -r ACORN_Nightly_%TimeStamp% Acorn
IF %ERRORLEVEL% LSS 1 goto GET_LogicalExpression:
set VarMailText=CVS checkout had problems 
set VarMailSubject=Sync_Code:Acorn checkout errors
goto BADEXIT:

:Latest_Acron
"C:\Program Files\CVSNT\cvs.exe" -d :pserver:mpandit@SMISRC:/CVS update -AC Acorn
IF %ERRORLEVEL% LSS 1 goto GET_LogicalExpression:
set VarMailText=CVS checkout had problems 
set VarMailSubject=Sync_Code:Acorn checkout errors
goto BADEXIT:

:GET_LogicalExpression
if "%TimeStamp%"=="" goto Latest_LogicalExpression:
"C:\Program Files\CVSNT\cvs.exe" -d :pserver:mpandit@SMISRC:/CVS update -C -r LogicalExpression_Nightly_%TimeStamp% LogicalExpression
IF %ERRORLEVEL% LSS 1 goto GET_AcornTestConsole:
set VarMailText=CVS checkout had problems 
set VarMailSubject=Sync_Code:LogicalExpression checkout errors
goto BADEXIT:

:Latest_LogicalExpression
"C:\Program Files\CVSNT\cvs.exe" -d :pserver:mpandit@SMISRC:/CVS update -AC LogicalExpression
IF %ERRORLEVEL% LSS 1 goto GET_AcornTestConsole:
set VarMailText=CVS checkout had problems 
set VarMailSubject=Sync_Code:LogicalExpression checkout errors
goto BADEXIT:

:GET_AcornTestConsole
if "%TimeStamp%"=="" goto Latest_AcornTestConsole:
"C:\Program Files\CVSNT\cvs.exe" -d :pserver:mpandit@SMISRC:/CVS update -C -r ACORN_Nightly_%TimeStamp% AcornTestConsole
IF %ERRORLEVEL% LSS 1 goto GET_SMI:
set VarMailText=CVS checkout had problems 
set VarMailSubject=Sync_Code:AcornTestConsole checkout errors
goto BADEXIT:

:Latest_AcornTestConsole
"C:\Program Files\CVSNT\cvs.exe" -d :pserver:mpandit@SMISRC:/CVS update -AC AcornTestConsole
IF %ERRORLEVEL% LSS 1 goto GET_SMI:
set VarMailText=CVS checkout had problems 
set VarMailSubject=Sync_Code:AcornTestConsole checkout errors
goto BADEXIT:

:GET_SMI
if "%TimeStamp%"=="" goto Latest_SMI:
"C:\Program Files\CVSNT\cvs.exe" -d :pserver:mpandit@SMISRC:/CVS update -C -r SMI_Nightly_%TimeStamp% SMI
IF %ERRORLEVEL% LSS 1 goto GET_QA:
set VarMailText=CVS checkout had problems 
set VarMailSubject=Sync_Code:SMI checkout errors
goto BADEXIT:

:Latest_SMI
"C:\Program Files\CVSNT\cvs.exe" -d :pserver:mpandit@SMISRC:/CVS update -AC SMI
IF %ERRORLEVEL% LSS 1 goto GET_QA:
set VarMailText=CVS checkout had problems 
set VarMailSubject=Sync_Code:SMI checkout errors
goto BADEXIT:

:GET_QA
"C:\Program Files\CVSNT\cvs.exe" -d :pserver:mpandit@SMISRC:/CVS update -AC QA
IF %ERRORLEVEL% LSS 1 goto THE_END:
set VarMailText=CVS checkout had problems 
set VarMailSubject=Sync_Code:QA checkout errors
goto BADEXIT:

:BADEXIT
echo ------------------ Bad Exit -------------------------------------------
echo %VarNotify% 
echo %VarMailSubject%
echo %VarMailText%

echo "This batch file failed."
exit /B 1

:THE_END

exit /B 0