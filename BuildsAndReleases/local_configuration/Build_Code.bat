ECHO ON

set SMI_SOURCE_ROOT=F:\Birst Workspace

:CLEAN_SMI
cd %SMI_SOURCE_ROOT%\SMI\staging\minnesota
call ant clean
IF %ERRORLEVEL% LSS 1 goto BUILD_SMI:
set VarMailText=SMI clean problems 
set VarMailSubject=Build_Code:Build Errors
goto BADEXIT:

:BUILD_SMI
call ant
IF %ERRORLEVEL% LSS 1 goto BUILD_LOGICAL_EXPRESSION:
set VarMailText=SMI build problems 
set VarMailSubject=Build_Code:Build Errors
goto BADEXIT:

:BUILD_LOGICAL_EXPRESSION
cd "%SMI_SOURCE_ROOT%\LogicalExpression"
"C:\Program Files\Microsoft Visual Studio 9.0\Common7\IDE\devenv" "LogicalExpression.sln" /clean
"C:\Program Files\Microsoft Visual Studio 9.0\Common7\IDE\devenv" "LogicalExpression.sln" /build "Debug"
IF %ERRORLEVEL% LSS 1 goto BUILD_ADMIN_TOOL:
set VarMailText=LogicalExpression build problems 
set VarMailSubject=Build_Code:Build Errors
goto BADEXIT:

:BUILD_ADMIN_TOOL
cd "%SMI_SOURCE_ROOT%\SMI\Performance Optimizer Administration"
"C:\Program Files\Microsoft Visual Studio 9.0\Common7\IDE\devenv" "Performance Optimizer Administration.sln" /clean
"C:\Program Files\Microsoft Visual Studio 9.0\Common7\IDE\devenv" "Performance Optimizer Administration.sln" /build "Release"
IF %ERRORLEVEL% LSS 1 goto BUILD_ACORN:
set VarMailText=Admin Tool build problems 
set VarMailSubject=Build_Code:Build Errors
goto BADEXIT:

:BUILD_ACORN
cd %SMI_SOURCE_ROOT%\Acorn
"C:\Program Files\Microsoft Visual Studio 9.0\Common7\IDE\devenv" acorn.sln /clean
"C:\Program Files\Microsoft Visual Studio 9.0\Common7\IDE\devenv" acorn.sln /deploy
IF %ERRORLEVEL% LSS 1 goto BUILD_ACORN_TEST_CONSOLE:
set VarMailText=Acorn build problems 
set VarMailSubject=Build_Code:Build Errors
goto BADEXIT:

:BUILD_ACORN_TEST_CONSOLE
cd %SMI_SOURCE_ROOT%\AcornTestConsole
"C:\Program Files\Microsoft Visual Studio 9.0\Common7\IDE\devenv" AcornTestConsole.sln /clean
"C:\Program Files\Microsoft Visual Studio 9.0\Common7\IDE\devenv" AcornTestConsole.sln /build "Release"
IF %ERRORLEVEL% LSS 1 goto THE_END:
set VarMailText=AcornTestConsole build problems 
set VarMailSubject=Build_Code:Build Errors
goto BADEXIT:

:BADEXIT
echo ------------------ Bad Exit -------------------------------------------
echo %VarNotify% 
echo %VarMailSubject%
echo %VarMailText%

echo "This batch file failed."
exit /B 1

:THE_END

exit /B 0