ECHO ON

REM
REM We need a timestamp.  If it is not provided, we should create our own.
REM


if "%1"=="" goto TIMESTAMP_DEFINE: 
set TStamp=%1
goto TIMESTAMP_DEFINED:

:TIMESTAMP_DEFINE
for /f "tokens=1-3 delims=:." %%a in ("%time%") do set hh_mm_stamp=%%a%%b
echo %hh_mm_stamp%
for /f "tokens=1-3 delims=/-" %%a in ("%date%") do set teststamp=%%c%%b%%a
echo %teststamp%
set TStamp="%teststamp%%hh_mm_stamp%"

:TIMESTAMP_DEFINED
echo TimeStamp=%TStamp%

echo ------------------------------------------------------------
echo --------Deploying All Projects to amdmpandit----------------

call "F:\BirstEnv\Deploy_Projects.bat" "F:\BirstEnv\smi\SavedConfig\Mitesh" -a "F:\BirstEnv\smi\Acorn" -p "F:\BirstEnv\smi\SMIPerformanceEngine" -w "F:\BirstEnv\smi\SMIWeb" -b "F:\BirstEnv\smi\AdminTool"
IF %ERRORLEVEL% LSS 1 goto DEPLOY_BUILDS:
set VarMailText=Acorn deploy Problems
set VarMailSubject=Mitesh_Deploy:Deployment Errors
goto BADEXIT:

:DEPLOY_BUILDS
echo -------------Deploying builds to amdmpandit-----------------

call "F:\BirstEnv\Copy_Builds.bat" "F:\BirstEnv\smi\Builds" %TStamp%
IF %ERRORLEVEL% LSS 1 goto THE_END:
set VarMailText=Builds deploy Problems
set VarMailSubject=Mitesh_Deploy:Deployment Errors
goto BADEXIT:


:BADEXIT
echo ------------------ Bad Exit --------------------------------
echo %VarNotify% 
echo %VarMailSubject%
echo %VarMailText%

echo "This batch file failed."
exit /B 1

:THE_END
echo "Successfully deployed builds to mitesh's machine."
exit /B 0