ECHO ON

set ADMIN_TOOL_ROOT=""
set ACORN_ROOT=""
set SMIPERF_ROOT=""
set SMIWEB_ROOT=""
set SMI_SOURCE_ROOT=F:\Birst Workspace

REM 
REM Setup parameters for notification
REM 

set VarNotify=mpandit@birst.com
set VarMailText=Setup email message
set VarMailSubject=Deploy Projects

if "%1"=="" goto PROCESS_INPUT_ERROR:

:SET_SAVED_CONFIG_ROOT
set SAVED_CONFIG_ROOT=%1
shift

if "%1"=="" goto PROCESS_INPUT_ERROR:

:PROCESS_INPUT

if "%1"=="-b" goto SET_ADMIN_TOOL_ROOT:
if "%1"=="-a" goto SET_ACORN_ROOT:
if "%1"=="-p" goto SET_SMIPERF_ROOT:
if "%1"=="-w" goto SET_SMIWEB_ROOT:
if "%1"=="" goto PROCESS_INPUT_DONE:

goto PROCESS_INPUT_ERROR:

:SET_ADMIN_TOOL_ROOT
shift
set ADMIN_TOOL_ROOT=%1
goto DEPLOY_ADMIN_TOOL:

:SET_ACORN_ROOT
shift
set ACORN_ROOT=%1
goto DEPLOY_ACORN:

:SET_SMIPERF_ROOT
shift
set SMIPERF_ROOT=%1
goto DEPLOY_SMI_PERF_ENGINE:

:SET_SMIWEB_ROOT
shift
set SMIWEB_ROOT=%1
goto DEPLOY_SMIWEB:

:PROCESS_INPUT_LOOP
shift
goto PROCESS_INPUT:

:PROCESS_INPUT_ERROR

echo ERROR: invalid input parameter
echo usage: Deploy_Projects ^<Saved_Config_Root^> [-options]
echo where ^<Saved_Config_Root^> is the directory where current configuration files are to be saved
echo at least 1 option is required
echo where options include:
echo.
echo  -a ^<Acorn_Root^>
echo 		Enter the directory where you wish to deploy Acorn
echo  -b ^<Admin_Tool_Root^> 
echo  		Enter the directory where you wish to deploy Admin Tool (Birst Administration.exe)
echo  -p ^<SMIPerformanceEngine_Root^>
echo 		Enter the directory where you wish to deploy SMI_Performance_Engine
echo  -w ^<SMIWeb_Root^>
echo 		Enter the directory where you wish to deploy SMIWeb
echo.

set VarMailText=Invalid arguement(s) provided. 
set VarMailSubject=Deploy_Projects:Process Arguements
goto BADEXIT:

:PROCESS_INPUT_DONE

echo SMI_SOURCE_ROOT	= %SMI_SOURCE_ROOT%
echo ACORN_ROOT		= %ACORN_ROOT%
echo SMIPERF_ROOT	= %SMIPERF_ROOT%
echo SMIWEB_ROOT	= %SMIWEB_ROOT%

goto THE_END:

:DEPLOY_ADMIN_TOOL
del "%ADMIN_TOOL_ROOT%\Birst Administration.exe"
copy "%SMI_SOURCE_ROOT%\SMI\Performance Optimizer Administration\bin\Release\Birst Administration.exe" %ADMIN_TOOL_ROOT%
IF %ERRORLEVEL% LSS 1 goto PROCESS_INPUT_LOOP:
set VarMailText=ADMIN_TOOL deploy problems 
set VarMailSubject=Deploy_Projects:Deployment Errors
goto BADEXIT:

:DEPLOY_SMI_PERF_ENGINE
del /Q %SAVED_CONFIG_ROOT%\run.bat
copy %SMIPERF_ROOT%\bin\run.bat %SAVED_CONFIG_ROOT%
del /Q %SAVED_CONFIG_ROOT%\load_warehouse.bat
copy %SMIPERF_ROOT%\bin\load_warehouse.bat %SAVED_CONFIG_ROOT%

"C:\Program Files\WinZip\WINZIP32.EXE" -e -o "%SMI_SOURCE_ROOT%\SMI\PerformanceEngine\minnesota\deployable_units\PerformanceEngine.zip" %SMIPERF_ROOT%
IF %ERRORLEVEL% LSS 1 goto PROCESS_INPUT_LOOP:
set VarMailText=SMI_PERF_ENGINE deploy problems 
set VarMailSubject=Deploy_Projects:Deployment Errors
goto BADEXIT:

:DEPLOY_SMIWEB
del "%SMIWEB_ROOT%\tomcat\webapps\SMIWeb.war"
cd "%SMIWEB_ROOT%\tomcat\webapps"
rmdir /S /Q "%SMIWEB_ROOT%\tomcat\webapps\SMIWeb"
cd "%SMIWEB_ROOT%\tomcat\temp"
del /Q /S *
cd "%SMIWEB_ROOT%\tomcat\work"
del /Q /S *

copy "%SMI_SOURCE_ROOT%\SMI\SMIWeb\minnesota\deployable_units\SMIWeb.war" "%SMIWEB_ROOT%\tomcat\webapps"
IF %ERRORLEVEL% LSS 1 goto PROCESS_INPUT_LOOP:
set VarMailText=SMIWeb deploy problems 
set VarMailSubject=Deploy_Projects:Deployment Errors
goto BADEXIT:

:DEPLOY_ACORN
del /Q "%SAVED_CONFIG_ROOT%\Web.config"
copy "%ACORN_ROOT%\Web.config" %SAVED_CONFIG_ROOT%
del /Q "%SAVED_CONFIG_ROOT%\Spaces.config"
copy "%ACORN_ROOT%\Spaces.config" %SAVED_CONFIG_ROOT%
del /Q "%SAVED_CONFIG_ROOT%\AcornTestConsole.exe.config"
copy "%ACORN_ROOT%\bin\AcornTestConsole.exe.config" %SAVED_CONFIG_ROOT%
del /Q "%SAVED_CONFIG_ROOT%\runAcornTestConsole.bat"
copy "%ACORN_ROOT%\bin\runAcornTestConsole.bat" %SAVED_CONFIG_ROOT%

del /S /Q %ACORN_ROOT%\*.*

cd %SMI_SOURCE_ROOT%\Acorn\Acorn

copy *.* %ACORN_ROOT%

pushd %ACORN_ROOT%
mkdir bin
cd bin
xcopy /Y/S "%SMI_SOURCE_ROOT%\Acorn\Acorn\bin" .
copy "%SMI_SOURCE_ROOT%\AcornTestConsole\AcornTestConsole\bin\Release\AcornTestConsole.exe" .
copy "F:\Birst Workspace\AcornTestConsole\AcornTestConsole\AcornTestConsole.exe.config" .
popd

pushd %ACORN_ROOT%
mkdir images
cd images
xcopy /Y/S "%SMI_SOURCE_ROOT%\Acorn\Acorn\images" .
popd

pushd %ACORN_ROOT%
mkdir lib
cd lib
xcopy /Y/S "%SMI_SOURCE_ROOT%\Acorn\Acorn\lib" .
popd

pushd %ACORN_ROOT%
mkdir swf
cd swf
xcopy /Y/S "%SMI_SOURCE_ROOT%\Acorn\Acorn\swf" .
popd
IF %ERRORLEVEL% LSS 1 goto PROCESS_INPUT_LOOP:
set VarMailText=Acorn deploy Problems
set VarMailSubject=Deploy_Projects:Deployment Errors
goto BADEXIT:

:BADEXIT
echo ------------------ Bad Exit -------------------------------------------
echo %VarNotify% 
echo %VarMailSubject%
echo %VarMailText%

echo "This batch file failed."
exit /B 1

:THE_END

exit /B 0