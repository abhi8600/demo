ECHO ON

REM
REM We need a timestamp.  If it is not provided, we should create our own.
REM


if "%1"=="" goto TIMESTAMP_DEFINE: 
set TStamp=%1
goto TIMESTAMP_DEFINED:

:TIMESTAMP_DEFINE
for /f "tokens=1-3 delims=:." %%a in ("%time%") do set hh_mm_stamp=%%a%%b
echo %hh_mm_stamp%
for /f "tokens=1-3 delims=/-" %%a in ("%date%") do set teststamp=%%c%%b%%a
echo %teststamp%
set TStamp="%teststamp%%hh_mm_stamp%"

:TIMESTAMP_DEFINED
echo TimeStamp=%TStamp%

echo ------------------------------------------------------------
echo ----------Synchronizing code with tag %TStamp%-----------

call "F:\BirstEnv\Sync_Code" %1

echo ------------------------------------------------------------
echo -------------------Building code----------------------------

call "F:\BirstEnv\Build_Code.bat"

echo ------------------------------------------------------------
echo -------------Deploying builds to amdmpandit-----------------

call "F:\BirstEnv\Deploy_Projects.bat" "F:\BirstEnv\smi\SavedConfig\Mitesh" -a "F:\BirstEnv\smi\Acorn" -p "F:\BirstEnv\smi\SMIPerformanceEngine" -w "F:\BirstEnv\smi\SMIWeb" -b "F:\BirstEnv\smi\AdminTool"

call "F:\BirstEnv\Copy_Builds.bat" "F:\BirstEnv\smi\Builds" %TStamp%

echo ------------------------------------------------------------
echo -------------Deploying builds to amdpvyas------------------

call "F:\BirstEnv\Deploy_Projects.bat" "F:\BirstEnv\smi\SavedConfig\Purvi" -a "\\amdpvyas\Dev\Main\acorn"

call "F:\BirstEnv\Copy_Builds.bat" "\\amdpvyas\Downloads\builds" %TStamp%

echo ------------------------------------------------------------
echo -------------Deploying builds to amdpjhala-----------------

call "F:\BirstEnv\Deploy_Projects.bat" "F:\BirstEnv\smi\SavedConfig\Pooja" -a "\\amdpjhala\main\Acorn"

call "F:\BirstEnv\Copy_Builds.bat" "\\amdpjhala\downloads\Builds" %TStamp%