ECHO ON

set SMI_SOURCE_ROOT=F:\Birst Workspace

if "%1"=="" goto PROCESS_INPUT_ERROR:
set COPY_ROOT=%1

if "%2"=="" goto PROCESS_INPUT_ERROR:
set TimeStamp=%2

goto :COPY

:PROCESS_INPUT_ERROR
echo ERROR: invalid input parameter
echo usage: Copy_Builds ^<Copy_Root^> ^<TimeStamp^>
echo where ^<Copy_Root^> is the directory under which ^<TimeStamp^> directory will be created and the builds will be saved
goto BADEXIT:

:COPY
pushd %COPY_ROOT%

rmdir /S /Q %TimeStamp%
mkdir %TimeStamp%
popd

copy "%SMI_SOURCE_ROOT%\SMI\Performance Optimizer Administration\bin\Release\Birst Administration.exe" "%COPY_ROOT%\%TimeStamp%"
copy "%SMI_SOURCE_ROOT%\SMI\PerformanceEngine\minnesota\deployable_units\PerformanceEngine.zip" "%COPY_ROOT%\%TimeStamp%"
copy "%SMI_SOURCE_ROOT%\SMI\SMIWeb\minnesota\deployable_units\SMIWeb.war" "%COPY_ROOT%\%TimeStamp%"
xcopy /Y/S "%SMI_SOURCE_ROOT%\QA" "%COPY_ROOT%\%TimeStamp%\QA\"

goto THE_END:

:BADEXIT
echo ------------------ Bad Exit -------------------------------------------
echo %VarNotify% 
echo %VarMailSubject%
echo %VarMailText%

echo "This batch file failed."
exit /B 1

:THE_END

exit /B 0