﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using System.IO;
using MoveSpace.Utils;

namespace MoveSpace.tests
{
    public class CompressUtilTest
    {

        [Test]
        public void testCompression()
        {
            string tmpFile = Path.GetTempFileName();
            String randomnText = "RandomnText";
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < 100; i++)
            {                
                sb.Append(randomnText);
                sb.AppendLine(i.ToString());
            }
            File.WriteAllText(tmpFile, sb.ToString());            
            CompressionUtil cUtil = new CompressionUtil();
            string compressedFile = cUtil.compressFile(tmpFile);
            Console.WriteLine("File compressed " + compressedFile);
            string backUpFile = tmpFile + ".1";
            File.Move(tmpFile, backUpFile);
            string deCompressedFile = cUtil.deCompressFile(compressedFile);
            Assert.AreEqual(tmpFile, deCompressedFile);
            Assert.AreEqual(File.ReadAllText(deCompressedFile), sb.ToString());            
            File.Delete(deCompressedFile);
            File.Delete(compressedFile);
            File.Delete(backUpFile);
        }

    }
}
