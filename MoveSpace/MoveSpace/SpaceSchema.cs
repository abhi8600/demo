﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Acorn;

namespace MoveSpace
{
    public class SpaceSchema
    {
        public Space sp;
        public List<TableSchema> Tables;
        public List<DatabaseIndex> IndexList;
        public List<Publish> Publishes;
        public List<User> Users;
        public string originalDatabaseDriver;
    }
}
