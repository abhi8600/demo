﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO.Compression;
using System.IO;

namespace MoveSpace.Utils
{
    public class CompressionUtil
    {   
        /// <summary>
        /// Compress the file .gz
        /// </summary>
        /// <param name="fileToCompress"></param>
        public string compressFile(string fileToCompress)
        {
            FileStream ifs = null;
            FileStream ofs = null;
            GZipStream gstream = null;
            string outputFilePath = null;
            try
            {
                outputFilePath = fileToCompress + ".gz";
                ifs = new FileStream(fileToCompress, FileMode.Open);                
                ofs = new FileStream(outputFilePath, FileMode.Create);
                gstream = new GZipStream(ofs, CompressionMode.Compress);
                ifs.CopyTo(gstream);
                return outputFilePath;
            }
            finally
            {
                closeStream(gstream, fileToCompress);
                closeStream(ofs, outputFilePath);
                closeStream(ifs, outputFilePath);
            }
        }

        private void closeStream(Stream stream, string referenceId)
        {
            try
            {
                if (stream != null)
                    stream.Close();
            }
            catch (Exception ex)
            {
                Program.systemLog.Error("Error in closing stream : " + referenceId, ex);
            }
        }

        
        /// <summary>
        /// Decompresses the file *.gz.
        /// </summary>
        /// <param name="fileToDecompress">Full file path including the extension .gz</param>
        public string deCompressFile(string fileToDecompress)
        {
            string outputFileName = null;
            FileStream inputFileStream = null;
            FileStream outputFileStream = null;
            GZipStream gzipStream = null;
            try
            {
                FileInfo inputFileInfo = new FileInfo(fileToDecompress);
                outputFileName = fileToDecompress.Remove(fileToDecompress.Length - inputFileInfo.Extension.Length);
                if (File.Exists(outputFileName))
                {
                    File.Delete(outputFileName);
                }

                inputFileStream = inputFileInfo.OpenRead();
                outputFileStream = new FileStream(outputFileName, FileMode.Create);
                gzipStream = new GZipStream(inputFileStream, CompressionMode.Decompress);                
                gzipStream.CopyTo(outputFileStream);
                return outputFileName;
            }
            finally
            {
                closeStream(gzipStream, outputFileName);
                closeStream(outputFileStream, outputFileName);
                closeStream(inputFileStream, fileToDecompress);
            }
        }
    }
}
