﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace MoveSpace
{
    class ProcessStartInfoBuilder
    {
        private string processArguments;
        private string workingDirectory;

        public static ProcessStartInfoBuilder builder()
        {
            return new ProcessStartInfoBuilder();
        }
        public ProcessStartInfo build(string engineCommand)
        {
            ProcessStartInfo psi = new ProcessStartInfo(engineCommand);
            psi.WindowStyle = ProcessWindowStyle.Hidden;
            psi.RedirectStandardOutput = true;
            psi.CreateNoWindow = true;
            psi.UseShellExecute = false;
            psi.RedirectStandardInput = false;
            psi.RedirectStandardError = true;
            if (this.workingDirectory != null)
            {
                psi.WorkingDirectory = this.workingDirectory;
            }
            if (this.processArguments != null)
            {
                psi.Arguments = processArguments;
            }
            return psi;
        }

        public ProcessStartInfo buildDefault(string engineCommand)
        {
            ProcessStartInfo psi = new ProcessStartInfo(engineCommand);
            psi.WindowStyle = ProcessWindowStyle.Hidden;
            psi.RedirectStandardOutput = true;
            psi.CreateNoWindow = true;
            psi.UseShellExecute = false;
            psi.RedirectStandardInput = false;
            psi.RedirectStandardError = true;
            return psi;
        }


        public ProcessStartInfoBuilder addProcessArguments(string args)
        {
            this.processArguments = args;
            return this;
        }

        public ProcessStartInfoBuilder setWorkingDirectory(string pwd)
        {
            this.workingDirectory = pwd;
            return this;
        }


    }
}
