﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MoveSpace
{
    public class Column
    {
        public string LogicalName;
        public string PhysicalName;
        public string Type;
        public int Width;
    }
}
