﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Acorn;
using System.Data.Odbc;
using Performance_Optimizer_Administration;
using System.IO;
using System.Diagnostics;
using System.Data;
using System.Xml.Serialization;
using System.Xml;
using System.Web.Security;
using System.Collections.Specialized;
using log4net;
using log4net.Config;
using System.Reflection;
using MoveSpace.Utils;
using Acorn.Utils;
using System.Web.Configuration;
using System.Configuration;

namespace MoveSpace
{
    class Program
    {
        public static readonly ILog systemLog = LogManager.GetLogger("SystemLog");
        private static bool TEST = false;
        static string connectStringWithPwd = null;
        // XXX should really pull these from Acorn.Util and also use them in copyspace
        // XXX Variables*, LogicalExpressions* are obsoleted, should remove in 5.4
 
        static void Main(string[] args)
        {            
            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler((sender, e) => {
                    if (e.IsTerminating) {
                        object o = e.ExceptionObject;
                        Debug.WriteLine(o.ToString());
                    }
            });
            
            log4net.Appender.ConsoleAppender appender = new log4net.Appender.ConsoleAppender();
            appender.Layout = new log4net.Layout.PatternLayout("%d %-5p - %m%n");
            log4net.Config.BasicConfigurator.Configure(appender);

            if (args.Length == 0)
            {
                Global.systemLog.Info("Usage:");
                Global.systemLog.Info("movespace get server=[server] database=[database] uid=[userid] pwd=[password] spaceid=[spaceid] owner=[owner username] spaceconfig=[spaces.config directory] [dir=[targetdirectory] [databasedir=[databasedir] driver=[drivername]]");
                Global.systemLog.Info("movespace put [overwrite] spaceid=[optional, use original if blank] server=[server] database=[database] uid=[userid] pwd=[password] dir=[sourcedirectory] spaceconfig=[spaces.config directory] owner=[owner username] contentdir=[contentdirectory] [noindices]  [driver=[drivername]]");
                Global.systemLog.Info("movespace migrate spaceid=[optional, use original if blank] server=[server] database=[database] uid=[userid] pwd=[password] dir=[sourcedirectory] spaceconfig=[spaces.config directory] owner=[owner username] contentdir=[contentdirectory] [noindices]  [driver=[drivername]]");
                Acorn.Util.logVersionInformation();
                return;
            }
            string server = null;
            string database = null;
            string username = null;
            string password = null;
            string schema = "dbo";
            string spaceID = null;
            string targetDir = null;
            string sconfig = null;
            string owner = null;
            string contentdir = null;
            bool get = false;
            bool overwrite = false;
            bool noindices = false;
            string databaseDir = null;
            string driver = null;
            foreach (string s in args)
            {
                if (s.StartsWith("server="))
                    server = s.Substring(7);
                else if (s.StartsWith("database="))
                    database = s.Substring(9);
                else if (s.StartsWith("uid="))
                    username = s.Substring(4);
                else if (s.StartsWith("pwd="))
                    password = s.Substring(4);
                else if (s.StartsWith("spaceid="))
                    spaceID = s.Substring(8);
                else if (s.StartsWith("dir="))
                    targetDir = s.Substring(4);
                else if (s == "get")
                    get = true;
                else if (s == "put")
                    get = false;
                else if (s.StartsWith("spaceconfig="))
                    sconfig = s.Substring(12);
                else if (s.StartsWith("owner="))
                    owner = s.Substring(6);
                else if (s.StartsWith("contentdir="))
                    contentdir = s.Substring(11);
                else if (s.StartsWith("noindices"))
                    noindices = true;
                else if (s.StartsWith("overwrite"))
                    overwrite = true;
                else if (s.StartsWith("databasedir="))
                    databaseDir = s.Substring(12);
                else if (s.StartsWith("driver="))
                    driver = s.Substring(7);
            }
            if (server == null)
            {
                Global.systemLog.Error("No server specified");
                return;
            }
            if (database == null)
            {
                Global.systemLog.Error("No database specified");
                return;
            }
            if (username == null)
            {
                Global.systemLog.Error("No username specified");
                return;
            }
            if (password == null)
            {
                Global.systemLog.Error("No password specified");
                return;
            }
            if (owner == null)
            {
                Global.systemLog.Error("No owner specified");
                return;
            }
            if (sconfig == null)
            {
                Global.systemLog.Error("No SpaceConfig specified");
                return;
            }
            string connectString = (driver!=null ? "Driver={"+driver+"}" : "Driver={SQL Server Native Client 10.0}") +";Server=" + server + ";Database=" + database + ";Uid=" + username + ";Pwd=" + password;
            connectStringWithPwd = connectString;
            if (targetDir == null)
            {
                Global.systemLog.Error("No directory specified");
                return;
            }
            if (!Directory.Exists(targetDir))
                Directory.CreateDirectory(targetDir);            

            Acorn.DBConnection.QueryConnection conn;
            try
            {
                conn = new Acorn.DBConnection.QueryConnection(connectString);
                conn.Open();
            }
            catch (OdbcException ex)
            {
                Global.systemLog.Error("Unable to open connection to database: " + connectString, ex);
                return;
            }
            if (get)
            {
                getSpace(spaceID, conn, schema, targetDir, databaseDir, connectString, owner, sconfig);
            }
            else
            {
                putSpace(spaceID, connectString, conn, schema, spaceID, targetDir, contentdir, sconfig, owner, noindices, overwrite);
            }
            conn.Close();
        }

        /**
         * Set all ACORN configurations from web.config
         * */
        private static void initAcornConfigurations(string configLocation)
        {
            Global.systemLog.Debug("Reading Configuration from  " + configLocation + "\\web.config");
            System.Configuration.ExeConfigurationFileMap fmap = new ExeConfigurationFileMap();
            fmap.ExeConfigFilename = configLocation + "\\web.config";

            Configuration config = ConfigurationManager.OpenMappedExeConfiguration(fmap, ConfigurationUserLevel.None);
            AppSettingsSection settings = (AppSettingsSection)config.GetSection("appSettings");
            foreach (String key in settings.Settings.AllKeys)
            {
                Global.systemLog.Debug(key + " = " + settings.Settings[key].Value);
                System.Web.Configuration.WebConfigurationManager.AppSettings[key] = settings.Settings[key].Value;
            }
        }

        private static void getSpace(string spaceID, Acorn.DBConnection.QueryConnection conn, string schema, string targetDir, string databaseDir, string connectorString, string owner, string sconfig)
        {
            initAcornConfigurations(sconfig);
            Space scratchSpace = null;

            if (spaceID == null)
            {
                Global.systemLog.Error("Space ID not specified");
                return;
            }
            Space sp = Database.getSpace(conn, schema, new Guid(spaceID));
            if (sp == null)
            {
                Global.systemLog.Error("Space ID does not exist in the source database");
                return;
            }
            Acorn.DBConnection.QueryConnection dataconn = Util.getDataConnection(sp);
            Acorn.DBConnection.QueryConnection scratchSpaceConnection = null;
           
            if (databaseDir == null && dataconn.isDBTypeMySQL())
            {
                Global.systemLog.Error("DatabaseDir is mandatory for IB");
                return;
            }

 
     
            // get the server, username, and password
            string server = Util.getDataConnectionServer(sp);
            string username = sp.AdminUser;
            string password = sp.AdminPwd;
            bool newRep;
            MainAdminForm maf = Acorn.Util.loadRepository(sp, out newRep);
            if (newRep)
            {
                Global.systemLog.Error("No repository for the space");
                return;
            }
            Acorn.Util.mainSchema = "dbo";
            Acorn.Util.MOVE_LOCAL_PROTOCOL = "http://";
            Acorn.Utils.ConfigUtils.RUNNING_MODE = Acorn.Utils.ConfigUtils.MOVE_MODE;
            Acorn.Utils.ConfigUtils.MOVE_CONNECT_STRING = connectorString;

            SpaceSchema ss = new SpaceSchema();
            ss.sp = sp;
            ss.Tables = new List<TableSchema>();
            List<object> olist = new List<object>();
            ss.originalDatabaseDriver = sp.DatabaseDriver;
           
            
           
            try
            {
                HashSet<string> uniquePhysicalTables = new HashSet<string>();
                foreach (MeasureTable mt in maf.measureTablesList)
                {
                    if (!mt.AutoGenerated)
                        continue;
                    if (mt.InheritTable != null && mt.InheritTable.Length > 0)
                        continue;
                    if (mt.TableSource.Schema != null && mt.TableSource.Schema.Length > 0 && mt.TableSource.Schema != sp.Schema)
                        continue;
                    if (!uniquePhysicalTables.Contains(mt.PhysicalName))
                    {
                        olist.Add(mt);
                        uniquePhysicalTables.Add(mt.PhysicalName);
                    }

                }
                foreach (DimensionTable dt in maf.dimensionTablesList)
                {
                    if (!dt.AutoGenerated)
                        continue;
                    if (dt.InheritTable != null && dt.InheritTable.Length > 0)
                        continue;
                    if (dt.TableSource.Schema != null && dt.TableSource.Schema.Length > 0 && dt.TableSource.Schema != sp.Schema)
                        continue;
                    if (!uniquePhysicalTables.Contains(dt.PhysicalName))
                    {
                        olist.Add(dt);
                        uniquePhysicalTables.Add(dt.PhysicalName);
                    }
                }
                if (Database.tableExists(dataconn, sp.Schema, "TXN_SNAPSHOT_POLICY"))
                {
                    olist.Add("TXN_SNAPSHOT_POLICY");
                }
                if (Database.tableExists(dataconn, sp.Schema, "TXN_COMMAND_HISTORY"))
                {
                    olist.Add("TXN_COMMAND_HISTORY");
                }
                if (Database.tableExists(dataconn, sp.Schema, "TXN_OBJECT_HISTORY"))
                {
                    olist.Add("TXN_OBJECT_HISTORY");
                }
                if (Database.tableExists(dataconn, sp.Schema, "TXN_SCRIPT_LOG"))
                {
                    olist.Add("TXN_SCRIPT_LOG");
                }
                if (Database.tableExists(dataconn, sp.Schema, "TXN_SNAPSHOT_POLICY"))
                {
                    olist.Add("TXN_SNAPSHOT_POLICY");
                }

                scratchSpace = createScratchSpace(conn, schema, connectorString, sp, owner, dataconn);
                if (scratchSpace == null)
                {
                    Global.systemLog.Error("MoveSpace was unable to create a Scratch Space for extraction");
                    return;
                }
                scratchSpaceConnection = Util.getDataConnection(scratchSpace);
            

                foreach (object o in olist)
                {
                    string lname = null;
                    string tname = null;
                    TableSchema ts = new TableSchema();
                    MeasureTable mt = null;
                    DimensionTable dt = null;
                    if (typeof(MeasureTable).Equals(o.GetType()))
                    {
                        tname = ((MeasureTable)o).TableSource.Tables[0].PhysicalName;
                        lname = ((MeasureTable)o).TableName;
                        mt = (MeasureTable)o;
                        ts.TableType = TableSchema.TYPE_MEASURE;
                    }
                    else if (typeof(DimensionTable).Equals(o.GetType()))
                    {
                        tname = ((DimensionTable)o).TableSource.Tables[0].PhysicalName;
                        lname = ((DimensionTable)o).TableName;
                        dt = (DimensionTable)o;
                        ts.TableType = TableSchema.TYPE_DIMENSION;
                    }
                    else if (typeof(string).Equals(o.GetType()))
                    {
                        tname = (string)o;
                        ts.TableType = TableSchema.TYPE_OTHER;
                    }
                    Global.systemLog.Info("Getting schema for table: " + tname + " : " + scratchSpace.Schema);
                    DataTable dbcols = (ts.TableType == TableSchema.TYPE_OTHER && scratchSpaceConnection.isDBTypeMySQL()) ? scratchSpaceConnection.GetSchema("Columns", new string[] { scratchSpace.Schema, null, tname }) : scratchSpaceConnection.GetSchema("Columns", new string[] { null, scratchSpace.Schema, tname });
                    DataTable originalSpaceCols = (ts.TableType == TableSchema.TYPE_OTHER && dataconn.isDBTypeMySQL()) ? dataconn.GetSchema("Columns", new string[] { sp.Schema, null, tname }) : dataconn.GetSchema("Columns", new string[] { null, sp.Schema, tname });
                    
                    List<Column> cols = new List<Column>();
                    foreach (DataRow dr in dbcols.Rows)
                    {
                        Column c = new Column();
                        c.PhysicalName = (string)dr["COLUMN_NAME"];
                        if (mt != null)
                        {
                            DataRowView[] drvlist = maf.measureColumnMappingsTablesView.FindRows(mt.TableName);
                            foreach (DataRowView drv in drvlist)
                            {
                                string pname = (string)drv.Row["PhysicalName"];
                                int index = pname.IndexOf('.');
                                if (index >= 0)
                                    pname = pname.Substring(index + 1);
                                if (pname == c.PhysicalName)
                                {
                                    c.LogicalName = (string)drv.Row["ColumnName"];
                                    break;
                                }
                            }
                        }
                        else if (dt != null)
                        {
                            DataRowView[] drvlist = maf.dimensionColumnMappingsTablesView.FindRows(dt.TableName);
                            foreach (DataRowView drv in drvlist)
                            {
                                string pname = (string)drv.Row["PhysicalName"];
                                int index = pname.IndexOf('.');
                                if (index >= 0)
                                    pname = pname.Substring(index + 1);
                                if (pname == c.PhysicalName)
                                {
                                    c.LogicalName = (string)drv.Row["ColumnName"];
                                    break;
                                }
                            }
                        }
                        c.Type = (string)dr["TYPE_NAME"];
                        c.Width = (int)dr["COLUMN_SIZE"];
                        cols.Add(c);
                    }
                    ts.LogicalName = lname;
                    ts.PhysicalName = tname;
                    ts.FileName = tname;
                    ts.Columns = cols;
                    ss.Tables.Add(ts);

                    List<string> originalTableColumns = new List<string>();
                    foreach (DataRow dr in originalSpaceCols.Rows)
                    {
                        originalTableColumns.Add((string)dr["COLUMN_NAME"]);
                    }


                    dataconn = Util.getDataConnection(sp);
                    string args = Util.getEngineCommandArgs(sp, 0);
                    string enginecmd = Acorn.Util.getEngineCommandFileName(sp);
                    StringBuilder sb = new StringBuilder();
                    bool isFirst = true;
                    foreach (Column col in cols)
                    {
                        if (originalTableColumns.Contains(col.PhysicalName))
                        {
                            if (!isFirst)
                                sb.Append(",");
                            sb.Append(col.PhysicalName);
                            isFirst = false;
                        }
                    }
                    if ((sb.ToString() == null || sb.ToString() == "") && scratchSpaceConnection.isDBTypeSQLServer())
                    {
                        Global.systemLog.Error("Error: MoveSpace was unable to identify columns to be extracted from " + tname);
                        return;
                    }
                    ProcessStartInfo psi = null;
                    ProcessStartInfoBuilder psInfoBuilder = ProcessStartInfoBuilder.builder();
                    if (dataconn.isDBTypeSQLServer())
                    {
                        string processToFork = "bcp";
                        string arguments = "\"SELECT " + sb.ToString() + " FROM " + dataconn.Database + "." + sp.Schema + "." + tname + "\" queryout \"" + targetDir + "\\" +
                            tname + ".txt\" " + "\"-t|\" -r\\n" + " -S" + server + " -U" + username + " -P" + password + " -w";
                        psi = psInfoBuilder.addProcessArguments(arguments).setWorkingDirectory(targetDir).build(processToFork);
                    }
                    else if (dataconn.isDBTypeMySQL())
                    {

                        string cmdFilename = sp.Directory + "\\" + "unload.cmd";
                        StringBuilder commands = new StringBuilder();
                        commands.Append("repository " + sp.Directory + "\\repository.xml\n");
                        //unload by keeping the source file names same as table names with true flag
                        if (ts.TableType == TableSchema.TYPE_DIMENSION || ts.TableType == TableSchema.TYPE_MEASURE)
                        {
                            commands.Append("unloadusingquery " + scratchSpace.Schema + " " +sp.Schema + " " + tname + " " + databaseDir + "\n");
                        }
                        else
                        {
                            commands.Append("unload " + sp.Schema + " " + tname + " " + databaseDir + " " + "true" + "\n");
                        }
                        File.WriteAllText(cmdFilename, commands.ToString());
                        string arguments = sp.Directory + "\\unload.cmd " + sp.Directory;
                        string workingDirectory = (new FileInfo(enginecmd)).Directory.FullName;
                        psi = psInfoBuilder.addProcessArguments(arguments).setWorkingDirectory(workingDirectory).build(enginecmd);
                    }

                    Global.systemLog.Info("Executing command: " + psi.FileName + " " + psi.Arguments);
                    Process p = Process.Start(psi);
                    string output = p.StandardOutput.ReadToEnd();
                    p.WaitForExit();
                    if (p.ExitCode != 0)
                    {
                        Global.systemLog.Warn("The command failed. This might be okay if there is a table in the repository that is not in the database.");
                    }
                    else
                    {
                        Global.systemLog.Info("Compressing table " + (ts.LogicalName != null && ts.LogicalName.Length > 0 ? ts.LogicalName : ts.PhysicalName));
                        Util.compressFile(targetDir, tname);
                    }

                }


                Global.systemLog.Info("Getting index information");
                ss.IndexList = Database.getIndices(dataconn, sp.Schema);
                Global.systemLog.Info("Getting publish history");
                ss.Publishes = Database.getPublishDetail(conn, schema, sp.ID);
                dataconn.Close();

                Util.copyFilesAndDirectories(sp.Directory, targetDir);

                Global.systemLog.Info("Saving users");
                List<SpaceMembership> members = Database.getUsers(conn, schema, sp);
                ss.Users = new List<Acorn.User>();
                foreach (SpaceMembership sm in members)
                {
                    if (sm.User != null)
                    {
                        Acorn.User user = Database.getUser(conn, schema, sm.User.Username);
                        ss.Users.Add(user);
                    }
                }

                Global.systemLog.Info("Creating space definition file");
                StreamWriter writer = new StreamWriter(Path.Combine(targetDir, "spacedefinition.xml"));
                XmlSerializer serializer = new XmlSerializer(typeof(SpaceSchema));
                serializer.Serialize(writer, ss);
                writer.Close();

                Global.systemLog.Info("Saving repository");
                maf.saveRepository(Path.Combine(targetDir, "repository.xml"));
                // Copy historical repositories
                Global.systemLog.Info("Saving saved repositories");
                DirectoryInfo di = new DirectoryInfo(sp.Directory);
                foreach (FileInfo fi in di.GetFiles())
                {
                    if (fi.Name.EndsWith(".save"))
                    {
                        File.Copy(fi.FullName, Path.Combine(targetDir, fi.Name), true);
                    }
                }


                try
                {
                    Global.systemLog.Info("Extracting Users and Groups information");
                    string targetFileName = Path.Combine(targetDir, Acorn.Utils.UserAndGroupUtils.USER_AND_GROUP_FILENAME);
                    sp.SpaceGroups = Database.getSpaceGroups(conn, schema, sp);
                    string writeTime = DateTime.Now.ToString();
                    DateTime st = Database.getMappingModifiedDate(conn, schema, sp);
                    if (st != null && st != DateTime.MinValue)
                    {
                        writeTime = st.ToString();
                    }

                    // populate the Global ACLs
                    ACLDetails.ACLDictionary = Database.getACLs(conn, schema);
                    Acorn.Utils.UserAndGroupTransient uAg = Acorn.Utils.UserAndGroupUtils.buildUserAndGroupTransient(sp, writeTime);
                    Acorn.Utils.UserAndGroupUtils.writeUserGroupMappingFromDbToXMLFile(uAg, targetFileName);
                }
                catch (Exception ex)
                {
                    Global.systemLog.Error("Error while extracting user/group/acl info", ex);
                }

                PackageImport[] packagesToImport = maf.Imports;
                if (packagesToImport != null && packagesToImport.Count() > 0)
                {
                    Global.systemLog.Info("Detected DBA References in the Space " + sp.Name);
                    Global.systemLog.Info("Please Move(get and then put) the Dependent Parent Spaces before " + sp.Name);
                    foreach (PackageImport import in packagesToImport)
                    {
                        Space parentSp = Database.getSpace(conn, schema, import.SpaceID);
                        Global.systemLog.Info("Package " + import.PackageName + "(" + import.PackageID + ") " + "depends on " + parentSp.Name + "(" + parentSp.ID + ")");
                    }
                }
            }
            finally
            {
                if (scratchSpace != null)
                {
                    Database.deleteSpace(conn, schema, scratchSpace, null);
                }
                if (scratchSpaceConnection != null)
                {
                    scratchSpaceConnection.Close();
                }
                if (scratchSpaceConnection != null)
                {
                    dataconn.Close();
                }
            }
            
        }

        private static void putSpace(string spaceID, string connectString, Acorn.DBConnection.QueryConnection conn, string schema, string spaceid, string targetDir,
            string contentdir, string sconfig, string owner, bool noindices, bool overwrite)
        {
            Acorn.Util.mainSchema = "dbo";
            Acorn.Util.MOVE_LOCAL_PROTOCOL = "http://";
            Acorn.Utils.ConfigUtils.RUNNING_MODE = Acorn.Utils.ConfigUtils.MOVE_MODE;
            Acorn.Utils.ConfigUtils.MOVE_CONNECT_STRING = connectString;
            MainAdminForm maf = new MainAdminForm();
            maf.setHeadless(true);
            maf.setLogger(systemLog);

            Global.systemLog.Info("Retrieving space definition");
            SpaceSchema ss = getSpaceSchema(targetDir);
            if (ss == null)
            {
                return;
            }

            Global.systemLog.Info("Checking database for space owner");
            Acorn.User u = Database.getUser(conn, schema, owner);
            if (u == null)
            {
                Global.systemLog.Error("Unable to find user: " + owner);
                return;
            }
            
            if (contentdir == null)
            {
                Global.systemLog.Error("No content directory specified");
                return;
            }
            if (!Directory.Exists(contentdir))
            {
                Global.systemLog.Error("Content directory does not exist: " + contentdir);
                return;
            }
            Global.systemLog.Info("Checking current space in database");
            Guid spaceguid = ss.sp.ID;
            if (spaceid != null)
                spaceguid = new Guid(spaceid);
            Space sp = Database.getSpace(conn, schema, spaceguid);
            string sourceDriver = ss.originalDatabaseDriver;
            
            bool newSpace = sp == null;
            if (!newSpace && !overwrite)
            {
                Global.systemLog.Warn("Space " + sp.Name + " (" + sp.ID + ") exists, use overwrite=true");
                return;
            }
            if (newSpace)
            {
                Global.systemLog.Info("Creating space");
                sp = ss.sp;
            }
            else
            {
                Global.systemLog.Info("Updating space");
                sp.LoadNumber = ss.sp.LoadNumber;
                sp.Folded = ss.sp.Folded;
                sp.Demo = ss.sp.Demo;
                sp.Locked = ss.sp.Locked;
                sp.Name = ss.sp.Name;
                sp.Settings = ss.sp.Settings;
                sp.SourceTemplateName = ss.sp.SourceTemplateName;
                sp.SSO = ss.sp.SSO;
                sp.SSOPassword = ss.sp.SSOPassword;
                sp.TestMode = ss.sp.TestMode;
                sp.Type = ss.sp.Type;
                sp.QueryLanguageVersion = ss.sp.QueryLanguageVersion;
            }
            if (sp.MaxQueryRows == 0)
                sp.MaxQueryRows = Acorn.Database.MAX_QUERY_ROWS;
            if (sp.MaxQueryTimeout == 0)
                sp.MaxQueryTimeout = Acorn.Database.MAX_QUERY_TIMEOUT;

            SpaceConfig sc = null;
            if (!overwrite)
            {

                sc = Database.getSpaceConfig(conn, schema, NewSpaceService.getSpaceType(u, schema));

                if (sc == null)
                {
                    Global.systemLog.Error("No space configuration of type " + ss.sp.Type);
                    return;
                }

                sp.AdminUser = sc.AdminUser;
                sp.AdminPwd = sc.AdminPwd;
                sp.ConnectString = sc.DatabaseConnectString;
                sp.DatabaseDriver = sc.DatabaseDriver;
                sp.setDatabaseLoadDir(sc.DatabaseLoadDir);
                sp.DatabaseType = sc.DatabaseType;

            }
            sp.Directory = Path.Combine(contentdir, sp.ID.ToString());

            // Clear space first
            DirectoryInfo dirI = new DirectoryInfo(sp.Directory);
            if (dirI.Exists)
                Acorn.Util.deleteDir(dirI);
            if (!Directory.Exists(sp.Directory))
                Directory.CreateDirectory(sp.Directory);


            if (newSpace)
            {
                Database.createSpace(conn, schema, u, sp);
            }
            else
            {
                Database.updateSpace(conn, schema, sp);
            }
            Acorn.DBConnection.QueryConnection dataconn = Util.getDataConnection(sp);
            
            // get the server, username, and password
            string server = Util.getDataConnectionServer(sp);
            string username = sp.AdminUser;
            string password = sp.AdminPwd;
            if (!TEST)
                Util.copyFilesAndDirectories(targetDir, sp.Directory);

            
            if (File.Exists(Path.Combine(targetDir, "repository.xml")))
            {
                try
                {
                    maf.setRepositoryDirectory(targetDir);
                    maf.loadRepositoryFromFile(Path.Combine(targetDir, "repository.xml"));
                    maf.setupTree();
                    Acorn.Util.buildApplication(maf, schema, DateTime.Now, sp.LoadNumber, null, sp, null);
                    // clear the requires publish flag - too much confusion when it is not cleared
                    maf.RequiresPublish = false;
                
                    Acorn.Util.saveApplication(maf, sp, null, u);
                    //Rebuild the repository in case the driver is different
                    if (sourceDriver!=null && sourceDriver != ss.sp.DatabaseDriver)
                    {
                            Global.systemLog.Debug("Cross Database Migration Detected.Rebuilding Application for " + ss.sp.DatabaseType);
                            MainAdminForm mafTarget = new MainAdminForm();
                            mafTarget.setHeadless(true);
                            mafTarget.setLogger(systemLog);
                            mafTarget.setRepositoryDirectory(sp.Directory);
                            mafTarget.loadRepositoryFromFile(Path.Combine(sp.Directory, "repository.xml"));
                            mafTarget.setupTree();
                            Acorn.Util.buildStagingTables(mafTarget);
                            Acorn.Util.buildApplication(mafTarget, schema, DateTime.Now, sp.LoadNumber, null, sp, null);
                            Acorn.Util.saveApplication(mafTarget, sp, null, u);

                            //Read the Space definitions, after the rebuilding of the app, modify the spaceSchema details to reflect any changs in physical table or column names
                            ApplicationBuilder ab = new ApplicationBuilder(mafTarget);
                            Util.reMapMeasureTables(ss, mafTarget, ab.getMaxPhysicalColumnNameLength());
                            Util.reMapDimensionTables(ss, mafTarget, ab.getMaxPhysicalColumnNameLength(),sp);
                            //backup before overwrite
                            Util.FileCopyWithoutOverwriting(Path.Combine(targetDir, "spacedefinition.xml"), "spacedefinition.xml", targetDir);
                            StreamWriter writer = new StreamWriter(Path.Combine(targetDir, "spacedefinition.xml"));
                            XmlSerializer serializer = new XmlSerializer(typeof(SpaceSchema));
                            serializer.Serialize(writer, ss);
                            writer.Close();
                            ss = getSpaceSchema(targetDir);
                            maf = mafTarget;
                    
                    }
                }
                catch (Exception)
                {
                    Global.systemLog.Warn("Error Building Application.");
                    return;
                }
            }
            else
            {
                Global.systemLog.Error("Unable to generate and load new repository");
                return;
            }

            //Generate Schema and Create Physical Tables for all Discovery Sources            
            File.WriteAllText(sp.Directory + "\\load.cmd", "repository " + sp.Directory + "\\repository_dev.xml\rresetetlrun 1\rgenerateschema 1 notime");
            foreach (StagingTable st in maf.stagingTableMod.getAllStagingTables())
            {
                if (st.LiveAccess || st.InheritTables != null)
                {
                    continue;
                }
                if (st.DiscoveryTable)
                {
                    File.AppendAllText(sp.Directory + "\\load.cmd", "\rcreatediscoveryst " + st.Name);
                }
            }

            bool spaceHasScriptLog = false;
            foreach (TableSchema ts in ss.Tables)
            {
                if (ts.PhysicalName != null && ts.PhysicalName.Equals("TXN_SCRIPT_LOG"))
                {
                    spaceHasScriptLog = true;
                    File.AppendAllText(sp.Directory + "\\load.cmd", "\rcreatescriptlog TXN_SCRIPT_LOG");
                }
            }

            bool spacehasSnapShotPolicy = false;
            foreach (TableSchema ts in ss.Tables)
            {
                if (ts.PhysicalName != null && ts.PhysicalName.Equals("TXN_SNAPSHOT_POLICY"))
                {
                    SnapshotPolicyManager spol = new SnapshotPolicyManager(dataconn, Acorn.Util.mainSchema, sp, maf);
                    spol.setupPolicyTable(1);
                    spacehasSnapShotPolicy = true;
                }
            }

            string args = Util.getEngineCommandArgs(sp, 0);
            string enginecmd = Acorn.Util.getEngineCommandFileName(sp);
            string workingDirectory = (new FileInfo(enginecmd)).Directory.FullName;
            ProcessStartInfo psi = ProcessStartInfoBuilder.builder().addProcessArguments(args).setWorkingDirectory(workingDirectory).build(enginecmd);
            Process proc = Process.Start(psi);
            string output = proc.StandardOutput.ReadToEnd();
            proc.WaitForExit();
            if (proc.ExitCode != 0)
            {
                Global.systemLog.Warn("The command to generate schema failed.");
                return;
            }
            
            Util.truncateTable(sp, "TXN_OBJECT_HISTORY");
            Util.truncateTable(sp, "TXN_COMMAND_HISTORY");
            if (spaceHasScriptLog)
            {
                Util.truncateTable(sp, "TXN_SCRIPT_LOG");
            }
            if (spacehasSnapShotPolicy)
            {
                Util.truncateTable(sp, "TXN_SNAPSHOT_POLICY");
            }

            foreach (TableSchema ts in ss.Tables)
            {
                
              string fname = targetDir + "\\" + ts.FileName +".txt";
                bool decompressed = false;
                if (File.Exists(fname + ".gz"))
                {
                    Util.decompressFile(targetDir, ts.FileName + ".txt.gz");
                    decompressed = true;
                }

                Global.systemLog.Info("Bulk loading table " + (ts.LogicalName != null && ts.LogicalName.Length > 0 ? ts.LogicalName : ts.PhysicalName));
                if (Util.loadUsingSQLTables.Contains(ts.PhysicalName))
                {
                    BulkLoad.loadUsingSQL(sp, ts, dataconn, targetDir,fname);
                }
                else
                {
                    if (sp.DatabaseDriver == "com.microsoft.sqlserver.jdbc.SQLServerDriver")
                    {
                        BulkLoad.bulkLoadMSSQL(sp, ts, dataconn, targetDir, server, username, password);
                    }
                    else if (sp.DatabaseDriver == "com.mysql.jdbc.Driver")
                    {
                        BulkLoad.bulkLoadInfobright(maf, sp, ts, dataconn, targetDir, sourceDriver);
                    }
                    else if (sp.DatabaseDriver == "memdb.jdbcclient.DBDriver")
                    {
                        BulkLoad.bulkLoadMemDB(maf, sp, ts, dataconn, targetDir, ss.sp.DatabaseDriver);
                    }
                }
                if (decompressed)
                    File.Delete(fname);
            }

            //Update Composite keys in Fact taables only in case of IB and when we have a cross-database migration
            if (sp.DatabaseDriver == "com.mysql.jdbc.Driver" && sourceDriver!=sp.DatabaseDriver)
            {
                MainAdminForm mafTarget = new MainAdminForm();
                mafTarget.setHeadless(true);
                mafTarget.setLogger(systemLog);
                mafTarget.setRepositoryDirectory(sp.Directory);
                mafTarget.loadRepositoryFromFile(Path.Combine(sp.Directory, "repository.xml"));
                mafTarget.setupTree();

                List<string> updateCompositeKeyTables = new List<string>();
                foreach (MeasureTable mt in mafTarget.measureTablesList)
                {
                    if (mt.PhysicalName != null && !updateCompositeKeyTables.Contains(mt.PhysicalName))
                    {
                        updateCompositeKeyTables.Add(mt.PhysicalName);
                    }
                }
                foreach (DimensionTable dt in mafTarget.dimensionTablesList)
                {
                    if (dt.PhysicalName != null && !updateCompositeKeyTables.Contains(dt.PhysicalName))
                    {
                        updateCompositeKeyTables.Add(dt.PhysicalName);
                    }
                }
                //Processing Engine to handle if the tables contain composite key and how to update it.
                if (updateCompositeKeyTables.Count >= 1)
                {
                    File.WriteAllText(sp.Directory + "\\updatecompositekeys.cmd", "repository " + sp.Directory + "\\repository_dev.xml");
                    foreach (string s in updateCompositeKeyTables)
                    {
                        File.AppendAllText(sp.Directory + "\\updatecompositekeys.cmd", "\rupdatecompositekeys " + s);
                    }
                }

                args = sp.Directory + "\\updatecompositekeys.cmd" + " " + sp.Directory;
                psi = ProcessStartInfoBuilder.builder().addProcessArguments(args)
                    .setWorkingDirectory(workingDirectory)
                    .build(enginecmd);

                proc = Process.Start(psi);
                output = proc.StandardOutput.ReadToEnd();
                proc.WaitForExit();
                if (proc.ExitCode != 0)
                {
                    Global.systemLog.Warn("The command to updateCompositeKeys in Fact tables Failed.Please check processing engine logs for details.");
                    return;
                }
            }

            if (!noindices && sp.DatabaseDriver == "com.microsoft.sqlserver.jdbc.SQLServerDriver")
                foreach (DatabaseIndex di in ss.IndexList)
                {
                    foreach (TableSchema ts in ss.Tables)
                    {
                        if (ts.PhysicalName == di.TableName)
                        {
                            Global.systemLog.Info("Creating index on table " + di.TableName + " on columns " + string.Join(",", di.Columns));                            
                            Database.createIndex(conn, sp.Schema, di.TableName, di.Name, di.Columns, di.IncludedColumns);
                            break;
                        }
                    }
                }
            dataconn.Close();
            
            Global.systemLog.Info("Copying publishing history");
            Database.removePublishes(conn, schema, sp.ID, -1);
            foreach (Publish p in ss.Publishes)
            {
                Database.addPublish(conn, schema, sp.ID, p.LoadNumber, p.NumRows, p.DataSize, p.LoadDate, p.PublishDate, p.TableName, p.LoadGroup, p.StartPublishDate);
            }

            Global.systemLog.Info("Copying saved repositories");
            dirI = new DirectoryInfo(targetDir);
            foreach (FileInfo fi in dirI.GetFiles())
            {
                if (fi.Name.EndsWith(".save"))
                {
                    File.Copy(fi.FullName, Path.Combine(sp.Directory, fi.Name), true);
                }
            }             

            Global.systemLog.Info("Restoring users");
            Database.mainSchema = schema;
            List<SpaceMembership> members = Database.getUsers(conn, schema, sp);
            UserProvider up = new UserProvider();
            up.connection = conn;
            up.headless = true;
            string newUserProductIDs = "0,9";
            string[] productIds = newUserProductIDs.Split(',');
            foreach (Acorn.User user in ss.Users)
            {
                Acorn.User curUser = Database.getUser(conn, schema, user.Username);
                if (curUser == null)
                {
                    MembershipCreateStatus status = new MembershipCreateStatus();
                    up.CreateUser(user.Username, user.Password, user.Email, "asdf1234", "poiuy09876", true, user.ID, out status);
                    if (conn.State != ConnectionState.Open)
                        conn.Open();
                    Database.setupNewUser(conn, schema, user.Username,
                        user.FirstName, user.LastName, user.Email,
                        user.Title, user.Company,
                        user.Phone,
                        user.City, user.State,
                        user.Zip, user.Country, true,
                        user.ContactStatus, null,
                        null, null, null, new List<string>(productIds), DateTime.MinValue);
                }
                bool found = false;
                foreach (SpaceMembership sm in members)
                {
                    if (sm.User.Username == user.Username)
                    {
                        found = true;
                        break;
                    }
                }
                if (!found)
                {
                    if (conn.State != ConnectionState.Open)
                        conn.Open();
                    Database.addSpaceMembership(conn, schema, curUser == null ? user.ID : curUser.ID, sp.ID, false, false);
                }
            }

            // populate user and group info from the mapping file to the target db
            try
            {
                ACLDetails.ACLDictionary = Database.getACLs(conn, schema);
                string sourceUserGroupFileName = targetDir + "\\" + Acorn.Utils.UserAndGroupUtils.USER_AND_GROUP_FILENAME;
                Global.systemLog.Info("Populating Users and Groups information from " + sourceUserGroupFileName);
                if (!File.Exists(sourceUserGroupFileName))
                {
                    Global.systemLog.Warn("Unable to find file : " + sourceUserGroupFileName);
                    return;
                }
                string targetUserGroupFileName = sp.Directory + "\\" + Acorn.Utils.UserAndGroupUtils.USER_AND_GROUP_FILENAME;
                File.Copy(sourceUserGroupFileName, targetUserGroupFileName, true);
                populateUserAndGroupInfoFromFile(conn, schema, targetUserGroupFileName, sp);

                // Get owner group and make sure it is updated with the right information
                List<SpaceGroup> spgList = Database.getSpaceGroups(conn, schema, sp);
                if (spgList != null && spgList.Count > 0)
                {
                    SpaceGroup ownerGroup = null;
                    foreach (SpaceGroup spg in spgList)
                    {
                        if (spg.InternalGroup && spg.Name == Acorn.Util.OWNER_GROUP_NAME)
                        {
                            ownerGroup = spg;
                            break;
                        }
                    }

                    if (ownerGroup != null)
                    {
                        List<string> groupUserNames = new List<string>();
                        groupUserNames.Add(u.Username);
                        Database.updateUsersForSpaceGroup(conn, schema, sp, ownerGroup, groupUserNames);
                    }
                }
            }
            catch (Exception ex)
            {
                Global.systemLog.Error("Error while populating db with user/group/acl info ", ex);
            }

            PackageImport[] packagesToImport = maf.Imports;
            if (packagesToImport != null && packagesToImport.Count() > 0)
            {
                foreach (PackageImport import in packagesToImport)
                {
                    Space parentSp = Database.getSpace(conn, schema, import.SpaceID);
                    if (parentSp == null)
                    {
                        Global.systemLog.Error("Unable to Find Parent Space with Space ID " + import.SpaceID + " for DBA. Please put the Parent Space Before Child Space for DBA to complete");
                        return;
                    }
                    Database.addUsageToPackage(conn, schema, u, parentSp.ID, import.PackageID, sp.ID);
                    Global.systemLog.Info("Packages Updated from " + parentSp.Name);
                }
            }
        }

       
        private static SpaceSchema getSpaceSchema(string targetDir)
        {
            StreamReader reader;
            try
            {
                reader = new StreamReader(Path.Combine(targetDir, "spacedefinition.xml"));
            }
            catch (FileNotFoundException ex)
            {
                Global.systemLog.Error("No space definition file found", ex);
                return null;
            }
            XmlReader xreader = new XmlTextReader(reader);
            XmlSerializer serializer = new XmlSerializer(typeof(SpaceSchema));
            SpaceSchema ss = null;
            try
            {
                ss = (SpaceSchema)serializer.Deserialize(xreader);
            }
            catch (System.InvalidOperationException ex)
            {
                Global.systemLog.Error(ex);
                reader.Close();
                return null;
            }
            reader.Close();
            return ss;
        }

        private static void populateUserAndGroupInfoFromFile(Acorn.DBConnection.QueryConnection conn, string schema, string fileName, Space sp)
        {

            Performance_Optimizer_Administration.MainAdminForm maf = new Performance_Optimizer_Administration.MainAdminForm();
            Acorn.Utils.UserAndGroupTransient uAg = Acorn.Utils.UserAndGroupUtils.readFromUserGroupMappingFile(sp, fileName);
            if (uAg == null)
            {
                Global.systemLog.Warn("Nothing to populate");
                return;
            }

            maf.usersAndGroups.usersList = new List<Performance_Optimizer_Administration.User>(uAg.Users);
            maf.usersAndGroups.accessList = new List<Performance_Optimizer_Administration.ACLItem>(uAg.ACL);
            maf.usersAndGroups.groupsList = new List<Performance_Optimizer_Administration.Group>(uAg.Groups);

            List<SpaceGroup> existingSpaceGroups = Database.getSpaceGroups(conn, schema, sp);
            Database.removeAllSpaceGroups(conn, schema, sp, existingSpaceGroups);            
            
            List<Performance_Optimizer_Administration.Group> groupList = maf.usersAndGroups.groupsList;
            List<Performance_Optimizer_Administration.ACLItem> groupaccessList = new List<Performance_Optimizer_Administration.ACLItem>(maf.usersAndGroups.accessList);

            Acorn.DBConnection.QueryCommand cmd = null;
            OdbcTransaction transaction = null;
            try
            {
                cmd = conn.CreateCommand();
                transaction = conn.BeginTransaction();
                cmd.Transaction = transaction;
                Dictionary<string, string[][]> mappingResults = new Dictionary<string, string[][]>();

                foreach (Performance_Optimizer_Administration.Group group in groupList)
                {
                    // Add group to the space

                    List<string> addedGroupACLs  = new List<string>();
                    List<string> addedGroupUsers = new List<string>();

                    cmd.CommandText = "";
                    cmd.Parameters.Clear();

                    Guid groupID = Guid.NewGuid();

                    cmd.CommandText = Database.getAddGroupToSpaceQuery(conn, schema);
                    cmd.Parameters.Add("@SPACE_ID", OdbcType.UniqueIdentifier).Value = sp.ID;
                    cmd.Parameters.Add("@GROUP_ID", OdbcType.UniqueIdentifier).Value = groupID;
                    cmd.Parameters.Add("@GroupName", OdbcType.VarChar).Value = group.Name;
                    cmd.Parameters.Add("@InternalGroup", OdbcType.Bit).Value = group.InternalGroup;

                    int groupAdded = cmd.ExecuteNonQuery();
                    if (!(groupAdded > 0))
                    {
                        Global.systemLog.Warn("Unable to create group " + group.Name);
                        continue;
                    }

                    // Add ACLs to the group
                    cmd.CommandText = "";
                    cmd.Parameters.Clear();
                    foreach (Performance_Optimizer_Administration.ACLItem groupACL in groupaccessList)
                    {
                        cmd.CommandText = "";
                        cmd.Parameters.Clear();
                        if (groupACL.Group != group.Name)
                        {
                            continue;
                        }

                        if (!groupACL.Access)
                        {
                            // We are skipping any false flags since we are only storing true tag in the database.
                            //Global.systemLog.Debug("ACLItem is " + groupACL.Access + " for acl " + groupACL.Tag + " and group " + group.Name);
                            continue;
                        }

                        int aclID = ACLDetails.getACLID(groupACL.Tag);
                        //Global.systemLog.Debug("ACL tag is " + groupACL.Tag);
                        //Global.systemLog.Debug("ACL id is " + aclID);

                        if (aclID <= 0)
                        {
                            Global.systemLog.Warn("Unable to get the acl id for tag " + groupACL.Tag + " Skipping the population of " +
                                group.Name + " for space " + sp.ID + " : " + sp.Name);
                            continue;
                        }

                        cmd.CommandText = Database.getAddACLToGroupQuery(conn, schema);
                        cmd.Parameters.Add("@GROUP_ID", OdbcType.UniqueIdentifier).Value = groupID;
                        cmd.Parameters.Add("@ACL_ID", OdbcType.Int).Value = aclID;
                        cmd.ExecuteNonQuery();
                        addedGroupACLs.Add(groupACL.Tag);
                    }

                    cmd.CommandText = "";
                    cmd.Parameters.Clear();

                    // Add user to the group
                    if (group.Usernames != null && group.Usernames.Length > 0)
                    {
                        List<string> groupUserNames = new List<string>(group.Usernames);
                        foreach (string userName in groupUserNames)
                        {
                            cmd.CommandText = "";
                            cmd.Parameters.Clear();

                            Acorn.User groupUser = null;

                            try
                            {
                                // Create another connection, retrieve the user and close it
                                Acorn.DBConnection.QueryConnection conn2 = ConnectionPool.getConnection(connectStringWithPwd);
                                groupUser = Database.getUser(conn2, schema, userName, Guid.Empty, null, true, false);
                                ConnectionPool.releaseConnection(conn2);
                            }
                            catch (Exception ex2)
                            {
                                Global.systemLog.Error("Skipping. Error while finding user info for " + userName, ex2);
                                continue;
                            }
                            if (groupUser == null)
                            {
                                Global.systemLog.Debug("Userid not found for user = " + userName + " . Skipping group-user mapping for  group = " + group.Name + " for space " + sp.ID);
                                continue;
                            }

                            cmd.CommandText = Database.getAddUserToGroupQuery(conn, schema);
                            cmd.Parameters.Add("@GROUP_ID", OdbcType.UniqueIdentifier).Value = groupID;
                            cmd.Parameters.Add("@USER_ID", OdbcType.UniqueIdentifier).Value = groupUser.ID;
                            cmd.ExecuteNonQuery();
                            addedGroupUsers.Add(userName);
                        }
                    }
                    
                    string[][] addedGroupInfo = new string[2][];
                    addedGroupInfo[0] = addedGroupUsers.ToArray();
                    addedGroupInfo[1] = addedGroupACLs.ToArray();

                    mappingResults.Add(group.Name, addedGroupInfo);
                }

                transaction.Commit();
                cmd.Dispose();                
                foreach(KeyValuePair<string, string[][]> kvPair in mappingResults)
                {
                    string gName = kvPair.Key;                    
                    string[][] results = kvPair.Value;
                    string displayUnames = "";
                    string displayAcls = "";
                    foreach(string displayUname in results[0])
                    {
                        displayUnames = displayUnames + displayUname + " ,";
                    }

                    foreach(string displayAcl in results[1])
                    {
                        displayAcls = displayAcls + displayAcl + " ,";
                    }

                    if (displayUnames.Length > 0)
                    {
                        displayUnames = displayUnames.Substring(0, displayUnames.Length - 1);
                    }
                    if (displayAcls.Length > 0)
                    {
                        displayAcls = displayAcls.Substring(0, displayAcls.Length - 1);
                    }
                    Global.systemLog.Debug("Group Mapping added : groupName " + gName + " \n usersAdded = " + displayUnames + " \n ACLs added = " + displayAcls);
                }
            }
            catch (Exception ex)
            {
                // Exception raised while adding space-group-user mapping. Rollback the entire transaction and send email to 
                Global.systemLog.Error("Error while creating Space-Group-UserBack mapping. Rolling back the transaction ", ex);

                try
                {
                    if (transaction != null)
                    {
                        transaction.Rollback();
                    }
                }
                catch (Exception ex2)
                {
                    Global.systemLog.Error("Error while rolling transaction for user-group-mapping", ex2);
                }

                try
                {
                    if (cmd != null)
                    {
                        cmd.Dispose();
                    }
                }
                catch (Exception)
                {
                    // Unable to close OdbcCommand object. Let system handle it.
                }
            }              
        }

        /**
         * When trying to get the space tables. Create a scratch space with the repository and then call a generate schema. 
         * This will be used to get valid metadata for tables
         * * */
        private static Space createScratchSpace(Acorn.DBConnection.QueryConnection conn, string schema, string connectorString,Space sourceSpace,string owner,Acorn.DBConnection.QueryConnection spconn)
        {
            Acorn.Util.mainSchema = "dbo";
            Acorn.Util.MOVE_LOCAL_PROTOCOL = "http://";
            Acorn.Utils.ConfigUtils.RUNNING_MODE = Acorn.Utils.ConfigUtils.MOVE_MODE;
            Acorn.Utils.ConfigUtils.MOVE_CONNECT_STRING = connectorString;
            MainAdminForm maf = new MainAdminForm();
            maf.setHeadless(true);
            maf.setLogger(systemLog);
            DirectoryInfo d = new DirectoryInfo(sourceSpace.Directory);
            string contentdir = sourceSpace.Directory.Substring(0,sourceSpace.Directory.IndexOf(d.Parent.ToString())+d.Parent.ToString().Length);
     
            string spaceName = "MoveSpace_Scratch_Space_" + DateTime.Now.Year + "_" + DateTime.Now.Month +"_"+DateTime.Now.Hour +"_"+ DateTime.Now.Minute + "_" + DateTime.Now.Second;
            
            Acorn.User u = Database.getUser(conn, schema, owner);
            if (u == null)
            {
                Global.systemLog.Error("Unable to find user: " + sourceSpace.AdminUser);
                return null;
            }

            Global.systemLog.Info("Creating Scratch Space  " + spaceName);


            Space sp = new Space(u, sourceSpace.Type, sourceSpace.ProcessVersionID, sourceSpace.SFDCVersionID, contentdir, null);
            sp.Type = sourceSpace.Type;
            sp.Name = spaceName;
            sp.Comments = "Scratch Space Created for MoveSpace";
            sp.Automatic = sourceSpace.Automatic;

            sp.ConnectString = sourceSpace.ConnectString;
            sp.DatabaseType = sourceSpace.DatabaseType;
            sp.DatabaseDriver = sourceSpace.DatabaseDriver;
            sp.AdminUser = sourceSpace.AdminUser;
            sp.AdminPwd = sourceSpace.AdminPwd;
            sp.QueryConnectionName = sourceSpace.QueryConnectionName;
            sp.QueryConnectString = sourceSpace.QueryConnectString;
            sp.QueryDatabaseDriver = sourceSpace.QueryDatabaseDriver;
            sp.QueryDatabaseType = sourceSpace.QueryDatabaseType;
            sp.QueryPwd = sourceSpace.QueryPwd;
            sp.QueryUser = sourceSpace.QueryUser;
            sp.Active = sourceSpace.Active;
            sp.LoadNumber = sourceSpace.LoadNumber;
            sp.QueryLanguageVersion = sourceSpace.QueryLanguageVersion;
            sp.MaxQueryRows = sourceSpace.MaxQueryRows;
            sp.MaxQueryTimeout = sourceSpace.MaxQueryTimeout;
            sp.UsageTracking = sourceSpace.UsageTracking;
            sp.ProcessVersionID = sourceSpace.ProcessVersionID;
            sp.UseDynamicGroups = sourceSpace.UseDynamicGroups;
            sp.AllowRetryFailedLoad = sourceSpace.AllowRetryFailedLoad;
            sp.SFDCVersionID = sourceSpace.SFDCVersionID;
            sp.DisallowExternalScheduler = sourceSpace.DisallowExternalScheduler;
            sp.SFDCNumThreads = sourceSpace.SFDCNumThreads;
            sp.DiscoveryMode = sourceSpace.DiscoveryMode;
            sp.MapNullsToZero = sourceSpace.MapNullsToZero;
            sp.awsAccessKeyId = sourceSpace.awsAccessKeyId;
            sp.awsSecretKey = sourceSpace.awsSecretKey;
            sp.ScheduleFrom = sourceSpace.ScheduleFrom;
            sp.ScheduleSubject = sourceSpace.ScheduleSubject;
            sp.dbTimeZone = sourceSpace.dbTimeZone;

            Database.createSpace(conn, Acorn.Util.mainSchema, u, sp);

            Space scratchSpace = Database.getSpace(conn, schema, sp.ID);
            Global.systemLog.Info("Created Scratch Space " + scratchSpace.Name + "(" + scratchSpace.ID + ")");

            File.Copy(Path.Combine(sourceSpace.Directory, "repository.xml"), Path.Combine(scratchSpace.Directory, "repository.xml"), true);
            Global.systemLog.Debug("Copied repository.xml from " + sourceSpace.Directory + " to " + scratchSpace.Directory);
            if (File.Exists(Path.Combine(scratchSpace.Directory, "repository.xml")))
            {
                 maf.setRepositoryDirectory(scratchSpace.Directory);
                 maf.loadRepositoryFromFile(Path.Combine(scratchSpace.Directory, "repository.xml"));
                 maf.setupTree();
                 Acorn.Util.buildApplication(maf, schema, DateTime.Now, scratchSpace.LoadNumber, null, sp, null);
                 maf.RequiresPublish = false;
                 Acorn.Util.saveApplication(maf, scratchSpace, null, u);
                 maf.setRepositoryDirectory(scratchSpace.Directory);
                 maf.loadRepositoryFromFile(Path.Combine(scratchSpace.Directory, "repository.xml"));
                 maf.setupTree();
            }
            else
            {
                Global.systemLog.Error("Unable to Create Scratch Space.");
                return null;
            }


            File.WriteAllText(scratchSpace.Directory + "\\load.cmd", "repository " + scratchSpace.Directory + "\\repository_dev.xml\rresetetlrun 1\rgenerateschema 1 notime");
            foreach (StagingTable st in maf.stagingTableMod.getAllStagingTables())
            {
                if (st.LiveAccess || st.InheritTables != null)
                {
                    continue;
                }
                if (st.DiscoveryTable)
                {
                    File.AppendAllText(scratchSpace.Directory + "\\load.cmd", "\rcreatediscoveryst " + st.Name);
                }
            }
            Global.systemLog.Debug("Checking for  TXN_SCRIPT_LOG in " + sp.Schema + " : " + Database.tableExists(spconn, sourceSpace.Schema, "TXN_SCRIPT_LOG"));
            if (Database.tableExists(spconn, sourceSpace.Schema, "TXN_SCRIPT_LOG"))
            {
                File.AppendAllText(scratchSpace.Directory + "\\load.cmd", "\rcreatescriptlog TXN_SCRIPT_LOG");
            }
            if (Database.tableExists(spconn, sourceSpace.Schema, "TXN_SNAPSHOT_POLICY"))
            {
                SnapshotPolicyManager spol = new SnapshotPolicyManager(conn, Acorn.Util.mainSchema, scratchSpace, maf);
                spol.setupPolicyTable(1);
            }
            string args = Util.getEngineCommandArgs(scratchSpace, 0);
            string enginecmd = Acorn.Util.getEngineCommandFileName(sp);
            if (enginecmd == null || !File.Exists(enginecmd))
            {
                Global.systemLog.Error("Unable to use the same Processing engine as Original Space. Required Process Version ID " + sp.ProcessVersionID);
                return null;
            }
            string workingDirectory = (new FileInfo(enginecmd)).Directory.FullName;
            ProcessStartInfo psi = ProcessStartInfoBuilder.builder().addProcessArguments(args).setWorkingDirectory(workingDirectory).build(enginecmd);
            Process proc = Process.Start(psi);
            string output = proc.StandardOutput.ReadToEnd();
            proc.WaitForExit();
            if (proc.ExitCode != 0)
            {
                Global.systemLog.Warn("The command to generate schema failed.");
                return  null;
            }

            return scratchSpace;
        }
      
    }
}
