﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Acorn;
using System.Data.Odbc;
using System.IO;
using System.Diagnostics;
using System.Data;
using System.Xml.Serialization;
using System.Xml;
using System.Web.Security;
using System.Collections.Specialized;
using log4net;
using log4net.Config;
using System.Reflection;
using Acorn.Utils;
using MoveSpace.Utils;
using Performance_Optimizer_Administration;

namespace MoveSpace
{
    class Util
    {
        static string[] metadataDirs = { "data", "catalog", "custom-subject-areas", "dcconfig", "DashboardTemplates", "DashboardBackgroundImage", "mapping" };
        static string[] metadataFiles = { "Variables.xml", "Variables_dev.xml", "LogicalExpressions.xml", "LogicalExpressions_dev.xml", Acorn.Util.REPOSITORY_DEV, Acorn.Util.REPOSITORY_PROD, "Packages_dev.xml", "Packages.xml",
                                       "CustomGeoMaps.xml", "DrillMaps.xml", "spacesettings.xml", "SavedExpressions.xml", "dcconfig.xml", "DashboardStyleSettings.xml", "sforce.xml" };
        public static string[] loadUsingSQLTables = { "TXN_COMMAND_HISTORY" };
       

        public static void copyFilesAndDirectories(string fromRootDir, string toRootDir)
        {
            Global.systemLog.Info("Copying meta data files");
            foreach (string file in metadataFiles)
            {
                string fromFile = Path.Combine(fromRootDir, file);
                if (File.Exists(fromFile))
                {
                    string toFile = Path.Combine(toRootDir, file);
                    Global.systemLog.Info("Copying " + file);
                    File.Copy(fromFile, toFile, true);
                }
            }

            Global.systemLog.Info("Copying meta data directories");
            foreach (string dir in metadataDirs)
            {
                string fromDir = Path.Combine(fromRootDir, dir);
                if (Directory.Exists(fromDir))
                {
                    string toDir = Path.Combine(toRootDir, dir);
                    Global.systemLog.Info("Copying " + dir);
                    Acorn.Util.copyDir(fromDir, toDir);
                }
            }
        }

        public static void compressFile(string targetDir, string tname)
        {
            CompressionUtil cUtil = new CompressionUtil();
            string toCompressFileName = Path.Combine(targetDir, tname);
            toCompressFileName = toCompressFileName + ".txt";
            string outputFileName = cUtil.compressFile(toCompressFileName);
            try
            {
                if (outputFileName != null && outputFileName.Equals(toCompressFileName + ".gz"))
                {
                    File.Delete(toCompressFileName);
                }
            }
            catch (Exception e)
            {
                Program.systemLog.Warn("Exception while cleaning up original file after compression", e);
            }
        }

        public static void decompressFile(string directory, string inputFile)
        {
            Global.systemLog.Info("Decompressing file " + inputFile);
            CompressionUtil cUtil = new CompressionUtil();
            string toDecompressFileName = Path.Combine(directory, inputFile);
            string backupFileName = toDecompressFileName + ".backup";
            File.Copy(toDecompressFileName, backupFileName, true);
            cUtil.deCompressFile(toDecompressFileName);
            try
            {
                if (!File.Exists(toDecompressFileName))
                {
                    // to keep the copy of the compressed file
                    File.Move(backupFileName, toDecompressFileName);
                }
                else
                {
                    File.Delete(backupFileName);
                }
            }
            catch (Exception ex)
            {
                Program.systemLog.Warn("Exception while cleaning up back up files during decompression", ex);
            }
        }

        public static void reMapMeasureTables(SpaceSchema ss, MainAdminForm maf,int maxPhysicalColumnLength)
        {
            foreach (TableSchema ts in ss.Tables)
            {
                foreach (MeasureTable mt in maf.measureTablesList)
                {
                    if (mt.TableName.Equals(ts.LogicalName))
                    {
                        List<string> surrogateKeyList = new List<string>();
                        if (ts.PhysicalName != mt.PhysicalName)
                        {
                            Global.systemLog.Debug(mt.TableName + " Renaming Physical Table Name from " + ts.PhysicalName + " to " + mt.PhysicalName);
                            ts.PhysicalName = mt.PhysicalName;
                        }
                        if (mt.Grain != null)
                        {
                            foreach (MeasureTableGrain mgGrain in mt.Grain.measureTableGrains)
                            {
                                Hierarchy h = maf.hmodule.getHierarchy(mgGrain.DimensionName);
                                Level l = h.findLevel(mgGrain.DimensionLevel);
                            
                                foreach (LevelKey lk in l.Keys)
                                {
                                    if (lk.SurrogateKey)
                                    {
                                        surrogateKeyList.AddRange(lk.ColumnNames);
                                        break;
                                    }
                                }
                            }
                        }
                        foreach (MeasureColumn mc in mt.MeasureColumns)
                        {
                            foreach (Column c in ts.Columns)
                            {
                                try
                                {
                                    string sLogicalName = c.LogicalName;
                                    string sSurrogateKey = "";
                                    if (c.LogicalName!=null && c.LogicalName.IndexOf(".") > 0)
                                    {
                                        string sFactTable = c.LogicalName.Substring(0, c.LogicalName.IndexOf("."));
                                        sSurrogateKey = c.LogicalName.Substring(c.LogicalName.IndexOf(".")+1);
                                        if (sSurrogateKey != null && sLogicalName.Length > maxPhysicalColumnLength)
                                        {
                                            sSurrogateKey = ApplicationBuilder.getMD5Hash(ApplicationBuilder.getSQLGenType(maf), sSurrogateKey, maxPhysicalColumnLength, maf.builderVersion);
                                        }
                                        sLogicalName = sFactTable + "." + sSurrogateKey;
                                    }
                                    if (surrogateKeyList.Contains(sSurrogateKey) && mc.ColumnName.Equals(sLogicalName))
                                    {
                                        c.PhysicalName = mc.PhysicalName;
                                    }
                                }
                                catch (ArgumentOutOfRangeException ex)
                                {
                                    Global.systemLog.Error("Error Deriving Surrogate Key Name. MoveSpace May Fail" + ex);
                                }
                               
                                if (mc.ColumnName.Equals(c.LogicalName) )
                                {
                                    if (mc.PhysicalName != c.PhysicalName)
                                    {
                                        Global.systemLog.Debug(mt.TableName + " Renaming Physical Column Name from " + c.PhysicalName + " to " + mc.PhysicalName);
                                        c.PhysicalName = mc.PhysicalName;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        public static void reMapDimensionTables(SpaceSchema ss, MainAdminForm maf,int maxPhysicalColumnLength,Space sp)
        {
            foreach (TableSchema ts in ss.Tables)
            {
                foreach (DimensionTable dt in maf.dimensionTablesList)
                {
                    if (dt.TableName.Equals(ts.LogicalName))
                    {
                        if (ts.PhysicalName != dt.PhysicalName)
                        {
                            Global.systemLog.Debug(dt.TableName + " Renaming Physical TableName Name from " + ts.PhysicalName + " to " + dt.PhysicalName);
                            ts.PhysicalName = dt.PhysicalName;
                        }
                        
                        foreach (DimensionColumn dc in dt.DimensionColumns)
                        {
                            foreach (Column c in ts.Columns)
                            {
                                if (c.PhysicalName != null && c.PhysicalName.Contains("CKSUM$"))
                                {
                                        if ( c.PhysicalName.Length > maxPhysicalColumnLength)
                                        {
                                            //get the actual table name MD5 hash
                                            string cTempName = c.PhysicalName.Replace("ST_","").Replace("_CKSUM$","");
                                            cTempName = ApplicationBuilder.getMD5Hash(ApplicationBuilder.getSQLGenType(maf), cTempName, maxPhysicalColumnLength, maf.builderVersion);
                                            string hashForCheckSumCol = "ST_" + cTempName + "_CKSUM$";
                                            c.PhysicalName = hashForCheckSumCol;
                                    }
                                    continue;
                                }
                                try
                                {
                                    string sLogicalName = c.LogicalName;
                                    if (c.LogicalName != null )
                                    {
                                        if (sLogicalName != null && sLogicalName.Length > maxPhysicalColumnLength)
                                        {
                                            sLogicalName = ApplicationBuilder.getMD5Hash(ApplicationBuilder.getSQLGenType(maf), sLogicalName, maxPhysicalColumnLength, maf.builderVersion);
                                        }
                                    }
                                    if (dc.ColumnName.Equals(sLogicalName))
                                    {
                                        c.PhysicalName = dc.PhysicalName;
                                    }
                                }
                                catch (ArgumentOutOfRangeException ex)
                                {
                                    Global.systemLog.Error("Error Deriving Surrogate Key Name. MoveSpace May Fail" + ex);
                                }
                               
                                if (dc.ColumnName.Equals(c.LogicalName))
                                {
                                    if (dc.PhysicalName != c.PhysicalName)
                                    {
                                        Global.systemLog.Debug(dt.TableName + " Renaming Physical Column Name from " + c.PhysicalName + " to " + dc.PhysicalName);
                                        c.PhysicalName = dc.PhysicalName;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

    
        private static string getCheckSum(string columnName, Space sp)
        {
            string enginecmd = Acorn.Util.getEngineCommandFileName(sp);
            string workingDirectory = (new FileInfo(enginecmd)).Directory.FullName;

            File.WriteAllText(sp.Directory + "\\getCheckSum.cmd", "getchecksum " +columnName);
            string args = sp.Directory + "\\getCheckSum.cmd" + " " + sp.Directory;

            ProcessStartInfo psi = ProcessStartInfoBuilder.builder().addProcessArguments(args).setWorkingDirectory(workingDirectory).build(enginecmd);
            Process proc = Process.Start(psi);
            string output = proc.StandardOutput.ReadToEnd();
            if(output==null || !output.Contains("[HashCode]"))
            {
                return null;
            }
            try
            {
                output = output.Substring(11).Replace("\r","").Replace("\n","");
            }
            catch (ArgumentOutOfRangeException ex)
            {
                Global.systemLog.Error("Unable to determine a valid checksum for " + columnName + " : " + ex);
            }
            proc.WaitForExit();
            if (proc.ExitCode != 0)
            {
                Global.systemLog.Warn("The command to generate schema failed.");
                return null;
            }
            return output;
        }

        public static string getDataConnectionServer(Space sp)
        {
            if (sp.ConnectString.ToLower().StartsWith("jdbc:sqlserver://"))
            {
                string temp = sp.ConnectString;
                int index = temp.IndexOf(';');  // separator after hostname
                int length = index - 17;        // 17 is the length of "jdbc:sqlserver://"
                return temp.Substring(17, length).Replace(':', ','); // Connect strings to BCP are host,port not host:port
            }
            else if (sp.ConnectString.ToLower().StartsWith("jdbc:mysql://"))
            {
                string temp = sp.ConnectString;
                int index = temp.IndexOf(';');  // separator after hostname
                if (index < 0)
                    index = temp.Length;
                int length = index - 13;        // 13 is the length of "jdbc:mysql://"
                return temp.Substring(13, length).Replace(':', ',');
            }
            else if (sp.ConnectString.ToLower().StartsWith("jdbc:memdb://"))
            {
                string temp = sp.ConnectString;
                int index = temp.IndexOf(';');  // separator after hostname
                if (index < 0)
                    index = temp.Length;
                int length = index - 13;        // 13 is the length of "jdbc:memdb://"
                string sname = temp.Substring(13, length);
                if (sname.Contains(':'))
                    sname = sname.Substring(0, sname.IndexOf(':'));
                return sname;
            }
            Global.systemLog.Error("Can not movespace to/from a non-SQL Server/Infobright/MemDB instance");
            return null;
        }

        public static String getEngineCommandArgs(Space sp, int loadNumber)
        {
            return sp.Directory + "\\load.cmd " + sp.Directory + " " + String.Format("{0:0000000000}", loadNumber);
        }

        public static Acorn.DBConnection.QueryConnection getDataConnection(Space sp)
        {
            Acorn.DBConnection.QueryConnection dataconn = new Acorn.DBConnection.QueryConnection(sp.getFullConnectString());
            dataconn.Open();
            return dataconn;
        }

        public static void truncateTable(Space sp, String tableName)
        {
            Acorn.DBConnection.QueryCommand cmd = null;
            try
            {
                Global.systemLog.Debug("TRUNCATE TABLE " + sp.Schema + "." + tableName);
                Acorn.DBConnection.QueryConnection dataconn = Util.getDataConnection(sp);
                cmd = dataconn.CreateCommand();
                cmd.CommandText = "TRUNCATE TABLE " + sp.Schema + "." + tableName;
                cmd.ExecuteNonQuery();
            }
            catch (Exception)
            {
                Global.systemLog.Error("Unable to truncate table " + tableName );
            }
            finally
            {
                if (cmd != null)
                    cmd.Dispose();
            }
        }

        public static void FileCopyWithoutOverwriting(string sourceFile, string fileName, string destinationPath)
        {
            
            if ((destinationPath.EndsWith("\\") || destinationPath.EndsWith("/")) == false)
                destinationPath += "\\";
            try
            {
                if (File.Exists(destinationPath + fileName))
                {
                    int count = 1;

                    FileInfo info = new FileInfo(sourceFile);
                    string ext = info.Extension;
                    string prefix;

                    if (ext.Length > 0)
                    {
                        prefix = fileName.Substring(0, fileName.Length - ext.Length);
                    }
                    else
                        prefix = fileName;

                    while (File.Exists(destinationPath + fileName))
                    {
                        fileName = prefix + "." + count.ToString() + ".save" + ext;
                        count++;
                    }
                    File.Copy(sourceFile, destinationPath + fileName);
                }
                else
                {
                    File.Copy(sourceFile, destinationPath + fileName);
                }
                Global.systemLog.Debug(fileName + " back up completed.");
            }
            catch (Exception)
            {
                Global.systemLog.Debug("Error Backing up " + fileName);
            }
        }
    }
}
