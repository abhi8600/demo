﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MoveSpace
{
    public class TableSchema
    {
        public static int TYPE_MEASURE = 1;
        public static int TYPE_DIMENSION = 2;
        public static int TYPE_OTHER = 0;
        public int TableType;
        public string LogicalName;
        public string PhysicalName;
        public string FileName;
        public List<Column> Columns;
    }
}
