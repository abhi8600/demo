﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Acorn;
using System.Data.Odbc;
using Performance_Optimizer_Administration;
using System.IO;
using System.Diagnostics;
using System.Data;
using System.Xml.Serialization;
using System.Xml;
using System.Web.Security;
using System.Collections.Specialized;
using log4net;
using log4net.Config;
using System.Reflection;
using MoveSpace.Utils;
using Acorn.Utils;

namespace MoveSpace
{
    class BulkLoad
    {
        private static char[] linechars = new char[] { '\n', '\r' };
        private static bool TEST = false;

        public static void bulkLoadMemDB(MainAdminForm maf, Space sp, TableSchema ts, Acorn.DBConnection.QueryConnection dataconn, string targetDir,
          string sourceDBDriver)
        {
            Acorn.DBConnection.QueryCommand cmd = dataconn.CreateCommand();
            cmd.CommandText = "SHOW TABLE " + sp.Schema + "." + ts.PhysicalName;
            Acorn.DBConnection.QueryReader qr = cmd.ExecuteReader();
            List<string> dbcolumns = new List<string>();
            while (qr.Read())
            {
                dbcolumns.Add(qr.GetString(0));
            }
            qr.Close();
            string cols = "";
            // Map to convert input field position to actual target schema position because IB needs fields in exact order and can't skip
            Dictionary<int, int> fieldmap = new Dictionary<int, int>();
            for (int i = 0; i < dbcolumns.Count; i++)
            {
                string colname = dbcolumns[i];
                if (cols.Length > 0)
                    cols += ",";
                cols += colname;
                bool found = false;
                if (ts.TableType == TableSchema.TYPE_MEASURE || ts.TableType == TableSchema.TYPE_DIMENSION)
                {
                    for (int j = 0; j < ts.Columns.Count; j++)
                    {
                        if (ts.TableType == TableSchema.TYPE_MEASURE)
                        {
                            DataRowView[] drvlist = maf.measureColumnMappingsTablesView.FindRows(ts.LogicalName);
                            foreach (DataRowView drv in drvlist)
                            {
                                string cname = (string)drv.Row["ColumnName"];
                                string pname = (string)drv.Row["PhysicalName"];
                                if (colname == pname && cname == ts.Columns[j].LogicalName)
                                {
                                    fieldmap.Add(j, i);
                                    found = true;
                                    break;
                                }
                            }
                        }
                        else if (ts.TableType == TableSchema.TYPE_DIMENSION)
                        {
                            DataRowView[] drvlist = maf.measureColumnMappingsTablesView.FindRows(ts.LogicalName);
                            foreach (DataRowView drv in drvlist)
                            {
                                string cname = (string)drv.Row["ColumnName"];
                                string pname = (string)drv.Row["PhysicalName"];
                                if (colname == pname && cname == ts.Columns[j].LogicalName)
                                {
                                    fieldmap.Add(j, i);
                                    found = true;
                                    break;
                                }
                            }
                        }
                        if (found)
                            break;
                    }
                    if (!found)
                    {
                        for (int j = 0; j < ts.Columns.Count; j++)
                        {
                            if (ts.Columns[j].PhysicalName == colname)
                            {
                                fieldmap.Add(j, i);
                                break;
                            }
                        }
                    }
                }
                else
                    fieldmap.Add(i, i);
            }
            string filename = ts.FileName + ".txt";
            if (sourceDBDriver == "com.microsoft.sqlserver.jdbc.SQLServerDriver")
            {
                if (!TEST || !File.Exists(targetDir + "\\" + filename + ".utf8"))
                {
                    // Convert UCS2 to UTF8
                    processMSSQLDataFileToUTF8(targetDir + "\\" + filename, ts, dbcolumns, fieldmap);
                }
                filename += ".utf8";
            }
            bool hasData = true;
            FileInfo fi = new FileInfo(targetDir + "\\" + filename);
            if (fi.Length < 100)
            {
                // read entire file to make sure it has data
                string s = File.ReadAllText(fi.FullName).Trim();
                if (!s.Contains('|'))
                {
                    hasData = false;
                }
            }
            if (hasData)
            {
                cmd.CommandText = "TRUNCATE TABLE " + sp.Schema + "." + ts.PhysicalName;
                Object result = cmd.ExecuteNonQuery();
                cmd.CommandText = "LOAD DATA INFILE '" + targetDir.Replace('\\', '/') + "/" + filename.Replace('\\', '/')
                                        + "' INTO TABLE " + sp.Schema + "." + ts.PhysicalName
                                        + " DELIMITED BY '|' ESCAPED BY '\\\\' ENCLOSED BY '\"' TERMINATED BY '\n' " + " (" + cols + ")";
                try
                {
                    result = cmd.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    Global.systemLog.Error("The bulk import failed for " + filename + "." + ex);
                }
                cmd.CommandText = "FLUSH";
                result = cmd.ExecuteNonQuery();
            }
            if (sourceDBDriver == "com.microsoft.sqlserver.jdbc.SQLServerDriver" && !TEST)
                System.IO.File.Delete(targetDir + "\\" + filename);
        }

        public static void bulkLoadInfobright(MainAdminForm maf, Space sp, TableSchema ts, Acorn.DBConnection.QueryConnection dataconn, string targetDir, string sourceDBDriver)
        {
            Acorn.DBConnection.QueryCommand cmd = dataconn.CreateCommand();
            cmd.CommandText = "DESCRIBE " + sp.Schema + "." + ts.PhysicalName;
            Acorn.DBConnection.QueryReader qr = cmd.ExecuteReader();
            List<string> dbcolumns = new List<string>();
            while (qr.Read())
            {
                dbcolumns.Add(qr.GetString(0));
            }
            qr.Close();
            string cols = "";
            // Map to convert input field position to actual target schema position because IB needs fields in exact order and can't skip
            Dictionary<int, int> fieldmap = new Dictionary<int, int>();
            for (int i = 0; i < dbcolumns.Count; i++)
            {
                string colname = dbcolumns[i];
                if (cols.Length > 0)
                    cols += ",";
                cols += colname;
                bool found = false;
                if (ts.TableType == TableSchema.TYPE_MEASURE || ts.TableType == TableSchema.TYPE_DIMENSION)
                {
                    for (int j = 0; j < ts.Columns.Count; j++)
                    {
                        if (ts.TableType == TableSchema.TYPE_MEASURE)
                        {
                            DataRowView[] drvlist = maf.measureColumnMappingsTablesView.FindRows(ts.LogicalName);
                            foreach (DataRowView drv in drvlist)
                            {
                                string cname = (string)drv.Row["ColumnName"];
                                string pname = (string)drv.Row["PhysicalName"];
                                if (colname == pname && cname == ts.Columns[j].LogicalName)
                                {
                                    fieldmap.Add(j, i);
                                    found = true;
                                    break;
                                }
                            }
                        }
                        else if (ts.TableType == TableSchema.TYPE_DIMENSION)
                        {
                            DataRowView[] drvlist = maf.measureColumnMappingsTablesView.FindRows(ts.LogicalName);
                            foreach (DataRowView drv in drvlist)
                            {
                                string cname = (string)drv.Row["ColumnName"];
                                string pname = (string)drv.Row["PhysicalName"];
                                if (colname == pname && cname == ts.Columns[j].LogicalName)
                                {
                                    fieldmap.Add(j, i);
                                    found = true;
                                    break;
                                }
                            }
                        }
                        if (found)
                            break;
                    }
                    if (!found)
                    {
                        for (int j = 0; j < ts.Columns.Count; j++)
                        {
                            if (ts.Columns[j].PhysicalName == colname)
                            {
                                fieldmap.Add(j, i);
                                break;
                            }
                        }
                    }
                }
                else
                    fieldmap.Add(i, i);
            }
            string filename = ts.FileName + ".txt";
            if (sourceDBDriver == "com.microsoft.sqlserver.jdbc.SQLServerDriver")
            {
                // Convert UCS2 to UTF8
                processMSSQLDataFileToUTF8(targetDir + "\\" + filename, ts, dbcolumns, fieldmap);
                filename += ".utf8";
            }
            bool hasData = true;
            FileInfo fi = new FileInfo(targetDir + "\\" + filename);
            if (fi.Length < 100)
            {
                // read entire file to make sure it has data
                string s = File.ReadAllText(fi.FullName).Trim();
                if (!s.Contains('|'))
                {
                    hasData = false;
                }
            }
            if (hasData)
            {
                cmd.CommandText = "TRUNCATE TABLE " + sp.Schema + "." + ts.PhysicalName;
                Object result = cmd.ExecuteNonQuery();
                cmd.CommandText = "LOAD DATA LOCAL INFILE '" + targetDir.Replace('\\', '/') + "/" + filename.Replace('\\', '/')
                                        + "' INTO TABLE " + sp.Schema + "." + ts.PhysicalName
                                        + " CHARACTER SET UTF8 FIELDS TERMINATED BY '|' ESCAPED BY '\\\\' OPTIONALLY ENCLOSED BY '\"' LINES TERMINATED BY '\n' " + " (" + cols + ")";
                Global.systemLog.Debug(cmd.CommandText);
                try
                {
                    result = cmd.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    Global.systemLog.Error("The bulk import failed for " + filename + "." + ex);
                }
            }
            if (sourceDBDriver == "com.microsoft.sqlserver.jdbc.SQLServerDriver")
                System.IO.File.Delete(targetDir + "\\" + filename);
        }

        public static void processMSSQLDataFileToUTF8(string filename, TableSchema ts, List<string> dbcolumns, Dictionary<int, int> fieldmap)
        {
            StreamReader reader = new StreamReader(filename, true);
            StreamWriter writer = new StreamWriter(filename + ".utf8", false, Encoding.UTF8);
            string line;
            while ((line = readline(reader, ts, dbcolumns, fieldmap)) != null)
            {
                writer.Write(line + '\n');
            }
            reader.Close();
            writer.Close();
        }

        
        public static string readline(StreamReader reader, TableSchema ts, List<string> dbcolumns, Dictionary<int, int> fieldmap)
        {
            /*
             * Since MSSQL doesn't quote strings, need to count the number of field terminators - which are guaranteed not to be inside a string.
             * It is assumed (which is the case for facts and dimensions) that the last field is not a string that can have an endline in it
             */
            int character = 0;
            StringBuilder sb = new StringBuilder();
            int numFields = 0;
            StringBuilder curField = new StringBuilder();
            string[] output = new string[dbcolumns.Count];
            while ((character = reader.Read()) >= 0)
            {
                if (character == '\r')
                    continue;
                if (character == '|')
                {
                    if (!fieldmap.ContainsKey(numFields))
                    {
                        numFields++;
                        continue;
                    }
                    string val = curField.ToString();
                    val = val.Replace("\\", "\\\\").Replace("\"", "\\\"");
                    if (val.IndexOfAny(linechars) >= 0)
                        val = "\"" + val + "\"";
                    output[fieldmap[numFields]] = val;
                    numFields++;
                    curField.Clear();
                }
                else if (character == '\n' && numFields >= ts.Columns.Count - 1)
                {
                    break;
                }
                else
                    curField.Append((char)character);
            }
            if (fieldmap.ContainsKey(numFields))
            {
                output[fieldmap[numFields]] = curField.ToString();
            }
            if (numFields > 0)
            {
                for (int i = 0; i < output.Length; i++)
                {
                    if (i > 0)
                        sb.Append('|');
                    if (output[i] != null)
                        sb.Append(output[i]);
                }
            }
            return sb.Length > 0 ? sb.ToString() : null;
        }

        public static void bulkLoadMSSQL(Space sp, TableSchema ts, Acorn.DBConnection.QueryConnection dataconn, string targetDir, string server,
            string username, string password)
        {
            List<string[]> columns = new List<string[]>();
            foreach (Column c in ts.Columns)
            {
                string type = c.Type;
                if (type.ToLower() == "nvarchar" || type.ToLower() == "varchar")
                {
                    if (c.Width != 0)
                        type = c.Type + "(" + c.Width + ")";
                    else
                        type = c.Type + "(max)"; // 0 means 'max' for *varchar
                }
                columns.Add(new string[] { c.PhysicalName, type });
            }


            string exeToFork = "bcp";
            string Arguments = dataconn.Database + "." + sp.Schema + "." + ts.PhysicalName + " in \"" + targetDir + "\\" +
                ts.FileName + ".txt\" " + "\"-t|\" -r\\n" + " -E -S" + server + " -U" + username + " -P" + password + " -w";
           
            ProcessStartInfo psi = ProcessStartInfoBuilder.builder()
                    .addProcessArguments(Arguments)
                    .setWorkingDirectory(targetDir)
                    .build(exeToFork);

            Global.systemLog.Info("Executing command: " + psi.FileName + " " + psi.Arguments);
            Process p = Process.Start(psi);
            string output = p.StandardOutput.ReadToEnd();
            p.WaitForExit();
            if (p.ExitCode != 0)
            {
                Global.systemLog.Error("The bulk import failed for " + psi.FileName + " . Please check for issues in bulkloading");
            }
        }

        /** Used to load Admin tables like TXN_COMMAND_HISTORY, in case the there are new additional columns added, insert them as NULL
         * 
         * */
        public static void loadUsingSQL(Space sp, TableSchema ts, Acorn.DBConnection.QueryConnection dataconn, string targetDir,string fname)
        {
            Acorn.DBConnection.QueryCommand cmd = null;
            StreamReader reader = null;
            try
            {
                List<string[]> columns = new List<string[]>();
                StringBuilder sbColumns = new StringBuilder();
                StringBuilder sbValues = new StringBuilder();
                bool isfirst = true;
                int totalColumnsCount = ts.Columns.Count();
                foreach (Column c in ts.Columns)
                {
                    if (!isfirst)
                    {
                        sbColumns.Append(",");
                        sbValues.Append(",");
                    }
                    string type = c.Type;
                    if (type.ToLower() == "nvarchar" || type.ToLower() == "varchar")
                    {
                        if (c.Width != 0)
                            type = c.Type + "(" + c.Width + ")";
                        else
                            type = c.Type + "(max)"; // 0 means 'max' for *varchar
                    }
                    columns.Add(new string[] { c.PhysicalName, type });
                    sbColumns.Append(c.PhysicalName);
                    sbValues.Append("?");
                    isfirst = false;
                }
               
                reader = new StreamReader(fname, true);
                string line;
                while ((line = reader.ReadLine()) != null)
                    {
                        if (cmd == null)
                        {
                            cmd = dataconn.CreateCommand();
                        }
                            cmd.CommandText = "INSERT INTO " + sp.Schema + "." + ts.PhysicalName + "(" + sbColumns.ToString() + ") values(" + sbValues + ")";
                        string[] columnList = line.Split('|');
                        int index = 0;
                        foreach (string s in columnList)
                        {

                            string sValue = (sp.DatabaseDriver == "com.mysql.jdbc.Driver") ? s.Replace("\"", "") : s;
                            string[] tsColumn = columns[index];

                            if (tsColumn[1] != null && tsColumn[1].Equals("datetime"))
                            {
                                if (s == null || s.Length == 0 || ("\\N" == s && sp.DatabaseDriver == "com.mysql.jdbc.Driver"))
                                {
                                    cmd.Parameters.Add("@" + tsColumn[0], OdbcType.DateTime).Value = DBNull.Value;
                                }
                                else
                                {
                                    cmd.Parameters.AddWithValue("@" + tsColumn[0], OdbcType.DateTime).Value = sValue;
                                }
                            }
                            else if (tsColumn[1] != null && tsColumn[1].Equals("int"))
                            {
                                if (s == null || s.Length == 0 || ("\\N" == s && sp.DatabaseDriver == "com.mysql.jdbc.Driver"))
                                {
                                    cmd.Parameters.Add("@" + tsColumn[0], OdbcType.Int).Value = DBNull.Value;
                                }
                                else
                                {
                                    cmd.Parameters.Add("@" + tsColumn[0], OdbcType.Int).Value = Convert.ToInt64(sValue);
                                }
                            }
                            else if (tsColumn[1] != null && tsColumn[1].Equals("bigint"))
                            {
                                if (s == null || s.Length == 0 || ("\\N" == s && sp.DatabaseDriver == "com.mysql.jdbc.Driver"))
                                {
                                    cmd.Parameters.Add("@" + tsColumn[0], OdbcType.BigInt).Value = DBNull.Value;
                                }
                                else
                                {
                                    cmd.Parameters.Add("@" + tsColumn[0], OdbcType.BigInt).Value = Convert.ToInt64(sValue);
                                }
                            }
                            else
                            {
                                if (s == null || s.Length == 0 || ("\\N" == s && sp.DatabaseDriver == "com.mysql.jdbc.Driver"))
                                {
                                    cmd.Parameters.Add("@" + tsColumn[0], OdbcType.NVarChar).Value = DBNull.Value;
                                }
                                else
                                {
                                    cmd.Parameters.Add("@" + tsColumn[0], OdbcType.NVarChar).Value = sValue;
                                }
                            }
                            index++;
                        }
                        cmd.ExecuteNonQuery();
                        if (cmd != null)
                        {
                            cmd.Dispose();
                            cmd = null;
                        }
                    }
            }
            catch (Exception e)
            {
              
                Global.systemLog.Error("Could not load " + ts.PhysicalName + " "+  e);
            }
            finally
            {
                if (cmd != null)
                    cmd.Dispose();
                if (reader != null)
                    reader.Close();
            }
        }

   }
}
