package com.birst.spacecleanup.util;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;
import org.postgresql.jdbc4.Jdbc4Connection;

import com.birst.spacecleanup.SpaceCleanupMain;
import com.birst.spacecleanup.beans.CleanUpConfigBean;
import com.birst.spacecleanup.beans.DatabaseConfigBean;
import com.paraccel.jdbc3g.Jdbc3gConnection;

public class DatabaseUtil
{
	private static Logger logger = Logger.getLogger(SpaceCleanupMain.class);
	private static Connection adminDbConnection = null;
	private static Connection dataDbConnection = null;
	private static Connection quartzDbConnection = null;

	/*
	 * dbConnection Parameter : 1 - BirstAdmin, 2 - BirstData, 3 - QuartzAdmin
	 */
	private static void getConnection(int dbConnection) throws Exception
	{
		try
		{
			CleanUpConfigBean configBean = ConfigReader.getConfigBean();
			if (adminDbConnection == null && dbConnection == 1)
			{
				String driverClassName = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
				if (("ORACLE").equalsIgnoreCase(configBean.getAdminDbType()))
				{
					driverClassName = "oracle.jdbc.driver.OracleDriver";
				}
				Class.forName(driverClassName);
				String connectionUrl = configBean.getAdminConnectionUrlMSSQL();
				if (("ORACLE").equalsIgnoreCase(configBean.getAdminDbType()))
				{
					connectionUrl = configBean.getAdminConnectionUrlOrcl();
				}
				adminDbConnection = DriverManager.getConnection(connectionUrl, configBean.getAdminUser(), configBean.getAdminPwd());
				logger.debug("ConnectionUrl for AdminDB--> " + connectionUrl);
				adminDbConnection.setAutoCommit(false);
			}
			if (dataDbConnection == null && dbConnection == 2)
			{
				DatabaseConfigBean dbConfigBean = configBean.getDbConfigBean();
				String driverClassName = dbConfigBean.getDatabaseDriver();
				Class.forName(driverClassName);
				String connectionUrl = dbConfigBean.getConnectionString();
				logger.debug("ConnectionUrl for DataDB--> " + connectionUrl);
				dataDbConnection = DriverManager.getConnection(connectionUrl, dbConfigBean.getAdminUser(), dbConfigBean.getAdminPwd());
			}
			if (quartzDbConnection == null && dbConnection == 3)
			{
				String driverClassName = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
				if (("ORACLE").equalsIgnoreCase(configBean.getAdminDbType()))
				{
					driverClassName = "oracle.jdbc.driver.OracleDriver";
				}
				Class.forName(driverClassName);
				String connectionUrl = configBean.getQuartzUrlMSSQL();
				if (("ORACLE").equalsIgnoreCase(configBean.getAdminDbType()))
				{
					connectionUrl = configBean.getQuartzUrlOrcl();
				}
				logger.debug("ConnectionUrl for QuartzDB--> " + connectionUrl);
				quartzDbConnection = DriverManager.getConnection(connectionUrl, configBean.getQuartzUser(), configBean.getQuartzPwd());
			}
		}
		catch (Exception e1)
		{
			logger.error("Exception in getting connection ", e1);
			throw e1;
		}
		// return databaseConn;
	}

	public static void commitAndCloseConnections(boolean commit) throws Exception
	{
		try
		{
			CleanUpConfigBean configBean = ConfigReader.getConfigBean();

			if (commit)
			{
				adminDbConnection.commit();
				quartzDbConnection.commit();
			}
			else
			{
				adminDbConnection.rollback();
				quartzDbConnection.rollback();
			}

			if (("MSSQL").equalsIgnoreCase(configBean.getAdminDbType()))
			{
				String adminDbName = "BirstAdmin";
				if (configBean.getAdminDbName()!=null && !configBean.getAdminDbName().isEmpty())
				{
					adminDbName = configBean.getAdminDbName();
				}
				String quartzDbName = "QuartzAdmin";
				if (configBean.getQuartzDbName()!=null && !configBean.getQuartzDbName().isEmpty())
				{
					quartzDbName = configBean.getQuartzDbName();
				}
				clearTransactLogs(1, adminDbName);
				clearTransactLogs(3, quartzDbName);
				if ("true".equalsIgnoreCase(configBean.getIsDataDbMSSQL()))
				{
					clearTransactLogs(2, configBean.getmSSQLDataDbName());
				}
			}

			adminDbConnection.close();
			dataDbConnection.close();
			quartzDbConnection.close();
		}
		catch (Exception e1)
		{
			logger.error("Exception occured when closing connections ", e1);
			throw e1;
		}
	}

	public static ArrayList<HashMap<String, Object>> getQueryResult(String SQL, ArrayList<Object> parameters) throws Exception
	{
		logger.debug("Query to be executed: " + SQL);
		logger.debug("Query parameters: " + parameters);
		ArrayList<HashMap<String, Object>> queryResultSet = null;
		PreparedStatement prepareStatement = null;
		ResultSet rs = null;
		try
		{
			if (adminDbConnection == null)
			{
				getConnection(1);
			}
			prepareStatement = adminDbConnection.prepareStatement(SQL);
			if (parameters != null && !parameters.isEmpty())
			{
				int parameterIndex = 1;
				for (Object lObjParameter : parameters)
				{
					if (lObjParameter instanceof String)
					{
						prepareStatement.setString(parameterIndex++, (String) lObjParameter);
					}
					else if (lObjParameter instanceof Integer)
					{
						prepareStatement.setInt(parameterIndex++, (Integer) lObjParameter);
					}
					else
					{
						prepareStatement.setObject(parameterIndex++, lObjParameter);
					}
				}
			}
			rs = prepareStatement.executeQuery();
			ResultSetMetaData rsmd = rs.getMetaData();
			int columnCount = rsmd.getColumnCount();
			while (rs.next())
			{
				HashMap<String, Object> mapRecordObj = new HashMap<String, Object>();
				for (int index = 1; index <= columnCount; index++)
				{
					String columnName = rsmd.getColumnName(index);
					mapRecordObj.put(columnName, rs.getObject(columnName));
				}
				if (queryResultSet == null)
				{
					queryResultSet = new ArrayList<HashMap<String, Object>>();
				}
				queryResultSet.add(mapRecordObj);
			}
		}
		catch (Exception ex1)
		{
			logger.error("Exception in execution of query ", ex1);
			throw ex1;
		}
		finally
		{
			if (rs != null)
			{
				rs.close();
			}
			if (prepareStatement != null)
			{
				prepareStatement.close();
			}
		}
		return queryResultSet;
	}

	public static boolean deleteFromAdminDB(String SQL, ArrayList<Object> parameters) throws Exception
	{
		PreparedStatement prepareStatement = null;
		boolean deleteresult = true;
		try
		{
			logger.debug(SQL);
			if (adminDbConnection == null)
			{
				getConnection(1);
			}
			prepareStatement = adminDbConnection.prepareStatement(SQL);
			if (parameters != null && !parameters.isEmpty())
			{
				int parameterIndex = 1;
				for (Object lObjParameter : parameters)
				{
					if (lObjParameter instanceof String)
					{
						prepareStatement.setString(parameterIndex++, (String) lObjParameter);
					}
					else if (lObjParameter instanceof Integer)
					{
						prepareStatement.setInt(parameterIndex++, (Integer) lObjParameter);
					}
					else
					{
						prepareStatement.setObject(parameterIndex++, lObjParameter);
					}
				}
			}
			deleteresult = prepareStatement.execute(); //TODO Enable
		}
		catch (Exception ex1)
		{
			logger.error("Exception in execution of delete query on Admin Db ", ex1);
			throw ex1;
		}
		finally
		{
			if (prepareStatement != null)
			{
				prepareStatement.close();
			}
		}
		return deleteresult;
	}

	public static boolean deleteFromQuartzDb(String SQL, ArrayList<Object> parameters) throws Exception
	{
		PreparedStatement prepareStatement = null;
		boolean deleteresult = true;
		try
		{
			if (quartzDbConnection == null)
			{
				getConnection(3);
			}
			logger.debug(SQL);
			prepareStatement = quartzDbConnection.prepareStatement(SQL);
			if (parameters != null && !parameters.isEmpty())
			{
				int parameterIndex = 1;
				for (Object lObjParameter : parameters)
				{
					if (lObjParameter instanceof String)
					{
						prepareStatement.setString(parameterIndex++, (String) lObjParameter);
					}
					else if (lObjParameter instanceof Integer)
					{
						prepareStatement.setInt(parameterIndex++, (Integer) lObjParameter);
					}
					else
					{
						prepareStatement.setObject(parameterIndex++, lObjParameter);
					}
				}
			}
			deleteresult = prepareStatement.execute(); //TODO Enable
		}
		catch (Exception ex1)
		{
			logger.error("Exception in execution of delete query on quartzDb ", ex1);
			throw ex1;
		}
		finally
		{
			if (prepareStatement != null)
			{
				prepareStatement.close();
			}
		}
		return deleteresult;
	}

	/*public static void main(String args[])
	{
		try
		{
			DatabaseUtil.deleteSchemaAndTables("F598164F-50BB-45ED-8990-0565C6A5FE9D");
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}*/

	public static boolean deleteSchemaAndTables(String schema) throws Exception
	{
		boolean isDeleteSchema = true;
		DatabaseMetaData dbMetaData = null;
		ResultSet rs = null;
		//ResultSet schemaRs = null;
		try
		{
			ArrayList<String> lTablesList = new ArrayList<String>();
			if (dataDbConnection == null)
			{
				getConnection(2);
			}
			boolean isDBTypeOrcl = false;
			boolean isDBTypeRedShift = false;
			CleanUpConfigBean configBean = ConfigReader.getConfigBean();
			if (configBean != null && configBean.getDbConfigBean() != null && configBean.getDbConfigBean().getDatabaseType().equals("Oracle"))
			{
				isDBTypeOrcl = true;
			}
			else if (configBean != null && configBean.getDbConfigBean() != null && configBean.getDbConfigBean().getDatabaseType().equalsIgnoreCase("REDSHIFT"))
			{
				isDBTypeRedShift = true;
			}
			dbMetaData = dataDbConnection.getMetaData();
			if (doesSchemaExist(schema, isDBTypeOrcl))
			{
				if (isDBTypeOrcl || isDBTypeRedShift)
				{
					dropSchema(schema, isDBTypeOrcl);
				}
				else
				{
					rs = dbMetaData.getTables(null, schema, "%", null);
					while (rs.next())
					{
						lTablesList.add(schema + "." + rs.getString("TABLE_NAME"));
					}
					if (!lTablesList.isEmpty())
					{
						boolean deleteAll = dropTables(lTablesList);
						if (deleteAll)
						{
							dropSchema(schema, isDBTypeOrcl);
						}
					}
					else
					{
						dropSchema(schema, isDBTypeOrcl);
					}
				}
			}
			else
			{
				logger.debug("schema does not exist " + schema);
			}
		}
		catch (Exception e1)
		{
			logger.error("Exception in execution of delete schema ", e1);
			throw e1;
		}
		finally
		{
			if (rs != null)
			{
				rs.close();
			}
		}
		return isDeleteSchema;
	}

	private static boolean doesSchemaExist(String schemaName, boolean isDbTypeOrcl) throws Exception
	{
		boolean isSchemaExist = true;
		PreparedStatement prepareStatement = null;
		ResultSet rs = null;
		try
		{
			if (dataDbConnection == null)
			{
				getConnection(2);
			}
			if (isDbTypeOrcl)
			{
				prepareStatement = dataDbConnection.prepareStatement("SELECT * FROM DBA_TABLESPACES WHERE LOWER(TABLESPACE_NAME) = LOWER(?)");
			}
			else
			{
				prepareStatement = dataDbConnection.prepareStatement("SELECT * FROM INFORMATION_SCHEMA.SCHEMATA WHERE LOWER(SCHEMA_NAME) = LOWER(?)");
			}
			prepareStatement.setString(1, schemaName);
			rs = prepareStatement.executeQuery();
			if (!rs.next())
			{
				isSchemaExist = false;
			}
		}
		catch (Exception ex1)
		{
			logger.error("Exception in execution of doesSchemaExist ", ex1);
			throw ex1;
		}
		finally
		{
			if (rs != null)
			{
				rs.close();
			}
			if (prepareStatement != null)
			{
				prepareStatement.close();
			}
		}
		return isSchemaExist;
	}

	private static boolean dropSchema(String schemaName, boolean isDbTypeOrcl) throws Exception
	{
		boolean isDropSchema = true;
		PreparedStatement dropPreparedStatement = null;
		try
		{
			if (dataDbConnection == null)
			{
				getConnection(2);
			}
			String dropStatement = "";
			if (isDbTypeOrcl)
			{
				dropStatement = "DROP TABLESPACE " + schemaName + " INCLUDING CONTENTS AND DATAFILES CASCADE CONSTRAINTS";
			}
			else
			{
				dropStatement = "DROP SCHEMA " + schemaName;
				if (dataDbConnection instanceof Jdbc3gConnection || dataDbConnection instanceof Jdbc4Connection)
				{
					dropStatement = dropStatement + " CASCADE";
				}				
			}
			logger.debug(dropStatement);
			dropPreparedStatement = dataDbConnection.prepareStatement(dropStatement);
			dropPreparedStatement.execute(); //TODO Enable	
			try
			{
				if (dropPreparedStatement!=null)
				{
					dropPreparedStatement.close();
				}
			}
			catch(Exception e1)
			{
				logger.error("Exception in closing preparedStatement ",e1);
			}
			if (isDbTypeOrcl)
			{
				dropStatement = "DROP USER " + schemaName + " CASCADE";
				logger.debug(dropStatement);
				dropPreparedStatement = dataDbConnection.prepareStatement(dropStatement);
				dropPreparedStatement.execute(); //TODO Enable
			}
		}
		catch (Exception e1)
		{
			logger.error("Exception in execution of delete Tables ", e1);
			isDropSchema = false;
			throw e1;
		}
		finally
		{
			if (dropPreparedStatement != null)
			{
				dropPreparedStatement.close();
			}
		}
		return isDropSchema;
	}

	private static boolean dropTables(ArrayList<String> lTablesList) throws Exception
	{
		boolean isDeleteAllTables = true;
		try
		{
			PreparedStatement dropPreparedStatement = null;
			if (dataDbConnection == null)
			{
				getConnection(2);
			}
			for (String tableName : lTablesList)
			{
				String dropStatement = "DROP TABLE " + tableName;
				logger.debug(dropStatement);
				try
				{
					dropPreparedStatement = dataDbConnection.prepareStatement(dropStatement);
					dropPreparedStatement.execute(); //TODO Enable					
				}
				catch (Exception e2)
				{
					logger.debug("Exception in deleting table " + tableName + " " + e2);
					throw e2;
				}
				finally
				{
					if (dropPreparedStatement != null)
					{
						dropPreparedStatement.close();
					}
				}
			}
		}
		catch (Exception e1)
		{
			logger.error("Exception in execution of delete Tables ", e1);
			throw e1;
		}
		return isDeleteAllTables;
	}

	public static void clearTransactLogs(int dbConnType, String dbName) throws Exception
	{
		PreparedStatement prepStatement = null;
		try
		{
			Connection dbConnectionLogs = null;
			if (dbConnType == 2)
			{
				dbConnectionLogs = dataDbConnection;
			}
			else
			{
				if (dbConnType == 1)
					dbConnectionLogs = adminDbConnection;
				else
					dbConnectionLogs = quartzDbConnection;
			}
			String shrinkFileSQL = "dbcc shrinkfile (N'" + dbName + "' , 1)";
			logger.debug(shrinkFileSQL);
			prepStatement = dbConnectionLogs.prepareStatement(shrinkFileSQL);
			prepStatement.execute();
		}
		catch (Exception ex1)
		{
			logger.debug("Exception in clearTransactions ", ex1);
			throw ex1;
		}
		finally
		{
			if (prepStatement != null)
			{
				prepStatement.close();
			}
		}
	}
}
