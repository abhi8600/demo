package com.birst.spacecleanup.util;

import java.io.File;

import org.apache.log4j.Logger;

import com.birst.spacecleanup.beans.CleanUpConfigBean;

public class FileUtil
{
	private static Logger logger = Logger.getLogger(FileUtil.class);

	public static boolean deleteSpaceDir(String spaceId) throws Exception
	{
		boolean isDeleteSpaceDir = true;
		try
		{
			CleanUpConfigBean configBean = ConfigReader.getConfigBean();
			String spaceDirPath = configBean.getBirstDataPath() + File.separator + spaceId.toLowerCase();
			logger.debug("rmdir /S /Q " + spaceDirPath);
			delete(new File(spaceDirPath));
		}
		catch (Exception ex)
		{
			logger.error("Exception in deleteSpaceDir ", ex);
			throw ex;
		}
		return isDeleteSpaceDir;
	}

	public static boolean deleteMongoCacheDir(String spaceId) throws Exception
	{
		boolean isDeleteMongoCache = true;
		try
		{
			CleanUpConfigBean configBean = ConfigReader.getConfigBean();
			String mongoDbFilePath = configBean.getMongoDBCachePath() + File.separator + "birst-" +spaceId.toLowerCase();
			logger.debug("rmdir /S /Q " + mongoDbFilePath);
			delete(new File(mongoDbFilePath));
		}
		catch (Exception ex)
		{
			logger.error("Exception in deleteSpaceDir ", ex);
			throw ex;
		}
		return isDeleteMongoCache;
	}

	public static void delete(File file) throws Exception
	{
		try
		{
			if (file.exists())
			{
				logger.debug("Delete " + file.getAbsolutePath());
				if (file.isDirectory())
				{
					File[] files = file.listFiles();
					for (int i = 0; i < files.length; i++)
					{
						delete(files[i]);
					}
					file.delete();
				}
				else
				{
					file.delete(); 
				}
			}
			else
			{
				logger.debug("File not found : " + file.getAbsolutePath());
			}
		}
		catch (Exception ex)
		{
			logger.error("Exception in delete ", ex);
			throw ex;
		}
	}
}
