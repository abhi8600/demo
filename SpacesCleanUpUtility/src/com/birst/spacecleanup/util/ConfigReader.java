package com.birst.spacecleanup.util;

import java.io.FileInputStream;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.birst.spacecleanup.beans.CleanUpConfigBean;

public class ConfigReader
{
	private static Logger logger = Logger.getLogger(ConfigReader.class);
	private static CleanUpConfigBean cleanUpConfigBean;

	public static CleanUpConfigBean getConfigBean() throws Exception
	{
		if (cleanUpConfigBean == null)
		{
			logger.debug("Initializing Configuration");
			cleanUpConfigBean = new CleanUpConfigBean();
			String filePathForConfig = "config/SpaceCleanupSettings.config";
			Properties props = new Properties();
			props.load(new FileInputStream(filePathForConfig));

			cleanUpConfigBean.setBirstDataPath(props.getProperty("BIRST_DATA_PATH"));
			cleanUpConfigBean.setMongoDBCachePath(props.getProperty("MONGO_CACHE_PATH"));
			cleanUpConfigBean.setAdminDbType(props.getProperty("ADMINDBTYPE"));
			cleanUpConfigBean.setAdminConnectionUrlMSSQL(props.getProperty("ADMINCONNECTION_URL_MSSQL"));
			cleanUpConfigBean.setAdminConnectionUrlOrcl(props.getProperty("ADMINCONNECTION_URL_ORCL"));
			cleanUpConfigBean.setQuartzUrlMSSQL(props.getProperty("QUARTZ_URL_MSSQL"));
			cleanUpConfigBean.setQuartzUrlOrcl(props.getProperty("QUARTZ_URL_ORCL"));
			cleanUpConfigBean.setAdminUser(props.getProperty("ADMINUSER"));
			cleanUpConfigBean.setAdminPwd(props.getProperty("ADMINPWD"));
			cleanUpConfigBean.setQuartzUser(props.getProperty("QUARTZUSER"));
			cleanUpConfigBean.setQuartzPwd(props.getProperty("QUARTZPWD"));
			cleanUpConfigBean.setIsPADBRun(props.getProperty("ISPADBRUN"));
			cleanUpConfigBean.setIsDataDbMSSQL(props.getProperty("ISDATADBMSSQL"));
			cleanUpConfigBean.setmSSQLDataDbName(props.getProperty("MSSQLDATADBNAME"));
			cleanUpConfigBean.setRedShiftJdbcUrl(props.getProperty("REDSHIFT_URL"));
			cleanUpConfigBean.setRedShiftDriver(props.getProperty("REDSHIFT_DRIVER"));
			cleanUpConfigBean.setRedShiftUsername(props.getProperty("REDSHIFT_USERNAME"));
			cleanUpConfigBean.setRedShiftPassword(props.getProperty("REDSHIFT_PWD"));
			cleanUpConfigBean.setAdminDbName(props.getProperty("ADMIN_DBNAME"));
			cleanUpConfigBean.setQuartzDbName(props.getProperty("QUARTZ_DBNAME"));
			cleanUpConfigBean.setRedShiftPassword(props.getProperty("REDSHIFT_PWD"));
			if (props.getProperty("CLEAN_DELETED")!=null)
				cleanUpConfigBean.setCleanDeleted(Boolean.parseBoolean(props.getProperty("CLEAN_DELETED")));
		}
		return cleanUpConfigBean;
	}
}