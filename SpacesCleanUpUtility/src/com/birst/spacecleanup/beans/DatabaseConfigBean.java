package com.birst.spacecleanup.beans;

import com.birst.spacecleanup.util.EncryptionService;

public class DatabaseConfigBean
{
	private String connectionString;
	private String databaseDriver;
	private String databaseType;
	private String adminUser;
	private String adminPwd;
	private String schemaName;

	public String getConnectionString()
	{
		return connectionString;
	}

	public void setConnectionString(String connectionString)
	{
		this.connectionString = connectionString;
	}

	public String getDatabaseDriver()
	{
		return databaseDriver;
	}

	public void setDatabaseDriver(String databaseDriver)
	{
		this.databaseDriver = databaseDriver;
	}

	public String getDatabaseType()
	{
		return databaseType;
	}

	public void setDatabaseType(String databaseType)
	{
		this.databaseType = databaseType;
	}

	public String getAdminUser()
	{
		return adminUser;
	}

	public void setAdminUser(String adminUser)
	{
		this.adminUser = adminUser;
	}

	public String getAdminPwd()
	{
		return EncryptionService.getInstance().decrypt(adminPwd);
	}

	public void setAdminPwd(String adminPwd)
	{
		this.adminPwd = adminPwd;
	}

	public String getSchemaName()
	{
		return schemaName;
	}

	public void setSchemaName(String schemaName)
	{
		this.schemaName = schemaName;
	}

}
