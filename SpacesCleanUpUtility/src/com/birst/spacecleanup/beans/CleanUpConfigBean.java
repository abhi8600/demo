package com.birst.spacecleanup.beans;

public class CleanUpConfigBean
{
	private String birstDataPath;
	private String mongoDBCachePath;
	private String adminDbType;
	private String adminConnectionUrlMSSQL;
	private String adminConnectionUrlOrcl;
	private String quartzUrlMSSQL;
	private String quartzUrlOrcl;
	private DatabaseConfigBean dbConfigBean;
	private String adminUser;
	private String adminPwd;
	private String quartzUser;
	private String quartzPwd;
	private String isPADBRun;
	private String isDataDbMSSQL;
	private String mSSQLDataDbName;
	private String redShiftJdbcUrl;
	private String redShiftDriver;
	private String redShiftUsername;
	private String redShiftPassword;
	private boolean cleanDeleted;
	private String adminDbName;
	private String quartzDbName;
	
	public String getRedShiftJdbcUrl() {
		return redShiftJdbcUrl;
	}

	public void setRedShiftJdbcUrl(String redShiftJdbcUrl) {
		this.redShiftJdbcUrl = redShiftJdbcUrl;
	}

	public String getRedShiftDriver() {
		return redShiftDriver;
	}

	public void setRedShiftDriver(String redShiftDriver) {
		this.redShiftDriver = redShiftDriver;
	}

	public String getRedShiftUsername() {
		return redShiftUsername;
	}

	public void setRedShiftUsername(String redShiftUsername) {
		this.redShiftUsername = redShiftUsername;
	}

	public String getRedShiftPassword() {
		return redShiftPassword;
	}

	public void setRedShiftPassword(String redShiftPassword) {
		this.redShiftPassword = redShiftPassword;
	}

	public String getIsDataDbMSSQL() {
		return isDataDbMSSQL;
	}

	public void setIsDataDbMSSQL(String isDataDbMSSQL) {
		this.isDataDbMSSQL = isDataDbMSSQL;
	}

	public String getmSSQLDataDbName() {
		return mSSQLDataDbName;
	}

	public void setmSSQLDataDbName(String mSSQLDataDbName) {
		this.mSSQLDataDbName = mSSQLDataDbName;
	}

	public String getBirstDataPath()
	{
		return birstDataPath;
	}

	public void setBirstDataPath(String birstDataPath)
	{
		this.birstDataPath = birstDataPath;
	}

	public String getMongoDBCachePath()
	{
		return mongoDBCachePath;
	}

	public void setMongoDBCachePath(String mongoDBCachePath)
	{
		this.mongoDBCachePath = mongoDBCachePath;
	}

	public String getAdminDbType()
	{
		return adminDbType;
	}

	public void setAdminDbType(String adminDbType)
	{
		this.adminDbType = adminDbType;
	}

	public String getAdminConnectionUrlMSSQL()
	{
		return adminConnectionUrlMSSQL;
	}

	public void setAdminConnectionUrlMSSQL(String adminConnectionUrlMSSQL)
	{
		this.adminConnectionUrlMSSQL = adminConnectionUrlMSSQL;
	}

	public String getAdminConnectionUrlOrcl()
	{
		return adminConnectionUrlOrcl;
	}

	public void setAdminConnectionUrlOrcl(String adminConnectionUrlOrcl)
	{
		this.adminConnectionUrlOrcl = adminConnectionUrlOrcl;
	}

	public String getQuartzUrlMSSQL()
	{
		return quartzUrlMSSQL;
	}

	public void setQuartzUrlMSSQL(String quartzUrlMSSQL)
	{
		this.quartzUrlMSSQL = quartzUrlMSSQL;
	}

	public String getQuartzUrlOrcl()
	{
		return quartzUrlOrcl;
	}

	public void setQuartzUrlOrcl(String quartzUrlOrcl)
	{
		this.quartzUrlOrcl = quartzUrlOrcl;
	}

	public DatabaseConfigBean getDbConfigBean()
	{
		return dbConfigBean;
	}

	public void setDbConfigBean(DatabaseConfigBean dbConfigBean)
	{
		this.dbConfigBean = dbConfigBean;
	}

	public String getAdminUser()
	{
		return adminUser;
	}

	public void setAdminUser(String adminUser)
	{
		this.adminUser = adminUser;
	}

	public String getAdminPwd()
	{
		return adminPwd;
	}

	public void setAdminPwd(String adminPwd)
	{
		this.adminPwd = adminPwd;
	}

	public String getQuartzUser()
	{
		return quartzUser;
	}

	public void setQuartzUser(String quartzUser)
	{
		this.quartzUser = quartzUser;
	}

	public String getQuartzPwd()
	{
		return quartzPwd;
	}

	public void setQuartzPwd(String quartzPwd)
	{
		this.quartzPwd = quartzPwd;
	}

	public String getIsPADBRun()
	{
		return isPADBRun;
	}

	public void setIsPADBRun(String isPADBRun)
	{
		this.isPADBRun = isPADBRun;
	}

	public boolean isCleanDeleted() {
		return cleanDeleted;
	}

	public void setCleanDeleted(boolean cleanDeleted) {
		this.cleanDeleted = cleanDeleted;
	}

	public String getAdminDbName() {
		return adminDbName;
	}

	public void setAdminDbName(String adminDbName) {
		this.adminDbName = adminDbName;
	}

	public String getQuartzDbName() {
		return quartzDbName;
	}

	public void setQuartzDbName(String quartzDbName) {
		this.quartzDbName = quartzDbName;
	}

}