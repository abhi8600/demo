package com.birst.spacecleanup;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.birst.spacecleanup.beans.CleanUpConfigBean;
import com.birst.spacecleanup.beans.DatabaseConfigBean;
import com.birst.spacecleanup.util.ConfigReader;
import com.birst.spacecleanup.util.DatabaseUtil;
import com.birst.spacecleanup.util.FileUtil;

public class SpaceCleanupMain
{
	private static Logger logger = Logger.getLogger(SpaceCleanupMain.class);
	private static int noOfDaysToClean;
	private static String birstUserName;
	private static CleanUpConfigBean configBeanMain = null;

	public SpaceCleanupMain()
	{

	}

	public static void main(String[] args)
	{
		try
		{
			noOfDaysToClean = 10;
			if (args.length < 1 || args.length > 2)
			{
				printHelpMsg();
				System.exit(0);
			}
			else if (args.length == 2)
			{
				noOfDaysToClean = Integer.parseInt(args[1]);
				if (noOfDaysToClean < 1)
				{
					noOfDaysToClean = 5;
					System.out.println("Number of days to clean must be 1 or greater. Default value of 5 is being used.");
					logger.debug("Number of days to clean must be 1 or greater. Default value of 5 is being used.");
				}
			}
			birstUserName = args[0];

			removeSpaces();
		}
		catch (Exception e)
		{
			logger.error("Exception in SpaceCleanupMain ", e);
		}
	}

	private static void removeSpaces()
	{
		try
		{
			configBeanMain = ConfigReader.getConfigBean();
			boolean cleanUpDeleted = configBeanMain.isCleanDeleted();
			StringBuilder sqlQuery = new StringBuilder();
			
			if (cleanUpDeleted)
			{
				if (("Oracle").equalsIgnoreCase(configBeanMain.getAdminDbType()))
				{
					sqlQuery.append("SELECT ID,NAME,CONNECTSTRING,DATABASEDRIVER,DATABASETYPE,ADMINUSER,ADMINPWD,SCHEMA FROM DBO.SPACES WHERE DELETED=1");
				}
				else
				{
					sqlQuery.append("SELECT ID,NAME,CONNECTSTRING,DATABASEDRIVER,DATABASETYPE,ADMINUSER,ADMINPWD,[SCHEMA] FROM DBO.SPACES WHERE DELETED=1");
				}				
			}
			else
			{
				if (("Oracle").equalsIgnoreCase(configBeanMain.getAdminDbType()))
				{
					sqlQuery.append("SELECT ID,NAME,CONNECTSTRING,DATABASEDRIVER,DATABASETYPE,ADMINUSER,ADMINPWD,SCHEMA FROM DBO.SPACES WHERE ID IN(\n");
				}
				else
				{
					sqlQuery.append("SELECT ID,NAME,CONNECTSTRING,DATABASEDRIVER,DATABASETYPE,ADMINUSER,ADMINPWD,[SCHEMA] FROM DBO.SPACES WHERE ID IN(\n");
				}
				sqlQuery.append("SELECT SPACE_ID FROM DBO.USER_SPACE_INXN\n");
				sqlQuery.append("INNER JOIN DBO.USERS ON DBO.USER_SPACE_INXN.USER_ID=USERS.PKID\n");
				sqlQuery.append("WHERE USERS.USERNAME=?\n");
				sqlQuery.append("AND USER_SPACE_INXN.OWNER=1\n");
	
				if (("false").equalsIgnoreCase(configBeanMain.getIsPADBRun()))
				{
					if (("Oracle").equalsIgnoreCase(configBeanMain.getAdminDbType()))
					{
						sqlQuery.append("AND (TRUNC(SYSTIMESTAMP)-TRUNC(STARTDATE))>?)\n");
					}
					else
					{
						sqlQuery.append("AND DATEDIFF(DAY,USER_SPACE_INXN.STARTDATE,SYSDATETIME()) > ?)\n");
					}
				}
				else
				{
					sqlQuery.append(")");
				}
			}

			ArrayList<Object> parameterList = new ArrayList<Object>();
			if (!cleanUpDeleted)
			{
				parameterList.add(birstUserName);
				if (("false").equalsIgnoreCase(configBeanMain.getIsPADBRun()))
				{
					parameterList.add(noOfDaysToClean);
				}
			}

			ArrayList<HashMap<String, Object>> lRecordList = DatabaseUtil.getQueryResult(sqlQuery.toString(), parameterList);

			if (lRecordList != null && !lRecordList.isEmpty())
			{
				int spaceDeletedCount = 0;
				for (HashMap<String, Object> lMapResult : lRecordList)
				{
					String spaceId = (String) lMapResult.get("ID");
					String spaceName = (String) lMapResult.get("NAME");
					String schema = (String) lMapResult.get("SCHEMA");
					try
					{
						logger.debug("Begin Delete " + spaceName + "~" + spaceId);
						initConfig(lMapResult);
						boolean isDropDbSchema = DatabaseUtil.deleteSchemaAndTables(schema);
						if (isDropDbSchema)
						{
							clearQuartzTables(spaceId);
							clearAdminDbTables(spaceId);
						}
						FileUtil.deleteSpaceDir(spaceId);
						FileUtil.deleteMongoCacheDir(spaceId);
						logger.debug("End Delete " + spaceName);
						spaceDeletedCount++;
					}
					catch (Exception ex1)
					{
						logger.error("Unable to complete delete operation for " + spaceName);
						continue;
					}
				}
				logger.debug("Total spaces deleted " + spaceDeletedCount);
				DatabaseUtil.commitAndCloseConnections(true);
			}
			else
			{
				logger.debug("No spaces for deletion ");
			}

		}
		catch (Exception e1)
		{
			logger.error("Exception in removeSpaces ", e1);
			try
			{
				DatabaseUtil.commitAndCloseConnections(false);
			}
			catch (Exception e2)
			{
				logger.error("Exception in close/commit action " + e2);
			}
		}
	}
	
	private static void initConfig(HashMap<String, Object> mapSpaceInfo) throws Exception
	{
		DatabaseConfigBean dbConfigBean = new DatabaseConfigBean();
		String connectionString = (String) mapSpaceInfo.get("CONNECTSTRING");
		String databaseDriver = (String) mapSpaceInfo.get("DATABASEDRIVER");
		String userName = (String) mapSpaceInfo.get("ADMINUSER");
		String dbPassword = (String) mapSpaceInfo.get("ADMINPWD");
		String schemaName = (String) mapSpaceInfo.get("SCHEMA");
		String databaseType = (String) mapSpaceInfo.get("DATABASETYPE");
		
		dbConfigBean.setDatabaseType(databaseType);
		dbConfigBean.setSchemaName(schemaName);
		
		if (databaseType.equalsIgnoreCase("REDSHIFT"))
		{
			dbConfigBean.setConnectionString(configBeanMain.getRedShiftJdbcUrl());
			dbConfigBean.setDatabaseDriver(configBeanMain.getRedShiftDriver());
			dbConfigBean.setAdminUser(configBeanMain.getRedShiftUsername());
			dbConfigBean.setAdminPwd(configBeanMain.getRedShiftPassword());	
		}
		else
		{
			dbConfigBean.setConnectionString(connectionString);
			dbConfigBean.setDatabaseDriver(databaseDriver);
			dbConfigBean.setAdminUser(userName);
			dbConfigBean.setAdminPwd(dbPassword);			
		}
		configBeanMain.setDbConfigBean(dbConfigBean);
	}

	private static void clearQuartzTables(String spaceId) throws Exception
	{
		ArrayList<Object> parameterList = new ArrayList<Object>();
		parameterList.add(spaceId);
		String custJobHis = "DELETE FROM DBO.CUST_JOB_HISTORY WHERE JOB_ID IN (SELECT JOB_ID FROM DBO.CUST_SPACE_STATUS WHERE SPACE_ID=?)";
		DatabaseUtil.deleteFromQuartzDb(custJobHis, parameterList);
		String custScdInfo = "DELETE FROM DBO.CUST_SCHEDULE_INFO WHERE JOB_ID IN (SELECT JOB_ID FROM DBO.CUST_SPACE_STATUS WHERE SPACE_ID=?)";
		DatabaseUtil.deleteFromQuartzDb(custScdInfo, parameterList);
		String custBdsInfo = "DELETE FROM DBO.CUST_BDS_INFO WHERE SPACE_ID=?";
		DatabaseUtil.deleteFromQuartzDb(custBdsInfo, parameterList);
		String custConnInfo = "DELETE FROM DBO.CUST_CONNECTORS_INFO WHERE SPACE_ID=?";
		DatabaseUtil.deleteFromQuartzDb(custConnInfo, parameterList);
		String custDelInfo = "DELETE FROM DBO.CUST_DELETE_INFO WHERE SPACE_ID=?";
		DatabaseUtil.deleteFromQuartzDb(custDelInfo, parameterList);
		String custProcessInfo = "DELETE FROM DBO.CUST_PROCESS_INFO WHERE SPACE_ID=?";
		DatabaseUtil.deleteFromQuartzDb(custProcessInfo, parameterList);
		String custScdReports = "DELETE FROM DBO.CUST_SCHEDULED_REPORTS WHERE SPACE_ID=?";
		DatabaseUtil.deleteFromQuartzDb(custScdReports, parameterList);
		String custSpaceStatus = "DELETE FROM DBO.CUST_SPACE_STATUS WHERE SPACE_ID=?";
		DatabaseUtil.deleteFromQuartzDb(custSpaceStatus, parameterList);
	}

	private static void clearAdminDbTables(String spaceId) throws Exception
	{
		ArrayList<Object> parameterList = new ArrayList<Object>();
		parameterList.add(spaceId);
		String spcGrpDropSQL = "DELETE FROM DBO.SPACE_GROUP_INXN WHERE SPACE_ID=?";
		DatabaseUtil.deleteFromAdminDB(spcGrpDropSQL, parameterList);
		String usrSpcDropSQL = "DELETE FROM DBO.USER_SPACE_INXN WHERE SPACE_ID=?";
		DatabaseUtil.deleteFromAdminDB(usrSpcDropSQL, parameterList);
		String spaceDropSQL = "DELETE FROM DBO.SPACES WHERE ID=?";
		DatabaseUtil.deleteFromAdminDB(spaceDropSQL, parameterList);
	}

	private static void printHelpMsg() throws Exception
	{
		StringBuffer helpMsg = new StringBuffer("");
		helpMsg.append("Birst Cleanup Help Utility\n");
		helpMsg.append("Usage: SpaceCleanupMain username [daysToCleanUp]\n");
		helpMsg.append("username: birst user from whose account you want to clean up spaces \n");
		helpMsg.append("[daysToCleanUp] : (optional) number of days preceding which spaces will be deleted. Default is 10 \n");
		helpMsg.append("E.g. SpaceCleanupMain atc@birst.com 15\n");
		helpMsg.append("If CLEAN_DELETED parameter is true in config file, all spaces from Admin DB will be deleted. Parameters will not be considered in this case.");
		System.out.println(helpMsg);
	}
}