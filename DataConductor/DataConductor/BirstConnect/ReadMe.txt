-------------------------------
Birst Connect Service ReadMe.txt
-------------------------------

--------
Overview
--------

Birst Connect Service is the Windows Service version of Birst Connect that enables 
running of Birst Connect in a non-interactive mode as a Service. 

This is the recommended way to run Birst Connect for Live Access connections. This 
enables Live Access connections to remain active without requiring an active user 
session since it runs as a Windows Service on the computer (which runs even if no user 
is logged into it).

Birst does not recommend running Birst Connect as a service when the requirement is to 
run scheduled tasks defined in Birst Connect. To schedule Birst Connect tasks, Birst 
recommends creating a batch script to run Birst Connect then using a native scheduler 
(rather than the Birst Connect Schedule tab) to schedule the batch script. Example 
batch scripts are provided in the Birst Online Help.

It is possible to install and run multiple Birst Connect Services on the same client 
for a single or multiple spaces.

----------------
Package Contents
----------------

Birst Connect Service contains BirstConnect.zip which has following components in it:
- BirstConnect.exe
- BirstConnect.xml
- log4j.xml
- log4j.properties
- BirstConnect.jnlp (sample jnlp file)
- ReadMe.txt
- logs folder (empty)
- dist folder containing DataConductor.jar and lib folder containing all dependent jars

--------------------------------------------
Installing and Running Birst Connect Service
--------------------------------------------

1. Create a new folder (eg. C:\BirstConnect) and extract BirstConnect.zip into it.
   We shall refer to this folder as HOME directory in the rest of configuration.

2. Open BirstConenct.xml in text editor and modify the properties as follows to
   suit to your environment:
   
   - id and name : no need to change if only need to run a Single instance of
   BirstConnect Service. If running multiple instances, the id and name of each 
   instance must be unique in order to install the service.
   
   - description : change it to suit to your requirement. This will appear as 
   service description.
   
   - logpath : This is the path where Service Wrapper, StandardOutput and StandardError 
   logs will go. You can specify this path as HOME\logs (ie. C:\BirstConnect\logs as 
   per our example)
   
   - logmode : Do not change this.
   
   - env name="CLASSPATH" : Do NOT change this parameter or the service may not run.
   
   - env name="JAVA_HOME" : set the 'value' of this parameter to your local java 
   installation (ie. path to jdk/jre). Note that as of Birst 5.1, Java 7 is required.
   
   - executable : Do not change this.
   
   - <argument>-Xmx1024m</argument> : Change this parameter in order to specify the 
   	amount of memory BirstConnect service should use.
   
   - <argument>-Xrs</argument> : Do not change this.
   
   - <argument>-cp %CLASSPATH%</argument> : Do not change this.
   
   - env name="BirstConnect_Home" : This value should be same as HOME (ie. C:\BirstConnect)
   
   - <argument>-Djnlp.file="%BirstConnect_Home%\BirstConnect.jnlp"</argument> : Launch the
   BirstConnect from Admin screen (for the configuration for which service is to be 
   installed) and 'save' the <spacename>.jnlp file to HOME directory and then only replace 
   'BirstConnect' in above argument to <spacename>. This parameter will enable BirstConnect 
   to read the properties from the jnlp file and launch BirstConnect using the same.
   
   - <argument>-Djnlp.log4j.Override="%BirstConnect_Home%\log4j.xml"</argument> : do 
   not modify this parameter unless you would like to use custom log4j.xml or 
   log4j.properties other than supplied by Birst for logging purposes.
   
   - <argument>-DBirstConnect.logs.path=%BirstConnect_Home%\logs\</argument> : this 
   parameter is consumed by log4j.xml (or log4j.properties) supplied by Birst in order 
   to specify the path where the daily log file should be generated. This path is 
   usually same as logpath where the other logs go.
   
   - <argument>-Djnlp.headless=true</argument> : This argument is mandatory and should 
   not be modified when running BirstConnect as Service. If you change the value of 
   this parameter, BirstConnect service may become unresponsive.
   
   - <argument>com.birst.dataconductor.DataConductorApp</argument> : Do not change this.
    
3. Now you are ready to install Birst Connect Service.
   - Open command prompt and change the directory to HOME.
   - Type BirstConnect install on command prompt to install BirstConnect Service.
   	If this command does not show any errors on console, the Birst Connect Service 
   	is installed successfully. You can now see this in list of services.

4. There are multiple ways to start the installed Birst Connect Service:
   i  	On command prompt navigate to HOME and type BirstConnect start
   ii 	On command prompt type net start <serviceid>
   iii	From services list click on the service and click on Start
   
   You should see the logfiles generated inside the HOME\logs (or where the logpath and 
   -DBirstConnect.logs.path are set to divert logs).
   
------------------------------------------
Stopping and Removing Birst Connect Service
------------------------------------------

- To Stop the running service, on command prompt navigate to HOME and type BirstConnect stop
OR type net stop <serviceid> OR From services list click on the service and click on Stop

- To Remove installed service, open command prompt and change the directory to HOME and 
  type BirstConnect uninstall.
  
Note: Do not delete files from HOME when BirstConnect service is installed and/or running. 
This might make the Service orphan and it may not be stopped and/or removed from machine.

Note: Whenever any change is made to BirstConnect tasks/liveaccess connection configuration
using BirstConnect UI, the installed service must be restarted.

Note: Whenever any change is required in BirstConnect.xml, first stop and uninstall the 
service if it is already configured. Then make changes to BirstConnect.xml and then 
reinstall and restart the service to take effect of the changes.

For more help, please contact the Birst Support team.
