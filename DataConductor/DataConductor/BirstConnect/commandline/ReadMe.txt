---------------------------------------------------------
Launch BirstConnect UI from CommandLine (Java) ReadMe.txt
---------------------------------------------------------

--------
Overview
--------

Running BirtsConnect from commandline (using Java and not using Java Web Start)
allows you to use BirstConnect UI interactively.

This is required in cases where users need to use features of BirstConnect that 
require third party resources that are not distributed through BirstConnect due 
to licensing requirements. When running BirstConnect via Java Web Start, the 
application runs within a security sandbox and must use any resources required 
from that server only.

Using BirstConnect from the commandline (via Java) enables users to use the 
third party resources that are missing in BirstConnect from their own machine.
For this, all one needs to do is to drop the thirdparty resources in the dist\lib
folder and set the classpath for running BirstConnect to use these resources.

-------------------------------------------------
Steps for running BirstConnect UI via commandline:
-------------------------------------------------

1. Create a new folder (eg. C:\BirstConnect) and extract BirstConnect.zip into it.
   We shall refer to this folder as the HOME directory for the following steps.
   
2. Open the cmdUI.bat file from the \commandline folder in a text editor and 
   update the following settings per your environment:
   
   i. set JAVA_HOME to the path of the jdk or jre on your machine 
   (e.g. JAVA_HOME=C:\Program Files\Java\jdk1.7.0). You can choose 32 bit or 64 
   bit JVM provided that you have the corresponding third party resources (dll).
   Note that as of Birst 5.1, Java 7 is required. 
   
   ii. set BirstConnect_Home to the path of the folder to which you extracted 
   the content of BirstConnect.zip (e.g. BirstConnect_Home=C:\BirstConnect). 

   iii. launch BirstConnect from the Birst admin screen and save your <spaceid>.jnlp 
   file to the HOME directory. 

   iv. -Djnlp.file="%BirstConnect_Home%\BirstConnect.jnlp"
   Replace 'BirstConnect' in the above argument with the <spaceid>. This parameter 
   will enable BirstConnect to read the properties from the jnlp file and launch 
   BirstConnect using the same.
   
   v. In order to change the amount of Memory BirstConnect should use, specify
   the amount of memory to -Xmx parameter. 
   
3. Save the required third party resource files to the \dist\lib folder.
    
4. To launch the BirstConnect UI, open a command prompt and navigate to HOME\commandline. 
   Run cmdUI.bat. This will launch the BirstConnect UI and use the command prompt as the console.

   
For more help, please contact Birst Support