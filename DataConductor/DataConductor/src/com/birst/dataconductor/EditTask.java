/*
 * EditTask.java
 *
 * Created on November 3, 2008, 6:36 PM
 */
package com.birst.dataconductor;

import com.birst.dataconductor.sap.SAPRemoteFunction;
import com.birst.dataconductor.sap.SAPRemoteFunctionDetails;
import com.birst.dataconductor.sap.SAPRemoteQuery;
import com.birst.dataconductor.sap.SAPRemoteQueryDetails;
import com.birst.dataconductor.sap.SAPSystem;
import java.awt.Frame;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import org.jdesktop.application.Action;
import java.util.HashMap;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.DefaultListCellRenderer;
import java.awt.Component;
import javax.swing.JLabel;
import javax.swing.JList;
import java.net.URL;
import javax.swing.ImageIcon;

/**
 *
 * @author  bpeters
 */
public class EditTask extends javax.swing.JDialog
{

    private Task task = null;
    private Frame parent = null;
    private List<Object> sourceList = new ArrayList<Object>();
    private SAPRemoteFunction srf = null;
    private SAPRemoteFunction clonedsrf = null;
    private SAPRemoteQuery srq = null;
    private SAPRemoteQuery clonedsrq = null;
    private DataConductorConfig config = null;
    private SSHTunnel sst = null;
    private SSHTunnel clonedSSHTunnel = null;
    // save away last viewed/set fields in connection dialog to ease creating new ones
    private final static QuerySource lastQS = new QuerySource();
    static 
    {
        lastQS.setGenericDriver(false);
        lastQS.setType(0);
        lastQS.setDriverName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
        lastQS.setCatalogName("");
        lastQS.setSchemaName("");
        lastQS.setServerName("localhost");
        lastQS.setConnectString("");
        lastQS.setTableName("");
        lastQS.setDatabaseName("");
        lastQS.setPort(0);
        lastQS.setUsername("");
        lastQS.putPassword("");
        lastQS.getSettings().setReplacements("");
        lastQS.getSettings().setIgnoreInteralQuotes(false);
        lastQS.getSettings().setIgnoreCarriageReturn(false);
        lastQS.getSettings().setQuote("\"");
        lastQS.setBucketName("");
        lastQS.setBucketKey("");
        lastQS.setCrendentialsFileName("");
        lastQS.setUrl("");
        lastQS.setIgnoreBadServerCertificates(false);
        lastQS.setMethod("GET");
    }
    
    public static void saveQuerySourceState(QuerySource qs)
    {
        lastQS.setGenericDriver(qs.isGenericDriver());
        lastQS.setType(qs.getType());
        lastQS.setDriverName(qs.getDriverName());
        lastQS.setTableName(qs.getTableName());
        lastQS.setCatalogName(qs.getCatalogName());
        lastQS.setSchemaName(qs.getSchemaName());
        lastQS.setConnectString(qs.getConnectString());
        lastQS.setServerName(qs.getServerName());
        lastQS.setDatabaseName(qs.getFullDatabaseName());
        lastQS.setPort(qs.getPort());
        lastQS.setUsername(qs.getUsername());
        lastQS.putPassword(qs.retrievePassword());
        lastQS.getSettings().setReplacements(qs.getSettings().getReplacements());
        lastQS.getSettings().setIgnoreInteralQuotes(qs.getSettings().isIgnoreInteralQuotes());
        lastQS.getSettings().setIgnoreCarriageReturn(qs.getSettings().isIgnoreCarriageReturn());
        lastQS.getSettings().setQuote(qs.getSettings().getQuote());
        lastQS.setBucketName(qs.getBucketName());
        lastQS.setBucketKey(qs.getBucketKey());
        lastQS.setCrendentialsFileName(qs.getCrendentialsFileName());
        lastQS.setSshTunnel(qs.getSshTunnel());
        lastQS.setUrl(qs.getUrl());
        lastQS.setIgnoreBadServerCertificates(qs.getIgnoreBadServerCertificates());
        lastQS.setMethod(qs.getMethod());
    }

    public Task getTask()
    {
        return task;
    }

    public void setTask(Task task)
    {
        this.task = task;
    }

    /** Creates new form EditTask */
    public EditTask(java.awt.Frame parent, boolean modal, Task t, DataConductorConfig config)
    {
        super(parent, modal);
        this.parent = parent;
        this.config = config;
        initComponents();
        if(Util.isDiscoveryTrial()){
            databaseTypeBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Microsoft SQL Server", "MySQL", "Oracle 11g", "PostgreSQL", "Hive/Hadoop", "Generic JDBC Database" }));
            queryDatabaseTypeBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Microsoft SQL Server", "MySQL", "Oracle 11g", "PostgreSQL", "Hive/Hadoop", "Generic Database" }));
        }
        btnConfigureSSHTunnelQuery.setEnabled(false);
                
        if(!DataConductorUtil.hasAmazonS3Connector)
        {
            btnAddS3Object.setVisible(false);
        }
        
        final HashMap databaseTypeMap = new HashMap();
        for(int i=0;i<queryDatabaseTypeBox.getItemCount();i++)
        {
            String dataBaseType = String.valueOf(queryDatabaseTypeBox.getItemAt(i));
            databaseTypeMap.put(dataBaseType, i);
        }
        
        queryDatabaseTypeBox.setRenderer(new DefaultListCellRenderer()
        {
          public Component getListCellRendererComponent(
              JList list,
              Object value,
              int index,
              boolean isSelected,
              boolean cellHasFocus)
          {
                 JLabel dbType = (JLabel)super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
                 URL warningIconUrl = getClass().getResource(Util.WARNING_IMG_ICON_PATH);
                 String driverName = Util.getDriverName(databaseTypeMap, value,false);
                 if(driverName != null && !Util.isDriverInstalled(driverName))
                 {
                      setIcon(new ImageIcon(warningIconUrl));
                      Util.flagDriverAsNotInstalled(dbType, index, list, isSelected);
                 }
                return (JLabel)dbType;
          }
        });
         
        if (DataConductorUtil.hasLocalETL())
        {
            isLocalETLTaskCheckBox.setVisible(true);
            downloadCloudSourcesCheckBox.setVisible(true);
            if (config.getLocalETLConfig() != null &&
                    (config.getLocalETLConfig().getLocalETLConnection() == null || config.getLocalETLConfig().getLocalETLConnection().length() == 0))
            {
                isLocalETLTaskCheckBox.setEnabled(false);
                downloadCloudSourcesCheckBox.setEnabled(false);
            }
            else
            {
                isLocalETLTaskCheckBox.setEnabled(true);
                isLocalETLTaskCheckBox.setSelected(t == null ? false : t.isLocalETLTask());
                downloadCloudSourcesCheckBox.setEnabled(true);
                downloadCloudSourcesCheckBox.setSelected( t == null ? false : t.isDowanloadCloudSources());
            }
        }
        else
        {
            isLocalETLTaskCheckBox.setVisible(false);
            downloadCloudSourcesCheckBox.setVisible(false);
        }
        filePanel.setVisible(false);
        tablePanel.setVisible(false);
        queryPanel.setVisible(false);
        weekPanel.setVisible(false);
        monthPanel.setVisible(false);
        sapObjectPanel.setVisible(false);
        sapQueryPanel.setVisible(false);
        S3ObjectPanel.setVisible(false);
        URLPanel.setVisible(false);
        
        Object[] days = new String[31];
        for (int i = 1; i <= 31; i++)
        {
            days[i - 1] = String.valueOf(i);
        }
        DefaultComboBoxModel dm = new DefaultComboBoxModel(days);
        dayOfMonthBox.setModel(dm);
        dayOfMonthBox.setSelectedIndex(0);
        int minuteBlockSize = 1;
        Object[] minutes = new String[60 / minuteBlockSize];
        for (int i = 0; i <= 59; i += minuteBlockSize)
        {
            if (i < 10)
            {
                minutes[i / minuteBlockSize] = '0' + String.valueOf(i);
            } else
            {
                minutes[i / minuteBlockSize] = String.valueOf(i);
            }
        }
        DefaultComboBoxModel mm = new DefaultComboBoxModel(minutes);
        minuteBox.setModel(mm);
        minuteBox.setSelectedIndex(0);
        retryTimesBox.setSelectedIndex(0);
        if (t != null)
        {
            processBox.setSelected(t.isProcessAutomatically());
            createQuickDashboardsBox.setSelected(t.isCreateQuickDashboards());
            consolidateSourcesBox.setSelected(t.isConsolidateSources());
            dateShiftBox.setValue(t.getDateShiftValue());
            sourceList = t.getSourceList();
            taskName.setText(t.getName());
            enabledBox.setSelected(t.isEnabled());

            scheduleBox.setSelected(t.isScheduled());
            mondayBox.setSelected(t.isMonday());
            tuesdayBox.setSelected(t.isTuesday());
            wednesdayBox.setSelected(t.isWednesday());
            thursdayBox.setSelected(t.isThursday());
            fridayBox.setSelected(t.isFriday());
            saturdayBox.setSelected(t.isSaturday());
            sundayBox.setSelected(t.isSunday());
            dailyButton.setSelected(t.getRecurrenceType() == Task.RECURRENCE_TYPE_DAILY);
            weeklyButton.setSelected(t.getRecurrenceType() == Task.RECURRENCE_TYPE_WEEKLY);
            monthlyButton.setSelected(t.getRecurrenceType() == Task.RECURRENCE_TYPE_MONTHLY);
            if (dailyButton.isSelected())
            {
                weekPanel.setVisible(false);
                monthPanel.setVisible(false);
            } else if (weeklyButton.isSelected())
            {
                weekPanel.setVisible(true);
                monthPanel.setVisible(false);
            } else
            {
                weekPanel.setVisible(false);
                monthPanel.setVisible(true);
            }

            hourBox.setSelectedItem(String.valueOf(t.getHour()));
            minuteBox.setSelectedItem((t.getMin() < 10 ? "0" : "") + String.valueOf(t.getMin()));
            ampmBox.setSelectedIndex(t.isPm() ? 1 : 0);
            dayOfMonthBox.setSelectedIndex(t.getDay() - 1);
            retryOnFailureBox.setSelected(t.isRetryOnFailure());
            int numRetries = t.getNumRetries();
            if (numRetries < 1)
            {
                numRetries = 1;
            }
            retryTimesBox.setSelectedIndex(numRetries - 1);
            if (t.getSubGroups() != null)
            {
                String gs = "";
                for (String s : t.getSubGroups())
                {
                    if (gs.length() > 0)
                        gs += ",";
                    gs += s;
                }
                groupsBox.setText(gs);
            }
            setList();
        }

        sourceTable.getSelectionModel().addListSelectionListener(new ListSelectionListener()
        {

            public void valueChanged(ListSelectionEvent e)
            {
                int index = sourceTable.getSelectedRow();
                if (index < 0)
                {
                    return;
                }

                Object o = sourceList.get(index);
                if (FileSource.class.isInstance(o))
                {
                    filePanel.setVisible(
                            true);
                    tablePanel.setVisible(
                            false);
                    queryPanel.setVisible(
                            false);
                    sapObjectPanel.setVisible(
                            false);
                    sapQueryPanel.setVisible(
                            false);
                    S3ObjectPanel.setVisible(
                            false);
                    URLPanel.setVisible(
                            false);
                    filenameBox.setText(
                            ((FileSource) o).getFileName());
                    pathBox.setText(
                            ((FileSource) o).getPath());
                    firstRowHeadersBox.setSelected(((FileSource) o).getSettings().isFirstRowHasHeaders());
                    ignoreInternalQuotesBox.setSelected(((FileSource) o).getSettings().isIgnoreInteralQuotes());
                    ignoreCarriageReturnBox.setSelected(((FileSource) o).getSettings().isIgnoreCarriageReturn());
                    beginSkipBox.setText(String.valueOf(((FileSource) o).getSettings().getBeginSkip()));
                    endSkipBox.setText(String.valueOf(((FileSource) o).getSettings().getEndSkip()));
                    if (((FileSource) o).getSettings().getReplacements() != null)
                        replacementsBox.setText(((FileSource) o).getSettings().getReplacements());
                    else
                        replacementsBox.setText("");
                    forceNumColumnsBox.setSelected(((FileSource) o).getSettings().isForceNumColumns());
                    quoteCharBox.setText(((FileSource) o).getSettings().getQuote());
                    if (((FileSource) o).getSettings().getSeparator() != null)
                        separatorBox.setText(((FileSource) o).getSettings().getSeparator());
                    else
                        separatorBox.setText("");
                    String encoding = ((FileSource) o).getSettings().getEncoding();
                    int enSelIndex = 0;
                    if (encoding != null && encoding.length() > 0)
                    {
                        for(int i=0; i<encodingTypeBox.getItemCount(); i++)
                        {
                            if (encoding.equals(encodingTypeBox.getItemAt(i)))
                            {
                                enSelIndex = i;
                                break;
                            }
                        }
                    }
                    encodingTypeBox.setSelectedIndex(enSelIndex);
                } else if (QuerySource.class.isInstance(o))
                {
                    filePanel.setVisible(
                            false);
                    QuerySource qs = (QuerySource) o;

                    JComboBox typeBox = null;
                    if (qs.getType() == QuerySource.TYPE_SAP)
                    {
                        EncryptionService en = EncryptionService.getInstance();
                        tablePanel.setVisible(false);
                        sapObjectPanel.setVisible(true);
                        sapQueryPanel.setVisible(false);
                        queryPanel.setVisible(false);
                        S3ObjectPanel.setVisible(false);
                        URLPanel.setVisible(false);
                        SAPSystem sapSystem = qs.getSapSystem();
                        routerHostBox.setText(sapSystem.getRouterhost());
                        routerPortBox.setText(sapSystem.getRouterport());
                        serverHostBox.setText(sapSystem.getServerhost());
                        objectServerNameBox.setText((sapSystem.getServername()));
                        systemNumberBox.setText(sapSystem.getSystemNumber());
                        clientBox.setText(sapSystem.getClient());
                        userNameBox.setText(en.decrypt(sapSystem.getUser()));
                        passwordBox.setText(en.decrypt(sapSystem.getPassword()));
                        sapSourceNameLabel.setText(qs.getSAPSourceName());
                        srf = qs.getSAPRemoteFunction();
                        clonedsrf = (SAPRemoteFunction)srf.clone();
                        objectIgnoreInternalQuotesBox.setSelected(((QuerySource) o).getSettings().isIgnoreInteralQuotes());
                        objectQuoteCharBox.setText(((QuerySource) o).getSettings().getQuote());
                    }
                    else if (qs.getType() == QuerySource.TYPE_SAP_QUERY)
                    {
                        EncryptionService en = EncryptionService.getInstance();
                        tablePanel.setVisible(false);
                        sapObjectPanel.setVisible(false);
                        queryPanel.setVisible(false);
                        sapQueryPanel.setVisible(true);
                        S3ObjectPanel.setVisible(false);
                        URLPanel.setVisible(false);
                        SAPSystem sapSystem = qs.getSapSystem();
                        sapQrouterHostBox.setText(sapSystem.getRouterhost());
                        sapQrouterPortBox.setText(sapSystem.getRouterport());
                        sapQserverHostBox.setText(sapSystem.getServerhost());
                        sapQobjectServerNameBox.setText((sapSystem.getServername()));
                        sapQsystemNumberBox.setText(sapSystem.getSystemNumber());
                        sapQclientBox.setText(sapSystem.getClient());
                        sapQuserNameBox.setText(en.decrypt(sapSystem.getUser()));
                        sapQpasswordBox.setText(en.decrypt(sapSystem.getPassword()));
                        sapQueryNameLabel.setText(qs.getSAPSourceName());
                        srq = qs.getSAPRemoteQuery();
                        clonedsrq = (SAPRemoteQuery)srq.clone();
                        sapQobjectIgnoreInternalQuotesBox.setSelected(((QuerySource) o).getSettings().isIgnoreInteralQuotes());
                        sapQobjectQuoteCharBox.setText(((QuerySource) o).getSettings().getQuote());
                    }
                    else if (qs.getType() == QuerySource.TYPE_SAP_DSO)
                    {
                        tablePanel.setVisible(false);
                        sapObjectPanel.setVisible(false);
                        sapQueryPanel.setVisible(false);
                        queryPanel.setVisible(false);
                        S3ObjectPanel.setVisible(false);
                        URLPanel.setVisible(false);
                    }
                    else if (qs.getType() == QuerySource.TYPE_S3_OBJECT)
                    {
                        
                       tablePanel.setVisible(false);
                       sapQueryPanel.setVisible(false);
                       queryPanel.setVisible(false);
                       sapObjectPanel.setVisible(false);
                       S3ObjectPanel.setVisible(true);
                       URLPanel.setVisible(false);
                      
                       txtS3HostName.setText(qs.getTableName());
                       txtCrendentialsFile.setText(qs.getCrendentialsFileName());
                       txtBucketName.setText(qs.getBucketName());
                       txtBucketKey.setText(qs.getBucketKey());
                       
                       if (((QuerySource) o).getSettings().getReplacements() != null)
                            replacementsBoxForS3Object.setText(((QuerySource) o).getSettings().getReplacements());
                        else
                            replacementsBoxForS3Object.setText("");
                       s3ObjectIgnoreInternalQuotesBox.setSelected(((QuerySource) o).getSettings().isIgnoreInteralQuotes());
                       s3ObjectIgnoreCarriageReturnBox.setSelected(((QuerySource) o).getSettings().isIgnoreCarriageReturn());
                       s3ObjectQuoteCharBox.setText(((QuerySource) o).getSettings().getQuote());
                    }
                    else if (qs.getType() == QuerySource.TYPE_URL_OBJECT)
                    {   
                       tablePanel.setVisible(false);
                       sapQueryPanel.setVisible(false);
                       queryPanel.setVisible(false);
                       sapObjectPanel.setVisible(false);
                       S3ObjectPanel.setVisible(false);
                       URLPanel.setVisible(true);

                       txtURL.setText(qs.getUrl());
                       txtName.setText(qs.getTableName());
                       URLMethodComboBox.setSelectedItem(qs.getMethod());
                       
                       if (((QuerySource) o).getSettings().getReplacements() != null)
                            replacementsBoxForURL.setText(((QuerySource) o).getSettings().getReplacements());
                        else
                            replacementsBoxForURL.setText("");
                       URLIgnoreInternalQuotesBox.setSelected(((QuerySource) o).getSettings().isIgnoreInteralQuotes());
                       URLIgnoreCarriageReturnBox.setSelected(((QuerySource) o).getSettings().isIgnoreCarriageReturn());
                       URLQuoteCharBox.setText(((QuerySource) o).getSettings().getQuote());
                       URLIgnoreBadServerCertificatesBox.setSelected(((QuerySource) o).getIgnoreBadServerCertificates());
                    }                    
                    else
                    {
                        sapObjectPanel.setVisible(false);
                        sapQueryPanel.setVisible(false);
                        int typeIndex = qs.isGenericDriver() ? DatabaseConnection.getGenericDatabaseIndex() : qs.getDriverIndex(false);
                        databaseName.setVisible(false);
                        queryDatabaseName.setVisible(false);
                        S3ObjectPanel.setVisible(false);
                        URLPanel.setVisible(false);
                        if (qs.getType() == QuerySource.TYPE_TABLE)
                        {
                            if(qs != null && qs.getSshTunnel() != null && qs.getSshTunnel().isIsSSHTunnelConfigured())
                            {
                                chkEnableSSHTable.setSelected(true);
                                btnConfigSSHTunnelTable.setEnabled(true);
                            }
                            else
                            {
                                chkEnableSSHTable.setSelected(false);
                                btnConfigSSHTunnelTable.setEnabled(false);
                            }
                             
                            sst = qs.getSshTunnel();
                            if(sst == null)
                            {
                                sst = new SSHTunnel();
                            }
                            clonedSSHTunnel = (SSHTunnel)sst.clone(); 
        
                            tablePanel.setVisible(true);
                            queryPanel.setVisible(false);
                            tableNameBox.setText(qs.getTableName());
                            catalogBox.setText(qs.getCatalogName());
                            schemaBox.setText(qs.getSchemaName());
                            serverName.setText(qs.getServerName());
                            databaseName.setText(qs.getDatabaseName());
                            databaseName.setVisible(QuerySource.getHasDatabaseName(typeIndex, false));
                            portNumber.setText(qs.getPort() > 0 ? String.valueOf(qs.getPort()) : "");
                            username.setText(qs.getUsername());
                            password.setText(qs.retrievePassword());
                            if (((QuerySource) o).getSettings().getReplacements() != null)
                                replacementsBoxForTable.setText(((QuerySource) o).getSettings().getReplacements());
                            else
                                replacementsBoxForTable.setText("");
                            tableIgnoreInternalQuotesBox.setSelected(((QuerySource) o).getSettings().isIgnoreInteralQuotes());
                            tableIgnoreCarriageReturnBox.setSelected(((QuerySource) o).getSettings().isIgnoreInteralQuotes());
                            tableQuoteCharBox.setText(((QuerySource) o).getSettings().getQuote());
                            typeBox = databaseTypeBox;
                        }  else
                        {
                            if(qs != null && qs.getSshTunnel() != null && qs.getSshTunnel().isIsSSHTunnelConfigured())
                            {
                                chkEnableSSHQuery.setSelected(true);
                                btnConfigureSSHTunnelQuery.setEnabled(true);
                            }
                            else
                            {
                                chkEnableSSHQuery.setSelected(false);
                                btnConfigureSSHTunnelQuery.setEnabled(false);
                            }
                            
                            sst = qs.getSshTunnel();
                            if(sst == null)
                            {
                                sst = new SSHTunnel();
                            }
                            clonedSSHTunnel = (SSHTunnel)sst.clone(); 
                            
                            tablePanel.setVisible(false);
                            queryPanel.setVisible(true);
                            queryBox.setText(qs.getQuery());
                            queryNameBox.setText(qs.getTableName());
                            queryServerName.setText(qs.getServerName());
                            queryDatabaseName.setText(qs.getFullDatabaseName());
                            queryDatabaseName.setVisible(QuerySource.getHasDatabaseName(typeIndex, false));
                            queryPortNumber.setText(qs.getPort() > 0 ? String.valueOf(qs.getPort()) : "");
                            queryUsername.setText(qs.getUsername());
                            queryPassword.setText(qs.retrievePassword());
                            if (((QuerySource) o).getSettings().getReplacements() != null)
                                replacementsBoxForQuery.setText(((QuerySource) o).getSettings().getReplacements());
                            else
                                replacementsBoxForQuery.setText("");
                            queryIgnoreInternalQuotesBox.setSelected(((QuerySource) o).getSettings().isIgnoreInteralQuotes());
                            queryIgnoreCarriageReturnBox.setSelected(((QuerySource) o).getSettings().isIgnoreCarriageReturn());
                            queryQuoteCharBox.setText(((QuerySource) o).getSettings().getQuote());
                            typeBox = queryDatabaseTypeBox;
                        }
                        typeBox.setSelectedIndex(typeIndex);
                        queryServerNameLabel.setText(QuerySource.getServerLabel(typeIndex, false));
                        serverNameLabel.setText(QuerySource.getServerLabel(typeIndex, false));
                        queryDatabaseNameLabel.setVisible(QuerySource.getHasDatabaseName(typeIndex, false));
                        portLabel.setVisible(QuerySource.getHasPort(typeIndex, false));
                        portNumber.setVisible(QuerySource.getHasPort(typeIndex, false));
                        queryPortLabel.setVisible(QuerySource.getHasPort(typeIndex, false));
                        queryPortNumber.setVisible(QuerySource.getHasPort(typeIndex, false));
                        
                        saveQuerySourceState(qs);
                    }
                }
            }
        });
    }

    public void showHideSAPConnector(boolean value)
    {
        addObjectsButton.setVisible(value);
        addSAPQueryButton.setVisible(value);
        addSAPDSOButton.setVisible(value);
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        recurrenceGroup = new javax.swing.ButtonGroup();
        editTabs = new javax.swing.JTabbedPane();
        propertyPanel = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        taskName = new javax.swing.JTextField();
        enabledBox = new javax.swing.JCheckBox();
        jButton5 = new javax.swing.JButton();
        jButton6 = new javax.swing.JButton();
        processBox = new javax.swing.JCheckBox();
        createQuickDashboardsBox = new javax.swing.JCheckBox();
        consolidateSourcesBox = new javax.swing.JCheckBox();
        dateShiftBox = new javax.swing.JSpinner();
        jLabel27 = new javax.swing.JLabel();
        jLabel28 = new javax.swing.JLabel();
        groupsBox = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        isLocalETLTaskCheckBox = new javax.swing.JCheckBox();
        downloadCloudSourcesCheckBox = new javax.swing.JCheckBox();
        sourcesPanel = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        sourceTable = new javax.swing.JTable() {
            public boolean isCellEditable(int rowIndex, int vColIndex) {
                return false;
            }
        };
        addFileButton = new javax.swing.JButton();
        addTablesButton = new javax.swing.JButton();
        addQueryButton = new javax.swing.JButton();
        removeSourceButton = new javax.swing.JButton();
        btnAddURL = new javax.swing.JButton();
        addObjectsButton = new javax.swing.JButton();
        addSAPQueryButton = new javax.swing.JButton();
        okButton = new javax.swing.JButton();
        cancelButton = new javax.swing.JButton();
        addSAPDSOButton = new javax.swing.JButton();
        btnAddS3Object = new javax.swing.JButton();
        masterPanel = new javax.swing.JPanel();
        filePanel = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        filenameBox = new javax.swing.JTextField();
        pathBox = new javax.swing.JTextField();
        updateFileButton = new javax.swing.JButton();
        firstRowHeadersBox = new javax.swing.JCheckBox();
        jLabel24 = new javax.swing.JLabel();
        jLabel25 = new javax.swing.JLabel();
        beginSkipBox = new javax.swing.JTextField();
        endSkipBox = new javax.swing.JTextField();
        ignoreInternalQuotesBox = new javax.swing.JCheckBox();
        forceNumColumnsBox = new javax.swing.JCheckBox();
        jLabel9 = new javax.swing.JLabel();
        replacementsBox = new javax.swing.JTextField();
        jLabel16 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        quoteCharBox = new javax.swing.JTextField();
        ignoreCarriageReturnBox = new javax.swing.JCheckBox();
        jLabel37 = new javax.swing.JLabel();
        separatorBox = new javax.swing.JTextField();
        encodingTypeBox = new javax.swing.JComboBox();
        jLabel38 = new javax.swing.JLabel();
        tablePanel = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        tableNameBox = new javax.swing.JTextField();
        catalogBox = new javax.swing.JTextField();
        updateTableButton = new javax.swing.JButton();
        jLabel7 = new javax.swing.JLabel();
        serverNameLabel = new javax.swing.JLabel();
        databaseNameLabel = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        username = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        password = new javax.swing.JPasswordField();
        databaseTypeBox = new javax.swing.JComboBox();
        serverName = new javax.swing.JTextField();
        databaseName = new javax.swing.JTextField();
        schemaBox = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        portLabel = new javax.swing.JLabel();
        portNumber = new javax.swing.JTextField();
        tableIgnoreInternalQuotesBox = new javax.swing.JCheckBox();
        jLabel29 = new javax.swing.JLabel();
        tableQuoteCharBox = new javax.swing.JTextField();
        jLabel35 = new javax.swing.JLabel();
        replacementsBoxForTable = new javax.swing.JTextField();
        jLabel36 = new javax.swing.JLabel();
        tableIgnoreCarriageReturnBox = new javax.swing.JCheckBox();
        btnConfigSSHTunnelTable = new javax.swing.JButton();
        chkEnableSSHTable = new javax.swing.JCheckBox();
        URLPanel = new javax.swing.JPanel();
        URLLabel = new javax.swing.JLabel();
        txtURL = new javax.swing.JTextField();
        URLUpdate = new javax.swing.JButton();
        replacementsForURLLabel = new javax.swing.JLabel();
        replacementsBoxForURL = new javax.swing.JTextField();
        jLabel42 = new javax.swing.JLabel();
        URLIgnoreCarriageReturnBox = new javax.swing.JCheckBox();
        URLIgnoreInternalQuotesBox = new javax.swing.JCheckBox();
        URLQuoteCharLabel = new javax.swing.JLabel();
        URLQuoteCharBox = new javax.swing.JTextField();
        s3HostNameLabel2 = new javax.swing.JLabel();
        txtName = new javax.swing.JTextField();
        URLIgnoreBadServerCertificatesBox = new javax.swing.JCheckBox();
        replacementsForURLLabel1 = new javax.swing.JLabel();
        URLMethodComboBox = new javax.swing.JComboBox();
        queryPanel = new javax.swing.JPanel();
        jLabel13 = new javax.swing.JLabel();
        updateQueryButton = new javax.swing.JButton();
        jLabel15 = new javax.swing.JLabel();
        queryServerNameLabel = new javax.swing.JLabel();
        queryDatabaseNameLabel = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        queryUsername = new javax.swing.JTextField();
        jLabel19 = new javax.swing.JLabel();
        queryPassword = new javax.swing.JPasswordField();
        queryDatabaseTypeBox = new javax.swing.JComboBox();
        queryServerName = new javax.swing.JTextField();
        queryDatabaseName = new javax.swing.JTextField();
        jLabel26 = new javax.swing.JLabel();
        queryNameBox = new javax.swing.JTextField();
        jScrollPane2 = new javax.swing.JScrollPane();
        queryBox = new javax.swing.JTextArea();
        queryPortLabel = new javax.swing.JLabel();
        queryPortNumber = new javax.swing.JTextField();
        queryIgnoreInternalQuotesBox = new javax.swing.JCheckBox();
        jLabel30 = new javax.swing.JLabel();
        queryQuoteCharBox = new javax.swing.JTextField();
        jLabel33 = new javax.swing.JLabel();
        jLabel34 = new javax.swing.JLabel();
        replacementsBoxForQuery = new javax.swing.JTextField();
        queryIgnoreCarriageReturnBox = new javax.swing.JCheckBox();
        btnConfigureSSHTunnelQuery = new javax.swing.JButton();
        chkEnableSSHQuery = new javax.swing.JCheckBox();
        sapQueryPanel = new javax.swing.JPanel();
        updateSAPQueryButton = new javax.swing.JButton();
        routerHostLabel1 = new javax.swing.JLabel();
        serverHostLabel1 = new javax.swing.JLabel();
        systemNumberLabel1 = new javax.swing.JLabel();
        userNameLabel1 = new javax.swing.JLabel();
        queryNameLabel = new javax.swing.JLabel();
        sapQserverHostBox = new javax.swing.JTextField();
        objectServerNameLabel1 = new javax.swing.JLabel();
        sapQrouterHostBox = new javax.swing.JTextField();
        routerPortLabel1 = new javax.swing.JLabel();
        sapQsystemNumberBox = new javax.swing.JTextField();
        sapQuserNameBox = new javax.swing.JTextField();
        passwordLabel1 = new javax.swing.JLabel();
        clientLabel1 = new javax.swing.JLabel();
        sapQrouterPortBox = new javax.swing.JTextField();
        sapQclientBox = new javax.swing.JTextField();
        sapQpasswordBox = new javax.swing.JPasswordField();
        sapQobjectServerNameBox = new javax.swing.JTextField();
        sapQobjectIgnoreInternalQuotesBox = new javax.swing.JCheckBox();
        jLabel32 = new javax.swing.JLabel();
        sapQobjectQuoteCharBox = new javax.swing.JTextField();
        sapQueryNameLabel = new javax.swing.JLabel();
        SAPQueryDetailsButton = new javax.swing.JButton();
        sapObjectPanel = new javax.swing.JPanel();
        updateObjectButton = new javax.swing.JButton();
        routerHostLabel = new javax.swing.JLabel();
        serverHostLabel = new javax.swing.JLabel();
        systemNumberLabel = new javax.swing.JLabel();
        userNameLabel = new javax.swing.JLabel();
        objectNameLabel = new javax.swing.JLabel();
        serverHostBox = new javax.swing.JTextField();
        objectServerNameLabel = new javax.swing.JLabel();
        routerHostBox = new javax.swing.JTextField();
        routerPortLabel = new javax.swing.JLabel();
        systemNumberBox = new javax.swing.JTextField();
        userNameBox = new javax.swing.JTextField();
        passwordLabel = new javax.swing.JLabel();
        clientLabel = new javax.swing.JLabel();
        routerPortBox = new javax.swing.JTextField();
        clientBox = new javax.swing.JTextField();
        passwordBox = new javax.swing.JPasswordField();
        objectServerNameBox = new javax.swing.JTextField();
        objectIgnoreInternalQuotesBox = new javax.swing.JCheckBox();
        jLabel31 = new javax.swing.JLabel();
        objectQuoteCharBox = new javax.swing.JTextField();
        sapSourceNameLabel = new javax.swing.JLabel();
        SAPFunctionDetailsButton = new javax.swing.JButton();
        S3ObjectPanel = new javax.swing.JPanel();
        s3HostNameLabel = new javax.swing.JLabel();
        txtS3HostName = new javax.swing.JTextField();
        crendentialsFileLabel = new javax.swing.JLabel();
        txtCrendentialsFile = new javax.swing.JTextField();
        browseCrendentoalsFileBtn = new javax.swing.JButton();
        jLabel39 = new javax.swing.JLabel();
        bucketNameLabel = new javax.swing.JLabel();
        txtBucketName = new javax.swing.JTextField();
        bucketKeyLabel = new javax.swing.JLabel();
        txtBucketKey = new javax.swing.JTextField();
        s3ObjectUpdate = new javax.swing.JButton();
        replacementsForS3ObjectLabel = new javax.swing.JLabel();
        replacementsBoxForS3Object = new javax.swing.JTextField();
        jLabel41 = new javax.swing.JLabel();
        s3ObjectIgnoreCarriageReturnBox = new javax.swing.JCheckBox();
        s3ObjectIgnoreInternalQuotesBox = new javax.swing.JCheckBox();
        s3ObjectQuoteCharLabel = new javax.swing.JLabel();
        s3ObjectQuoteCharBox = new javax.swing.JTextField();
        schedulePanel = new javax.swing.JPanel();
        jButton3 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        dailyButton = new javax.swing.JRadioButton();
        weeklyButton = new javax.swing.JRadioButton();
        monthlyButton = new javax.swing.JRadioButton();
        weekPanel = new javax.swing.JPanel();
        mondayBox = new javax.swing.JCheckBox();
        thursdayBox = new javax.swing.JCheckBox();
        sundayBox = new javax.swing.JCheckBox();
        tuesdayBox = new javax.swing.JCheckBox();
        fridayBox = new javax.swing.JCheckBox();
        wednesdayBox = new javax.swing.JCheckBox();
        saturdayBox = new javax.swing.JCheckBox();
        monthPanel = new javax.swing.JPanel();
        jLabel22 = new javax.swing.JLabel();
        dayOfMonthBox = new javax.swing.JComboBox();
        jLabel23 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jLabel14 = new javax.swing.JLabel();
        hourBox = new javax.swing.JComboBox();
        jLabel20 = new javax.swing.JLabel();
        minuteBox = new javax.swing.JComboBox();
        ampmBox = new javax.swing.JComboBox();
        retryOnFailureBox = new javax.swing.JCheckBox();
        retryTimesBox = new javax.swing.JComboBox();
        jLabel21 = new javax.swing.JLabel();
        scheduleBox = new javax.swing.JCheckBox();
        jLabel40 = new javax.swing.JLabel();
        jLabel43 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setName("Form"); // NOI18N

        editTabs.setTabLayoutPolicy(javax.swing.JTabbedPane.SCROLL_TAB_LAYOUT);
        org.jdesktop.application.ResourceMap resourceMap = org.jdesktop.application.Application.getInstance(com.birst.dataconductor.DataConductorApp.class).getContext().getResourceMap(EditTask.class);
        editTabs.setFont(resourceMap.getFont("editTabs.font")); // NOI18N
        editTabs.setName("editTabs"); // NOI18N

        propertyPanel.setName("propertyPanel"); // NOI18N

        jLabel1.setText(resourceMap.getString("jLabel1.text")); // NOI18N
        jLabel1.setName("jLabel1"); // NOI18N

        jLabel2.setText(resourceMap.getString("jLabel2.text")); // NOI18N
        jLabel2.setName("jLabel2"); // NOI18N

        taskName.setText(resourceMap.getString("taskName.text")); // NOI18N
        taskName.setName("taskName"); // NOI18N

        enabledBox.setSelected(true);
        enabledBox.setText(resourceMap.getString("enabledBox.text")); // NOI18N
        enabledBox.setName("enabledBox"); // NOI18N

        javax.swing.ActionMap actionMap = org.jdesktop.application.Application.getInstance(com.birst.dataconductor.DataConductorApp.class).getContext().getActionMap(EditTask.class, this);
        jButton5.setAction(actionMap.get("okButton")); // NOI18N
        jButton5.setName("jButton5"); // NOI18N
        jButton5.setPreferredSize(new java.awt.Dimension(50, 23));

        jButton6.setAction(actionMap.get("cancel")); // NOI18N
        jButton6.setName("jButton6"); // NOI18N
        jButton6.setPreferredSize(new java.awt.Dimension(50, 23));

        processBox.setText(resourceMap.getString("processBox.text")); // NOI18N
        processBox.setName("processBox"); // NOI18N

        createQuickDashboardsBox.setAction(actionMap.get("changeProcessing")); // NOI18N
        createQuickDashboardsBox.setText(resourceMap.getString("createQuickDashboardsBox.text")); // NOI18N
        createQuickDashboardsBox.setName("createQuickDashboardsBox"); // NOI18N

        consolidateSourcesBox.setAction(actionMap.get("changeProcessing")); // NOI18N
        consolidateSourcesBox.setText(resourceMap.getString("consolidateSourcesBox.text")); // NOI18N
        consolidateSourcesBox.setName("consolidateSourcesBox"); // NOI18N

        dateShiftBox.setModel(new javax.swing.SpinnerNumberModel(0, -14, 14, 1));
        dateShiftBox.setName("dateShiftBox"); // NOI18N

        jLabel27.setText(resourceMap.getString("jLabel27.text")); // NOI18N
        jLabel27.setName("jLabel27"); // NOI18N

        jLabel28.setText(resourceMap.getString("jLabel28.text")); // NOI18N
        jLabel28.setName("jLabel28"); // NOI18N

        groupsBox.setName("groupsBox"); // NOI18N

        jLabel8.setText(resourceMap.getString("jLabel8.text")); // NOI18N
        jLabel8.setName("jLabel8"); // NOI18N

        isLocalETLTaskCheckBox.setText(resourceMap.getString("isLocalETLTaskCheckBox.text")); // NOI18N
        isLocalETLTaskCheckBox.setName("isLocalETLTaskCheckBox"); // NOI18N

        downloadCloudSourcesCheckBox.setText(resourceMap.getString("downloadCloudSourcesCheckBox.text")); // NOI18N
        downloadCloudSourcesCheckBox.setName("downloadCloudSourcesCheckBox"); // NOI18N

        javax.swing.GroupLayout propertyPanelLayout = new javax.swing.GroupLayout(propertyPanel);
        propertyPanel.setLayout(propertyPanelLayout);
        propertyPanelLayout.setHorizontalGroup(
            propertyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(propertyPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(propertyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(propertyPanelLayout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(taskName, javax.swing.GroupLayout.DEFAULT_SIZE, 743, Short.MAX_VALUE))
                    .addComponent(downloadCloudSourcesCheckBox)
                    .addComponent(isLocalETLTaskCheckBox)
                    .addComponent(createQuickDashboardsBox)
                    .addComponent(consolidateSourcesBox)
                    .addComponent(processBox)
                    .addGroup(propertyPanelLayout.createSequentialGroup()
                        .addComponent(jLabel27)
                        .addGap(18, 18, 18)
                        .addComponent(dateShiftBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel28))
                    .addGroup(propertyPanelLayout.createSequentialGroup()
                        .addComponent(jLabel8)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(groupsBox, javax.swing.GroupLayout.DEFAULT_SIZE, 595, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, propertyPanelLayout.createSequentialGroup()
                        .addComponent(jButton5, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jButton6, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(propertyPanelLayout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(enabledBox)))
                .addContainerGap())
        );
        propertyPanelLayout.setVerticalGroup(
            propertyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(propertyPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(propertyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(taskName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(21, 21, 21)
                .addGroup(propertyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(groupsBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(14, 14, 14)
                .addGroup(propertyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(enabledBox, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(26, 26, 26)
                .addComponent(processBox)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(createQuickDashboardsBox)
                .addGap(4, 4, 4)
                .addComponent(consolidateSourcesBox)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(isLocalETLTaskCheckBox)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(downloadCloudSourcesCheckBox)
                .addGap(5, 5, 5)
                .addGroup(propertyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel27)
                    .addComponent(dateShiftBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel28))
                .addGap(27, 27, 27)
                .addGroup(propertyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(328, 328, 328))
        );

        editTabs.addTab(resourceMap.getString("propertyPanel.TabConstraints.tabTitle"), propertyPanel); // NOI18N

        sourcesPanel.setName("sourcesPanel"); // NOI18N

        jScrollPane1.setName("jScrollPane1"); // NOI18N

        sourceTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Type", "Name", "Location"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        sourceTable.setName("sourceTable"); // NOI18N
        sourceTable.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jScrollPane1.setViewportView(sourceTable);
        sourceTable.getColumnModel().getColumn(0).setHeaderValue(resourceMap.getString("sourceTable.columnModel.title0")); // NOI18N
        sourceTable.getColumnModel().getColumn(1).setHeaderValue(resourceMap.getString("sourceTable.columnModel.title1")); // NOI18N
        sourceTable.getColumnModel().getColumn(2).setHeaderValue(resourceMap.getString("sourceTable.columnModel.title2")); // NOI18N

        addFileButton.setAction(actionMap.get("addFile")); // NOI18N
        addFileButton.setName("addFileButton"); // NOI18N

        addTablesButton.setAction(actionMap.get("addTables")); // NOI18N
        addTablesButton.setText(resourceMap.getString("addTablesButton.text")); // NOI18N
        addTablesButton.setName("addTablesButton"); // NOI18N

        addQueryButton.setAction(actionMap.get("addQuery")); // NOI18N
        addQueryButton.setText(resourceMap.getString("addQueryButton.text")); // NOI18N
        addQueryButton.setName("addQueryButton"); // NOI18N

        removeSourceButton.setAction(actionMap.get("removeAction")); // NOI18N
        removeSourceButton.setText(resourceMap.getString("removeSourceButton.text")); // NOI18N
        removeSourceButton.setActionCommand(resourceMap.getString("removeSourceButton.actionCommand")); // NOI18N
        removeSourceButton.setName("removeSourceButton"); // NOI18N

        btnAddURL.setAction(actionMap.get("addURL")); // NOI18N
        btnAddURL.setText(resourceMap.getString("btnAddURL.text")); // NOI18N
        btnAddURL.setName("btnAddURL"); // NOI18N

        addObjectsButton.setAction(actionMap.get("addObjects")); // NOI18N
        addObjectsButton.setText(resourceMap.getString("addObjectsButton.text")); // NOI18N
        addObjectsButton.setName("addObjectsButton"); // NOI18N

        addSAPQueryButton.setAction(actionMap.get("addSAPQueries")); // NOI18N
        addSAPQueryButton.setText(resourceMap.getString("addSAPQueryButton.text")); // NOI18N
        addSAPQueryButton.setName("addSAPQueryButton"); // NOI18N

        okButton.setAction(actionMap.get("okButton")); // NOI18N
        okButton.setText(resourceMap.getString("okButton.text")); // NOI18N
        okButton.setName("okButton"); // NOI18N
        okButton.setPreferredSize(new java.awt.Dimension(50, 23));

        cancelButton.setAction(actionMap.get("cancel")); // NOI18N
        cancelButton.setName("cancelButton"); // NOI18N
        cancelButton.setPreferredSize(new java.awt.Dimension(50, 23));

        addSAPDSOButton.setAction(actionMap.get("addSAPDSOs")); // NOI18N
        addSAPDSOButton.setText(resourceMap.getString("addSAPDSOButton.text")); // NOI18N
        addSAPDSOButton.setName("addSAPDSOButton"); // NOI18N

        btnAddS3Object.setAction(actionMap.get("addS3Object")); // NOI18N
        btnAddS3Object.setText(resourceMap.getString("btnAddS3Object.text")); // NOI18N
        btnAddS3Object.setName("btnAddS3Object"); // NOI18N

        masterPanel.setName("masterPanel"); // NOI18N
        masterPanel.setPreferredSize(new java.awt.Dimension(795, 306));

        filePanel.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(-16777216,true)));
        filePanel.setName("filePanel"); // NOI18N

        jLabel3.setText(resourceMap.getString("jLabel3.text")); // NOI18N
        jLabel3.setName("jLabel3"); // NOI18N

        jLabel4.setText(resourceMap.getString("jLabel4.text")); // NOI18N
        jLabel4.setName("jLabel4"); // NOI18N

        filenameBox.setText(resourceMap.getString("filenameBox.text")); // NOI18N
        filenameBox.setName("filenameBox"); // NOI18N

        pathBox.setName("pathBox"); // NOI18N

        updateFileButton.setAction(actionMap.get("updateFile")); // NOI18N
        updateFileButton.setText(resourceMap.getString("updateFileButton.text")); // NOI18N
        updateFileButton.setName("updateFileButton"); // NOI18N

        firstRowHeadersBox.setSelected(true);
        firstRowHeadersBox.setText(resourceMap.getString("firstRowHeadersBox.text")); // NOI18N
        firstRowHeadersBox.setName("firstRowHeadersBox"); // NOI18N

        jLabel24.setText(resourceMap.getString("jLabel24.text")); // NOI18N
        jLabel24.setName("jLabel24"); // NOI18N

        jLabel25.setText(resourceMap.getString("jLabel25.text")); // NOI18N
        jLabel25.setName("jLabel25"); // NOI18N

        beginSkipBox.setText(resourceMap.getString("beginSkipBox.text")); // NOI18N
        beginSkipBox.setName("beginSkipBox"); // NOI18N

        endSkipBox.setText(resourceMap.getString("endSkipBox.text")); // NOI18N
        endSkipBox.setName("endSkipBox"); // NOI18N

        ignoreInternalQuotesBox.setText(resourceMap.getString("ignoreInternalQuotesBox.text")); // NOI18N
        ignoreInternalQuotesBox.setActionCommand(resourceMap.getString("ignoreInternalQuotesBox.actionCommand")); // NOI18N
        ignoreInternalQuotesBox.setName("ignoreInternalQuotesBox"); // NOI18N

        forceNumColumnsBox.setText(resourceMap.getString("forceNumColumnsBox.text")); // NOI18N
        forceNumColumnsBox.setName("forceNumColumnsBox"); // NOI18N

        jLabel9.setText(resourceMap.getString("jLabel9.text")); // NOI18N
        jLabel9.setName("jLabel9"); // NOI18N

        replacementsBox.setName("replacementsBox"); // NOI18N

        jLabel16.setText(resourceMap.getString("jLabel16.text")); // NOI18N
        jLabel16.setName("jLabel16"); // NOI18N

        jLabel17.setText(resourceMap.getString("jLabel17.text")); // NOI18N
        jLabel17.setName("jLabel17"); // NOI18N

        quoteCharBox.setText(resourceMap.getString("quoteCharBox.text")); // NOI18N
        quoteCharBox.setName("quoteCharBox"); // NOI18N

        ignoreCarriageReturnBox.setText(resourceMap.getString("ignoreCarriageReturnBox.text")); // NOI18N
        ignoreCarriageReturnBox.setName("ignoreCarriageReturnBox"); // NOI18N

        jLabel37.setText(resourceMap.getString("jLabel37.text")); // NOI18N
        jLabel37.setName("jLabel37"); // NOI18N

        separatorBox.setText(resourceMap.getString("separatorBox.text")); // NOI18N
        separatorBox.setName("separatorBox"); // NOI18N

        encodingTypeBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Detect", "UTF-8", "UTF-16(Little Endian)", "UTF-16(Big Endian)" }));
        encodingTypeBox.setName("encodingTypeBox"); // NOI18N

        jLabel38.setText(resourceMap.getString("jLabel38.text")); // NOI18N
        jLabel38.setName("jLabel38"); // NOI18N

        javax.swing.GroupLayout filePanelLayout = new javax.swing.GroupLayout(filePanel);
        filePanel.setLayout(filePanelLayout);
        filePanelLayout.setHorizontalGroup(
            filePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(filePanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(filePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(filePanelLayout.createSequentialGroup()
                        .addGroup(filePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel3)
                            .addComponent(jLabel4))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(filePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(pathBox)
                            .addComponent(filenameBox, javax.swing.GroupLayout.DEFAULT_SIZE, 352, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 328, Short.MAX_VALUE)
                        .addComponent(updateFileButton))
                    .addGroup(filePanelLayout.createSequentialGroup()
                        .addGroup(filePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(filePanelLayout.createSequentialGroup()
                                .addGroup(filePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel24)
                                    .addComponent(jLabel25))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(filePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(beginSkipBox, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(endSkipBox, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addComponent(firstRowHeadersBox)
                            .addComponent(ignoreInternalQuotesBox)
                            .addComponent(ignoreCarriageReturnBox))
                        .addGap(86, 86, 86)
                        .addGroup(filePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(forceNumColumnsBox)
                            .addGroup(filePanelLayout.createSequentialGroup()
                                .addComponent(jLabel9)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(filePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(replacementsBox, javax.swing.GroupLayout.DEFAULT_SIZE, 341, Short.MAX_VALUE)
                                    .addComponent(jLabel16)))
                            .addGroup(filePanelLayout.createSequentialGroup()
                                .addGroup(filePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, filePanelLayout.createSequentialGroup()
                                        .addComponent(jLabel37)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(separatorBox, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, filePanelLayout.createSequentialGroup()
                                        .addComponent(jLabel17)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(quoteCharBox, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGap(10, 10, 10)
                                .addComponent(jLabel38)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(encodingTypeBox, 0, 232, Short.MAX_VALUE)))))
                .addContainerGap())
        );
        filePanelLayout.setVerticalGroup(
            filePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(filePanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(filePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(updateFileButton)
                    .addComponent(filenameBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(filePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(pathBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(filePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(firstRowHeadersBox)
                    .addComponent(jLabel37)
                    .addComponent(encodingTypeBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(separatorBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel38))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(filePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(ignoreInternalQuotesBox)
                    .addComponent(jLabel17)
                    .addComponent(quoteCharBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(filePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel24)
                    .addComponent(beginSkipBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(forceNumColumnsBox))
                .addGap(9, 9, 9)
                .addGroup(filePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel25)
                    .addComponent(endSkipBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel9)
                    .addComponent(replacementsBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(filePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel16)
                    .addComponent(ignoreCarriageReturnBox))
                .addContainerGap(15, Short.MAX_VALUE))
        );

        jLabel37.getAccessibleContext().setAccessibleName(resourceMap.getString("jLabel37.AccessibleContext.accessibleName")); // NOI18N

        tablePanel.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(-16777216,true)));
        tablePanel.setName("tablePanel"); // NOI18N
        tablePanel.setPreferredSize(new java.awt.Dimension(741, 222));

        jLabel5.setText(resourceMap.getString("jLabel5.text")); // NOI18N
        jLabel5.setName("jLabel5"); // NOI18N

        jLabel6.setText(resourceMap.getString("jLabel6.text")); // NOI18N
        jLabel6.setName("jLabel6"); // NOI18N

        tableNameBox.setName("tableNameBox"); // NOI18N

        catalogBox.setName("catalogBox"); // NOI18N

        updateTableButton.setAction(actionMap.get("updateTable")); // NOI18N
        updateTableButton.setName("updateTableButton"); // NOI18N

        jLabel7.setText(resourceMap.getString("jLabel7.text")); // NOI18N
        jLabel7.setName("jLabel7"); // NOI18N

        serverNameLabel.setText(resourceMap.getString("serverNameLabel.text")); // NOI18N
        serverNameLabel.setName("serverNameLabel"); // NOI18N

        databaseNameLabel.setText(resourceMap.getString("databaseNameLabel.text")); // NOI18N
        databaseNameLabel.setName("databaseNameLabel"); // NOI18N

        jLabel10.setText(resourceMap.getString("jLabel10.text")); // NOI18N
        jLabel10.setName("jLabel10"); // NOI18N

        username.setText(resourceMap.getString("username.text")); // NOI18N
        username.setName("username"); // NOI18N

        jLabel11.setText(resourceMap.getString("jLabel11.text")); // NOI18N
        jLabel11.setName("jLabel11"); // NOI18N

        password.setText(resourceMap.getString("password.text")); // NOI18N
        password.setName("password"); // NOI18N

        databaseTypeBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Microsoft SQL Server", "MySQL", "Oracle 11g", "ODBC", "PostgreSQL", "Hive/Hadoop (please use Generic JDBC Connection)", "SAP Hana", "Generic JDBC Connection" }));
        databaseTypeBox.setName("databaseTypeBox"); // NOI18N
        databaseTypeBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                changeDatabaseType(evt);
            }
        });

        serverName.setText(resourceMap.getString("serverName.text")); // NOI18N
        serverName.setName("serverName"); // NOI18N

        databaseName.setText(resourceMap.getString("databaseName.text")); // NOI18N
        databaseName.setName("databaseName"); // NOI18N

        schemaBox.setName("schemaBox"); // NOI18N

        jLabel12.setText(resourceMap.getString("jLabel12.text")); // NOI18N
        jLabel12.setName("jLabel12"); // NOI18N

        portLabel.setText(resourceMap.getString("portLabel.text")); // NOI18N
        portLabel.setName("portLabel"); // NOI18N

        portNumber.setName("portNumber"); // NOI18N

        tableIgnoreInternalQuotesBox.setText(resourceMap.getString("tableIgnoreInternalQuotesBox.text")); // NOI18N
        tableIgnoreInternalQuotesBox.setName("tableIgnoreInternalQuotesBox"); // NOI18N

        jLabel29.setText(resourceMap.getString("jLabel29.text")); // NOI18N
        jLabel29.setName("jLabel29"); // NOI18N

        tableQuoteCharBox.setText(resourceMap.getString("tableQuoteCharBox.text")); // NOI18N
        tableQuoteCharBox.setName("tableQuoteCharBox"); // NOI18N

        jLabel35.setText(resourceMap.getString("jLabel35.text")); // NOI18N
        jLabel35.setName("jLabel35"); // NOI18N

        replacementsBoxForTable.setName("replacementsBoxForTable"); // NOI18N

        jLabel36.setText(resourceMap.getString("jLabel36.text")); // NOI18N
        jLabel36.setName("jLabel36"); // NOI18N

        tableIgnoreCarriageReturnBox.setText(resourceMap.getString("tableIgnoreCarriageReturnBox.text")); // NOI18N
        tableIgnoreCarriageReturnBox.setName("tableIgnoreCarriageReturnBox"); // NOI18N

        btnConfigSSHTunnelTable.setAction(actionMap.get("configureSSHTunnel")); // NOI18N
        btnConfigSSHTunnelTable.setText(resourceMap.getString("btnConfigSSHTunnelTable.text")); // NOI18N
        btnConfigSSHTunnelTable.setName("btnConfigSSHTunnelTable"); // NOI18N

        chkEnableSSHTable.setAction(actionMap.get("isConfigureSSHTunnel")); // NOI18N
        chkEnableSSHTable.setText(resourceMap.getString("chkEnableSSHTable.text")); // NOI18N
        chkEnableSSHTable.setName("chkEnableSSHTable"); // NOI18N

        javax.swing.GroupLayout tablePanelLayout = new javax.swing.GroupLayout(tablePanel);
        tablePanel.setLayout(tablePanelLayout);
        tablePanelLayout.setHorizontalGroup(
            tablePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(tablePanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(tablePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(tablePanelLayout.createSequentialGroup()
                        .addGroup(tablePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel5)
                            .addComponent(jLabel6)
                            .addComponent(jLabel12))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(tablePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(catalogBox)
                            .addComponent(tableNameBox, javax.swing.GroupLayout.DEFAULT_SIZE, 352, Short.MAX_VALUE)
                            .addComponent(schemaBox))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 27, Short.MAX_VALUE)
                        .addComponent(chkEnableSSHTable)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnConfigSSHTunnelTable)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(updateTableButton))
                    .addGroup(tablePanelLayout.createSequentialGroup()
                        .addGroup(tablePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(tablePanelLayout.createSequentialGroup()
                                .addGroup(tablePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel7)
                                    .addComponent(serverNameLabel)
                                    .addComponent(databaseNameLabel)
                                    .addComponent(jLabel10))
                                .addGroup(tablePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(username)
                                    .addComponent(databaseTypeBox, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(serverName, javax.swing.GroupLayout.DEFAULT_SIZE, 134, Short.MAX_VALUE)
                                    .addComponent(databaseName, javax.swing.GroupLayout.PREFERRED_SIZE, 163, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addComponent(tableIgnoreInternalQuotesBox)
                            .addGroup(tablePanelLayout.createSequentialGroup()
                                .addComponent(jLabel35)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(tablePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel36)
                                    .addComponent(replacementsBoxForTable, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addGap(44, 44, 44)
                        .addGroup(tablePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(tableIgnoreCarriageReturnBox)
                            .addGroup(tablePanelLayout.createSequentialGroup()
                                .addComponent(jLabel29)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(tableQuoteCharBox, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(tablePanelLayout.createSequentialGroup()
                                .addComponent(portLabel)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(portNumber))
                            .addGroup(tablePanelLayout.createSequentialGroup()
                                .addComponent(jLabel11)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(password, javax.swing.GroupLayout.PREFERRED_SIZE, 134, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addContainerGap())
        );
        tablePanelLayout.setVerticalGroup(
            tablePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(tablePanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(tablePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(tableNameBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(updateTableButton)
                    .addComponent(btnConfigSSHTunnelTable)
                    .addComponent(chkEnableSSHTable))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(tablePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(catalogBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(tablePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(schemaBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel12))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(tablePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(databaseTypeBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(tablePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(serverNameLabel)
                    .addComponent(serverName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(portLabel)
                    .addComponent(portNumber, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(tablePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(databaseName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(databaseNameLabel))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(tablePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel10)
                    .addComponent(username, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel11)
                    .addComponent(password, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(tablePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(tablePanelLayout.createSequentialGroup()
                        .addComponent(tableIgnoreInternalQuotesBox)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(tablePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel35)
                            .addComponent(replacementsBoxForTable, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel36))
                    .addGroup(tablePanelLayout.createSequentialGroup()
                        .addGroup(tablePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel29)
                            .addComponent(tableQuoteCharBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(tableIgnoreCarriageReturnBox)))
                .addContainerGap(64, Short.MAX_VALUE))
        );

        URLPanel.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(-16777216,true)));
        URLPanel.setToolTipText(resourceMap.getString("URLPanel.toolTipText")); // NOI18N
        URLPanel.setName("URLPanel"); // NOI18N

        URLLabel.setText(resourceMap.getString("URLNameLabel.text")); // NOI18N
        URLLabel.setName("URLNameLabel"); // NOI18N

        txtURL.setName("txtURL"); // NOI18N

        URLUpdate.setAction(actionMap.get("updateURL")); // NOI18N
        URLUpdate.setText(resourceMap.getString("URLUpdate.text")); // NOI18N
        URLUpdate.setName("URLUpdate"); // NOI18N

        replacementsForURLLabel.setText(resourceMap.getString("replacementsForURLLabel.text")); // NOI18N
        replacementsForURLLabel.setName("replacementsForURLLabel"); // NOI18N

        replacementsBoxForURL.setName("replacementsBoxForURL"); // NOI18N

        jLabel42.setText(resourceMap.getString("jLabel42.text")); // NOI18N
        jLabel42.setName("jLabel42"); // NOI18N

        URLIgnoreCarriageReturnBox.setText(resourceMap.getString("URLIgnoreCarriageReturnBox.text")); // NOI18N
        URLIgnoreCarriageReturnBox.setName("URLIgnoreCarriageReturnBox"); // NOI18N

        URLIgnoreInternalQuotesBox.setText(resourceMap.getString("URLIgnoreInternalQuotesBox.text")); // NOI18N
        URLIgnoreInternalQuotesBox.setName("URLIgnoreInternalQuotesBox"); // NOI18N

        URLQuoteCharLabel.setText(resourceMap.getString("URLQuoteCharLabel.text")); // NOI18N
        URLQuoteCharLabel.setName("URLQuoteCharLabel"); // NOI18N

        URLQuoteCharBox.setText(resourceMap.getString("URLQuoteCharBox.text")); // NOI18N
        URLQuoteCharBox.setName("URLQuoteCharBox"); // NOI18N

        s3HostNameLabel2.setText(resourceMap.getString("s3HostNameLabel2.text")); // NOI18N
        s3HostNameLabel2.setName("s3HostNameLabel2"); // NOI18N

        txtName.setName("txtName"); // NOI18N

        URLIgnoreBadServerCertificatesBox.setText(resourceMap.getString("URLIgnoreBadServerCertificatesBox.text")); // NOI18N
        URLIgnoreBadServerCertificatesBox.setName("URLIgnoreBadServerCertificatesBox"); // NOI18N

        replacementsForURLLabel1.setText(resourceMap.getString("replacementsForURLLabel1.text")); // NOI18N
        replacementsForURLLabel1.setName("replacementsForURLLabel1"); // NOI18N

        URLMethodComboBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "GET", "POST" }));
        URLMethodComboBox.setName("URLMethodComboBox"); // NOI18N

        javax.swing.GroupLayout URLPanelLayout = new javax.swing.GroupLayout(URLPanel);
        URLPanel.setLayout(URLPanelLayout);
        URLPanelLayout.setHorizontalGroup(
            URLPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(URLPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(URLPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(URLPanelLayout.createSequentialGroup()
                        .addGroup(URLPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(URLLabel)
                            .addComponent(s3HostNameLabel2)
                            .addComponent(replacementsForURLLabel1))
                        .addGap(28, 28, 28)
                        .addGroup(URLPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(URLPanelLayout.createSequentialGroup()
                                .addComponent(URLMethodComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(53, 53, 53)
                                .addComponent(URLIgnoreBadServerCertificatesBox)
                                .addGap(360, 360, 360))
                            .addGroup(URLPanelLayout.createSequentialGroup()
                                .addGroup(URLPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txtName, javax.swing.GroupLayout.DEFAULT_SIZE, 433, Short.MAX_VALUE)
                                    .addComponent(txtURL, javax.swing.GroupLayout.DEFAULT_SIZE, 433, Short.MAX_VALUE))
                                .addGap(18, 18, 18)
                                .addComponent(URLUpdate)
                                .addGap(115, 115, 115))))
                    .addGroup(URLPanelLayout.createSequentialGroup()
                        .addComponent(replacementsForURLLabel)
                        .addGap(17, 17, 17)
                        .addGroup(URLPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, URLPanelLayout.createSequentialGroup()
                                .addComponent(URLIgnoreInternalQuotesBox)
                                .addGap(76, 76, 76)
                                .addComponent(URLQuoteCharLabel)
                                .addGap(18, 18, 18)
                                .addComponent(URLQuoteCharBox, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, URLPanelLayout.createSequentialGroup()
                                .addGap(10, 10, 10)
                                .addGroup(URLPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel42, javax.swing.GroupLayout.PREFERRED_SIZE, 139, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(URLPanelLayout.createSequentialGroup()
                                        .addComponent(replacementsBoxForURL, javax.swing.GroupLayout.PREFERRED_SIZE, 181, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(URLIgnoreCarriageReturnBox, javax.swing.GroupLayout.PREFERRED_SIZE, 161, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                        .addContainerGap(129, Short.MAX_VALUE))))
        );
        URLPanelLayout.setVerticalGroup(
            URLPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(URLPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(URLPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(URLLabel)
                    .addComponent(txtURL, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(URLUpdate))
                .addGap(18, 18, 18)
                .addGroup(URLPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(s3HostNameLabel2)
                    .addComponent(txtName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(URLPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(replacementsForURLLabel1)
                    .addGroup(URLPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(URLMethodComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(URLIgnoreBadServerCertificatesBox)))
                .addGap(18, 18, 18)
                .addGroup(URLPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(URLIgnoreCarriageReturnBox)
                    .addComponent(replacementsForURLLabel)
                    .addComponent(replacementsBoxForURL, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel42)
                .addGap(10, 10, 10)
                .addGroup(URLPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(URLQuoteCharBox)
                    .addComponent(URLIgnoreInternalQuotesBox, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(URLQuoteCharLabel))
                .addContainerGap(19, Short.MAX_VALUE))
        );

        queryPanel.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(-16777216,true)));
        queryPanel.setName("queryPanel"); // NOI18N
        queryPanel.setPreferredSize(new java.awt.Dimension(750, 217));

        jLabel13.setText(resourceMap.getString("jLabel13.text")); // NOI18N
        jLabel13.setName("jLabel13"); // NOI18N

        updateQueryButton.setAction(actionMap.get("updateQuery")); // NOI18N
        updateQueryButton.setName("updateQueryButton"); // NOI18N

        jLabel15.setText(resourceMap.getString("jLabel15.text")); // NOI18N
        jLabel15.setName("jLabel15"); // NOI18N

        queryServerNameLabel.setText(resourceMap.getString("queryServerNameLabel.text")); // NOI18N
        queryServerNameLabel.setName("queryServerNameLabel"); // NOI18N

        queryDatabaseNameLabel.setText(resourceMap.getString("queryDatabaseNameLabel.text")); // NOI18N
        queryDatabaseNameLabel.setName("queryDatabaseNameLabel"); // NOI18N

        jLabel18.setText(resourceMap.getString("jLabel18.text")); // NOI18N
        jLabel18.setName("jLabel18"); // NOI18N

        queryUsername.setText(resourceMap.getString("queryUsername.text")); // NOI18N
        queryUsername.setName("queryUsername"); // NOI18N

        jLabel19.setText(resourceMap.getString("jLabel19.text")); // NOI18N
        jLabel19.setName("jLabel19"); // NOI18N

        queryPassword.setText(resourceMap.getString("queryPassword.text")); // NOI18N
        queryPassword.setName("queryPassword"); // NOI18N

        queryDatabaseTypeBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Microsoft SQL Server", "MySQL", "Oracle 11g", "ODBC", "PostgreSQL", "Hive/Hadoop (please use Generic JDBC Connection)", "SAP Hana", "Generic JDBC Connection" }));
        queryDatabaseTypeBox.setName("queryDatabaseTypeBox"); // NOI18N
        queryDatabaseTypeBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                queryChangeDatabaseType(evt);
            }
        });

        queryServerName.setText(resourceMap.getString("queryServerName.text")); // NOI18N
        queryServerName.setName("queryServerName"); // NOI18N

        queryDatabaseName.setText(resourceMap.getString("queryDatabaseName.text")); // NOI18N
        queryDatabaseName.setName("queryDatabaseName"); // NOI18N

        jLabel26.setText(resourceMap.getString("jLabel26.text")); // NOI18N
        jLabel26.setName("jLabel26"); // NOI18N

        queryNameBox.setText(resourceMap.getString("queryNameBox.text")); // NOI18N
        queryNameBox.setName("queryNameBox"); // NOI18N

        jScrollPane2.setName("jScrollPane2"); // NOI18N

        queryBox.setColumns(20);
        queryBox.setRows(5);
        queryBox.setName("queryBox"); // NOI18N
        jScrollPane2.setViewportView(queryBox);

        queryPortLabel.setText(resourceMap.getString("queryPortLabel.text")); // NOI18N
        queryPortLabel.setName("queryPortLabel"); // NOI18N

        queryPortNumber.setName("queryPortNumber"); // NOI18N

        queryIgnoreInternalQuotesBox.setText(resourceMap.getString("queryIgnoreInternalQuotesBox.text")); // NOI18N
        queryIgnoreInternalQuotesBox.setActionCommand(resourceMap.getString("queryIgnoreInternalQuotesBox.actionCommand")); // NOI18N
        queryIgnoreInternalQuotesBox.setName("queryIgnoreInternalQuotesBox"); // NOI18N

        jLabel30.setText(resourceMap.getString("jLabel30.text")); // NOI18N
        jLabel30.setName("jLabel30"); // NOI18N

        queryQuoteCharBox.setText(resourceMap.getString("queryQuoteCharBox.text")); // NOI18N
        queryQuoteCharBox.setName("queryQuoteCharBox"); // NOI18N

        jLabel33.setText(resourceMap.getString("jLabel33.text")); // NOI18N
        jLabel33.setName("jLabel33"); // NOI18N

        jLabel34.setText(resourceMap.getString("jLabel34.text")); // NOI18N
        jLabel34.setName("jLabel34"); // NOI18N

        replacementsBoxForQuery.setName("replacementsBoxForQuery"); // NOI18N

        queryIgnoreCarriageReturnBox.setText(resourceMap.getString("queryIgnoreCarriageReturnBox.text")); // NOI18N
        queryIgnoreCarriageReturnBox.setName("queryIgnoreCarriageReturnBox"); // NOI18N

        btnConfigureSSHTunnelQuery.setAction(actionMap.get("configureSSHTunnel")); // NOI18N
        btnConfigureSSHTunnelQuery.setText(resourceMap.getString("btnConfigureSSHTunnelQuery.text")); // NOI18N
        btnConfigureSSHTunnelQuery.setName("btnConfigureSSHTunnelQuery"); // NOI18N

        chkEnableSSHQuery.setAction(actionMap.get("changeSSHTunelStatus")); // NOI18N
        chkEnableSSHQuery.setText(resourceMap.getString("chkEnableSSHQuery.text")); // NOI18N
        chkEnableSSHQuery.setName("chkEnableSSHQuery"); // NOI18N

        javax.swing.GroupLayout queryPanelLayout = new javax.swing.GroupLayout(queryPanel);
        queryPanel.setLayout(queryPanelLayout);
        queryPanelLayout.setHorizontalGroup(
            queryPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(queryPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(queryPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(queryPanelLayout.createSequentialGroup()
                        .addGroup(queryPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel15)
                            .addGroup(queryPanelLayout.createSequentialGroup()
                                .addGroup(queryPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(queryDatabaseNameLabel)
                                    .addComponent(jLabel19)
                                    .addComponent(jLabel18)
                                    .addComponent(queryPortLabel)
                                    .addComponent(jLabel26)
                                    .addComponent(queryServerNameLabel)
                                    .addComponent(jLabel33))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(queryPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(queryServerName, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 273, Short.MAX_VALUE)
                                    .addComponent(queryPortNumber, javax.swing.GroupLayout.PREFERRED_SIZE, 66, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(queryUsername, javax.swing.GroupLayout.DEFAULT_SIZE, 273, Short.MAX_VALUE)
                                    .addComponent(queryPassword, javax.swing.GroupLayout.DEFAULT_SIZE, 273, Short.MAX_VALUE)
                                    .addComponent(queryDatabaseName, javax.swing.GroupLayout.DEFAULT_SIZE, 273, Short.MAX_VALUE)
                                    .addComponent(queryDatabaseTypeBox, javax.swing.GroupLayout.Alignment.TRAILING, 0, 273, Short.MAX_VALUE)
                                    .addComponent(queryNameBox, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 273, Short.MAX_VALUE)
                                    .addComponent(replacementsBoxForQuery, javax.swing.GroupLayout.DEFAULT_SIZE, 273, Short.MAX_VALUE)
                                    .addComponent(jLabel34))))
                        .addGap(18, 18, 18))
                    .addGroup(queryPanelLayout.createSequentialGroup()
                        .addComponent(queryIgnoreInternalQuotesBox)
                        .addGap(97, 97, 97)))
                .addGroup(queryPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(queryPanelLayout.createSequentialGroup()
                        .addComponent(jLabel30)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(queryQuoteCharBox, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(queryIgnoreCarriageReturnBox)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, queryPanelLayout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(queryPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, queryPanelLayout.createSequentialGroup()
                                .addComponent(jLabel13)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(chkEnableSSHQuery)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(btnConfigureSSHTunnelQuery)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(updateQueryButton))
                            .addComponent(jScrollPane2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 377, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap())
        );
        queryPanelLayout.setVerticalGroup(
            queryPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(queryPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(queryPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(updateQueryButton)
                    .addComponent(jLabel26)
                    .addComponent(queryNameBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel13)
                    .addComponent(btnConfigureSSHTunnelQuery)
                    .addComponent(chkEnableSSHQuery))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(queryPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(queryPanelLayout.createSequentialGroup()
                        .addGroup(queryPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel15)
                            .addComponent(queryDatabaseTypeBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(queryPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(queryServerNameLabel)
                            .addComponent(queryServerName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(queryPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(queryPortLabel)
                            .addComponent(queryPortNumber, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(queryPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(queryDatabaseNameLabel)
                            .addComponent(queryDatabaseName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(queryPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel18)
                            .addComponent(queryUsername, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(queryPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel19)
                            .addComponent(queryPassword, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(queryPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(replacementsBoxForQuery, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel33))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel34)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, queryPanelLayout.createSequentialGroup()
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 154, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 67, Short.MAX_VALUE)
                        .addComponent(queryIgnoreCarriageReturnBox)
                        .addGap(19, 19, 19)))
                .addGroup(queryPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(queryIgnoreInternalQuotesBox)
                    .addComponent(queryQuoteCharBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel30))
                .addContainerGap())
        );

        sapQueryPanel.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(-16777216,true)));
        sapQueryPanel.setName("sapQueryPanel"); // NOI18N

        updateSAPQueryButton.setAction(actionMap.get("updateSAPQuery")); // NOI18N
        updateSAPQueryButton.setText(resourceMap.getString("updateSAPQueryButton.text")); // NOI18N
        updateSAPQueryButton.setName("updateSAPQueryButton"); // NOI18N

        routerHostLabel1.setText(resourceMap.getString("routerHostLabel1.text")); // NOI18N
        routerHostLabel1.setName("routerHostLabel1"); // NOI18N

        serverHostLabel1.setText(resourceMap.getString("serverHostLabel1.text")); // NOI18N
        serverHostLabel1.setName("serverHostLabel1"); // NOI18N

        systemNumberLabel1.setText(resourceMap.getString("systemNumberLabel1.text")); // NOI18N
        systemNumberLabel1.setName("systemNumberLabel1"); // NOI18N

        userNameLabel1.setText(resourceMap.getString("userNameLabel1.text")); // NOI18N
        userNameLabel1.setName("userNameLabel1"); // NOI18N

        queryNameLabel.setText(resourceMap.getString("queryNameLabel.text")); // NOI18N
        queryNameLabel.setName("queryNameLabel"); // NOI18N

        sapQserverHostBox.setName("sapQserverHostBox"); // NOI18N

        objectServerNameLabel1.setText(resourceMap.getString("objectServerNameLabel1.text")); // NOI18N
        objectServerNameLabel1.setName("objectServerNameLabel1"); // NOI18N

        sapQrouterHostBox.setName("sapQrouterHostBox"); // NOI18N

        routerPortLabel1.setText(resourceMap.getString("routerPortLabel1.text")); // NOI18N
        routerPortLabel1.setName("routerPortLabel1"); // NOI18N

        sapQsystemNumberBox.setName("sapQsystemNumberBox"); // NOI18N

        sapQuserNameBox.setName("sapQuserNameBox"); // NOI18N

        passwordLabel1.setText(resourceMap.getString("passwordLabel1.text")); // NOI18N
        passwordLabel1.setName("passwordLabel1"); // NOI18N

        clientLabel1.setText(resourceMap.getString("clientLabel1.text")); // NOI18N
        clientLabel1.setName("clientLabel1"); // NOI18N

        sapQrouterPortBox.setName("sapQrouterPortBox"); // NOI18N

        sapQclientBox.setName("sapQclientBox"); // NOI18N

        sapQpasswordBox.setName("sapQpasswordBox"); // NOI18N

        sapQobjectServerNameBox.setName("sapQobjectServerNameBox"); // NOI18N

        sapQobjectIgnoreInternalQuotesBox.setText(resourceMap.getString("sapQobjectIgnoreInternalQuotesBox.text")); // NOI18N
        sapQobjectIgnoreInternalQuotesBox.setName("sapQobjectIgnoreInternalQuotesBox"); // NOI18N

        jLabel32.setText(resourceMap.getString("jLabel32.text")); // NOI18N
        jLabel32.setName("jLabel32"); // NOI18N

        sapQobjectQuoteCharBox.setName("sapQobjectQuoteCharBox"); // NOI18N

        sapQueryNameLabel.setName("sapQueryNameLabel"); // NOI18N

        SAPQueryDetailsButton.setAction(actionMap.get("showQueryDetailsDialog")); // NOI18N
        SAPQueryDetailsButton.setText(resourceMap.getString("SAPQueryDetailsButton.text")); // NOI18N
        SAPQueryDetailsButton.setName("SAPQueryDetailsButton"); // NOI18N

        javax.swing.GroupLayout sapQueryPanelLayout = new javax.swing.GroupLayout(sapQueryPanel);
        sapQueryPanel.setLayout(sapQueryPanelLayout);
        sapQueryPanelLayout.setHorizontalGroup(
            sapQueryPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(sapQueryPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(sapQueryPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(sapQobjectIgnoreInternalQuotesBox)
                    .addGroup(sapQueryPanelLayout.createSequentialGroup()
                        .addGroup(sapQueryPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(serverHostLabel1)
                            .addComponent(systemNumberLabel1)
                            .addComponent(routerHostLabel1)
                            .addComponent(userNameLabel1)
                            .addComponent(queryNameLabel))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(sapQueryPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, sapQueryPanelLayout.createSequentialGroup()
                                .addGroup(sapQueryPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(sapQuserNameBox, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 199, Short.MAX_VALUE)
                                    .addComponent(sapQsystemNumberBox, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 199, Short.MAX_VALUE)
                                    .addComponent(sapQrouterHostBox, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 199, Short.MAX_VALUE)
                                    .addComponent(sapQserverHostBox, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 175, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(35, 35, 35)
                                .addGroup(sapQueryPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(sapQueryPanelLayout.createSequentialGroup()
                                        .addGap(1, 1, 1)
                                        .addGroup(sapQueryPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jLabel32)
                                            .addComponent(passwordLabel1)
                                            .addComponent(clientLabel1)))
                                    .addComponent(routerPortLabel1)
                                    .addComponent(objectServerNameLabel1)))
                            .addComponent(sapQueryNameLabel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 205, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(sapQueryPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(SAPQueryDetailsButton, javax.swing.GroupLayout.DEFAULT_SIZE, 200, Short.MAX_VALUE)
                            .addComponent(sapQobjectServerNameBox, javax.swing.GroupLayout.PREFERRED_SIZE, 175, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(sapQclientBox, javax.swing.GroupLayout.DEFAULT_SIZE, 200, Short.MAX_VALUE)
                            .addComponent(sapQpasswordBox, javax.swing.GroupLayout.DEFAULT_SIZE, 200, Short.MAX_VALUE)
                            .addComponent(sapQrouterPortBox, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(sapQobjectQuoteCharBox, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(80, 80, 80)
                .addComponent(updateSAPQueryButton, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        sapQueryPanelLayout.setVerticalGroup(
            sapQueryPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(sapQueryPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(sapQueryPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(sapQueryPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(updateSAPQueryButton)
                        .addComponent(SAPQueryDetailsButton))
                    .addGroup(sapQueryPanelLayout.createSequentialGroup()
                        .addGap(4, 4, 4)
                        .addGroup(sapQueryPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(queryNameLabel)
                            .addComponent(sapQueryNameLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 12, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(sapQueryPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(sapQrouterHostBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(routerHostLabel1)
                    .addComponent(routerPortLabel1)
                    .addComponent(sapQrouterPortBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(15, 15, 15)
                .addGroup(sapQueryPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(serverHostLabel1)
                    .addComponent(sapQserverHostBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(sapQobjectServerNameBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(objectServerNameLabel1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(sapQueryPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(systemNumberLabel1)
                    .addComponent(sapQsystemNumberBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(sapQclientBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(clientLabel1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(sapQueryPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(sapQuserNameBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(userNameLabel1)
                    .addComponent(sapQpasswordBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(passwordLabel1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(sapQueryPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(sapQobjectIgnoreInternalQuotesBox)
                    .addComponent(jLabel32)
                    .addComponent(sapQobjectQuoteCharBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(140, Short.MAX_VALUE))
        );

        sapObjectPanel.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(-16777216,true)));
        sapObjectPanel.setName("sapObjectPanel"); // NOI18N

        updateObjectButton.setAction(actionMap.get("updateSAPObject")); // NOI18N
        updateObjectButton.setText(resourceMap.getString("updateObjectButton.text")); // NOI18N
        updateObjectButton.setName("updateObjectButton"); // NOI18N

        routerHostLabel.setText(resourceMap.getString("routerHostLabel.text")); // NOI18N
        routerHostLabel.setName("routerHostLabel"); // NOI18N

        serverHostLabel.setText(resourceMap.getString("serverHostLabel.text")); // NOI18N
        serverHostLabel.setName("serverHostLabel"); // NOI18N

        systemNumberLabel.setText(resourceMap.getString("systemNumberLabel.text")); // NOI18N
        systemNumberLabel.setName("systemNumberLabel"); // NOI18N

        userNameLabel.setText(resourceMap.getString("userNameLabel.text")); // NOI18N
        userNameLabel.setName("userNameLabel"); // NOI18N

        objectNameLabel.setText(resourceMap.getString("objectNameLabel.text")); // NOI18N
        objectNameLabel.setName("objectNameLabel"); // NOI18N

        serverHostBox.setName("serverHostBox"); // NOI18N

        objectServerNameLabel.setText(resourceMap.getString("objectServerNameLabel.text")); // NOI18N
        objectServerNameLabel.setName("objectServerNameLabel"); // NOI18N

        routerHostBox.setName("routerHostBox"); // NOI18N

        routerPortLabel.setText(resourceMap.getString("routerPortLabel.text")); // NOI18N
        routerPortLabel.setName("routerPortLabel"); // NOI18N

        systemNumberBox.setName("systemNumberBox"); // NOI18N

        userNameBox.setName("userNameBox"); // NOI18N

        passwordLabel.setText(resourceMap.getString("passwordLabel.text")); // NOI18N
        passwordLabel.setName("passwordLabel"); // NOI18N

        clientLabel.setText(resourceMap.getString("clientLabel.text")); // NOI18N
        clientLabel.setName("clientLabel"); // NOI18N

        routerPortBox.setName("routerPortBox"); // NOI18N

        clientBox.setName("clientBox"); // NOI18N

        passwordBox.setText(resourceMap.getString("passwordBox.text")); // NOI18N
        passwordBox.setName("passwordBox"); // NOI18N

        objectServerNameBox.setName("objectServerNameBox"); // NOI18N

        objectIgnoreInternalQuotesBox.setText(resourceMap.getString("objectIgnoreInternalQuotesBox.text")); // NOI18N
        objectIgnoreInternalQuotesBox.setName("objectIgnoreInternalQuotesBox"); // NOI18N

        jLabel31.setText(resourceMap.getString("jLabel31.text")); // NOI18N
        jLabel31.setName("jLabel31"); // NOI18N

        objectQuoteCharBox.setText(resourceMap.getString("objectQuoteCharBox.text")); // NOI18N
        objectQuoteCharBox.setName("objectQuoteCharBox"); // NOI18N

        sapSourceNameLabel.setText(resourceMap.getString("sapSourceNameLabel.text")); // NOI18N
        sapSourceNameLabel.setName("sapSourceNameLabel"); // NOI18N

        SAPFunctionDetailsButton.setAction(actionMap.get("showFunctionDetailsDialog")); // NOI18N
        SAPFunctionDetailsButton.setText(resourceMap.getString("SAPFunctionDetailsButton.text")); // NOI18N
        SAPFunctionDetailsButton.setName("SAPFunctionDetailsButton"); // NOI18N

        javax.swing.GroupLayout sapObjectPanelLayout = new javax.swing.GroupLayout(sapObjectPanel);
        sapObjectPanel.setLayout(sapObjectPanelLayout);
        sapObjectPanelLayout.setHorizontalGroup(
            sapObjectPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(sapObjectPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(sapObjectPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(objectIgnoreInternalQuotesBox)
                    .addGroup(sapObjectPanelLayout.createSequentialGroup()
                        .addGroup(sapObjectPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(serverHostLabel)
                            .addComponent(systemNumberLabel)
                            .addComponent(routerHostLabel)
                            .addComponent(userNameLabel)
                            .addComponent(objectNameLabel))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(sapObjectPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, sapObjectPanelLayout.createSequentialGroup()
                                .addGroup(sapObjectPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(userNameBox, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 203, Short.MAX_VALUE)
                                    .addComponent(systemNumberBox, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 203, Short.MAX_VALUE)
                                    .addComponent(routerHostBox, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 203, Short.MAX_VALUE)
                                    .addComponent(serverHostBox, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 175, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(35, 35, 35)
                                .addGroup(sapObjectPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(sapObjectPanelLayout.createSequentialGroup()
                                        .addGap(1, 1, 1)
                                        .addGroup(sapObjectPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jLabel31)
                                            .addComponent(passwordLabel)
                                            .addComponent(clientLabel)))
                                    .addComponent(routerPortLabel)
                                    .addComponent(objectServerNameLabel)))
                            .addComponent(sapSourceNameLabel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 205, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(sapObjectPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(SAPFunctionDetailsButton, javax.swing.GroupLayout.DEFAULT_SIZE, 203, Short.MAX_VALUE)
                            .addComponent(objectServerNameBox, javax.swing.GroupLayout.PREFERRED_SIZE, 175, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(clientBox, javax.swing.GroupLayout.DEFAULT_SIZE, 203, Short.MAX_VALUE)
                            .addComponent(passwordBox, javax.swing.GroupLayout.DEFAULT_SIZE, 203, Short.MAX_VALUE)
                            .addComponent(routerPortBox, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(objectQuoteCharBox, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(80, 80, 80)
                .addComponent(updateObjectButton, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        sapObjectPanelLayout.setVerticalGroup(
            sapObjectPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(sapObjectPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(sapObjectPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(sapObjectPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(updateObjectButton)
                        .addComponent(SAPFunctionDetailsButton))
                    .addGroup(sapObjectPanelLayout.createSequentialGroup()
                        .addGap(4, 4, 4)
                        .addGroup(sapObjectPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(objectNameLabel)
                            .addComponent(sapSourceNameLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 12, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(sapObjectPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(routerHostBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(routerHostLabel)
                    .addComponent(routerPortLabel)
                    .addComponent(routerPortBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(15, 15, 15)
                .addGroup(sapObjectPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(serverHostLabel)
                    .addComponent(serverHostBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(objectServerNameBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(objectServerNameLabel))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(sapObjectPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(systemNumberLabel)
                    .addComponent(systemNumberBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(clientBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(clientLabel))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(sapObjectPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(userNameBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(userNameLabel)
                    .addComponent(passwordBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(passwordLabel))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(sapObjectPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(objectIgnoreInternalQuotesBox)
                    .addComponent(jLabel31)
                    .addComponent(objectQuoteCharBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(140, Short.MAX_VALUE))
        );

        S3ObjectPanel.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(-16777216,true)));
        S3ObjectPanel.setName("S3ObjectPanel"); // NOI18N

        s3HostNameLabel.setText(resourceMap.getString("s3HostNameLabel.text")); // NOI18N
        s3HostNameLabel.setName("s3HostNameLabel"); // NOI18N

        txtS3HostName.setText(resourceMap.getString("txtS3HostName.text")); // NOI18N
        txtS3HostName.setName("txtS3HostName"); // NOI18N

        crendentialsFileLabel.setText(resourceMap.getString("crendentialsFileLabel.text")); // NOI18N
        crendentialsFileLabel.setName("crendentialsFileLabel"); // NOI18N

        txtCrendentialsFile.setText(resourceMap.getString("txtCrendentialsFile.text")); // NOI18N
        txtCrendentialsFile.setName("txtCrendentialsFile"); // NOI18N

        browseCrendentoalsFileBtn.setAction(actionMap.get("browseCredentialsFile")); // NOI18N
        browseCrendentoalsFileBtn.setText(resourceMap.getString("browseCrendentoalsFileBtn.text")); // NOI18N
        browseCrendentoalsFileBtn.setName("browseCrendentoalsFileBtn"); // NOI18N

        jLabel39.setText(resourceMap.getString("jLabel39.text")); // NOI18N
        jLabel39.setName("jLabel39"); // NOI18N

        bucketNameLabel.setText(resourceMap.getString("bucketNameLabel.text")); // NOI18N
        bucketNameLabel.setName("bucketNameLabel"); // NOI18N

        txtBucketName.setText(resourceMap.getString("txtBucketName.text")); // NOI18N
        txtBucketName.setName("txtBucketName"); // NOI18N

        bucketKeyLabel.setText(resourceMap.getString("bucketKeyLabel.text")); // NOI18N
        bucketKeyLabel.setName("bucketKeyLabel"); // NOI18N

        txtBucketKey.setText(resourceMap.getString("txtBucketKey.text")); // NOI18N
        txtBucketKey.setName("txtBucketKey"); // NOI18N

        s3ObjectUpdate.setAction(actionMap.get("updateS3Object")); // NOI18N
        s3ObjectUpdate.setText(resourceMap.getString("s3ObjectUpdate.text")); // NOI18N
        s3ObjectUpdate.setName("s3ObjectUpdate"); // NOI18N

        replacementsForS3ObjectLabel.setText(resourceMap.getString("replacementsForS3ObjectLabel.text")); // NOI18N
        replacementsForS3ObjectLabel.setName("replacementsForS3ObjectLabel"); // NOI18N

        replacementsBoxForS3Object.setText(resourceMap.getString("replacementsBoxForS3Object.text")); // NOI18N
        replacementsBoxForS3Object.setName("replacementsBoxForS3Object"); // NOI18N

        jLabel41.setText(resourceMap.getString("jLabel41.text")); // NOI18N
        jLabel41.setName("jLabel41"); // NOI18N

        s3ObjectIgnoreCarriageReturnBox.setText(resourceMap.getString("s3ObjectIgnoreCarriageReturnBox.text")); // NOI18N
        s3ObjectIgnoreCarriageReturnBox.setName("s3ObjectIgnoreCarriageReturnBox"); // NOI18N

        s3ObjectIgnoreInternalQuotesBox.setText(resourceMap.getString("s3ObjectIgnoreInternalQuotesBox.text")); // NOI18N
        s3ObjectIgnoreInternalQuotesBox.setName("s3ObjectIgnoreInternalQuotesBox"); // NOI18N

        s3ObjectQuoteCharLabel.setText(resourceMap.getString("s3ObjectQuoteCharLabel.text")); // NOI18N
        s3ObjectQuoteCharLabel.setName("s3ObjectQuoteCharLabel"); // NOI18N

        s3ObjectQuoteCharBox.setText(resourceMap.getString("s3ObjectQuoteCharBox.text")); // NOI18N
        s3ObjectQuoteCharBox.setName("s3ObjectQuoteCharBox"); // NOI18N

        javax.swing.GroupLayout S3ObjectPanelLayout = new javax.swing.GroupLayout(S3ObjectPanel);
        S3ObjectPanel.setLayout(S3ObjectPanelLayout);
        S3ObjectPanelLayout.setHorizontalGroup(
            S3ObjectPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, S3ObjectPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(S3ObjectPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(S3ObjectPanelLayout.createSequentialGroup()
                        .addGroup(S3ObjectPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(bucketNameLabel)
                            .addComponent(bucketKeyLabel)
                            .addComponent(s3HostNameLabel)
                            .addComponent(crendentialsFileLabel))
                        .addGap(18, 18, 18)
                        .addGroup(S3ObjectPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, S3ObjectPanelLayout.createSequentialGroup()
                                .addGroup(S3ObjectPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txtS3HostName, javax.swing.GroupLayout.DEFAULT_SIZE, 244, Short.MAX_VALUE)
                                    .addComponent(txtCrendentialsFile, javax.swing.GroupLayout.DEFAULT_SIZE, 244, Short.MAX_VALUE)
                                    .addComponent(txtBucketName, javax.swing.GroupLayout.DEFAULT_SIZE, 244, Short.MAX_VALUE)
                                    .addComponent(txtBucketKey, javax.swing.GroupLayout.DEFAULT_SIZE, 244, Short.MAX_VALUE))
                                .addGroup(S3ObjectPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addGroup(S3ObjectPanelLayout.createSequentialGroup()
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(browseCrendentoalsFileBtn)
                                        .addGap(132, 132, 132))
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, S3ObjectPanelLayout.createSequentialGroup()
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 82, Short.MAX_VALUE)
                                        .addGroup(S3ObjectPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                            .addGroup(S3ObjectPanelLayout.createSequentialGroup()
                                                .addComponent(s3ObjectQuoteCharLabel)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                .addComponent(s3ObjectQuoteCharBox, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE))
                                            .addComponent(s3ObjectUpdate))
                                        .addGap(16, 16, 16))))
                            .addComponent(jLabel39)))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, S3ObjectPanelLayout.createSequentialGroup()
                        .addComponent(replacementsForS3ObjectLabel)
                        .addGap(17, 17, 17)
                        .addGroup(S3ObjectPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(s3ObjectIgnoreInternalQuotesBox, javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, S3ObjectPanelLayout.createSequentialGroup()
                                .addGap(10, 10, 10)
                                .addGroup(S3ObjectPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel41, javax.swing.GroupLayout.PREFERRED_SIZE, 139, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(S3ObjectPanelLayout.createSequentialGroup()
                                        .addComponent(replacementsBoxForS3Object, javax.swing.GroupLayout.PREFERRED_SIZE, 181, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(s3ObjectIgnoreCarriageReturnBox, javax.swing.GroupLayout.PREFERRED_SIZE, 161, javax.swing.GroupLayout.PREFERRED_SIZE))))))))
        );
        S3ObjectPanelLayout.setVerticalGroup(
            S3ObjectPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(S3ObjectPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(S3ObjectPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(s3HostNameLabel)
                    .addComponent(txtS3HostName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(s3ObjectUpdate))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(S3ObjectPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(crendentialsFileLabel)
                    .addComponent(txtCrendentialsFile, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(browseCrendentoalsFileBtn))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel39)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(S3ObjectPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(bucketNameLabel)
                    .addComponent(txtBucketName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(S3ObjectPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtBucketKey, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bucketKeyLabel))
                .addGap(33, 33, 33)
                .addGroup(S3ObjectPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(s3ObjectIgnoreCarriageReturnBox)
                    .addComponent(replacementsForS3ObjectLabel)
                    .addComponent(replacementsBoxForS3Object, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel41)
                .addGap(10, 10, 10)
                .addGroup(S3ObjectPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(s3ObjectIgnoreInternalQuotesBox)
                    .addComponent(s3ObjectQuoteCharLabel)
                    .addComponent(s3ObjectQuoteCharBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout masterPanelLayout = new javax.swing.GroupLayout(masterPanel);
        masterPanel.setLayout(masterPanelLayout);
        masterPanelLayout.setHorizontalGroup(
            masterPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(filePanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(masterPanelLayout.createSequentialGroup()
                .addGap(1, 1, 1)
                .addGroup(masterPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(queryPanel, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 798, Short.MAX_VALUE)
                    .addComponent(sapQueryPanel, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(sapObjectPanel, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(tablePanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
            .addGroup(masterPanelLayout.createSequentialGroup()
                .addComponent(S3ObjectPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(223, Short.MAX_VALUE))
            .addGroup(masterPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, masterPanelLayout.createSequentialGroup()
                    .addComponent(URLPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(86, Short.MAX_VALUE)))
        );
        masterPanelLayout.setVerticalGroup(
            masterPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(masterPanelLayout.createSequentialGroup()
                .addGap(11, 11, 11)
                .addComponent(tablePanel, javax.swing.GroupLayout.DEFAULT_SIZE, 337, Short.MAX_VALUE))
            .addGroup(masterPanelLayout.createSequentialGroup()
                .addGap(11, 11, 11)
                .addComponent(filePanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGroup(masterPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(sapObjectPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(masterPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(sapQueryPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(masterPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(queryPanel, javax.swing.GroupLayout.DEFAULT_SIZE, 337, Short.MAX_VALUE))
            .addGroup(masterPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(S3ObjectPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(69, Short.MAX_VALUE))
            .addGroup(masterPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, masterPanelLayout.createSequentialGroup()
                    .addComponent(URLPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(122, Short.MAX_VALUE)))
        );

        URLPanel.getAccessibleContext().setAccessibleName(resourceMap.getString("URLPanel.AccessibleContext.accessibleName")); // NOI18N
        URLPanel.getAccessibleContext().setAccessibleParent(masterPanel);

        javax.swing.GroupLayout sourcesPanelLayout = new javax.swing.GroupLayout(sourcesPanel);
        sourcesPanel.setLayout(sourcesPanelLayout);
        sourcesPanelLayout.setHorizontalGroup(
            sourcesPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(sourcesPanelLayout.createSequentialGroup()
                .addGroup(sourcesPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(sourcesPanelLayout.createSequentialGroup()
                        .addContainerGap(649, Short.MAX_VALUE)
                        .addComponent(okButton, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(10, 10, 10)
                        .addComponent(cancelButton, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(sourcesPanelLayout.createSequentialGroup()
                        .addGap(20, 20, 20)
                        .addGroup(sourcesPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(masterPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 799, Short.MAX_VALUE)
                            .addGroup(sourcesPanelLayout.createSequentialGroup()
                                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 637, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(sourcesPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(addSAPDSOButton, javax.swing.GroupLayout.DEFAULT_SIZE, 156, Short.MAX_VALUE)
                                    .addComponent(addSAPQueryButton, javax.swing.GroupLayout.DEFAULT_SIZE, 156, Short.MAX_VALUE)
                                    .addComponent(addObjectsButton, javax.swing.GroupLayout.DEFAULT_SIZE, 156, Short.MAX_VALUE)
                                    .addComponent(addTablesButton, javax.swing.GroupLayout.DEFAULT_SIZE, 156, Short.MAX_VALUE)
                                    .addComponent(addQueryButton, javax.swing.GroupLayout.DEFAULT_SIZE, 156, Short.MAX_VALUE)
                                    .addComponent(removeSourceButton, javax.swing.GroupLayout.DEFAULT_SIZE, 156, Short.MAX_VALUE)
                                    .addComponent(addFileButton, javax.swing.GroupLayout.DEFAULT_SIZE, 156, Short.MAX_VALUE)
                                    .addComponent(btnAddS3Object, javax.swing.GroupLayout.DEFAULT_SIZE, 156, Short.MAX_VALUE)
                                    .addComponent(btnAddURL, javax.swing.GroupLayout.DEFAULT_SIZE, 156, Short.MAX_VALUE))))))
                .addContainerGap())
        );
        sourcesPanelLayout.setVerticalGroup(
            sourcesPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(sourcesPanelLayout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addGroup(sourcesPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(sourcesPanelLayout.createSequentialGroup()
                        .addComponent(addFileButton)
                        .addGap(7, 7, 7)
                        .addComponent(addTablesButton)
                        .addGap(7, 7, 7)
                        .addComponent(addQueryButton)
                        .addGap(7, 7, 7)
                        .addComponent(removeSourceButton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnAddURL)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnAddS3Object)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 8, Short.MAX_VALUE)
                        .addComponent(addObjectsButton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(addSAPQueryButton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(addSAPDSOButton))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 260, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(masterPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 348, Short.MAX_VALUE)
                .addGap(18, 18, 18)
                .addGroup(sourcesPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(okButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cancelButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(13, 13, 13))
        );

        editTabs.addTab(resourceMap.getString("sourcesPanel.TabConstraints.tabTitle"), sourcesPanel); // NOI18N

        schedulePanel.setName("schedulePanel"); // NOI18N

        jButton3.setAction(actionMap.get("okButton")); // NOI18N
        jButton3.setName("jButton3"); // NOI18N
        jButton3.setPreferredSize(new java.awt.Dimension(50, 23));

        jButton4.setAction(actionMap.get("cancel")); // NOI18N
        jButton4.setName("jButton4"); // NOI18N
        jButton4.setPreferredSize(new java.awt.Dimension(50, 23));

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(resourceMap.getString("jPanel1.border.title"))); // NOI18N
        jPanel1.setName("jPanel1"); // NOI18N

        recurrenceGroup.add(dailyButton);
        dailyButton.setSelected(true);
        dailyButton.setText(resourceMap.getString("dailyButton.text")); // NOI18N
        dailyButton.setName("dailyButton"); // NOI18N
        dailyButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                recurrenceClick(evt);
            }
        });

        recurrenceGroup.add(weeklyButton);
        weeklyButton.setText(resourceMap.getString("weeklyButton.text")); // NOI18N
        weeklyButton.setName("weeklyButton"); // NOI18N
        weeklyButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                recurrenceClick(evt);
            }
        });

        recurrenceGroup.add(monthlyButton);
        monthlyButton.setText(resourceMap.getString("monthlyButton.text")); // NOI18N
        monthlyButton.setName("monthlyButton"); // NOI18N
        monthlyButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                recurrenceClick(evt);
            }
        });

        weekPanel.setName("weekPanel"); // NOI18N

        mondayBox.setText(resourceMap.getString("mondayBox.text")); // NOI18N
        mondayBox.setName("mondayBox"); // NOI18N

        thursdayBox.setText(resourceMap.getString("thursdayBox.text")); // NOI18N
        thursdayBox.setName("thursdayBox"); // NOI18N

        sundayBox.setText(resourceMap.getString("sundayBox.text")); // NOI18N
        sundayBox.setName("sundayBox"); // NOI18N

        tuesdayBox.setText(resourceMap.getString("tuesdayBox.text")); // NOI18N
        tuesdayBox.setName("tuesdayBox"); // NOI18N

        fridayBox.setText(resourceMap.getString("fridayBox.text")); // NOI18N
        fridayBox.setName("fridayBox"); // NOI18N

        wednesdayBox.setText(resourceMap.getString("wednesdayBox.text")); // NOI18N
        wednesdayBox.setName("wednesdayBox"); // NOI18N

        saturdayBox.setText(resourceMap.getString("saturdayBox.text")); // NOI18N
        saturdayBox.setName("saturdayBox"); // NOI18N

        javax.swing.GroupLayout weekPanelLayout = new javax.swing.GroupLayout(weekPanel);
        weekPanel.setLayout(weekPanelLayout);
        weekPanelLayout.setHorizontalGroup(
            weekPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(weekPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(weekPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(mondayBox)
                    .addComponent(thursdayBox)
                    .addComponent(sundayBox))
                .addGap(18, 18, 18)
                .addGroup(weekPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(tuesdayBox)
                    .addComponent(fridayBox))
                .addGap(18, 18, 18)
                .addGroup(weekPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(wednesdayBox)
                    .addComponent(saturdayBox))
                .addContainerGap(40, Short.MAX_VALUE))
        );
        weekPanelLayout.setVerticalGroup(
            weekPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(weekPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(weekPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(weekPanelLayout.createSequentialGroup()
                        .addComponent(wednesdayBox)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(saturdayBox))
                    .addGroup(weekPanelLayout.createSequentialGroup()
                        .addComponent(tuesdayBox)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(fridayBox))
                    .addGroup(weekPanelLayout.createSequentialGroup()
                        .addComponent(mondayBox)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(thursdayBox)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(sundayBox)))
                .addContainerGap(30, Short.MAX_VALUE))
        );

        monthPanel.setName("monthPanel"); // NOI18N

        jLabel22.setText(resourceMap.getString("jLabel22.text")); // NOI18N
        jLabel22.setName("jLabel22"); // NOI18N

        dayOfMonthBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        dayOfMonthBox.setName("dayOfMonthBox"); // NOI18N

        jLabel23.setText(resourceMap.getString("jLabel23.text")); // NOI18N
        jLabel23.setName("jLabel23"); // NOI18N

        javax.swing.GroupLayout monthPanelLayout = new javax.swing.GroupLayout(monthPanel);
        monthPanel.setLayout(monthPanelLayout);
        monthPanelLayout.setHorizontalGroup(
            monthPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(monthPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel22)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(dayOfMonthBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel23)
                .addContainerGap(102, Short.MAX_VALUE))
        );
        monthPanelLayout.setVerticalGroup(
            monthPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(monthPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(monthPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel22)
                    .addComponent(dayOfMonthBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel23))
                .addContainerGap(81, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(dailyButton)
                    .addComponent(weeklyButton)
                    .addComponent(monthlyButton))
                .addGap(36, 36, 36)
                .addComponent(weekPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(monthPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(weekPanel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(dailyButton)
                        .addGap(18, 18, 18)
                        .addComponent(weeklyButton)
                        .addGap(18, 18, 18)
                        .addComponent(monthlyButton))
                    .addComponent(monthPanel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(resourceMap.getString("jPanel2.border.title"))); // NOI18N
        jPanel2.setName("jPanel2"); // NOI18N

        jLabel14.setText(resourceMap.getString("jLabel14.text")); // NOI18N
        jLabel14.setName("jLabel14"); // NOI18N

        hourBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12" }));
        hourBox.setName("hourBox"); // NOI18N

        jLabel20.setText(resourceMap.getString("jLabel20.text")); // NOI18N
        jLabel20.setName("jLabel20"); // NOI18N

        minuteBox.setName("minuteBox"); // NOI18N

        ampmBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "AM", "PM" }));
        ampmBox.setName("ampmBox"); // NOI18N

        retryOnFailureBox.setText(resourceMap.getString("retryOnFailureBox.text")); // NOI18N
        retryOnFailureBox.setName("retryOnFailureBox"); // NOI18N

        retryTimesBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "1", "2", "3", "4", "5" }));
        retryTimesBox.setName("retryTimesBox"); // NOI18N

        jLabel21.setText(resourceMap.getString("jLabel21.text")); // NOI18N
        jLabel21.setName("jLabel21"); // NOI18N

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(retryOnFailureBox)
                        .addGap(18, 18, 18)
                        .addComponent(retryTimesBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel21))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel14)
                        .addGap(18, 18, 18)
                        .addComponent(hourBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel20)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(minuteBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(ampmBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(589, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel14)
                    .addComponent(hourBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel20)
                    .addComponent(minuteBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(ampmBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(retryOnFailureBox)
                    .addComponent(retryTimesBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel21))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        scheduleBox.setText(resourceMap.getString("scheduleBox.text")); // NOI18N
        scheduleBox.setName("scheduleBox"); // NOI18N

        jLabel40.setText(resourceMap.getString("jLabel40.text")); // NOI18N
        jLabel40.setName("jLabel40"); // NOI18N

        jLabel43.setText(resourceMap.getString("jLabel43.text")); // NOI18N
        jLabel43.setName("jLabel43"); // NOI18N

        javax.swing.GroupLayout schedulePanelLayout = new javax.swing.GroupLayout(schedulePanel);
        schedulePanel.setLayout(schedulePanelLayout);
        schedulePanelLayout.setHorizontalGroup(
            schedulePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(schedulePanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(schedulePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, schedulePanelLayout.createSequentialGroup()
                        .addComponent(jButton3, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton4, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(scheduleBox)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 809, Short.MAX_VALUE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel40)
                    .addComponent(jLabel43))
                .addContainerGap())
        );
        schedulePanelLayout.setVerticalGroup(
            schedulePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(schedulePanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(scheduleBox)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(schedulePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(33, 33, 33)
                .addComponent(jLabel40)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel43)
                .addContainerGap(168, Short.MAX_VALUE))
        );

        editTabs.addTab(resourceMap.getString("schedulePanel.TabConstraints.tabTitle"), schedulePanel); // NOI18N

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(editTabs, javax.swing.GroupLayout.DEFAULT_SIZE, 834, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(editTabs, javax.swing.GroupLayout.DEFAULT_SIZE, 591, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

private void recurrenceClick(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_recurrenceClick
    if (evt.getSource() == dailyButton)
    {
        weekPanel.setVisible(false);
        monthPanel.setVisible(false);
    } else if (evt.getSource() == weeklyButton)
    {
        weekPanel.setVisible(true);
        monthPanel.setVisible(false);
    } else if (evt.getSource() == monthlyButton)
    {
        weekPanel.setVisible(false);
        monthPanel.setVisible(true);
    }
}//GEN-LAST:event_recurrenceClick

private void changeDatabaseType(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_changeDatabaseType
    int typeIndex = databaseTypeBox.getSelectedIndex();
    serverNameLabel.setText(QuerySource.getServerLabel(typeIndex, false));
    databaseName.setVisible(QuerySource.getHasDatabaseName(typeIndex, false));
    databaseNameLabel.setVisible(QuerySource.getHasDatabaseName(typeIndex, false));
    databaseNameLabel.setText(QuerySource.getDatabaseNameLabel(typeIndex, false));
    portLabel.setVisible(QuerySource.getHasPort(typeIndex, false));
    portNumber.setVisible(QuerySource.getHasPort(typeIndex, false));
}//GEN-LAST:event_changeDatabaseType

private void queryChangeDatabaseType(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_queryChangeDatabaseType
    int typeIndex = queryDatabaseTypeBox.getSelectedIndex();
    queryServerNameLabel.setText(QuerySource.getServerLabel(typeIndex, false));
    queryDatabaseName.setVisible(QuerySource.getHasDatabaseName(typeIndex, false));
    queryDatabaseNameLabel.setVisible(QuerySource.getHasDatabaseName(typeIndex, false));
    queryDatabaseNameLabel.setText(QuerySource.getDatabaseNameLabel(typeIndex, false));
    queryPortLabel.setVisible(QuerySource.getHasPort(typeIndex, false));
    queryPortNumber.setVisible(QuerySource.getHasPort(typeIndex, false));

}//GEN-LAST:event_queryChangeDatabaseType

    @Action
    public void changeSSHTunelStatus()
    {
        if(chkEnableSSHQuery.isSelected())
        {
            btnConfigureSSHTunnelQuery.setEnabled(true);
        }
        else
        {
            btnConfigureSSHTunnelQuery.setEnabled(false);
        }
    }
    
    @Action
    public void isConfigureSSHTunnel()
    {
        if(chkEnableSSHTable.isSelected())
        {
            btnConfigSSHTunnelTable.setEnabled(true);
        }
        else
        {
            btnConfigSSHTunnelTable.setEnabled(false);
        }
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[])
    {
        java.awt.EventQueue.invokeLater(new Runnable()
        {

            public void run()
            {
                EditTask dialog = new EditTask(new javax.swing.JFrame(), true, null, null);
                dialog.addWindowListener(new java.awt.event.WindowAdapter()
                {

                    public void windowClosing(java.awt.event.WindowEvent e)
                    {
                        Util.exitApp(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    @Action
    public void okButton()
    {
        if (taskName.getText().length() == 0)
        {
            JOptionPane.showMessageDialog(parent, "Cannot save a task without a name. Please specify a name.");
            return;
        }
        task = new Task();
        task.setName(taskName.getText());
        task.setEnabled(enabledBox.isSelected());
        task.setLocalETLTask(isLocalETLTaskCheckBox.isSelected());
        task.setDowanloadCloudSources(downloadCloudSourcesCheckBox.isSelected());
        task.setProcessAutomatically(processBox.isSelected());
        task.setCreateQuickDashboards(createQuickDashboardsBox.isSelected());
        task.setConsolidateSources(consolidateSourcesBox.isSelected());
        task.setDateShiftValue((Integer) dateShiftBox.getValue());
        task.setSourceList(sourceList);
        task.setScheduled(scheduleBox.isSelected());
        task.setMonday(mondayBox.isSelected());
        task.setTuesday(tuesdayBox.isSelected());
        task.setWednesday(wednesdayBox.isSelected());
        task.setThursday(thursdayBox.isSelected());
        task.setFriday(fridayBox.isSelected());
        task.setSaturday(saturdayBox.isSelected());
        task.setSunday(sundayBox.isSelected());
        if (dailyButton.isSelected())
        {
            task.setRecurrenceType(Task.RECURRENCE_TYPE_DAILY);
        } else if (weeklyButton.isSelected())
        {
            task.setRecurrenceType(Task.RECURRENCE_TYPE_WEEKLY);
        } else if (monthlyButton.isSelected())
        {
            task.setRecurrenceType(Task.RECURRENCE_TYPE_MONTHLY);
        }
        task.setHour(Integer.valueOf(hourBox.getSelectedItem().toString()));
        task.setMin(Integer.valueOf(minuteBox.getSelectedItem().toString()));
        if (ampmBox.getSelectedIndex() == 0)
        {
            task.setPm(false);
        } else
        {
            task.setPm(true);
        }
        String val = (String) dayOfMonthBox.getSelectedItem();
        if (val != null)
        {
            task.setDay(Integer.valueOf(val));
        } else
        {
            task.setDay(1);
        }
        task.setRetryOnFailure(retryOnFailureBox.isSelected());
        task.setNumRetries(retryTimesBox.getSelectedIndex() + 1);
        String gs = groupsBox.getText().trim();
        if (gs.length() > 0)
        {
            List<String> sglist = new ArrayList<String>();
            for (String s : gs.split(","))
                sglist.add(s);
            task.setSubGroups(sglist);
        } else
            task.setSubGroups(null);
        this.setVisible(false);
    }

    @Action
    public void cancel()
    {
        task = null;
        this.setVisible(false);
    }

    @Action
    public void addFile()
    {
        JFileChooser jfc = new JFileChooser();
        jfc.setMultiSelectionEnabled(true);
        int returnVal = jfc.showOpenDialog((java.awt.Frame) this.getParent());
        if (returnVal == JFileChooser.APPROVE_OPTION)
        {
            File[] files = jfc.getSelectedFiles();
            if (files == null || files.length == 0)
                return;
            for (int i = 0; i < files.length; i++) {
                File f = files[i];
                boolean found = false;
                for (Object o : sourceList) {
                    if (FileSource.class.isInstance(o)) {
                        if (((FileSource) o).getFileName().equals(f.getName())) {
                            found = true;
                            break;
                        }
                    }

                }
                if (!found) {
                    FileSource fs = new FileSource();
                    fs.setFileName(f.getName());
                    fs.setPath(f.getParent());
                    sourceList.add(fs);
                }
            }
            setList();
        }
    }
    
    @Action
    public void configureSSHTunnel()
    {
        CreateSSHTunnel sshTunnel = new CreateSSHTunnel((java.awt.Frame) this.getParent(), true,clonedSSHTunnel);
        sshTunnel.setLocationRelativeTo(null);
        sshTunnel.setVisible(true);
    }
    
    @Action
    public void browseCredentialsFile()
    {
       JFileChooser jfcS3Object = new JFileChooser();
       FileNameExtensionFilter filter = new FileNameExtensionFilter("*.properties","properties");
       jfcS3Object.setFileFilter(filter);
       int returnVal = jfcS3Object.showOpenDialog((java.awt.Frame) this.getParent());
       
       if((returnVal == JFileChooser.APPROVE_OPTION) && (jfcS3Object.getSelectedFile() !=null))
       {
           String selectedFilePath = jfcS3Object.getSelectedFile().getPath();
           if(selectedFilePath.endsWith(".properties"))
           {
                txtCrendentialsFile.setText(selectedFilePath);
           }
           else
           {
               JOptionPane.showMessageDialog(parent, "Please select valid crendentials file");
               browseCredentialsFile();
           }
       }
    }

    private void setList()
    {
        DefaultTableModel tm = new DefaultTableModel(new String[]
                {
                    "Type", "Name"
                }, 0);
        for (Object o : sourceList)
        {
            if (QuerySource.class.isInstance(o))
            {
                QuerySource qs = (QuerySource) o;
                if (qs.getType() == QuerySource.TYPE_TABLE)
                {
                    tm.addRow(new String[]
                            {
                                "Table", qs.getTableName()
                            });
                } else if (qs.getType() == QuerySource.TYPE_SAP)
                {
                    tm.addRow(new String[]
                            {
                                "Object", qs.getSAPSourceName()
                            });
                } else if (qs.getType() == QuerySource.TYPE_SAP_QUERY)
                {
                    tm.addRow(new String[]
                            {
                                "Query", qs.getSAPSourceName()
                            });
                } else if (qs.getType() == QuerySource.TYPE_SAP_DSO)
                {
                    tm.addRow(new String[]
                            {
                                "DataSource Object", qs.getSAPDSOName()
                            });
                } else if (qs.getType() == QuerySource.TYPE_S3_OBJECT)
                {
                        tm.addRow(new String[]
                            {
                                "S3 Object", qs.getTableName()
                            });
                } else if (qs.getType() == QuerySource.TYPE_URL_OBJECT)
                {
                        tm.addRow(new String[]
                            {
                                "URL", qs.getTableName()
                            });                      
                } else
                {
                    tm.addRow(new String[]
                            {
                                "Query", qs.getTableName()
                            });
                }
            } else if (FileSource.class.isInstance(o))
            {
                FileSource fs = (FileSource) o;
                tm.addRow(
                        new String[]
                        {
                            "File",
                            fs.getFileName()
                        });

            }
        }

        sourceTable.clearSelection();
        sourceTable.setModel(tm);
    }

    @Action
    public void updateFile()
    {
        int selectedRow = sourceTable.getSelectedRow();
        if (selectedRow < 0)
        {
            return;
        }
        FileSource fs = (FileSource) sourceList.get(selectedRow);
        fs.setFileName(filenameBox.getText());
        fs.setPath(pathBox.getText());
        fs.getSettings().setFirstRowHasHeaders(firstRowHeadersBox.isSelected());
        fs.getSettings().setIgnoreInteralQuotes(ignoreInternalQuotesBox.isSelected());
        fs.getSettings().setIgnoreCarriageReturn(ignoreCarriageReturnBox.isSelected());
        fs.getSettings().setForceNumColumns(forceNumColumnsBox.isSelected());
        if (replacementsBox.getText().length() > 0)
            fs.getSettings().setReplacements(replacementsBox.getText());
        else
            fs.getSettings().setReplacements(null);
        String qchar = quoteCharBox.getText();
        if (qchar.length() > 1)
            qchar = qchar.substring(0, 1);
        fs.getSettings().setQuote(qchar);
        String separator = separatorBox.getText();
        if (separator.length() > 0)
            fs.getSettings().setSeparator(separator);
        else
            fs.getSettings().setSeparator(null);
        String encoding = (String)encodingTypeBox.getItemAt(encodingTypeBox.getSelectedIndex());
        if (encoding != null && !encoding.toLowerCase().equals("detect"))
            fs.getSettings().setEncoding(encoding);
        else
            fs.getSettings().setEncoding(null);
        int val = 0;
        try
        {
            val = Integer.valueOf(beginSkipBox.getText());
        } catch (Exception ex)
        {
            val = 0;
        }
        if (val >= 0)
        {
            fs.getSettings().setBeginSkip(val);
        }
        val = 0;
        try
        {
            val = Integer.valueOf(endSkipBox.getText());
        } catch (Exception ex)
        {
            val = 0;
        }
        if (val >= 0)
        {
            fs.getSettings().setEndSkip(val);
        }
        setList();
    }

    @Action
    public void addTables()
    {
        AddDatabaseSource ads = new AddDatabaseSource((java.awt.Frame) this.getParent(), true, lastQS);
        ads.setLocationRelativeTo(null);
        ads.setVisible(true);
        for (Object o : ads.sourceList)
        {
            if (QuerySource.class.isInstance(o))
            {
                QuerySource qs = (QuerySource) o;
                boolean found = false;
                for (Object co : sourceList)
                {
                    if (QuerySource.class.isInstance(co))
                    {
                        QuerySource cqs = (QuerySource) co;
                        if (cqs.getType() == QuerySource.TYPE_TABLE && cqs.getTableName().equals(qs.getTableName()))
                        {
                            found = true;
                            break;
                        }
                    }
                }
                if (!found)
                {
                    sourceList.add(o);
                }
            }
        }

        setList();
    }

    @Action
    public void updateTable()
    {
        int selectedRow = sourceTable.getSelectedRow();
        if (selectedRow < 0)
        {
            return;
        }
        QuerySource qs = (QuerySource) sourceList.get(selectedRow);
        qs.setCatalogName(catalogBox.getText());
        qs.setSchemaName(schemaBox.getText());
        qs.setTableName(tableNameBox.getText());
        qs.setServerName(serverName.getText());
        qs.setDatabaseName(databaseName.getText());
        qs.setPort(portNumber.getText());
        qs.setDriverName(databaseTypeBox.getSelectedIndex(), false);
        qs.setUsername(username.getText());
        qs.putPassword(password.getText());
        qs.setType(QuerySource.TYPE_TABLE);
        if (replacementsBoxForTable.getText().length() > 0)
		   qs.getSettings().setReplacements(replacementsBoxForTable.getText());
	    else
           qs.getSettings().setReplacements(null);
        qs.getSettings().setIgnoreInteralQuotes(tableIgnoreInternalQuotesBox.isSelected());
        qs.getSettings().setIgnoreCarriageReturn(tableIgnoreCarriageReturnBox.isSelected());
        String qchar = tableQuoteCharBox.getText();
        if (qchar.length() > 1)
            qchar = qchar.substring(0, 1);
        qs.getSettings().setQuote(qchar);
        
        if(chkEnableSSHTable.isSelected())
        {
            clonedSSHTunnel.setIsSSHTunnelConfigured(true);
            
            if(portNumber != null)
            {
                 clonedSSHTunnel.setTunnelInternalPort(portNumber.getText());
            }
            else
            {
                clonedSSHTunnel.setTunnelInternalPort("0");
            }
            qs.setSshTunnel(clonedSSHTunnel);    
        }
        else
        {
             clonedSSHTunnel.setIsSSHTunnelConfigured(false);
             qs.setSshTunnel(null);    
        }
       
        setList();
        
        saveQuerySourceState(qs);
    }

    @Action
    public void addQuery()
    {
        QuerySource qs = new QuerySource();
        qs.setTableName("New Query " + System.currentTimeMillis());
        qs.setType(QuerySource.TYPE_QUERY);
        qs.setDriverName(lastQS.getDriverName());
        qs.setGenericDriver(lastQS.isGenericDriver());
        qs.setCatalogName(lastQS.getCatalogName());
        qs.setDatabaseName(lastQS.getDatabaseName());
        qs.setPort(lastQS.getPort());
        qs.setUsername(lastQS.getUsername());
        qs.putPassword(lastQS.retrievePassword());
        qs.setSchemaName(lastQS.getSchemaName());
        qs.setServerName(lastQS.getServerName());
        qs.getSettings().setReplacements(lastQS.getSettings().getReplacements());
        qs.getSettings().setIgnoreInteralQuotes(lastQS.getSettings().isIgnoreInteralQuotes());
        qs.getSettings().setIgnoreCarriageReturn(lastQS.getSettings().isIgnoreCarriageReturn());
        qs.getSettings().setQuote(lastQS.getSettings().getQuote());
        sourceList.add(qs);
        qs.setSshTunnel(clonedSSHTunnel);        
        setList();

    }

    @Action
    public void updateQuery()
    {
           int dataBaseTypeIndex = queryDatabaseTypeBox.getSelectedIndex();
           String nameOfDriver = DatabaseConnection.getDriverName(dataBaseTypeIndex,false);
           boolean isDriverInstalled = Util.isDriverInstalled(nameOfDriver);
           int result = -1;
           
           if(nameOfDriver != null && !isDriverInstalled)
           {
               result = JOptionPane.showConfirmDialog(null, Util.DRIVER_NOT_INSTALLED_WARN_MESSAGE, "Warning", JOptionPane.OK_CANCEL_OPTION);
           }
           if((nameOfDriver == null) || (isDriverInstalled) || (!isDriverInstalled) && (result == 0))
           {
                String tableName = queryNameBox.getText();
                if (tableName.trim().length() == 0)
                {
                    JOptionPane.showMessageDialog(parent, "Query must have a name specified");
                    return;
                }
                if (!tableName.trim().equals(tableName))
                {
                    JOptionPane.showMessageDialog(parent, "Query can not have a name ending with space. Removing trailing spaces.");
                    tableName = tableName.trim();
                    queryNameBox.setText(tableName);
                }
                int selectedRow = sourceTable.getSelectedRow();
                if (selectedRow < 0)
                {
                    return;
                }
                QuerySource qs = (QuerySource) sourceList.get(selectedRow);
                qs.setTableName(tableName);
                qs.setQuery(queryBox.getText());
                qs.setServerName(queryServerName.getText());
                qs.setDatabaseName(queryDatabaseName.getText());
                qs.setPort(queryPortNumber.getText());
                int typeIndex = queryDatabaseTypeBox.getSelectedIndex();
                if (typeIndex != DatabaseConnection.getGenericDatabaseIndex())
                {
                    qs.setDriverName(QuerySource.getDriverName(typeIndex, false));
                    qs.setGenericDriver(false);
                }
                else
                {
                    qs.setDriverName(serverName.getText());
                    qs.setGenericDriver(true);
                }
                qs.setUsername(queryUsername.getText());
                qs.putPassword(queryPassword.getText());
                qs.setType(QuerySource.TYPE_QUERY);
                if (replacementsBoxForQuery.getText().length() > 0)
                           qs.getSettings().setReplacements(replacementsBoxForQuery.getText());
                        else
                   qs.getSettings().setReplacements(null);
                qs.getSettings().setIgnoreInteralQuotes(queryIgnoreInternalQuotesBox.isSelected());
                qs.getSettings().setIgnoreCarriageReturn(queryIgnoreCarriageReturnBox.isSelected());
                String qchar = queryQuoteCharBox.getText();
                if (qchar.length() > 1)
                    qchar = qchar.substring(0, 1);
                qs.getSettings().setQuote(qchar);

                if(chkEnableSSHQuery.isSelected())
                {
                    clonedSSHTunnel.setIsSSHTunnelConfigured(true);
                    if(queryPortNumber != null)
                    {
                        clonedSSHTunnel.setTunnelInternalPort(queryPortNumber.getText());
                    }
                    else
                    {
                        clonedSSHTunnel.setTunnelInternalPort("0");
                    }
                    qs.setSshTunnel(clonedSSHTunnel); 
                }
                else
                {
                     clonedSSHTunnel.setIsSSHTunnelConfigured(false);
                     qs.setSshTunnel(null); 
                }
                setList();
                saveQuerySourceState(qs);
           }
    }
    
    @Action
    public void addURL()
    {
        QuerySource qs = new QuerySource();
        qs.setType(QuerySource.TYPE_URL_OBJECT);
        qs.setTableName("URL " + System.currentTimeMillis());
        sourceList.add(qs);
        setList();
    }
    
    @Action
    public void updateURL()
    {
        int selectedRow = sourceTable.getSelectedRow();
        if (selectedRow < 0)
        {
            return;
        }
        QuerySource qs = (QuerySource) sourceList.get(selectedRow);
        qs.setUrl(txtURL.getText());
        qs.setTableName(txtName.getText());
        qs.setMethod(URLMethodComboBox.getSelectedItem().toString());
        
        if (replacementsBoxForURL.getText().length() > 0)
            qs.getSettings().setReplacements(replacementsBoxForURL.getText());
        else
            qs.getSettings().setReplacements(null);
        qs.getSettings().setIgnoreInteralQuotes(URLIgnoreInternalQuotesBox.isSelected());
        qs.getSettings().setIgnoreCarriageReturn(URLIgnoreCarriageReturnBox.isSelected());
       
        String qchar = URLQuoteCharBox.getText();
        if (qchar.length() > 1)
            qchar = qchar.substring(0, 1);
        qs.getSettings().setQuote(qchar);
        
        qs.setIgnoreBadServerCertificates(URLIgnoreBadServerCertificatesBox.isSelected());

        setList();
        saveQuerySourceState(qs);
    }
    
    @Action
    public void addS3Object()
    {
        QuerySource qs = new QuerySource();
        qs.setType(QuerySource.TYPE_S3_OBJECT);
        qs.setTableName("Object " + System.currentTimeMillis());
        sourceList.add(qs);
        setList();

    }
    
    @Action
    public void updateS3Object()
    {
        int selectedRow = sourceTable.getSelectedRow();
        if (selectedRow < 0)
        {
            return;
        }
        String tableName = txtS3HostName.getText();
        QuerySource qs = (QuerySource) sourceList.get(selectedRow);
        qs.setBucketName(txtBucketName.getText());
        qs.setBucketKey(txtBucketKey.getText());
        
        if(txtCrendentialsFile.getText().endsWith(".properties"))
        {
            qs.setCrendentialsFileName(txtCrendentialsFile.getText());    
        }
        else
        {
             JOptionPane.showMessageDialog(parent, "Please select valid crendentials file");
             browseCredentialsFile();
        }
        
        if (replacementsBoxForS3Object.getText().length() > 0)
		   qs.getSettings().setReplacements(replacementsBoxForS3Object.getText());
		else
           qs.getSettings().setReplacements(null);
        qs.setTableName(tableName);
        qs.getSettings().setIgnoreInteralQuotes(s3ObjectIgnoreInternalQuotesBox.isSelected());
        qs.getSettings().setIgnoreCarriageReturn(s3ObjectIgnoreCarriageReturnBox.isSelected());
       
        String qchar = s3ObjectQuoteCharBox.getText();
        if (qchar.length() > 1)
            qchar = qchar.substring(0, 1);
        qs.getSettings().setQuote(qchar);

        setList();
        
        saveQuerySourceState(qs);
    }
        
    @Action
    public void removeAction()
    {
        int index = sourceTable.getSelectedRow();
        if (index < 0)
        {
            return;
        }
        ((DefaultTableModel) sourceTable.getModel()).removeRow(index);
        sourceList.remove(index);
        filePanel.setVisible(
                false);
        tablePanel.setVisible(
                false);
        queryPanel.setVisible(
                false);
        sapObjectPanel.setVisible(
                false);
        S3ObjectPanel.setVisible(
                false);
        URLPanel.setVisible(
                false);
    }

    @Action
    public void addObjects() {
        AddSAPSource ass = new AddSAPSource((java.awt.Frame) this.getParent(), true);
        ass.setLocationRelativeTo(null);
        ass.setVisible(true);
        for (Object o : ass.sourceList)
        {
            if (QuerySource.class.isInstance(o))
            {
                QuerySource qs = (QuerySource) o;
                boolean found = false;
                for (Object co : sourceList)
                {
                    if (QuerySource.class.isInstance(co))
                    {
                        QuerySource cqs = (QuerySource) co;
                        if (cqs.getType() == QuerySource.TYPE_SAP && cqs.getSAPSourceName().equals(qs.getSAPSourceName()))
                        {
                            found = true;
                            break;
                        }
                    }
                }
                if (!found)
                {
                    sourceList.add(o);
                }
            }
        }
        setList();
    }

    @Action
    public void addSAPQueries() {
        AddSAPQuerySource asq = new AddSAPQuerySource((java.awt.Frame) this.getParent(), true);
        asq.setLocationRelativeTo(null);
        asq.setVisible(true);
        for (Object o : asq.sourceList)
        {
            if (QuerySource.class.isInstance(o))
            {
                QuerySource qs = (QuerySource) o;
                boolean found = false;
                for (Object co : sourceList)
                {
                    if (QuerySource.class.isInstance(co))
                    {
                        QuerySource cqs = (QuerySource) co;
                        if (cqs.getType() == QuerySource.TYPE_SAP_QUERY && cqs.getSAPSourceName().equals(qs.getSAPSourceName()))
                        {
                            found = true;
                            break;
                        }
                    }
                }
                if (!found)
                {
                    sourceList.add(o);
                }
            }
        }
        setList();
    }

    @Action
    public void addSAPDSOs() {
        AddSAPDataSourceObject asd = new AddSAPDataSourceObject((java.awt.Frame) this.getParent(), true);
        asd.setLocationRelativeTo(null);
        asd.setVisible(true);
        for (Object o : asd.sourceList)
        {
            if (QuerySource.class.isInstance(o))
            {
                QuerySource qs = (QuerySource) o;
                boolean found = false;
                for (Object co : sourceList)
                {
                    if (QuerySource.class.isInstance(co))
                    {
                        QuerySource cqs = (QuerySource) co;
                        if (cqs.getType() == QuerySource.TYPE_SAP_DSO && cqs.getSAPSourceName().equals(qs.getSAPSourceName()))
                        {
                            found = true;
                            break;
                        }
                    }
                }
                if (!found)
                {
                    sourceList.add(o);
                }
            }
        }
        setList();
    }

    @Action
    public void updateSAPObject() {
        int selectedRow = sourceTable.getSelectedRow();
        if (selectedRow < 0)
        {
            return;
        }
        QuerySource qs = (QuerySource) sourceList.get(selectedRow);
        String routerhost = routerHostBox.getText();
        String routerport = routerPortBox.getText();
        String serverhost = serverHostBox.getText();
        String servername = objectServerNameBox.getText();
        String systemNumber = systemNumberBox.getText();
        String client = clientBox.getText();
        String sapUserName = userNameBox.getText();
        String sapPassword = passwordBox.getText();
        SAPSystem sapSystem = new SAPSystem(routerhost, routerport, serverhost, servername, systemNumber, client, sapUserName , sapPassword);
        qs.setSapSystem(sapSystem);
        qs.setType(QuerySource.TYPE_SAP);
        qs.setSAPRemoteFunction(clonedsrf);
        qs.getSettings().setIgnoreInteralQuotes(objectIgnoreInternalQuotesBox.isSelected());
        String qchar = objectQuoteCharBox.getText();
        if (qchar.length() > 1)
            qchar = qchar.substring(0, 1);
        qs.getSettings().setQuote(qchar);

        setList();
    }

    @Action
    public void updateSAPQuery() {
        int selectedRow = sourceTable.getSelectedRow();
        if (selectedRow < 0)
        {
            return;
        }
        QuerySource qs = (QuerySource) sourceList.get(selectedRow);
        String routerhost = sapQrouterHostBox.getText();
        String routerport = sapQrouterPortBox.getText();
        String serverhost = sapQserverHostBox.getText();
        String servername = sapQobjectServerNameBox.getText();
        String systemNumber = sapQsystemNumberBox.getText();
        String client = sapQclientBox.getText();
        String sapUserName = sapQuserNameBox.getText();
        String sapPassword = sapQpasswordBox.getText();
        SAPSystem sapSystem = new SAPSystem(routerhost, routerport, serverhost, servername, systemNumber, client, sapUserName , sapPassword);
        qs.setSapSystem(sapSystem);
        qs.setType(QuerySource.TYPE_SAP_QUERY);
        qs.setSAPRemoteQuery(clonedsrq);
        qs.getSettings().setIgnoreInteralQuotes(sapQobjectIgnoreInternalQuotesBox.isSelected());
        String qchar = sapQobjectQuoteCharBox.getText();
        if (qchar.length() > 1)
            qchar = qchar.substring(0, 1);
        qs.getSettings().setQuote(qchar);

        setList();
    }

    @Action
    public void showFunctionDetailsDialog() {
        SAPRemoteFunctionDetails dialog = new SAPRemoteFunctionDetails(parent, true, clonedsrf, sapSourceNameLabel.getText());
        dialog.setLocationRelativeTo(null);
        dialog.setVisible(true);
    }

    @Action
    public void showQueryDetailsDialog() {
        SAPRemoteQueryDetails dialog = new SAPRemoteQueryDetails(parent, true, clonedsrq, sapQueryNameLabel.getText());
        dialog.setLocationRelativeTo(null);
        dialog.setVisible(true);
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel S3ObjectPanel;
    private javax.swing.JButton SAPFunctionDetailsButton;
    private javax.swing.JButton SAPQueryDetailsButton;
    private javax.swing.JCheckBox URLIgnoreBadServerCertificatesBox;
    private javax.swing.JCheckBox URLIgnoreCarriageReturnBox;
    private javax.swing.JCheckBox URLIgnoreInternalQuotesBox;
    private javax.swing.JLabel URLLabel;
    private javax.swing.JComboBox URLMethodComboBox;
    private javax.swing.JPanel URLPanel;
    private javax.swing.JTextField URLQuoteCharBox;
    private javax.swing.JLabel URLQuoteCharLabel;
    private javax.swing.JButton URLUpdate;
    private javax.swing.JButton addFileButton;
    private javax.swing.JButton addObjectsButton;
    private javax.swing.JButton addQueryButton;
    private javax.swing.JButton addSAPDSOButton;
    private javax.swing.JButton addSAPQueryButton;
    private javax.swing.JButton addTablesButton;
    private javax.swing.JComboBox ampmBox;
    private javax.swing.JTextField beginSkipBox;
    private javax.swing.JButton browseCrendentoalsFileBtn;
    private javax.swing.JButton btnAddS3Object;
    private javax.swing.JButton btnAddURL;
    private javax.swing.JButton btnConfigSSHTunnelTable;
    private javax.swing.JButton btnConfigureSSHTunnelQuery;
    private javax.swing.JLabel bucketKeyLabel;
    private javax.swing.JLabel bucketNameLabel;
    private javax.swing.JButton cancelButton;
    private javax.swing.JTextField catalogBox;
    private javax.swing.JCheckBox chkEnableSSHQuery;
    private javax.swing.JCheckBox chkEnableSSHTable;
    private javax.swing.JTextField clientBox;
    private javax.swing.JLabel clientLabel;
    private javax.swing.JLabel clientLabel1;
    private javax.swing.JCheckBox consolidateSourcesBox;
    private javax.swing.JCheckBox createQuickDashboardsBox;
    private javax.swing.JLabel crendentialsFileLabel;
    private javax.swing.JRadioButton dailyButton;
    private javax.swing.JTextField databaseName;
    private javax.swing.JLabel databaseNameLabel;
    private javax.swing.JComboBox databaseTypeBox;
    private javax.swing.JSpinner dateShiftBox;
    private javax.swing.JComboBox dayOfMonthBox;
    private javax.swing.JCheckBox downloadCloudSourcesCheckBox;
    private javax.swing.JTabbedPane editTabs;
    private javax.swing.JCheckBox enabledBox;
    private javax.swing.JComboBox encodingTypeBox;
    private javax.swing.JTextField endSkipBox;
    private javax.swing.JPanel filePanel;
    private javax.swing.JTextField filenameBox;
    private javax.swing.JCheckBox firstRowHeadersBox;
    private javax.swing.JCheckBox forceNumColumnsBox;
    private javax.swing.JCheckBox fridayBox;
    private javax.swing.JTextField groupsBox;
    private javax.swing.JComboBox hourBox;
    private javax.swing.JCheckBox ignoreCarriageReturnBox;
    private javax.swing.JCheckBox ignoreInternalQuotesBox;
    private javax.swing.JCheckBox isLocalETLTaskCheckBox;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton5;
    private javax.swing.JButton jButton6;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel32;
    private javax.swing.JLabel jLabel33;
    private javax.swing.JLabel jLabel34;
    private javax.swing.JLabel jLabel35;
    private javax.swing.JLabel jLabel36;
    private javax.swing.JLabel jLabel37;
    private javax.swing.JLabel jLabel38;
    private javax.swing.JLabel jLabel39;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel40;
    private javax.swing.JLabel jLabel41;
    private javax.swing.JLabel jLabel42;
    private javax.swing.JLabel jLabel43;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JPanel masterPanel;
    private javax.swing.JComboBox minuteBox;
    private javax.swing.JCheckBox mondayBox;
    private javax.swing.JPanel monthPanel;
    private javax.swing.JRadioButton monthlyButton;
    private javax.swing.JCheckBox objectIgnoreInternalQuotesBox;
    private javax.swing.JLabel objectNameLabel;
    private javax.swing.JTextField objectQuoteCharBox;
    private javax.swing.JTextField objectServerNameBox;
    private javax.swing.JLabel objectServerNameLabel;
    private javax.swing.JLabel objectServerNameLabel1;
    private javax.swing.JButton okButton;
    private javax.swing.JPasswordField password;
    private javax.swing.JPasswordField passwordBox;
    private javax.swing.JLabel passwordLabel;
    private javax.swing.JLabel passwordLabel1;
    private javax.swing.JTextField pathBox;
    private javax.swing.JLabel portLabel;
    private javax.swing.JTextField portNumber;
    private javax.swing.JCheckBox processBox;
    private javax.swing.JPanel propertyPanel;
    private javax.swing.JTextArea queryBox;
    private javax.swing.JTextField queryDatabaseName;
    private javax.swing.JLabel queryDatabaseNameLabel;
    private javax.swing.JComboBox queryDatabaseTypeBox;
    private javax.swing.JCheckBox queryIgnoreCarriageReturnBox;
    private javax.swing.JCheckBox queryIgnoreInternalQuotesBox;
    private javax.swing.JTextField queryNameBox;
    private javax.swing.JLabel queryNameLabel;
    private javax.swing.JPanel queryPanel;
    private javax.swing.JPasswordField queryPassword;
    private javax.swing.JLabel queryPortLabel;
    private javax.swing.JTextField queryPortNumber;
    private javax.swing.JTextField queryQuoteCharBox;
    private javax.swing.JTextField queryServerName;
    private javax.swing.JLabel queryServerNameLabel;
    private javax.swing.JTextField queryUsername;
    private javax.swing.JTextField quoteCharBox;
    private javax.swing.ButtonGroup recurrenceGroup;
    private javax.swing.JButton removeSourceButton;
    private javax.swing.JTextField replacementsBox;
    private javax.swing.JTextField replacementsBoxForQuery;
    private javax.swing.JTextField replacementsBoxForS3Object;
    private javax.swing.JTextField replacementsBoxForTable;
    private javax.swing.JTextField replacementsBoxForURL;
    private javax.swing.JLabel replacementsForS3ObjectLabel;
    private javax.swing.JLabel replacementsForURLLabel;
    private javax.swing.JLabel replacementsForURLLabel1;
    private javax.swing.JCheckBox retryOnFailureBox;
    private javax.swing.JComboBox retryTimesBox;
    private javax.swing.JTextField routerHostBox;
    private javax.swing.JLabel routerHostLabel;
    private javax.swing.JLabel routerHostLabel1;
    private javax.swing.JTextField routerPortBox;
    private javax.swing.JLabel routerPortLabel;
    private javax.swing.JLabel routerPortLabel1;
    private javax.swing.JLabel s3HostNameLabel;
    private javax.swing.JLabel s3HostNameLabel2;
    private javax.swing.JCheckBox s3ObjectIgnoreCarriageReturnBox;
    private javax.swing.JCheckBox s3ObjectIgnoreInternalQuotesBox;
    private javax.swing.JTextField s3ObjectQuoteCharBox;
    private javax.swing.JLabel s3ObjectQuoteCharLabel;
    private javax.swing.JButton s3ObjectUpdate;
    private javax.swing.JPanel sapObjectPanel;
    private javax.swing.JTextField sapQclientBox;
    private javax.swing.JCheckBox sapQobjectIgnoreInternalQuotesBox;
    private javax.swing.JTextField sapQobjectQuoteCharBox;
    private javax.swing.JTextField sapQobjectServerNameBox;
    private javax.swing.JPasswordField sapQpasswordBox;
    private javax.swing.JTextField sapQrouterHostBox;
    private javax.swing.JTextField sapQrouterPortBox;
    private javax.swing.JTextField sapQserverHostBox;
    private javax.swing.JTextField sapQsystemNumberBox;
    private javax.swing.JLabel sapQueryNameLabel;
    private javax.swing.JPanel sapQueryPanel;
    private javax.swing.JTextField sapQuserNameBox;
    private javax.swing.JLabel sapSourceNameLabel;
    private javax.swing.JCheckBox saturdayBox;
    private javax.swing.JCheckBox scheduleBox;
    private javax.swing.JPanel schedulePanel;
    private javax.swing.JTextField schemaBox;
    private javax.swing.JTextField separatorBox;
    private javax.swing.JTextField serverHostBox;
    private javax.swing.JLabel serverHostLabel;
    private javax.swing.JLabel serverHostLabel1;
    private javax.swing.JTextField serverName;
    private javax.swing.JLabel serverNameLabel;
    private javax.swing.JTable sourceTable;
    private javax.swing.JPanel sourcesPanel;
    private javax.swing.JCheckBox sundayBox;
    private javax.swing.JTextField systemNumberBox;
    private javax.swing.JLabel systemNumberLabel;
    private javax.swing.JLabel systemNumberLabel1;
    private javax.swing.JCheckBox tableIgnoreCarriageReturnBox;
    private javax.swing.JCheckBox tableIgnoreInternalQuotesBox;
    private javax.swing.JTextField tableNameBox;
    private javax.swing.JPanel tablePanel;
    private javax.swing.JTextField tableQuoteCharBox;
    private javax.swing.JTextField taskName;
    private javax.swing.JCheckBox thursdayBox;
    private javax.swing.JCheckBox tuesdayBox;
    private javax.swing.JTextField txtBucketKey;
    private javax.swing.JTextField txtBucketName;
    private javax.swing.JTextField txtCrendentialsFile;
    private javax.swing.JTextField txtName;
    private javax.swing.JTextField txtS3HostName;
    private javax.swing.JTextField txtURL;
    private javax.swing.JButton updateFileButton;
    private javax.swing.JButton updateObjectButton;
    private javax.swing.JButton updateQueryButton;
    private javax.swing.JButton updateSAPQueryButton;
    private javax.swing.JButton updateTableButton;
    private javax.swing.JTextField userNameBox;
    private javax.swing.JLabel userNameLabel;
    private javax.swing.JLabel userNameLabel1;
    private javax.swing.JTextField username;
    private javax.swing.JCheckBox wednesdayBox;
    private javax.swing.JPanel weekPanel;
    private javax.swing.JRadioButton weeklyButton;
    // End of variables declaration//GEN-END:variables
}
