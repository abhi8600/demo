/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.birst.dataconductor;

import com.birst.dataconductor.sap.SAPRemoteFunction;
import com.birst.dataconductor.sap.SAPRemoteQuery;
import com.birst.dataconductor.sap.SAPSystem;
import java.beans.XMLEncoder;
import java.io.*;
import java.util.List;
import org.apache.log4j.Logger;
import org.jdom.Element;


/**
 *
 * @author bpeters
 */
public class QuerySource extends DatabaseConnection
{
    public static final int TYPE_TABLE = 0;
    public static final int TYPE_QUERY = 1;
    public static final int TYPE_SAP = 2;
    public static final int TYPE_SAP_QUERY = 3;
    public static final int TYPE_SAP_DSO = 4;
    public static final int TYPE_S3_OBJECT = 5;
    public static final int TYPE_URL_OBJECT = 6;
    public int type;
    private String catalogName;
    private String schemaName;
    private String tableName;
    private String query;
    private UploadSettings settings = new UploadSettings();
    private String SAPSourceName;
    private SAPSystem sapSystem = new SAPSystem();
    private SAPRemoteFunction SAPRemoteFunction;
    private SAPRemoteQuery SAPRemoteQuery;
    private String SAPDSOName;
    private String crendentialsFileName;
    private String bucketName;
    private String bucketKey;
    private SSHTunnel sshTunnel = new SSHTunnel();
    private boolean isSSHTunnelConfigured;
    private String url;
    private String method = "GET";
    private Boolean ignoreBadServerCertificates = false;

    public Element getElement()
    {
        Element e = new Element("object");
        e.setAttribute("class", QuerySource.class.getName());
        if (type != 0)
        {
            e.addContent(XmlEncoderUtil.getIntPropertyElement("type", type));
        }
        if (catalogName != null)
        {
            e.addContent(XmlEncoderUtil.getStringPropertyElement("catalogName", catalogName));
        }
        if (schemaName != null)
        {
            e.addContent(XmlEncoderUtil.getStringPropertyElement("schemaName", schemaName));
        }
        if (tableName != null)
        {
            e.addContent(XmlEncoderUtil.getStringPropertyElement("tableName", tableName));
        }
        if (query != null)
        {
            e.addContent(XmlEncoderUtil.getStringPropertyElement("query", query));
        }
        if (settings != null && !settings.equals(new UploadSettings()))
        {
            e.addContent(settings.getElement());
        }
        if (SAPSourceName != null)
        {
            e.addContent(XmlEncoderUtil.getStringPropertyElement("SAPSourceName", SAPSourceName));
        }
        if (sapSystem != null && !sapSystem.equals(new SAPSystem()))
        {
            e.addContent(sapSystem.getElement());
        }
        if (SAPRemoteFunction != null)
        {
            e.addContent(SAPRemoteFunction.getElement());
        }
        if (SAPRemoteQuery != null)
        {
            e.addContent(SAPRemoteQuery.getElement());
        }
        if (SAPDSOName != null)
        {
            e.addContent(XmlEncoderUtil.getStringPropertyElement("SAPDSOName", SAPDSOName));
        }
        if (bucketName != null)
        {
            e.addContent(XmlEncoderUtil.getStringPropertyElement("bucketName", bucketName));
        }
        if (bucketKey != null)
        {
            e.addContent(XmlEncoderUtil.getStringPropertyElement("bucketKey", bucketKey));
        }
        if (crendentialsFileName != null)
        {
            e.addContent(XmlEncoderUtil.getStringPropertyElement("crendentialsFileName", crendentialsFileName));
        }
        if (sshTunnel != null && !sshTunnel.equals(new SSHTunnel()))
        {
            e.addContent(sshTunnel.getElement());
        }
        if (url != null)
        {
            e.addContent(XmlEncoderUtil.getStringPropertyElement("url", url));
        }       
        if (method != null)
        {
            e.addContent(XmlEncoderUtil.getStringPropertyElement("method", method));
        }    
        if (ignoreBadServerCertificates != null)
        {        
            e.addContent(XmlEncoderUtil.getBooleanPropertyElement("ignoreBadServerCertificates", ignoreBadServerCertificates.toString()));
        }
        List<Element> children = super.getElements();
        if (children != null && !children.isEmpty())
            e.addContent(children);
        return e;
    }
    
    public QuerySource()
    {
    }

    public void setSettings(UploadSettings settings) {
        this.settings = settings;
    }

    public UploadSettings getSettings() {
        return settings;
    }
    
    public String getCatalogName()
    {
        return catalogName;
    }

    public void setCatalogName(String catalogName)
    {
        this.catalogName = catalogName;
    }

    public String getQuery()
    {
        return query;
    }

    public void setQuery(String query)
    {
        this.query = query;
    }

    public String getSchemaName()
    {
        return schemaName;
    }

    public void setSchemaName(String schemaName)
    {
        this.schemaName = schemaName;
    }

    public int getType()
    {
        return type;
    }

    public void setType(int type)
    {
        this.type = type;
    }

    public String getTableName()
    {
        return tableName;
    }

    public SAPSystem getSapSystem() {
        return sapSystem;
    }

    public void setSapSystem(SAPSystem sapSystem) {
        this.sapSystem = sapSystem;
    }

    public String getQueryTableName()
    {
        if (!genericDriver && driverName.equals("com.microsoft.sqlserver.jdbc.SQLServerDriver"))
        {
            return "[" + tableName + "]";
        }
        else if (genericDriver && serverName.equals("com.microsoft.sqlserver.jdbc.SQLServerDriver"))
        {
            return "[" + tableName + "]";
        }
        else if (!genericDriver && driverName.equals("org.postgresql.Driver"))
        {
            return "\"" + tableName + "\"";
        }
        else if (genericDriver && serverName.equals("org.postgresql.Driver"))
        {
            return "\"" + tableName + "\"";
        }
        return tableName;
    }

    public void setTableName(String tableName)
    {
        this.tableName = tableName;
    }

    public String getSAPSourceName() {
        return SAPSourceName;
    }

    public void setSAPSourceName(String SAPSourceName) {
        this.SAPSourceName = SAPSourceName;
    }

    public SAPRemoteFunction getSAPRemoteFunction() {
        return SAPRemoteFunction;
    }

    public void setSAPRemoteFunction(SAPRemoteFunction SAPRemoteFunction) {
        this.SAPRemoteFunction = SAPRemoteFunction;
    }

    public SAPRemoteQuery getSAPRemoteQuery() {
        return SAPRemoteQuery;
    }

    public void setSAPRemoteQuery(SAPRemoteQuery SAPRemoteQuery) {
        this.SAPRemoteQuery = SAPRemoteQuery;
    }

    public String getSAPDSOName() {
        return SAPDSOName;
    }

    public void setSAPDSOName(String SAPDSOName) {
        this.SAPDSOName = SAPDSOName;
    }
    public String getCrendentialsFileName() {
        return crendentialsFileName;
    }

    public void setCrendentialsFileName(String crendentialsFileName) {
        this.crendentialsFileName = crendentialsFileName;
    }

    public String getBucketName() {
        return bucketName;
    }

    public void setBucketName(String bucketName) {
        this.bucketName = bucketName;
    }

    public String getBucketKey() {
        return bucketKey;
    }

    public void setBucketKey(String bucketKey) {
        this.bucketKey = bucketKey;
    }
    
    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public Boolean getIgnoreBadServerCertificates() {
        return ignoreBadServerCertificates;
    }

    public void setIgnoreBadServerCertificates(Boolean ignoreBadServerCertificates) {
        this.ignoreBadServerCertificates = ignoreBadServerCertificates;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public SSHTunnel getSshTunnel() {
        return sshTunnel;
    }

    public void setSshTunnel(SSHTunnel sshTunnel) {
        this.sshTunnel = sshTunnel;
    }

    public boolean isIsSSHTunnelConfigured() {
        return isSSHTunnelConfigured;
    }

    public void setIsSSHTunnelConfigured(boolean isSSHTunnelConfigured) {
        this.isSSHTunnelConfigured = isSSHTunnelConfigured;
    }
    
    public String getXML()
    {
        try
        {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            BufferedOutputStream os = new BufferedOutputStream(baos);

            XMLEncoder xml = new XMLEncoder(os);
            xml.writeObject(this);
            xml.flush();
            baos.close();
            return baos.toString();
        } catch (IOException ex)
        {
            Logger.getLogger(QuerySource.class).error(null, ex);
        }
        return (null);
    }
}
