/*
 * DataConductorView.java
 */
package com.birst.dataconductor;

import com.birst.dataconductor.localetl.LocalETLConfig;
import com.birst.dataconductor.localetl.LocalETLConfigDialog;
import java.awt.Component;
import javax.swing.JButton;
import javax.swing.JTable;
import org.jdesktop.application.Action;
import org.jdesktop.application.ResourceMap;
import org.jdesktop.application.SingleFrameApplication;
import org.jdesktop.application.FrameView;
import org.jdesktop.application.TaskMonitor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.atomic.AtomicBoolean;
import java.io.File;
import java.net.URL;
import javax.swing.Timer;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

/**
 * The application's main frame.
 */
public class DataConductorView extends FrameView
{

    private DataConductorConfig config = new DataConductorConfig();
    private ActivityLog activityLog = new ActivityLog();
    private ViewActivity va = new ViewActivity(this.getFrame(), true, activityLog);
    private Map<String, Boolean> statusMap = new HashMap<String, Boolean>();
    private static final String OP_ADD_CONNECTION = "addnew";
    private static final String OP_DELETE_CONNECTION = "delete";
    private static final String OP_EDIT_CONNECTION = "edit";

    public DataConductorConfig getConfig()
    {
        return config;
    }

    public void setConfig(DataConductorConfig config)
    {
        this.config = config;
    }

    public JButton getEditTaskButton()
    {
        return editTaskButton;
    }

    public void setEditTaskButton(JButton editTaskButton)
    {
        this.editTaskButton = editTaskButton;
    }
    private Timer taskTimer = null;
    private Map<Task, Calendar> taskCalendar = new HashMap<Task, Calendar>();
    private JFrame frame = this.getFrame();
    private AtomicBoolean running = new AtomicBoolean(false);
    private DataConductorUtil dcu;
    private GraphicsObject go;
    private SingleFrameApplication app;

    public DataConductorView(SingleFrameApplication app)
    {
        super(app);
        this.app = app;

        initComponents();

        go = new GraphicsObject(this.getFrame(), this.statusMessageLabel, this.progressBar);

        dcu = new DataConductorUtil(go, activityLog);
        if (dcu.isRunningHeadLess())
        {
            dcu.resetGraphicsObject();
        }
        localETL.setVisible(DataConductorUtil.hasLocalETL());

        String fname = dcu.getDCConfigFileName();
        String dcViewName = fname.equals(DataConductorUtil.DCCONFIG_FILE_NAME) ? "Default" : fname.endsWith("_" + DataConductorUtil.DCCONFIG_FILE_NAME) ? fname.substring(0, fname.indexOf("_" + DataConductorUtil.DCCONFIG_FILE_NAME)) : "Default";
        this.getFrame().setTitle("Birst Connect - " + dcu.getSpaceName() + "/" + dcu.getSpace() + " (" + dcViewName + ")");
        // status bar initialization - message timeout, idle icon and busy animation, etc
        ResourceMap resourceMap = getResourceMap();
        int messageTimeout = resourceMap.getInteger("StatusBar.messageTimeout");
        messageTimer = new Timer(messageTimeout, new ActionListener()
        {

            public void actionPerformed(ActionEvent e)
            {
                statusMessageLabel.setText("");
            }
        });
        messageTimer.setRepeats(false);
        for (int i = 0; i < busyIcons.length; i++)
        {
            busyIcons[i] = resourceMap.getIcon("StatusBar.busyIcons[" + i + "]");
        }
        idleIcon = resourceMap.getIcon("StatusBar.idleIcon");
        statusAnimationLabel.setIcon(idleIcon);
        int busyAnimationRate = resourceMap.getInteger("StatusBar.busyAnimationRate");
        busyIconTimer = new Timer(busyAnimationRate, new ActionListener()
        {

            public void actionPerformed(ActionEvent e)
            {
                busyIconIndex = (busyIconIndex + 1) % busyIcons.length;
                statusAnimationLabel.setIcon(busyIcons[busyIconIndex]);
            }
        });

        try
        {
            config = dcu.setConfig();
        }
        catch (Exception ex)
        {
            Util.logStackTrace(ex);
            Util.exitApp(1);
        }
        if (config.getLogFileName() != null)
            logFileName.setText(config.getLogFileName());
        setList();
        if (dcu.hasRealtime())
        {
            setRealtimeConnections();
        }
        progressBar.setVisible(false);

        // connecting action tasks to status bar via TaskMonitor
        TaskMonitor taskMonitor = new TaskMonitor(getApplication().getContext());
        taskMonitor.addPropertyChangeListener(new java.beans.PropertyChangeListener()
        {

            public void propertyChange(java.beans.PropertyChangeEvent evt)
            {
                String propertyName = evt.getPropertyName();
                if ("started".equals(propertyName))
                {
                    if (!busyIconTimer.isRunning())
                    {
                        statusAnimationLabel.setIcon(busyIcons[0]);
                        busyIconIndex = 0;
                        busyIconTimer.start();
                    }
                    progressBar.setVisible(true);
                    progressBar.setIndeterminate(true);
                } else if ("done".equals(propertyName))
                {
                    busyIconTimer.stop();
                    statusAnimationLabel.setIcon(idleIcon);
                    progressBar.setVisible(false);
                    progressBar.setValue(0);
                } else if ("message".equals(propertyName))
                {
                    String text = (String) (evt.getNewValue());
                    statusMessageLabel.setText((text == null) ? "" : text);
                    messageTimer.restart();
                } else if ("progress".equals(propertyName))
                {
                    int value = (Integer) (evt.getNewValue());
                    progressBar.setVisible(true);
                    progressBar.setIndeterminate(false);
                    progressBar.setValue(value);
                }
            }
        });
        // Task execution timer
        taskTimer = new Timer(1000, taskTimerAction);
        taskTimer.start();
        if (dcu.hasRealtime())
        {
            Map<String, RealtimeConnection> rcs = config.getRealtimeConnections();
            for (String s : rcs.keySet())
            {
                RealtimeConnection rc = rcs.get(s);
                if (rc.getName() == null)
                {
                    rc.setName(s);
                }
                Thread t = new Thread(new RealtimeThread(rcs.get(s), va, config, dcu, s, activityLog, statusMap));
                t.start();
            }
            Timer connectTimer = new Timer(15000, new ActionListener()
            {

                public void actionPerformed(ActionEvent e)
                {
                    setRealtimeConnections();
                }
            });
            connectTimer.start();
        }
        realtimePane.setVisible(dcu.hasRealtime());
    }

    public GraphicsObject getGraphicsObject()
    {
        return go;
    }
    ActionListener taskTimerAction = new ActionListener()
    {

        public void actionPerformed(ActionEvent e)
        {
            if (running.get())
            {
                return;
            }
            Calendar curTime = new GregorianCalendar();
            List<Task> taskList = new ArrayList<Task>();
            for (Entry<Task, Calendar> en : taskCalendar.entrySet())
            {
                if (en.getValue().before(curTime))
                {
                    taskList.add(en.getKey());
                }
            }
            if (!taskList.isEmpty())
            {
                running.set(true);
                LoadThread lt = new LoadThread(app, frame, false, taskList);
                lt.start();
            }
        }
    };

    @Action
    public void showAboutBox()
    {
        if (aboutBox == null)
        {
            JFrame mainFrame = DataConductorApp.getApplication().getMainFrame();
            aboutBox = new DataConductorAboutBox(mainFrame);
            aboutBox.setLocationRelativeTo(mainFrame);
        }
        DataConductorApp.getApplication().show(aboutBox);
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        mainPanel = new javax.swing.JPanel();
        realtimePane = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        realtimeTable = new javax.swing.JTable();
        addRealtimeButton = new javax.swing.JButton();
        editRealtimeButton = new javax.swing.JButton();
        deleteRealtimeButton = new javax.swing.JButton();
        viewActivityButton = new javax.swing.JButton();
        logFileName = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        taskPanel = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        taskTable = new javax.swing.JTable();
        addTaskButton = new javax.swing.JButton();
        editTaskButton = new javax.swing.JButton();
        deleteTaskButton = new javax.swing.JButton();
        runTaskButton = new javax.swing.JButton();
        menuBar = new javax.swing.JMenuBar();
        javax.swing.JMenu fileMenu = new javax.swing.JMenu();
        saveMenuItem = new javax.swing.JMenuItem();
        javax.swing.JMenuItem exitMenuItem = new javax.swing.JMenuItem();
        localETL = new javax.swing.JMenu();
        localEtlConfig = new javax.swing.JMenuItem();
        javax.swing.JMenu helpMenu = new javax.swing.JMenu();
        javax.swing.JMenuItem aboutMenuItem = new javax.swing.JMenuItem();
        checkForUpdateButton = new javax.swing.JMenuItem();
        statusPanel = new javax.swing.JPanel();
        statusMessageLabel = new javax.swing.JLabel();
        javax.swing.JSeparator statusPanelSeparator = new javax.swing.JSeparator();
        statusAnimationLabel = new javax.swing.JLabel();
        progressBar = new javax.swing.JProgressBar();

        mainPanel.setName("mainPanel"); // NOI18N
        mainPanel.setPreferredSize(new java.awt.Dimension(500, 575));

        org.jdesktop.application.ResourceMap resourceMap = org.jdesktop.application.Application.getInstance(com.birst.dataconductor.DataConductorApp.class).getContext().getResourceMap(DataConductorView.class);
        realtimePane.setBorder(javax.swing.BorderFactory.createTitledBorder(resourceMap.getString("realtimePane.border.title"))); // NOI18N
        realtimePane.setName("realtimePane"); // NOI18N

        jScrollPane2.setName("jScrollPane2"); // NOI18N

        realtimeTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Name", "Details", "Status"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.Object.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        realtimeTable.setName("realtimeTable"); // NOI18N
        jScrollPane2.setViewportView(realtimeTable);
        realtimeTable.getColumnModel().getColumn(0).setPreferredWidth(100);
        realtimeTable.getColumnModel().getColumn(0).setHeaderValue(resourceMap.getString("taskTable.columnModel.title0")); // NOI18N
        realtimeTable.getColumnModel().getColumn(1).setPreferredWidth(200);
        realtimeTable.getColumnModel().getColumn(1).setHeaderValue(resourceMap.getString("realtimeTable.columnModel.title1")); // NOI18N

        javax.swing.ActionMap actionMap = org.jdesktop.application.Application.getInstance(com.birst.dataconductor.DataConductorApp.class).getContext().getActionMap(DataConductorView.class, this);
        addRealtimeButton.setAction(actionMap.get("createRealtimeConnection")); // NOI18N
        addRealtimeButton.setText(resourceMap.getString("addRealtimeButton.text")); // NOI18N
        addRealtimeButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        addRealtimeButton.setMargin(new java.awt.Insets(2, 5, 2, 5));
        addRealtimeButton.setMaximumSize(new java.awt.Dimension(120, 23));
        addRealtimeButton.setMinimumSize(new java.awt.Dimension(100, 23));
        addRealtimeButton.setName("addRealtimeButton"); // NOI18N
        addRealtimeButton.setPreferredSize(new java.awt.Dimension(120, 25));
        addRealtimeButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        editRealtimeButton.setAction(actionMap.get("editRealtimeConnection")); // NOI18N
        editRealtimeButton.setText(resourceMap.getString("editRealtimeButton.text")); // NOI18N
        editRealtimeButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        editRealtimeButton.setMargin(new java.awt.Insets(2, 5, 2, 5));
        editRealtimeButton.setMaximumSize(new java.awt.Dimension(120, 23));
        editRealtimeButton.setMinimumSize(new java.awt.Dimension(100, 23));
        editRealtimeButton.setName("editRealtimeButton"); // NOI18N
        editRealtimeButton.setPreferredSize(new java.awt.Dimension(120, 25));
        editRealtimeButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        deleteRealtimeButton.setAction(actionMap.get("deleteRealtimeConnection")); // NOI18N
        deleteRealtimeButton.setText(resourceMap.getString("deleteRealtimeButton.text")); // NOI18N
        deleteRealtimeButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        deleteRealtimeButton.setMargin(new java.awt.Insets(2, 5, 2, 5));
        deleteRealtimeButton.setMaximumSize(new java.awt.Dimension(120, 23));
        deleteRealtimeButton.setMinimumSize(new java.awt.Dimension(100, 23));
        deleteRealtimeButton.setName("deleteRealtimeButton"); // NOI18N
        deleteRealtimeButton.setPreferredSize(new java.awt.Dimension(120, 25));
        deleteRealtimeButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        viewActivityButton.setAction(actionMap.get("viewActivity")); // NOI18N
        viewActivityButton.setText(resourceMap.getString("viewActivityButton.text")); // NOI18N
        viewActivityButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        viewActivityButton.setMargin(new java.awt.Insets(2, 5, 2, 5));
        viewActivityButton.setMaximumSize(new java.awt.Dimension(120, 23));
        viewActivityButton.setMinimumSize(new java.awt.Dimension(100, 23));
        viewActivityButton.setName("viewActivityButton"); // NOI18N
        viewActivityButton.setPreferredSize(new java.awt.Dimension(120, 25));
        viewActivityButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        logFileName.setText(resourceMap.getString("logFileName.text")); // NOI18N
        logFileName.setName("logFileName"); // NOI18N

        jLabel3.setText(resourceMap.getString("jLabel3.text")); // NOI18N
        jLabel3.setName("jLabel3"); // NOI18N

        javax.swing.GroupLayout realtimePaneLayout = new javax.swing.GroupLayout(realtimePane);
        realtimePane.setLayout(realtimePaneLayout);
        realtimePaneLayout.setHorizontalGroup(
            realtimePaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(realtimePaneLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(realtimePaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 517, Short.MAX_VALUE)
                    .addGroup(realtimePaneLayout.createSequentialGroup()
                        .addComponent(addRealtimeButton, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(editRealtimeButton, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(deleteRealtimeButton, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(viewActivityButton, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(realtimePaneLayout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(logFileName, javax.swing.GroupLayout.DEFAULT_SIZE, 464, Short.MAX_VALUE)))
                .addContainerGap())
        );

        realtimePaneLayout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {addRealtimeButton, deleteRealtimeButton, editRealtimeButton, viewActivityButton});

        realtimePaneLayout.setVerticalGroup(
            realtimePaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(realtimePaneLayout.createSequentialGroup()
                .addGroup(realtimePaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(addRealtimeButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(editRealtimeButton, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(deleteRealtimeButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(viewActivityButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(realtimePaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(logFileName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3))
                .addContainerGap())
        );

        realtimePaneLayout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {addRealtimeButton, deleteRealtimeButton, editRealtimeButton, viewActivityButton});

        viewActivityButton.getAccessibleContext().setAccessibleName(resourceMap.getString("viewActivityButton.AccessibleContext.accessibleName")); // NOI18N

        taskPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(resourceMap.getString("taskPanel.border.title"))); // NOI18N
        taskPanel.setName("taskPanel"); // NOI18N

        jScrollPane1.setName("dataSources"); // NOI18N

        taskTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Type", "Next Run"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        taskTable.setName("taskTable"); // NOI18N
        jScrollPane1.setViewportView(taskTable);
        taskTable.getColumnModel().getColumn(0).setPreferredWidth(100);
        taskTable.getColumnModel().getColumn(0).setHeaderValue(resourceMap.getString("taskTable.columnModel.title0")); // NOI18N

        javax.swing.GroupLayout taskPanelLayout = new javax.swing.GroupLayout(taskPanel);
        taskPanel.setLayout(taskPanelLayout);
        taskPanelLayout.setHorizontalGroup(
            taskPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(taskPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 517, Short.MAX_VALUE)
                .addContainerGap())
        );
        taskPanelLayout.setVerticalGroup(
            taskPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(taskPanelLayout.createSequentialGroup()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 216, Short.MAX_VALUE)
                .addContainerGap())
        );

        addTaskButton.setAction(actionMap.get("addTask")); // NOI18N
        addTaskButton.setBorder(null);
        addTaskButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        addTaskButton.setMaximumSize(new java.awt.Dimension(100, 51));
        addTaskButton.setMinimumSize(new java.awt.Dimension(100, 51));
        addTaskButton.setName("addTaskButton"); // NOI18N
        addTaskButton.setPreferredSize(new java.awt.Dimension(100, 51));
        addTaskButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        editTaskButton.setAction(actionMap.get("editTask")); // NOI18N
        editTaskButton.setText(resourceMap.getString("editTaskButton.text")); // NOI18N
        editTaskButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        editTaskButton.setMaximumSize(new java.awt.Dimension(100, 59));
        editTaskButton.setMinimumSize(new java.awt.Dimension(100, 59));
        editTaskButton.setName("editTaskButton"); // NOI18N
        editTaskButton.setPreferredSize(new java.awt.Dimension(100, 59));
        editTaskButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        deleteTaskButton.setAction(actionMap.get("deleteTask")); // NOI18N
        deleteTaskButton.setText(resourceMap.getString("deleteTaskButton.text")); // NOI18N
        deleteTaskButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        deleteTaskButton.setMaximumSize(new java.awt.Dimension(100, 59));
        deleteTaskButton.setName("deleteTaskButton"); // NOI18N
        deleteTaskButton.setPreferredSize(new java.awt.Dimension(100, 59));
        deleteTaskButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        runTaskButton.setAction(actionMap.get("runTask")); // NOI18N
        runTaskButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        runTaskButton.setName("runTaskButton"); // NOI18N
        runTaskButton.setPreferredSize(new java.awt.Dimension(105, 59));
        runTaskButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        javax.swing.GroupLayout mainPanelLayout = new javax.swing.GroupLayout(mainPanel);
        mainPanel.setLayout(mainPanelLayout);
        mainPanelLayout.setHorizontalGroup(
            mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(mainPanelLayout.createSequentialGroup()
                .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(mainPanelLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(taskPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(mainPanelLayout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addComponent(addTaskButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(editTaskButton, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(12, 12, 12)
                        .addComponent(deleteTaskButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(10, 10, 10)
                        .addComponent(runTaskButton, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(53, 53, 53))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, mainPanelLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(realtimePane, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
        );

        mainPanelLayout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {addTaskButton, deleteTaskButton, editTaskButton, runTaskButton});

        mainPanelLayout.setVerticalGroup(
            mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(mainPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(addTaskButton, javax.swing.GroupLayout.PREFERRED_SIZE, 57, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(editTaskButton, javax.swing.GroupLayout.PREFERRED_SIZE, 57, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(deleteTaskButton, javax.swing.GroupLayout.PREFERRED_SIZE, 57, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(runTaskButton, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(11, 11, 11)
                .addComponent(taskPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(realtimePane, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        mainPanelLayout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {addTaskButton, deleteTaskButton, editTaskButton});

        taskPanel.getAccessibleContext().setAccessibleName(resourceMap.getString("jPanel1.AccessibleContext.accessibleName")); // NOI18N

        menuBar.setName("menuBar"); // NOI18N

        fileMenu.setText(resourceMap.getString("fileMenu.text")); // NOI18N
        fileMenu.setName("fileMenu"); // NOI18N

        saveMenuItem.setAction(actionMap.get("saveSettings")); // NOI18N
        saveMenuItem.setActionCommand(resourceMap.getString("saveMenuItem.actionCommand")); // NOI18N
        saveMenuItem.setName("saveMenuItem"); // NOI18N
        fileMenu.add(saveMenuItem);

        exitMenuItem.setAction(actionMap.get("quit")); // NOI18N
        exitMenuItem.setName("exitMenuItem"); // NOI18N
        fileMenu.add(exitMenuItem);

        menuBar.add(fileMenu);

        localETL.setText(resourceMap.getString("localETL.text")); // NOI18N
        localETL.setName("localETL"); // NOI18N

        localEtlConfig.setAction(actionMap.get("setUpLocalETLConfiguration")); // NOI18N
        localEtlConfig.setText(resourceMap.getString("localEtlConfig.text")); // NOI18N
        localEtlConfig.setName("localEtlConfig"); // NOI18N
        localETL.add(localEtlConfig);

        menuBar.add(localETL);

        helpMenu.setText(resourceMap.getString("helpMenu.text")); // NOI18N
        helpMenu.setName("helpMenu"); // NOI18N

        aboutMenuItem.setAction(actionMap.get("showAboutBox")); // NOI18N
        aboutMenuItem.setName("aboutMenuItem"); // NOI18N
        helpMenu.add(aboutMenuItem);

        checkForUpdateButton.setAction(actionMap.get("checkForBirstConnectVersionUpdate")); // NOI18N
        checkForUpdateButton.setText(resourceMap.getString("checkForUpdateButton.text")); // NOI18N
        checkForUpdateButton.setName("checkForUpdateButton"); // NOI18N
        helpMenu.add(checkForUpdateButton);

        menuBar.add(helpMenu);

        statusPanel.setName("statusPanel"); // NOI18N

        statusMessageLabel.setName("statusMessageLabel"); // NOI18N

        statusPanelSeparator.setName("statusPanelSeparator"); // NOI18N

        statusAnimationLabel.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        statusAnimationLabel.setName("statusAnimationLabel"); // NOI18N

        progressBar.setName("progressBar"); // NOI18N

        javax.swing.GroupLayout statusPanelLayout = new javax.swing.GroupLayout(statusPanel);
        statusPanel.setLayout(statusPanelLayout);
        statusPanelLayout.setHorizontalGroup(
            statusPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(statusPanelSeparator, javax.swing.GroupLayout.DEFAULT_SIZE, 569, Short.MAX_VALUE)
            .addGroup(statusPanelLayout.createSequentialGroup()
                .addGroup(statusPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(statusPanelLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(statusMessageLabel)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 533, Short.MAX_VALUE))
                    .addGroup(statusPanelLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(progressBar, javax.swing.GroupLayout.PREFERRED_SIZE, 274, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)))
                .addComponent(statusAnimationLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        statusPanelLayout.setVerticalGroup(
            statusPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(statusPanelLayout.createSequentialGroup()
                .addComponent(statusPanelSeparator, javax.swing.GroupLayout.PREFERRED_SIZE, 2, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(statusPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(statusMessageLabel)
                    .addComponent(statusAnimationLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(progressBar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(3, 3, 3))
        );

        setComponent(mainPanel);
        setMenuBar(menuBar);
        setStatusBar(statusPanel);
    }// </editor-fold>//GEN-END:initComponents

    private class LoadThread extends Thread
    {
        JFrame parent;
        boolean interactive;
        List<Task> taskList;
        SingleFrameApplication app;
        
        public LoadThread(SingleFrameApplication app, JFrame parent, boolean interactive, List<Task> taskList)
        {
            this.parent = parent;
            this.interactive = interactive;
            this.taskList = taskList;
            this.app = app;
        }

        @Override
        public void run()
        {
            progressBar.setVisible(true);
            addTaskButton.setEnabled(false);
            editTaskButton.setEnabled(false);
            deleteTaskButton.setEnabled(false);
            runTaskButton.setEnabled(false);
            taskTable.setEnabled(false);
            statusAnimationLabel.setVisible(true);
            busyIconTimer.start();
            List<Task> compeletedTaskList = load(taskList, interactive);
            int numComplete = compeletedTaskList.size();
            if (interactive)
            {
                boolean processing = false;
                int[] selections = taskTable.getSelectedRows();
                for (int index = 0; index < taskList.size(); index++)
                {
                    boolean found = selections.length == 0;
                    if (!found)
                    {
                        for (int i : selections)
                        {
                            if (i == index)
                            {
                                found = true;
                                break;
                            }
                        }
                    }
                    if (!found)
                    {
                        continue;
                    }
                    Task t = taskList.get(index);
                    if (t.isEnabled() && t.isProcessAutomatically() && compeletedTaskList.contains(t))
                    {
                        processing = true;
                        break;
                    }
                }
                if (processing && numComplete > 0)
                    JOptionPane.showMessageDialog(parent, numComplete + " upload tasks have been completed. Processing has been initiated.");
                else
                    JOptionPane.showMessageDialog(parent, numComplete + " upload tasks have been completed.");
            }
            addTaskButton.setEnabled(true);
            editTaskButton.setEnabled(true);
            deleteTaskButton.setEnabled(true);
            runTaskButton.setEnabled(true);
            busyIconTimer.stop();
            statusAnimationLabel.setVisible(false);
            taskTable.setEnabled(true);
            statusMessageLabel.setText(" ");
            progressBar.setVisible(false);
            if (numComplete > 0)
            {
                try
                {
                    config = dcu.setConfig();
                }
                catch (Exception ex)
                {
                    Util.logStackTrace(ex);
                    Util.exitApp(1);
                }
            }
            setList();
            running.set(false);
        }
    }

        public void setList()
    {
        DefaultTableModel tm = new DefaultTableModel(new String[]
                {
                    "Name", "Next Run"
                }, 0);
        setSchedules();
        for (Task t: config.getTaskList())
        {
                tm.addRow(new String[]
                        {
                            t.getName(), taskCalendar.get(t) == null ? "" : taskCalendar.get(t).getTime().toString()
                        });
        }
        taskTable.setModel(tm);
    }

    public void setRealtimeConnections()
    {
        DefaultTableModel tm = new DefaultTableModel(new String[]
                {
                    "Name", "Details", "Status"
                }, 0);
        for (Entry<String, RealtimeConnection> entry: config.getRealtimeConnections().entrySet())
        {
            DatabaseConnection dc = entry.getValue();
            String dbname = dc.getDatabaseName();
            String desc = dc.getServerName() + (dbname == null || dbname.length() == 0 ? "" : "/" + dbname);
            tm.addRow(new String[]
                    {
                        entry.getKey(), desc, entry.getKey()
                    });
        }
        realtimeTable.setModel(tm);
        realtimeTable.getColumnModel().getColumn(2).setCellRenderer(new StatusRenderer());
    }

    URL greenIconURL = getClass().getResource("resources/green_icon.png");
    URL redIconURL = getClass().getResource("resources/red_icon.png");
    
    ImageIcon greenIcon = new ImageIcon(greenIconURL);            
    ImageIcon redIcon = new ImageIcon(redIconURL);            
    
    public class StatusRenderer extends DefaultTableCellRenderer {
        // This method is called each time a cell in a column
        // using this renderer needs to be rendered.
        public Component getTableCellRendererComponent(JTable table, Object value,
                boolean isSelected, boolean hasFocus, int rowIndex, int vColIndex) 
        {
            if (statusMap.containsKey(value) && statusMap.get(value))
                this.setIcon(greenIcon);
            else
                this.setIcon(redIcon);
            this.setHorizontalAlignment(SwingConstants.CENTER);
            return this;
        }
    }

    @Action
    public void addTask()
    {
        EditTask et = new EditTask(this.getFrame(), true, null, config);
        et.setLocationRelativeTo(null);
        et.showHideSAPConnector(dcu.hasSAPConnector());
        et.setVisible(true);
        Task t = et.getTask();
        if (t != null)
        {
            boolean found =false;
            for(Task st: config.getTaskList())
            {
                if (t.getName().equals(st.getName()))
                {
                    found = true;
                    break;
                }
            }
            if (!found)
                config.getTaskList().add(t);
            setList();
        }
    }

    @Action
    public void editTask()
    {
        int index = taskTable.getSelectedRow();
        if (index < 0)
            return;
        EditTask et = new EditTask(this.getFrame(), true, config.getTaskList().get(index), config);
        et.setLocationRelativeTo(null);
        et.showHideSAPConnector(dcu.hasSAPConnector());
        et.setVisible(true);
        Task t = et.getTask();
        if (t != null)
        {
            config.getTaskList().set(taskTable.getSelectedRow(), t);
            setList();
        }
    }

    @Action
    public void deleteTask()
    {
        int index = taskTable.getSelectedRow();
        if (index < 0)
            return;
        config.getTaskList().remove(index);
        setList();
    }

    @Action
    public void runTask()
    {
        LoadThread lt = new LoadThread(app, this.getFrame(), true, config.getTaskList());
        lt.start();
    }
    
    public List<Task> load(List<Task> taskList, boolean interactive)
    {
        int[] selections = taskTable.getSelectedRows();
        List<Task> compeletedTaskList = new ArrayList<Task>();
        List<Task> tasksToRun = new ArrayList<Task>();
        Map varMap=null;
        for (int index = 0; index < taskList.size(); index++)
        {
            boolean found = !interactive || selections.length == 0;
            if (!found)
            {
                for (int i : selections)
                {
                    if (i == index)
                    {
                        found = true;
                        break;
                    }
                }
            }
            if (!found)
            {
                continue;
            }
            Task t = taskList.get(index);
            if (!t.isEnabled())
            {
                continue;
            }
            
            tasksToRun.add(t);            
        }
        boolean downloadCloudSources = false;
        for (Task t : tasksToRun)
        {
            if (t.isDowanloadCloudSources())
            {
                downloadCloudSources = true;
                break;
            }
        }
        //Get all the variables for all queries before any task run start
        varMap=dcu.getVariablesForAllQuerySources(tasksToRun, config);
        for (Task t : tasksToRun)
        {            
            WebDAVError werror = dcu.loadTask(t,config,varMap);
            if (werror != null)
            {
                 go.showMessageDialog("Failed to complete task: " + t.getName() + ": " + werror.getMessage());
                 break;
            }
            else
            {
                if (downloadCloudSources && t.isDowanloadCloudSources())
                {
                    downloadCloudSources = false;
                    CloudDataDownloader cdd = new CloudDataDownloader(dcu.getUname(), dcu.getPword(), dcu.getSpace(), dcu.getUrl(), config.getLocalETLConfig());
                    if (cdd.downloadData(CloudDataDownloader.RequestType.DownloadCloudData) == null)
                        compeletedTaskList.add(t);
                }
                else
                {
                    compeletedTaskList.add(t);
                }
            }
        }
        return compeletedTaskList;
    }

    public void setSchedules()
    {
        if (config == null)
            return;
        taskCalendar.clear();
        Calendar curTime = Calendar.getInstance();
        for(Task t: config.getTaskList())
        {
            if (t.isEnabled() && t.isScheduled())
            {
                Calendar time = (Calendar) curTime.clone();
                time.set(Calendar.HOUR_OF_DAY, t.getHour()+(t.isPm()?12:0));
                time.set(Calendar.MINUTE, t.getMin());
                time.set(Calendar.SECOND, 0);
                if (time.before(curTime))
                    time.add(Calendar.DATE, 1);
                boolean valid = true;
                if (t.getRecurrenceType() == Task.RECURRENCE_TYPE_WEEKLY)
                {
                    boolean found = false;
                    for(int i = 0; i < 7; i++)
                    {
                        int val = time.get(Calendar.DAY_OF_WEEK);
                        if (val == Calendar.MONDAY && !t.isMonday())
                            time.add(Calendar.DATE, 1);
                        else if (val == Calendar.TUESDAY && !t.isTuesday())
                            time.add(Calendar.DATE, 1);
                        else if (val == Calendar.WEDNESDAY && !t.isWednesday())
                            time.add(Calendar.DATE, 1);
                        else if (val == Calendar.THURSDAY && !t.isThursday())
                            time.add(Calendar.DATE, 1);
                        else if (val == Calendar.FRIDAY && !t.isFriday())
                            time.add(Calendar.DATE, 1);
                        else if (val == Calendar.SATURDAY && !t.isSaturday())
                            time.add(Calendar.DATE, 1);
                        else if (val == Calendar.SUNDAY && !t.isSunday())
                            time.add(Calendar.DATE, 1);
                        else
                        {
                            found = true;
                            break;
                        }
                    }
                    if (!found)
                        valid = false;
                } else if (t.getRecurrenceType() == Task.RECURRENCE_TYPE_MONTHLY)
                {
                    time.set(Calendar.DAY_OF_MONTH, t.getDay());
                    if (time.before(curTime))
                        time.add(Calendar.MONTH, 1);
                }
                if (valid)
                {
                    time.getTime();
                    taskCalendar.put(t, time);
                }
            }
        }
    }

    public DataConductorUtil getDataConductorUtil()
    {
        return dcu;
    }

    @Action
    public void saveSettings() {
        File dirFile = null;
        try {
            if (DataConductorUtil.isLocalDCConfigInUse())
                dirFile = new File(DataConductorUtil.localDCConfigLocation);
            else
                dirFile = dcu.getTempDirectory();
            go.setStatusMessage("Saving Settings");
            // Upload data sources settings
            if (logFileName.getText().length() > 0) {
                config.setLogFileName(logFileName.getText());
                activityLog.setLogFile(config.getLogFileName());
            } else {
                config.setLogFileName(null);
                activityLog.setLogFile(null);
            }
            dcu.saveSettings(config, dirFile);
        }
        finally
        {
            dcu.cleanupTempDirectory(dirFile);
        }
    }

    @Action
    public void createRealtimeConnection()
    {
        EditRealtimeConnection erc = new EditRealtimeConnection(this.getFrame(), true, null, null, false);
        erc.setLocationRelativeTo(null);
        erc.setVisible(true);
        RealtimeConnection dc = erc.getConnection();
        String visiblename = erc.getName();
        if (dc != null && visiblename != null && visiblename.length() > 0 && !visiblename.equals("Default Connection"))
        {
            boolean found = config.isVisibleNameExist(visiblename);
            if (found)
                go.showInfoMessageDialog("A Live Access connection with the name \"" + visiblename + "\" already exist. Please choose a different name.");
            else
            {
                Map response = dcu.getRealtimeConnectionStatus(OP_ADD_CONNECTION, dc.getName(), visiblename);
                if (response.containsKey("statuscode"))
                {
                    int statusCode = (Integer) response.get("statuscode");
                    if (statusCode == 1)
                    {
                        go.showInfoMessageDialog("A Live Access connection with the name \"" + visiblename + "\" already exist for space. Please choose a different name.");
                    }
                    else if (statusCode == 0)
                    {
                        if (response.containsKey("name"))
                        {
                            String name = (String)response.get("name");
                            if (name != null)
                            {
                                dc.setName(name);
                                config.getRealtimeConnections().put(visiblename, dc);
                                Thread t = new Thread(new RealtimeThread(dc, va, config, dcu, visiblename, activityLog, statusMap));
                                t.start();
                            }
                        }
                    }
                }
                else if (response.containsKey("error"))
                {
                    go.showMessageDialog(response.get("error").toString());
                }
            }
        }
        setRealtimeConnections();
    }

    @Action
    public void deleteRealtimeConnection()
    {
        int index = realtimeTable.getSelectedRow();
        if (index < 0)
            return;
        String visiblename = (String) realtimeTable.getModel().getValueAt(index, 0);
        RealtimeConnection rc = config.getRealtimeConnections().get(visiblename);
        LocalETLConfig localETLConfig = config.getLocalETLConfig();
        if (localETLConfig != null && localETLConfig.isUseLocalETLConnection())
        {
            String localETLConn = localETLConfig.getLocalETLConnection();
            if (rc.getName().equals(localETLConn))
            {
                go.showInfoMessageDialog("The live access connection \"" + visiblename + "\" is currently in use for local setup. Please deactivate the connection first.");
                return;
            }
        }
        Map response = dcu.getRealtimeConnectionStatus(OP_DELETE_CONNECTION, rc.getName(), visiblename);
        if (response.containsKey("statuscode"))
        {
            int statusCode = (Integer) response.get("statuscode");
            if (statusCode == 1)
            {
                go.showInfoMessageDialog("The Live Access connection \"" + visiblename +"\" is currently in use. Please deactivate the connection first.");
            }
            else if (statusCode == 0)
            {
                rc.cancelConnect();
                config.getRealtimeConnections().remove(visiblename);
            }
        }
        else if (response.containsKey("error"))
        {
            go.showMessageDialog(response.get("error").toString());
        }
        setRealtimeConnections();
    }

    @Action
    public void editRealtimeConnection()
    {

        int index = realtimeTable.getSelectedRow();
        if (index < 0)
            return;
        String oldname = (String) realtimeTable.getModel().getValueAt(index, 0);
        RealtimeConnection oldConn = config.getRealtimeConnections().get(oldname);
        EditRealtimeConnection erc = null;
        if (config.getLocalETLConfig() != null && config.getLocalETLConfig().getLocalETLConnection() != null
                && config.getLocalETLConfig().getLocalETLConnection().equals(oldConn.getName()))
        {
            erc = new EditRealtimeConnection(this.getFrame(), true, oldname, oldConn, true);
        }
        else
        {
            erc = new EditRealtimeConnection(this.getFrame(), true, oldname, oldConn, false);
        }
        erc.setLocationRelativeTo(null);
        erc.setVisible(true);
        RealtimeConnection dc = erc.getConnection();
        String newname = erc.getName();
        if (dc != null && newname != null && newname.length() > 0 && !newname.equals("Default Connection"))
        {
            dc.setName(oldConn.getName());
            dc.setSchema(oldConn.getSchema());
            boolean hasValidName = oldname.equals(newname);
            if (!oldname.equals(newname))
            {
                if (config.isVisibleNameExist(newname))
                {
                    go.showInfoMessageDialog("A Live Access connection with the name \"" + newname + "\" already exist. Please choose a different name.");
                }
                else
                {
                    Map response = dcu.getRealtimeConnectionStatus(OP_EDIT_CONNECTION, dc.getName(), newname);
                    if (response.containsKey("statuscode"))
                    {
                        int statusCode = (Integer) response.get("statuscode");
                        hasValidName = statusCode == 0;
                        if (statusCode == 1)
                           go.showInfoMessageDialog("A Live Access connection with the name \"" + newname + "\" already exist for space. Please choose a different name.");
                    }
                    else if (response.containsKey("error"))
                    {
                        go.showMessageDialog(response.get("error").toString());
                    }
                }
             }
             if (hasValidName)
             {
                oldConn.cancelConnect();
                config.getRealtimeConnections().remove(oldname);
                config.getRealtimeConnections().put(newname, dc);
                Thread t = new Thread(new RealtimeThread(dc, va, config, dcu, newname, activityLog, statusMap));
                t.start();
             }
        }
        setRealtimeConnections();
    }
    
    @Action
    public void viewActivity()
    {
        va.setLocationRelativeTo(null);
        va.setVisible(true);
    }

    @Action
    public void setUpLocalETLConfiguration() {
        LocalETLConfigDialog etlConfigDialog = new LocalETLConfigDialog(this.getFrame(), true, config);
        etlConfigDialog.setLocationRelativeTo(null);
        etlConfigDialog.setVisible(true);
    }

    @Action
    public void checkForBirstConnectVersionUpdate() {
        String serverBCVersion = null;
        WebDavResponse wdr = WebDavUploader.getServerBirstConnectVersion(dcu.getUrl(), dcu.getSpace());
        if (wdr != null && wdr.getCode() == 0)
        {
            serverBCVersion = wdr.getMessage();
        }
        if (serverBCVersion == null || serverBCVersion.equals("-1"))
        {
            JOptionPane.showMessageDialog(frame, "Could not retrieve BirstConnect version from server.");
            return;
        }
        boolean isLatest = DataConductorUtil.isLatestVersion(serverBCVersion);
        if (!isLatest)
        {
            JOptionPane.showMessageDialog(frame, "BirstConnect version (" + DataConductorUtil.BirstConnectVersion.trim() + ") " +
                    "is older than server's version (" + serverBCVersion.trim() + ").");
            return;
        }
        else
        {
            JOptionPane.showMessageDialog(frame, "System is running latest Birst Connect Version (" + DataConductorUtil.BirstConnectVersion.trim() +").");
            return;
        }
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton addRealtimeButton;
    private javax.swing.JButton addTaskButton;
    private javax.swing.JMenuItem checkForUpdateButton;
    private javax.swing.JButton deleteRealtimeButton;
    private javax.swing.JButton deleteTaskButton;
    private javax.swing.JButton editRealtimeButton;
    private javax.swing.JButton editTaskButton;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JMenu localETL;
    private javax.swing.JMenuItem localEtlConfig;
    private javax.swing.JTextField logFileName;
    private javax.swing.JPanel mainPanel;
    private javax.swing.JMenuBar menuBar;
    private javax.swing.JProgressBar progressBar;
    private javax.swing.JPanel realtimePane;
    private javax.swing.JTable realtimeTable;
    private javax.swing.JButton runTaskButton;
    private javax.swing.JMenuItem saveMenuItem;
    private javax.swing.JLabel statusAnimationLabel;
    private javax.swing.JLabel statusMessageLabel;
    private javax.swing.JPanel statusPanel;
    private javax.swing.JPanel taskPanel;
    private javax.swing.JTable taskTable;
    private javax.swing.JButton viewActivityButton;
    // End of variables declaration//GEN-END:variables
    private final Timer messageTimer;
    private Timer busyIconTimer = null;
    private final Icon idleIcon;
    private final Icon[] busyIcons = new Icon[15];
    private int busyIconIndex = 0;
    private JDialog aboutBox;
}
