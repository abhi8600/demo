/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.birst.dataconductor;

import java.util.Map;

/**
 *
 * @author mpandit
 */
public class RealtimeThread extends Thread
    {
        private RealtimeConnection rc;
        private ViewActivity va;
        private DataConductorConfig config;
        private DataConductorUtil dcu;
        private String rcVisibleName;
        private ActivityLog activityLog;
        private Map<String, Boolean> statusMap;

        public RealtimeThread(RealtimeConnection rc, ViewActivity va, DataConductorConfig config, DataConductorUtil dcu, String rcVisibleName, 
                ActivityLog activityLog, Map<String, Boolean> statusMap)
        {
            this.rc = rc;
            this.va = va;
            this.config = config;
            this.dcu = dcu;
            this.rcVisibleName = rcVisibleName;
            this.activityLog = activityLog;
            this.statusMap = statusMap;
        }

        public void run()
        {
            rc.connect(rc.getName(), dcu.getUname(), dcu.getPword(), dcu.getSpace(), dcu.getUrl(), activityLog, va, statusMap, config, dcu, rcVisibleName);
        }
    }
