package com.birst.dataconductor.localetl;

import com.birst.dataconductor.ActivityLog;
import com.birst.dataconductor.DataConductorConfig;
import com.birst.dataconductor.DataConductorUtil;
import com.birst.dataconductor.RealtimeConnection;
import com.birst.dataconductor.ViewActivity;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.Map;
import org.apache.log4j.Logger;

public class DeleteData extends Thread
{
    private String connectionName;
    private String uname;
    private String pword;
    private String space;
    private String url;
    private ActivityLog activityLog;
    private ViewActivity va;
    Map<String, Boolean> statusMap;
    private DataConductorConfig config;
    private DataConductorUtil dcu;
    private int qindex;
    private String command;
    private int iteration;
    private String cmdArgs;
    
    private final static Logger logger = Logger.getLogger(DeleteData.class);

    public DeleteData(String connectionName, String uname, String pword, String space, String url,
            ActivityLog activityLog, ViewActivity va, Map<String, Boolean> statusMap,
            DataConductorConfig config, DataConductorUtil dcu, int qindex, String command, String cmdArgs)
    {
        this.connectionName = connectionName;
        this.uname = uname;
        this.pword = pword;
        this.space = space;
        this.url = url;
        this.activityLog = activityLog;
        this.va = va;
        this.statusMap = statusMap;
        this.config = config;
        this.dcu = dcu;
        this.qindex = qindex;
        this.command = command;
        if (this.command.equals("liveaccessdeletelastload") && cmdArgs != null && !cmdArgs.isEmpty())
        {
            iteration  = Integer.parseInt(cmdArgs);
        }
        else
        {
            this.cmdArgs = cmdArgs;
        }
        logger.info("DeleteData: " + connectionName + ", command: " + command + ", commandargs: " + cmdArgs);
    }

    @Override
    public void run()
    {
        ByteArrayOutputStream baos = null;
        String log = null;
        LocalETLConfig leConfig = null;
        try
        {
            if (command.equals("liveaccessdeletelastload") || command.equals("liveaccessdeletealldata") || command.equals("liveaccessdeletespace"))
            {
                baos = new ByteArrayOutputStream();
                leConfig = config.getLocalETLConfig();
                log = "Initiating " + command + " for load group " + leConfig.getLocalETLLoadGroup();
                logger.debug(log);
                LocalETLUtil.addToActivityLog(va, activityLog, log, null);
                RealtimeConnection rc = LocalETLUtil.getLocalETLConnection(config, connectionName);
                if (rc.getSchema() == null || rc.getSchema().trim().isEmpty()) //schema not exists - only clean directories for delete space or delete all
                {
                    log = "Schema not exists";
                    logger.debug(log);
                    LocalETLUtil.addToActivityLog(va, activityLog, log, null);
                }
                else
                {
                    Object repository = LocalETLUtil.initRepository(url, space, leConfig, activityLog);
                    executeDelete(repository);
                }
                String spDirPath = leConfig.getApplicationDirectory() + "\\" + space;
                cleanDirectories(spDirPath);
                baos.write("OK".getBytes());
            }
            LocalETLUtil.writeData(url, space, connectionName, qindex, baos);
        }
        catch (Exception ex)
        {
        	LocalETLUtil.addToActivityLog(va, activityLog, "Error: " + ex.toString(), ex);
            logger.error(null, ex);
            try {
                if (baos == null)
                    baos = new ByteArrayOutputStream();
                baos.write(("Error: " + ex.getMessage()).getBytes());
                LocalETLUtil.writeData(url, space, connectionName, qindex, baos);
            } catch (Exception ex1) {
            	LocalETLUtil.addToActivityLog(va, activityLog, ex1.toString(), ex1);
                logger.error(null, ex1);
            }
        }
        finally
        {
            log = "Exiting " + command + (leConfig != null ? " for load group " + leConfig.getLocalETLLoadGroup() : "");
            LocalETLUtil.addToActivityLog(va, activityLog, log, null);
        }
    }

    private void executeDelete(Object repository) throws Exception
    {
        LocalETLConfig leConfig = config.getLocalETLConfig();
        String spDirPath = leConfig.getApplicationDirectory() + "\\" + space;
        LocalETLUtil.downloadDeleteDataCommandFile(url, space, leConfig, spDirPath);
        String file = "delete." + leConfig.getLocalETLLoadGroup() + ".cmd";
        LocalETLUtil.runCommands(spDirPath, repository, file, iteration, activityLog);
    }

    public void cleanDirectories(String spDirPath)
    {
        File oFile = new File(spDirPath);
        if(oFile.isDirectory())
        {
            String log = null;
            if (command.equals("liveaccessdeletealldata"))
            {
                String logDir = spDirPath + File.separator + "logs";
                log = "Deleting logs at: " + logDir;
                logger.debug(log);
                LocalETLUtil.addToActivityLog(va, activityLog, log, null);
                LocalETLUtil.deleteFiles(logDir);
            }
            else if (command.equals("liveaccessdeletespace"))
            {
                log = "Creating deleted.lock file and cleaning directories at " + spDirPath;
                logger.debug(log);
                LocalETLUtil.addToActivityLog(va, activityLog, log, null);
                try
                {
                    //create deleted.lock file
                    String file = spDirPath + "\\deleted.lock";
                    File f = new File(file);
                    f.createNewFile();
                    //cleanup directories
                } catch (IOException ex)
                {
                    logger.info("Error while creating deleted lock file at " + spDirPath, ex);
                }
                LocalETLUtil.cleanAndRemoveDir(spDirPath + File.separator + "data");
                LocalETLUtil.cleanAndRemoveDir(spDirPath + File.separator + "catalog");
                LocalETLUtil.cleanAndRemoveDir(spDirPath + File.separator + "logs");
                LocalETLUtil.cleanAndRemoveDir(spDirPath + File.separator + "attachments");
            }
        }
    }
}