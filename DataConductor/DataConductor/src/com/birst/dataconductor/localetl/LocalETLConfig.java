/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.birst.dataconductor.localetl;

import com.birst.dataconductor.*;
import org.jdom.Element;

/**
 *
 * @author mpandit
 */
public class LocalETLConfig {

    private String localETLLoadGroup;
    private boolean useLocalETLConnection;
    private String applicationDirectory;
    private String localETLConnection;

    public String getApplicationDirectory() {
        return applicationDirectory;
    }

    public void setApplicationDirectory(String applicationDirectory) {
        this.applicationDirectory = applicationDirectory;
    }

    public String getLocalETLConnection() {
        return localETLConnection;
    }

    public void setLocalETLConnection(String localETLConnection) {
        this.localETLConnection = localETLConnection;
    }

    public String getLocalETLLoadGroup() {
        return localETLLoadGroup;
    }

    public void setLocalETLLoadGroup(String localETLLoadGroup) {
        this.localETLLoadGroup = localETLLoadGroup;
    }

    public boolean isUseLocalETLConnection() {
        return useLocalETLConnection;
    }

    public void setUseLocalETLConnection(boolean useLocalETLConnection) {
        this.useLocalETLConnection = useLocalETLConnection;
    }

    public void setUseLocalETLConnection(boolean useLocalETLConnection, DataConductorConfig config) {
        this.useLocalETLConnection = useLocalETLConnection;
        if (!useLocalETLConnection) //if local etl connection not in use, unmark all tasks that are marked for localetl
        {
            for (Task task :config.getTaskList())
            {
                task.setLocalETLTask(false);
                task.setDowanloadCloudSources(false);
            }
        }
    }

    public boolean isLocalETLConnection(String connectionName)
    {
        if (connectionName == null || localETLConnection == null)
            return false;
        return connectionName.equalsIgnoreCase(localETLConnection);
    }
    
    public Element getElement()
    {
        Element e = new Element("void");
        e.setAttribute("property", "localETLConfig");
        if (applicationDirectory != null)
        {
            e.addContent(XmlEncoderUtil.getStringPropertyElement("applicationDirectory", applicationDirectory));
        }
        if (localETLConnection != null)
        {
            e.addContent(XmlEncoderUtil.getStringPropertyElement("localETLConnection", localETLConnection));
        }
        if (localETLLoadGroup != null)
        {
            e.addContent(XmlEncoderUtil.getStringPropertyElement("localETLLoadGroup", localETLLoadGroup));
        }
        if (useLocalETLConnection)
        {
            e.addContent(XmlEncoderUtil.getBooleanPropertyElement("useLocalETLConnection"));
        }
        return e;
    }
}
