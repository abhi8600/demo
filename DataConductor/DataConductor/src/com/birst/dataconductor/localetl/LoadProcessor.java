/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.birst.dataconductor.localetl;

import com.birst.dataconductor.ActivityLog;
import com.birst.dataconductor.AuthenticationException;
import com.birst.dataconductor.DataConductorConfig;
import com.birst.dataconductor.DataConductorUtil;
import com.birst.dataconductor.GeneralErrorException;
import com.birst.dataconductor.SpaceUnavailableException;
import com.birst.dataconductor.Util;
import com.birst.dataconductor.ViewActivity;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.util.Map;
import org.apache.log4j.Logger;

/**
 *
 * @author mjani
 */
public class LoadProcessor extends Thread
{
    private String connectionName;
    private String uname;
    private String pword;
    private String space;
    private String url;
    private ActivityLog activityLog;
    private ViewActivity va;
    Map<String, Boolean> statusMap;
    private DataConductorConfig config;
    private DataConductorUtil dcu;
    private int qindex;    
    private String command;
    private int iteration;
    private String cmdArgs;
    private boolean isFinished;

    private final static Logger logger = Logger.getLogger(LoadProcessor.class);

    public LoadProcessor(String connectionName, String uname, String pword, String space, String url,
            ActivityLog activityLog, ViewActivity va, Map<String, Boolean> statusMap,
            DataConductorConfig config, DataConductorUtil dcu, int qindex, String command, String cmdArgs)
    {
        this.connectionName = connectionName;
        this.uname = uname;
        this.pword = pword;
        this.space = space;
        this.url = url;
        this.activityLog = activityLog;
        this.va = va;
        this.statusMap = statusMap;
        this.config = config;
        this.dcu = dcu;
        this.qindex = qindex;
        this.command = command;
        if (this.command.equals("initiateliveaccessload") && cmdArgs != null && !cmdArgs.isEmpty())
        {
            iteration  = Integer.parseInt(cmdArgs);
        }
        else
        {
            this.cmdArgs = cmdArgs;
        }
        logger.info("LoadProcessor: " + connectionName + ", command: " + command + ", commandargs: " + cmdArgs);
    }

    @Override
    public void run()
    {
        ByteArrayOutputStream baos = null;
        try
        {
            if (command.equals("initiateliveaccessload"))
            {
                baos = new ByteArrayOutputStream();
                boolean timeExists = LocalETLUtil.createSchemaAndUpdateRealtimeConnection(Util.MAIN_SCHEMA, config, dcu, connectionName, activityLog, va);
                // always initialize repository first for:
                // 1. to make sure txn_command_history is created
                // 2. to find out if there is any fatal error loading repository itself. If so, Acorn
                // can be intimated that load cannot be initiated.
                Object repository = LocalETLUtil.initRepository(url, space, config.getLocalETLConfig(), activityLog);
                baos.write("OK".getBytes());
                LocalETLUtil.writeData(url, space, connectionName, qindex, baos);
                processLoad(repository, timeExists);
            }
            else
            {
                LocalETLUtil.writeData(url, space, connectionName, qindex, baos);
            }
        } catch (Exception ex)
        {
        	LocalETLUtil.addToActivityLog(va, activityLog, "Error: " + ex.toString(), ex);
            logger.error(null, ex);
            try {
                if (baos == null)
                    baos = new ByteArrayOutputStream();
                baos.write(("Error: " + ex.getMessage()).getBytes());
                LocalETLUtil.writeData(url, space, connectionName, qindex, baos);
            } catch (Exception ex1) {
            	LocalETLUtil.addToActivityLog(va, activityLog, ex1.toString(), ex1);
                logger.error(null, ex1);
            }
        } finally
        {
            isFinished = true;
            if (va != null)
            {
                va.update();
            }
        }
    }

    private void processLoad(Object repository, boolean timeExists) throws IOException, GeneralErrorException, MalformedURLException, AuthenticationException, ClassNotFoundException, IllegalAccessException, NoSuchMethodException, IllegalArgumentException, InvocationTargetException, SpaceUnavailableException
    {
        LocalETLConfig leConfig = config.getLocalETLConfig();
        String spDirPath = leConfig.getApplicationDirectory() + "\\" + space;
        LocalETLUtil.createDirectories(spDirPath);
        String confPath = spDirPath + "\\conf";
        String message = "Initiating process for load group " + leConfig.getLocalETLLoadGroup();
        LocalETLUtil.addToActivityLog(va, activityLog, message, null);
        logger.debug(message);
        if(!timeExists)
        {
            LocalETLUtil.generateTimeTables(dcu, leConfig, spDirPath, confPath, activityLog, va, iteration);
        }
        LocalETLUtil.downloadCommandFile(url, space, leConfig, spDirPath);
        LocalETLUtil.downloadCustomerProperties(url, space, leConfig, confPath);
        String file = "load." + leConfig.getLocalETLLoadGroup() + ".cmd";
        LocalETLUtil.runCommands(spDirPath, repository, file, iteration, activityLog);
    }

    public boolean isFinished()
    {
        return isFinished;
    }
}
