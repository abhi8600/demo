/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.birst.dataconductor.localetl;

import com.birst.dataconductor.ActivityLog;
import com.birst.dataconductor.DataConductorConfig;
import com.birst.dataconductor.DataConductorUtil;
import com.birst.dataconductor.ViewActivity;
import com.birst.dataconductor.WebDAVError;
import com.birst.dataconductor.WebDavUploader;
import java.io.ByteArrayOutputStream;
import java.io.StringReader;
import java.lang.reflect.Method;
import java.util.Iterator;
import java.util.Map;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamReader;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.impl.builder.StAXOMBuilder;
import org.apache.log4j.Logger;

/**
 *
 * @author mpandit
 */
public class ScriptExecutor extends Thread {
    private String connectionName;
    private String uname;
    private String pword;
    private String space;
    private String url;
    private ActivityLog activityLog;
    private ViewActivity va;
    Map<String, Boolean> statusMap;
    private DataConductorConfig config;
    private DataConductorUtil dcu;
    private int qindex;
    private String command;
    private String argsXML;
    private int loadNumber;
    private  String inputQuery;
    private String output;
    private String script;
    private int maxNumRows;
    private boolean result;
    private long numInputRows;
    private long numOutputRows;

    private final static Logger logger = Logger.getLogger(ScriptExecutor.class);

    public ScriptExecutor(String connectionName, String uname, String pword, String space, String url,
            ActivityLog activityLog, ViewActivity va, Map<String, Boolean> statusMap,
            DataConductorConfig config, DataConductorUtil dcu, int qindex, String command, String argsXML)
    {
        this.connectionName = connectionName;
        this.uname = uname;
        this.pword = pword;
        this.space = space;
        this.url = url;
        this.activityLog = activityLog;
        this.va = va;
        this.statusMap = statusMap;
        this.config = config;
        this.dcu = dcu;
        this.qindex = qindex;
        this.command = command;
        this.argsXML = argsXML;        
    }

    @Override
    public void run()
    {
        ByteArrayOutputStream baos = null;
        try
        {
            if (command.equals("executeliveaccessscript"))
            {
                if (this.argsXML != null)
                {
                    XMLStreamReader parser = XMLInputFactory.newInstance().createXMLStreamReader(new StringReader((argsXML)));
                    StAXOMBuilder builder = new StAXOMBuilder(parser);
                    OMElement doc = null;
                    doc = builder.getDocumentElement();
                    doc.build();
                    doc.detach();
                    for (Iterator<OMElement> iter = doc.getChildElements(); iter.hasNext(); )
                    {
                        OMElement el = iter.next();
                        if (el.getLocalName().equals("LoadNumber"))
                        {
                            loadNumber = Integer.parseInt(el.getText());
                        }
                        else if (el.getLocalName().equals("InputQuery"))
                        {
                            inputQuery = el.getText();
                        }
                        else if (el.getLocalName().equals("Output"))
                        {
                            output = el.getText();
                        }
                        else if (el.getLocalName().equals("Script"))
                        {
                            script = el.getText();
                        }
                        else if (el.getLocalName().equals("MaxNumRows"))
                        {
                            maxNumRows = Integer.parseInt(el.getText());
                        }
                    }
                    baos = new ByteArrayOutputStream();
                    LocalETLUtil.initRepository(url, space, config.getLocalETLConfig(), activityLog);
                    executeScript();
                    if (result)
                    {
                        baos.write(("OK\r\n" + numInputRows + "\r\n" + numOutputRows).getBytes());
                    }
                    else
                    {
                        baos.write(("Error: Failed to perform script execution on live access connection").getBytes());
                    }
                }                
            }
            else if (command.equals("getsourcefile"))
            {
                baos = new ByteArrayOutputStream();
                boolean uploadStatus = uploadSourceFile();
                baos.write(String.valueOf(uploadStatus).getBytes());
            }
            LocalETLUtil.writeData(url, space, connectionName, qindex, baos);
        }
        catch (Exception ex)
        {
        	LocalETLUtil.addToActivityLog(va, activityLog, "Error: " + ex.toString(), ex);
            logger.error(null, ex);
            try
            {
                if (baos == null)
                    baos = new ByteArrayOutputStream();
                baos.write(("Error: " + ex.getMessage()).getBytes());
                LocalETLUtil.writeData(url, space, connectionName, qindex, baos);
            }
            catch (Exception ex1)
            {
            	LocalETLUtil.addToActivityLog(va, activityLog, ex1.toString(), ex1);
                logger.error(null, ex1);
            }
        }
        finally
        {
            if (va != null)
            {
                va.update();
            }
        }        
    }

    private boolean uploadSourceFile() throws Exception
    {
        Object rep = LocalETLUtil.initRepository(url, space, config.getLocalETLConfig(), activityLog);
        Class repClass = Class.forName("com.successmetricsinc.Repository");
        Method getCharacterEncoding = repClass.getMethod("getCharacterEncodingForDefaultConnection");
        String charEncoding = (String) getCharacterEncoding.invoke(rep);
        WebDavUploader wdu = new WebDavUploader(uname, pword, space, url, null, dcu.NUM_DEFAULT_TRIES, dcu, true);
        wdu.setCharacterEncoding(charEncoding);
        WebDAVError werror = wdu.uploadFile(config.getLocalETLConfig().getApplicationDirectory() + "\\" + space + "\\data", argsXML, false, null, true);
        if (werror != null)
        {
            throw new Exception("Error uploading sourcefile: " + argsXML);
        }
        return true;
    }

    private void executeScript() throws Exception
    {
        LocalETLConfig leConfig = config.getLocalETLConfig();
        Object repository = LocalETLUtil.initRepository(url, space, leConfig, activityLog);
        String applicationPath = leConfig.getApplicationDirectory() + "\\" + space;
        Class ScriptExecutor = Class.forName("com.successmetricsinc.localetl.ScriptExecutor");
        Object se = ScriptExecutor.newInstance();
        Method executeScript = ScriptExecutor.getMethod("executeScript", Class.forName("com.successmetricsinc.Repository"), Integer.class, String.class, String.class, String.class, String.class,
			String.class, String.class, Integer.class);
        result = (Boolean) executeScript.invoke(se, repository, loadNumber, applicationPath, applicationPath + "\\data", leConfig.getLocalETLLoadGroup(), inputQuery,
			output, script, maxNumRows);
        if (result)
        {
            Method getNumInputRows = ScriptExecutor.getMethod("getNumInputRows", new Class[] { });
            numInputRows = (Long) getNumInputRows.invoke(se, new Object [] { });
            Method getnumOutputRows = ScriptExecutor.getMethod("getnumOutputRows", new Class[] { });
            numOutputRows = (Long) getnumOutputRows.invoke(se, new Object [] { });
        }
    }
}
