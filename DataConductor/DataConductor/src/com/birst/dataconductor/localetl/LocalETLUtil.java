/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.birst.dataconductor.localetl;

import com.birst.dataconductor.ActivityLog;
import com.birst.dataconductor.AuthenticationException;
import com.birst.dataconductor.DataConductorConfig;
import com.birst.dataconductor.DataConductorUtil;
import com.birst.dataconductor.Database;
import com.birst.dataconductor.DatabaseConnection;
import com.birst.dataconductor.GeneralErrorException;
import com.birst.dataconductor.QuerySource;
import com.birst.dataconductor.RealtimeConnection;
import com.birst.dataconductor.SpaceUnavailableException;
import com.birst.dataconductor.Util;
import com.birst.dataconductor.ViewActivity;
import com.birst.dataconductor.WebDavGetFile;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.sql.Types;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;

/**
 *
 * @author rchandarana
 */
public class LocalETLUtil
{

    private final static Logger logger = Logger.getLogger(LocalETLUtil.class);

    public static RealtimeConnection getLocalETLConnection(DataConductorConfig config, String connectionName)
    {
        RealtimeConnection rc = null;
        Map<String, RealtimeConnection> connections = config.getRealtimeConnections();
        for (String connVisibleName : connections.keySet())
        {
            RealtimeConnection realtimeConn = connections.get(connVisibleName);
            if (realtimeConn.getName().equals(connectionName))
            {
                rc = realtimeConn;
                break;
            }
        }
        return rc;
    }

    public static boolean isConnectionInUse(RealtimeConnection rc, Statement st, ActivityLog activityLog, ViewActivity va) throws Exception
    {
        boolean inUse = false;
        String schema = rc.getSchema();
        if (schema != null && schema.trim().length() > 0)
        {
            // txn command hist table has iterations?
            String tableName = Util.TXN_COMMAND_HISTORY_TABLE_NAME;
            boolean tableExists = LocalETLUtil.tableExists(rc, schema, tableName, st, activityLog, va);
            if (tableExists)
            {
                String query = "SELECT MAX(ITERATION) FROM " + schema + "." + tableName + " HAVING MAX(ITERATION) IS NOT NULL";
                List<String> result = executeQuery(rc, query, st, activityLog, va);
                if (result.size() > 0)
                {
                    inUse = true;
                }
            }
        }
        return inUse;
    }

    public static Map<String, RealtimeConnection> getLocalETLConnectionMap(DataConductorConfig config)
    {
        Map<String, RealtimeConnection> localETLConnectionMap = new HashMap<String, RealtimeConnection>();
        if (config.getRealtimeConnections() != null)
        {
            Map<String, RealtimeConnection> connections = config.getRealtimeConnections();
            for (String connVisibleName : connections.keySet())
            {
                RealtimeConnection rc = connections.get(connVisibleName);
                if ((!rc.isGenericDriver()) && (rc.getDatabaseType().equals(DatabaseConnection.MSSQL) || rc.getDatabaseType().equals(DatabaseConnection.INFOBRIGHT) || rc.getDatabaseType().equals(DatabaseConnection.MEMDB)))
                {
                    localETLConnectionMap.put(connVisibleName, rc);
                }
            }
        }
        return localETLConnectionMap;
    }

    public static int executeUpdate(RealtimeConnection rc, String query, Statement st, ActivityLog activityLog, ViewActivity va) throws Exception
    {
        Statement stmt = st;
        Connection conn = null;
        boolean newStatementCreated = false;
        try
        {
            if (stmt == null) // reuse the statement if passed from caller and do not close statement object in finally
            {
                StringBuilder message = new StringBuilder();
                conn = getDatabaseConnection(rc, message);
                if (conn == null)
                {
                    throw new Exception("Unable to connect to the database: " + rc.getServerName() + ((message.length() > 0) ? " root cause:" + message.toString() : ""));
                }
                stmt = conn.createStatement();
                newStatementCreated = true;
            }
            logger.info(query);
            addToActivityLog(va, activityLog, query, null);
            int count = stmt.executeUpdate(query);
            logger.info("Query results: " + count + " rows");
            addToActivityLog(va, activityLog, "Query results: " + count + " rows", null);
            return count;
        } finally
        {
            try
            {
                if (stmt != null && newStatementCreated)
                    stmt.close();
                if (conn != null)
                    conn.close();
            } catch (Exception ex)
            {
            	if (logger.isTraceEnabled()) {
                    logger.trace("Exception " + ex + " occured while processing method LocalETLUtil.executeUpdate(RealtimeConnection, String, Statement, ActivityLog, ViewActivity) ", ex);
                }
            }
        }
    }

    public static boolean tableExists(RealtimeConnection rc, String schemaName, String tableName, Statement st, ActivityLog activityLog, ViewActivity va) throws Exception
    {
        boolean tableExists = false;
        boolean newStatementCreated = false;
        Statement stmt = st;
        ResultSet rs = null;
        Connection conn = null;
        try
        {
            if (stmt == null) // reuse the statement if passed from caller and do not close statement object in finally
            {
                StringBuilder message = new StringBuilder();
                conn = getDatabaseConnection(rc, message);
                if (conn == null)
                {
                    throw new Exception("Unable to connect to the database: " + rc.getServerName() + ((message.length() > 0) ? " root cause:" + message.toString() : ""));
                }
                stmt = conn.createStatement();
                newStatementCreated = true;
            }
            String tableExistsQuery = "SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA='" + schemaName +
                    "' AND TABLE_NAME = '" + tableName + "'";
            List<String> result = LocalETLUtil.executeQuery(rc, tableExistsQuery, stmt, activityLog, va);
            if (result.size() > 0)
            {
                tableExists = true;
            }
        } finally
        {
            try
            {
                if (stmt != null && newStatementCreated)
                    stmt.close();
                if (rs != null)
                    rs.close();
                if (conn != null)
                    conn.close();
            } catch (Exception ex)
            {
            	if (logger.isTraceEnabled()) {
                    logger.trace("Exception " + ex + " occured while processing method LocalETLUtil.tableExists(RealtimeConnection, String, String, Statement, ActivityLog, ViewActivity) ", ex);
                }
            }
        }
        return tableExists;
    }

    public static List<String> executeQuery(RealtimeConnection rc, String query, Statement st, ActivityLog activityLog, ViewActivity va) throws Exception
    {
        boolean newStatementCreated = false;
        Statement stmt = st;
        Connection conn = null;
        ResultSet rs = null;
        List<String> rowList = new ArrayList<String>();
        try
        {
            if (stmt == null) // reuse the statement if passed from caller and do not close statement object in finally
            {
                StringBuilder message = new StringBuilder();
                conn = getDatabaseConnection(rc, message);
                if (conn == null)
                {
                    throw new Exception("Unable to connect to the database: " + rc.getServerName() + ((message.length() > 0) ? " root cause:" + message.toString() : ""));
                }
                stmt = conn.createStatement();
                newStatementCreated = true;
            }
            logger.info(query);
            addToActivityLog(va, activityLog, query, null);
            rs = stmt.executeQuery(query);
            ResultSetMetaData rsmd = rs.getMetaData();
            int numColumns = rsmd.getColumnCount();
            int columnTypes[] = new int[numColumns + 1];
            for (int i = 1; i < numColumns + 1; i++)
                columnTypes[i] = rsmd.getColumnType(i);
            long rowCount = 0;
            StringBuilder row = new StringBuilder();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
            while (rs.next())
            {
                row.setLength(0);
                for (int i = 1; i <= numColumns; i++)
                {
                    if (i > 1)
                    {
                        row.append('|');
                    }
                    Object o = null;
                    try
                    {
                        switch (columnTypes[i])
                        {
                            case Types.CHAR:
                            case Types.NCHAR:
                            case Types.NVARCHAR:
                            case Types.VARCHAR:
                            case Types.LONGVARCHAR:
                            case Types.LONGNVARCHAR:
                            case Types.ROWID:
                                o = rs.getString(i);
                                break;
                            case Types.TIMESTAMP:
                                o = rs.getTimestamp(i);
                                break;
                            default:
                                o = rs.getObject(i);
                        }

                    } catch (SQLException sex)
                    {
                        // MySQL can cause an exception if a timestamp is not valid; e.g., 0000-00-00 00:00:00
                        //logger.warn(sex.getMessage());
                    	if (logger.isTraceEnabled()) {
                            logger.trace("Exception " + sex + " occured while processing method LocalETLUtil.executeQuery(RealtimeConnection, String, Statement, ActivityLog, ViewActivity) ", sex);
                        }
                    }
                    if (o != null)
                    {
                        if (Timestamp.class.isInstance(o))
                        {
                            row.append(sdf.format((Timestamp) o));
                        } else
                        {
                            String s = o.toString();
                            if (s.length() > 0 && !(s.charAt(0) == '"' && s.charAt(s.length() - 1) == '"') &&
                                    (s.indexOf('\r') >= 0 || s.indexOf('\n') >= 0 || s.indexOf('|') >= 0 || s.indexOf('"') >= 0 || s.length() == 1))
                            {
                                StringBuilder sb = new StringBuilder();
                                for (int j = 0; j < s.length(); j++)
                                {
                                    if (s.charAt(j) == '\\' || s.charAt(j) == '"' || s.charAt(j) == '|')
                                    {
                                        sb.append('\\');
                                    }
                                    sb.append(s.charAt(j));
                                }
                                row.append(sb.toString());
                            } else
                            {
                                row.append(s);
                            }
                        }
                    }
                }
                rowList.add(row.toString());
                rowCount++;
            }
            logger.info("Query results: " + rowCount + " rows, " + numColumns + " columns");
            addToActivityLog(va, activityLog, "Query results: " + rowCount + " rows, " + numColumns + " columns", null);
        } finally
        {
            try
            {
                if (stmt != null && newStatementCreated)
                    stmt.close();
                if (rs != null)
                    rs.close();
                if (conn != null)
                    conn.close();
            } catch (Exception ex)
            {
            	if (logger.isTraceEnabled())
            	{
                    logger.trace("Exception " + ex + " occured while processing method LocalETLUtil.executeQuery(RealtimeConnection, String, Statement, ActivityLog, ViewActivity)", ex);
                }
            }
        }
        return rowList;
    }

    public static void writeData(String url, String space, String connectionName, int qindex, ByteArrayOutputStream baos) throws IOException, MalformedURLException, GeneralErrorException, AuthenticationException, SpaceUnavailableException
    {
        HttpURLConnection hs = null;
        try
        {
            if (baos == null || baos.size() == 0)
            {
                baos = new ByteArrayOutputStream();
                baos.write("EMPTY".getBytes());
            }
            String path = "/" + URLEncoder.encode(space, "UTF-8").replace("+", "%20") + "/dcrealtimedata/" + URLEncoder.encode(connectionName, "UTF-8").replace("+", "%20");
            hs = Util.getHttpURLConnection(url, path, "POST", true);
            hs.setRequestProperty("Content-Length", Integer.toString(baos.size()));
            hs.setRequestProperty("RequestIndex", Integer.toString(qindex));
            hs.connect();
            OutputStream sos = hs.getOutputStream();
            baos.writeTo(sos);
            Util.dumpHttpResponse(hs);
        } finally
        {
            try
            {
                if (hs != null)
                    hs.disconnect();
            } catch (Exception ex)
            {
            	if (logger.isTraceEnabled())
            	{
                    logger.trace("Exception " + ex + " occured while processing method LocalETLUtil.writeData(String, String, String, int, ByteArrayOutputStream)", ex);
                }
            }
        }
    }

    public static void addToActivityLog(ViewActivity va, ActivityLog activityLog, String message, Throwable throwable)
    {
        try
        {
            activityLog.add(message, throwable);
            if (va != null)
            {
                va.update();
            }
        } catch (Exception ex)
        {
            logger.warn("Failed to add message to the activity log - " + message + " - " + ex.getMessage());
        }
    }

    public static void runCommands(String home, String[] smiCommands, ActivityLog activityLog) throws Exception
    {
        activityLog.add(System.getProperty("classpath"), null);
        System.setProperty("smi.home", home);
        InetAddress addr = InetAddress.getLocalHost();
        String host = addr.getHostName();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String today = sdf.format(new Date(Calendar.getInstance().getTimeInMillis()));
        String[] commands = new String[smiCommands.length + 2];
        commands[0] = "-logfile";
        commands[1] = home + "\\" + "logs\\birstwarehouse." + host + "." + today + ".log";
        for (int i = 0; i < smiCommands.length; i++)
        {
            commands[i + 2] = smiCommands[i];
        }
        Class[] argTypes =
        {
            commands.getClass()
        };
        Class clz = Class.forName("com.successmetricsinc.Main");
        Method m = clz.getMethod("main", argTypes);
        Object[] argObjs =
        {
            commands
        };
        m.invoke(null, argObjs);
    }

    public static void runCommands(String home, Object repository, String loadCmdFilename, int loadId, ActivityLog activityLog) throws IOException, GeneralErrorException, MalformedURLException, AuthenticationException, ClassNotFoundException, NoSuchMethodException, IllegalAccessException, IllegalArgumentException, InvocationTargetException
    {
        activityLog.add(System.getProperty("classpath"), null);
        System.setProperty("smi.home", home);
        InetAddress addr = InetAddress.getLocalHost();
        String host = addr.getHostName();
        DecimalFormat df = new DecimalFormat("0000000000");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String today = sdf.format(new Date(Calendar.getInstance().getTimeInMillis()));
        String logfileName = null;
        if (loadId == 0)
        {
            logfileName = home + "\\" + "logs\\birstwarehouse." + host + "." + today + ".log";
        } else
        {
            logfileName = "\"" + home + "\\" + "logs\\smiengine." + host + "." + df.format(loadId) + ".log\"";
        }
        Object[] args = new String[4];
        args[0] = "-c";
        args[1] = home + "\\" + loadCmdFilename;
        args[2] = "-logfile";
        args[3] = logfileName;
        Class[] argTypes =
        {
            args.getClass()
        };
        Class clz = Class.forName("com.successmetricsinc.Main");
        Method setRepMethod = clz.getMethod("setRepository", Object.class);
        setRepMethod.invoke(null, repository);
        Method m = clz.getMethod("main", argTypes);
        Object[] argObjs =
        {
            args
        };
        m.invoke(null, argObjs);
    }

    public static void createDirectories(String spaceDirectoryPath) throws IOException
    {
        String confPath = spaceDirectoryPath + File.separator + "conf";
        String cachePath = spaceDirectoryPath + File.separator + "cache";
        String logPath = spaceDirectoryPath + File.separator + "logs";
        File spDir = new File(spaceDirectoryPath);
        if (!spDir.exists() || !spDir.isDirectory())
        {
            spDir.mkdirs();
        }
        File confDir = new File(confPath);
        if (!confDir.exists() || !confDir.isDirectory())
        {
            confDir.mkdirs();
        }
        File cacheDir = new File(cachePath);
        if (!cacheDir.exists() || !cacheDir.isDirectory())
        {
            cacheDir.mkdirs();
        }
        File logDir = new File(logPath);
        if (!logDir.exists() || !logDir.isDirectory())
        {
            logDir.mkdirs();
        }
        File f = new File(spaceDirectoryPath + File.separator + ".." + File.separator + "publish.lock");
        f.createNewFile();
    }

    public static void swapDirectories(DataConductorUtil dcu, File srcPath, File dstPath) throws IOException
    {
        File temp1 = null;
        File temp2 = null;
        try
        {
            temp1 = dcu.getTempDirectory();
            temp2 = dcu.getTempDirectory();
            copyDirectory(srcPath, temp1);
            copyDirectory(dstPath, temp2);
            dcu.cleanupTempDirectory(srcPath);
            dcu.cleanupTempDirectory(dstPath);
            copyDirectory(temp1, dstPath);
            copyDirectory(temp2, srcPath);
        } finally
        {
            dcu.cleanupTempDirectory(temp1);
            dcu.cleanupTempDirectory(temp2);
        }
    }

    public static void copyDirectory(File srcPath, File dstPath) throws IOException
    {
        if (srcPath.isDirectory())
        {
            if (!dstPath.exists())
            {
                dstPath.mkdir();
            }

            String files[] = srcPath.list();
            for (int i = 0; i < files.length; i++)
            {
                copyDirectory(new File(srcPath, files[i]), new File(dstPath, files[i]));
            }
        } else
        {
            if (!srcPath.exists())
            {
                System.out.println("File or directory does not exist.");
                System.exit(0);
            } else
            {
                InputStream in = new FileInputStream(srcPath);
                OutputStream out = new FileOutputStream(dstPath);

                // Transfer bytes from in to out
                byte[] buf = new byte[1024];
                int len;
                while ((len = in.read(buf)) > 0)
                {
                    out.write(buf, 0, len);
                }
                in.close();
                out.close();
            }
        }
        System.out.println("Directory copied.");
    }

    public static boolean cleanAndRemoveDir(String dirPath)
    {
        try
        {
            File oFile = new File(dirPath);
            if (oFile.isDirectory())
            {
                File[] files = oFile.listFiles();
                if (files.length == 0)
                    oFile.delete();
                else
                {
                    for (File file : files)
                    {
                        cleanAndRemoveDir(file.getAbsolutePath());
                    }
                }
            }
            return oFile.delete();
        } catch (Exception ex)
        {
            logger.error(ex.getMessage(), ex);
            return false;
        }
    }

    public static void deleteFiles(String dirPath)
    {
        File oFile = new File(dirPath);
        if (oFile.isDirectory())
        {
            File[] files = oFile.listFiles();
            for (File file : files)
            {
                try
                {
                    file.delete();
                } catch (Exception ex)
                {
                    logger.error(ex.getMessage(), ex);
                }
            }
        }
    }

    public static Connection getDatabaseConnection(RealtimeConnection rc, StringBuilder message)
    {
        Connection conn = null;
        if (rc.isGenericDriver())
        {
            //create jdbc connection using drivername, connectionstring, username and password
            conn = Database.getConnection(rc.getServerName(), rc.getDatabaseName(), rc.getUsername(), rc.retrievePassword(), message,null);
        } else
        {
            String connectStr = QuerySource.getConnectString(rc.getDriverName(), rc.getServerName(), rc.getDatabaseName(), rc.getPort(), rc.getDatabaseType(), rc.getSchema(), true, null,null);
            conn = Database.getConnection(rc.getDriverName(), connectStr, rc.getUsername(), rc.retrievePassword(), message,null);
        }
        return conn;
    }

    public static void downloadRepository(String url, String spaceID, String dir) throws IOException, GeneralErrorException, MalformedURLException, AuthenticationException, SpaceUnavailableException
    {
        String repositoryFilename = "repository_dev.xml";
        WebDavGetFile wdgf = new WebDavGetFile(url, spaceID);
        wdgf.getFile(dir, repositoryFilename, repositoryFilename, true);
    }

    public static void downloadCustomerProperties(String url, String spaceID, LocalETLConfig leConfig, String dir) throws IOException, GeneralErrorException, MalformedURLException, AuthenticationException, SpaceUnavailableException
    {
        String customerPropertiesFilename = "customer." + leConfig.getLocalETLLoadGroup() + ".properties";
        WebDavGetFile wdgf = new WebDavGetFile(url, spaceID);
        wdgf.getFile(dir, customerPropertiesFilename, "customer.properties", true);
    }

    public static void downloadTimeCustomerProperties(String url, String spaceID, LocalETLConfig leConfig, String dir) throws IOException, GeneralErrorException, MalformedURLException, AuthenticationException, SpaceUnavailableException
    {
        String timeCustomerPropertiesFilename = "customer.time." + leConfig.getLocalETLLoadGroup() + ".properties";
        WebDavGetFile wdgf = new WebDavGetFile(url, spaceID);
        wdgf.getFile(dir, timeCustomerPropertiesFilename, "customer.properties", true);
    }

    public static void downloadCommandFile(String url, String spaceID, LocalETLConfig leConfig, String dir) throws IOException, GeneralErrorException, MalformedURLException, AuthenticationException, SpaceUnavailableException
    {
        String commandFilename = "load." + leConfig.getLocalETLLoadGroup() + ".properties";
        WebDavGetFile wdgf = new WebDavGetFile(url, spaceID);
        wdgf.getFile(dir, commandFilename, "load." + leConfig.getLocalETLLoadGroup() + ".cmd", true);
    }

    public static void downloadTimeCommandFile(String url, String spaceID, LocalETLConfig leConfig, String dir) throws IOException, GeneralErrorException, MalformedURLException, AuthenticationException, SpaceUnavailableException
    {
        String timeCommandFilename = "generatetime." + leConfig.getLocalETLLoadGroup() + ".properties";
        WebDavGetFile wdgf = new WebDavGetFile(url, spaceID);
        wdgf.getFile(dir, timeCommandFilename, "generatetime." + leConfig.getLocalETLLoadGroup() + ".cmd", true);
    }

    public static void downloadDCConfig(String url, String spaceID, String dir, String dcFileName) throws IOException, GeneralErrorException, MalformedURLException, AuthenticationException, SpaceUnavailableException
    {
        WebDavGetFile wdgf = new WebDavGetFile(url, spaceID);
        wdgf.getFile(dir, dcFileName, dcFileName, true);
    }

    public static Object initRepository(String url, String spaceID, LocalETLConfig leConfig, ActivityLog activityLog) throws IOException, GeneralErrorException, MalformedURLException, AuthenticationException, ClassNotFoundException, NoSuchMethodException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, SpaceUnavailableException
    {
        return initRepository(url, spaceID, leConfig, activityLog, true);
    }

    public static Object initRepository(String url, String spaceID, LocalETLConfig leConfig, ActivityLog activityLog, boolean timeExists) throws IOException, GeneralErrorException, MalformedURLException, AuthenticationException, ClassNotFoundException, NoSuchMethodException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, SpaceUnavailableException
    {
        String applicationPath = leConfig.getApplicationDirectory() + File.separator + spaceID;
        logger.debug("Application path: " + applicationPath);
        System.setProperty("smi.home", applicationPath);
        LocalETLUtil.createDirectories(applicationPath);
        String confPath = applicationPath + File.separator + "conf";
        downloadRepository(url, spaceID, applicationPath);
        if (timeExists)
            downloadCustomerProperties(url, spaceID, leConfig, confPath);
        else
            downloadTimeCustomerProperties(url, spaceID, leConfig, confPath);
        Class repInitializer = Class.forName("com.successmetricsinc.localetl.RepositoryInitializer");
        Method m = repInitializer.getMethod("initRepository", String.class);
        return m.invoke(null, applicationPath + File.separator + "repository_dev.xml");
    }

    public static void downloadDeleteDataCommandFile(String url, String spaceID, LocalETLConfig leConfig, String dir) throws Exception
    {
        String commandFilename = "delete." + leConfig.getLocalETLLoadGroup() + ".properties";
        WebDavGetFile wdgf = new WebDavGetFile(url, spaceID);
        wdgf.getFile(dir, commandFilename, "delete." + leConfig.getLocalETLLoadGroup() + ".cmd", true);
    }

    public static boolean isSchemaExists(Statement st, RealtimeConnection rc, ActivityLog activityLog, ViewActivity va) throws Exception
    {
        String databaseType = rc.getDatabaseType();
        boolean isIB = databaseType != null && databaseType.equals(DatabaseConnection.INFOBRIGHT);
        boolean isMemDB = databaseType != null && databaseType.equals(DatabaseConnection.MEMDB);
        if (isMemDB)
            return true;
        String schemaExistsSQL = null;
        if (isIB)
        {
            schemaExistsSQL = "SELECT * FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME='" + rc.getSchema() + "'";
        } else
        {
            schemaExistsSQL = "SELECT * FROM SYS.SCHEMAS WHERE NAME='" + rc.getSchema() + "'";
        }
        List<String> result = LocalETLUtil.executeQuery(rc, schemaExistsSQL, st, activityLog, va);
        return result.size() > 0;
    }

    public static boolean createSchemaAndUpdateRealtimeConnection(String mainSchema, DataConductorConfig config, DataConductorUtil dcu,
            String leConnectionName, ActivityLog activityLog, ViewActivity va) throws Exception
    {
        Connection conn = null;
        Statement stmt = null;
        RealtimeConnection rc = LocalETLUtil.getLocalETLConnection(config, leConnectionName);
        try
        {
            StringBuilder message = new StringBuilder();
            conn = LocalETLUtil.getDatabaseConnection(rc, message);
            //Now check to see if the schema required for loading exists
            if (rc.getSchema() == null) //set schema to Realtime Connection after getting connection object.
            {
                //This is first time around - no schema name is set on the realtime connection
                String schema = "S_" + Util.generatePhysicalName(dcu.getSpace());
                rc.setSchema(schema);
                dcu.saveSettings(config, dcu.getTempDirectory());
            }
            if (conn == null)
            {
                throw new Exception("Unable to connect to the database: " + rc.getServerName() + ((message.length() > 0) ? " root cause:" + message.toString() : ""));
            }
            stmt = conn.createStatement();
            String databaseType = rc.getDatabaseType();
            boolean isIB = databaseType != null && databaseType.equals(DatabaseConnection.INFOBRIGHT);
            String createSchemaSQL = null;
            boolean schemaExists = LocalETLUtil.isSchemaExists(stmt, rc, activityLog, va);
            String log = null;
            if (schemaExists == false) // Schema not exists and creating new
            {
                log = "Creating new schema " + rc.getSchema();
                LocalETLUtil.addToActivityLog(va, activityLog, log, null);
                logger.debug(log);
                if (isIB)
                    createSchemaSQL = "CREATE DATABASE IF NOT EXISTS " + rc.getSchema();
                else
                    createSchemaSQL = "CREATE SCHEMA [" + rc.getSchema() + "] AUTHORIZATION [" + mainSchema + "]";
                LocalETLUtil.executeUpdate(rc, createSchemaSQL, stmt, activityLog, va);
            }
            boolean timeExists = LocalETLUtil.tableExists(rc, Util.MAIN_SCHEMA, Util.TIME_TABLE_NAME, stmt, activityLog, va);
            log = "Time table" + (timeExists ? " exists " : " not exists");
            LocalETLUtil.addToActivityLog(va, activityLog, log, null);
            logger.debug(log);
            return timeExists;
        } catch (Exception e)
        {
            throw (e);
        } finally
        {
            try
            {
                if (conn != null)
                    conn.close();
                if (stmt != null)
                    stmt.close();
            } catch (Exception ex)
            {
            	if (logger.isTraceEnabled())
            	{
                    logger.trace("Exception " + ex + " occured while processing method LocalETLUtil.createSchemaAndUpdateRealtimeConnection(String, DataConductorConfig, DataConductorUtil, String, ActivityLog, ViewActivity)", ex);
                }
            }
        }
    }

    public static void generateTimeTables(DataConductorUtil dcu, LocalETLConfig leConfig, String spDirPath,
            String confPath, ActivityLog activityLog, ViewActivity va, int iteration) throws IOException, MalformedURLException,
            GeneralErrorException, AuthenticationException, ClassNotFoundException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, SpaceUnavailableException
    {
        Object repository = LocalETLUtil.initRepository(dcu.getUrl(), dcu.getSpace(), leConfig, activityLog, false);
        LocalETLUtil.downloadTimeCommandFile(dcu.getUrl(), dcu.getSpace(), leConfig, spDirPath);
        String file = "generatetime." + leConfig.getLocalETLLoadGroup() + ".cmd";
        LocalETLUtil.runCommands(spDirPath, repository, file, iteration, activityLog);
        String message = "Generating Time Tables";
        LocalETLUtil.addToActivityLog(va, activityLog, message, null);
        logger.debug(message);
        File timeCustPropsFile = new File(confPath, "customer.properties");
        if (timeCustPropsFile.exists())
        {
            timeCustPropsFile.delete();
        }
    }

    private static String getQueryToCheckLoadFailed(String schema, String tablename, String loadgroup, int loadNumber)
    {
        return ("SELECT * FROM " + schema + "." + tablename + " WHERE COMMAND_TYPE = 'ETL' AND STEP='LoadWarehouse' AND SUBSTEP='" + loadgroup + "'" +
                " AND ITERATION=" + loadNumber + " AND STATUS=3");
    }

    private static String getQueryToMarkLoadFailed(String schema, String tablename, String loadgroup, int loadNumber, boolean isIB)
    {
        return ("INSERT INTO " + schema + "." + tablename + " (TM,COMMAND_TYPE,STEP,SUBSTEP,ITERATION,STATUS) VALUES (" + (isIB ? "current_timestamp()" : "getdate()") + ",'ETL','LoadWarehouse'," + "'" + loadgroup + "'" + "," + loadNumber + ", 3)");
    }

    public static String markLoadAsFailed(DataConductorConfig config, RealtimeConnection rc, String space, String loadNumber, ActivityLog activityLog, ViewActivity va)
    {
        String status = "OK";
        LocalETLConfig leconfig = null;
        try
        {
            String tablename = Util.TXN_COMMAND_HISTORY_TABLE_NAME;
            String databaseType = rc.getDatabaseType();
            String message = "Starting MarkLoadAsFailed: ";
            LocalETLUtil.addToActivityLog(va, activityLog, message, null);
            logger.debug(message);
            boolean isIB = databaseType != null && databaseType.equals(DatabaseConnection.INFOBRIGHT);
            int iteration = Integer.parseInt(loadNumber);
            if (config != null && config.getLocalETLConfig() != null)
            {
                leconfig = config.getLocalETLConfig();
                String loadgroup = leconfig.getLocalETLLoadGroup();
                String selectQuery = getQueryToCheckLoadFailed(rc.getSchema(), tablename, loadgroup, iteration);
                List<String> result = executeQuery(rc, selectQuery, null, activityLog, va);
                if (result.size() == 0)
                {
                    String insertQuery = getQueryToMarkLoadFailed(rc.getSchema(), tablename, loadgroup, iteration, isIB);
                    executeUpdate(rc, insertQuery, null, activityLog, va);
                }
            }
        } catch (Exception ex)
        {
            LocalETLUtil.addToActivityLog(va, activityLog, ex.toString(), ex);
            logger.error(null, ex);
            status = "Error while trying to stop running data processing";
        } finally
        {
            if (leconfig != null)
            {
                String spaceDirectoryPath = leconfig.getApplicationDirectory() + File.separator + space;
                File f = new File(spaceDirectoryPath + File.separator + "publish.lock");
                String log = null;
                if (f.exists())
                {
                    log = "deleting publish.lock file at " + f.getPath();
                    LocalETLUtil.addToActivityLog(va, activityLog, log, null);
                    logger.debug(log);
                    f.delete();
                } else
                {
                    log = "publish.lock file does not exist at " + spaceDirectoryPath;
                    LocalETLUtil.addToActivityLog(va, activityLog, log, null);
                    logger.debug(log);
                }
            }
        }
        String message = "Exiting MarkLoadAsFailed: ";
        LocalETLUtil.addToActivityLog(va, activityLog, message, null);
        logger.debug(message);
        return status;
    }

    public static String getLocalProcessingEngineVersion()
    {
        try
        {
            Class utilClass = Class.forName("com.successmetricsinc.util.Util");
            Method getBuildString = utilClass.getMethod("getBuildString", new Class[]
                    {
                    });
            String buildString = (String) getBuildString.invoke(null, new Object[]
                    {
                    });
            if (buildString != null && !buildString.isEmpty() && buildString.contains("Release: ") && buildString.contains(", Build: "))
            {
                String releaseNumber = buildString.substring("Release: ".length(), buildString.indexOf(", Build: ")).trim();
                if (releaseNumber.contains(" "))
                {
                    releaseNumber = releaseNumber.substring(0, releaseNumber.indexOf(" "));
                    return releaseNumber;
                }
            }
            return "Undefined";
        } catch (Exception e)
        {
            logger.error(e.getMessage(), e);
            return "Undefined";
        }
    }

    public static boolean deletePublishLockFile(DataConductorConfig config, String space)
    {
        if (config != null && config.getLocalETLConfig() != null)
        {
            File file = new File(config.getLocalETLConfig().getApplicationDirectory() + File.separator + space + File.separator + "publish.lock");
            if (file.exists())
            {
                try
                {
                    file.delete();
                    return true;
                } catch (Exception ex)
                {
                    logger.error("Error while deleting publish.lock file ", ex);
                    return false;
                }
            }
        }
        return false;
    }
}
