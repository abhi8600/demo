/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.birst.dataconductor.localetl;

import com.birst.dataconductor.ActivityLog;
import com.birst.dataconductor.CloudDataDownloader;
import com.birst.dataconductor.DataConductorConfig;
import com.birst.dataconductor.DataConductorUtil;
import com.birst.dataconductor.DatabaseConnection;
import com.birst.dataconductor.RealtimeConnection;
import com.birst.dataconductor.Util;
import com.birst.dataconductor.ViewActivity;
import com.birst.dataconductor.WebDAVError;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStreamReader;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;

/**
 *
 * @author mpandit
 */
public class SpaceAdmin extends Thread
{
    private String connectionName;
    private String uname;
    private String pword;
    private String space;
    private String url;
    private ActivityLog activityLog;
    private ViewActivity va;
    Map<String, Boolean> statusMap;
    private DataConductorConfig config;
    private DataConductorUtil dcu;
    private int qindex;
    private String command;
    private String args;
    public String copySpaceSchemaName;
    public String copySpaceSpaceID;
    private boolean isRunning = false;
    private boolean isSuccessfullyCompleted = false;

    private final static Logger logger = Logger.getLogger(SpaceAdmin.class);

    public SpaceAdmin(String connectionName, String uname, String pword, String space, String url,
            ActivityLog activityLog, ViewActivity va, Map<String, Boolean> statusMap,
            DataConductorConfig config, DataConductorUtil dcu, int qindex, String command, String args)
    {
        this.connectionName = connectionName;
        this.uname = uname;
        this.pword = pword;
        this.space = space;
        this.url = url;
        this.activityLog = activityLog;
        this.va = va;
        this.statusMap = statusMap;
        this.config = config;
        this.dcu = dcu;
        this.qindex = qindex;
        this.command = command;
        this.args = args;
        logger.info("SpaceAdmin: " + connectionName + ", command: " + command + ", commandargs: " + args);
    }

    @Override
    public void run()
    {
        ByteArrayOutputStream baos = null;
        try
        {
            if (command.equals("initiatecopyspacedatatonewspace"))
            {
                baos = new ByteArrayOutputStream();
                String[] arguments = args.split("\t");
                if (arguments.length != 2)
                    throw new Exception("Invalid arguments supplied for initiatecopyspacedatatonewspace operation:" + args);
                String targetSchemaName = arguments[0];
                if (targetSchemaName == null || targetSchemaName.trim().isEmpty())
                    throw new Exception("Target Schema Name not supplied for initiatecopyspacedatatonewspace operation");
                String targetSpaceID = arguments[1];
                if (targetSpaceID == null || targetSpaceID.trim().isEmpty())
                    throw new Exception("Target SpaceID not supplied for initiatecopyspacedatatonewspace operation");
                this.copySpaceSchemaName = targetSchemaName;
                this.copySpaceSpaceID = targetSpaceID;
                isRunning = true;
                baos.write("True".getBytes());
                LocalETLUtil.writeData(url, space, connectionName, qindex, baos);
                
                Object repository = LocalETLUtil.initRepository(url, space, config.getLocalETLConfig(), activityLog);
                Class CopySpaceData = Class.forName("com.successmetricsinc.localetl.CopySpaceData");
                Constructor cons = CopySpaceData.getConstructor(Class.forName("com.successmetricsinc.Repository"), String.class);
                Object csd = cons.newInstance(repository, targetSchemaName);
                Method m = CopySpaceData.getMethod("copyData", new Class[] {});
                boolean result = (Boolean) m.invoke(csd, new Object[] {});
                if (result)
                {
                    //now copy over local application directory contents
                    String localAppDir = config.getLocalETLConfig().getApplicationDirectory();
                    File sourceDir = new File(localAppDir + File.separator + space);
                    File targetDir = new File(localAppDir + File.separator + targetSpaceID);
                    if (targetDir.exists() && targetDir.isDirectory())
                        dcu.cleanupTempDirectory(targetDir);
                    LocalETLUtil.copyDirectory(sourceDir, targetDir);
                }
                isSuccessfullyCompleted = true;
            }
            else if (command.equals("swapspacedirectories"))
            {
                baos = new ByteArrayOutputStream();
                String targetSpaceID = args;
                if (targetSpaceID == null || targetSpaceID.trim().isEmpty())
                    throw new Exception("Target Space ID not supplied for swapspacedirectories operation");
                String localAppDir = config.getLocalETLConfig().getApplicationDirectory();
                File sourceDir = new File(localAppDir + File.separator + space);
                File targetDir = new File(localAppDir + File.separator + targetSpaceID);
                if (!sourceDir.exists() ||  !sourceDir.isDirectory())
                    throw new Exception("Directory " + sourceDir + " does not exist");
                if (!targetDir.exists() ||  !targetDir.isDirectory())
                    throw new Exception("Directory " + targetDir + " does not exist");
                LocalETLUtil.swapDirectories(dcu, sourceDir, targetDir);
                baos.write("true".getBytes());
            }
            else if (command.equals("migratespacetolocal"))
            {
                baos = new ByteArrayOutputStream();
                List<String> tablesToPopulate = Arrays.asList(args.split("\t"));
                boolean timeExists = LocalETLUtil.createSchemaAndUpdateRealtimeConnection(Util.MAIN_SCHEMA, config, dcu, connectionName, activityLog, va);
                Object repository = LocalETLUtil.initRepository(url, space, config.getLocalETLConfig(), activityLog);
                LocalETLConfig leConfig = config.getLocalETLConfig();
                String spDirPath = leConfig.getApplicationDirectory() + "\\" + space;
                String confPath = spDirPath + "\\conf";
                LocalETLUtil.createDirectories(spDirPath);
                if (!timeExists)
                {                    
                    LocalETLUtil.generateTimeTables(dcu, leConfig, spDirPath, confPath, activityLog, va, 0);
                }
                LocalETLUtil.downloadCustomerProperties(url, space, leConfig, confPath);
                Class GenerateLocalSchema = Class.forName("com.successmetricsinc.localetl.GenerateLocalSchema");
                Object gls = GenerateLocalSchema.newInstance();
                Method generate = GenerateLocalSchema.getMethod("generate", Class.forName("com.successmetricsinc.Repository"));
                boolean result = (Boolean)generate.invoke(gls, repository);
                if(!result)
                {
                    throw new Exception("Failed to perform generateschema for migrating space to local connection:" + connectionName);
                }
                String migrateDir = spDirPath + File.separator + "data" + File.separator + "migrate";
                File f = new File(migrateDir);
                f.mkdirs();
                RealtimeConnection rc = LocalETLUtil.getLocalETLConnection(config, connectionName);
                if (rc.getDatabaseType().equals(DatabaseConnection.MSSQL))
                {
                    List<String> srcList = new ArrayList<String>();
                    srcList.add("DatabaseIndices.xml");
                    CloudDataDownloader indicesDownloader = new CloudDataDownloader(uname, pword, space, url, leConfig);
                    indicesDownloader.setSources(srcList);
                    indicesDownloader.pathToSave = migrateDir;
                    indicesDownloader.downloadData(CloudDataDownloader.RequestType.DownloadMigrationData);
                    WebDAVError indicesDownloadError = indicesDownloader.downloadData(CloudDataDownloader.RequestType.DownloadMigrationData);
                    if (indicesDownloadError != null)
                    {
                        throw new Exception("Failed to download table indices data for migrating space to local connection:" + connectionName + ": " + indicesDownloadError.getMessage());
                    }
                    Method createIndices = GenerateLocalSchema.getMethod("createIndices", String.class);
                    result = (Boolean)createIndices.invoke(gls, migrateDir + File.separator + "DatabaseIndices.xml");
                    if(!result)
                    {
                        throw new Exception("Failed to create indices for migrating space to local connection:" + connectionName);
                    }
                }
                CloudDataDownloader cdd = new CloudDataDownloader(uname, pword, space, url, leConfig);
                cdd.setSources(tablesToPopulate);
                cdd.pathToSave = migrateDir;
                WebDAVError error = cdd.downloadData(CloudDataDownloader.RequestType.DownloadMigrationData);
                if (error != null)
                {
                    throw new Exception("Failed to download table data for migrating space to local connection:" + connectionName + ": " + error.getMessage());
                }
                for (String tname : tablesToPopulate)
                {
                    Process pr = Runtime.getRuntime().exec("cmd /Q /C \"bcp " + rc.getDatabaseName() + "." + rc.getSchema() + "." + tname.substring(0, tname.indexOf(".dat")) + " in \"" + migrateDir + "\\" +
                        tname + "\" -E -S" + rc.getServerName() + " -U" + rc.getUsername() + " -P" + rc.retrievePassword() + " -N\"");
                    BufferedReader stdOut = new BufferedReader(new InputStreamReader(pr.getInputStream()));
					String line = null;
					while ((line = stdOut.readLine()) != null)
					{
						logger.debug(line);
					}
					try
					{
						pr.waitFor();
					} catch (InterruptedException e)
					{
						logger.error(e);
                        throw e;
					}                    
                }
                cdd = new CloudDataDownloader(uname, pword, space, url, leConfig);
                cdd.retriveAllLocalSources = true;
                error = cdd.downloadData(CloudDataDownloader.RequestType.DownloadCloudData);
                if (error != null)
                {
                    throw new Exception("Failed to download datafiles for migrating space to local connection:" + connectionName + ": " + error.getMessage());
                }
                baos.write("true".getBytes());
            }
            if (!isRunning)
            {
                LocalETLUtil.writeData(url, space, connectionName, qindex, baos);
            }
        }
        catch (Exception ex)
        {
            LocalETLUtil.addToActivityLog(va, activityLog, "Error: " + ex.toString(), ex);
            logger.error(null, ex);
            try
            {
                if (baos == null)
                    baos = new ByteArrayOutputStream();
                if (!isRunning)
                {
                    baos.write(("Error: " + ex.getMessage()).getBytes());
                    LocalETLUtil.writeData(url, space, connectionName, qindex, baos);
                }
            }
            catch (Exception ex1)
            {
            	LocalETLUtil.addToActivityLog(va, activityLog, ex1.toString(), ex1);
                logger.error(null, ex1);
            }
        }
        finally
        {
            if (va != null)
            {
                va.update();
            }
            isRunning = false;
        }
    }
    
    public boolean getIsRunning()
    {
        return isRunning;
    }
    
    public boolean getIsSuccessfullyCompleted()
    {
        return isSuccessfullyCompleted;
    }
}
