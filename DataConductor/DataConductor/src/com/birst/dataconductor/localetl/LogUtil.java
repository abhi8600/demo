package com.birst.dataconductor.localetl;

import com.birst.dataconductor.ActivityLog;
import com.birst.dataconductor.DataConductorConfig;
import com.birst.dataconductor.DataConductorUtil;
import com.birst.dataconductor.RealtimeConnection;
import com.birst.dataconductor.Util;
import com.birst.dataconductor.ViewActivity;
import com.birst.dataconductor.WebDAVError;
import com.birst.dataconductor.WebDavUploader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.InetAddress;
import java.sql.Connection;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;

/**
 *
 * @author rchandarana
 */
public class LogUtil extends Thread
{
    private String connectionName;
    private String uname;
    private String pword;
    private String space;
    private String url;
    private ActivityLog activityLog;
    private ViewActivity va;
    Map<String, Boolean> statusMap;
    private DataConductorConfig config;
    private DataConductorUtil dcu;
    private int qindex;
    private String command;
    private int iteration;
    private String cmdArgs;

    private final static Logger logger = Logger.getLogger(LoadProcessor.class);

    public LogUtil(String connectionName, String uname, String pword, String space, String url,
            ActivityLog activityLog, ViewActivity va, Map<String, Boolean> statusMap,
            DataConductorConfig config, DataConductorUtil dcu, int qindex, String command, String cmdArgs)
    {
        this.connectionName = connectionName;
        this.uname = uname;
        this.pword = pword;
        this.space = space;
        this.url = url;
        this.activityLog = activityLog;
        this.va = va;
        this.statusMap = statusMap;
        this.config = config;
        this.dcu = dcu;
        this.qindex = qindex;
        this.command = command;
        if (this.command.equals("gettxncommandhistorysnapshot") && cmdArgs != null && !cmdArgs.isEmpty())
        {
            iteration  = Integer.parseInt(cmdArgs);
        }
        else
        {
            this.cmdArgs = cmdArgs;
        }
        logger.info("LogUtil: " + connectionName + ", command: " + command + ", commandargs: " + cmdArgs);
    }

    @Override
    public void run()
    {
        ByteArrayOutputStream baos = null;
        try
        {
            if (command.equals("gettxncommandhistorysnapshot"))
            {
                baos = new ByteArrayOutputStream();
                boolean uploadStatus = uploadTxnCommandHistoryStatus();
                baos.write(String.valueOf(uploadStatus).getBytes());
            }
            else if (command.equals("fileexists"))
            {
                baos = new ByteArrayOutputStream();
                File f = new File(cmdArgs);
                baos.write(String.valueOf(f.exists()).getBytes());
            }
            else if (command.equals("getlastloadlog") || command.equals("getlogforloadid") || command.equals("gettracelog"))
            {
                baos = uploadLogFile();
            }
            else if (command.equals("getactivitylog"))
            {
                baos = new ByteArrayOutputStream();
                int bytesToRead = 0;
                try
                {
                    bytesToRead = Integer.parseInt(cmdArgs);
                } catch (NumberFormatException ex) {
                	if (logger.isTraceEnabled()) {
                        logger.trace("Unable to parse bytesToRead " + cmdArgs + ". Using default timeout 0");            
                    }
                }
                String logs = getActivityLogs(bytesToRead);
                baos.write(logs.getBytes());
            }
            LocalETLUtil.writeData(url, space, connectionName, qindex, baos);
        } catch (Exception ex)
        {
        	LocalETLUtil.addToActivityLog(va, activityLog, "Error: " + ex.toString(), ex);
            logger.error(null, ex);
            try {
                if (baos == null)
                    baos = new ByteArrayOutputStream();
                baos.write(("Error: " + ex.getMessage()).getBytes());
                LocalETLUtil.writeData(url, space, connectionName, qindex, baos);
            } catch (Exception ex1) {
            	LocalETLUtil.addToActivityLog(va, activityLog, ex1.toString(), ex1);
                logger.error(null, ex1);
            }
        } finally
        {
            if (va != null)
            {
                va.update();
            }
        }
    }

    private ByteArrayOutputStream uploadLogFile() throws Exception
    {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        String[] args = cmdArgs.split("\t");
        long bytesToRead = Long.parseLong(args[0]);
        LocalETLConfig leConfig = config.getLocalETLConfig();
        File dir = new File(leConfig.getApplicationDirectory() + File.separator + space +  File.separator + "logs");
            List<File> logFiles = new ArrayList<File>();
            File latestLogFile = null;
            if (dir.exists() && dir.isDirectory())
            {
                if (command.equals("gettracelog"))
                {
                    File traceLogFile = new File(dir + File.separator + "trace.log");
                    if (traceLogFile.exists())
                    {
                        latestLogFile = traceLogFile;
                    }
                }
                else if (command.equals("getlogforloadid") && args.length > 1)
                {
                    iteration = Integer.parseInt(args[1]);
                    InetAddress addr = InetAddress.getLocalHost();
                    String host = addr.getHostName();
                    DecimalFormat df = new DecimalFormat("0000000000");
                    File loadLogFile = new File(leConfig.getApplicationDirectory() + "\\" + space + "\\" + "logs\\smiengine." + host + "." + df.format(iteration) + ".log");
                    if (loadLogFile.exists())
                    {
                        latestLogFile = loadLogFile;
                    }
                }
                else if (command.equals("getlastloadlog"))
                {
                    File[] files = dir.listFiles();
                    if (files != null)
                    {
                        for (File logFile : files)
                        {
                            if (logFile.getName().startsWith("smiengine.") && logFile.getName().endsWith(".log"))
                            {
                                logFiles.add(logFile);
                            }
                        }
                    }
                    boolean isFirst = true;
                    long maxLastModified = Long.MIN_VALUE;
                    for (File f: logFiles)
                    {
                        if (isFirst || f.lastModified() > maxLastModified)
                        {
                            isFirst = false;
                            maxLastModified = f.lastModified();
                            latestLogFile = f;
                        }
                    }
                }
            }

            if (latestLogFile != null)
            {
                if (bytesToRead <= 0 || bytesToRead > latestLogFile.length())
                    bytesToRead = latestLogFile.length();
                FileInputStream fis = null;
                try
                {
                    fis = new FileInputStream(latestLogFile);
                    fis.skip(latestLogFile.length() - bytesToRead);
                    int bufLen = bytesToRead < 10000 ? new Long(bytesToRead).intValue() : 10000;
                    byte[] buffer = new byte[bufLen];
                    int bytesRead = 0;
                    while((bytesRead = fis.read(buffer, 0, bufLen)) != -1)
                    {
                        baos.write(buffer, 0, bytesRead);
                    }
                }
                finally
                {
                    if (fis != null)
                        fis.close();
                }
            }
            else
            {
                baos.write("Error: could not find log file.".getBytes());
            }
            return baos;
    }

    private boolean uploadTxnCommandHistoryStatus() throws Exception
    {
        RealtimeConnection rc = LocalETLUtil.getLocalETLConnection(config, connectionName);
        List<String> result = null;
        Connection conn = null;
        Statement stmt = null;
        File dirFile = null;
        try
        {
            String query = null;
            StringBuilder message = new StringBuilder();
            conn = LocalETLUtil.getDatabaseConnection(rc, message);
            if (conn == null)
            {
                throw new Exception("Unable to connect to the database: " + rc.getServerName() + ((message.length() > 0) ? " root cause:" + message.toString() : ""));
            }
            stmt = conn.createStatement();
            if (iteration == -1)
            {
                query = "SELECT MAX(ITERATION) FROM " + rc.getSchema() + "." + Util.TXN_COMMAND_HISTORY_TABLE_NAME;
                result = LocalETLUtil.executeQuery(rc, query, stmt, activityLog, va);
                if (result.size() > 0)
                    iteration = Integer.parseInt(result.get(0));
            }

            query = "SELECT TM,COMMAND_TYPE,STEP,SUBSTEP,ITERATION,STATUS,NUMROWS,NUMERRORS,NUMWARNINGS,DURATION,MESSAGE,PROCESSINGGROUP " +
                           "FROM " + rc.getSchema() + "." + Util.TXN_COMMAND_HISTORY_TABLE_NAME +
                           " WHERE ITERATION=" + iteration +
                           " ORDER BY TM";

            result = LocalETLUtil.executeQuery(rc, query, stmt, activityLog, va);
            dirFile = dcu.getTempDirectory();
            File f = new File(dirFile + File.separator +
                    Util.TXN_COMMAND_HISTORY_TABLE_NAME + "." + config.getLocalETLConfig().getLocalETLLoadGroup() + "." + iteration + ".upload");
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(f), "UTF-8"));
            // Write data
            for (String row : result) {
                 writer.write(row);
                 writer.write("\r\n");
            }
            writer.close();
            WebDavUploader wdu = new WebDavUploader(uname, pword, space, url, null, dcu.NUM_DEFAULT_TRIES, dcu, true);
            WebDAVError werror = wdu.uploadFile(dirFile.getPath(), f.getName(), false, null, false);
            if (werror != null)
            {
                logger.error("Error uploading TXN_COMMAND_HISTORY snapshot for iteration " + iteration);
                return false;
            }
            return true;
        }
        catch (Exception ex)
        {
            LocalETLUtil.addToActivityLog(va, activityLog,"Error: " + ex.toString(), ex);
            logger.error(null, ex);
        }
        finally {
            // Cleanup
            dcu.cleanupTempDirectory(dirFile);
            try
            {
                if (conn != null)
                    conn.close();
                if (stmt != null)
                    stmt.close();
            } catch (Exception ex) {
            	if (logger.isTraceEnabled()) {
                    logger.trace("Exception " + ex + " occured while processing method LogUtil.run() ", ex);
                }
            }
        }
        return false;
    }

    private String getActivityLogs(long bytesToRead) throws FileNotFoundException, IOException
    {
        BufferedWriter writer = null;
        String activityLogs ="";
        //first try to get logs from logfile. if log file does not exist then from activity logs
        String filename = config.getLogFileName();
        if (filename != null && filename.length() > 0)
        {
            activityLogs = va.activityLog.getLogs(filename, bytesToRead);
        }
        if (activityLogs.length() == 0)
        {
            List<String> logList = va.activityLog.getLogList();
            if (logList.size() > 0)
            {
                File dirFile = dcu.getTempDirectory();
                File f = new File(dirFile + File.separator + "activity_" + System.currentTimeMillis() +".log");
                try
                {
                    writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(f), "UTF-8"));
                    for (String log : logList)
                    {
                        writer.write(log);
                        writer.write("\r\n");
                    }
                    writer.flush();
                    writer.close();
                    activityLogs = va.activityLog.getLogs(f.getAbsolutePath(), bytesToRead);
                }
                finally
                {
                    dcu.cleanupTempDirectory(dirFile);
                }
            }
        }
        return activityLogs;
    }
}
