package com.birst.dataconductor.localetl;

import com.birst.dataconductor.ActivityLog;
import com.birst.dataconductor.DataConductorConfig;
import com.birst.dataconductor.DataConductorUtil;
import com.birst.dataconductor.ViewActivity;
import com.birst.dataconductor.WebDavGetFile;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.Map;
import org.apache.log4j.Logger;

/**
 *
 * @author rchandarana
 */
public class CommandExecutor extends Thread
{
    private String connectionName;
    private String uname;
    private String pword;
    private String space;
    private String url;
    private ActivityLog activityLog;
    private ViewActivity va;
    Map<String, Boolean> statusMap;
    DataConductorConfig config;
    private DataConductorUtil dcu;
    private int qindex;
    private String command;
    private String filename;
    private String commandfile;

    private final static Logger logger = Logger.getLogger(CommandExecutor.class);

    public CommandExecutor(String connectionName, String uname, String pword, String space, String url,
            ActivityLog activityLog, ViewActivity va, Map<String, Boolean> statusMap,
            DataConductorConfig config, DataConductorUtil dcu, int qindex, String command, String cmdArgs)
    {
        this.connectionName = connectionName;
        this.uname = uname;
        this.pword = pword;
        this.space = space;
        this.url = url;
        this.activityLog = activityLog;
        this.va = va;
        this.statusMap = statusMap;
        this.config = config;
        this.dcu = dcu;
        this.qindex = qindex;
        this.command = command;
        this.filename = cmdArgs;
        if (filename.indexOf(".") > 0)
        {
            this.commandfile = filename.substring(0, filename.lastIndexOf('.')) + ".cmd";
        }
        logger.info("CommandExecutor: " + connectionName + ", command: " + command + ", commandargs: " + commandfile);
    }

    @Override
    public void run()
    {
        ByteArrayOutputStream baos = null;
        try
        {
            if (command.equals("runcommandfileonliveaccess") && commandfile != null && commandfile.trim().length() > 0)
            {
                logger.debug("Executing command file " + commandfile);
                LocalETLUtil.addToActivityLog(va, activityLog, "Executing command file " + commandfile, null);
                LocalETLConfig leConfig = config.getLocalETLConfig();
                if (leConfig != null)
                {
                    Object repository = LocalETLUtil.initRepository(url, space, leConfig, activityLog);
                    baos = new ByteArrayOutputStream();
                    String spDirPath = leConfig.getApplicationDirectory() + File.separator + space;
                    downloadFile(url, space, spDirPath, filename, commandfile);
                    LocalETLUtil.runCommands(spDirPath, repository, commandfile, 0, activityLog);
                    baos.write("OK".getBytes());
                    logger.debug("Finished executing command file " + commandfile);
                    LocalETLUtil.addToActivityLog(va, activityLog, "Finished executing command file " + commandfile, null);
                }
            }
            LocalETLUtil.writeData(url, space, connectionName, qindex, baos);
        }
        catch (Exception ex)
        {
        	LocalETLUtil.addToActivityLog(va, activityLog, "Error: " + ex.toString(), ex);
            logger.error(null, ex);
            try {
                if (baos == null)
                    baos = new ByteArrayOutputStream();
                baos.write(("Error: " + ex.getMessage()).getBytes());
                LocalETLUtil.writeData(url, space, connectionName, qindex, baos);
            } catch (Exception ex1) {
            	LocalETLUtil.addToActivityLog(va, activityLog, ex1.toString(), ex1);
                logger.error(null, ex1);
            }
        }
        finally
        {
            if (va != null)
            {
                va.update();
            }
        }
    }

    public void downloadFile(String url, String spaceID, String dir, String filename, String saveAsFileName) throws Exception
    {
        WebDavGetFile wdgf = new WebDavGetFile(url, spaceID);
        wdgf.getFile(dir, filename, saveAsFileName, true);
    }
}
