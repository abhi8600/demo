/**
 * $Id: PutResults.java,v 1.135 2012-11-26 11:27:38 BIRST\mpandit Exp $
 *
 * Copyright (C) 2010 Birst, Inc. All rights reserved.
 * BIRST PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.birst.dataconductor;

import com.birst.dataconductor.localetl.LocalETLUtil;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.sql.Types;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import java.util.regex.Pattern;
import java.util.zip.GZIPOutputStream;
import org.apache.log4j.Logger;
import org.olap4j.CellSet;
import org.olap4j.CellSetAxis;
import org.olap4j.OlapConnection;
import org.olap4j.OlapStatement;
import org.olap4j.OlapWrapper;
import org.olap4j.Position;
import org.olap4j.metadata.Cube;
import org.olap4j.metadata.Dimension;
import org.olap4j.metadata.Hierarchy;
import org.olap4j.metadata.Measure;
import org.olap4j.metadata.Member;
import org.olap4j.metadata.NamedList;
import org.olap4j.metadata.Schema;

/**
 *
 * @author bpeters
 */
public class PutResults extends Thread
{

    private DatabaseConnection dc;
    private String query;
    private String qString;
    private String space;
    private String url;
    private int qindex;
    private String name;
    private ActivityLog activityLog;
    private ViewActivity va;
    private String command;
    private TimeZone processingTZ;
    private boolean isFinished = false;
    private boolean isCancelled = false;
    private Statement statement = null;
    private DataConductorConfig config;
    
    private final static Logger logger = Logger.getLogger(PutResults.class);
    public final static int RELATIONAL_DIMENSIONAL_STRUCTURE = 0;
    public final static int OLAP_DIMENSIONAL_STRUCTURE = 1;
    
    private int maxRecords;

    public PutResults(String name, DatabaseConnection dc, DataConductorConfig config, int qindex, String command, TimeZone processingTZ,
            String query, String space, String url,
            ActivityLog activityLog, ViewActivity va,int maxRecords)
    {
        this.name = name;
        this.command = command;
        this.dc = dc;
        this.config = config;
        this.query = query;
        this.qString = query;
        this.space = space;
        this.url = url;
        this.qindex = qindex;
        this.activityLog = activityLog;
        this.va = va;
        this.processingTZ = processingTZ;
        this.maxRecords = maxRecords;
        
        logger.info("PutResults: " + name + ", command: " + command + ", query: " + query);
    }

    public boolean isFinished()
    {
        return isFinished;
    }
    
    public boolean isCancelled()
    {
    	return isCancelled;
    }

    public void kill()
    {
        logger.info("Attemping to cancel query");
        isCancelled = true;
        try
        {
        	if (statement != null)
        		statement.cancel();
        }
        catch(Exception e)
        {
            addToActivityLog("Cannot process request to cancel query: " + e.toString(), e);
        }
    }

    @Override
    public void run()
    {
        ByteArrayOutputStream baos = null;
        try
        {
            if (command.equals("ping"))
            {
                // just acknowledge response and return
                baos = new ByteArrayOutputStream();
                baos.write("True".getBytes("UTF-8"));
            }
            else if (command.equals("query") && config.getLocalETLConfig() != null &&
                    name.equals(config.getLocalETLConfig().getLocalETLConnection())
                    && new File(config.getLocalETLConfig().getApplicationDirectory() + File.separator + space + File.separator + "publish.lock").exists())
            {
                //local etl load being processed, cannot execute query
                baos = new ByteArrayOutputStream();
                ObjectOutputStream oo = getObjectOutputStream(baos, command);
                logger.error("Birst Local load being processed, cannot execute query");
                addToActivityLog("Error: Birst Local load being processed, cannot execute query", null);
                QueryMetaData qmd = new QueryMetaData("Birst Local load being processed, cannot execute query");
                oo.writeObject(qmd);
                oo.flush();
                oo.close();
            }
            // Execute query
            else if (dc.getDriverName() != null && dc.getDriverName().equals("org.olap4j.driver.xmla.XmlaOlap4jDriver"))
                baos = getXMLAOutputStream(null, null, false);
            else
                baos = getSQLOutputStream(null, null, false);
            if (isCancelled)
            {
                baos = new ByteArrayOutputStream();
                ObjectOutputStream oo = getObjectOutputStream(baos, command);
                QueryMetaData qmd = new QueryMetaData("Query Cancelled");
                oo.writeObject(qmd);
                oo.flush();
                oo.close();                
            }
            writeData(baos);

            if (!command.equals("ping"))
            {
                if (!isCancelled)
                    addToActivityLog("Query results sent", null);
                else
                    addToActivityLog("Query execution cancelled", null);
            }
        } catch (Exception ex)
        {
            addToActivityLog("Error: " + ex.toString(), ex);
            logger.error(null, ex);
            try {
                if (baos == null)
                    baos = new ByteArrayOutputStream();
                baos.write(("Error: " + ex.getMessage()).getBytes("UTF-8"));
                writeData(baos);
            } catch (Exception ex1) {
            	addToActivityLog(ex1.toString(), ex1);
                logger.error(null, ex1);
            }
        } finally
        {
            if (va != null)
            {
                va.update();
            }
            isFinished = true;
        }
    }

    public ByteArrayOutputStream getSQLOutputStream(Connection conn, Statement stmt, boolean usingConnectionFromConnectionPool) throws IOException, SQLException {
        StringBuilder message = new StringBuilder();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try 
        {
            if (!usingConnectionFromConnectionPool)
            {
                if (dc.isGenericDriver())
                {
                    //create jdbc connection using drivername, connectionstring, username and password
                    conn = Database.getConnection(dc.getServerName(), dc.getDatabaseName(), dc.getUsername(), dc.retrievePassword(), message,null);
                }
                else
                {
                    conn = Database.getConnection(dc.getDriverName(), QuerySource.getConnectString(dc.getDriverName(), dc.getServerName(), dc.getDatabaseName(), dc.getPort(), dc.getDatabaseType(), dc.getSchema(), true, (this.processingTZ == null ? null : this.processingTZ.getID()),null), dc.getUsername(), dc.retrievePassword(), message,null);
                }
            }
            if (command.equals("checkconnection"))
            {
                int timeout = 120;
                if (query.startsWith("timeout="))
                {
                    String tOutStr = query.substring("timeout=".length());
                    try
                    {
                        timeout = Integer.parseInt(tOutStr);
                    } catch (NumberFormatException ne) { 
                        logger.debug("Unable to parse timeout " + tOutStr + ". Using default timeout 120");
                    }
                }
                if (isValidConnection(conn, timeout))
                {
                    baos.write("OK".getBytes("UTF-8"));
                }
                return baos;
            }
            if (conn == null) 
            {
                logger.error("Unable to connecto to the database: " + dc.getServerName());
                addToActivityLog("Unable to connect to database: " + dc.getServerName(), null);
                //baos.write(("Error: Unable to connect to database: " + dc.getServerName()).getBytes());
                if (message.length() > 0)
                {
                    addToActivityLog(message.toString(), null);
                }
                if (command.equals("query"))
                {
                    ObjectOutputStream oo = getObjectOutputStream(baos, command);
                    QueryMetaData qmd = new QueryMetaData("Unable to connecto to the database: " + dc.getServerName());
                    oo.writeObject(qmd);
                    oo.flush();
                    oo.close();
                }
            } else 
            {
                ResultSet rs = null;
                boolean isDbHiveImpala = dc.getDriverName().equals("org.apache.hadoop.hive.jdbc.HiveDriver") || dc.getDriverName().equals("org.apache.hive.jdbc.HiveDriver");
                if (command.equals("query") || command.equals("resultsonlyquery") || command.equals("resultsonlyquerywithheaders"))
                {
                    addToActivityLog("Query: " + query, null);
                    ObjectOutput oo = null;
                    QueryMetaData qmd = null;
                    try {
                        if (!usingConnectionFromConnectionPool)
                        {
                            if (isDbHiveImpala)
                                stmt = conn.createStatement();
                            else
                                stmt = conn.createStatement(java.sql.ResultSet.TYPE_FORWARD_ONLY, java.sql.ResultSet.CONCUR_READ_ONLY);
                        }
                        this.statement = stmt;
                        try {
                            if (dc.getFetchSize(true) != 0) {
                                stmt.setFetchSize(dc.getFetchSize(true));
                            }
                            if (dc.getTimeout() > 0) {
                                stmt.setQueryTimeout(dc.getTimeout());
                            }
                            int curMaxRows = stmt.getMaxRows();
                            // use some limiters to make sure we don't trash the SQL Server or the web application server
                            if ((maxRecords > 0) && (maxRecords != Integer.MAX_VALUE) && ((maxRecords +2) != curMaxRows))
                            {
                                stmt.setMaxRows(maxRecords + 2);
                            }
                        } catch (Exception ex) {
                            // operations not supported, just ignore
                        	if (logger.isTraceEnabled()) {
                                    logger.trace("Exception " + ex + " occured while processing method PutResults.getSQLOutputStream()", ex);
                            }
                        }
                        int record = 0;
                        rs = stmt.executeQuery(query);
                        logger.info("executeQuery returned");
                        int numColumns = rs.getMetaData().getColumnCount();
                        ResultSetMetaData rsmd = rs.getMetaData();
                        qmd = new QueryMetaData(rsmd);
                        List<Object[]> data = new ArrayList<Object[]>();
                        while (rs.next() && (++record <= maxRecords)) {
                            Object[] row = new Object[numColumns];
                            for (int i = 0; i < numColumns; i++) {
                                switch (rsmd.getColumnType(i + 1)) {
                                    case Types.CHAR:
                                    case Types.NCHAR:
                                    case Types.NVARCHAR:
                                    case Types.VARCHAR:
                                    case Types.LONGVARCHAR:
                                    case Types.LONGNVARCHAR:
                                    case Types.ROWID:
                                        row[i] = rs.getString(i + 1);
                                        break;
                                    case Types.TIMESTAMP:
                                        if (dc.getDriverName().equals("com.mysql.jdbc.Driver")) //My SQL/Infobright only
                                        {
                                            row[i] = rs.getTimestamp(i + 1);
                                        } else if (isDbHiveImpala)
                                        {
                                            // Impala does not implement getTimestamp
                                            Object obj = rs.getObject(i + 1);
                                            if (obj != null && obj instanceof java.sql.Timestamp)
                                            {
                                                if (processingTZ != null)
                                                {
                                                    long ts = ((java.sql.Timestamp) obj).getTime();
                                                    row[i] = new Timestamp(ts + TimeZone.getDefault().getOffset(ts) - processingTZ.getOffset(ts));
                                                }
                                                else
                                                    row[i] = (java.sql.Timestamp) obj;
                                            }
                                            else
                                                row[i] = null;
                                        } else if (dc.getDriverName().equals("sun.jdbc.odbc.JdbcOdbcDriver") && this.processingTZ != null)
                                        {
                                            java.sql.Timestamp tstmp = rs.getTimestamp(i + 1);
                                            if (tstmp != null)
                                            {
                                                long ts = tstmp.getTime();
                                                row[i] = new Timestamp(ts + TimeZone.getDefault().getOffset(ts) - processingTZ.getOffset(ts));
                                            }
                                            else
                                                row[i] = null;
                                        } else if (this.processingTZ != null) //Other dbs like SQL server, oracle etc.
                                        {
                                            row[i] = rs.getTimestamp(i + 1, Calendar.getInstance(processingTZ));
                                        } else //We should never get here
                                        {
                                            row[i] = rs.getTimestamp(i + 1); //uses system (client's) default timezone
                                        }
                                        break;
                                    case Types.DATE:
                                        if (dc.getDriverName().equals("com.mysql.jdbc.Driver")) //My SQL/Infobright only
                                        {
                                            row[i] = rs.getDate(i + 1);
                                        } else if (dc.getDriverName().equals("sun.jdbc.odbc.JdbcOdbcDriver") && this.processingTZ != null) //JDBC  ODBC
                                        {
                                            long ts = rs.getDate(i + 1).getTime();
                                            row[i] = new Date(ts + TimeZone.getDefault().getOffset(ts) - processingTZ.getOffset(ts));
                                        } else if (this.processingTZ != null) //Other dbs like SQL server, oracle etc.
                                        {
                                            row[i] = rs.getDate(i + 1, Calendar.getInstance(processingTZ));
                                        } else //We should never get here
                                        {
                                            row[i] = rs.getDate(i + 1); //uses system (client's) default timezone
                                        }
                                        break;
                                    case Database.oracle_sql_TimestampTZ://oracle.sql.TimestampTZ
                                    case Database.oracle_sql_TimestampLTZ://oracle.sql.TimestampLTZ
                                    	if (dc.getDriverName().equals("oracle.jdbc.OracleDriver"))//Convert to java.sql.Timestamp only for Oracle
                                    	{
                                            try
                                            {
                                    			if (rsmd.getColumnType(i + 1) == Database.oracle_sql_TimestampTZ)
                                    			{
                                                    Class tzClass = Class.forName("oracle.sql.TIMESTAMPTZ");
                                                    Method tzMethod = tzClass.getMethod("timestampValue", new Class[] {java.sql.Connection.class});
                                                    row[i] = tzMethod.invoke(rs.getObject(i + 1), new Object[] { conn });
                                    			}
                                    			else if (rsmd.getColumnType(i + 1) == Database.oracle_sql_TimestampLTZ)
                                    			{
                                                    Class tzClass = Class.forName("oracle.sql.TIMESTAMPLTZ");
                                                    Method tzMethod = tzClass.getMethod("timestampValue", new Class[] {java.sql.Connection.class, java.util.Calendar.class});
                                                    row[i] = tzMethod.invoke(rs.getObject(i + 1), new Object[] { conn, processingTZ == null ? Calendar.getInstance() : Calendar.getInstance(processingTZ) });
                                    			}
                                            }
                                            catch (Exception ex)
                                            {
                                                logger.error("Could not convert to java.sql.Timestamp - " + ex.getMessage());
                                                row[i] = rs.getObject(i + 1);
                                            }
                                    	}
                                    	else
                                    	{
                                            row[i] = rs.getObject(i + 1);
                                    	}
                                    	break;                                       
                                    default:
                                        row[i] = rs.getObject(i + 1);
                                }
                                if (BigDecimal.class.isInstance(row[i])) {
                                    BigDecimal bd = (BigDecimal) row[i];
                                    row[i] = bd.doubleValue();
                                } else if (Short.class.isInstance(row[i])) {
                                    Short s = (Short) row[i];
                                    row[i] = s.intValue();
                                } else if (Boolean.class.isInstance(row[i])) {
                                    Boolean b = (Boolean) row[i];
                                    row[i] = b ? 1 : 0;
                                }
                            }
                            data.add(row);
                        }
                        logger.info("retrieved all data");
                        if (record > maxRecords)
                        {
                            // clear out the rows
                            data.clear();
                            data = null;
                            logger.info("Too much data returned in the query (max = " + maxRecords + ")");
                            throw new Exception("Too much data returned in the query (max = " + maxRecords + "). Please add filters to reduce the size of the result set.");
        		}
                        if (command.equals("resultsonlyquery") || command.equals("resultsonlyquerywithheaders"))
                        {
                            PrintWriter pw = new PrintWriter(baos);
                            if (command.equals("resultsonlyquerywithheaders"))
                            {
                                for (int i = 0; i < numColumns; i++)
                                {
                                    pw.write((i > 0 ? "|" : "") + rsmd.getColumnName(i + 1));
                                }
                                pw.write("\r\n");
                            }
                            if (!data.isEmpty())
                            {
                                for (Object[] row : data)
                                {
                                    if (row.length > 0)
                                    {
                                        boolean firstT = true;
                                        for (int rowIdx = 0; rowIdx < row.length; rowIdx++)
                                        {
                                            if (firstT)
                                            {
                                                firstT = false;
                                            } else
                                            {
                                                pw.write("|");
                                            }
                                            if (row[rowIdx] == null)
                                            {
                                            } else
                                            {
                                                pw.write(row[rowIdx].toString().replace("\r\n", " "));
                                            }
                                        }
                                        pw.write("\r\n");
                                    }
                                }
                            }
                            pw.close();
                        }
                        else
                        {
                            oo = getObjectOutputStream(baos, command);
                            oo.writeObject(qmd);
                            oo.writeObject(data);
                            oo.flush();
                        }
                        logger.info("Query results: " + data.size() + " rows, " + numColumns + " columns");
                        addToActivityLog("Query results: " + data.size() + " rows, " + numColumns + " columns", null);
                    } catch (Exception ex) {
                        if (oo == null)
                            oo = getObjectOutputStream(baos, command);
                        logger.error(ex, ex);
                        addToActivityLog("Error: " + ex.toString(), ex);
                        qmd = new QueryMetaData(ex.toString());
                        oo.writeObject(qmd);
                    } finally {
                        if (rs != null) {
                            rs.close();
                        }
                        if (stmt != null) {
                            stmt.close();
                        }
                        if (oo != null) {
                            oo.flush();
                            oo.close();
                        }
                    }
                } else if (command.equals("gettables"))
                {
                    String schema = query == null || query.isEmpty() ? null : query;
                    addToActivityLog("Get schema tables" + (schema == null ? "" : " - schema=" + schema), null);
                    String[] types = null;
                    if (isDbHiveImpala) {
                        // should really use this for all DBs and allow the user to specify which types they want to use
                        rs = conn.getMetaData().getTableTypes();
                        List<String> typs = new ArrayList<String>();
                        while (rs.next()) {
                            typs.add(rs.getString(1));
                        }
                        types = typs.toArray(new String[typs.size()]);
                    } else {
                        boolean useViews = dc.getUseViews();
                        if (useViews) {
                            types = new String[]{"TABLE", "VIEW"};
                        } else {
                            types = new String[]{"TABLE"};
                        }
                    }
                    if (logger.isDebugEnabled())
                    {
                        for (String ty : types)
                        {
                            logger.debug("Using: " + ty);
                        }
                    }
                    StringBuilder sb = new StringBuilder();
                    int numTables = 0;
                    try {
                        String dccatalog = dc.getCatalogFilter();
                        String dcschema = schema != null ? schema : dc.getSchemaFilter();
                        if (dc.getDriverName().equals("com.mysql.jdbc.Driver"))
                            rs = conn.getMetaData().getTables(dcschema, null, null, types);
                        else if (isDbHiveImpala)
                            rs = conn.getMetaData().getTables(dccatalog, dcschema, "%", types); // Impala requires a true search parameter for table name
                        else
                            rs = conn.getMetaData().getTables(dccatalog, dcschema, null, types);
                        while (rs.next()) {
                            String temp = rs.getString(1) + "\t" + rs.getString(2) + "\t" + rs.getString(3) + "\t" + rs.getString(4);
                            logger.debug(temp);
                            sb.append(temp);
                            sb.append("\r\n");
                            numTables++;
                        }
                    } catch (Exception ex) {
                        logger.error(ex, ex);
                        addToActivityLog("Error: " + ex.toString(), ex);
                        return null;
                    } finally {
                        if (rs != null) {
                            rs.close();
                        }
                    }
                    logger.info("Retrieved " + numTables + " tables");
                    addToActivityLog("Retrieved " + numTables + " tables", null);
                    baos.write(sb.toString().getBytes("UTF-8"));
                } else if (command.equals("gettableschema"))
                {
                    addToActivityLog("Get table schema", null);
                    StringBuilder sb = new StringBuilder();
                    int numTables = 0;
                    String schema = null;
                    for (String tname : query.split("\t")) {
                        if (numTables == 0 && schema == null && tname.startsWith("schema="))
                        {
                            schema = tname.substring("schema=".length());
                            continue;
                        }
                        try {
                            if (dc.getDriverName().equals("com.mysql.jdbc.Driver"))
                                rs = conn.getMetaData().getColumns(schema, null, tname, null);
                            else if (isDbHiveImpala)
                                rs = conn.getMetaData().getColumns(null, schema, tname, "%"); // Impala requires a true search parameter for column name
                            else
                                rs = conn.getMetaData().getColumns(null, schema, tname, null);

                            while (rs.next())
                            {
                                StringBuilder sb2 = new StringBuilder();
                                sb2.append(rs.getString("TABLE_CAT"));
                                sb2.append('\t');
                                if (dc.getDriverName().equals("org.apache.hive.jdbc.HiveDriver"))
                                    sb2.append(rs.getString("TABLE_MD")); // XXX Impala
                                else
                                    sb2.append(rs.getString("TABLE_SCHEM"));
                                sb2.append('\t');
                                sb2.append(rs.getString("TABLE_NAME"));
                                sb2.append('\t');
                                sb2.append(rs.getString("COLUMN_NAME"));
                                sb2.append('\t');
                                sb2.append(rs.getInt("DATA_TYPE"));
                                sb2.append('\t');
                                String tn = rs.getString("TYPE_NAME");
                                // for Impala ODBC, there is a leading space, so let's trim this to be safe
                                logger.debug("'" + tn + "'");
                                if (tn != null)
                                    tn = tn.trim();
                                sb2.append(tn);
                                sb2.append('\t');
                                sb2.append(rs.getInt("COLUMN_SIZE"));
                                String temp = sb2.toString();
                                logger.debug(temp);
                                sb.append(temp);
                                sb.append("\r\n");
                            }
                            numTables++;
                        } catch (Exception ex) {
                            logger.error(ex, ex);
                            addToActivityLog("Error: " + ex.toString(), ex);
                            return null;
                        } finally {
                            if (rs != null) {
                                rs.close();
                            }
                        }
                    }
                    logger.info("Retrieved schema for " + numTables + " tables");
                    addToActivityLog("Retrieved schema for " + numTables + " tables", null);
                    baos.write(sb.toString().getBytes("UTF-8"));
                }
                else if(command.equals("tableexists"))
                {
                    addToActivityLog("tableexists: " + query, null);
                    boolean exists = false;
                    String params[] = query.split("\t");
                    String schemaName = params[0];
                    String tableName = params[1];
                    try
                    {
                        String tableExistsQuery = "SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA='" + schemaName +
                               "' AND TABLE_NAME = '" + tableName + "'";
                        if (isDbHiveImpala)
                            stmt = conn.createStatement();
                        else
                            stmt = conn.createStatement(java.sql.ResultSet.TYPE_FORWARD_ONLY, java.sql.ResultSet.CONCUR_READ_ONLY);
                        rs = stmt.executeQuery(tableExistsQuery);
                        if (rs.next()) {
                            exists = true;
                        }
                    }
                    catch (Exception ex)
                    {
                        logger.error(ex, ex);
                        addToActivityLog("Error: " + ex.toString(), ex);
                    } 
                    finally
                    {
                        if(exists)
                        {
                            baos.write("true".getBytes("UTF-8"));
                            logger.debug(tableName + " exists");
                        }
                        else
                        {
                            baos.write("false".getBytes("UTF-8"));
                            logger.debug(tableName + " not exists");
                        }
                        writeData(baos);
                        if (rs != null)
                            rs.close();
                    }
                }
                else if(command.equals("executeupdatequery"))
                {
                    addToActivityLog("executeupdatequery: " + query, null);
                    logger.debug("executeupdatequery: " + query);
                    int result = -1;
                    try {
                        result = LocalETLUtil.executeUpdate((RealtimeConnection)dc, query, stmt, activityLog, va);
                    }catch (Exception ex) {
                        logger.error(ex, ex);
                        addToActivityLog("Error: " + ex.toString(), ex);
                    } finally {
                        if (stmt != null) {
                            stmt.close();
                        }
                        baos.write(String.valueOf(result).getBytes("UTF-8"));
                        writeData(baos);
                    }

                }
            }
        } catch (Exception ex)
        {
            logger.error(ex, ex);
            addToActivityLog("Error: " + ex.toString(), ex);
            return null;
        } finally {
            if (conn != null) {
                conn.close();
            }
        }
        return baos;
    }

    static Pattern MEMBERKEY = Pattern.compile(".+[&]\\[.+\\]");

    public ByteArrayOutputStream getXMLAOutputStream(OlapConnection olapConnection, OlapStatement stmt, boolean usingConnectionFromConnectionPool) throws IOException, SQLException
    {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        System.setProperty("http.keepAlive", "true");
        try
        {
            if (!usingConnectionFromConnectionPool)
            {         
            	Connection connection = null;
            	String connectStr = null;
            	if (!dc.isGenericDriver())
            	{
	                Class.forName("org.olap4j.driver.xmla.XmlaOlap4jDriver");
	                connectStr = "jdbc:xmla:Server=" + dc.getServerName();
	                if (dc.getDatabaseName() != null && !dc.getDatabaseName().isEmpty())
	                    connectStr += ";Catalog=" + dc.getDatabaseName();
	                if (dc.getUsername() != null && !dc.getUsername().isEmpty())
	                    connectStr += ";user=" + dc.getUsername();
	                if (dc.getPassword() != null && !dc.retrievePassword().isEmpty())
	                    connectStr += ";password=" + dc.retrievePassword();
                        if (dc.getDatabaseType().equals(DatabaseConnection.HYPERIAN_ESSBASE))
	                    connectStr += ";Provider=Essbase";
	                if (dc.getDatabaseType().equals(DatabaseConnection.SAP_BW))
	                    connectStr += ";Provider=SAP_BW";
	                connection = (OlapConnection) DriverManager.getConnection(connectStr);	                
            	}
            	else
            	{
            		Class.forName(dc.getServerName());
            		connectStr = dc.getDatabaseName();
            		if (connectStr.indexOf("user=") < 0 && connectStr.indexOf("password=") < 0 
                                && dc.getUsername() != null && !dc.getUsername().isEmpty()
                                && dc.getPassword() != null && !dc.retrievePassword().isEmpty())
                        {
                                connectStr = connectStr + ";user=" + dc.getUsername() + ";password=" + dc.retrievePassword();
                        }
                        if (dc.getDatabaseType().equals(DatabaseConnection.HYPERIAN_ESSBASE))
	                    connectStr += ";Provider=Essbase";
	                if (dc.getDatabaseType().equals(DatabaseConnection.SAP_BW))
	                    connectStr += ";Provider=SAP_BW";
            		connection = (OlapConnection) DriverManager.getConnection(connectStr);	                
            	}
            	OlapWrapper wrapper = (OlapWrapper) connection;
                olapConnection = wrapper.unwrap(OlapConnection.class);
                if (connectStr.indexOf("Roles=") >= 0)
                {
                    int startIndex = connectStr.indexOf("Roles=") + "Roles=".length() ;
                    int endIndex = connectStr.indexOf(";", startIndex);
                    String roles = connectStr.substring(startIndex, (endIndex == -1 ? connectStr.length(): endIndex));
                    olapConnection.setRoleName(roles);                    
                }
                if (connectStr.indexOf("ExtendedConnectionProperties=\"") >= 0)
                {
                    int startIndex = connectStr.indexOf("ExtendedConnectionProperties=\"") + "ExtendedConnectionProperties=\"".length();
                    int endIndex = connectStr.indexOf("\"", startIndex);
                    String extendedProperties = connectStr.substring(startIndex, (endIndex == -1 ? connectStr.length(): endIndex));
                    if (extendedProperties != null && extendedProperties.trim().length() > 0)
                    {
                        Map<String, String> extendedPropertiesMap = new HashMap<>();
                        String[] properties = extendedProperties.trim().split(";");
                        if (properties != null)
                        {
                            for (String prop : properties)
                            {
                                String[] property = prop.split("=");
                                if (property != null && property.length == 2)
                                {
                                    extendedPropertiesMap.put(property[0], property[1]);
                                }
                            }
                        }
                        olapConnection.setExtendedPropertiesMap(extendedPropertiesMap);
                    }                                        
                }
            }            
        } catch (ClassNotFoundException e)
        {
            addToActivityLog("Unable to connect to database: " + dc.getServerName(), e);
            baos.write(("Error: Unable to connect to database: " + dc.getServerName()).getBytes("UTF-8"));
        }
        try
        {
            if (command.equals("checkconnection"))
            {
                int timeout = 120;
                if (query.startsWith("timeout="))
                {
                    String tOutStr = query.substring("timeout=".length());
                    try
                    {
                        timeout = Integer.parseInt(tOutStr);
                    } catch (NumberFormatException ne) {
                        logger.debug("Unable to parse timeout " + tOutStr + ". Using default timeout 120");            
                    }
                }
                if (isValidConnection(olapConnection, timeout))
                {
                    baos.write("OK".getBytes("UTF-8"));
                }
                return baos;
            }
            if (olapConnection == null)
            {
                addToActivityLog("Unable to connect to database: " + dc.getServerName(), null);
                baos.write(("Error: Unable to connect to database: " + dc.getServerName()).getBytes("UTF-8"));
            } else
            {
                if (command.equals("query") || command.equals("resultsonlyquery") || command.equals("resultsonlyquerywithheaders"))
                {
                    ObjectOutput oo = getObjectOutputStream(baos, command);
                    QueryMetaData qmd = null;
                    List<Object[]> data = new ArrayList<Object[]>();
                    try
                    {
                    	if (!usingConnectionFromConnectionPool)
                    	{
                    		stmt = olapConnection.createStatement();
                    	}
                        
                        this.statement = stmt;
                        int index = query.indexOf("WITH");
                        if (index < 0)
                            index = query.indexOf("SELECT");
                        if (index < 0)
                            throw new Exception("Malformed XMLA query");
                        int topindex = query.indexOf("TOPN");
                        int qstart = index;
                        int topn = -1;
                        if (topindex >= 0 && topindex < index - 4)
                        {
                            String topnstr = query.substring(topindex + 4, index);
                            index = topindex;
                            try
                            {
                                topn = Integer.valueOf(topnstr.trim());
                            } catch (Exception e)
                            {
                                topn = -1;
                            }
                        }
                        String[] positions = query.substring(0, index - 1).split("~");
                        Map<String, List<Integer>> posarr = new HashMap<String, List<Integer>>();
                        Map<String, String> dTypes = new HashMap<String, String>();
                        Map<String, String> cFormats = new HashMap<String, String>();
                        int rowlength = 0;
                        for (String s : positions[0].split(","))
                        {
                            if (s.length() == 0)
                                continue;
                            s = s.substring(1, s.length() - 1);
                            String[] posstrlist = s.split("\\|");
                            List<Integer> plist = new ArrayList<Integer>();
                            posarr.put(posstrlist[0], plist);
                            dTypes.put(posstrlist[0], posstrlist[1]);
                            for (int pi = 2; pi < posstrlist.length; pi++)
                            {
                                String s2 = posstrlist[pi];
                                int i = Integer.valueOf(s2);
                                if (i + 1 > rowlength)
                                    rowlength = i + 1;
                                plist.add(i);
                            }
                        }
                        if (positions.length > 1)
                        {
                            for (String s : positions[1].split(","))
                            {
                                if (s.length() == 0)
                                    continue;
                                s = s.substring(1, s.length() - 1);
                                String[] posstrlist = s.split("\\|");
                                List<Integer> plist = new ArrayList<Integer>();
                                posarr.put(posstrlist[0], plist);
                                dTypes.put(posstrlist[0], posstrlist[1]);
                                for (int pi = 2; pi < posstrlist.length; pi++)
                                {
                                    String s2 = posstrlist[pi];
                                    int i = Integer.valueOf(s2);
                                    if (i + 1 > rowlength)
                                        rowlength = i + 1;
                                    plist.add(i);
                                }
                            }
                        }
                        if (positions.length > 2)
                        {
                            //check if column format is supplied
                            for (String s : positions[2].split("\\^"))
                            {
                                if (s.length() == 0)
                                    continue;
                                String[] formatstrlist = s.split("\\|");
                                cFormats.put(formatstrlist[0], formatstrlist[1]);
                            }
                        }
                        if(topn == 0)
                        {
                            query = query.substring(qstart);
                            qmd = new QueryMetaData();
                            addToActivityLog("Top 0 query will not be executed on cube. Sending metadata only for query " + query, null);
                        }
                        else
                        {
                            query = query.substring(qstart);
                            addToActivityLog("Query: " + query, null);
                            CellSet result = stmt.executeOlapQuery(query);
                            qmd = new QueryMetaData();
                            List<Integer> coords = new ArrayList<Integer>();
                            List<CellSetAxis> alist = result.getAxes();
                            CellSetAxis columns = alist.get(0);
                            List<Position> cplist = columns.getPositions();
                            if (alist.size() == 1)
                            {
                                coords.add(0);
                                // Just measures
                                int colnum = 0;
                                int[] colTypes = new int[rowlength];
                                qmd.setColumnTypes(colTypes);
                                qmd.setColumnCount(colTypes.length);
                                int rownum = 0;
                                Object[] row = new Object[rowlength];
                                colnum = 0;
                                if (!cplist.isEmpty())
                                {
                                    for (Position cp : cplist)
                                    {
                                        List<Member> cmlist = cp.getMembers();
                                        for (Member mem : cmlist)
                                        {
                                            List<Integer> plist = posarr.get(mem.getName());
                                            if (plist == null)
                                                continue;
                                            String dType = dTypes.get(mem.getName());
                                            coords.set(0, colnum);
                                            for (int colindex : plist)
                                            {
                                                if (colTypes[colindex] == 0)
                                                {
                                                    colTypes[colindex] = DataConductorUtil.getDataType(dType);
                                                }
                                                row[colindex] = getRowObject(String.valueOf(result.getCell(coords).getValue()), colTypes[colindex], cFormats.get(mem.getName()));
                                            }
                                            colnum++;
                                            if (isCancelled)
                                                return null;
                                        }
                                    }
                                }
                                if (topn < 0 || rownum < topn)
                                    data.add(row);
                                rownum++;
                            } else
                            {
                                coords.add(0);
                                coords.add(0);
                                CellSetAxis rows = alist.get(1);
                                List<Position> rplist = rows.getPositions();
                                if (!rplist.isEmpty())
                                {
                                    int[] colTypes = new int[rowlength];
                                    int rownum = 0;
                                    int record = 0;
                                    for (Position p : rplist)
                                    {
                                        if (++record<=maxRecords)
                                        {
                                            List<Member> mlist = p.getMembers();
                                            Object[] row = new Object[rowlength];
                                            for (Member mem : mlist)
                                            {
                                                String cname  = null;
                                                String dnname = null;
                                                String hnname = null;
                                                if (mem.getLevel() != null)
                                                {
                                                    dnname = mem.getLevel().getHierarchy().getDimension().getName();
                                                    hnname = mem.getLevel().getHierarchy().getName();
                                                }
                                                if (dc.getDatabaseType().equals(DatabaseConnection.HYPERIAN_ESSBASE))
                                                {
                                                    //String lname = mem.getHierarchy().getLevels().get(mem.getDepth()).getName();
                                                    //int li = lname.lastIndexOf('.');
                                                    //lname = lname.substring(li + 1);
                                                    //cname = "[" + mem.getLevel().getHierarchy().getDimension().getName() + "].[" + mem.getLevel().getHierarchy().getName() + "]." + lname;
                                                    cname = mem.getCaption(null);
                                                } else if (dc.getDatabaseType().equals(DatabaseConnection.SAP_BW))
                                                {
                                                    cname = mem.getLevel().getUniqueName();
                                                } else
                                                {
                                                    String lname = mem.getLevel().getName();
                                                    cname = "[" + dnname + "]." + (hnname == null ? "" : "[" + hnname + "].") + "[" + lname + "]";
                                                }
                                                List<Integer> plist = posarr.get(cname);
                                                if (plist == null)
                                                {
                                                    if (dc.getDatabaseType().equals(DatabaseConnection.HYPERIAN_ESSBASE))
                                                        continue;
                                                    // If this is a member name, look for it
                                                    String lname = mem.getUniqueName();
                                                    List<Integer> mplist = new ArrayList<Integer>();
                                                    if (MEMBERKEY.matcher(lname).matches())
                                                    {
                                                        // If this is a unique member, add value to all levels in the report
                                                        for(String key: posarr.keySet())
                                                        {
                                                            if (key.startsWith("[" + dnname + "]" + (hnname == null ? "" : ".[" + hnname + "]")))
                                                            {
                                                                mplist.addAll(posarr.get(key));
                                                            }
                                                        }
                                                        plist = mplist;
                                                    } else
                                                    {
                                                        cname = "[" + dnname + "]." + lname;
                                                        plist = posarr.get(cname);
                                                    }
                                                    if (plist == null)
                                                        continue;
                                                }
                                                String dType = dTypes.get(cname);
                                                String objectStr = null;
                                                if (dc.getDatabaseType().equals(DatabaseConnection.SAP_BW))
                                                {
                                                    String uName = mem.getUniqueName() == null ? mem.getName() : mem.getUniqueName();
                                                    if (uName.indexOf("].[") > 0)
                                                    {
                                                        uName = uName.substring(uName.indexOf("].[")+2);
                                                        objectStr = mem.getCaption(null) + " " + uName;
                                                    }
                                                    else
                                                    {
                                                        objectStr = mem.getCaption(null) + " [" + uName + "]";
                                                    }
                                                }
                                                else
                                                {
                                                    objectStr = mem.getName();
                                                }
                                                for (int colindex : plist)
                                                {
                                                    if (colTypes[colindex] == 0)
                                                    {
                                                        colTypes[colindex] = DataConductorUtil.getDataType(dType);
                                                    }
                                                    row[colindex] = getRowObject(String.valueOf(objectStr), colTypes[colindex], cFormats.get(cname));
                                                }
                                                if (isCancelled)
                                                    return null;
                                            }
                                            int colnum = 0;
                                            if (!cplist.isEmpty())
                                            {
                                                for (Position cp : cplist)
                                                {
                                                    List<Member> cmlist = cp.getMembers();
                                                    for (Member mem : cmlist)
                                                    {
                                                        List<Integer> plist = null;
                                                        String dType = null;
                                                        String cFormat = null;
                                                        if (dc.getDatabaseType().equals(DatabaseConnection.HYPERIAN_ESSBASE))
                                                        {
                                                            plist = posarr.get(mem.getCaption(null));
                                                            dType = dTypes.get(mem.getCaption(null));
                                                            cFormat = cFormats.get(mem.getCaption(null));
                                                        } else if (dc.getDatabaseType().equals(DatabaseConnection.SAP_BW))
                                                        {
                                                            String uName = mem.getUniqueName() == null ? mem.getName() : mem.getUniqueName();
                                                            if (uName.startsWith("[Measures]."))
                                                            {
                                                                uName = uName.substring("[Measures].".length());
                                                                if (uName.startsWith("[") && uName.endsWith("]"))
                                                                {
                                                                    uName = uName.substring(1, uName.length()-1);
                                                                }
                                                            }
                                                            plist = posarr.get(uName);
                                                            dType = dTypes.get(uName);
                                                            cFormat = cFormats.get(uName);
                                                        } else
                                                        {
                                                            plist = posarr.get(mem.getName());
                                                            dType = dTypes.get(mem.getName());
                                                            cFormat = cFormats.get(mem.getName());
                                                        }
                                                        if (plist == null && mem.getUniqueName() != null && mem.getUniqueName().startsWith("[Measures]."))
                                                        {
                                                            plist = posarr.get(mem.getUniqueName());
                                                            dType = dTypes.get(mem.getUniqueName());
                                                            cFormat = cFormats.get(mem.getUniqueName());
                                                        }
                                                        if (plist == null)
                                                            continue;
                                                        coords.set(0, colnum);
                                                        coords.set(1, rownum);
                                                        for (int colindex : plist)
                                                        {
                                                            if (colTypes[colindex] == 0)
                                                            {
                                                                colTypes[colindex] = DataConductorUtil.getDataType(dType);
                                                            }
                                                            row[colindex] = getRowObject(String.valueOf(result.getCell(coords).getValue()), colTypes[colindex], cFormat);
                                                        }
                                                        colnum++;
                                                        if (isCancelled)
                                                            return null;
                                                    }
                                                }
                                            }
                                            if (topn < 0 || rownum < topn)
                                                data.add(row);
                                            rownum++;
                                        }
                                    }
                                    if (record > maxRecords)
                                    {
                                        // clear out the rows
                                        data.clear();
                                        data = null;
                                        logger.info("Too much data returned in the query (max = " + maxRecords + ")");
                                        throw new Exception("Too much data returned in the query (max = " + maxRecords + "). Please add filters to reduce the size of the result set.");
                                    }
                                    qmd.setColumnTypes(colTypes);
                                    qmd.setColumnCount(colTypes.length);
                                }
                            }
                        }
                        if (isCancelled)
                            return null;
                        if (qmd.getColumnCount() == 0)
                        {
                            int[] colTypes = new int[rowlength];
                            for (String dkey: dTypes.keySet())
                            {
                                List<Integer> plist = posarr.get(dkey);
                                if (plist == null)
                                    continue;
                                String dType = dTypes.get(dkey);
                                for (int colindex : plist)
                                {
                                    if (colTypes[colindex] == 0)
                                    {
                                        colTypes[colindex] = DataConductorUtil.getDataType(dType);
                                    }
                                }
                            }
                            qmd.setColumnTypes(colTypes);
                            qmd.setColumnCount(colTypes.length);
                        }
                        if (command.equals("resultsonlyquery") || command.equals("resultsonlyquerywithheaders"))
                        {
                            baos = new ByteArrayOutputStream();
                            PrintWriter pw = new PrintWriter(baos);
                            if (command.equals("resultsonlyquerywithheaders"))
                            {
                                int colCount = qmd.getColumnCount();
                                if (colCount > 0)
                                {
                                    String[] colNames = new String[colCount];
                                    for (String cKey : posarr.keySet())
                                    {
                                        List<Integer> plist = posarr.get(cKey);
                                        for (int colindex : plist)
                                        {
                                            colNames[colindex] = cKey;
                                        }
                                    }
                                    boolean first = true;
                                    for (String colName: colNames)
                                    {
                                        if (first)
                                            first = false;
                                        else
                                            pw.write("|");
                                        pw.write(colName);
                                    }
                                    pw.write("\r\n");
                                }
                            }
                            if (!data.isEmpty())
                            {
                                for (Object[] row : data)
                                {
                                    if (row.length > 0)
                                    {
                                        boolean firstT = true;
                                        for (int rowIdx = 0; rowIdx < row.length; rowIdx++)
                                        {
                                            if (firstT)
                                            {
                                                firstT = false;
                                            } else
                                            {
                                                pw.write("|");
                                            }
                                            if (row[rowIdx] == null)
                                            {
                                            } else
                                            {
                                                pw.write(row[rowIdx].toString().replace("\r\n", " "));
                                            }
                                        }
                                        pw.write("\r\n");
                                    }
                                }
                            }
                            pw.close();
                        }
                        else
                        {
                            oo.writeObject(qmd);
                            oo.writeObject(data);
                            addToActivityLog("Query results: " + data.size() + " rows, " + (!data.isEmpty() ? data.get(0).length : 0) + " columns", null);
                        }
                    } catch (Exception ex)
                    {
                        if (ex.getCause() != null && ex.getCause().getMessage().toLowerCase().contains("out of memory"))
                        {
                            qmd = new QueryMetaData("Query returned too much data, please refine");
                            addToActivityLog("Error: Query returned too much data, please refine", ex);
                            oo.writeObject(qmd);
                        } else
                        {
                            qmd = new QueryMetaData(ex.toString());
                            addToActivityLog("Error: " + ex.toString(), ex);
                            logger.error(ex.toString(), ex);
                            oo.writeObject(qmd);
                        }
                    }
                    oo.flush();
                    oo.close();
                } else if (command.equals("getcubes"))
                {
                    addToActivityLog("Get cubes", null);
                    try
                    {
                        ResultSet rs = olapConnection.getMetaData().getCubes(olapConnection.getCatalog(), null, null);
                        StringBuilder sb = new StringBuilder();
                        int numCubes = 0;
                        while (rs.next())
                        {
                            sb.append(rs.getString("CUBE_NAME") + "\r\n");
                            numCubes++;
                        }
                        addToActivityLog("Retrieved " + numCubes + " cubes", null);
                        baos.write(sb.toString().getBytes("UTF-8"));
                    } catch (Exception ex)
                    {
                        addToActivityLog("Error: " + ex.toString(), ex);
                        baos.write(("Error: Unable to get cubes from Server: " + dc.getServerName()).getBytes("UTF-8"));
                    }
                } else if (command.equals("getcube"))
                {
                    int dimStructureImportType = RELATIONAL_DIMENSIONAL_STRUCTURE;
                    String[] args = query.split("\t");
                    String cubeName = args[0];
                    if (args.length > 1)
                    {
                        dimStructureImportType = Integer.parseInt(args[1]);
                    }
                    addToActivityLog("Geting cube: " + cubeName, null);
                    try
                    {
                        Schema sch = ((org.olap4j.driver.xmla.XmlaOlap4jConnection)olapConnection).getOlapSchema();
                        NamedList<Cube> cubelist = sch.getCubes();
                        StringBuilder sb = new StringBuilder();
                        for (Cube cube : cubelist)
                        {
                            if (!cube.getName().equals(cubeName))
                                continue;
                            NamedList<Dimension> dimlist = cube.getDimensions();
                            for (Dimension dim : dimlist)
                            {
                                if (dim.getName().equals("Measures"))
                                    continue;
                                String dimName = dim.getName();
                                String dimUniqueName = dim.getUniqueName();
                                NamedList<Hierarchy> hlist = dim.getHierarchies();
                                for (Hierarchy h : hlist)
                                {
                                    String hname = h.getName();
                                    String hUniqueName = h.getUniqueName();
                                    NamedList<org.olap4j.metadata.Level> llist = h.getLevels();
                                    XMLADataTypeMap dtMap = new XMLADataTypeMap(olapConnection, cubeName, dimUniqueName, hUniqueName);
                                    dtMap.fetchDataTypesForAllLevels();
                                    for (org.olap4j.metadata.Level l : llist)
                                    {
                                        int width = 0;
                                        // Only calc cardinality of small levels (huge performance hit)
                                        /*if (l.getCardinality() > 0 && l.getCardinality() < 50 && !DatabaseConnection.SAP_BW.equals(dc.getDatabaseType()))
                                        {
                                            for (Member m : l.getMembers())
                                            {
                                                int mwidth = m.getName().length();
                                                if (mwidth > width)
                                                    width = mwidth;
                                            }
                                        } else
                                            */width = 100;
                                        String lname = l.getName();
                                        if (lname.startsWith('[' + dimName + "]."))
                                        {
                                            int index = lname.indexOf('.');
                                            lname = lname.substring(index + 1);
                                        }
                                        String dataType = dtMap.getDataTypeForLevel(l.getUniqueName());
                                        if (dataType == null)
                                        {
                                            addToActivityLog("Could not find DataType for level " + l.getUniqueName() + " from OlapConnection, defaulting DataType to Varchar", null);
                                            logger.debug("Could not find DataType for level " + l.getUniqueName() + " from OlapConnection, defaulting DataType to Varchar");
                                            dataType = "Varchar";
                                        }
                                        String logicalDimName = dimName;
                                        String logicalHName = hname;
                                        String logicalLName = lname;
                                        String lUniqueName = l.getUniqueName();
                                        if (DatabaseConnection.SAP_BW.equals(dc.getDatabaseType()))
                                        {
                                            String dimCaption = dim.getCaption(null);
                                            if (dimCaption != null && !dimCaption.trim().isEmpty())
                                            {
                                                logicalDimName = dimCaption.trim();
                                            }
                                            String hCaption = h.getCaption(null);
                                            if (hCaption != null && !hCaption.trim().isEmpty())
                                            {
                                                logicalHName = hCaption.trim();
                                            }
                                            String lCaption = l.getCaption(null);
                                            if (lCaption != null && !lCaption.trim().isEmpty())
                                            {
                                                logicalLName = lCaption.trim();
                                            }
                                        }
                                        sb.append(cube.getName() + "\t" + dimName + "\t" + logicalDimName + "\t" + hname + "\t" + logicalHName + "\t" + lname + "\t" + logicalLName + "\t" + lUniqueName + "\t" + dataType + "\t" + width + "\r\n");
                                    }
                                    dtMap = null;
                                }
                            }
                            List<Measure> mlist = cube.getMeasures();
                            for (Measure m : mlist)
                            {
                                String aggregator = "SUM";
                                if (m.getAggregator() != null)
                                    aggregator = m.getAggregator().name();
                                String logicalMName = m.getName();
                                if (DatabaseConnection.SAP_BW.equals(dc.getDatabaseType()))
                                {
                                    String mCaption = m.getCaption(null);
                                    if (mCaption != null && !mCaption.trim().isEmpty())
                                    {
                                        logicalMName = mCaption.trim();
                                    }
                                }
                                sb.append(cube.getName() + "\tMeasures\t\t" + m.getName() + "\t" + logicalMName + "\t" + m.getDatatype() + "\t" + aggregator + "\r\n");
                            }
                        }
                        addToActivityLog("Retrieved cube", null);
                        baos.write(sb.toString().getBytes("UTF-8"));
                    } catch (Exception ex)
                    {
                        addToActivityLog("Error: " + ex.toString(), ex);
                        baos.write(("Error: Unable to retrieve cube: " + cubeName).getBytes("UTF-8"));
                    }
                }
            }
        }
        finally
        {
            if (!usingConnectionFromConnectionPool && olapConnection != null)
            {
                olapConnection.close();
            }
        }
        System.setProperty("http.keepAlive", "false");
        return baos;
    }

    private void writeData(ByteArrayOutputStream baos) throws IOException, MalformedURLException, GeneralErrorException, AuthenticationException, SpaceUnavailableException
    {
        if (baos == null || baos.size() == 0)
        {
            baos = new ByteArrayOutputStream();
            baos.write("EMPTY".getBytes("UTF-8"));
        }        String path = "/" + URLEncoder.encode(space, "UTF-8").replace("+", "%20") + "/dcrealtimedata/" + URLEncoder.encode(name, "UTF-8").replace("+", "%20");
        HttpURLConnection hs = Util.getHttpURLConnection(url, path, "POST", true);
        hs.setRequestProperty("Content-Length", Integer.toString(baos.size()));
        hs.setRequestProperty("RequestIndex", Integer.toString(qindex));
        hs.connect();
        OutputStream sos = hs.getOutputStream();
        baos.writeTo(sos);
        Util.dumpHttpResponse(hs);
    }

    private Object getRowObject(String value, int dataType, String format)
    {
        Object obj = null;
        try
        {
            if(dataType == java.sql.Types.VARCHAR)
            {
                obj = value;
            }
            else if(dataType == java.sql.Types.INTEGER)
            {
                obj = Integer.valueOf(value);
            }
            else if(dataType == java.sql.Types.NUMERIC || dataType == java.sql.Types.DOUBLE || dataType == java.sql.Types.FLOAT)
            {
                obj = Double.valueOf(value);
            }
            else if(dataType == java.sql.Types.TIMESTAMP)
            {
                SimpleDateFormat sdf = new SimpleDateFormat(format != null && !format.trim().isEmpty() ? format : "yyyy-MM-dd HH:mm:ss.SSS");
                sdf.setLenient(false);
                obj = new java.sql.Timestamp(sdf.parse(value).getTime());
            }
            else if(dataType == java.sql.Types.DATE)
            {
                SimpleDateFormat sdf = new SimpleDateFormat(format != null && !format.trim().isEmpty() ? format : "yyyy-MM-dd");
                sdf.setLenient(false);
                obj = new java.sql.Timestamp(sdf.parse(value).getTime());
            }
            else
            {
                obj = value;
            }
        }
        catch(Exception e)
        {
            obj = null;
        }
        return obj;
    }

    private void addToActivityLog(String message, Throwable throwable)
    {
        try
        {
        	if (activityLog != null)
        	{
        		activityLog.add(message, throwable);
        	}
            if (va != null)
            {
                va.update();
            }
        }
        catch (Exception ex)
        {
            logger.warn("Failed to add message to the activity log - " + message + " - " + ex.getMessage());
        }
    }

    public boolean isValidConnection(Connection conn, int timeout)
    {
         if (conn == null)
         {
            logger.error("Unable to connecto to the database: " + dc.getServerName());
            addToActivityLog("Unable to connect to database: " + dc.getServerName(), null);
         }
         else
         {
            try
            {
                if (conn.isValid(timeout))
                    return true;
            } catch (SQLException ex)
            {
                logger.error(ex, ex);
                addToActivityLog("Error: " + ex.toString(), ex);
            }
         }
         return false;
    }
    
    public String getQuery()
    {
    	return qString;
    }
    
    private ObjectOutputStream getObjectOutputStream(ByteArrayOutputStream baos, String commandType) throws IOException
    {
        ObjectOutputStream oo = null;
        if (commandType.equals("query"))
        {
            GZIPOutputStream gzis = new GZIPOutputStream(baos);
            oo = new ObjectOutputStream(gzis);
        }
        else
        {
            oo = new ObjectOutputStream(baos);
        }
        return oo;
    }
}
