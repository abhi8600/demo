/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.birst.dataconductor;

import java.io.Serializable;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Types;
import org.apache.log4j.Logger;


/**
 *
 * @author bpeters
 */
public class QueryMetaData implements Serializable
{

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private int columnCount;
    private int[] columnTypes;
    private String errorMessage;

    public QueryMetaData()
    {
    }

    public QueryMetaData(ResultSetMetaData rsm)
    {
        try
        {
            this.columnCount = rsm.getColumnCount();
            this.columnTypes = new int[columnCount];
            for (int i = 0; i < columnCount; i++)
            {
                switch (rsm.getColumnType(i + 1))
                {
                    case Database.oracle_sql_TimestampTZ://oracle.sql.TimestampTZ
                    case Database.oracle_sql_TimestampLTZ://oracle.sql.TimestampLTZ
                        columnTypes[i] = Types.TIMESTAMP;
                        break;
                    default:
                        columnTypes[i] = rsm.getColumnType(i + 1);
                }                
            }
        } catch (SQLException ex)
        {
            Logger.getLogger(QueryMetaData.class).error(null, ex);
        }
    }

    public QueryMetaData(String errorMessage)
    {
        this.errorMessage = errorMessage;
    }

    public int getColumnCount()
    {
        return columnCount;
    }

    public int getColumnType(int index)
    {
        return columnTypes[index];
    }

    public void setColumnCount(int columnCount)
    {
        this.columnCount = columnCount;
    }

    public void setColumnTypes(int[] columnTypes)
    {
        this.columnTypes = columnTypes;
    }

    public boolean isError()
    {
        return errorMessage != null;
    }

    public String errorMessage()
    {
        return errorMessage;
    }
}
