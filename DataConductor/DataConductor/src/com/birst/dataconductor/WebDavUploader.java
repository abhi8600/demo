/**
 * $Id: WebDavUploader.java,v 1.52 2012-10-10 13:11:42 BIRST\mpandit Exp $
 *
 * Copyright (C) 2010 Birst, Inc. All rights reserved.
 * BIRST PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.birst.dataconductor;

import java.io.*;
import java.net.*;
import java.text.*;
import java.util.zip.*;
import org.apache.log4j.Logger;
import java.security.*;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 *
 * @author bpeters
 */
public class WebDavUploader
{

    private String url;
    String username;
    String password;
    String space;
    GraphicsObject go;
    int numRetries;
    DataConductorUtil dcu;
    boolean ignoreSpaceInUseCheck;
    String characterEncoding;

    private static long SEGMENT_SIZE = 50 * 200 * 1024; // 10MB
    private static int CHUNK_SIZE = 100 * 1024;
    private static final int READTIMEOUT = 4*60*1000; // 4 minutes - DO NOT SET TO ZERO, ZERO causes HttpURLConnection to try a second POST on timeout
    private static final int CONNECTTIMEOUT = 30*1000; // 30 seconds
    private static final int INITIAL_SLEEP_TIME = 4 * 1000; // 4 seconds, for the first few sleeps
    private static final int NUMBER_OF_SMALL_SLEEPS = 15; // 15 INITIAL_SLEEP_TIME iterations
    private static final int SLEEP_TIME = 30 * 1000; // 30 seconds, sleep time between status check requests
    private static final int RETRY_SLEEP_TIME = 30 * 1000; // 30 seconds

    // status codes returned from the server when checking the status of the scan
    private static final int POLL_RUNNING = 1;
    private static final int POLL_FAILED = -1;
    private static final int POLL_COMPLETE = 0;

    private static final int MAX_POLL_FAILURES = 20; // 20 failures in a row - 10 minutes of failures

    private static final int ROWS_TO_SCAN = 100000; // for local etl task, only send 100k rows of sourcefile to server for scanning

    private final static Logger logger = Logger.getLogger(WebDavUploader.class);

    private String responseLogFile = null;
    
    public static final String PHASE_UPLOAD = "1";
    public static final String PHASE_UNPACK = "2";
    public static final String PHASE_POLL = "3";
    public static final String PHASE_INIT_PARALLEL_UPLOAD = "4";
    public static final String PHASE_ASSEMBLE_PARALLEL_UPLOAD = "5";
    public static final String PHASE_POLL_FINISH_PARALLEL_UPLOAD = "6";
    private BlockingQueue<Segment> queue;
    private AtomicBoolean uploadAborted = new AtomicBoolean(false);
    
    public WebDavUploader(String username, String password, String space, String url, GraphicsObject go, int numRetries, DataConductorUtil dcu, boolean ignoreSpaceInUseCheck)
    {
        this.username = username;
        this.password = password;
        this.space = space;
        this.url = url;
        this.go = go;
        this.numRetries = 10; // this is silly, not the correct use of retries (this is a micro level retry, what is passed in is a macro level retry), so overriding the value passed in
        this.dcu = dcu;
        this.ignoreSpaceInUseCheck = ignoreSpaceInUseCheck;
    }

    public void setResponseLogFile(String responseLogFile)
    {
        this.responseLogFile = responseLogFile;
    }

    public void setCharacterEncoding(String characterEncoding)
    {
        this.characterEncoding = characterEncoding;
    }
    /*
     * set up the url connection for file uploading
     */
    private HttpURLConnection setupConnection(String space, String filename, String phase) throws UnsupportedEncodingException, MalformedURLException, IOException, GeneralErrorException, AuthenticationException, SpaceUnavailableException {
        String path = "/" + URLEncoder.encode(space, "UTF-8").replace("+", "%20") + "/" + URLEncoder.encode(filename, "UTF-8").replace("+", "%20");
        HttpURLConnection hs = Util.getHttpURLConnection(url, path, "POST", ignoreSpaceInUseCheck);
        hs.setReadTimeout(READTIMEOUT); // DO NOT SET TO ZERO, ZERO causes HttpURLConnection to try a second POST on timeout
        hs.setConnectTimeout(CONNECTTIMEOUT);
        hs.setRequestProperty("Version", "1"); // new way to upload files, this will eventually go away as the new will become the old
        hs.setRequestProperty("Phase", phase);
        return hs;
    }

    public WebDAVError uploadFile(String directory, String filename, boolean scan, UploadResult result, boolean uploadForScanningOnly)
    {
        File tempDir = null;
        try
        {
            File f = new File(directory + File.separator + filename);
            if (!f.exists())
            {
                return new WebDAVError(WebDAVError.UPLOAD_ERROR, f.getAbsolutePath() + "does not exist");
            }
            if (go != null)
                go.setStatusMessage("Uploading: " + f.getName());

            if (uploadForScanningOnly)
            {
                // only send ROWS_TO_SCAN rows to server for scanning sourcefile
                BufferedReader br = null;
                BufferedWriter bw = null;
                try
                {
                    if (characterEncoding != null)
                        logger.debug("uploadfile characterset: " +  characterEncoding);
                    if (characterEncoding == null)
                        br = new BufferedReader(new InputStreamReader(new FileInputStream(f)));
                    else
                        br = new BufferedReader(new InputStreamReader(new FileInputStream(f), characterEncoding));
                    tempDir = dcu.getTempDirectory();
                    File df = new File(tempDir + File.separator + f.getName());
                    df.createNewFile();
                    if (characterEncoding == null)
                        bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(df)));
                    else
                      bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(df), characterEncoding));
                    String line = null;
                    int noLines = 0;
                    while ((line = br.readLine()) != null && noLines < ROWS_TO_SCAN)
                    {
                        bw.write(line);
                        bw.newLine();
                        noLines++;
                    }
                    bw.flush();
                    f = df;
                }
                catch (Exception ex)
                {
                    logger.error(ex.getMessage(), ex);
                } finally
                {
                    if (br != null)
                        br.close();
                    if (bw != null)
                        bw.close();                    
                }
            }

            // Calculate number of segments
            int numSegments = (int) Math.ceil((double) f.length() / SEGMENT_SIZE);
            int numThreads = 1;
            boolean usesParallelUpload = false;
            if (numSegments > 1 && scan)
            {
                //first find out if the server can deal with multiple segments being uploaded in parallel
                HttpURLConnection hs = null;
                try
                {
                    String path = "/" + URLEncoder.encode(space, "UTF-8").replace("+", "%20") + "/" + URLEncoder.encode("supportsparallelupload", "UTF-8").replace("+", "%20");
                    hs = Util.getHttpURLConnection(url, path, "GET", false);
                    hs.setReadTimeout(60 * 1000); // set the polling timeout (60 seconds)
                    hs.connect();
                    Util.dumpHttpResponse(hs);
                    int code = hs.getResponseCode();
                    if (code != HttpURLConnection.HTTP_OK)
                    {
                        logger.debug("Server not able to handle parallel upload for file, using single upload thread");
                    }
                    else
                    {
                        BufferedReader breader = new BufferedReader(new InputStreamReader(hs.getInputStream()));
                        String response = null;
                        String inputLine;
                        StringBuilder sb = new StringBuilder();
                        while ((inputLine = breader.readLine()) != null)
                        {
                            sb.append(inputLine);
                            sb.append("\n");
                        }
                        breader.close();
                        if (sb.length() == 0)
                        {
                            throw new Exception("Failure in finding max no. of parallel threads server supports");
                        }
                        response = sb.toString();
                        WebDavResponse wdr = WebDavResponse.getResponse(response);
                        if (wdr.getCode() > 1)
                        {
                            hs.disconnect();
                            hs = setupConnection(space, filename, PHASE_INIT_PARALLEL_UPLOAD);
                            hs.connect();
                            // dummy write to force the connection
                            OutputStream sos = hs.getOutputStream();
                            sos.write(0);
                            sos.close();
                            Util.dumpHttpResponse(hs);
                            code = hs.getResponseCode();
                            if (code != HttpURLConnection.HTTP_OK)
                            {
                                throw new Exception("Failure in sending init upload request to server");
                            }
                            numThreads = Math.min(numSegments, wdr.getCode());
                            logger.debug("Using parallel upload for file - " + filename + " - using " + numThreads + " threads for parallel upload");
                            usesParallelUpload = true;
                        }
                    }
                }
                catch (Exception ex)
                {
                    logger.error(ex.getMessage(), ex);
                    return new WebDAVError(WebDAVError.UPLOAD_ERROR, ex.getMessage());                    
                }
                finally
                {
                    
                    if (hs != null)
                        hs.disconnect();
                }
            }
            
            queue = new LinkedBlockingQueue<Segment>(numSegments);
            for (int i=0; i<numSegments; i++)
            {
                Segment seg = new Segment(i);
                queue.add(seg);
            }
            UploadThread[] uploadThreads = new UploadThread[numThreads];
            for (int i=0; i<numThreads; i++)
            {
                uploadThreads[i] = new UploadThread(usesParallelUpload, i, numSegments, filename, f, scan, result);
                uploadThreads[i].start();
            }
            for (int i=0; i<numThreads; i++)
            {
                try
                {
                    uploadThreads[i].join();
                }
                catch (InterruptedException e)
                {
                    logger.error(e);
                }
            }
            
            if (uploadAborted.get())
            {
                logger.error("Problem encountered in parallel upload of file");
                return new WebDAVError(WebDAVError.UPLOAD_ERROR, "Problem encountered in parallel upload of file");
            }
            
            if (usesParallelUpload)
            {
                try
                {
                    WebDAVError err = triggerOperation(filename, numSegments, PHASE_ASSEMBLE_PARALLEL_UPLOAD, "assembling");
                    if (err != null)
                        return err;
                    err = pollForStatus(filename, "assembling", PHASE_POLL_FINISH_PARALLEL_UPLOAD);
                    if (err != null)
                        return err;
                }
                catch (Exception ex)
                {
                    logger.error(ex.getMessage(), ex);
                    return new WebDAVError(WebDAVError.UPLOAD_ERROR, ex.getMessage());                    
                }                
            }
            logger.info("Successfully uploaded: " + filename);
            
            if (!scan) // don't scan some files
                return null;

            // start scanning the zip file
            WebDAVError err = triggerOperation(filename, 0, PHASE_UNPACK, "scanning");
            if (err != null)
                return err;

            // wait for the result
            return pollForStatus(filename, "scanning", PHASE_POLL);
            
        } catch (IOException ex)
        {
            logger.error(ex.getMessage(), ex);
            return new WebDAVError(WebDAVError.UPLOAD_ERROR, ex.getMessage());
        } finally {
            dcu.cleanupTempDirectory(tempDir);
        }
    }
    
    private class Segment
    {
        int segmentNo;
        public Segment(int segmentNo)
        {
            this.segmentNo = segmentNo;
        }
    }
    
    private class UploadThread extends Thread
    {
        boolean usesParallelUpload = false;
        int numSegments;
        String filename;
        File file;
        boolean scan;
        UploadResult result;
        public UploadThread(boolean usesParallelUpload, int threadNo, int numSegments, String filename, File file, boolean scan, UploadResult result)
        {
            super("UploadThread - " + threadNo);
            this.usesParallelUpload = usesParallelUpload;
            this.numSegments = numSegments;
            this.filename = filename;
            this.file = file;
            this.scan = scan;
            this.result = result;
        }
        
        public void run()
        {
            while(!queue.isEmpty())
            {
                Segment seg = queue.poll();
                if (seg == null)
                {
                    logger.debug(super.toString() + " - No Segment found in queue");
                    return;
                }
                WebDAVError error = null;
                try
                {
                    logger.debug(super.toString() + " - Uploading segment - " + seg.segmentNo);
                    error = uploadSegment(usesParallelUpload, seg.segmentNo, filename, numSegments, file, scan, result);
                    if (error != null)
                    {
                        logger.error(super.toString() + " - Error in uploading segment : " + seg.segmentNo + " - " + error.toString());
                        uploadAborted.set(true);
                    }
                    else
                    {
                        logger.info(super.toString() + " - Successfully uploaded segment : " + seg.segmentNo);
                    }
                }
                catch (IOException ex)
                {
                    logger.error(super.toString() + " - Error in uploading segment : " + seg.segmentNo + " - " + ex.getMessage(), ex);
                    uploadAborted.set(true);
                }
                if (uploadAborted.get())
                {
                    logger.error(super.toString() + " - Upload aborted - exiting thread - " + this.getName());
                    return;
                }
            }
            logger.debug(super.toString() + " - Exiting upload thread - " + this.getName());
        }
    }
    
    private WebDAVError uploadSegment(boolean usesParallelUpload, int segment, String filename, int numSegments, File f, boolean scan, UploadResult result) throws IOException
    {
        DecimalFormat dec = new DecimalFormat("###.##");
        WebDAVError error = null;
        Exception ex = null;
        int tries = 0; // allow retries if the connection fails
        do
        {
            FileInputStream fis = null;
            HttpURLConnection hs = null;
            BufferedReader in = null;
            try
            {
                error = null;
                hs = setupConnection(space, filename, PHASE_UPLOAD);
                hs.setRequestProperty("Content-Type", "application/octet-stream"); // XXX really should use proper form encoding

                // determine if a full segment or a final partial segment (or just a small file)
                long remainder = f.length() - (segment * SEGMENT_SIZE);
                long length = SEGMENT_SIZE;
                if (remainder < 0)
                    length = Math.min(f.length(), length);
                else
                    length = Math.min(remainder, length);
                hs.setRequestProperty("Segment-size", Long.toString(SEGMENT_SIZE));
                hs.setRequestProperty("Segment", Integer.toString(segment));
                hs.setRequestProperty("Num-segments", Integer.toString(numSegments));
                hs.setRequestProperty("Segment", Integer.toString(segment));
                if (usesParallelUpload)
                {
                    hs.setRequestProperty("isParallelUpload", Boolean.TRUE.toString());
                }

                // read the segment into memory (in order to calculate the hash)
                fis = new FileInputStream(f);
                if (segment > 0)
                    fis.skip(SEGMENT_SIZE * segment); // skip to the correct segment within the file
                int pos = 0;
                int chunk = CHUNK_SIZE;
                byte[] data = new byte[(int) length];
                byte[] buf = new byte[chunk];
                do
                {
                    int len = fis.read(buf);
                    System.arraycopy(buf, 0, data, pos, len);
                    pos += len;
                } while (pos < length);

                // calculate SHA-256 hash value for the segment
                try
                {
                    MessageDigest sha256 = MessageDigest.getInstance("SHA-256");
                    sha256.update(data);
                    byte[] hash = sha256.digest();
                    String strhash = Util.getHexString(hash);
                    hs.setRequestProperty("Hash256", strhash);
                } catch (java.security.NoSuchAlgorithmException e)
                {
                	if (logger.isTraceEnabled()) {
                        logger.trace("Exception " + ex + " occured while processing method WebDavUploader.uploadSegment(boolean, int, String, int, File, boolean, UploadResult) ", ex);
                    }
                }
                long glength = length;
                // compress bigger data using gzip
                if (scan) {
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    GZIPOutputStream gzipStream = new GZIPOutputStream(baos);
                    gzipStream.write(data);
                    gzipStream.close();
                    glength = baos.size();
                    hs.setRequestProperty("Content-Encoding", "gzip");
                    data = baos.toByteArray();
                    baos.close();
                }
                logger.debug("Uploading: " + filename + " (" + dec.format((segment + 1) * 100.0 / numSegments) + "%), seg: " + segment + ", len: " + length + " (" + glength + "), retries: " + tries);
                hs.setRequestProperty("Content-Length", Long.toString(length)); // XXX calculated by Java

                // make the connection (deals with proxies and other stuff)
                hs.connect();
                // write out the segment
                OutputStream sos = null;
                try {
                    sos = hs.getOutputStream();
                    sos.write(data, 0, (int) glength);
                } finally {
                    if (sos != null) {
                        sos.close();
                    }
                }

                // get the response
                Util.dumpHttpResponse(hs);
                int code = hs.getResponseCode();
                if (code != HttpURLConnection.HTTP_OK)
                {
                    // unlikely to be recoverable, try again later
                    error = new WebDAVError(WebDAVError.UPLOAD_ERROR, hs.getResponseMessage());
                    try {
                        if (tries + 1 < numRetries)
                            Thread.sleep(RETRY_SLEEP_TIME);
                    } catch (InterruptedException ex1) {
                    }
                }
                else
                {
                    // this is needed for dealing with responses from the server, like command file execution output
                    in = new BufferedReader(new InputStreamReader(hs.getInputStream()));
                    String inputLine;
                    StringBuilder sb = new StringBuilder();
                    while ((inputLine = in.readLine()) != null)
                    {
                        sb.append(inputLine);
                        sb.append("\n");
                    }
                    if (result != null)
                        result.responseString = sb.toString();
                }
            } catch (Exception sex)
            {
                // could be a recoverable connection failure (XXX check for ConnectException?)
                error = new WebDAVError(WebDAVError.UPLOAD_ERROR, sex.getMessage());
                ex = sex;
                logger.error(sex.getMessage(), sex);
                try {
                    if (tries + 1 < numRetries)
                        Thread.sleep(RETRY_SLEEP_TIME);
                } catch (InterruptedException ex1) {
                }
            } finally
            {
                if (in != null)
                    in.close();
                if (fis != null)
                    fis.close();
                if (hs != null)
                    hs.disconnect();
            }
        } while (error != null && tries++ < numRetries); // try numRetries if there is an error

        // still failed after numRetries
        if (error != null) {
            if (ex != null) {
                logger.error(ex.getMessage(), ex);
            }
            logger.error(error.toString());
            return error;
        }
        return null;
    }
    
    // start the scanning
    private WebDAVError triggerOperation(String filename, int numSegments, String phase, String operation)
    {
        go.setStatusMessage(operation + ": " + filename); // update the status message in the UI
        HttpURLConnection hs = null;
        try {
            hs = setupConnection(space, filename, phase);
            hs.setRequestProperty("Num-segments", Integer.toString(numSegments));
            hs.connect();
            // dummy write to force the connection
            OutputStream sos = hs.getOutputStream();
            sos.write(0);
            sos.close();
            Util.dumpHttpResponse(hs);
            int code = hs.getResponseCode();
            if (code != HttpURLConnection.HTTP_OK) {
                return new WebDAVError(WebDAVError.UPLOAD_ERROR, hs.getResponseMessage());
            }
        } catch (Exception ex) {
            return new WebDAVError(WebDAVError.UPLOAD_ERROR, ex.getMessage());
        } finally {
            if (hs != null) {
                hs.disconnect();
            }
        }
        return null;
    }

    // poll for the status of the scan - should use GET XXX
    public WebDAVError pollForStatus(String filename, String operation, String phase)
    {
        go.setStatusMessage("Waiting for the " + operation + " to complete: " + filename); // update the status message in the UI
        int failures = 0;
        int count = 0;
        do {
            HttpURLConnection hs = null;
            BufferedReader in = null;
            try {
                hs = setupConnection(space, filename, phase);
                logger.info("Polling for " + operation + " status (" + failures + "): " + filename);
                hs.connect();
                // dummy write to force the connection (this is why this should be a GET XXX)
                OutputStream sos = hs.getOutputStream();
                sos.write(0);
                sos.close();
                Util.dumpHttpResponse(hs);
                int code = hs.getResponseCode();
                if (code != HttpURLConnection.HTTP_OK) {
                    throw new Exception(hs.getResponseMessage());
                } else {
                	// read the status response from the server
                    in = new BufferedReader(new InputStreamReader(hs.getInputStream()));
                    String inputLine;
                    StringBuilder sb = new StringBuilder();
                    while ((inputLine = in.readLine()) != null) {
                        sb.append(inputLine);
                        sb.append('\n');
                    }

                    String temp = sb.toString();
                    int exitStatus = POLL_RUNNING;
                    if (temp.startsWith("Status ")) {
                        try
                        {
                            int end = temp.indexOf('\n');
                            String status = temp.substring("Status ".length(), end);
                            exitStatus = Integer.parseInt(status);
                            failures = 0; // reset failures
                        }
                        catch  (NumberFormatException e) 
                        {
                            failures++; // bad number, malformed status response
                            logger.warn("Malformed response from server (bad status code): " + temp + " - " + e.getMessage());
                        }
                        // return if we received a non-running status result
                        if (exitStatus == POLL_FAILED)
                            return new WebDAVError(WebDAVError.UPLOAD_ERROR, temp);
                        if (exitStatus == POLL_COMPLETE)
                        {
                            logger.info(sb.toString());
                            if (responseLogFile != null)
                            {
                                PrintWriter pw = new PrintWriter(new FileOutputStream(responseLogFile, true));
                                pw.write(sb.toString());
                                pw.flush();
                                pw.close();
                            }
                            return null; // null is success
                        }
                    }
                    else
                    {
                        failures++; // malformed status response
                        logger.warn("Malformed response from server (bad status line): " + temp);
                    }
                }
            } catch (Exception ex) {
                failures++; // connection/read failed
                logger.warn("Malformed response from server: " + ex.getMessage());
            }
            finally
            {
                if (in != null)
                    try {
                        in.close();
                    }
                    catch (IOException ioex)
                    {
                        failures++;
                        logger.warn("Malformed response from server: " + ioex.getMessage());
                    }
                if (hs != null)
                    hs.disconnect();
            }
            // sleep between polls
            try
            {
                count++;
                if (count < NUMBER_OF_SMALL_SLEEPS)
                    Thread.sleep(INITIAL_SLEEP_TIME); // so small uploads don't wait the full 30 seconds
                else
                    Thread.sleep(SLEEP_TIME);                
            }
            catch (InterruptedException ex)
            {
            	if (logger.isTraceEnabled()) {
                    logger.trace("Exception " + ex + " occured while processing method WebDavUploader.pollForStatus(String, String, String)", ex);
                }
            }
        } while (failures < MAX_POLL_FAILURES);
        
        // too many failures during the polling
        logger.warn("Failed after " + MAX_POLL_FAILURES + " polling failures.  Could be a network issue or a Birst server issue.");
        return new WebDAVError(WebDAVError.UPLOAD_ERROR, "Failed after " + MAX_POLL_FAILURES + " polling failures");
    }

    // XXX should use GET
    public static String getVariables(String url, String space, String variables)
    {
        HttpURLConnection hs = null;
        BufferedReader in = null;
        OutputStream hos = null;
        try
        {
            String path = "/" + URLEncoder.encode(space, "UTF-8").replace("+", "%20") + "/" + URLEncoder.encode("birst.variables", "UTF-8");
            hs = Util.getHttpURLConnection(url, path, "POST", false);
            hs.setRequestProperty("Connection", "Keep-Alive");
            hs.setRequestProperty("Content-Type", "application/octet-stream");
            hs.connect();
            hos = hs.getOutputStream();
            hos.write("birst.variables".getBytes());
            hos.write("\r\n".getBytes());
            hos.write(("variables=" + variables).getBytes());
            Util.dumpHttpResponse(hs);
            in = new BufferedReader(new InputStreamReader(hs.getInputStream()));
            String response = null;
            String line;
            StringBuilder sb = new StringBuilder();
            while ((line = in.readLine()) != null)
            {
                sb.append(line);                
            }
            response = sb.toString();
            return response;
         }
        catch (Exception ex)
         {
             if (hos != null)
             {
                 try
                 {
                     hos.close();
                 }
                 catch (IOException ioex) 
                 {
                    if (logger.isTraceEnabled()) 
                    {
                        logger.trace("Exception " + ioex.getMessage() + " occured while closing output stream for getVariables", ioex); 
                    }
                 }                    
             }
            logger.error(ex.getMessage(), ex);
            return "|";
         }
        finally
        {
            try
            {
                if (hs != null)
                    hs.disconnect();
                if (in != null)
                    in.close();
            } catch (Exception ex) { if (logger.isTraceEnabled()) {
                logger.trace("Exception " + ex + " occured while processing method WebDavUploader.getVariables(String, String)", ex);
            } }
        }
    }

    public static WebDavResponse checkStatus(String url, String space)
    {
        HttpURLConnection hs = null;
        BufferedReader breader = null;
        try
        {
            String path = "/" + URLEncoder.encode(space, "UTF-8").replace("+", "%20") + "/birst.checkstatus";
            hs = Util.getHttpURLConnection(url, path, "GET", false);
            hs.connect();
            Util.dumpHttpResponse(hs);
            breader = new BufferedReader(new InputStreamReader(hs.getInputStream()));
            String response = null;
            String inputLine;
            StringBuilder sb = new StringBuilder();
            while ((inputLine = breader.readLine()) != null)
            {
                System.out.println(inputLine);
                sb.append(inputLine);
                sb.append("\n");
            }
            breader.close();
            if (sb.length() == 0)
            {
                return null;
            }
            response = sb.toString();
            WebDavResponse wdr = WebDavResponse.getResponse(response);
            return wdr;
         }
        catch (Exception ex)
        {
            logger.error(ex.getMessage(), ex);
            return null;
        }
        finally
        {
            try
            {
                if (hs != null)
                    hs.disconnect();
                if (breader != null)
                    breader.close();
            }
            catch (Exception ex) { if (logger.isTraceEnabled()) {
                logger.trace("Exception " + ex + " occured while processing method WebDavUploader.checkStatus(String, String)", ex);
            } }
        }
    }

    public static WebDavResponse getServerBirstConnectVersion(String url, String space)
    {
        HttpURLConnection hs = null;
        BufferedReader breader = null;
        try
        {
            String path = "/" + URLEncoder.encode(space, "UTF-8").replace("+", "%20") + "/birst.birstconnect.serverversion";
            hs = Util.getHttpURLConnection(url, path, "GET", false);
            hs.connect();
            Util.dumpHttpResponse(hs);
            breader = new BufferedReader(new InputStreamReader(hs.getInputStream()));
            String response = null;
            String inputLine;
            StringBuilder sb = new StringBuilder();
            while ((inputLine = breader.readLine()) != null)
            {
                sb.append(inputLine);
                sb.append("\n");
            }
            breader.close();
            if (sb.length() == 0)
            {
                return null;
            }
            response = sb.toString();
            WebDavResponse wdr = WebDavResponse.getResponse(response);
            return wdr;
         }
        catch (Exception ex)
        {
            logger.error(ex.getMessage(), ex);
            return null;
        }
        finally
        {
            try
            {
                if (hs != null)
                    hs.disconnect();
                if (breader != null)
                    breader.close();
            }
            catch (Exception ex) {if (logger.isTraceEnabled()) {
                logger.trace("Exception " + ex + " occured while processing method WebDavUploader.getServerBirstConnectVersion(String, String) ", ex);
            }}
        }
    }

    public static String getRealtimeConnectionStatus(String url, String space, String operation, String name, String visiblename)
    {
        HttpURLConnection hs = null;
        OutputStream hos = null;
        BufferedReader in = null;
        StringBuilder sb = new StringBuilder();
        try
        {
            String path = "/" + URLEncoder.encode(space, "UTF-8").replace("+", "%20") + "/" + URLEncoder.encode("birst.getrealtimeconnectionstatus", "UTF-8");
            hs = Util.getHttpURLConnection(url, path, "POST", false);
            hs.setReadTimeout(0);
            hs.setConnectTimeout(0);
            hs.connect();
            hos = hs.getOutputStream();
            hos.write(("operation=" + operation).getBytes());
            hos.write("\r\n".getBytes());
            hos.write(("visiblename=" + visiblename).getBytes());
            hos.write("\r\n".getBytes());
            hos.write(("name=" + name).getBytes());
            Util.dumpHttpResponse(hs);
            in = new BufferedReader(new InputStreamReader(hs.getInputStream()));
            String inputLine = null;
            while ((inputLine = in.readLine()) != null)
            {
                sb.append(inputLine);
                sb.append("\r\n");
            }
        }
        catch (Exception ex)
        {
            logger.error(ex.getMessage(), ex);
            sb.setLength(0);
            sb.append("ERROR "+ex.getMessage());
        }
        finally
        {
            try
            {
                if (hs != null)
                    hs.disconnect();
                if (hos != null)
                    hos.close();
                if (in != null)
                    in.close();
            }
            catch (Exception ex) { if (logger.isTraceEnabled()) {
                logger.trace("Exception " + ex + " occured while processing method WebDavUploader.getRealtimeConnectionStatus(String, String, String, String, String) ", ex);
            } }
        }
        return sb.toString();
    }
    
    public static String getEnryptionKey(String url, String space)
    {
        HttpURLConnection hs = null;
        OutputStream hos = null;
        BufferedReader in = null;
        StringBuilder sb = new StringBuilder();
        try
        {
            String path = "/" + URLEncoder.encode(space, "UTF-8").replace("+", "%20") + "/" + URLEncoder.encode("birst.getencryptionkey", "UTF-8");
            hs = Util.getHttpURLConnection(url, path, "POST", false);
            hs.setReadTimeout(0);
            hs.setConnectTimeout(0);
            hs.connect();
            hos = hs.getOutputStream();
            hs.getOutputStream().write("birst.getencryptionkey".getBytes());
            Util.dumpHttpResponse(hs);
            in = new BufferedReader(new InputStreamReader(hs.getInputStream()));
            String inputLine = null;
            while ((inputLine = in.readLine()) != null)
            {
                sb.append(inputLine);
                sb.append("\r\n");
            }
        }
        catch (Exception ex)
        {
            logger.error(ex.getMessage(), ex);
            sb.setLength(0);
            sb.append("encryptionkey=");
        }
        finally
        {
            try
            {
                if (hs != null)
                    hs.disconnect();
                if (hos != null)
                    hos.close();
                if (in != null)
                    in.close();
            }
            catch (Exception ex) { if (logger.isTraceEnabled()) {
                logger.trace("Exception " + ex + " occured while processing method WebDavUploader.getEnryptionKey(String, String) ", ex);
            }  }
        }
        return sb.toString();
    }
}