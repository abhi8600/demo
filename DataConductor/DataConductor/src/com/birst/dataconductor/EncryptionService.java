/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.birst.dataconductor;

/**
 * $Id: EncryptionService.java,v 1.9 2011-07-15 22:13:54 ricks Exp $
 *
 * Copyright (C) 2007-2011 Birst, Inc. All rights reserved.
 * SUCCESS METRICS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.BufferedReader;
import java.io.StringReader;
import java.io.IOException;
import org.apache.log4j.Logger;

public final class EncryptionService
{
    private static EncryptionService instance;
    private static String key;
    private final static Logger logger = Logger.getLogger(EncryptionService.class);

    private EncryptionService()
    {
    }

    public static synchronized EncryptionService getInstance()
    {
        if (instance == null)
        {
            instance = new EncryptionService();
        }
        return instance;
    }

    public static void setKey(String response)
    {
        try
        {
            BufferedReader reader = new BufferedReader(new StringReader(response));
            String line = null;
            while ((line = reader.readLine()) != null)
            {
                if (line.startsWith("encryptionkey="))
                {
                    key = line.substring("encryptionkey=".length());
                }
            }
        } catch (IOException ex)
        {
            logger.error(null, ex);
        }
    }

    /**
     * decrypt the DB password
     */
    public String decrypt(String text)
    {
        return decrypt(text, key);
    }

    /**
     * Decrypt a string using a password
     * 
     * @param text
     * @param password
     * @return
     * @throws Exception
     */
    public synchronized String decrypt(String text, String password)
    {
        if (text == null)
        {
            return (null);
        }
        try
        {
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            byte[] keyBytes = new byte[16];
            byte[] b = password.getBytes("UTF-8");
            int len = b.length;
            if (len > keyBytes.length)
            {
                len = keyBytes.length;
            }
            System.arraycopy(b, 0, keyBytes, 0, len);
            SecretKeySpec keySpec = new SecretKeySpec(keyBytes, "AES");
            IvParameterSpec ivSpec = new IvParameterSpec(keyBytes);
            cipher.init(Cipher.DECRYPT_MODE, keySpec, ivSpec);
            byte[] results = cipher.doFinal(Base64Encoder.decode(text.getBytes("UTF-8")));
            return new String(results, "UTF-8");
        } catch (Exception ex)
        {
            logger.error("Decryption failed", ex);
        }
        return (null);
    }

    /**
     * Encrypt the db password
     */
    public String encrypt(String text)
    {
        return encrypt(text, key);
    }

    /**
     * Encrypt a string using a password
     */
    private synchronized String encrypt(String text, String password)
    {
        if (text == null)
        {
            return (null);
        }
        try
        {
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            byte[] keyBytes = new byte[16];
            byte[] b = password.getBytes("UTF-8");
            int len = b.length;
            if (len > keyBytes.length)
            {
                len = keyBytes.length;
            }
            System.arraycopy(b, 0, keyBytes, 0, len);
            SecretKeySpec keySpec = new SecretKeySpec(keyBytes, "AES");
            IvParameterSpec ivSpec = new IvParameterSpec(keyBytes);
            cipher.init(Cipher.ENCRYPT_MODE, keySpec, ivSpec);
            byte[] inputBytes = text.getBytes("UTF8");
            byte[] outputBytes = cipher.doFinal(inputBytes);
            return new String(Base64Encoder.encodebytes(outputBytes), "UTF-8");
        } catch (Exception ex)
        {
           logger.error("Encryption failed", ex);
        }
        return (null);
    }
}
