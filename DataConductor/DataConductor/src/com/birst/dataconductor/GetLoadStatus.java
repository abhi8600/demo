/**
 * $Id: GetLoadStatus.java,v 1.11 2011-07-26 07:50:36 rchandarana Exp $
 *
 * Copyright (C) 2010 Birst, Inc. All rights reserved.
 * BIRST PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.birst.dataconductor;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;
import java.net.*;
import org.apache.log4j.Logger;

/**
 *
 * @author bpeters
 */
public class GetLoadStatus
{
    private final static Logger logger = Logger.getLogger(GetLoadStatus.class);

    public static WebDAVError get(String uname, String pword, String space, String url)
    {
        HttpURLConnection hs = null;
        BufferedReader breader = null;
        try
        {
            String path = "/" + URLEncoder.encode(space, "UTF-8").replace("+", "%20") + "/birst.processing";
            hs = Util.getHttpURLConnection(url, path, "GET", false);
            hs.connect();
            Util.dumpHttpResponse(hs);
            breader = new BufferedReader(new InputStreamReader(hs.getInputStream()));
            String response = null;
            String inputLine;
            StringBuilder sb = new StringBuilder();
            while ((inputLine = breader.readLine()) != null)
            {
                System.out.println(inputLine);
                sb.append(inputLine);
                sb.append("\n");
            }
            breader.close();
            if (sb.length() == 0)
            {
                return null;
            }
            response = sb.toString();
            WebDAVError error = WebDAVError.getError(response);
            if (error != null)
            {
                return error;
            }
            return null;
        } catch (SpaceUnavailableException ex)
        {
            return new WebDAVError(WebDAVError.SPACE_UNAVAILABLE, ex.getMessage());
        } catch (Exception ex)
        {
            logger.error(ex.getMessage(), ex);
        }
        finally
        {
            try
            {
                if (breader != null)
                    breader.close();
                }
            catch (IOException ioex)
            {if (logger.isTraceEnabled()) {
                logger.trace("Exception " + ioex + " occured while processing method GetLoadStatus.get(String, String, String, String)", ioex);
            }}
            if (hs != null)
                hs.disconnect();
        }
        return null;
    }
}
