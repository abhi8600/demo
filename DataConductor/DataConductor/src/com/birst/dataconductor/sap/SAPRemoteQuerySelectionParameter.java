/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.birst.dataconductor.sap;

import com.birst.dataconductor.XmlEncoderUtil;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.jdom.Element;

/**
 *
 * @author mpandit
 */
public class SAPRemoteQuerySelectionParameter implements Cloneable {
    private String name;//name of SELECT-OPTION / PARAMETER
    private char kind;//S=SELECT-OPTION / P=PARAMETER
    private char type;//Selection field type
    private String dtyp;//Dictionary type of a selection
    private String dbField;//Reference field for selection <-- displayname of SELECT-OPTION
    private char noSelset;//No value set allowed for selection?
    private char obligatory;//Flag for various purposes (selections)
    private String operator;
    private String includeType;//I=Include/E=Exclude
    private Object lowValue;
    private Object highValue;
    
    public List<Element> getElements()
    {
        List<Element> elements = new ArrayList<Element>();
        if (name != null)
        {
            elements.add(XmlEncoderUtil.getStringPropertyElement("name", name));
        }
        if (kind != '\u0000')
        {
            elements.add(XmlEncoderUtil.getCharPropertyElement("kind", kind));
        }
        if (type != '\u0000')
        {
            elements.add(XmlEncoderUtil.getCharPropertyElement("type", type));
        }
        if (dtyp != null)
        {
            elements.add(XmlEncoderUtil.getStringPropertyElement("dtyp", dtyp));
        }
        if (dbField != null)
        {
            elements.add(XmlEncoderUtil.getStringPropertyElement("dbField", dbField));
        }
        if (noSelset != '\u0000')
        {
            elements.add(XmlEncoderUtil.getCharPropertyElement("noSelset", noSelset));
        }
        if (obligatory != '\u0000')
        {
            elements.add(XmlEncoderUtil.getCharPropertyElement("obligatory", obligatory));
        }
        if (operator != null)
        {
            elements.add(XmlEncoderUtil.getStringPropertyElement("operator", operator));
        }
        if (includeType != null)
        {
            elements.add(XmlEncoderUtil.getStringPropertyElement("includeType", includeType));
        }
        if (lowValue != null)
        {
            Element obj = new Element("void");
            obj.setAttribute("property", "lowValue");
            Element objType = null;
            if (lowValue instanceof String)
            {
                objType = new Element("string");                            
            }
            else if (lowValue instanceof Character)
            {
                objType = new Element("char");                            
            }
            else if (lowValue instanceof Integer)
            {
                objType = new Element("int");                
            }
            else if (lowValue instanceof Long || lowValue instanceof Date)
            {
                objType = new Element("long");
            }
            else if (lowValue instanceof Float)
            {
                objType = new Element("float");                
            }
            else if (lowValue instanceof Double)
            {
                objType = new Element("double");                
            }
            
            if(lowValue instanceof Date)
                objType.addContent(String.valueOf(((Date)lowValue).getTime()));
            else
                objType.addContent(lowValue.toString());
            obj.addContent(objType);
            elements.add(obj);
        }
        if (highValue != null)
        {
            Element obj = new Element("void");
            obj.setAttribute("property", "highValue");
            Element objType = null;
            if (highValue instanceof String)
            {
                objType = new Element("string");                            
            }
            else if (highValue instanceof Character)
            {
                objType = new Element("char");                            
            }
            else if (highValue instanceof Integer)
            {
                objType = new Element("int");                
            }
            else if (highValue instanceof Long || highValue instanceof Date)
            {
                objType = new Element("long");
            }
            else if (highValue instanceof Float)
            {
                objType = new Element("float");                
            }
            else if (highValue instanceof Double)
            {
                objType = new Element("double");                
            }
            
            if(highValue instanceof Date)
                objType.addContent(String.valueOf(((Date)highValue).getTime()));
            else
                objType.addContent(highValue.toString());
            obj.addContent(objType);
            elements.add(obj);
        }
        return elements;
    }    

    public String getDbField() {
        return dbField;
    }

    public void setDbField(String dbField) {
        this.dbField = dbField;
    }

    public String getDtyp() {
        return dtyp;
    }

    public void setDtyp(String dtyp) {
        this.dtyp = dtyp;
    }

    public char getKind() {
        return kind;
    }

    public void setKind(char kind) {
        this.kind = kind;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public char isNoSelset() {
        return noSelset;
    }

    public void setNoSelset(char noSelset) {
        this.noSelset = noSelset;
    }

    public char isObligatory() {
        return obligatory;
    }

    public void setObligatory(char obligatory) {
        this.obligatory = obligatory;
    }

    public char getType() {
        return type;
    }

    public void setType(char type) {
        this.type = type;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public String getIncludeType() {
        return includeType;
    }

    public void setIncludeType(String includeType) {
        this.includeType = includeType;
    }

    public Object getHighValue() {
        return highValue;
    }

    public void setHighValue(Object highValue) {
        this.highValue = highValue;
    }

    public Object getLowValue() {
        return lowValue;
    }

    public void setLowValue(Object lowValue) {
        this.lowValue = lowValue;
    }

}
