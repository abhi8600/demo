/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.birst.dataconductor.sap;

import com.birst.dataconductor.EncryptionService;
import com.sap.conn.jco.ext.DestinationDataEventListener;
import com.sap.conn.jco.ext.DestinationDataProvider;
import java.util.Properties;

/**
 * Represents the destination to a specific SAP system.
 * The destination is maintained via a property file
 *
 * @author mpandit
 */
public class SAPDestinationDataProvider implements DestinationDataProvider
{
	private final Properties ABAP_AS_properties;
    private static EncryptionService es = EncryptionService.getInstance();

    public SAPDestinationDataProvider(SAPSystem system)
    {
		Properties properties = new Properties();
		properties.setProperty(DestinationDataProvider.JCO_ASHOST, system.getAsHost());
		properties.setProperty(DestinationDataProvider.JCO_SYSNR, system.getSystemNumber());
		properties.setProperty(DestinationDataProvider.JCO_CLIENT, system.getClient());
		properties.setProperty(DestinationDataProvider.JCO_USER, es.decrypt(system.getUser()));
		properties.setProperty(DestinationDataProvider.JCO_PASSWD, es.decrypt(system.getPassword()));
        ABAP_AS_properties = properties;
	}

    @Override
	public Properties getDestinationProperties(String system)
    {
		return ABAP_AS_properties;
	}

	@Override
	public void setDestinationDataEventListener(
			DestinationDataEventListener eventListener)
    {
	}

	@Override
	public boolean supportsEvents()
    {
		return false;
	}

}
