/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.birst.dataconductor.sap;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import org.apache.log4j.Logger;

/**
 *
 * @author mpandit
 */
public class SAPDSOField
{
    String name;
    char type;
    int offset;
    int length;
    private static SimpleDateFormat parseSDF = new SimpleDateFormat("yyyyMMdd");
    private static SimpleDateFormat formatSDF = new SimpleDateFormat("MM/dd/yyyy");
    private static final Logger logger = Logger.getLogger(SAPDSOField.class);

    public String formatValue(String fieldValue)
    {
        if (type == 'D')
        {
            try
            {
                return formatSDF.format(parseSDF.parse(fieldValue));
            }
            catch (ParseException pe)
            {
                logger.debug("Unable to parse Date value : " + fieldValue);
                return fieldValue;
            }
        }
        else
            return fieldValue;
    }
}
