/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.birst.dataconductor.sap;

import com.birst.dataconductor.XmlEncoderUtil;
import java.util.ArrayList;
import java.util.List;
import org.jdom.Element;

/**
 *
 * @author mpandit
 */
public class SAPRemoteFunction implements Cloneable {

    private String functionName;
    private String exportTableName;
    private List<SAPRemoteFunctionImportParameter> importParameters;
    private boolean hasReturnStructure;
    private List<String> exportTables;
    
    public Element getElement()
    {
        Element e = new Element("void");
        e.setAttribute("property", "SAPRemoteFunction");
        Element obj = new Element("object");
        obj.setAttribute("class", SAPRemoteFunction.class.getName());
        if (functionName != null)
        {
            obj.addContent(XmlEncoderUtil.getStringPropertyElement("functionName", functionName));
        }
        if (exportTableName != null)
        {
            obj.addContent(XmlEncoderUtil.getStringPropertyElement("exportTableName", exportTableName));
        }
        if (importParameters != null && !importParameters.isEmpty())
        {
            Element impParams = new Element("void");
            impParams.setAttribute("property", "importParameters");
            Element lstObj = new Element("object");
            lstObj.setAttribute("class", ArrayList.class.getName());
            for (SAPRemoteFunctionImportParameter param : importParameters)
            {
                Element met = new Element("void");
                met.setAttribute("method", "add");
                Element paramObj = new Element("object");
                paramObj.setAttribute("class", SAPRemoteFunctionImportParameter.class.getName());
                List<Element> children = param.getElements();
                if (children != null && !children.isEmpty())
                    paramObj.addContent(children);
                met.addContent(paramObj);
                lstObj.addContent(met);
            }
            impParams.addContent(lstObj);
            obj.addContent(impParams);
        }
        if (hasReturnStructure)
        {
            obj.addContent(XmlEncoderUtil.getBooleanPropertyElement("hasReturnStructure"));
        }
        if (exportTables != null && !exportTables.isEmpty())
        {
            Element exTabs = new Element("void");
            exTabs.setAttribute("property", "exportTables");
            Element lstObj = new Element("object");
            lstObj.setAttribute("class", ArrayList.class.getName());
            for (String et : exportTables)
            {
                Element met = new Element("void");
                met.setAttribute("method", "add");
                Element str = new Element("string");
                str.addContent(et);
                met.addContent(str);
                lstObj.addContent(met);
            }
            exTabs.addContent(lstObj);
            obj.addContent(exTabs);
        }
        e.addContent(obj);
        return e;
    }

    public String getExportTableName() {
        return exportTableName;
    }

    public void setExportTableName(String exportTableName) {
        this.exportTableName = exportTableName;
    }

    public String getFunctionName() {
        return functionName;
    }

    public void setFunctionName(String functionName) {
        this.functionName = functionName;
    }

    public List<SAPRemoteFunctionImportParameter> getImportParameters() {
        return importParameters;
    }

    public void setImportParameters(List<SAPRemoteFunctionImportParameter> importParameters) {
        this.importParameters = importParameters;
    }

    public boolean isHasReturnStructure() {
        return hasReturnStructure;
    }

    public void setHasReturnStructure(boolean hasReturnStructure) {
        this.hasReturnStructure = hasReturnStructure;
    }

    public List<String> getExportTables() {
        return exportTables;
    }

    public void setExportTables(List<String> exportTables) {
        this.exportTables = exportTables;
    }

    @Override
	public Object clone()
    {
        try
        {
            SAPRemoteFunction newObj = (SAPRemoteFunction)super.clone();
            List<SAPRemoteFunctionImportParameter> importParams = null;
            if (newObj.getImportParameters() != null)
            {
                importParams = new ArrayList<SAPRemoteFunctionImportParameter>();
                importParams.addAll(newObj.getImportParameters());
            }
            newObj.setImportParameters(importParams);
            List<String> exportTablesLst = new ArrayList<String>();
            exportTablesLst.addAll(newObj.getExportTables());
            newObj.setExportTables(exportTablesLst);
			return newObj;
		}
        catch (CloneNotSupportedException e)
        {
			e.printStackTrace();
		}
		return null;
    }

}