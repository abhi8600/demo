/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * SAPRemoteFunctionDetails.java
 *
 * Created on Jan 3, 2011, 11:05:04 AM
 */

package com.birst.dataconductor.sap;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.ParseException;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author mpandit
 */
public class SAPRemoteFunctionDetails extends javax.swing.JDialog {
    java.awt.Frame parent;
    private SAPRemoteFunction srf;
    private String sapSourceName;

    /** Creates new form SAPRemoteFunctionDetails */
    public SAPRemoteFunctionDetails(java.awt.Frame parent, boolean modal, SAPRemoteFunction srf, String sapSourceName) {
        super(parent, modal);
        this.parent = parent;
        this.srf = srf;
        this.sapSourceName = sapSourceName;
        initComponents();
    }

    private void initComponents()
    {
        int y = 0;
        this.getContentPane().setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();

        JPanel topPanel = new JPanel();
        c.fill = GridBagConstraints.HORIZONTAL;
        c.weightx = 0.0;
        c.gridwidth = 3;
        c.gridx = 0;
        c.gridy = y++;
        this.getContentPane().add(topPanel, c);
        topPanel.add(new JLabel(sapSourceName));

        if (srf.getImportParameters() != null)
        {
            for (SAPRemoteFunctionImportParameter parameter : srf.getImportParameters())
            {
                String paramName = parameter.getName();
                JLabel paramNameLabel = new JLabel(paramName + ": ");
                paramNameLabel.setToolTipText(parameter.getDescription());
                c.fill = GridBagConstraints.HORIZONTAL;
                c.weightx = 0.0;
                c.gridx = 0;
                c.gridwidth = parameter.isOptional() ? 2 : 1;
                c.gridy = y;
                c.insets = new Insets(10,10,0,10);
                this.getContentPane().add(paramNameLabel, c);

                if (!parameter.isOptional()) // mandatory parameter
                {
                    JLabel mandatory = new JLabel("*");
                    c.fill = GridBagConstraints.HORIZONTAL;
                    c.weightx = 0.0;
                    c.gridx = 1;
                    c.gridy = y;
                    c.insets = new Insets(10,10,0,5);
                    mandatory.setToolTipText(parameter.getDescription());
                    this.getContentPane().add(mandatory, c);
                }

                String paramValue = parameter.getStringValueForValueObject();
                JTextField paramValueTextField = new JTextField(paramValue);
                paramValueTextField.setName(paramName);
                paramValueTextField.setPreferredSize(new Dimension(150, 20));
                paramValueTextField.setToolTipText(parameter.getDescription());
                c.fill = GridBagConstraints.HORIZONTAL;
                c.weightx = 0.0;
                c.gridx = 2;
                c.gridy = y;
                c.insets = new Insets(10,0,0,10);
                this.getContentPane().add(paramValueTextField, c);
                y++;
            }
        }

        JPanel exportTablesPanel = new JPanel();
        exportTablesPanel.setLayout(new BorderLayout());
        exportTablesPanel.add(new JLabel("ExportTable to be used : "), BorderLayout.LINE_START);
        JComboBox exportTablesComboBox = new JComboBox(srf.getExportTables().toArray());
        if (srf.getExportTableName() != null)
            exportTablesComboBox.setSelectedItem(srf.getExportTableName());
        exportTablesComboBox.setPreferredSize(new Dimension(150, 20));
        exportTablesPanel.add(exportTablesComboBox, BorderLayout.LINE_END);
        c.fill = GridBagConstraints.HORIZONTAL;
        c.weightx = 0.0;
        c.gridwidth = 3;
        c.gridx = 0;
        c.gridy = y++;
        c.insets = new Insets(10,10,0,10);
        this.getContentPane().add(exportTablesPanel, c);

        JPanel bottomPanel = new JPanel();
        c.fill = GridBagConstraints.HORIZONTAL;
        c.weightx = 0.0;
        c.gridwidth = 3;
        c.gridx = 0;
        c.gridy = y++;
        this.getContentPane().add(bottomPanel, c);
        JButton okButton = new JButton("OK");
        okButton.addActionListener(new OkButtonActionListener());
        bottomPanel.add(okButton);

        this.setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        this.setName("SAPRemoteFunctionDetails");
        this.setTitle("SAPRemoteFunctionDetails");
        this.setLocationRelativeTo(parent);
        pack();        
    }

    public class OkButtonActionListener implements ActionListener
    {
        @Override
        public void actionPerformed(ActionEvent e)
        {
            if (srf.getImportParameters() != null)
            {
                for (SAPRemoteFunctionImportParameter parameter : srf.getImportParameters())
                {
                    String paramName = parameter.getName();
                    for (Component c : getContentPane().getComponents())
                    {
                        if (c.getName() != null && c.getName().equals(paramName))
                        {
                            String paramValue = ((JTextField)c).getText();
                            // if value not specified, textfield.getText() returns emptystring ("")                            
                            if (paramValue.trim().isEmpty())
                                paramValue = null;
                            boolean isValueSet = false;
                            String message = null;
                            try
                            {
                                parameter.setValueObjectFromStrValue(paramValue);
                                isValueSet = true;
                            }
                            catch (ParseException pe)
                            {
                                message =  "Cannot parse date, enter date in 'MM/dd/yyyy' format.";
                            }                            
                            if (!isValueSet)
                            {
                                JOptionPane.showMessageDialog(parent, message);
                                return;
                            }                            
                        }
                    }
                }
            }

            JPanel exportTablesPanel = (JPanel)(getContentPane().getComponent(getContentPane().getComponentCount() - 2));
            JComboBox exportTablesComboBox = (JComboBox)(exportTablesPanel.getComponent(1));
            srf.setExportTableName(exportTablesComboBox.getSelectedItem().toString());

            dispose();
        }
    }

    /**
    * @param args the command line arguments
    */
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                SAPRemoteFunctionDetails dialog = new SAPRemoteFunctionDetails(new javax.swing.JFrame(), true, null, null);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

}
