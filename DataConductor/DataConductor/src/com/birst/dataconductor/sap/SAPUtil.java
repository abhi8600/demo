/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.birst.dataconductor.sap;

import com.birst.dataconductor.GraphicsObject;
import com.birst.dataconductor.QuerySource;
import com.sap.conn.jco.JCoException;
import com.sap.conn.jco.JCoFunction;
import com.sap.conn.jco.JCoMetaData;
import com.sap.conn.jco.JCoRecordMetaData;
import com.sap.conn.jco.JCoTable;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author mpandit
 */
public class SAPUtil {

    private final static Logger logger = Logger.getLogger(SAPUtil.class);
    public static final String UIDateFormatForSAP = "MM/dd/yyyy";
    public static final String SourceFileDateFormatForSAP = "M/d/yyyy";
    public static final String SourceFileDateTimeFormatForSAP = "M/d/yyyy h:mm:ss a";

    /**
     * fetch the records in an SAP object and save them to a file for uploading
     * @param directory
     * @param qs
     * @return File
     * @throws java.io.IOException
     * @throws com.sap.conn.JCoException
     */
    public static File getObject(String directory, QuerySource qs, GraphicsObject go)
            throws IOException, SAPJCoException
    {
        StringBuilder message = new StringBuilder();
        try
        {
            File f = new File(directory + File.separator + qs.getSAPSourceName() + ".txt");
            
            SAPConnection sapConn = new SAPConnection(qs.getSapSystem());
            if (sapConn == null)
            {
                message.append("Cannot Connect to SAP System.");
                logger.error(message.toString());
                throw new JCoException(JCoException.JCO_ERROR_COMMUNICATION, message.toString());
            }
            JCoTable jcoTable = SAPUtil.execute(sapConn, qs.getSAPRemoteFunction());

            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(f), "UTF-8"));
            int numColumns = -1;
            // Write headers
            JCoRecordMetaData rmd = jcoTable.getRecordMetaData();
            numColumns = rmd.getFieldCount();
            int columnTypes[] = new int[numColumns];
            for (int i = 0; i < numColumns; i++)
            {
                columnTypes[i] = rmd.getType(i);
                if (i > 0)
                {
                    writer.write('|');
                }
                writer.write(rmd.getDescription(i));
            }
            writer.write("\r\n");

            // Write data
            SimpleDateFormat sdfDate = new SimpleDateFormat(SourceFileDateFormatForSAP);
            SimpleDateFormat sdfDateTime = new SimpleDateFormat(SourceFileDateTimeFormatForSAP);
            long rowCount = 0;
            for (int i=0; i<jcoTable.getNumRows(); i++,jcoTable.nextRow())
            {
                for (int j = 0; j < numColumns; j++)
                {
                    if (j > 0)
                    {
                        writer.write('|');
                    }
                    Object o = jcoTable.getValue(j);
                    if (o != null)
                    {
                        switch (columnTypes[j])
                        {
                            case JCoMetaData.TYPE_DATE:
                                    writer.write(sdfDate.format((Date) o));
                                    break;
                            case JCoMetaData.TYPE_TIME:
                                    writer.write(sdfDateTime.format((Date) o));
                                    break;
                            default:
                                    writer.write(o.toString());
                        }
                    }
                }
                writer.write("\r\n");
                if (rowCount > 0 && rowCount % 10000 == 0)
                {
                    go.setStatusMessage("Extracting Object: " + qs.getSAPSourceName() + " (" + rowCount + ")");
                }
                rowCount++;
            }
            writer.close();
            go.setStatusMessage("Extracted Object: " + qs.getSAPSourceName() + " (" + rowCount + ")");

            return f;
        }
        catch (JCoException ex)
        {
            message.append(ex.getMessage());
            logger.error(message.toString());
            throw new SAPJCoException(ex.getMessage(), ex.getCause());
        }
    }

    public static JCoTable execute(SAPConnection sapConn, SAPRemoteFunction rfm) throws JCoException
    {
        JCoFunction func = sapConn.getRepository().getFunction(rfm.getFunctionName());
        if (func == null)
        {
            throw new JCoException(JCoException.JCO_ERROR_FUNCTION_NOT_FOUND, "Cannot find remotefunction " + rfm.getFunctionName());
        }
        if (rfm.getImportParameters() != null)
        {
            for (SAPRemoteFunctionImportParameter parameter : rfm.getImportParameters())
            {
                func.getImportParameterList().setValue(parameter.getName(), parameter.getValueObject());
            }
        }
        sapConn.execute(func);
        if (func.getExceptionList() != null && func.getExceptionList().length > 0)
        {
            throw new JCoException(JCoException.JCO_ERROR_ABAP_EXCEPTION, "Exception(s) in executing JCoFunction, cannot retrive SAP Object.");
        }
        else if (rfm.isHasReturnStructure())
        {
            String type = func.getExportParameterList().getStructure("RETURN").getString("TYPE");
            if (type != null && !type.isEmpty() && !(type.equals("S") || type.equals(" ")))
            {
                String returnMsg = null;
                returnMsg = func.getExportParameterList().getStructure("RETURN").getString("MESSAGE");
                if (returnMsg != null && !returnMsg.isEmpty())
                    throw new JCoException(JCoException.JCO_ERROR_ABAP_EXCEPTION, returnMsg);
                else
                    throw new JCoException(JCoException.JCO_ERROR_ABAP_EXCEPTION, "Unknown error encountered executing BAPI function call.");
            }
        }
        return func.getTableParameterList().getTable(rfm.getExportTableName());
    }

    public static File getSAPQuery(String directory, QuerySource qs, GraphicsObject go)
            throws IOException, SAPJCoException
    {
        StringBuilder message = new StringBuilder();
        try
        {
            SAPConnection sapConn = new SAPConnection(qs.getSapSystem());
            if (sapConn == null)
            {
                message.append("Cannot Connect to SAP System.");
                logger.error(message.toString());
                throw new JCoException(JCoException.JCO_ERROR_COMMUNICATION, message.toString());
            }
            JCoFunction func = sapConn.getRepository().getFunction("RSAQ_REMOTE_QUERY_CALL");
            if (func == null)
            {
                message.append("Cannot find remotefunction RSAQ_REMOTE_QUERY_CALL.");
                logger.error(message.toString());
                throw new JCoException(JCoException.JCO_ERROR_FUNCTION_NOT_FOUND, message.toString());
            }
            SAPRemoteQuery srq = qs.getSAPRemoteQuery();
            func.getImportParameterList().setValue("USERGROUP", srq.getUsergroup());
            func.getImportParameterList().setValue("QUERY", srq.getName());
            func.getImportParameterList().setValue("DBACC", srq.getNumRows());
            String workspace = srq.getWorkspace();
            if (workspace == null || workspace.trim().isEmpty())
            {
                workspace = " ";
            }            
            func.getImportParameterList().setValue("WORKSPACE", workspace);
            func.getImportParameterList().setValue("DATA_TO_MEMORY", "X");
            func.getImportParameterList().setValue("SKIP_SELSCREEN", "X");
            func.getImportParameterList().setValue("EXTERNAL_PRESENTATION", "X");
            func.getImportParameterList().setValue("VARIANT", srq.getVariant());
            JCoTable selTable = func.getTableParameterList().getTable("SELECTION_TABLE");
            if (srq.getSelectionParameters() != null)
            {
                for (SAPRemoteQuerySelectionParameter param : srq.getSelectionParameters())
                {
                    if (param.getLowValue() != null || param.getHighValue() != null)
                    {
                        selTable.appendRow();
                        selTable.setValue("SELNAME", param.getName());
                        selTable.setValue("KIND", param.getKind());
                        selTable.setValue("OPTION", param.getOperator());
                        selTable.setValue("LOW", param.getLowValue());
                        selTable.setValue("HIGH", param.getHighValue());
                        selTable.setValue("SIGN", param.getIncludeType());
                    }
                }
            }
            sapConn.execute(func);
            JCoTable descTable = func.getTableParameterList().getTable("LISTDESC");
            File f = new File(directory + File.separator + qs.getSAPSourceName() + ".txt");
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(f), "UTF-8"));
            int numColumns = descTable.getNumRows();
            // Write headers
            JCoRecordMetaData rmd = descTable.getRecordMetaData();
            for (int i = 0; i < numColumns; i++, descTable.nextRow())
            {
                if (i > 0)
                {
                    writer.write('|');
                }
                writer.write(descTable.getString("FNAMENEW"));
            }
            writer.write("\r\n");
            JCoTable dataTable = func.getTableParameterList().getTable("LDATA");
            int numRows = dataTable.getNumRows();
            long rowCount = 0;
            String line = null;
            int leftOverChars = 0;
            for (int r = 0; r < numRows; r++, dataTable.nextRow())
            {
                line = dataTable.getString("LINE");
                while (line.length() > 0)
                {
                    if (line.equals("/") && r == numRows -1)
                        break;
                    List<Integer> hasLeftOver = new ArrayList<Integer>();
                    if (leftOverChars > 0)
                    {
                        writer.write(line.substring(0, leftOverChars));
                        line = line.substring(leftOverChars);
                    }
                    else
                    {
                        line = writeCellValue(line, writer, hasLeftOver);
                    }
                    if (hasLeftOver.size() > 0)
                    {
                        leftOverChars = hasLeftOver.get(0);
                    }
                    else
                    {
                        leftOverChars = 0;
                    }
                    if (line.length() > 0)
                    {
                        if (line.startsWith(","))
                        {
                            writer.write("|");
                            line = line.substring(1);
                        }
                        else if (line.startsWith(";"))
                        {
                            writer.write("\r\n");
                            rowCount++;
                            line = line.substring(1);
                        }
                    }
                }
            }
            writer.close();
            logger.debug("Extracted Query: " + qs.getSAPRemoteQuery().getName() + " (" + rowCount + ")");
            return f;
        }
        catch (JCoException ex)
        {
            message.append(ex.getMessage());
            logger.error(message.toString());
            throw new SAPJCoException(ex.getMessage(), ex.getCause());
        }        
    }

    private static String writeCellValue(String line, BufferedWriter writer, List<Integer> hasLeftOver)
            throws IOException
    {
        int cellEndIndex = line.indexOf(":");
        if (cellEndIndex >= 0)
        {
            int cellLength = Integer.parseInt(line.substring(0, cellEndIndex));
            if (cellLength > 0)
            {
                if (cellLength > line.substring(cellEndIndex + 1).length())
                {
                    hasLeftOver.add(cellLength - line.substring(cellEndIndex + 1).length());
                    writer.write(line.substring(cellEndIndex + 1));
                    line = "";
                }
                else
                {
                    writer.write(line.substring(cellEndIndex + 1, cellEndIndex + cellLength + 1));
                    line = line.substring(cellEndIndex + cellLength + 1);
                }
            }
            else
            {
                line = line.substring(cellEndIndex + 1);
            }
        }
        return line;
    }

    public static File getDSO(String directory, QuerySource qs, GraphicsObject go)
            throws IOException, SAPJCoException
    {
        StringBuilder message = new StringBuilder();
        try
        {
            SAPConnection sapConn = new SAPConnection(qs.getSapSystem());
            if (sapConn == null)
            {
                message.append("Cannot Connect to SAP System.");
                logger.error(message.toString());
                throw new JCoException(JCoException.JCO_ERROR_COMMUNICATION, message.toString());
            }
            JCoFunction func = sapConn.getRepository().getFunction("BAPI_ODSO_READ_DATA_UC");
            if (func == null)
            {
                message.append("Cannot find remotefunction BAPI_ODSO_READ_DATA_UC.");
                logger.error(message.toString());
                throw new JCoException(JCoException.JCO_ERROR_FUNCTION_NOT_FOUND, message.toString());
            }
            
            func.getImportParameterList().setValue("ODSOBJECT", qs.getSAPDSOName());
            func.getImportParameterList().setValue("SELECTALLINFOOBJECTS", "X");
            sapConn.execute(func);
            JCoTable descTable = func.getTableParameterList().getTable("DATALAYOUT");
            File f = new File(directory + File.separator + qs.getSAPDSOName() + ".txt");
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(f), "UTF-8"));
            int numColumns = descTable.getNumRows();
            List<SAPDSOField> fields = new ArrayList<SAPDSOField>();
            // Write headers
            for (int i = 0; i < numColumns; i++, descTable.nextRow())
            {
                SAPDSOField field = new SAPDSOField();
                field.name = descTable.getString("INFOOBJECT");
                field.type = descTable.getChar("TYPE");
                field.offset = descTable.getInt("OFFSET");
                field.length = descTable.getInt("LENGTH");
                fields.add(field);
                if (i > 0)
                {
                    writer.write('|');
                }
                writer.write(field.name);
            }
            writer.write("\r\n");
            JCoTable dataTable = func.getTableParameterList().getTable("RESULTDATA");
            int numRows = dataTable.getNumRows();
            long rowCount = 0;
            String line = null;
            int fieldIndex = 0;
            int dataAppended = 0;
            String fieldValue = "";
            for (int r = 0; r < numRows; r++, dataTable.nextRow())
            {
                String cont = dataTable.getString("CONTINUATION");
                boolean newLine = !cont.equals("X");
                if (newLine && fieldIndex > 0)
                {
                    dataAppended = 0;
                    fieldValue = "";
                    fieldIndex = 0;
                    writer.write("\r\n");
                    rowCount++;
                }
                //int available = Integer.parseInt(dataTable.getString("USABLE"));
                line = dataTable.getString("DATA");
                int available = line.length();
                int offset = 0;
                while (available > 0)
                {
                    SAPDSOField field = fields.get(fieldIndex);
                    int len = field.length - dataAppended;
                    boolean incompleteField = false;
                    if (len > available)
                    {
                        len = available;
                        incompleteField = true;
                    }
                    //writer.write(line.substring(offset, offset + len));
                    fieldValue += line.substring(offset, offset + len);
                    offset += len;
                    if (len <= available)
                    {
                        if (!incompleteField)
                        {
                            dataAppended = 0;
                            writer.write(field.formatValue(fieldValue).trim());
                            fieldValue = "";
                            fieldIndex++;
                            if (fieldIndex == fields.size())
                            {
                                fieldIndex = 0;
                                writer.write("\r\n");
                                rowCount++;
                            }
                            else
                            {
                                writer.write("|");
                            }
                        }                        
                        else
                        {
                            dataAppended += available;
                        }
                    }
                    available -= len;
                }
            }
            writer.close();
            logger.debug("Extracted DSO: " + qs.getSAPDSOName() + " (" + rowCount + ")");
            return f;
        }
        catch (JCoException ex)
        {
            message.append(ex.getMessage());
            logger.error(message.toString());
            throw new SAPJCoException(ex.getMessage(), ex.getCause());
        }
    }
}
