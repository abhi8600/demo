/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.birst.dataconductor.sap;

import com.birst.dataconductor.XmlEncoderUtil;
import java.util.ArrayList;
import java.util.List;
import org.jdom.Element;

/**
 *
 * @author mpandit
 */
public class SAPRemoteQuery implements Cloneable {
    private String name;
    private String workspace;
    private String usergroup;
    private String reportname;
    private int numRows;
    private String variant;
    private List<SAPRemoteQuerySelectionParameter> selectionParameters;
    
    public Element getElement()
    {
        Element e = new Element("void");
        e.setAttribute("property", "SAPRemoteQuery");
        Element obj = new Element("object");
        obj.setAttribute("class", SAPRemoteQuery.class.getName());
        if (name != null)
        {
            obj.addContent(XmlEncoderUtil.getStringPropertyElement("name", name));
        }
        if (usergroup != null)
        {
            obj.addContent(XmlEncoderUtil.getStringPropertyElement("usergroup", usergroup));
        }
        if (reportname != null)
        {
            obj.addContent(XmlEncoderUtil.getStringPropertyElement("reportname", reportname));
        }
        if (numRows != 0)
        {
            obj.addContent(XmlEncoderUtil.getIntPropertyElement("numRows", numRows));
        }
        if (variant != null)
        {
            obj.addContent(XmlEncoderUtil.getStringPropertyElement("variant", variant));
        }
        if (workspace != null)
        {
            obj.addContent(XmlEncoderUtil.getStringPropertyElement("workspace", workspace));
        }
        if (selectionParameters != null && !selectionParameters.isEmpty())
        {
            Element impParams = new Element("void");
            impParams.setAttribute("property", "selectionParameters");
            Element lstObj = new Element("object");
            lstObj.setAttribute("class", ArrayList.class.getName());
            for (SAPRemoteQuerySelectionParameter param : selectionParameters)
            {
                Element met = new Element("void");
                met.setAttribute("method", "add");
                Element paramObj = new Element("object");
                paramObj.setAttribute("class", SAPRemoteQuerySelectionParameter.class.getName());
                List<Element> children = param.getElements();
                if (children != null && !children.isEmpty())
                    paramObj.addContent(children);
                met.addContent(paramObj);
                lstObj.addContent(met);
            }
            impParams.addContent(lstObj);
            obj.addContent(impParams);
        }
        e.addContent(obj);
        return e;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public String getWorkspace() {
        return workspace;
    }

    public void setWorkspace(String workspace) {
        this.workspace = workspace;
    }

    public String getUsergroup() {
        return usergroup;
    }

    public void setUsergroup(String usergroup) {
        this.usergroup = usergroup;
    }

    public String getReportname() {
        return reportname;
    }

    public void setReportname(String reportname) {
        this.reportname = reportname;
    }

    public int getNumRows() {
        return numRows;
    }

    public void setNumRows(int numRows) {
        this.numRows = numRows;
    }

    public String getVariant() {
        return variant;
    }

    public void setVariant(String variant) {
        this.variant = variant;
    }

    public List<SAPRemoteQuerySelectionParameter> getSelectionParameters() {
        return selectionParameters;
    }

    public void setSelectionParameters(List<SAPRemoteQuerySelectionParameter> selectionParameters) {
        this.selectionParameters = selectionParameters;
    }

    @Override
	public Object clone()
    {
        try
        {
            SAPRemoteQuery newObj = (SAPRemoteQuery)super.clone();
            List<SAPRemoteQuerySelectionParameter> selectionParams = new ArrayList<SAPRemoteQuerySelectionParameter>();
            selectionParams.addAll(newObj.getSelectionParameters());
            newObj.setSelectionParameters(selectionParams);
            return newObj;
		}
        catch (CloneNotSupportedException e)
        {
			e.printStackTrace();
		}
		return null;
    }
}
