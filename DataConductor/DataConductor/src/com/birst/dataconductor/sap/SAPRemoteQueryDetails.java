/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.birst.dataconductor.sap;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author mpandit
 */
public class SAPRemoteQueryDetails extends javax.swing.JDialog {
    java.awt.Frame parent;
    private SAPRemoteQuery srq;
    private String sapSourceName;

    /** Creates new form SAPRemoteQueryDetails */
    public SAPRemoteQueryDetails(java.awt.Frame parent, boolean modal, SAPRemoteQuery srq, String sapSourceName) {
        super(parent, modal);
        this.parent = parent;
        this.srq = srq;
        this.sapSourceName = sapSourceName;
        initComponents();
    }

    private static Map<String, String> operatorMap = new HashMap<String, String>();
    private static List<String> operatorList = new ArrayList<String>();
    static {
        operatorMap.put("EQ", "=");
        operatorMap.put("GE", ">=");
        operatorMap.put("GT", ">");
        operatorMap.put("LT", "<");
        operatorMap.put("LE", "<=");
        operatorMap.put("NE", "<>");
        operatorList.add("EQ");
        operatorList.add("GE");
        operatorList.add("GT");
        operatorList.add("LT");
        operatorList.add("LE");
        operatorList.add("NE");
    }

    private static Map<String, String> includeTypeMap = new HashMap<String, String>();
    static {
        includeTypeMap.put("I", "Include");
        includeTypeMap.put("E", "Exclude");
    }

    private void initComponents()
    {
        int y = 0;
        this.getContentPane().setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();

        JPanel topPanel = new JPanel();
        c.fill = GridBagConstraints.HORIZONTAL;
        c.insets = new Insets(10,10,10,10);
        c.weightx = 0.0;
        c.gridwidth = 5;
        c.gridx = 0;
        c.gridy = y++;
        this.getContentPane().add(topPanel, c);
        JLabel queryName = new JLabel(sapSourceName);
        queryName.setFont(new Font(queryName.getFont().getName(), queryName.getFont().getStyle(), queryName.getFont().getSize()+2));
        topPanel.add(queryName);

        c.fill = GridBagConstraints.HORIZONTAL;
        c.weightx = 0.0;
        c.gridx = 0;
        c.gridwidth = 1;
        c.gridy = y;
        c.insets = new Insets(10,10,0,0);
        JLabel lbl = new JLabel("Variant: ");
        lbl.setFont(new Font(lbl.getFont().getName(), Font.BOLD, lbl.getFont().getSize()));
        this.getContentPane().add(lbl, c);

        c.fill = GridBagConstraints.HORIZONTAL;
        c.weightx = 0.0;
        c.gridx = 1;
        c.gridy = y;
        c.insets = new Insets(10,0,0,5);
        JTextField variant = new JTextField(srq.getVariant() == null ? "" : srq.getVariant());
        variant.setName("variant");
        this.getContentPane().add(variant, c);
        

        c.fill = GridBagConstraints.HORIZONTAL;
        c.weightx = 0.0;
        c.gridx = 3;
        c.gridy = y;
        c.insets = new Insets(10,0,0,5);
        lbl = new JLabel("NumRows: ");
        lbl.setFont(new Font(lbl.getFont().getName(), Font.BOLD, lbl.getFont().getSize()));
        this.getContentPane().add(lbl, c);

        c.fill = GridBagConstraints.HORIZONTAL;
        c.weightx = 0.0;
        c.gridx = 4;
        c.gridy = y;
        c.insets = new Insets(10,0,0,10);
        JTextField numRows = new JTextField(srq.getNumRows() == Integer.MAX_VALUE ? "ALL" : String.valueOf(srq.getNumRows()));
        numRows.setName("numrows");
        this.getContentPane().add(numRows, c);

        y++;
        
        if (srq.getSelectionParameters() != null && srq.getSelectionParameters().size() > 0)
        {
            JLabel label = new JLabel("Parameter Name");
            label.setFont(new Font(label.getFont().getName(), Font.BOLD, label.getFont().getSize()));
            c.fill = GridBagConstraints.HORIZONTAL;
            c.weightx = 0.0;
            c.gridx = 0;
            c.gridwidth = 1;
            c.gridy = y;
            c.insets = new Insets(10,10,0,0);
            this.getContentPane().add(label, c);

            c.fill = GridBagConstraints.HORIZONTAL;
            c.weightx = 0.0;
            c.gridx = 1;
            c.gridy = y;
            c.insets = new Insets(10,0,0,5);
            label = new JLabel("Operator");
            label.setFont(new Font(label.getFont().getName(), Font.BOLD, label.getFont().getSize()));
            this.getContentPane().add(label, c);

            c.fill = GridBagConstraints.HORIZONTAL;
            c.weightx = 0.0;
            c.gridx = 2;
            c.gridy = y;
            c.insets = new Insets(10,0,0,5);
            label = new JLabel("LowValue");
            label.setFont(new Font(label.getFont().getName(), Font.BOLD, label.getFont().getSize()));
            this.getContentPane().add(label, c);

            c.fill = GridBagConstraints.HORIZONTAL;
            c.weightx = 0.0;
            c.gridx = 3;
            c.gridy = y;
            c.insets = new Insets(10,0,0,5);
            label = new JLabel("HighValue");
            label.setFont(new Font(label.getFont().getName(), Font.BOLD, label.getFont().getSize()));
            this.getContentPane().add(label, c);

            c.fill = GridBagConstraints.HORIZONTAL;
            c.weightx = 0.0;
            c.gridx = 4;
            c.gridy = y;
            c.insets = new Insets(10,0,0,10);
            label = new JLabel("InclusionType");
            label.setFont(new Font(label.getFont().getName(), Font.BOLD, label.getFont().getSize()));
            this.getContentPane().add(label, c);

            y++;

            for (SAPRemoteQuerySelectionParameter parameter : srq.getSelectionParameters())
            {
                String paramName = parameter.getDbField() + "(" + parameter.getName() + ")";
                JLabel paramNameLabel = new JLabel(paramName + ": ");
                c.fill = GridBagConstraints.HORIZONTAL;
                c.weightx = 0.0;
                c.gridx = 0;
                c.gridwidth = 1;
                c.gridy = y;
                c.insets = new Insets(10,10,0,0);
                paramNameLabel.setName(parameter.getName());
                this.getContentPane().add(paramNameLabel, c);

                c.fill = GridBagConstraints.HORIZONTAL;
                c.weightx = 0.0;
                c.gridx = 1;
                c.gridy = y;
                c.insets = new Insets(10,0,0,5);
                JComboBox operatorComboBox = new JComboBox(new String[] { "=", ">=", ">", "<", "<=", "<>" });
                if (parameter.getOperator() != null)
                    operatorComboBox.setSelectedItem(operatorMap.get(parameter.getOperator()));
                else
                    operatorComboBox.setSelectedItem("=");
                operatorComboBox.setName("operator_" + parameter.getName());
                this.getContentPane().add(operatorComboBox, c);

                c.fill = GridBagConstraints.HORIZONTAL;
                c.weightx = 0.0;
                c.gridx = 2;
                c.gridy = y;
                c.insets = new Insets(10,0,0,5);
                JTextField paramLowValue = new JTextField(parameter.getLowValue() == null ? "" : String.valueOf(parameter.getLowValue()));
                paramLowValue.setName("lowvalue_" + parameter.getName());
                this.getContentPane().add(paramLowValue, c);

                c.fill = GridBagConstraints.HORIZONTAL;
                c.weightx = 0.0;
                c.gridx = 3;
                c.gridy = y;
                c.insets = new Insets(10,0,0,5);
                JTextField paramHighValue = new JTextField(parameter.getHighValue() == null ? "" : String.valueOf(parameter.getHighValue()));
                paramHighValue.setName("highvalue_" + parameter.getName());
                this.getContentPane().add(paramHighValue, c);

                c.fill = GridBagConstraints.HORIZONTAL;
                c.weightx = 0.0;
                c.gridx = 4;
                c.gridy = y;
                c.insets = new Insets(10,0,0,10);
                JComboBox inclusionTypeComboBox = new JComboBox(new String[] { "Include", "Exclude"});
                inclusionTypeComboBox.setSelectedItem(includeTypeMap.containsKey(parameter.getIncludeType()) ? includeTypeMap.get(parameter.getIncludeType()) : "Include");
                inclusionTypeComboBox.setName("inclusiontype_" + parameter.getName());
                this.getContentPane().add(inclusionTypeComboBox, c);

                y++;
            }
        }

        JPanel bottomPanel = new JPanel();
        c.fill = GridBagConstraints.HORIZONTAL;
        c.weightx = 0.0;
        c.gridwidth = 5;
        c.gridx = 0;
        c.insets = new Insets(10,10,10,10);
        c.gridy = y++;
        this.getContentPane().add(bottomPanel, c);
        JButton okButton = new JButton("OK");
        okButton.addActionListener(new OkButtonActionListener());
        bottomPanel.add(okButton);

        this.setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        this.setName("SAPRemoteQueryDetails");
        this.setTitle("SAPRemoteQueryDetails");
        this.setLocationRelativeTo(parent);
        pack();
    }

    public class OkButtonActionListener implements ActionListener
    {
        @Override
        public void actionPerformed(ActionEvent e)
        {
            for (Component c : getContentPane().getComponents())
            {
                if (c.getName() == null)
                    continue;
                String value = null;
                if (c.getName() != null && c.getName().equals("variant"))
                {
                    value = ((JTextField)c).getText();
                    if (value == null || value.trim().isEmpty())
                        value = null;
                    srq.setVariant(value);
                }
                else if (c.getName() != null && c.getName().equals("numrows"))
                {
                    int numRows = 0;
                    value = ((JTextField)c).getText();
                    if (value == null || value.trim().isEmpty())
                        value = "ALL";
                    if (!value.equals("ALL"))
                    {
                        int i;
                        try
                        {
                            i = Integer.parseInt(value);
                            if (i < 0)
                                value = "ALL";
                            else
                                numRows = i;
                        }
                        catch (NumberFormatException nfe)
                        {
                            value = "ALL";
                        }
                    }
                    if (value.equals("ALL"))
                        numRows = Integer.MAX_VALUE;
                    srq.setNumRows(numRows);
                }
                else if (srq.getSelectionParameters() != null && srq.getSelectionParameters().size() > 0)
                {
                    for (SAPRemoteQuerySelectionParameter param : srq.getSelectionParameters())
                    {
                        if (c.getName().endsWith("_" + param.getName()))
                        {
                            if (c.getName().equals("operator_" + param.getName()))
                            {
                                param.setOperator(operatorList.get(((JComboBox)c).getSelectedIndex()));
                            }
                            else if (c.getName().equals("lowvalue_" + param.getName()))
                            {
                                value = ((JTextField)c).getText();
                                if (value == null || value.trim().isEmpty())
                                    value = null;
                                param.setLowValue(value);
                            }
                            else if (c.getName().equals("highvalue_" + param.getName()))
                            {
                                value = ((JTextField)c).getText();
                                if (value == null || value.trim().isEmpty())
                                    value = null;
                                param.setHighValue(value);
                            }
                            else if (c.getName().equals("inclusiontype_" + param.getName()))
                            {
                                int index = ((JComboBox)c).getSelectedIndex();
                                param.setIncludeType(index == 0 ? "I" : "E");
                            }
                        }
                    }
                }
            }
            // reset operators and inclusiontype if operands are not set
            if (srq.getSelectionParameters() != null && srq.getSelectionParameters().size() > 0)
            {
                for (SAPRemoteQuerySelectionParameter param : srq.getSelectionParameters())
                {
                    if (param.getLowValue() == null && param.getHighValue() == null)
                    {
                        param.setOperator(null);
                        param.setIncludeType("I");
                    }
                }
            }
            dispose();
        }
    }

    /**
    * @param args the command line arguments
    */
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                SAPRemoteQueryDetails dialog = new SAPRemoteQueryDetails(new javax.swing.JFrame(), true, null, null);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }
}
