/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.birst.dataconductor.sap;

import com.birst.dataconductor.EncryptionService;
import com.birst.dataconductor.XmlEncoderUtil;
import org.apache.log4j.Logger;
import org.jdom.Element;

/**
 * Represents a SAP System Immutable object
 *
 * @author mpandit
 *
 */
public class SAPSystem implements java.lang.Cloneable
{
    private static EncryptionService es = EncryptionService.getInstance();
    private static final String connectionName = "Birst_SAP_Server";
    private String asHost;
    private String client;
    private String systemNumber;
    private String user;
    private String password;
    private String language = "en"; // English will be used as login language
    private String routerhost;
    private String routerport;
    private String serverhost;
    private String servername;
    private final static Logger logger = Logger.getLogger(SAPSystem.class);
    
    public Element getElement()
    {
        Element e = new Element("void");
        e.setAttribute("property", "sapSystem");
        if(asHost != null)
        {
            e.addContent(XmlEncoderUtil.getStringPropertyElement("asHost", asHost));
        }
        if(client != null)
        {
            e.addContent(XmlEncoderUtil.getStringPropertyElement("client", client));
        }
        if(systemNumber != null)
        {
            e.addContent(XmlEncoderUtil.getStringPropertyElement("systemNumber", systemNumber));
        }
        if(user != null)
        {
            e.addContent(XmlEncoderUtil.getStringPropertyElement("user", user));
        }
        if(password != null)
        {
            e.addContent(XmlEncoderUtil.getStringPropertyElement("password", password));
        }
        if(routerhost != null)
        {
            e.addContent(XmlEncoderUtil.getStringPropertyElement("routerhost", routerhost));
        }
        if(routerport != null)
        {
            e.addContent(XmlEncoderUtil.getStringPropertyElement("routerport", routerport));
        }
        if(serverhost != null)
        {
            e.addContent(XmlEncoderUtil.getStringPropertyElement("serverhost", serverhost));
        }
        if(servername != null)
        {
            e.addContent(XmlEncoderUtil.getStringPropertyElement("servername", servername));
        }
        if(!language.equals("en"))
        {
            e.addContent(XmlEncoderUtil.getStringPropertyElement("language", language));
        }
        return e;
    }

    public SAPSystem() { }

    /**
     * Constructor, Login language is assumed to be English
     * @param routerhost
     * @param routerport
     * @param serverhost
     * @param servername
     * @param systemNumber
     * @param client
     * @param user
     * @param password	 
     */
    public SAPSystem(String routerhost, String routerport, String serverhost, String servername, String systemNumber,
            String client, String user, String password)
    {
        this.client = client;
        this.user = es.encrypt(user);
        this.password = es.encrypt(password);
        this.systemNumber = systemNumber;
        this.routerhost = routerhost;
        this.routerport = routerport;
        this.serverhost = serverhost;
        this.servername = servername;
        this.asHost = getASHostString(routerhost, routerport, serverhost, servername);
    }
    
    public String getClient()
    {
        return client;
    }

    public String getUser()
    {
        return user;
    }

    public String getPassword()
    {
        return password;
    }

    public String getLanguage()
    {
	return language;
    }

    public String getAsHost()
    {
	return asHost;
    }

    public String getSystemNumber()
    {
	return systemNumber;
    }
    
    public String getRouterhost() 
    {
        return routerhost;
    }

    public String getServerhost() 
    {
        return serverhost;
    }

    public String getServername() 
    {
        return servername;
    }

    @Override
    public String toString()
    {
	return "Client " + client + " User " + user + " PW " + password
			+ " Language " + language + " ASHost " + asHost + " SysID "
			+ systemNumber;
    }

    @Override
    public int hashCode()
    {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((client == null) ? 0 : client.hashCode());
	result = prime * result + ((asHost == null) ? 0 : asHost.hashCode());
	result = prime * result
			+ ((language == null) ? 0 : language.hashCode());
	result = prime * result + ((connectionName == null) ? 0 : connectionName.hashCode());
	result = prime * result
			+ ((password == null) ? 0 : password.hashCode());
	result = prime * result
			+ ((systemNumber == null) ? 0 : systemNumber.hashCode());
	result = prime * result + ((user == null) ? 0 : user.hashCode());
        result = prime * result + ((routerhost == null) ? 0 : routerhost.hashCode());
        result = prime * result + ((routerport == null) ? 0 : routerport.hashCode());
        result = prime * result + ((serverhost == null) ? 0 : serverhost.hashCode());
        result = prime * result + ((servername == null) ? 0 : servername.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj)
    {
	if (this == obj)
            return true;
	if (obj == null)
            return false;
	if (getClass() != obj.getClass())
            return false;
	SAPSystem other = (SAPSystem) obj;
	if (client == null)
        {
            if (other.client != null)
		return false;
	}
        else if (!client.equals(other.client))
            return false;
	if (asHost == null)
        {
            if (other.asHost != null)
		return false;
	}
        else if (!asHost.equals(other.asHost))
            return false;
	if (language == null)
        {
            if (other.language != null)
		return false;
	}
        else if (!language.equals(other.language))
            return false;
	if (connectionName == null)
        {
            if (other.connectionName != null)
		return false;
	}
        else if (!connectionName.equals(other.connectionName))
            return false;
	if (password == null)
        {
            if (other.password != null)
		return false;
	}
        else if (!password.equals(other.password))
            return false;
	if (systemNumber == null)
        {
            if (other.systemNumber != null)
		return false;
	}
        else if (!systemNumber.equals(other.systemNumber))
            return false;
	if (user == null)
        {
            if (other.user != null)
		return false;
	}
        else if (!user.equals(other.user))
            return false;
        if (routerhost == null)
        {
            if (other.routerhost != null)
		return false;
	}
        else if (!routerhost.equals(other.routerhost))
            return false;
        if (routerport == null)
        {
            if (other.routerport != null)
		return false;
	}
        else if (!routerport.equals(other.routerport))
            return false;
        if (serverhost == null)
        {
            if (other.serverhost != null)
		return false;
	}
        else if (!serverhost.equals(other.serverhost))
            return false;
        if (servername == null)
        {
            if (other.servername != null)
		return false;
	}
        else if (!servername.equals(other.servername))
            return false;
	return true;
    }

    @Override
    public Object clone()
    {
	try
        {
            return super.clone();
	} 
        catch (CloneNotSupportedException e)
        {
            e.printStackTrace();
	}
	return null;
    }

    public void setAsHost(String asHost) 
    {
        this.asHost = asHost;
    }

    public void setClient(String client) 
    {
        this.client = client;
    }
    
    public void setLanguage(String language) 
    {
        this.language = language;
    }

    public void setPassword(String password) 
    {
        this.password = password;
    }

    public void setRouterhost(String routerhost) 
    {
        this.routerhost = routerhost;
    }

    public void setRouterport(String routerport) 
    {
        this.routerport = routerport;
    }

    public void setServerhost(String serverhost) 
    {
        this.serverhost = serverhost;
    }

    public void setServername(String servername) 
    {
        this.servername = servername;
    }

    public void setSystemNumber(String systemNumber) 
    {
        this.systemNumber = systemNumber;
    }

    public void setUser(String user) 
    {
        this.user = user;
    }

    public String getRouterport() 
    {
        return routerport;
    }
    
    public static String getASHostString(String routerhost, String routerport, String serverhost, String servername)
    {
        StringBuilder asHostSB = new StringBuilder();
        if (routerhost != null && !routerhost.trim().isEmpty())
            asHostSB.append("/H/").append(routerhost);
        //look for routerport only if routerhost is found
        if (routerport != null && !routerport.trim().isEmpty() && asHostSB.length() > 0)
        {
            try
            {
                int port = Integer.parseInt(routerport);
                asHostSB.append("/S/").append(port);
            }
            catch (NumberFormatException ex) {
            	if (logger.isTraceEnabled()) {
                    logger.trace("Unable to parse routerport " + routerport + ". Port will not be used");
                }
            }
        }
        if (serverhost != null && !serverhost.trim().isEmpty())
        	asHostSB.append("/H/").append(serverhost).append("/H/");
        if (servername != null && !servername.trim().isEmpty())
        	asHostSB.append(servername);
        return asHostSB.toString();
    }
}
