/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.birst.dataconductor.sap;

import com.birst.dataconductor.ActivityLog;
import com.birst.dataconductor.DataConductorConfig;
import com.birst.dataconductor.DataConductorUtil;
import com.birst.dataconductor.QuerySource;
import com.birst.dataconductor.Util;
import com.sap.conn.jco.JCoRecordMetaData;
import com.sap.conn.jco.JCoTable;

/**
 *
 * @author mpandit
 */
public class SAPConnectionTester
{
    
	public static void main(String args[])
    {
        DataConductorUtil dcu = new DataConductorUtil(new ActivityLog());
        DataConductorConfig config = null;
        try
        {
            config = dcu.setConfig();
        }
        catch (Exception ex)
        {
            Util.logStackTrace(ex);
            Util.exitApp(-1);
        }
		SAPSystem system = new SAPSystem("smicolo2","3299","155.56.49.28","cpce801", "75", "850", "birst", "Bir$t00");
        try
        {
            String type = null;
            if (args.length > 0)
            {
                type = args[0];
            }
            if (type != null && type.equals("QUERY"))
            {
                QuerySource qs = new QuerySource();
                qs.setSAPSourceName("QUE_BIRST_02");
                qs.setType(QuerySource.TYPE_SAP_QUERY);
                qs.setSapSystem(system);
                SAPRemoteQuery srq = new SAPRemoteQuery();
                srq.setName("QUE_BIRST_02");
                srq.setUsergroup("ZTEST");
                qs.setSAPRemoteQuery(srq);
                SAPUtil.getSAPQuery("C:\\Users\\mpandit.AMDMPANDIT\\Desktop\\SAPQuery", qs, null);
            }
            else
            {
                SAPConnection sapConn = new SAPConnection(system);
                SAPRemoteFunction rfm = new SAPRemoteFunction();
                rfm.setFunctionName("BAPI_COMPANY_GETLIST");
                rfm.setExportTableName("COMPANY_LIST");
                rfm.setHasReturnStructure(true);
                JCoTable table = SAPUtil.execute(sapConn, rfm);

                JCoRecordMetaData rmd = table.getRecordMetaData();
                System.out.println("rmd.getFieldCount() = " + rmd.getFieldCount());
                System.out.println("rmd.getRecordLength() = " + rmd.getRecordLength());
                System.out.println();

                for (int i=0;i<rmd.getFieldCount();i++)
                {
                    System.out.println("Column " + (i+1) + " MetaData :");
                    System.out.println("Name=" + rmd.getName(i));
                    System.out.println("Description=" + rmd.getDescription(i));
                    System.out.println("Type=" + rmd.getType(i));
                    System.out.println("TypeAsString=" + rmd.getTypeAsString(i));
                    System.out.println("Length=" + rmd.getLength(i));
                    System.out.println("ClassNameOfField=" + rmd.getClassNameOfField(i));
                    System.out.println();
                }
                System.out.println();

                System.out.println("No. of Columns=>" + table.getNumColumns());
                System.out.println("No. of Rows=>" + table.getNumRows());
                System.out.println();

                for (int i=0;i<rmd.getFieldCount();i++)
                {
                    System.out.print(rmd.getName(i) + "\t");
                }
                System.out.println();
                for (int i=0; i<table.getNumRows(); i++,table.nextRow())
                {
                    for (int j=0;j<rmd.getFieldCount();j++)
                    {
                        System.out.print(table.getValue(j).toString() + "\t");
                    }
                    System.out.println();
                }
                System.out.println();
            }
            Util.exitApp(0);
        }
        catch (Exception e)
        {
            System.out.println(e);
            Util.exitApp(1);
        }
	}

}
