/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.birst.dataconductor.sap;

import com.sap.conn.jco.JCoContext;
import com.sap.conn.jco.JCoDestination;
import com.sap.conn.jco.JCoDestinationManager;
import com.sap.conn.jco.JCoException;
import com.sap.conn.jco.JCoFunction;
import com.sap.conn.jco.JCoRepository;
import com.sap.conn.jco.ext.Environment;

/**
 * Connection allows to get and execute SAP functions. The constructor expect a
 * SAPSystem and will save the connection data to a file. The connection will
 * also be automatically be established.
 *
 * @author mpandit
 */
public class SAPConnection
{
	static String BIRST_SAP_CONNECTION_POOL = "BIRST_SAP_CONNECTION_POOL";
    private static SAPDestinationDataProvider myProvider;
	private JCoRepository repos;
	private JCoDestination dest;
    
	public SAPConnection(SAPSystem system) throws JCoException
    {
        if (Environment.isDestinationDataProviderRegistered())
            Environment.unregisterDestinationDataProvider(myProvider);
		myProvider = new SAPDestinationDataProvider(system);
        Environment.registerDestinationDataProvider(myProvider);
		dest = JCoDestinationManager.getDestination(BIRST_SAP_CONNECTION_POOL);
        //System.out.println("Attributes:");
        //System.out.println(dest.getAttributes());
        repos = dest.getRepository();		         
	}

    /**
	 * Method execute will call a function. The Caller of this function has
	 * already set all required parameters of the function
	 *
	 */
	public void execute(JCoFunction function) throws JCoException
    {
		try
        {
			JCoContext.begin(dest);
			function.execute(dest);
		} 
        catch (JCoException e)
        {
			e.printStackTrace();
            throw e;
		} 
        finally
        {
			try
            {
				JCoContext.end(dest);
			} 
            catch (JCoException e)
            {
				e.printStackTrace();
                throw e;
			}
		}
	}

    public JCoRepository getRepository()
    {
        return repos;
    }

}