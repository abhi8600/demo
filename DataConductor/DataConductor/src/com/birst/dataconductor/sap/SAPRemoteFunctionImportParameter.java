/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.birst.dataconductor.sap;

import com.birst.dataconductor.XmlEncoderUtil;
import com.sap.conn.jco.JCoMetaData;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.jdom.Element;

/**
 *
 * @author mpandit
 */
public class SAPRemoteFunctionImportParameter implements Cloneable {
    private String name;
    private String description;
    private String defaultValue;
    private int length;
    private boolean optional;
    private int type;
    private Object valueObject;
    
    public List<Element> getElements()
    {
        List<Element> elements = new ArrayList<Element>();
        if (name != null)
        {
            elements.add(XmlEncoderUtil.getStringPropertyElement("name", name));
        }
        if (description != null)
        {
            elements.add(XmlEncoderUtil.getStringPropertyElement("description", description));
        }
        if (defaultValue != null)
        {
            elements.add(XmlEncoderUtil.getStringPropertyElement("defaultValue", defaultValue));
        }
        if (length != 0)
        {
            elements.add(XmlEncoderUtil.getIntPropertyElement("length", length));
        }
        if (optional)
        {
            elements.add(XmlEncoderUtil.getBooleanPropertyElement("optional"));
        }
        if (type != 0)
        {
            elements.add(XmlEncoderUtil.getIntPropertyElement("type", type));
        }
        if (valueObject != null)
        {
            Element obj = new Element("void");
            obj.setAttribute("property", "valueObject");
            Element objType = null;
            if (valueObject instanceof Date)
            {
                objType = new Element("long");
                objType.addContent(String.valueOf(((Date)valueObject).getTime()));                
            }
            else if (valueObject instanceof Long)
            {
                objType = new Element("long");
                objType.addContent(String.valueOf((Long)valueObject));
            }
            else
            {
                objType = new Element("string");
                objType.addContent(valueObject.toString());                
            }
            obj.addContent(objType);
            elements.add(obj);
        }
        return elements;
    }

    public Object getValueObject() {
        if (type == JCoMetaData.TYPE_DATE && valueObject != null && valueObject instanceof Long)
            return new Date((Long)valueObject);
        return valueObject;
    }

    public void setValueObject(Object valueObject) {
        this.valueObject = valueObject;
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isOptional() {
        return optional;
    }

    public void setOptional(boolean optional) {
        this.optional = optional;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public boolean setValueObjectFromStrValue(String value) throws ParseException {
        if (value != null)
        {
            if (type == JCoMetaData.TYPE_DATE)
            {
                SimpleDateFormat sdf = new SimpleDateFormat(SAPUtil.UIDateFormatForSAP);
                sdf.setLenient(false);
                this.valueObject = sdf.parse(value);
            }
            else
            {
                this.valueObject = value;
            }
        }
        else
            this.valueObject = null;
        return (this.valueObject != null);
    }

    public String getStringValueForValueObject() {
        String value = null;
        if (this.valueObject != null)
        {
            if (type == JCoMetaData.TYPE_DATE)
            {
                SimpleDateFormat sdf = new SimpleDateFormat(SAPUtil.UIDateFormatForSAP);
                sdf.setLenient(false);
                value = sdf.format(this.valueObject);
            }
            else
            {
                value = this.valueObject.toString();
            }
        }
        return value;
    }

}