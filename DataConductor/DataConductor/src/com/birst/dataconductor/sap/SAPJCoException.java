/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.birst.dataconductor.sap;

/**
 *
 * @author mpandit
 */
public class SAPJCoException extends Exception {
    public SAPJCoException(String message, Throwable cause)
    {
        super(message, cause);
    }
}
