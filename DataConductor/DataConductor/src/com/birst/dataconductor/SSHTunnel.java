/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.birst.dataconductor;
import com.jcraft.jsch.Session;
import org.jdom.Element;
/**
 *
 * @author cpatel
 */
public class SSHTunnel implements Cloneable
{
    private String publicKeyFile;
    private String tunnelExternalHost;
    private String externalPort;
    private String localPort;
    private String ec2User;
    private String tunnelInternalHost;
    
    private boolean isSSHTunnelConfigured;
    public transient Session session = null;
    private String tunnelInternalPort;
   
    /**
     * @return the publicKeyFile
     */
    
     public void closeSession() {
        if (session != null)
            session.disconnect();
    }
     
    public String getPublicKeyFile() {
        return publicKeyFile;
    }

    /**
     * @param publicKeyFile the publicKeyFile to set
     */
    public void setPublicKeyFile(String publicKeyFile) {
        this.publicKeyFile = publicKeyFile;
    }

    /**
     * @return the tunnelExternalHost
     */
    public String getTunnelExternalHost() {
        return tunnelExternalHost;
    }

    /**
     * @param tunnelExternalHost the tunnelExternalHost to set
     */
    public void setTunnelExternalHost(String tunnelExternalHost) {
        this.tunnelExternalHost = tunnelExternalHost;
    }

    /**
     * @return the ec2User
     */
    public String getEc2User() {
        return ec2User;
    }

    /**
     * @param ec2User the ec2User to set
     */
    public void setEc2User(String ec2User) {
        this.ec2User = ec2User;
    }

    /**
     * @return the tunnelInternalHost
     */
    public String getTunnelInternalHost() {
        return tunnelInternalHost;
    }

     /**
     * @return the isSSHTunnelConfigured
     */
    public boolean isIsSSHTunnelConfigured() {
        return isSSHTunnelConfigured;
    }

    /**
     * @param isSSHTunnelConfigured the isSSHTunnelConfigured to set
     */
    public void setIsSSHTunnelConfigured(boolean isSSHTunnelConfigured) {
        this.isSSHTunnelConfigured = isSSHTunnelConfigured;
    }
    /**
     * @param tunnelInternalHost the tunnelInternalHost to set
     */
    public void setTunnelInternalHost(String tunnelInternalHost) {
        this.tunnelInternalHost = tunnelInternalHost;
    }
    public Element getElement()
    {
        Element e = new Element("void");
        e.setAttribute("property", "sshTunnel");
        
        if(publicKeyFile != null)
        {
            e.addContent(XmlEncoderUtil.getStringPropertyElement("publicKeyFile", publicKeyFile));
        }
        if(tunnelExternalHost != null)
        {
            e.addContent(XmlEncoderUtil.getStringPropertyElement("tunnelExternalHost", tunnelExternalHost));
        }
        if(getExternalPort() != null)
        {
            e.addContent(XmlEncoderUtil.getStringPropertyElement("externalPort", getExternalPort()));
        }
        if(getLocalPort() != null)
        {
            e.addContent(XmlEncoderUtil.getStringPropertyElement("localPort", getLocalPort()));
        }
        if(ec2User != null)
        {
            e.addContent(XmlEncoderUtil.getStringPropertyElement("ec2User", ec2User));
        }
        if(tunnelInternalHost != null)
        {
            e.addContent(XmlEncoderUtil.getStringPropertyElement("tunnelInternalHost", tunnelInternalHost));
        } 
        if(isSSHTunnelConfigured)
        {
            e.addContent(XmlEncoderUtil.getBooleanPropertyElement("isSSHTunnelConfigured", String.valueOf(isSSHTunnelConfigured)));    
        }
        if(getTunnelInternalPort() != null)
        {
           e.addContent(XmlEncoderUtil.getStringPropertyElement("tunnelInternalPort", getTunnelInternalPort())); 
        }
        return e;
    }
    @Override
    public Object clone()
    {
        try
        {
            SSHTunnel newObj = (SSHTunnel)super.clone();
            return newObj;
	}
        catch (CloneNotSupportedException e)
        {
            e.printStackTrace();
	}
	return null;
    }

    /**
     * @return the externalPort
     */
    public String getExternalPort() {
        return externalPort;
    }

    /**
     * @param externalPort the externalPort to set
     */
    public void setExternalPort(String externalPort) {
        this.externalPort = externalPort;
    }

    /**
     * @return the localPort
     */
    public String getLocalPort() {
        return localPort;
    }

    /**
     * @param localPort the localPort to set
     */
    public void setLocalPort(String localPort) {
        this.localPort = localPort;
    }

    /**
     * @return the tunnelInternalPort
     */
    public String getTunnelInternalPort() {
        return tunnelInternalPort;
    }

    /**
     * @param tunnelInternalPort the tunnelInternalPort to set
     */
    public void setTunnelInternalPort(String tunnelInternalPort) {
        this.tunnelInternalPort = tunnelInternalPort;
    }

    



   
}
