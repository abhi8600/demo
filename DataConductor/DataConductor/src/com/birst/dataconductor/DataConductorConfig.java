/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.birst.dataconductor;

import com.birst.dataconductor.localetl.LocalETLConfig;
import java.beans.ExceptionListener;
import java.beans.XMLDecoder;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import org.jdom.Element;
import org.apache.log4j.Logger;


/**
 *
 * @author bpeters
 */
public class DataConductorConfig
{
    private LocalETLConfig localETLConfig = new LocalETLConfig();
    private List<Task> taskList = new ArrayList<Task>();
    private Map<String, RealtimeConnection> realtimeConnections = new HashMap<String, RealtimeConnection>();
    private String logFileName;
    private final static Logger logger = Logger.getLogger(DataConductorConfig.class);

    public LocalETLConfig getLocalETLConfig() {
        return localETLConfig;
    }

    public void setLocalETLConfig(LocalETLConfig localETLConfig) {
        this.localETLConfig = localETLConfig;
    }

    public String getLogFileName()
    {
        return logFileName;
    }

    public void setLogFileName(String logFileName)
    {
        this.logFileName = logFileName;
    }

    public List<Task> getTaskList()
    {
        return taskList;
    }

    public void setTaskList(List<Task> taskList)
    {
        this.taskList = taskList;
    }

    public Map<String, RealtimeConnection> getRealtimeConnections()
    {
        return realtimeConnections;
    }

    public void setRealtimeConnections(Map<String, RealtimeConnection> realtimeConnections)
    {
        this.realtimeConnections = realtimeConnections;
    }

    public Element getElement()
    {
        Element e = new Element("object");
        e.setAttribute("class", DataConductorConfig.class.getName());
        if (localETLConfig != null)
            e.addContent(localETLConfig.getElement());
        if (realtimeConnections != null && !realtimeConnections.isEmpty())
            e.addContent(getRealtimeConnectionsElement());
        if (taskList != null && !taskList.isEmpty())
            e.addContent(getTasksElement());
        if (logFileName != null)
            e.addContent(XmlEncoderUtil.getStringPropertyElement("logFileName", logFileName));
        return e;
    }
    
    public Element getRealtimeConnectionsElement()
    {
        Element e = new Element("void");
        e.setAttribute("property", "realtimeConnections");
        for (String key : realtimeConnections.keySet())
        {
            Element put = new Element("void");
            put.setAttribute("method", "put");
            Element putStr = new Element("string");
            putStr.addContent(key);
            put.addContent(putStr);
            RealtimeConnection conn = realtimeConnections.get(key);
            put.addContent(conn.getElement());
            e.addContent(put);            
        }
        return e;
    }
    
    public Element getTasksElement()
    {
        Element e = new Element("void");
        e.setAttribute("property", "taskList");
        for (Task task : taskList)
        {
            Element add = new Element("void");
            add.setAttribute("method", "add");
            add.addContent(task.getElement());
            e.addContent(add);            
        }
        return e;
    }

    public static DataConductorConfig createFromXML(String xml)
    {
        ByteArrayInputStream bais = null;
        try {
            // remove the <?xml ... > tag
            int index = xml.indexOf("<java ");
            if (index == -1)
                return null;
            xml = xml.substring(index);
            bais = new ByteArrayInputStream(xml.getBytes("UTF-8"));
            XMLDecoder decode = new XMLDecoder(bais);
            // Ignore XML parsing warnings
            decode.setExceptionListener(new ExceptionListener()
            {
                public void exceptionThrown(Exception e)
                {
                    //
                }
            });
            Object o = null;
            DataConductorConfig conf = null;
            try
            {
                o = decode.readObject();
                if (o != null)
                {
                    conf = (DataConductorConfig) o;
                    if (conf != null)
                    {
                        conf.initializeGenericConnection();
                    }
                }
            }
            catch (Exception ex)
            {
                return null;
            }
            return conf;
        } catch (UnsupportedEncodingException ex) {
            ex.printStackTrace();
            return null;
        } finally {
            try {
                if(bais!=null){
                    bais.close();
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    public boolean isVisibleNameExist(String visiblename)
    {
        boolean found = false;
        String lvname = visiblename.toLowerCase();
        for(Entry<String, RealtimeConnection> entry: getRealtimeConnections().entrySet())
        {
            if (entry.getKey().toLowerCase().equals(lvname))
            {
                found = true;
                break;
            }
        }
        return found;
    }
    
    public void updateGenericConnection()
    {
        try
        {
            if (realtimeConnections != null)
            {
                for(String key : realtimeConnections.keySet())
                {
                    RealtimeConnection rc = realtimeConnections.get(key);
                    if (rc != null && rc.isGenericDriver())
                    {
                        rc.setConnectString(rc.getDatabaseName());
                        rc.setDriverName(rc.getServerName());
                        rc.setDatabaseName(null);
                        rc.setServerName(null);
                        rc.setPort(null);
                    }
                }
            }
            for(Task t : getTaskList())
            {
                for (Object obj : t.getSourceList())
                {
                    if (obj instanceof QuerySource)
                    {
                        QuerySource qc = (QuerySource) obj;
                        if (qc.isGenericDriver())
                        {
                            qc.setConnectString(qc.getDatabaseName());
                            qc.setDriverName(qc.getServerName());
                            qc.setDatabaseName(null);
                            qc.setServerName(null);
                            qc.setPort(null);
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            logger.error("Error at updateGenericConnection" , ex);
        }
    }
    
    public void initializeGenericConnection()
    {
        try
        {
            if (realtimeConnections != null)
            {
                for(String key : realtimeConnections.keySet())
                {
                    RealtimeConnection rc = realtimeConnections.get(key);
                    if (rc != null && rc.isGenericDriver())
                    {
                        rc.setDatabaseName(rc.getConnectString());
                        rc.setServerName(rc.driverName);
                        rc.setConnectString(null);
                        rc.setDriverName(null);
                    }
                }
            }
            for(Task t : getTaskList())
            {
                for (Object obj : t.getSourceList())
                {
                    if (obj instanceof QuerySource)
                    {
                        QuerySource qc = (QuerySource) obj;
                        if (qc.isGenericDriver())
                        {
                            qc.setDatabaseName(qc.getConnectString());
                            qc.setServerName(qc.driverName);
                            qc.setConnectString(null);
                            qc.setDriverName(null);
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            logger.error("Error at initializeGenericConnection" , ex);
        }
    }
}
