/**
 * $Id: DataConductorCommandLine.java,v 1.12 2012-06-27 22:49:15 ricks Exp $
 *
 * Copyright (C) 2010 Birst, Inc. All rights reserved.
 * BIRST PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.birst.dataconductor;

/**
 *
 * @author ricks
 */
public class DataConductorCommandLine
{
    private static boolean realtimeConnection = false;

    public static void main(String[] args)
    {
        if (args == null || args.length <= 1)
        {
            System.out.println("\nUsage:\njava -cp DataConductor.jar\n\t-Djnlp.username=<user name> -Djnlp.password=<password>\n" + "\t-Djnlp.space=<spacename> -Djnlp.url=<host url>\n\t[-Djnlp.realm=authenticate@birst.com]\n\t\n\t[-Djnlp.version=<version>]\n" + "\tcom.birst.dataconductor.DataConductorCommandLine -tasks <comma separated list of task names>\n\n" + "\t<user name> is the login name (normally the email address)\n" + "\t<password> is the encrypted and hashed value, see the About Box in the UI\n" + "\t<spacename> is the name of the space\n" + "\t<host url> is the URL for the host\n");
            Util.exitApp(1, false);
        }
        ActivityLog activityLog = new ActivityLog();
        DataConductorUtil dcu = new DataConductorUtil(activityLog);
        DataConductorConfig config = null;

        try
        {
            config = dcu.setConfig(true);
        }
        catch (Exception ex)
        {
            Util.logStackTrace(ex);
            Util.exitApp(-1, false);
        }

        if (realtimeConnection)
        {
            RealtimeConnection rc = new RealtimeConnection();
            rc.setDatabaseName("BirstData");
            rc.setDriverName(0, realtimeConnection);
            rc.setUsername("smi");
            rc.putPassword("smi");
            rc.setServerName("localhost");
        // rc.connect(sslSocketFactory, uname, pword, space, url, port, ssl);
        }

        dcu.processArgs(args, config);
        Util.exitApp(0, false);
    }
}
