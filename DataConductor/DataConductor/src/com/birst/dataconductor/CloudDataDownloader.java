/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.birst.dataconductor;

import com.birst.dataconductor.localetl.*;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.List;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.GZIPInputStream;
import org.apache.log4j.Logger;

/**
 *
 * @author mpandit
 */
public class CloudDataDownloader {

    private String url;
    String username;
    String password;
    String space;
    private LocalETLConfig leConfig;
    int numRetries = 10;

    private boolean CLOUD_SOURCES_FOR_DOWNLOAD_FOUND;
    private int NO_OF_SOURCES;
    private List<String> sources;
    private Map<String, SourceMetaData> srcMetaData;
    public String fileToSave;
    public String pathToSave;
    public boolean retriveAllLocalSources = false;

    private static final int READTIMEOUT = 4*60*1000; // 4 minutes - DO NOT SET TO ZERO, ZERO causes HttpURLConnection to try a second POST on timeout
    private static final int CONNECTTIMEOUT = 30*1000; // 30 seconds
    private static final int RETRY_SLEEP_TIME = 30 * 1000; // 30 seconds
    private static final int POLL_SLEEP_TIME = 30 * 1000; // 30 seconds

    public enum RequestType
    {
        DownloadCloudData, DownloadBulkQueryResults, DownloadMigrationData;

        @Override
        public String toString()
        {
            switch(this.ordinal())
            {
                case 0:
                    return "birst.downloadclouddata";
                case 1:
                    return "birst.downloadbulkqueryresults";
                case 2:
                    return "birst.downloadmigrationdata";
            }
            return null;
        }
    }

    private final static Logger logger = Logger.getLogger(CloudDataDownloader.class);

    public CloudDataDownloader(String username, String password, String space, String url, LocalETLConfig leConfig)
    {
        this.username = username;
        this.password = password;
        this.space = space;
        this.url = url;
        this.leConfig = leConfig;
    }

    public void setSources(List<String> sources)
    {
        this.sources = sources;
    }

    public WebDAVError downloadData(RequestType requestType)
    {
        WebDAVError error = null;
        if (requestType.equals(RequestType.DownloadCloudData))
        {
            if (leConfig != null)
            {
                File dataDir = new File(leConfig.getApplicationDirectory());
                if (dataDir.exists())
                {
                    dataDir = new File(leConfig.getApplicationDirectory() + File.separator + space + File.separator + "data");
                    dataDir.mkdirs();
                }
            }
            logger.debug("Discovering metadata for cloud sources");
            error = dispatchRequest(requestType, "DiscoverMetaData", null, null);
            if (error != null)
            {
                logger.error("Error discovering metadata for cloud sources");
                return error;
            }
            if (!CLOUD_SOURCES_FOR_DOWNLOAD_FOUND)
            {
                logger.error("No cloud sources found for download");
                return new WebDAVError(WebDAVError.DOWNLOAD_ERROR, "No cloud sources found for download");
            }
            if (NO_OF_SOURCES == 0)
            {
                logger.error("No cloud sources found for download");
                return new WebDAVError(WebDAVError.DOWNLOAD_ERROR, "No cloud sources found for download");
            }
            if (sources == null || NO_OF_SOURCES != sources.size())
            {
                logger.error("Error getting cloud source names for download");
                return new WebDAVError(WebDAVError.DOWNLOAD_ERROR, "Error getting cloud source names for download");
            }
            logger.debug("Metadata for cloud sources discovered successfully");
        }
        for (String source : sources)
        {
            logger.debug("Starting to discover metadata for cloud source: " + source);
            Map<String, String> requestProperties = new HashMap<String, String>();
            requestProperties.put("Source", source);
            error = dispatchRequest(requestType, "GetSourceMetaData", source, requestProperties);
            if (error != null)
            {
                logger.error("Error discovering metadata for cloud source: " + source);
                return error;
            }
            logger.debug("Metadata for cloud source: " + source + " discovered successfully");
        }
        logger.debug("Metadata for all cloud sources discovered successfully");
        for (String source : sources)
        {
            logger.debug("Starting data downloading for cloud source: " + source);
            SourceMetaData smd = srcMetaData.get(source);
            int segment = 0;
            do
            {
                segment++;
                Map<String, String> requestProperties = new HashMap<String, String>();
                requestProperties.put("Source", source);
                requestProperties.put("Segment", String.valueOf(segment));
                error = dispatchRequest(requestType, "DownloadData", source, requestProperties);
                if (error != null)
                {
                    logger.error("Error downloading data for cloud source: " + source);
                    return error;
                }
                logger.debug("Downloading data for cloud source: " + source + ": segment " + segment + "/" + smd.segments + " retrieved successfully");
            }while(segment < smd.segments);
            logger.debug("Completed data downloading for cloud source: " + source);
        }
        logger.debug("All cloud sources downloaded successfully");
        return error;
    }

    private WebDAVError dispatchRequest(RequestType connectionType, String requestMode, String source, Map<String, String> requestProperties)
    {
        try
        {
            WebDAVError error = null;
            Exception ex = null;
            int tries = 0; // allow retries if the connection fails
            do
            {
                FileInputStream fis = null;
                HttpURLConnection hs = null;
                BufferedReader in = null;
                try
                {
                    error = null;
                    hs = setupConnection(space, connectionType.toString());
                    hs.setRequestProperty("RequestMode", requestMode);
                    if (connectionType.equals(RequestType.DownloadCloudData))
                    {
                        hs.setRequestProperty("LocalConnection", leConfig.getLocalETLConnection());
                        hs.setRequestProperty("RetriveAllLocalSources", String.valueOf(retriveAllLocalSources));
                    }
                    if (requestProperties != null)
                    {
                        for (String rp : requestProperties.keySet())
                        {
                            hs.setRequestProperty(rp, requestProperties.get(rp));
                        }
                    }
                    // make the connection (deals with proxies and other stuff)
                    hs.connect();
                    // dummy write to force the connection
                    OutputStream sos = hs.getOutputStream();
                    sos.write(0);
                    sos.close();
                    // get the response
                    Util.dumpHttpResponse(hs);
                    int code = hs.getResponseCode();
                    if (code != HttpURLConnection.HTTP_OK)
                    {
                        // unlikely to be recoverable, try again later
                        error = new WebDAVError(WebDAVError.DOWNLOAD_ERROR, hs.getResponseMessage());
                        try {
                            if (tries + 1 < numRetries)
                                Thread.sleep(RETRY_SLEEP_TIME);
                        } catch (InterruptedException ex1) {
                        }
                    }
                    else
                    {
                        in = new BufferedReader(new InputStreamReader(hs.getInputStream()));
                        String inputLine;
                        List<String> metaData = new ArrayList<String>();
                        byte[] data = null;
                        if (hs.getHeaderField("ContentEncoding") != null && hs.getHeaderField("ContentEncoding").equals("gzip"))
                            data = getRequestInputGZIP(hs);
                        else
                        {
                            while ((inputLine = in.readLine()) != null)
                            {
                                metaData.add(inputLine);
                            }
                        }
                        if (requestMode.equals("DiscoverMetaData"))
                            parseMetaData(metaData);
                        else if (requestMode.equals("GetSourceMetaData"))
                        {
                            SourceMetaData smd = parseSourceMetaData(metaData);
                            if (smd != null && smd.segments > 0)
                            {
                                logger.debug("Successfully discovered metadata for cloud source: " + source + ": segments=" + smd.segments);
                                if (srcMetaData == null)
                                    srcMetaData = new HashMap<String, SourceMetaData>();
                                srcMetaData.put(source, smd);
                            }
                            else
                            {
                                logger.error("Error discovering metadata for cloud source: " + source);
                                return new WebDAVError(WebDAVError.DOWNLOAD_ERROR, "Error discovering metadata for cloud source: " + source);
                            }
                        }
                        else if (requestMode.equals("DownloadData"))
                        {
                            String serverHash = hs.getHeaderField("Hash256");
                            String strHash = null;
                            // calculate SHA-256 hash value for the segment
                            try
                            {
                                MessageDigest sha256 = MessageDigest.getInstance("SHA-256");
                                sha256.update(data);
                                byte[] hash = sha256.digest();
                                strHash = Util.getHexString(hash).replace("-", "").toLowerCase();                                
                            } catch (java.security.NoSuchAlgorithmException e)
                            {
                            	if (logger.isTraceEnabled()) {
                                    logger.trace("Exception " + ex + " occured while processing method CloudDataDownloader.dispatchRequest(RequestType, String, String, Map<String, String>) ", ex);
                                }
                            }
                            if (!serverHash.equals(strHash))
                            {
                               logger.error("Client and server hash values do not match; client: " + strHash + ", server: " + serverHash);
                               return new WebDAVError(WebDAVError.DOWNLOAD_ERROR, "Client and server hash values do not match; client: " + strHash + ", server: " + serverHash);
                            }
                            logger.debug("Client and server hash values match; client: " + strHash + ", server: " + serverHash);

                            String outputFilePath = null;
                            if (connectionType.equals(RequestType.DownloadCloudData))
                            {
                                outputFilePath = leConfig.getApplicationDirectory() + File.separator + space + File.separator + "data" + File.separator + source;
                            }
                            else if (connectionType.equals(RequestType.DownloadBulkQueryResults) && fileToSave != null)
                            {
                                outputFilePath = fileToSave;
                            }
                            else if (connectionType.equals(RequestType.DownloadMigrationData) && pathToSave != null)
                            {
                                outputFilePath = pathToSave + File.separator + source;
                            }
                            File partFi = new File(outputFilePath + ".part");
                            int segment = Integer.parseInt(requestProperties.get("Segment"));
                            if (segment == 1)
                            {
                                if (partFi.exists())
                                    partFi.delete();
                                partFi.createNewFile();
                            }                            
                            FileOutputStream fos = new FileOutputStream(partFi, true);
                            fos.write(data);
                            fos.close();
                            if (segment == srcMetaData.get(source).segments)
                            {
                                File f = new File(outputFilePath);
                                if (f.exists())
                                    f.delete();
                                if (!partFi.renameTo(f))
                                {
                                    logger.error("Cannot rename " + partFi.getName() + " to " + f.getName());
                                    return new WebDAVError(WebDAVError.DOWNLOAD_ERROR, "Cannot rename " + partFi.getName() + " to " + f.getName());
                                }
                            }
                        }
                    }
                }
                catch (Exception sex)
                {
                    // could be a recoverable connection failure (XXX check for ConnectException?)
                    error = new WebDAVError(WebDAVError.DOWNLOAD_ERROR, sex.getMessage());
                    ex = sex;
                    logger.error(sex.getMessage(), sex);
                    try {
                        if (tries + 1 < numRetries)
                            Thread.sleep(RETRY_SLEEP_TIME);
                    } catch (InterruptedException ex1) {
                    }
                } finally
                {
                    if (in != null)
                        in.close();
                    if (fis != null)
                        fis.close();
                    if (hs != null)
                        hs.disconnect();
                }
            } while (error != null && tries++ < numRetries); // try numRetries if there is an error
        }
        catch (IOException ex)
        {
            logger.error(ex.getMessage(), ex);
            return new WebDAVError(WebDAVError.DOWNLOAD_ERROR, ex.getMessage());
        }
        return null;
    }

    private byte[] getRequestInputGZIP(HttpURLConnection hs) throws Exception
    {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        GZIPInputStream inputStream = null;
        try
        {
            inputStream = new GZIPInputStream(hs.getInputStream());
            byte[] data = new byte[16384];
            int size = 0;            
            while ((size = inputStream.read(data)) != -1)
            {
                baos.write(data, 0, size);                                
            }            
        }
        catch (IOException ioe)
        {
            logger.error(ioe.getMessage(), ioe);
        }
        finally{
            if (inputStream != null)
                inputStream.close();
        }
        return baos.toByteArray();
    }

    /*
     * set up the url connection for file downloading
     */
    private HttpURLConnection setupConnection(String space, String requestSuffix) throws Exception {
        String path = "/" + URLEncoder.encode(space, "UTF-8").replace("+", "%20") + "/PullCloudData/" + requestSuffix;
        HttpURLConnection hs = Util.getHttpURLConnection(url, path, "POST", true);
        hs.setReadTimeout(READTIMEOUT); // DO NOT SET TO ZERO, ZERO causes HttpURLConnection to try a second POST on timeout
        hs.setConnectTimeout(CONNECTTIMEOUT);
        return hs;
    }

    private void parseMetaData(List<String> metaData)
    {
        for(String line: metaData)
        {
            if (line.startsWith("CLOUD_SOURCES_FOR_DOWNLOAD_FOUND="))
                CLOUD_SOURCES_FOR_DOWNLOAD_FOUND = Boolean.parseBoolean(line.substring("CLOUD_SOURCES_FOR_DOWNLOAD_FOUND=".length()));
            if (line.startsWith("NO_OF_SOURCES="))
                NO_OF_SOURCES = Integer.parseInt(line.substring("NO_OF_OBJECTS=".length()));
            if (line.startsWith("SOURCES="))
                sources = Arrays.asList(line.substring("SOURCES=".length()).split("/"));
        }
    }

    private SourceMetaData parseSourceMetaData(List<String> metaData)
    {
        SourceMetaData smd = new SourceMetaData();
        for(String line: metaData)
        {
            if (line.startsWith("SEGMENTS="))
                smd.segments = Integer.parseInt(line.substring("SEGMENTS=".length()));
        }
        return smd;
    }

    private class SourceMetaData
    {
        int segments;        
    }

}
