/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.birst.dataconductor;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import org.apache.log4j.Logger;
import org.olap4j.OlapConnection;
import org.olap4j.OlapException;

/**
 *
 * @author mjani
 */
public class XMLADataTypeMap
{
    //START: OLE DB Data type constants

    //Map to Integer
    public static final String BIGINT = "20";
    public static final String BOOLEAN = "11";
    public static final String GUID = "72";
    public static final String SMALLINT = "2";
    public static final String UNSIGNEDTINYINT = "17";
    public static final String INTEGER = "3";
    //Map to Numeric
    public static final String CURRENCY = "6";
    public static final String DECIMAL = "14";
    public static final String NUMERIC = "131";
    //Map to Date
    public static final String DATE = "7";
    //Map to DateTime
    public static final String DBTIMESTAMP = "135";
    public static final String BINARY = "128";
    //Map to Double
    public static final String DOUBLE = "5";
    public static final String SINGLE = "4";
    //Map to Varchar
    public static final String CHAR = "129";
    public static final String DISPATCH = "9";
    public static final String LONGVARBINARY = "205";
    public static final String LONGVARCHAR = "201";
    public static final String LONGVARWCHAR = "203";
    public static final String VARBINARY = "204";
    public static final String VARCHAR = "200";
    public static final String VARIANT = "12";
    public static final String VARWCHAR = "202";
    public static final String WCHAR = "130";
    //END: OLE DB Data type constants

    private OlapConnection olapConnection;
    private String cubeName;
    private String dimName;
    private String hName;
    private Map<String, String> levelDataTypeMap;
    private final static Logger logger = Logger.getLogger(XMLADataTypeMap.class);

    private static Map<String, Integer> sapBWMeasureDataTypeMap = new HashMap<String, Integer>();
    static
    {
        sapBWMeasureDataTypeMap.put("INT4", 3);
        sapBWMeasureDataTypeMap.put("FLTP", 5);
        sapBWMeasureDataTypeMap.put("QUAN", 5);
        sapBWMeasureDataTypeMap.put("DEC", 5);
        sapBWMeasureDataTypeMap.put("CURR", 6);
    }
    public XMLADataTypeMap(OlapConnection olapConnection, String cubeName, String dimName,
            String hName)
    {
        this.olapConnection = olapConnection;
        this.cubeName = cubeName;
        this.dimName = dimName;
        this.hName = hName;
        levelDataTypeMap = new HashMap<String, String>();
    }

    public void fetchDataTypesForAllLevels() throws OlapException, SQLException //used only for Dimensions
    {
        ResultSet proprs = null;
        try
        {
            proprs = olapConnection.getMetaData().getProperties(olapConnection.getCatalog(),
                        null, cubeName, dimName, hName, null, null, null);
            String propName = null;
            String nameDataType = null;
            String nameDataTypeStr = null;
            String memberValueDataType = null;
            String memberValueDataTypeStr = null;

            while(proprs.next())
            {
                String lvlUniqueName = proprs.getString("LEVEL_UNIQUE_NAME");
                propName = proprs.getString("PROPERTY_NAME");
                if(propName.equals("NAME"))
                {
                    nameDataType = proprs.getString("DATA_TYPE");
                    nameDataTypeStr = getDataTypeStr(nameDataType);
                    levelDataTypeMap.put(lvlUniqueName, nameDataTypeStr);
                }
                else if(propName.equals("MEMBER_VALUE") && !levelDataTypeMap.containsKey(lvlUniqueName))
                {
                    memberValueDataType = proprs.getString("DATA_TYPE");
                    memberValueDataTypeStr = getDataTypeStr(memberValueDataType);
                    levelDataTypeMap.put(lvlUniqueName, memberValueDataTypeStr);
                }
            }
        }
        catch(OlapException oe)
        {
            //throw oe;
        	if (logger.isTraceEnabled()) {
                logger.trace("Exception " + oe + " occured while processing method XMLADataTypeMap.fetchDataTypesForAllLevels() ", oe);
            }
        }
        catch(SQLException sqle)
        {
            throw sqle;
        }
        finally
        {
            if(proprs != null)
            {
                proprs.close();
            }
        }
    }

    public String getDataTypeForLevel(String lvlUniqueName)
    {
        return levelDataTypeMap.get(lvlUniqueName);
    }
    
    private String getDataTypeStr(String dataType)
    {
        String dataTypeStr = "Varchar";
        if(dataType != null)
        {
            if(dataType.equals(BIGINT) ||  dataType.equals(GUID) ||
                    dataType.equals(SMALLINT) || dataType.equals(UNSIGNEDTINYINT) || dataType.equals(INTEGER))
            {
                dataTypeStr = "Integer";
            }
            else if (dataType.equals(BOOLEAN))
            {
                dataTypeStr = "Varchar";
            }
            else if(dataType.equals(CURRENCY) || dataType.equals(DECIMAL) || dataType.equals(NUMERIC)
                    || dataType.equals(SINGLE) || dataType.equals(DOUBLE))
            {
                dataTypeStr = "Number";
            }
            //importing both date and datetime as datetime currently
            else if(dataType.equals(DATE) || dataType.equals(DBTIMESTAMP) || dataType.equals(BINARY))
            {
                dataTypeStr = "DateTime";
            }
            else
            {
                dataTypeStr = "Varchar";
            }
        }
        return dataTypeStr;
    }

    public static int getSapBWMeasureDataType(String dataType)
    {
        if (sapBWMeasureDataTypeMap.containsKey(dataType))
            return sapBWMeasureDataTypeMap.get(dataType);
        else
            return 0;
    }
}
