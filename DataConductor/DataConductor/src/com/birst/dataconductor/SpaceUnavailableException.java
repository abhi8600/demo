/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.birst.dataconductor;

/**
 *
 * @author rchandarana
 */
public class SpaceUnavailableException extends BaseException
{
    public SpaceUnavailableException(String s)
    {
        super(s);
    }

    public SpaceUnavailableException(String s, Throwable t)
    {
        super(s, t);
    }
}
