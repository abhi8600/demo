/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.birst.dataconductor;

import org.jdom.Element;
import org.jdom.output.XMLOutputter;

/**
 *
 * @author mpandit
 */
public class XmlEncoderUtil {
    private static XMLOutputter out = new XMLOutputter();
    public static Element getStringPropertyElement(String propertyName, String propertyValue)
    {
        Element strEl = new Element("void");
        strEl.setAttribute("property", propertyName);
        Element strValEl = new Element("string");
        strValEl.setText(propertyValue);        
        strEl.addContent(strValEl);
        return strEl;
    }
    
    public static Element getBooleanPropertyElement(String propertyName)
    {
        return getBooleanPropertyElement(propertyName, Boolean.TRUE.toString());
    }
    
    public static Element getBooleanPropertyElement(String propertyName, String propertyValue)
    {
        Element boolEl = new Element("void");
        boolEl.setAttribute("property", propertyName);
        Element boolValEl = new Element("boolean");
        boolValEl.setText(propertyValue);
        boolEl.addContent(boolValEl);
        return boolEl;
    }
    
    public static Element getIntPropertyElement(String propertyName, int propertyValue)
    {
        Element intEl = new Element("void");
        intEl.setAttribute("property", propertyName);
        Element intValEl = new Element("int");
        intValEl.addContent(String.valueOf(propertyValue));
        intEl.addContent(intValEl);
        return intEl;
    }
    
    public static Element getCharPropertyElement(String propertyName, char propertyValue)
    {
        Element charEl = new Element("void");
        charEl.setAttribute("property", propertyName);
        Element charValEl = new Element("char");
        charValEl.setText(String.valueOf(propertyValue));        
        charEl.addContent(charValEl);
        return charEl;
    }
    
}
