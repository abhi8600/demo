/**
 * $Id: BirstProxySelector.java,v 1.1 2010-06-21 17:08:17 ricks Exp $
 *
 * Copyright (C) 2010 Birst, Inc. All rights reserved.
 * BIRST PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.birst.dataconductor;

import java.io.IOException;
import java.net.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author ricks
 */
public class BirstProxySelector extends ProxySelector
{
    ProxySelector defsel = null;
    BirstProxySelector(ProxySelector def)
    {
        defsel = def;
    }

    public List<Proxy> select(URI uri)
    {
        if (uri == null)
        {
            throw new IllegalArgumentException("URI can't be null.");
        }
        String protocol = uri.getScheme();
        if ("http".equalsIgnoreCase(protocol) || "https".equalsIgnoreCase(protocol))
        {
            List<Proxy> l = new ArrayList<Proxy>();
            // Populate the ArrayList with proxies
            l.add(new Proxy(Proxy.Type.SOCKS, new InetSocketAddress("192.168.90.151", 1080)));
            l.add(new Proxy(Proxy.Type.HTTP, new InetSocketAddress("192.168.90.151", 3128)));
            return l;
        }
        if (defsel != null)
        {
            return defsel.select(uri);
	}
        else
        {
            List<Proxy> l = new ArrayList<Proxy>();
            l.add(Proxy.NO_PROXY);
            return l;
        }
    }

    public void connectFailed(URI uri, SocketAddress sa, IOException ioe)
    {
        //
    }
}
