/**
 * $Id: DataConductorApp.java,v 1.6 2011-02-23 22:28:00 ricks Exp $
 *
 * Copyright (C) 2010 Birst, Inc. All rights reserved.
 * BIRST PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.birst.dataconductor;

import org.jdesktop.application.Application;
import org.jdesktop.application.SingleFrameApplication;

/**
 * The main class of the application.
 */
public class DataConductorApp extends SingleFrameApplication
{
    static
    {
        System.setProperty("http.maxConnections", "1");
        System.setProperty("http.keepAlive", "false");
        System.setProperty("sun.net.http.retryPost", "false");
    }

    private String[] args;

    /**
     * At startup create and show the main frame of the application.
     */
    @Override
    protected void startup()
    {
        DataConductorView dcv = new DataConductorView(this);
        DataConductorUtil dcu = dcv.getDataConductorUtil();
        if (!dcu.processArgs(args, dcv.getConfig()))
        {
            show(dcv);
        }
    }

    @Override
    protected void initialize(String[] args)
    {
        super.initialize(args);
        this.args = args;
    }

    /**
     * This method is to initialize the specified window by injecting resources.
     * Windows shown in our application come fully initialized from the GUI
     * builder, so this additional configuration is not needed.
     */
    @Override
    protected void configureWindow(java.awt.Window root)
    {
    }

    /**
     * A convenient static getter for the application instance.
     * @return the instance of DataConductorApp
     */
    public static DataConductorApp getApplication()
    {
        return Application.getInstance(DataConductorApp.class);
    }

    /**
     * Main method launching the application.
     */
    public static void main(String[] args)
    {
        launch(DataConductorApp.class, args);
    }
}
