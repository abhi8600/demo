/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.birst.dataconductor;


import com.birst.dataconductor.amazon.AmazonUtil;
import com.birst.dataconductor.sap.SAPJCoException;
import com.birst.dataconductor.sap.SAPUtil;
import java.io.BufferedReader;
import java.net.*;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.StringReader;
import java.nio.channels.FileChannel;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Timestamp;
import java.sql.Types;
import java.sql.ResultSetMetaData;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.StringTokenizer;
import java.util.List;
import java.util.ArrayList;
import java.sql.SQLException;
import java.nio.charset.Charset;

import java.util.Enumeration;
import java.util.Iterator;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamReader;
import org.apache.axiom.om.OMAttribute;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.impl.builder.StAXOMBuilder;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.ConsoleAppender;
import org.apache.log4j.Logger;
import org.apache.log4j.Level;
import org.apache.log4j.PatternLayout;
import org.apache.log4j.PropertyConfigurator;
import org.apache.log4j.WriterAppender;
import org.apache.log4j.xml.DOMConfigurator;
import org.jdom.Element;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;
import java.io.File;
import java.sql.DatabaseMetaData;

/**
 *
 * @author ricks
 */
public class DataConductorUtil
{
    public static final int NUM_DEFAULT_TRIES = 10;
    private static final String key = "AZ" + "RB" + "PZ" + "WN" + "Z4" + "WH" + "BH" + "C2" + "W9" + "2I";
    private String realm;
    private String uname;
    private String pword;
    private String space;
    private String url;
    private boolean isRunningHeadLess = false; // true if running BirstConnect as windows service
    GraphicsObject go;
    private ActivityLog activityLog;

    private final static Logger logger = Logger.getLogger(DataConductorUtil.class);
    private int exitStatus = 0;
    private boolean exitUsingExitStatus = false;
    private static WriterAppender defaultLogfileAppender = null;
    public static final String DEFAULT_LOG_FORMAT = "%d{yyyy-MM-dd HH:mm:ss z} %-5p %m%n";
    private String dcConfigFileName;
    public static final String DCCONFIG_FILE_NAME = "dcconfig.xml";
    public static final String BirstConnectVersion = getBirstConnectVersion();
    private String responseLogFile = null;
    public static String localDCConfigLocation = null;
    public static boolean hasAmazonS3Connector = false;
    public static final String STATUS_PREFIX = "Status ";
    
    static
    {
        System.setProperty("http.maxConnections", "1");
        System.setProperty("http.keepAlive", "false");
        System.setProperty("sun.net.http.retryPost", "false");
    }

    public DataConductorUtil(ActivityLog activityLog)
    {
        go = new GraphicsObject();
        this.activityLog = activityLog;
        init();
    }

    public DataConductorUtil(GraphicsObject go, ActivityLog activityLog)
    {
        this.go = go;
        this.activityLog = activityLog;
        init();
    }

    private void init()
    {
        System.setProperty("http.maxConnections", "1");
        System.setProperty("http.keepAlive", "false");
        System.setProperty("sun.net.http.retryPost", "false");
        
        String jnlpFileException = null;

        Level currLevel = Logger.getRootLogger().getLevel();
        Logger.getRootLogger().setLevel(Level.OFF);
        
        String jnlpFile = System.getProperty("jnlp.file");
        if (jnlpFile != null && !jnlpFile.isEmpty())
        {
            try {
                XMLStreamReader parser = XMLInputFactory.newInstance().createXMLStreamReader(new FileInputStream(jnlpFile));
                StAXOMBuilder builder = new StAXOMBuilder(parser);
				OMElement doc = null;
				doc = builder.getDocumentElement();
				doc.build();
				doc.detach();
                for (Iterator<OMElement> iter = doc.getChildElements(); iter.hasNext(); ) {
                    OMElement el = iter.next();
                    if (el.getLocalName().equals("resources"))
                    {
                        for (Iterator<OMElement> iter2 = el.getChildElements(); iter2.hasNext(); ) {
                            OMElement el2 = iter2.next();
                            if (el2.getLocalName().equals("property"))
                            {
                                String propName = null, propValue = null;
                                for (Iterator<OMAttribute> iter3 = el2.getAllAttributes(); iter3.hasNext(); ) {
                                    OMAttribute attr = iter3.next();
                                    if (attr.getLocalName().equals("name"))
                                        propName = attr.getAttributeValue();
                                    if (attr.getLocalName().equals("value"))
                                        propValue = attr.getAttributeValue();
                                }
                                if (propName != null && propValue != null)
                                    System.setProperty(propName, propValue);
                            }
                        }
                    }
                }
            } catch (Exception ex) {
                jnlpFileException = ex.getMessage();
            }
        }

        Logger.getRootLogger().setLevel(currLevel);

        String log4jOverride = System.getProperty("jnlp.log4j.Override");
        if (log4jOverride != null && !log4jOverride.isEmpty())
        {
            if (log4jOverride.endsWith(".xml"))
            {
                DOMConfigurator.configure(log4jOverride);
            }
            else if (log4jOverride.endsWith(".properties"))
            {
                PropertyConfigurator.configure(log4jOverride);
            }
            else
            {
                setBasicConfiguration();
            }
        }
        else
        {
            setBasicConfiguration();
        }
        
        if (jnlpFileException != null)
        {
        	logger.error("file in the -Djnlp.file specification could not be processed, exiting - " + jnlpFile + " - " + jnlpFileException);
            Util.exitApp(-100);
        }

        if (hasSAPConnector())
        {
            try
            {
                Class.forName("com.sap.conn.jco.JCo");
                logger.info("SAP Support found.");
            }
            catch(ClassNotFoundException e)
            {
                logger.info("SAP Support not found.");
                System.setProperty("jnlp.sapconnect", "false");
            }
        }

        if (hasLocalETL())
        {
            try
            {
                Class.forName("com.successmetricsinc.Main");
                logger.info("Local ETL Support found.");
            }
            catch(ClassNotFoundException e)
            {
                logger.info("Local ETL Support not found.");
            }
        }
        
        try
        {
             Class.forName("com.amazonaws.services.s3.AmazonS3Client");
             hasAmazonS3Connector = true;
             logger.info("Amazon S3 Object Support found.");
        }
        catch(ClassNotFoundException e)
        {
            logger.info("Amazon S3 Object Support not found.");
        }

        Util.logRuntimeInfo();

        realm = this.getRealm();
        uname = System.getProperty("jnlp.username");
        pword = this.getPassword();
        space = this.getSpace();
        url = System.getProperty("jnlp.url");
        isRunningHeadLess = Boolean.parseBoolean(System.getProperty("jnlp.headless"));
        if (isRunningHeadLess)
        	logger.info("Running headless");
        dcConfigFileName = System.getProperty("jnlp.dcconfig");
        logger.info(url + ", " + uname + ", " + pword + ", " + realm + ", " + space + ", " + dcConfigFileName);
        if (dcConfigFileName == null || dcConfigFileName.length() == 0)
        {
            logger.info("jnlp.dcconfig parameter is not set. Trying to load default configuration");
            dcConfigFileName = DCCONFIG_FILE_NAME;
        }
        if (BirstConnectVersion != null)
            logger.info("BirstConnectVersion = " + BirstConnectVersion);
        else
            logger.error("Failed to read BirstConnect version info");

        responseLogFile = System.getProperty("jnlp.responseLogFile");
        if (responseLogFile != null)
            logger.info("responseLogFile = " + responseLogFile);
        localDCConfigLocation = System.getProperty("localdconfiglocation");
        if (localDCConfigLocation != null && localDCConfigLocation.length() > 0)
        {
            logger.info("localdconfiglocation = " + localDCConfigLocation);
            File dirFile = new File(localDCConfigLocation);
            if (!dirFile.exists())
            {
                boolean isCreated = false;
                try
                {
                    isCreated = dirFile.mkdir();
                }
                catch (Exception ex)
                {
                    logger.error(ex.getMessage(), ex);
                }
                if (!isCreated)
                {
                    logger.error("Not able to create directory at " + localDCConfigLocation);
                    Util.exitApp(-100);
                }
            }
        }
    }

    public static final String getBirstConnectVersion()
    {
        try
        {
            ResourceBundle rb = new PropertyResourceBundle(DataConductorApp.class.getResource("resources/DataConductorApp.properties").openStream());
            return rb.getString("Application.version");
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        return null;
    }

    public static boolean isLatestVersion(String serverBCVersion)
    {
        if (BirstConnectVersion == null)
        {
            return false;
        }
        int majorVer1=0,majorVer2=0,minorVer=0,buildNo=0;
        int serMajorVer1=0,serMajorVer2=0,serMinorVer=0,serBuildNo=0;
        
        String[] splitVersion = BirstConnectVersion.split("\\.");
        String[] splitSerVersion = serverBCVersion.split("\\.");
        
        if (splitVersion.length > 0)
            majorVer1 = Integer.parseInt(splitVersion[0].trim());
        if (splitVersion.length > 1)
            majorVer2 = Integer.parseInt(splitVersion[1].trim());
        if (splitVersion.length > 2)
            minorVer = Integer.parseInt(splitVersion[2].trim());
        if (splitVersion.length > 3)
            buildNo = Integer.parseInt(splitVersion[3].trim());

        if (splitSerVersion.length > 0)
            serMajorVer1 = Integer.parseInt(splitSerVersion[0].trim());
        if (splitSerVersion.length > 1)
            serMajorVer2 = Integer.parseInt(splitSerVersion[1].trim());
        if (splitSerVersion.length > 2)
            serMinorVer = Integer.parseInt(splitSerVersion[2].trim());
        if (splitSerVersion.length > 3)
            serBuildNo = Integer.parseInt(splitSerVersion[3].trim());
        
        if (serMajorVer1 > majorVer1)
            return false;
        if (serMajorVer2 > majorVer2)
            return false;
        if (serMinorVer > minorVer)
            return false;
        if (serMajorVer1 == majorVer1 && serMajorVer2 == majorVer2 && serMinorVer == minorVer && serBuildNo > buildNo)
            return false;
        return true;
    }

    private void setBasicConfiguration()
    {
        PatternLayout ly = new PatternLayout(DEFAULT_LOG_FORMAT);
        defaultLogfileAppender = new ConsoleAppender(ly);
        BasicConfigurator.configure(defaultLogfileAppender);
        Logger.getRootLogger().setLevel(Level.DEBUG);        
    }

    public String getDCConfigFileName()
    {
        return dcConfigFileName;
    }

    public boolean isRunningHeadLess()
    {
        return isRunningHeadLess;
    }

    public void resetGraphicsObject()
    {
        this.go = new GraphicsObject();
    }

    public String getUrl()
    {
        return url;
    }

    public boolean hasRealtime()
    {
        String hr = System.getProperty("jnlp.realtime");
        return hr != null && hr.equalsIgnoreCase("true");
    }

    public boolean hasSAPConnector()
    {
        String hs = System.getProperty("jnlp.sapconnect");
        return hs != null && hs.equalsIgnoreCase("true");
    }

    public static boolean hasLocalETL()
    {
        String hr = System.getProperty("jnlp.localetl");
        return hr != null && hr.equalsIgnoreCase("true");
    }

    public String getUname()
    {
        return uname;
    }

    public String getPword()
    {
        return pword;
    }

    /**
     * returns the realm, defaults to authenticate@birst.com
     * @return
     */
    private String getRealm()
    {
        String temp = System.getProperty("jnlp.realm");
        if (temp == null || temp.length() == 0)
        {
            temp = "authenticate@birst.com";
        }
        return temp;
    }

    private String getPassword()
    {
        String pwordval = getRawPassword(); // encrypted and md5 hashed

        if (pwordval == null || pwordval.length() == 0)
        {
            uname = go.getUsername(); // XXX this really sucks
            return getHashedPassword(go.getPassword()); // hash the clear text
        } else
        {
            return EncryptionService.getInstance().decrypt(pwordval, key); // decrypt the hash
        }
    }

    /**
     * Returns the password as passed in via JWS or the command line - it is an encrypted and hashed verison of the cleartext password
     * @return
     */
    public static String getRawPassword()
    {
        return System.getProperty("jnlp.password");
    }

    /**
     * return a hashed representation of a cleartext password
     * @param s
     * @return
     */
    private String getHashedPassword(String s)
    {
        MessageDigest md = null;
        try
        {
            md = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException ex)
        {
            logger.error(ex.getMessage());
        }
        try
        {
            // Generate Digest authentication
        	String valueToHash = uname + ":" + realm + ":" + s;
        	boolean containsUnicode = false;
        	for (int i = 0; i < valueToHash.length(); i++) {
        		char c = valueToHash.charAt(i);
        		if (c > 0xFF) {
        			containsUnicode = true;
        			break;
        		}
        	}
        	byte[] b = null;
        	if (containsUnicode) {
        		b = valueToHash.getBytes("UTF-8");
        	}
        	else {
        		b = valueToHash.getBytes();
        	}
            md.update(b);
            byte[] pass = md.digest();
            return Util.getHexString(pass);
        } catch (Exception ex)
        {
            logger.error(ex.getMessage());
            return null;
        }
    }

    public String getSpace()
    {
        return System.getProperty("jnlp.space");
    }

    public String getSpaceName()
    {
        return System.getProperty("jnlp.spacename");
    }

    /**
     * create a temporary directory for the various files that are dynamically created and uploaded or downloaded
     * @return
     */
    public File getTempDirectory()
    {
        try
        {
            File df = File.createTempFile("birst", null);
            String dir = df.getAbsolutePath();
            df.delete();
            File dirFile = new File(dir);
            dirFile.mkdir();
            return (dirFile);
        } catch (IOException ex)
        {
            go.showMessageDialog("Unable to create temp directory: " + ex.getMessage());
            return null;
        }
    }

    /**
     * upload the current settings to the server
     * @param config
     * @param dirFile
     * @return
     */
    public boolean saveSettings(DataConductorConfig config, File dirFile)
    {
        // Upload data sources settings
        config.updateGenericConnection();
        Element e = config.getElement();
        config.initializeGenericConnection();
        XMLOutputter xmlOutputter = new XMLOutputter(Format.getPrettyFormat());
        xmlOutputter.getFormat().setEncoding("utf-8");
        xmlOutputter.getFormat().setOmitDeclaration(false);
        xmlOutputter.getFormat().setOmitEncoding(false);
        BufferedWriter bw = null;
        try
        {
            bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(dirFile.getPath() + File.separator + dcConfigFileName), "UTF-8"));
            bw.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n");
            bw.write("<java version=\"" + System.getProperty("java.version") + "\" class=\"java.beans.XMLDecoder\">\r\n");
            xmlOutputter.output(e, bw);
            bw.flush();
            bw.close();
        }
        catch (IOException ex)
        {
            go.showMessageDialog("Unable to save configuration file: " + ex.getMessage());
            return false;
        }
        finally
        {
            if (bw != null)
            {
                try
                {
                    bw.close();
                }
                catch(Exception ex)
                {
                    logger.error("Error closing configuration File: " + ex.getMessage());
                }
            }                
        }
        if (!isLocalDCConfigInUse())
        {
            WebDavUploader wdu = new WebDavUploader(uname, pword, space, url, go, NUM_DEFAULT_TRIES, this, false);
            WebDAVError werror = wdu.uploadFile(dirFile.getPath(), dcConfigFileName, false, null, false);
            if (werror != null)
            {
                go.showMessageDialog(werror.toString());
                return false;
            }
        }
        logger.info("Saved configuration information");
        return true;
    }

    public Map getRealtimeConnectionStatus(String operation, String name, String visiblename)
    {
        BufferedReader br = null;
        String line = null;
        Map<String, Object> response = new HashMap <String, Object>();
        try
        {
            logger.info("Checking realtime connection status for " + operation + " (Name=" + name + ", Visiblename=" + visiblename + ")");
            String responseString = WebDavUploader.getRealtimeConnectionStatus(url, space, operation, name, visiblename);
            logger.info(responseString);
            if (responseString != null && responseString.length() > 0)
            {
                br = new BufferedReader(new StringReader(responseString));
                while ((line = br.readLine()) != null)
                {
                    if (line.startsWith("Status "))
                    {
                        response.put("statuscode", Integer.parseInt(line.substring("Status ".length())));
                    }
                    else if (line.startsWith("name="))
                    {
                        response.put("name", line.substring("name=".length()));
                    }
                    else if (line.startsWith("ERROR "))
                    {
                        response.put("error", line.substring("ERROR ".length()));
                    }
                }
            }
        }
        catch (Exception ex)
        {
             response.clear();
             String msg = ex.getMessage() == null ? ex.toString() : ex.getMessage();
             response.put("error", msg);
             logger.error(ex.getMessage(), ex);
        }
        finally
        {
            if (br != null)
            {
                try
                {
                    br.close();
                } catch (Exception ex) { 
                	if (logger.isTraceEnabled()) {
                        logger.trace("Exception " + ex + " DataConductorUtil.getRealtimeConnectionStatus(String, String, String) ", ex);
                    }
                }
            }
        }
        return response;
    }
    /**
     * Execute a task, such as upload files or tables
     * @param t
     * @return
     */
    public WebDAVError loadTask(Task t, DataConductorConfig config,Map varMap) {
        File dirFile = null;
        try {

            dirFile = getTempDirectory();

            go.setStatusMessage("Saving Settings");
            go.setProgressBarMaximum(2 + (2 * t.getSourceList().size()));
            // Upload data sources settings
            File configDirFile = dirFile;
            if (isLocalDCConfigInUse())
                configDirFile = new File(localDCConfigLocation);
            if (!saveSettings(config, configDirFile)) {
                return new WebDAVError(0, "Unable to save settings");
            }
            WebDavUploader wdu;
            WebDAVError werror;
            int curValue = 0;
            go.setProgressBarValue(++curValue);
            werror = GetLoadStatus.get(uname, pword, space, url);
            if (werror != null) {
                go.showMessageDialog(werror.toString());
                return werror;
            }
            Map<UploadSettings, List<Object>> ulist = getFileLists(t);
            // iterate over all unique sets of upload settings, processing each file in that group
            for (Entry<UploadSettings, List<Object>> en : ulist.entrySet()) {
                logger.info("Upload settings for this group of files: " + en.getKey().toString() + ", consolidatelikesources: " + t.isConsolidateSources());
                try {
                    File f = new File(dirFile.getPath() + File.separator + "birst.upload");
                    f.createNewFile();
                    PrintWriter writer = new PrintWriter(new OutputStreamWriter(new FileOutputStream(f), "UTF-8"));
                    writer.println("firstrowhasheaders=" + en.getKey().isFirstRowHasHeaders());
                    writer.println("ignoreinternalquotes=" + en.getKey().isIgnoreInteralQuotes());
                    writer.println("beginskip=" + en.getKey().getBeginSkip());
                    writer.println("endskip=" + en.getKey().getEndSkip());
                    writer.println("consolidatelikesources=" + t.isConsolidateSources());
                    writer.println("forcenumcolumns=" + en.getKey().isForceNumColumns());
                    writer.println("quotechar=" + en.getKey().getQuote());
                    if (en.getKey().getReplacements() != null)
                        writer.println("replacements=" + en.getKey().getReplacements());
                    writer.println("ignorecarriagereturn=" + en.getKey().isIgnoreCarriageReturn());
                    if (en.getKey().getSeparator() != null)
                        writer.println("separator=" + en.getKey().getSeparator());
                    if (en.getKey().getEncoding() != null)
                        writer.println("encoding=" + en.getKey().getEncoding());
       
                    if (t.isLocalETLTask() && config.getLocalETLConfig().getLocalETLConnection() != null)
                    {
                        writer.println("localetlsource=true");
                        writer.println("localetlloadgroup=" + config.getLocalETLConfig().getLocalETLLoadGroup());
                        writer.println("localetlconnection=" + config.getLocalETLConfig().getLocalETLConnection());
                    }
                    writer.close();
                } catch (IOException ex) {
                    go.showMessageDialog("Unable to save upload settings file: " + ex.getMessage());
                    return new WebDAVError(0, "Unable to save upload settings file: " + ex.getMessage());
                }
                wdu = new WebDavUploader(uname, pword, space, url, null, NUM_DEFAULT_TRIES, this, false);
                werror = wdu.uploadFile(dirFile.getPath(), "birst.upload", false, null, false);
                if (werror != null) {
                    go.showMessageDialog(werror.toString());
                    return werror;
                }
                logger.info("Sent upload options");

                File dataDir = null;
                boolean useDataDirectory = false;
                if (t.isLocalETLTask() && config.getLocalETLConfig().getApplicationDirectory() != null)
                {
                    dataDir = new File(config.getLocalETLConfig().getApplicationDirectory());
                    if (dataDir.exists())
                    {
                        dataDir = new File(config.getLocalETLConfig().getApplicationDirectory() + File.separator + getSpace() + File.separator + "data");
                        dataDir.mkdirs();
                        useDataDirectory = true;
                    }
                }

                // Now process the files
                List<File> fileList = new ArrayList<File>();
                for (Object o : en.getValue()) {
                    if (QuerySource.class.isInstance(o)) {
                        QuerySource qs = (QuerySource) o;
                        File f = null;
                        String error = null;
                        try {
                            if (qs.getType() == QuerySource.TYPE_SAP)
                            {
                                Class.forName("com.sap.conn.jco.JCo");//throws exception if class not found
                                go.setStatusMessage("Extracting SAP Object: " + qs.getSAPSourceName());
                                f = SAPUtil.getObject(useDataDirectory ? dataDir.getPath() : dirFile.getPath(), qs, go);
                            }
                            else if (qs.getType() == QuerySource.TYPE_SAP_QUERY)
                            {
                                Class.forName("com.sap.conn.jco.JCo");//throws exception if class not found
                                go.setStatusMessage("Extracting SAP Query: " + qs.getSAPSourceName());
                                f = SAPUtil.getSAPQuery(useDataDirectory ? dataDir.getPath() : dirFile.getPath() , qs, go);
                            }
                            else if (qs.getType() == QuerySource.TYPE_SAP_DSO)
                            {
                                Class.forName("com.sap.conn.jco.JCo");//throws exception if class not found
                                go.setStatusMessage("Extracting SAP DSO: " + qs.getSAPDSOName());
                                f = SAPUtil.getDSO(useDataDirectory ? dataDir.getPath() : dirFile.getPath() , qs, go);
                            }
                            else if (qs.getType() == QuerySource.TYPE_S3_OBJECT)
                            {
                                Class.forName("com.amazonaws.services.s3.AmazonS3Client");//throws exception if class not found
                                go.setStatusMessage("Extracting Amazon S3 Object: " + qs.getTableName());
                                f = AmazonUtil.getS3Object(useDataDirectory ? dataDir.getPath() : dirFile.getPath(), qs);
                            }
                            else if (qs.getType() == QuerySource.TYPE_URL_OBJECT)
                            {
                                go.setStatusMessage("Extracting data using " + qs.getMethod() + " from URL: " + qs.getUrl());
                                f = Util.copyURLtoFile(useDataDirectory ? dataDir.getPath() : dirFile.getPath(), qs, varMap);
                            }
                            else
                            {
                                go.setStatusMessage("Extracting table: " + qs.getTableName());
                                f = getTable(useDataDirectory ? dataDir.getPath() : dirFile.getPath(), qs,varMap);
                            }
                        } catch (IOException ex) {
                            String message = ex.getMessage() + " (I/O)";
                            Throwable tt = ex.getCause();
                            if (tt != null) {
                                message += " [" + tt.getMessage() + "]";
                            }
                            logger.error(message);
                            error = message;
                            return new WebDAVError(0, "Error occurred while uploading the data: " + message);
                        } catch (SQLException ex) {
                            String message = ex.getMessage() + " (SQL)";
                            Throwable tt = ex.getCause();
                            if (tt != null) {
                                message += " [" + tt.getMessage() + "]";
                            }
                            logger.error(message);
                            error = message;
                            return new WebDAVError(0, "Error occurred while uploading the data: " + message);
                        } catch (SAPJCoException ex) {
                            String message = ex.getMessage() + " (JCo)";
                            Throwable tt = ex.getCause();
                            if (tt != null) {
                                message += " [" + tt.getMessage() + "]";
                            }
                            logger.error(message);
                            error = message;
                            return new WebDAVError(0, "Error occurred while uploading the data: " + message);
                        } catch (ClassNotFoundException ex)
                        {
                            Throwable tt = ex.getCause();
                            String message = null;
                            if (qs.getType() == QuerySource.TYPE_S3_OBJECT)
                            {
                                message = "Amazon S3 support not found, cannot run Amazon S3 task.";    
                            }
                            else
                            {
                                message = "SAP support not found, cannot run SAP task.";    
                            }
                            
                            if (tt != null) {
                                message += " [" + tt.getMessage() + "]";
                            }
                            logger.error(message);
                            error = message;
                            return new WebDAVError(0, "Error occurred while uploading the data: " + message);
                        }
                        catch (Exception ex)
                        {
                            String message = ex.getMessage();
                            Throwable tt = ex.getCause();
                            if (tt != null) {
                                message += " [" + tt.getMessage() + "]";
                            }
                            logger.error(message);
                            error = message;
                            return new WebDAVError(0, "Error occurred while uploading the data: " + message);
                        }

                        if (f != null) {
                            fileList.add(f);
                        } else {
                            if (qs.getType() == QuerySource.TYPE_TABLE) {
                                go.showMessageDialog("Unable to extract table: " + qs.getTableName() + (error != null ? " - " + error : ""));
                            } else if (qs.getType() == QuerySource.TYPE_QUERY) {
                                go.showMessageDialog("Unable to extract query: " + (error != null ? " - " + error : ""));
                            } else if (qs.getType() == QuerySource.TYPE_URL_OBJECT) {
                                go.showMessageDialog("Unable to extract from URL: " + (error != null ? " - " + error : ""));
                            } else if (qs.getType() == QuerySource.TYPE_SAP) {
                                go.showMessageDialog("Unable to extract object: " + qs.getSAPSourceName() + (error != null ? " - " + error : ""));
                            } else if (qs.getType() == QuerySource.TYPE_SAP_QUERY) {
                                go.showMessageDialog("Unable to extract SAP query: " + qs.getSAPSourceName() + (error != null ? " - " + error : ""));
                            }
                        }
                        go.setProgressBarValue(++curValue);
                    } else {
                        File f = new File(((FileSource) o).getPath() + File.separator + ((FileSource) o).getFileName());
                        if (useDataDirectory && (f.getName().toLowerCase().endsWith(".xls") ||f.getName().toLowerCase().endsWith(".xlsx") || f.getName().toLowerCase().endsWith(".mdb")))
                        {
                            go.showMessageDialog("Cannot upload " + f.getName() + " for Birst Local task.");
                            logger.error("Cannot upload " + f.getName() + " for Birst Local task.");
                            continue;
                        }
                        fileList.add(f);
                        if (useDataDirectory && !f.getName().toLowerCase().endsWith(".command"))
                        {
                            try
                            {
                                if (f.getName().toLowerCase().endsWith(".zip"))
                                {
                                    //extract zip file into Local Application Directory
                                    String filename = f.getPath();
                                    FileInputStream zfs = null;
                                    ZipInputStream zs = null;
                                    List<String> files = new ArrayList<String>();
                                    List<String> existingFiles = new ArrayList<String>();
                                    try
                                    {
                                        // retrieve the meta data information
                                        Map<String, ZipEntry> zipEntriesByName = new HashMap<String, ZipEntry>();
                                        ZipFile zf = null;
                                        try
                                        {
                                            zf = new ZipFile(filename);
                                            ZipEntry zen = null;
                                            Enumeration e = zf.entries();
                                            while (e.hasMoreElements())
                                            {
                                                zen = (ZipEntry)e.nextElement();
                                                zipEntriesByName.put(zen.getName(), zen);
                                            }
                                        }
                                        finally
                                        {
                                            if (zf != null)
                                                zf.close();
                                        }
                                        zfs = new FileInputStream(f);
                                        zs = new ZipInputStream(zfs);
                                        ZipEntry ze = null;
                                        while ((ze = zs.getNextEntry()) != null)
                                        {
                                            // Don't include directories - load all into the 'data' directory
                                            if (ze.isDirectory())
                                                continue;
                                            String zfname = ze.getName();
                                            long zesize = ze.getSize();
                                            if (zesize == -1) // need to get the size from the cached meta data (streaming zipfiles have the size at the end)
                                            {
                                                if (zipEntriesByName.get(ze.getName()) != null)
                                                {
                                                    zesize = zipEntriesByName.get(ze.getName()).getSize();
                                                }
                                            }
                                            zfname = zfname.replace('\\', '_').replace('/', '_'); // get rid of path separators
                                            // Don't expand duplicates
                                            if (!files.contains(zfname) && !existingFiles.contains(zfname))
                                            {
                                                FileOutputStream fs = null;
                                                String zfilename = dataDir + File.separator + zfname;
                                                try
                                                {
                                                    logger.debug("Unpacking file: " + zfilename + " (" + zesize + " bytes)");
                                                    fs = new FileOutputStream(new File(zfilename));
                                                    int size = 2048;
                                                    long total = 0;
                                                    byte[] data = new byte[2048];
                                                    while (true)
                                                    {
                                                        size = zs.read(data, 0, data.length);
                                                        total += size;
                                                        // sanity check, make sure that the file is not bigger than the zip entry size (sadly, files that we create put the sizes at the end..., so we see -1)
                                                        if (zesize != -1 && total > zesize)
                                                        {
                                                            logger.warn("Writing file larger than the size in the zip entry, aborting: current size = " + total + ", zip entry size = " + zesize);
                                                            throw new Exception("Writing file larger than the size in the zip entry, aborting: current size = " + total + ", zip entry size = " + zesize);
                                                        }
                                                        if (size > 0)
                                                        {
                                                            fs.write(data, 0, size);
                                                        }
                                                        else
                                                        {
                                                            break;
                                                        }
                                                    }
                                                    files.add(zfname);
                                                }
                                                catch (Exception ex)
                                                {
                                                    logger.warn("Unable to unpack file: " + zfilename + ", " + ex.toString());
                                                    logger.warn("Unable to unzip file: " + filename + ", " + ex.toString());
                                                }
                                                finally
                                                {
                                                    if (fs != null)
                                                        fs.close();
                                                }
                                            }
                                        }
                                    }
                                    catch (IOException ex)
                                    {
                                        logger.error(ex.getMessage(), ex);
                                    }
                                    finally
                                    {
                                        if (zs != null)
                                            zs.close();
                                        if (zfs != null)
                                            zfs.close();
                                    }
                                }
                                else
                                {
                                    //simply copy the file to data directory
                                    FileChannel source = null;
                                    FileChannel destination = null;
                                    source = new FileInputStream(f).getChannel();
                                    destination = new FileOutputStream(dataDir + File.separator + ((FileSource) o).getFileName()).getChannel();
                                    destination.transferFrom(source, 0, source.size());
                                }
                            }
                            catch (IOException ex)
                            {
                                logger.error(ex.getMessage(), ex);
                            }
                        }
                        go.setProgressBarValue(++curValue);
                    }
                }
                if (fileList.isEmpty()) {
                    go.showMessageDialog("No sources available for upload");
                    return new WebDAVError(0, "No sources available for upload");
                }
                try {
                    int numCommandFiles = 0;
                    // Process command files first
                    for (File f : fileList) {
                        if (!f.getName().toLowerCase().endsWith(".command")) {
                            continue;
                        }
                        numCommandFiles++;
                        wdu = new WebDavUploader(uname, pword, space, url, go, t.isRetryOnFailure() ? t.getNumRetries() : -1, this, false);
                        if (responseLogFile != null)
                            wdu.setResponseLogFile(responseLogFile);
                        UploadResult ur = new UploadResult();
                        WebDAVError error = wdu.uploadFile(f.getParent(), f.getName(), false, ur, false);
                        if (error != null) {
                            go.showMessageDialog("Error: " + (error.getMessage() != null ? error.getMessage() : "Unable to process command file"));
                            return new WebDAVError(0, "Error: " + (error.getMessage() != null ? error.getMessage() : "Unable to process command file"));
                        }
                        if (ur.responseString != null) {
                            exitUsingExitStatus = true;
                            exitStatus = -1;
                            if (ur.responseString.startsWith("Status ")) {
                                BufferedReader br = new BufferedReader(new StringReader(ur.responseString));
                                String statusLine = br.readLine();
                                br.close();
                                try {
                                    exitStatus = Integer.parseInt(statusLine.substring("Status ".length()));
                                } catch (NumberFormatException e) {
                                	if (logger.isTraceEnabled()) {
                                        logger.trace("Exception " + e + " occured while processing method DataConductorUtil.loadTask(Task, DataConductorConfig) ", e);
                                    }
                                }
                            }
                            logger.info(ur.responseString);                            
                        }
                        go.setProgressBarValue(++curValue);
                    }
                    if (fileList.size() - numCommandFiles > 0) {
                        // Process data files
                        for (File f : fileList) {
                            if (f.getName().toLowerCase().endsWith(".command")) {
                                continue;
                            }
                            if (!f.exists())
                            {
                                logger.warn("File does not exist, skipping: " + f.getAbsolutePath());
                            }
                            wdu = new WebDavUploader(uname, pword, space, url, go, t.isRetryOnFailure() ? t.getNumRetries() : -1, this, false);
                            if (responseLogFile != null)
                                wdu.setResponseLogFile(responseLogFile);
                            UploadResult ur = new UploadResult();
                            WebDAVError error = wdu.uploadFile(f.getParent(), f.getName(), true, ur, t.isLocalETLTask() && !f.getName().toLowerCase().endsWith(".zip"));
                            if (error != null) {
                                go.showMessageDialog("Error: " + (error.getMessage() != null ? error.getMessage() : "Unable to scan the file"));
                                return new WebDAVError(0, "Error: " + (error.getMessage() != null ? error.getMessage() : "Unable to scan the file"));
                            }
                            if (ur.responseString != null) {
                                //exitUsingExitStatus = true;
                                int uploadStatus = -1;
                                logger.info(ur.responseString);
                                if (ur.responseString.startsWith("Status ")) {
                                    BufferedReader br = new BufferedReader(new StringReader(ur.responseString));
                                    String statusLine = br.readLine();
                                    br.close();
                                    try {                                        
                                        uploadStatus = Integer.parseInt(statusLine.substring("Status ".length()));                                        
                                    } catch (NumberFormatException e) {
                                            logger.debug("Received incorrect upload status from server: " + statusLine.substring("Status ".length()));
                                    }
                                    if (uploadStatus != 0) {
                                        go.showMessageDialog("Error: Unable to import the definition for one or more sourcefiles. Please see the log for further details.");
                                        return new WebDAVError(WebDAVError.GENERAL_ERROR, "Unable to import the definition for one or more sourcefiles. Please see the log for further details.");
                                    }
                                }
                            }
                        }
                        logger.info("Uploaded and scanned the data files");
                        go.setProgressBarValue(++curValue);
                    }
                } catch (FileNotFoundException ex) {
                    go.showMessageDialog("File not found while uploading the data: " + ex.getMessage());
                    return new WebDAVError(0, "File not found while uploading the data: " + ex.getMessage());
                } catch (IOException ex) {
                    go.showMessageDialog("I/O Error occurred while uploading the data: " + ex.getMessage());
                    return new WebDAVError(0, "I/O Error occurred while uploading the data: " + ex.getMessage());
                }
            }
        } finally {
            // Cleanup
            cleanupTempDirectory(dirFile);
        }
        // Process if appropriate
        if (t.isProcessAutomatically())
        {
            sendProcessingCommand(t.isCreateQuickDashboards(), t.isConsolidateSources(), t.getSubGroups(), t.getDateShiftValue());
        }
        return null;
    }

    /*
     * Return a map of upload settings (first row header, begin and end skip) and list of files that match that upload setting
     */
    private Map<UploadSettings, List<Object>> getFileLists(Task t)
    {
        Map<UploadSettings, List<Object>> uploadMap = new HashMap<UploadSettings, List<Object>>();
        for (Object o : t.getSourceList())
        {
            UploadSettings us = null;
            if (QuerySource.class.isInstance(o))
            {
                us = ((QuerySource)o).getSettings();
            } else
            {
                us = ((FileSource) o).getSettings();
            }
            List<Object> list = uploadMap.get(us);
            if (list == null)
            {
                list = new ArrayList<Object>();
                uploadMap.put(us, list);
            }
            list.add(o);
        }
        return (uploadMap);
    }

    /**
     * return a directory and it's contents
     * @param dirFile
     */
    public void cleanupTempDirectory(File dirFile)
    {
        go.setStatusMessage("");
        if (dirFile == null || (isLocalDCConfigInUse() && dirFile.getAbsolutePath().equalsIgnoreCase(localDCConfigLocation)))
            return;
        try
        {
            for (File f : dirFile.listFiles())
            {
                if (f.isDirectory())
                    cleanupTempDirectory(f);
                else
                    f.delete();
            }
            dirFile.delete();
        } catch (Exception ex)
        {
            go.showMessageDialog("IO Error occurred cleaning up: " + ex.getMessage());
            return;
        }
    }
    
    public Map<String, String> getVariablesForAllQuerySources(List<Task> tasksToRun, DataConductorConfig config)
    {
          Map varMap=null;
          String queryList=null;
          
          for (Task t : tasksToRun)
          {
            Map<UploadSettings, List<Object>> ulist = getFileLists(t);
            for (Entry<UploadSettings, List<Object>> en : ulist.entrySet()) 
            {
                for (Object o : en.getValue()) 
                {
                        if (QuerySource.class.isInstance(o)) 
                        {
                                QuerySource qs = (QuerySource) o;
                                File f = null;
                                if (qs.type == QuerySource.TYPE_QUERY)
                                {
                                        queryList = queryList + ";" + qs.getQuery();
                                }
                                else
                                {
                                        continue;
                                }
                        }
                }
            }
        }
          
        logger.debug("Getting variables for all queries");  
        
        if (queryList != null && queryList.indexOf("V{") > 0) 
        {
            String vars = getVariables(queryList);
            varMap = sendGetVariablesCommand(vars);
        }
        return varMap;        
    }

    /**
     * send a processing command to the server
     * @param createQuickDashboards
     * @param consolidateSources
     * @param dateShift
     */
    public WebDAVError sendProcessingCommand(boolean createQuickDashboards, boolean consolidateSources, List<String> subgroups,
            int dateShift) {
        logger.info("Sending a processing request");
        File dirFile = null;
        // Upload data sources settings
        try {
            dirFile = getTempDirectory();
            File f = new File(dirFile.getPath() + File.separator + "birst.process");
            f.createNewFile();
            PrintWriter writer = new PrintWriter(new FileWriter(f));
            Calendar c = Calendar.getInstance();
            c.add(Calendar.DATE, dateShift);
            writer.println("loaddate=" + String.format("%1$tm/%1$td/%1$tY", c.getTime()));
            writer.println("createquickdashboards=" + createQuickDashboards);
            writer.println("dcconfigfilename=" + getDCConfigFileName());
            if (subgroups != null && !subgroups.isEmpty()) {
                writer.print("subgroups=");
                for (int i = 0; i < subgroups.size(); i++) {
                    if (i > 0) {
                        writer.print('\t');
                    }
                    writer.print(subgroups.get(i));
                }
                writer.println();
            }
            writer.close();
            WebDAVError werror = GetLoadStatus.get(uname, pword, space, url);
            if (werror != null) {
                return werror;
            }
            WebDavUploader wdu = new WebDavUploader(uname, pword, space, url, go, NUM_DEFAULT_TRIES, this, false);
            werror = wdu.uploadFile(dirFile.getPath(), "birst.process", false, null, false);
            if (werror != null) {
                return werror;
            }
        } catch (IOException ex) {
            go.showMessageDialog("Unable to save birst.process file: " + ex.getMessage());
            return new WebDAVError(0, "Unable to save birst.process file: " + ex.getMessage());
        } finally {
            cleanupTempDirectory(dirFile);
        }
        return null;
    }

     public Map<String, String> sendGetVariablesCommand(String variables)
    {
        logger.debug("request birst.variables: " + variables);
        String vars =  WebDavUploader.getVariables(url, space, variables);
        logger.debug("response birst.variables:" + vars);
        return getVarsMap(vars);
    }
     

     public Map<String, String> getVarsMap(String vars)
     {
        Map<String, String> varMap = new HashMap<String, String>();
        varMap.put("V{SPACE}", getSpace());
        //will override V{SPACE} if getting from vars
        try
        {
            if (vars.length() > 1)
            {
                List<String> variableList = new ArrayList<String>();
                String delimiter = "|V{";
                int idx = 0;
                while (vars.indexOf(delimiter, idx) != -1)
                {
                    int endIdx = vars.indexOf("|V{", idx+1);
                    if (endIdx == -1)
                    {
                        endIdx = vars.length()-1;
                    }
                    String variable = vars.substring(idx+1, endIdx);
                    idx = idx + variable.length() + 1;
                    variableList.add(variable);
                }
                for (String variable: variableList)
                {
                    int index = variable.indexOf('=');
                    varMap.put(variable.substring(0, index), variable.substring(index+1));
                }
            }
        } catch (Exception ex)
        {
            logger.error(ex.getMessage(), ex);
            return varMap;
        }
        return varMap;
     }
     
     private String getVariables(String query)
     {
        StringBuffer variableNames = new StringBuffer();
        List<String> varList = new ArrayList<String>();
        if (query != null)
        {
            boolean isFirst = true;
            Pattern p = Pattern.compile("V\\{.*?\\}");
            Matcher m = p.matcher(query);
            while (m.find()) 
            {
                String varName = m.group();
                varName = varName.substring(2, varName.indexOf("}"));
                if(!varList.contains(varName))
                {
                    if (isFirst)
                    {
                        isFirst = false;
                    }
                    else
                    {
                        variableNames.append(",");
                    }
                    variableNames.append(varName);
                    varList.add(varName);
                }
            }
        }
        return variableNames.toString();
     }

    /**
     * fetch the records in a table or query and save them to a file for uploading
     * @param directory
     * @param qs
     * @return File
     * @throws java.io.IOException
     * @throws java.sql.SQLException
     */
    private File getTable(String directory, QuerySource qs,Map varMap) throws IOException, SQLException
    {
        File f = new File(directory + File.separator + qs.getTableName().trim() + ".txt");
        StringBuilder message = new StringBuilder();
        long rowCount = 0;
        boolean dealwithUTF16BEFromEasySoft = false;
        Connection conn = null;
        try {
            if (qs.isGenericDriver())
            {
                //create jdbc connection using drivername, connectionstring, username and password
                conn = Database.getConnection(qs.getServerName(), qs.getDatabaseName(), qs.getUsername(), qs.retrievePassword(), message,null);
                if (qs.getServerName().equals("easysoft.sql.jobDriver") && qs.getDatabaseName().indexOf("unicode=off") < 0)
                {
                    dealwithUTF16BEFromEasySoft = true;
                    go.setStatusMessage("Using UTF16BE hack");
                }
            }
            else
            {
                conn = Database.getConnection(qs.getDriverName(), QuerySource.getConnectString(qs.getDriverName(), qs.getServerName(), qs.getDatabaseName(), qs.getPort(), null, null, false, null,qs.getSshTunnel()), qs.getUsername(), qs.retrievePassword(), message,qs.getSshTunnel());
            }
            if (conn == null) {
                logger.error(message.toString());
                throw new SQLException(message.toString());
            }

            // sometimes you need this to get setFetchSize to work properly
            // http://stackoverflow.com/questions/1468036/java-jdbc-ignores-setfetchsize
            // http://jdbc.postgresql.org/documentation/83/query.html#query-with-cursor
            DatabaseMetaData dbMetaData = conn.getMetaData();
            if (dbMetaData != null)
            {
                if (dbMetaData.supportsTransactions())
                {
                    conn.setAutoCommit(false);
                }
            }
            
            Statement stmt = null;
            boolean isDbHiveImpala = qs.getDriverName().equals("org.apache.hadoop.hive.jdbc.HiveDriver") || qs.getDriverName().equals("org.apache.hive.jdbc.HiveDriver");
            if (isDbHiveImpala)
                stmt = conn.createStatement();
            else
                stmt = conn.createStatement(java.sql.ResultSet.TYPE_FORWARD_ONLY, java.sql.ResultSet.CONCUR_READ_ONLY);
            ResultSet rs = null;
            int size = qs.getFetchSize(false);
            if (size != 0) 
            {
                try
                {
                    stmt.setFetchSize(size);
                }
                catch (Exception ex)
                {
                    logger.debug("Could not set fetch size to: " + size);
                }
            }
            String query = null;
            if (qs.type == QuerySource.TYPE_TABLE) {
                query = "SELECT * FROM "
                        + (qs.getSchemaName() != null && qs.getSchemaName().length() > 0 ? qs.getSchemaName() + "." : "")
                        + qs.getQueryTableName();
            } else {
                query = qs.getQuery();
                if (query != null && query.indexOf("V{") > 0) {
                    go.setStatusMessage("Query prior to variable substitution: " + query);
                    //Pattern V{...} means V{... and first(samllest possible) match for }
                    Pattern p = Pattern.compile("V\\{.*?\\}");
                    Matcher m = p.matcher(query);
                    while (m.find()) {
                        String varName = m.group();
                        //do not replace variable if invalid/wrong variable name is used
                        if (varMap.containsKey(varName)) {
                            String value = (String) varMap.get(varName);
                            query = query.replace(varName, value);
                        } else {
                            logger.warn("No variable replacement for: " + varName);
                        }
                    }
                }
            }
            
            go.setStatusMessage("Query: " + query);
            rs = stmt.executeQuery(query);
            
            //bugfix 7288.
            BufferedWriter writer = null;
            try {
                writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(f), "UTF-8"));

                ResultSetMetaData rsmd = rs.getMetaData();
                int numColumns = rsmd.getColumnCount();
                int columnTypes[] = new int[numColumns + 1];

                // Write headers
                for (int i = 1; i <= numColumns; i++) {
                    columnTypes[i] = rsmd.getColumnType(i);
                    if (i > 1) {
                        writer.write('|');
                    }
                    String label = rsmd.getColumnLabel(i);
                    logger.debug(label + ", " + columnTypes[i]);
                    writer.write(label);
                }
                writer.write("\r\n");
                // Write data
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
                Charset utf16be = Charset.forName("UTF-16LE");
                while (rs.next()) {
                    for (int i = 1; i <= numColumns; i++) {
                        if (i > 1) {
                            writer.write('|');
                        }
                        Object o = null;
                        try {
                            switch (columnTypes[i]) {

                                case Types.NCHAR:
                                case Types.NVARCHAR:
                                case Types.LONGNVARCHAR:
                                case Types.CHAR:
                                case Types.VARCHAR:
                                case Types.LONGVARCHAR:
                                case Types.ROWID:
                                    o = rs.getString(i);
                                    if (dealwithUTF16BEFromEasySoft && o != null)
                                    {
                                        // EasySoft encodes Unicode as UTF-16LE as two characters rather than 1
                                        String x = o.toString();
                                        byte[] rawBytes = new byte[x.length()];
                                        for (int z = 0; z < x.length(); z++)
                                        {
                                            rawBytes[z] = (byte) (((int) x.charAt(z)) & 0xFF);
                                        }
                                        o = new String(rawBytes, utf16be);
                                    }
                                    break;
                                case Types.TIMESTAMP:
                                    if (isDbHiveImpala)
                                        o = rs.getObject(i); // getTimestamp not implemented for Hive/Impala
                                    else
                                        o = rs.getTimestamp(i);
                                    break;
                                default:
                                    o = rs.getObject(i);
                            }

                        } catch (SQLException sex) {
                            // MySQL can cause an exception if a timestamp is not valid; e.g., 0000-00-00 00:00:00
                            logger.warn(sex.getMessage());
                        }
                        if (o != null) {
                            if (Timestamp.class.isInstance(o)) {
                                writer.write(sdf.format((Timestamp) o));
                            } else {
                                String s = o.toString();
                                if (s.length() > 0 && !(s.charAt(0) == '"' && s.charAt(s.length() - 1) == '"')
                                        && (s.indexOf('\r') >= 0 || s.indexOf('\n') >= 0 || s.indexOf('|') >= 0 || s.indexOf('"') >= 0 || s.length() == 1)) {
                                    StringBuilder sb = new StringBuilder();
                                    for (int j = 0; j < s.length(); j++) {
                                        if (s.charAt(j) == '\\' || s.charAt(j) == '"' || s.charAt(j) == '|') {
                                            sb.append('\\');
                                        }
                                        sb.append(s.charAt(j));
                                    }
                                    writer.write('"' + sb.toString() + '"');
                                } else {
                                    writer.write(s);
                                }
                            }
                        }
                    }
                    writer.write("\r\n");
                    if (rowCount > 0 && rowCount % 10000 == 0) {
                        if (qs.getType() == QuerySource.TYPE_TABLE) {
                            go.setStatusMessage("Extracting table: " + qs.getTableName() + " (" + rowCount + ")");
                        } else {
                            go.setStatusMessage("Extracting query (" + rowCount + ")");
                        }
                    }
                    rowCount++;
                }
            } finally {
                if (writer != null) {
                    writer.close();
                }
            }
        } catch (Exception ex)
        {
            Logger.getLogger(AddDatabaseSource.class).error(null, ex);
            throw ex;
        } finally {
            try {
                if (conn != null) {
                    conn.close();
                }
                 if(qs != null && qs.getSshTunnel() != null)
                    {
                        qs.getSshTunnel().closeSession();    
                    }
            } catch (Exception ex) {
                logger.debug("Close: " + ex.getMessage());
            }
        }
 
        if (qs.getType() == QuerySource.TYPE_TABLE)
        {
            go.setStatusMessage("Extracted table: " + qs.getTableName() + " (" + rowCount + ")");
        } else
        {
            go.setStatusMessage("Extracted query (" + rowCount + ")");
        }
        return f;
    }

    /**
     * retrieve the configuration settings from the server (such as a list of tasks)
     */
    
    public DataConductorConfig setConfig() throws MalformedURLException, IOException, GeneralErrorException, AuthenticationException
    {
        return setConfig(false);
    }

    public DataConductorConfig setConfig(boolean redirect) throws MalformedURLException, IOException, GeneralErrorException, AuthenticationException
    {
        if (Util.USE_TEST_PROXIES)
        {
            // install test proxy settings
            BirstProxySelector ps = new BirstProxySelector(ProxySelector.getDefault());
            ProxySelector.setDefault(ps);
        }
        Util.dumpMatchingProxies(url);
        if (Util.USE_TEST_PROXIES)
            Util.dumpMatchingProxies("https://beta.birst.com");

        CookieManager manager = new CookieManager();
        manager.setCookiePolicy(CookiePolicy.ACCEPT_ALL);
        CookieHandler.setDefault(manager);
        
        BirstAuthenticator ba = new BirstAuthenticator();

        ba.setServerUsername(uname);
        ba.setServerPassword(pword);

        ba.setProxyUsername(System.getProperty("jnlp.proxy.username"));
        ba.setProxyPassword(EncryptionService.getInstance().decrypt(System.getProperty("jnlp.proxy.password"), key));

        Authenticator.setDefault(ba);
        
        // probe server with BC version, get back redirect URL based upon BC version - how does this work if there is a proxy auth?
        if (redirect)
            url = Util.getRedirectURL(url);
        
        DataConductorConfig config = GetConfig.get(uname, pword, space, url, dcConfigFileName);
        if (config == null)
        {
            config = new DataConductorConfig();
        } else
        {
            if (config.getLogFileName() != null)
            {
                activityLog.setLogFile(config.getLogFileName());
            }
        }
        if (isLocalDCConfigInUse() && config.getRealtimeConnections().size() > 0)
        {
            logger.warn("Birst Connect is using local dconfig and any changes to realtime connection/s will not be saved on server");
        }
        return config;
    }

    /**
     * iterate through a comma separated list of tasks
     * @param taskList
     */
    private void processTasks(String taskList, DataConductorConfig config) throws GeneralErrorException
    {
        // crack the task list
        StringTokenizer st = new StringTokenizer(taskList, ",");
        List<String> tasks = new ArrayList<String>();
        List<String> verifiedTasks = new ArrayList<String>();
        List<Task> tasksToRun = new ArrayList<Task>();
        Map varMap=null;
        
        while (st.hasMoreTokens())
        {
            tasks.add(st.nextToken());
        }
        boolean downloadCloudSources = false;
        for (Task t : config.getTaskList())
        {
            if (tasks.contains(t.getName()))
            {
                verifiedTasks.add(t.getName());
                if (t.isDowanloadCloudSources())
                    downloadCloudSources = true;                
            }
        }
        if(tasks.size() != verifiedTasks.size())
        {
            tasks.removeAll(verifiedTasks);
            for(int i=0;i<tasks.size();i++)
            {
                logger.error("Task name '"+tasks.get(i) + "' not found");    
            }
            
            Util.exitApp(1, false);
        }
       
       	for (Task t : config.getTaskList())
        {
            if (tasks.contains(t.getName()))
            {
        	tasksToRun.add(t);
            }
	}
	 
	varMap=getVariablesForAllQuerySources(tasksToRun, config);
	 
	 for (Task t : config.getTaskList())
	 {
            if (tasks.contains(t.getName()))
            {
                logger.info("Uploading task: " + t.getName());
	        WebDAVError werror = loadTask(t, config,varMap);
                if (werror != null)
                {
                    logger.warn("Failed to complete task: " + t.getName());
                    throw new GeneralErrorException("Failed to complete task: " + t.getName());
                }
                if (downloadCloudSources && t.isDowanloadCloudSources())
                {
                    downloadCloudSources = false;
                    CloudDataDownloader cdd = new CloudDataDownloader(uname, pword, space, url, config.getLocalETLConfig());
                    werror = cdd.downloadData(CloudDataDownloader.RequestType.DownloadCloudData);
                    if (werror != null)
                    {
                        logger.warn("Failed to complete task: " + t.getName());
                        throw new GeneralErrorException("Failed to complete task: " + t.getName());
                    }
                }
                logger.info("Completed task: " + t.getName());
            }
            logger.info("All tasks complete");
        }
    }

    public WebDavResponse sendCheckStatusCommand()
    {
        return WebDavUploader.checkStatus(url, space);
    }
    /**
     * process the command line arguments
     * 
     * open for javaws, -tasks or -open for command line
     * @param args
     * @return
     */
    public boolean processArgs(String[] args, DataConductorConfig config)
    {
        for (int i = 0; i < args.length; i++)
            System.out.println(args[i]);
        if (args.length > 1 && (args[0].equals("-open") || args[0].equals("-tasks") || args[0].equals("-getreportresults") ||
                args[0].toLowerCase().equals("-generatebulkqueryresults") || args[0].toLowerCase().equals("-downloadbulkqueryresults")))
        {
            if (args[1].equals("process"))
            {
                WebDAVError result = sendProcessingCommand(false, false, null, 0);
                if (result != null)
                {
                    // Exit with error code
                    if (result.getCode() == WebDAVError.CURRENTLY_PROCESSING)
                        Util.exitApp(2, false);
                    else
                    // Exit with error code
                        Util.exitApp(1, false);
                }
            } else if (args[1].equals("-checkstatus"))
            {
                WebDavResponse wdr = sendCheckStatusCommand();
                if (wdr == null)
                    Util.exitApp(WebDavResponse.NO_SERVER_RESPONSE, false);
                else
                    Util.exitApp(wdr.getCode(), false);
            } else if (args[1].toLowerCase().equals("-liveaccess")) {
                try
                {
                    //start live access threads
                    if (!hasRealtime())
                    {
                        System.out.println("LiveAccess Support not found");
                        Util.exitApp(1, false);
                    }
                    Map<String, RealtimeConnection> rcs = config.getRealtimeConnections();
                    if (rcs == null || rcs.size() == 0)
                    {
                        System.out.println("No LiveAccess Connections found");
                        Util.exitApp(1, false);
                    }
                    Map<String, Boolean> statusMap = new HashMap<String, Boolean>();
                    Thread[] realTimeThreads = new Thread[rcs.size()];
                    int noThread = 0;
                    for (String s : rcs.keySet())
                    {
                        RealtimeConnection rc = rcs.get(s);
                        if (rc.getName() == null)
                        {
                            rc.setName(s);
                        }
                        Thread t = new Thread(new RealtimeThread(rcs.get(s), null, config, this, s, activityLog, statusMap));
                        realTimeThreads[noThread++] = t;
                        t.start();
                    }
                    System.out.println("Running BirstConnect with LiveAccess in headless mode...");
                    //join threads to run DataConductor infinitely
                    for (int i=0; i< rcs.size(); i++)
                    {
                        realTimeThreads[i].join(); 
                    }
                }
                catch (Exception ex)
                {
                    // Exit with error code
                    logger.debug(ex, ex);
                    Util.exitApp(1, false);
                }
                Util.exitApp(0, false);
            } else if (args[0].toLowerCase().equals("-getreportresults"))
            {
                if (args.length != 3)
                {
                    System.out.println("Invalid command (missing arguments to -getreportresults)");
                    // Exit with error code
                    Util.exitApp(1, false);
                }
                GetReportResults grr = new GetReportResults(args[1], args[2], this, go);
                try
                {
                    grr.run();
                }
                catch (Exception ex)
                {
                    // Exit with error code
                    logger.debug(ex, ex);
                    Util.exitApp(1, false);
                }
                Util.exitApp(0, false);
            } else if (args[0].toLowerCase().equals("-generatebulkqueryresults") || args[0].toLowerCase().equals("-downloadbulkqueryresults"))
            {
                try
                {
                    if (args[0].toLowerCase().equals("-generatebulkqueryresults"))
                    {
                        if (args.length < 3 )
                        {
                            System.out.println("Invalid command arguments to -generatebulkqueryresults");
                            // Exit with error code
                            Util.exitApp(1, false);
                        }
                        String query = null,reportpath = null;
                        if (args[1].toLowerCase().startsWith("query="))
                        {
                            query = args[1].substring("query=".length());
                            if (!query.startsWith("\""))
                                query = "\"" + query;
                            if (!query.endsWith("\""))
                                query = query + "\"";
                        }
                        else if (args[1].toLowerCase().startsWith("reportpath="))
                        {
                            reportpath = args[1].substring("reportpath=".length());
                            if (!reportpath.startsWith("\""))
                                reportpath = "\"" + reportpath;
                            if (!reportpath.endsWith("\""))
                                reportpath = reportpath + "\"";
                        }
                        else
                        {
                            System.out.println("Invalid command arguments to -generatebulkqueryresults (query or reportpath must be specified)");
                            Util.exitApp(1, false);
                        }
                        String filename = null;
                        if (args[2].toLowerCase().startsWith("filename="))
                        {
                            filename = args[2].substring("filename=".length());
                            if (!filename.startsWith("\""))
                                filename = "\"" + filename;
                            if (!filename.endsWith("\""))
                                filename = filename + "\"";
                        }
                        else
                        {
                            System.out.println("Invalid command arguments to -generatebulkqueryresults (filename must be specified)");
                            Util.exitApp(1, false);
                        }                        
                        int numRows = 250000;
                        boolean useLogicalQuery = false;
                        if (args.length > 3)
                        {
                            for (int i = 3; i < args.length; i++)
                            {
                                if (args[i].toLowerCase().startsWith("numrows="))
                                {
                                    String rows = args[i].substring("numrows=".length());
                                    try
                                    {
                                        numRows = Integer.parseInt(rows);
                                    }
                                    catch(NumberFormatException nfe)
                                    {
                                        System.out.println("Invalid command arguments to -generatebulkqueryresults (invalid value for numrows specified) " + rows);
                                        Util.exitApp(1, false);
                                    }
                                }
                                else if (args[i].toLowerCase().startsWith("uselogical="))
                                {
                                    useLogicalQuery = Boolean.parseBoolean(args[i].substring("uselogical=".length()));
                                }
                                else
                                {
                                    System.out.println("Invalid command arguments to -generatebulkqueryresults : " + args[i]);
                                    Util.exitApp(1, false);
                                }
                            }
                        }
                        if (numRows > 1000000)
                        {
                            System.out.println("numrows = " + numRows + " : cannot exceed 1000000");
                            Util.exitApp(1, false);
                        }
                        if (numRows > 250000 && useLogicalQuery)
                        {
                            System.out.println("Cannot use logicalquery when numrows exceeds 250000");
                            Util.exitApp(1, false);
                        }
                        executeGenerateBulkQueryResultsCommmand(query, reportpath, filename, numRows, useLogicalQuery);
                        System.out.println("Bulk Query Results generated successfully on server");
                    }
                    else
                    {
                        if (args.length != 3 )
                        {
                            System.out.println("Invalid command arguments to -downloadbulkqueryresults");
                            // Exit with error code
                            Util.exitApp(1, false);
                        }
                        String filename = args[1],pathToSave = args[2];
                        CloudDataDownloader cdd = new CloudDataDownloader(uname, pword, space, url, null);
                        List<String> sources = new ArrayList<String>();
                        sources.add(filename);
                        cdd.setSources(sources);
                        cdd.fileToSave = pathToSave;
                        cdd.downloadData(CloudDataDownloader.RequestType.DownloadBulkQueryResults);
                    }
                }
                catch (Exception ex)
                {
                    // Exit with error code
                    Util.exitApp(1, false);
                }
                Util.exitApp(0, false);
            } else
            {
                try
                {
                    processTasks(args[1], config);
                    if (exitUsingExitStatus)
                    {
                        Util.exitApp(exitStatus, false);
                    }
                }
                catch (Exception ex)
                {
                    // Exit with error code
                    Util.exitApp(1, false);
                }
            }
            return true; // processed args
        }
        return false; // no args processed
    }
    
    private void executeGenerateBulkQueryResultsCommmand(String query, String reportpath, String filename, int numRows, boolean useLogicalQuery) throws Exception
    {
        File dirFile = null;
        // Create command file
        try
        {
            PrintWriter writer = null;
            try
            {
                dirFile = getTempDirectory();
                File f = new File(dirFile.getPath() + File.separator + "birst.generatebulkqueryresults.command");
                f.createNewFile();
                writer = new PrintWriter(new FileWriter(f));
                writer.println("generatebulkqueryresults " + (query != null ? "query=" + query : "reportpath=" + reportpath) +
                        " filename=" + filename + " numrows=" + numRows + " uselogical=" + useLogicalQuery);
            }
            catch (Exception ex2)
            {
                if (go != null) {
                    go.showMessageDialog("Unable to write the command file: " + ex2.getMessage());
                }
                throw new GeneralErrorException("Unable to write the command file: " + ex2.getMessage());
            }
            finally
            {
                writer.close();
            }
            WebDavUploader wdu = new WebDavUploader(getUname(), getPword(), getSpace(), getUrl(), go, NUM_DEFAULT_TRIES, this, false);
            UploadResult result = new UploadResult();
            WebDAVError werror = wdu.uploadFile(dirFile.getPath(), "birst.generatebulkqueryresults.command", false, result, false);
            if (werror != null)
            {
                if (go != null) {
                    go.showMessageDialog("Unable to upload the command file: " + werror.getMessage());
                }
                throw new GeneralErrorException("Unable to upload the command file: " + werror.getMessage());
            }
            werror = wdu.pollForStatus("birst.generatebulkqueryresults.command", "commandfile execution", WebDavUploader.PHASE_POLL);
            if (werror != null)
            {
                if (go != null) {
                    go.showMessageDialog("Unable to get polling status for commandfile execution: " + werror.getMessage());
                }
                throw new GeneralErrorException("Unable to get polling status for commandfile execution: " + werror.getMessage());
            }
            if (result.responseString != null && !result.responseString.isEmpty())
            {
                if (result.responseString != null) {
                    exitStatus = -1;
                    if (result.responseString.startsWith("Status ")) {
                        BufferedReader br = new BufferedReader(new StringReader(result.responseString));
                        String statusLine = br.readLine();
                        br.close();
                        try {
                            exitStatus = Integer.parseInt(statusLine.substring("Status ".length()));
                        } catch (NumberFormatException e) {
                        	if (logger.isTraceEnabled()) {
                                logger.trace("Exception " + e + " occured while processing method DataConductorUtil.executeGenerateBulkQueryResultsCommmand(String, String, String, int, boolean) ", e);
                            }
                        }
                    }
                    logger.debug(result.responseString);
                }
                if (exitStatus != 0)
                {
                    if (go != null) {
                        go.showMessageDialog("Failed to geneate bulk query results, see logs for details");
                    }
                    throw new GeneralErrorException("Failed to geneate bulk query results, see logs for details");
                }
            }
        }
        catch (Exception ex) {
            if (go != null) {
                go.showMessageDialog("Unable to geneate bulk query results: " + ex.getMessage());
            }
            throw new Exception("Unable to geneate bulk query results: " + ex.getMessage());
        } finally {
            cleanupTempDirectory(dirFile);
        }
    }

    public static int getDataType(String dType)
    {
        if ("Double".equalsIgnoreCase(dType))
            return java.sql.Types.DOUBLE;
        else if ("Number".equalsIgnoreCase(dType))
            return java.sql.Types.NUMERIC;
        else if ("Integer".equalsIgnoreCase(dType))
            return java.sql.Types.INTEGER;
        else if ("Float".equalsIgnoreCase(dType))
            return java.sql.Types.FLOAT;
        else if ("Date".equalsIgnoreCase(dType))
            return java.sql.Types.DATE;
        else if ("DateTime".equalsIgnoreCase(dType))
            return java.sql.Types.TIMESTAMP;
        return java.sql.Types.VARCHAR;
    }
    
    public static boolean isLocalDCConfigInUse()
    {
        return localDCConfigLocation != null && localDCConfigLocation.trim().length() > 0;
    }
}
