/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.birst.dataconductor;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.Proxy;
import java.net.ProxySelector;
import java.net.SocketTimeoutException;
import java.net.URI;
import java.net.URL;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookieStore;
import java.net.HttpCookie;
import java.net.URLConnection;
import java.util.List;
import java.util.Map;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSession;
import java.net.InetAddress;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;

import java.io.IOException;
import java.io.InputStream;

import java.util.HashMap;
import javax.swing.JLabel;
import org.apache.log4j.Logger;

import javax.swing.JList;
import java.awt.Color;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
/**
 *
 * @author ricks
 */
public class Util {
    private static final String PROTOCOL_VERSION = "2.0";   // this needs to be kept in sync with birstConnectProtocolVersion in web.config in Acorn

    public static final boolean USE_TEST_PROXIES = false;   // put Birst test proxies in the list and force them to be used
    public static final boolean DEBUG = true;               // turns on debug logging
    public static final boolean DUMP_COOKIES = true;

    public static final String NEW_LINE_CHAR = System.getProperty("line.separator");
    public static final String MAIN_SCHEMA = "dbo";
    public static final String TIME_TABLE_NAME = "DW_DM_TIME_MONTH";
    public static final String TXN_COMMAND_HISTORY_TABLE_NAME = "TXN_COMMAND_HISTORY";
    public static final int RESPONSE_CODE_SPACE_UNAVAILABLE = 601;
    
    private final static Logger logger = Logger.getLogger(Util.class);
    public static final String WARNING_IMG_ICON_PATH = "resources/warning.png";
    public static final String DRIVER_NOT_INSTALLED_WARN_MESSAGE = "Warning: Selected database driver is not installed";
    
    public static String getUserAgent()
    {
        return "Birst-WebDAV/" + PROTOCOL_VERSION;
    }

    /*
     * dummy hostname verifier - no verification
     */
    private static class NoOpHNVerifier implements HostnameVerifier
    {

        public boolean verify(String arg0, SSLSession arg1)
        {
            return true;
        }
    }

    /*
     * dump to the log the list of proxies that match the url
     */
    public static void dumpMatchingProxies(String url)
    {
        if (logger.isInfoEnabled()) {
            try {
                List<Proxy> proxies = ProxySelector.getDefault().select(new URI(url));
                StringBuilder sb = new StringBuilder("-- Proxies for ");
                sb.append(url);
                sb.append(" --");
                if (proxies == null || proxies.isEmpty()) {
                    logger.info("\n\tNo proxies");
                    return;
                }
                for (Proxy proxy : proxies) {
                    sb.append("\n\t");
                    sb.append(proxy.toString());
                }
                logger.info(sb.toString());
            } catch (Exception ex) {
                logger.error(ex.getMessage(), ex);
                //logStackTrace(ex);
            }
        }
    }

    /*
     * dump to the log the http response
     */
    public static void dumpHttpResponse(HttpURLConnection hs) throws SpaceUnavailableException
    {
        try {
            int code = hs.getResponseCode();
            // good place to check for version mismatch
            if (code == HttpURLConnection.HTTP_PRECON_FAILED) {
                logger.error("Version mismatch: Birst Connect is out of date with respect to the server.  Please log into the Birst Customer Support Portal at http://birst.custhelp.com to get a new version of Birst Connect.");
                exitApp(-100);
            }
            if (code == HttpURLConnection.HTTP_OK) {                
                   return; // no need to log success
            }
            if (code == Util.RESPONSE_CODE_SPACE_UNAVAILABLE) {
                throw new SpaceUnavailableException("Space Unavailable: Space is involved in either swap, delete or copy operation.");
            }
            if (logger.isDebugEnabled()) {

                StringBuilder header = new StringBuilder();
                header.append("-- HTTP Response --");
                header.append("\n\tRequest: ");
                header.append(hs.getRequestMethod());
                header.append(' ');
                header.append(hs.getURL());
                header.append("\n\tCode: ");
                header.append(code);
                header.append("\n\tMessage: ");
                header.append(hs.getResponseMessage());
                Map<String, List<String>> props = hs.getHeaderFields();
                for (String key : props.keySet()) {
                    StringBuilder sb = new StringBuilder();
                    if (key == null) {
                        continue;
                    }
                    List<String> lst = props.get(key);
                    for (int i = 0; i < lst.size(); i++) {
                        if (sb.length() > 0) {
                            sb.append(',');
                        }
                        sb.append(lst.get(i));
                    }
                    header.append("\n\t");
                    header.append(key);
                    header.append(": ");
                    header.append(sb);
                }
                logger.debug(header.toString());                
            }            
        } catch (SocketTimeoutException ex) {
            // expected timeout for live access
        	if (logger.isTraceEnabled()) {
                logger.trace("Exception " + ex + " occured while processing method Util.dumpHttpResponse(HttpURLConnection)", ex);
            }
        } catch (SpaceUnavailableException ex)
        {
            throw ex;
        }
        catch (Exception ex) {
            // XXX logger.error(ex.getMessage());
        	if (logger.isTraceEnabled()) {
                logger.trace("Exception " + ex + " occured while processing method Util.dumpHttpResponse(HttpURLConnection)", ex);
            }
        }
    }

    public static void logResponse(HttpURLConnection hs)
    {
        BufferedReader in = null;
        InputStreamReader isr = null;
        try
        {
            isr = new InputStreamReader(hs.getInputStream());
            in = new BufferedReader(isr);
            String inputLine;
            while ((inputLine = in.readLine()) != null)
            {
                logger.info(inputLine);
            }
        }
        catch (IOException ioex)
        {
            logger.debug(ioex.getMessage(), ioex);
        }
        finally
        {
            try
            {
                if (in != null)
                    in.close();
                if (isr != null)
                    isr.close();
            }
            catch (IOException ioex)
            {
            	if (logger.isTraceEnabled()) {
                    logger.trace("Exception " + ioex + " occured while processing method Util.logResponse(HttpURLConnection)", ioex);
                }
            }
        }
    }

    public static void dumpCookies()
    {
        CookieManager manager = (CookieManager) CookieHandler.getDefault();
        if (manager == null)
            return;
        CookieStore cookieJar = manager.getCookieStore();
        if (cookieJar == null)
            return;
        List<HttpCookie> cookies = cookieJar.getCookies();
        logger.info("Cookie count: " + cookies.size());
        for (HttpCookie cookie : cookies) {
            logger.info("Cookie: " + cookie.toString());
        }
    }
    
    public static String getRedirectURL(String url)
    {
        try
        {
            String overrideVersion = System.getProperty("jnlp.version");
            String path = "/BCRedirect.aspx?birstconnect.version=" + (overrideVersion != null ? overrideVersion : DataConductorUtil.getBirstConnectVersion());
            HttpURLConnection hs = Util.getHttpURLConnection(url, path, "GET", true, true);
            hs.connect();
            Util.dumpHttpResponse(hs);
            InputStream is = hs.getInputStream();
            BufferedReader br = null;
            try 
            {
                br = new BufferedReader(new InputStreamReader(is));
                String inputLine = br.readLine();
                if (inputLine == null) {
                    logger.info("No redirect URL returned, using original: " + url);
                    return url;
                }
                inputLine = inputLine.trim();
                if (!inputLine.startsWith("http")) {
                    logger.info("Malformed redirect URL (" + inputLine + "), using original: " + url);
                    return url;
                }
                logger.info("Redirect URL: " + inputLine);
                return inputLine;
            } 
            finally
            {
                if (br != null) {
                    try {
                        br.close();
                    } catch (Exception ex) {
                    	if (logger.isTraceEnabled()) {
                            logger.trace("Exception " + ex + " occured while processing method Util.getRedirectURL(String) ", ex);
                        }
                    }
                }
            }
        } 
        catch (Exception ex)
        {
            logger.info("No redirect URL (exception), using original: " + url);
            return url;
        }
    }

    /*
     * get an http connection, include authorization information
     */
    
    public static HttpURLConnection getHttpURLConnection(String url, String path, String method, boolean ignoreSpaceInUseCheck) throws MalformedURLException, IOException, GeneralErrorException, AuthenticationException
    {
        return getHttpURLConnection(url, path, method, ignoreSpaceInUseCheck, false);
    }
    public static HttpURLConnection getHttpURLConnection(String url, String path, String method, boolean ignoreSpaceInUseCheck, boolean explicitURL) throws MalformedURLException, IOException, GeneralErrorException, AuthenticationException
    {
        if (DUMP_COOKIES)
            Util.dumpCookies();
        URL urlObj = explicitURL ? new URL(url + path) : new URL(url + "/BirstConnect" + path);
        logger.debug(urlObj.toString());
        HttpsURLConnection.setDefaultHostnameVerifier(new NoOpHNVerifier());
        URLConnection s = urlObj.openConnection();
        HttpURLConnection hs = (HttpURLConnection) s;
        if (hs instanceof HttpsURLConnection)
        {
            // turn off validation of host names and certificates
            try
            {
                TrustManager[] trustAll = new javax.net.ssl.TrustManager[]
                {
                    new javax.net.ssl.X509TrustManager()
                    {
                        public java.security.cert.X509Certificate[] getAcceptedIssuers()
                        {
                            return null;
                        }
                        public void checkClientTrusted(java.security.cert.X509Certificate[] certs, String authType)
                        {
                        }
                        public void checkServerTrusted(java.security.cert.X509Certificate[] certs, String authType)
                        {
                        }
                    }
                };
                SSLContext sslContext = SSLContext.getInstance("SSL");
                sslContext.init(null, trustAll, new java.security.SecureRandom());
                SSLSocketFactory sf = sslContext.getSocketFactory();
                ((HttpsURLConnection) hs).setSSLSocketFactory(sf);
            }
            catch (Exception ex)
            {
                logger.error(ex.getMessage(), ex);
                //logStackTrace(ex);
            }
        }

        hs.setDoInput(true);
        hs.setDoOutput(true);
        hs.setUseCaches(false);
        hs.setRequestMethod(method);
        hs.setRequestProperty("User-Agent", getUserAgent());
        hs.setRequestProperty("Host", getHostFromURL(url));
        hs.setRequestProperty("IgnoreSpaceInUseCheck", String.valueOf(ignoreSpaceInUseCheck));

        return hs;
    }
    
    public static HttpURLConnection getExternalHttpURLConnection(String url, String method, boolean ignoreBadServerCertificate) throws MalformedURLException, IOException, GeneralErrorException, AuthenticationException
    {
        if (DUMP_COOKIES)
            Util.dumpCookies();
        URL urlObj = new URL(url);
        logger.debug(urlObj.toString());
        HttpsURLConnection.setDefaultHostnameVerifier(new NoOpHNVerifier());
        URLConnection s = urlObj.openConnection();
        HttpURLConnection hs = (HttpURLConnection) s;
        if (hs instanceof HttpsURLConnection) {
            if (ignoreBadServerCertificate) {
                // turn off validation of host names and certificates
                try {
                    TrustManager[] trustAll = new javax.net.ssl.TrustManager[]{
                        new javax.net.ssl.X509TrustManager() {

                            public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                                return null;
                            }
                            public void checkClientTrusted(java.security.cert.X509Certificate[] certs, String authType) {
                            }
                            public void checkServerTrusted(java.security.cert.X509Certificate[] certs, String authType) {
                            }
                        }
                    };
                    SSLContext sslContext = SSLContext.getInstance("SSL");
                    sslContext.init(null, trustAll, new java.security.SecureRandom());
                    SSLSocketFactory sf = sslContext.getSocketFactory();
                    ((HttpsURLConnection) hs).setSSLSocketFactory(sf);
                } catch (Exception ex) {
                    logger.error(ex.getMessage(), ex);
                }
            }
        }

        hs.setDoInput(true);
        hs.setDoOutput(true);
        hs.setUseCaches(false);
        hs.setRequestMethod(method);

        return hs;
    }

    /*
     * get the hostname from the URL
     */
    public static String getHostFromURL(String url) throws MalformedURLException
    {
        URL urlObj = new URL(url);
        return urlObj.getHost();
    }
    
    public static File copyURLtoFile(String directory, QuerySource qs, Map varMap) throws Exception
    {
        File file = null;
        InputStream is = null;        
        FileOutputStream fos = null;
        
        try
        {
            String url = qs.getUrl();
            
            logger.debug("URL prior to variable substitution: " + url);
            //Pattern V{...} means V{... and first(samllest possible) match for }
            Pattern p = Pattern.compile("V\\{.*?\\}");
            Matcher m = p.matcher(url);
            while (m.find()) {
                String varName = m.group();
                //do not replace variable if invalid/wrong variable name is used
                if (varMap.containsKey(varName)) {
                    String value = (String) varMap.get(varName);
                    url = url.replace(varName, value);
                } else {
                    logger.info("No variable replacement for: " + varName);
                }
            }
            
            HttpURLConnection hs = getExternalHttpURLConnection(url, qs.getMethod(), qs.getIgnoreBadServerCertificates());
            hs.connect();
            Util.dumpHttpResponse(hs);
            String contentType = hs.getContentType();
            file = new File(directory + File.separator + qs.getTableName().trim() + getExtension(contentType)); 

            is = hs.getInputStream();
            fos = new FileOutputStream(file);

            byte[] buf = new byte[1024];
            int len;
            while ((len = is.read(buf)) > 0){
                fos.write(buf, 0, len);
            }
            return file;
        }
        catch (Exception ex)
        {
            logger.error(ex.getMessage());
            throw new Exception(ex.getMessage(),ex.getCause());
        }        
        finally
        {
            if (fos != null)
                try { fos.close(); } catch (Exception ex) {};
            if (is != null)
                try { is.close(); } catch (Exception ex) {};
        }
    }
    
    private static String getExtension(String contentType) throws Exception
    {
        if (contentType == null || contentType.isEmpty() || contentType.equals("text/plain") || contentType.equals("text/csv"))
            return ".txt";
        else if (contentType.equals("application/vnd.ms-excel"))
            return ".xls";
        else if (contentType.equals("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"))
            return ".xlsx";
        else if (contentType.equals("application/x-msaccess"))
            return ".mdb";
        else if (contentType.equals("application/zip") || contentType.equals("application/x-zip-compressed"))
            return ".zip";
        throw new Exception("Unsupported content type - " + contentType);
    }

    public static void exitApp(int code)
    {
        exitApp(code, true);
    }
    
    public static void exitApp(int code, boolean hasGraphics)
    {
        if (code != 0)
            logger.info("Birst Connect exiting with non-zero status: " + code);
        if (hasGraphics && logger.isDebugEnabled())
        {
             logger.debug("Birst Connect waiting 10 seconds in order to copy the Java console");
             try {
                Thread.sleep(10 * 1000);
            } catch (InterruptedException ex) {
                logger.error(null, ex);
            }
        }
        System.exit(code);
    }

    public static String getHexString(byte[] b)
    {
        String result = "";
        for (int i = 0; i < b.length; i++)
        {
            result += Integer.toString((b[i] & 0xff) + 0x100, 16).substring(1);
        }
        return result;
    }

    public static void logStackTrace(Exception ex)
    {
        logger.error("", ex);        
    }

    public static String generatePhysicalName(String str)
    {
        if (str.length() == 0)
            return str;
        char c = str.charAt(0);
        if (c >= '0' && c <= '9')
        {
            str = 'N' + str;
        }
        String p1 = str.trim().replace('$', 'D').replace('#', 'N');
        String p2 = p1.replaceAll("[^\\w]", "_");
        return p2;
    }
    
    private static String hostName = null;
    private static String ipAddress = null;
    
    /**
     * return the hostname for this host, if problems return 'localhost'
     * 
     * @return
     */
    public static String getHostName()
    {
            if (hostName == null)
            {
                    try
                    {
                            hostName = java.net.InetAddress.getLocalHost().getHostName();
                    } catch (IOException ie)
                    {
                            hostName = "localhost";
                    }
            }
            return hostName;
    }

    private static String getIPAddress()
    {
            if (ipAddress == null)
            {
                    try
                    {
                            InetAddress tmp = java.net.InetAddress.getLocalHost();
                            ipAddress = tmp.toString();
                    } catch (IOException ie)
                    {
                            ipAddress = "localhost/127.0.0.l";
                    }
            }
            return ipAddress;
    }
    
    /**
     * log java version information
     */
    public static void logRuntimeInfo()
    {
        try
        {
            logger.info("========");
            logger.info("Host: " + Util.getHostName() + "/" + Util.getIPAddress());
            logger.info("OS: " + System.getProperty("os.name") + " / " + System.getProperty("os.version") + " / " + System.getProperty("sun.os.patch.level"));
            logger.info("Architecture: " + System.getProperty("os.arch"));
            logger.info("Java VM Name: " + System.getProperty("java.vm.name"));
            logger.info("Java VM Vendor: " + System.getProperty("java.vm.vendor"));		
            logger.info("Java version: " + System.getProperty("java.version")
                            + " / " + System.getProperty("java.runtime.version") 
                            + " / " + System.getProperty("java.specification.version")
                            + " / " + System.getProperty("java.vm.version"));
            logger.info("Java maximum memory setting: " + (int) (((double) Runtime.getRuntime().maxMemory()) / 1000000) + " Mbytes");
            logger.info("Available processors: " + Runtime.getRuntime().availableProcessors());
            logger.info("========");	
        }
        catch (Exception e)
        {
        	if (logger.isTraceEnabled()) {
                logger.trace("Exception " + e + " occured while processing method Util.logRuntimeInfo() ", e);
            }
        }
    }
    
    public static String getDriverName(HashMap databaseTypeMap, Object dbType,boolean isRealtime)
    {
         int indexOfDataBaseType = Integer.parseInt(String.valueOf(databaseTypeMap.get(dbType)));
         String driverName = DatabaseConnection.getDriverName(indexOfDataBaseType, isRealtime);
         return driverName;
    }
    
    public static boolean isDiscoveryTrial()
    {
        String hr = System.getProperty("jnlp.dtrial");
        return hr != null && hr.equalsIgnoreCase("true");
    }
    
    public static boolean isDriverInstalled(String driverName)
    {
            boolean isDriverInsatlled = false;
            try
            {
                    Class.forName(driverName);
                    isDriverInsatlled = true;
            }
            catch(Exception e)
            {
                /** Catch block will remain blank intentionally **/
            	if (logger.isTraceEnabled()) {
                    logger.trace("Exception " + e + " occured while processing method Util.isDriverInstalled(String)", e);
                }
            }
            return isDriverInsatlled;
    }
    
    public static void flagDriverAsNotInstalled(JLabel dbType, int index, JList list, boolean isSelected)
    {
        if (isSelected) 
        {
            if (-1 < index) 
            {
              list.setToolTipText(Util.DRIVER_NOT_INSTALLED_WARN_MESSAGE);
            }
        }
        else
        {
               list.setToolTipText("");
        }
    }
}
