/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.birst.dataconductor.amazon;

/**
 *
 * @author cpatel
 */
public class AmazonClientExceptionWrapper extends Exception
{
    public AmazonClientExceptionWrapper(String message, Throwable cause)
    {
       super(message,cause);
    }
}
