/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.birst.dataconductor.amazon;

/**
 *
 * @author cpatel
 */
public class AmazonServiceExceptionWrapper extends Exception
{
    public AmazonServiceExceptionWrapper(String message, Throwable cause)
    {
        super(message, cause);
    }
}
