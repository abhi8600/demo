/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.birst.dataconductor.amazon;


import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import java.io.File;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.PropertiesCredentials;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.ProgressEvent;
import com.amazonaws.services.s3.model.ProgressListener;
import com.birst.dataconductor.QuerySource;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;
import org.apache.log4j.Logger;
/**
 *
 * @author cpatel
 */
public class AmazonUtil 
{
    private final static Logger logger = Logger.getLogger(AmazonUtil.class);
    public static Listener listener = new Listener();
    
    public static File getS3Object(String directory, QuerySource qs) throws IOException, SQLException, AmazonServiceException, AmazonServiceExceptionWrapper, Exception
    {
        File file = null;
        try
        {
            String credentialsFile = qs.getCrendentialsFileName();
            String bucketName = qs.getBucketName();
            String bucketKey = qs.getBucketKey();
            String extension = null;
            if(bucketKey.contains("."))
            {
                extension=bucketKey.substring(bucketKey.lastIndexOf(".")); 
            }
            else
            {
                extension = ".txt";
            }
                 
            File propFile = new File(credentialsFile);
            
            AWSCredentials myCredentials = new PropertiesCredentials(propFile);
            AmazonS3Client s3Client = new AmazonS3Client(myCredentials);   
            if(extension != null && !extension.isEmpty())
            {
                file = new File(directory + File.separator + qs.getTableName().trim() + extension);
            }
            else
            {
                file = new File(directory + File.separator + qs.getTableName().trim() + ".txt");
            }
            
            GetObjectRequest gor = new GetObjectRequest(bucketName, bucketKey);
            gor.setProgressListener(listener);
            ObjectMetadata omd = s3Client.getObject(gor, file);
            gor.setProgressListener(null);
            
            return file;
        }
        catch(AmazonServiceException ase)
        {
            logger.error("Caught an AmazonServiceException, which" +
                    " means your request made it " +
                    "to Amazon S3, but was rejected with an error response" +
                    " for some reason.");
            logger.error("Error Message:    " + ase.getMessage());
            logger.error("HTTP Status Code: " + ase.getStatusCode());
            logger.error("AWS Error Code:   " + ase.getErrorCode());
            logger.error("Error Type:       " + ase.getErrorType());
            logger.error("Request ID:       " + ase.getRequestId());

            throw new AmazonServiceExceptionWrapper(ase.getMessage(), ase.getCause());
	}
        catch (AmazonClientException ace) 
        {
            logger.error("Caught an AmazonClientException, which means"+
                    " the client encountered " +
                    "an internal error while trying to " +
                    "communicate with S3, " +
                    "such as not being able to access the network.");

            throw new AmazonServiceExceptionWrapper(ace.getMessage(), ace.getCause());
	}
        catch(FileNotFoundException fnfe)
        {
             logger.error("Caught a FileNotFoundException, which means path you specified is invalid");
             throw new Exception("Credentials "+fnfe.getMessage(),fnfe.getCause());
        }
        catch (Exception ex)
        {
                logger.error(ex.getMessage());
                throw new Exception(ex.getMessage(),ex.getCause());
        }
    }
    public static class Listener implements ProgressListener
    {
            int count = 0;
            int loop = 0;
            public void progressChanged(ProgressEvent event)
            {
                    count += event.getBytesTransfered();
                    loop++;
                    if (loop % 100 == 0)
                            logger.debug(count);

            }
    }
}
