/**
 * $Id: GetConfig.java,v 1.19 2012-01-18 10:42:06 rchandarana Exp $
 *
 * Copyright (C) 2010 Birst, Inc. All rights reserved.
 * BIRST PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.birst.dataconductor;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URLEncoder;
import org.apache.log4j.Logger;

import com.birst.dataconductor.localetl.LocalETLUtil;


/**
 *
 * @author bpeters
 */
public class GetConfig
{
    private final static Logger logger = Logger.getLogger(GetConfig.class);

    public static DataConductorConfig get(String uname, String pword, String space, String url, String dcConfigFileName) throws MalformedURLException, IOException, GeneralErrorException, AuthenticationException
    {
        DataConductorConfig config = null;
        HttpURLConnection hs = null;
        InputStream is = null;
        String enctyptionKey = null;
        try
        {
            if (DataConductorUtil.isLocalDCConfigInUse())
            {
                String dcfile = DataConductorUtil.localDCConfigLocation + File.separator + dcConfigFileName;
                File dcf = new File(dcfile);
                if (!dcf.exists())
                {
                    logger.info("File \"" + dcConfigFileName + "\" does not exist at \"" + DataConductorUtil.localDCConfigLocation +"\". Downloading config file from server.");
                    LocalETLUtil.downloadDCConfig(url, space, DataConductorUtil.localDCConfigLocation, dcConfigFileName);
                }
                else
                {
                    enctyptionKey = WebDavUploader.getEnryptionKey(url, space);
                }
                is = new FileInputStream(dcfile);
                logger.debug("Reading dcconfig file : " + DataConductorUtil.localDCConfigLocation + File.separator + dcConfigFileName);
            }
            else
            {
                String path = "/" + URLEncoder.encode(space, "UTF-8").replace("+", "%20") + "/" + URLEncoder.encode(dcConfigFileName, "UTF-8").replace("+", "%20");
                hs = Util.getHttpURLConnection(url, path, "GET", false);
                hs.connect();
                Util.dumpHttpResponse(hs);
                is = hs.getInputStream();
            }
            if (enctyptionKey == null)
                config = getConfig(is);
            else 
                config = getConfig(is, enctyptionKey);
        }
        catch (SpaceUnavailableException ex)
        {
            logger.debug(ex.getMessage());
            return null;
        }
        finally
        {
            if (hs != null)
            {
                try
                {
                    hs.disconnect();
                } catch (Exception ex){
                	if (logger.isTraceEnabled()) {
                        logger.trace("Exception " + ex + " occured while processing method GetConfig.get(String, String, String, String, String)", ex);
                    }
                }
            }
            if (is != null)
            {
                try
                {
                    is.close();
                } catch (Exception ex) {
                	if (logger.isTraceEnabled()) {
                        logger.trace("Exception " + ex + " occured while processing method GetConfig.get(String, String, String, String, String)", ex);
                    }
                }
            }
        }
        if (config != null) {
            logger.info("Retrieved configuration information");
        }
        return config;
    }
    
    private static DataConductorConfig getConfig(InputStream is) throws IOException, GeneralErrorException
    {
        BufferedReader br = null;
        try
        {
            br = new BufferedReader(new InputStreamReader(is,"UTF-8"));
            String inputLine;
            StringBuilder sb = new StringBuilder();
            StringBuilder cf = new StringBuilder();
            boolean isCF = false;
            boolean isFirst = true;
            while ((inputLine = br.readLine()) != null)
            {
                if (isFirst)
                {
                    isFirst = false;
                    if (inputLine.startsWith(DataConductorUtil.STATUS_PREFIX))
                    {
                        int status = getStatusCode(inputLine);
                        if (status != 0)
                        {
                            throw new GeneralErrorException("Birst Connect is unable to access configuration information from the server, therefore tasks cannot be loaded. " + inputLine + ".");
                        }
                        else
                        {   
                            continue; //do not write status line
                        }
                    }
                }  
                if (inputLine == null || inputLine.length() == 0)
                {
                    isCF = true;
                }
                if (isCF) 
                {
                    cf.append(inputLine);
                    cf.append("\n");
                } 
                else 
                {
                    sb.append(inputLine);
                    sb.append("\n");
                }
            }
            EncryptionService.setKey(sb.toString()); // set the encryption key
            return DataConductorConfig.createFromXML(cf.toString());
        }
        finally
        {
            if (br != null)
            {
                try
                {
                    br.close();
                } catch (Exception ex) {
                	if (logger.isTraceEnabled()) {
                        logger.trace("Exception " + ex + " occured while processing method GetConfig.getConfig(InputStream)", ex);
                    }
                }
            }
        }
    }
    
    private static DataConductorConfig getConfig(InputStream is, String encryptionKey) throws IOException, GeneralErrorException
    {
        BufferedReader br = null;
        try
        {
            br = new BufferedReader(new InputStreamReader(is));
            String inputLine;
            StringBuilder cf = new StringBuilder();
            boolean isFirst = true;
            while ((inputLine = br.readLine()) != null)
            {
                if (isFirst)
                {
                    isFirst = false;
                    if(inputLine.startsWith(DataConductorUtil.STATUS_PREFIX)) 
                    {
                        int status = getStatusCode(inputLine);
                        if (status != 0)
                        {
                            String errorMessage = br.readLine();
                            logger.error(errorMessage);
                            throw new GeneralErrorException("Birst Connect is unable to access configuration information from the server, therefore tasks cannot be loaded. " + inputLine + ".");
                        }
                        else
                        {   
                            continue; //do not write status line
                        }
                    }
                }
                cf.append(inputLine);
                cf.append("\n");
            }
            EncryptionService.setKey(encryptionKey); // set the encryption key
            return DataConductorConfig.createFromXML(cf.toString());
        }
        finally
        {
            if (br != null)
            {
                try
                {
                    br.close();
                } catch (Exception ex) {
                	if (logger.isTraceEnabled()) {
                        logger.trace("Exception " + ex + " occured while processing method GetConfig.getConfig(InputStream, String)", ex);
                    }
                }
            }
        }
    }
    
    private static int getStatusCode(String statusLine) throws GeneralErrorException
    {
        try 
        {
            return Integer.parseInt(statusLine.substring(DataConductorUtil.STATUS_PREFIX.length()));
        } 
        catch (NumberFormatException e) 
        {
            throw new GeneralErrorException("Birst Connect is unable to access configuration information from the server, therefore tasks cannot be loaded. " + statusLine + ".");
        }                
    }
}
