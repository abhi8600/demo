/**
 * $Id: RealtimeConnection.java,v 1.74 2012-11-06 12:58:10 BIRST\mpandit Exp $
 *
 * Copyright (C) 2010 Birst, Inc. All rights reserved.
 * BIRST PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.birst.dataconductor;

import com.birst.dataconductor.localetl.CommandExecutor;
import com.birst.dataconductor.localetl.SpaceAdmin;
import com.birst.dataconductor.localetl.DeleteData;
import com.birst.dataconductor.localetl.LoadProcessor;

import com.birst.dataconductor.localetl.LocalETLUtil;
import com.birst.dataconductor.localetl.LogUtil;
import com.birst.dataconductor.localetl.ScriptExecutor;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.URLEncoder;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import org.apache.log4j.Logger;
import org.jdom.Element;


/**
 *
 * @author bpeters
 */
public class RealtimeConnection extends DatabaseConnection
{

    private boolean connecting = false;
    private boolean cancel = false;
    private List<PutResults> requestsList = new ArrayList<PutResults>();
    private List<SpaceAdmin> copySpaceRequestsList = new ArrayList<SpaceAdmin>();
    private String name;
    private List<LoadProcessor> loadProcessors = new ArrayList<LoadProcessor>();
    
    public Element getElement()
    {
        Element e = new Element("object");
        e.setAttribute("class", RealtimeConnection.class.getName());
        Element nameEl = new Element("void");
        nameEl.setAttribute("property", "name");
        Element nameValEl = new Element("string");
        nameValEl.addContent(name);
        nameEl.addContent(nameValEl);
        e.addContent(nameEl);
        List<Element> children = super.getElements();
        if (children != null && !children.isEmpty())
            e.addContent(children);
        return e;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    private final static Logger logger = Logger.getLogger(RealtimeConnection.class);


    public RealtimeConnection()
    {
        setTimeout("300");
    }

    public boolean isConnecting()
    {
        return connecting;
    }

    public void cancelConnect()
    {
        cancel = true;
    }

    public Object connect(String name, String uname, String pword, String space, String url,
            ActivityLog activityLog, ViewActivity va, Map<String, Boolean> statusMap, 
            DataConductorConfig config, DataConductorUtil dcu, String rcVisibleName)
    {
        connecting = true;
        statusMap.put(name, false);
        while (!cancel)
        {
            HttpURLConnection hs = null;
            try
            {
                List<PutResults> toRemoveLst = new ArrayList<PutResults>();
                for (PutResults pr : requestsList) {
                    if (pr.isFinished())
                        toRemoveLst.add(pr);
                }
                requestsList.removeAll(toRemoveLst);
                List<LoadProcessor> toRemoveLoadLst = new ArrayList<LoadProcessor>();
                for (LoadProcessor lp : loadProcessors) {
                    if (lp.isFinished())
                        toRemoveLoadLst.add(lp);
                }
                loadProcessors.removeAll(toRemoveLoadLst);
                String path = "/" + URLEncoder.encode(space, "UTF-8").replace("+", "%20") + "/dcrealtimeconnection/" + URLEncoder.encode(name, "UTF-8").replace("+", "%20");
                boolean isLocalConnection = false;
                if (config != null && config.getLocalETLConfig() != null )
                    isLocalConnection = config.getLocalETLConfig().isLocalETLConnection(name);
                hs = Util.getHttpURLConnection(url, path, "GET", isLocalConnection);
                hs.setReadTimeout(60 * 1000); // set the polling timeout (60 seconds)
                hs.connect();
                String resMsg = hs.getResponseMessage();
                if (resMsg != null && resMsg.contains("Space not found"))
                {
                   logger.info(resMsg);
                   logger.info("Ignoring connect request for \""+ rcVisibleName + "\" connection");
                   break;
                }
                Util.dumpHttpResponse(hs);
                statusMap.put(name, true);
                // Get request
                // Get index
                InputStream is = null;
                char[] buff = null;
                String line = null;
                try {
                    is = hs.getInputStream();
                    BufferedReader breader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
                    line = breader.readLine();
                    if (line == null || line.length() == 0) {
                        continue;
                    }
                    int length = 0;
                    try {
                        length = Integer.parseInt(line);
                    } catch (NumberFormatException ex) {
                       logger.error(ex.getMessage() + ": length=" + line);
                       continue;
                    }
                    if (length < 0) {
                        logger.error("length=" + length);
                        continue;
                    }
                    logger.debug("length=" + length);
                    buff = new char[length];
                    int read = 0;
                    int count = 0;
                    while ((read = breader.read(buff, count, length - count)) != -1) {
                        count += read;
                        logger.debug("count=" + count);
                        if (count >= length)
                            break;
                    }
                    if (count != length)
                    {
                        logger.error("characters read (" + count + ") less than the specified length (" + length + ")");
                    }
                } finally {
                    if (is != null)
                        is.close();
                }
                BufferedReader reader = new BufferedReader(new StringReader(new String(buff)));
                line = reader.readLine();
                int qindex = 0;
                try
                {
                    qindex = Integer.parseInt(line);
                } catch (NumberFormatException ex)
                {
                    logger.error(ex.getMessage() + ": qindex=" + line);
                    continue;
                }
                if (qindex < 0)
                {
                    logger.error("qindex=" + qindex);
                    continue;
                }
                // Get the request type
                String type = reader.readLine();
                logger.debug("realtime request: " + type);
                if (type.equals("killrequests"))
                {
                	line = reader.readLine();
                	if (line != null && line.startsWith("kill:"))
                	{
                		String qString = line.substring("kill:".length());
                		if (qString != null && qString.trim().length() > 0)
                		{
                			logger.info("Cancel query requst: \""+ qString + "\"");
                			for (PutResults pr : requestsList)
                			{
                				if (pr.getQuery() != null && qString.equals(pr.getQuery()))
                				{
                					pr.kill();
                				}
                			}
                		}
                	}
                	else
                	{
                		logger.info("Trying to kill all user queries");
                		for (PutResults pr : requestsList) 
                		{
                			pr.kill();
                		}
                	}
                }
               
                TimeZone processingTZ = null;
                DatabaseConnection qdc = null;
                int maxRecords = 100000;
                // Get the request
                StringBuilder sb = new StringBuilder();
                while ((line = reader.readLine()) != null)
                {
                    if (type.equals("query") && processingTZ == null && line.startsWith("ProcessingTZ="))
                    {
                        String tzID = line.substring("ProcessingTZ=".length());
                        processingTZ = TimeZone.getTimeZone(tzID);
                        continue;
                    }
                    else if (type.equals("query") && qdc == null && line.startsWith("QueryConnectionParameters="))
                    {
                        String paramsStr = line.substring("QueryConnectionParameters=".length());
                        String[] params = paramsStr.split("\t");
                        qdc = new DatabaseConnection();
                        qdc.genericDriver = this.genericDriver;
                        qdc.serverName = this.serverName;
                        qdc.setDatabaseName(params[0]);
                        qdc.setUsername(params[1]);
                        qdc.setPassword(params[2]);
                        qdc.setDatabaseType(this.getDatabaseType());
                        continue;
                    }
                    if (type.equals("query") && line.startsWith("MaxRecords"))
                    {
                        maxRecords = Integer.parseInt(line.substring("MaxRecords=".length()));
                        continue;
                    }
                    sb.append(line);
                    if (type.equals("executeliveaccessscript"))
                        sb.append(System.getProperty("line.separator"));
                }
                reader.close();
                
                if (type.equals("ping") || type.equals("query") || type.equals("resultsonlyquery") || type.equals("resultsonlyquerywithheaders")
                        || type.equals("executeupdatequery")
                        || type.equals("gettables") || type.equals("gettableschema")
                        || type.equals("getcubes") || type.equals("getcube") || type.equals("tableexists") ||  type.equals("checkconnection"))
                {
                    PutResults pr = new PutResults(name, (qdc != null ? qdc : this), config, qindex, type, processingTZ, sb.toString(), space, url, activityLog, va,maxRecords);
                    pr.start();
                    if (type.equals("query"))
                        requestsList.add(pr);
                }
                else if (type.equals("initiateliveaccessload"))
                {
                    LoadProcessor lp = new LoadProcessor(name, uname, pword, space,
                            url, activityLog, va, statusMap, config, dcu, qindex, type, sb.toString());
                    lp.start();
                    loadProcessors.add(lp);
                }
                else if (type.equals("gettxncommandhistorysnapshot") || type.equals("fileexists") || type.equals("getlastloadlog") || type.equals("getlogforloadid") || type.equals("gettracelog") || type.equals("getactivitylog"))
                {
                    LogUtil lu = new LogUtil(name, uname, pword, space,
                            url, activityLog, va, statusMap, config, dcu, qindex, type, sb.toString());
                    lu.start();
                }
                else if (type.equals("liveaccessdeletelastload") || type.equals("liveaccessdeletealldata") || type.equals("liveaccessdeletespace"))
                {
                    DeleteData dd = new DeleteData(name, uname, pword, space,
                            url, activityLog, va, statusMap, config, dcu, qindex, type, sb.toString());
                    dd.start();
                }
                else if (type.equals("executeliveaccessscript") || type.equals("getsourcefile"))
                {
                    ScriptExecutor se = new ScriptExecutor(name, uname, pword, space,
                            url, activityLog, va, statusMap, config, dcu, qindex, type, sb.toString());
                    se.start();
                }
                else if (type.equals("runcommandfileonliveaccess")) //download command file and run it against perf engine
                {
                    CommandExecutor ce = new CommandExecutor(name, uname, pword, space,
                            url, activityLog, va, statusMap, config, dcu, qindex, type, sb.toString());
                    ce.start();
                }
                else if (type.equals("initiatecopyspacedatatonewspace") || type.equals("swapspacedirectories")
                        || type.equals("migratespacetolocal"))
                {
                    SpaceAdmin cs = new SpaceAdmin(name, uname, pword, space,
                            url, activityLog, va, statusMap, config, dcu, qindex, type, sb.toString());
                    cs.start();
                    if (type.equals("initiatecopyspacedatatonewspace"))
                    {
                        synchronized (copySpaceRequestsList)
                        {
                            copySpaceRequestsList.add(cs);
                        }
                    }
                }
                else if (type.equals("pollcopyspacedatatonewspace"))
                {
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    try
                    {
                        String[] arguments = sb.toString().split("\t");
                        if (arguments.length != 2)
                            throw new Exception("Invalid arguments supplied for pollCopyspacedatatonewspace operation:" + sb.toString());
                        String targetSchemaName = arguments[0];
                        if (targetSchemaName == null || targetSchemaName.trim().isEmpty())
                            throw new Exception("Target Schema Name not supplied for pollCopyspacedatatonewspace operation");
                        String targetSpaceID = arguments[1];
                        if (targetSpaceID == null || targetSpaceID.trim().isEmpty())
                            throw new Exception("Target SpaceID not supplied for pollCopyspacedatatonewspace operation");
                        boolean found = true;
                        SpaceAdmin sa = null;
                        synchronized (copySpaceRequestsList)
                        {
                            for (SpaceAdmin cs : copySpaceRequestsList)
                            {
                                if (cs.copySpaceSchemaName.equals(targetSchemaName) && cs.copySpaceSpaceID.equals(targetSpaceID))
                                {
                                    found = true;
                                    if (cs.getIsRunning())
                                    {
                                        baos.write("Running".getBytes("UTF-8"));
                                    }
                                    else
                                    {
                                        if (cs.getIsSuccessfullyCompleted())
                                        {
                                            baos.write("True".getBytes("UTF-8"));
                                        }
                                        else
                                        {
                                            baos.write("False".getBytes("UTF-8"));
                                        }
                                        sa = cs;                                    
                                    }
                                    break;
                                }
                            }
                            if (!found)
                            {
                                baos.write("SchemaOrSpaceIDNotFound".getBytes("UTF-8"));
                            }                                                
                            if (sa != null)
                                copySpaceRequestsList.remove(sa);
                        }
                        LocalETLUtil.writeData(url, space, name, qindex, baos);
                    }
                    catch(Exception ex)
                    {
                        LocalETLUtil.addToActivityLog(va, activityLog, "Error: " + ex.toString(), ex);
                        logger.error(null, ex);
                        baos.write(("Error: " + ex.getMessage()).getBytes("UTF-8"));
                        LocalETLUtil.writeData(url, space, name, qindex, baos);
                    }
                    finally
                    {
                        if (va != null)
                        {
                            va.update();
                        }
                    }                    
                }
                else if (type.equals("markloadasfailed"))
                {
                    String result = null;
                    if (loadProcessors.size() == 0)
                    {
                        result = LocalETLUtil.markLoadAsFailed(config, this, space, sb.toString(), activityLog, va);
                    }
                    else
                    {
                        String message = "Please restart birst connect and try again to stop running data processing";
                        result = message;
                        LocalETLUtil.addToActivityLog(va, activityLog, message, null);
                        logger.debug(message);
                    }
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    baos.write(result.getBytes("UTF-8"));
                    LocalETLUtil.writeData(url, space, name, qindex, baos);
                }
                else if (type.equals("getlocalprocessingengineversion"))
                {
                    String localPEVersion = LocalETLUtil.getLocalProcessingEngineVersion();
                    logger.debug("Local Processing Engine Version = " + localPEVersion);
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    baos.write(localPEVersion.getBytes("UTF-8"));
                    LocalETLUtil.writeData(url, space, name, qindex, baos);
                }
                else if (type.equals("deletepublishlock"))
                {
                    Boolean isDeleted = LocalETLUtil.deletePublishLockFile(config, space);
                    logger.debug("Delete publish.lock : " + isDeleted);
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    baos.write(isDeleted.toString().getBytes("UTF-8"));
                    LocalETLUtil.writeData(url, space, name, qindex, baos);
                }
                else
                {
                    try
                    {
                        ByteArrayOutputStream baos = new ByteArrayOutputStream();
                        if (!type.equals("killrequests"))
                        {
                            baos.write(("Error: Unrecognized Live Access Command : " + type).getBytes("UTF-8"));
                        }
                        LocalETLUtil.writeData(url, space, name, qindex, baos);
                    }
                    catch (Exception ex)
                    {
                        activityLog.add(ex.getMessage(), ex);
                        if (va != null)
                        {
                            va.update();
                        }
                        logger.error(null, ex);
                    }
                }
            } catch (SocketTimeoutException ex)
            {
                statusMap.put(name, false);
                logger.info(ex.getMessage());
            } catch (SocketException ex)
            {
                statusMap.put(name, false);
                try
                {
                    logger.error(ex.getMessage()); // caused by the Birst Server going away, no need for a stack trace
                    Thread.sleep(30 * 1000); // sleep 30 seconds
                } catch (InterruptedException iex)
                {
                    logger.error(iex.getMessage(), iex);
                    break;
                }
            } catch (UnknownHostException ex)
            {
                // see if we've moved
                String newurl = Util.getRedirectURL(url);
                if (!url.equals(newurl))
                {
                    url = newurl;
                    continue;
                }
                statusMap.put(name, false);
                logger.error(ex.getMessage(), ex);
                break;
            } catch (SpaceUnavailableException ex)
            {
                statusMap.put(name, false);
                try
                {
                    logger.error(ex.getMessage(), ex);
                    Thread.sleep(30 * 1000); // sleep 30 seconds
                } catch (InterruptedException iex)
                {
                    logger.error(iex.getMessage(), iex);
                    break;
                }
            } catch (IOException ex)
            {
                statusMap.put(name, false);
                try
                {
                    logger.error(ex.getMessage(), ex);
                    Thread.sleep(30 * 1000); // sleep 30 seconds
                } catch (InterruptedException iex)
                {
                    logger.error(iex.getMessage(), iex);
                    break;
                }
            } catch (Exception ex)
            {
                statusMap.put(name, false);
                logger.error(ex.getMessage(), ex);
                break;
            }
            finally
            {
                if (hs != null)
                    hs.disconnect();
            }
        }
        return null;
    }
}
