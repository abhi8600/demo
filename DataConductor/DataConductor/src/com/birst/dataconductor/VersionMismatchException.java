/**
 * $Id: VersionMismatchException.java,v 1.1 2010-06-21 17:08:17 ricks Exp $
 *
 * Copyright (C) 2010 Birst, Inc. All rights reserved.
 * BIRST PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.birst.dataconductor;

/**
 *
 * @author ricks
 */
public class VersionMismatchException extends BaseException
{
    public VersionMismatchException(String s)
    {
        super(s);
    }

    public VersionMismatchException(String s, Throwable t)
    {
        super(s, t);
    }
}
