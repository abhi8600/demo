/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.birst.dataconductor;

import org.jdom.Element;

/**
 *
 * @author bpeters
 */
public class FileSource
{
    private String path;
    private String fileName;
    private UploadSettings settings = new UploadSettings();
    
    public Element getElement()
    {
        Element e = new Element("object");
        e.setAttribute("class", FileSource.class.getName());
        if (path != null)
        {
            e.addContent(XmlEncoderUtil.getStringPropertyElement("path", path));            
        }
        if (fileName != null)
        {
            e.addContent(XmlEncoderUtil.getStringPropertyElement("fileName", fileName));            
        }
        if (settings != null)
        {
            e.addContent(settings.getElement());
        }
        return e;
    }

    public String getFileName()
    {
        return fileName;
    }

    public void setFileName(String fileName)
    {
        this.fileName = fileName;
    }

    public String getPath()
    {
        return path;
    }

    public void setPath(String path)
    {
        this.path = path;
    }

    public UploadSettings getSettings()
    {
        return settings;
    }

    public void setSettings(UploadSettings settings)
    {
        this.settings = settings;
    }

}
