/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.birst.dataconductor;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JLabel;
import javax.swing.JProgressBar;
import org.apache.log4j.Logger;

/**
 *
 * @author ricks
 */
public class GraphicsObject {

    private JFrame frame;
    private JLabel statusMessageLabel;
    private JProgressBar progressBar;

    private String uname;
    private String pword;

    private final static Logger logger = Logger.getLogger(GraphicsObject.class);

    public GraphicsObject()
    {
    }

    public GraphicsObject(JFrame frame, JLabel statusMessageLabel, JProgressBar progressBar)
    {
        this.frame = frame;
        this.statusMessageLabel = statusMessageLabel;
        this.progressBar = progressBar;
    }

    public JFrame getFrame()
    {
        return frame;
    }

    public void showMessageDialog(String message)
    {
        if (frame != null)
            JOptionPane.showMessageDialog(getFrame(), message);
        logger.error(message);
    }

    public void showInfoMessageDialog(String message)
    {
        if (frame != null)
            JOptionPane.showMessageDialog(getFrame(), message);
        logger.info(message);
    }

    public void showMessageDialog(String message1, String message2, int opt)
    {
        if (frame != null)
            JOptionPane.showMessageDialog(getFrame(), message1, message2, opt);
        logger.error(message1 + ": " + message2);
    }


    public void setStatusMessage(String message)
    {
        if (statusMessageLabel != null)
            statusMessageLabel.setText(message);
        if (message != null && message.length() > 0)
            logger.info(message);
    }

    public void setProgressBarMaximum(int max)
    {
        if (progressBar != null)
            progressBar.setMaximum(max);
    }

    public void setProgressBarValue(int val)
    {
        if (progressBar != null)
            progressBar.setValue(val);
    }

    private void getData()
    {
        if (uname != null)
            return;
        GetCredentials gc = new GetCredentials(this.getFrame(), true);
        gc.setLocationRelativeTo(this.getFrame());
        gc.setVisible(true);
        uname = gc.getUsername();
        pword = gc.getPassword();
    }

    public String getUsername()
    {
        getData();
        return uname;
    }

    public String getPassword()
    {
        getData();
        return pword;
    }
}
