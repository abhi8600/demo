/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.birst.dataconductor;

import java.util.ArrayList;
import java.util.List;
import org.jdom.Element;

/**
 *
 * @author bpeters
 */
public class Task
{

    public static final int RECURRENCE_TYPE_DAILY = 0;
    public static final int RECURRENCE_TYPE_WEEKLY = 1;
    public static final int RECURRENCE_TYPE_MONTHLY = 2;
    private String name;
    private boolean enabled;
    private boolean localETLTask;
    private boolean dowanloadCloudSources;
    private List<Object> sourceList = new ArrayList<Object>();
    private boolean scheduled;
    private int recurrenceType;
    private int hour;
    private int min;
    private boolean pm;
    private boolean Sunday;
    private boolean Monday;
    private boolean Tuesday;
    private boolean Wednesday;
    private boolean Thursday;
    private boolean Friday;
    private boolean Saturday;
    private int month;
    private int day;
    private boolean retryOnFailure;
    private int numRetries;
    private boolean processAutomatically;
    private boolean createQuickDashboards;
    private boolean consolidateSources;
    private int dateShiftValue;
    private List<String> subGroups;
    
    public Element getElement()
    {
        Element e = new Element("object");
        e.setAttribute("class", Task.class.getName());
        if (name != null)
        {
            e.addContent(XmlEncoderUtil.getStringPropertyElement("name", name));            
        }
        if (enabled)
        {
            e.addContent(XmlEncoderUtil.getBooleanPropertyElement("enabled"));            
        }
        if (localETLTask)
        {
            e.addContent(XmlEncoderUtil.getBooleanPropertyElement("localETLTask"));            
        }
        if (dowanloadCloudSources)
        {
            e.addContent(XmlEncoderUtil.getBooleanPropertyElement("dowanloadCloudSources"));            
        }
        if (sourceList != null && !sourceList.isEmpty())
        {
            Element sourceListEl = new Element("void");
            sourceListEl.setAttribute("property", "sourceList");
            for (Object obj : sourceList)
            {
                Element addEl = new Element("void");
                addEl.setAttribute("method", "add");
                addEl.addContent(getSourceElement(obj));
                sourceListEl.addContent(addEl);
            }
            e.addContent(sourceListEl);
        }
        if (scheduled)
        {
            e.addContent(XmlEncoderUtil.getBooleanPropertyElement("scheduled"));            
        }
        if (recurrenceType != 0)
        {
            e.addContent(XmlEncoderUtil.getIntPropertyElement("recurrenceType", recurrenceType));            
        }
        if (hour != 0)
        {
            e.addContent(XmlEncoderUtil.getIntPropertyElement("hour", hour));            
        }
        if (min != 0)
        {
            e.addContent(XmlEncoderUtil.getIntPropertyElement("min", min));            
        }
        if (pm)
        {
            e.addContent(XmlEncoderUtil.getBooleanPropertyElement("pm"));            
        }
        if (Sunday)
        {
            e.addContent(XmlEncoderUtil.getBooleanPropertyElement("Sunday"));
        }
        if (Monday)
        {
            e.addContent(XmlEncoderUtil.getBooleanPropertyElement("Monday"));            
        }
        if (Tuesday)
        {
            e.addContent(XmlEncoderUtil.getBooleanPropertyElement("Tuesday"));            
        }
        if (Wednesday)
        {
            e.addContent(XmlEncoderUtil.getBooleanPropertyElement("Wednesday"));            
        }
        if (Thursday)
        {
            e.addContent(XmlEncoderUtil.getBooleanPropertyElement("Thursday"));            
        }
        if (Friday)
        {
            e.addContent(XmlEncoderUtil.getBooleanPropertyElement("Friday"));            
        }
        if (Saturday)
        {
            e.addContent(XmlEncoderUtil.getBooleanPropertyElement("Saturday"));            
        }
        if (month != 0)
        {
            e.addContent(XmlEncoderUtil.getIntPropertyElement("month", month));            
        }
        if (day != 0)
        {
            e.addContent(XmlEncoderUtil.getIntPropertyElement("day", day));            
        }
        if (retryOnFailure)
        {
            e.addContent(XmlEncoderUtil.getBooleanPropertyElement("retryOnFailure"));            
        }
        if (numRetries != 0)
        {
            e.addContent(XmlEncoderUtil.getIntPropertyElement("numRetries", numRetries));            
        }
        if (processAutomatically)
        {
            e.addContent(XmlEncoderUtil.getBooleanPropertyElement("processAutomatically"));            
        }
        if (createQuickDashboards)
        {
            e.addContent(XmlEncoderUtil.getBooleanPropertyElement("createQuickDashboards"));            
        }
        if (consolidateSources)
        {
            e.addContent(XmlEncoderUtil.getBooleanPropertyElement("consolidateSources"));            
        }
        if (dateShiftValue != 0)
        {
            e.addContent(XmlEncoderUtil.getIntPropertyElement("dateShiftValue", dateShiftValue));            
        }
        if (subGroups != null && !subGroups.isEmpty())
        {
            Element sgEl = new Element("void");
            sgEl.setAttribute("property", "subGroups");
            Element sgList = new Element("object");
            sgList.setAttribute("class", ArrayList.class.getName());
            for (String sg : subGroups)
            {
                Element subEl = new Element("void");
                subEl.setAttribute("method", "add");
                Element subValEl = new Element("string");
                subValEl.addContent(sg);
                subEl.addContent(subValEl);
                sgList.addContent(subEl);
            }
            sgEl.addContent(sgList);
            e.addContent(sgEl);
        }
        return e;
    }
    
    private Element getSourceElement(Object obj)
    {
        if (obj == null)
            return null;
        if (obj instanceof FileSource)
        {
            FileSource src = (FileSource)obj;
            return src.getElement();
        }
        else if (obj instanceof QuerySource)
        {
            QuerySource src = (QuerySource)obj;
            return src.getElement();
        }
        return null;
    }

    public List<String> getSubGroups() {
        return subGroups;
    }

    public void setSubGroups(List<String> subGroups) {
        this.subGroups = subGroups;
    }

    public boolean isLocalETLTask() {
        return localETLTask;
    }

    public void setLocalETLTask(boolean localETLTask) {
        this.localETLTask = localETLTask;
    }

    public boolean isDowanloadCloudSources() {
        return dowanloadCloudSources;
    }

    public void setDowanloadCloudSources(boolean dowanloadCloudSources) {
        this.dowanloadCloudSources = dowanloadCloudSources;
    }

    public boolean isEnabled()
    {
        return enabled;
    }

    public void setEnabled(boolean enabled)
    {
        this.enabled = enabled;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public List<Object> getSourceList()
    {
        return sourceList;
    }

    public void setSourceList(List<Object> sourceList)
    {
        this.sourceList = sourceList;
    }

    public boolean isFriday()
    {
        return Friday;
    }

    public void setFriday(boolean Friday)
    {
        this.Friday = Friday;
    }

    public boolean isMonday()
    {
        return Monday;
    }

    public void setMonday(boolean Monday)
    {
        this.Monday = Monday;
    }

    public boolean isSaturday()
    {
        return Saturday;
    }

    public void setSaturday(boolean Saturday)
    {
        this.Saturday = Saturday;
    }

    public boolean isSunday()
    {
        return Sunday;
    }

    public void setSunday(boolean Sunday)
    {
        this.Sunday = Sunday;
    }

    public boolean isThursday()
    {
        return Thursday;
    }

    public void setThursday(boolean Thursday)
    {
        this.Thursday = Thursday;
    }

    public boolean isTuesday()
    {
        return Tuesday;
    }

    public void setTuesday(boolean Tuesday)
    {
        this.Tuesday = Tuesday;
    }

    public boolean isWednesday()
    {
        return Wednesday;
    }

    public void setWednesday(boolean Wednesday)
    {
        this.Wednesday = Wednesday;
    }

    public int getDay()
    {
        return day;
    }

    public void setDay(int day)
    {
        this.day = day;
    }

    public int getHour()
    {
        return hour;
    }

    public void setHour(int hour)
    {
        this.hour = hour;
    }

    public int getMin()
    {
        return min;
    }

    public void setMin(int min)
    {
        this.min = min;
    }

    public int getMonth()
    {
        return month;
    }

    public void setMonth(int month)
    {
        this.month = month;
    }

    public boolean isPm()
    {
        return pm;
    }

    public void setPm(boolean pm)
    {
        this.pm = pm;
    }

    public int getRecurrenceType()
    {
        return recurrenceType;
    }

    public void setRecurrenceType(int recurrenceType)
    {
        this.recurrenceType = recurrenceType;
    }

    public boolean isScheduled()
    {
        return scheduled;
    }

    public void setScheduled(boolean scheduled)
    {
        this.scheduled = scheduled;
    }

    public int getNumRetries()
    {
        return numRetries;
    }

    public void setNumRetries(int numRetries)
    {
        this.numRetries = numRetries;
    }

    public boolean isRetryOnFailure()
    {
        return retryOnFailure;
    }

    public void setRetryOnFailure(boolean retryOnFailure)
    {
        this.retryOnFailure = retryOnFailure;
    }
    public boolean isProcessAutomatically()
    {
        return processAutomatically;
    }

    public void setProcessAutomatically(boolean processAutomatically)
    {
        this.processAutomatically = processAutomatically;
    }

    public boolean isConsolidateSources()
    {
        return consolidateSources;
    }

    public void setConsolidateSources(boolean consolidateSources)
    {
        this.consolidateSources = consolidateSources;
    }

    public boolean isCreateQuickDashboards()
    {
        return createQuickDashboards;
    }

    public void setCreateQuickDashboards(boolean createQuickDashboards)
    {
        this.createQuickDashboards = createQuickDashboards;
    }

    public int getDateShiftValue()
    {
        return dateShiftValue;
    }

    public void setDateShiftValue(int dateShiftValue)
    {
        this.dateShiftValue = dateShiftValue;
    }
    
}
