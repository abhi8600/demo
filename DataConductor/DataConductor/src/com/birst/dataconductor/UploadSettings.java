/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.birst.dataconductor;

import org.jdom.Element;

/**
 *
 * @author bpeters
 */
public class UploadSettings
{

    private boolean firstRowHasHeaders = true;
    private boolean ignoreInteralQuotes = false;
    private int beginSkip;
    private int endSkip;
    private boolean forceNumColumns;
    private String replacements;
    private String quote = "\"";
    private boolean ignoreCarriageReturn;
    private String separator;
    private String encoding;
    
    public Element getElement()
    {
        Element e = new Element("void");
        e.setAttribute("property", "settings");
        if (!firstRowHasHeaders)
        {
            e.addContent(XmlEncoderUtil.getBooleanPropertyElement("firstRowHasHeaders", Boolean.FALSE.toString()));
        }
        if (ignoreInteralQuotes)
        {
            e.addContent(XmlEncoderUtil.getBooleanPropertyElement("ignoreInteralQuotes"));
        }
        if (beginSkip != 0)
        {
            e.addContent(XmlEncoderUtil.getIntPropertyElement("beginSkip", beginSkip));
        }
        if (endSkip != 0)
        {
            e.addContent(XmlEncoderUtil.getIntPropertyElement("endSkip", endSkip));
        }
        if (forceNumColumns)
        {
            e.addContent(XmlEncoderUtil.getBooleanPropertyElement("forceNumColumns"));
        }
        if (replacements != null)
        {
            e.addContent(XmlEncoderUtil.getStringPropertyElement("replacements", replacements));
        }
        if (!quote.equals("\""))
        {
            e.addContent(XmlEncoderUtil.getStringPropertyElement("quote", quote));
        }
        if (ignoreCarriageReturn)
        {
            e.addContent(XmlEncoderUtil.getBooleanPropertyElement("ignoreCarriageReturn"));
        }
        if (separator != null)
        {
            e.addContent(XmlEncoderUtil.getStringPropertyElement("separator", separator));
        }
        if (encoding != null)
        {
            e.addContent(XmlEncoderUtil.getStringPropertyElement("encoding", encoding));
        }        
        return e;
    }

    public String getQuote()
    {
        return quote;
    }

    public void setQuote(String quote)
    {
        this.quote = quote;
    }

    public boolean isForceNumColumns()
    {
        return forceNumColumns;
    }

    public void setForceNumColumns(boolean forceNumColumns)
    {
        this.forceNumColumns = forceNumColumns;
    }

    public String getReplacements()
    {
        return replacements;
    }

    public void setReplacements(String replacements)
    {
        this.replacements = replacements;
    }

    public int getBeginSkip()
    {
        return beginSkip;
    }

    public void setBeginSkip(int beginSkip)
    {
        this.beginSkip = beginSkip;
    }

    public int getEndSkip()
    {
        return endSkip;
    }

    public void setEndSkip(int endSkip)
    {
        this.endSkip = endSkip;
    }

    public boolean isFirstRowHasHeaders()
    {
        return firstRowHasHeaders;
    }

    public void setFirstRowHasHeaders(boolean firstRowHasHeaders)
    {
        this.firstRowHasHeaders = firstRowHasHeaders;
    }

    public boolean isIgnoreInteralQuotes()
    {
        return ignoreInteralQuotes;
    }

    public void setIgnoreInteralQuotes(boolean ignoreInteralQuotes)
    {
        this.ignoreInteralQuotes = ignoreInteralQuotes;
    }

    public boolean isIgnoreCarriageReturn()
    {
        return ignoreCarriageReturn;
    }

    public void setIgnoreCarriageReturn(boolean ignoreCarriageReturn)
    {
        this.ignoreCarriageReturn = ignoreCarriageReturn;
    }
    
    public String getSeparator()
    {
        return separator;
    }

    public void setSeparator(String separator)
    {
        this.separator = separator;
    }
    
      public String getEncoding()
    {
        return encoding;
    }

    public void setEncoding(String encoding)
    {
        this.encoding = encoding;
    }

    public String toString()
    {
        return "beginSkip: " + beginSkip + ", endSkip: " + endSkip + ", firstRowHasHeaders: " + firstRowHasHeaders + ", ignoreInteralQuotes: " + ignoreInteralQuotes + ", forceNumColumns: " + forceNumColumns + ", replacements:" + replacements + ",ignoreCarriageReturn:" + ignoreCarriageReturn + ",separator:" + separator + ",encoding:" + encoding;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (obj == null)
        {
            return false;
        }
        if (getClass() != obj.getClass())
        {
            return false;
        }
        final UploadSettings other = (UploadSettings) obj;
        if (this.firstRowHasHeaders != other.firstRowHasHeaders)
        {
            return false;
        }
        if (this.ignoreInteralQuotes != other.ignoreInteralQuotes)
        {
            return false;
        }
        if (this.ignoreCarriageReturn != other.ignoreCarriageReturn)
        {
            return false;
        }
        if (this.beginSkip != other.beginSkip)
        {
            return false;
        }
        if (this.endSkip != other.endSkip)
        {
            return false;
        }
        if (!((this.quote == null && other.quote == null) ||
                (this.quote != null && other.quote != null && this.quote.equals(other.quote))))
            return false;
        if (this.forceNumColumns != other.forceNumColumns)
            return false;
        if (!((this.replacements == null && other.replacements == null) ||
                (this.replacements != null && other.replacements != null && this.replacements.equals(other.replacements))))
            return false;
        if (!((this.separator == null && other.separator == null) ||
                (this.separator != null && other.separator != null && this.separator.equals(other.separator))))
            return false;
        if (!((this.encoding == null && other.encoding == null) ||
                (this.encoding != null && other.encoding != null && this.encoding.equals(other.encoding))))
            return false;
        return true;
    }

    @Override
    public int hashCode()
    {
        int hash = 3;
        hash = 41 * hash + (this.firstRowHasHeaders ? 1 : 0);
        hash = 41 * hash + this.beginSkip;
        hash = 41 * hash + this.endSkip;
        hash = 41 * hash + (this.forceNumColumns ? 1 : 0);
        return hash;
    }
}
