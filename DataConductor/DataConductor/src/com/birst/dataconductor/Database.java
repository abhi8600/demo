/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.birst.dataconductor;

import com.jcraft.jsch.JSch;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import org.apache.log4j.Logger;

/**
 *
 * @author bpeters
 */
public class Database
{
    public static final int oracle_sql_TimestampTZ = -101;
    public static final int oracle_sql_TimestampLTZ = -102;
    
    public static Connection getConnection(String driverName, String connectString, String username, String password, StringBuilder message,SSHTunnel sshTunnel)
    {
        Connection conn;
        try
        {
            Class.forName(driverName);
        } catch (ClassNotFoundException ex)
        {
            Logger.getLogger(Database.class).error("Could not find the JDBC driver: " + driverName);
            if (message != null)
                message.append("Could not find the JDBC driver: " + driverName);
            return null;
        }
        try
        {
            if (username != null && password != null && username.length() > 0)
            {
                if(sshTunnel != null && sshTunnel.isIsSSHTunnelConfigured())
                {
                    try
                    {
                        JSch jsch = new JSch();
                        jsch.addIdentity(sshTunnel.getPublicKeyFile());

                        sshTunnel.session = jsch.getSession(sshTunnel.getEc2User(), sshTunnel.getTunnelExternalHost(), Integer.valueOf(sshTunnel.getExternalPort()));
                        sshTunnel.session.setConfig("StrictHostKeyChecking", "no");
                        sshTunnel.session.connect();
                        sshTunnel.session.setPortForwardingL( Integer.valueOf(sshTunnel.getLocalPort()),sshTunnel.getTunnelInternalHost(),Integer.valueOf(sshTunnel.getTunnelInternalPort()));
                    }
                    catch(Exception ex)
                    {
                          sshTunnel.closeSession();
                          Logger.getLogger(Database.class).error(ex.getMessage());
                          throw new SQLException(message.toString());
                    }
                }
               conn = DriverManager.getConnection(connectString, username, password);    
            }
            else
            {
                conn = DriverManager.getConnection(connectString);
            }
        } catch (SQLException sqe)
        {
            // set up the message so that we don't accidentally log the credentials
            String msg = "Connection to the JDBC driver failed: " + driverName;
            if (username != null && password != null && username.length() > 0)
                msg = msg + ", " + connectString; // credentials not part of the connection string
            msg = msg + ", " + sqe.getMessage();
            // try to pull as much from the SQL exception as possible
            Throwable t = sqe.getCause();
            if (t != null)
                msg = msg + ", " + t.getMessage();
            SQLException xsqe = sqe.getNextException();
            while (xsqe != null)
            {
                msg = msg + ", " + xsqe.getMessage();
                xsqe = xsqe.getNextException();
            }
            Logger.getLogger(Database.class).error(msg, sqe);
            if (message != null)
                message.append(msg);
            return null;
        }
        return conn;
    }
}
