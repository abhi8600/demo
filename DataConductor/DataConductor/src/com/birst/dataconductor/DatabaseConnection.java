/**
 * $Id: DatabaseConnection.java,v 1.50 2012-11-10 01:16:48 BIRST\ricks Exp $
 *
 * Copyright (C) 2010 Birst, Inc. All rights reserved.
 * BIRST PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.birst.dataconductor;

import java.beans.XMLEncoder;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import org.jdom.Element;

/**
 *
 * @author bpeters
 */
public class DatabaseConnection
{
    private final static Logger logger = Logger.getLogger(DatabaseConnection.class);

    public static final String MSSQL = "Microsoft SQL Server";
    public static final String MYSQL = "MySQL";
    public static final String INFOBRIGHT = "Infobright";
    public static final String ORACLE = "Oracle 11g";
    public static final String ORACLE_ODBC = "Oracle ODBC";
    public static final String MYSQL_ODBC = "MySQL ODBC";
    public static final String DB2_ODBC = "DB2 ODBC";
    public static final String MSSQL_ODBC = "Microsoft SQL Server ODBC";
    public static final String IMPALA = "Impala";
    public static final String MICROSOFT_ANALYSIS_SERVICES = "Microsoft Analysis Services (XMLA)";
    public static final String PENTAHO_MONDRIAN = "Pentaho (Mondrian) Analysis Services (XMLA)";
    public static final String HYPERIAN_ESSBASE = "Hyperion Essbase (XMLA)";
    public static final String SAP_BW = "SAP BW (XMLA)";
    public static final String VERTICA = "Vertica";
    public static final String TERADATA = "Teradata";
    public static final String SYBASEIQ = "Sybase IQ";
    public static final String PARACCEL = "ParAccel";
    public static final String HIVE = "Hive/Hadoop";
    public static final String MEMDB = "In-memory Database";
    public static final String ODBC = "ODBC";
    public static final String POSTGRESQL = "PostgreSQL";
    public static final String HANA = "SAP Hana";
    private static EncryptionService es = EncryptionService.getInstance();
    public static final int GENERIC_DATABASE = 7;   // one more than the largest index (6 for Hana)
    public static final int REALTIME_GENERIC_DATABASE = 21; // one more than the largest index (20 for Hana)
    public static final int TRIAL_GENERIC_DATABASE = 6;   // one more than the largest index (5 for Hana, missing ODBC)
    public static final int TRIAL_REALTIME_GENERIC_DATABASE = 1; // one more than the largest index (1 for SQLServer driver) XXX seems limiting, why not more types
    public boolean genericDriver;
    public String driverName;
    public String serverName;
    private String databaseName;
    private String username;
    private String password;
    private int port;
    private String databaseType;
    private String schema;
    private String connectString;
    private int timeout = -1;
    private boolean useDirectConnection = false;
    private String filter;
    
    public List<Element> getElements()
    {
        List<Element> elements = new ArrayList<Element>();
        if (genericDriver)
        {
            elements.add(XmlEncoderUtil.getBooleanPropertyElement("genericDriver"));
            if (filter != null)
            {
                elements.add(XmlEncoderUtil.getStringPropertyElement("filter", filter));
            }
        }
        if (driverName != null)
        {
            elements.add(XmlEncoderUtil.getStringPropertyElement("driverName", driverName));            
        }
        if (connectString != null)
        {
            elements.add(XmlEncoderUtil.getStringPropertyElement("connectString", connectString));            
        }
        if (serverName != null)
        {
            elements.add(XmlEncoderUtil.getStringPropertyElement("serverName", serverName));            
        }
        if (databaseName != null)
        {
            elements.add(XmlEncoderUtil.getStringPropertyElement("databaseName", databaseName));
        }
        if (username != null)
        {
            elements.add(XmlEncoderUtil.getStringPropertyElement("username", es.decrypt(username)));
        }
        if (password != null)
        {
            elements.add(XmlEncoderUtil.getStringPropertyElement("password", password));
        }
        if (port != 0)
        {
            elements.add(XmlEncoderUtil.getIntPropertyElement("port", port));            
        }
        if (databaseType != null)
        {
            elements.add(XmlEncoderUtil.getStringPropertyElement("databaseType", databaseType));
        }
        if (schema != null)
        {
            elements.add(XmlEncoderUtil.getStringPropertyElement("schema", schema));            
        }
        if (timeout != -1)
        {
            elements.add(XmlEncoderUtil.getIntPropertyElement("timeout", timeout));            
        }
        if (useDirectConnection)
        {
             elements.add(XmlEncoderUtil.getBooleanPropertyElement("useDirectConnection"));
        }
        return elements;
    }
    
    public DatabaseConnection()
    {
    }

    public boolean isGenericDriver() {
        return genericDriver;
    }

    public void setGenericDriver(boolean genericDriver) {
        this.genericDriver = genericDriver;
    }
    
    public String getDatabaseType()
    {
        return databaseType;
    }

    public void setDatabaseType(String databaseType)
    {
        this.databaseType = databaseType;
    }
    
    public void setFilter(String filter)
    {
        this.filter = filter;
    }
    
    public String getFilter()
    {
        return filter;
    }
    
    public boolean isUseDirectConnection() 
    {
        return useDirectConnection;
    }
    
    public void setUseDirectConnection(boolean useDirectConnection) {
        this.useDirectConnection = useDirectConnection;
    }
    
    public static String getBaseDatabaseName(String databaseName)
    {
        if(databaseName != null)
        {
            String[] parts = databaseName.split(";");
            if (parts.length > 0)
                return parts[0];    
        }
        return databaseName;
    }
    
    public String getDatabaseName()
    {
    	if (genericDriver)
    		return databaseName;
        return getBaseDatabaseName(databaseName);
    }

    public static String getCatalogFilterFromDatabaseName(String databaseName)
    {
        String[] parts = databaseName.split(";");
        for(String s: parts)
        {
            if (s.toLowerCase().startsWith("catalog="))
            {
                return s.substring(8);
            }
        }
        return null;
    }

    public String getCatalogFilter()
    {
        if (genericDriver)
            return getCatalogFilterFromDatabaseName(filter);
        return getCatalogFilterFromDatabaseName(databaseName);
    }

    public static String getSchemaFilterFromDatabaseName(String databaseName)
    {
        String[] parts = databaseName.split(";");
        for(String s: parts)
        {
            if (s.toLowerCase().startsWith("schema="))
            {
                return s.substring(7);
            }
        }
        return null;
    }

    public String getSchemaFilter()
    {
        if (genericDriver)
            return getSchemaFilterFromDatabaseName(filter);
        return getSchemaFilterFromDatabaseName(databaseName);
    }

    public static boolean getUseViewsFromDatabaseName(String databaseName)
    {
        String[] parts = databaseName.split(";");
        for(String s: parts)
        {
            if (s.toLowerCase().startsWith("useviews="))
            {
                return s.substring(9).toLowerCase().equals("true");
            }
        }
        return true;
    }

    public boolean getUseViews()
    {
        if (genericDriver)
            return getUseViewsFromDatabaseName(filter);
        return getUseViewsFromDatabaseName(databaseName);
    }

    public String getFullDatabaseName()
    {
        return databaseName;
    }

    public void setDatabaseName(String databaseName)
    {
        this.databaseName = databaseName;
    }

    public String getServerName()
    {
        return serverName;
    }

    public void setServerName(String serverName)
    {
        this.serverName = serverName;
    }

    public String getDriverName()
    {
        return isGenericDriver() ? serverName : driverName;
    }

    public void setDriverName(String driverName)
    {
        this.driverName = driverName;
    }
    
    public String getConnectString()
    {
        return connectString;
    }
    
    public void setConnectString(String connectString)
    {
        this.connectString = connectString;
    }

    public String getSchema()
    {
        return schema;
    }

    public void setSchema(String schema)
    {
        this.schema = schema;
    }
    
    public void setDriverName(int index, boolean isRealtime)
    {
        this.driverName = getDriverName(index, isRealtime);
    }
    
    public static int getRealTimeGenericDatabaseIndex(){
        if(Util.isDiscoveryTrial()){
            return DatabaseConnection.TRIAL_REALTIME_GENERIC_DATABASE;
        }
        return DatabaseConnection.REALTIME_GENERIC_DATABASE;
    }
    
    public static int getGenericDatabaseIndex(){
        if(Util.isDiscoveryTrial()){
            return DatabaseConnection.TRIAL_GENERIC_DATABASE;
        }
        return DatabaseConnection.GENERIC_DATABASE;
    }
    
    public static boolean isTrialGenericDatabaseIndex(int index){
        if(Util.isDiscoveryTrial()){
            return index == getGenericDatabaseIndex();
        }
        return false;
    }
    
    public static boolean isTrialRealTimeGenericDatabaseIndex(int index){
        if(Util.isDiscoveryTrial()){
            return index == getRealTimeGenericDatabaseIndex();
        }
        return false;
    }
    
    public static String getDiscoveryTrialDriverName(int index, boolean isRealtime){     
        if (isRealtime){
            if(index == 0){
                // only one non-generic in the list
                return "com.microsoft.sqlserver.jdbc.SQLServerDriver";             
            }
        }
        else{
            switch (index)
            {
                case 0:
                    return "com.microsoft.sqlserver.jdbc.SQLServerDriver";
                case 1:
                    return "com.mysql.jdbc.Driver";
                case 2:
                    return "oracle.jdbc.OracleDriver";                
                case 3:
                    return "org.postgresql.Driver";
                case 4:
                    return "org.apache.hadoop.hive.jdbc.HiveDriver";
                case 5:
                    return "com.sap.db.jdbc.Driver";
            }
        }
        return null;
    }
    
    public static String getDriverName(int index, boolean isRealtime)
    {
        if(Util.isDiscoveryTrial()){
            return getDiscoveryTrialDriverName(index, isRealtime);
        }
        
        if (isRealtime)
        {
            switch (index)
            {
                case 0:
                    return "com.microsoft.sqlserver.jdbc.SQLServerDriver";
                case 1:
                case 2:
                    return "com.mysql.jdbc.Driver";
                case 3:
                    return "oracle.jdbc.OracleDriver";
                case 4:
                case 5:
                case 6:
                case 7:
                    return "sun.jdbc.odbc.JdbcOdbcDriver";
                case 8:
                case 9:
                case 10:
                case 11:
                    return "org.olap4j.driver.xmla.XmlaOlap4jDriver";
                case 12:
                    return "com.vertica.Driver";
                case 13:
                    return "com.teradata.jdbc.TeraDriver";
                case 14:
                    return "com.sybase.jdbc4.jdbc.SybDriver";
                case 15:
                    return "com.paraccel.Driver";
                case 16:
                    return "org.apache.hadoop.hive.jdbc.HiveDriver";
                case 17:
                    return "memdb.jdbcclient.DBDriver";
                case 18:
                    return "org.postgresql.Driver";   
                case 19:
                    return "org.apache.hive.jdbc.HiveDriver";
                case 20:
                    return "com.sap.db.jdbc.Driver";
            }
        }
        else
        {
            switch (index)
            {
                case 0:
                    return "com.microsoft.sqlserver.jdbc.SQLServerDriver";
                case 1:
                    return "com.mysql.jdbc.Driver";
                case 2:
                    return "oracle.jdbc.OracleDriver";
                case 3:
                    return "sun.jdbc.odbc.JdbcOdbcDriver";
                case 4:
                    return "org.postgresql.Driver";
                case 5:
                    return "org.apache.hadoop.hive.jdbc.HiveDriver";
                case 6:
                    return "com.sap.db.jdbc.Driver";
            }
        }
        return null;
    }

    public String retrievePassword()
    {
        return es.decrypt(password);
    }

    public void putPassword(String password)
    {
        this.password = es.encrypt(password);
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    public String getUsername()
    {
        return es.decrypt(username);
    }

    public void setUsername(String username)
    {
        this.username = es.encrypt(username);
    }

    public int getPort()
    {
        return port;
    }

    public void setPort(int port)
    {
        this.port = port;
    }

    public void setPort(String portString)
    {
        port = 0;
        try
        {
            if (portString.length() > 0)
                port = Integer.valueOf(portString);
        } catch (Exception ex)
        {
            logger.debug("Unable to parse portNumber " + portString + ". Using default port 0");
        }
    }

    public String getXML()
    {
        try
        {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            BufferedOutputStream os = new BufferedOutputStream(baos);

            XMLEncoder xml = new XMLEncoder(os);
            xml.writeObject(this);
            xml.flush();
            baos.close();
            return baos.toString();
        } catch (IOException ex)
        {
            Logger.getLogger(DatabaseConnection.class).error(null, ex);
        }
        return (null);
    }

    public int getTimeout()
    {
        return timeout;
    }

    public void setTimeout(int timeout)
    {
        this.timeout = timeout;
    }

    public void setTimeout(String timeoutString)
    {
        timeout = -1;
        try
        {
            if (timeoutString.length() > 0)
                timeout = Integer.valueOf(timeoutString);
        } catch (Exception ex)
        {
            logger.debug("Unable to parse timeout " + timeoutString + ". Using default timeout -1");            
        }
    }
    
    public int getTrialFetchSize(int index, boolean isRealtime){
        if (isRealtime)
        {
            switch (index)
            {
                case 0:
                    return 10000; // MS SQL Server                
                default:
                    return 0;
            }
        }
        else
        {
            switch (index)
            {
                case 0:
                    return 10000; // MS SQL Server
                case 1:
                    return Integer.MIN_VALUE; // MySQL - working around a bug
                case 2:
                    return 0; // Oracle - don't set a value                
                case 3:
                    return 10000; // PostgreSQL / Redshift
                default:
                    return 0;
            }
        }
    }

    public int getFetchSize(int index, boolean isRealtime)
    {
        if(Util.isDiscoveryTrial()){
            return getTrialFetchSize(index, isRealtime);
        }
        if (isRealtime)
        {
            switch (index)
            {
                case 0:
                    return 10000; // MS SQL Server
                case 1:
                case 2:
                    return Integer.MIN_VALUE; // MySQL - working around a bug
                case 3:
                    return 0; // Oracle - don't set a value
                case 4:
                case 5:
                case 6:
                case 7:
                    return 0; // JDBC-ODBC bridge - don't set a value
                case 8:
                    return 0;
                case 9:
                    return 0;
                case 10:
                    return 0;
                case 11:
                    return 0;
                case 12:
                    return 0;
                case 13:
                    return 0;
                case 14:
                    return 0;
                case 18:
                    return 10000; // PostgreSQL / Redshift                    
                case 19:
                    return 0;
                default:
                    return 0;
            }
        }
        else
        {
            switch (index)
            {
                case 0:
                    return 10000; // MS SQL Server
                case 1:
                    return Integer.MIN_VALUE; // MySQL - working around a bug
                case 2:
                    return 0; // Oracle - don't set a value
                case 3:
                    return 0; // JDBC-ODBC bridge - don't set a value
                case 4:
                    return 10000; // PostgreSQL / Redshift
                default:
                    return 0;
            }
        }
    }

    public int getTrialDriverIndex(boolean isRealtime){
        if (isRealtime){
            if (driverName.equals("com.microsoft.sqlserver.jdbc.SQLServerDriver"))
                return 0;            
        }
        else{
            if (driverName.equals("com.microsoft.sqlserver.jdbc.SQLServerDriver"))
                return 0;
            else if (driverName.equals("com.mysql.jdbc.Driver"))
                return 1;
            else if (driverName.equals("oracle.jdbc.OracleDriver"))
                return 2;            
            else if (driverName.equals("org.postgresql.Driver"))
                return 3;
            else if (driverName.equals("org.apache.hadoop.hive.jdbc.HiveDriver"))
                return 4;
            else if (driverName.equals("com.sap.db.jdbc.Driver"))
                return 5;
        }
        return 0;
    }
    
    public int getFetchSize(boolean isRealtime)
    {
        return getFetchSize(genericDriver ? getGenericDatabaseIndex() : getDriverIndex(isRealtime), isRealtime);
    }

    public int getDriverIndex(boolean isRealtime)
    {
        if (driverName == null)
            return 0;
        if(Util.isDiscoveryTrial()){
            return getTrialDriverIndex(isRealtime);
        }
        if (isRealtime)
        {
            if (driverName.equals("com.microsoft.sqlserver.jdbc.SQLServerDriver"))
                return 0;
            else if (driverName.equals("com.mysql.jdbc.Driver"))
            {
                if (databaseType != null && databaseType.equals(INFOBRIGHT))
                {
                    return 1;
                }
                return 2;
            }
            else if (driverName.equals("oracle.jdbc.OracleDriver"))
                return 3;
            else if (driverName.equals("sun.jdbc.odbc.JdbcOdbcDriver"))
            {
                if (databaseType != null)
                {
                    if (databaseType.equals(ORACLE_ODBC))
                        return 4;
                    else if (databaseType.equals(MYSQL_ODBC))
                        return 5;
                    else if (databaseType.equals(DB2_ODBC))
                        return 6;
                    else if (databaseType.equals(MSSQL_ODBC))
                        return 7;
                } else
                {
                    return 4;
                }
            }
            else if (driverName.equals("org.olap4j.driver.xmla.XmlaOlap4jDriver"))
            {
                if (databaseType.equals(MICROSOFT_ANALYSIS_SERVICES))
                    return 8;
                if (databaseType.equals(PENTAHO_MONDRIAN))
                    return 9;
                if (databaseType.equals(HYPERIAN_ESSBASE))
                    return 10;
                if (databaseType.equals(SAP_BW))
                    return 11;
            }
            else if (driverName.equals("com.vertica.Driver"))
                return 12;
            else if (driverName.equals("com.teradata.jdbc.TeraDriver"))
                return 13;
            else if (driverName.equals("com.sybase.jdbc4.jdbc.SybDriver"))
                return 14;
            else if (driverName.equals("com.paraccel.Driver"))
                return 15;
            else if (driverName.equals("org.apache.hadoop.hive.jdbc.HiveDriver")) // XXX old hive connection information, should we kill hive for live access?
                return 16;
            else if (driverName.equals("memdb.jdbcclient.DBDriver"))
                return 17;
            else if (driverName.equals("org.postgresql.Driver"))
                return 18;
            else if (driverName.equals("org.apache.hive.jdbc.HiveDriver")) // HiveServer2 connection string
                return 19;            
            else if (driverName.equals("com.sap.db.jdbc.Driver"))
                return 20;
        }
        else
        {
            if (driverName.equals("com.microsoft.sqlserver.jdbc.SQLServerDriver"))
                return 0;
            else if (driverName.equals("com.mysql.jdbc.Driver"))
                return 1;
            else if (driverName.equals("oracle.jdbc.OracleDriver"))
                return 2;
            else if (driverName.equals("sun.jdbc.odbc.JdbcOdbcDriver"))
                return 3;
            else if (driverName.equals("org.postgresql.Driver"))
                return 4;
            else if (driverName.equals("org.apache.hadoop.hive.jdbc.HiveDriver")) // XXX old hive connection information
                return 5;
            else if (driverName.equals("com.sap.db.jdbc.Driver"))
                return 6;
        }
        return 0;
    }

    public static String getServerLabel(int index, boolean isRealTime)
    {
        if (isRealTime)
        {    
            if(isTrialRealTimeGenericDatabaseIndex(index)){
                return "Driver Name:";
            }
            switch (index)
            {
                case 4:
                case 5:
                case 6:
                case 7:
                    return "Data source Name:";
                case REALTIME_GENERIC_DATABASE:
                    return "Driver Name:";
                default:
                    return "Server Name:";
            }
        }
        else
        {
            if(Util.isDiscoveryTrial()){
                if(DatabaseConnection.isTrialGenericDatabaseIndex(index)){
                    return "Driver Name";
                }
                return "Server Name:";
            }
            
            switch (index)
            {
                case 3:
                    return "Data source Name:";
                case GENERIC_DATABASE:
                    return "Driver Name:";
                default:
                    return "Server Name:";
            }
        }        
    }
    
    public static String getDefaultServerNameText(int index, boolean isRealTime)
    {
        if (isRealTime)
        {
            if(isTrialRealTimeGenericDatabaseIndex(index)){
                return "";
            }
            switch (index)
            {
                case REALTIME_GENERIC_DATABASE:
                    return "";
                default:
                    return "localhost";
            }
        }
        else
        {
            if(DatabaseConnection.isTrialGenericDatabaseIndex(index)){
                return "";//Generic JDBC Driver Name
            }
            switch (index)
            {
                case GENERIC_DATABASE:
                    return "";//Generic JDBC Driver Name
                default:
                    return "localhost";
            }
        }        
    }
    
    public static String getDatabaseNameLabel(int index, boolean isRealTime)
    {
        if (isRealTime)
        {
            if(isTrialRealTimeGenericDatabaseIndex(index)){
                return "Connection String:";
            }
            switch (index)
            {
                case REALTIME_GENERIC_DATABASE:
                     return "Connection String:";
                default:
                     return "Database Name:";
            }
        }
        else
        {
            if(DatabaseConnection.isTrialGenericDatabaseIndex(index)){
                return "Connection String:";
            }
            
            switch (index)
            {
                case GENERIC_DATABASE:
                    return "Connection String:";
                default:
                    return "Database Name:";
            }
        }        
    }
    
    public static String getDatabaseNameToolTip(int index, boolean isRealTime)
    {
        if (isRealTime)
        {
            if(isTrialRealTimeGenericDatabaseIndex(index)){
                return "For example: jdbc:sqlserver://localhost;databaseName=Northwind";
            }
            switch (index)
            {
                case REALTIME_GENERIC_DATABASE:
                     return "For example: jdbc:sqlserver://localhost;databaseName=Northwind";
                default:
                     return "";
            }
        }
        else
        {
            if(DatabaseConnection.isTrialGenericDatabaseIndex(index)){
                return "For example: jdbc:sqlserver://localhost;databaseName=Northwind";
            }
            switch (index)
            {
                case GENERIC_DATABASE:
                    return "For example: jdbc:sqlserver://localhost;databaseName=Northwind";
                default:
                    return "";
            }
        }        
    }
    
    public static String getServerNameToolTip(int index, boolean isRealTime)
    {
        if (isRealTime)
        {
            if(isTrialRealTimeGenericDatabaseIndex(index)){
                return "For example: com.microsoft.sqlserver.jdbc.SQLServerDriver";
            }
            switch (index)
            {
                case REALTIME_GENERIC_DATABASE:
                     return "For example: com.microsoft.sqlserver.jdbc.SQLServerDriver";
                default:
                     return "";
            }
        }
        else
        {
            if(DatabaseConnection.isTrialGenericDatabaseIndex(index)){
                return "For example: com.microsoft.sqlserver.jdbc.SQLServerDriver";
            }
            switch (index)
            {
                case GENERIC_DATABASE:
                    return "For example: com.microsoft.sqlserver.jdbc.SQLServerDriver";
                default:
                    return "";
            }
        }        
    }

    public static boolean getHasDatabaseName(int index, boolean isRealTime)
    {
        if (isRealTime)
        {
            switch (index)
            {
                case 4:
                case 5:
                case 6:
                case 7:
                case 19: // impala
                case 20: // hana
                    return false;
                default:
                    return true;
            }
        }
        else
        {
            if(Util.isDiscoveryTrial()){  
                switch (index)
                {
                    case 5: // hana
                        return false;
                }
                return true;
            }
            switch (index)
            {
                case 3:
                case 6: // hana
                    return false;
                default:
                    return true;
            }
        }
    }
    
    public static boolean getHasPort(int index, boolean isRealTime)
    {
        if (isRealTime)
        {
            if(isTrialRealTimeGenericDatabaseIndex(index)){
                return false;
            }
            switch (index)
            {
                case 4:
                case 5:
                case 6:
                case 7:
                case REALTIME_GENERIC_DATABASE:
                    return false;
                default:
                    return true;
            }
        }
        else
        {
            if(Util.isDiscoveryTrial()){
                if(DatabaseConnection.isTrialGenericDatabaseIndex(index)){
                    return false;
                }   
                return true;
            }
            
            switch (index)
            {
                case 3:
                    return false;
                case GENERIC_DATABASE:
                    return false;
                default:
                    return true;
            }
        }
    }
    
    public static boolean getHasSQLType(int index, boolean isRealTime)
    {
        if (isRealTime)
        {
            if(isTrialRealTimeGenericDatabaseIndex(index)){
                return true;
            }
            switch (index)
            {
                case REALTIME_GENERIC_DATABASE:
                    return true;
                default:
                    return false;
            }
        }
        return false;
    }

    public static String getConnectString(String driverName, String serverName, String databaseName, int port, String databaseType, String schema, boolean useTimeZone, String processingTZ,SSHTunnel sshTunnel)
    {
        int sshInternalPort = 0;
        if(sshTunnel!= null && sshTunnel.isIsSSHTunnelConfigured())
        {
            sshInternalPort = port;
            port = Integer.valueOf(sshTunnel.getLocalPort());
        }
        if (driverName == null || driverName.equals("com.microsoft.sqlserver.jdbc.SQLServerDriver"))
        {
            if(sshTunnel != null && sshTunnel.isIsSSHTunnelConfigured())
            {
                sshInternalPort = (sshInternalPort > 0 ? sshInternalPort : 1433);
                sshTunnel.setTunnelInternalPort(String.valueOf(sshInternalPort));
            }
            return "jdbc:sqlserver://" + serverName + (port > 0 ? ":" + port : "") 
                    + ";databaseName=" + databaseName;
        } else if (driverName.equals("com.mysql.jdbc.Driver"))
        {
            boolean isIB = databaseType != null && databaseType.equals(INFOBRIGHT);
            String schemaOrDbAppendStr = "";
            if(schema != null)
            {
                schemaOrDbAppendStr = schema;
            }
            else if(databaseName != null)
            {
                schemaOrDbAppendStr = databaseName;
            }
            String connectString = "jdbc:mysql://" + serverName + (port > 0 ? ":" + port : (isIB ? ":5029" : ":3306")) 
                    + "/" + schemaOrDbAppendStr;
            boolean isFirst = true;
            if (useTimeZone == true && processingTZ != null)
            {
                connectString = connectString + "?useTimezone=true" + "&serverTimezone=" + processingTZ;
                isFirst = false;
            }
            connectString = connectString + (isFirst? "?" : "&") + "useUnicode=true&characterEncoding=utf8";
            
            if(sshTunnel != null && sshTunnel.isIsSSHTunnelConfigured())
            {
                sshInternalPort = (sshInternalPort > 0 ? sshInternalPort : (isIB ? 5029 : 3306));
                sshTunnel.setTunnelInternalPort(String.valueOf(sshInternalPort));
            }
            return connectString;
        } else if (driverName.equals("oracle.jdbc.OracleDriver"))
        {
            if(sshTunnel != null && sshTunnel.isIsSSHTunnelConfigured())
            {
                sshInternalPort = (sshInternalPort > 0 ? sshInternalPort : 1521);
                sshTunnel.setTunnelInternalPort(String.valueOf(sshInternalPort));
            }
            return "jdbc:oracle:thin:@" + serverName + ":" + (port > 0 ? port : "1521") + ":" + databaseName;
        } else if (driverName.equals("sun.jdbc.odbc.JdbcOdbcDriver"))
        {
            return "jdbc:odbc:" + serverName;
        } else if (driverName.equals("org.olap4j.driver.xmla.XmlaOlap4jDriver"))
        {
            if (databaseType.equals(MICROSOFT_ANALYSIS_SERVICES))
                return "jdbc:xmla:Server=http://" + serverName + ";Catalog=" + databaseName;
            else if (databaseType.equals(PENTAHO_MONDRIAN))
                return "jdbc:xmla:Server=http://" + serverName + ";Catalog=" + databaseName;
            else if (databaseType.equals(SAP_BW))
                return "jdbc:xmla:Server=http://" + serverName + ";Catalog=" + databaseName+";Provider=SAP_BW";
            else if (databaseType.equals(HYPERIAN_ESSBASE))
                return "jdbc:xmla:Server=http://" + serverName + ";Catalog=" + databaseName+";Provider=Essbase";
        } else if (driverName.equals("com.vertica.Driver"))
        {
            return "jdbc:vertica://" + serverName + (port > 0 ? ":" + port : "") + "/" + databaseName;
        } else if (driverName.equals("com.teradata.jdbc.TeraDriver"))
        {
            return "jdbc:teradata://" + serverName + "/database=" + databaseName + ",tmode=ANSI,charset=UTF8";
        } else if (driverName.equals("org.postgresql.Driver"))
        {
            if(sshTunnel != null && sshTunnel.isIsSSHTunnelConfigured())
            {
                sshInternalPort = (sshInternalPort > 0 ? sshInternalPort : 5432);
                sshTunnel.setTunnelInternalPort(String.valueOf(sshInternalPort));
            }
            return "jdbc:postgresql://" + serverName + ":" + (port > 0 ? port : "5432") + "/" + databaseName;
        } else if (driverName.equals("com.sybase.jdbc4.jdbc.SybDriver"))
        {
            if(sshTunnel != null && sshTunnel.isIsSSHTunnelConfigured())
            {
                sshInternalPort = (sshInternalPort > 0 ? sshInternalPort : 2638);
                sshTunnel.setTunnelInternalPort(String.valueOf(sshInternalPort));
            }
            String connectStr = "jdbc:sybase:Tds:" + serverName + ":" + (port > 0 ? port : "2638");
            if (databaseName != null && !databaseName.isEmpty())
                connectStr = connectStr + "?ServiceName=" + databaseName;
            return connectStr;
        } else if (driverName.equals("com.paraccel.Driver"))
        {
            if(sshTunnel != null && sshTunnel.isIsSSHTunnelConfigured())
            {
                sshInternalPort = (sshInternalPort > 0 ? sshInternalPort : 5439);
                sshTunnel.setTunnelInternalPort(String.valueOf(sshInternalPort));
            }
            String connectStr = "jdbc:paraccel://" + serverName + ":" + (port > 0 ? port : "5439");
            if (databaseName != null && !databaseName.isEmpty())
                connectStr = connectStr + "/" + databaseName;
            return connectStr;
        } else if (driverName.equals("org.apache.hadoop.hive.jdbc.HiveDriver"))
        {
            int defaultport = 10000;
            if(sshTunnel != null && sshTunnel.isIsSSHTunnelConfigured())
            {
                sshInternalPort = (sshInternalPort > 0 ? sshInternalPort : defaultport);
                sshTunnel.setTunnelInternalPort(String.valueOf(sshInternalPort));
            }
            String connectStr = "jdbc:hive://" + serverName + ":" + (port > 0 ? port : defaultport);
            if (databaseName != null && !databaseName.isEmpty())
                connectStr = connectStr + "/" + databaseName;
            else
        	connectStr = connectStr + "/default";
            return connectStr;
        } else if (driverName.equals("org.apache.hive.jdbc.HiveDriver"))
        {
            int defaultport = 21050;
            if(sshTunnel != null && sshTunnel.isIsSSHTunnelConfigured())
            {
                sshInternalPort = (sshInternalPort > 0 ? sshInternalPort : defaultport);
                sshTunnel.setTunnelInternalPort(String.valueOf(sshInternalPort));
            }
            String connectStr = "jdbc:hive2://" + serverName + ":" + (port > 0 ? port : defaultport) + "/;auth=noSasl";
            // no database name
            return connectStr;                
        } else if (driverName.equals("com.sap.db.jdbc.Driver"))
        {
            int defaultport = 30015;
            if(sshTunnel != null && sshTunnel.isIsSSHTunnelConfigured())
            {
                sshInternalPort = (sshInternalPort > 0 ? sshInternalPort : defaultport);
                sshTunnel.setTunnelInternalPort(String.valueOf(sshInternalPort));
            }
            String connectStr = "jdbc:sap://" + serverName + ":" + (port > 0 ? port : defaultport);
            // no database name
            return connectStr;
                
        } else if (driverName.equals("memdb.jdbcclient.DBDriver"))
        {
            return ("inprocess://"+databaseName);
        }
        logger.error("Returning null connection string: " + driverName + ", " + serverName + ", " + databaseName + ", " + port + ", " + databaseType + ", " + schema);
        return null;
    }
}
