/**
 * $Id: WebDAVError.java,v 1.8 2011-07-26 07:50:36 rchandarana Exp $
 *
 * Copyright (C) 2010 Birst, Inc. All rights reserved.
 * BIRST PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.birst.dataconductor;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author bpeters
 */
public class WebDAVError
{

    private static Pattern errorPattern = Pattern.compile("Error \\d\\d\\d:");
    private int code;
    private String message;
    public static final int GENERAL_ERROR = 0;
    public static final int UNABLE_NEGOTIATE_AUTHENTICATION = 1;
    public static final int UNABLE_TO_GENERATE_DIGEST_AUTHENTICATION = 2;
    public static final int GENERAL_AUTHENTICATION_ERROR = 3;
    public static final int NO_WEBDAV_PRIVILEGES = 4;
    public static final int UNABLE_TO_AUTHENTICATE = 5;
    public static final int UNABLE_TO_CONNECT = 6;
    public static final int SPACE_NOT_FOUND = 7;
    public static final int UPLOAD_ERROR = 8;
    public static final int CURRENTLY_PROCESSING = 9;
    public static final int VERSION_MISMATCH = 10;
    public static final int DOWNLOAD_ERROR = 11;
    public static final int SPACE_UNAVAILABLE = 12;
    
    public WebDAVError()
    {
    }
    
    public WebDAVError(int code, String message)
    {
        this.code = code;
        this.message = message;
    }
    
    public int getCode()
    {
        return code;
    }

    public void setCode(int code)
    {
        this.code = code;
    }

    public WebDAVError(int code)
    {
        this.code = code;
    }

    public String getMessage()
    {
        return message;
    }

    public void setMessage(String message)
    {
        this.message = message;
    }

    public static WebDAVError getError(String response)
    {
        Matcher m = errorPattern.matcher(response);
        if (m.find())
        {
            String s = m.group();
            int responsecode = Integer.valueOf(s.substring(7, 9));
            int index = response.indexOf("</body>",m.end());
            WebDAVError wde = new WebDAVError(responsecode);
            wde.message = response.substring(m.end(),index);
            return (wde);
        }
        return (null);
    }

    @Override
    public String toString()
    {
        String resp = "Unknown Error";
        switch (code)
        {
            case GENERAL_ERROR:
               resp = "General error";
               break;
            case UNABLE_NEGOTIATE_AUTHENTICATION:
                resp = "Unable to negotiate authentication";
                break;
            case UNABLE_TO_GENERATE_DIGEST_AUTHENTICATION:
                resp = "Unable to generate digest authentication";
                break;
            case GENERAL_AUTHENTICATION_ERROR:
                resp = "Authentication error";
                break;
            case NO_WEBDAV_PRIVILEGES:
                resp = "Insufficient privileges to run Birst Connect";
                break;
            case UNABLE_TO_AUTHENTICATE:
                resp = "Unable to authenticate";
                break;
            case UNABLE_TO_CONNECT:
                resp = "Unable to connect to server";
                break;
            case SPACE_NOT_FOUND:
                resp = "Space not found - please make sure it was not deleted or renamed";
                break;
            case UPLOAD_ERROR:
                resp = "Error uploading file";
                break;
            case CURRENTLY_PROCESSING:
                resp = "Unable to upload data - currently processing";
                break;
            case VERSION_MISMATCH:
                resp = "Version mismatch";
                break;
            case DOWNLOAD_ERROR:
                resp = "Download error";
                break;
            case SPACE_UNAVAILABLE:
                resp = "Space Unavailable: Space is involved in either swap, delete or copy operation.";
                break;
        }
        return resp + ": " + message;
    }
}
