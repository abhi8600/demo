/**
 * $Id: BirstAuthenticator.java,v 1.5 2012-08-14 00:48:00 BIRST\ricks Exp $
 *
 * Copyright (C) 2010 Birst, Inc. All rights reserved.
 * BIRST PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.birst.dataconductor;

import java.net.Authenticator;
import java.net.PasswordAuthentication;
import java.net.URL;
import org.apache.log4j.Logger;

/**
 *
 * @author ricks
 */
public class BirstAuthenticator extends Authenticator {
    private static final String realm = "authenticate@birst.com";
    private final static Logger logger = Logger.getLogger(BirstAuthenticator.class);

    private String serverUser;
    private String serverPassword;

    private String proxyUser;
    private String proxyPassword;

    public void setServerUsername(String name)
    {
        serverUser = name;
    }

    public void setServerPassword(String password)
    {
        serverPassword = password;
    }

    public void setProxyUsername(String name)
    {
        proxyUser = name;
    }

    public void setProxyPassword(String password)
    {
        proxyPassword = password;
    }

    @Override
    public PasswordAuthentication getPasswordAuthentication()
    {
        if (logger.isDebugEnabled())
        {
            StringBuilder sb = new StringBuilder("-- Password Authentication --");
            sb.append("\n\ttype: " + getRequestorType());
            sb.append("\n\thost: " + getRequestingHost());
            sb.append("\n\tport: " + getRequestingPort());
            sb.append("\n\tprompt: " + getRequestingPrompt());
            sb.append("\n\tprotocol: " + getRequestingProtocol());
            sb.append("\n\tscheme: " + getRequestingScheme());
            sb.append("\n\tsite: " + getRequestingSite());
            sb.append("\n\tURL: " + filterPassword(getRequestingURL()));
            logger.debug(sb.toString());
        }
        if (getRequestorType() == Authenticator.RequestorType.SERVER && realm.equals(getRequestingPrompt()))
        {
            return new PasswordAuthentication(serverUser, serverPassword.toCharArray());
        }
        if (getRequestorType() == Authenticator.RequestorType.PROXY)
        {
            return new PasswordAuthentication(proxyUser, proxyPassword.toCharArray());
        }
        return null;
    }
    
    private String filterPassword(URL url)
    {
        // protocol://name:password@host/path
        if (url != null)
            return url.getProtocol() + "://" + url.getHost() + ':' + url.getPort() + '/' + url.getPath();
        return null;
    }
}
