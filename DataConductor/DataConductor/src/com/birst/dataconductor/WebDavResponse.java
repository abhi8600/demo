/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author rchandarana
 */

package com.birst.dataconductor;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author bpeters
 */
public class WebDavResponse
{

    private static Pattern responsePattern = Pattern.compile("Status \\d{3}:");
    private int code;
    private String message;
    public static final int LAST_PROCESS_FAILED = 2;
    public static final int LAST_PROCESS_SUCCESSFULLY_COMPELETED = 0;
    public static final int CURRENTLY_PROCESSING = 1;
    public static final int NO_SERVER_RESPONSE = 3;
    
    public WebDavResponse()
    {
    }

    public WebDavResponse(int code, String message)
    {
        this.code = code;
        this.message = message;
    }

    public int getCode()
    {
        return code;
    }

    public void setCode(int code)
    {
        this.code = code;
    }

    public WebDavResponse(int code)
    {
        this.code = code;
    }

    public String getMessage()
    {
        return message;
    }

    public void setMessage(String message)
    {
        this.message = message;
    }

    public static WebDavResponse getResponse(String response)
    {
        Matcher m = responsePattern.matcher(response);
        if (m.find())
        {
            String s = m.group();
            int responsecode = Integer.valueOf(s.substring(7, 10));
            int index = response.indexOf("</body>",m.end());
            WebDavResponse wdr = new WebDavResponse(responsecode);
            wdr.message = response.substring(m.end(),index);
            return (wdr);
        }
        return (null);
    }

    @Override
    public String toString()
    {
        String resp = "";
        switch (code)
        {
            case LAST_PROCESS_FAILED:
               resp = "Last Processing Failed";
               break;
            case LAST_PROCESS_SUCCESSFULLY_COMPELETED:
                resp = "Last Processing Successfully Compeleted";
                break;
            case CURRENTLY_PROCESSING:
                resp = "Currently Processing";
                break;
        }
        return resp;
    }
}
