/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.birst.dataconductor;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.MalformedURLException;

/**
 *
 * @author bpeters
 */
public class GetReportResults
{

    private String path;
    private String directory;
    private DataConductorUtil dcu;
    private GraphicsObject go;

    public GetReportResults(String path, String directory, DataConductorUtil dcu, GraphicsObject go)
    {
        this.path = path;
        this.directory = directory;
        this.dcu = dcu;
        this.go = go;
    }

    public void run() throws GeneralErrorException, IOException, MalformedURLException, AuthenticationException {
        File dirFile = null;
        // Create command file
        try {
            PrintWriter writer = null;
            try
            {
                dirFile = dcu.getTempDirectory();
                File f = new File(dirFile.getPath() + File.separator + "birst.command");
                f.createNewFile();
                writer = new PrintWriter(new FileWriter(f));
                writer.println("exportreport " + path);
            }
            catch (Exception ex2)
            {
                if (go != null) {
                    go.showMessageDialog("Unable to write the command file: " + ex2.getMessage());
                }
                throw new GeneralErrorException("Unable to write the command file: " + ex2.getMessage());
            }
            finally
            {
                writer.close();
            }
            WebDavUploader wdu = new WebDavUploader(dcu.getUname(), dcu.getPword(), dcu.getSpace(), dcu.getUrl(), go, dcu.NUM_DEFAULT_TRIES, dcu, true);
            WebDAVError werror = wdu.uploadFile(dirFile.getPath(), "birst.command", false, null, false);
            if (werror != null)
            {
                if (go != null) {
                    go.showMessageDialog("Unable to upload the command file: " + werror.getMessage());
                }
                throw new GeneralErrorException("Unable to upload the command file: " + werror.getMessage());
            }
            WebDavGetFile wdgf = new WebDavGetFile(dcu.getUname(), dcu.getPword(), dcu.getSpace(), dcu.getUrl(), go, null, dcu.NUM_DEFAULT_TRIES);
            int index = path.lastIndexOf('/');
            String fname = path;
            if (index >= 0) {
                fname = path.substring(index + 1);
            }
            try
            {
                wdgf.getFile(directory, "output/" + fname + ".zip", fname + ".txt", true);
            }
            catch (Exception ex1)
            {
                if (go != null) {
                    go.showMessageDialog("Unable to download the report results: " + ex1.getMessage());
                }
                throw new GeneralErrorException("Unable to download the report results: " + ex1.getMessage());
           }
        } catch (Exception ex) {
            if (go != null) {
                go.showMessageDialog("Unable to export the report: " + ex.getMessage());
            }
            throw new GeneralErrorException("Unable to export the report: " + ex.getMessage());
        } finally {
            dcu.cleanupTempDirectory(dirFile);
        }
    }
}
