/**
 * $Id: WebDavGetFile.java,v 1.9 2011-07-25 13:04:36 rchandarana Exp $
 *
 * Copyright (C) 2010 Birst, Inc. All rights reserved.
 * BIRST PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.birst.dataconductor;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.*;

/**
 *
 * @author bpeters
 */
public class WebDavGetFile
{

    private String url;
    String username;
    String password;
    String space;
    GraphicsObject go;
    String baseLabel;
    int numRetries;
    private static int BUF_LEN = 10 * 1024;

    public WebDavGetFile(String username, String password, String space, String url, GraphicsObject go, String baseLabel, int numRetries)
    {
        this.username = username;
        this.password = password;
        this.space = space;
        this.url = url;
        this.go = go;
        this.baseLabel = baseLabel;
        this.numRetries = numRetries;
    }

    public WebDavGetFile(String url, String space)
    {
        this.url = url;
        this.space = space;
    }
    
    public void getFile(String directory, String filename, String saveasFilename, boolean insertNewLine) throws IOException, GeneralErrorException, MalformedURLException, AuthenticationException, SpaceUnavailableException {
        int index = saveasFilename.lastIndexOf("/");
        String saveasfname = index >= 0 ? saveasFilename.substring(index + 1) : saveasFilename;

        String path = "/" + URLEncoder.encode(space, "UTF-8").replace("+", "%20") + "/" + URLEncoder.encode(filename, "UTF-8").replace("+", "%20");
        HttpURLConnection hs = null;
        FileOutputStream fos = null;
        BufferedReader breader = null;
        try {
            hs = Util.getHttpURLConnection(url, path, "GET", true);
            hs.connect();
            Util.dumpHttpResponse(hs);
            int code = hs.getResponseCode();
            if (code < HttpURLConnection.HTTP_OK || code >= HttpURLConnection.HTTP_MULT_CHOICE) {
                throw new GeneralErrorException(hs.getResponseMessage());
            }
            String inputLine = null;
            File f = new File(directory + File.separator + saveasfname);
            fos = new FileOutputStream(f);
            breader = new BufferedReader(new InputStreamReader(hs.getInputStream()));
            while ((inputLine = breader.readLine()) != null) {
                //System.out.println(inputLine);
                fos.write(inputLine.getBytes());
                if(insertNewLine)
                {
                    fos.write(Util.NEW_LINE_CHAR.getBytes());
                }
                else
                {
                    System.out.println(inputLine);
                }
            }
        } finally {
            if (breader != null) {
                breader.close();
            }
            if (fos != null) {
                fos.close();
            }
            if (hs != null) {
                hs.disconnect();
            }
        }
    }
}
