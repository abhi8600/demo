/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.birst.dataconductor;

import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import org.apache.log4j.Logger;
import java.util.Collections;

/**
 *
 * @author bpeters
 */
public class ActivityLog
{
    private final static int MAX_SIZE = 10000; // so we don't blow memory, scrolling list of the last 10000 lines
    private List<String> activityLog = Collections.synchronizedList(new LinkedList<String>());
    private BufferedWriter logWriter;
    private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss z");
    private final static Logger logger = Logger.getLogger(ActivityLog.class);

    private void add(String line)
    {
        line = sdf.format(new Date()) + ": " + line;
        if (activityLog.size() > MAX_SIZE)
               activityLog.remove(0);
        activityLog.add(line);
        if (logWriter != null)
        {
            try
            {
                logWriter.write(line + "\r\n");
                logWriter.flush();
            } catch (Exception e)
            {
                logWriter = null;
                if (activityLog.size() > MAX_SIZE)
                    activityLog.remove(0);
                activityLog.add(e.getMessage());
            }
        }
    }

    public void add(String message, Throwable throwable)
    {
        StringWriter sw = null;
        PrintWriter pw = null;
        try {
            if(message != null)
            {
                this.add(message);
            }
            if(throwable != null)
            {
                sw = new StringWriter();
                pw = new PrintWriter(sw);
                throwable.printStackTrace(pw);
                pw.flush();
                this.add(sw.toString());
            }
        } catch(Exception ex) {
            if (logger.isTraceEnabled()) {
                logger.trace("Exception " + ex + " occured while processing method com.birst.dataconductor.ActivityLog.add ", ex);
            }
        }
        finally
        {
            try
            {
                if (pw != null)
                    pw.close();
            } catch (Exception e) {
                if (logger.isTraceEnabled()) {
                    logger.trace("Exception " + e + " occured while processing method com.birst.dataconductor.ActivityLog.add ", e);
                }
            }
        }

    }

    public void setLogFile(String filename)
    {
        if (logWriter != null)
            try
            {
                logWriter.close();
            } catch (IOException ex)
            {
                logger.error(null, ex);
            }
        logWriter = null;
        if (filename == null || filename.length() == 0)
            return;
        try
        {
            logWriter = new BufferedWriter(new FileWriter(filename, true));
        } catch (Exception e)
        {
            if (activityLog.size() > MAX_SIZE)
                activityLog.remove(0);
            activityLog.add(e.getMessage());
            logWriter = null;
            return;
        }
    }

    public String getLogs(String filename, long bytesToRead) throws FileNotFoundException, IOException
    {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        File f = new File (filename);
        if (f.exists())
        {
            if (bytesToRead <= 0 || bytesToRead > filename.length())
                bytesToRead = filename.length();
            FileInputStream fis = null;
            try
            {
                fis = new FileInputStream(filename);
                fis.skip(filename.length() - bytesToRead);
                int bufLen = bytesToRead < 10000 ? new Long(bytesToRead).intValue() : 10000;
                byte[] buffer = new byte[bufLen];
                int bytesRead = 0;
                while((bytesRead = fis.read(buffer, 0, bufLen)) != -1)
                {
                    baos.write(buffer, 0, bytesRead);
                }
            }
            finally
            {
                if (fis != null)
                {
                    try
                    {
                        fis.close();
                    } catch (Exception ex) {
                    	if (logger.isTraceEnabled()) {
                            logger.trace("Exception " + ex + " occured while processing method ActivityLog.getLogs(String, long) ", ex);
                        }
                    }
                }
            }
        }
        return baos.toString();
    }
    public String getText()
    {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < activityLog.size(); i++)
        {
            sb.append(activityLog.get(i));
            sb.append("\r\n");
        }
        return sb.toString();
    }

    public List<String> getLogList()
    {
        return activityLog;
    }

    public int getNumLines()
    {
        return activityLog.size();
    }
}
