/**
 * $Id: BaseException.java,v 1.1 2010-06-21 17:08:17 ricks Exp $
 *
 * Copyright (C) 2010 Birst, Inc. All rights reserved.
 * BIRST PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.birst.dataconductor;

/**
 *
 * @author ricks
 */
public class BaseException extends Exception
{
    protected BaseException()
    {
        super();
    }

    protected BaseException(String s)
    {
        super(s);
    }

    protected BaseException(String s, Throwable cause)
    {
        super(s, cause);
    }
}
