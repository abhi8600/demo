package com.birst.repositoryupgrade;

import java.io.File;

public abstract class RepositoryFix {

	abstract boolean validateRepository(File repository);
	
	abstract boolean fixRepository(File repository,File newrepository);
}
