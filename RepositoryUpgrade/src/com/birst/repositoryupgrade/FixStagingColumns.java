package com.birst.repositoryupgrade;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.Namespace;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.Format.TextMode;
import org.jdom2.output.XMLOutputter;

public class FixStagingColumns extends RepositoryFix{

	@Override
	/*
	 * (non-Javadoc) Verify if a customers repository has logicalName in StagingColumns.
	 * @see com.birst.repositoryupgrade.RepositoryFix#validateRepository(java.io.File)
	 */
	boolean validateRepository(File fromFile) {
		SAXBuilder builder = new SAXBuilder();
		try{
			Document document = (Document) builder.build(fromFile);
			Namespace ns = Namespace.getNamespace("http://www.successmetricsinc.com");
			Element rootNode = document.getRootElement();
			Element stagingTables = rootNode.getChild("StagingTables",ns);
			if(stagingTables == null){
				System.out.println("Invalid Repository. No Staging Tables Found");
				return false;
			}
			List<Element> stagingTablesList = stagingTables.getChildren();
			if(stagingTablesList.isEmpty()){
				System.out.println("Invalid Repository. No Staging Tables Found");
				return false;
			}
			for(Element stagingTable : stagingTablesList){
				Element columns = stagingTable.getChild("Columns",ns);
				List<Element> stagingColumns = columns.getChildren();
				for(Element sc : stagingColumns)
				{
					if(sc.getChild("LogicalName",ns) !=null){
						return true;
					}
				}
			}
		
		}
		catch (IOException io) {
			System.out.println(io.getMessage());
		  } catch (JDOMException jdomex) {
			System.out.println(jdomex.getMessage());
		  }
		return false;
	}

	@Override
	boolean fixRepository(File repository, File newrepository) {
		SAXBuilder builder = new SAXBuilder();
		try{
			Document document = (Document) builder.build(repository);
			Namespace ns = Namespace.getNamespace("http://www.successmetricsinc.com");
			Element rootNode = document.getRootElement();
			rootNode.setNamespace(ns);
			Element stagingTables = rootNode.getChild("StagingTables",ns);
			if(stagingTables == null){
				System.out.println("Invalid Repository. No Staging Tables Found");
				return false;
			}
			List<Element> stagingTablesList = stagingTables.getChildren();
			if(stagingTablesList.isEmpty()){
				System.out.println("Invalid Repository. No Staging Tables Found");
				return false;
			}
			for(Element stagingTable : stagingTablesList){
				Element columns = stagingTable.getChild("Columns",ns);
				List<Element> stagingColumns = columns.getChildren();
				for(Element sc : stagingColumns)
				{
					Element logicalName = sc.getChild("LogicalName",ns);
					if(logicalName !=null){
						Element name = sc.getChild("Name",ns);
						Element physicalName = new Element("PhysicalName",ns);
						System.out.println("Removing LogicalName  : " + logicalName.getText() );
						System.out.println("Adding PhysicalName : " + name.getText());
						physicalName.setText(name.getText());
						name.setText(logicalName.getText());
						sc.removeChild("LogicalName",ns);
						int i = sc.indexOf(name);
						sc.addContent(i+2,physicalName);
					}
				}
			}
			Format xmlFormat = Format.getRawFormat();
			xmlFormat.setTextMode(TextMode.PRESERVE);			
			xmlFormat.setEncoding("utf-8");
			xmlFormat.setOmitDeclaration(false);
			xmlFormat.setOmitEncoding(false);
			xmlFormat.setExpandEmptyElements(false);
			XMLOutputter xmlOutputter = new XMLOutputter(xmlFormat);
			xmlOutputter.output(document, new FileWriter(newrepository));
			System.out.println("Repository Fix Completed");
		}
		catch (IOException io) {
			System.out.println(io.getMessage());
			return false;
		  } catch (JDOMException jdomex) {
			System.out.println(jdomex.getMessage());
			return false;
		  }
		return true;
	}

}
