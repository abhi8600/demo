package com.birst.repositoryupgrade;

import java.io.File;
import java.io.IOException;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;

public class Main {

	public static void main(String[] args){
			
			if(args.length==0 || args[0] ==null || args[1] == null ){
				System.out.println("Usage : java com.birst.repositoryupgrade <fromfilename> <tofilename>");
				return;
			}
			String fromFileName = args[0];
			String toFileName = args[1];
			
			
			File fromFile = new File(fromFileName);
			if(!fromFile.exists()){
				System.out.println("File Not Found : " + fromFileName);
				return;
			}
			
			File toFile = new File(toFileName);
			FixStagingColumns fx = new FixStagingColumns();
			if(fx.validateRepository(fromFile)){
				System.out.println("BPD-22689 Logical Name was found in the Repository. This will be fixed by removing LogicalName and adding up the PhysicalName in the repository.");
				fx.fixRepository(fromFile, toFile);
			}
	}
	
}
