Starting this project as a standalone utility in case we want to upgrade the repository and cannot depracate the parameters.
The current project removes the logicalName for StagingColumns and adds up a PhysicalName instead. This may be required by support and services team
when upgrading a space with column lenghts exceeding length than that supported by the underlying database.

Usage :

java com.birst.repositoryupgrade.Main <path_to_original_repository> <path_to_new_repository>