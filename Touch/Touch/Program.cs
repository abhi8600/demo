﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Touch
{
    class Program
    {
        public static void Main(string[] args)
        {
            try
            {
                if (args.Length != 1)
                {
                    Console.Out.WriteLine("Invalid arguments for touch");
                }
                FileInfo fi = new FileInfo(args[0]);
                if (!fi.Exists)
                {
                    Console.Out.WriteLine("File does not exist:" + fi.FullName);
                }
                File.SetLastWriteTime(fi.FullName, DateTime.Now);
                Console.Out.WriteLine("Touched " + fi.FullName + " successfully");
            }
            catch (Exception ex)
            {
                Console.Out.WriteLine(ex.Message, ex);
            }
        }
    }
}
