package com.successmetricsinc.util;

import java.util.Properties;
import java.util.Map.Entry;

import org.apache.log4j.Logger;
import org.apache.log4j.Level;

public class DebugUtils {
	
	private static final String HEADER = "-----------------------------------------------";
	
	/**
	 * Dumps the system environment variables (System.getenv()) to the provided logger
     * instance at the logging level requested.
	 * @param logger is the logger instance to which these results should be output.
	 * @param level is the level at which to log these results.
	 */
	public static void logEnviroment(final Logger logger, final Level level) {
		// Dump all system environment variables
		logger.log(level, HEADER);
		logger.log(level, "SYSTEM ENVIRONMENT VARIABLES");
		logger.log(level, HEADER);
		for (String varKey : System.getenv().keySet()) {
			logger.log(level, "Key: " + varKey + ";\tValue: " + System.getenv(varKey));
		}
    }

    /**
     * Dumps the system properties (System.getProperties()) to the provided logger
     * instance at the logging level requested.
     * @param logger is the logger instance to which these results should be output.
     * @param level is the level at which to log these results.
     */
    public static void logProperties(final Logger logger, final Level level) {
		// Dump all system environment variables
		logger.log(level, HEADER);
		logger.log(level, "JVM PROPERTIES");
		logger.log(level, HEADER);
		final Properties properties = System.getProperties();
		for (Entry<Object, Object> entry : properties.entrySet()) {
			logger.log(level, "Key: " + entry.getKey() + ";\tValue: " + entry.getValue());
		}
		logger.log(level, HEADER);
	}
	
	/**
	 * For cases where the developer wants to see the details of the exception but 
	 * doesn't want to wrap and rethrow it.  For example, a third-party class throws an
	 * exception whose information is of interest but does not warrant terminating the process thread.
	 * @param logger is the logger instance to which these results should be output.
	 * @param throwable is the exception to be dumped.
	 * @param level is the level at which to log these results.
	 */
	public static void logException(final Logger logger, final Throwable throwable, final Level level) {
		// Dump each line in the exception stacktrace to the logger instance
		logger.log(level, "Exception occurred: " + throwable.getMessage());
		for (StackTraceElement element : throwable.getStackTrace()) {
			logger.log(level, "Stacktrace:\t" + element.toString());
		}
	}
}
