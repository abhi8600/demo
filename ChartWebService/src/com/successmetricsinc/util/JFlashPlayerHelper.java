package com.successmetricsinc.util;

import java.io.File;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.jpackages.jflashplayer.FlashPanel;
import com.jpackages.jflashplayer.JFlashLibraryLoadFailedException;

/**
 * Utility/helper methods related to the use of JFlashPlayer.
 * @author pconnolly
 */
public class JFlashPlayerHelper {

	/** 
	 * Required Flash Player version. 
	 * TODO: Should be externalized to config file. 
	 */
	public static final String REQUIRED_FLASH_VERSION = "10";
	
	/** 
	 * JFlashPlayer registration key for Birst. 
	 * TODO: Should be externalized to config file. 
	 */
	public static final String JFLASHPLAYER_REGISTRATION_KEY = "L6L4-36K9-94NI-4B7N-268N-84YK";
    private static final String CHECKING_VERSION
            = "ChartServer requires version %s of the Adobe Flash Player. Checking...";
	private static final String BAD_FLASH_VERSION 
	= "!!! Server does not have proper version of Flash installed! Should be version: %s. Functionality may be diminished.";
	private static Logger logger = Logger.getLogger(JFlashPlayerHelper.class);
	
	/**
	 * Checks that a valid CheckRenderer.swf file exists. 
	 * @param flashFilePath is the fully-qualified file path to the ChartRenderer.swf file.
     * @return true if .swf file exists; false otherwise.
	 */
	public static boolean validateSwfFileExists(final String flashFilePath) {
		// Basic check
		if (flashFilePath == null || flashFilePath.equals("")) {
			throw new IllegalArgumentException("Null or empty 'flashFilePath' passed to ChartRenderer().");
		}
		
		// Now check to see that it exists
		if (!new File(flashFilePath).exists()) {
			logger.fatal("ChartRenderer.swf file was not found using path: " + flashFilePath);
            return false;
		} else {
            logger.info("ChartRenderer.swf file found at: " + flashFilePath);
            return true;
        }
	}

	/**
	 * Seed JFlashPlayer with registration key before any other access.
	 */
	public static void setFlashPanelRegistration() {
	    FlashPanel.setRegistrationKey(JFLASHPLAYER_REGISTRATION_KEY);
	}

	/**
	 * Checks to see if the JFlashPlayer DLLs:
	 * <ul>
	 * <li>atk2k.dll</li>
	 * <li>jflash.dll</li>
	 * </ul>
	 * are loaded.  These two DLLs are required in order for the JFlashPlayer interface to 
	 * communicate with the Adobe Flash Player under Windows.
	 * @return true if the DLLs are able to load properly; false otherwise.
	 */
	public static boolean areJFlashPlayerDLLsLoaded() {
		boolean dllsAreLoaded = true;
		try {
			// Don't really care about the result in this case
			FlashPanel.hasFlashVersion(REQUIRED_FLASH_VERSION);
		} catch (JFlashLibraryLoadFailedException e) {
			logger.error("Load of JFlashPlayer DLLs failed.");
			DebugUtils.logException(logger, e, Level.FATAL);
			DebugUtils.logEnviroment(logger, Level.FATAL);
			DebugUtils.logProperties(logger, Level.FATAL);
			dllsAreLoaded = false;
		}
		return dllsAreLoaded;
	}
	
	/**
	 * Does two checks:
	 * <ol>
	 * <li>Checks that the required version (or later) of the Adobe Flash Player is available.</li>
	 * <li>Reports that it cannot load the Flash Player (usually the two JFlashPlayer DLLs are missing or corrupted)</li>
	 * </ol>
	 * @return true if the Flash Player is the correct version; false otherwise.  Whether the problem is the version
	 * or the missing DLLs, a 'false' will usually be a compelling reason to abort processing.
	 */
	public static boolean checkForValidFlashPlayer() {
		// Check to see if the correct version of Flash is installed
		boolean isValid = false;
		logger.info(String.format(CHECKING_VERSION, REQUIRED_FLASH_VERSION));
		try {
			isValid = FlashPanel.hasFlashVersion(REQUIRED_FLASH_VERSION);
            if (isValid) {
                logger.info("Valid version of Adobe Flash Player installed.");
            } else {
				logger.error(String.format(BAD_FLASH_VERSION, REQUIRED_FLASH_VERSION));
}
        } catch (JFlashLibraryLoadFailedException e) {
			logger.error("Attempting to check version of Flash Player, but load of JFlashPlayer library failed.");
			DebugUtils.logException(logger, e, Level.FATAL);
			DebugUtils.logEnviroment(logger, Level.FATAL);
			DebugUtils.logProperties(logger, Level.FATAL);
		}
		return isValid;
	}

	/**
	 * Once a FlashPanel instance has been created, a client can query the current version of
	 * the Adobe Flash Player.
	 * @param flashPanel is the instance of FlashPanel that has loaded the current Flash Player.
	 * @return a String representation of the Flash Player's version number.
	 */
	public static String getCurrentFlashPlayerVersion(final FlashPanel flashPanel) {
		return flashPanel.getVariable("flash.system.Capabilities.version");
	}
}
