/**
 * 
 */
package com.successmetricsinc.chart;

import java.io.File;
import java.lang.reflect.Field;
import java.net.URL;

import javax.jws.WebService;
import javax.servlet.ServletException;

import org.apache.log4j.Logger;
import org.apache.log4j.helpers.Loader;

/**
 * @author agarrison
 *
 */
@WebService( serviceName="ChartService" )
public class ChartRendererWS {
	
	static Logger logger = Logger.getLogger(ChartRendererWS.class);

    public static final String SWF_RESOURCE_LOCATION = "ChartRenderer.swf";
    
    private static String chartRendererResource = null;
    
    public static Throwable exception = null;
    
	private static ThreadLocal<ChartRenderer> staticChartRenderer = new ThreadLocal<ChartRenderer>() {
		protected ChartRenderer initialValue() {
			return new ChartRenderer(getChartRendererResource());
		}
	};
	
	private static String getChartRendererResource() {
		if (chartRendererResource == null) {
			URL url = Loader.getResource(SWF_RESOURCE_LOCATION);
			chartRendererResource = url.getFile();
			logger.info("ChartServer: Loading swf file from " + chartRendererResource);
			
			String libPath = System.getProperty("java.library.path");
			File parentPath = new File(chartRendererResource);
			System.setProperty("java.library.path", libPath + ";" + parentPath.getParent());
			
			try {
				// reload the system paths, so that the above call takes effect
				Field fieldSysPath = ClassLoader.class.getDeclaredField("sys_paths");
				fieldSysPath.setAccessible(true);
				fieldSysPath.set(null, null);
			}
			catch (Exception e) {}
			
		}
		return chartRendererResource;
	}
	
	private static final int SLEEP_TIME = 60 * 1000; // 1 minute
	public String renderChart(Integer height, Integer width, String xml) throws Throwable {
		String ret = null;
		try {
			logger.debug("ChartServer: Starting to render chart: height " + height + " width " + width);
			if (exception != null) {
				Thread.sleep(SLEEP_TIME);
				throw new ServletException("Chart server is in bad state");
			}
			ret = staticChartRenderer.get().renderChart(height, width, xml);
			if (ret == null) {
				exception = new NullPointerException();
				throw exception;
			}
		}
		catch (Throwable t) {
			exception = t;
			logger.error(t, t);
			throw t;
			
		}
		return ret;
	}
}
