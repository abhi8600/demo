package com.successmetricsinc.chart;

import java.awt.BorderLayout;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.concurrent.Callable;

import javax.swing.JFrame;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.jpackages.jflashplayer.FlashPanel;
import com.jpackages.jflashplayer.FlashPanelListener;
import com.jpackages.jflashplayer.JFlashInvalidFlashException;
import com.jpackages.jflashplayer.JFlashLibraryLoadFailedException;
import com.successmetricsinc.util.DebugUtils;
import com.successmetricsinc.util.JFlashPlayerHelper;

/**
 * ChartRenderer's principal method is renderChart().  It takes a {@link RenderRequest} as input.
 * The {@link RenderRequest} contains the width, height and AnyChart XML required to render a
 * PNG representation of a chart.  The {@link RenderRequest} also contains the TCP socket so
 * that a response can be returned to the requester.
 * <p/>
 * <b>Note</b>: ChartRenderer is both {@link Runnable} and {@link Callable}.
 * <p/>
 * The <b>run()</b> method is the principal one used to loop on the queue to retrieve
 * {@link RenderRequest}s. It loops continously until the ChartRenderer thread is interrupted.
 * After rendering the chart, run() returns the rendered PNG file back to the requesting TCP client.
 * <p/>
 * The call() method is mainly used in unit testing.  It allows the unit tests to schedule a
 * thread and retrieve the {@link RenderResults} from the {@link java.util.concurrent.FutureTask}.
 */
public class ChartRenderer extends JFrame {
	/** 
	 * The number of milliseconds to pause before checking to see if the .swf is ready. 
	 * TODO: Should be externalized to config file. 
	 */
	private static final long SWF_SLEEP_INTERVAL = 100L;
	
	/** 
	 * The maximum number of milliseconds to pause before failing. (15 seconds)
	 * TODO: Should be externalized to config file. 
	 */
	private static final long MAX_SLEEP_INTERVAL = 15000L;

	private static final String PANEL_TITLE = "ChartRenderer instance for JFlashPlayer AnyChart chart generation";
	private static final String INVALID_VERSION
		                    = "Either Flash Player is invalid version or the JFlashPlayer DLLs can't be loaded.";
    private static final String SWF_LOAD_TIMEOUT = "Maximum time elapsed waiting for .swf file to be ready.";
    private static final String PNG_STR_MSG = " - String output from ChartRenderer.renderChart(): ";
    private static final long serialVersionUID = 1L;
	private static Logger logger = Logger.getLogger(ChartRenderer.class);

	private String flashFilePath;
	private FlashPanel flashPanel;
    /** Flag set by Adobe Flash Player when ChartRenderer.swf file has been completely initialized. */
	private boolean swfReady;
	private boolean isDrawn;

	/**
	 * Default constructor for ChartRenderer.
     * <p/>
     * The JFlashPlayer FlashPanel is activated and added to the JPanel (ChartRenderer) contentPane.
     * Note that the JPanel must be made .setVisible(true) in order to activate the Flash Player.
     * There is a .setVisible(false) immediately after that so that the developer/operator
     * doesn't have to see lots of Flash Player pop-ups.
     * <p/>
     * The JFlashPlayer registration code must be set (via a static call) before any other
     * JFlashPlayer methods are invoked.  Unfortunately, there is no feedback with that call
     * to tell whether it was successful.
     * <p/>
     * setSWFIsReady() is a callback method that the ChartRenderer.swf/FlashPlayer uses to
     * communicate back to this class that it is ready for business.  Since this could
     * potentially take a while on startup, there is a timed loop in the .renderChart() method
     * that will wait for up to MAX_SLEEP_INTERVAL (15 sec) before failing.
     * <p/>
     * <b>Note:</b>  If there is a need for more concurrent copies of ChartRenderer, then the
     * ChartRendererFactory can be expanded to provide a pool of ChartRenderer instances....
     * @param threadName is the display name for the thread instance that will be created.
     * @param queue is the queue from which ChartRenderer will fetch {@link RenderRequest}s.
	 * @param flashFilePath is the new, non-default location of the .swf file.
	 * null value assumes the default (Tomcat) location for the .swf file.
     * @param reportingInterval is the number of milliseconds between reporting events.
     * @param parentThread is the ultimate parent (e.g., ConsoleServer) that will mediate all thread termination.
	 */
	public ChartRenderer(final String flashFilePath) {
		super(PANEL_TITLE);

		// Check for valid filepath to ChartRenderer.swf
		validateSwfFileExists(flashFilePath);
		this.flashFilePath = flashFilePath;
		
		// Seed JFlashPlayer with registration key before any other access
		JFlashPlayerHelper.setFlashPanelRegistration();
		// Check to see if the correct version of Flash is installed (or missing JFlashPlayer DLLs)
		if (!JFlashPlayerHelper.checkForValidFlashPlayer()) {
			// In either case, this is a critical problem for the ChartService
			throw new IllegalStateException(INVALID_VERSION);
		}
		createNewFlashPlayer();
	}
	
	private void createNewFlashPlayer() {
		if (flashPanel != null) {
			this.getContentPane().remove(flashPanel);
			flashPanel.dispose();
		}
		// Construct a FlashPanel displaying the SWF flash animation file
		logger.debug("Creating instance of FlashPanel with path: " + flashFilePath);
        try {
            flashPanel = new FlashPanel(new File(flashFilePath));
        } catch (JFlashLibraryLoadFailedException e) {
            DebugUtils.logEnviroment(logger, Level.FATAL);
            DebugUtils.logProperties(logger, Level.FATAL);
            throw new IllegalStateException("Error creating instance of FlashPanel. "
                                          + "JFlash.dll and/or atl2k.dll files are missing or damaged.", e);
        } catch (JFlashInvalidFlashException e) {
            DebugUtils.logEnviroment(logger, Level.FATAL);
            DebugUtils.logProperties(logger, Level.FATAL);
            throw new IllegalStateException("Error creating instance of FlashPanel. Invalid Flash version.", e);
        } catch (FileNotFoundException e) {
            DebugUtils.logEnviroment(logger, Level.FATAL);
            DebugUtils.logProperties(logger, Level.FATAL);
            throw new IllegalStateException("Error creating instance of FlashPanel. ChartRenderer.swf file not found.", e);
        }
        // Specify the object for Flash ExternalInterface.call events to search for the called method on
        flashPanel.setFlashCallObject(this);

        flashPanel.setScale(FlashPanel.SCALE_SHOWALL);

        // add the FlashPanel to the frame
        this.getContentPane().add(flashPanel, BorderLayout.CENTER);

        // I believe that now our servers are so fast, that it becomes ready before the listener can be set up.
        // set it as ready now.
//        this.swfReady = true;
        
        // The .setVisible(true) activates the Flash Player
        this.setVisible(true);
        this.getContentPane().setSize(100, 100);
//        this.setVisible(false);
        
        try {
            // Check to make sure FlashPlayer is ready. Should only be an issue at startup.
    		int sleepTime = 0;
    		while (!swfReady) {
            // Sleep before checking again. Time out if over the maximum time limit.
                Thread.sleep(SWF_SLEEP_INTERVAL);
                sleepTime += SWF_SLEEP_INTERVAL;
                // ~15 seconds should be ample time to load the player
                if (sleepTime > MAX_SLEEP_INTERVAL) {
                    throw new RuntimeException(String.format(SWF_LOAD_TIMEOUT));
                }
            }
        }
        catch (Exception e) {}
	}

    /**
	/**
	 * Render the AnyChart chart using the XML input and the height and width provided.
	 * The output is a String representation of the PNG image.
	 * @param renderRequest is the object containing the AnyChart XML input, height and width of the
     * requested chart to be rendered.
	 * @return the RenderResults instance representing the requested chart.
     * @throws InterruptedException possible while waiting for swfReader. Pass to caller so they can exit.
	 */
	public final String renderChart(Integer height, Integer width, String xml)
            throws InterruptedException {

        // Flash is ready, so go ahead and render chart
        //logger.info("ChartXML being sent: " + renderRequest.getXml());
		isDrawn = false;
        Object o = flashPanel.callFlashFunction("setXMLString", new Object[] {width, height, xml});
        if (o != null && o instanceof String) {
        	logger.error("setXMLString: " + o.toString());
        	
        	// assume jflashplayer died for some reason - create a new one
        	createNewFlashPlayer();
        	flashPanel.callFlashFunction("setXMLString", new Object[] {width, height, xml});
        }
        flashPanel.setScale(FlashPanel.SCALE_SHOWALL);
        
        int count = 0;
        int nullCount = 0;
        String lastPngString = null;
        while (count < 30)
        {
        	try {
        		Thread.sleep(500);
        	} catch (InterruptedException e) {
        	}
        	
        	String pngString = (String) flashPanel.callFlashFunction("getImage", new Object[] { });
         	if (lastPngString != null)
        	{
        		if (lastPngString.equals(pngString))
        			break;
        	}
         	else {
         		nullCount++;
         		logger.warn("getImage returned null.  null count is " + nullCount);
         	}
        	lastPngString = pngString;
        	count++;
        	
        }

        if (count == 30)
        {
        	logger.error("FlashPanel getImage request did not stabilize after 30 seconds");
        }
        // It appears that FlashPanel returns null results in certain circumstances
        if (lastPngString == null || lastPngString.length() <= 0) {
            logger.error(" FlashPanel getImage request returned an invalid PNG String!");
            logger.error("--- width = " + width + "; height = " + height);
        } else {
            // Logging at trace level (Birst runs live at "DEBUG" level)
            logRenderedPngString(lastPngString);
        }
        
        // recreate flash player so we don't cache old data
//        createNewFlashPlayer();
        return lastPngString;
	}

	/**
	 * Required by JFlashPlayer interface.
	 */
	public final void setSWFIsReady() {
        synchronized (this) {
            this.swfReady = true;
        }
        logger.info("Flash Player \"movie\" - ChartRenderer.swf - is loaded and ready.");
    }
	
	public final void setDrawComplete() {
		synchronized (this) {
			this.isDrawn = true;
		}
	}

    /* --------------------------------------------------------------------------------------------
       -- Helper methods (private)                                                               --
       -------------------------------------------------------------------------------------------- */

	/**
	 * Checks that a valid CheckRenderer.swf file exists.
     * protected scope for unit testing.
	 * @param flashFilePath is the fully-qualified file path to the ChartRenderer.swf file.
	 */
	protected static void validateSwfFileExists(final String flashFilePath) {
		// Basic check
		if (flashFilePath == null || flashFilePath.equals("")) {
			throw new IllegalArgumentException("Null or empty 'flashFilePath' passed to ChartRenderer().");
		}
		
		// Now check to see that it exists
		if (!new File(flashFilePath).exists()) {
			throw new IllegalArgumentException("ChartRenderer.swf file was not found using path: " + flashFilePath);
		}
	}


    /**
     * Used only for testing!
     * @param flashPanel is the mock instance of FlashPanel.
     */
    protected void setFlashPanel(final FlashPanel flashPanel) {
        this.flashPanel = flashPanel;
    }

    /**
     * Outputs a short or a long version of the PNG string depending on the logging level.
     * @param pngString is the string representation of the rendered PNG.
     */
    private void logRenderedPngString(final String pngString) {
        // Logging at trace level (Birst runs live at "DEBUG" level)
        if (logger.isTraceEnabled()) {
            logger.trace(PNG_STR_MSG + pngString);
        }

    }

}
