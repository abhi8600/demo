/**
 * $Id: Health.java,v 1.2 2012-06-18 23:47:52 agarrison Exp $
 *
 * Copyright (C) 2011 Birst, Inc. All rights reserved.
 * BIRST PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.successmetricsinc;

import javax.servlet.*;
import javax.servlet.http.*;

import com.successmetricsinc.chart.ChartRendererWS;

import java.io.*;

/**
 * This servlet is used to check the basic health of the tomcat server 
 */

public class Health extends HttpServlet {

	private static final long serialVersionUID = 1L;

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException 
	{
		response.setContentType("text/plain");
		PrintWriter writer = response.getWriter();
		if (ChartRendererWS.exception != null) {
			writer.write("failed");
		}
		else {
			writer.write("success");
		}
		writer.flush();
		writer.close();
	}
}
