package com.birst.flex.modules
{
	import mx.events.FlexEvent;
	
	public interface ITabModule
	{
		function getLabel():String;
		function getLabelLocKey():String;
		function tabShow(events:FlexEvent):void;
		function tabHide(events:FlexEvent):void;
	}
}