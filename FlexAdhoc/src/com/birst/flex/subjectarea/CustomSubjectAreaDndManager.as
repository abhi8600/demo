package com.birst.flex.subjectarea
{
    import com.birst.flex.core.SoundUtil;
    import com.birst.flex.core.Tracer;
    import com.birst.flex.core.Util;
    import com.birst.flex.modules.CustomSubjectAreaModule;
    import com.birst.flex.subjectarea.events.SubjectAreaEvent;
    
    import flash.events.EventDispatcher;
    
    import mx.collections.XMLListCollection;
    import mx.controls.Alert;
    import mx.controls.Tree;
    import mx.controls.listClasses.ListBase;
    import mx.core.UIComponent;
    import mx.events.CloseEvent;
    import mx.events.DragEvent;
    import mx.managers.DragManager;
    
	public class CustomSubjectAreaDndManager extends EventDispatcher
	{
		private var csam:CustomSubjectAreaModule;
        private var defaultTree:Tree;
        private var customTree:Tree;

        private var _draggedNodes:Array;
        private var targetNode:XML;
        private var targetIndex:int;
        
        [Bindable]
        [Embed("/images/warning.png")]
        private var warningIcon:Class;
        [Bindable]
        [Embed("/images/question2.png")]
        private var questionIcon:Class;

		public function CustomSubjectAreaDndManager(csam:CustomSubjectAreaModule, defaultTree:Tree, customTree:Tree) {
			this.csam = csam;
			this.defaultTree = defaultTree;
			this.customTree = customTree;

            addEventListener(SubjectAreaEvent.NEW_SUBJECT_AREA, csam.handleNewSubjectAreaEvent);

		}

        /* ------------------------------------------------------------------------------------
         * ---                                                                              ---
         * --- Drag-and-drop methods                                                        ---
         * ---                                                                              --- 
         * ------------------------------------------------------------------------------------ */

        /**
         * This method saves off the dragged node because the handleDragOver()
         * method is modifying the drop feedback by setting the selectedIndex to
         * the tree item over which the mouse is currently hovering.  This trick
         * in the later handlers means that the selectedItem will be incorrect,
         * hence we save it here in the first drag event.
         */
        public function handleDefaultTreeDragStart(event:DragEvent):void {
            // Get the target control & dataprovider
            var targetTree:Tree = event.dragInitiator as Tree;
            var draggedNode:XML = targetTree.selectedItem as XML;
            // Left-hand tree is single-select only
            _draggedNodes = new Array();
            _draggedNodes.push(draggedNode);
            Tracer.debug("'handleDefaultTreeDragStart()', 'draggedNode': " + draggedNode.@id + ": " + draggedNode.l, this);
        }

        /**
         * This method is used by the 'customTree' to determine if a move within 
         * the tree is allowed.
         */
        public function handleCustomTreeDragStart(event:DragEvent):void {        	
        	// Cover case where user does mouse down and starts drag before selecting item
        	var selectedNode:XML = customTree.selectedItem as XML;
        	addDraggedNode(selectedNode);
        	for each (var draggedNode:XML in _draggedNodes) {
	            Tracer.debug("'handleCustomTreeDragStart()', 'draggedNode': " + draggedNode.@id + ": " + draggedNode.l, this);
            }
        }

        public function handleDragEnter(event:DragEvent):void {
            Tracer.debug("'handleDragEnter()' in 'customTree' Tree.", this);
            // This, with the one in handleDragOver(), suppresses the drop line
            event.preventDefault();
            // Accept drop
            DragManager.acceptDragDrop(UIComponent(event.currentTarget));
        }

        /**
         * Is for 'customTree' only.
         */
        public function handleDragOver(event:DragEvent):void {
            Tracer.debug("'handleDragOver()' in 'customTree' Tree.", this);
            // This, with the one in handleDragEnter(), suppresses the drop line
            event.preventDefault();

            // Dragging left-hand subject area to an empty customTree, trigger editing of subject area
            var customProvider:XMLListCollection = customTree.dataProvider as XMLListCollection;
            var sourceTree:Tree = event.dragInitiator as Tree;
            if (sourceTree.id == "defaultTree" && (customProvider == null || customProvider.length == 0)) {
            	// Dragging a default subject area?
            	if (SubjectAreaHelper.isDefaultRootId(_draggedNodes[0].@id)) {
                    SoundUtil.playDoneSound();
                    Util.yesNoConfirmation("Would you like to edit the Default subject area?", 
                            handleDefaultSubjectAreaEditConfirmation, csam);
	             } else {
	             	if(csam.hasImportedCSA() && isImportedSubjectAreaNode(_draggedNodes[0]))
	             	{
	             		Util.showBirstAlert("You cannot edit an imported custom subject area.", "Warning", warningIcon, csam);
	             		csam.stopDrag();
            			return;
	             	}
	             	// Dragging a custom subject area?
	                if (SubjectAreaHelper.isRootId(_draggedNodes[0].@id)) {
                        // Switch work area to edit a custom subject area after confirmation
                        SoundUtil.playDoneSound();
                        Util.yesNoConfirmation("Would you like to edit the '" + _draggedNodes[0].l + "' subject area?", 
                                handleSubjectAreaEditConfirmation, csam);
	                } else {
                        // Show the new subject area dialog if user is dragging a non-Subject area node
                        var addSubjectAreaEvent:SubjectAreaEvent = new SubjectAreaEvent(SubjectAreaEvent.NEW_SUBJECT_AREA);
                        dispatchEvent(addSubjectAreaEvent);
                    }
	            }
	            // In all three cases, exit dragOver handler
	            csam.stopDrag();
                return;
            }
            
            if(_draggedNodes != null && csam.hasImportedCSA() && isImportedSubjectAreaNode(_draggedNodes[0]))
         	{
         		Util.showBirstAlert("You cannot drag an imported custom subject area element.", "Warning", warningIcon, csam);
    			targetNode = null;
    			DragManager.showFeedback(DragManager.NONE);
    			csam.stopDrag();
                return;
         	}
            // Show drop point, Windows-style row highlighting
            var dropTarget:Tree = event.currentTarget as Tree;
            var row:int = dropTarget.calculateDropIndex(event);
            customTree.selectedIndex = row;
            targetNode = customTree.selectedItem as XML;
            Tracer.debug("'handleDragOver()' 'targetNode' - " + targetNode.@id + ": " + targetNode.l, this);
            
            // Check for restricted operations
            if (isRestrictedMoveOperation(event)) {
                DragManager.showFeedback(DragManager.NONE);
            } else {
	            if (event.ctrlKey) {
	                DragManager.showFeedback(DragManager.COPY);
	            } else {
	                if (event.shiftKey) {
    	                DragManager.showFeedback(DragManager.LINK);
    	            } else {
	                    DragManager.showFeedback(DragManager.MOVE);
	                }
	            }
            }
        }

		private function isImportedSubjectAreaNode(node:XML):Boolean
		{
			var xml:XML = node;
	   		while (xml != null && xml.@id != null && !SubjectAreaHelper.isRootId(xml.@id)){
				xml = xml.parent();	
	   		}
	   		
	   		return xml != null && xml.@isImported != null && xml.@isImported == true;
		}
		
        public function handleDragDrop(event:DragEvent):void {
            Tracer.debug("'handleDragDrop()'...", this);
            // Nothing to drop
            if (_draggedNodes == null || _draggedNodes.length == 0 || targetNode == null) {
                return;
            }
            // Prohibit copying a subject area into an already active work area
            var dragInitiator:Tree = event.dragInitiator as Tree;
            if (dragInitiator.id == "defaultTree" && SubjectAreaHelper.isRootId(_draggedNodes[0].@id) && customTree.dataProvider != null) {
                Util.showBirstAlert("You cannot drag an entire subject area into an already "
                       + "active work area. 'Cancel' first if you wish to edit a new subject area. "
                       + "Otherwise drag lower-level items.", "Warning", warningIcon, csam);

                return;
            }

            // If a MOVE request within the 'customTree'
            if (dragInitiator.id == "customTree" && !event.ctrlKey) {
            	// Check for each dragged node
            	for each (var draggedNode:XML in _draggedNodes) {
	                // Can't move a folder to a subfolder
	                if (draggedNode.@type == "f" && isTargetAChildOfDragged(draggedNode, targetNode)) {
	                	_draggedNodes = null;
	                    SoundUtil.playErrorSound();
	                    Util.showBirstAlert("You cannot drop a folder onto one of this subfolders.",
	                            "Warning", warningIcon, csam);
	                    return;
	                }
	                // Can't move a item onto itself
	                if (draggedNode.@id == targetNode.@id) {
	                	_draggedNodes = null;
	                    SoundUtil.playErrorSound();
	                    Util.showBirstAlert("You cannot move a item onto itself.",
	                            "Warning", warningIcon, csam);
	                    return;
	                }
                }
            }
            // Get 'customTree' dataProvider
            var customProvider:XMLListCollection = customTree.dataProvider as XMLListCollection;
            var customXml:XML = customProvider[0] as XML;

            // Save current tree node open/close config
            var openItems:Object = customTree.openItems;
            
            /* ---------------------------
             * Delete 'MOVE'd source nodes
             * --------------------------- */

            // Only care about 'MOVE' requests within 'customTree' 
            if (dragInitiator.id == "customTree" && !event.ctrlKey) {
                Tracer.debug("Deleting '_draggedNodes' for MOVE operation.", this);
                if (_draggedNodes.length > 1 && !isValidMultiSelectMove()) {
                    _draggedNodes = null;
                	SoundUtil.playErrorSound();
                	Util.showBirstAlert("Can't move elements and their subelements together", "Warning", warningIcon, csam);
                	return;
                }
                for each (var nodeToDelete:XML in _draggedNodes) {
                	Tracer.debug("'nodeToDelete': " + nodeToDelete.@id + "/" + nodeToDelete.l, this);
                	deleteDraggedNode(customXml, nodeToDelete);
                }
            }
            
            // Add 'draggedNode' to 'targetNode'
            for each (var nodeToAdd:XML in _draggedNodes) {
            	var nodeCopy:XML = nodeToAdd.copy();
            	addDraggedNodeToTree(customXml, nodeCopy);
            }

            csam.customTreeProvider = new XMLListCollection(new XMLList(customXml));
            customTree.validateNow();
            customTree.openItems = openItems;
        }

        public function handleDragExit(event:DragEvent):void {
            Tracer.debug("'handleDragExit()' in 'customTree' Tree.", this);
            var dropTarget:ListBase = ListBase(event.currentTarget);
            dropTarget.hideDropFeedback(event);
        }

        public function handleDragComplete(event:DragEvent):void {
            Tracer.debug("'handleDragComplete()' in 'customTree' Tree.", this);
            event.preventDefault();
            _draggedNodes = null;
        }
        
        /* ----------------------------------------------------------------------------------------
         * ---                                                                                  ---
         * --- Confirmation methods                                                             ---
         * ---                                                                                  ---
         * ---------------------------------------------------------------------------------------- */

        public function handleDefaultSubjectAreaEditConfirmation(event:CloseEvent):void {
            Tracer.debug("'handleDefaultSubjectAreaEditConfirmation()'...", this);
            csam.removeEventListener(CloseEvent.CLOSE, handleDefaultSubjectAreaEditConfirmation);
            // If they say "Yes", then attach the currently dragged subject area to the work area Tree
            if (event.detail == Alert.NO) {
                return;
            }
            // Set up for default subject area edit after confirmation
            csam.setCurrentSubjectAreaGroups(_draggedNodes[0].l);
            csam.showSubjectAreaPropertiesDialog(_draggedNodes[0]);
        }

        public function handleSubjectAreaEditConfirmation(event:CloseEvent):void {
            Tracer.debug("'handleSubjectAreaEditConfirmation()'...", this);
            csam.removeEventListener(CloseEvent.CLOSE, handleSubjectAreaEditConfirmation);
            // If they say "Yes", then attach the currently dragged subject area to the work area Tree
            if (event.detail == Alert.NO) {
                return;
            }
            // Set currently edited 'rootId'
            var subjectAreaNode:XML = _draggedNodes[0] as XML;
            var nodeId:String = subjectAreaNode.@id;
            var parts:Array = nodeId.split(".");
            if (parts != null && parts.length == 2) {
                csam.rootId = nodeId;
                csam.rootPrefix = new int(parts[0]);
            }
            // Set the groups for this custom subject area
            csam.setCurrentSubjectAreaGroups(subjectAreaNode);
            
            // Now connect the XML to the customTree dataProvider
            csam.customTreeProvider = new XMLListCollection(new XMLList(subjectAreaNode.copy()));
            customTree.validateNow();
            SubjectAreaHelper.openFirstLevelNodes(customTree);
            // Calculate next available 'id' for any added folders
            csam.idSequenceNumber = SubjectAreaHelper.computeNextAvailableId(customTree);
            // ...and enable the 'Save' and 'Cancel' buttons
            csam.cancelCustomButton.enabled = true;
            csam.saveCustomButton.enabled = true;
        }
        
        /* ----------------------------------------------------------------------------------------
         * ---                                                                                  ---
         * --- _draggedNode methods                                                             ---
         * ---                                                                                  ---
         * ---------------------------------------------------------------------------------------- */

        [Bindable]
        public function set draggedNodes(value:Array):void {
        	_draggedNodes = value;
        }
        public function get draggedNodes():Array {
        	return _draggedNodes;
        }
        
        private function addDraggedNode(selectedNode:XML):void {
        	if (_draggedNodes == null || _draggedNodes.length == 0) {
        		_draggedNodes = new Array();
        		_draggedNodes.push(selectedNode);
        	} else {
        		// Only add it if it's not already in the list
        		var notInList:Boolean = true;
        		for each (var draggedNode:XML in _draggedNodes) {
        			if (selectedNode.@id == draggedNode.@id) {
        				notInList = false;
        				break;
        			}
        		}
        		if (notInList) {
        			_draggedNodes.push(selectedNode);
        		}
        	}
        }

        /* ----------------------------------------------------------------------------------------
         * ---                                                                                  ---
         * --- Helper methods                                                                   ---
         * ---                                                                                  ---
         * ---------------------------------------------------------------------------------------- */

        private function deleteDraggedNode(customXml:XML, draggedNode:XML):void {
            // If <c> parent of source <n> only had one child, delete at <c> otherwise just delete the one <n> 
            var nodeToBeDeleted:XML = customXml..n.(@id == draggedNode.@id)[0].copy();
            var parentOfNodeToBeDeleted:XML = customXml..n.(@id == draggedNode.@id)[0].parent().parent();
            // No <n>'s left after this one is deleted so just delete the <c> above it
            Tracer.debug("'parentOfNodeToBeDeleted' has " + parentOfNodeToBeDeleted.c.children().length() + " <c> children.", this);
            if (parentOfNodeToBeDeleted.c.children().length() == 1) {
                Tracer.debug("Deleting and reassigning <c> element of : " + parentOfNodeToBeDeleted.@id, this);
                if (SubjectAreaHelper.isRootId(parentOfNodeToBeDeleted.@id)) {
                    // Delete <c> using ROOT notation
                    delete customXml.(@id == parentOfNodeToBeDeleted.@id).c;
                    // (delete won't work on XMLList. Force to XML with [0]. (only 1 <c> ever))
                } else {
                    // Delete <c> using dependent notation
                    var childrenToBeDeleted:XMLList = customXml..n.(@id == parentOfNodeToBeDeleted.@id).c;
                    var childToBeDeleted:XML = childrenToBeDeleted[0] as XML;
                    Tracer.debug("<c> element to be deleted: " + childToBeDeleted.n.@id, this);
                    delete customXml..n.(@id == parentOfNodeToBeDeleted.@id).c[0];
                }
            } else {
                Tracer.debug("Deleting single element: " + draggedNode.@id, this);
                // Otherwise just delete the one <n> sibling, leave the rest
                if (SubjectAreaHelper.isRootId(parentOfNodeToBeDeleted.@id)) {
                    // Delete <n> using ROOT notation. [0] because there's only one
                    // var test:XMLList = customXml.c.n.(@id == draggedNode.@id);
                    delete customXml.c.n.(@id == draggedNode.@id)[0];
                } else {
                    // Delete <n> using dependent notation
                    delete customXml..n.(@id == draggedNode.@id)[0];
                }
            }
        }

        /**
         * Adding to a folder adds each 'nodeToAdd' to the end of the folder's contents.
         * "Adding" to an element (attribute or measure) adds each 'nodeToAdd' before
         * the 'targetNode'.
         */
        private function addDraggedNodeToTree(customXml:XML, nodeToAdd:XML):void {
        	if (targetNode == null) {
        		Tracer.debug("ERROR: 'addDraggedNodeToTree()' does not have a valid 'targetNode'!", this);
        		return;
        	}
        	// Make sure the 'id' is unique by grabbing the next available
        	nodeToAdd.@id = csam.getNextNodeId();
        	if (targetNode.@type == "f") {
        		addDraggedFolderToTree(customXml, nodeToAdd);
        	} else {
        		addDraggedElementToTree(customXml, nodeToAdd);
        	}
        }

        /**
         * Two complications: #1 is the fact that e4x notation is different for the root
         * vs. other dependent nodes; #2 there may not be a <c> element already, in which
         * case we have to build it and its dependent node and then assign it to the 
         * parent node.
         */
        private function addDraggedFolderToTree(customXml:XML, nodeToAdd:XML):void {
            Tracer.debug("'addDraggedFolderToTree()'; 'nodeToAdd': " + nodeToAdd.@id + ": " + nodeToAdd.l, this);
            // Does target already have <c> children?
            var currentChildren:XMLList = targetNode.c;
            // There's only one <c> child, if any
            var childNode:XML = currentChildren[0] as XML;
            // If there's no children already, you're going to add 'draggedNode' directly
            if (childNode == null) {
                // Remember, have to address differently if it's the root vs a dependent node
                if (SubjectAreaHelper.isRootId(targetNode.@id)) {
                    // Add dragged node to root
                    customXml.(@id == targetNode.@id).c.n = nodeToAdd;
                } else {
                    // Add dragged node to any dependent node
                    customXml..n.(@id == targetNode.@id).c.n = nodeToAdd;
                }
            } else {
                // There are already children.  First, add 'draggedNode' as sibling
                var appendedChildNode:XML = childNode.insertChildAfter(childNode.n[childNode.n.length() - 1], nodeToAdd);
                if (appendedChildNode == null) {
                    Tracer.debug("Couldn't insert 'nodeToAdd' after child nodes in: '" + childNode.@id + ":" + childNode.l + "'", this);
                    return; 
                }
                // Remember, have to address differently if it's the root vs a dependent node
                if (SubjectAreaHelper.isRootId(targetNode.@id)) {
                    // Add dragged node to root
                    customXml.(@id == targetNode.@id).c = appendedChildNode;
                } else {
                    // Add dragged node to any dependent node
                    customXml..n.(@id == targetNode.@id).c = appendedChildNode;
                }
            }
        }

        /**
         * Since the target is an element (attribute or measure), there is already a
         * <c> children node in which to insert this 'nodeToAdd'.
         */
        private function addDraggedElementToTree(customXml:XML, nodeToAdd:XML):void {
        	Tracer.debug("'addDraggedElementToTree()'; 'nodeToAdd': " + nodeToAdd.@id + ": " + nodeToAdd.l, this);
            // Have to address differently if it's the root vs a dependent node
            if (SubjectAreaHelper.parentIsRoot(customXml, targetNode)) {
            	// Remember, since 'id' is unique we can pick off the first entry [0]
            	customXml.c[0].insertChildBefore(targetNode, nodeToAdd);
            } else {
                // Handle the case where the target is below the root
                var nodes:XMLList = customXml..n.(@id == targetNode.@id);
                var node:XML = nodes[0] as XML;
                var nodeParent:XML = customXml..n.(@id == targetNode.@id).parent().parent();
                if (nodeParent != null) {
                    customXml..n.(@id == nodeParent.@id)[0].c[0].insertChildBefore(targetNode, nodeToAdd);
                } else {
                	Tracer.error("ERROR: Can't locate 'targetNode': " + targetNode.@id + ": " + targetNode.l, this);
                }
            }
        }
        
        private function isValidMultiSelectMove():Boolean {
            var isValidMove:Boolean = true;
            // Check that no nodes are children of other nodes
            for each (var draggedNode:XML in _draggedNodes) {
                for each (var nodeToCheck:XML in _draggedNodes) {
                    var rootCheck:XMLList = nodeToCheck.c.n.(@id == draggedNode.@id);
                    var dependentCheck:XMLList = nodeToCheck..n.(@id == draggedNode.@id);
                    if (rootCheck != null && rootCheck.length() > 0) {
                        isValidMove = false;
                        break;
                    }
                    if (dependentCheck != null && dependentCheck.length() > 0) {
                        isValidMove = false;
                        break;
                    }
                }
                if (!isValidMove) {
                    break;
                }
            }
            return isValidMove;
        }

        private function isTargetAChildOfDragged(draggedNode:XML, targetNode:XML):Boolean {
            var children:XMLList = draggedNode..n.(@id == targetNode.@id);
            if (children != null && children.length() > 0) {
                return true;
            }
            return false;
        }

        private function isIncluded(nodeToCheck:XML, listToCheck:Array):Boolean {
            // Check if the selectedItem is already in the list of dragged nodes
            var isIncluded:Boolean = false;
            for each (var listItem:XML in listToCheck) {
                if (nodeToCheck.@id == listItem.@id) {
                    isIncluded = true;
                    break;
                }
            }
            return isIncluded;
        }
        
        private function isRestrictedMoveOperation(event:DragEvent):Boolean {
            if (_draggedNodes == null ||_draggedNodes.length == 0 || targetNode == null) {
                return false;
            }
            for each (var draggedNode:XML in _draggedNodes) {
                if (draggedNode.@id == targetNode.@id) {
                    return true;
                }
            }
            return false;
        }
	}
}