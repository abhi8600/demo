package com.birst.flex.subjectarea
{
	import com.birst.flex.webservice.SubjectAreaWebService;
	
	import mx.collections.ICollectionView;
	import mx.collections.XMLListCollection;
	import mx.controls.Tree;
	import mx.controls.treeClasses.ITreeDataDescriptor;

	public class SubjectAreaTreeDataDescriptor implements ITreeDataDescriptor
	{
        [Embed("/images/Birst_swirl.png")]
        private var hSubjectArea:Class;
        [Embed("/images/DimCube.png")]
        private var hDimension:Class;
        [Embed("/images/Measure.png")]
        private var hMeasure:Class;
        [Embed("/images/folder.png")]
        private var hFolder:Class;
        [Embed("/images/hierarchy_icon.png")]
        private var hHierarchy:Class;
        [Embed("/images/member_icon.png")]
        private var hMember:Class;
        [Embed("/images/hierarchy.gif")]
        private var hODimension:Class;

		private var subjectAreaWebService:SubjectAreaWebService; 
		private var rootNode:SubjectAreaNode;
		private var parent:SubjectAreaNode;
		protected var _tree:Tree;
		private var refreshData:Boolean;
		private var open:Object;
		
		[Embed("/images/square.png")]
		protected var hSquare:Class;
		
		public function SubjectAreaTreeDataDescriptor(tree:Tree)
		{
			_tree = tree;
			refreshData = false;
			_tree.labelFunction = treeNode_labelFunc;
			_tree.iconFunction = treeNode_iconFunc;
			_tree.dataTipFunction = treeNode_tooltipFunc;
		}
		
		private function getObjectFromList(list:XMLList, xml:XML):XML {
			for (var i:int = 0; i < list.length(); i++) {
				var node:XML = list[i];
				if (node.SubjectAreaNode.name == xml.SubjectAreaNode.name) {
					return node;
				}
				if (xml.SubjectAreaNode.name.toString().indexOf(node.SubjectAreaNode.name) == 0) {
					getObjectFromList(node.SubjectAreaNode.children, xml);
				}
			}
			return null;
		}
		
		private function getRootNode(): SubjectAreaNode {
			return rootNode;
		}
		
		public function getChildren(node:Object, model:Object=null):ICollectionView
		{
			var list:XMLList = (node as XML).child("c");
			if (list != null && list.length() > 0) {
				var x:XML = list[0];
				
				return new XMLListCollection(x.child("n"));
			}
			
			return null;
		}
		
		public function hasChildren(node:Object, model:Object=null):Boolean
		{
			return isBranch(node, model);
		}
		
		public function isBranch(node:Object, model:Object=null):Boolean
		{
			if (node.@t == "oh" || node.@t == "od") // || (node.@t == "a" && node.@om == "true"))
				return true;
				
			if (node.@om == "true" && node.@lf != "true")
				return true;
				
			var list:XMLList = (node as XML).child("c");
			return ! (list == null || list.length() == 0);
		}
		
		public function treeNode_labelFunc(item:Object):String 
		{
			if(item.attribute('l').length() == 0){
				var name:String = item.@v.toString();
				var attrLabel:Array = name.split('.');
				if(attrLabel.length == 2){
					return attrLabel[1];
				}
			}
			return item.@l;
		}
		
		public function treeNode_iconFunc(item:Object): Class
		{
            var currentNode:XML = item as XML;
            // Handle folders
            if (currentNode.hasOwnProperty("isRootSA")) {
            	return hSubjectArea;
            }
            // For folder of all subject area nodes
            if (currentNode.@t == "f" && currentNode.l == "Subject Areas") {
            	return hSubjectArea;
            }
	        if (currentNode.@t == "od") {
	        	return hODimension;
	        } else if (currentNode.@t == "oh") {
	        	return hHierarchy;
	        } else if (currentNode.@t == "om") {
	        	return hMember;
            } else if (currentNode.@t == "a") {
                return hDimension;
            } else if (currentNode.@t == "m") {
                return hMeasure;
            } else if (currentNode.@t == "s") {
	            return hMeasure;
	        } else if (isBranch(item) || currentNode.@t == "f") {
                return hFolder;
	        }
            return hSquare;
		}
		
		public function treeNode_tooltipFunc(item:Object):String{
			var node:XML = XML(item);
			var s:String = node.@d;
			if (!s)
				s = node.@l;
			if (node.@t == "om" && s) {
				s = s + "\n[" + node.@v + "]";
			}
			if (!s) {
				s = node.@v;
				var i:int = s.indexOf('.');
				if (i > 0)
					s = s.substr(i+1);
			}
			
			return s;
		}
		
		public function getData(node:Object, model:Object=null):Object
		{
			return node;
		}
		
		public function addChildAt(parent:Object, newChild:Object, index:int, model:Object=null):Boolean
		{
			return false;
		}
		
		public function removeChildAt(parent:Object, child:Object, index:int, model:Object=null):Boolean
		{
			return false;
		}
	}
}

