package com.birst.flex.subjectarea.events
{
	import flash.events.Event;

	public class ElementEvent extends Event
	{
		public static const UPDATE_ELEMENT:String = "updateElement";
		
		private var _displayName:String;
		private var _columnLabel:String;
		private var _description:String;
		private var _targetNode:XML;
		
		public function ElementEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
		
        [Bindable]
        public function get displayName():String {
            return _displayName;
        }
        public function set displayName(value:String):void {
            _displayName = value;
        }
        
        [Bindable]
        public function get columnLabel():String {
            return _columnLabel;
        }
        public function set columnLabel(value:String):void {
            _columnLabel = value;
        }
        
        [Bindable]
        public function get description():String {
            return _description;
        }
        public function set description(value:String):void {
            _description = value;
        }
        
        [Bindable]
        public function get targetNode():XML {
            return _targetNode;
        }
        public function set targetNode(value:XML):void {
            _targetNode = value;
        }
	}
}