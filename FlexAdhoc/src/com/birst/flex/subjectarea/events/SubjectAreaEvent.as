package com.birst.flex.subjectarea.events
{
	import flash.events.Event;
	
	import mx.collections.ArrayCollection;

	public class SubjectAreaEvent extends Event
	{
		public static const NEW_SUBJECT_AREA:String = "newSubjectArea";
		public static const UPDATED_SUBJECT_AREA:String = "updatedSubjectArea";
		public static const SAVE_DEFAULT_GROUPS:String = "saveDefaultGroups";
		
        private var _subjectAreaName:String;
        private var _subjectAreaOriginalName:String;
        private var _subjectAreaDescription:String;
        private var _subjectAreaHidden:Boolean;
        private var _subjectAreaGroups:ArrayCollection;

		public function SubjectAreaEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
		
        [Bindable]
        public function get subjectAreaName():String {
            return _subjectAreaName;
        }
        public function set subjectAreaName(value:String):void {
            _subjectAreaName = value;
        }
        
        [Bindable]
        public function get subjectAreaOriginalName():String {
            return _subjectAreaOriginalName;
        }
        public function set subjectAreaOriginalName(value:String):void {
            _subjectAreaOriginalName = value;
        }
        
        [Bindable]
        public function get subjectAreaDescription():String {
            return _subjectAreaDescription;
        }
        public function set subjectAreaDescription(value:String):void {
            _subjectAreaDescription = value;
        }
        
        [Bindable]
        public function get subjectAreaHidden():Boolean {
            return _subjectAreaHidden;
        }
        public function set subjectAreaHidden(value:Boolean):void {
            _subjectAreaHidden = value;
        }
        
        [Bindable]
        public function get subjectAreaGroups():ArrayCollection {
            return _subjectAreaGroups;
        }
        public function set subjectAreaGroups(value:ArrayCollection):void {
            _subjectAreaGroups = value;
        }
    }
}