package com.birst.flex.subjectarea.events
{
	import flash.events.Event;

	public class FolderEvent extends Event
	{
		public static const NEW_FOLDER:String = "newFolder";
		public static const UPDATED_FOLDER:String = "updatedFolder";
		
		private var _folderName:String;
		private var _folderDescription:String;
		private var _targetNode:XML;
		
		public function FolderEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
		
		[Bindable]
		public function get folderName():String {
			return _folderName;
		}
		public function set folderName(value:String):void {
			_folderName = value;
		}
        
        [Bindable]
        public function get folderDescription():String {
            return _folderDescription;
        }
        public function set folderDescription(value:String):void {
            _folderDescription = value;
        }
        
        [Bindable]
        public function get targetNode():XML {
            return _targetNode;
        }
        public function set targetNode(value:XML):void {
            _targetNode = value;
        }
	}
}