package com.birst.flex.subjectarea.events
{
	import flash.events.Event;

	public class DeleteSubjectAreaEvent extends Event
	{
		public static const DELETE_SUBJECT_AREA:String = "deleteSubjectArea";
		
		private var _subjectAreaName:String;
		
		public function DeleteSubjectAreaEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
		
		public function get subjectAreaName():String {
			return _subjectAreaName;
		}
		public function set subjectAreaName(value:String):void {
			_subjectAreaName = value;
		}
	}
}