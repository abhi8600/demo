package com.birst.flex.subjectarea.events
{
	import flash.events.Event;

	public class DeleteEvent extends Event
	{
		public static const DELETE_ITEMS:String = "deleteItems";
		
		private var _itemsToDelete:Array;
		
		public function DeleteEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
		
		public function get itemsToDelete():Array {
			return _itemsToDelete;
		}
		public function set itemsToDelete(value:Array):void {
			_itemsToDelete = value;
		}
	}
}