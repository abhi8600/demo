package com.birst.flex.subjectarea
{
	import com.birst.flex.core.AppEvent;
	import com.birst.flex.core.AppEventManager;
	import com.birst.flex.webservice.SubjectAreaWebService;
	import com.birst.flex.webservice.WebServiceResult;
	
	import mx.collections.ICollectionView;
	import mx.collections.XMLListCollection;
	import mx.controls.Alert;
	import mx.controls.Tree;
	import mx.controls.treeClasses.ITreeDataDescriptor;
	import mx.controls.treeClasses.TreeItemRenderer;
	import mx.rpc.events.ResultEvent;

	public class BaseCustomSubjectAreaTreeDataDescriptor implements ITreeDataDescriptor
	{
        [Embed("/images/Birst_swirl.png")]
        private var hSubjectArea:Class;
        [Embed("/images/DimCube.png")]
        private var hDimension:Class;
        [Embed("/images/Measure.png")]
        private var hMeasure:Class;
        [Embed("/images/folder.png")]
        private var hFolder:Class;

		private var subjectAreaWebService:SubjectAreaWebService; 
		private var rootNode:SubjectAreaNode;
		private var parent:SubjectAreaNode;
		protected var _tree:Tree;
		private var refreshData:Boolean;
		private var open:Object;
		
		[Embed("/images/square.png")]
		protected var hSquare:Class;
		
		public function BaseCustomSubjectAreaTreeDataDescriptor(tree:Tree)
		{
			_tree = tree;
			refreshData = false;
			_tree.labelFunction = treeNode_labelFunc;
			_tree.iconFunction = treeNode_iconFunc;
			_tree.dataTipFunction = treeNode_tooltipFunc;
		}
		
		private function getObjectFromList(list:XMLList, xml:XML):XML {
			for (var i:int = 0; i < list.length(); i++) {
				var node:XML = list[i];
				if (node.SubjectAreaNode.name == xml.SubjectAreaNode.name) {
					return node;
				}
				if (xml.SubjectAreaNode.name.toString().indexOf(node.SubjectAreaNode.name) == 0) {
					getObjectFromList(node.SubjectAreaNode.children, xml);
				}
			}
			return null;
		}
		
		private function getRootNode(): SubjectAreaNode {
			return rootNode;
		}
		
		public function getChildren(node:Object, model:Object=null):ICollectionView
		{
			var list:XMLList = (node as XML).child("c");
			if (list != null && list.length() > 0) {
				var x:XML = list[0];
				
				return new XMLListCollection(x.child("n"));
			}
			
			return null;
		}
		
		public function hasChildren(node:Object, model:Object=null):Boolean
		{
			return isBranch(node, model);
		}
		
		public function isBranch(node:Object, model:Object=null):Boolean
		{
			var list:XMLList = (node as XML).child("c");
			return ! (list == null || list.length() == 0);
		}
		
		public function treeNode_labelFunc(item:Object):String 
		{
			return item.l;
		}
		
		public function treeNode_iconFunc(item:Object): Class
		{
            var currentNode:XML = item as XML;
            // Handle folders
            if (currentNode.hasOwnProperty("isRootSA")) {
            	return hSubjectArea;
            }
            // For folder of all subject area nodes
            if (currentNode.@type == "f" && currentNode.l == "Subject Areas") {
            	return hSubjectArea;
            }
            if (isBranch(item) || currentNode.@type == "f") {
                return hFolder;
            } else {
                if (currentNode.@type == "a") {
                    return hDimension;
                } else {
                    if (currentNode.@type == "m") {
                        return hMeasure;
                    } 
                }
            } 
            return hSquare;
		}
		
		public function treeNode_tooltipFunc(item:Object):String{
            if (XML(item).vl != false) {
                return XML(item).d;
            } else { 
                return null;
            }
		}
		
		public function getData(node:Object, model:Object=null):Object
		{
			return node;
		}
		
		public function addChildAt(parent:Object, newChild:Object, index:int, model:Object=null):Boolean
		{
			return false;
		}
		
		public function removeChildAt(parent:Object, child:Object, index:int, model:Object=null):Boolean
		{
			return false;
		}
	}
}

