package com.birst.flex.subjectarea
{
	import mx.controls.treeClasses.TreeItemRenderer;
	import mx.controls.treeClasses.TreeListData;
	import mx.events.ToolTipEvent;
	import mx.managers.ToolTipManager;

	public class SubjectAreaNodeItemRenderer extends TreeItemRenderer
	{
        [Embed("/images/DimCube.png")]
		private var hDimension:Class;
        [Embed("/images/Measure.png")]
		private var hMeasure:Class;
		
		public function SubjectAreaNodeItemRenderer()
		{
			//TODO: implement function
			super();
			this.addEventListener(ToolTipEvent.TOOL_TIP_SHOWN, toolTipShownEventHandler);
		}
		
		override public function set data(value:Object):void {
			super.data = value;
			
			//Disabled coloring
			if (XML(value).@vl == false) {
				setStyle("color", 0x999999);
			}
			else {
				setStyle("color", 0);
			}
			
			//Display icon for top level tree nodes
			
			/*
			var tdata:TreeListData = super.listData as TreeListData;
			if (tdata != null && tdata.depth == 1){
				var name:String = value.v;
				var names:Array = name.split("|~%#");
				var index: int = 1;
				if ("Attributes" == names[0] || "Measures" == names[0] || "Time Series Measures" == names[0]) 
					index = 0;
				
				if ("Attributes" == names[index]) {
					tdata.icon = hDimension;
				}
				else {
					tdata.icon = hMeasure;
				}
			}
			*/
			
		}
		
		public function toolTipShownEventHandler(event:ToolTipEvent):void{
			event.toolTip.move(event.toolTip.x + 10, event.toolTip.y + 20);
		}
	}
}