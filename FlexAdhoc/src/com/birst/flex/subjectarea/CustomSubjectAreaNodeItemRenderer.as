package com.birst.flex.subjectarea
{
	import mx.controls.treeClasses.TreeItemRenderer;
	import mx.events.ToolTipEvent;

	public class CustomSubjectAreaNodeItemRenderer extends TreeItemRenderer {
		
		public function CustomSubjectAreaNodeItemRenderer() {
			super();
			this.addEventListener(ToolTipEvent.TOOL_TIP_SHOWN, toolTipShownEventHandler);
		}
		
		override public function set data(value:Object):void {
			super.data = value;
			var invalid:Boolean = XML(value).vl == false;
			//Disabled coloring
			/*
			if (XML(value).vl == false) {
				setStyle("color", 0x999999);
			} else {
				setStyle("color", 0);
			}
			*/
			
			var xml:XML = XML(value);
	   		while (xml != null && xml.@id != null && !SubjectAreaHelper.isRootId(xml.@id)){
				xml = xml.parent();	
	   		}
			var imported:Boolean = xml != null && xml.@isImported != null && xml.@isImported == true;
			if (imported) {
				setStyle("color", 0xAAB3B3);
				setStyle("fontStyle", "italic");					
			}
			else {
				setStyle("fontStyle", "normal");
				//Disabled coloring
				if (invalid) {
					setStyle("color", 0x999999);
				} else {
					setStyle("color", 0);
				}
			}
		}
		
		public function toolTipShownEventHandler(event:ToolTipEvent):void {
            // event.toolTip.move(event.currentTarget.x + 50, event.currentTarget.y + 40);
            event.toolTip.move(event.toolTip.x + 36, event.toolTip.y + 22);
		}
	}
}
