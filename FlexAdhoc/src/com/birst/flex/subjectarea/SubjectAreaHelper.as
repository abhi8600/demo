package com.birst.flex.subjectarea
{
    import mx.collections.XMLListCollection;
    import mx.controls.Tree;

	public class SubjectAreaHelper
	{
		public static const DEFAULT_ROOT_ID:String = "0.0";
		
		public function SubjectAreaHelper() {
		}
		
        public static function renumberCustomTree(tree:Tree):XMLListCollection {
            // Id of root (== 0) is never altered, start at 1
            var idSequenceNumber:int = 1;
            var provider:XMLListCollection = tree.dataProvider as XMLListCollection;
            
            // Get the root prefix of the current tree
            var rootNode:XML = provider[0];
            var rootId:String = rootNode.@id;
            var parts:Array = rootId.split(".");
            var rootPrefix:int = new int(parts[0]);
            
            // Get all the <n> nodes...
            var nodes:XMLList = provider.descendants("n");
            // ...and renumber
            for each (var nNode:XML in provider.descendants("n")) {
                nNode.@id = rootPrefix + "." + idSequenceNumber++;
            }
            return provider;
        }
        
        public static function computeNextAvailableId(tree:Tree):int {
            var maxId:int = 0;
            var provider:XMLListCollection = tree.dataProvider as XMLListCollection;
            // Get all the <n> nodes...
            for each (var nNode:XML in provider.descendants("n")) {
                // ...and keep maximum id
                var id:String = nNode.@id;
                var parts:Array = id.split(".");
                if (parts == null || parts.length != 2) {
                	continue;
                }
                var currentId:int = new int(parts[1]);
                if (currentId > maxId) {
                	maxId = currentId;
                }
            }
            return ++maxId;
        }
        
        public static function isRootId(nodeId:String):Boolean {
            var parts:Array = nodeId.split(".");
            if (parts[1] != null && parts[1] == "0") {
                return true;
            }
            return false;
        }
        
        public static function isDefaultRootId(nodeId:String):Boolean {
            if (nodeId == DEFAULT_ROOT_ID) {
                return true;
            }
            return false;
        }
        
        /**
         * Is the parent of 'targetNode' in 'customXml' the root folder of the subject area?
         */
        public static function parentIsRoot(customXml:XML, targetNode:XML):Boolean {
        	// Root notation only works at root of XML: <n id="0.0"><c><n (target node)></c></n>
        	var newTarget:XML = customXml.c.n.(@id == targetNode.@id)[0] as XML;
        	if (newTarget != null) {
        		return true;
        	} else {
        		return false;
        	}
        } 
        
        public static function openFirstLevelNodes(tree:Tree):void {
            // Open first level of child nodes
            var treeProvider:XMLListCollection = tree.dataProvider as XMLListCollection;
            var treeRootNode:XML = treeProvider[0] as XML;
            tree.expandItem(treeRootNode, true, true);
            var childNodes:XMLList = treeRootNode.c.n;
            for each (var childNode:XML in childNodes) {
                tree.expandItem(childNode, true, true);
            }
        }
        
        /**
         * For certain cases (see Bug 7715) the e4x notation does not retrieve a node's parent.
         * For example:
         * parentNode = (customRootNode..n.(@id == itemToDelete.@id)[0] as XML).parent().parent();
         * works most of the time but not always, even in cases where it "should" work.
         */
        public static function getParentBruteForce(providerRoot:XML, targetNode:XML):XML {
        	var parentNode:XML = getParentOfChildNode(providerRoot, targetNode);
        	return parentNode;
        }
        
        private static function getParentOfChildNode(rootNode:XML, targetNode:XML):XML {
        	// There's either one or no <c> elements under a 'rootNode'
            var cNodes:XMLList = rootNode.child("c");
            if (cNodes == null || cNodes.length() == 0) {
            	return null;
            }
            var childNode:XML = cNodes[0] as XML;
            // Get list of <n> child nodes for rootNode
            var nNodes:XMLList = childNode.child("n");
            if (nNodes == null || nNodes.length() == 0) {
            	return null;
            }
            
            // Phew!  You've got a list of nNodes
            
            var parentNode:XML = null;
            // Now check if any of them are your targetNode
            for each (var nNode:XML in nNodes) {
            	if (nNode.@id == targetNode.@id) {
            		// You found it. Return the parent.
            		parentNode = rootNode;
            		break;
            	} else {
            		parentNode = getParentOfChildNode(nNode, targetNode);
            		if (parentNode != null) {
            			break;
            		}
            	}
            }
            return parentNode;
        }
	}
}