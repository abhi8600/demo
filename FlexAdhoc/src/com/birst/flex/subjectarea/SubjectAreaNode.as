package com.birst.flex.subjectarea
{
	import mx.collections.ArrayCollection;

	public class SubjectAreaNode
	{
        public static const NODE_SEPARATOR:String = "|~%#";

		private var _label:String;
		private var _name:String;
		private var _isValid:Boolean;
		private var _isLeafNode:Boolean;
		private var _children: ArrayCollection;
		
		public function SubjectAreaNode() {
			_label = "Subject Area"
			_name = null;
			_isValid = true;
			_isLeafNode = false;
		}
		
		public function setData(data:XML) : void {
			_label = data.label;
			_name = data.name;
			_isValid = data.isValid;
			_isLeafNode = data.isLeafNode;
			
			var x:XMLList = data.children;
			
			if (x.length() > 0) {
				var childList:ArrayCollection = new ArrayCollection();
				for each(var child: XML in data.children[0].SubjectAreaNode) {
					var s:SubjectAreaNode = new SubjectAreaNode();
					s.setData(child);
					childList.addItem(s);
				}
				_children = childList;
			}
		}
		
		public function toXML():XML {
			var xml:XML = new XML("<SubjectAreaNode></SubjectAreaNode>");
			xml.appendChild("<label>" + this._label + "</label>");
			xml.appendChild("<name>" + this._name + "</name>");
			xml.appendChild("<isValid>" + this._isValid + "</isValid>");
			xml.appendChild("<isLeafNode>" + this._isLeafNode + "</isLeafNode>");
			
			if (this._children != null) {
				var c:XML = new XML("<children></children>");
				for each (var child:SubjectAreaNode in this.children) {
					c.appendChild(child.toXML());
				}
				xml.appendChild(c);
			}
			return xml;
		}
		
		public function get label():String {
			return _label;
		}
		public function set label(lbl:String):void {
			_label = lbl;
		}
		
		public function get name():String {
			return _name;
		}
		public function set name(nm:String):void {
			_name = nm;
		}
		
		public function get isValid():Boolean {
			return _isValid;
		}
		public function set isValid(valid:Boolean):void {
			_isValid = valid;
		}
		
		public function get isLeafNode():Boolean {
			return _isLeafNode;
		}
		public function set isLeafNode(leafNode:Boolean):void {
			_isLeafNode = leafNode;
		}
		
		public function get children(): ArrayCollection {
			return _children;
		}
		public function set children(childs:ArrayCollection):void {
			_children = childs;
		}
	}
}
