package com.birst.flex.subjectarea
{
	import mx.controls.Tree;
	
	public class CustomSubjectAreaTreeDataDescriptor extends BaseCustomSubjectAreaTreeDataDescriptor
	{
        [Embed("/images/Birst_swirl.png")]
        private var hSubjectArea:Class;
        [Embed("/images/DimCube.png")]
        private var hDimension:Class;
        [Embed("/images/Measure.png")]
        private var hMeasure:Class;
        [Embed("/images/folder.png")]
        private var hFolder:Class;

		public function CustomSubjectAreaTreeDataDescriptor(tree:Tree)
		{
			super(tree);
		}
		
        override public function treeNode_iconFunc(item:Object):Class {
        	var currentNode:XML = item as XML;
        	if (SubjectAreaHelper.isRootId(currentNode.@id)) {
        		// Subject Area
        		return hSubjectArea;
        	} else {
	        	// Handle folders
	        	if (currentNode.@type == "f") {
	                return hFolder;
	            } else {
	            	// Dimension/Attribute
	            	if (currentNode.@type == "a") {
		                return hDimension;
		            } else {
		            	// Measure
		                if (currentNode.@type == "m") {
		                    return hMeasure;
		                } 
		            }
	            } 
	        }
            return hSquare;
        }

		override public function treeNode_tooltipFunc(item:Object):String {
            if (XML(item).vl != false) {
       		    return XML(item).d;
            } else { 
			    return null;
            }
		}
	}
}

