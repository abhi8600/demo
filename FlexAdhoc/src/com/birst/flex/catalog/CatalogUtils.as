package com.birst.flex.catalog
{
    import com.birst.flex.core.FileUtils;
    import com.birst.flex.core.Tracer;
    import com.birst.flex.core.SoundUtil;
    import com.birst.flex.core.XmlUtils;
    
    import mx.collections.ArrayCollection;
    import mx.collections.XMLListCollection;
    import mx.controls.Alert;
    import mx.events.DragEvent;
    import mx.managers.DragManager;
	
	public class CatalogUtils
	{
        public static function showFeedback(event:DragEvent):void 
        {
            // Show appropriate move/copy drag proxy
            var isCopy:Boolean = event.ctrlKey;
            if (isCopy) 
            {
                DragManager.showFeedback(DragManager.COPY);
            }
            else 
            {
                DragManager.showFeedback(DragManager.MOVE);
            }
        }
        
        public static function isRestrictedMoveOperation(event:DragEvent,
                                                         draggedListings:Array):Boolean
        {
            // Only care if it's a MOVE request, not a copy.
            if (!event.ctrlKey) 
            {
            	for each (var draggedListing:IListing in draggedListings) {
	                if (FileUtils.isRestrictedFolderMoveOperation(draggedListing))
	                {
	                	SoundUtil.playErrorSound();
	                    Alert.show("Cannot move 'private' or 'Shared' folders..", "Invalid request", Alert.OK);
	                    return true;
	                }
                }
            }
            return false;
        } 
        
        public static function movingPageFileToDashboardFolder(targetListing:IListing, 
                                                               draggedListings:Array):Boolean
        {
        	var validMove:Boolean = true;
            // Check that .page files are being moved/copied to dashboard directories
            for (var i:int = 0; i < draggedListings.length; i++)
            {
            	var draggedListing:IListing = draggedListings[i] as IListing;
                if (draggedListing.filetype.toLowerCase() == "page" && !targetListing.isDashboard())
                {
                    validMove = false;
                    break;
                }
            }
            return validMove;
        }
        
        public static function isValidActionFor(action:String, listing:IListing):Boolean {
            if (listing == null) {
                SoundUtil.playErrorSound();
                Alert.show("Please select folder before requesting " + action, "Folder not selected", Alert.OK);
                return false;
            } else {
                if (listing.isFile()) {
                    SoundUtil.playErrorSound();
                    Alert.show("Please select a folder rather than a file before requesting " + action, 
                           "Folder not selected", Alert.OK);
                    return false;
                }
                // Cannot create folders directly under 'private'
                if (listing.path == "private") {
                    SoundUtil.playErrorSound();
                    Alert.show(action + " directly under 'private' is not allowed.", "Invalid Request", Alert.OK);
                    return false;
                }
            }
            return true;
        }

        public static function buildListing(node:XML, 
                                            treeProvider:XMLListCollection):IListing
        {
        	// First get the targeted node's parent
            var parentNode:XML = XmlUtils.getParent(node.label, node.name, treeProvider);
            var parentListing:IListing;
            if (parentNode != null) 
            {
                parentListing = new Listing(parentNode);
            }
        	// Then create the requested listing with its parent embedded
        	var listing:IListing = new Listing(node, parentListing);
        	Tracer.debug("'buildListing()' created 'listing': " + listing);
        	return listing;
        }
        
        public static function getFiles(selectedNode:XML, 
                                        treeProvider:XMLListCollection):ArrayCollection {
            var files:ArrayCollection = new ArrayCollection();
            if (selectedNode != null) {
                var child:XMLList = selectedNode.child("children");
                // Should only be one <children> node under a folder FileNode
                if (child.length() == 1) {
                    var children:XMLList = (child[0] as XML).child("FileNode");
                    var parent:IListing;
                    for (var i:int = 0; i < children.length(); i++) {
                        var aFileNode:XML = children[i] as XML;
                        // Don't re-retrieve unless necessary
                        if (parent == null) {
                            var parentNode:XML = XmlUtils.getParent(aFileNode.label, aFileNode.name, treeProvider);
                            var grandparentNode:XML = XmlUtils.getParent(parentNode.label, parentNode.name, treeProvider);
                            var grandparent:IListing = new Listing(grandparentNode);
                            parent = new Listing(parentNode, grandparent);
                        }
                        files.addItem(new Listing(aFileNode, parent));
                    }
                    parent = null;
                }
            }
            return files;
        }
        
        public static function extractPathsFromListings(listings:Array):Array {
        	var paths:Array = new Array();
        	for each (var listing:IListing in listings) {
        		paths.push(listing.path);
        	} 
        	return paths;
        }
   	}
}