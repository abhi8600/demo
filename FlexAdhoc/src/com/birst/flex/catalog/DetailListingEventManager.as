package com.birst.flex.catalog
{
    import com.birst.flex.core.FileUtils;
    import com.birst.flex.core.Tracer;
    import com.birst.flex.core.SoundUtil;
    import com.birst.flex.modules.CatalogManagementModule;
    import com.birst.flex.webservice.FileWebService;
    
    import flash.events.EventDispatcher;
    import flash.events.KeyboardEvent;
    import flash.events.MouseEvent;
    import flash.ui.Keyboard;
    
    import mx.controls.Alert;
    import mx.controls.DataGrid;
    import mx.controls.TextInput;
    import mx.controls.dataGridClasses.DataGridItemRenderer;
    import mx.events.DataGridEvent;
    import mx.events.DataGridEventReason;

	public class DetailListingEventManager extends EventDispatcher
	{
        private var catMan:CatalogManagementModule;
        private var detailListing:DataGrid;

	    private var savedListing:IListing;
	
		public function DetailListingEventManager(detailListing:DataGrid, catMan:CatalogManagementModule)
		{
			this.detailListing = detailListing;
			this.catMan = catMan;
			
            // Add listeners (keyboard & context menu --> BEGINNING)
            addEventListener(KeyboardEvent.KEY_UP, handleKeyboardInput);
			addEventListener(DataGridEvent.ITEM_EDIT_BEGINNING, handleItemEditBeginning);
			addEventListener(DataGridEvent.ITEM_EDIT_BEGIN, handleItemEditBegin);
			addEventListener(DataGridEvent.ITEM_EDIT_END, handleItemEditEnd);
		}

        /**
         * Specifically pulls the file path from the 'detailListing' DataGrid's IListing item
         * and shows the item's path in the header.  The file path is the fully-qualified path
         * so this function trims off the pre-root portion of the path.
         * 
         * This method also checks to see if the user has single-clicked this entry twice
         * in a row.  If so, it initiates the item edit sequence to rename the file/folder.
         */
        public function showPath(event:MouseEvent):void {
            // Show the currently selected tree item's path in the header area
            var clickedListing:IListing = event.currentTarget.selectedItem as IListing;
            if (clickedListing != null) {
                // Set path in header bar
	            var currentFolder:IListing = clickedListing.isFile() ? clickedListing.parent : clickedListing;
	            catMan.pathLabel.text = currentFolder.path;
                if (savedListing != null) {
                    // Was this listing already selected?  Then flip into edit (rename) mode
                    if (event.type != MouseEvent.DOUBLE_CLICK
                        && savedListing.path == clickedListing.path) {
                        var dataGridEvent:DataGridEvent = new DataGridEvent(DataGridEvent.ITEM_EDIT_BEGINNING, false, true);
		                dataGridEvent.columnIndex = 1;             // Always column 1
		                dataGridEvent.dataField = "filename";      // Always 'filename'
                        dataGridEvent.rowIndex = detailListing.selectedIndex;
                        dataGridEvent.itemRenderer = new DataGridItemRenderer();
                        dispatchEvent(dataGridEvent);
                    }
                }
                // Saved here so it can be used in the ITEM_EDIT_BEGINNING handler below
                // TODO: Investigate why we can't set savedListing in BEGINNING handler
                savedListing = clickedListing;
	            // And always set the 'selectedListing' 
	            if (clickedListing.isFile()) {
	                catMan.selectedListing = clickedListing.parent;
	            } else {
	                catMan.selectedListing = clickedListing;
	            }
            }
        }

        /**
         * Traps the 'F2' keystroke and initiates the editing of the file name.
         * This and the context menu's rename item trigger the DataGridEvent.ITEM_EDIT_BEGINNING.
         */
        public function handleKeyboardInput(event:KeyboardEvent):void {
            if (event.keyCode == Keyboard.F2) {
                // Start editing the data grid's filename column
                var dataGridEvent:DataGridEvent = new DataGridEvent(DataGridEvent.ITEM_EDIT_BEGINNING, false, true);
                dataGridEvent.columnIndex = 1;             // Always column 1
                dataGridEvent.dataField = "filename";      // Always 'filename'
                var dataGrid:DataGrid = event.currentTarget as DataGrid;
                dataGridEvent.rowIndex = dataGrid.selectedIndex;
                dataGridEvent.itemRenderer = new DataGridItemRenderer();
                dispatchEvent(dataGridEvent);
            }
        }
        
        /**
         * Sets up the on-row editing of the file name.
         * Prevents users from modifying the 'shared' and 'private' directories.
         */
        public function handleItemEditBeginning(event:DataGridEvent):void {
            Tracer.debug("DetailListingEventManager.handleItemEditBeginning()", this);
            if (event.hasOwnProperty("itemRenderer")) {
                var renderer:DataGridItemRenderer = event.itemRenderer as DataGridItemRenderer;
                if (renderer != null && renderer.hasOwnProperty("data")) {
                	// 'savedListing' should have already been set in 'showPath()' above
                	if (savedListing == null) {
                		// TODO: For some reason renderer.data is usually null at this point!
                        savedListing = renderer.data as IListing;
                    }
                    // If this is a restricted folder don't allow editing
                    if (savedListing == null) {
                    	SoundUtil.playErrorSound();
                        event.preventDefault();
                    	Alert.show("Please select a folder before requesting a rename.", "Information", Alert.OK);
                    } else {
                        if (savedListing.isDirectory() && FileUtils.isRestrictedFolderMoveOperation(savedListing)) {
                            SoundUtil.playErrorSound();
	                        event.preventDefault();
                            Alert.show("Cannot rename top-level folders.", "Restricted Folders", Alert.OK);
	                    } else {
	                    	// Okay to edit...
	                    	detailListing.editable = true;
	                        // This is what triggers the ITEM_EDIT_BEGIN event
	                    	detailListing.editedItemPosition = {columnIndex:event.columnIndex, rowIndex:event.rowIndex};
		                }
                    }
                }
            }
        }

        public function handleItemEditBegin(event:DataGridEvent):void {
            Tracer.debug("'DetailListingEventManager.handleItemEditBegin()'; 'savedListing.name': " + savedListing.name, this);
        }

        public function handleItemEditEnd(event:DataGridEvent):void {
            Tracer.debug("'DetailListingEventManager.handleItemEditEnd()'", this);
            if (savedListing == null) {
                Tracer.debug("'DetailListingEventManager.handleItemEditEnd()' encountered null 'savedListing'!", this);
                return;
            }
            if (event.reason == DataGridEventReason.CANCELLED) {
                detailListing.editable = false;
            	return;
            }
            Tracer.debug("'DetailListingEventManager.handleItemEditEnd()' with savedListing: " + savedListing.name, this);
            var targetDataGrid:DataGrid = event.currentTarget as DataGrid;
            var editorInstance:TextInput = targetDataGrid.itemEditorInstance as TextInput;
            var newFileName:String = editorInstance.text;

            // If they've changed focus, check to see if there's work to do
            if (event.reason == DataGridEventReason.OTHER
                || event.reason == DataGridEventReason.NEW_ROW
                || event.reason == DataGridEventReason.NEW_COLUMN) {
                if (newFileName == savedListing.basename) {
                    // No change, so exit
                    detailListing.editable = false;
                    return;
                }
            }
            // Validate input
            if (newFileName == null || newFileName.length == 0) {
                SoundUtil.playErrorSound();
                editorInstance.text = savedListing.basename;
                detailListing.editable = false;
                Alert.show("Please enter a new file name.", "Invalid input", Alert.OK);
                return;
            }

            // Was filename really changed?
            if (newFileName == savedListing.basename) {
                SoundUtil.playErrorSound();
                detailListing.editable = false;
                Alert.show("File name - " + newFileName + " - did not change.", "Invalid input", Alert.OK);
                return;
            }

            // Refreshed 'detailListing' folder should always be the target's parent
            catMan.selectedListing = savedListing.parent;
            Tracer.debug("Refresh folder for 'doRename()' set to: " + catMan.selectedListing, this);

            Tracer.debug("old file/dir path to rename: [" + savedListing.path + "]", this);
            Tracer.debug("new file/dir name:           [" + newFileName + "]", this);
            // Send rename request
            FileWebService.processRenameFileRequest(newFileName,
                                                    savedListing,
                                                    catMan.getRootDirs,
                                                    catMan.standaloneTesting,
                                                    catMan.wsPort,
                                                    catMan.sessionId,
                                                    catMan.hostName);
            savedListing = null;
            detailListing.editable = false;
        }
	}
}