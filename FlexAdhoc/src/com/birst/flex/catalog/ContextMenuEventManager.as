package com.birst.flex.catalog
{
    import com.birst.flex.catalog.dialogs.CreateFolderDialog;
    import com.birst.flex.catalog.dialogs.FileDeleteDialog;
    import com.birst.flex.catalog.dialogs.FileDownloadDialog;
    import com.birst.flex.catalog.dialogs.FileUploadDialog;
    import com.birst.flex.catalog.dialogs.NewFolderPermissionsDialog;
    import com.birst.flex.catalog.events.AddFolderEvent;
    import com.birst.flex.catalog.events.FileDeleteEvent;
    import com.birst.flex.catalog.events.PermissionsEvent;
    import com.birst.flex.core.FileUtils;
    import com.birst.flex.core.SoundUtil;
    import com.birst.flex.core.Tracer;
    import com.birst.flex.core.Util;
    import com.birst.flex.core.XmlUtils;
    import com.birst.flex.modules.CatalogManagementModule;
    import com.birst.flex.webservice.FileWebService;
    import com.birst.flex.webservice.WebServiceResult;
    
    import flash.events.ContextMenuEvent;
    import flash.events.EventDispatcher;
    import flash.events.IEventDispatcher;
    import flash.ui.ContextMenu;
    import flash.ui.ContextMenuItem;
    import flash.utils.ByteArray;
    
    import mx.collections.XMLListCollection;
    import mx.controls.Alert;
    import mx.controls.DataGrid;
    import mx.controls.Tree;
    import mx.controls.dataGridClasses.DataGridItemRenderer;
    import mx.controls.dataGridClasses.DataGridListData;
    import mx.controls.treeClasses.TreeItemRenderer;
    import mx.controls.treeClasses.TreeListData;
    import mx.events.DataGridEvent;
    import mx.events.ListEvent;
    import mx.managers.PopUpManager;
    import mx.rpc.events.ResultEvent;
    import mx.utils.Base64Decoder;

	public class ContextMenuEventManager extends EventDispatcher
	{
        private var catMan:CatalogManagementModule;
        private var folderTree:Tree;
        private var detailListing:DataGrid;
        private var listing:IListing;
        [Bindable]
        private var itemContextMenu:ContextMenu;

		public function ContextMenuEventManager(folderTree:Tree, detailListing:DataGrid, target:IEventDispatcher=null)
		{
			super(target);
			this.folderTree = folderTree;
			this.detailListing = detailListing;
			catMan = target as CatalogManagementModule;
			// Create context menu instance
			itemContextMenu = new ContextMenu();
            itemContextMenu.addEventListener(ContextMenuEvent.MENU_SELECT, setUpContextMenuItems);
            // Set the context menu to the HBox around Tree & DataGrid
            catMan.fileArea.contextMenu = itemContextMenu;
		}
		
        public function setUpContextMenuItems(event:ContextMenuEvent):void 
        {
        	// Clear old items from menu
        	var itemCount:int = itemContextMenu.customItems.length;
        	for (var j:int = 0; j < itemCount; j++) {
        	   itemContextMenu.customItems.pop();
        	}
        	// Hide built-in menu items
            itemContextMenu.hideBuiltInItems();
            itemContextMenu.builtInItems.print = false;

            var listing:IListing = extractListingFromMenuEvent(event);
            // Possible to click over a border, not a tree or datagrid cell
            if (listing == null) 
            {
                return;
            }
            
            /*
             * Build the context menus
             */
            var addFolderMenuItem:ContextMenuItem = new ContextMenuItem("Add folder");
            itemContextMenu.customItems.push(addFolderMenuItem);
            addFolderMenuItem.addEventListener(ContextMenuEvent.MENU_ITEM_SELECT, addFolder);
            
            var renameMenuItem:ContextMenuItem = new ContextMenuItem("Rename item");
            itemContextMenu.customItems.push(renameMenuItem);
            renameMenuItem.addEventListener(ContextMenuEvent.MENU_ITEM_SELECT, renameItem);
            
            var removeMenuItem:ContextMenuItem = new ContextMenuItem("Delete item(s)");
            itemContextMenu.customItems.push(removeMenuItem);
            removeMenuItem.addEventListener(ContextMenuEvent.MENU_ITEM_SELECT, removeItem);
            
            var uploadMenuItem: ContextMenuItem = new ContextMenuItem("Upload");
            itemContextMenu.customItems.push(uploadMenuItem);
            uploadMenuItem.addEventListener(ContextMenuEvent.MENU_ITEM_SELECT, uploadFile);
            uploadMenuItem.separatorBefore = true;
            
            var downloadMenuItem: ContextMenuItem = new ContextMenuItem("Download");
            itemContextMenu.customItems.push(downloadMenuItem);
            downloadMenuItem.addEventListener(ContextMenuEvent.MENU_ITEM_SELECT, downloadFile);

            var securityMenuItem: ContextMenuItem = new ContextMenuItem("Security");
            itemContextMenu.customItems.push(securityMenuItem);
            securityMenuItem.addEventListener(ContextMenuEvent.MENU_ITEM_SELECT, securityAndSharing);
            securityMenuItem.separatorBefore = true;

            // Grey out all but 'delete' on the context menu if multiple items are selected.
            if (multipleItemsSelected(event)) {
            	addFolderMenuItem.enabled = false;
            	renameMenuItem.enabled = false;
            	uploadMenuItem.enabled = false;
            	downloadMenuItem.enabled = false;
            	securityMenuItem.enabled = false;
            } else {
	            // Cannot change folder permissions in the 'private' subfolders
	            if (FileUtils.isInPrivateFolder(listing.path)) {
	            	securityMenuItem.enabled = false;
	            } else {
	            	// Only folders have permissions, not files
	            	if (listing.isFile()) {
	            		securityMenuItem.enabled = false;
	            	} else {
			            securityMenuItem.enabled = true;
		            }
	            }
            }
            
            // Finally, check for Flash version. Upload/download requires version 10+
            if (!Util.isPlayer10Plus()) {
            	uploadMenuItem.enabled = false;
            	downloadMenuItem.enabled = false;
            }
        }
        
        /* ----------------------------------------------------------------------------------------
         * -- Rename file/directory                                                              --
         * ---------------------------------------------------------------------------------------- */
         
        public function renameItem(event:ContextMenuEvent):void 
        {
            Tracer.debug("Entered 'renameItem()' function. ContextMenuEvent: " + event, this);
            catMan.menuEvent = event;
            var listing:IListing = extractListingFromMenuEvent(event);
            // Possible to click over a border, not a tree or datagrid cell
            if (listing == null) 
            {
	            SoundUtil..playErrorSound();
	            Alert.show("Please select a file or folder before requesting a rename.", 
	                       "Info", Alert.OK);
            	return;
            }
            if (FileUtils.isRestrictedFolderMoveOperation(listing)) 
            {
            	SoundUtil..playErrorSound();
                Alert.show("Cannot rename top-level folders.", "Restricted Folders", Alert.OK);
            	return;
            }

            if (isDataGridEvent(event)) {
                // Start editing the data grid's filename column
                var dataGridEvent:DataGridEvent = new DataGridEvent(DataGridEvent.ITEM_EDIT_BEGINNING, false, true);
                dataGridEvent.columnIndex = 1;
                dataGridEvent.dataField = "filename";
                var gridRenderer:DataGridItemRenderer = event.mouseTarget as DataGridItemRenderer;
                var gridList:DataGridListData = gridRenderer.listData as DataGridListData;
                dataGridEvent.rowIndex = gridList.rowIndex;
                dataGridEvent.itemRenderer = gridRenderer;
                catMan.gridEvtMgr.dispatchEvent(dataGridEvent);
            } else {
	            var listEvent:ListEvent = new ListEvent(ListEvent.ITEM_EDIT_BEGINNING, false, true);
                var treeRenderer:TreeItemRenderer = event.mouseTarget.parent as TreeItemRenderer;
                var nodeXml:XML = treeRenderer.data as XML;
	            var treeProvider:XMLListCollection = folderTree.dataProvider as XMLListCollection;
	            catMan.selectedListing = CatalogUtils.buildListing(nodeXml, treeProvider);
                var treeList:TreeListData = treeRenderer.listData as TreeListData;
                listEvent.columnIndex = treeList.columnIndex;
                listEvent.rowIndex = treeList.rowIndex;
                listEvent.itemRenderer = treeRenderer;
                listEvent.itemRenderer.data = nodeXml;
                catMan.treeEvtMgr.dispatchEvent(listEvent);            
            }
        }

        /* ----------------------------------------------------------------------------------------
         * -- Delete file/directory                                                              --
         * ---------------------------------------------------------------------------------------- */
         
        public function removeItem(event:ContextMenuEvent):void 
        {
        	Tracer.debug("Entered 'removeItem()' function. ContextMenuEvent: " + event, this);
            catMan.menuEvent = event;
            var listings:Array;
            // Populate array of selected items 
            if (isDataGridEvent(event)) {
            	listings = detailListing.selectedItems;
            } else {
            	// 'folderTree' does not have multi-select on; only one item can be selected
            	var treeNode:XML = folderTree.selectedItem as XML;
            	if (treeNode != null) {
	            	var treeProvider:XMLListCollection = folderTree.dataProvider as XMLListCollection;
	            	listings = new Array();
	            	listings.push(CatalogUtils.buildListing(treeNode, treeProvider));
            	}
            }
            // Possible to click over a border, not a tree or datagrid cell
            if (listings == null || listings.length == 0) {
            	SoundUtil..playErrorSound();
                Alert.show("Before requesting a delete, please select a file or folder first.", 
                           "Information", Alert.OK);
                return;
            }
            for each (var listing:IListing in listings) {
	            if (listing.isDirectory() && FileUtils.isRestrictedFolderDeleteOperation(listing)) {
	            	SoundUtil..playErrorSound();
	                Alert.show("Cannot delete top-level folders.", "Restricted Folders", Alert.OK);
	                return;
	            }
            }
            // Refreshed 'detailListing' folder should always be the target's parent
            catMan.selectedListing = listings[0].parent;
            catMan.pathLabel.text = listings[0].parent.path;
            Tracer.debug("Refresh folder for 'doDelete()' set to: " + catMan.selectedListing, this);

            // Prompt to check that they should proceed
            var deleteDialog:FileDeleteDialog = new FileDeleteDialog();
            deleteDialog.addEventListener(FileDeleteEvent.DELETE, doDelete);
           	deleteDialog.listings = listings;
            PopUpManager.addPopUp(deleteDialog, catMan, true);
            PopUpManager.bringToFront(deleteDialog);
            PopUpManager.centerPopUp(deleteDialog);
        }

        public function doDelete(event:FileDeleteEvent):void 
        {
            Tracer.debug("Entered 'doDelete()' function. FileDeleteEvent: " + event.listings, this);

            // Send delete request
            FileWebService.processDeleteFileRequest(event.listings, 
                                                    catMan.getRootDirs, 
                                                    catMan.standaloneTesting, 
                                                    catMan.wsPort, 
                                                    catMan.sessionId, 
                                                    catMan.hostName);
        }
        
        /* ----------------------------------------------------------------------------------------
         * -- Add folder                                                                         --
         * ---------------------------------------------------------------------------------------- */
         
        public function addFolder(event:ContextMenuEvent):void 
        {
            Tracer.debug("Entered 'addFolder()' function. ContextMenuEvent: " + event, this);
            catMan.menuEvent = event;
            var listing:IListing = extractListingFromMenuEvent(event);
            // Possible to click over a border, not a tree or datagrid cell
            if (listing == null) 
            {
	            SoundUtil..playErrorSound();
	            Alert.show("First, please select an item where you would like to add the folder.", 
	                       "Info", Alert.OK);
                return;
            }
            // Folder to refresh 'detailListing' after re-retrieving filesystem
            if (event.mouseTarget is DataGridItemRenderer && listing.isFile()) 
            {
            	// Adding subfolder to 'detailListing' DataGrid: refresh parent folder in DataGrid
                catMan.selectedListing = listing.parent;
            } 
            else 
            {
            	// Adding subfolder to 'folderTree' Tree: refresh current folder in DataGrid
                catMan.selectedListing = listing;
            }
            Tracer.debug("Refresh folder for 'addFolder()' set to: " + catMan.selectedListing, this);
            
            // Cannot create folders directly under 'private'
            if (!CatalogUtils.isValidActionFor("'Add folder'", catMan.selectedListing))
            {
                return;
            }

            // Prompt for the new name
            var createFolderDialog:CreateFolderDialog = new CreateFolderDialog();
            createFolderDialog.listing = listing;
            createFolderDialog.addEventListener(AddFolderEvent.ADD_FOLDER, doCreateFolder);
            PopUpManager.addPopUp(createFolderDialog, catMan, true);
            PopUpManager.bringToFront(createFolderDialog);
            PopUpManager.centerPopUp(createFolderDialog);
        }

        public function doCreateFolder(event:AddFolderEvent):void 
        {
        	// If they press enter with focus on the "Create" button
        	if (event.folderName == "\r") {
        		return;
        	}
        	
            var newFolderName:String = event.folderName;

            // Save the currently selected folder for refresh later            
            var listing:IListing = event.listing;
            if (listing.isFile()) 
            {
            	if (listing.parent == null) 
            	{
            		Tracer.error("ERROR: file listing from 'detailListing' does not have a parent.", this);
            	}
            	listing = listing.parent;
            }

            Tracer.debug("current directory: [" + listing.path + "]", this);
            Tracer.debug("new folder name:   [" + newFolderName + "]", this);
            
            // Send create folder request
            FileWebService.processCreateFolderRequest(newFolderName, 
                                                      listing,
                                                      catMan.getRootDirs,
                                                      catMan.standaloneTesting,
                                                      catMan.wsPort,
                                                      catMan.sessionId,
                                                      catMan.hostName);
        }

        /* ----------------------------------------------------------------------------------------
         * -- Upload file                                                                        --
         * ---------------------------------------------------------------------------------------- */
         
        public function uploadFile(event:ContextMenuEvent):void 
        {
            Tracer.debug("Entered 'uploadFile()' method. ContextMenuEvent: " + event, this);
            catMan.menuEvent = event;
            var listing:IListing = extractListingFromMenuEvent(event);
            // Possible to click over a border, not a tree or datagrid cell
            if (listing == null) {
                return;
            }
            // Only upload to directories
            if (listing.isFile()) {
            	listing = listing.parent;
            }
            // Don't allow superusers to upload to 'private' directory
            if (listing.name == "private") {
            	SoundUtil..playErrorSound();
            	Alert.show("Uploading files to the 'private' folder is not allowed.", "Invalid Request", Alert.OK);
            	return;
            }
            // Save folder to be refreshed upon successful return
            catMan.selectedListing = listing;
            // Initialize and show the FileUploadDialog
            var fileUploadDialog:FileUploadDialog = new FileUploadDialog();
            fileUploadDialog.uploadFolder = listing.path;
            fileUploadDialog.catalogManagement = catMan;
            PopUpManager.addPopUp(fileUploadDialog, catMan, true);
            PopUpManager.bringToFront(fileUploadDialog);
            PopUpManager.centerPopUp(fileUploadDialog);
        }

        /* ----------------------------------------------------------------------------------------
         * -- Download file                                                                      --
         * ---------------------------------------------------------------------------------------- */
         
        public function downloadFile(event:ContextMenuEvent):void 
        {
            Tracer.debug("Entered 'downloadFile()' method. ContextMenuEvent: " + event, this);
            catMan.menuEvent = event;
            var listing:IListing = extractListingFromMenuEvent(event);
            // Possible to click over a border, not a tree or datagrid cell
            if (listing == null) 
            {
                return;
            }
            // Only upload to directories
            if (listing.isDirectory()) 
            {
            	SoundUtil..playErrorSound();
                Alert.show("Only files can be downloaded, not folders.", "File download", Alert.OK);
                return;
            }

            Tracer.debug("Calling 'processFileDownloadRequest()' method with relativeFilePath: " + listing.path, this);
            
            // Send downloadFile WS request
            FileWebService.processFileDownloadRequest(listing.path, 
                                                      writeFileDialog,
                                                      catMan.standaloneTesting,
                                                      catMan.wsPort,
                                                      catMan.sessionId,
                                                      catMan.hostName);
        }

        public function writeFileDialog(event:ResultEvent):void
        {
            Tracer.debug("'writeFileDialog()' with ResultEvent: " + event, this);
            var serviceResult:WebServiceResult = WebServiceResult.parse(event); 
            if (!serviceResult.isSuccess())
            {
            	SoundUtil..playErrorSound();
                Alert.show(serviceResult.getErrorMessage(), "File download error", Alert.OK);
                return;
            }
            
            // Get FileDownload result object
            var fileDownload:XMLList = serviceResult.getResult().FileDownload as XMLList;
            var filename:String = fileDownload.FileName;
            var encodedFileContents:String = fileDownload.EncodedFileContents;
            
            // Decode the file contents
            var decoder:Base64Decoder = new Base64Decoder();
            decoder.decode(encodedFileContents);
            var fileByteArray:ByteArray = decoder.toByteArray();
            
            // Bring up confirmation dialog (download proceeds from there since FileReference
            // requires user confirmation)            
            var fileDownloadDialog:FileDownloadDialog = new FileDownloadDialog();
            fileDownloadDialog.filename = filename;
            fileDownloadDialog.fileByteArray = fileByteArray;
            PopUpManager.addPopUp(fileDownloadDialog, catMan, true);
            PopUpManager.bringToFront(fileDownloadDialog);
            PopUpManager.centerPopUp(fileDownloadDialog);
            SoundUtil..playDoneSound();
        }
            
        /* ----------------------------------------------------------------------------------------
         * -- Security and folder sharing                                                        --
         * ---------------------------------------------------------------------------------------- */
         
        public function securityAndSharing(event:ContextMenuEvent):void 
        {
            Tracer.debug("securityAndSharing: " + event, this);
            catMan.menuEvent = event;
            var listing:IListing = extractListingFromMenuEvent(event);
            // Possible to click over a border, not a tree or datagrid cell
            if (listing == null) 
            {
                return;
            }
            // If file, then use its parent instead
            if (listing.isFile())
            {
            	listing = listing.parent;
            }
            // Folder to refresh 'detailListing' after re-retrieving filesystem
            catMan.selectedListing = listing;
            Tracer.debug("Refresh folder for 'securityAndSharing()' set to: " + catMan.selectedListing, this);

        	var folderPermissionsDialog:NewFolderPermissionsDialog = new NewFolderPermissionsDialog();
        	folderPermissionsDialog.folder = listing.path;
        	
            folderPermissionsDialog.addEventListener(PermissionsEvent.UPDATE, updatePermissions);
            // Pop it up
            PopUpManager.addPopUp(folderPermissionsDialog, catMan, true);
            PopUpManager.bringToFront(folderPermissionsDialog);
            PopUpManager.centerPopUp(folderPermissionsDialog);
        }
        

        public function updatePermissions(event:PermissionsEvent):void 
        {
        	Tracer.debug("'updatePermissions()'...", this);

        	var permissions:XMLList = event.permissions;
        	var permissionsString:String = permissions.toXMLString();
        	var folderPath:String = event.folderPath as String;

            // Save the folder's permissions
            FileWebService.saveFolderPermissions(folderPath,
                                                 permissionsString, 
                                                 checkSavePermissionsResult,
                                                 catMan.standaloneTesting, 
                                                 catMan.wsPort, 
                                                 catMan.sessionId, 
                                                 catMan.hostName);
        }
        
        public function checkSavePermissionsResult(event:ResultEvent):void {
        	var serviceResult:WebServiceResult = WebServiceResult.parse(event); 
            if (!serviceResult.isSuccess())
            {
                SoundUtil..playErrorSound();
                Alert.show(serviceResult.getErrorMessage(), "Request encountered a problem", Alert.OK);
            }
            else 
            {
                SoundUtil..playDoneSound();
            }
        }
        
        /* ----------------------------------------------------------------------------------------
         * -- Helper methods                                                                     --
         * ---------------------------------------------------------------------------------------- */
        
        private function extractListingFromMenuEvent(event:ContextMenuEvent):IListing 
        {
            var listing:IListing = null;
            // Source of event is 'folderTree' - folders ONLY
            if (event.mouseTarget.parent is TreeItemRenderer) 
            {
            	// Create the Listing instance from Tree XML
                var item:XML = (event.mouseTarget.parent as TreeItemRenderer).data as XML;
                // Get the parent's and grandparent's node from the Tree
                var treeXlc:XMLListCollection = catMan.folderTree.dataProvider as XMLListCollection;
                var parentXml:XML = XmlUtils.getParent(item.label, item.name, treeXlc);
                var grandparentXml:XML = XmlUtils.getParent(parentXml.label, parentXml.name, treeXlc);
                // Build Listing with recursive parent back to grandparent to handle situations like
                // folder deletion where the grandparent folder is the appropriate folder to refresh
                var grandParent:IListing = new Listing(grandparentXml);
                var parentListing:IListing = new Listing(parentXml, grandParent);
                // TODO: Should enhance and use CatalogUtils.buildListing() here
                listing = new Listing(item, parentListing);
            } 
            else 
            {
            	// Source of event is 'detailListing' - folders AND files
                if (event.mouseTarget is DataGridItemRenderer) 
                {
                    var target:DataGridItemRenderer = event.mouseTarget as DataGridItemRenderer;
                    listing = target.data as IListing;
                } 
                else 
                {
		            SoundUtil.playErrorSound();
		            Alert.show("Please select an item before requesting an action.", 
		                       "Info", Alert.OK);
                    return null;
                }
            }
            return listing;
        }
        
        private function isDataGridEvent(event:ContextMenuEvent):Boolean {
        	if (event.mouseTarget is DataGridItemRenderer) {
        		return true;
        	} else {
        		return false;
        	}
        }
        
        private function multipleItemsSelected(event:ContextMenuEvent):Boolean {
        	if (isDataGridEvent(event) && detailListing.selectedItems.length > 1) {
        		return true;
            } else {
        	    return false;
        	}
        }
	}
}