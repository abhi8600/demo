package com.birst.flex.catalog
{
	import com.birst.flex.catalog.events.RefreshDetailListingEvent;
	import com.birst.flex.modules.CatalogManagementModule;
	
	import flash.events.MouseEvent;
	
	import mx.controls.DataGrid;
	import mx.controls.dataGridClasses.DataGridItemRenderer;
	
	/** 
	 *  DataGrid that only allows editing if you double click
	 */
	public class DoubleClickDataGrid extends DataGrid
	{
	    private var _catalogManagement:CatalogManagementModule;
	    
	    public function DoubleClickDataGrid() {
	        super();
            mouseEnabled = true;
	        doubleClickEnabled = true;
	    }
	
	    override protected function mouseDoubleClickHandler(event:MouseEvent):void {
	    	// Ignore non-double-click events
	    	if (event.type != MouseEvent.DOUBLE_CLICK) {
	    		return;
	    	}
	    	// If folder clicked on, open that folder in the detailListing DataGrid
	    	var clickedListing:IListing = this.selectedItem as IListing;
	    	if (clickedListing != null && clickedListing.isDirectory()) {
		    	var refreshEvent:RefreshDetailListingEvent = new RefreshDetailListingEvent(RefreshDetailListingEvent.REFRESH, false, true);
		    	refreshEvent.listing = clickedListing;
		    	_catalogManagement.dispatchEvent(refreshEvent);
            }

	    	// And pass it on...
	        super.mouseDoubleClickHandler(event);
	    }
	    
	    public function get catalogManagement():CatalogManagementModule {
	    	return _catalogManagement;
	    }
	    public function set catalogManagement(value:CatalogManagementModule):void {
	    	_catalogManagement = value;
	    }
	}
}
