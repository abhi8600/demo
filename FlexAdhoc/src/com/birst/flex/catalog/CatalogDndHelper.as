package com.birst.flex.catalog
{
    import com.birst.flex.catalog.dialogs.ConfirmFileReplaceDialog;
    import com.birst.flex.catalog.events.MoveCopyEvent;
    import com.birst.flex.modules.CatalogManagementModule;
    import com.birst.flex.webservice.FileWebService;
    
    import flash.events.EventDispatcher;
    
    import mx.collections.XMLListCollection;
    import mx.controls.Tree;
    import mx.events.DragEvent;
    import mx.managers.PopUpManager;
	
	public class CatalogDndHelper extends EventDispatcher
	{
		private static const SLASH:String = "/";
		
		private var catMan:CatalogManagementModule;
		
		// Common move/copy drag-and-drop variables 
		private var _originalListings:Array;
		private var _filteredListings:Array;
		private var moveCopyEventCount:int;
		private var moveCopyPopUps:Array = new Array();
		
		public function CatalogDndHelper(catMan:CatalogManagementModule) {
			this.catMan = catMan;
		}

        /**
         * Client is dropping a set of 'sourceListings' files and/or folders onto a 'targetDirectory' 
         * destination.  This method determines if there are duplicate file/folder names and,
         * if so, prompts the user to get permission to continue.  Each file/folder is prompted
         * for individually.  
         * <p/>
         * The user can approve/disapprove individual files/folders or cancel the entire operation.  
         * Starting with the full set of 'sourceListings', the method dispatched from the confirmation
         * dialog - accumulateConfirmationResults - (see below) removes any disapproved file/folder 
         * from the 'sourceListings' array.  The array can be empty when it is returned to the caller.
         */
        public function createConfirmedList(dragEvent:DragEvent,
                                            folderTree:Tree, 
                                            sourceListings:Array, 
                                            targetDirectory:String):void {
            // Copy requests do not require confirmation. Will create "Copy of " versions.
            if (dragEvent.ctrlKey) {
            	var moveCopyEvent:MoveCopyEvent = new MoveCopyEvent(MoveCopyEvent.COMPLETE);
            	moveCopyEvent.dragEvent = dragEvent;
            	moveCopyEvent.filteredListings = sourceListings;
            	moveCopyEvent.targetDirectory = targetDirectory;
            	sendMoveCopyRequest(moveCopyEvent);
            	return;
            }          
              
            // Save off the original set of dragged listings
            _originalListings = sourceListings;
            // Set the count down counter
            moveCopyEventCount = sourceListings.length;
            // Set up the event handler for the completed 'canOverwriteFiles()' processing...
            addEventListener(MoveCopyEvent.COMPLETE, sendMoveCopyRequest);
            // Get folder tree data
        	var treeProvider:XMLListCollection = folderTree.dataProvider as XMLListCollection;

        	// Examine each dragged listing and see if the path already exists in the folder tree
        	for each (var sourceListing:IListing in sourceListings) {
                // Build new path
                var targetFilepath:String = targetDirectory + SLASH + sourceListing.name;
                var existingFileNode:XMLList;
                // Check each XML element in the XMLListCollection
                var fileFound:Boolean = false;
	        	for each (var rootFolder:XML in treeProvider) {
	        		// Examine this array element for existence of the targetFilePath
	                existingFileNode = rootFolder..FileNode.(name==targetFilepath);
	                if (existingFileNode.label == sourceListing.name) {
                    	fileFound = true;
	                    // If it does exist, prompt the user for permission
	                    var popUp:ConfirmFileReplaceDialog = new ConfirmFileReplaceDialog();
	                    popUp.listing = sourceListing;
	                    popUp.existingNode = existingFileNode;
	                    popUp.dragEvent = dragEvent;
	                    popUp.targetDirectory = targetDirectory;
	                    popUp.initializeFields();
	                    // Set the listener
	                    popUp.addEventListener(MoveCopyEvent.CONFIRM, accumulateConfirmationResults);
	                    popUp.addEventListener(MoveCopyEvent.CANCEL, cancelMoveCopyOperation);
	                    moveCopyPopUps.push(popUp);
	                    // Display the PopUp
	                    PopUpManager.addPopUp(popUp, catMan);
	                    PopUpManager.centerPopUp(popUp);
	                    PopUpManager.bringToFront(popUp);
	                    
	                    break;
                    }
	            }
	            if (!fileFound) {
	                // Just call 'accumulate' method with a null-listing event for countdown
	                var copyMoveEvent:MoveCopyEvent = new MoveCopyEvent(MoveCopyEvent.CONFIRM);
	                copyMoveEvent.dragEvent = dragEvent;
	                copyMoveEvent.targetDirectory = targetDirectory;
	                accumulateConfirmationResults(copyMoveEvent);
                }
            }
        }
        
        /**
         * This method is called after the "Yes" or "No" reply to each ConfirmFileReplaceDialog 
         * popup dialog box.  It counts down to determine when there is a decision on every file.
         * When all files have been confirmed or denied, this method "dispatches" (calls) the
         * sendMoveCopyRequest() which ... em ... sends the move/copy request.
         */ 
        public function accumulateConfirmationResults(event:MoveCopyEvent):void {
        	// Make sure filtered list is initialized
            if (_filteredListings == null) {
                _filteredListings = _originalListings;
            } 
        	// Remove entry from list if returned from event
        	if (event.listingToRemove != null) {
        		var tempArray:Array = new Array();
        		var listingToRemove:IListing = event.listingToRemove;
        		for each (var listing:IListing in _filteredListings) {
        			if (listing.path != listingToRemove.path) {
        				tempArray.push(listing);
        			}
        		}
        		_filteredListings = tempArray;
        	}
        	// Count down
        	moveCopyEventCount--;
        	
        	// Check accumulator and see if we are done now
        	if (moveCopyEventCount == 0) {
        		// We're done! If there's any listings to process...
        		if (_filteredListings.length > 0) {
	        		// Send the filtered results back to *DndManager
	        		var doneEvent:MoveCopyEvent = new MoveCopyEvent(MoveCopyEvent.COMPLETE);
	        		doneEvent.dragEvent = event.dragEvent;
	        		doneEvent.targetDirectory = event.targetDirectory;
	        		doneEvent.filteredListings = _filteredListings;
	        		dispatchEvent(doneEvent);
        		}
        	} 
        }
        
        /**
         * Sends the move/copy request via the FileWebService class.
         */
        public function sendMoveCopyRequest(event:MoveCopyEvent):void {
            // Get values from the event
            var dragEvent:DragEvent = event.dragEvent;
            var draggedListings:Array = event.filteredListings;
            var draggedPaths:Array = CatalogUtils.extractPathsFromListings(draggedListings);
            var targetDirectory:String = event.targetDirectory;
            
            // Build and process the webservice request
            if (catMan.standaloneTesting)
            {
                FileWebService.processMoveCopyFileRequest(dragEvent.ctrlKey, 
                                                          draggedPaths, 
                                                          targetDirectory,
                                                          catMan.getRootDirs,
                                                          catMan.standaloneTesting,
                                                          catMan.wsPort,
                                                          catMan.sessionId,
                                                          catMan.hostName);
            }
            else
            {
                FileWebService.processMoveCopyFileRequest(dragEvent.ctrlKey, 
                                                          draggedPaths, 
                                                          targetDirectory,
                                                          catMan.getRootDirs);
            }
        }

        public function cancelMoveCopyOperation(event:MoveCopyEvent):void {
        	for each (var popUp:ConfirmFileReplaceDialog in moveCopyPopUps) {
                // Application.application.removeChild(popUp);
                PopUpManager.removePopUp(popUp);
        	}
        	moveCopyPopUps = new Array();
        }

        public function get originalListings():Array {
            return _originalListings;
        }
        public function set originalListings(value:Array):void {
            _originalListings = value;
        }
        
        public function get filteredListings():Array {
            return _filteredListings;
        }
        public function set filteredListings(value:Array):void {
            _filteredListings = value;
        }
	}
}