<?xml version="1.0" encoding="utf-8"?>
<mx:Panel xmlns:mx="http://www.adobe.com/2006/mxml" 
	layout="absolute" height="128"
	verticalScrollPolicy="off" horizontalScrollPolicy="off"
	title="File upload"
	styleName="catManDialog"
	keyUp="handleKeyInput(event)"
	creationComplete="creationCompleteHandler(event)">
	
<mx:Style source="styles/CatalogManagement.css"/>
	   
<mx:Script>
    <![CDATA[
    	import com.birst.flex.core.Util;
    	import com.birst.flex.core.Tracer;
        import com.birst.flex.core.SoundUtil;
        import com.birst.flex.modules.CatalogManagementModule;
        import com.birst.flex.webservice.FileWebService;
        import com.birst.flex.webservice.WebServiceResult;
        import com.birst.flex.core.Localization;			
        
        import flash.events.TimerEvent;
        import flash.net.FileReference;
        import flash.utils.Timer;
        import mx.controls.Image;
        import mx.controls.Alert;
        import mx.core.SoundAsset;
        import mx.effects.Pause;
        import mx.managers.PopUpManager;
        import mx.rpc.events.ResultEvent;
        import mx.utils.Base64Encoder;
        import mx.utils.SHA256;
        import mx.core.Application;
        
        public static const ALLOWED_FILES:String = "*.jpg; *.png; *.gif; *.AdhocReport; *.dashlet; *.jrxml; *.page;";
        
        protected var file:FileReference;
        
        private var _uploadFolder:String;
        
        private var _catalogManagement:CatalogManagementModule;
        
        private var timer:Timer;
        private var _errorMessage:String = "";

        [Bindable] 
        private var imageSrc:String; 
        
        [Embed(source="/images/question.png")]
		[Bindable]
		private var helpImageClz:Class;
		private var _helpImage:Image = null;		
		private var helpUrl:String="file_upload_dialog_box";   

        // Limit the file types in the Browse popup box 
        protected var birstTypes:FileFilter;
        
        /**
         * This method primarily instantiates a FileReference object and sets its listeners.
         * <p/>
         * The regular FileReference events were blindfolded and thrown in the trunk since we 
         * are not using the standard upload servlet POST approach.  Instead, we are loading 
         * the file into memory, base64-encoding it and sending it as one of the String
         * parameters to a new 'uploadFile' Axis2 BaseFileWS webservice.  It is then 
         * decoded on the Java server side and written to disk.
         * TODO: Break the upload into ~20K chunks and show progress after each upload increment.
         * @param event Event object contains details of the event triggering this handler.
         */ 
        protected function creationCompleteHandler(event:Event):void 
        {
            file = new FileReference();
            
            // Event when the user selects a file from disk
            file.addEventListener(Event.SELECT, selectHandler);
            // Event when file is opened
            file.addEventListener(Event.OPEN, openHandler);
            // Event when upload is complete. Hijacked to do the uploadFile webservice
            file.addEventListener(Event.COMPLETE,handleLoadComplete);
            file.addEventListener(ProgressEvent.PROGRESS, progressHandler);
            file.addEventListener(IOErrorEvent.IO_ERROR, uploadIoErrorHandler);
            file.addEventListener(DataEvent.UPLOAD_COMPLETE_DATA, uploadCompleteHandler);
            
            // Instantiate a new FileFilter.
            birstTypes = new FileFilter("Birst files", ALLOWED_FILES);
                                        
            focusManager.setFocus(btnBrowse);
        }
        
        override protected function createChildren():void {
    		// Call the createChildren() method of the superclass.
    		super.createChildren();
    		
    		//Create the help url on titlebar
    		if (Application.application.helpEnable && helpUrl != null && _helpImage == null){
     			_helpImage = new Image();
    			_helpImage.source = helpImageClz;
    			_helpImage.width = 14;
    			_helpImage.height = 14;
    			_helpImage.addEventListener(MouseEvent.CLICK, helpHandler);
    			Localization.bindPropertyString(_helpImage, "toolTip", "TT_help");
    			this.titleBar.addChild(_helpImage);
    		}
    	}
    	
    	/**
		 * We have to calculate where to put our help icon
		 */
		override protected function updateDisplayList (unscaledWidth:Number, unscaledHeight:Number):void
        {
            super.updateDisplayList(unscaledWidth, unscaledHeight);
            if (_helpImage != null){
            	var y:int = 7;
            	var x:int = this.width - _helpImage.width - 5;
            	_helpImage.move(x, y);
            }
        }
    	
        public function handleKeyInput(event:KeyboardEvent):void
        {
        	if (event.keyCode == Keyboard.ESCAPE) {
        		closeDialog();
        	} else {
        		if (event.keyCode == Keyboard.ENTER) {
        			var focusObject:Object = focusManager.getFocus();
        			if (focusObject is Button) {
        				var focusButton:Button = focusObject as Button;
        				if (focusButton.id == "btnBrowse") {
                            browseHandler(null);
        				} else {
        					if (focusButton.id == "btnUpload") {
        						uploadHandler(null);
        					}
        				}
        			} else {
        				closeDialog();
        			}
        		}
        	}
        }

        /**
         * Event handler for the "Browse" button's click.
         * 
         * @param event Event object contains details of the event triggering this handler.
         */             
        private function browseHandler(event:Event):void 
        {
            pbrUploadProgress.visible = false;
            btnUpload.visible = false;
            file.browse([birstTypes]);
        }
        
        /**
         * Event handler for when the user selects a file from disk.
         */ 
        private function selectHandler(event:Event):void 
        {
            txtFilename.text = file.name;
            btnUpload.visible = true;
            focusManager.setFocus(btnUpload);
        }
        
        public function helpHandler(event:MouseEvent):void{
 			Util.gotoHelp(helpUrl, Application.application.helpSiteURL, Application.application.helpWindowName);
		} 
        
        /**
         * Event handler for when the file is opened.
         */ 
        private function openHandler(event:Event):void 
        {
            pbrUploadProgress.visible = true;
            pbrUploadProgress.label = "Uploading %3%% of file.";
        }

        /**
         * Event handler for the "Upload" button. Loads the selected FileReference
         * into memory asynchronously.
         */ 
        private function uploadHandler(event:MouseEvent):void 
        {
        	// Async read of file. When complete, 'handleLoadComplete()' will be invoked.
        	file.load();
        }
        
        /**
         * Called after the load() of the FileReference is complete.
         */
        public function handleLoadComplete(event:Event):void 
        {
        	// Read file contents and do a base64 encode
        	var filename:String = file.name;
        	var fileContents:ByteArray = file.data as ByteArray;
        	var encoder:Base64Encoder = new Base64Encoder();
        	encoder.encodeBytes(fileContents, 0, fileContents.length);
        	var encodedContents:String = encoder.toString();
        	// Get the SHA digest
        	var digest:String = SHA256.computeDigest(fileContents);
        	Tracer.debug("Encoded file is:" + encodedContents.substr(0,64), this);
        	Tracer.debug("Unencoded length: " + fileContents.length + "; Encoded length: " 
        	        + encodedContents.length + "; Digest: " + digest, this);
            // Send request to server
            FileWebService.processFileUploadRequest(uploadFolder,
                                                    filename,
                                                    digest,
                                                    encodedContents,
                                                    checkResults,
                                                    _catalogManagement.standaloneTesting,
                                                    _catalogManagement.wsPort,
                                                    _catalogManagement.sessionId,
                                                    _catalogManagement.hostName);
            focusManager.setFocus(btnBrowse);
        }
        
        public function checkResults(event:ResultEvent):void 
        {
            var rs:WebServiceResult = WebServiceResult.parse(event); 
            if (!rs.isSuccess())
            {
            	_errorMessage = rs.getErrorMessage();
                pbrUploadProgress.setProgress(0, 100);
                pbrUploadProgress.label = "Upload failed.";
                // Attention!
                SoundUtil.playErrorSound();
                // A short wait so the error message sinks in
                timer = new Timer(2000, 1);
                timer.addEventListener(TimerEvent.TIMER, handleTimer);
                timer.start();
            } 
            else 
            {
            	btnCancel.label = "Done";
           	    _catalogManagement.getRootDirs(event);
           	}
        }

        public function handleTimer(event:TimerEvent):void {
            Alert.show(_errorMessage, "Request unsuccessful", Alert.OK);
            _errorMessage = "";
            closeDialog();
        }
        
        /**
         * Event handler for when the upload starts.
         * 
         * @param event ProgressEvent object contains details of the event triggering this handler.
         */ 
        private function progressHandler(event:ProgressEvent):void 
        {
            pbrUploadProgress.setProgress(event.bytesLoaded, event.bytesTotal);                             
        }

        /**
         * Event handler for when the upload operation has completed.
         * Specifically manages response data.
         * 
         * @param event DataEvent object contains details of the event triggering this handler.
         */         
        private function uploadCompleteHandler():void 
        {
            pbrUploadProgress.label = "Upload complete.";
        }
        
        /**
         * Event handler for the file upload IO error.
         * 
         * @param event IOErrorEvent object contains details of the event triggering this handler.
         */  
        private function uploadIoErrorHandler(event:IOErrorEvent):void 
        {
        	SoundUtil.playErrorSound();
            Alert.show(event.toString(), "Request encountered a problem", Alert.OK);
            pbrUploadProgress.label = "Encountered error.";
        }
        
        private function closeDialog():void
        {
            // Close this popup dialog
            if (this != null && this.parent != null) 
            {
                this.parent.removeChild(this);
                PopUpManager.removePopUp(this);
            }
        }
        
        [Bindable]
        public function set uploadFolder(uploadFolder:String):void
        {
            _uploadFolder = uploadFolder;
        }
        
        public function get uploadFolder():String
        {
            return _uploadFolder;
        }

        public function set catalogManagement(value:CatalogManagementModule):void
        {
            _catalogManagement = value;
        }
        
        public function get catalogManagement():CatalogManagementModule
        {
            return _catalogManagement;
        }
    ]]>
</mx:Script>

<mx:HBox height="100%">
	<mx:VBox height="100%" id="vbMain">
	    <mx:Spacer/>
	    <mx:HBox width="100%">
            <mx:Spacer/>
		    <mx:Label text="Upload to: " fontStyle="italic"/>
		    <mx:Label width="100%" id="lblUploadFolder" text="{uploadFolder}" fontStyle="italic" fontWeight="bold"/>
            <mx:Spacer/>
        </mx:HBox>
        <mx:HBox width="100%">
            <mx:Spacer/>
    	    <mx:TextInput id="txtFilename" editable="false"  width="100%"/>
            <mx:Spacer/>
        </mx:HBox>
	    <mx:HBox id="controlBox" width="100%" height="100%" horizontalAlign="right" verticalAlign="middle">
	        <mx:Spacer/>
	        <mx:ProgressBar id="pbrUploadProgress" width="100%" height="24"  visible="false" 
	        	indeterminate="false" mode="manual" fontWeight="bold"
	            barColor="#FFA824" labelPlacement="center"/>
            <mx:Button id="btnUpload" label="Upload" paddingBottom="3" visible="false"
                paddingTop="3" click="uploadHandler(event)" />
	        <mx:Button id="btnBrowse" label="Browse" paddingBottom="3"
	            paddingTop="3" click="browseHandler(event)" />
            <mx:Button id="btnCancel" label="Cancel" paddingBottom="3"
                paddingTop="3" click="closeDialog()"  fontWeight="normal"/>
	        <mx:Spacer/>
	    </mx:HBox>
	</mx:VBox>
</mx:HBox>
</mx:Panel>
