package com.birst.flex.catalog
{
	import com.birst.flex.core.FileUtils;
	
	public class Listing implements IListing
	{
		private var _name:String;
		private var _path:String;
		private var _isDirectory:String;
		private var _isDashboard:Boolean;
		private var _size:String;
        private var _createdDate:String;
        private var _modifiedDate:String;
        private var _parent:IListing;

		public function Listing(xmlNode:XML, parent:IListing=null)
		{
            _name = xmlNode.label;
            _path = xmlNode.name;
            _isDirectory = xmlNode.isDirectory;
            // <isDashboard> is optional. if present, assume 'true'
            _isDashboard = xmlNode.hasOwnProperty("isDashboard") ? true : false; 
            _size = xmlNode.size;
            _createdDate = xmlNode.createdDate;
            _modifiedDate = xmlNode.modifiedDate;
            
            if (parent != null) {
                _parent = parent;
            }
		}

        [Bindable]
        public function set name(value:String):void 
        {
        	_name = value;
        }
        public function get name():String
        {
        	return _name;
        }
        
        public function set basename(value:String):void 
        {
        	if (_isDirectory == "true")
        	{
        		_name = value;
        	}
        	else
        	{
        		_name = value + "." + filetype;
        	}
        }
        
        public function get basename():String 
        {
        	if (_isDirectory == "true") 
        	{
        		return _name;
        	}
        	else
        	{
                return FileUtils.getFileBasename(_name);
        	}
        }
        
        public function set filetype(value:String):void
        {
        	if (_isDirectory == "false") 
        	{
        		_name = FileUtils.getFileBasename(_name) + "." + value;
        	}
        }
        
        public function get filetype():String
        {
        	if (_isDirectory == "true") 
        	{
        		return "";
        	}
        	else 
        	{
        	   return FileUtils.getFileExtension(_name);
        	}
        }
        
        public function isDirectory():Boolean
        {
        	if (_isDirectory == "true") 
        	{
        		return true;
        	}
        	else 
        	{
        		return false;
        	}
        }
        
        public function isDashboard():Boolean
        {
        	return _isDashboard;
        }
        
        public function isFile():Boolean
        {
            if (_isDirectory == "false") 
            {
                return true;
            }
            else 
            {
                return false;
            }
        }
        
        [Bindable]
        public function set path(value:String):void
        {
        	_path = value;
        }
        public function get path():String 
        {
        	return _path;
        }
        
        [Bindable]
        public function set size(value:String):void
        {
        	_size = value;
        }
        public function get size():String 
        {
        	if (_size == null) {
        		return "";
        	} else {
        	   return _size;
        	}
        }
        
        [Bindable]
        public function set createdDate(value:String):void
        {
            _createdDate = value;
        }
        public function get createdDate():String
        {
        	if (_createdDate == null) {
        		return "";
        	} else {
                return _createdDate;
            }
        }
        
        [Bindable]
        public function set modifiedDate(value:String):void
        {
            _modifiedDate = value;
        }
        public function get modifiedDate():String
        {
        	if (_modifiedDate == null) {
        		return "";
        	} else {
               return _modifiedDate;
            }
        }
        
        public function set parent(value:IListing):void 
        {
        	_parent = value;
        }
        
        public function get parent():IListing 
        {
        	return _parent;
        }
        
        public function toString():String
        {
            return "name: " + _name + "; path: " + _path + "; isDirectory: " + _isDirectory
                + "; isDashboard: " + isDashboard() + "; size: " + _size 
                + "; modifiedDate: " + _modifiedDate 
                + "; has parent: " + (_parent == null ? "null" : _parent.name);
        }
	}
}