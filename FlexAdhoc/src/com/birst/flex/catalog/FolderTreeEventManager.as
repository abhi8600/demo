package com.birst.flex.catalog
{
    import com.birst.flex.core.FileUtils;
    import com.birst.flex.core.Tracer;
    import com.birst.flex.core.SoundUtil;
    import com.birst.flex.modules.CatalogManagementModule;
    import com.birst.flex.webservice.FileWebService;
    
    import flash.events.KeyboardEvent;
    import flash.events.MouseEvent;
    import flash.ui.Keyboard;
    
    import mx.collections.ICollectionView;
    import mx.collections.XMLListCollection;
    import mx.controls.Alert;
    import mx.controls.TextInput;
    import mx.controls.Tree;
    import mx.controls.treeClasses.TreeItemRenderer;
    import mx.events.ListEvent;
    import mx.events.ListEventReason;
	
	public class FolderTreeEventManager
	{		
		private var catMan:CatalogManagementModule;
        private var folderTree:Tree;

        [Bindable]
        [Embed("/images/folder.png")]
        private var folderIcon:Class;

        private var savedListing:IListing;
    
		public function FolderTreeEventManager(folderTree:Tree, catMan:CatalogManagementModule) 
		{
			this.catMan = catMan;
            this.folderTree = folderTree;
            this.folderTree.labelFunction = treeNode_labelFunc;
            this.folderTree.iconFunction = treeNode_iconFunc;
            this.folderTree.dataTipFunction = treeNode_tooltipFunc;

            // Add listeners (keyboard & context menu --> BEGINNING)
            addEventListener(KeyboardEvent.KEY_UP, handleKeyboardInput);
            addEventListener(ListEvent.ITEM_EDIT_BEGINNING, handleItemEditBeginning);
            addEventListener(ListEvent.ITEM_EDIT_BEGIN, handleItemEditBegin);
            addEventListener(ListEvent.ITEM_EDIT_END, handleItemEditEnd);
		}

        /**
         * Changes the dataProvider of the 'detailListing' DataGrid to point
         * to the entry on the 'folderTree' Tree that was just clicked on.
         */
        public function displayChildNodes(event:MouseEvent):void {
            // Build dataProvider for 'detailListing' DataGrid
            var targetTree:Tree = event.currentTarget as Tree;
            var selectedTreeXml:XML = targetTree.selectedItem as XML;
            
            // Expand the current item when clicked on (only affects folders)
            targetTree.expandItem(selectedTreeXml, true);

            // Get the parent node of the selected node (used for refreshing detailListing)
            var treeProvider:XMLListCollection = targetTree.dataProvider as XMLListCollection;
            // Pull files and directories from the selected tree XML  
            var listings:ICollectionView = CatalogUtils.getFiles(selectedTreeXml, treeProvider);
            // Build new 'dataProvider' for 'detailListing' DataGrid
            catMan.detailListing.dataProvider = listings;
        }

        /**
         * Specifically pulls the file path from the 'detailListing' DataGrid's IListing item
         * and shows the item's path in the header.  The file path is the fully-qualified path
         * so this function trims off the pre-root portion of the path.
         * 
         * This method also checks to see if the user has single-clicked this entry twice
         * in a row.  If so, it initiates the item edit sequence to rename the file/folder.
         */
        public function showPath(event:MouseEvent):void {
            // Show the currently selected tree item's path in the header area
            var listingNode:XML = event.currentTarget.selectedItem as XML;
            if (listingNode != null) {
            	// Always set the path in the header
                catMan.pathLabel.text = listingNode.name;
                var targetTree:Tree = event.currentTarget as Tree;
                var treeProvider:XMLListCollection = targetTree.dataProvider as XMLListCollection;
            	var clickedListing:IListing = CatalogUtils.buildListing(listingNode, treeProvider);
            	if (catMan.selectedListing != null) {
                    // Was this listing already selected?  Then flip into edit (rename) mode
            		if (event.type != MouseEvent.DOUBLE_CLICK
            		    && catMan.selectedListing.path == clickedListing.path) {
            			var listEvent:ListEvent = new ListEvent(ListEvent.ITEM_EDIT_BEGINNING, false, true);
		                listEvent.columnIndex = 0;  // Tree is always column zero
		                listEvent.rowIndex = targetTree.selectedIndex;
		                listEvent.itemRenderer = new TreeItemRenderer();
		                listEvent.itemRenderer.data = targetTree.selectedItem as XML;
		                dispatchEvent(listEvent);
            		}
            	}
                // And always set the 'selectedListing' 
                catMan.selectedListing = clickedListing;
            }
        }

        /**
         * Traps the 'F2' keystroke and initiates the editing of the file name.
         * This and the context menu's rename item trigger the ListEvent.ITEM_EDIT_BEGINNING.
         */
        public function handleKeyboardInput(event:KeyboardEvent):void {
            if (event.keyCode == Keyboard.F2) {
                // Start editing the data grid's filename column
                var listEvent:ListEvent = new ListEvent(ListEvent.ITEM_EDIT_BEGINNING, false, true);
                var targetTree:Tree = event.currentTarget as Tree;
                listEvent.columnIndex = 0;  // Tree is always column zero
                listEvent.rowIndex = targetTree.selectedIndex;
                listEvent.itemRenderer = new TreeItemRenderer();
                listEvent.itemRenderer.data = targetTree.selectedItem as XML;
                dispatchEvent(listEvent);
            }
        }

        /**
         * Sets up the on-row editing of the file name.
         * Prevents users from modifying the 'shared' and 'private' directories.
         */
        public function handleItemEditBeginning(event:ListEvent):void {
            Tracer.debug("FolderTreeEventManager.handleItemEditBeginning()", this);
            // A node of tree must be selected in order for rename to work
            if (catMan.selectedListing == null) {
                event.preventDefault();
                SoundUtil.playErrorSound();
                Alert.show("Please select a folder before requesting rename.", "Information", Alert.OK);
                return;
            }
            if (event.hasOwnProperty("itemRenderer")) {
                var renderer:TreeItemRenderer = event.itemRenderer as TreeItemRenderer;
                if (renderer != null && renderer.hasOwnProperty("data")) {
                	var filenodeXml:XML = renderer.data as XML;
                	var treeProvider:XMLListCollection = folderTree.dataProvider as XMLListCollection;
                	savedListing = CatalogUtils.buildListing(filenodeXml, treeProvider);
                    // If this is a restricted folder don't allow editing
                    if (savedListing != null && savedListing.isDirectory() 
                        && FileUtils.isRestrictedFolderMoveOperation(savedListing)) {
                        SoundUtil.playErrorSound();
                        event.preventDefault();
                        Alert.show("Cannot rename top-level folders.", "Restricted Folders", Alert.OK);
                    } else {
                        folderTree.editable = true;
                        // This is what triggers the ITEM_EDIT_BEGIN
                        folderTree.editedItemPosition = {columnIndex:event.columnIndex, rowIndex:event.rowIndex};
                    } 
                }
            }
        }

        public function handleItemEditBegin(event:ListEvent):void {
            Tracer.debug("'FolderTreeEventManager.handleItemEditBegin()'; 'savedListing.name': " + savedListing.name, this);
        }

        public function handleItemEditEnd(event:ListEvent):void {
            Tracer.debug("'FolderTreeEventManager.handleItemEditEnd()' with event.reason: " + event.reason, this);
            if (savedListing == null) {
            	Tracer.debug("'FolderTreeEventManager.handleItemEditEnd()' encountered null 'savedListing'!", this);
                return;
            }
            if (event.reason == ListEventReason.CANCELLED) {
                cancelItemEdit(event);
                return;
            }
            Tracer.debug("'FolderTreeEventManager.handleItemEditEnd()' with savedListing: " + savedListing.name, this);
            var targetTree:Tree = event.currentTarget as Tree;
            var editorInstance:TextInput = targetTree.itemEditorInstance as TextInput;
            var newFileName:String = editorInstance.text;

            // If they've changed focus, check to see if there's work to do
            if (event.reason == ListEventReason.OTHER
                || event.reason == ListEventReason.NEW_ROW) {
                if (newFileName == savedListing.basename) {
                	// No change, so exit
                	cancelItemEdit(event);
                	return;
                }
            }
            // Validate input
            if (newFileName == null || newFileName.length == 0) {
                SoundUtil.playErrorSound();
                editorInstance.text = savedListing.basename;
                cancelItemEdit(event);
                Alert.show("Please enter a new file name.", "Invalid input", Alert.OK);
                return;
            }

            // Was filename really changed?
            if (newFileName == savedListing.basename) {
                SoundUtil.playErrorSound();
                cancelItemEdit(event);
                Alert.show("File name - " + newFileName + " - did not change.", "Invalid input", Alert.OK);
                return;
            }

            // Refreshed 'detailListing' folder should always be the target's parent
            catMan.selectedListing = savedListing.parent;
            Tracer.debug("Refresh folder for 'doRename()' set to: " + catMan.selectedListing, this);

            Tracer.debug("old file/dir path to rename: [" + savedListing.path + "]", this);
            Tracer.debug("new file/dir name:           [" + newFileName + "]", this);
            // Send rename request
            FileWebService.processRenameFileRequest(newFileName,
                                                    savedListing,
                                                    catMan.getRootDirs,
                                                    catMan.standaloneTesting,
                                                    catMan.wsPort,
                                                    catMan.sessionId,
                                                    catMan.hostName);
            savedListing = null;
            folderTree.editable = false;
        }

        /* ----------------------------------------------------------------------------------------
         * -- Label, Icon & Tooltip event functions.                                             --
         * ---------------------------------------------------------------------------------------- */
        
        public function treeNode_labelFunc(item:Object):String {
            return item.label;
        }
        
        public function treeNode_iconFunc(item:Object):Class {
            return folderIcon;
        }
        
        public function treeNode_tooltipFunc(item:Object):String {
            //Don't show tooltips if we're already open
            if (folderTree.isItemOpen(item)){
                return null;
            } else {
            	return "Click to open";
            }
        }
        
        private function getListing(event:MouseEvent):IListing 
        {
            var targetTree:Tree = event.currentTarget as Tree;
            var treeProvider:XMLListCollection = targetTree.dataProvider as XMLListCollection;
            var clickedXml:XML = targetTree.selectedItem as XML;
            Tracer.debug("'getListing()', 'clickedXml': " + clickedXml.toXMLString(), this);
            var clickedListing:IListing = CatalogUtils.buildListing(clickedXml, treeProvider);
            Tracer.debug("'getListing()', 'clickedListing': " + clickedListing, this);
            return clickedListing;
        }
        
        private function cancelItemEdit(event:ListEvent):void {
            event.stopImmediatePropagation();
            event.preventDefault();
            folderTree.destroyItemEditor();
            folderTree.editable = false;
        }
    }
}
