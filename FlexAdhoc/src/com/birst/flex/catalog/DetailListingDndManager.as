package com.birst.flex.catalog
{
    import com.birst.flex.core.FileUtils;
    import com.birst.flex.core.Tracer;
    import com.birst.flex.core.SoundUtil;
    import com.birst.flex.modules.CatalogManagementModule;
    
    import flash.events.TimerEvent;
    import flash.utils.Timer;
    
    import mx.collections.ArrayCollection;
    import mx.controls.Alert;
    import mx.controls.DataGrid;
    import mx.controls.ToolTip;
    import mx.controls.Tree;
    import mx.controls.listClasses.ListBase;
    import mx.core.DragSource;
    import mx.core.UIComponent;
    import mx.effects.Effect;
    import mx.events.DragEvent;
    import mx.managers.DragManager;
    import mx.managers.PopUpManager;

	public class DetailListingDndManager
	{
        [Bindable]
        [Embed("/images/warning.png")]
        private var warningIcon:Class;
        
		private var detailListing:DataGrid;
		private var catMan:CatalogManagementModule;
		
		private var smallPopUp:ToolTip;
		
		public function DetailListingDndManager(detailListing:DataGrid, catMan:CatalogManagementModule) 
		{
			this.detailListing = detailListing;
			this.catMan = catMan;
		}

        /**
         * This method saves off the dragged Listing because the handleDragOver()
         * method is modifying the drop feedback by setting the selectedIndex to
         * the tree item over which the mouse is currently hovering.  This trick
         * in the later handlers means that the selectedItem will be incorrect,
         * hence we save it here in the first drag event.
         */
        public function handleDragStart(event:DragEvent):void
        {
            // Get the target control & dataprovider
            var targetDataGrid:DataGrid = event.dragInitiator as DataGrid;
            catMan.draggedListings = targetDataGrid.selectedItems as Array;
            for each (var draggedListing:IListing in catMan.draggedListings) {
                Tracer.debug("Tree.selectedItem 'draggedListing': " + draggedListing, this);
            }
        }

        /**
         * This function handles the dragDrop event for the folderTree Tree.
         * Since the DnD is copying a user type and not one of the AS3 native objects, 
         * it 'preventDefault()'s so that it can do its own object copy. 
         */
        public function handleDragDrop(event:DragEvent):void 
        {
            Tracer.debug("'handleDragDrop()' for 'detailListing' DataGrid.", this);
            
            if (event.dragInitiator is Tree) {
            	handleDragDropFromTree(event);
            } else {
            	if (event.dragInitiator is DataGrid) {
            	   handleDragDropFromDataGrid(event);
            	} else {
            		Tracer.error("ERROR: drag events coming from: " + event.dragInitiator, this);
            	}
            }
        }
        
        protected function handleDragDropFromTree(event:DragEvent):void
        {
        	Tracer.debug("'handleDragDropFromTree()' in 'DetailListingDndManager'.", this);
        	if (catMan.draggedListings == null || catMan.draggedListings.length == 0) {
        		Tracer.error("ERROR: 'detailListing' DataGrid drop action has no 'draggedListings set!", this);
        		return;
        	}
        	var draggedListings:Array = catMan.draggedListings;
        	var draggedPaths:Array = CatalogUtils.extractPathsFromListings(draggedListings);
        	
        	// Determine 'targetDirectory'
        	var dgTarget:DataGrid = event.currentTarget as DataGrid;
        	var dropIndex:int = dgTarget.calculateDropIndex(event);
        	var dgProvider:ArrayCollection = dgTarget.dataProvider as ArrayCollection;
        	// ...dropped past the last entry?  Assume the last entry in the list.
        	if (dropIndex >= dgProvider.length) {
        		dropIndex = dgProvider.length - 1;
        	}
        	var targetListing:IListing = dgTarget.dataProvider[dropIndex] as IListing;
            var targetDirectory:String;
        	if (targetListing.isDirectory()) {
            	targetDirectory = targetListing.path;
            } else {
            	targetDirectory = FileUtils.getParentDirectory(targetListing.path);
            }
            
            for each (var draggedListing:IListing in draggedListings) {
	            // Check that we're not trying to move a 'private' or 'shared' folder
	            if (!event.ctrlKey && FileUtils.isRestrictedFolderMoveOperation(draggedListing)) {
	                SoundUtil.playErrorSound();
	                Alert.show("Cannot move a 'private' or 'shared' folder.", "Invalid request", Alert.OK);
	                return;
	            }
	
	            // Check that we're not trying to move to the same directory
	            if (!event.ctrlKey && draggedListing.parent.path == targetDirectory) {
	                SoundUtil..playErrorSound();
	                Alert.show("Attempting to move to the same folder. Not allowed.", "Warning", Alert.OK);
	                return;
	            }
	            
	            // And, cannot move a directory to one of its subdirectories
	            if (draggedListing.isDirectory() && targetDirectory.indexOf(draggedListing.path) != -1) {
	                SoundUtil.playErrorSound();
	                Alert.show("Cannot move/copy a folder to one of its subfolders. Not allowed.", "Warning", Alert.OK);
	                return;
	            }
            }
        	
            // Even superusers can't move/copy objects to the 'private/' folder
            if (targetListing.name == "private") {
                SoundUtil.playErrorSound();
                Alert.show("Cannot move or copy files/folders to the 'private'folder.", "Invalid request", Alert.OK);
                return;
            }
                
            // Set the directory to be refreshed in the DataGrid after the update
       		catMan.selectedListing = targetListing.parent;

            // Check to see if any of these files are overwriting existing files
            var catalogDndHelper:CatalogDndHelper = new CatalogDndHelper(catMan);
            // The rest of the processing is in CatalogDndHelper
            catalogDndHelper.createConfirmedList(event, catMan.folderTree, draggedListings, targetDirectory);
        }

        protected function handleDragDropFromDataGrid(event:DragEvent):void 
        {
        	// Check for the type of drop target
            var dropTarget:DataGrid = event.currentTarget as DataGrid;
            var row:int = dropTarget.calculateDropIndex(event);
            var rowCount:int = (detailListing.dataProvider as ArrayCollection).length;
            // If target out of range use last entry in table
            if (row >= rowCount) {
                row = rowCount - 1;
            }
            var targetListing:IListing = dropTarget.selectedItem as IListing;
        	// Remember, can't get selectedItem. It's been tampered with.
        	var draggedListings:Array = catMan.draggedListings;
            // Check for invalid requests
        	if (!isValidMoveCopyOperation(event, event.ctrlKey, draggedListings, targetListing)) {
        		return;
        	}

            var firstListing:IListing = draggedListings[0] as IListing;
            if (firstListing != null) {
                catMan.selectedListing = firstListing.parent;
            }

            // Figure out the target directory path
            var targetDirectory:String;
            if (targetListing.isDirectory()) {
            	targetDirectory = targetListing.path;
            } else {
            	if (targetListing.parent != null) {
            		targetDirectory = targetListing.parent.path;
            	} else {
            		Tracer.error("ERROR: DataGrid-to-DataGrid dropListing: " + targetListing.path 
            		      + " did not have a valid parent.", this);
            	}
            }

            // Check to see if any of these files are overwriting existing files
            var catalogDndHelper:CatalogDndHelper = new CatalogDndHelper(catMan);
            // The rest of the processing is in CatalogDndHelper
            catalogDndHelper.createConfirmedList(event, catMan.folderTree, draggedListings, targetDirectory);
        }
        
        public function handleDragEnter(event:DragEvent):void 
        {
            Tracer.debug("'handleDragEnter()' in 'detailListing' DataGrid.", this);
            DragManager.acceptDragDrop(UIComponent(event.currentTarget));
        }
        
        public function handleDragOver(event:DragEvent):void 
        {
            Tracer.debug("'handleDragOver()' in 'detailListing' DataGrid.", this);
            // Show drop point
            var dropTarget:DataGrid = event.currentTarget as DataGrid;
            var row:int = dropTarget.calculateDropIndex(event);
            // Force the selection of the current drop target
            detailListing.selectedIndex = row;
            if (row > (detailListing.dataProvider as ArrayCollection).length) {
                DragManager.showFeedback(DragManager.NONE);
                return;
            }
            // Check that .page files are being moved/copied to dashboard directories
            var targetListing:IListing = detailListing.selectedItem as IListing;
            if (!CatalogUtils.movingPageFileToDashboardFolder(targetListing, catMan.draggedListings))
            {
                DragManager.showFeedback(DragManager.NONE);
                return;
            }
            if (event.ctrlKey)
                DragManager.showFeedback(DragManager.COPY);
            else if (event.shiftKey)
                DragManager.showFeedback(DragManager.LINK);
            else {
                DragManager.showFeedback(DragManager.MOVE);
            }
        }
        
        public function handleDragExit(event:DragEvent):void 
        {
            Tracer.debug("'handleDragExit()' in 'detailListing' DataGrid.", this);
            var dropTarget:ListBase = ListBase(event.currentTarget);
            dropTarget.hideDropFeedback(event);
        }

        public function handleDragComplete(event:DragEvent):void 
        {
            Tracer.debug("'handleDragComplete()' in 'detailListing' DataGrid.", this);
            event.preventDefault();
        }
        
        public static function getDraggedEntry(event:DragEvent):IListing
        {
            // Determine 'sourcePath'
            var ds:DragSource = event.dragSource;
            var arr:Array = ds.dataForFormat("items") as Array;
            // at this point there should only be one folder at a time...
            if (arr.length != 1) {
                Tracer.error("ERROR: There should only be one tree item dragged at a time (for now).");
                return null;
            }
            return arr[0] as IListing;
        }
        
        /* ------------------------
         * 'private' helper methods
         * ------------------------ */

        /**
         * Two operations are not allowed:
         * 1) "move" from a file to a file
         * 2) "move" from a dir to a file
         */
        private function isValidMoveCopyOperation(event:DragEvent,
                                                  isCopy:Boolean,
                                                  sourceListings:Array, 
                                                  targetListing:IListing):Boolean
        {
        	// Check for valid dragged object
        	for each (var sourceListing:IListing in sourceListings) {
	            if (sourceListing == null) {
                    SoundUtil.playErrorSound();
                    Alert.show("No dragged object found.", "Invalid request", Alert.OK);
                    return false;
	            }
	            // Two MOVE-only combinations are prohibited
	            if (!event.ctrlKey) {
	                if (sourceListing.isFile() && targetListing.isFile()) {
	                    SoundUtil.playErrorSound();
	                    Alert.show("Cannot move a file to another file.", "Invalid request", Alert.OK);
	                    return false;
	                }
	                if (sourceListing.isDirectory() && targetListing.isFile()) {
	                    SoundUtil.playErrorSound();
	                    Alert.show("Cannot move a file to another file.", "Invalid request", Alert.OK);
	                    return false;
	                }
	            }
            }
            if (targetListing == null) {
                    SoundUtil.playErrorSound();
                    Alert.show("No valid target object found.", "Invalid request", Alert.OK);
                    return false;
            }
	        
            return true;
        }

        private function showErrorToolTip():void {
        	// Create and show tool tip instance
        	smallPopUp = new ToolTip();
            smallPopUp.text = "Cannot move/copy within detailed listing.";
            PopUpManager.addPopUp(smallPopUp, detailListing);
            PopUpManager.bringToFront(smallPopUp);
            PopUpManager.centerPopUp(smallPopUp);
            
            // Create and play effect
            var glowPopUpEffect:Effect = SoundUtil.setUpGlowEffect(smallPopUp);
            var unglowPopUpEffect:Effect = SoundUtil.setUpUnglowEffect(smallPopUp);
            glowPopUpEffect.play();
            SoundUtil.playErrorSound();
            unglowPopUpEffect.play();

            // Wait for an interval and hide the tooltip            
            var timer:Timer = new Timer(1500, 1);
            timer.addEventListener(TimerEvent.TIMER_COMPLETE, removeSmallPopUp);
            timer.start();
        }
        
        private function removeSmallPopUp(event:TimerEvent):void {
            if (smallPopUp != null) {
               PopUpManager.removePopUp(smallPopUp);
            }
            smallPopUp = null;
        }
    }
}
