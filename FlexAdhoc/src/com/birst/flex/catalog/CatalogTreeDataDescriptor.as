package com.birst.flex.catalog
{
	import com.birst.flex.core.FileNode;
	import com.birst.flex.core.FileUtils;
	import com.birst.flex.core.Tracer;
	
	import mx.collections.ArrayCollection;
	import mx.collections.ICollectionView;
	import mx.collections.XMLListCollection;
	import mx.controls.Tree;
	import mx.controls.treeClasses.ITreeDataDescriptor;

	public class CatalogTreeDataDescriptor implements ITreeDataDescriptor
	{
		private var rootNode:FileNode;
		private var parent:FileNode;
		private var _tree:Tree;
		private var refreshData:Boolean;
		private var open:Object;
		
		public function CatalogTreeDataDescriptor(tree:Tree) {
			_tree = tree;
			refreshData = false;
		}
		
		/* ----------------------------------------------------------------------------------------
		 * -- Functions required by the ITreeDataDescriptor interface                            --
		 * ---------------------------------------------------------------------------------------- */
		
        /**
         * Dummy function for adding a child node to the tree.  Addition is handled by the
         * process of moving, copying or uploading a file on the server and having a fresh version of the
         * file directory tree returned to the CatalogManagement component.
         */
        public function addChildAt(parent:Object, newChild:Object, index:int, model:Object=null):Boolean {
            return false;
        }
        
        public function getChildren(node:Object, model:Object=null):ICollectionView {
            var children:ArrayCollection = new ArrayCollection();
            if (node is XML) {
                var list:XMLList = (node as XML).child("children");
                if (list != null && list.length() > 0) {
                    // There's only one <children> element per FileNode
                    var xml:XML = list[0];
                    var allChildren:XMLListCollection = new XMLListCollection(xml.child("FileNode"));
                    for (var i:int = 0; i < allChildren.length; i++) {
                        if (allChildren[i].isDirectory == true) {
                            children.addItem(allChildren[i]);
                        }
                    }
                }
            } else {
                Tracer.error("Unexpected type in 'getChildren()' method.", this);
            }
            return children;
        }
        
//        public function getChildren(node:Object, model:Object=null):ICollectionView {
//            var children:XMLListCollection = null;
//            if (node is XML) {
//                var list:XMLList = (node as XML).child("children");
//                if (list != null && list.length() > 0) {
//                    // There's only one <children> element per FileNode
//                    var xml:XML = list[0];
//                    children = new XMLListCollection(xml.child("FileNode"));
//                }
//            } else {
//                Tracer.debug("Unexpected type in 'getChildren()' method.", this);
//            }
//            return children;
//        }
//        

        public function getData(node:Object, model:Object=null):Object {
            return node;
        }
        
        public function hasChildren(node:Object, model:Object=null):Boolean
        {
            var children:ICollectionView = getChildren(node, model);
            if (children == null || children.length == 0) {
                return false;
            } else {
                return true;
            }
        }
        
        public function isBranch(node:Object, model:Object=null):Boolean {
            // Set the folder icon 
            var folderIcon:Class = FileUtils.getFolderIcon(node as XML);
            _tree.setItemIcon(node, folderIcon, folderIcon); 
            var item:XML = node as XML;
            return item.isDirectory == "true";
        }
        
        /**
         * Dummy function for removing a child node out of the tree.  Removal is handled by the
         * process of moving or deleting a file on the server and having a fresh version of the
         * file directory tree returned to the CatalogManagement component.
         */
        public function removeChildAt(parent:Object, child:Object, index:int, model:Object=null):Boolean {
            return false;
        }

        /* ----------------------------------------------------------------------------------------
         * -- Helper functions                                                                   --
         * ---------------------------------------------------------------------------------------- */
        
		private function getObjectFromList(list:XMLList, xml:XML):XML {
			for (var i:int = 0; i < list.length(); i++) {
				var node:XML = list[i];
				if (node.FileNode.name == xml.FileNode.name) {
					return node;
				}
				if (xml.FileNode.name.toString().indexOf(node.FileNode.name) == 0) {
					getObjectFromList(node.FileNode.children, xml);
				}
			}
			return null;
		}
	}
}
