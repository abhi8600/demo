package com.birst.flex.catalog
{
    import com.birst.flex.core.FileUtils;
    import com.birst.flex.core.Tracer;
    import com.birst.flex.core.SoundUtil;
    import com.birst.flex.modules.CatalogManagementModule;
    
    import mx.collections.XMLListCollection;
    import mx.controls.Alert;
    import mx.controls.DataGrid;
    import mx.controls.Tree;
    import mx.controls.listClasses.ListBase;
    import mx.core.UIComponent;
    import mx.events.DragEvent;
    import mx.managers.DragManager;

	public class FolderTreeDndManager
	{
		private var folderTree:Tree;
		private var catMan:CatalogManagementModule;
		
		public function FolderTreeDndManager(folderTree:Tree, catMan:CatalogManagementModule) {
			this.folderTree = folderTree;
			this.catMan = catMan;
		}

        /**
         * This method saves off the dragged Listing because the handleDragOver()
         * method is modifying the drop feedback by setting the selectedIndex to
         * the tree item over which the mouse is currently hovering.  This trick
         * in the later handlers means that the selectedItem will be incorrect,
         * hence we save it here in the first drag event.
         */
        public function handleDragStart(event:DragEvent):void
        {
            // Get the target control & dataprovider
            var targetTree:Tree = event.dragInitiator as Tree;
            var treeProvider:XMLListCollection = targetTree.dataProvider as XMLListCollection;
            var draggedXml:XML = targetTree.selectedItem as XML;
            Tracer.debug("'handleDragStart()', 'draggedXml': " + draggedXml.toXMLString(), this);
            // Create instance of Listing from the dragged item
            catMan.draggedListings = new Array();
            var draggedListing:IListing = CatalogUtils.buildListing(draggedXml, treeProvider);
            catMan.draggedListings.push(draggedListing);
            Tracer.debug("'handleDragStart()', 'draggedListing': " + draggedListing, this);
        }

        /**
         * This function handles the dragDrop event for the folderTree Tree.
         * Since the DnD is copying a user type and not one of the AS3 native objects, 
         * it 'preventDefault()'s so that it can do its own object copy. 
         */
        public function handleDragDrop(event:DragEvent):void 
        {
            Tracer.debug("'handleDragDrop()' for 'folderTree' Tree.", this);
            
            if (event.dragInitiator is Tree) 
            {
            	handleDragDropFromTree(event);
            } 
            else 
            {
            	if (event.dragInitiator is DataGrid) 
            	{
            	   handleDragDropFromDataGrid(event);
            	}
            	else
            	{
            		Tracer.error("ERROR: Getting drag-and-drop event from: " + event.dragInitiator, this);
            	}
            }
        }
        
        protected function handleDragDropFromTree(event:DragEvent):void
        {
        	Tracer.debug("'handleDragDropFromTree()' in 'FolderTreeDndManager'.", this);
            // Pick up the dragged Listing from CatMan
            if (catMan.draggedListings == null || catMan.draggedListings.length == 0) {
                Tracer.debug("'handleDragDropFromTree()' encountered null or empty 'draggedListings'.", this); 
                return;
            }
        	        	
            // Get the single dragged listing (tree has multi-select off)
            var draggedListings:Array = catMan.draggedListings;
            
            // Derive the target directory
            var targetedListing:IListing = getTargetedListing(event);
            var targetDirectory:String = targetedListing.path; 

            // Check for invalid moves
            if (!isValidMoveCopyOperation(event, event.ctrlKey, draggedListings, targetDirectory)) 
            {
            	return;
            }
            
            var firstListing:IListing = draggedListings[0] as IListing;
            if (firstListing != null) {
                catMan.selectedListing = firstListing.parent;
            }

            // Check to see if any of these files are overwriting existing files
            var catalogDndHelper:CatalogDndHelper = new CatalogDndHelper(catMan);
            // The rest of the processing is in CatalogDndHelper
            catalogDndHelper.createConfirmedList(event, folderTree, draggedListings, targetDirectory);
        }

        protected function handleDragDropFromDataGrid(event:DragEvent):void 
        {
            Tracer.debug("'handleDragDropFromDataGrid()' in 'FolderTreeDndManager'.", this);
            // Pick up the dragged Listing from CatMan
            if (catMan.draggedListings == null || catMan.draggedListings.length == 0) {
            	Tracer.debug("'handleDragDropFromDataGrid()' encountered null or empty 'draggedListings'.", this); 
            	return;
            }
            var draggedListings:Array = catMan.draggedListings;

            // Derive the target directory
            var targetListing:IListing = getTargetedListing(event);
            var targetDirectory:String = targetListing.path;

            // Check for invalid moves
            if (!isValidMoveCopyOperation(event, event.ctrlKey, draggedListings, targetDirectory)) {
                return;
            }

            var firstListing:IListing = draggedListings[0] as IListing;
            if (firstListing != null) {
            	catMan.selectedListing = firstListing.parent;
            }

            // Check to see if any of these files are overwriting existing files
            var catalogDndHelper:CatalogDndHelper = new CatalogDndHelper(catMan);
            // The rest of the processing is in CatalogDndHelper
            catalogDndHelper.createConfirmedList(event, folderTree, draggedListings, targetDirectory);
        }
        
        public function handleDragEnter(event:DragEvent):void 
        {
            Tracer.debug("'handleDragEnter()' in 'folderTree' Tree.", this);
            DragManager.acceptDragDrop(UIComponent(event.currentTarget));
        }
        
        public function handleDragOver(event:DragEvent):void 
        {
            Tracer.debug("'handleDragOver()' in 'folderTree' Tree.", this);
            // Show drop point
            var dropTarget:Tree = Tree(event.currentTarget);
            var row:int = dropTarget.calculateDropIndex(event);
            folderTree.selectedIndex = row;
            var node:XML = folderTree.selectedItem as XML;
            // Check for restricted operations
            if (CatalogUtils.isRestrictedMoveOperation(event, catMan.draggedListings))
            {
                DragManager.showFeedback(DragManager.NONE);
                return;
            }
            // Check that .page files are being moved/copied to dashboard directories
            if (!CatalogUtils.movingPageFileToDashboardFolder(new Listing(node), catMan.draggedListings))
            {
    			DragManager.showFeedback(DragManager.NONE);
    			return;
            }
            if (event.ctrlKey)
                DragManager.showFeedback(DragManager.COPY);
            else if (event.shiftKey)
                DragManager.showFeedback(DragManager.LINK);
            else {
                DragManager.showFeedback(DragManager.MOVE);
            }
        }
        
        public function handleDragExit(event:DragEvent):void 
        {
            Tracer.debug("'handleDragExit()' in 'folderTree' Tree.", this);
            var dropTarget:ListBase = ListBase(event.currentTarget);
            dropTarget.hideDropFeedback(event);
        }

        public function handleDragComplete(event:DragEvent):void 
        {
            Tracer.debug("'handleDragComplete()' in 'folderTree' Tree.", this);
            event.preventDefault();
        }

        /* ------------------------
         * 'private' helper methods
         * ------------------------ */

        private function getTargetedListing(event:DragEvent):IListing
        {
        	// Build the listing from the target XML node
        	var targetXml:XML = (event.currentTarget as Tree).selectedItem as XML;
        	var treeProvider:XMLListCollection = folderTree.dataProvider as XMLListCollection;
        	var targetListing:IListing = CatalogUtils.buildListing(targetXml, treeProvider);
            Tracer.debug("'getTargetedListing' 'targetListing': " + targetListing, this);
            return targetListing;
        }
         
        private function isValidMoveCopyOperation(event:DragEvent,
                                                  isCopy:Boolean,
                                                  sourceListings:Array, 
                                                  targetDirectory:String):Boolean
        {
        	if (targetDirectory == null || targetDirectory == "")
        	{
        		SoundUtil.playErrorSound();
                Alert.show("Invalid target for a move or copy.", "Invalid request", Alert.OK);
        		return false;
        	}
            // Moving within same directory 
            for each (var sourceListing:IListing in sourceListings) {
	            if (!isCopy) {
	            	var sourceDirectory:String = FileUtils.getParentDirectory(sourceListing.path);
		            if (sourceAndTargetIdentical(sourceDirectory, targetDirectory)) 
		            {            
		                SoundUtil.playErrorSound();
		                Alert.show("Can't move an item to the same folder.", "Invalid request", Alert.OK);
		                return false;
		            }
                }
                // Moving/copying a parent to a subfolder?  No. No. No.
	            if (targetIsChildOfSource(sourceListing.path, targetDirectory)) 
	            {
	                SoundUtil.playErrorSound();
	                Alert.show("Can't move or copy a parent\nfolder to one of its subfolders.",
	                           "Invalid request", Alert.OK);
	                return false;
	            }
	            // Moving a child folder of 'private'?  No. No. No.
	            if (!event.ctrlKey && sourceIsChildOfPrivate(sourceListing.path))  
	            {
	                SoundUtil.playErrorSound();
	                Alert.show("Cannot move a direct subfolder of the main 'private' folder.", "Invalid request", Alert.OK);
	                return false;
	            }
	            // Can't move the 'private' folder
	            if (!event.ctrlKey && sourceListing.path == "private") {
	            	SoundUtil.playErrorSound();
	            	Alert.show("Cannot move 'private' folder.", "Invalid request", Alert.OK);
	            	return false;
	            }
	        }
            // Can't move or copy files to the 'private/' folder
            if (targetDirectory == "private") {
                SoundUtil.playErrorSound();
                Alert.show("Cannot move or copy files/folders to the 'private' folder.", "Invalid request", Alert.OK);
                return false;
            }
            return true;
        }

        private function sourceAndTargetIdentical(sourceDirectory:String,
                                                  targetDirectory:String):Boolean
        {
            // Moving within same directory 
            if (sourceDirectory == targetDirectory) 
            {            
                return true;
            }
            return false;
        }
        
        private function targetIsChildOfSource(sourceDirectory:String,
                                               targetDirectory:String):Boolean
        {
            // Moving/copying a parent to a subfolder?  No. No. No.
            var index:int = targetDirectory.indexOf(sourceDirectory); 
            if (index == 0) 
            {
                return true;
            }
            return false;
        }
        
        private function sourceIsChildOfPrivate(sourceDirectory:String):Boolean 
        {
        	var folders:Array = sourceDirectory.split("/");
        	if (folders != null && folders.length == 2 && folders[0] == "private")
        	{
        		return true;
        	} 
        	return false;
        }
	}
}