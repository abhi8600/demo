package com.birst.flex.catalog.events
{
	import flash.events.Event;

	public class FileDeleteEvent extends Event
	{
		public static const DELETE:String = "Delete";
		
		private var _listings:Array;
		
        public function FileDeleteEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
        {
            super(type, bubbles, cancelable);
        }
        
        [Bindable]
		public function set listings(value:Array):void {
			_listings = value;
		}
		public function get listings():Array {
			return _listings;
		}
	}
}