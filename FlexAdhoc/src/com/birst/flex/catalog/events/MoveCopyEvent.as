package com.birst.flex.catalog.events
{
	import com.birst.flex.catalog.IListing;
	
	import flash.events.Event;
	
	import mx.events.DragEvent;

	public class MoveCopyEvent extends Event
	{
		public static const CONFIRM:String = "confirm";
        public static const COMPLETE:String = "complete";
        public static const CANCEL:String = "cancel";
		/** 
		 * In 'confirm' processing, a non-null entry means this entry should be removed 
		 * from the filtered list.
		 */ 
		private var _listingToRemove:IListing;
		/**
		 * In 'complete' processing, this list is dispatched to the *DndManager instance
		 * to complete the processing with the final filtered list of files/folders to 
		 * move or copy.
		 */
		private var _filteredListings:Array;
		
		private var _dragEvent:DragEvent;
		
		private var _targetDirectory:String;
		
		public function MoveCopyEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
        
        public function get listingToRemove():IListing {
            return _listingToRemove;
        }
        public function set listingToRemove(value:IListing):void {
            _listingToRemove = value;
        }
        
        public function get filteredListings():Array {
            return _filteredListings;
        }
        public function set filteredListings(value:Array):void {
            _filteredListings = value;
        }
        
        public function get dragEvent():DragEvent {
            return _dragEvent;
        }
        public function set dragEvent(value:DragEvent):void {
            _dragEvent = value;
        }
        
        public function get targetDirectory():String {
            return _targetDirectory;
        }
        public function set targetDirectory(value:String):void {
            _targetDirectory = value;
        }
	}
}