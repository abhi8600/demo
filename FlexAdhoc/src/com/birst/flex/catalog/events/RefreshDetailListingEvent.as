package com.birst.flex.catalog.events
{
	import com.birst.flex.catalog.IListing;
	
	import flash.events.Event;

	public class RefreshDetailListingEvent extends Event
	{
		public static const REFRESH:String = "refresh";
		
		// This is the listing to be used as the 'selectedListing' for the refresh
		private var _listing:IListing;
		
		public function RefreshDetailListingEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
		
		public function get listing():IListing {
			return _listing;
		}
        public function set listing(value:IListing):void {
        	_listing = value;
        }
	}
}