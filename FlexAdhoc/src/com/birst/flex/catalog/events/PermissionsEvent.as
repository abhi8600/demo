package com.birst.flex.catalog.events
{
	import flash.events.Event;

	public class PermissionsEvent extends Event
	{
		public static const UPDATE:String = "Update";
		
		private var _folderPath:String;
		private var _permissions:XMLList;
		
		public function PermissionsEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
        
        [Bindable]
        public function set folderPath(folderPath:String):void
        {
            _folderPath = folderPath;
        }
        
        public function get folderPath():String
        {
            return _folderPath;
        }
        
        [Bindable]
        public function set permissions(permissions:XMLList):void
        {
            _permissions = permissions;
        }
        
        public function get permissions():XMLList
        {
            return _permissions;
        }
	}
}