package com.birst.flex.catalog.events
{
	import com.birst.flex.catalog.IListing;
	
	import flash.events.Event;

	public class AddFolderEvent extends Event
	{
        public static const ADD_FOLDER:String = "AddFolder";

		private var _folderName:String;
		private var _listing:IListing;
		
		public function AddFolderEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
        
        [Bindable]
        public function set folderName(value:String):void {
            _folderName = value;
        }
        public function get folderName():String {
            return _folderName;
        }
        
        [Bindable]
        public function set listing(value:IListing):void {
            _listing = value;
        }
        public function get listing():IListing{
            return _listing;
        }
	}
}