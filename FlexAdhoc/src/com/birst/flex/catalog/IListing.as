package com.birst.flex.catalog
{
	public interface IListing
	{
        function set name(name:String):void;        

        function get name():String;
        
        function set basename(basename:String):void;
        
        function get basename():String;
        
        function set filetype(filetype:String):void;

        function get filetype():String;
        
        function isDirectory():Boolean;
        
        function isDashboard():Boolean;
        
        function isFile():Boolean;
        
        function set path(path:String):void;
        
        function get path():String;
        
        function set size(size:String):void;
        
        function get size():String; 
        
        function set createdDate(createdDate:String):void;
        
        function get createdDate():String;
        
        function set modifiedDate(modifiedDate:String):void;
        
        function get modifiedDate():String;
        
        function set parent(parent:IListing):void;
        
        function get parent():IListing;
        
        function toString():String;
	}
}