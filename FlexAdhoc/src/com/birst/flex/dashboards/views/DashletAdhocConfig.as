package com.birst.flex.dashboards.views
{
	import com.birst.flex.adhoc.AdHocConfig;
	import com.birst.flex.core.ReportXml;

	public class DashletAdhocConfig extends AdHocConfig
	{
		private static var _inst:DashletAdhocConfig;
		private var _dashletCnt:DashletContentBase;
		private var _prompts:XML;
		
		public function DashletAdhocConfig()
		{
			super();
		}
		
		public static function instance():DashletAdhocConfig{
			if(!_inst){
				_inst = new DashletAdhocConfig();
			}
			return _inst;
		}
		
		public function set dashletContent(dash:DashletContentBase):void{
			_dashletCnt = dash;	
		}
		
		public function get dashletContent():DashletContentBase{
			return _dashletCnt;
		}
		
		public function set prompts(prompts:XML) :void {
			_prompts = prompts;
		}
		public function get prompts() : XML {
			return (_prompts) ? _prompts : XML(<Prompts></Prompts>);
		}
		
		public function setReportXml(conf:ReportXml):void{
			_config = conf;	
		}
		
		override public function asyncSaveConfigToServer(caller:String, method:String, data:Object = null):void{	
			if (_dashletCnt){
				_dashletCnt.loadReportByReportXml();
			}	
		}
		
		override public function asyncUpdateConfig(caller:String, method:String):void{
			if(_dashletCnt){
				_dashletCnt.loadReportByReportXml();
			}
		}
	}
}