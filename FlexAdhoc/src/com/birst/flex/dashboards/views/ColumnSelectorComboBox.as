package com.birst.flex.dashboards.views
{
	import mx.controls.ComboBox;
	import mx.events.FlexEvent;
	import mx.events.ListEvent;

	public class ColumnSelectorComboBox extends ComboBox
	{
		private var _xml:XML;
		
		public function ColumnSelectorComboBox()
		{
			super();
			
			addEventListener(FlexEvent.CREATION_COMPLETE, onCreateComplete);
		}
		
		private function onCreateComplete(event:FlexEvent):void{
			addEventListener(ListEvent.CHANGE, onChange, false, 9999);	
		}
		
		private function onChange(event:ListEvent):void{
			var xml:XML = this.selectedItem as XML;
			var selected:XMLList = _xml.Selected;
			if(selected.length() > 0){
				selected[0] = String(xml.ID);
			}			
		}
			
		private function columnSelLabelFunc(item:XML):String{
			return item.Label.toString();	
		}
		
		public function set xml(xml:XML):void{
			_xml = xml;
			
			var selected:String = xml.Selected.toString();
			var selectors:XMLList = xml.ColumnSelector;
			if(selectors.length() > 0){
				labelFunction = columnSelLabelFunc;
				dataProvider = selectors;
				setSelectedIndexByDimCol(selected);
			}
		}
		
		public function setSelectedIndexByDimCol(selected:String):Boolean{
			var selectors:XMLList = _xml.ColumnSelector;
			for(var i:int = 0; i < selectors.length(); i++){
				var colSel:XML = selectors[i];
				var colDim:String = colSel.ID.toString();
				if (colDim == selected){
					selectedIndex = i;
					return true;
				}
			}	
			return false;
		}
		
		public function get xml():XML{
			return _xml;
		}
	}
}