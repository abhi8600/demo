package com.birst.flex.dashboards.views
{
	import com.birst.flex.core.CoreApp;
	import com.birst.flex.core.IconPanel;
	import com.birst.flex.core.MenuItemActions;
	import com.birst.flex.core.Tracer;
	import com.birst.flex.core.Util;
	import com.birst.flex.dashboards.events.DashletEvent;
	import com.birst.flex.dashboards.layout.ScaleType;
	import com.birst.flex.webservice.WebServiceClient;
	
	import flash.display.DisplayObjectContainer;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.ui.ContextMenu;
	
	import mx.containers.HBox;
	import mx.controls.Alert;
	import mx.controls.Button;
	import mx.controls.Label;
	import mx.controls.Menu;
	import mx.controls.Spacer;
	import mx.core.Application;
	import mx.effects.Move;
	import mx.effects.Parallel;
	import mx.effects.Resize;
	import mx.effects.easing.Exponential;
	import mx.events.FlexEvent;
	import mx.events.MenuEvent;
	import mx.resources.ResourceManager;

	public class Dashlet extends IconPanel
	{
		[Bindable]
		public var editMode:Boolean = false;
		
		[Bindable]
		public var enableSelfSchedule:Boolean = false;
		
		[Bindable]
		public var enableVisualizer:Boolean = false;
		
		[Bindable]
		public var enablePDF:Boolean = false;
		
		[Bindable]
		public var enablePPT:Boolean = false;
	
		[Bindable]
		public var enableCSV:Boolean = false;
		
		[Bindable]
		public var enableExcel:Boolean = false;
			
		[Bindable]
		public var  enableViewSelector:Boolean = false;
		
		[Bindable]
		public var enablePivotCtrl:Boolean = false;
		
		[Bindable]
		public var enableSelectorsCtrl:Boolean = false;
			
		[Bindable]
		public var enableAdhoc:Boolean = false;
		
		[Bindable]
		public var applyAllPrompts:Boolean = true;
			
		[Bindable]
		public var ignorePromptList:XML = null;
		
		[Bindable]
		public var dashletDescription:String = null;
		
		[Bindable]
		public var enableDashletDescriptionMode:Boolean = false;
		
		//External iframe section
		[Bindable]
		public var isExternalIframe:Boolean = false;
		public var externalUrl:String = '';
		
		[Bindable]
		public var isInternalPlugin:Boolean = false;
		public var pluginExtension:String = '';
		
		protected var _content:DashletContentBase;
		private var _scaleType:String;
		private var _name:String;
		
		public static const VIEW_FULL:int = 0;
		public static const VIEW_HEADERLESS:int = 1;
		public static const VIEW_HEADER_BORDER_LESS:int = 2;
		
		//This is global
		public static var viewMode:int = VIEW_FULL;
		private var _instanceViewMode:int = VIEW_FULL;
		
		//This is override per dashlet for viewMode
		private var xmlViewMode:String = null;
		
		public static var showMaxMinCloseBttn:Boolean = true;
		public static var showIconsInDashletTitleBar:Boolean = true;
		
		public static const MINIMIZED_HEIGHT:Number = 22;
		
   		private var minimizeButton:Button;
    	private var maximizeRestoreButton:Button;
    	
    	//edit mode icon
    	private var editButton:Button;
    	private var editReportButton:Button;

		private var adhocButton:Button;
		private var pivotButton:Button;
		private var pdfButton:Button;
		private var pptButton:Button;
		private var xlsButton:Button;
		private var csvButton:Button;
		private var selfScheduleButton:Button;
		private var descriptionModeButton:Button;
		
		private var execTime:Number;
		private var pagePath:String;
		private var pageUuid:String;
		private var filename:String;
		private var noRender:Boolean;
		private var isDashletEdit:Boolean;
		private var prompts:XML;		
		
		//options (gear) icon
		private var gearButton:Button;
		private var optionsMenu:Menu;
		
		//private var headerDivider:Sprite;
	
		public function Dashlet()
		{
			super();
			this.windowState = WINDOW_STATE_DEFAULT;
			this.setStyle('roundedBottomCorners', true);
			this.percentHeight = 100;
			this.percentWidth = 100;
			this.addEventListener(FlexEvent.CREATION_COMPLETE, onCreationComplete);
		}

		private function onCreationComplete(event:FlexEvent):void{
			this.titleBar.addEventListener(MouseEvent.DOUBLE_CLICK, onTitleBarDoubleClick);
		}
		
		private function onTitleBarDoubleClick(event:MouseEvent):void{
			toggleMaximizeMinimize();
		}
		
		private function createHeaderIcons():void{
			if(Application.application.isAdhocable && enableAdhoc && !adhocButton){
	        	adhocButton = createTitleBarIcon("editReportButton", "editReportButton", onClickAdhocButton);
	        }
	        	
        	if (Application.application.isPivotControlInDashletsEnabled && enablePivotCtrl && !pivotButton){
        		pivotButton = createTitleBarIcon("pivotButton", "pivotToolTip", onClickPivotButton);
        	}
        	
        	if (Application.application.isDownloadable && enableCSV && !csvButton){
        		csvButton = createTitleBarIcon("csvButton", "csvTooltip", onClickCsvButton);
        	}
        	
        	if (Application.application.isDownloadable && enableExcel && !xlsButton) {
        		xlsButton = createTitleBarIcon("xlsButton", "xlsTooltip", onClickXlsButton);
        	}
        	
        	if (Application.application.isDownloadable && enablePDF && !pdfButton){
        		pdfButton = createTitleBarIcon("pdfButton", "pdfTooltip",onClickPdfButton);
        	}
        	
        	if (Application.application.isDownloadable && enablePPT && !pptButton) {
        		pptButton = createTitleBarIcon("pptButton", "pptTooltip", onClickPptButton);
        	}
        	
        	if(Application.application.isSelfScheduleEnabled && enableSelfSchedule && !selfScheduleButton) {
        		selfScheduleButton = createTitleBarIcon("scheduleButton", "scheduleTooltip", onClickScheduleButton);
        	}
        	
        	if (Application.application.isEnableAsyncReportGeneration && enableDashletDescriptionMode && !descriptionModeButton) {
        		descriptionModeButton = createTitleBarIcon("descriptionMode", "descriptionMode", onClickDescriptionModeButton);
        	}
		}
		
		private function onClickOptionButton(event:MouseEvent = null):void{
			if(!this.optionsMenu){
				var dp:Array = [];
				
				//Pivot Control
				addMenuItem(Application.application.isPivotControlInDashletsEnabled && enablePivotCtrl, dp, "DO_pivot", onClickPivotButton);
				
				//Export
				if(Application.application.isDownloadable){
					var exportList:Array = [];
					addMenuItem(enableCSV, exportList, "DO_csv", onClickCsvButton);
					addMenuItem(enableExcel, exportList, "DO_xcl", onClickXlsButton);
					addMenuItem(enablePDF, exportList, "DO_pdf", onClickPdfButton);
					addMenuItem(enablePPT, exportList, "DO_ppt", onClickPptButton);
				
					if(exportList.length > 0){
						var expt:Object = addMenuItem(true, dp, "DO_expt", null);
						delete expt['fn'];
						expt['children'] = exportList;
					}
				}
				
				//Schedule report
				addMenuItem(Application.application.isSelfScheduleEnabled && enableSelfSchedule, dp, "DO_schedule", onClickScheduleButton);
				
				//Adhoc / Designer
				addMenuItem(Application.application.isAdhocable && enableAdhoc, dp, "DO_designer", onClickEditReportButton);
				
				addMenuItem(Application.application.isEnableAsyncReportGeneration && enableDashletDescriptionMode, dp, "DO_render", onClickDescriptionModeButton);
				
				this.optionsMenu = Menu.createMenu(Application.application as DisplayObjectContainer, dp);
				this.optionsMenu.addEventListener(MenuEvent.ITEM_CLICK, MenuItemActions.menuItemClickHandler);
				
				//Hack - have it show and hide the first time for us to know the menu's width
				this.optionsMenu.show();
				this.optionsMenu.hide();
			}	
			
			var pt:Point = gearButton.localToGlobal(new Point(0,0));
			this.optionsMenu.show(pt.x - this.optionsMenu.width + 5, pt.y + 15);
		}
		
		private function addMenuItem(add:Boolean, dp:Array, labelKey:String, fn:Function):Object{
			if(add){
				var label:String =  ResourceManager.getInstance().getString(resourceProperty, labelKey);
				var o:Object = {label: label, fn: fn};
				dp.push(o);
				return o;
			}
			return null;
		}
		
		override protected function createChildren():void {
	        super.createChildren();
	        
	        this.titleBar.doubleClickEnabled = true;
	        
	        if(editMode){
	        	if(!editButton){
	        		editButton = createTitleBarIcon("editButton", "editButton", onClickEditButton);
	        	}
	        	if(!editReportButton && Application.application.isAdhocable && !this.isExternalIframe && !this.isInternalPlugin){
	        		editReportButton = createTitleBarIcon("editReportButton", "editReportButton", onClickEditReportButton);
	        	}
	        }else{
	        	//Old implementation, display multiple icons on header
	        	if (Dashlet.showIconsInDashletTitleBar)
	        	{
	        		createHeaderIcons();
	        	}
	        	else
	        	{
	        		//Create the gear button only if needed 
	        		if(enableAdhoc || enablePivotCtrl ||
	        			(Application.application.isSelfScheduleEnabled && enableSelfSchedule) ||
	        			(Application.application.isDownloadable && (enableExcel || enablePDF || enablePPT)) ){
	        	 			gearButton = createTitleBarIcon("optButton", "gearTooltip", onClickOptionButton);
	        	 		}
	        	 }
	        	 if(this.isExternalIframe || this.isInternalPlugin){
	        	 	var spacer:Spacer = new Spacer();
	        	 	spacer.percentHeight = 50;
	        	 	this.addChild(spacer);
	        	 	
	        	 	var hbox:HBox = new HBox();
	        	 	hbox.percentWidth = 100;
	        	 	
	        	 	var halfWidth:Spacer = new Spacer();
	        	 	halfWidth.percentWidth = 30;
	        	 	hbox.addChild(halfWidth);
	        	 	
	        	 	var label:Label = new Label();
	        	 	label.text = ResourceManager.getInstance().getString(resourceProperty, 'DO_oops');
	        	 	hbox.addChild(label);
	        	 	
	        	 	addChild(hbox);
	        	 }
	        }
	        
	        if (!maximizeRestoreButton) {
	            maximizeRestoreButton = createTitleBarIcon("maximizeRestoreButton", "maximizeRestoreButton", onClickMaximizeRestoreButton);
	        }
    	}

		override protected function updateDisplayList(unscaledWidth:Number, unscaledHeight:Number):void {
			if(editMode){
				toggleMode(Dashlet.VIEW_FULL);
			}else{
				if(this.xmlViewMode){
					this.setViewModeFromDefinition(this.xmlViewMode);
					displayViewMode(this._instanceViewMode);
				}else{
	         	 	toggleMode(Dashlet.viewMode);
	  			 }
	  		}
	  		this.maximizeRestoreButton.visible = Dashlet.showMaxMinCloseBttn;
	  		
	        updateTitleBar = (_instanceViewMode == Dashlet.VIEW_FULL || _instanceViewMode == Dashlet.VIEW_HEADERLESS);
	        super.updateDisplayList(unscaledWidth, unscaledHeight);
    	}

		public function set content(cnt:DashletContentBase):void{
			this._content = cnt;
		}	
		
		public function isContentDisplayed():Boolean{
			if(_content){
				try{
					getChildIndex(_content);
					return true;
				}catch(e:Error){}
			}	
			return false;
		}
		
		//Re-add the child
		public function regainContent():void{
			if(_content){
				try{
					getChildIndex(_content);
				}catch(e:Error){
					_content.enabled = true;
					addChild(_content);
				}
			}
		}
		
		public function get content():DashletContentBase{
			return _content;	
		}
		
		public function renderReportByPath(execTime:Number, 
										   pagePath:String, 
										   pageUuid:String, 
										   filename:String, 
										   prompts:XML, 
										   getReportPrompts:Boolean, 
										   enableRectSelect:Boolean,
										   noRender:Boolean = false,
										   isDashletEdit:Boolean = false):void{
			if(filename){
				
				//Notify our parent that they need to retrieve report prompts and delete the previous report prompts
				//Only in edit mode
				if(editMode && getReportPrompts){
					dispatchEvent(new DashletEvent(DashletEvent.GET_REPORT_PROMPTS, true, false, {oldReport: _name, newReport: filename}));
				}
				
				_name = filename;
				if (!_content){
					_content = new DashletContentBase(enableSelectorsCtrl, enableViewSelector, applyAllPrompts, ignorePromptList, enableDashletDescriptionMode);
					
					
					
					//We cache the report in edit mode
					_content.cacheReport = editMode;
					
					//Set the apropriate context menu
					if(contextMenu){
						_content.contextMenu = contextMenu;
					}else if (parent && parent.contextMenu){
						_content.contextMenu = parent.contextMenu;
					}
					
					setupScaling();
					_content.addEventListener(DashletEvent.RENDERING_CONTENT, throwUpEvent);
					addChild(_content);
				}
				
				//rectangle select
				_content.rectangleSelect = enableRectSelect;
	
				if (this.enableDashletDescriptionMode && Application.application.isEnableAsyncReportGeneration) {
					this.execTime = execTime;
					this.pagePath = pagePath;
					this.pageUuid = pageUuid;
					this.filename = filename;
					this.noRender = noRender;
					this.isDashletEdit = isDashletEdit;
					this.prompts = prompts;
					_content.renderDashletDescription(this.dashletDescription, filename, pageUuid, pagePath);
				}
				else {
					_content.renderReportByPath(filename, prompts, execTime, pagePath, pageUuid, noRender, isDashletEdit);
				}
				
			}
		}
		
		public function dispatchRemovePrompts():void{
			dispatchEvent(new DashletEvent(DashletEvent.REMOVE_REPORT_PROMPTS, true, false, _name));	
		}
		
		public function set contentContextMenu(ctx:ContextMenu):void{
			if (_content){
				_content.contextMenu = ctx;
			}
		}
		
		private function throwUpEvent(event:DashletEvent):void{
			dispatchEvent(new DashletEvent(DashletEvent.RENDERING_CONTENT));
			if (this.enableDashletDescriptionMode) {
				var app:CoreApp = CoreApp(Application.application);
				var title:String = app.getLocString('AR_Information');
				var msg:String = app.getLocString('AR_Complete', [this.title]);
				Alert.show(msg, title, Alert.OK);
			}
		}
		
		public function load(prompts:XML, execTime:Number, pagePath:String, pageUid:String):void{
			if(this._content){
				Tracer.debug('dashlet.load() : ' + this._content, this);
				if (this.enableDashletDescriptionMode && !this._content.isAlreadyRendered()) {
					this.prompts = prompts;
					this.execTime = execTime;
					this.pagePath = pagePath;
					this.pageUuid = pageUid;
				}
				else {
					_content.loadReport(prompts, 0, execTime, pagePath, pageUid);
				}
			}	
		}
		
		public function fitToDimensions():void{
			if(this._content){
				this._content.fitToDimensions();
			}			
		}
		
		public function scaleAutoFit():void{
			_scaleType = ScaleType.SCALE_TO_FIT;
			_content && (_content.autoFit = true);	
		}
		
		public function noScaling():void{
			_scaleType = ScaleType.NO_SCALE;
			_content && (_content.autoFit = false);	
		}
		
		private function setupScaling():void{
			if(_content){
				_content.autoFit = (_scaleType == ScaleType.SCALE_TO_FIT);
			}
		}
		
		public function reRenderContent(noCache:Boolean = false):void{
			_content.reRenderContent(noCache);
		}
		
		public function unscale():void{
			if(this._content){
				this._content.unscale();
			}
		}
		
		private function onClickMaximizeRestoreButton(event:MouseEvent):void{
			Application.application.trackEvent("min/max dashlet");
			Tracer.debug('onClickMaximizeRestoreButton: state - ' + windowState, this);
			if (this.windowState == IconPanel.WINDOW_STATE_MINIMIZED || this.windowState == WINDOW_STATE_DEFAULT){
				this.dispatchEvent(new DashletEvent(DashletEvent.MAXIMIZE));
				Tracer.debug('set state to ' + IconPanel.WINDOW_STATE_MAXIMIZED, this);
				this.windowState = IconPanel.WINDOW_STATE_MAXIMIZED;
				this._content.maximizie();
			}else if (this.windowState == IconPanel.WINDOW_STATE_MAXIMIZED){
				this.dispatchEvent(new DashletEvent(DashletEvent.MINIMIZE));
				this.windowState = IconPanel.WINDOW_STATE_MINIMIZED;
			}
		}
		
		private function onClickPivotButton(event:Object):void{
			if(_content){
				_content.showPivotTable();
			}	
		}
		
		private function onClickPdfButton(event:Object):void{
			if(_content){
				_content.generatePdf();
			}
		}
		
		private function onClickPptButton(event:Object):void {
			if (_content) {
				_content.generatePpt();
			}
		}
		
		private function onClickCsvButton(event:Object):void{
			if(_content){
				_content.generateCsv();	
			}
		}
		
		private function onClickXlsButton(event:Object):void{
			if(_content){
				_content.generateXls();	
			}
		}
		
		private function onClickEditButton(event:MouseEvent):void{
			dispatchEvent(new DashletEvent(DashletEvent.EDIT, true));
		}
		
		private function onClickEditReportButton(event:Object):void{
			if(_content){
				dispatchEvent(new DashletEvent(DashletEvent.EDIT_REPORT, true, false, _content._name));
			}
		}
		
		private function onClickAdhocButton(event:Object):void{
			if(_content){
				dispatchEvent(new DashletEvent(DashletEvent.EDIT_REPORT, true, false, _content._name));
			}
		}
		
		/**
		 * Returns the list of prompts to be ignored / not applied
		 */
		public function getExcludedPrompts():Array{
			var list:Array = new Array();
			if (this.ignorePromptList != null) {
				for each (var c:XML in this.ignorePromptList.child("item")) {
					list.push(c.toString());
				}
			}
			return list;
		}
		
		private function onClickScheduleButton(event:Object):void{
			if(_content){
				dispatchEvent(new DashletEvent(DashletEvent.SELF_SCHEDULE_NEW, true, false, {name: _content._name, applyAllPromts: _content.applyAllPrompts, excludedPrompts: this.getExcludedPrompts()}));
			}
		}
		
		private function onClickDescriptionModeButton(event:Object):void {
			if (_content) {
				// open report
				// set async mode
				// render
                    var ws:WebServiceClient = new WebServiceClient("Adhoc", 
                                                                   "openReport", 
                                                                   "FileDialog", 
                                                                   "saveButtonClickHandler");
                    ws.setResultListener(_content.openReport);
                    ws.execute(filename);
//				_content.renderReportByPath(filename, prompts, execTime, pagePath, pageUuid, noRender, isDashletEdit)
			}
		}
		public function toggleMaximizeMinimize():void{
			onClickMaximizeRestoreButton(new MouseEvent(MouseEvent.CLICK));	
		}
		
		public function minimizedEffectDone():void{
			this._content.minimize();	
		}
		
		public function getReportWidthDiff():int{
			if (_content){
				var w:int = _content.getContentWidth();
				if (w > this._content.width){
					var diff:int = w - this._content.width;
					Tracer.debug('getReportWidthDiff() ' + this + ' = ' + diff, this);
					return diff;
				}
			}
			return 0;
		}
		
		public function getReportHeightDiff():int{
			if (_content){
				var w:int = _content.getContentHeight();
				if (w > this._content.height){
					var diff:int = w - this._content.height;
					Tracer.debug('getReportHeightDiff() ' + this + ' = ' + diff, this);
					return diff;
				}
			}
			return 0;
		}
		
		// Creates a resize and move event and adds them to a parallel effect.
	    private function addResizeAndMoveToParallel(target:Dashlet, widthTo:Number, heightTo:Number, xTo:Number, yTo:Number):void
	    {
	    	var parallel:Parallel = new Parallel();
	    	
	        var resize:Resize = new Resize(target);
	        resize.widthTo = widthTo;
	        resize.heightTo = heightTo;
	        resize.easingFunction = Exponential.easeOut;
	        parallel.addChild(resize);
	        
	        var move:Move = new Move(target);
	        move.xTo = xTo;
	        move.yTo = yTo;
	        move.easingFunction = Exponential.easeOut;
	        parallel.addChild(move);
	        
	        parallel.duration = 1000;
	        parallel.play();
	    }

		public function setAsInternalPlugin(path:String, extension:String):void{
			this.isInternalPlugin = true;
			this.isExternalIframe = false;
			this._name = path;
			this.pluginExtension = extension;	
		}
		
		public function setProperties(props:Array):void{
			if(props){
				if(this.isExternalIframe){
					this._name = '';
					this.title = props.Title;
					this.externalUrl = props.Url;
				}else if (this.isInternalPlugin){
					this.title = props.Title;
					this.enableVisualizer = props.EnableVisualizer;
				}else{
					this.title = props.Title;
					this.enableCSV = props.EnableCSV;
					this.enableExcel = props.EnableExcel;
					this.enablePDF = props.EnablePDF;
					this.enablePPT = props.EnablePPT;
					this.enableViewSelector = props.EnableViewSelector;
					this.enablePivotCtrl =  props.EnablePivotCtrl;
					this.enableSelectorsCtrl = props.EnableSelectorsCtrl;
					this.enableAdhoc = props.EnableAdhoc;
					this.enableSelfSchedule = props.EnableSelfSchedule;
					this.enableVisualizer = props.EnableVisualizer;
					this.applyAllPrompts = props.ApplyAllPrompts
					this.enableDashletDescriptionMode = props.enableDashletDescriptionMode;
					this.dashletDescription = props.DashletDescription;
					
					if(_name != props.Path){
						renderReportByPath(0, "NO_LOG", "NO_LOG", props.Path, null, true, false, false, true);
					}
					this._name = props.Path;
				}
				
				this.ignorePromptList = props.ignorePromptList;
				
				setViewModeFromDefinition(props.ViewMode);
			}	
		}
		
		public function fromXML(xml:XML):void{
			if(xml){
				isExternalIframe = xml.IsExternalIframe.toString() == "true";
				isInternalPlugin = xml.IsInternalPlugin.toString() == "true";
				if(this.isInternalPlugin){
					this.pluginExtension = xml.PluginExt;
				}
				externalUrl = xml.Url;
				_name = xml.Path;
				title = xml.Title;
				enableCSV = isEnabled(xml, "EnableCSV");
				enableExcel = isEnabled(xml, "EnableExcel");
				enablePDF = isEnabled(xml, "EnablePDF");
				enablePPT = isEnabled(xml, "EnablePPT");
				enableViewSelector = isEnabled(xml, "EnableViewSelector");
				enablePivotCtrl =  isEnabled(xml, "EnablePivotCtrl");
				enableSelectorsCtrl = isEnabled(xml, "EnableSelectorsCtrl");
				enableAdhoc = isEnabled(xml, "EnableAdhoc");
				applyAllPrompts = isEnabled(xml, "ApplyAllPrompts");
				enableSelfSchedule = isEnabled(xml, "EnableSelfSchedule");
				enableVisualizer = isEnabled(xml, "EnableVisualizer");
				this.enableDashletDescriptionMode = isEnabled(xml, "enableDashletDescriptionMode");
				if (Application.application.isEnableAsyncReportGeneration == false) {
					this.enableDashletDescriptionMode = false;
				}
				
				this.dashletDescription = xml.DashletDescription;
				var l:XMLList = xml.child("ignorePromptList");
				if (l && l.length() > 0){
					ignorePromptList = l[0];
				}
				
				//individual viewMode
				var vMode:String = xml.ViewMode;
				setViewModeFromDefinition(vMode);
			}
		}
		
		public function toXML():XML{
			var xml:XML = <dashlet/>;
			xml.Path = this._name ? this._name : '';
			xml.Title = this.title;
			xml.EnablePDF = this.enablePDF;
			xml.EnablePPT = this.enablePPT;
			xml.EnableCSV = this.enableCSV;
			xml.EnableExcel = this.enableExcel;
			xml.EnableViewSelector = this.enableViewSelector;
			xml.EnablePivotCtrl = this.enablePivotCtrl;
			xml.EnableSelectorsCtrl = this.enableSelectorsCtrl; 
			xml.EnableAdhoc = this.enableAdhoc;
			xml.ApplyAllPrompts = this.applyAllPrompts;
			xml.ignorePromptList = ignorePromptList ? this.ignorePromptList : '';
			xml.EnableSelfSchedule = this.enableSelfSchedule;
			xml.IsExternalIframe = this.isExternalIframe;
			xml.IsInternalPlugin = this.isInternalPlugin;
			xml.EnableVisualizer = this.enableVisualizer;
			xml.enableDashletDescriptionMode = enableDashletDescriptionMode;
			xml.DashletDescription = dashletDescription;
			if(this.isInternalPlugin){
				xml.PluginExt = this.pluginExtension;
			}
			xml.Url = this.externalUrl ? this.externalUrl : '';
			
			if(this.xmlViewMode){
				xml.ViewMode = this.xmlViewMode;
			}
			
			return xml;	
		}
		
		private function setViewModeFromDefinition(vMode:String):void{
			if(vMode){
				this.xmlViewMode = vMode;
				
				if(vMode == "full"){
					this._instanceViewMode = Dashlet.VIEW_FULL;
				}else if(vMode == "headerless"){
					this._instanceViewMode = Dashlet.VIEW_HEADERLESS;
				}else if(vMode == "headerBorderless"){
					this._instanceViewMode = Dashlet.VIEW_HEADER_BORDER_LESS;
				}else{
					this.xmlViewMode = null;
				}
			}
		}
		
		private function isEnabled(xml:XML, name:String):Boolean{
			var children:XMLList = xml.child(name);
			if(children.length() > 0){
				return children[0].toString() != "false";
			}
			
			return false;
		}
		
		public function getState():XML{
			if (_content){
				var dashlet:XML = new XML('<dashlet/>');
				dashlet.appendChild(new XML('<path>' + _name + '</path>'));
				return _content.getState(dashlet);
			}
			return null;
		}
		
		public function setState(xml:XML):Boolean{
			if (_content){
				return _content.setState(xml);
			}
			return false;
		}
		
		public function getReportModificationTime():Number{
			if(_content){
				return _content.getReportModificationTime();
			}
			return Number.MAX_VALUE;
		}
		
		public function get path():String{
			return _name;			
		}
		
		public function toggleMode(mode:int):void{
			//No op if we've done this before
			if(_instanceViewMode == mode){
				return;
			}
			
			displayViewMode(mode);
		}
		
		public function displayViewMode(mode:int):void{
			switch(mode){
				case Dashlet.VIEW_HEADER_BORDER_LESS:
					hideHeader();
					removeHoverListeners();
					setStyle('borderStyle', 'none');
				break;
				case Dashlet.VIEW_HEADERLESS:
					hideHeader();
					addHoverListeners();
					setStyle('borderStyle', 'solid');
				break;
				case Dashlet.VIEW_FULL :
				default:
					removeHoverListeners();
					Util.show(controlsHolder);
					(this._content) && _content.showSelectors(true);
					setStyle('headerHeight', 16);
					setStyle('borderStyle', 'solid');
					viewMode = Dashlet.VIEW_FULL;		
			}
			
			_instanceViewMode = mode;
		}
		
		private function addHoverListeners():void{
			this.addEventListener(MouseEvent.ROLL_OVER, onMouseRollOver);	
			this.addEventListener(MouseEvent.ROLL_OUT, onMouseRollOut);
		}
		
		private function removeHoverListeners():void{
			this.removeEventListener(MouseEvent.ROLL_OVER, onMouseRollOver);	
			this.removeEventListener(MouseEvent.ROLL_OUT, onMouseRollOut);
		}
		
		private function onMouseRollOver(event:MouseEvent):void{
			if (this._content){
				Tracer.debug('onMouseRollOver, start showSelector ' + this, this);
				Util.show(this.controlsHolder);
				setStyle('headerHeight', 16);
				_content.showSelectors(true);
				Tracer.debug('onMouseRollOver, end showSelector' + this, this);
			}	
		}
		
		private function onMouseRollOut(event:MouseEvent):void{
			if (this._content && !_content.isSelectorInUse()){
				Tracer.debug('onMouseRollOut, start showSelector' + this, this);
				Util.hide(this.controlsHolder);
				setStyle('headerHeight', 0);
				_content.showSelectors(false);
				Tracer.debug('onMouseRollOut, end showSelector' + this, this);
			}	
		}
		
		private function hideHeader():void{
			Tracer.debug('hideHeader start', this);
			Util.hide(controlsHolder);
			(this._content) && _content.showSelectors(false);
			setStyle('headerHeight', 0);
			Tracer.debug('hideHeader end', this);
		}
	}
}