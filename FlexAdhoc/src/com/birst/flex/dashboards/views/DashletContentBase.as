package com.birst.flex.dashboards.views
{
	import com.anychart.AnyChartFlex;
	import com.birst.flex.adhoc.LabelMenuBarItem;
	import com.birst.flex.adhoc.PivotLandingBox;
	import com.birst.flex.adhoc.ReportRenderer;
	import com.birst.flex.adhoc.dialogs.DialogPopUpEvent;
	import com.birst.flex.core.AppEvent;
	import com.birst.flex.core.AppEventManager;
	import com.birst.flex.core.ColumnTextCell;
	import com.birst.flex.core.DimensionScaledCanvas;
	import com.birst.flex.core.ExportFile;
	import com.birst.flex.core.LinkNavigatorEvent;
	import com.birst.flex.core.MenuItemActions;
	import com.birst.flex.core.PagerCanvas;
	import com.birst.flex.core.ReportXml;
	import com.birst.flex.core.SaveCancelDialogBox;
	import com.birst.flex.core.Tracer;
	import com.birst.flex.core.Util;
	import com.birst.flex.core.maps.UMapperMaps;
	import com.birst.flex.dashboards.components.DashletsContainer;
	import com.birst.flex.dashboards.components.Page;
	import com.birst.flex.dashboards.events.DashletEvent;
	import com.birst.flex.webservice.WebServiceClient;
	import com.birst.flex.webservice.WebServiceResult;
	
	import flash.display.DisplayObject;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.ui.ContextMenu;
	
	import mx.containers.Canvas;
	import mx.containers.VBox;
	import mx.controls.MenuBar;
	import mx.controls.TextArea;
	import mx.core.Application;
	import mx.core.IFlexDisplayObject;
	import mx.core.ScrollPolicy;
	import mx.core.UIComponent;
	import mx.events.MenuEvent;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;
	import mx.rpc.events.ResultEvent;

	public class DashletContentBase extends VBox
	{
		[Bindable]
		public var _name:String = null;
		
		private var renderedReportWS:WebServiceClient;
		private var adhocReportWS:WebServiceClient;
		
		private var renderer:ReportRenderer;
		private var canvas:DimensionScaledCanvas;
		private var pager:PagerCanvas;
		private var selectorBar:GraphAttributeMeasuresSelectorBar;
		
		private var reportWidth:int;
		private var reportHeight:int;
		
		//Report modification time
		private var reportModificationTime:Number = Number.MAX_VALUE;
		
		[Bindable]
		public var autoFit:Boolean;
		
		private var isDimensionResize:Boolean;
		
		//Cached report xml
		private var cachedReportXML:XML;
		
		[Bindable]
		public var cacheReport:Boolean = false;
		
		[Bindable]
      	public var numberOfPages:int = 0;
      	
      	[Bindable]
		public var currentPage:int = 0;
		
		[Bindable]
		private var paginationInfo:String = "0 of 0";
		
		private var adhocReportXML:ReportXml;
		
		private var enableSelectorsCtrl:Boolean = false;
		private var enableViewSelector:Boolean = false;
		private var _applyAllPrompts:Boolean = false;
		private var _enableRectangleSelect:Boolean = true;
		private var _descriptionMode:Boolean = false;
		
		private var _scaledChartHeight:Number = -1;
		private var _scaledChartWidth:Number = -1;
		
		//Reference of prompts for paging
		private var _prompts:XML;
		private var _ignorePrompts:XML;
		
		//For audit info passed to webservice call
		public var pagePath:String = '';
		public var pageUuid:String = '';
		
		public function DashletContentBase(enableSelectorsCtrl:Boolean, enableViewSelector:Boolean, applyAllPrompts:Boolean, ignorePrompts:XML, descriptionMode:Boolean)
		{
			super();
			percentWidth = 100;
	    	percentHeight = 100;
	    	
	    	setStyle('verticalGap', 10);
	    	
	    	autoFit = isDimensionResize = false;	
	  
			this.enableSelectorsCtrl = enableSelectorsCtrl;
			this.enableViewSelector = enableViewSelector;
			this._applyAllPrompts = applyAllPrompts;
			this._ignorePrompts = ignorePrompts;
			this._descriptionMode = descriptionMode;

	    	addEventListener(LinkNavigatorEvent.DRILL_DOWN, onDrillDown);
	    	addEventListener(LinkNavigatorEvent.NAVIGATE, onNavigate);
	    	addEventListener(LinkNavigatorEvent.OLAP_DRILL_DOWN, onDrillDown);
	    	
	    	renderedReportWS = new WebServiceClient("Adhoc", "getDashboardRenderedReportV2", "DashletContentBase", 'constructor');
	    	renderedReportWS.cancellable = true;
			renderedReportWS.setResultListener(renderContentWithReportXML);
		}
		
		public function getPromptXML():XML
		{
			return _prompts;
		}
		
		override protected function createChildren():void{
			super.createChildren();
			
			if(!selectorBar && (enableViewSelector || enableSelectorsCtrl)){
				selectorBar = createGraphAttrsMeasuresSelectors(enableViewSelector, enableSelectorsCtrl); 
				addChild(selectorBar);
			}
			
			if(!canvas){
				canvas = new DimensionScaledCanvas();
	    		canvas.percentHeight = 100;
	    		canvas.percentWidth = 100;
	    		addChild(canvas);
	    		
	    		renderer = new ReportRenderer(canvas, false, _enableRectangleSelect);
	    		renderer.addMenuBarItemFunc = addMenuBarItemsReport;
	    		renderer.showColumnLabelMenu = false; // no menu items for column label headers
			}
			
			if(!pager){
				pager = createPager();
				addChild(pager);
			}
		}
		
		private function createGraphAttrsMeasuresSelectors(enableViewSelector:Boolean, enableColumnSelector:Boolean):GraphAttributeMeasuresSelectorBar{
			var selectorBar:GraphAttributeMeasuresSelectorBar = new GraphAttributeMeasuresSelectorBar();
			selectorBar.enableViewSelector = enableViewSelector;
			selectorBar.enableColumnSelector = enableColumnSelector;
			selectorBar.addEventListener(DashletEvent.SELECTOR_CHANGE, onSelectorChange);	
			return selectorBar;
		}
		
		private function onSelectorChange(event:DashletEvent):void{
			if (this.adhocReportXML) {
				if(String(event.data) == 'table'){
					unscale();
				}
				
				loadReportByReportXml();
			}
		}
		
		private function createPager():PagerCanvas{
			var pager:PagerCanvas = new PagerCanvas();
			pager.visible = pager.includeInLayout = false;
			pager.pageBackward = pageBackward;
			pager.pageForward = pageForward;
			pager.pageStart = pageStart;
			pager.pageEnd = pageEnd;
			pager.paginationInfo = paginationInfo;
			
			return pager;			
		}
		
		private function updatePaginationInfo(currPage:uint, numPages:uint):void{
			var currentPageDisplay:int = (numberOfPages > 0) ? (currentPage + 1) : 0;
			paginationInfo = currentPageDisplay + ' of ' + numberOfPages;
			pager.paginationInfo = paginationInfo;
		}
		
		public function getCurrentTime():Number{
			return (new Date()).getTime();	
		}
		
		private function pageStart():void{
			if (currentPage != 0)
				loadReport(_prompts, 0, getCurrentTime(), pagePath, pageUuid);
		}
		
		private function pageBackward():void{
			if (currentPage > 0)
			 	loadReport(_prompts, currentPage - 1, getCurrentTime(), pagePath, pageUuid);	
		}
		
		private function pageForward():void{
			if (((currentPage + 1) < numberOfPages) && (numberOfPages > 1)) 
				loadReport(_prompts, currentPage + 1, getCurrentTime(), pagePath, pageUuid);
		}
		
		private function pageEnd():void{
			if ((currentPage < numberOfPages) && ((currentPage + 1) != numberOfPages)) 
				loadReport(_prompts, numberOfPages - 1, getCurrentTime(), pagePath, pageUuid);
		}
		
		//Adds us for usage on the page level on bubbling upwards.
		private function onDrillDown(event:LinkNavigatorEvent):void{
			event.data.dashletContent = this;
		}
		
		//Issues a webservice call to create a new drill down filter, add it and update dashlets
		public function createDrillDownFilter(event:LinkNavigatorEvent):void{
			var data:Object = event.data;
			
			//Prompt exists on dashboard, we don't add a new filter
			var promptExists:Boolean = (data.hasOwnProperty('promptExists') && data.promptExists == true);
			
			var currentReportName:String = this._name;
			var index:int = currentReportName.lastIndexOf(".");
			if (index > 0) {
				currentReportName = currentReportName.substr(0, index);
			}
			if (String(data.reportPath).indexOf("/") == 0 && currentReportName.indexOf("/") != 0) {
				currentReportName = "/" + currentReportName;
			}
			if (data.reportPath != null  && currentReportName != data.reportPath) {
				this._name = data.reportPath;
				var wsvc:WebServiceClient;
				var ws:WebServiceClient = new WebServiceClient('Adhoc', 'openReport', "FileDialog", "saveButtonClickHandler");
				ws.setResultListener(function(result:ResultEvent):void{
					var rs:WebServiceResult = WebServiceResult.parse(result); 
					if (rs.isSuccess()){
						// can't use cacheXML since the result is differnt xml
	    				var resultXML:XML = rs.getResult()[0];
					    adhocReportXML = new ReportXml((resultXML["com.successmetricsinc.adhoc.AdhocReport"])[0]);
					    if (event.type == LinkNavigatorEvent.OLAP_DRILL_DOWN) {
							wsvc = new WebServiceClient("Adhoc", "olapDrillDown", '', '', true);
							wsvc.setResultListener(onGetFiltersForReport);
							wsvc.execute(adhocReportXML.toXMLString(), data.value);
					    }
					    else {
							wsvc = new WebServiceClient("Dashboard", "drillDown", '', '', promptExists);
							wsvc.setResultListener(onGetFiltersForReport);
							wsvc.execute(adhocReportXML.toXMLString(), data.columns, data.filters);
					    }
					}else{
						Util.localeAlertError("DC_failDrill", rs);
						return;
			}
				});
					ws.execute(data.reportPath + ".AdhocReport");
			}
			else {
			    if (event.type == LinkNavigatorEvent.OLAP_DRILL_DOWN) {
					wsvc = new WebServiceClient("Adhoc", "olapDrillDown", '', '', true);
					wsvc.setResultListener(onGetFiltersForReport);
					wsvc.execute(adhocReportXML.toXMLString(), data.value);
			    }
			    else {
					wsvc = new WebServiceClient("Dashboard", "drillDown", '', '', promptExists);
					wsvc.setResultListener(onGetFiltersForReport);
					wsvc.execute(adhocReportXML.toXMLString(), data.columns, data.filters);
			    }
			}
		}
		
		private function onNavigate(event:LinkNavigatorEvent):void {
			var data:Object = event.data;
			var filtersArr: Array = new Array(1);
			if (data.filters is Array)
			{
				filtersArr = data.filters;
			}
			else
			{ 
				// bypassed acorn, so need to handle decoding here.
				filtersArr = data.filters.split("&");
				for (var i:int = 0; i < filtersArr.length; i++) {
					// + is a special character that needs to be converted to spaces.  
					// everything else should be handled by decodeURIComponent
					var filter:Array = filtersArr[i].split("=");
					for (var j:int = 0; j < filter.length; j++) {
						var d:String = filter[j];
						d = Util.decodeURIComponentAndSpaces(d);
						filter[j] = d;
					}
					filtersArr[i] = filter;
				}
			}
			Application.application.openDashboardPage(data.dashName, data.pageName, filtersArr);
		}
		
		//Notify our parent container of the new filter prompts
		//Cache the new report xml
		private function onGetFiltersForReport(event:ResultEvent):void{
			var result:WebServiceResult = WebServiceResult.parse(event);
			if(!result.isSuccess()){
				Util.localeAlertError('DC_failDFilters', result);
				return;
			}
			
			var list:XMLList = result.getResult();
			cacheXmlReport(list);
			
			//Prompt Exists
			var promptExists:Boolean = Boolean(result.getCallerData());
			var prompt:XML = (list.root.Prompts.length() > 0) ? list.root.Prompts[0] : new XML(<Prompts></Prompts>);
			
			if(parent){
				parent.dispatchEvent(new DashletEvent(DashletEvent.DRILLDOWN_FILTERS, true, false, {prompt: prompt, promptExists: promptExists}));
			}
		}
		
		public function renderDashletDescription(description:String, filename:String, pageUuid:String, pagePath:String):void {
			if(_name != filename){
				//clear out our adhoc report cache for reloading of the other report
				adhocReportXML = null;
			}
			_name = filename;
			this.pageUuid = pageUuid;
			this.pagePath =  pagePath;
			var top:int = this.canvas.height / 2;
			
			var s:String = new String(description) + "\nThis report is in Description mode.  Please export the report or <u><font-color:blue>click here</font-color></u> to initiate rendering"; 
			var text:TextArea = new TextArea();
			text.htmlText = s;
			text.percentHeight = 50;
			text.percentWidth = 100;
			text.y = top;
			text.x = 0;
			text.editable = false;
			text.wordWrap = true;
			text.setStyle("verticalCenter", "0");
			text.setStyle("textAlign", "center");
			text.setStyle("fontSize", 14);
			text.setStyle("borderStyle", "none");
			text.alpha = 1;
			this.canvas.addChild(text);
			
			text.addEventListener(MouseEvent.CLICK, this.renderDescriptionReport);
			
			var ws:WebServiceClient = new WebServiceClient("Adhoc", "getReportColumnSelectors", "DashletContentBase", "renderDashletDescription");
			ws.setResultListener(gotColumnSelectors);
			ws.execute(filename);
		}
		
		private function gotColumnSelectors(event:ResultEvent):void {
			var rs:WebServiceResult = WebServiceResult.parse(event); 
			if (rs.isSuccess()){
				var resultXML:XML = rs.getResult()[0];
			    var selectorXml:XML = (resultXML["Selectors"])[0];
			    renderSelectors(selectorXml);
			}
		}
		
		private function renderDescriptionReport(event:MouseEvent):void {
            var ws:WebServiceClient = new WebServiceClient("Adhoc", 
                                                           "openReport", 
                                                           "DashletContentBase", 
                                                           "renderDescriptionReport");
            ws.setResultListener(this.openReport);
            ws.execute(_name);
		}
		
		public function renderReportByPath(filename:String, prompts:XML, execTime:Number, pagePath:String, pageUuid:String, noRender:Boolean = false, isDashletEdit:Boolean = false):void{
			if(_name != filename){
				//clear out our adhoc report cache for reloading of the other report
				adhocReportXML = null;
			}
			_name = filename;
			
			//noRender is true when the dashboard is set to 'auto apply the default bookmark' when the dashboard page is
			//first rendered. We render by path to retrieve the report xml because the report xml, prompts are set from the bookmarks
			if(!noRender){
				loadReport(prompts, 0, execTime, pagePath, pageUuid, isDashletEdit);
			}	
		}
		
		/**
		 * We call getDashboardRenderedReport() which gets us the report xml representation which we cache.
		 * Subsequent calls use getDashboardRenderedReportFromXML() which passes the report xml back for rendering.
		 */
		public function loadReport(prompts:XML, page:int, execTime:Number, pagePath:String, pageUuid:String, isDashletEdit:Boolean = false):void{
			if(this._name.indexOf(".dashlet") > 0){
				return;
			}
			
			_prompts = new XML("<Prompts></Prompts>");
			var promptList:XMLList = prompts ? prompts.children() : new XMLList();
			if (_ignorePrompts != null) {
				for (var i:uint = 0; i < promptList.length(); i++) {
					var prompt:XML = promptList[i];
					var label:String = prompt.VisibleName;
					if (_ignorePrompts.child("item").length() > 0) {
						var b:Boolean = false;
						for each (var c:XML in _ignorePrompts.child("item")) {
							if (c.toString() == label) {
								b = true;
								break;
							}
						}
						if (b == true)
							continue;
					}
						
					_prompts.appendChild(prompt);
				}
			}
			else
				_prompts = prompts;
				
			this.pagePath =  pagePath;
			this.pageUuid = pageUuid;
			var guid:String = execTime + "_" + pageUuid;
			var promptStr:String = (_prompts) ? _prompts.toXMLString() : "<Prompts/>";
			if(adhocReportXML){
				loadReportByReportXml(promptStr, page, pagePath, guid, isDashletEdit);
			}else{
				renderedReportWS && renderedReportWS.execute(_name, promptStr, page, applyAllPrompts, pagePath, guid, isDashletEdit);			
			}
		}
		
		public function openReport(event:ResultEvent):void {
			var rs:WebServiceResult = WebServiceResult.parse(event); 
			if (rs.isSuccess()){
				var resultXML:XML = rs.getResult()[0];
			    this.adhocReportXML = new ReportXml((resultXML["com.successmetricsinc.adhoc.AdhocReport"])[0]);
			    delete this.adhocReportXML.xml.AsynchReportGeneration;
			    this.adhocReportXML.xml.AsynchReportGeneration = 'true';
			    var promptStr:String = null;
			    var doc:DashletsContainer = null;
			    var o:Object = this.document;
			    if (o && o instanceof DashletsContainer)
			    	doc = (o as DashletsContainer);
			    if (doc != null) {
			    	var p:Page = null;
			    	o = doc.parentDocument;
			    	if (o && o instanceof Page) {
			    		p = o as Page;
			    		var x:XML = p.promptDisplay.xml;
			    		if (x) {
			    			promptStr = x.toXMLString();
			    		}
			    	}
			    }
			    loadReportByReportXml(promptStr); 
			}else{
				Util.localeAlertError("DC_failRender", rs);
				return;
			}
		}
		
		public function loadReportByReportXml(promptStr:String = null, page:int = 0, pagePath:String = null, guid:String = null, isDashletEdit:Boolean = false):void{
			if(!promptStr){
				promptStr = (_prompts) ? _prompts.toXMLString() :  "<Prompts/>";
			}
			if(!pagePath){
				pagePath = this.pagePath;
			}
			if(!guid){
				guid = getCurrentTime() + "_" + this.pageUuid;
			}
			
			if(!adhocReportWS){
				adhocReportWS = new WebServiceClient("Adhoc", "getDashboardRenderedReportFromXMLV2", "DashletContentBase", 'constructor');
				adhocReportWS.cancellable = true;
				adhocReportWS.setResultListener(renderContentOnly);
			}

			var selectorStr:String = this.selectors;
			adhocReportWS.execute(adhocReportXML.toXMLString(), promptStr, page, selectorStr, applyAllPrompts, pagePath, guid, isDashletEdit);
		}
		
		public function isSelectorInUse():Boolean{
			if (selectorBar){
				return selectorBar.inUse;
			}	
			
			return false;
		}
		
		public function get selectors(): String {
			var selectors:String = '<Selectors/>';
			if(selectorBar){
				var xml:XML = selectorBar.selectors;
				
				if (xml) {
					var hasChart:Boolean = false;
					if (xml.IncludeChartSelector.toString() == 'true') {
						var chartType:String = xml.ChartType.toString();
						var includeTable:Boolean = chartType == '';
						var conf:Array = [];
						conf['IncludeTable'] = includeTable.toString();
						adhocReportXML.setReportPropertiesConfig(conf);
						adhocReportXML.drawChart((!includeTable) ? chartType : null);
					}
					selectors = xml.toXMLString();
				}	
			}
			return selectors;
		}
		
		public function get applyAllPrompts(): Boolean {
			return _applyAllPrompts;
		}

		public function set rectangleSelect(enableSelect:Boolean):void{
			if(renderer){
				renderer.rectangleSelect = enableSelect;	
			}
			_enableRectangleSelect = enableSelect;
		}
		
		public function getState(dashlet:XML):XML{
			if(selectorBar && (enableViewSelector || enableSelectorsCtrl)){	
				dashlet.appendChild(selectorBar.getSelectedValues());	
			}
			if(adhocReportXML){
				dashlet.appendChild(adhocReportXML.xml);
			}
			return dashlet;
		}
		
		public function setState(xml:XML):Boolean{
			var hits:XMLList = xml.dashlet.(path == _name);
			if (hits.length() <= 0){
				return false;
			}
			
			var xml:XML = hits[0];
			
			//report xml
			if(adhocReportXML && (xml.child(ReportXml.ROOT).length() > 0)){
				adhocReportXML.xml = xml[ReportXml.ROOT][0];
			}
			
			//do selectors
			if(selectorBar && (enableViewSelector || enableSelectorsCtrl) && xml.selectors.length() > 0){
				selectorBar.setSelectedValues(xml.selectors[0]);
			}
			
			return true;
		}
			
		public function refreshPivotControl():void{
			var pivotBox:PivotLandingBox = Application.application.dashletPivotBox;
			if (pivotBox && pivotBox.isPopUp){
				var conf:DashletAdhocConfig = DashletAdhocConfig.instance();
				if(conf.dashletContent == this){
					conf.prompts = _prompts;
					conf.setReportXml(adhocReportXML);
					pivotBox.config = conf;
					pivotBox.populatePanelsWithColumns();
					
					var appEvent:AppEvent = new AppEvent(AppEvent.EVT_CONFIG_CHANGED, "DashletContentBase", "refreshPivotControl", this);
					AppEventManager.fire(appEvent);
				}
			}
		}
		
		public static function addMenuBarItemsReport(mb:MenuBar, col:int, lab:int, label:String, width:String, addMenuItems:Boolean):void {
			var itemList:Array = [ 	{label: label} ];
			mb.dataProvider = itemList;
			mb.validateProperties();
			
			//We have to explicitly get the menubar item to change the text in the menubar
			var mbi:LabelMenuBarItem = mb.menuBarItems[0] as LabelMenuBarItem;
			if (mbi && width){
				mbi.width = Number(width);
			}
		}
		
		private static function getLocLabel(labelKey:String):String {
			return ResourceManager.getInstance().getString('dashboard', labelKey);
		}
		
		public static function addMenuBarItemsPivot(mb:MenuBar, col:int, lab:int, label:String, width:String, addMenuItems:Boolean):void {
		 	var conf:DashletAdhocConfig = DashletAdhocConfig.instance();
			var sortInfo:Object;
			if (conf){
				var sortedCol:Array = conf.getSortedColumns();
				for(var i:int = 0; i < sortedCol.length; i++){
					var sCol:Object = sortedCol[i];
					if (sCol.id == col){
						sortInfo = sCol;
						break;
					}
				}
			}
			var itemList:Array = [ {label: label,
			 						children: [ 
			 							{label: getLocLabel('DL_sort'),
								    	 children: [
								    	 	{ label: getLocLabel('DL_ascend'),  fn: sortAction, type: "check", toggled: (sortInfo && sortInfo.order && sortInfo.order.value == "Ascending"),  dir: 'Ascending', col: col},
								    	 	{ label: getLocLabel('DL_descend'), fn: sortAction, type: "check", toggled: (sortInfo && sortInfo.order && sortInfo.order.value  == "Descending"), dir: 'Descending', col: col},
								    	 	{ label: getLocLabel('DL_unsort'),   fn: sortAction, type: "check", toggled: (sortInfo && sortInfo.order && sortInfo.order.value  == "Unsorted"),   dir: 'Unsorted', col: col }
								    	 ]
								    	}
								    ]
								}];
			mb.dataProvider = itemList;
			mb.addEventListener(MenuEvent.ITEM_CLICK, MenuItemActions.menuItemClickHandler);
		}
		
		public static function sortAction(menuItem:Object):void{
			var conf:DashletAdhocConfig = DashletAdhocConfig.instance();
			if (conf){
				conf.setColumnSortValue(menuItem.col, menuItem.dir);
				conf.dashletContent.refreshPivotControl();
				conf.dashletContent.reRenderContent();
			}	
		}
		
		public function showPivotTable():void{
			//Set our config
			var conf:DashletAdhocConfig = DashletAdhocConfig.instance();
			conf.prompts = _prompts;
			conf.setReportXml(adhocReportXML);
			conf.dashletContent = this;
			
			//Create the pivot pop up
			var pivotBox:PivotLandingBox = Application.application.dashletPivotBox;
			
			if (!pivotBox){
				pivotBox = Application.application.dashletPivotBox = PopUpManager.createPopUp(this, PivotLandingBox) as PivotLandingBox;
				pivotBox.setStyle('headerColors', this.getStyle('headerColors'));
				pivotBox.setStyle('titleStyleName', 'dashletTitle');
				pivotBox.addMenuBarItemsFunc = addMenuBarItemsPivot;
				PopUpManager.bringToFront(pivotBox as IFlexDisplayObject);
			}
			
			pivotBox.config = conf;
			
			//We re-display if the window was closed
			if (!pivotBox.isPopUp){
				PopUpManager.addPopUp(pivotBox as IFlexDisplayObject, this);			
			}
			
			//Move to bottom right
			var bottomLoc:Point = this.localToGlobal(new Point(Math.max(this.width - pivotBox.width, 0), Math.max(this.height - pivotBox.height, 0)));
			pivotBox.move(bottomLoc.x, bottomLoc.y);
			
			//We tell the pop up to do its thing..
			pivotBox.dispatchEvent(new DialogPopUpEvent(SaveCancelDialogBox.POPPED_UP, null));
		}
		
		public function doExport(func:Function):void {
			if (this.adhocReportXML == null) {
				var promptStr:String = (_prompts) ? _prompts.toXMLString() : "<Prompts/>";
				var guid:String = "0_" + pageUuid;
				var wsClient:WebServiceClient = new WebServiceClient("Adhoc", "openReport", "DashletContentBase", 'doExport', func);
				wsClient.setResultListener(finishExport);
				wsClient.execute(_name);
			}
			else {
				func.call(this);
			}
		}
		
		private function finishExport(event:ResultEvent):void {
			var result:WebServiceResult = WebServiceResult.parse(event);

			if (!result.isSuccess()){
				Util.localeAlertError("DC_failRender", result);
				return;
			}
			
			var list:XMLList = result.getResult();
			cacheXmlReport(list);

			(result.getCallerData() as Function).call(this);
		}
		
		public function generatePdf():void{
			if (this.adhocReportXML == null) {
				doExport(generatePdf);
				return;
			}
			Application.application.trackEvent("generatePdf");
			var report:Object = adhocReportXML ? adhocReportXML.xml : _name;
			if(report != null){
				var promptStr:String = (_prompts) ? _prompts.toXMLString() : "<Prompts/>";
				var selectorStr:String = (selectorBar && selectorBar.selectors) ? selectorBar.selectors.toXMLString() : null;
				ExportFile.exportPDF(report, promptStr, selectorStr, applyAllPrompts);
			}
		}
		
		public function generatePpt():void {
			if (this.adhocReportXML == null) {
				doExport(generatePpt);
				return;
			}
			Application.application.trackEvent("generatePpt");
			var report:Object = adhocReportXML ? adhocReportXML.xml : _name;
			if(report != null){
				var promptStr:String = (_prompts) ? _prompts.toXMLString() : "<Prompts/>";
				var selectorStr:String = (selectorBar && selectorBar.selectors) ? selectorBar.selectors.toXMLString() : null;
				ExportFile.exportPPT(report, promptStr, selectorStr, applyAllPrompts);
			}
		}
		public function generateCsv():void{
			if (this.adhocReportXML == null) {
				doExport(generateCsv);
				return;
			}
			Application.application.trackEvent("generateCsv");
			var report:Object = adhocReportXML ? adhocReportXML.xml : _name;
			if(report != null){
				var promptStr:String = (_prompts) ? _prompts.toXMLString() : "<Prompts/>";
				var selectorStr:String = (selectorBar && selectorBar.selectors) ? selectorBar.selectors.toXMLString() : null;
				ExportFile.exportCSV(report, promptStr, selectorStr, applyAllPrompts);
			}
		}
		
		public function generateXls():void{
			if (this.adhocReportXML == null) {
				doExport(generateXls);
				return;
			}
			Application.application.trackEvent("generateXls");
			var report:Object = adhocReportXML ? adhocReportXML.xml : _name;
			if(report != null){
				var promptStr:String = (_prompts) ? _prompts.toXMLString() : "<Prompts/>";
				var selectorStr:String = (selectorBar && selectorBar.selectors) ? selectorBar.selectors.toXMLString() : null;
				ExportFile.exportXLS(report, promptStr, selectorStr, applyAllPrompts);
			}
		}
		protected function renderContentOnly(event:ResultEvent):void{
			var result:WebServiceResult = WebServiceResult.parse(event);

			if (!result.isSuccess()){
				Util.localeAlertError("DC_failRender", result);
				return;
			}
	
			var list:XMLList = result.getResult();
			if(list.root.reportRenderer.length() > 0){
			 	renderReport(list.root.reportRenderer.data.report[0]);
				updatePageInfo(list.root.reportRenderer);
			}
			else if (list.reportRenderer.length() > 0) {
				renderReport(list.reportRenderer.data.report[0]);
				updatePageInfo(list.reportRenderer);
			}
			
			refreshPivotControl();	
			dispatchEvent(new DashletEvent(DashletEvent.RENDERING_CONTENT, true));	
		}
		
		//First time or refresh, we get a brand new report xml and render xml by giving the report file path
		public function renderContentWithReportXML(event:ResultEvent):void{
			var result:WebServiceResult = WebServiceResult.parse(event);

			if (!result.isSuccess()){
				Util.localeAlertError("DC_failRender", result);
				return;
			}
	
			var list:XMLList = result.getResult();
			if(list.root.reportRenderer.length() > 0){
			 	renderReport(list.root.reportRenderer.data.report[0]);
				updatePageInfo(list.root.reportRenderer);
				if(list.root.Selectors.length() > 0){
					renderSelectors(list.root.Selectors[0]);
				}
			}
			else if (list.reportRenderer.length() > 0) {
			 	renderReport(list.reportRenderer.data.report[0]);
				updatePageInfo(list.reportRenderer);
				if(list.Selectors.length() > 0){
					renderSelectors(list.Selectors[0]);
				}
			}	
			
			//Report file last modification time
			if(list.root.lastModified.length() > 0){
				reportModificationTime = Number(list.root.lastModified[0].toString());
			}
			
			//cache reportxml for later use
			cacheXmlReport(list);
			refreshPivotControl();	
			
			dispatchEvent(new DashletEvent(DashletEvent.RENDERING_CONTENT, true));	
		}
		
		public function getReportModificationTime():Number{
			return reportModificationTime;
		}
		
		//Update paging information
		private function updatePageInfo(xml:XMLList):void{
			numberOfPages = (xml.numberOfPages.length() > 0) ? int(xml.numberOfPages.toString()) : 0;
			currentPage = (xml.page.length() > 0) ? int(xml.page.toString()) : 0;
			
			if(numberOfPages > 1){
				Util.show(pager);
				updatePaginationInfo(currentPage, numberOfPages);
			}else{  
				Util.hide(pager);
			}	
		}
		
		private function cacheXmlReport(list:XMLList):void{
			var reportXML:XMLList = list.descendants("com.successmetricsinc.adhoc.AdhocReport");
			if(reportXML.length() > 0){
				adhocReportXML = new ReportXml(reportXML[0]);
				if (this._descriptionMode && Application.application.isEnableAsyncReportGeneration) {
					delete adhocReportXML.xml.AsynchReportGeneration;
					adhocReportXML.xml.AsynchReportGeneration = true;
				}
				/*
				if(selectorBar){
					var chartType:String = adhocReportXML.getFirstChartType();
					selectorBar.chartType = chartType == "" ? 'table' : chartType;
				}*/
			}
		}
		
		// called in async processing to determine if the dashlet has already been rendered
		public function isAlreadyRendered():Boolean {
			return (adhocReportXML != null)
		}
		
		protected function renderReport(report:XML):void{
			_scaledChartHeight = _scaledChartHeight = -1;
			this.reportHeight = report.@height;
			this.reportWidth = report.@width;
			renderer.render(report, (this.contextMenu as ContextMenu), this);

			canvas.doScaling = false;
			
			if(autoFit && !renderer.hasImage){
			   	if(!scaleAutoFit()){
			   		unscale();
			   	}
			}else{
				this.unscale();
			}
			   
			if(cacheReport){
			   	cachedReportXML = report;
			}
		}
		
		protected function renderSelectors(selectors:XML):void{
			if (selectorBar){
				selectorBar.selectors = selectors;
			}
		}
		
		
		public function reRenderContent(noCache:Boolean = false):void{
			if (noCache){
				adhocReportXML = null;
				cachedReportXML = null;
			}
			
			if (cacheReport && cachedReportXML){
				renderReport(cachedReportXML);	
			}else{
				//Bug 9964
				var prompts:XML = (_prompts) ? _prompts : <Prompts/>;
				loadReport(prompts, 0, getCurrentTime(), pagePath, pageUuid);
			}
		}
		
		public function getContentWidth():int{
			Tracer.debug('getCanvasWidth()  ' + this + ' = ' + this.width + ' renderer.width = ' + renderer.width + ' canvas.width = ' + canvas.width, this);
			return (renderer.width > this.width) ? renderer.width : this.width;
		}
		
		public function getContentHeight():int{
			Tracer.debug('getContentHeight()  ' + this + ' = ' + this.height + ' renderer.height = ' + renderer.height + ' canvas.height = ' + canvas.height, this);
			return (renderer.height > this.height) ? renderer.height : this.height;
		}
		
		public function fitToDimensions():void{
			isDimensionResize = true;
			if (this.reportHeight > 0 && this.reportWidth > 0){
				this.canvas.horizontalScrollPolicy = ScrollPolicy.OFF;
				this.canvas.verticalScrollPolicy = ScrollPolicy.OFF
				this.canvas.scaleX = this.canvas.width / this.reportWidth;
				this.canvas.scaleY = this.canvas.height / this.reportHeight;
			}		
		}
		
		public function unscale():void{
			this.canvas.horizontalScrollPolicy = ScrollPolicy.AUTO;
			this.canvas.verticalScrollPolicy = ScrollPolicy.AUTO;
			this.canvas.scaleX = this.canvas.scaleY = 1;
		}
		
		public function showSelectors(show:Boolean):void{
			if(selectorBar){
				show ? Util.show(selectorBar) : Util.hide(selectorBar);
				Tracer.debug('showSelectors: ' + show, this);
			}
		}
		
		public function revertToUnscale():void{
			unscale();
			
			var kids:Array = canvas.getChildren();
			var theSingleKid:AnyChartFlex;

			for(var i:int = 0; i < kids.length; i++){
				var kid:DisplayObject = kids[i];
				if(kid is AnyChartFlex){
					if(!theSingleKid)
						theSingleKid = kid as AnyChartFlex;
					else{
						theSingleKid = null;
						break;
					}
				}
			}
			
			if(theSingleKid && (_scaledChartHeight > 0) && (_scaledChartWidth > 0)){
				theSingleKid.height = this._scaledChartHeight;
				theSingleKid.width = this._scaledChartWidth;
				canvas.doScaling = false;
				canvas.invalidateDisplayList();	
			}		
		}
		
		public function scaleAutoFit():Boolean{
			isDimensionResize = false;
			
			var kids:Array = canvas.getChildren();
			
			if(kids.length == 1){
				if(kids[0] is AnyChartFlex || kids[0] is UMapperMaps){
					scaleAnyChartFlex(kids[0] as UIComponent, 100, 100);
					return true;
				}else if (kids[0] is Canvas){ 
					//Check for Trellis Charts (normally embedded in 2 frames (canvas)), then all anycharts
					var cKids:Array = Canvas(kids[0]).getChildren();
					if(cKids.length == 1 && cKids[0] is Canvas){
						var cKid:Canvas = Canvas(cKids[0]);
						var trellisChildren:Array = cKid.getChildren();
						for(var i:int = 0; i < trellisChildren.length; i++){
							if(!trellisChildren[i] is AnyChartFlex){
								return false;
							}
						}
						//We need to steal the charts from the 2 embedded frames to the current
						//dimension scaled canvas because the dimension scaled canvas only scales its immediate children
						if(trellisChildren.length > 0){
							canvas.removeAllChildren();
							canvas.resetPositionsCache();
							for(var y:int = 0; y < trellisChildren.length; y++){
								canvas.addChild(trellisChildren[y]);
							}
							scaleCanvas();
							return true;
						}
					}
				}
			}else if (kids.length == 3){
				if(kids[0] is ColumnTextCell && kids[1] is ColumnTextCell && kids[2] is AnyChartFlex){
					scaleCanvas();
					return true;
				}
			}
			return false;
		}
		
		private function scaleCanvas():void{
			canvas.contentMaxHeight = renderer.height;
			canvas.contentMaxWidth = renderer.width;
			canvas.doScaling = true;
			canvas.invalidateDisplayList();
		}
		
		private function scaleAnyChartFlex(chart:UIComponent, percentHeight:Number, percentWidth:Number):void{
			this.isDimensionResize = false;
				
			this.canvas.horizontalScrollPolicy = ScrollPolicy.OFF;
			this.canvas.verticalScrollPolicy = ScrollPolicy.OFF
				
			//Saved for if we need to unscale later
			_scaledChartHeight = chart.height;
			_scaledChartWidth = chart.width;
				
			chart.percentHeight = percentHeight;
			chart.percentWidth = percentWidth;
		}
	
		
		public function minimize():void{
			if(isDimensionResize){
				fitToDimensions();
			}	
		}
		
		public function maximizie():void{
			if(isDimensionResize){
				this.unscale();
			}	
		}
		
		private function scaleAllPercentages():void{
			var kids:Array = canvas.getChildren();
			for(var i:int = 0; i < kids.length; i++){
				var kid:UIComponent = kids[i] as UIComponent;
				
				var scaleX:Number = canvas.width / kid.width; 
				var scaleY:Number = canvas.height / kid.height;
			
				kid.percentWidth = scaleX * 100;
				kid.percentHeight = scaleY * 100;
				
				/*
				var scaleX:int = kid.width / canvas.width;
				if(scaleX > 1){
					scaleX = kid.width / (canvas.width * scaleX);
				}
				
				var scaleY:int = kid.height / canvas.height;
				if(scaleY > 1){
					scaleY = kid.height / ( canvas.height * scaleY);
				}
				
				kid.percentWidth = scaleX * 100;
				kid.percentHeight = scaleY * 100;
				*/
				
				Tracer.debug('scaled ' + kid + ' to ' + kid.percentWidth + 'x' + kid.percentHeight + ' percentages', this); 
			}
		}
		
		public function getColumnXML(reportIndex:String) :XMLList {
			if (!adhocReportXML)
				return null;
				
			return adhocReportXML.getColumnXML(reportIndex);
		}
		
		public function getSortColumns():Array {
			if (!adhocReportXML)
				return null;
				
			return adhocReportXML.getSortedColumns();
		}
		
		public function setSortedColumns(columns:Array) : void {
			if (adhocReportXML) {
				adhocReportXML.setSortedColumns(columns);
				reRenderContent();
			}
		}	

	}
}