package com.birst.flex.dashboards.events
{
	import flash.events.Event;

	public class DashboardEvent extends Event
	{
		public static var NO_PAGES:String = "noPages";
		public static var PAGE_NAME_CHANGE:String = "pageNameChange";
		public static var TAB_HILITE:String = "bttnHilite";
		public static var PAGE_HISTORY:String = "pgHis";
		public static var PAGE_RETRIEVED:String = "pgRetrieved";
		
		public var data:Object;
				
		public function DashboardEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
		
	}
}