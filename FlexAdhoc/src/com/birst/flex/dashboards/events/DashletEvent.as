package com.birst.flex.dashboards.events
{
	import flash.events.Event;

	public class DashletEvent extends Event
	{
		public static var MAXIMIZE:String = "maximize";
		public static var MINIMIZE:String = "minimize";
		public static var EDIT:String = "edit";
		public static var EDIT_REPORT:String = "editReport";
		public static var RENDERING_CONTENT:String = "renderContent";
		public static var DRILLDOWN_FILTERS:String = "drillDownFilters";
		public static var GET_REPORT_PROMPTS:String = "getPrompts";
		public static var REMOVE_REPORT_PROMPTS:String = "removePrompts";
		public static var SELECTOR_CHANGE:String = "selectorChange";
		public static var SELF_SCHEDULE_NEW:String = "newSelfSchedule";
		
		public var data:Object;
		
		public function DashletEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false, data:Object = null)
		{
			super(type, bubbles, cancelable);
			this.data = data;
		}
		
	}
}