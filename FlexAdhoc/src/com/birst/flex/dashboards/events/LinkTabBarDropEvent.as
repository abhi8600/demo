package com.birst.flex.dashboards.events
{
	import com.birst.flex.dashboards.components.DashboardTab;
	
	import flash.events.Event;
	
	import mx.core.DragSource;

	public class LinkTabBarDropEvent extends Event
	{
		public var targetIndex:int;
		public var sourceIndex:int;
		public var targetDashboard:DashboardTab;
		public var sourceDashboard:DashboardTab;
		public var dragSource:DragSource;
		
		public static var DRAGGING:String = "dragging";
		public static var DROP:String = "drop";
		public static var DROP_PAGE:String = "dropPage";
		
		public function LinkTabBarDropEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false, 
											target:int = 0, source:int = 0, targetDash:DashboardTab = null, sourceDash:DashboardTab = null)
		{
			super(type, bubbles, cancelable);
			targetIndex = target;
			sourceIndex = source;
			targetDashboard = targetDash;
			sourceDashboard = sourceDash;
		}
		
	}
}