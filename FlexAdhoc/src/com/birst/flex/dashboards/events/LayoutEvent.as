package com.birst.flex.dashboards.events
{
	import com.birst.flex.dashboards.layout.LayoutBase;
	
	import flash.events.Event;

	public class LayoutEvent extends Event
	{
		public static var MAXIMIZE:String = "maximize";
		public static var MINIMIZE:String = "minimize";
		public static var NEW_CHILD:String = "newChild";
		public static var ROLL_OVER:String = "rollOver";
		public static var ROLL_OUT:String = "rollOut";
		public static var EDIT:String = "edit";
		
		//testing only
		public static var TOGGLE_GAPS:String = "toggleGaps";
			
		public var child:LayoutBase;
		
		public function LayoutEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
		
	}
}