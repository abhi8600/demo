package com.birst.flex.dashboards.events
{
	import flash.events.Event;

	public class PromptControlEvent extends Event
	{
		public static var COMBOBOX_EXPANDED:String = "ComboBoxExpanded";
		public static var VARIABLE_UPDATED:String = "VariableUpdate";
		public static var ADDED_FILTERS:String = "addedFilters";
		public static var UPDATE_PARENT_PROMPTS:String = "updateParentPrompts";
		public static var PARENT_PROMPT_VALUE_CHANGED:String = "parentPromptValueChanged";
		public static var PARSED_XML:String = "parsedXml";
		public static var PARAMETER_NAME_CHANGED:String = "parameterNameChanged";
		public static var ADDED_PROMPT:String = "addPrompt";
		public static var REMOVE_PROMPT:String = "removePrompt";
		public static var SUMMARY_UPDATE:String = "updateSummary";
		public static var PROMPT_NAME_CHANGED:String = "nameChanged";
		public static var MINIMIZE:String = "minimize";
		public static var IN_DASHLET_SELECTED:String = "selected"; 
		public static var USER_CLICKED_PROMPT:String = "userClickPrompt";
		public static var PROMPT_TO_CHANGE:String = "pC"; 
		public static var HIDE_PROMPTS_SECTION:String = "aPID";
		public static var DATE_CHANGED:String = "dateChanged";
		
		public var data:Object;
		public function PromptControlEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
		
	}
}