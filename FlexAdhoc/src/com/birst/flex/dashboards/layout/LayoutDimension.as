package com.birst.flex.dashboards.layout
{
	import com.birst.flex.core.Tracer;
	
	public class LayoutDimension
	{
		public var width:Number;
		public var height:Number;
		
		public static var SCROLLBAR_OFFSET:int = 16;
		
		public function LayoutDimension(w:Number = 0, h:Number = 0){
			width = w;
			height = h;
		}

		public function extrapolate(layout:LayoutBase, totalLayoutWidth:int, totalLayoutHeight:int):void{
			var reportWidth:int =  layout.getDashlet().getReportWidthDiff();
			Tracer.debug('computeExtrapolatedDimension() for layout: ' + layout + ' of dimension ' + layout.width + 'x' + layout.height, this);
			if (reportWidth > 0){
				var extrapolatedWidth:int = (layout.width + reportWidth) * (totalLayoutWidth / layout.width) + SCROLLBAR_OFFSET;
				Tracer.debug('reportWidth = ' + reportWidth + ' extrapolatedWidth = ' + extrapolatedWidth + ' width = ' + width, this);
				if (extrapolatedWidth > width){
					width = extrapolatedWidth;
				}
			}
			var reportHeight:int =  layout.getDashlet().getReportHeightDiff();
			if (reportHeight > 0){
				var extrapolatedHeight:int = (layout.height + reportHeight) * (totalLayoutHeight / layout.height) + SCROLLBAR_OFFSET;
				Tracer.debug('reportHeight = ' + reportHeight + ' extrapolatedHeight = ' + extrapolatedHeight + ' height = ' + height, this);
				if (extrapolatedHeight > height){
					height = extrapolatedHeight;
				}
			}							    	
		}
		
	}
}