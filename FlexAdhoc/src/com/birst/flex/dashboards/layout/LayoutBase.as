package com.birst.flex.dashboards.layout
{
	import com.birst.flex.dashboards.views.Dashlet;
	
	import mx.core.IContainer;
	import mx.core.IUIComponent;
	
	public interface LayoutBase extends IUIComponent, IContainer
	{
		function getDashlet(isExternalIframe:Boolean = false):Dashlet;
		function createDashlet(xml:XML = null, noRender:Boolean = false):void;
		function setDashlet(dashlet:Dashlet):void;
		function hasDashlet():Boolean;
		function removeDashlet():Dashlet;
		function getLayoutKids():Array;
		function reAddDashlet():void;
		function addDashletEventListeners():void;
		function toXML(parent:XML = null):XML;
		function fromXML(xml:XML, existingDashlets:Array = null, dashletPrompts:Array = null, noRender:Boolean = false):void;
		function removeLayoutKids():void;
		function set scaleType(scale:String):void;
		function set isMaster(isRoot:Boolean):void;
		function get isMaster():Boolean;
		function getReportWidthDiff():int;
	}
}