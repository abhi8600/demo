package com.birst.flex.dashboards.layout
{
	import mx.containers.BoxDirection;
	import mx.containers.Canvas;
	import mx.controls.Label;
	import mx.core.ScrollPolicy;
	import mx.events.FlexEvent;

	public class LayoutBackgroundCanvas extends Canvas
	{
		
		private var middleText:String;
		
		private var _label:Label;
		private var _direction:String;
	
		public function LayoutBackgroundCanvas()
		{
			super();
			
			horizontalScrollPolicy = mx.core.ScrollPolicy.OFF;
			verticalScrollPolicy = mx.core.ScrollPolicy.OFF;
			
			this.setStyle('backgroundColor', 0xffffff);
			this.percentHeight = this.percentWidth = 100;
			
			//this.addEventListener(FlexEvent.CREATION_COMPLETE, onCreationComplete);
		}
		
		private function onCreationComplete(event:FlexEvent):void{
			//debugging only
			if (parent){
				_label.text = _label.text + " #" + (parent as LayoutMovable).countId;
			}
		}
		
		public function set direction (direction:String):void{
			middleText	= (direction == BoxDirection.HORIZONTAL) ? "Horizontal" : "Vertical";
			_label && (_label.text = middleText);
		}	
		
		override protected function createChildren():void{
			super.createChildren();
			
			if (!_label){
				_label = new Label();
				_label.setStyle('fontSize', 10);
				_label.setStyle('fontFamily', 'Arial');
				_label.text = (this.middleText) ? this.middleText: "";
				addChild(_label);
			}	
		}
		
		override protected function updateDisplayList(unscaledWidth:Number, unscaledHeight:Number):void{
			super.updateDisplayList(unscaledWidth, unscaledHeight);
			
			if (_label){
				var middleX:int = (unscaledWidth / 2 );
				var middleY:int = (unscaledHeight/ 2);
				_label.move(middleX, middleY);
			}
		}
	}
}