package com.birst.flex.dashboards.layout
{
	import com.birst.flex.dashboards.components.Page;
	import com.birst.flex.dashboards.components.PromptControl;
	import com.birst.flex.dashboards.events.LayoutEvent;
	import com.birst.flex.dashboards.views.Dashlet;
	
	import flash.display.DisplayObject;
	
	import mx.containers.Box;
	
	public class LayoutFixed extends Box implements LayoutBase
	{
		private var _operation:LayoutOperations;
		private var _dashlet:Dashlet;
		private var _prompt:PromptControl;
		private var _layouts:Array;
		private var _scaleType:String;
		private var _isMaster:Boolean;
		private var _prompts:XML;
		private var _pagePath:String = '';
		private var _pageUuid:String = '';
		private var _execTime:Number = 0;
		private var _rectangleSelect:Boolean;
		
		public function LayoutFixed()
		{
			super();
			
			_isMaster = false;
			_layouts = [];
			_operation = new LayoutOperations(this);
		}
		
		public function getDashlet(isExternalIframe:Boolean = false):Dashlet{
			if (!_dashlet){
				createDashlet();
			}	
			
			return _dashlet;
		}
		
		public function setDashlet(dashlet:Dashlet):void{
			_dashlet = dashlet;
		}
		
		public function hasDashlet():Boolean{
			return _dashlet != null;	
		}
		
		public function removeDashlet():Dashlet{
			return _operation.removeDashlet();	
		}
		
		public function getLayoutKids():Array{
			return _layouts;	
		}
		
		public function removeLayoutKids():void{
			this.removeAllChildren();
			this._layouts = [];
		}
		
		public function reAddDashlet():void{
			_operation.reAddDashlet();	
		}
		
		public function addDashletEventListeners():void{
			_operation.addDashletEventListeners();
		}
		
		public function fromXML(xml:XML, existingDashlets:Array = null, dashletPrompts:Array = null, noRender:Boolean = false):void{
			_pagePath = Page.getPath(xml);
			_pageUuid = Page.getUuid(xml);
			_execTime = (new Date()).getTime();
			_rectangleSelect = Page.getRectangleSelect(xml);
			parsePrompts(xml);
			staggeredFromXML(xml, 0, dashletPrompts, noRender);
		}
		
		private function parsePrompts(xml:XML):void{
			_prompts = (xml.Prompts.length() > 0) ? xml.Prompts[0] : <Prompts/>;
		}
		
		public function set scaleType(scaleType:String):void{
			_scaleType = scaleType;
			if (scaleType == ScaleType.SCALE_TO_FIT){
				_dashlet && _dashlet.scaleAutoFit();
			}	
		}	
		
		public function getReportWidthDiff():int{
			return _operation.getReportWidthDiff();
		}
		
		private function staggeredFromXML(xml:XML, staggerCount:int, dashletPrompts:Array = null, noRender:Boolean = false):void{
			if(xml){
				this.percentHeight = xml.@percentHeight;
				this.percentWidth = xml.@percentWidth;
				this.direction = xml.@direction;
				
				/*
				//Create dashlet on a delayed timer
				if(xml.dashlet.length() > 0){
					Tracer.debug('delayed render at ' + staggerCount * 1000, this);
					var timer:Timer = new Timer(staggerCount * 1000, 1);
					timer.addEventListener(TimerEvent.TIMER, 
					function(event:TimerEvent):void{
						createDashlet(xml.dashlet[0]);
					});
					timer.start();
				}*/
				createDashlet(xml.dashlet[0], noRender);
				createPrompt(xml.Prompt[0], dashletPrompts);	
				var children:XMLList = xml.Layout;
				for(var i:int = 0; i < children.length(); i++){
					var layout:XML = children[i];
					
					var child:LayoutFixed = new LayoutFixed();
					child._prompts = _prompts;
					child.scaleType = _scaleType;
					child._pagePath = _pagePath;
					child._pageUuid = _pageUuid;
					child._execTime = _execTime;
					child._rectangleSelect = _rectangleSelect;
					
					_layouts.push(child);
					addChild(child as DisplayObject);
					
					var evt:LayoutEvent = new LayoutEvent(LayoutEvent.NEW_CHILD);
					evt.child = child;
					dispatchEvent(evt);
					
					child.staggeredFromXML(layout, staggerCount++, dashletPrompts, noRender);
				}
				
			}
		}
		
		public function createDashlet(xml:XML = null, noRender:Boolean = false):void{
			if (!_dashlet && xml){
					_dashlet = new Dashlet();
					_operation.addDashletEventListeners();
					setupDashletScaling();
					_dashlet.fromXML(xml);
					addChild(_dashlet);
					
					//render report
					_dashlet.renderReportByPath(_execTime, _pagePath, _pageUuid, xml.Path, _prompts, false, _rectangleSelect, noRender);
			}	
		}
		
		//Render the prompt in place of the dashlet by removing the prompt from the prompt display section
		//to here.
		public function createPrompt(xml:XML, dashletPrompts:Array = null):void{
			if(!_prompt && xml && dashletPrompts){
				var refId:String = xml.@ref.toString();
				_prompt = dashletPrompts[refId];
				if(_prompt){
					_prompt.autoSizeControl();
					_prompt.isRenderedInDashlet = true;
					_prompt.dropdownLabel.setStyle('fontWeight', 'bold');
					addChild(_prompt);
				}
			}
		}
		
		private function setupDashletScaling():void{
			if (_scaleType == ScaleType.SCALE_TO_FIT){
				_dashlet && _dashlet.scaleAutoFit();
			}	
		}
		
		public function toXML(parent:XML = null):XML{
			return new XML();
		}
		
		public function set isMaster(root:Boolean):void{
			_isMaster = root;
		}
		
		public function get isMaster():Boolean{
			return _isMaster;
		}
	}
}