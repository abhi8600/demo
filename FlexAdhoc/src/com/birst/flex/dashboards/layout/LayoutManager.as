package com.birst.flex.dashboards.layout {
	import com.birst.flex.core.Tracer;
	import com.birst.flex.dashboards.events.LayoutEvent;
	import com.birst.flex.dashboards.views.Dashlet;
	
	import flash.events.MouseEvent;
	import flash.geom.Point;
	
	import mx.containers.Canvas;
	import mx.containers.ViewStack;
	import mx.core.UIComponent;
	import mx.effects.Move;
	import mx.effects.Parallel;
	import mx.effects.Resize;
	import mx.effects.easing.Exponential;
	import mx.events.EffectEvent;
	
	public class LayoutManager {
		
		//List of all our layout objects in our view stack
		private var _layouts:Array;
		
		//Our main layout object which houses all the layouts
		private var _rootLayout:LayoutBase;
		
		//Parameters for saving the maximize / minimize dashlet 
		private var _dashletHeight:Number;
		private var _dashletWidth:Number;
		private var _dashletPosX:Number;
		private var _dashletPosY:Number;
		private var _targetLayout:LayoutBase;
		
		//private var _targetIndex:int;
		
		//The view stack which has our preview canvas
		private var _viewstack:ViewStack;
		private var _effectsCanvas:Canvas;
		
		//parallel effect for the sizing of layouts
		private var _parallel:Parallel;
		
		private var _layoutInFocus:UIComponent
		
		public function LayoutManager(){
			_layouts = new Array();
			_parallel = new Parallel();
		}
		
		public function set root(root:LayoutBase):void{
			this._rootLayout = root;
		}
		
		public function set viewstack(viewstack:ViewStack):void{
			this._viewstack = viewstack;
		}
		
		public function updateDashlets(prompts:XML, pagePath:String, pageUid:String):void{
			var execTime:Number = (new Date()).getTime();
			
			var count:int = 0;
			for(var i:int = 0; i < _layouts.length; i++){
				var layout:LayoutBase = _layouts[i];
				if(layout.hasDashlet()){
					layout.getDashlet().load(prompts, execTime, pagePath, pageUid);
				}
			}
		}
		
		public function getDashletsStates():XML{
			var dashlets:XML = new XML('<dashlets/>');
			for(var i:int = 0; i < _layouts.length; i++){
				var layout:LayoutBase = _layouts[i];
				if(layout.hasDashlet()){
					var selectors:XML = layout.getDashlet().getState();
					if (selectors){
						dashlets.appendChild(selectors);
					}
				}
			}
			return dashlets;	
		}
		
		public function setDashletsStates(xml:XML):Boolean{
			var applied:Boolean = false;
			for(var i:int = 0; i < _layouts.length; i++){
				var layout:LayoutBase = _layouts[i];
				if(layout.hasDashlet()){
					if(layout.getDashlet().setState(xml)){
						applied = true;
					}
				}
			}
			return applied;
		}
		
		public function validateViewMode():void{
			var applied:Boolean = false;
			for(var i:int = 0; i < _layouts.length; i++){
				var layout:LayoutBase = _layouts[i];
				if(layout.hasDashlet()){
					layout.getDashlet().toggleMode(Dashlet.viewMode);
				}
			}
		}
		
		public function areReportsBeforeTime(time:Number):Boolean{
			for(var i:int = 0; i < _layouts.length; i++){
				var layout:LayoutBase = _layouts[i];
				if(layout.hasDashlet()){
					if(layout.getDashlet().getReportModificationTime() > time){
						return false;
					}
				}
			}
			return true;	
		}
		
		public function toggleMode(mode:int):void{
			for(var i:int = 0; i < _layouts.length; i++){
				var layout:LayoutBase = _layouts[i];
				if(layout.hasDashlet()){
					layout.getDashlet().toggleMode(mode);
				}
			}	
		}
		
		public function getDashlets():Array{
			var dashlets:Array = new Array();
			for(var i:int = 0; i < _layouts.length; i++){
				var layout:LayoutBase = _layouts[i];
				if(layout.hasDashlet()){
					dashlets.push(layout.getDashlet());
				}
			}
			return dashlets;		
		}
		
		public function verifyDashletReportPaths(dashlets:XMLList):Boolean{
			if(dashlets.length() > 0){
				var pathMap:Array = [];
				var paths:XMLList = dashlets.path;
				var numPaths:int = paths.length();
				for(var j:int = 0; j < numPaths; j++){
					pathMap[paths[j].toString()] = 1;
				}	
				
				var numDashlets:int = 0;
				for(var i:int = 0; i < _layouts.length; i++){
					var layout:LayoutBase = _layouts[i];
					if(layout.hasDashlet()){
						numDashlets++;
						var dashlet:Dashlet = layout.getDashlet();
						if(!pathMap[dashlet.path]){
							return false;
						}
					}
				}
				
				if (numDashlets < numPaths){
					return false;
				}
			}
			return true;	
		}
		
		/*
		private function delayLoad(prompts:XML, dashlet:Dashlet, delay:int):void{
			var timer:Timer = new Timer(delay * 800, 1);
			timer.addEventListener(TimerEvent.TIMER, function(event:TimerEvent):void{
				dashlet.load(prompts);
			});
			Tracer.debug('delayLoad for ' + dashlet, this);
			timer.start();
		}*/
		
		/*
		protected function onKeyDown(event:KeyboardEvent):void{
			if(event.keyCode == Keyboard.UP){
				if (_targetLayout != null){
					//close
					_targetLayout.getDashlet().toggleMaximizeMinimize();
					if(_parallel.isPlaying){
						_parallel.addEventListener(EffectEvent.EFFECT_END, findNextToMaximize);
					}else{
						findNextToMaximize();
					}
				}else{
					if(_layouts.length > 1){
						var firstChild:LayoutMovable = _layouts[1] as LayoutMovable;
						var dashlet:Dashlet = firstChild.getDashlet()
						dashlet.toggleMaximizeMinimize();
					}
				}
			}
		}
		
		private function findNextToMaximize(event:EffectEvent = null):void{
			if (_targetLayout){
				var parent:DisplayObject = _targetLayout.parent;
				//Do siblings first
				if (parent && parent is LayoutMovable){
					var p:LayoutMovable = LayoutMovable(parent);
					var kids:Array = p.getLayoutKids();
					for(var i:int = 0; i < kids.length; i++){
						var k:LayoutMovable = kids[i];
						if(k == _targetLayout && ((i + 1 ) < kids.length)){
							(kids[i+1] as LayoutMovable).getDashlet().toggleMaximizeMinimize();
							return;
						}
					}
				}
				//Then kids
				var targetKids:Array = _targetLayout.getLayoutKids();
				if(targetKids.length > 0){
					(targetKids[i] as LayoutMovable).getDashlet().toggleMaximizeMinimize();
				}
			}	
			
			if(event){
				_parallel.removeEventListener(EffectEvent.EFFECT_END, findNextToMaximize);
			}
		}*/
		
		public function set effectsCanvas(canvas:Canvas):void{
			this._effectsCanvas = canvas;	
		}
		
		public function addLayout(layout:LayoutBase):void{
			if(layout){
				layout.addEventListener(LayoutEvent.MAXIMIZE, onLayoutMaximize);
				layout.addEventListener(LayoutEvent.MINIMIZE, onLayoutMinimize);
				layout.addEventListener(LayoutEvent.NEW_CHILD, onLayoutNewChild);
				layout.addEventListener(MouseEvent.ROLL_OVER, onLayoutRollOver);
				//layout.addEventListener(LayoutEvent.TOGGLE_GAPS, onLayoutToggleGap);
				_layouts.push(layout);
			}
		}
		
		protected function onLayoutRollOver(event:MouseEvent):void{
			if (this._layoutInFocus){
				_layoutInFocus.setStyle('borderColor', 0x000000);
			}
			
			var target:UIComponent = event.target as UIComponent;
			target.setStyle("borderColor", 0xFF0000);
			_layoutInFocus = event.currentTarget as UIComponent;
		}
		
		/*
		protected function onLayoutToggleGap(event:LayoutEvent):void{
			if(_rootLayout){
				var verticalGap:int = (_rootLayout as UIComponent).getStyle('verticalGap') as int;
				(verticalGap > 0) ? this.removeGaps() : this.restoreGaps();	
			}
		}*/
		
		protected function onLayoutNewChild(event:LayoutEvent):void{
			addLayout(event.child);	
		}
		
		private function getContentToGlobalCoords(object:UIComponent):Point{
			var pt:Point = new Point(object.x, object.y);
			return object.localToGlobal(pt);
		}
		
		private function getGlobalToContentCoords(object:UIComponent, pt:Point):Point{
			return object.globalToLocal(pt);	
		}
		
		protected function onLayoutMaximize(event:LayoutEvent):void {
			var target:LayoutBase = event.currentTarget as LayoutBase;
			if (_viewstack){
				
				_targetLayout = target;
				var targetDashlet:Dashlet = target.getDashlet();
				
				var globalPt:Point = getContentToGlobalCoords(targetDashlet);
				var localPt:Point = getGlobalToContentCoords(_rootLayout as UIComponent, globalPt);
				
				_dashletHeight = targetDashlet.height;
				_dashletWidth = targetDashlet.width;
				
				Tracer.debug('dashlet dimensions: ' + _dashletWidth + 'x' + _dashletHeight, this);
					
				target.removeDashlet();
				
				if (targetDashlet){
					
					_viewstack.selectedIndex = 1;
					
					_effectsCanvas.addChild(targetDashlet);
					targetDashlet.move(localPt.x, localPt.y);
					_dashletPosX = targetDashlet.x = localPt.x;
					_dashletPosY = targetDashlet.y = localPt.y;
				
					moveAndResizeParallel(targetDashlet, 0, 0, _rootLayout.width, _rootLayout.height);
				}
			}
		}
		
		/*
		public function removeGaps():void{
			setHorizontalVerticalGaps(0, 0);
			toggleLayoutPaddings(false);
		}*/
		
		/*
		public function restoreGaps():void{
			setHorizontalVerticalGaps(6, 6); //apparent 6 is the default
			toggleLayoutPaddings(true);	
		}*/
		
		/*
		protected function toggleLayoutPaddings(on:Boolean):void{
			for(var i:int = 0; i < _layouts.length;	i++){
				var layout:LayoutMovable = _layouts[i];
				(on) ? layout.addPaddingsBorder() : layout.noPaddingsBorder();
			}	
		}*/
		
		/*
		protected function setHorizontalVerticalGaps(verticalGap:Number, horizontalGap:Number):void{
			for(var i:int = 0; i < _layouts.length;	i++){
				var layout:LayoutMovable = _layouts[i];
				layout.setStyle('verticalGap', verticalGap);
				layout.setStyle('horizontalGap', horizontalGap);
			}
		}*/
		
		private function moveAndResizeParallel(target:Dashlet, toX:Number, toY:Number, widthTo:Number, heightTo:Number, func:Function = null):void{
			var parallel:Parallel = new Parallel();
			
			parallel.duration = 1000;
			
			if (func != null){
				parallel.addEventListener(EffectEvent.EFFECT_END, func);
			}
			
			var resize:Resize = new Resize(target);
	        resize.widthTo = widthTo;
	        resize.heightTo = heightTo;
	        resize.easingFunction = Exponential.easeOut;
	        parallel.addChild(resize);
	        
	        var move:Move = new Move(target);
	        move.xTo = toX;
	        move.yTo = toY;
	        move.easingFunction = Exponential.easeOut;
	        parallel.addChild(move);
	        try{
	        	parallel.play();
	        }catch(e:Error){
	        	//Bug 9895
	        	//Try to recover. Make it the size we want to be and re-render content.
	        	parallel.stop();
	        	target.width = widthTo;
	        	target.height = heightTo;
	        	target.reRenderContent();
	        }
		}
		
		public function minimizeMaximizedDashlet():void{
			minimizeDashletEffect(_targetLayout);
		}
		
		protected function onLayoutMinimize(event:LayoutEvent):void{
			var target:LayoutBase = event.currentTarget as LayoutBase;
			minimizeDashletEffect(target);
		}
		
		private function minimizeDashletEffect(target:LayoutBase):void{
			if (_viewstack && target){
				moveAndResizeParallel(target.getDashlet(), this._dashletPosX, this._dashletPosY, _dashletWidth, _dashletHeight, undisplayPreview);	
			}
		}
		
		private function undisplayPreview(event:EffectEvent):void{
			_effectsCanvas.removeAllChildren();
			_targetLayout.reAddDashlet();
			_viewstack.selectedIndex = 0;
			_targetLayout.getDashlet().minimizedEffectDone();
			_targetLayout = null;
		}
		
		public function calculateDashletsWidthDiff():LayoutDimension{
			var dimension:LayoutDimension = new LayoutDimension();
			Tracer.debug('_rootLayout dimensions: ' + _rootLayout.width + 'x' + _rootLayout.height, this);
			getSumReportWidthDiff(_rootLayout, _rootLayout.width, _rootLayout.height, dimension);
			return dimension;
		}
		
		private function getSumReportWidthDiff(layout:LayoutBase, totalLayoutWidth:int, totalLayoutHeight:int, dimension:LayoutDimension):void{
			if (layout.hasDashlet()){
				dimension.extrapolate(layout, totalLayoutWidth, totalLayoutHeight);
			}else{
				var kids:Array = layout.getLayoutKids();
				if (kids.length == 0)
					return;
					
				for(var i:int = 0; i < kids.length; i++){
					var kiddo:LayoutBase = kids[i];
					getSumReportWidthDiff(kiddo, totalLayoutWidth, totalLayoutHeight, dimension);
				}
			}
		}
		
		public function reset():void{
			this._layouts = [];
		}
		
		public function scaleAutoFitAllDashlets():void{
			setScalingAll(true);
		}
			
		public function unscaleAllDashlets():void{
			setScalingAll(false);
		}
		
		public function setScalingAll(scale:Boolean):void {
			setScaling(_rootLayout, scale);
			for(var i:int = 0; i < _layouts.length; i++){
				setScaling(_layouts[i], scale);			
			}
		}

		private function setScaling(layout:LayoutBase, scale:Boolean):void{
			if(layout){
				layout.scaleType = (scale) ?  ScaleType.SCALE_TO_FIT : ScaleType.NO_SCALE;
				if(layout.hasDashlet()){
					var dash:Dashlet = layout.getDashlet();
					(scale) ? dash.scaleAutoFit() : dash.noScaling();
					if(dash.content){
						(scale) ? dash.content.scaleAutoFit() : dash.content.revertToUnscale();	
					}else{
						dash.reRenderContent();	
					}
				}
			}			
		}
	}
}