package com.birst.flex.dashboards.layout
{
	import com.birst.flex.dashboards.views.Dashlet;
	import com.birst.flex.dashboards.events.LayoutEvent;
	import com.birst.flex.dashboards.events.DashletEvent;
	
	import mx.core.UIComponent;
	
	public class LayoutOperations
	{
		private var _layout:LayoutBase;
		
		public function LayoutOperations(base:LayoutBase) {
			_layout = base;
		}

		public function addDashletEventListeners():void {
			var dashlet:Dashlet = _layout.getDashlet();
			if(dashlet){
				dashlet.addEventListener(DashletEvent.MINIMIZE, onDashletMinimize);
				dashlet.addEventListener(DashletEvent.MAXIMIZE, onDashletMaximize);
			}
		}
		
		public function removeDashletEventListeners():void{
			var dashlet:Dashlet = _layout.getDashlet();
			if(dashlet){
				dashlet.removeEventListener(DashletEvent.MINIMIZE, onDashletMinimize);
				dashlet.removeEventListener(DashletEvent.MAXIMIZE, onDashletMaximize);
			}
		}
		
		public function reAddDashlet():void{
			var dashlet:Dashlet = _layout.getDashlet();
			if (dashlet){
				dashlet.percentHeight = 100;
				dashlet.percentWidth = 100;
				_layout.addChild(dashlet);
			}
		}
		
		public function removeDashlet():Dashlet{
			var dashlet:Dashlet = _layout.getDashlet();
			if (dashlet){
				_layout.removeChild(dashlet);
				dashlet.dispatchRemovePrompts();
			}	
			return dashlet;
		}
		
		public function changeDashletOwnership(newOwner:LayoutBase):void{
			//Remove the dashlet from us and give it to our new first kid
			if(_layout.hasDashlet()){
				var dashlet:Dashlet = _layout.getDashlet();
				removeDashletEventListeners();
				
				newOwner.setDashlet(dashlet);
				_layout.removeChild(dashlet);
				newOwner.addChild(dashlet);
				newOwner.addDashletEventListeners();
				_layout.setDashlet(null);
			}	
		}
		
		public function getReportWidthDiff():int{
			var reportWidth:int = 0;
			if(_layout.hasDashlet()){
				reportWidth = _layout.getDashlet().getReportWidthDiff();
			}
			
			if (reportWidth > _layout.width){
				return reportWidth - _layout.width;
			}
			
			return _layout.width;
		}
		
		protected function onDashletMinimize(event:DashletEvent):void{
			(_layout as UIComponent).dispatchEvent(new LayoutEvent(LayoutEvent.MINIMIZE));	
		}
		
		protected function onDashletMaximize(event:DashletEvent):void{
			(_layout as UIComponent).dispatchEvent(new LayoutEvent(LayoutEvent.MAXIMIZE));
		}
	}
}