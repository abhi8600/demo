package com.birst.flex.dashboards.layout
{
	public class ScaleType
	{
		[Bindable]
		public static var SCALE_TO_FIT:String = "scaleToFit";
		
		[Bindable]
		public static var NO_SCALE:String = "noScale";
		
		[Bindable]
		public static var SCALE_MAIN_LAYOUT:String = "scaleMainLayout";
		
		public function ScaleType()
		{
		}

	}
}