package com.birst.flex.dashboards.layout
{
	import com.birst.flex.core.Localization;
	import com.birst.flex.core.Tracer;
	import com.birst.flex.core.Util;
	import com.birst.flex.dashboards.components.PromptButton;
	import com.birst.flex.dashboards.components.PromptControl;
	import com.birst.flex.dashboards.events.LayoutEvent;
	import com.birst.flex.dashboards.views.Dashlet;
	
	import flash.events.ContextMenuEvent;
	import flash.events.MouseEvent;
	import flash.ui.ContextMenu;
	import flash.ui.ContextMenuItem;
	
	import mx.containers.BoxDirection;
	import mx.containers.DividedBox;
	import mx.core.Application;
	import mx.core.DragSource;
	import mx.core.EdgeMetrics;
	import mx.events.DragEvent;
	import mx.events.FlexEvent;
	import mx.managers.DragManager;
	import mx.resources.ResourceManager;
	
	public class LayoutMovable extends DividedBox implements LayoutBase
	{
		[Bindable]
		public var changeDirectionMessage:String;
		
		private var _dashlet:Dashlet;
		private var _prompt:PromptControl;
		private var _scaleType:String;
		private var bgCanvas:LayoutBackgroundCanvas;
		private var childLayouts:Array;
		private var _operation:LayoutOperations;
		private var _isMaster:Boolean;
		private var borderColor:uint;
		
		public var countId:int;
		public var master:LayoutMovable;
			
		public function LayoutMovable(){
			super();
			this._isMaster = false;
			this.countId = 0;
			this.childLayouts = [];
			
			this.setStyle('backgroundColor', 0xCCCCCC);
			horizontalScrollPolicy = verticalScrollPolicy = "off";
					
			addPaddingsBorder();

			_operation = new LayoutOperations(this);
			
			this.direction = BoxDirection.HORIZONTAL;
			this.percentHeight = this.percentWidth = 100;
			this.borderColor = uint(getStyle('borderColor'));
			this.addEventListener(FlexEvent.CREATION_COMPLETE, onCreationComplete);
		}
		
		public function getLayoutKids():Array{
			return childLayouts;
		}
		
		public function noPaddingsBorder():void{
			this.setStyle('paddingTop', 0);
			this.setStyle('paddingBottom', 0);
			this.setStyle('paddingRight', 0);
			this.setStyle('paddingLeft', 0);
			this.setStyle('borderStyle', 'none');
		}
		
		public function addPaddingsBorder():void{
			this.setStyle('paddingTop', 5);
			this.setStyle('paddingBottom', 5);
			this.setStyle('paddingRight', 5);
			this.setStyle('paddingLeft', 5);
			this.setStyle('borderStyle', 'solid');
		}
		
		override protected function createChildren():void{
			super.createChildren();
			
			addDragDropListeners();
			
			//this.addEventListener(MouseEvent.ROLL_OVER, onRollOver);
			this.addEventListener(MouseEvent.ROLL_OUT, onRollOut);
			
			/*
			if(getChildren().length == 0){
				bgCanvas = new LayoutBackgroundCanvas();
				bgCanvas.direction = this.direction;
				addChild(bgCanvas);
			}*/
		}
	
		public function onRollOut(event:MouseEvent):void{
			this.setStyle("borderColor", borderColor);	
		}
		
		private function onCreationComplete(event:FlexEvent):void{
			setupDirectionMessage();
			setupContextMenu();
			if(this._prompt){
				this.addChild(this._prompt);
			}
		}
		
		public function hasDashlet():Boolean{
			return _dashlet != null;	
		}
		
		public function getDashlet(isExternalIframe:Boolean = false):Dashlet{
			if (!_dashlet){
				//hideBackgroundCanvas();
				_dashlet = newDashlet(); 
				_dashlet.editMode = true;
				_dashlet.isExternalIframe = isExternalIframe;
				setupDashletScaling();
				addDashletEventListeners();
				addChild(_dashlet);
			}	
			
			return _dashlet;
		}
		
		public function setDashlet(dashlet:Dashlet):void{
			if(dashlet){
				_dashlet = dashlet;	
				_dashlet.contentContextMenu = setupContextMenu();
			}
		}
		
		public function removeDashlet():Dashlet{
			return _operation.removeDashlet();
		}
		
		public function reAddDashlet():void{
			_operation.reAddDashlet();
		}
		
		public function setupContextMenu():ContextMenu{
			if(!contextMenu){
				var ctx:ContextMenu = Util.createCleanContextMenu();
				//ctx.addEventListener(ContextMenuEvent.MENU_SELECT, onContextMenuSelect);
				Util.addLocaleContextMenuItem(ctx, changeDirectionMessage, flipOrientation);
				Util.addLocaleContextMenuItem(ctx, "LM_remove", removeThis);
				Util.addLocaleContextMenuItem(ctx, "LM_vert", splitVertical);
				Util.addLocaleContextMenuItem(ctx, "LM_horiz", splitHorizontal);
				//Util.addLocaleContextMenuItem(ctx, "LM_resize", sizing);
				//testing only
				/*
				Util.addContextMenuItem(ctx, "Toggle gaps", toggleGap);
				Util.addContextMenuItem(ctx, "Scale", scale);
				Util.addContextMenuItem(ctx, "One chart scale", scaleIfOneChart);
				Util.addContextMenuItem(ctx, "Unscale", unscale);
				*/
				contextMenu = ctx;
			}
			return contextMenu;
		}
		
		private function toggleGap(event:ContextMenuEvent):void{
			dispatchEvent(new LayoutEvent(LayoutEvent.TOGGLE_GAPS));
		}
		
		private function scale(event:ContextMenuEvent):void{
			if(this._dashlet){
				_dashlet.fitToDimensions();
			}	
		}
		
		private function unscale(event:ContextMenuEvent):void{
			if(this._dashlet){
				_dashlet.unscale();
			}
		}
		
		private function scaleIfOneChart(event:ContextMenuEvent):void{
			if(this._dashlet){
				_dashlet.scaleAutoFit();
			}	
		}
		
		private function onContextMenuSelect(event:ContextMenuEvent):void{
			this.setStyle('backgroundColor', 0xCC3300);
			Application.application.addEventListener(MouseEvent.CLICK, onAppMouseClick);
		}
		
		private function onAppMouseClick(event:MouseEvent):void{
			this.setStyle('backgroundColor', 0xCCCCCC);
			Application.application.removeEventListener(MouseEvent.CLICK, onAppMouseClick);
		}
		
		private function setupDirectionMessage():void{
			changeDirectionMessage = (direction == BoxDirection.HORIZONTAL) ? 
									 	"LM_chgVert" : "LM_chgHoriz";
		}
		
		public function removeThis(event:ContextMenuEvent):void{
			Application.application.trackEvent("removeThis");
			if (this.parent){
				if(parent is LayoutMovable){		
					(parent as LayoutMovable).removeLayoutChild(this);
				}
			}
		}
	
		private function recurseDispatchRemovePrompts(child:LayoutMovable):void{
			if(child){
				if(child.hasDashlet()){
					child.getDashlet().dispatchRemovePrompts();
				}
				var childKids:Array = child.getChildren();
				for(var i:int = 0; i < childKids.length; i++){
					if (childKids[i] is LayoutMovable){
						recurseDispatchRemovePrompts(childKids[i] as LayoutMovable);
					}
				}
			}	
		}
		
		private function removeLayoutChild(child:LayoutMovable):void{
			recurseDispatchRemovePrompts(child);
			removeChild(child);
			
			for(var i:int = 0; i < childLayouts.length; i++){
				var c:LayoutMovable = childLayouts[i] as LayoutMovable;
				if (c == child){
					childLayouts.splice(i, 1);
				}	
			}
			
			if(childLayouts.length == 0){
				addDragDropListeners();	
			}
			
			/*
			if (childLayouts.length == 0 && this.bgCanvas){
				this.showBackgroundCanvas();	
			}*/	
		}
		
		public function splitVertical(event:ContextMenuEvent):void{
			Application.application.trackEvent("splitVertical");
			this.splitLayout(BoxDirection.VERTICAL);
		}
		
		public function splitHorizontal(event:ContextMenuEvent):void{
			Application.application.trackEvent("splitVertical");
			this.splitLayout(BoxDirection.HORIZONTAL);
		}
		
		public function sizing(event:ContextMenuEvent):void{
			
		}
		
		private function splitLayout(direction:String):void{
			this.direction = direction;
			
			//this.hideBackgroundCanvas();
			
			//Create a new kid and give it our dashlet
			if (childLayouts.length == 0){
				
				var dashlet:Dashlet = null;
				if(_dashlet){
				  	dashlet = _operation.removeDashlet();
				}
					
				var firstChild:LayoutMovable = addChildLayout(this.direction);
				if(dashlet){
					firstChild.setDashlet(dashlet);
					firstChild.addChild(dashlet);
					_dashlet = null;
				}
			}
			
			//Create a new layout
			addChildLayout(direction);
			resizeChildrenEvenly();
			
			//We need to remove our drag drop listeners since we want only the kids to be able to accept drags
			removeDragDropListeners();
		}
		
		protected function changeDashletOwnership(newOwner:LayoutMovable):void{
			//Remove the dashlet from us and give it to our new first kid
			_operation.changeDashletOwnership(newOwner);
		}
		
		public function addDashletEventListeners():void{
			_operation.addDashletEventListeners();
		}
		
		protected function removeDashletEventListeners():void{
			_operation.removeDashletEventListeners();
		}
		
		private function addDragDropListeners():void{
			this.addEventListener(DragEvent.DRAG_DROP, onDragDrop);
			this.addEventListener(DragEvent.DRAG_ENTER, onDragEnter);
			this.addEventListener(DragEvent.DRAG_EXIT, onDragExit);	
		}
		
		private function removeDragDropListeners():void{
			this.removeEventListener(DragEvent.DRAG_DROP, onDragDrop);
			this.removeEventListener(DragEvent.DRAG_ENTER, onDragEnter);
			this.removeEventListener(DragEvent.DRAG_EXIT, onDragExit);
		}
		
		private function resizeChildrenEvenly():void{
			var numKids:int = childLayouts.length;		
			if (numKids > 0){
				var percentEach:Number = 100 / numKids;
				for(var i:int = 0; i < numKids; i++){
					var kid:LayoutMovable = childLayouts[i] as LayoutMovable;
					if(this.direction == BoxDirection.HORIZONTAL){
						kid.percentWidth = percentEach;
						kid.percentHeight = 100;
					}else{
						kid.percentWidth = 100;
						kid.percentHeight = percentEach;
					}
				}
			}
		}
		
		private function addChildLayout(direction:String):LayoutMovable{
			if(getStyle('backgroundImage') != null){
				setStyle('backgroundImage', null);
			}
			
			var box:LayoutMovable = new LayoutMovable();
			box.direction = direction;
			box._scaleType = _scaleType;
			box.master = this.master;
			box.countId = ++this.countId;
			childLayouts.push(box);
			addChild(box);
			
			var evt:LayoutEvent = new LayoutEvent(LayoutEvent.NEW_CHILD);
			evt.child = box;
			dispatchEvent(evt);
			
			return box;
		}
		
		public override function set direction(value:String):void{
			super.direction = value;
			/*
			if(bgCanvas){
				bgCanvas.direction = value;
			}*/
		}
		
		
		public function flipOrientation(event:ContextMenuEvent):void{
			Application.application.trackEvent("flipOrientation");
			direction = (direction == BoxDirection.HORIZONTAL) ?  BoxDirection.VERTICAL : BoxDirection.HORIZONTAL;
			setupDirectionMessage();
			var item:ContextMenuItem = event.target as ContextMenuItem;
			item.caption = ResourceManager.getInstance().getString(Localization.getAppMainBundle(), this.changeDirectionMessage);
			
			//Go to each kid, flip their width percentages to their height percentages, etc
			for(var i:int = 0; i < childLayouts.length; i++){
				var c:LayoutMovable = childLayouts[i] as LayoutMovable;
				var tmpW:Number = c.percentWidth;
				c.percentWidth = c.percentHeight;
				c.percentHeight = tmpW;
			}
		}
		
		/*
		public function hideBackgroundCanvas():void{
			if(this.bgCanvas){
				try{
					this.removeChild(bgCanvas);
				}catch(e:ArgumentError){}
			}	
		}
		
		public function showBackgroundCanvas():void{
			if(this.bgCanvas){
				try{
					this.getChildIndex(bgCanvas);
				}catch(e:ArgumentError){
					this.addChild(bgCanvas);
				}
			}
		}*/
		
		protected function onDragEnter(event:DragEvent):void{
			var target:LayoutMovable = event.currentTarget as LayoutMovable;
			
			var source:DragSource = event.dragSource;		
			var treeItems:Array = source.dataForFormat("treeItems") as Array;
				
			//Check if the dragged tree item is the leaf node
			var isLeafNode:Boolean = false;
			if (treeItems && treeItems.length > 0){
				var node:XML = treeItems[0];
				var d:String = node.@dir;
				isLeafNode = (d != "true") && (node.@n.length() > 0);
			}
			
			//Acceptable drop object?
			if (isLeafNode || event.dragInitiator is PromptButton){
				Tracer.debug("onDragEnter: accepted", this);
				
				DragManager.acceptDragDrop(event.currentTarget as LayoutMovable);
					
				//target.setStyle("borderThickness", 3);
				target.setStyle("borderColor", 0xFFC56C);
			}
			
		}
		
		protected function onDragDrop(event:DragEvent):void{
			var target:LayoutMovable = event.currentTarget as LayoutMovable;				
			var ds:DragSource = event.dragSource as DragSource;
			
			//Tree nodes dragged onto us
			if( ds.hasFormat("treeItems") ) {		
         		var treeList:Array = ds.dataForFormat("treeItems") as Array;
         		if(treeList.length > 0){
         			var node:XML = treeList[0] as XML;	
         			
         			var name:String = node.@n.toString();
         			
         			var index:int = name.lastIndexOf(".");
         			var suffix:String = '';
         			if(index > 0){
         				suffix = name.substr(index).toLowerCase();
         			}
         			
         			var dashlet:Dashlet = null;
         			if(suffix == ".external"){ //Iframe
         				dashlet = this.getDashlet(true);
         			}else if (suffix == ".dashlet"){ //Plugin
         				dashlet = this.getDashlet(false);
         				var nodeLabel:String = name.substring(0, index);
         				var doIdx:int = nodeLabel.lastIndexOf(".");
         				var pluginExtension:String = doIdx > 0 ? nodeLabel.substr(doIdx + 1) : nodeLabel;
         				dashlet.setAsInternalPlugin(name, pluginExtension);
         			}else{ //Regular report
         				dashlet = this.getDashlet(false);
	         			dashlet.renderReportByPath(0, "NO_LOG", "NO_LOG", name, null, true, false, false, true);
	         			dashlet.content.enabled = false;
         			}
	         		dashlet.title = node.@l;
				}
			}else if (event.dragInitiator is PromptButton){
				var pbutton:PromptButton = event.dragInitiator as PromptButton;
				if(this._prompt){
					this.removeChild(this._prompt);
				}
				
				this._prompt = new PromptControl();
				pbutton.linkInDashlet(this._prompt);
				this._prompt.isRenderedInDashlet = true;
				this._prompt.xml = pbutton.xml;
				this.addChild(this._prompt);
			}
			
			revertBorderThickness(target);
		}
		
		protected function onDragExit(event:DragEvent):void{
			Tracer.debug('exitting ' + event.currentTarget, this);
			revertBorderThickness(event.currentTarget as LayoutMovable);
		}
		
		private function revertBorderThickness(target:LayoutMovable):void{
			//target.setStyle("borderThickness", 1);
			target.setStyle("borderColor", 0x000000);
		}
		
		
		public function toXML(parent:XML = null):XML{
			var root:XML = rootTag();
			/*
			for(var i:int = 0; i < this.childLayouts.length; i++){
				var child:LayoutMovable = LayoutMovable(childLayouts[i]);
				child.toXML(root);
			}*/
			var children:Array = this.getChildren();
			for(var i:int = 0; i < children.length; i++){
				if (children[i] is LayoutMovable){
					var child:LayoutMovable = children[i] as LayoutMovable;
					child.toXML(root);
				}
			}
			
			if (parent){
				parent.appendChild(root);
			}
			
			return (parent) ? parent : root;
		}
		
		public function fromXML(xml:XML, existingDashlets:Array = null, dashletPrompts:Array = null, noRender:Boolean = false):void{
			if(xml.Layout.length() > 0){
				staggeredFromXML(xml.Layout[0], 0, existingDashlets, dashletPrompts);
			}
			
			if(this.isMaster){
				Tracer.debug('fromXML: ' + this + ' number of dividers: ' + this.numDividers, this);	
			}
		}
		
		
		private function staggeredFromXML(xml:XML, staggerCount:int, existingDashlets:Array, dashletPrompts:Array):void{
			if(xml){
				this.percentHeight = xml.@percentHeight;
				this.percentWidth = xml.@percentWidth;
				this.direction = xml.@direction;
				
				Tracer.debug('layoutMovable: ' + this + ' ' + this.percentWidth + '%x' + this.percentHeight + '%', this);
			
				if(xml.dashlet.length() > 0){
					var dashletXml:XML = xml.dashlet[0];
					var reportPath:String = dashletXml.Path.toString();
					
					createDashlet(dashletXml);
	
					var existingDashlet:Dashlet = getExistingDashlet(reportPath, existingDashlets);
					if(existingDashlet && existingDashlet.content){
						_dashlet.content = existingDashlet.content;
						_dashlet.content.enabled = false;
						_dashlet.addChild(_dashlet.content);
					}else{
						if(!_dashlet.isExternalIframe){
							//Render report
							_dashlet.renderReportByPath(0, "NO_LOG", "NO_LOG", reportPath, 
														(dashletXml.Prompts.length() > 0) ? dashletXml.Prompts[0] : <Prompts/>, false, false);
							_dashlet.content.enabled = false;
						}
					}
				}else if (xml.Prompt.length() > 0){
					createPrompt(xml.Prompt[0], dashletPrompts);
				}
					
				var children:XMLList = xml.Layout;
				var numChildren:int = children.length();
				
				for(var i:int = 0; i < children.length(); i++){
					var layout:XML = children[i];
					var child:LayoutMovable = this.addChildLayout(layout.@direction);
					child.scaleType = this._scaleType;
					child.staggeredFromXML(layout, staggerCount++, existingDashlets, dashletPrompts);
				}
				
			}
		}
	
		//Get an existing dashlet which matches our path and hasn't yet been borrowed
		private function getExistingDashlet(path:String, existingDashlets:Array):Dashlet{
			if(existingDashlets){
				for(var i:int = 0; i < existingDashlets.length; i++){
					var dashlet:Dashlet = Dashlet(existingDashlets[i]);
					if (path == dashlet.path && dashlet.isContentDisplayed()){
						return dashlet;
					}
				}
			}	
			return null;
		}
		
		//TO-DO - consolidate this code, used by LayoutFixed and us..
		public function createDashlet(xml:XML = null, noRender:Boolean = false):void{
			if (!_dashlet){
				_dashlet = newDashlet();
				_dashlet.editMode = true;
				_operation.addDashletEventListeners();
				setupDashletScaling();
				_dashlet.fromXML(xml);
				addChild(_dashlet);
			}	
		}
		
		public function createPrompt(xml:XML, dashletPrompts:Array):void{
			if(xml && !this._prompt && dashletPrompts){
				var ref:String = xml.@ref;
				var xmlDef:XML = dashletPrompts[ref] as XML;
				if(xmlDef){
					this._prompt = new PromptControl();
					this._prompt.isRenderedInDashlet = true;
					this._prompt.xml = xmlDef;
					this._prompt.percentHeight = 100;			
				}	
			}
		}
		
		private function newDashlet():Dashlet{
			var dash:Dashlet = new Dashlet();
			dash.contentContextMenu = setupContextMenu();
			return dash;
		}
		
		private function setupDashletScaling():void{
			if(_dashlet){
				(_scaleType == ScaleType.SCALE_TO_FIT) ? 
					_dashlet.scaleAutoFit() : _dashlet.noScaling();
			}
		}
		
		private function rootTag():XML{
			var root:XML = <Layout/>;
			root.@direction = this.direction;

			if (parent && parent is LayoutMovable){
				var p:LayoutMovable = LayoutMovable(parent);
				
				Tracer.debug('parent width = ' + parent.width + ' height = ' + parent.height, this);
				
				var parentWidth:Number = p.getLayoutWidth();
				var parentHeight:Number = p.getLayoutHeight();
				
				var ourWidth:Number = this.getLayoutWidth();
				var ourHeight:Number = this.getLayoutHeight();
				
				root.@percentWidth = Math.round((ourWidth/p.width) * 100);
				root.@percentHeight = Math.round((ourHeight/p.height) * 100);
			}else{
				//Assuming we are root or standalone
				root.@percentWidth = this.percentWidth;
				root.@percentHeight = this.percentHeight;
			}
			
			if(_dashlet){
				root.appendChild(_dashlet.toXML());
			}
			
			if(_prompt){
				var pr:XML = <Prompt/>;
				pr.@ref = _prompt.xml.@ref.toString();
				root.appendChild(pr);
			}
			
			return root;			
		}
		
		private function getLayoutHeight():Number{
			var borders:EdgeMetrics = this.viewMetricsAndPadding;
			return this.height + borders.bottom + borders.top;
		}
		
		private function getLayoutWidth():Number{
			var borders:EdgeMetrics = this.viewMetricsAndPadding;
			return this.width + borders.left + borders.right;
		}
		
		public function set scaleType(scaleType:String):void{
			_scaleType = scaleType;	
		}	 
		
		public function removeLayoutKids():void{
			this.removeAllChildren();
			this.childLayouts = [];
		}
		
		public function getReportWidthDiff():int{
			return _operation.getReportWidthDiff();
		}
	
		public function set isMaster(root:Boolean):void{
			_isMaster = root;
		}
		
		public function get isMaster():Boolean{
			return _isMaster;
		}
		
	}
}