package com.birst.flex.dashboards.components
{
	import com.birst.flex.dashboards.events.DashboardEvent;
	import com.birst.flex.dashboards.events.LinkTabBarDropEvent;
	
	import flash.events.MouseEvent;
	
	import mx.controls.Button;
	import mx.controls.Label;
	import mx.controls.LinkBar;
	import mx.core.Application;
	import mx.core.DragSource;
	import mx.core.IFlexDisplayObject;
	import mx.events.DragEvent;
	import mx.events.FlexEvent;
	import mx.managers.DragManager;

	[Event(name="drop", type="com.birst.flex.dashboards.events.LinkTabBarDropEvent")]
	
	public class DraggableLinkBar extends LinkBar
	{

		public function DraggableLinkBar()
		{
			super();
		}
		
		override protected function createNavItem(label:String, icon:Class=null):IFlexDisplayObject{
			var link:Button = super.createNavItem(label, icon) as Button;
			if(Application.application.isEditable){
				addDragDropListeners(link);
			}
			return link;
		}
		
		private function addDragDropListeners(link:Button):void{
			link.addEventListener(MouseEvent.MOUSE_DOWN, onMouseDownLink);
			link.addEventListener(MouseEvent.MOUSE_UP, onMouseUpLink);
			link.addEventListener(DragEvent.DRAG_ENTER, onDragEnterLink);
			link.addEventListener(DragEvent.DRAG_DROP, onDragDropLink);
			link.addEventListener(DragEvent.DRAG_EXIT, onDragExitLink);
		}
		
		private function onMouseDownLink(event:MouseEvent):void{
			var link:Button = event.currentTarget as Button;
			link.addEventListener(MouseEvent.MOUSE_MOVE, onMouseMoveLink);
		}
		
		private function onMouseUpLink(event:MouseEvent):void{
			var link:Button = event.currentTarget as Button;	
			link.removeEventListener(MouseEvent.MOUSE_MOVE, onMouseMoveLink);
		}
		
		private function onDragEnterLink(event:DragEvent):void{
			//No dropping on self
			if(event.dragInitiator == event.currentTarget){
				return;
			}
			
			var source:DragSource = event.dragSource as DragSource;
			if(source.hasFormat('linkIndex')){
				DragManager.acceptDragDrop(mx.core.IUIComponent(event.target));	
				DragBarUtility.hoverIndicator(event.target as Button);
			}
		}

		private function onDragDropLink(event:DragEvent):void{
			DragBarUtility.unhoverIndicator(event.target as Button);
			var sourceIndex:int = getChildIndex(event.dragInitiator as Button);
			var targetIndex:int = getChildIndex(event.target as Button);
			dispatchEvent(new LinkTabBarDropEvent(LinkTabBarDropEvent.DROP, false, false, targetIndex, sourceIndex));	
		}
		
		private function onDragExitLink(event:DragEvent):void{
			DragBarUtility.unhoverIndicator(event.target as Button);
		}
		
		private function onMouseMoveLink(event:MouseEvent):void{
			// Get the drag initiator component from the event object.
            var dragInitiator:Button = event.currentTarget as Button;
    
        	// Create a DragSource object.
        	var dragSource:DragSource = new DragSource();

        	// Add the data to the object.
			dragSource.addData(getChildIndex(dragInitiator), 'linkIndex');
			
			// Add our parent (DashboardTab)
			var evt:LinkTabBarDropEvent = new LinkTabBarDropEvent(LinkTabBarDropEvent.DRAGGING, true, false);
			evt.dragSource = dragSource;
			dispatchEvent(evt);
				
       		// Create a bold label of the menubar text to use as a drag proxy.
        	var dragProxy:Label = new Label();
        	dragProxy.setStyle("fontWeight", "bold");
        	dragProxy.text = dragInitiator.label;
			
        	// Call the DragManager doDrag() method to start the drag. 
         	DragManager.doDrag(dragInitiator, dragSource, event, dragProxy);
		}
		
		override protected function hiliteSelectedNavItem(index:int):void{
			super.hiliteSelectedNavItem(index);
			
			if(index > -1){
				var evt:DashboardEvent = new DashboardEvent(DashboardEvent.TAB_HILITE);
				evt.data = Button(getChildAt(index));
				dispatchEvent(evt);
			}
		}	
	}
}