package com.birst.flex.dashboards.components
{
	import com.birst.flex.core.IconPanel;
	import com.birst.flex.core.Tracer;
	import com.birst.flex.core.Util;
	import com.birst.flex.webservice.WebServiceClient;
	import com.birst.flex.webservice.WebServiceResult;
	
	import flash.display.DisplayObjectContainer;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	
	import mx.containers.HBox;
	import mx.controls.Alert;
	import mx.controls.Image;	
	import mx.controls.Button;
	import mx.controls.Menu;
	import mx.controls.TextArea;
	import mx.controls.TextInput;
	import mx.core.Application;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	import mx.events.MenuEvent;
	import mx.events.ResizeEvent;
	import mx.managers.PopUpManager;
	import mx.rpc.events.ResultEvent;
    import com.birst.flex.core.Localization;			
	


	public class StickyNote extends com.birst.flex.core.IconPanel
	{
		[Bindable]
		public var editMode:Boolean;
		
		public var page:Page;
		public var creator:String;
		
		private var helpUrl:String="sticky_note_dialog_box";   	
		private var helpBttn:Button;	
		
		private var content:TextArea;
		private var textHolder:String;
		private var dirty:Boolean = false;
		private var uuid:String = '';
		
		private var inputBoxHolder:HBox;
		private var inputBox:TextInput;
		private var submitBttn:Button;
		
		private var removeBttn:Button;
		private var shareBttn:Button;
		
		//Saved position of self on mouse roll over	
		private var rollOverX:int;
		private var rollOverY:int;
		
		//Saved page width and height, required for positioning based on browser resizing
		private var savedPageWidth:int;
		private var savedPageHeight:int;
		private var savedX:int;
		private var savedY:int;
		
		private var isPublic:Boolean = false;
		
		private var creationTime:Number = -1;
		
		public function StickyNote(p:Page, c:String = '')
		{
			super();
			
			page = p;
			creator = c;
			
			//defaults
			height = 180;
			width = 250;
			x = y = 200;
			
			addEventListener(FlexEvent.CREATION_COMPLETE, onCreationComplete);
		}
		
		private function isOwner():Boolean{
			return (creator == Application.application.username);
		}
		
		private function onCreationComplete(event:FlexEvent):void{
			//Only the owner can drag their stickies
			isPopUp = isOwner();	
			
			if(page){
				page.addEventListener(ResizeEvent.RESIZE, onPageResize);
			}
			
			addEventListener(MouseEvent.MOUSE_MOVE, onMouseMove);
			addEventListener(MouseEvent.ROLL_OVER, onMouseRollOver);	
			addEventListener(MouseEvent.ROLL_OUT, onMouseRollOut);
			
			//Move it to its previous position
			if(savedX && savedY){
				x = savedX;
				y = savedY;
			}
			
			if(Application.application.stickiesPrivateOnly){
				isPublic = false;
			}
		}
		
		override protected function createChildren():void{
			super.createChildren();
			
			setTitleDateString()
			
			if(!content){
				content = new TextArea();
				content.editable = false;
				if(textHolder){
					content.htmlText = textHolder;
					textHolder = null;
				}
				content.percentHeight = content.percentWidth = 100;
				content.setStyle('backgroundAlpha', 0);
				content.setStyle('borderStyle', 'none');
				content.setStyle('paddingRight', 0);
				//content.addEventListener(Event.CHANGE, onChangeContent);
				addChild(content);	
			}
			
			if(!inputBoxHolder){
				inputBox = new TextInput();
				inputBox.setStyle('backgroundColor', 0xcccccc);
				inputBox.width = 185;
				
				submitBttn = new Button();
				submitBttn.label = 'Post';
				submitBttn.addEventListener(MouseEvent.CLICK, onSubmitClick);

				inputBoxHolder = new HBox();
				inputBoxHolder.setStyle('paddingBottom', 2);
				inputBoxHolder.setStyle('paddingLeft', 2);
				inputBoxHolder.setStyle('horizontalGap', 2);
				inputBoxHolder.percentWidth = 100;
				inputBoxHolder.addChild(inputBox);
				inputBoxHolder.addChild(submitBttn);
				Util.hide(inputBoxHolder);
				addChild(inputBoxHolder);
			}
			
			//Only owner can modify sharing permissions
			if(!shareBttn && isOwner()){
				shareBttn = createTitleBarIcon('shareSticky', 'SN_share', onClickShareButton);	
			}
			
			//Only owner can delete sticky
			if(!removeBttn && isOwner()){
				removeBttn = createTitleBarIcon('deleteSticky', 'SN_confirmRem', onClickRemoveButton);
			}
			
    		//Create the help url on titlebar
    		if (Application.application.helpEnable && helpUrl != null && !helpBttn){
     			helpBttn = createTitleBarIcon('question', 'TT_help', helpHandler);
    		}			
		}
        
        public function helpHandler(event:MouseEvent):void{
 			Util.gotoHelp(helpUrl, Application.application.helpSiteURL, Application.application.helpWindowName);
		}        		
		
		private function setTitleDateString():void{
			if(creator){
				var dateStr:String;
				if(creationTime > 0){
					var created:Date = new Date();
					created.setTime(creationTime);
					dateStr = Util.getDateTimeStringInFormat(created, Application.application.localeDateFormat);				
				}
				title = dateStr ? creator + '   ' + dateStr : creator;	
			}
		}
		
		//Mark as dirty and need to save if new text is written
		private function onChangeContent(event:Event):void{
			var txt:String = TextArea(event.currentTarget).htmlText;
			dirty = (txt && txt != '');
		}
		
		public function set xml(xml:XML):void{
			this.creator = xml.@creator.toString();
			
			//Based on the previous saved page's width and height, we ratio it with our current page width and height
			savedPageWidth = int(xml.@savedPageWidth);
			savedPageHeight = int(xml.@savedPageHeight);
			
			Tracer.debug('set xml: ===========================');
			Tracer.debug('savedPageWidth,savedPageHeight: ' + savedPageWidth + 'x' + savedPageHeight);
			
			var ratioWidth:Number = (page.width / savedPageWidth);
			var ratioHeight:Number = (page.height / savedPageHeight);
			Tracer.debug('conversion width,height: ' + ratioWidth  + ',' + ratioHeight);
			
			savedX = int(xml.@x.toString()) * ratioWidth;
			savedY = int(xml.@y.toString()) * ratioHeight;
			
			this.uuid = xml.@uuid.toString();
			if(!content){
				textHolder = xml.Content.toString();
			}
			
			var createTime:String = xml.@creationTime.toString();
			if(createTime && createTime != ""){
				creationTime = Number(createTime);
				setTitleDateString();
			}
			
			isPublic = xml.@isPublic.toString() == 'true';
			
			Tracer.debug('=====================================');
		}
		
		public function popupDisplay():void{
			PopUpManager.addPopUp(this, this.page);
		}
		
		private function onSubmitClick(event:MouseEvent):void{
			//prefix
			var dateTimeStr:String = Util.getDateTimeStringInFormat(new Date(), Application.application.localeDateTimeFormat);
			var userStrPrefix:String = '<b>' + Application.application.username + ' ' + dateTimeStr + "</b>:\n";
			
			content.htmlText = userStrPrefix + inputBox.text + "\n" + content.htmlText;				
			
			inputBox.text = null;
			
			dirty = true;
			save();
		}
		
		public function save():void{
			//we can't save if we don't know which page
			if(!page || !page.path || page.uuid == ''){
				Util.localeAlertError('SN_invalidPage');
				return;	
			}
			
			if (dirty){
				
				var xml:XML = <Note/>;
				xml.@creator = creator;
				
				//Convert our location relative to space such that when page resizing sets it in place
				xml.@savedPageWidth = savedPageWidth = page.width;
				xml.@savedPageHeight = savedPageHeight = page.height;
				
				//Global position
				xml.@x = this.savedX = x
				xml.@y = this.savedY = y
				xml.Content = content.htmlText;

				//Pass the creation time
				if(creationTime == -1){
					creationTime = (new Date()).getTime();
					setTitleDateString();
				}
				xml.@creationTime = creationTime;

				//Public or private
				xml.@isPublic = isPublic;
				
				var ws:WebServiceClient = new WebServiceClient('Dashboard', 'saveStickyNote', 'StickyNote', 'save');
				ws.setResultListener(onSave);
				ws.execute(page.uuid, uuid, xml.toXMLString());
				
				Tracer.debug('save(): \n' + xml.toXMLString(), this);
			}
		}
		
		private function onSave(event:ResultEvent):void{
			var rs:WebServiceResult = WebServiceResult.parse(event);
			if(!rs.isSuccess()){
				Util.localeAlertError('SN_failSave', rs);
				return;	
			}
			dirty = false;
			Tracer.debug('OnSave() uuid before server: ' + uuid);
			uuid = rs.getResult().uuid.toString();
			Tracer.debug('onSave() uuid from server: ' + uuid);
		}
		
		public function remove(event:CloseEvent = null):void{
			if(event.detail != Alert.YES){
				return;
			}
			
			//we can't save if we don't know which page
			if(!page || !page.path || page.uuid == ''){
				Util.localeAlertError('SN_invalidPage');
				return;	
			}
			
			if(uuid != ''){
				var ws:WebServiceClient = new WebServiceClient('Dashboard', 'deleteStickyNote', 'StickyNote', 'delete');
				ws.setResultListener(onDelete);
				ws.execute(page.uuid, uuid);
			}else{
				destroy();
			}	
		}
		
		private function onDelete(event:ResultEvent):void{
			var rs:WebServiceResult = WebServiceResult.parse(event);
			if(!rs.isSuccess()){
				Util.localeAlertError('SN_failDelete', rs);
				return;	
			}
			destroy();	
		}
		
		private function destroy():void{
			page.removeStickyNote(this);	
		}
		
		private function onPageResize(event:ResizeEvent):void{
			if(page){
				Tracer.debug('=======================================');
				Tracer.debug('onPageResize(), savedX, savedY : ' + this.savedX + ',' + this.savedY 
							 + ' savedPageWidth, savedPageHeight: ' + this.savedPageWidth + ',' + this.savedPageHeight);
				Tracer.debug('onPageResize() before: x,y = ' + x + ',' + y);
				var ratioWidth:Number = page.width / savedPageWidth;
				var ratioHeight:Number = page.height / savedPageHeight;
				x = savedX * ratioWidth;
				y = savedY * ratioHeight;
				Tracer.debug('onPageResize() after: ratioWidth,ratioHeight x,y = ' + ratioWidth + ',' + ratioHeight + ' ' + x + ',' + y);
				Tracer.debug('=======================================');
			}	
		}
		
		//Save on mouse roll out if the user has changed the position or
		//text
		private function onMouseRollOut(event:MouseEvent):void{
			Util.hide(controlsHolder);
			Util.hide(inputBoxHolder);	
			setStyle('headerHeight', 0);
			setStyle('backgroundAlpha', 0.3);
			
			Tracer.debug('onMouseRollOut : rollOverX, rollOverY = ' + rollOverX + ',' + rollOverY);
			
			if(rollOverX != x || rollOverY != y){
				Tracer.debug('onMouseRollOut: dirty!!: x,y = ' + x + ',' + y);
				dirty = true;
			}
			
			if(dirty){
				this.save();	
			}
		}
		
		//For debugging only
		private function onMouseMove(event:MouseEvent):void{
			Tracer.debug('====================');
			Tracer.debug('x,y = ' + this.x + ',' + this.y);
			Tracer.debug('localX, localY = ' + event.localX + ',' + event.localY);
			Tracer.debug('stageX,stageY = ' + event.stageX + ',' + event.stageY);
			Tracer.debug('====================');
		}
		
		//Note the dialog box's current position to check if it changed later
		private function onMouseRollOver(event:MouseEvent):void{
			rollOverX = x;
			rollOverY = y;
			Tracer.debug('onMouseRollOver : rollOverX, rollOverY = ' + rollOverX + ',' + rollOverY);
			
			Util.show(controlsHolder);	
			Util.show(inputBoxHolder);
			setStyle('headerHeight', 16);
			setStyle('backgroundAlpha', 0.95);
		}
		
		private function onClickShareButton(event:MouseEvent):void{
			var bttn:Button = Button(event.target);
			var pt:Point = bttn.localToGlobal(new Point(0,0));
			
			var permsData:Array; 
			
			if(Application.application.stickiesPrivateOnly){
				permsData = [{ label: Application.application.getLocString('SN_private'), permission: 'private'}];
			} else {
				permsData = [
						{ label: Application.application.getLocString('SN_private'), 
						  permission: 'private', type: 'radio', groupName: 'perm', toggled: !isPublic},
						{ label: Application.application.getLocString('SN_public'), 
						  permission: 'public', type: 'radio', groupName: 'perm', toggled: isPublic}
				];
			}
			
			var menu:Menu = Menu.createMenu(Application.application as DisplayObjectContainer, permsData);
			menu.addEventListener(MenuEvent.ITEM_CLICK, onItemClickPermission);	
			menu.show(pt.x, pt.y + bttn.height);
		}
		
		private function onItemClickPermission(event:MenuEvent):void{
			isPublic = (Application.application.stickiesPrivateOnly) ? false : event.item.permission == 'public';
			dirty = true;
		}
		
		private function onClickRemoveButton(event:MouseEvent):void{
			var message:String = Application.application.getLocString('SN_remove');
			var title:String = Application.application.getLocString('SN_confirmRem');
			Util.alertYesNoConfirmation(message, remove, title);
		}
		
	}
}