package com.birst.flex.dashboards.components
{
	import com.birst.flex.webservice.WebServiceClient;
	import com.birst.flex.webservice.WebServiceResult;
	
	import mx.collections.ArrayCollection;
	import mx.collections.ICollectionView;
	import mx.controls.treeClasses.ITreeDataDescriptor;
	import mx.rpc.events.ResultEvent;

	public class PromptTreeDataDescriptor implements ITreeDataDescriptor
	{
		private var _tree:TreePrompt;
		private var loadingNodes:Object = new Object();
		
		public function PromptTreeDataDescriptor(tree:TreePrompt)
		{
			_tree = tree;
		}
		
		public function getChildren(node:Object, model:Object=null):ICollectionView
		{
			if (node.hasOwnProperty("gotChildren")) {
				if (node["gotChildren"] == false) {
					node["children"] = new ArrayCollection([{label: 'Loading'}]); 
					getChildrenOfNode(node);
				}
				return node["children"];
			}
			return null;
		}
		
		public function hasChildren(node:Object, model:Object=null):Boolean
		{
			return node["gotChildren"] == false || node["children"];
		}
		
		public function isBranch(node:Object, model:Object=null):Boolean
		{
			return hasChildren(node, model);
		}
		
		public function getData(node:Object, model:Object=null):Object
		{
			return node;
		}
		
		public function addChildAt(parent:Object, newChild:Object, index:int, model:Object=null):Boolean
		{
			return false;
		}
		
		public function removeChildAt(parent:Object, child:Object, index:int, model:Object=null):Boolean
		{
			return false;
		}
		
	private function getChildrenOfNode(node:Object):void {
			var prompt:XML = node["prompt"];
			if (prompt) {
				node["gotChildren"] = true;
				var parentName:String = prompt.ParameterName;
				var root:XML = <Prompts/>;
				root.appendChild(prompt);
				for each (var o:Object in _tree.getPrompts()) {
					var p:XML = XML(o);
					if (p == prompt)
						continue;
					var parents:XMLList = p.Parents.Parent;
					if (parents.length() > 0) {
						for (var i:int = 0; i < parents.length(); i++) {
							var parent:String = parents[i];
								
							if (parentName == parent) {
								root.appendChild(p);
								var ws:WebServiceClient = new WebServiceClient('Adhoc', 'getFilteredFilledOptions', "Dashboards", "promptTreeDataDescriptor", [node, p]);
								ws.setResultListener(addToTree);
								var currentSelectedValues:XMLList = prompt.selectedValues; //.selectedValue
								prompt.selectedValues = XML("<selectedValues><selectedValue>" + node["value"] + "</selectedValue></selectedValues>");
								var currentParents:XMLList = prompt.Parents;
								delete prompt.Parents;
								ws.execute(root.toXMLString(), false);
								prompt.selectedValues = currentSelectedValues;
								prompt.Parents = currentParents;
								return;
							}
						}
					}
				}
			}
			node["children"] = null;
		}
		
		private function addToTree(result:ResultEvent):void{
			var rs:WebServiceResult = WebServiceResult.parse(result); 
			if (rs.isSuccess()){
				var ret:XMLList = rs.getResult();
				var callerData:Array = rs.getCallerData() as Array;
				var node:Object = callerData[0];
				var prompt:XML = callerData[1];
				node["gotChildren"] = true;
				node["children"] = null; 

				var children:XML = PromptDisplay.findPromptValuesFromList(String(prompt.ParameterName), ret);
				if (children) {
					_tree.setupData(children, prompt, node);
				}
			}
		}
		
	}
}