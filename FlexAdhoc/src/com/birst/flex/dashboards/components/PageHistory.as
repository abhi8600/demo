package com.birst.flex.dashboards.components
{
	import com.birst.flex.core.MenuItemLabelRenderer;
	
	import flash.events.MouseEvent;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Menu;
	import mx.controls.PopUpButton;
	import mx.core.Application;
	import mx.core.ClassFactory;
	import mx.core.mx_internal;
	import mx.events.FlexEvent;
	import mx.events.MenuEvent;
	
	use namespace mx_internal;
	
	public class PageHistory extends mx.controls.PopUpButton{
		
		public static const H_DRILL_DOWN:String = 'PH_DrillDown';
		public static const H_PROMPT_APPLY:String = 'PH_PromptApply';
		public static const H_DRILL_DASH:String = 'PH_Dash';
		public static const H_VISUAL_FILTER:String = 'PH_Visual';
		
			
		[Bindable]
		private var _history:ArrayCollection;
		
		private var _menu:Menu;
		
		public function PageHistory() {
			super();
			this.addEventListener(FlexEvent.INITIALIZE, onInit);
			this.addEventListener(MouseEvent.CLICK, onClick);	
		}
		
		private function onInit(event:FlexEvent):void{
			_menu = new Menu();
			_menu.maxWidth = 400;
			_menu.itemRenderer = new ClassFactory(MenuItemLabelRenderer);
			_menu.addEventListener(MenuEvent.ITEM_CLICK, onMenuItemClick);
			_menu.dataProvider = _history = new ArrayCollection();
			
			this.popUp = _menu;
		}
		
		private function onClick(event:MouseEvent):void{
			applyEntry(0);
		}
		
		private function onMenuItemClick(event:MenuEvent):void{
			applyEntry(event.index);
		}
		
		public function recordCurrentState(type:String, dashboard:DashboardTab, page:Page):void {
			if(_history.length >= 10){
				_history.removeItemAt(0);
			}
						
			var label:String = (PageHistory.H_DRILL_DOWN == type) ? "Undo drill action" : "Undo filter" ;
			var tooltip:String = dashboard.label + '/' + page.pageName + '\n' + page.promptSummary.text;
			var state:XML = page.getPageState('', '', PageHistory.H_PROMPT_APPLY == type);
		
			_history.addItemAt({label: label,  toolTip: tooltip, dashboard:dashboard, page: page, state: state}, 0);
			_history.refresh();
		}
		
		public function applyEntry(idx:int):void{
			if(this._history.length > idx){
				var entry:Object = _history.getItemAt(idx);
				
				var page:Page = entry.page as Page;
				var dashboard:DashboardTab = entry.dashboard as DashboardTab;
				var xml:XML = entry.state as XML;
				
				//Go to the dashboard
				//Apply bookmark xml
				if(dashboard && page && xml){
					//Open the dashboard & page
					Application.application.selectDashboardPage(dashboard, page);
					
					_history.removeItemAt(idx);
					_history.refresh();
					page.applyHistory(xml);
				}	
			}
		}
	}
}