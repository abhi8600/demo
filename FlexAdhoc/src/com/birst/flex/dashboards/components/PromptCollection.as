package com.birst.flex.dashboards.components
{
	
	import mx.containers.TitleWindow;
	import mx.controls.Button;
	import mx.core.Application;
	import mx.core.mx_internal;
	import mx.events.CloseEvent;
	import mx.events.FlexEvent;
	
	use namespace mx_internal;
	
	/**
	 * PromptCollection comprises of PromptControls. We can toggle open or close all prompts.
	 */
	public class PromptCollection extends mx.containers.TitleWindow
	{
		private var _close:Boolean = false;
	
		[Bindable]
		public var colName:String;
	
		[Bindable]
		public var isDefault:Boolean = false;
	
		[Embed(source="/images/expand.png")]
		private var expandIcon:Class;
		
		[Embed(source="/images/collapse.png")]
		private var collapseIcon:Class;			
					
		public function PromptCollection(){
			super();
			
			layout = "horizontal";
			showCloseButton = true;
			
			addEventListener(FlexEvent.UPDATE_COMPLETE, onUpdateComplete);
			addEventListener(CloseEvent.CLOSE, onClose);
		}
		
		override protected function createChildren():void{
			super.createChildren();
			
			//Our icons are actually 9x7 instead of the internal 16x16
			var closeBttn:Button = mx_internal::closeButton;
			if(closeBttn){
				closeBttn.explicitWidth = 9;
				closeBttn.explicitHeight = 7;
				closeBttn.toolTip = Application.application.getLocString('PC_ttCollapse');
				
				//Did we started off with prompts all minimized?
				_close = PromptDisplay.promptCreatedMinimized;
				
				setCloseButtonStyle();
			}
		}
		
		
		override protected function layoutChrome(unscaledWidth:Number, unscaledHeight:Number):void{
        	super.layoutChrome(unscaledWidth, unscaledHeight);
        	
        	//Move the close button closer to the edge (Flex moves it 10px from the end)
        	var closeBttn:Button = mx_internal::closeButton;
			if(closeBttn){
				closeBttn.move(closeBttn.x + 6, closeBttn.y);	
			}
			
			//Move the title text closer to the left
			if(titleTextField){
				titleTextField.move(titleTextField.x - 4, titleTextField.y);
			}
    	}
		
		public function hasPrompts():Boolean{
			var children:Array = this.getChildren();
			for(var i:int = 0; i < children.length; i++){
				if (children[i] is PromptControl){
					return true;
				}
			}
			return false;
		}
		
		private function onUpdateComplete(event:FlexEvent):void{
			if(verticalScrollBar){
				height = measuredHeight + verticalScrollBar.width;
			}
		}
		
		private function onClose(event:CloseEvent):void{
			_close = !_close;
			var children:Array = this.getChildren();
			for(var i:int = 0; i < children.length; i++){
				if (children[i] is PromptControl){
					if(_close){
						PromptControl(children[i]).minimize();
					}else{
						PromptControl(children[i]).showControls();
					}
				}
			}
			
			setCloseButtonStyle();
		}
		
		private function setCloseButtonStyle():void{
			//Change the button dynamically
			var closeBttn:Button = mx_internal::closeButton;
			if(closeBttn){
				var clz:Class = (_close) ? expandIcon : collapseIcon;
				var tt:String = (_close) ? 'PC_ttExpand' : 'PC_ttCollapse';
				closeBttn.toolTip = Application.application.getLocString(tt);		
				closeBttn.setStyle('closeButtonSkin', clz);
			}
		}
		
	}
}