package com.birst.flex.dashboards.components
{
	import com.birst.flex.core.Util;
	import com.birst.flex.webservice.WebServiceClient;
	import com.birst.flex.webservice.WebServiceResult;
	
	import flash.events.Event;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.controls.Tree;
	import mx.core.ClassFactory;
	import mx.core.ScrollPolicy;
	import mx.core.mx_internal;
	import mx.events.CollectionEvent;
	import mx.events.CollectionEventKind;
	import mx.rpc.events.ResultEvent;
 
	public class TreePrompt extends Tree
	{
		private var _topPrompt:XML = null;
		private var _prompts:Array = [];
		
		public function TreePrompt(prompt:XML = null)
		{
			super();
			dataDescriptor = new PromptTreeDataDescriptor(this);
			this.width = 200;
			horizontalScrollPolicy = ScrollPolicy.AUTO;
			itemRenderer = new ClassFactory(PromptTreeRenderer);
			setStyle("folderClosedIcon", null);
			setStyle("folderOpenIcon", null);
			setStyle("defaultLeafIcon", null);
			if (prompt) {
				_topPrompt = prompt;
				if (String(prompt.UseDrillPath) == "true") {
					// find drill path and add them
					var ws:WebServiceClient = new WebServiceClient("Adhoc", "getDrillDownPrompts", "TreePrompt", "Constructor");
					ws.setResultListener(gotDrillDownPrompts);
					ws.execute(prompt); 
				}
			}
		}
		
         override protected function updateDisplayList(unscaledWidth:Number, unscaledHeight:Number):void
         {
              // we call measureWidthOfItems to get the max width of the item renderers.
              // then we see how much space we need to scroll, setting maxHorizontalScrollPosition appropriately
              var diffWidth:Number = measureWidthOfItems(0,0) - (unscaledWidth - viewMetrics.left - viewMetrics.right) + 5;
 			  var measuredWidthItems:Number = measureWidthOfItems(0, 0);
 			  var viewMetrics:Number = (unscaledWidth - viewMetrics.left - viewMetrics.right);

              if (diffWidth <= 0)
                  maxHorizontalScrollPosition = NaN;
              else
                  maxHorizontalScrollPosition = diffWidth;
 
              super.updateDisplayList(unscaledWidth, unscaledHeight);
         }
		
	     
		public function addPrompt(prompt:XML):void {
			_prompts.push(prompt);
		}
		
		public function getPrompts():Array {
			var ret:Array = [_topPrompt];
			return ret.concat(_prompts);
		}
		
		public function isChild(prompt:XML):Boolean {
			var parents:XMLList = prompt.Parents.Parent;
			if (parents.length() > 0) {
				for (var i:int = 0; i < parents.length(); i++) {
					var parent:String = parents[i];
					if (containsPrompt(parent))
						return true;
				}
			}
			return false;
		}
		
		private function containsPrompt(name:String):Boolean {
			if (String(_topPrompt.ParameterName) == name)
				return true;
				
			for (var i:int = 0; i < _prompts.length; i++) {
				if (String(_prompts[i].ParameterName) == name)
					return true;
			}
			return false;
		}
		
		private function getFilledOptions(prompt:XML):void{
			var wsFunc:String = 'getFilledOptions'; //(parentPrompts.length > 0) ? 'getFilteredFilledOptions' : 'getFilledOptions';
			var ws:WebServiceClient = new WebServiceClient('Adhoc', wsFunc, 'PromptDisplay', 'xml');
			ws.setResultListener(onGetFilledOptions);
			ws.execute(prompt.toXMLString());
		}
		
		/**
		 * The event listener for webservice to get the list of options for the prompts
		 */
		private function onGetFilledOptions(event:ResultEvent):void{
			
			var result:WebServiceResult = WebServiceResult.parse(event);
			if(!result.isSuccess()){
				Util.localeAlertError('PD_failPrompts', result);
				return;
			}else{
				dataProvider = result.getResult();
								
			}
		}
		
		private function gotDrillDownPrompts(event:ResultEvent):void {
			var result:WebServiceResult = WebServiceResult.parse(event);
			if (result.isSuccess()) {
				var res:XMLList = result.getResult();
				if (res.length() > 0) {
					var prompts:XMLList = res[0]..Prompt;
					for each (var x:XML in prompts) {
						addPrompt(x);
					}
				}
			}
			else {
				Util.localeAlertError('TP_failHierarchicalPrompts', result);
			}
		}
		
		public function setupData(data:XML, prompt:XML, node:Object = null):void {
			if (data) {
				var dataList:XMLList = data.Value;
				var gotChildren:Boolean = false;
				if (_prompts.length > 1 && String(prompt.ParameterName) == String(_prompts[_prompts.length - 1].ParameterName))
					gotChildren = true;
				var selectedValues:ArrayCollection = null;
				var selectedValuesXml:XMLList = prompt.selectedValues.selectedValue;
				if (selectedValuesXml != null && selectedValuesXml.length() > 0) {
					selectedValues = new ArrayCollection();
					for each (var p:XML in selectedValuesXml) {
						selectedValues.addItem(String(p));
					}
				}
				var dp:ArrayCollection = new ArrayCollection();
				for (var j:int = 0; j < dataList.length(); j++) {
					var o:Object = {label: String(dataList[j].Label), 
						value: String(dataList[j].Value), 
						gotChildren: gotChildren, 
						prompt: prompt,
						parent: node,
						uid: (node ? node["value"] : '') + String(dataList[j].Value),
						selected: selectedValues == null ? false : selectedValues.contains(String(dataList[j].Value)),
						enabled: dataList[j].hasOwnProperty('Enabled') ? String(dataList[j].Enabled) == 'true' : true};
					dp.addItem(o);
				}
				if (node == null)
					dataProvider = dp;
				else {
					var item:Object = this.firstVisibleItem;
					node["children"] = dp;
					invalidateDisplayList();
			        var event:CollectionEvent = new CollectionEvent(
			                                        CollectionEvent.COLLECTION_CHANGE,
			                                        false, 
			                                        true,
			                                        CollectionEventKind.RESET);
			        event.items = [node];
					ArrayCollection(dataProvider).dispatchEvent(event);
					this.firstVisibleItem = item;
				}
			}
		}
		
		public function setSelectedValues():void {
			var prompt:XML;
			for each (prompt in _prompts) {
				delete prompt.selectedValues;
			}
			
			setSelectionInList(dataProvider as ArrayCollection);
		}
		
		private function setSelectionInList(list:ArrayCollection):void {
			if (list) {
				for each (var o:Object in list) {
					if (o.hasOwnProperty("selected") && o["selected"] == true && o.hasOwnProperty("prompt")) {
						var prompt:XML = XML(o["prompt"]);
						if ((prompt.selectedValues as XMLList).length() == 0) {
							prompt.appendChild(<selectedValues/>);
						}
						var selectedValues:XML = prompt.selectedValues[0];
						var selectedValue:XML = XML("<selectedValue>" + o["value"] + "</selectedValue>");
						selectedValues.appendChild(selectedValue);
					}
					
					if (o.hasOwnProperty("children")) {
						setSelectionInList(o["children"] as ArrayCollection);
					}
				}
			}
		}
		
		public function getDescription(xml:XML, control:TreePrompt):String{
			var desc:String = '';
			for each (var prompt:XML in control._prompts) {
				var promptDesc:String = PromptControl.getDescription(prompt, control, null, false);
				if (promptDesc && promptDesc.length > 0) {
					if (desc.length > 0) {
						desc = desc + ' ';
					}
					
					desc = desc + promptDesc; 
				} 
			}
			return desc;
		}
		
	}
}