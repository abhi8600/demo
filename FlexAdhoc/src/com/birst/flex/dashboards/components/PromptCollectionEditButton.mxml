<?xml version="1.0" encoding="utf-8"?>
<mx:Button xmlns:mx="http://www.adobe.com/2006/mxml" initialize="init();" styleName="promptCollectionEditButton" buttonMode="true" width="150" height="30">
<!--
	This component serves as a button in the prompt landing area in the page edit component. It represents the 
	prompt collection. Attributes and measures from the subject are can be dragged on it, where it will be added to
	its encapsulated prompt collection edit box. 
-->
	<mx:Script>
		<![CDATA[
			import mx.core.UIComponent;
			import mx.managers.DragManager;
			import mx.controls.Label;
			import mx.core.DragSource;
			import com.birst.flex.core.Localization;
			import mx.controls.Alert;
			import mx.events.CloseEvent;
			import com.birst.flex.core.Util;
			import com.birst.flex.dashboards.events.PromptControlEvent;
			import mx.events.DragEvent;
			import mx.events.ToolTipEvent;
			import mx.managers.ToolTipManager;
			import mx.controls.ToolTip;
			import com.birst.flex.core.MenuItemActions;
			import com.birst.flex.dashboards.dialogs.PromptCollectionDialogBox
			import com.birst.flex.core.Localization;
			
			private var _landingBox:PromptCollectionDialogBox;
			private var _infoTooltip:ToolTip;
			
		
			//Parent's parent container which created us.
			public var parentLandingBox:PromptLandingBox;
			
			//Is this the default, base prompt collection? (which has no name)
			public var isDefault:Boolean = false;
			
			private function init():void{
				//addEventListener(ToolTipEvent.TOOL_TIP_SHOW, onToolTipShow);
				addEventListener(MouseEvent.ROLL_OVER, onRollOver);
				addEventListener(MouseEvent.ROLL_OUT, onRollOut);
				addEventListener(MouseEvent.CLICK, onClick);
				addEventListener(MouseEvent.MOUSE_MOVE, onMouseMove);
				
				//Drag n drop events
				addEventListener(DragEvent.DRAG_ENTER, onDragEnter);
				addEventListener(DragEvent.DRAG_DROP, onDragDrop);
				addEventListener(DragEvent.DRAG_EXIT, onDragExit);
			}
			
			//Returns the container with list of prompt edit controls. Create it
			//if it wasn't created before
			public function get landingBox():PromptCollectionDialogBox{
				if(!_landingBox){
					_landingBox = new PromptCollectionDialogBox();
					_landingBox.promptButtonParent = this;
					_landingBox.title = label;
					
					_landingBox.addEventListener(PromptControlEvent.UPDATE_PARENT_PROMPTS, onUpdateParentPromptButton);
					_landingBox.addEventListener(PromptControlEvent.PARAMETER_NAME_CHANGED, onParameterNameChanged);
					_landingBox.addEventListener(PromptControlEvent.ADDED_PROMPT, onAddedPrompt);
					
					if(!isDefault){
						_landingBox.collectionName = label;
					}
					
					_landingBox.initialize();
				}	
				
				return _landingBox;
			}
			
			public function closeCollectionDialogBox():void{
				if(_landingBox){
					_landingBox.closeWindow();
				}
			}
			
			//Let our parent container update the landing box's prompt properties dialog's 
			//parent combobox
			private function onUpdateParentPromptButton(event:PromptControlEvent):void{
				//we're the last one listening
				event.stopImmediatePropagation();
				
				if(parentLandingBox){
					parentLandingBox.updateParentPromptButton(event.target as PromptButton);
				}
			}
			
			//Let our parent container know when a prompt's parameter has been changed so it'll
			//its hashmap key
			private function onParameterNameChanged(event:PromptControlEvent):void{
				//we're the last one listening
				event.stopImmediatePropagation();
				
				if(parentLandingBox){
					var data:Object = event.data;
					parentLandingBox.promptParameterNameChange(data.oldName, event.data.newName, PromptButton(event.target));
				}
			}
			
			private function onAddedPrompt(event:PromptControlEvent):void{
				var promptButton:PromptButton = PromptButton(event.data);
				
				if(promptButton && parentLandingBox && _landingBox){
					
					//A prompt of the same parameter already exists? Ask the user for 
					//confirmation he is adding a duplicate
					var paramName:String = promptButton.parameterName;
					if(parentLandingBox.hasPromptInMap(paramName)){
						var warning:String = Localization.localizeString('PL_failExists', 'dashboard');
						promptButton.xml.IsExpression = 'true';
						for (var index:int = 0; ; index++) {
							var pName:String = paramName + index;
							if (! parentLandingBox.hasPromptInMap(pName)) {
								paramName = pName;
								break;
							}
						}
						promptButton.xml.ParameterName = paramName
						Util.alertYesNoConfirmation(warning, 
							function(e:CloseEvent):void {
								if (e.detail == Alert.YES) {
									addPromptButton(promptButton, true);
								}
							}); 
						return;		
					}
						
					addPromptButton(promptButton, true);
				}
			}
			
			//Adds a prompt button. Show the tooltip indicating the prompt added 
			//to this button
			public function addPromptButton(bttn:PromptButton, showToolTip:Boolean = false):void{
				if(bttn){
					if(!isDefault)
						bttn.xml.Collection = label;
						
					landingBox.draggedPrompts.addChild(bttn);
					
					if(parentLandingBox){
						parentLandingBox.addPromptToMap(bttn.xml.ParameterName.toString(), bttn);
					}
					
					if(showToolTip){
						showTooltip(bttn.label, true);
					}
				}
			}
			
			//Append the prompt button xml definitions to the supplied root xml element
			public function appendXml(root:XML):void{
				landingBox.appendXml(root);
			}
			
			//Pops up the tool tip informing the prompt being added to this 
			//button. Makes use of the error tooltip but with a different background color 
			private function showTooltip(str:String, disappearLater:Boolean = false):void{
				destroyToolTip();	
				if(str){
					//Get current location
					var pt:Point = localToGlobal(new Point(0,0));
			
					//Create custom error tool tip
					_infoTooltip = ToolTipManager.createToolTip(str, pt.x + 10, pt.y + 20, 'errorTipBelow') as ToolTip;
					_infoTooltip.setStyle('borderColor', 0xFFFFCC);
					_infoTooltip.setStyle('color', 0x000000);
					
					//Mark border
					setStyle('borderColor', 0xff9933);
					
					//Remove tooltip and border styling after one second
					if(disappearLater){
						var timer:Timer = new Timer(1000, 1);
						timer.addEventListener(TimerEvent.TIMER, resetBorderRemoveTooltip);
						timer.start();
					}
				}
			}
			
			//Resets the border color to normal and remove our informational tooltip
			private function resetBorderRemoveTooltip(event:TimerEvent):void{
				setStyle('borderColor', 0x000000);
				destroyToolTip();
			}
			
			//Remove the informational tooltip from view
			private function destroyToolTip():void{
				if(_infoTooltip){
					ToolTipManager.destroyToolTip(_infoTooltip);
					_infoTooltip = null;
				}
			}
			
			//Intercepts the tooltip show event and change to tooltip message to show
			//the prompts' names and # of prompts.
			private function onToolTipShow(event:ToolTipEvent):void{
				if(!_infoTooltip){
					ToolTipManager.currentToolTip.text = tooltipMessage('Click to edit. ');
				}else{
					ToolTipManager.currentToolTip.visible = false;
				}
			}
			
			private function tooltipMessage(prefix:String = ''):String{
			//	var msg:String = '0' + resourceManager.getString(mKey, ('PCD_prompts'));
				var msg:String = '0 prompts';
				if(_landingBox){
					var prompts:Array = _landingBox.getPromptNames();
					
					msg =  prefix + prompts.length + " " + Localization.localizeString('PCD_prompt', 'dashboard')+' \n';
				//	msg =  prefix + prompts.length +  resourceManager.getString(mKey, 'PCD_prompts')+' \n';
					for(var i:int = 0; i < prompts.length; i++){
						msg += prompts[i] + '\n';
					}
				}
				return msg;	
			}
			
			//PCD_prompts
			//Pop up the editable prompt landing box
			private function onClick(event:MouseEvent):void{
				MenuItemActions.invokeAction({klass: PromptCollectionDialogBox, parentButton: this, popUp: this.landingBox, modal: false});		
			}
			
			private function onRollOver(event:MouseEvent):void{
				showTooltip(tooltipMessage());	
			}
			
			private function onRollOut(event:MouseEvent):void{
				revertBorderDestroyToolTip();
			}
			
			private function onMouseMove(event:MouseEvent):void{
				// Get the drag initiator component from the event object.
           		 var dragInitiator:PromptCollectionEditButton = event.currentTarget as PromptCollectionEditButton;
    
        		// Create a DragSource object.
        		var dragSource:DragSource = new DragSource();

       			// Create a bold label of the menubar text to use as a drag proxy.
        		var dragProxy:Label = new Label();
        		dragProxy.setStyle("fontWeight", "bold");
        		dragProxy.text = label;
			
        		// Call the DragManager doDrag() method to start the drag. 
        		DragManager.doDrag(dragInitiator, dragSource, event, dragProxy);
			}
			
			//Drag drop events
			private function onDragEnter(event:DragEvent):void{
				if(event.dragInitiator is PromptCollectionEditButton){
					DragManager.acceptDragDrop(event.currentTarget as UIComponent);
					return;
				}
				
				if(PromptLandingBox.dragEnterOperation(event, this)){
					var msg:String = tooltipMessage();//tooltipMessage('Create and add prompt to collection. \n');
					showTooltip(msg);
				}
			}
			
			//On drop, convert the attributes / measures to our prompt buttons
			private function onDragDrop(event:DragEvent):void{
				if(event.dragInitiator is PromptCollectionEditButton){
					parent.addChildAt(event.dragInitiator as UIComponent, parent.getChildIndex(this));
					return;	
				}else{
					landingBox.onDragDrop(event);
					destroyToolTip();
				}
			}
			
			//Revert back to normal border when drag over
			private function onDragExit(event:DragEvent):void{
				revertBorderDestroyToolTip();
			}
			
			private function revertBorderDestroyToolTip():void{
				setStyle('borderColor', 0x000000);
				destroyToolTip();
			}
			
			public function remove():void{
				if(_landingBox){
					var prompts:Array = _landingBox.draggedPrompts.getChildren();
					for(var i:int = 0; i < prompts.length; i++){
						var bttn:PromptButton = PromptButton(prompts[i]);
						parentLandingBox.removePromptButton(bttn);
					}
				}
				
				parent.removeChild(this);
				if(isDefault){
					parentLandingBox.detachDefault(this);
				}	
			}
		]]>
	</mx:Script>
	
</mx:Button>
