package com.birst.flex.dashboards.components
{
	import com.birst.flex.dashboards.events.DashletEvent;
	import com.birst.flex.dashboards.layout.LayoutDimension;
	import mx.containers.ViewStack;
	
	public interface EditInterface
	{
		function set editViewStack(vs:ViewStack):void;
		function set cancelFunction(f:Function):void;
		function set saveFunction(f:Function):void;
		function set isMovableLayout(b:Boolean):void;
		function set pageName(s:String):void;
		
		function toXML():XML;
   		function set xml(xml:XML):void;
   		function get scaleDashlets():Boolean;
   		function set scaleDashlets(scaleIt:Boolean):void;
   		function onEditDashlet(event:DashletEvent):void;
		function setPageName(name:String):void;
		function setExplicitDimensions(pageWidth:int, pageHeight:int):void;
		function unsetExplicitDimensions():void;
		function getLayoutDimension():LayoutDimension;
		function isExplicitDimensions():Boolean;
	}
}