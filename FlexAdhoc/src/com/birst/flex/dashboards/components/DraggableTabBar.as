package com.birst.flex.dashboards.components
{
	import com.birst.flex.dashboards.events.LinkTabBarDropEvent;
	import com.birst.flex.dashboards.events.DashboardEvent;
	
	import flash.display.DisplayObject;
	import flash.events.MouseEvent;
	
	import mx.containers.ViewStack;
	import mx.controls.Button;
	import mx.controls.Label;
	import mx.controls.TabBar;
	import mx.core.Application;
	import mx.core.DragSource;
	import mx.core.IFlexDisplayObject;
	import mx.events.DragEvent;
	import mx.managers.DragManager;

	[Event(name="drop", type="com.birst.flex.dashboards.events.LinkTabBarDropEvent")]
	[Event(name="dropPage", type="com.birst.flex.dashboards.events.LinkTabBarDropEvent")]
	
	public class DraggableTabBar extends TabBar
	{
		public function DraggableTabBar()
		{
			super();
		}
		
		override protected function createNavItem(label:String, icon:Class=null):IFlexDisplayObject{
			var tab:Button = super.createNavItem(label, icon) as Button;
			if(Application.application.isEditable){
				addDragDropListeners(tab);
			}
			return tab;
		}
		
		private function addDragDropListeners(tab:Button):void{
			tab.addEventListener(MouseEvent.MOUSE_DOWN, onMouseDownTab);
			tab.addEventListener(MouseEvent.MOUSE_UP, onMouseUpTab);
			tab.addEventListener(DragEvent.DRAG_ENTER, onDragEnterTab);
			tab.addEventListener(DragEvent.DRAG_DROP, onDragDropTab);
			tab.addEventListener(DragEvent.DRAG_EXIT, onDragExitTab);
		}
		
		private function onMouseDownTab(event:MouseEvent):void{
			var tab:Button = event.currentTarget as Button;
			tab.addEventListener(MouseEvent.MOUSE_MOVE, onMouseMoveTab);
		}
		
		private function onMouseUpTab(event:MouseEvent):void{
			var tab:Button = event.currentTarget as Button;	
			tab.removeEventListener(MouseEvent.MOUSE_MOVE, onMouseMoveTab);
		}
		
		private function onDragEnterTab(event:DragEvent):void{
			//No dropping on self
			if(event.dragInitiator == event.currentTarget){
				return;
			}
			
			//No dropping on Create Dashboard button (the last one)
			if(getChildIndex(event.target as Button) == numChildren - 1){
				return;
			}
			
			var source:DragSource = event.dragSource as DragSource;
			//Dragged a dashboard onto a dashboard
			if(source.hasFormat('tabIndex')){
				DragManager.acceptDragDrop(mx.core.IUIComponent(event.target));	
				DragBarUtility.hoverIndicator(event.target as Button);
			}
			
			//Dragged a page onto us, a dashboard
			if(source.hasFormat('linkIndex') && source.hasFormat('dashboard')){
				var idx:int = int(source.dataForFormat('linkIndex'));
				var sourceDash:DashboardTab = DashboardTab(source.dataForFormat('dashboard'));
				
				//verify it is not to the same dashboard
				var ourDashboardTab:DashboardTab = this.getOurDashboardTab(event.target);
				if(ourDashboardTab && sourceDash){	
					if(sourceDash != ourDashboardTab){
						DragManager.acceptDragDrop(mx.core.IUIComponent(event.target));	
						DragBarUtility.hoverIndicator(event.target as Button);
					}
				}	
			}
		}

		private function onDragDropTab(event:DragEvent):void{
			DragBarUtility.unhoverIndicator(event.target as Button);
			
			var source:DragSource = event.dragSource as DragSource;
			var targetIndex:int = getChildIndex(event.target as Button);
			
			//Dashboard dropped on dashboard
			if(source.hasFormat('tabIndex')){
				var sourceIndex:int = getChildIndex(event.dragInitiator as Button);	
				dispatchEvent(new LinkTabBarDropEvent(LinkTabBarDropEvent.DROP, false, false, targetIndex, sourceIndex));	
			}
			
			//Other dashboard's page dropped on dashboard
			if(source.hasFormat('linkIndex') && source.hasFormat('dashboard')){
				var sourceDash:DashboardTab = DashboardTab(source.dataForFormat('dashboard'));
				var sourcePageIndex:int = int(source.dataForFormat('linkIndex'));
				var targetDash:DashboardTab = getOurDashboardTab(event.target);
				var evt:LinkTabBarDropEvent = 
					new LinkTabBarDropEvent(LinkTabBarDropEvent.DROP_PAGE, false, false, targetIndex, sourcePageIndex, targetDash, sourceDash);
				dispatchEvent(evt);
			}
		}
		
		private function getOurDashboardTab(target:Object):DashboardTab{
			if(!target) return null;
			
			var ourDash:DashboardTab = null;
			var dashTabs:ViewStack = this.dataProvider as ViewStack;
			if(dashTabs){
				ourDash = dashTabs.getChildAt(getChildIndex(target as DisplayObject)) as DashboardTab;
			}
			return ourDash;
		}
		
		private function onDragExitTab(event:DragEvent):void{
			DragBarUtility.unhoverIndicator(event.target as Button);
		}
		
		private function onMouseMoveTab(event:MouseEvent):void{
			// Get the drag initiator component from the event object.
            var dragInitiator:Button = event.currentTarget as Button;
    
        	// Create a DragSource object.
        	var dragSource:DragSource = new DragSource();

        	// Add the data to the object.
			dragSource.addData(getChildIndex(dragInitiator), 'tabIndex');

       		// Create a bold label of the menubar text to use as a drag proxy.
        	var dragProxy:Label = new Label();
        	dragProxy.setStyle("fontWeight", "bold");
        	dragProxy.text = dragInitiator.label;
			
        	// Call the DragManager doDrag() method to start the drag. 
         	DragManager.doDrag(dragInitiator, dragSource, event, dragProxy);
		}
		
		//Uncomment below if need to be notified of button highlighting. 
		/*
		override protected function hiliteSelectedNavItem(index:int):void{
			super.hiliteSelectedNavItem(index);
			
			if(index > -1){
				var evt:DashboardEvent = new DashboardEvent(DashboardEvent.TAB_HILITE);
				evt.data = Button(getChildAt(index));
				dispatchEvent(evt);
			}
		}*/		
	}
}