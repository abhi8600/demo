package com.birst.flex.dashboards.components
{
	import mx.controls.Button;
	
	public class DragBarUtility
	{
		public function DragBarUtility()
		{
		}

		public static function hoverIndicator(tab:Button):void{
			tab.visible = false;
		}
			
		public static function unhoverIndicator(tab:Button):void{
			tab.visible = true;
		}
	}
}