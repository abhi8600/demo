package com.birst.flex.dashboards.components
{
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.controls.CheckBox;
	import mx.controls.treeClasses.TreeItemRenderer;
	import mx.controls.treeClasses.TreeListData;

	public class PromptTreeRenderer extends TreeItemRenderer
	{
		private var cb:CheckBox;
		private var _data:Object;
		
		public function PromptTreeRenderer()
		{
			//TODO: implement function
			super();
		}
		
		override public function set data(value:Object):void {
			_data = value;
			if (value != null) {
				super.data = value;
				
				if (value.hasOwnProperty("selected")) {
					cb.selected = value["selected"];
				}
				else
					cb.selected = false;
					
				if (value.hasOwnProperty("enabled") && value["enabled"] == false) {
					this.enabled = false;
					cb.selected = false;
					cb.enabled = false;
				}
				else {
					this.enabled = true;
					cb.enabled = true;
				}
			}
		}
		
		override protected function createChildren():void {
			super.createChildren();
			cb = new CheckBox();
			cb.addEventListener(MouseEvent.CLICK, handleClick);
			addChild(cb);
		}
		
		override protected function updateDisplayList(unscaledWidth:Number, unscaledHeight:Number):void {
			super.updateDisplayList(unscaledWidth, unscaledHeight);
			cb.x = super.label.x;
			super.label.x = cb.x + 17;
			cb.y = super.label.y + 8;
		}
		
		private function handleClick(event:MouseEvent):void {
			(listData as TreeListData).item["selected"] = cb.selected;
			this.parent.parent.dispatchEvent(new Event(PromptControl.TreeChangeEvent));
		}
	}
}