<?xml version="1.0" encoding="utf-8"?>
<SaveCancelDialogBox xmlns="com.birst.flex.core.*" xmlns:mx="http://www.adobe.com/2006/mxml" localizedTitleId="DP_title" helpUrl="dashlet_properties_dialog_box" creationComplete="creationComplete();">
	<mx:Script>
		<![CDATA[
			import mx.controls.Alert;
			import mx.rpc.events.ResultEvent;
			import com.birst.flex.webservice.WebServiceResult;
			import com.birst.flex.webservice.WebServiceClient;
			import mx.collections.XMLListCollection;
			import mx.collections.ArrayCollection;
			import mx.core.Application;
			import com.birst.flex.core.CoreApp;
			import mx.events.CloseEvent;
			import com.birst.flex.dashboards.dialogs.DashletLocationFileDialog;
			import com.birst.flex.core.MenuItemActions;
			import com.birst.flex.adhoc.dialogs.FileDialog;
			import mx.controls.Label;
			import mx.containers.GridItem;
			import mx.containers.GridRow;
			import com.birst.flex.dashboards.views.Dashlet;
			import com.birst.flex.core.Util;
			import com.birst.flex.adhoc.dialogs.DialogPopUpEvent;
			
			[Bindable]
			public var dashlet:Dashlet;
			
			[Bindable]
			public var prompts:Array;
			
			[Bindable]
			private var isFreeTrial:Boolean = false;
			
			[Bindable]
			private var app:CoreApp = CoreApp(Application.application);
			
			private var checkboxList:ArrayCollection;
			
			private function creationComplete():void{
				this.addValidators(this.validators);
			}
			
			override public function poppedUpHandler(event:DialogPopUpEvent):void{
				super.poppedUpHandler(event);
				isFreeTrial = false;
				if (app.hasOwnProperty("isFreeTrial") && app["isFreeTrial"] == true)
					isFreeTrial = true;
					
				if(dashlet){
					var props:Array = Util.convertFirstLevelXml2Array(dashlet.toXML());
					setUIValuesById(props);
					showHideDescription();
					
					if(dashlet.isExternalIframe){
						Util.hide(this.pathFormItem);
						Util.hide(this.applyAllPromptsGrid);
						Util.hide(this.checkboxesGrid);
						if(dashlet.isExternalIframe){
							Util.show(this.urlFormItem);
						}
					}else if (dashlet.isInternalPlugin){
						Path.enabled = false;
						ChangePathBttn.enabled = false;
						
						Util.hide(this.GR_pdfView);
						Util.hide(this.GR_csvPivot);
						Util.hide(this.GR_exSel);
						Util.hide(this.GR_pptAdhoc);
						
						this.GI_selfSched.parent.removeChild(this.GI_selfSched);
						
						Util.hide(this.applyAllPromptsGrid);
						Util.hide(this.enablePromptsFormItem);
						Util.hide(this.enableDescriptionModeItem);
						Util.hide(this.enableDescriptionItem);
					}
				}

				populatePrompts(XML(props["ignorePromptList"]));
				var viz:Boolean = !dashlet.isInternalPlugin && Application.application.isEnableAsyncReportGeneration;
				enableDescriptionModeItem.visible = viz;
				enableDescriptionItem.visible = viz;
				enableDescriptionModeItem.includeInLayout = viz;
				enableDescriptionItem.includeInLayout = viz;
			}

			private function populatePrompts(ignorePromptList:XML):void{
				checkboxList = new ArrayCollection();
				
				var vis:Boolean = (prompts && prompts.length > 0) && !dashlet.isInternalPlugin;
				enablePromptsFormItem.visible = vis;
				if(prompts){
					for(var i:int = 0; i < prompts.length; i++){
						var prompt:Object = prompts[i];
						
						var row:GridRow = new GridRow();
						
						var labelItem:GridItem = new GridItem();
						var label:Label = new Label();
						label.text = prompt.label;
						labelItem.addChild(label);
						
						var ckItem:GridItem = new GridItem();
						var ckbox:CheckBox = new CheckBox();
						ckbox.name = prompt.label;
						var b:Boolean = true;
						if (ignorePromptList.child("item").length() > 0) {
							for each (var c:XML in ignorePromptList.child("item")) {
								if (c.toString() == ckbox.name) {
									b = false;
									break;
								}
							}
						}
						ckbox.selected = b;
						ckItem.addChild(ckbox);
						checkboxList.addItem(ckbox);
						
						row.addChild(labelItem);
						row.addChild(ckItem);		
						
						enablePrompts.addChild(row);
					}
				}
			}
			
			override public function saveButtonClickHandler(event:Event):void{
				var props:Array = null;
				
				if(dashlet.isExternalIframe){
					props = getUIValuesById(["Title", "Url", "ViewMode"]);
					this.formValidator.validateForm(event);
					if(!this.formValidator.formIsValid){
						return;
					}
					this.testUrl(props.Url, props, event);
					return;
				}else if (dashlet.isInternalPlugin){
					props = getUIValuesById(["Path", "Title", "ViewMode", "EnableVisualizer"]);
				}else{
					props = getUIValuesById(["Path", "Title", "EnablePDF", "EnableCSV", "EnableExcel", 
												   "EnableViewSelector", "EnablePivotCtrl", "EnableSelectorsCtrl", 
												   "EnableAdhoc", "EnablePPT", "ApplyAllPrompts", "EnableSelfSchedule", 
												   "ViewMode", "EnableVisualizer", "enableDashletDescriptionMode", "DashletDescription"]);
				}
				this.finishSave(props, event);
			}
			
			private function finishSave(props:Array, event:Event):void{
				var ignorePromptProp:XML = new XML("<ignorePromptList/>");
				if(checkboxList && checkboxList.length > 0){
					for (var i:uint = 0; i < checkboxList.length; i++) {
						var ckbox:CheckBox = (checkboxList.getItemAt(i) as CheckBox);
						if (ckbox.selected == false) {
							var item:XML = new XML("<item>" + ckbox.name + "</item>");
							ignorePromptProp.appendChild(item);
						}
					}
				}
				props.ignorePromptList = ignorePromptProp;
				
				dashlet && dashlet.setProperties(props);
				super.saveButtonClickHandler(event);
			}
			
			private function testUrl(url:String, props:Array, event:Event):void{
				var ws:WebServiceClient = new WebServiceClient('Dashboard', 'isUrlIFrameable', 'Dashboards', 'testUrl', {props: props, event: event});
				ws.setResultListener(onIsUrlIFrameable);
				ws.execute(url);	
			}
			
			private function onIsUrlIFrameable(event:ResultEvent):void{
				var result:WebServiceResult = WebServiceResult.parse(event);
				if(!result.isSuccess()){
					Util.localeAlertError('DP_failUrl', result);
					return;
				}
				
				var xml:XMLList = result.getResult();
				
				var urlValid:String = xml.Url.Valid.toString();
				
				var valid:Boolean = urlValid == "true";
				if(!valid){
					var errLocKey:String = urlValid == "RedirectedToHTTP" ? 'DP_errUrlRedirectHttp' : 'DP_errUrlNotExist';
					Util.alertErrMessage(app.getLocString(errLocKey));
					return;
				}
				
				var statusCode:String = xml.Url.StatusCode.toString();
				if(statusCode != '200'){
					Util.alertErrMessage(app.getLocString('DP_errUrlSC') + ' ' + statusCode);	
					return;
				}
				
				var xframeOpt:String = xml.Url['X-Frame-Options'].toString();
				xframeOpt = xframeOpt.toUpperCase();
				if(xframeOpt ==  "SAMEORIGIN" || xframeOpt == "DENY"){
					Util.alertErrMessage(app.getLocString('DP_errUrlIframeDeny'));
					return;
				}
				
				var data:Object = result.getCallerData();
				
				var allowFromIdx:int = xframeOpt.indexOf("ALLOW-FROM ");
				if( allowFromIdx >= 0){
					var hosts:String = xframeOpt.substr(allowFromIdx + 10);
					Util.alertYesNoConfirmation(app.getLocString('DP_urlAllowFrom') + ' ' + hosts, function(evt:CloseEvent):void{
						if(evt.detail == mx.controls.Alert.YES){
							finishSave(data.props, data.event);
						}
					});
					return;
				}
				
				finishSave(data.props, data.event);
			}
			
			private function onClickChange():void{
				var popup:DashletLocationFileDialog = new DashletLocationFileDialog();
				var f:Function = function(event:CloseEvent):void{
					if(popup.selectedFile){
						Path.text = popup.selectedFile;
					}
					popup.removeEventListener(CloseEvent.CLOSE, f);
				};
				popup.addEventListener(CloseEvent.CLOSE, f); 
				MenuItemActions.popUp(DashletLocationFileDialog, { popUp: popup });
			}
			
			private function showHideDescription():void {
				this.enableDescriptionItem.enabled = enableDashletDescriptionMode.selected;
			}
		]]>
	</mx:Script>
	<mx:Form width="100%">
		<LocalizedFormItem id="pathFormItem" localizedLabelId="DP_rpt" width="100%">
			<mx:HBox>
				<mx:TextInput id="Path" editable="false"/>
				<mx:Button id="ChangePathBttn" label="{app.getLocString('DP_chg')}" click="onClickChange();"/>
			</mx:HBox>
		</LocalizedFormItem>
		<LocalizedFormItem id="pluginTypeFormItem" localizedLabelId="DP_plugin" width="100%" visible="false" includeInLayout="false">
			<mx:Label text="Plugin"/>
		</LocalizedFormItem>
		<LocalizedFormItem localizedLabelId="DP_tt" width="100%">
			<mx:TextInput id="Title" width="100%"/>
		</LocalizedFormItem>
		<LocalizedFormItem id="urlFormItem" localizedLabelId="DP_extUrl" localizedTooltipId="DP_ttUrl" width="100%" visible="false" includeInLayout="false">
			<mx:TextInput id="Url" width="300" change="this.validateForm(event);"/>
		</LocalizedFormItem>
		<mx:Grid id="applyAllPromptsGrid" width="100%">
			<mx:GridRow>
				<mx:GridItem horizontalAlign="right">
					<LocalizedFormItem localizedLabelId="DP_applyAllPrompts" localizedTooltipId="DP_ttapplyAllPrompts" width="100%">
						<mx:CheckBox id="ApplyAllPrompts" width="100%"/>
					</LocalizedFormItem>
				</mx:GridItem>
			</mx:GridRow>					
		</mx:Grid>
		<mx:Grid id="checkboxesGrid" width="100%">
			<mx:GridRow id="GR_pdfView">
				<mx:GridItem horizontalAlign="right">
					<LocalizedFormItem localizedLabelId="DP_pdf" horizontalAlign="right">
						<mx:CheckBox id="EnablePDF"/>
					</LocalizedFormItem>
				</mx:GridItem>
				<mx:GridItem horizontalAlign="right">
					<LocalizedFormItem localizedLabelId="DP_sel">
						<mx:CheckBox id="EnableViewSelector"/>
					</LocalizedFormItem>
				</mx:GridItem>
			</mx:GridRow>
			<mx:GridRow id="GR_csvPivot">
				<mx:GridItem horizontalAlign="right">
					<LocalizedFormItem localizedLabelId="DP_csv">
						<mx:CheckBox id="EnableCSV"/>
					</LocalizedFormItem>
				</mx:GridItem>
				<mx:GridItem  horizontalAlign="right">
					<LocalizedFormItem localizedLabelId="DP_pvt">
						<mx:CheckBox id="EnablePivotCtrl"/>
					</LocalizedFormItem>
				</mx:GridItem>
			</mx:GridRow>
			<mx:GridRow id="GR_exSel">
				<mx:GridItem horizontalAlign="right">
					<LocalizedFormItem localizedLabelId="DP_xcl" horizontalAlign="right">
						<mx:CheckBox id="EnableExcel"/>
					</LocalizedFormItem>
				</mx:GridItem>
				<mx:GridItem visible="{!isFreeTrial}" horizontalAlign="right">
					<LocalizedFormItem localizedLabelId="DP_csel">
						<mx:CheckBox id="EnableSelectorsCtrl"/>
					</LocalizedFormItem>
				</mx:GridItem>
			</mx:GridRow>
			<mx:GridRow id="GR_pptAdhoc">
				<mx:GridItem horizontalAlign="right">
					<LocalizedFormItem localizedLabelId="DP_ppt">
						<mx:CheckBox id="EnablePPT"/>
					</LocalizedFormItem>
				</mx:GridItem>
				<mx:GridItem horizontalAlign="right">
					<LocalizedFormItem localizedLabelId="DP_adhoc" horizontalAlign="right">
						<mx:CheckBox id="EnableAdhoc"/>
					</LocalizedFormItem>
				</mx:GridItem>
			</mx:GridRow>
			<mx:GridRow visible="{!isFreeTrial}" includeInLayout="{!isFreeTrial}">
				<mx:GridItem id="GI_selfSched" horizontalAlign="right">
					<LocalizedFormItem localizedLabelId="DP_selfSchedule">
						<mx:CheckBox id="EnableSelfSchedule"/>
					</LocalizedFormItem>
				</mx:GridItem>
				<mx:GridItem id="GI_viz" horizontalAlign="right" visible="{Application.application.isVizEnabled}" includeInLayout="{Application.application.isVizEnabled}">
					<LocalizedFormItem localizedLabelId="DP_vz">
						<mx:CheckBox id="EnableVisualizer"/>
					</LocalizedFormItem>
				</mx:GridItem>
			</mx:GridRow>			
		</mx:Grid>
		<LocalizedFormItem localizedLabelId="DP_viewMode" width="100%">
			<mx:ComboBox id="ViewMode">
				<mx:ArrayCollection>
					<mx:Object label="{app.getLocString('DP_auto')}" value="auto"/> <!-- This follows the global view mode -->
					<mx:Object label="{app.getLocString('DP_default')}" value="full"/>
					<mx:Object label="{app.getLocString('DP_noHeader')}" value="headerless"/>
					<mx:Object label="{app.getLocString('DP_noHeaderBorder')}" value="headerBorderless"/>
				</mx:ArrayCollection>
			</mx:ComboBox>
		</LocalizedFormItem>
		<LocalizedFormItem id="enablePromptsFormItem" localizedLabelId="DP_enablePrompts" width="100%">
			<mx:Grid id="enablePrompts" backgroundColor="0xFFFFFF"  borderStyle="inset" verticalGap="2"  maxHeight="250"/>
		</LocalizedFormItem> 
		<LocalizedFormItem id="enableDescriptionModeItem" localizedLabelId="DP_enableDescriptionMode" width="100%">
			<mx:CheckBox id="enableDashletDescriptionMode" click="showHideDescription();"/>
		</LocalizedFormItem>
		<LocalizedFormItem id="enableDescriptionItem" localizedLabelId="DP_enableDescriptionItem" width="100%">
			<mx:TextArea id="DashletDescription" width="100%" height="40"/>
		</LocalizedFormItem>
	</mx:Form>
	<mx:Array id="validators">
		<mx:StringValidator id="urlStrValidator" source="{Url}" property="text" required="false" minLength="10"/>
		<mx:RegExpValidator id="urlValidator" source="{Url}" property="text" required="false" expression="^https://[\w\d]+\.\w+" />
	</mx:Array>
</SaveCancelDialogBox>
