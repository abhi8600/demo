package com.birst.flex.core
{
	import com.birst.flex.scheduler.utils.SchedulerInit;
	import com.birst.flex.webservice.WebServiceClient;
	import com.birst.flex.webservice.WebServiceResult;
	import com.google.analytics.AnalyticsTracker;
	import com.google.analytics.GATracker;
	
	import flash.events.Event;
	import flash.events.IEventDispatcher;
	import flash.events.KeyboardEvent;
	import flash.ui.Keyboard;
	
	import flash.external.ExternalInterface;
	
	import mx.controls.Alert;
	import mx.controls.ComboBox;
	import mx.core.Application;
	import mx.events.FlexEvent;
	import mx.events.ListEvent;
	import mx.events.ResourceEvent;
	import mx.managers.PopUpManager;
	import mx.resources.IResourceManager;
	import mx.resources.ResourceManager;
	import mx.rpc.events.ResultEvent;    
	
	[ResourceBundle('sharable')]
	public class CoreApp extends mx.core.Application
	{
		// Google Analytics Tracking variables (see trackEvent below)
	    private static var _tracker:AnalyticsTracker = null;
	    private var _GAAccount:String = null;
	    private var _GADebug:Boolean = false;
		private var _GASegment:String = null;

		[Bindable]
		public var mainBundle:String;
		
		[Bindable]
		public var retrieveLocaleInfo:Boolean = false;
		
		[Bindable]
		public var localeDateTimeFormat:String;
		
		[Bindable]
		public var localeDateFormat:String;
		
		[Bindable]
		public var processingTimezoneUTC:String;
		
		[Bindable]
		public var displayTimezoneUTC:String;
		
		[Bindable]
		public var processingTimezoneOffset:int;
		
		[Bindable]
		public var displayTimezoneOffset:int;
		
		[Bindable]
		public var isSelfScheduleEnabled:Boolean;
		
		protected static var EN_US:String = "en_US";
		protected var _name:String;
		protected var _locale:String;
		
		private var _localePopUp:SaveCancelDialogBox;
		private var _loadingLocale:String;
		private var _localeInfo:XML;
		
		private var _defaultReport:XML;
		
		// used during self scheduling - passed to the scheduler
		private var _overrideVariables:XML;

		[Bindable]
		public var useNewQueryLang:Boolean = true;		
		
		[Bindable]
		public var xsrfToken:String = null;
		
		public function CoreApp(){
			addEventListener(mx.events.FlexEvent.INITIALIZE, onInitCore, false, 9999);
			addEventListener(mx.events.FlexEvent.APPLICATION_COMPLETE, createTracker, false); 
		}
		
		protected function onInitCore(event:FlexEvent):void{
			//Trap 
			//Attempt to get the name of our application. The mx.core.Application.application.name 
			//is normally something like 'Dashboards0', 'Dashboard1', etc.
			//Strip out the last digit and use that substring
			var regex:RegExp = /(.*)\d+$/;
			if (name){
				var result:Object = regex.exec(name);
				if (result && result is Array){
					var rst:Array = result as Array;
					_name = (rst.length > 1) ? rst[1] : (rst.length > 0) ? rst[0] : "";
				}
			}
			
			// set the xsrf token
			xsrfToken = ExternalInterface.call("getCookie", "XSRF-TOKEN");
			
			//retrive locale / datetime format information
			retrieveLocaleInfo && loadLocaleInfo();
			
			loadReportDefaults();
			
			//set our locale
			_locale = this.parameters.locale ? this.parameters.locale : EN_US;
			
			//Check if locale is different than English US
			loadNonUSResourceModule();
			
			//Test locale dialog
			setLocaleDialogOnKeyPress();
			
			retreiveOverrideVariables();		
		}
		
		public function createTracker(event:FlexEvent):void{
			try
			{
				if (_tracker == null)
				{
					_GAAccount = Util.getApplicationGoogleAccount();
					if (_GAAccount != null && _GAAccount.length > 0)
						_tracker = new GATracker(this, _GAAccount, "AS3", _GADebug);
				}
				trackEvent("entry");
			}
			catch (e:Error)
			{}
		}
		
		protected function loadLocaleInfo():void{
			var ws:WebServiceClient = new WebServiceClient('ClientInitData', 'getLocaleInfo', '', '');
			ws.setResultListener(onLoadLocaleInfo);
			ws.execute();	
		}
		
		//TODO - failLoadLocaleInfo message
		protected function onLoadLocaleInfo(event:ResultEvent):void{
			var result:WebServiceResult = WebServiceResult.parse(event);
			if(!result.isSuccess()){
				Util.localeAlertError('failLoadLocaleInfo', result, 'sharable');
				return;
			}
			
			var locInfo:XMLList = result.getResult().LocaleInfo;
			if(locInfo.length() > 0){
				_localeInfo = locInfo[0];
				localeDateTimeFormat = Util.getFlexDatePattern(getLocaleInfo('DateTimeFormat'));
				localeDateFormat = Util.getFlexDatePattern(getLocaleInfo('DateFormat'));
				try{
					processingTimezoneUTC = getLocaleInfo('ProcTimeZoneUTC');
					processingTimezoneOffset = int(getLocaleInfo('ProcTimeZoneUTCOffset'));
					displayTimezoneUTC = getLocaleInfo('DispTimeZoneUTC');
					displayTimezoneOffset = int(getLocaleInfo('DispTimeZoneUTCOffset'));
				}catch(e:Error){
					Tracer.error('unable to parse timezone offset: ' + e.message, this);
				}
				useNewQueryLang = getLocaleInfo('UseNewQueryLang') == 'true';
				
				com.birst.flex.core.DateTimePicker.processingUtcOffset; 
			}
		}
		
		protected function retreiveOverrideVariables():void {
			var ws:WebServiceClient = new WebServiceClient('ClientInitData', 'getOverrideVariables', '', '');
			ws.setResultListener(getOverrideVariablesListener);
			ws.execute();	
		}
		protected function getOverrideVariablesListener(event:ResultEvent):void{
			var result:WebServiceResult = WebServiceResult.parse(event);
			if(!result.isSuccess()){
				Util.localeAlertError('failOverrideVariables', result, 'sharable');
				return;
			}
			
			var resultXML:XML = result.getResult()[0];
			if (resultXML && resultXML.OverrideVariables != null && resultXML.OverrideVariables.length() > 0)
			{
		    	_overrideVariables = resultXML.OverrideVariables[0];
		 	}
		}
		
		public function getOverrideVariables():XML
		{
			return _overrideVariables;
		}
		
		protected function loadReportDefaults():void {
			var ws:WebServiceClient = new WebServiceClient('ClientInitData', 'getDefaultReportProperties', '', '');
			ws.setResultListener(onLoadReportDefaults);
			ws.execute();	
		}
		protected function onLoadReportDefaults(event:ResultEvent):void{
			var result:WebServiceResult = WebServiceResult.parse(event);
			if(!result.isSuccess()){
				Util.localeAlertError('failLoadReportDefaults', result, 'sharable');
				return;
			}
			
			var resultXML:XML = result.getResult()[0];
			if (resultXML)
		    	_defaultReport = (resultXML[ReportXml.ROOT])[0];
		}
		
		protected function loadNonUSResourceModule():void{
			var locale:String = getLocale();
			if (locale != EN_US){
				ResourceManager.getInstance().addEventListener(Event.CHANGE, localizeAlert);
				loadResourceModuleByLocale(locale);
			}		
		}
		
		protected function getLocaleResourceBundleUrl(locale:String):String{
			if (locale && birstAppName){
				return '/swf/' + locale + "_" + birstAppName + ".swf";
			}
			return null;			
		}
		
		public function getLocaleInfo(key:String):String{
			if (key && _localeInfo){
				if (_localeInfo[key].length() > 0){
					return _localeInfo[key][0].toString();
				}	
			}
			
			return null;
		}
		
		public function trackEvent(category:String, action:String = null):void {
			if (_tracker)
			{
				try
				{
					if (this.hasOwnProperty("isFreeTrial") && this["isFreeTrial"] == true && _GASegment == null)
					{
						_GASegment = "BXE"
						_tracker.setVar(_GASegment);
					}
					_tracker.trackEvent(birstAppName, category, action);
				}
				catch (e:Error)
				{}
			}
		}
		
		public function getDefaultChartProperties():XMLList {
			if (_defaultReport) {
				var reportId:String = _defaultReport['DefaultChartIndex'];
				var charts:XMLList = _defaultReport.Entities[ReportXml.CHART].(ReportIndex == reportId);
				return charts;
			}
			
			return null;
		}
		
		public function loadResourceModuleByLocale(locale:String):void{
			_loadingLocale = locale;	
			if (locale == EN_US){
				onCompleteLoadLocaleResourceModule(null);
				return;
			}
			loadResourceModule(getLocaleResourceBundleUrl(locale));
		}
		
		public function loadResourceModule(url:String):void{
			if(url){
				var dispatcher:IEventDispatcher = resourceManager.loadResourceModule(url);	
				dispatcher.addEventListener(ResourceEvent.COMPLETE, onCompleteLoadLocaleResourceModule);
				dispatcher.addEventListener(ResourceEvent.ERROR, onErrorLoadLocaleResourceModule); 	
			}
		}
		
		protected function onCompleteLoadLocaleResourceModule(event:ResourceEvent):void{
			resourceManager.localeChain = [ _loadingLocale, EN_US ];
			_locale = _loadingLocale;
		}
		
		protected function onErrorLoadLocaleResourceModule(event:ResourceEvent):void{
			_loadingLocale = null;
		}
		
		public function setLocale(locale:String):void{
			if (resourceManager.getLocales().indexOf(locale) != -1){
				onCompleteLoadLocaleResourceModule(null);
			}else{
				loadResourceModuleByLocale(locale);
			}	
		}
		
		public function getLocale():String{
			return _locale;
		}
		
		public function get birstAppName():String{
			return (_name) ? _name : null;
		}
		
		override protected function resourcesChanged():void{
			dispatchEvent(new Event("localeChange"));
		}
		
		protected function localizeAlert(event:Event):void{
			var bundle:String = 'sharable';
			var mgr:IResourceManager = ResourceManager.getInstance();
			
			Alert.yesLabel = mgr.getString(bundle, 'AL_yes');
			Alert.noLabel = mgr.getString(bundle, 'AL_no');
			Alert.cancelLabel = mgr.getString(bundle, 'AL_cancel');
			Alert.okLabel = mgr.getString(bundle, 'AL_ok');
		}
		
		[Bindable("localeChange")]
		public function getLocString(key:String, params:Array = null, locale:String = null):String{
			return resourceManager.getString(mainBundle, key, params, locale);
		}
		
		[Bindable("localeChange")]
		public function getLocClass(key:String, locale:String = null):Class{
			return resourceManager.getClass(mainBundle, key, locale);
		}
		
		//Locale test dialog
		private function setLocaleDialogOnKeyPress():void{
			addEventListener(flash.events.KeyboardEvent.KEY_DOWN, onKeyDownLocale);
		}	
		
		private function onKeyDownLocale(event:KeyboardEvent):void{
			if (event.keyCode == flash.ui.Keyboard.F8){
				if (!_localePopUp){
					_localePopUp = createLocaleDialog();	
				}
				PopUpManager.addPopUp(_localePopUp, this);
				PopUpManager.bringToFront(_localePopUp);
				PopUpManager.centerPopUp(_localePopUp);
			}
		}	
		
		public function setSelfScheduleMode(value:Boolean):void{
			if(value && !isSelfScheduleEnabled){
				this.isSelfScheduleEnabled = true;
				AppEventManager.fire(new AppEvent(AppEvent.EVT_SELF_SCHEDULE_ALLOWED, "CoreApp", "setSelfSchedule", this));
			}
		}
		
		// Only retrieve the scheduler init properties if the self scheduling is available
		// on dashboard or designer. FYI - Self scheduling is available via group settings on Admin page
		public function getSchedulerInitPropsIfNeeded():void{
			if(isSelfScheduleEnabled){
				SchedulerInit.instance().getInitProps();
			}
		}
		
		private function createLocaleDialog():SaveCancelDialogBox{
			var c:ComboBox = new ComboBox();
			c.addEventListener(ListEvent.CHANGE, function(event:ListEvent):void{
				loadResourceModuleByLocale(c.selectedItem.value);	
			});
			c.addEventListener(FlexEvent.CREATION_COMPLETE, function(event:FlexEvent):void{
				var locales:Array = ResourceManager.getInstance().localeChain;
				if (locales.length > 0){
					Util.comboBoxSetter(locales[0], c);
				}
			});
			c.dataProvider = [{label: 'English', value: 'en_US'},
							  {label: 'Spanish', value: 'es_ES'},
							  {label: 'Canadian French', value: 'fr_CA'}];
							    		
			var w:SaveCancelDialogBox = new SaveCancelDialogBox();
			w.enableSaveCancelButtons = false;
			w.title = "Change locale";
			w.showCloseButton = true;
			w.addChild(c);

			return w;
		}
	}
}