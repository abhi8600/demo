package com.birst.flex.core
{
	import flash.display.DisplayObject;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.controls.Menu;
	import mx.controls.PopUpMenuButton;
	import mx.core.Application;
	import mx.core.IFlexDisplayObject;
	import mx.events.MenuEvent;
	import mx.events.ModuleEvent;
	import mx.managers.CursorManager;
	import mx.managers.PopUpManager;
	import mx.modules.IModuleInfo;
	import mx.modules.ModuleManager;
	
	public class DialogBoxPopUpMenuButton extends PopUpMenuButton
	{
		public var display:Object = null;
		
		protected var _localizedLabel:String = null;
		protected var _saveListener:Function = null;
		protected var _moduleName:String = null;
		protected var _module:IModuleInfo = null;
		protected var _clickEvent:MouseEvent = null;
		protected var _moduleLoaded:Boolean = false;	
		protected var _menuItemsModule:MenuItemsModule;
		
		//Resource bundle is set default to ADHOC
		public var localizedBundle:String = Localization.ADHOC;
				
		public function DialogBoxPopUpMenuButton()
		{	
			super();
			if (display == null)
				display = Application.application;
			this.addEventListener(MenuEvent.ITEM_CLICK, menuItemClickHandler);
		}
		
		public function get localizedLabelId():String{
			return _localizedLabel;
		}
		
		public function set localizedLabelId(key:String):void{
			if (key){
				Localization.bindPropertyString(this, "label", key, localizedBundle);
               	_localizedLabel = key;
			}
		}

		public function get module():String{
			return _moduleName;
		}
		
		public function set module(name:String):void{
			_moduleName = name;			
		} 
		
		override protected function clickHandler(event:MouseEvent):void {
			//only load first time. TODO - a global progress bar, a module manager wrapper
			if (_moduleName != null && !_moduleLoaded){
				_module = ModuleManager.getModule(_moduleName);
				_module.addEventListener(ModuleEvent.READY, moduleLoadedEventHandler);
				_module.addEventListener(ModuleEvent.ERROR, moduleErrorEventHandler);
				//Show that we're busy
				CursorManager.setBusyCursor();
				//Start loading, save the click event for later
				_module.load();
				_clickEvent = event;
			}else{
				super.clickHandler(event);
			}
		}
		
		public function moduleLoadedEventHandler(event:ModuleEvent):void{
			
			if (!_moduleLoaded){
				Tracer.debug('moduleLoadedEventHandler(): ' + _moduleName, this);
				
				//Undo busy cursor
				CursorManager.removeBusyCursor();
				
				_menuItemsModule = _module.factory.create() as MenuItemsModule;
				
				//Set the menu item list
				setProviderOnModuleLoad();	
			
				//do our click handler
				super.clickHandler(_clickEvent);
			
				//mark module loaded
				_moduleLoaded = true;
			}
		}
		
		public function setProviderOnModuleLoad():void{
			var list:ArrayCollection = _menuItemsModule.getList();
			this.dataProvider = list;	
		}
		
		//TODO - there should be a global / predefined handler 
		public function moduleErrorEventHandler(event:ModuleEvent):void{
			//Undo busy cursor
			CursorManager.removeBusyCursor();
			Alert.show("Error loading module " + _moduleName + " Reason:\n" + event.errorText);
		}
		
		/**
		 * Handler for menu item click event
		 */
		public function menuItemClickHandler(event:MenuEvent):void{
			var menuItem:Object = event.item;
			
			if (menuItem.hasOwnProperty("klass")){
			 	if(menuItem.klass is Class)
					_popUp(menuItem.klass, menuItem);
			}else if (menuItem.hasOwnProperty("fn")){
				if (menuItem.fn is Function)
					menuItem.fn(menuItem);
			}
		}
		
		/**
		 * Create a new popup dialog if one doesn't exist.
		 * Cache the new popup and reuse it for future.
		 */
		public function _popUp(klass:Class, menuItem:Object, modal:Boolean = true):void{
			var popup:IFlexDisplayObject = menuItem.popUp;
			if (popup){
					PopUpManager.addPopUp(popup, display as DisplayObject, modal);
			}
			
			if (!popup){
				popup = PopUpManager.createPopUp(display as DisplayObject, klass, modal);
					
				menuItem.popUp = popup;	
			}
			
			//tell the dialog box its opened up
			var event:Event = new Event(SaveCancelDialogBox.POPPED_UP);
			popup.dispatchEvent(event);
			
			PopUpManager.centerPopUp(popup);
		}
		
		/**
		 * Override the dataProvider assignment operation because we will 
		 * look for the localization key for localizing the label
		 */
		override public function set dataProvider(value:Object):void{
			var setRSListener:Boolean = false;
			if (value is ArrayCollection){
				setRSListener = Localization.localizeArrayCollection(ArrayCollection(value));
			}
			super.dataProvider = value;	
			if (setRSListener){
				resourceManager.addEventListener(Event.CHANGE, localizeDataProvider);	
			}
		}
		
		/**
		 * Comb through our data provider list and localize the labels as required
		 */
		protected function localizeDataProvider(event:Event):void{
			var dp:Object = this.dataProvider;
			if (dp is ArrayCollection){
				dp = ArrayCollection(dp);
				for(var i:int; i < dp.length; i++){
					var x:Object = dp.getItemAt(i);
					Localization.localizeLabel(x, localizedBundle);
				}
				//Mark our list as dirty so the menu will redraw its list with our new localized labels
				var menu:Menu = Menu(this.popUp);
				menu.invalidateList();
			}
		}
			
	}
}