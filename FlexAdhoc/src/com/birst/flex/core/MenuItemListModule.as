package com.birst.flex.core
{
	import flash.display.DisplayObject;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Menu;
	import mx.controls.menuClasses.MenuBarItem;
	import mx.core.Application;
	import mx.events.ModuleEvent;
	import mx.managers.CursorManager;
	import mx.modules.IModuleInfo;
	import mx.modules.ModuleManager;
	
	[ResourceBundle('sharable')]
	public class MenuItemListModule
	{
		public var display:Object = null;
		public var children:ArrayCollection = null;
		public var label:String = null;
		
		public var loadFromClick:Boolean = true;
		protected var _localizedLabel:String = null;
		protected var _saveListener:Function = null;
		protected var _moduleName:String = null;
		protected var _module:IModuleInfo = null;
		protected var _clickEvent:MouseEvent = null;
		protected var _moduleLoaded:Boolean = false;	
		protected var _menuItemsModule:MenuItemsModule;
		protected var _menu:Menu = null;
		protected var _menuItem:MenuBarItem;
		
		//Resource bundle is set default to ADHOC
		public var localizedBundle:String = Localization.ADHOC;
				
		public function MenuItemListModule()
		{	
			super();
			if (display == null)
				display = Application.application;
		}
		
		public function get localizedLabelId():String{
			return _localizedLabel;
		}
		
		public function set localizedLabelId(key:String):void{
			if (key){
				Localization.bindPropertyString(this, "label", key, localizedBundle);
               	_localizedLabel = key;
			}
		}

		public function get module():String{
			return _moduleName;
		}
		
		public function set module(name:String):void{
			_moduleName = AppModuleManager.pathify(name);			
		} 
		
		public function isLoaded():Boolean {
			return _moduleLoaded;
		}
		
		public function loadModule(menuItem:MenuBarItem):void{
			if (_moduleName != null && !_moduleLoaded){
				_menu = menuItem.menuBar.getMenuAt(menuItem.menuBarItemIndex);
				_menuItem = menuItem;
				_module = ModuleManager.getModule(_moduleName);
				_module.addEventListener(ModuleEvent.READY, moduleLoadedEventHandler);
				_module.addEventListener(ModuleEvent.ERROR, moduleErrorEventHandler);
				
				if (this.loadFromClick == true){
					//Show that we're busy
					CursorManager.setBusyCursor();
				}
				
				//Start loading, save the click event for later
				_module.load();
				//_clickEvent = event;
			}
		}
		
		public function moduleLoadedEventHandler(event:ModuleEvent):void{
			if (!_moduleLoaded){
				Tracer.debug('moduleLoadedEventHandler(): ' + _moduleName, this);
				
				//mark module loaded
				_moduleLoaded = true;
					
				//Undo busy cursor
				CursorManager.removeBusyCursor();
				
				_menuItemsModule = _module.factory.create() as MenuItemsModule;
			
				//Let the module initialize the menu object with its list
				_menuItemsModule.init(_menu);
			
				//This is for the menubar's dataSource to recognize that
				//there is a list of menu items to display, by setting this object's children
				var list:ArrayCollection = _menuItemsModule.getList();
				children = list;
				
				if (loadFromClick == true){	
					//Manually display the menu
					var pt:Point = new Point(0, 0);
        			pt = DisplayObject(_menuItem).localToGlobal(pt);
        			pt.y += _menuItem.height + 2;
        			_menu.show(pt.x, pt.y);
    			}
    
				//Set the menu item list
				//setProviderOnModuleLoad();	
			
			}
		}
		
		public function setProviderOnModuleLoad():void{
			
			/*
			if (Localization.localizeArrayCollection(list) == true)
				ResourceManager.getInstance().addEventListener(Event.CHANGE, localizeDataProvider);	
			
			_menuItemsModule	
			_menu.dataProvider = list;
			_menu.invalidateList();	
			 
			//Manually display the menu
			var pt:Point = new Point(0, 0);
        	pt = DisplayObject(_menuItem).localToGlobal(pt);
        	pt.y += _menuItem.height + 2;
        	_menu.show(pt.x, pt.y);
        	*/
		}
		
		//TODO - there should be a global / predefined handler 
		public function moduleErrorEventHandler(event:ModuleEvent):void{
			//Undo busy cursor
			CursorManager.removeBusyCursor();
			Util.localeAlertError('MIL_failLoad', null, 'sharable', [_moduleName, event.errorText]);
		}
	}
}