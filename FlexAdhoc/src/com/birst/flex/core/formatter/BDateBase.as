package com.birst.flex.core.formatter
{
	import mx.formatters.DateBase;
	import mx.core.mx_internal;
	use namespace mx_internal;
	
	public class BDateBase extends DateBase
	{
		public function BDateBase()
		{
			super();
		}
	
		public static function extractTokenDate(date:Date, tokenInfo:Object):String
		{
			//initialize();
	
			var result:String = "";
			
			var key:int = int(tokenInfo.end) - int(tokenInfo.begin);
			
			var day:int;
			var hours:int;
			
			switch (tokenInfo.token)
			{
				case "Y":
				{
					// year
					var year:String = date.getUTCFullYear().toString();
					if (key < 3)
						return year.substr(2);
					else if (key > 4)
						return setValue(Number(year), key);
					else
						return year;
				}
	
				case "M":
				{
					// month in year
					var month:int = int(date.getUTCMonth());
					if (key < 3)
					{
						month++; // zero based
						result += setValue(month, key);
						return result;
					}
					else if (key == 3)
					{
						return monthNamesShort[month];
					}
					else
					{
						return monthNamesLong[month];
					}
				}
	
				case "D":
				{
					// day in month
					day = int(date.getUTCDate());
					result += setValue(day, key);
					return result;
				}
	
				case "E":
				{
					// day in the week
					day = int(date.getUTCDay());
					if (key < 3)
					{
						result += setValue(day, key);
						return result;
					}
					else if (key == 3)
					{
						return dayNamesShort[day];
					}
					else
					{
						return dayNamesLong[day];
					}
				}
	
				case "A":
				{
					// am/pm marker
					hours = int(date.getUTCHours());
					if (hours < 12)
						return timeOfDay[0];
					else
						return timeOfDay[1];
				}
	
				case "H":
				{
					// hour in day (1-24)
					hours = int(date.getUTCHours());
					if (hours == 0)
						hours = 24;
					result += setValue(hours, key);
					return result;
				}
	
				case "J":
				{
					// hour in day (0-23)
					hours = int(date.getUTCHours());
					result += setValue(hours, key);
					return result;
				}
	
				case "K":
				{
					// hour in am/pm (0-11)
					hours = int(date.getUTCHours());
					if (hours >= 12)
						hours = hours - 12;
					result += setValue(hours, key);
					return result;
				}
	
				case "L":
				{
					// hour in am/pm (1-12)
					hours = int(date.getUTCHours());
					if (hours == 0)
						hours = 12;
					else if (hours > 12)
						hours = hours - 12;
					result += setValue(hours, key);
					return result;
				}
	
				case "N":
				{
					// minutes in hour
					var mins:int = int(date.getUTCMinutes());
					result += setValue(mins, key);
					return result;
				}
	
				case "S":
				{
					// seconds in minute
					var sec:int = int(date.getUTCSeconds());
					result += setValue(sec, key);
					return result;
				}
				
				case "Q":
				{
					// milliseconds in second
					var ms:int = int(date.getUTCMilliseconds());
					result += setValue(ms, key);
					return result;
				}
			}
	
			return result;
		}
		
		private static function setValue(value:Object, key:int):String
		{
			var result:String = "";
	
			var vLen:int = value.toString().length;
			if (vLen < key)
			{
				var n:int = key - vLen;
				for (var i:int = 0; i < n; i++)
				{
					result += "0"
				}
			}
	
			result += value.toString();
	
			return result;
		}	
	}
}