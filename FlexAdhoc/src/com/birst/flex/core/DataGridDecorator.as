package com.birst.flex.core
{
	import mx.collections.ArrayCollection;
	import mx.controls.DataGrid;
	
	[ResourceBundle('sharable')]
	public class DataGridDecorator
	{
		private var _grid:DataGrid;
		
		public function DataGridDecorator(datagrid:DataGrid)
		{
			_grid = datagrid;
		}
		
		public function deleteSelectedRow():void{
        	var row:Object = _grid.selectedItem;
			
			if (row == null){
				Util.localeAlertError('DGC_selRow', null, 'sharable');
				return;
			}
			
        	var provider:ArrayCollection = _grid.dataProvider as ArrayCollection;
        	var deleteIndex:int = provider.getItemIndex(row);
        	provider.removeItemAt(deleteIndex);
        	
        	//validate for it to refresh and set the ui positions correctly
        	_grid.validateNow();
        	
        	//readjust selected row. 
        	if (provider.length > 0){
        		if (deleteIndex < provider.length){	
        			_grid.selectedIndex = deleteIndex;
        		}else{
        			--deleteIndex;
        			if (deleteIndex < provider.length)
        				_grid.selectedIndex = deleteIndex;
        		}
        	}
        }
        
        //Find the index of the row object, highlight and scroll to it.
		public function highlightScrollToRow(row:Object):void{
			var provider:ArrayCollection = _grid.dataProvider as ArrayCollection;
			if (provider){
				var index:int = provider.getItemIndex(row);
				_grid.validateNow();
				_grid.selectedIndex = index;
				_grid.scrollToIndex(index);	
			}
		}
		
		//Add a single item to and scroll to it.
		public function addItemScrolltoHighlight(data:Object):void{
			var provider:ArrayCollection = _grid.dataProvider as ArrayCollection;
			if (provider){
				provider.addItem(data);
				highlightScrollToRow(data);
			}
		}
        
	}
}