package com.birst.flex.core
{
	import mx.collections.ArrayCollection;
	import mx.controls.Menu;
	
	public interface MenuItemsModule
	{
		function getList():ArrayCollection;
		function init(menu:Menu):void;
	}
}