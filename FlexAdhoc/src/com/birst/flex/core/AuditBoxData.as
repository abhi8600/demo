package com.birst.flex.core
{
	[Bindable]
	public class AuditBoxData
	{
		/**
		 * Constructor, initializes the type class
		 */
		public function AuditBoxData() {}
            
		public var Id:String;
		public var CreatedDate:String;
		public var LastModifiedDate:String;
		public var CreatedUsername:String;
		public var LastModifiedUsername:String;
		public var Name:String;
	}	
}