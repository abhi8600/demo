package com.birst.flex.core
{
	import com.birst.flex.webservice.WebServiceClient;
	import com.birst.flex.webservice.WebServiceResult;
	
	import flash.external.ExternalInterface;
	import flash.net.URLRequest;
	import flash.net.URLRequestHeader;
	import flash.net.URLRequestMethod;
	import flash.net.URLVariables;
	import flash.net.navigateToURL;
	
	import mx.core.Application;
	import mx.rpc.events.ResultEvent;
	import mx.utils.Base64Encoder;
	
	public class ExportFile
	{	
		public function ExportFile(){}

		public static function exportCSV(report:Object, prompts:String, selectorStr:String=null, applyAllPrompts:Boolean=false):void{
			export('/SMIWeb/ExportServlet.jsp', 'csv', report, prompts, selectorStr, applyAllPrompts);	
		}
		
		public static function exportXLS(report:Object, prompts:String, selectorStr:String=null, applyAllPrompts:Boolean=false):void{
			export('/SMIWeb/ExportServlet.jsp', 'xls', report, prompts, selectorStr, applyAllPrompts);	
		}
		
		public static function exportPDF(report:Object, prompts:String, selectorStr:String=null, applyAllPrompts:Boolean=false):void{
			export('/SMIWeb/ExportServlet.jsp', 'pdf', report, prompts, selectorStr, applyAllPrompts);	
		}
		
		public static function exportPPT(report:Object, prompts:String, selectorStr:String=null, applyAllPrompts:Boolean=false):void{
			export('/SMIWeb/ExportServlet.jsp', 'ppt', report, prompts, selectorStr, applyAllPrompts);	
		}
		
		public static function exportServlet(type:String, report:Object, prompts:String, selectorStr:String=null, applyAllPrompts:Boolean=false):void{
			export('/SMIWeb/ExportServlet.jsp', type, report, prompts, selectorStr, applyAllPrompts);	
		}
		
		public static function export(url:String, type:String, report:Object, prompts:String, selectorStr:String, applyAllPrompts:Boolean):void {
			var ws:WebServiceClient = new WebServiceClient('Adhoc', 'preExport', 'ExportFile', 'export', {url: url, type: type});
			ws.setResultListener(preExportHandler);
			ws.execute(type, report is XML ? report : '', report is String ? report : '', 
				prompts == null ? '' : prompts, applyAllPrompts, '', '', selectorStr == null ? '' : selectorStr);
		}
		
		private static function preExportHandler(event:ResultEvent):void { 
			var result:WebServiceResult = WebServiceResult.parse(event);
			if (!result.isSuccess()){
				Util.localeAlertError("WC_export", result);
				return;
			}
			var id:String = result.getPollingId();
			
           	var data:Object = result.getCallerData();
           	var url:String = data.url;
	   		var variables:URLVariables = new URLVariables();
	   		variables["birst.jasperPrintId"] = id;
	   		
            var flashVarRootURL:String = Util.getApplicationSMIWebURL();
            if(flashVarRootURL != null && flashVarRootURL.length > 0)
            {
            	url = flashVarRootURL + url;
            }
            variables["birst.anticsrftoken"] = Application.application.parameters['jsessionId']; // XXX hack for Chrome, Flash player not passing the header below
            
            // try to use submitRequest javascript to export
            if (ExternalInterface.available) {
	            try {
            		// post a message
            		var message:Object = {};
            		message["jasperPrintId"] = variables["birst.jasperPrintId"];
            		message["anticsrftoken"] = variables["birst.anticsrftoken"];
            		message["exportType"] = data.type;
            		message["birstUrl"] = url;
            		ExternalInterface.call("submitRequest", message);
            		return;
	            }
	            catch (error:Error) {
	            }
	        }
            
            // fallback to old way if the other fails for some reason
	   		var request:URLRequest = new URLRequest(url);
	   		request.method = URLRequestMethod.POST;
	   		request.data = variables;
	   		var header:URLRequestHeader = new URLRequestHeader ("X-XSRF-TOKEN",Application.application.parameters['jsessionId']);
			request.requestHeaders.push (header);
			var cookieHeader:URLRequestHeader = new URLRequestHeader("X-BirstSessionId", Application.application.parameters['jsessionId']);
			request.requestHeaders.push (cookieHeader);
			
	   		navigateToURL(request, '_blank');
		}
		
		private static var REPLACE_ALL_PLUS:RegExp = /\+/g;
		
		private static function URLEncode(s:String) : String {
			if (s == null)
				return null;
			var encoder:Base64Encoder = new Base64Encoder();
			encoder.encodeUTFBytes(s);
			return encoder.toString();
		}     
	}
}