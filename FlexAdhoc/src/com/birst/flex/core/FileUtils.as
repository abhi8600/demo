package com.birst.flex.core
{
	import com.birst.flex.catalog.IListing;
	
	import mx.collections.ArrayCollection;
	
	public class FileUtils
	{
        public static const ROOT_DIR:String = "/";
        public static const SLASH:String = ROOT_DIR;
		public static const PRIVATE:String = "private";
		public static const SHARED:String = "shared";
		public static const DASHBOARD_MARKER:String = "dashboard.xml";
		public static const ADHOCREPORT_FILETYPE:String = "adhocreport";
		public static const JRXML_FILETYPE:String = "jrxml";
		
		public static var VALID_FILETYPES:Array = new Array(
			ADHOCREPORT_FILETYPE,
			"jrxml",
			"dashboard",
			"page",
			"pdf",
			"png",
			"gif",
			"jpg"
		);
		
        [Bindable]
        [Embed("/images/dashboard.gif")]
        private static var dashboardIcon:Class;
        [Bindable]
        [Embed("/images/dashboard_folder.png")]
        private static var dashboardFolderIcon:Class;
        [Bindable]
        [Embed("/images/folder.png")]
        private static var folderIcon:Class;
        [Bindable]
        [Embed("/images/image.gif")]
        private static var imageIcon:Class;
        [Bindable]
        [Embed("/images/pdf_small.png")]
        private static var pdfIcon:Class;
        [Bindable]
        [Embed("/images/report.png")]
        private static var reportIcon:Class;
        [Bindable]
        [Embed("/images/square.png")]
        private static var otherIcon:Class;
        [Bindable]
        [Embed("/images/Visualizer_14.png")]
        private static var visualizerIcon:Class;

		public static function getParentDirectory(filepath:String):String {
			var dirPath:String;
            var index:int = filepath.lastIndexOf(SLASH);
            if (index > 0) {
                dirPath = filepath.substr(0, index);
            } 
            return dirPath;
		} 
		
		public static function getFolderIcon(xml:XML):Class {
			var folderIcon:Class;
			if (xml.hasOwnProperty("isDashboard")) {
				folderIcon = dashboardFolderIcon;
			} else {
				folderIcon = folderIcon;
			}
			return folderIcon;
		}
		
		public static function getFilesIcon(listing:IListing):Class {
			// None of these should happen...
			if (listing == null || listing.name == null || listing.name == "") {
				Tracer.error("getFilesIcon(): listing was invalid: " + listing);
				return otherIcon;
			}
			if (listing.isDirectory()) {
				if (listing.isDashboard()) {
					return dashboardFolderIcon;
				} else {
				    return folderIcon;
				}
			}
			var fileExtension:String = getFileExtension(listing.name).toLocaleLowerCase();
			if (fileExtension == null) {
				return otherIcon;
			}
			if (fileExtension == "dashboard" || fileExtension == "page") {
				return dashboardIcon;
			}
			if (fileExtension == "adhocreport" || fileExtension == "jrxml") {
				return reportIcon;
			}
			if (fileExtension == "viz") {
				return visualizerIcon;
			}
			if (fileExtension == "pdf") {
				return pdfIcon;
			}
			if (fileExtension == "png" || fileExtension == "gif" || fileExtension == "jpg") {
				return imageIcon;
			}
			return otherIcon;
		}
        
        public static function getFileExtension(filename:String):String {
            var index:int = filename.lastIndexOf(".");
            if (index == -1) {
                return null;
            }
            var extension:String = filename.substring(index + 1);
            if (extension.toLowerCase() == "dashlet") {
            	var index2:int = filename.lastIndexOf(".", index - 1);
            	if (index2 >= 0) {
            		extension = filename.substring(index2 + 1, index);
            	}
            }
             
            return extension;
        }
        
        public static function getFileBasename(filename:String):String {
            var index:int = filename.lastIndexOf(".");
            if (index < 1) {
                return filename;
            }
            var extension:String = filename.substring(index + 1);
            if (extension.toLowerCase() == "dashlet") {
            	var index2:int = filename.lastIndexOf(".", index - 1);
            	if (index2 >= 0) {
            		index = index2;
            	}
            }
            return filename.substring(0, index);
        }
        
        public static function getFileType(listing:IListing):String {
        	if (listing.isDirectory()) {
        		return "Folder";
        	} else {
        		return getFileExtension(listing.name);
        	}
        }
        
        public static function isValidFileType(fileExtension:String):Boolean
        {
        	var isValid:Boolean = false;
        	for (var i:int = 0; i < VALID_FILETYPES.length; i++)
        	{
        		if (fileExtension == VALID_FILETYPES[i])
        		{
        			isValid = true;
        			break;
        		}
        	}
        	return isValid;
        }

        /**
         * Restricted in the sense that users cannot move or rename these folders.
         */
        public static function isRestrictedFolderMoveOperation(listing:IListing):Boolean {
            if (listing.isFile()) {
                return false;
            }
            var folderPath:String = listing.path.toLowerCase();
            // 'shared' is always restricted
            if (folderPath == SHARED) {
                return true;
            }
            // Is this a 'private/user@domain' directory?
            var folders:Array = folderPath.split('/');
            if (folders.length == 2 && folders[0] == PRIVATE) {
                return true;
            }
            // Is superuser with access to 'private'?
            if (folderPath == PRIVATE) {
                return true;
            }
            // Everything else is fair game
            return false;
        }
        
        /**
         * Restricted in the sense that users cannot delete these folders.
         * Let the BaseFileWS decide about the 'private' folders since it
         * knows whether the current user is 'superuser' or not.
         */
        public static function isRestrictedFolderDeleteOperation(listing:IListing):Boolean {
            if (listing.isFile()) {
                return false;
            }
            var folderPath:String = listing.path.toLowerCase();
            // 'shared' is always restricted
            if (folderPath == SHARED) {
                return true;
            }
            // Everything else is fair game
            return false;
        }
        
        public static function isInPrivateFolder(path:String):Boolean {
        	var index:int = path.toLowerCase().indexOf(PRIVATE);
        	if (index == -1) {
        		return false;
        	} else {
        		return true;
        	}
        }
        
        public static function isAdhocReport(fileName:String):Boolean
        {
            var index:int = fileName.lastIndexOf(".");
            if (index > 0) 
            {
                var fileType:String = fileName.substr(index + 1);
                if (fileType.toLocaleLowerCase() != ADHOCREPORT_FILETYPE)
                    return false;
            }
            return true;
        }
        
        public static function isTopLevelDirectory(path:String, superUser:Boolean=false):Boolean 
        {
            var isTopLevel:Boolean = false;
            if (path == null || path.length == 0 || path == FileUtils.SHARED)
            {
                isTopLevel = true;
            }
            else 
            {
                if (!superUser && path.indexOf("private") == 0 && path.split(SLASH).length == 2) 
                {
                    isTopLevel = true;
                }
                else
                {
                	// Is this a case of 'superuser' or 'proxyuser'?
                    if (superUser && path == FileUtils.PRIVATE) 
                    {
                        isTopLevel = true;
                    }
                }
            }
            return isTopLevel;
        }
        
        public static function removeUserFromPrivate(path:String):String
        {
        	var filteredPath:String = path;
        	if (path != null)
        	{
	    		if (path.indexOf(PRIVATE) == 0)
	    		{
	    			// Replace 'private/user@domain' with 'private'
	    			if (path.split(SLASH).length == 2)
	    			{
	    			    filteredPath = PRIVATE;
	    			} 
	    			else
	    			{
	    				filteredPath = "";
		    			var parts:Array = path.split(SLASH);
		    			for (var i:int = 0; i < parts.length; i++)
		    			{
		    				filteredPath += parts[i] + SLASH;
		    				if (i == 0)
		    				    i++;
		    			}
		    			// Strip off trailing slash
		    		    filteredPath = filteredPath.substr(0, filteredPath.length - 1);
	    			}
	    		}
            }
        	return filteredPath;
        }
        
        public static function getDirectoryLevel(directoryPath:String):int
        {
        	if (directoryPath == null)
        	{
        		return 0;
        	}
        	var pathParts:Array = directoryPath.split(SLASH);
        	if (pathParts != null)
        	{
        		if (pathParts[0] == PRIVATE)
        		{
        			return pathParts.length - 1;
        		}
        		else {
        			return pathParts.length;
        		}
        	}
        	return -1;
        }

        public static function getPathParts(path:String):ArrayCollection 
        {
            if (path == null || path.length == 0) 
            {
                return new ArrayCollection();
            }
            var pathParts:Array = path.split('/');
            var filteredParts:ArrayCollection = new ArrayCollection();
            var firstPart:Boolean = true;
            for (var i:int = 0; i < pathParts.length; i++) 
            {
            	if (pathParts[i].toString().length == 0)
            		continue;
            		
                if (firstPart == true && pathParts[i] == "private" && i + 1 < pathParts.length) 
                {
                    filteredParts.addItem(pathParts[i] + '/' + pathParts[++i]);
                } 
                else 
                {
                    var temp:String = "";
                    for (var j:int = 0; j <= i; j++) 
                    {
                        temp += '/' + pathParts[j];
                    } 
                    filteredParts.addItem(temp.substr(1));
                }
                firstPart = false;
            }
            return filteredParts;
        }
        
        public static function isAdhocReportOrJrxmlFile(fileName:String):Boolean
        {
            var index:int = fileName.lastIndexOf(".");
            if (index > 0) 
            {
                var fileType:String = fileName.substr(index + 1);
                if (fileType.toLocaleLowerCase() == ADHOCREPORT_FILETYPE || fileType.toLocaleLowerCase() == JRXML_FILETYPE)
                    return true;                
            }
            return false;
        }
	}
}