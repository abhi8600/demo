package com.birst.flex.core
{
	public interface IFileNode
	{
        function set name(name:String):void;        

        function get name():String;
        
        function isDirectory():Boolean;
        
        function isFile():Boolean;
        
        function set path(path:String):void;
        
        function get path():String;
        
        function set size(size:int):void;
        
        function get size():int; 
        
        function set createdDate(createdDate:Date):void;
        
        function get createdDate():Date;
        
        function set modifiedDate(modifiedDate:Date):void;
        
        function get modifiedDate():Date;
        
        function get xmlNode():XML;
        
        function toString():String;
	}
}