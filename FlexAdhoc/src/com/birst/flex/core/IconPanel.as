package com.birst.flex.core
{
	import flash.display.Graphics;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	
	import mx.containers.HBox;
	import mx.containers.Panel;
	import mx.controls.Button;
	import mx.resources.ResourceManager;

	public class IconPanel extends Panel
	{
		public var resourceProperty:String = 'dashboard';
		
		public static const WINDOW_STATE_DEFAULT:Number = -1;
		public static const WINDOW_STATE_MINIMIZED:Number = 0;
    	public static const WINDOW_STATE_MAXIMIZED:Number = 1;
    	
    	public var windowState:Number = WINDOW_STATE_DEFAULT; // Corresponds to one of the WINDOW_STATE variables.
    	public var updateTitleBar:Boolean = true;
    	public var iconHeight:int = 14;
    	public var iconWidth:int = 14;
    	
		protected var controlsHolder:HBox;
		protected var headerDivider:Sprite;
		
		public function IconPanel()
		{
			super();
		}
		
		override protected function createChildren():void {
	        super.createChildren();		
	        
	        if (!headerDivider)
	        {
	            headerDivider = new Sprite();
	            titleBar.addChild(headerDivider);
	        }
	        
	       	if (!controlsHolder)
	        {
	            controlsHolder = new HBox();
	            controlsHolder.setStyle("paddingRight", getStyle("paddingRight"));
	            controlsHolder.setStyle("horizontalAlign", "right");
	            controlsHolder.setStyle("verticalAlign", "middle");
	            controlsHolder.setStyle("horizontalGap", 3);
	            rawChildren.addChild(controlsHolder);
	        }
	 	}
	 	
	 	override protected function updateDisplayList(unscaledWidth:Number, unscaledHeight:Number):void{
	        super.updateDisplayList(unscaledWidth, unscaledHeight);
	        updateTitleBar && updateTitleBarDisplay();
	   }
	   
	 	protected function createTitleBarIcon(styleName:String, tooltipKey:String, eventListener:Function):Button{
			var button:Button = new Button();
			button.width = iconWidth;
			button.height = iconHeight;
	        button.styleName = styleName;
	        button.toolTip = ResourceManager.getInstance().getString(resourceProperty, tooltipKey);
	        button.addEventListener(MouseEvent.CLICK, eventListener);
	       	controlsHolder.addChild(button);
	       	
	       	return button;	
		}
		
		protected function updateTitleBarDisplay():void{
			var deltaY:Number = windowState == WINDOW_STATE_MINIMIZED ? -1 : 0;
        	var graphics:Graphics = headerDivider.graphics;
        	graphics.clear();
        	graphics.lineStyle(1, getStyle("borderColor"));
        	graphics.moveTo(1, titleBar.height + deltaY);
        	graphics.lineTo(titleBar.width, titleBar.height + deltaY);
        
        	controlsHolder.y = titleBar.y;
        	controlsHolder.width = unscaledWidth;
        	controlsHolder.height = titleBar.height;
        
        	titleTextField.width = titleBar.width - getStyle("paddingLeft") - getStyle("paddingRight");
		}
	}
}