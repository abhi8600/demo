package com.birst.flex.core
{
	/**
	 * Acts as a wrapper class around the 'FileNode' XML node
	 * to behave like a value object to be used in on-screen components.
	 */
	public class FileNode implements IFileNode
	{
		private var _xmlNode:XML;

		public function FileNode(xmlNode:XML)
		{
			_xmlNode = xmlNode;
		}

        [Bindable]
        public function set name(name:String):void 
        {
        	_xmlNode.label = name;
        }
        
        public function get name():String
        {
        	if (_xmlNode.label == null) {
        		return "";
        	} else {
        	   return _xmlNode.label;
        	}
        }

        public function isDirectory():Boolean {
            if(_xmlNode.isDirectory == true) {
                return true;
            } else {
            	return false;
            }
        }
        
        public function isFile():Boolean {
            if (_xmlNode.isDirectory == true) {
                return false;
            } else {
                return true;
         }
        }

        [Bindable]
        public function set path(path:String):void 
        {
            _xmlNode.name = path;
        }
        
        public function get path():String
        {
            if (_xmlNode.name == null) {
                return "";
            } else {
                return _xmlNode.name;
         }
        }

        [Bindable]
        public function set size(size:int):void
        {
        	_xmlNode.size = size;
        }
        
        public function get size():int 
        {
            return _xmlNode.size;
        }
        
        [Bindable]
        public function set createdDate(createdDate:Date):void
        {
            _xmlNode.createdDate = createdDate;
        }
        
        public function get createdDate():Date
        {
            return _xmlNode.createdDate;
        }
        
        [Bindable]
        public function set modifiedDate(modifiedDate:Date):void
        {
            _xmlNode.modifiedDate = modifiedDate;
        }
        
        public function get modifiedDate():Date
        {
            return _xmlNode.modifiedDate;
        }
        
        public function get xmlNode():XML
        {
        	return _xmlNode;
        }
        
        public function toString():String
        {
            return "name: " + name 
                 + "; isDirectory: " + isDirectory() 
                 + "; path: " + path 
                 + "; size: " + size 
                 + "; createdDate: " + createdDate 
                 + "; modifiedDate: " + modifiedDate;
        }

        public function toXML():XML {
            var xml:XML = new XML("<FileNode></FileNode>");
            xml.appendChild("<label>" + _xmlNode.label + "</label>");
            xml.appendChild("<name>" + _xmlNode.name + "</name>");
            xml.appendChild("<isDirectory>" + _xmlNode.isDirectory + "</isDirectory>");
            xml.appendChild("<createdDate>" + _xmlNode.createdDate + "</createdDate>");
            xml.appendChild("<modifiedDate>" + _xmlNode.modifiedDate + "</modifiedDate>");
            xml.appendChild("<size>" + _xmlNode.size + "</size>");
            
            if (_xmlNode.children != null) {
                var c:XML = new XML("<children></children>");
                for each (var child:FileNode in _xmlNode.children) {
                    c.appendChild(child.toXML());
                }
                xml.appendChild(c);
            }
            return xml;
        }
	}
}