package com.birst.flex.core
{
	import mx.controls.ToolTip;
	import mx.managers.PopUpManager;
	import mx.core.UIComponent;
	import mx.core.SoundAsset;
	
	import flash.utils.Timer;
	import flash.events.TimerEvent;
	
	import mx.effects.Effect;
	import mx.effects.Glow;
	
	public class SoundUtil
	{
		private static var smallPopUp:ToolTip;

		 // Free MP3: from http://www.pacdv.com/sounds/interface_sounds.html
        [Embed("/sounds/wine-glass-clink-2.mp3")]
        private static const doneWave:Class;
        private static const doneSound:SoundAsset = new doneWave() as SoundAsset;
        
        // Free MP3: from http://www.pacdv.com/sounds/interface_sounds.html
        [Embed("/sounds/single_tomtom.mp3")]
        private static const errorWave:Class;
        private static const errorSound:SoundAsset = new errorWave() as SoundAsset;
        
		public function SoundUtil()
		{
		}
		
		public static function playErrorSound():void {
            errorSound.play();
        }
        
        public static function playDoneSound():void {
            doneSound.play();
        }
        
        public static function showErrorToolTip(message:String, caller:UIComponent):void {
            // Create and show tool tip instance
            smallPopUp = new ToolTip();
            smallPopUp.text = message;
            PopUpManager.addPopUp(smallPopUp, caller);
            PopUpManager.bringToFront(smallPopUp);
            PopUpManager.centerPopUp(smallPopUp);
            
            // Create and play effect
            var glowPopUpEffect:Effect = setUpGlowEffect(smallPopUp);
            var unglowPopUpEffect:Effect = setUpUnglowEffect(smallPopUp);
            glowPopUpEffect.play();
            playErrorSound();
            unglowPopUpEffect.play();

            // Wait for an interval and hide the tooltip            
            var timer:Timer = new Timer(1500, 1);
            timer.addEventListener(TimerEvent.TIMER_COMPLETE, removeSmallPopUp);
            timer.start();
        }

        public static function removeSmallPopUp(event:TimerEvent):void {
            if (smallPopUp != null) {
               PopUpManager.removePopUp(smallPopUp);
            }
            smallPopUp = null;
        }

		        public static function setUpGlowEffect(target:Object):Effect 
        {
            var glowObject:Glow = new Glow(target);
            glowObject.duration = 100;
            glowObject.alphaFrom = 0;
            glowObject.alphaTo = 1;
            glowObject.blurXFrom = 0.0;
            glowObject.blurXTo = 30.0;
            glowObject.blurYFrom = 0.0;
            glowObject.blurYTo = 30.0;
            glowObject.strength = 2;
            glowObject.color = 0xFFCC99;
            return glowObject;
        }
        
        public static function setUpUnglowEffect(target:Object):Effect 
        {
            var unglowObject:Glow = new Glow(target);
            unglowObject.duration = 100;
            unglowObject.alphaFrom = 1;
            unglowObject.alphaTo = 0;
            unglowObject.blurXFrom = 30.0;
            unglowObject.blurXTo = 0.0;
            unglowObject.blurYFrom = 30.0;
            unglowObject.blurYTo = 0.0;
            unglowObject.strength = 2;
            unglowObject.color = 0xFFCC99;
            return unglowObject;
        }
	}
}