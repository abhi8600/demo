package com.birst.flex.core
{
    import mx.containers.Canvas;
    import mx.styles.StyleManager;

	[Style(name="fillColors", type="Array", arrayType="uint", format="Color")]
	[Style(name="fillAlphas", type="Array", arrayType="Number")]
	[Style(name="cornerRadius", type="Number")]
	
    public class GradientCanvas extends Canvas
    {
        override protected function updateDisplayList(w: Number, h: Number):void
        {
            super.updateDisplayList (w, h);
			Util.drawGradient(this, w, h);
        }
    }
}