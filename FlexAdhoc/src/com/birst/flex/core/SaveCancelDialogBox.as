package com.birst.flex.core
{
	import com.birst.flex.adhoc.dialogs.DialogPopUpEvent;
	import com.birst.flex.core.expression.ColumnExpressionPanel;
	
	import flash.display.DisplayObject;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.containers.Form;
	import mx.containers.HBox;
	import mx.controls.Button;
	import mx.controls.CheckBox;
	import mx.controls.ColorPicker;
	import mx.controls.ComboBox;
	import mx.controls.Image;
	import mx.controls.NumericStepper;
	import mx.controls.RadioButton;
	import mx.controls.RadioButtonGroup;
	import mx.controls.Spacer;
	import mx.controls.TextArea;
	import mx.controls.TextInput;
	import mx.core.Application;
	import mx.core.UIComponent;
	import mx.events.CloseEvent;
	import mx.managers.PopUpManager;

	public class SaveCancelDialogBox extends ResizableTitleWindow
	{
		//Resource bundle title key
		private var _localizedTitleId:String = null;
	
		//Title bar help image
		[Embed(source="/images/question.png")]
		[Bindable]
		private var helpImageClz:Class;
		private var _helpImage:Image = null;
		
		//Save, Cancel button building blocks		
		private var _enableSaveCancelButton:Boolean = true;
		protected var _saveCancelHBox:HBox = null;
		public var saveButton:Button = null;
		public var cancelButton:Button = null;
		private var _spacer:Spacer = null;
		
		private var _saveButtonLocalizedId:String = "RP_save";
		private var _cancelButtonLocalizedId:String  = "RP_cancel";
	
		//Custom events
		public static var POPPED_UP:String = "poppedUp";
		
		//Variable to set for the localization / resource bundle properties file name
		public var localizedBundle:String = Localization.SHARABLE;
		
		[Bindable]
		public var isMultiSelectEdit:Boolean = false;
		
		//Help page url for the help icon
		public var helpUrl:String = null;
		
		public var formValidator:FormValidator;
		
		
		public var useAppBundleForSaveButton:Boolean = false;
		public var useAppBundleForCancelButton:Boolean = false;
		
		public function SaveCancelDialogBox()
		{
			super();
			
			this.styleName = "saveCancelDialogBoxStyle";
			
			//set the border to solid
			this.setStyle('borderAlpha', 1);
			
			//this.setStyle('modalTransparency', 0.8);
			
			//Show top right [x] button
			this.showCloseButton = true;
			
			//Event handler for when user hits the top right [x] button
			this.addEventListener(CloseEvent.CLOSE, closeWindow);
			
			//Event handler for when our dialog box is invoked and displayed
			this.addEventListener(POPPED_UP, poppedUpHandler);
			
			//Form Validator
			formValidator = new FormValidator();
		}
		
		/**
		 * Get the resource title key
		 */
		public function get localizedTitleId():String{
			return _localizedTitleId;
		}
		
		/**
		 * Sets the resource title key. We bind to the resource manager so that
		 * locale language change is captured and our title string changes as well
		 */
		public function set localizedTitleId(key:String):void{
			if (key){
				Localization.bindPropertyString(this, "title", key);
				_localizedTitleId = key;
			}
		}
		
		public function get saveButtonLocalizedId():String{
			return this._saveButtonLocalizedId;	
		}
		
		public function set saveButtonLocalizedId(key:String):void{
			if (key){
				if (saveButton)
					Localization.bindPropertyString(this.saveButton, "label", key);
					
				this._saveButtonLocalizedId = key;
			}
		}
		
		public function get cancelButtonLocalizedId():String{
			return this._cancelButtonLocalizedId;	
		}
		
		public function set cancelButtonLocalizedId(key:String):void{
			if (key){
				if (cancelButton)
					Localization.bindPropertyString(this.cancelButton, "label", key);
					
				this._cancelButtonLocalizedId = key;
			}
		}
		/**
		 * Parameter for determining if we create the Save and Cancel buttons
		 */
		public function get enableSaveCancelButtons():Boolean{
			return _enableSaveCancelButton;
		}
		
		/**
		 * Sets parameter for creating Save and Cancel buttons, default is yes
		 */
		public function set enableSaveCancelButtons(yesNo:Boolean):void{
			_enableSaveCancelButton = yesNo;
		}
		
		/**
		 * Closes this window
		 */
		public function closeWindow(event:Event = null):void{
			PopUpManager.removePopUp(this);
		}
		
		/**
		 * We have to calculate where to put our help icon
		 */
		override protected function updateDisplayList (unscaledWidth:Number, unscaledHeight:Number):void
         {
            super.updateDisplayList(unscaledWidth, unscaledHeight);
            if (_helpImage != null){
            	var y:int = 7;
            	var x:int = this.width - _helpImage.width - 30;
            	_helpImage.move(x, y);
            }
         }
         
		/**
		 * We append a Save and Cancel button at the bottom right by default,
		 * unless overriden by the enableSaveCancelButton parameter = false
		 */
		override protected function createChildren():void {
    		// Call the createChildren() method of the superclass.
    		super.createChildren();
    		
    		//Create the help url on titlebar
    		if (Application.application.helpEnable && helpUrl != null && _helpImage == null){
    			_helpImage = new Image();
    			_helpImage.source = helpImageClz;
    			_helpImage.width = 14;
    			_helpImage.height = 14;
    			_helpImage.addEventListener(MouseEvent.CLICK, helpImageClickHandler);
    			Localization.bindPropertyString(_helpImage, "toolTip", "TT_help");
    			this.titleBar.addChild(_helpImage);
    		}
    		
    		//Create the Save Cancel buttons at the bottom of the dialog box
        	if (_enableSaveCancelButton){
        		if (!_saveCancelHBox){
        			_saveCancelHBox = new HBox();
        			_saveCancelHBox.percentWidth = 100;
        			_saveCancelHBox.setStyle("paddingBottom", 5);
        			_saveCancelHBox.setStyle("paddingRight", 5);
        		
        			addChild(_saveCancelHBox);
        		
        			_spacer = new Spacer();
        			_spacer.percentWidth = 70; 
        			_saveCancelHBox.addChild(_spacer);
        			
    		 		saveButton = new Button();
    		 		var sbundle:String = (useAppBundleForSaveButton) ? Localization.getAppMainBundle() : localizedBundle;
    		 		Localization.bindPropertyString(saveButton, "label", _saveButtonLocalizedId, sbundle);

    		 		saveButton.addEventListener(MouseEvent.CLICK, saveButtonClickHandler);
    		 		_saveCancelHBox.addChild(saveButton);
    		 		
    		 		//Default button, press enter in the dialog box will call this save.
    		 		defaultButton = saveButton;
    		 	
    		 		cancelButton = new Button();
    		 		var cbundle:String = (useAppBundleForCancelButton) ? Localization.getAppMainBundle() : localizedBundle;
    		 		Localization.bindPropertyString(cancelButton, "label", _cancelButtonLocalizedId, cbundle);
    		 		cancelButton.addEventListener(MouseEvent.CLICK, cancelButtonClickHandler);
    		 		
    		 		_saveCancelHBox.addChild(cancelButton);
        		}
        	}
        	
        	var formItem:DisplayObject = null;
        	var children:Array = this.getChildren();
    		for (var i:int = 0; i < children.length; i++) {
    			var d:DisplayObject = children[i] as DisplayObject;
    			if (d is Form)
    				formItem = d;
    		}
        	this.validateNow();
        	
			if (this.height > Application.application.height && formItem != null) {
				var panelHeight:int = Application.application.height - this.getHeaderHeight();
				if (_saveCancelHBox) {
					panelHeight -= _saveCancelHBox.height;
				}
				panelHeight -= 30;
				formItem.height = panelHeight;
				this.height = Application.application.height;
				this.width += 17;
				formItem.width += 17;
			}
		}

		public function helpImageClickHandler(event:MouseEvent):void{
			Util.gotoHelp(helpUrl, Application.application.helpSiteURL, Application.application.helpWindowName);
		}
		
		/**
		 * Default save button click handler. Closes window and dispatches the 
		 * global dialog saved event.
		 */
		public function saveButtonClickHandler(event:Event):void{
			this.closeWindow(event);
			
			var saveEvent:AppEvent = new AppEvent(AppEvent.EVT_DIALOG_SAVE, 'SaveCancelDialogBox', 'save', this);
			AppEventManager.fire(saveEvent);
		}

		/**
		 * Default cancel button click handler. Closes the window
		 */
		public function cancelButtonClickHandler(event:Event):void{
			this.closeWindow(event);
		}
		
		/**
		 * Our window is popped open
		 */
		public function poppedUpHandler(event:DialogPopUpEvent):void{
			Tracer.debug("[" + this + "] we're on stage!", this);	
			Application.application.trackEvent(this.title);
		}
		
		public function validateForm(event:Event):void{
			this.formValidator.validateForm(event);
			saveButton.enabled = this.formValidator.formIsValid;
		}
		
		public function addValidators(validators:Array):void
		{
			this.formValidator.validators = validators;
		}
		
		public function getUIValuesByIdKeyValue(props:Array):Array{
			//Get attribute names in props.
			var keys:Array = [];
			for(var key:String in props.*){
				keys.push(key);
			}
			return getUIValuesById(keys);
		}
		
		/**
		 * Bases on the hash ref key value pair keys parameter,
		 * get our UI element's values. We will grow the number of supported 
		 * UI elements as we move along.
		 */
		public function getUIValuesById(keys:Array, parent:UIComponent = null):Array{
			if (parent == null)
				parent = this;
				
			var props:Array = new Array();
			for (var i:int = 0; i < keys.length; i++){
				var k:String = keys[i];
				if (parent.hasOwnProperty(k)){
					var c:String = k + "Changed";
					if (parent.hasOwnProperty(c)) {
						// if editing, don't set a value if not the checkbox is not checked
						var changedObj:Object = parent[c];
						if (changedObj && changedObj is CheckBox && (changedObj as CheckBox).visible == true) {
							if (changedObj.selected == false)
								continue;
						}
					}
					var uiObj:Object = parent[k];
					if (uiObj){
						if (uiObj is TextInput || uiObj is TextArea || uiObj is FlexField || uiObj is ColumnExpressionPanel){
							props[k] = uiObj.text;
						}else if (uiObj is ComboBox){
							var selected:Object = uiObj.selectedItem;
							if (selected != null){
								if (selected is String)
									props[k] = selected;
								else{
									if (selected.hasOwnProperty("value"))
										props[k] = selected.value;
								}
							}
						}else if (uiObj is CheckBox){
							props[k] = uiObj.selected;
						}else if (uiObj is ColorPicker){
							props[k] = uiObj.selectedColor.toString(16).toUpperCase();
						}else if (uiObj is FontSelectionHBox){
							props[k] = uiObj.getSelectedFont();
						}else if (uiObj is FormatSelectorHBox){
							props[k] = uiObj.getSelectedFormat();
						}else if (uiObj is RadioButtonGroup|| uiObj is RadioButton){
							var rg:RadioButtonGroup = RadioButtonGroup(uiObj is RadioButtonGroup ? uiObj : RadioButton(uiObj).group);
							if(rg && rg.numRadioButtons > 0){
								props[k] = rg.selectedValue.toString();
							}
						}else if (uiObj is NumericStepper){
							props[k] = uiObj.value;
						}else if (uiObj is FileUploader){
							props[k] = FileUploader(uiObj).filePath.text;
						}					
					}
				}
			}
			return props;			
		}
		
		/**
		 * Bases on the hash ref key value pair keys parameter,
		 * set our UI element's values. We will grow the number of supported 
		 * UI elements as we move along.
		 */
		public function setUIValuesById(props:Object, parent:UIComponent = null):void {
			if (parent == null)
				parent = this;
				
			for (var key:String in props){
				Tracer.debug('setUIValuesById(): ' + key  + ' = ' + props[key], parent);
				if (parent.hasOwnProperty(key)){
					setUIValue(props[key], parent[key]);		
				}
			}
		}
		
		public function setUIValue(value:Object, uiObj:Object):void{
			if (uiObj is TextInput || uiObj is TextArea || uiObj is FlexField || uiObj is ColumnExpressionPanel){
				uiObj.text = value as String;
			}else if (uiObj is ComboBox){
				var cb:ComboBox = uiObj as ComboBox;
				comboBoxSetter(value as String, uiObj as ComboBox);
			}else if (uiObj is CheckBox){
				checkBoxSetter(value.toString(), uiObj as CheckBox);
			}else if (uiObj is ColorPicker){
				var cp:ColorPicker = uiObj as ColorPicker;
				var color:uint = (value is String) ? uint('0x' + value) : uint(value);
				cp.selectedColor = color;
				Tracer.debug('setUIValuesById(): ' + value + ' to ' +  color + ' hex color: ' + cp.selectedColor.toString(16), parent);
			}else if (uiObj is FontSelectionHBox){
				Util.fontSelectionHboxSetter(value as String, uiObj as FontSelectionHBox);
			}else if (uiObj is FormatSelectorHBox){
				Util.formatSelecorHboxSetter(value as String, uiObj as FormatSelectorHBox);
			}else if (uiObj is RadioButtonGroup || uiObj is RadioButton){
				var rg:RadioButtonGroup = RadioButtonGroup(uiObj is RadioButtonGroup ? uiObj : RadioButton(uiObj).group);
				for (var i:int = 0; i < rg.numRadioButtons; i++){
					var rb:RadioButton = rg.getRadioButtonAt(i);
					if (rb.value.toString() == value.toString()){
						rb.selected = true;
					}
				}
			}else if (uiObj is NumericStepper){
				uiObj.value = value;
			}else if (uiObj is FileUploader){
				FileUploader(uiObj).filePath.text = value as String;
			}
		}
		
		protected function checkBoxSetter(value:String, checkbox:CheckBox):void{
			Util.checkBoxSetter(value, checkbox);
		}
		
		protected function comboBoxSetter(value:String, combo:ComboBox):void{
			Util.comboBoxSetter(value, combo);
		}
		
		
	}
}