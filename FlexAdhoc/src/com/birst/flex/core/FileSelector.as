package com.birst.flex.core
{
	import mx.controls.LinkBar;
	
	public class FileSelector extends LinkBar
	{
		private var _uiComponentContainer:UIComponent = new UIComponent();
		public var _linkWidth:Number = 50; 
		public var _currentSelectedDirectory:String = "";
		public var _previouslySelectedDirectory:String = "";
		
		public function FileSelector()
		{
			super();
		}
		
		// split the designated directory into an array of strings			
		public function splitDirectory(dir:String):Array {
		if ((dir == null) || (dir.length==0)){ return new String[0]; }
		return dir.split("/");
		}
		
		// create the children of this one in a special way
		override protected function createChildren():void {
		super.createChildren();
		for(var i:int = 0; i < this.numChildren; i++){
		BindingUtils.bindProperty(getChildAt(i), "width", this, "linkWidth"); 
		}	
		} 
}