package com.birst.flex.core
{
	import mx.core.UIComponent;
	import mx.controls.Alert;
    import mx.rpc.http.HTTPService;
    import mx.rpc.events.ResultEvent;
    import mx.rpc.events.FaultEvent;
    import flash.events.*;
    import flash.utils.*;
    import flash.net.*;
    import mx.utils.*;
   	import mx.core.Application;

	public class Chatter
	{		
		public function Chatter():void
		{
		}
		
		public static function postLink(name:String, link:String, comment:String):void {
   		var req:URLRequest = new URLRequest(getChatterUrl());
    		req.method = URLRequestMethod.POST;
    		var header:URLRequestHeader = new URLRequestHeader ("X-XSRF-TOKEN",Application.application.xsrfToken);
			req.requestHeaders.push (header);
			
    		var vars:URLVariables = new URLVariables();
    		vars["birst.anticsrftoken"] = Application.application.xsrfToken; // XXX hack for Chrome, Flash player not passing the header below
    		
    		vars.name = name;
    		vars.comment = comment;
    		vars.type = "link";
    		vars.link = link;    		
    		req.data = vars;
    		var loader:URLLoader = new URLLoader();
    		loader.dataFormat = URLLoaderDataFormat.BINARY;
 			
    		loader.addEventListener(Event.COMPLETE, loader_complete);
    		loader.addEventListener(IOErrorEvent.IO_ERROR, onIOError);
    		loader.load(req);
		}
		
		private static function loader_complete(event:Event):void {
			// Alert.show('load complete');
		} 
		
		private static function onIOError(event:IOErrorEvent):void
		{
			Alert.show('Post to Chatter Failed: ' + event);		
		}		
		
		public static function postPDF(name:String, comment:String, bytes:ByteArray):void {
    		var req:URLRequest = new URLRequest(getChatterUrl());
    		req.method = URLRequestMethod.POST;
    		var header:URLRequestHeader = new URLRequestHeader ("X-XSRF-TOKEN",Application.application.xsrfToken);
			req.requestHeaders.push (header);
			
    		var vars:URLVariables = new URLVariables();
    		vars["birst.anticsrftoken"] = Application.application.xsrfToken; // XXX hack for Chrome, Flash player not passing the header below
    		
    		vars.name = name;
    		vars.comment = comment;
    		vars.type = "file";
    		var encoder:Base64Encoder = new Base64Encoder();
    		bytes.position = 0;
    		encoder.encodeBytes(bytes);
    		vars.pdfData = encoder.flush();
    		
    		req.data = vars;
    		
    		var loader:URLLoader = new URLLoader();
    		loader.dataFormat = URLLoaderDataFormat.BINARY;
 			
    		loader.addEventListener(Event.COMPLETE, loader_complete);
    		loader.addEventListener(IOErrorEvent.IO_ERROR, onIOError);
    		loader.load(req);
		}
		
		public static function postFile(page:UIComponent, name:String, comment:String):void
		{
			Printing.chatterSingleLetterLandscape(page, name, comment);		
		}
		
		public static function getChatterUrl():String {
			var _curl:String = '/SMIWeb/PostToChatterFeed.jsp';
			var flashVarRootURL:String = Util.getApplicationSMIWebURL();
        	if(flashVarRootURL != null && flashVarRootURL.length > 0){
        		_curl = flashVarRootURL + _curl;
        	}
			return _curl;
		}
	}
}

