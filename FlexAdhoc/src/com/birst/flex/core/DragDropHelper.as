package com.birst.flex.core
{
	import mx.core.DragSource;
	import mx.core.UIComponent;
	import mx.events.DragEvent;
	import mx.managers.DragManager;
	
	public class DragDropHelper
	{
		public function DragDropHelper()
		{
		}

		public static function acceptIfSubjectAreaNode(event:DragEvent, acceptDisabledNode:Boolean = false, colorBorder:Boolean = true):Boolean{
			var target:UIComponent = event.currentTarget as UIComponent;
			var source:DragSource = event.dragSource;
			
			var treeItems:Array = source.dataForFormat("treeItems") as Array;
			
			//Check if the dragged tree item is the leaf node
			var isLeafNode:Boolean = false;
			if (treeItems && treeItems.length > 0){
				var node:XML = treeItems[0];
				isLeafNode = (node.c.length() == 0 && (acceptDisabledNode || node.@vl != "false") && String(node.@t) != "oh");
			}
			
			//Acceptable drop object?
			if (isLeafNode){
				DragManager.acceptDragDrop(target);
				hiliteBorder(target);			
			}
			
			return isLeafNode;		
		}
	
		public static function hiliteBorder(target:UIComponent):void{
			target.setStyle("borderThickness", 3);
			target.setStyle("borderColor", 0xFFC56C);	
		}	
		
		public static function revertBorder(target:UIComponent):void{
			target.setStyle("borderThickness", 1);
			target.setStyle("borderColor", 0xe6f2e7);
		}
		
		public static function getSubjectAreaTreeLeaf(event:DragEvent):XML{
			var ds:DragSource = event.dragSource as DragSource;
			
			var node:XML = null;
			
			//Tree nodes dragged onto us
			if( ds.hasFormat("treeItems") ) {
     			var treeList:Array = ds.dataForFormat("treeItems") as Array;
     			if(treeList.length > 0){
     				node = treeList[0] as XML;
     				
     				//No op on sub folders
     				if (node.c.length() > 0){
     					node = null;
     				}
				}
			}
			
			return node;
		}
		
	}
}