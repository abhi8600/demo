package com.birst.flex.core
{
	import mx.controls.menuClasses.MenuItemRenderer;

	public class MenuItemLabelRenderer extends MenuItemRenderer
	{
		override public function set data(value:Object):void {
			if(value == null) return;
			super.data = value;
			if(this.label && value.hasOwnProperty('toolTip')){
				this.label.toolTip = value.toolTip;
			}
		}
		
		override protected function commitProperties():void{
			//When we removed the last item in the list, it becomes null and super.commitProperties() barfs on null
			if(super.listData != null){ 
				super.commitProperties();
			}
		}
	}
}