package com.birst.flex.core
{
	import com.birst.flex.core.skins.MenuBarLabelBorderSkin;
	
	import mx.controls.Image;
	import mx.controls.MenuBar;

	public class ColumnLabelMenuBar extends MenuBar
	{
		[Bindable]
        [Embed(source="/images/NoSort.gif")]
        public static var noSort:Class;
		[Bindable]
        [Embed(source="/images/AscendingSort.png")]
        public static var ascSort:Class;
		[Bindable]
        [Embed(source="/images/DescendingSort.png")]
        public static var descSort:Class;
        
		private var o:XML;
		private var sortIcon:Image = null;
		
		[Bindable]
		public var showSortIcons:Boolean = true;
		
		public function ColumnLabelMenuBar(index:XML)
		{
			super();
			o = index;
			setStyle("backgroundSkin", com.birst.flex.core.skins.MenuBarLabelBorderSkin);
        	sortIcon = new Image();
        	sortIcon.width = 8;
        	sortIcon.height = 10;
        	addChild(sortIcon);
		}
		
		public function getRenderData(): XML {
			return o;
		}
		
		override protected function updateDisplayList(unscaledWidth:Number, unscaledHeight:Number):void {
			super.updateDisplayList(unscaledWidth, unscaledHeight);

			if(showSortIcons){
				var x:int = unscaledWidth - 10;
				sortIcon.y = 2;
				sortIcon.source = getSortIcon(o);
				var align:String = getStyle("textAlign");
				if (align == "right") {
					x = 4;
				}
							
				sortIcon.x = x;
			}
		}
		
		private function getSortIcon(t:XML): Class {
			var icon:Class = noSort;
			var sortOrder:String = t.sortOrder;
			if (sortOrder == "Ascending") {
				icon = ascSort;
			}
			else if (sortOrder == "Descending") {
				icon = descSort;
			}
			return icon;
		}
	}
}