package com.birst.flex.core
{
	import generated.webservices.ArrayOfString;
	import generated.webservices.ArrayOfUserPreference;
	import generated.webservices.PreferencesService;
	import generated.webservices.Type;
	import generated.webservices.UserPreference;
	
	import mx.core.Application;
	import com.birst.flex.core.Util;
	
	public class UserPreferences
	{
		public static const GLOBAL:int = 0;
		public static const USER:int = 1;
		public static const USER_SPACE:int = 2;
		
        // UserPreferences endpoint
        public static const PREFERENCES_ENDPOINT:String = "PreferencesService.asmx";
        // WSDL URL for UserPreferences
        public static const PREFERENCES_WSDL:String = PREFERENCES_ENDPOINT + "?WSDL";

        // Time Zone
        public static const TIME_ZONE:String = "TimeZone";
        // Locale
        public static const LOCALE:String = "Locale";

        // Key of user preference for 'expert' mode (i.e., display of active help)
        public static const SUBJECT_AREA_EXPERT_MODE:String = "CustomSubjectAreas_ExpertMode";
        
        // Key of user preference for last-accessed directory in Adhoc/Designer
        public static const DESIGNER_LAST_USED_FOLDER:String = "Designer_LastUsedFolder";  
		
		public function UserPreferences()
		{
		}
		
        public static function updateUserPreference(preferenceType:int,
                                                    preferenceKey:String, 
                                                    preferenceValue:String,
                                                    resultHandler:Function=null,
                                                    faultHandler:Function=null):void
        {
            // Build data structure for request
            var preferences:ArrayOfUserPreference = new ArrayOfUserPreference();
            var preference:UserPreference = new UserPreference();
            var type:Type = new Type();
            switch (preferenceType)
            {
            	case 0:
	                type._Type = "Global";
	                break;
	            case 1:
                    type._Type = "User";
                    break;
                case 2:
                    type._Type = "UserSpace";
                    break;
                default:
                    Tracer.error("'updateUserPreference()' 'Type' input encountered: " + preferenceType);
            }
            preference.PreferenceType = type;
            preference.Key = preferenceKey;
            preference.Value = preferenceValue;
            preferences.addUserPreference(preference);
            // Initialize instance of webservice
            var rootUrl:String = Util.getBaseUrl();
            var preferencesService:PreferencesService = new PreferencesService(null, rootUrl + PREFERENCES_ENDPOINT);
            // Add listeners
            preferencesService.addupdateUserPreferencesEventListener(resultHandler);
            preferencesService.addPreferencesServiceFaultEventListener(faultHandler);
            // Send request
            preferencesService.updateUserPreferences(preferences);
        }
    
        public static function getUserPreference(preferenceKey:String,
                                                 resultHandler:Function=null,
                                                 faultHandler:Function=null):void
        {
            // Build data structure for request
            var preferences:ArrayOfString = new ArrayOfString();
            preferences.addItem(UserPreferences.DESIGNER_LAST_USED_FOLDER);
            // Initialize instance of webservice
            var rootUrl:String = Util.getBaseUrl();
            var preferencesService:PreferencesService = new PreferencesService(null, rootUrl + PREFERENCES_ENDPOINT);
            // Add listeners
            preferencesService.addgetUserPreferencesEventListener(resultHandler);
            preferencesService.addPreferencesServiceFaultEventListener(faultHandler);
            // Send request
            preferencesService.getUserPreferences(preferences);
        }
	}
}