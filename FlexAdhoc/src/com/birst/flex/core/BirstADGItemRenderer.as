package com.birst.flex.core
{
	import mx.charts.styles.HaloDefaults;
	import mx.controls.advancedDataGridClasses.AdvancedDataGridItemRenderer;
	import mx.styles.CSSStyleDeclaration;

    [Style(name="rowColor", type="uint", format="Color", inherit="yes")] 
	public class BirstADGItemRenderer extends AdvancedDataGridItemRenderer
	{
		
		private static var stylesInitialized:Boolean = initStyles();
		
		private static function initStyles():Boolean {
			HaloDefaults.init();
			var BirstADGItemRendererExStyle:CSSStyleDeclaration = HaloDefaults.createSelector("BirstADGItemRenderer");
			BirstADGItemRendererExStyle.defaultFactory = function():void {
				this.rowColor = 0xFFFFFF;
			}
			return true;
		}
		public function BirstADGItemRenderer()
		{
			super();
			this.background = true;
		}
		
		override public function validateNow():void {
			backgroundColor = getStyle("rowColor");
			super.validateNow();
		}
		
	}
}