package com.birst.flex.core
{
	import flash.events.Event;

	public class LinkNavigatorEvent extends Event
	{
		public static var DRILL_DOWN:String = "DrillDown";
		public static var NAVIGATE:String = "Navigate";
		public static var OLAP_DRILL_DOWN:String = "OlapDrillDown"; 
		
		public var data:Object;
		
		public function LinkNavigatorEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false, data:Object = null)
		{
			super(type, bubbles, cancelable);
			this.data = data;
		}
		
	}
}