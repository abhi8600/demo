package com.birst.flex.core
{
	import flash.events.Event;

	public class ReportRendererEvent extends Event
	{
		public static var RECTANGLE_SELECT:String = 'rectangleSelect';
		
		public var data:Object;
		public function ReportRendererEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false, data:Object = null)
		{
			super(type, bubbles, cancelable);
			this.data = data;
		}
	}
}