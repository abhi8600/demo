package com.birst.flex.core
{
	import mx.containers.Canvas;
	import mx.controls.ComboBox;

	public class PromptGroupDropDown extends ComboBox
	{
		private var values:Array;
		
		public function PromptGroupDropDown(dd:XML, parentXML:XML, canvas:Canvas)
		{
			super();
			
			this.name = dd.@name;
			var ddOptions:XMLList = dd.Options;
			if (ddOptions != null && ddOptions.children().length() > 0)
			{
				var options:XMLList = ddOptions[0].child("Option");
				if (options != null && options.length() > 0)
				{
					this.values = new Array();
					for (var i:int =0 ; i < options.length() ; i++)
					{
						values.push({ label: options[i].Label, value: options[i].Value});						
					}
					this.dataProvider = values;
				}
			}			
		}
		
	}
}