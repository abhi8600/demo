package com.birst.flex.core.expression
{
	import com.birst.flex.core.IconPanel;
	import com.birst.flex.core.MenuItemActions;
	import flash.events.MouseEvent;
	
	import mx.controls.Button;
	import mx.controls.TextArea;
	
	public class ColumnExpressionPanel extends com.birst.flex.core.IconPanel
	{
		private var editButton:Button;
		private var exprTextArea:TextArea;
		private var _expr:String;
		
		public function ColumnExpressionPanel()
		{
			super();
			styleName = "columnExpr";
		}
		
		override protected function createChildren():void {
	        super.createChildren();		
	       
	       	if(!editButton){
	        	editButton = createTitleBarIcon("editButton", "editButton", onClickEditButton);
	        }
	        
	    	if(!exprTextArea){
	    		exprTextArea = new TextArea();
	    		exprTextArea.percentHeight = exprTextArea.percentWidth = 100;
	    		exprTextArea.text = _expr;
	    		addChild(exprTextArea);
	    	}    
	 	}
	 	
	 	private function onClickEditButton(event:MouseEvent):void{
	 		MenuItemActions.popUp(ExprBuilderDialogBox, {expr: exprTextArea, requestOp: 'evaluateQuery'}, true);	
	 	}
	 	
	 	public function get text():String{
	 		return (exprTextArea) ? exprTextArea.text : _expr;
	 	}
	
		public function set text(expr:String):void{
			_expr = expr;	
			if(exprTextArea){
				exprTextArea.text = _expr;
			}
		}	
	}
}