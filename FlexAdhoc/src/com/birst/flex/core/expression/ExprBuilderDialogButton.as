package com.birst.flex.core.expression
{
	import com.birst.flex.core.MenuItemActions;
	
	import flash.events.MouseEvent;
	
	import mx.controls.Button;
	import mx.controls.TextArea;
	import mx.resources.ResourceManager;
	
	public class ExprBuilderDialogButton extends mx.controls.Button
	{
		[Bindable]
		public var exprTextArea:TextArea;
		
		public function ExprBuilderDialogButton()
		{
		}
		
		override public function initialize():void{
			super.initialize();
			this.minHeight = this.minHeight = 14;
	        this.styleName = "editButton";
	        this.toolTip = ResourceManager.getInstance().getString('sharable', 'EX_editBttn');
	        this.addEventListener(MouseEvent.CLICK, onClickEditButton);
		}
		
		private function onClickEditButton(event:MouseEvent):void{
	 		MenuItemActions.popUp(ExprBuilderDialogBox, {expr: exprTextArea}, true);	
	 	}

	}
}