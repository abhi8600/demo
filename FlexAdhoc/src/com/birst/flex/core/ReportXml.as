package com.birst.flex.core
{
	import mx.collections.ArrayCollection;
	import mx.collections.XMLListCollection;
	import mx.utils.StringUtil;
	
	public class ReportXml
	{

		public static var ROOT:String = "com.successmetricsinc.adhoc.AdhocReport";		
		public static var COLUMN:String = "com.successmetricsinc.adhoc.AdhocColumn";
		public static var LABEL:String = "com.successmetricsinc.adhoc.AdhocLabel";
		public static var IMAGE:String = "com.successmetricsinc.adhoc.AdhocImage";
		public static var SUBREPORT:String = "com.successmetricsinc.adhoc.AdhocSubreport";
		public static var CHART:String     = "com.successmetricsinc.adhoc.AdhocChart";
		public static var RECTANGLE:String = "com.successmetricsinc.adhoc.AdhocRectangle";
		public static var BUTTON:String =    "com.successmetricsinc.adhoc.AdhocButton";
		public static var PIVOT_MEASURE_LABELS_COLWISE:String = "PivotMeasureLabelsColumnWise";
		
		//Report Properties dialog box ui elements
		//HideTitle is "Show title" checkbox. Should it be inversed using not operator?
		public static var REPORT_IDS:Array = [ 
			"Title",  //Report Title
			"HideTitle",  //Show Title
			"IncludeTable", //Show Table
			"AlternateRowColors", //Alternate Table Row Colors
			"FirstAlternateRGB",  //First Color
			"SecondAlternateRGB", //Second Color
			"Replace_NaN_To_Blank", //Replace Division By Zero errors (NaN) To Blank
			"Replace_Infinite_To_Blank", //Replace Division By Zero errors (Infinity) To Blank - updating checkbox status only with NaN flag
			"Replace_Null_To_Zero", //Replace Null Measure values To Zero
			"CSVSeparator", //Separator (\t for Tab)
			"CSV_Filename", //File name
			"CSV_Mimetype",  //MIME Type
			"FiltersAlignment",
			"FiltersForeground",
			"FiltersBackground",
			"FiltersFont",
			"ShowFilters",
			"FilterShowInDashboards",
			"FilterShowOnOneLine",
			"FilterSkipAll",
			"BackgroundColor",
			"BackgroundInvisible",
			"UseFlexGrid",
			"DoNotStyleHyperlinks"
		];

		//Report Page Layout dialog box ui elements
		public static var PAGE_LAYOUT_IDS:Array = [
			"PageSize", //Page Size
			"PageOrientation", //Page Orientation
			"PageWidth", //Page Width (pt)
			"PageHeight",   //Page Height (pt)
			"LeftMargin",  //Left Margin (pt)
			"RightMargin", //Right Margin (pt)
			"TopMargin", //Top Margin (pt)
			"BottomMargin" //Bottom Margin (pt)
		];
		
		//Charts Legends and Labels dialog box ui elements 
		public static var CHART_LEGEND_LABELS_IDS:Array = [
			"legend", //Show Legend
			"legendanchor", //Legend Location
			"legendFont", //Legend Font
			"labelFont", //Label Font
			"categorylabelangle", //Category Label Angle
			"domainLabels", //Show Domain Labels
			"domainTickMarkLabels", //Show Domain Tickmark Labels
			"rangeLabels", //Show Range Labels
			"rangeTickMarkLabels", //Show Range Tickmark Labels
			"domainGroupLabels", // show grouped domain labels
			"chartType",
			"Height",
			"Width",
			"threeD",
			"animation",
			"vertical",
			"tickmarksVisible",
			"leftOutsideMargin",
			"rightOutsideMargin",
			"topOutsideMargin",
			"bottomOutsideMargin",
			"gradient",
			"thresholdType",
			"valueMin",
			"valueMax",
			"valueSecondMin",
			"valueSecondMax",
			"meterMin",
			"meterCritical",
			"meterNormal",
			"meterMax",
			"colorNegative",
			"numSegments",
			"lineWidth",
			"lowerMargin",
			"upperMargin",
			"chartBackgroundColor",
			"plotBackgroundColor",
			"chartBackgroundOpacity",
			"plotBackgroundOpacity",
			"noborder",
			"borderColor",
			"displayValues",			
			"shareAxis",
			"categorylabelangle",
			"series",
			"geoMapType",
			"subMapName",
			"mapColumnName",
			"mapNameColumn",
			"mapProjection",
			"enableMapZooming",
			"enableMapScrolling",
			"showGeoMapLabel",
			"hideCoordinateGrid",
			"colorColumn",
			"majorInterval",
			"minorInterval",
			"secondAxisMajorInterval",
			"secondAxisMinorInterval",
			"showMajorGrid",
			"showMinorGrid",
			"showMajorTickmarks",
			"showMinorTickmarks"
		];
		
		public static var CHART_LEGEND_IDS:Array = [
			"Location",
			"Font",
			"Color",
			"LegendFormat",
			"LegendSource"
		];
		
		public static var CHART_TYPE_IDS:Array = [
			"ChartType",
			"DataSourcePath",
			"threeD",
			"animation",
			"thresholdType",
			"valueMin",
			"valueMax",
			"valueSecondMin",
			"valueSecondMax",
			"meterMin",
			"meterCritical",
			"meterNormal",
			"meterMax",
			"numSegments",
			"lineWidth",
			"lowerMargin",
			"upperMargin",
			"displayValues",			
			"shareAxis",
			"series",
			"geoMapType",
			"subMapName",
			"mapColumnName",
			"mapNameColumn",
			"mapProjection",
			"enableMapZooming",
			"enableMapScrolling",
			"showGeoMapLabel",
			"hideCoordinateGrid",
			"colorColumn",
			"majorInterval",
			"minorInterval",
		];
		
		public static var CHART_SETTINGS:Array = [
			"Height",
			"Width",
			"MarginLeft",
			"MarginRight",
			"MarginTop",
			"MarginBottom",
			"ShowLegend",
			"Orientation",
			"ThreeD",
			"Animation"
		];
		
		public static var CHART_BACKGROUND_SETTINGS:Array = [
			"FillColor",
			"Enabled",
			"BorderColor",
			"BorderThickness",
			"PlotOpacity",
			"BackgroundOpacity",
			"PlotFillColor"
		];
		
		public static var CHART_AXES_SETTINGS:Array = [
				"font",
				"XAxisShare",
				"XAxisEnabled",
				"XAxisPosition",
				"XAxisLabelAngle",
				"XAxisTitle",
				"XAxisLabels",
				"XAxisTickmarkLabels",
				"XAxisGroupLabels",
				"XAxisShowMajorGrid",
				"XAxisShowMinorGrid",
				"XAxisShowMajorTickmarks",
				"XAxisShowMinorTickmarks",
				"XAxisDefaultTitle",
				"XAxisMinorInterval",
				"XAxisMajorInterval",
				"YAxisShare",
				"YAxisEnabled",
				"YAxisEnabled",
				"YAxisPosition",
				"YAxisLabelAngle",
				"YAxisTitle",
				"YAxisLabels",
				"YAxisTickmarkLabels",
				"YAxisGroupLabels",
				"YAxisShowMajorGrid",
				"YAxisShowMinorGrid",
				"YAxisShowMajorTickmarks",
				"YAxisShowMinorTickmarks",
				"YAxisMinorInterval",
				"YAxisMajorInterval",
				"YAxisDefaultTitle",
				"YAxisMaximum",
				"YAxisMinimum",
				"TitleFont",
				"LabelColor",
				"TitleColor",
				"XAxisShowInterlace",
				"XAxisScaleType",
				"YAxisScaleType",
				"XAxisLineStyle",
				"XAxisLineWidth",
				"XAxisLineColor",
				"XAxisZeroLineStyle",
				"XAxisZeroLineWidth",
				"XAxisZeroLineColor",				
				"XAxisMajorGridLineType",
				"XAxisMajorGridLineWidth",
				"XAxisMajorGridLineColor",
				"XAxisMinorGridLineType",
				"XAxisMinorGridLineWidth",
				"XAxisMinorGridLineColor",
				"XAxisMajorTickmarkWidth",
				"XAxisMajorTickmarkColor",
				"XAxisMinorTickmarkWidth",
				"XAxisMinorTickmarkColor",
				"XAxisMaximum",
				"XAxisMinimum",
				"YAxisLineStyle",
				"YAxisLineWidth",
				"YAxisLineColor",
				"YAxisMajorGridLineStyle",
				"YAxisMajorGridLineWidth",
				"YAxisMajorGridLineColor",
				"YAxisMinorGridLineStyle",
				"YAxisMinorGridLineWidth",
				"YAxisMinorGridLineColor",
				"YAxisMajorTickmarkWidth",
				"YAxisMajorTickmarkColor",
				"YAxisMinorTickmarkWidth",
				"YAxisMinorTickmarkColor",
				"XAxisMaxChar",
				"XAxisLabelFormat",
				"YAxisLabelFormat",
				"YAxisZeroLineStyle",			
				"YAxisZeroLineWidth",
				"YAxisZeroLineColor",
				"TitleColor",
				"LabelColor",
				"TitleFont",
				"font"
		];
		
		//Size and Position dialog box ui elements
		public static var SIZE_POSITION_IDS:Array = [
			"ReportIndex", //For determining the id
			"Width",  //Width (pts)
			"Height",  //Height (pts)
			"Left",  //Left (pts)
			"Top",  //Top (pts)
			"PositionType", //Position in band
			"StretchType" //Stretch Type
		];
		
		//Column Properties dialog box ui elements	
		public static var COLUMN_PROPERTIES_IDS:Array = [
			"ReportIndex",
			"Name", 
			"Format", 
			"PivotColumn", //Pivot on Column
			"PivotTotal",  //Pivot Totals
			"DisplayExpression", //Expression
			"ExpressionAggRule", //Group Aggregation Rule
			"Font",
			"Alignment",
			"ForegroundColor", 
			"BackgroundColor",
			"InvisibleBackground",
			"DrillTo",  //Navigate to Dashboard
			"DrillToDashPage",
			"DrillToDash",
			"DataType",	//Date/DateTime
			"AdditionalDrillThruColumns",
			"DateTimeFormat", // format to be used to format date/datetime operands
			"PercentOfColumnValue",
			"IsMeasureExpression",
			"showInReport",
			"ShowRepeatingValues",
			"DenominatorExpression",
			"RemoveColumnOnDrill",
			"AdditionalDrillThruString",
			"WriteBackType",
			"Hierarchy", 
			"DataType", 
			"SourceFormat",
			"DrillToParamName"	
		];
		
		//Label Properties dialog box ui elements
		public static var LABEL_PROPERTIES_IDS:Array = [
			"ReportIndex",
			"Label",
			"Font",
			"Alignment",
			"ForegroundColor",
			"InvisibleBackground",
			"BackgroundColor"
		];
		
		public static var BUTTON_PROPERTIES_IDS:Array = [
			"ReportIndex",
			"Label",
			"Font",
			"Alignment",
			"ForegroundColor",
			"InvisibleBackground",
			"BackgroundColor",
			"CornerRadius",
			"GradientEndColor",
			"DrillTo",  //Navigate to Dashboard
			"DrillToDashPage",
			"DrillToDash",
			"AdditionalDrillThruColumns",
			"UseGradient", 
			"AdditionalDrillThruString",
			"WriteBack",
			"WriteBackName",
			"WriteBackField",
			"WriteBackValidationMessage"
		];
		
		// note that EllipseHeight and EllipseWidth have been combined into a single displayed entity (Roundness).  Only EllipseHeight is used.
		public static var RECTANGLE_PROPERTIES_IDS:Array = [
			"ReportIndex",
			"ForegroundColor",
			"InvisibleBackground",
			"BackgroundColor",
			"EllipseHeight",
			"EllipseWidth",
			"LineColor",
			"LineWidth",
			"GradientEndColor",
			"UseGradient"
		];
		
		public static var REPORT_FILTER_PROPERTIES_IDS:Array = [
			"FiltersAlignment",
			"FiltersForeground",
			"FiltersBackground",
			"FiltersFont",
			"FilterShowInDashboards",
			"FilterShowOnOneLine",
			"FilterSkipAll"
		];
		
		//Border dialog box ui elements
		public static var BORDER_IDS:Array = [
			"ReportIndex",
			"BorderColor",
			"LeftBorder",
			"RightBorder",
			"TopBorder",
			"BottomBorder"
		];
		
		//Filter column dialog ui elements
		/*
			<Filter>
      			<Type>0</Type>
      			<ColumnType>0</ColumnType>
      			<Dimension>Order Date</Dimension>
      			<Column>Year</Column>
      			<Operator>=</Operator>
      			<Operand>1900</Operand>
      			<Name/>
      			<PromptName/>
    		</Filter>
    	*/
		public static var FILTER_COLUMN_IDS:Array = [
			"Type",
			"ColumnType",
			"Dimension", //Data only
			"Hierarchy",
			"Column",
			"Operator",
			"Operand",
			"Name",
			"PromptName",
			"ColumnName", //Display only
			"ColumnPosition", //Display only
			"TableName", //Display only
			"Valid",     //Display only
			"DisplayName", //Display only
			"DataType",	//for Date/DateTime Operands only
			"Operand_DisplayValue",	//for Date/DateTime Operands only
			"DateTimeFormat", // format to be used to format date/datetime operands
			"ReportIndex",
			"IsTrellisChart",
			"InputText"
		];
		
		//Conditional column formatting dialog ui elements
		public static var CONDITIONAL_COLUMN_FORMAT_IDS:Array = [
			"Name",
			"FontSize",
			"FontStyle",
			"FontName",
			"ForegroundColor",
			"BackgroundColor",
			"Expression",
			"Format"
		];
		
		public static var SHOW_IF_FORMAT_IDS:Array = [
			"includeIfExpression",
		];
		
		public static var HYPERLINK_FORMAT_IDS:Array = [
			"hyperlinkExpression",
			"hyperlinkTargetSelf"
		];
		
		public static var HEADER_AND_FOOTER_IDS:Array = [
			"Footer",
			"Title",
			"Subtitle",
			"FooterFont",
			"TitleFont",
			"SubtitleFont",
			"SubTitleAlign",
			"FooterAlign",
			"TitleAlign",
			"FooterColor",
			"TitleColor",
			"SubtitleColor"
		];
		
		public static var SAVED_EXPRESSION_IDS:Array = [
			"Name",
			"Format",
			"Expression",
			"Location",
			"ColumnType",
			"LastModified",
			"UUID",
			"ModifiedBy",
			"CreatedBy",
			"CreatedDate"
		];
		
		public static var GRADIENT_IDS: Array = [
			"Type",
			"Direction",
			"Angle",
			"FocalPoint",
		];
		
		public static var THREE_D_IDS:Array = [
			"AspectMode",
			"Aspect",
			"Rotation",
			"Elevation",
		];
		
		private var _config:XML = <{ROOT}/>;
		
		public function ReportXml(config:XML = null){
			if(config){
				_config = config;
			}
		}
		
		public function set xml(conf:XML):void{
			if( conf.name() == ROOT){
				_config = conf;
			}
		}
		
		public function get xml():XML{
			return _config;
		}
		
		/*
		public static function asyncSaveConfigToServer(caller:String, method:String, data:Object = null):void{	
			var ws:WebServiceClient = new WebServiceClient("Adhoc", "setAdhocReport", caller, method, data);
			ws.setResultListener(_asyncSaveConfigToServerHandler);
			ws.execute(_config.toXMLString());
		}
		
		public static function asyncUpdateConfig(caller:String, method:String):void{
			var ws:WebServiceClient = new WebServiceClient("Adhoc", "getAdhocReport", caller, method);
			ws.setResultListener(updateConfigResultHandler);
			ws.execute();
		} */
		
		public function getNextElementId(): uint {
			var nextId:String = _config.NextId;
			var ret:uint = uint(nextId);
			var next:uint = ret + 1;
			_config.NextId = next;
			return ret;
		}
		
		/*
		private static function _asyncSaveConfigToServerHandler(event:ResultEvent):void{
			Tracer.debug("setAdhocReport called", this);
			var result:WebServiceResult = WebServiceResult.parse(event);
			var component:String = result.getCallerComponent();
			var operation:String = result.getCallerOperation();
			var data:Object = result.getCallerData();
			
			if (!result.isSuccess()){
				var msg:String = "Failed to save configuration. Reverting to previously saved and valid configuration. Your changes did not take effect.";
				Util.alertErrMessage(msg, result);
				_config = new XML(result.getResult().children().toXMLString());
				AppEventManager.fire(new AppEvent(AppEvent.EVT_CONFIG_SAVE_FAILED, component, operation, data));
				return;
			}
			_config = new XML(result.getResult().children().toXMLString());
			AppEventManager.fire(new AppEvent(AppEvent.EVT_CONFIG_SAVE_SUCCESS, component, operation, data));	
		}
		
		public static function updateConfigResultHandler(event:ResultEvent):void{
			var result:WebServiceResult = WebServiceResult.parse(event);
	
			if (result.isSuccess() != true){
				Util.alertErrMessage("Error updating config", result);
				return;
			}
		
			var resultXML:XMLList = result.getResult();
			var topResult:XML = resultXML[0];
			_config = (topResult[AdHocConfig.ROOT])[0];
			
			AppEventManager.dispatchAppEvent(new AppEvent(AppEvent.EVT_CONFIG_CHANGED, result.getCallerComponent(), result.getCallerOperation(), AdHocConfig));
		}
		*/
		
		public function setPageLayoutConfig(conf:Array):void{
			copyAllArrayProperties(conf);
			//asyncSaveConfigToServer(Components.ADHOC_APP, 'setPageLayoutConfig');
		}
		
		public function getPageLayoutConfig():Array{
			var conf:Array = new Array();
			for (var i:int; i < PAGE_LAYOUT_IDS.length; i++){
				var key:String = PAGE_LAYOUT_IDS[i];
				var element:XMLList = _config[key];
				var text:String = element.text();
				conf[key] = text;
			}
			return conf;
		}
		
		private function getFirstLevelXMLValues(props:Array, root:Object = null):Array{
			var topNode:Object = root;
			if (root == null)
				topNode = _config;
				
			var conf:Array = new Array();
			for (var i:int; i < props.length; i++){
				var key:String = props[i];
				var element:XMLList = topNode[key];
				var text:String = element.text();
				conf[key] = text;
			}
			return conf;
		}
		
		public function setReportPropertiesConfig(conf:Array):void{
			copyAllArrayProperties(conf);
			//asyncSaveConfigToServer(Components.ADHOC_APP, 'setPropertiesConfig');
		}
		
		/**
		 * Convert a key-value pair to the ChartOptions tag values of form key1=value1;key2=value2;...
		 */
		public function setChartsConfig(conf:Array):void{
			var currentConf:Array = getChartsConfig();
			
			//Remove extraneous
			delete currentConf['Categories'];
			delete currentConf['Series'];
			delete currentConf['Series2'];
			delete currentConf['Colors'];
			
			//update with our current conf values
			for (var key:String in conf){
				//skip series and category as they are a each a collection of 'series' and 'category' values
				if (key == 'Categories' || key == 'Series' || key == 'Colors' || key == 'Series2')
					continue;
				currentConf[key] = conf[key];
			}
			
			//rewrite into a new conf string- key1=value1;key2=value2..
			var newConf:String = '';
			for (var k:String in currentConf){
				newConf += k + "=" + currentConf[k] + ";"; 
			}
			//nip the last ;
			newConf = newConf.substr(0, newConf.length - 1);
			
			joinToNVPairs(conf, 
										[{ confKey: 'Series', key: 'series' }, 
										 { confKey: 'Categories', key: 'category' },
										 { confKey: 'Colors', key: 'color' },
										 { confKey: 'Series2', key: 'series2' }]);
										 
			newConf = appendValueToConfString(conf, ['Series', 'Series2', 'Categories', 'Colors'], newConf);
										  
			Tracer.debug('setChartLegendsLabelsConfig() new configuration - \n' + newConf, this);
			_config.ChartOptions = newConf;
			//asyncSaveConfigToServer(Components.ADHOC_APP, 'setChartsConfig');
		}
		
		private function joinToNVPairs(conf:Array, params:Array):void{
			for(var i:int = 0; i < params.length; i++){
				var confKey:String = params[i].confKey as String;
				var key:String = params[i].key as String;
				
				var values:String = conf[confKey];
				if (values && (values.indexOf("=") < 0)){
					conf[confKey] = Util.joinNameValuePair(values.split(";"), key);	
				}
			}
		}
		
		private function appendValueToConfString(conf:Array, params:Array, confString:String, sep:String = ';'):String{
			for(var i:int = 0; i < params.length; i++){
				var val:String = conf[params[i]];
				if (val){
					confString += sep + val;
				}
			}
			return confString;
		}
		
		/**
		 * Parses the ChartOptions tag values from key1=value1;key2=value2;...
		 * to an hash Array of array['key1'] = value1
		 */
		public function getChartsConfig():Array{
			var chartOptStr:String = _config.ChartOptions;
			var params:Array = chartOptStr.split(";");
				
			var conf:Array = new Array();
			for (var i:int = 0; i < params.length; i++){
				var p:String = params[i];
				var keyValuePair:Array = p.split("=");
				if (keyValuePair.length == 2){
					var key:String = keyValuePair[0];
					var value:String = keyValuePair[1];
					
					//special case, series and categories can be duplicates
					if (key == 'series'){
						emptyStringIfNull(conf, 'Series');
						conf['Series'] += value + ';';
					}else if (key == 'series2'){
						emptyStringIfNull(conf, 'Series2');
						conf['Series2'] += value + ';';
					}else if(key == 'category'){
						emptyStringIfNull(conf, 'Categories');
						conf['Categories'] += value + ';';	
					}else if (key == 'color'){
						emptyStringIfNull(conf, 'Colors');
						conf['Colors'] += value + ';';
					}else{
						conf[key] = value;		
					}
					
				}
			}
			
			createIfEmptyAndTrim(['Series', 'Categories', 'Colors', 'Series2'], conf);
			
			return conf;
		}
		
		public function newChartsSeriesCategoriesLabel(oldLabel:String, newLabel:String):void{
			var chartOptStr:String = _config.ChartOptions;
			var params:Array = chartOptStr.split(";");
		
			var dirty:Boolean = false;
			for (var i:int = 0; i < params.length; i++){
				var p:String = params[i];
				var keyValuePair:Array = p.split("=");
				if (keyValuePair.length == 2){
					var key:String = keyValuePair[0];
					var value:String = keyValuePair[1];
					if (key == 'series' || key == 'category'){
						if (value == oldLabel){
							dirty = true;
							params[i] = key + '=' + newLabel;
						}
					}
				}
			}
			
			if (dirty){
				var newConfig:String = params.join(";");
				_config.ChartOptions = newConfig;
			}
			
		}
		
		private function createIfEmptyAndTrim(keys:Array, conf:Array):void{
			for(var i:int = 0; i < keys.length; i++){
				var key:String = keys[i];
				emptyStringIfNull(conf, key);
				var value:String = conf[key];
				if (value != ""){
					if (value.charAt(value.length - 1) == ';')
						conf[key] = value.substring(0, value.length - 1);
				}
			}	
				
		}
		
		private function appendDuplicateValues(conf:Array, key:String, value:String):void{
			emptyStringIfNull(conf, key);
			conf[key] += value + ';';
		}
		
		private function emptyStringIfNull(conf:Array, key:String):void{
			if (conf[key] == null)
				conf[key] = '';
		}
		
		public function getReportPropertiesConfig():Array{
			var conf:Array = new Array();
			for (var i:int; i < REPORT_IDS.length; i++){
				var key:String = REPORT_IDS[i];
				var element:XMLList = _config[key];
				var text:String = element.text();
				conf[key] = text;
			}
			return conf;
		}
		
		public function getDefaultChartId():String {
			return _config['DefaultChartIndex'];
		}
		public function setDefaultChartId(id:String):void {
			_config['DefaultChartIndex'] = id;
		}
		
		public function setLabelProperties(labelId:String, properties:Array):void{
			var label:XMLList = getLabelXML(labelId);
			if (label.length() == 0){
				label = new XMLList(<{LABEL} id={labelId}/>);
				getRootEntity().appendChild(label);
			}
			copyAllArrayProperties(properties, label);
			//asyncSaveConfigToServer(Components.ADHOC_APP, 'setLabelProperties');
		}
		
		public function setButtonProperties(id:String, properties:Array):void {
			var label:XMLList = getAdhocButtonXML(id);
			if (label.length() == 0){
				label = new XMLList(<{BUTTON} id={id}/>);
				getRootEntity().appendChild(label);
			}
			copyAllArrayProperties(properties, label);
		}
		
		public function setRectangleProperties(id:String, properties:Array):void {
			var label:XMLList = getRectangleXML(id);
			if (label.length() == 0){
				label = new XMLList(<{RECTANGLE} id={id}/>);
				getRootEntity().appendChild(label);
			}
			copyAllArrayProperties(properties, label);
		}
		
		public function getLabelNameByColumnId(columnId:String):String{
			var labelName:String = '';
			var column:XMLList = getColumnXML(columnId);
			if (column.length() > 0){
				labelName = getLabelName(column[0].LabelName.toString());
			}	
			return labelName;
		}
		
		public function getLabelNameByColumnXML(colXML:XML):String{
			var labelName:String = '';
			if (colXML){
				labelName = getLabelName(colXML.LabelName.toString());
			}
			if (! labelName || labelName.length == 0)
				labelName = colXML.Name;
				
			return labelName;
		}
		
		public function getLabelName(labelId:String):String{
			var labels:XMLList = getLabelXML(labelId);
			if (labels.length() > 0){
				return labels[0].Label;
			}
			return '';
		}
		
		public function getLabelXML(labelId:String):XMLList{
			return _config.Entities[LABEL].(ReportIndex == labelId);	
		}
		
		public function getRectangleXML(id:String):XMLList {
			return _config.Entities[RECTANGLE].(ReportIndex == id);
		}
		
		public function getAdhocButtonXML(id:String):XMLList {
			return _config.Entities[BUTTON].(ReportIndex == id);
		}
		
		public function getImageXML(imageId:String):XMLList{
			return _config.Entities[IMAGE].(ReportIndex == imageId);	
		}
		
		public function getSubreportXML(reportId:String):XMLList{
			return _config.Entities[SUBREPORT].(ReportIndex == reportId);	
		}
		
		public function getChartXML(reportId:String):XMLList {
			return _config.Entities[CHART].(ReportIndex == reportId);
		}
		
		public function setColumnProperties(columnId:String, properties:Array):void{
			Tracer.debug("setColumnProperties() of id: " + columnId, this);
			var column:XMLList = getColumnXML(columnId);
			if (column.length() == 0){
				Tracer.debug("setColumnProperties() bad.. not suppose to be here..", this);
				column = new XMLList(<{COLUMN} id={columnId}/>);
				getRootEntity().appendChild(column);				
			}
			copyAllArrayProperties(properties, column);
			//asyncSaveConfigToServer(Components.ADHOC_APP, 'setColumnProperties');
		}
		public function setChartProperties(columnId:String, properties:Array):void{
			Tracer.debug("setChartProperties() of id: " + columnId, this);
			var column:XMLList = getChartXML(columnId);
			if (column.length() == 0){
				Tracer.debug("setChartProperties() bad.. not suppose to be here..", this);
				column = new XMLList(<{CHART} id={columnId}/>);
				getRootEntity().appendChild(column);				
			}
			copyAllArrayPropertiesRecursively(properties, column);
			//asyncSaveConfigToServer(Components.ADHOC_APP, 'setColumnProperties');
		}
		
		public function getRootEntity():XMLList{
			return getCreateChildElement("Entities");
		}
	
		public function getRootFilters():XMLList{
			return getCreateChildElement("Filters");
		}	
		
		public function setSelectedLayoutObjectProperties(properties:Array, selectedLayoutObject:XMLList):void{
			copyAllArrayProperties(properties, selectedLayoutObject);
		}
		
		//Gets the selected layout object's size and position properties
		public function getSelectedLayoutObjectSizePosition(layoutObj:XMLList):Array{
			return getFirstLevelXMLValues(SIZE_POSITION_IDS, layoutObj);
		}
		
		public function getSelectedLayoutShowIf(layoutObj:XMLList):Array{
			return getFirstLevelXMLValues(SHOW_IF_FORMAT_IDS, layoutObj);
		}
		
		public function getSelectedLayoutHyperlink(layoutObj:XMLList):Array{
			return getFirstLevelXMLValues(HYPERLINK_FORMAT_IDS, layoutObj);
		}
		
		public function getSelectedLayoutObjectBorder(layoutObj:XMLList):Array{
			return getFirstLevelXMLValues(BORDER_IDS, layoutObj);
		}
		
		//Gets the selected column in the layout object
		public function getSelectedColumnProperties(layoutObj:XMLList):Array{
			return getSelectedLayoutProperties("column", layoutObj);
		}
		
		//Retrieves the selected label properties in the Layout section
		public function getSelectedLabelProperties(layoutObj:XMLList):Array{
			return getSelectedLayoutProperties("label", layoutObj);
		}
		
		//Gets the selected column in the layout object
		public function getSelectedLayoutProperties(type:String, selected:XMLList):Array{
			var props:Array = [];
			if (selected.length() == 0){
				//barf here
				Tracer.error('ERROR!! getSelectedLayoutProperties() 0', this);
				return props;
			}	
			var col:XML = selected[0];
			if ((type == "column") && (col.name() != COLUMN)){
				//barf here
				Tracer.error('ERROR!! getSelectedColumnProperties() not COLUMN!', this);
				return props;
			}
			
			var parameters:Array = (type == "column") ? COLUMN_PROPERTIES_IDS : LABEL_PROPERTIES_IDS;
			return getFirstLevelXMLValues(parameters, col); 
		}
		
		public function getSelectedButtonPropertes(selected:XMLList):Array {
			return getFirstLevelXMLValues(BUTTON_PROPERTIES_IDS, selected[0]);
		}
		
		public function getSelectedRectangleProperties(selected:XMLList):Array {
			return getFirstLevelXMLValues(RECTANGLE_PROPERTIES_IDS, selected[0]);
		}
		
		public function setColumnConditionalFormats(formats:Array):void{
			//remove all conditional formats
			var allCols:XMLList = getAllColumns();
			for(var x:int = 0; x < allCols.length(); x++){
				delete allCols[x].ConditionalFormats;
			}
			
			//var removeOnce:Array = new Array();
			
			for(var i:int = 0; i < formats.length; i++){
				var format:Object = formats[i];
				var columnXML:XML = getColumnXML(format['ReportIndex'])[0];
				
				//remove extraneous 
				delete format['Column'];
				delete format['ReportIndex'];
				delete format['Dimension'];
				
				if (columnXML.ConditionalFormats.length() == 0)
					columnXML.appendChild(new XML('<ConditionalFormats/>'));
				
				//add our new ones
				var newCond:XML = new XML('<ConditionalFormat/>');
				for(var key:String in format){
					newCond[key] = format[key];
				}
				columnXML.ConditionalFormats.appendChild(newCond);
			}
			
			//AdHocConfig.asyncSaveConfigToServer(Components.ADHOC_APP, 'setColumnConditionalFormats');
		}
		
		public function getColumnConditionalFormats(columnId:String, conditionalFormatId:String):XMLList{
			var column:XMLList = getColumnXML(columnId);
			//Hmm...should we auto create this?
			if (column.length() == 0){
				column = new XMLList(<{COLUMN} id={columnId}/>);
				getRootEntity().appendChild(column);				
			}
			var conditionalFormats:XMLList = getCreateChildElement("ConditionalFormats", column);
			return conditionalFormats.ConditionalFormat.(@id == conditionalFormatId);
		}
		
		private function getCreateChildElement(tagname:String, root:Object = null):XMLList{
			var parent:Object = (root == null) ? _config : root;
			var element:XMLList = XMLList(parent[tagname]);
			if (element.length() == 0 && element.parent() == null){
				element = new XMLList(<{tagname}/>);
				parent.appendChild(element);
			}
			return element;	
		}
		
		public function setColumnPivot(columnIndex:String, isPivot:Boolean):int{
			var cols:XMLList = getColumnXML(columnIndex);
			if (cols.length() > 0){
				var col:XML = cols[0];
				col.PivotColumn = (isPivot) ? "true" : "false";
			}
			
			return cols.length();	
		}
		
		public function moveColumnToAfter(columnIndex:String, columnAfter:String):Boolean {
			if (columnIndex == columnAfter)
				return false;
				
			var ents:XMLList = _config.Entities.children();
			var i:int;
			for (i = 0; i < ents.length(); i++) {
				if (ents[i].ReportIndex == columnIndex) {
					if (i == 0 && columnAfter == null)
						return false;
						
					if (i > 0 && ents[i - 1].ReportIndex == columnAfter)
						return false;
				}
			}
			var newEnts:XMLListCollection = new XMLListCollection(ents);
			var col:XML = null;
			for (i = 0; i < newEnts.length; i++) {
				if (newEnts[i].ReportIndex == columnIndex) {
					col = XML(newEnts.removeItemAt(i));
					break;
				}
			}
			if (col != null) {
				if (columnAfter == null) {
					newEnts.addItemAt(col, 0);
				}
				else {
					for (var j:int = 0; j < newEnts.length; j++) {
						if (newEnts[j].ReportIndex == columnAfter) {
							newEnts.addItemAt(col, j+1);
							col = null;
							break;
						}
					}
					
					if (col != null) {
						newEnts.addItem(col);
					}
				}
			}
			
			return true;
		}

		public function getColumnByType(type:String):XMLList {
			return _config.Entities[COLUMN].(ColumnType == type);	
		}
		
		public function getMeasureColumnNames():Array{
			var names:Array = [];
			var columns:XMLList = getMeasureColumns();
			for(var i:int = 0; i < columns.length(); i++){
				var col:XML = columns[i];
				var labels:XMLList = getLabelXML(col.LabelName);
				if (labels.length() > 0)
					names.push(labels[0].Label.toString());	
			}	
			
			return names;
		}
		
		public function getMeasureColumns():XMLList{
			return getColumnByType("Measure");
		}
		
		public function getMeasureColumnByName(name:String):XML{
			// name is the label name
			var measures:XMLList = getMeasureColumns();
			for (var i:int = 0; i < measures.length(); i++){
				var column:XML = measures[i];
				var label:XMLList = getLabelXML(column.LabelName);
				if (label.length() > 0 && label.Label == name)
					return column;
			}	
			
			return null;
		}
		
		public function removeColumnLabel(columnId:String):void{
			var column:XMLList = getColumnXML(columnId);
			if (column.length() > 0){
				var targetCol:XML = column[0];
				var label:XMLList = getLabelXML(targetCol.LabelName); // XXX LabelName!?!?
				if (label.length() > 0){
					delete label[0];
				}
				delete column[0];
			
				//After config is saved / failed to save, throw a delete column event - subject area wants to refresh.
				AppEventManager.on(AppEvent.EVT_CONFIG_SAVE_SUCCESS, throwColumnDeletedEvent, false, 0, true);
				AppEventManager.on(AppEvent.EVT_CONFIG_SAVE_FAILED, throwColumnDeletedEvent, false, 0, true);
				
				//AdHocConfig.asyncSaveConfigToServer(Components.ADHOC_APP, 'removeColumnLabel', columnId);
			}
		}
		
		private function throwColumnDeletedEvent(event:AppEvent):void{
			//Remove us from listening to the events again.
			AppEventManager.removeAppEventListener(AppEvent.EVT_CONFIG_SAVE_FAILED, throwColumnDeletedEvent);
			AppEventManager.removeAppEventListener(AppEvent.EVT_CONFIG_SAVE_SUCCESS, throwColumnDeletedEvent);
			
			AppEventManager.fire(new AppEvent(AppEvent.EVT_COLUMN_DELETED, event.callerComponent, event.callerOperation, event.dispatcher));	
		}
		
		public function removeLabel(imageId:String):void{
			var image:XMLList = getLabelXML(imageId);
			if (image.length() > 0){
				delete image[0];
				//AdHocConfig.asyncSaveConfigToServer(Components.ADHOC_APP, 'removeLabel');
			}
		}
		
		public function removeChart(chartId:String):void{
			var chart:XMLList = getChartXML(chartId);
			if (chart.length() > 0){
				delete chart[0];
				//AdHocConfig.asyncSaveConfigToServer(Components.ADHOC_APP, 'removeLabel');
			}
		}
		
		public function removeImage(imageId:String):void{
			var image:XMLList = getImageXML(imageId);
			if (image.length() > 0){
				delete image[0];
				//AdHocConfig.asyncSaveConfigToServer(Components.ADHOC_APP, 'removeImage');
			}
		}
		
		public function removeSubReport(subreportId:String):void{
			var subreport:XMLList = getSubreportXML(subreportId);
			if (subreport.length() > 0){
				delete subreport[0];
				//AdHocConfig.asyncSaveConfigToServer(Components.ADHOC_APP, 'removeSubReport');
			}	
		}
		
		public function getColumnXML(columnId:String):XMLList{
			return _config.Entities[COLUMN].(ReportIndex == columnId);	
		}
		
		public function getAllColumnNames():Array{
			var names:Array = [];
			
			var cols:XMLList = _config.Entities[COLUMN];
			for (var i:int = 0; i < cols.length(); i++){
				var column:XML = cols[i];
				names.push(column.Name.text());
			}	
			
			return names;
		}
		
		public function getAllColumns():XMLList{
			return _config.Entities[COLUMN];
		}
		
		public function getAllCharts():XMLList{
			return _config.Entities[CHART];
		}
		
		public function setSortedColumns(columns:Array):void{
			//Due to possible ordering changes, we create a temporary in memory xml entities, using 
			//values from the sort entities section, where they exist.
			//Then we delete the original and replace.
			
			var tmpSorts:XML = new XML("<Sorts/>");
			
			for(var i:int = 0; i < columns.length; i++){
				var col:Object = columns[i];
				
				var order:String = col.order.value;
				order = StringUtil.trim(order);
				if (order != "Unsorted"){
					var tmpSort:XML = new XML(<Sort/>);
					tmpSort.appendChild(XML(<Column/>));
					//We're in the sorted entities, get the values from there
					//otherwise get from the existing entities section
					var existingEntry:XML = null;
					if (col.inSortedEntities == true) {
						existingEntry = getSortedEntityColumn(col.id);
						if (existingEntry)
							existingEntry = existingEntry.copy();
					}

					if (existingEntry == null)
						existingEntry = getColumnXML(col.id)[0];
						
					if (existingEntry == null)
						continue;
						
					tmpSort.Column = existingEntry;
					
					//Add order type to temp SortOrders section
					var isAscending:String = (order == "Ascending") ? "Asc" : "Desc";
					tmpSort.Order = isAscending;
					
					tmpSort.Type = col.type;
					
					tmpSorts.appendChild(tmpSort);
				}
			}
			
			//remove existing xml entries
			delete _config.Sorts;
			delete _config.SortEntities;
			delete _config.SortOrders;
			
			//set our new ones
			_config.appendChild(tmpSorts);
			
			//save to server
			//asyncSaveConfigToServer(Components.ADHOC_APP, 'setSortedColumn');	
		}
		
		public function setColumnSortValue(id:int, sortValue:String):void{
			var targets:XMLList = _config.Sorts.Sort;
			
			delete _config.SortEntities;
			delete _config.SortOrders;
			
			if (targets.length() > 0){
				var found:Boolean = false;
				for(var i:int = 0; i < targets.length(); i++){
					var sortEntity:XML = targets[i][COLUMN][0];
					if (int(sortEntity.ReportIndex.toString()) == id){
						found = true;
						if (sortValue != "Unsorted"){
							targets[i].Order = (sortValue == "Ascending") ? "Asc" : "Desc";
						}else{
							delete  _config.Sorts.Sort[i];
						}		
						return;
					}
				}
			}
			//fresh new entry
			addColumnToSortEntities(id, sortValue);
		}
		
		public function addColumnToSortEntities(id:int, sortValue:String):void{
			if (sortValue != "Unsorted"){
				var cols:XMLList = getColumnXML(id.toString());
				if (cols.length() > 0){
					var sort:XML = XML(<Sort/>);
					if ((_config.Sorts as XMLList).length() == 0){
						_config.appendChild(XML(<Sorts/>));
					}
					sort.appendChild(cols[0]);
					sort.Order = (sortValue == "Ascending") ? "Asc" : "Desc";
					sort.Type = "Data";
					(_config.Sorts[0] as XML).appendChild(sort);
				}
			}
		}
		
		
		public function getSortedEntityColumn(id:String):XML {
			var targets:XMLList = _config.Sorts[COLUMN].(ReportIndex == id);
			if (targets.length() > 0)
				return targets[0];
			
			return null;	
		}
		
		private var _sortedOrderArray: ArrayCollection;
		public function getSortedOrderArray() : ArrayCollection {
			if (_sortedOrderArray == null) {
				_sortedOrderArray = new ArrayCollection(
												[{labelLocKey: 'CS_ascend', value:'Ascending'}, 
												 {labelLocKey: 'CS_descend', value: 'Descending'}, 
												 {labelLocKey: 'CS_unsort', value: 'Unsorted'}]);
					
				Localization.localizeArrayCollection(_sortedOrderArray);
			}
			return _sortedOrderArray;
		}
		public function getSortedOrderItem(value:String):Object {
			var arr:ArrayCollection = getSortedOrderArray();
			for (var i:int = 0; i < arr.length; i++) {
				var o:Object = arr.getItemAt(i);
				if (o && o.value == value) 
					return o;
			}
			return null;
		}
		
		public function getSortedColumns():Array{
			var props:Array = [];
			var dups:Object = {};
			
			var columns:XMLList = _config.Sorts.Sort;
			var sort:XML;
			var cols:XMLList;
			var col:XML;
			var reportIndex:String;
			var name:String;
			var sortOrder:String;
			var obj:Object;
			if (columns.length() > 0){
				
				var i:int = 0;
				for each (sort in columns){
					// skip ratios
					cols = sort["com.successmetricsinc.adhoc.AdhocColumn"];
					if (!cols || cols.length() == 0)
						continue;
						
					col = cols[0];
					if (col.ColumnType.toString() == "Ratio")
						continue;
						
					reportIndex = col.ReportIndex;
					name = getLabelNameByColumnId(reportIndex);
					if (name == '') {
						// no label - just use column name
						name = col.Name;
					}
					
					//check for duplicates
					if (dups[name] == 1) 
						continue;
					//mark selected
					dups[name] = 1;
					
					sortOrder = (String(sort.Order) == "Asc") ? "Ascending" : "Descending";
					var sortType:String = String(sort.Type);
					obj = { column: name, order: getSortedOrderItem(sortOrder), id: reportIndex, inSortedEntities: true, type: sortType};
					props.push(obj);
					i++;
				}
			}
			else {
				columns = _config.SortEntities;
				if (columns.length() > 0) {
					var sortOrders:XMLList = _config.SortOrders.String;
					i = 0;
					var j:int = 0;
					for each (sort in columns){
						j++;
						// skip ratios
						cols = sort["com.successmetricsinc.adhoc.AdhocColumn"];
						if (!cols || cols.length() == 0)
							continue;
							
						col = cols[0];
						if (col.ColumnType.toString() == "Ratio")
							continue;
							
						reportIndex = col.ReportIndex;
						name = getLabelNameByColumnId(reportIndex);
						if (name == '') {
							// no label - just use column name
							name = col.Name;
						}
						
						//check for duplicates
						if (dups[name] == 1) 
							continue;
						//mark selected
						dups[name] = 1;
						
						sortOrder = (String(sortOrders[j-1]) == "true") ? "Ascending" : "Descending";
						obj = { column: name, order: getSortedOrderItem(sortOrder), id: reportIndex, inSortedEntities: true, type: 'Data'};
						props.push(obj);
						i++;
					}
				}
			}
			
			//Go through all columns to pick unsorted ones out
			var allCols:XMLList = getAllColumns();
			for(var y:int = 0; y < allCols.length(); y++){
				var colm:XML = allCols[y];
				// skip ratios
				if (colm.ColumnType.toString() == "Ratio")
					continue;
					
				var colIndex:String = colm.ReportIndex;
				var colName:String = getLabelNameByColumnId(colIndex);
				//check for duplicates
				if (dups[colName] == 1) 
					continue; 
				//mark selected	
				dups[colName] = 1;
				var objCol:Object = { column: colName, order: getSortedOrderItem("Unsorted"), id: colIndex, inSortedEntities: false, type: "Data"};
				props.push(objCol);
			}
			
			return props;
		}
		
		public function setFilters(filters:Array):void{
			//delete existing filters
			delete _config.Filters;
			delete _config.DisplayFilters;
			delete _config.ReportFilters;
			
			var measureCols:XMLList = getMeasureColumns();
			for(var x:int = 0; x < measureCols.length(); x++){
				if (measureCols[x].FilterList.length() > 0)
					delete measureCols[x].FilterList;
			}
			
			var tmpFilters:XML = new XML("<Filters/>");
			var tmpDisplayFilters:XML = new XML("<DisplayFilters/>");
			var tmpReportFilters:XML = new XML("<ReportFilters/>");
			
			for (var i:int = 0; i < filters.length; i++){
				var filter:Object = filters[i];
				var xml:XML = new XML("<Filter/>");
				
				//This comes from the Data Grid's internal key
				delete filter['mx_internal_uid'];
				
				//Get all param for the filter object
				for (var k:String in filter){
					if (k == "MeasureColumn")
						continue;
					xml[k] = filter[k];	
				}
				
				//Filter to Measure columns go to that particular measure column
				if (filter.MeasureColumn != null){
					var measure:XML = getMeasureColumnByName(filter.MeasureColumn);
					if (measure){
						if (measure.FilterList.length() == 0){
							measure.appendChild(XML('<FilterList/>'));
						}
						var filterList:XML = measure.FilterList[0];
						filterList.appendChild(xml);	
					}
				}else{
					if (filter.Type == "Predicate" || filter.Type == '3'){
						tmpDisplayFilters.appendChild(xml);			
					}else if (filter.Type == "Set"){
						tmpReportFilters.appendChild(xml); 
					} else {
						tmpFilters.appendChild(xml);
					}
				}	
			}
			
			//set new ones
			_config.appendChild(tmpFilters);
			_config.appendChild(tmpDisplayFilters);
			_config.appendChild(tmpReportFilters);
		
			//save to server
			//asyncSaveConfigToServer(Components.ADHOC_APP, 'setFilters');
		}
		
		public function getFilters():Array{
			var conf:Array = [];
			
			//Get data and display filters
			addFiltersToArray(_config.Filters, conf);
			addFiltersToArray(_config.DisplayFilters, conf);
			addFiltersToArray(_config.ReportFilters, conf);
			
			//Get measured column filters
			var measureColumns:XMLList = getMeasureColumns();
			for(var i:int = 0; i < measureColumns.length(); i++){
				var measureCol:XML = measureColumns[i];
				if (measureCol.FilterList.length() > 0){
				var measureColName:String = measureCol.Name;
				var labels:XMLList = getLabelXML(measureCol.LabelName);
				if (labels.length() > 0)
					measureColName = labels[0].Label;	
				addFiltersToArray(measureCol.FilterList, conf, measureColName);
				}			
			}
			
			return conf;
		}
		
		public function addFiltersToArray(rootFilter:XMLList, conf:Array, measureColumn:String = ''):void{
			if (rootFilter.children().length() > 0){
				var allFilters:XMLList = rootFilter.child('Filter');	
				for(var i:int = 0; i < allFilters.length(); i++){
					var cell:Object = new Object();
					var values:Array = getFirstLevelXMLValues(FILTER_COLUMN_IDS, allFilters[i]);
					for(var k:String in values){
						cell[k] = values[k];
					}
					//Indicator that we're a measure column
					if (measureColumn != ''){
						cell['MeasureColumn'] = measureColumn;
					}
					conf.push(cell);
				}
			}
		}
		
		public function isHideContent():Boolean{
			return _config.HideContent == "true";
		}
		
		public function setHideContent(hideOrNot:Boolean, component:String, operation:String):void{
			_config.HideContent = hideOrNot.toString().toLowerCase();
			//AdHocConfig.asyncSaveConfigToServer(component, operation);
		}
		
		public function isAutomaticLayout():Boolean {
			return _config.AutomaticLayout == "true";
		}		
		
		public function isShowFlexGrid():Boolean {
			return _config.UseFlexGrid == "true";
		}		

		public function setShowFlexGrid(showOrNot:Boolean):void {
			_config.UseFlexGrid = showOrNot.toString().toLowerCase();
		}
		
		public function setUseOlapIndentation(useIt:Boolean):void {
			_config.OlapUseIndentation = useIt.toString().toLowerCase();
		}
		
		public function isUseOlapIndentation():Boolean {
			return _config.OlapUseIndentation == "true";
		}
		
		public function getConditionalFormats():Array{
			var conf:Array = [];
			
			var columns:XMLList = getAllColumns();
			for (var i:int = 0; i < columns.length(); i++){
				var column:XML = columns[i];
				var condFormats:XMLList = column.ConditionalFormats.ConditionalFormat;
				if (condFormats.children().length() > 0){
					for (var y:int = 0; y < condFormats.length(); y++) {
						var format:XML = condFormats[y];
						var values:Array = getFirstLevelXMLValues(CONDITIONAL_COLUMN_FORMAT_IDS, format);
						var cell:Object = new Object();
						for(var k:String in values){
							cell[k] = values[k];
						} 
						cell['Column'] = column.Name.text();
						cell['Dimension'] = column.Dimension.text();
						cell['ReportIndex'] = column.ReportIndex.text();
						conf.push(cell);
					}
				}
			}
			
			return conf;	
		}
		
		public function setReportFile(path:String, filename:String):void{
			_config.Name = filename;
			_config.Path = path;
		}
		
		public function createNewImage(type:String, location:String):XML{
			var imageXML:XML = 
				<com.successmetricsinc.adhoc.AdhocImage>
					<LeftBorder>None</LeftBorder>
					<RightBorder>None</RightBorder>
					<TopBorder>None</TopBorder>
					<BottomBorder>None</BottomBorder>
					<ReportIndex>-1</ReportIndex>
					<ForegroundColor>33d3d</ForegroundColor>
					<BackgroundColor>e6e6e6</BackgroundColor>
					<BorderColor>ffffff</BorderColor>
					<Width>100</Width>
					<Height>13</Height>
					<Left>0</Left>
					<Top>0</Top>
					<Band/>
					<GroupNumber>0</GroupNumber>
					<Font>Arial-PLAIN-8</Font>
					<Alignment>Left</Alignment>
					<PositionType>Float</PositionType>
					<StretchType>Relative to Tallest Object</StretchType>
					<ImageType>File</ImageType>
					<Location/>
				</com.successmetricsinc.adhoc.AdhocImage>;
				
			imageXML.ImageType = type;
			imageXML.Location = location;
			
			getNextElementId();
            addElement(imageXML);
            return imageXML;
		}
		
		public function createNewSubreport(PassValueFromParent:Boolean, subReport:String):void{
			var subreportXML:XML = 
				<com.successmetricsinc.adhoc.AdhocSubreport>
					<LeftBorder>None</LeftBorder>
					<RightBorder>None</RightBorder>
					<TopBorder>None</TopBorder>
					<BottomBorder>None</BottomBorder>
					<ReportIndex>-1</ReportIndex>
					<ForegroundColor>33d3d</ForegroundColor>
					<BackgroundColor>e6e6e6</BackgroundColor>
					<BorderColor>ffffff</BorderColor>
					<Width>100</Width>
					<Height>13</Height>
					<Left>0</Left>
					<Top>0</Top>
					<Band/>
					<GroupNumber>0</GroupNumber>
					<Font>Arial-PLAIN-8</Font>
					<Alignment>Left</Alignment>
					<PositionType>Float</PositionType>
					<StretchType>Relative to Tallest Object</StretchType>
					<Subreport/>
				</com.successmetricsinc.adhoc.AdhocSubreport>;
				
       		subreportXML.Subreport = subReport;
       		subreportXML.PassValueFromParent = PassValueFromParent.toString();
			
			getNextElementId();
            addElement(subreportXML);
            
		}
		public function getBandProperties():Object{
			var pageInfo:XMLList = 	getPageInfoXML();
			var numGroups:int = int(_config.NumGroups);
	
			var groupHeader:XMLList = pageInfo.GroupHeader;
			var headerList:Array = new Array();
			for (var i:int = 0; i < groupHeader.length(); i++){
				var o:Object = {height: int(groupHeader[i].Height), 
					forceBreak: String(groupHeader[i].ForcePageBreak), 
					includeIf: String(groupHeader[i].IncludeIf),
					includeIfIndex: int(groupHeader[i].IncludeIfIndex)};
				headerList.push(o);	
			}
			
			var groupFooter:XMLList = pageInfo.GroupFooter;
			var footerList:Array = new Array();
			for(var y:int = 0; y < groupFooter.length(); y++){
				var of:Object = {height: int(groupFooter[y].Height), 
					forceBreak: String(groupFooter[y].ForcePageBreak), 
					includeIf: String(groupFooter[y].IncludeIf),
					includeIfIndex: int(groupFooter[y].IncludeIfIndex)};
				footerList.push(of);
			}
			
			var od:Boolean;
			var doNotBreak:XMLList = pageInfo.DoNotBreakGroups.DoNotBreakGroup;
			var dnbList:Array = new Array();
			for (var d:int = 0; d < doNotBreak.length(); d++) {
				var t:String = doNotBreak[d].text();
				od = (t == "true");
				dnbList.push(od);
			}
			
			var startOnNewPage:XMLList = pageInfo.StartOnNewPages.StartOnNewPage;
			var snpList:Array = new Array();
			for (var s:int = 0; s < startOnNewPage.length(); s++) {
				var tf:String = startOnNewPage[s].text();
				od = (tf == "true");
				snpList.push(od);
			}
			
			var bandObject:Object = { 
				NumGroups: numGroups,
				PageHeaderBand: {height: int(pageInfo.PageHeaderBandHeight),
								 forceBreak: null, 
								 includeIf: String(pageInfo.PageHeaderIncludeIf),
								 includeIfIndex: int(pageInfo.PageHeaderIncludeIfIndex)},
				TitleBand: {height: int(pageInfo.TitleBandHeight), 
							forceBreak: String(pageInfo.TitleForceBreak), 
							includeIf: String(pageInfo.TitleIncludeIf),
							includeIfIndex: int(pageInfo.TitleIncludeIfIndex)},
				HeaderBand: {height: int(pageInfo.HeaderBandHeight), 
							forceBreak: String(pageInfo.HeaderForceBreak), 
							includeIf: String(pageInfo.HeaderIncludeIf),
							includeIfIndex: int(pageInfo.HeaderIncludeIfIndex)},
				DetailBand: {height: int(pageInfo.DetailBandHeight), 
							forceBreak: String(pageInfo.DetailForceBreak), 
							includeIf: String(pageInfo.DetailIncludeIf),
							includeIfIndex: int(pageInfo.DetailIncludeIfIndex)},
				SummaryBand: {height: int(pageInfo.SummaryBandHeight), 
							forceBreak: String(pageInfo.SummaryForceBreak), 
							includeIf: String(pageInfo.SummaryIncludeIf),
							includeIfIndex: int(pageInfo.SummaryIncludeIfIndex)},
				PageFooterBand: {height: int(pageInfo.PageFooterBandHeight), 
							forceBreak: null, 
							includeIf: String(pageInfo.PageFooterIncludeIf),
							includeIfIndex: int(pageInfo.PageFooterIncludeIfIndex)},
				GroupHeader: headerList,
				GroupFooter: footerList,
				DoNotBreak: dnbList,
				StartOnNewPage: snpList
			};
			return bandObject;
		}
		
		public function setBandProperties(o:Object):void{
			var pageInfo:XMLList = 	getPageInfoXML();
			if (pageInfo.length() == 0){
				pageInfo = new XMLList(<PageInfo/>);
				_config.appendChild(pageInfo);
			}
			
			pageInfo.TitleBandHeight = o.TitleBand.height;
			pageInfo.TitleForceBreak = o.TitleBand.forceBreak;
			pageInfo.TitleIncludeIf = o.TitleBand.includeIf;
			pageInfo.TitleIncludeIfIndex = o.TitleBand.includeIfIndex;
			pageInfo.HeaderBandHeight = o.HeaderBand.height;
			pageInfo.HeaderForceBreak = o.HeaderBand.forceBreak;
			pageInfo.HeaderIncludeIf = o.HeaderBand.includeIf;
			pageInfo.HeaderIncludeIfIndex = o.HeaderBand.includeIfIndex;
			pageInfo.DetailBandHeight = o.DetailBand.height;
			pageInfo.DetailForceBreak = o.DetailBand.forceBreak;
			pageInfo.DetailIncludeIf = o.DetailBand.includeIf;
			pageInfo.DetailIncludeIfIndex = o.DetailBand.includeIfIndex;
			pageInfo.SummaryBandHeight = o.SummaryBand.height;
			pageInfo.SummaryForceBreak = o.SummaryBand.forceBreak;
			pageInfo.SummaryIncludeIf = o.SummaryBand.includeIf;
			pageInfo.SummaryIncludeIfIndex = o.SummaryBand.includeIfIndex;
			pageInfo.PageHeaderBandHeight = o.PageHeaderBand.height;
			pageInfo.PageHeaderIncludeIf = o.PageHeaderBand.includeIf;
			pageInfo.PageHeaderIncludeIfIndex = o.PageHeaderBand.includeIfIndex;
			pageInfo.PageFooterBandHeight = o.PageFooterBand.height;
			pageInfo.PageFooterIncludeIf = o.PageFooterBand.includeIf;
			pageInfo.PageFooterIncludeIfIndex = o.PageFooterBand.includeIfIndex;
			
			if (o.GroupHeader.length > 0){
				_bandPropSetGroupHeight(pageInfo, "GroupHeader", o.GroupHeader);
			}
			if (o.GroupFooter.length > 0){
				_bandPropSetGroupHeight(pageInfo, "GroupFooter", o.GroupFooter);
			}
			
			var i: int;
			var l:XML;
			delete pageInfo['DoNotBreakGroups'];
			var dnbgs:XML = <DoNotBreakGroups/>;
			for (i = 0; i < o.DoNotBreak.length; i++) {
				l = <DoNotBreakGroup>{o.DoNotBreak[i] == true ? "true" : "false"}</DoNotBreakGroup>;
				dnbgs.appendChild(l);
			}
			pageInfo.appendChild(dnbgs);
			
			delete pageInfo['StartOnNewPages'];
			var sonps:XML = <StartOnNewPages/>;
			for (i = 0; i < o.StartOnNewPage.length; i++) {
				l = <StartOnNewPage>{o.StartOnNewPage[i] == true ? "true" : "false"}</StartOnNewPage>;
				sonps.appendChild(l);
			}
			pageInfo.appendChild(sonps);
			
			_config.NumGroups = o.NumGroups;
		}

		private function _bandPropSetGroupHeight(pageInfo:XMLList, tagName:String, list:Array):void{
			delete pageInfo[tagName];
			
			for(var x:int; x < list.length; x++){
				var leaf:XMLList = new XMLList(<{tagName}/>);		
				var height:XML = <Height>{list[x].height}</Height>;
				leaf.appendChild(height);
				var forceBreak:XML = <ForcePageBreak>{list[x].forceBreak}</ForcePageBreak>;
				leaf.appendChild(forceBreak);
				var includeIf:XML = <IncludeIf>{list[x].includeIf}</IncludeIf>;
				leaf.appendChild(includeIf);
				var includeIfIndex:XML = <IncludeIfIndex>{list[x].includeIfIndex}</IncludeIfIndex>;
				leaf.appendChild(includeIfIndex);
				pageInfo.appendChild(leaf);
			}
			
		}
		
		public function getPageInfoXML():XMLList{
			return _config.PageInfo;	
		}
		
		//Toggle on or off the summation operation
		public function toggleSum():void{
			if (_config.GrandTotals.length() > 0){
				_config.GrandTotals = (_config.GrandTotals == "true") ? "false" : "true";
			}else{
				//Pristine state, toggle on
				_config.appendChild(new XML('<GrandTotals>true</GrandTotals>'));
			}
			
			//asyncSaveConfigToServer(Components.ADHOC_APP, 'toggleSum');	
		}
		
		public function setPivotMeasureLabelsColumnWise(columnWise:Boolean):void{
			if (_config){
				_config.PivotTable.MeasureLabelPosition = columnWise ? "Column" : "Row";
				//asyncSaveConfigToServer(Components.ADHOC_APP, 'setPivotMeasureLabelsColumnWise');
			}	
		}
		
		public function swapColumnOnBottom(top:String, bottom:String):Boolean{
			return swapColumnOnTop(bottom, top);
		}
		
		public function swapColumnOnTop(top:String, bottom:String):Boolean{
			var topCol:XML = getColumnXML(top)[0].copy();
			var bottomCol:XML = getColumnXML(bottom)[0];
			if (topCol && bottomCol){
				delete getColumnXML(top)[0];
				_config.Entities.insertChildBefore(bottomCol, topCol);	
				return true;
			}
			
			return false;
		}
		
		public function isPivotMeasureLabelsColumnWise():Boolean{
			return "Column" == _config.PivotTable.MeasureLabelPosition.toString();
		}
		
		public function getPivotTableProperties():Array{
			var props:Array = new Array();
			if (_config){
				var p:XMLList = _config.PivotTable.*;
				if (p.length() > 0){
					for each (var prop:XML in p){
						props[prop.name()] = prop.toString();
					}
				}
			}
			return props;
		}
		
		public function setPivotTableProperties(conf:Array):void{
			if (conf){
				copyAllArrayProperties(conf, _config.PivotTable);
				//asyncSaveConfigToServer(Components.ADHOC_APP, 'setPivotTableProperties');		
			}
		}
		
		public function setTopN(value:String):void{
			var topFilters:XMLList = _config.TopFilter.Top;
			if (topFilters.length() == 0) {
				_config.TopFilter = XML(<TopFilter><Top/></TopFilter>);
			}
			var topFilter:XML = _config.TopFilter.Top[0];
				
			if (value == String(topFilter.Operand)){
				return;
			}
			
			if (value.toLowerCase() == "all")
				value = "-1";
				
			topFilter.Operand = value;
			delete topFilter.PromptName;
			//asyncSaveConfigToServer(Components.ADHOC_APP, 'setTopN');
		}
		
		public function setTopNPrompt(value:String):void {
			var topFilters:XMLList = _config.TopFilter.Top;
			if (topFilters.length() == 0) {
				_config.TopFilter = XML(<TopFilter><Top/></TopFilter>);
			}
			var topFilter:XML = _config.TopFilter.Top[0];
			
			if (topFilter && value == String(topFilter.PromptName)){
				return;
			}
			
			delete topFilter.Operand;
			topFilter.PromptName = value;
		}
		
		public function getTopN():XML{
			var top:XMLList = _config.TopFilter.Top 
			return top.length() > 0 ? top[0] : null;
		}
		
		private function copyAllArrayProperties(conf:Array, root:XMLList = null): void {
			var topNode:Object = root;
			if (root == null)
				topNode = _config;
			
			for (var key:String in conf){
				topNode[key] = conf[key];
			}	
		}
		
		private function copyAllArrayPropertiesRecursively(conf:Array, root:XMLList):void {
			for (var key:String in conf){
				if (conf[key] is Array) {
					var child:XML = XML("<" + key + "/>");
					copyAllArrayPropertiesRecursively(conf[key], XMLList(child));
					root[key] = child;
				}
				else
					root[key] = conf[key];
			}	
		}
		
		private function copyAllProperties(conf:XML, root:XMLList = null):void{
			var topNode:Object = root;
			if (root == null)
				topNode = _config;
			
			for each (var property:XML in conf.*){
				topNode[property.name()] = property;
			}	
		}
		
		public function get config():String{
			return _config.toXMLString();
		}
		
		public function getConfig():XML{
			return _config;
		}
		
		public function toXMLString():String{
			return _config.toXMLString();	
		}
		
		public function clearEmptyConfig():void{
			delete _config.*;
		}
	
		public function addElement(element:XML): void {
			_config.Entities.appendChild(element);
			//asyncSaveConfigToServer(Components.ADHOC_APP, 'addElement');
		}
		
		public function getColumnSelectors(columnIndex:String):XMLList{
			var cols:XMLList = getColumnXML(columnIndex);
			if(cols.length() > 0){
				return cols[0].ColumnSelectors.ColumnSelector;
			}
			
			return new XMLList();
		}
		
		public function setColumnSelectors(columnIndex:String, selectors:Array):void{
			//Add the column selector xml to the column
			var cols:XMLList = getColumnXML(columnIndex);
			if(cols.length() > 0){
				delete cols[0].ColumnSelectors;
				cols[0].appendChild(<ColumnSelectors/>);
				for(var i:int = 0; i < selectors.length; i++){
					if (selectors[i] is XML){
						cols.ColumnSelectors.appendChild(selectors[i]);	
					}
				}
			}
		}
		
		public function getFirstChartType():String{
			var defaultChartId:String = getDefaultChartId();
			var chartXML:XMLList = null;
			if (defaultChartId && defaultChartId != "-1") {
				chartXML = getChartXML(defaultChartId);
				if (chartXML && chartXML.length() > 0) {
					return chartXML.Charts.Chart[0].ChartType.toString();		
				}
			}else{
				var conf:Array = getChartsConfig();
				if(conf && conf['chartType']){
					return conf['chartType'];
				}
			}
			
			return '';
		}
		
		public function drawChart(type:String):void{
			var defaultChartId:String = getDefaultChartId();
			var chartXML:XMLList = null;
			if (defaultChartId && defaultChartId != "-1") {
				chartXML = getChartXML(defaultChartId);
			}
			
			if (chartXML && chartXML.length() > 0) {
				if (type == null || type == '') {
					chartXML[0].showInReport = 'false';
				}
				else {
					chartXML[0].showInReport = 'true';
					if(type != 'Composite'){
						var list:XMLList = chartXML.Charts.Chart;
						list[0].ChartType = type;
					}
				}
			}
			else if (type != null) {
				var conf:Array = getChartsConfig();
				conf['chartType'] = type;
				
				//Set default categories and series
				if (!conf['Series'] && !conf['Categories']){
					var dimensions:XMLList = getColumnByType('Dimension');
					var measures:XMLList = getColumnByType('Measure');
					if (dimensions.length() > 0 && measures.length() > 0){
				 		conf['Categories'] = 'category=' + getLabelXML(dimensions[0].LabelName)[0].Label.toString();
				 		conf['Series'] = 'series=' + getLabelXML(measures[0].LabelName)[0].Label.toString();
					}
				}
				//AdHocConfig.instance().setChartsConfig(conf);
			}
		}
		
		public function undoLayoutChanges():void {
			var defaultChartId:String = getDefaultChartId();
			var entityNode:XMLList = _config.Entities;
			var entities:XMLList = (entityNode[0]).children();
			var newEntities:XMLListCollection = new XMLListCollection();
			for (var i:uint = 0; i < entities.length(); i++) {
				var entity:XML = entities[i];
				var name:String = entity.localName();
				if (name == LABEL) {
					newEntities.addItem(entity);
					entity.Band = "Header";
				}
				else if (name == COLUMN) {
					newEntities.addItem(entity);
					entity.Band = "Detail";
				}
				else if (name == CHART && String(entity.ReportIndex) == defaultChartId) {
					newEntities.addItem(entity);
				}
			}
			entityNode[0].setChildren(XMLList(newEntities));
			var rowHeight:String = _config.RowHeight;
			if (rowHeight != "13") {
				rowHeight = "15";
				_config.RowHeight = rowHeight;
			}
			_config.PageInfo.HeaderBandHeight = rowHeight;
			_config.PageInfo.DetailBandHeight = rowHeight;
			_config.AutomaticLayout = "true";
			_config.NumGroups = 0;
		}
		
		public function getTrellisColumnIndex():String{
			var charts:XMLList =  _config.Entities[CHART].(IsTrellis == 'true');
			if(charts.length() > 0){
				return charts[0].TrellisColIndex.toString();		
			}
			return null;
		}
		
		public function getTrellisColumnIndex2():String{
			var charts:XMLList =  _config.Entities[CHART].(IsTrellis == 'true');
			if(charts.length() > 0){
				return charts[0].TrellisColIndex2.toString();		
			}
			return null;
		}
		
		public function getTrellisChartIndex():String{
			var charts:XMLList =  _config.Entities[CHART].(IsTrellis == 'true');
			if(charts.length() > 0){
				return charts[0].ReportIndex.toString();	
			}
			
			return null;
		}
		
		public function undoTrellisChartIfColumnMatch(columnIndex:String):void{
			var charts:XMLList =  _config.Entities[CHART].(IsTrellis == 'true');
			if(charts.length() > 0){
				var trellis:XML = charts[0];
				var idx1:String = trellis.TrellisColIndex.toString();
				var idx2:String = trellis.TrellisColIndex2.toString();
				if(columnIndex == idx1){
					if(idx2.length > 0 && idx2 != '-1'){
						trellis.TrellisColumn = trellis.TrellisColumn2.toString();
						trellis.TrellisColIndex = idx2;
						trellis.TrellisColumn2 = '';
						trellis.TrellisColIndex2 = '-1';
					}else{
						undoTrellis(trellis);
					}
				}else if (columnIndex == idx2){
					if(idx1.length == 0 || idx1 == '-1'){
						undoTrellis(trellis);
					}
				}
			}
		}
		
		public function undoTrellisTooLarge():Boolean{
			var charts:XMLList =  _config.Entities[CHART].(IsTrellis == 'true');
			var isTrellis:Boolean = charts.length() > 0;
			if(isTrellis){
				delete charts[0];
				isTrellis = true;
			}
			_config.IncludeTable = true;
			
			return isTrellis;
		}
		
		private function undoTrellis(trellis:XML):void{
			//default width
			trellis.Width = 400; 
			//default height
			_config.PageInfo.TitleBandHeight = trellis.Height = 300;
			
			trellis.IsTrellis = 'false';
			trellis.TrellisColumn = trellis.TrellisColumn2 = '';
			trellis.TrellisColIndex = trellis.TrellisColIndex2 = '-1';
		}
		
		public function setTrellisChartColumn(columnIndex:String):void{
			if(columnIndex){
				var charts:XMLList =  _config.Entities[CHART].(IsTrellis == 'true');
				if(charts.length() > 0){
					var trellis:XML = charts[0];
					var cols:XMLList = getColumnXML(columnIndex);
					if(cols.length() > 0){
						var column:XML = cols[0];
						trellis.TrellisColumn = column.Name.toString();
						trellis.TrellisColIndex = columnIndex;
					}
				}
			}
		}
		
		public function setInVisualization():void {
			_config.IsInVisualization = true;
		}
		
		public function replaceDefaultChart(chart:XML):Boolean {
			var defaultChartId:String = this.getDefaultChartId();
			if (defaultChartId && defaultChartId != "-1") {
				var currentCharts:XMLList = getChartXML(defaultChartId);
				if (currentCharts && currentCharts.length() > 0) { 
					var oldChart:XML = currentCharts[0];
					var newChartChart:XML = chart.Charts.Chart[0];
					var oldChartChart:XML = oldChart.Charts.Chart[0];
					var a:Array = oldChartChart.namespaceDeclarations();
					for (var i:int = 0; i < a.length; i++) {
						newChartChart.addNamespace(a[i]);
					}
					if (oldChartChart.toXMLString() == newChartChart.toXMLString())
						return false;
				}
					
				removeChart(defaultChartId);
			}
			else {
				defaultChartId = String(getNextElementId());
				this.setDefaultChartId(defaultChartId);
			}
			chart.ReportIndex = defaultChartId;
			addElement(chart);
			return true;
		}
	}
}