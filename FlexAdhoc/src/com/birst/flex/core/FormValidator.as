package com.birst.flex.core
{
	import flash.events.Event;
	import flash.display.DisplayObject;
	import flash.events.EventDispatcher;
	import flash.events.IEventDispatcher;
	
	import mx.events.ValidationResultEvent;
	import mx.validators.Validator;
	
	public class FormValidator extends EventDispatcher
	{
		// form is valid - used for showing the save button
		public var formIsValid:Boolean = false;
		// list of validators from the form
		public var validators:Array;
		private var focusedFormControl:DisplayObject;

		public function FormValidator(target:IEventDispatcher=null)
		{
			super(target);
		}
		
		// validate the entire form
		public function validateForm(event:Event):void
		{
			focusedFormControl = event.target as DisplayObject;
			formIsValid = true;
 
			for each( var validator:Validator in validators ) {
				// the field doesn't exist because its not in a state?   
				if (validator.source == null) {
					continue;
				}
				
				validate(validator);
			}
		}
 
 		// run an individual validator
		private function validate(validator:Validator):Boolean
		{
			if (validator.enabled == false)
				return true;
				
			var validatorSource:DisplayObject = validator.source as DisplayObject;
			var supressEvents:Boolean = validatorSource != focusedFormControl;
			var event:ValidationResultEvent = validator.validate(null, supressEvents)
			var currentControlIsValid:Boolean = event.type == ValidationResultEvent.VALID;
 
			formIsValid = formIsValid && currentControlIsValid;
			return currentControlIsValid;
		}
	}
}