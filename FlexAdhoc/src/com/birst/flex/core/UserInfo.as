package com.birst.flex.core
{
	import com.birst.flex.webservice.WebServiceClient;
	import com.birst.flex.webservice.WebServiceResult;
	
	
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	
	public class UserInfo
	{
		public static const adminServiceRoot:String = "/AdminService.asmx";
		
		private var _userID:String;
		private var _spaceID:String;
		private static var _instance:UserInfo;
		
		public function UserInfo()
		{
		}
		
		public static function instance():UserInfo{
			if(_instance == null){
				_instance = new UserInfo();
			}
			return _instance;
		}
		
		public function set LoggedInUserID(value:String):void{
			_userID = value;
		}
		
		public function get LoggedInUserID():String{
			return _userID;
		}

		public function set LoggedInSpaceID(value:String):void{
			_spaceID = value;
		}
		
		public function get LoggedInSpaceID():String{
			return _spaceID;
		}

		private static var ws:WebServiceClient;
		public static function populateUserAndSpaceInfo(listener:Function=null, param:Object=null):void{
			ws = new WebServiceClient("Admin", "getUserAndSpaceDetails", "UserInfo", "populateUserAndSpaceInfo", param);
			ws.setResultListener(listener == null ? parseUserAndSpaceResponse : listener);
			ws.setFaultListener(getUserAndSpaceFaultListener);
			ws.execute();
		}
		
		
		public static function parseUserAndSpaceResponse(event:ResultEvent):void{
			var userInfo:UserInfo = UserInfo.instance();
			var webserviceResult:WebServiceResult = WebServiceResult.parse(event);
			
			var result:WebServiceResult = WebServiceResult.parse(event);
			if(!result.isSuccess()){
				return;
			}	
			var rootXmlList:XMLList = result.getResult().root;
			if(rootXmlList != null && rootXmlList.length() > 0)
			{
				var userIDXmlList:XMLList = rootXmlList[0].userID;
				if(userIDXmlList != null && userIDXmlList.length() > 0){
					userInfo.LoggedInUserID = userIDXmlList[0].toString().length > 0 ? userIDXmlList[0].toString() : null;
				}
				
				var spaceIDXmlList:XMLList = rootXmlList[0].spaceID;
				if(spaceIDXmlList != null && spaceIDXmlList.length() > 0){
					userInfo.LoggedInSpaceID = spaceIDXmlList[0].toString().length > 0 ?spaceIDXmlList[0].toString(): null ;
				}
			}
		}
		
		
		public static function  parseOtherNode(key:String,otherXml:XML):String{
			if(otherXml != null){
				var stringList:XMLList = otherXml.string;
				if(stringList != null && stringList.length() > 0){
					for(var i:int=0; i < stringList.length(); i++){
						var strArray:Array = String(stringList[0]).split("=");
						if(strArray != null && strArray.length == 2 && strArray[1] == key){
							return strArray[2];	
						}	
					}
				}
			}
			
			return null;
		}
		
		
		public static function getUserAndSpaceFaultListener(event:FaultEvent):void{			
		}
		
	}
}