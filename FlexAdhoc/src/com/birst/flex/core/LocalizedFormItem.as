package com.birst.flex.core
{
	import com.birst.flex.core.Localization;
	import mx.containers.FormItem;
	
	public class LocalizedFormItem extends FormItem
	{
		private var _localizedTitle:String = null;
		private var _localizedLabel:String = null;
		private var _localizedTooltip:String = null;
	
		public function LocalizedFormItem()
		{
			super();
		}

		public function get localizedTitleId():String{
			return _localizedLabel;
		}
		
		public function set localizedTitleId(key:String):void{
			if (key){
				Localization.bindPropertyString(this, "title", key);
	            _localizedTitle = key;
			}
		}
		
		public function get localizedLabelId():String{
			return _localizedLabel;
		}
			
		public function set localizedLabelId(key:String):void{
			if (key){
				Localization.bindPropertyString(this, "label", key);
	            _localizedLabel = key;
			}
		}
		
		public function get localizedTooltipId():String{
			return _localizedTooltip;
		}
			
		public function set localizedTooltipId(key:String):void{
			if (key){
				Localization.bindPropertyString(this, "toolTip", key);
	            _localizedTooltip = key;
			}
		}
	}
}