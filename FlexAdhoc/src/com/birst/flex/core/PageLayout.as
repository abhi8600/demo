package com.birst.flex.core
{
	import mx.collections.ArrayCollection;
	
	public class PageLayout
	{
		public static var LAYOUT:Array =[
			"LETTER",
			"NOTE",
			"LEGAL",
			"HALFLETTER",
			"11x17",
			"LEDGER",
			"CUSTOM",
			"A0",
			"A1",
			"A2",
			"A3",
			"A4",
			"A5",
			"A6",
			"A7",
			"A8",
			"A9",
			"A10",
			"B0",
			"B1",
			"B2",
			"B3",
			"B4",
			"B5",
			"ARCH_A",
			"ARCH_B",
			"ARCH_C",
			"ARCH_D",
			"ARCH_E",
			"FLSA",
			"FLSE"	
		];
		
		public static var LAYOUT_SIZE:Array = new Array();
		LAYOUT_SIZE[LAYOUT[0]] = {width:612, height:792};
		LAYOUT_SIZE[LAYOUT[1]] = {width: 540, height: 720};
		LAYOUT_SIZE[LAYOUT[2]] = {width: 612, height: 1008};
		LAYOUT_SIZE[LAYOUT[3]] = {width:396, height:612};
		LAYOUT_SIZE[LAYOUT[4]] = {width:792, height:1224};
		LAYOUT_SIZE[LAYOUT[5]] = {width:792, height:1224};
		LAYOUT_SIZE[LAYOUT[6]] = {width:0, height:0};
		LAYOUT_SIZE[LAYOUT[7]] = {width: 2380, height: 3368};
		LAYOUT_SIZE[LAYOUT[8]] = {width:1684, height:2380};
		LAYOUT_SIZE[LAYOUT[9]] = {width:1190, height:1684};
		LAYOUT_SIZE[LAYOUT[10]] = {width:842, height:1190};
		LAYOUT_SIZE[LAYOUT[11]] = {width:595, height:842};
		LAYOUT_SIZE[LAYOUT[12]] = {width:421, height:595};
		LAYOUT_SIZE[LAYOUT[13]] = {width:297, height:421};
		LAYOUT_SIZE[LAYOUT[14]] = {width:210, height:297};
		LAYOUT_SIZE[LAYOUT[15]] = {width:148, height:210};
		LAYOUT_SIZE[LAYOUT[16]] = {width:105, height:148};
		LAYOUT_SIZE[LAYOUT[17]] = {width:74, height:105};
		LAYOUT_SIZE[LAYOUT[18]] = {width:2836, height:4008};
		LAYOUT_SIZE[LAYOUT[19]] = {width:2004, height:2836};
		LAYOUT_SIZE[LAYOUT[20]] = {width:1418, height:2004};
		LAYOUT_SIZE[LAYOUT[21]] = {width:1002, height:1418};
		LAYOUT_SIZE[LAYOUT[22]] = {width:709, height:1002};
		LAYOUT_SIZE[LAYOUT[23]] = {width:501, height:709};
		LAYOUT_SIZE[LAYOUT[24]] = {width:595, height:842};
		LAYOUT_SIZE[LAYOUT[25]] = {width:595, height:842};
		LAYOUT_SIZE[LAYOUT[26]] = {width:595, height:842};
		LAYOUT_SIZE[LAYOUT[27]] = {width:595, height:842};
		LAYOUT_SIZE[LAYOUT[28]] = {width:595, height:842};
		LAYOUT_SIZE[LAYOUT[29]] = {width:612, height:936};
		LAYOUT_SIZE[LAYOUT[30]] = {width:612, height:936};
		
			
		public static function getLayout(size:String, orientation:String):Object{
			var width:int = 0;
			var height:int = 0;
			
			//Find and set our layout based on the defined layouts
			var definedLayout:Object = LAYOUT_SIZE[size];
			if (definedLayout){
				if (orientation.toUpperCase() == "LANDSCAPE"){
					width = definedLayout.height;
					height = definedLayout.width;			
				}else{
					width = definedLayout.width;
					height = definedLayout.height;
				}
			}
			
			return {width: width, height: height};
		}
		
		public function PageLayout()
		{
		}

	}
}