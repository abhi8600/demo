package com.birst.flex.core
{
	import com.birst.flex.modules.ITabModule;
	
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
	import flash.events.Event;
	
	import mx.containers.VBox;
	import mx.core.Application;
	import mx.events.FlexEvent;
	import mx.events.ModuleEvent;
	import mx.modules.IModuleInfo;
	import mx.modules.ModuleManager;
	import mx.resources.ResourceManager;
	
	public class AppModuleManager
	{
		private var _modules:Array;
		private var _numToLoad:int;
		
		public function AppModuleManager()
		{
			_modules = [];
			_numToLoad = 0;
		}
		
		public function addModuleToLoad(moduleName:String, 
								   parent:DisplayObjectContainer, 
								   loaded:Function = null, 
								   error:Function = null, 
								   defaultHandler:Boolean = true):void
		{
								   	
			
			
			moduleName = pathify(moduleName);
			
			//no-op for loaded module
			if (isModuleLoaded(moduleName) == true){
				Tracer.debug('module ' + moduleName + ' was already loaded ', this);
				return;
			}
						
			//All our predefined event handlers should execute before the user's event handlers, hence higher priority = 9999
			var mod:IModuleInfo = ModuleManager.getModule(moduleName);	
			mod.addEventListener(ModuleEvent.PROGRESS, onProgress, false, 9999);
			
			if (defaultHandler == true)
				mod.addEventListener(ModuleEvent.READY, onReady, false, 9999);
				
			if (loaded != null)
				mod.addEventListener(ModuleEvent.READY, loaded);
			
			if (defaultHandler == true)	
				mod.addEventListener(ModuleEvent.ERROR, onError, false, 9999);
				
			if (error != null)
				mod.addEventListener(ModuleEvent.READY, error);
			
			this.addModule(moduleName, mod, parent);
		}	
		
		public function startLoad():void{
			_numToLoad = 0;
			for (var k:String in _modules) {
				var modObj:Object = _modules[k];
				var module:IModuleInfo = modObj.moduleObject;
				_numToLoad++;
				module.load();
			}
		}
		
		protected function onReady(event:ModuleEvent):void {
			var module:IModuleInfo = event.module;
			var intModInfo:Object = this.getModule(module.url);
			
			//Get the parent and attach our module to it
			if (intModInfo){
				var parent:DisplayObjectContainer = intModInfo.attachTo;
				var child:DisplayObject = module.factory.create() as DisplayObject;
				intModInfo.displayObject = child;
				if (child is ITabModule && parent is VBox){
					var childTab:ITabModule = child as ITabModule;
					var vbox:VBox = parent as VBox;
					var locKey:String = childTab.getLabelLocKey();
					if (locKey && locKey != ""){
						vbox.label = CoreApp(Application.application).getLocString(locKey);
						ResourceManager.getInstance().addEventListener(Event.CHANGE,
																	   function(event:Event):void{
																	   		vbox.label = CoreApp(Application.application).getLocString(locKey);
																	   });
					}else{
						vbox.label = childTab.getLabel();
					}
					vbox.addEventListener(FlexEvent.HIDE, childTab.tabHide);
					vbox.addEventListener(FlexEvent.SHOW, childTab.tabShow);
				}
				parent.addChild(child);
			}else{
				//Shouldn't get here
				Tracer.error("AppModuleManager.onReady() cannot find internal module info: " + module.url, this);
			}
			
			--_numToLoad;
			if (_numToLoad == 0){
				AppEventManager.fire(new AppEvent(AppEvent.EVT_ALL_MODULES_LOADED, 'AppModuleManager', 'onReady', this));
			}
    	}

		protected function onProgress(event:ModuleEvent):void{
			var module:IModuleInfo = event.module;
		}
		
		protected function onError(event:ModuleEvent):void{
			var module:IModuleInfo = event.module;
			Util.localeAlertError('MM_failLoad', null, 'sharable', [ module.url, event.errorText ]);
			
			//retry?
		}
		
		public function isModuleLoaded(name:String):Boolean{
			var module:Object = getModule(name);
			return (module != null && module.moduleObject != null && module.moduleObject.loaded == true);	
		}
		
		//Add the appropriate prefix, suffix and version
		public static function pathify(name:String):String{
			var moduleName:String = name;
	
	/*     GS 
		Modifying the addition of /SMIWeb/swf to /swf
		all the modules now reside on ASP.NET under swf directory 
	*/ 
			//prefix with "/swf/"
			if (moduleName.indexOf("/swf/") == -1)
				moduleName = "/swf/" + moduleName;
				
			//add extension if required
			if (moduleName.indexOf(".swf") == -1)
				moduleName += ".swf";
			
			//add version
			if (moduleName.indexOf("?v=") == -1){	
				var version:String = Util.getApplicationVersion();
				if (version != null){
					moduleName += "?v=" + version; 
				}
			}
			return moduleName;
		}
		
		public function getModule(name:String):Object {
			var moduleName:String = pathify(name);
			return _modules[moduleName];
		}
			
		private function addModule(name:String, moduleObj:Object, parent:DisplayObjectContainer):void{
			if (name != null && parent != null && moduleObj != null)
				_modules[name] = { moduleObject: moduleObj, attachTo: parent, displayObject: null };
		}
	}
}