package com.birst.flex.core
{
	import mx.containers.HBox;

	[Style(name="fillColors", type="Array", arrayType="uint", format="Color")]
	[Style(name="fillAlphas", type="Array", arrayType="Number")]
	[Style(name="cornerRadius", type="Number")]
	
	public class GradientHBox extends HBox
	{
		public function GradientHBox()
		{
			super();
		}
		
		override protected function updateDisplayList(w: Number, h: Number):void
        {
            super.updateDisplayList (w, h);
			Util.drawGradient(this, w, h);
        } 
	}
}