package com.birst.flex.core
{
	import flash.events.Event;
	
	import mx.binding.utils.BindingUtils;
	import mx.collections.ArrayCollection;
	import mx.core.Application;
	import mx.resources.IResourceManager;
	import mx.resources.ResourceManager;
	
	//Metadata required for us to use adhoc.properties files under locale/{locale} where 
	//{locale} could be en_US, es_ES, etc. Add multiple declarations for multiple locale files
	//[ResourceBundle("adhoc")]
	
	public class Localization
	{
		//Name of the Adhoc resouce bundle
		[Bindable]
		public static var ADHOC:String = "adhoc";
		
		//Shared resource bundle
		[Bindable]
		public static var SHARABLE:String = "sharable";
		
		//Shared resource bundle
		[Bindable]
		public static var SCHEDULER:String = "scheduler";		
		
		public function Localization()
		{
		}

		/**
		 * Binds the obj's parameter to the localized key's value. If the localization chain changes, then
		 * our object parameter's value changes automatically. Resource bundle defaults to adhoc
		 */
		public static function bindPropertyString(obj:Object, param:String, key:String, bundle:String = null):void{
			if (key){
				if(!bundle){
					bundle = getAppMainBundle();	
				}
				
				BindingUtils.bindProperty(obj, param, ResourceManager.getInstance(),
                    						{ 
                    							name: "getString",
                        						getter: function(resMan:IResourceManager):String {
                        							return resMan.getString(bundle, key);
                        						}
                    						});
			}		
		}
		
		public static function localizeSetDataProvider(component:Object, arrayCol:ArrayCollection, bundle:String = null):void{
			
			if (component.hasOwnProperty('dataProvider')){
				if(!bundle){
					bundle = getAppMainBundle();	
				}
			
				if(localizeArrayCollection(arrayCol, bundle)){
					ResourceManager.getInstance().addEventListener(Event.CHANGE, function(event:Event):void{
												    var array:ArrayCollection = ArrayCollection(component.dataProvider);
													Localization.localizeArrayCollection(array, bundle);
													component.dataProvider = array;
											    });
				}
				component.dataProvider = arrayCol;
			}
		}
		
		/**
		 * Localize any of the label in the array collection if it has the property labelLocKey
		 */
		public static function localizeArrayCollection(collection:ArrayCollection, bundle:String = null):Boolean{
			if(!bundle){
				bundle = getAppMainBundle();	
			}
			
			var didLocalized:Boolean = false;
			for(var i:int; i < collection.length; i++){
				var x:Object = collection.getItemAt(i);
				localizeAll(x, bundle) && (didLocalized = true);
				if(x.children){
					if (x.children is Array){
						recurseChildren(x.children as Array, bundle) && (didLocalized = true);
					}else if (x.children is ArrayCollection){
						localizeArrayCollection(x.children as ArrayCollection) && (didLocalized = true);
					}
				}
			}
			return didLocalized;
		}
		
		private static function recurseChildren(obj:Array, bundle:String):Boolean{
			var didLocalized:Boolean = false;
			for(var i:int = 0; i < obj.length; i++){
				var x:Object = obj[i];
				localizeAll(x, bundle) && (didLocalized = true);
				if(x.children){
					recurseChildren(x.children as Array, bundle) && (didLocalized = true);
				}
			}
			return didLocalized;
		}
		
		private static function localizeAll(obj:Object, bundle:String):Boolean{
			return localizeLabel(obj, bundle) || localizeTooltip(obj, bundle);
		}
		
		/**
		 * If the object has property labelLocKey (resource key), localize its label
		 */
		public static function localizeLabel(x:Object, bundle:String = null):Boolean{
			
			
			if (x.hasOwnProperty('labelLocKey')){
				x.label = localizeString(x.labelLocKey, bundle);
				return true;
			}	
			return false;
		}
		
		public static function localizeString(s:String, bundle:String = null):String {
			if(!bundle){
				bundle = getAppMainBundle();	
			}
			return ResourceManager.getInstance().getString(bundle, s);
		}
		
		/**
		 * If the object has property localizedTooltipId (resource key), localize its tooltip
		 */
		public static function localizeTooltip(x:Object, bundle:String = null):Boolean{
			if (x.hasOwnProperty('tooltipLocKey')){
				x.tooltip = localizeString(x.tooltipLocKey, bundle);
				return true;
			}	
			return false;
		}
		
		public static function getAppMainBundle():String{
			if (Application.application is CoreApp){
				return CoreApp(Application.application).mainBundle;
			}
			//historically, we only had adhoc.properties file
			return Localization.ADHOC;
		}
	}
}