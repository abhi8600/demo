package com.birst.flex.core
{
	import flash.utils.getQualifiedClassName;
	
	import mx.logging.ILogger;
	import mx.logging.Log;
	import mx.logging.LogEvent;
	import mx.logging.LogEventLevel;
	import mx.logging.targets.TraceTarget;

	public class Tracer extends TraceTarget
	{
		private static var _instance:Tracer;
		private var _ilogger:ILogger;
		
		public function Tracer(enforcer:Enforcer)
		{
			super();
			
			this.level = LogEventLevel.ALL;
			this.includeDate = true;
			this.includeTime = true;
			this.includeCategory = true;
			this.includeLevel = true;
			this.filters = ["birst", "com.birst.flex.*"];
			
			Log.addTarget(this);
		}
		
		override public function logEvent(event:LogEvent):void{
			var category:String = ILogger(event.target).category;
			var idx:int = category.indexOf("mx");
			if(idx < 0){
				super.logEvent(event);
			}
		}
		
		public static function getInstance():Tracer{
			if(!_instance){
				_instance = new Tracer(new Enforcer());
			}
			return _instance;
		}
		
		public function getDefaultLogger():ILogger{
			if(!_ilogger){
				_ilogger = Log.getLogger("birst");
			}	
			return _ilogger;	
		}
		
		public static function error(msg:String, obj:Object = null):void{
			log(LogEventLevel.ERROR, msg, obj);
		}
		
		public static function debug(msg:String, obj:Object = null):void{
			log(LogEventLevel.DEBUG, msg, obj);
		}
		
		public static function log(level:int, msg:String, obj:Object = null):void{
			CONFIG::debug{
				var logger:ILogger = getInstance().getDefaultLogger();
				if(obj){
					var category:String = flash.utils.getQualifiedClassName(obj).replace('::','.');
					logger = Log.getLogger(category);
				}
				logger.log(level, msg);
			}
		}
	}
}

class Enforcer{
	public function Enforcer(){}
}	