package com.birst.flex.core.maps
{
	public interface BaseMapInterface
	{
		function mapSetSize(width:Number, height:Number):void;
		function createLatLng(lat:Number, lon:Number):Object;
		function createPolygon(key:String, value:String, latLngs:Array):void;
		function centerOnBounds():void;
	}
}