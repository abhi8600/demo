package com.birst.flex.core.maps
{
	import com.afcomponents.umap.display.InfoWindow;
	import com.afcomponents.umap.events.InfoWindowEvent;
	import com.afcomponents.umap.overlays.Polygon;
	
	import flash.events.MouseEvent;
	
	import mx.controls.ToolTip;
	import mx.managers.ToolTipManager;

	public class UMapperPolygon extends Polygon
	{
		public var tooltip:ToolTip;
		public var amount:Number;
		
		public function UMapperPolygon(param:Object = null, style:Object = null)
		{
			super(param, style);
			addEventListener(MouseEvent.ROLL_OVER, onRollOver);
			addEventListener(MouseEvent.ROLL_OUT, onRollOut);
			addEventListener(InfoWindowEvent.OPEN, onInfoWindowOpen);
		}
		
		public function set infoWindowContent(cnt:String):void{
			this.infoParam = new Object();
			this.infoParam.content = cnt;
			
			this.infoStyle = InfoWindow.getDefaultStyle();
			this.infoStyle.close = false;
			this.infoStyle.title = false;
		}
		
		public function get infoWindowContent():String{
			return (infoParam && infoParam.content) ? infoParam.content : null;
		}
		
		protected function onRollOver(event:MouseEvent):void{
			tooltip = ToolTip(ToolTipManager.createToolTip(infoWindowContent, 0, 0));
			tooltip.setStyle("backgroundColor",0xFFCC00);
			UMapperOverlayOps.positionToolTip(this, tooltip, event);
		}
		
		protected function onRollOut(event:MouseEvent):void{
			if(tooltip){
				ToolTipManager.destroyToolTip(tooltip);
				tooltip = null;
			}
		}
		
		protected function onInfoWindowOpen(event:InfoWindowEvent):void{
			onRollOut(null);
		}
	}
}