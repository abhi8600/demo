package com.birst.flex.core.maps
{
	import com.afcomponents.umap.display.InfoWindow;
	import com.afcomponents.umap.styles.InfoWindowStyle;
	
	import flash.events.MouseEvent;
	import flash.geom.Point;
	
	import mx.controls.ToolTip;
	
	public class UMapperOverlayOps
	{
		public function UMapperOverlayOps()
		{
		}

		public static function getInfoWindowObj(overlay:Object, content:String):Object{
			var style:InfoWindowStyle = InfoWindow.getDefaultStyle();
			style.close = false;
			style.title = false;
			
			var params:Object = new Object();
			params.content = content;
			
			return params;
		}
		
		public static function positionToolTip(overlay:Object, toolTip:ToolTip, event:MouseEvent):void{
			var pt:Point = new Point(event.localX, event.localY);
           	pt =  overlay.localToGlobal(pt);
			toolTip.x = pt.x;
			toolTip.y = pt.y;           		
		}
	}
}


