package com.birst.flex.core.maps
{
	import com.afcomponents.umap.core.UMap;
	import com.afcomponents.umap.events.DisplayEvent;
	import com.afcomponents.umap.events.MapEvent;
	import com.afcomponents.umap.gui.BingControl;
	import com.afcomponents.umap.gui.UIMapControl;
	import com.afcomponents.umap.styles.GeometryStyle;
	import com.afcomponents.umap.styles.MarkerStyle;
	import com.afcomponents.umap.types.LatLng;
	import com.afcomponents.umap.types.LatLngBounds;
	import com.birst.flex.core.Tracer;
	import com.birst.flex.core.Util;
	import com.google.maps.extras.gradients.GradientBar;
	import com.google.maps.extras.gradients.GradientControl;
	import com.google.maps.extras.gradients.GradientRule;
	import com.google.maps.extras.gradients.GradientRuleList;
	
	import flash.utils.Dictionary;
	
	import mx.containers.HBox;
	import mx.containers.VBox;
	import mx.controls.Label;
	import mx.core.EdgeMetrics;
	import mx.core.UIComponent;
	import mx.resources.ResourceManager;
	
	public class UMapperMaps extends VBox
	{
		private var map:UMap;
		private var mapWrapper:UIComponent;
		private var gradientBar:GradientBar;
		private var bounds:LatLngBounds;
		
		public var _xml:XML;
		protected var _bubbles:Array = new Array();
		protected var _markers:Array = new Array();
		protected var _polygons:Array = new Array();
		protected var customPolyColors:Dictionary = new Dictionary();
		protected var nameY:Dictionary = new Dictionary();
		protected var nameYToolTip:Dictionary = new Dictionary();
		
		private var gradientBarHeight:int = 20;
		private var gradientRuleList:GradientRuleList;
		private var displaySwatch:Boolean = true;
		private var colors:Array = [];
		private var msgBox:VBox;
		
		public function UMapperMaps(){
			super();
			setStyle('verticalGap', 0);
		}
		
		public function get xml():XML{
			return this._xml;
		}
		
		public function set xml(data:XML):void{
			this._xml = data;
			this.displaySwatch = displaySwatch = _xml && _xml.chart_settings.controls.color_swatch.length() > 0;
			this.gradientBarHeight = (displaySwatch) ? 20 : 0;
			
			if(this._xml){
				var clrs:XMLList = _xml.palettes.palette.gradient.key;
				var defaultColors:Boolean = false;
				for(var i:int = 0; !defaultColors && i < clrs.length(); i++){
					var key:XML = clrs[i];
					var colorStr:String = key.@color.toString();
					if(colorStr == 'green'){
						//hack here - green indicates default values, ie no user defined color palette
						defaultColors = true;
						continue;
					}
					var rgbValue:int = Util.rgbString2int(colorStr);
					this.colors.push(rgbValue);
				}		
			}
		}
		
		override protected function updateDisplayList(unscaledWidth:Number, unscaledHeight:Number):void{
			super.updateDisplayList(unscaledWidth, unscaledHeight);
			if(Util.getApplicationBingMapsOn()){
				// Get information about the container border area. 
	            // The usable area of the container for its children is the 
	            // container size, minus any border areas.
	            var vm:EdgeMetrics = viewMetricsAndPadding;
	            
	            var mapHeight:Number = unscaledHeight - gradientBarHeight - vm.bottom;
				mapSetSize(unscaledWidth, mapHeight);
				mapWrapper.setActualSize(unscaledWidth, mapHeight);
				if(gradientBar){
					gradientBar.setActualSize(unscaledWidth, gradientBarHeight);
					gradientBar.drawGradientRule(gradientRuleList);
					gradientBar.move(gradientBar.x, mapWrapper.y + mapHeight);
				}
			}else{
				msgBox.setActualSize(unscaledWidth, unscaledHeight);
			}
		}
		
		override protected function createChildren():void{
			super.createChildren();
			if(Util.getApplicationBingMapsOn()){
				if(!map){
					map = new UMap();
					map.addEventListener(DisplayEvent.UPDATE_POSITION, onMapUpdatePosition);
					map.addEventListener(MapEvent.READY, onMapReady);
					mapWrapper = new UIComponent();
					mapWrapper.height = this.height - gradientBarHeight;
					mapWrapper.width = this.width;
					mapWrapper.addChild(map);
					addChild(mapWrapper);
	            
	            	if(displaySwatch){
						gradientBar = new GradientBar();
						gradientBar.height = gradientBarHeight;
						gradientBar.width = width;
						gradientBar.setStyle('borderStyle', 'solid');
						gradientBar.setStyle('borderThickness', 2);
						addChild(gradientBar);
						calculatePresetGradient();
	            	}
				}
			}else{
				if(!this.msgBox){
					msgBox = new VBox();
					msgBox.percentHeight = 100;
					msgBox.percentWidth = 100;
					msgBox.setStyle('verticalAlign', 'middle');
					var hbox:HBox = new HBox();
					hbox.percentWidth = 100;
					hbox.setStyle('horizontalAlign', 'middle');
					var label:Label = new Label();
					label.percentWidth = 100;
					label.text = mx.resources.ResourceManager.getInstance().getString('sharable', 'UM_noLicense');
					hbox.addChild(label);
					msgBox.addChild(hbox);
					addChild(msgBox);
				}
			}
		}
		
		private function calculatePresetGradient():void{
			calculateGradient(0, 25, 50, 100);
		}
		
		private function calculateGradientFromMinMax(minValue:Number, maxValue:Number):void{
			if(colors.length >= 2){
				var rules:Array = new Array();
				var prev:GradientRule;
			
				var rangeIncrement:Number = (maxValue - minValue) / colors.length;
				var borderValues:Array = [];
				borderValues.push(0);
				var rangeValue:Number = minValue;
				
				for(var i:int = 0; i < colors.length; i++){
					var color:Number = Number(colors[i]);
					//last one
					if(i == colors.length - 1){
						prev.maxColor = color;
						prev.maxValue = maxValue;
						borderValues.push(maxValue);
						continue;
					}
					
					var g:GradientRule = new GradientRule();
					g.minColor = color;
					if(i == 0){
						g.minValue = minValue;
					}
					if(prev){
						prev.maxColor = color;
					}
					g.maxValue = rangeValue + rangeIncrement;
					prev = g;
					rules.push(g);
					
					borderValues.push(rangeValue);
					rangeValue += rangeIncrement;
				}
				gradientRuleList = new GradientRuleList(rules);
				if(gradientBar){
					gradientBar.borders = borderValues;
					gradientBar.drawGradientRule(gradientRuleList);
				}
			}else{
				var range:Number = maxValue - minValue;
				var n1:Number = minValue + (range / 4);
				var n2:Number = n1 + (range / 4); 
				calculateGradient(minValue, n1, n2, maxValue);
			}
		}
		
		private function calculateGradient(n0:Number, n1:Number, n2:Number, n3:Number):void{
			var g1:GradientRule = new GradientRule();
		   	g1.minValue = n0;
		   	g1.maxValue = n1;
		    //g1.minColor = 0x008000;
		    g1.minColor = 0x00cc99;
		    g1.maxColor = 0xffff00;
		
		   	var g2:GradientRule = new GradientRule();
		   	g2.maxColor = 0xff7700;
		    g2.maxValue = n2;
		
		    var g3:GradientRule = new GradientRule();
		    g3.maxValue = n3;
		    g3.maxColor = 0xff0000;
			
		    gradientRuleList = new GradientRuleList([g1, g2, g3]);
		    if(gradientBar){
			    gradientBar.borders = [0, n1, n2, n3];
			    gradientBar.drawGradientRule(gradientRuleList);
		    }
		}
		
		private function onMapReady(event:MapEvent):void{
			var bingCtrl:BingControl = new BingControl();
			bingCtrl.display = UIMapControl.DISPLAY_COMPACT;
			map.addControl(bingCtrl);
			parseMarkersGetPlacemarks();
		}

		protected function parseBubbleScatterMarkers():void{

			if(xml){
				resetOverlaysList();
				
				//Bubble vars
				var points:Array = new Array();
				var minRadius:Number = 8;
				var maxRadius:Number = 22;
				var minSize:Number = 1;
				var maxSize:Number = 2;
				var values:Array = new Array();
				
				var series:XMLList = xml.data.series;
				var seriesCount:int = series.length();
				for (var i:int = 0; i < seriesCount; i++){
					var s:XML = series[i];
					var seriesName:String = s.@name.toString();
					if(s.@type == 'Marker'){
						var markers:XMLList = s.point;
						for(var x:int = 0; x < markers.length(); x++){
							var m:XML = markers[x] as XML;
							createMarker(m, seriesName);
						}
					}else if(s.@type == 'Bubble'){
						var point:XMLList = s.point;
						if(point.length() > 0){
							var xml:XML = point[0];
							var latLon:LatLng = new LatLng(Number(xml.@y), Number(xml.@x))
							var size:Number = Number(xml.@size);
							if(i == 0){
								minSize = maxSize = size;
							}
							if(size > maxSize){
								maxSize = size;
							}else if (size < minSize){
								minSize = size;
							}
							var obj:Object = { name:seriesName, ll: latLon, sz: size };
							values.push(size);
							points.push(obj);
						}
					}
				}//for(var i..
				
				//Bubble 
				if(points.length == 1){
					addCircle(points[0], minRadius); 	
				}else{
					var range:Number = maxSize - minSize;
					points.sortOn('sz', Array.NUMERIC | Array.DESCENDING);
					for(var y:int = 0; y < points.length; y++){
						var data:Object = points[y] as Object;
						var ratio:Number = ((Number(data.sz) - minSize) / range);
						var radiusSize:Number = (data.sz <= 0) ? minSize :  ratio * maxRadius;
						if(radiusSize < minRadius){
							radiusSize = minRadius;
						}
						addCircle(data, radiusSize);		
					} 
					
				}
				
				centerOnBounds();
			}//if(xml)
		}	
		
		protected function parseMarkersGetPlacemarks():void{
			if(xml){
				var mapSeries:XML = xml.data_plot_settings.map_series[0];
				var source:String = mapSeries.@source.toString();
				
				var hasCustomColors:Boolean = false;
				
				var polygonKeys:XMLList = xml.data.series.(@type == 'Bar');
				var polySize:int = polygonKeys.length();
				if(source != "none" && polySize > 0){
					for (var i:int = 0; i < polySize; i++){
						var s:XML = polygonKeys[i];
						var points:XMLList = s.point;
						var count:int = points.length();
						if(count > 0){
							for(var y:int = 0; y < count; y++){
								var p:XML = points[y];
								var name:String = p.@name.toString();
								if(!nameY[name]){
									nameY[name] = p.@y.toString();
									var attributes:XMLList = p.attributes.attribute.(@name == 'umap');
									var cnt:int = attributes.length();
									if (cnt > 0) {
										var attr:String = attributes[0].toString();
										nameYToolTip[name] = attr;
									} 
								}
								
								var color:String = p.@color.toString();
								if(color && color.length > 0){
									this.customPolyColors[name] = Util.rgbString2int(color);
									hasCustomColors = true;
								}
							}	
						}
						
					}
					
					parsePlacemarks(hasCustomColors);
				}else{
					removeGradientBar();
					parseBubbleScatterMarkers();
				}
			}else{
				dispatchRendered();
			}
		}
		
		private function removeGradientBar():void{
			//Don't show the color range
			if(gradientBar){
				gradientBarHeight = 0;
				removeChild(gradientBar);
				gradientBar = null;
				validateNow();	
			}	
		}
		
		private function parsePlacemarks(hasCustomColors:Boolean = false):void{
			if(xml){
				
				var minValue:Number;
				var maxValue:Number;
				
				var values:Array = new Array();
				var polys:XMLList = xml.placemarks.pm;
				var numPolys:int = polys.length();
				for(var i:int = 0; i < numPolys; i++){
					var pol:XML = polys[i];
					var key:String = pol.@id.toString();
					var coords:XMLList = pol.coords;
					for(var y:int = 0; y < coords.length(); y++){
						var latlngs:Array = new Array();
						var cordsStr:String = coords[y].toString(); //-121.66521992838,38.1692852818694-121.659581042989...
						var cordPairs:Array = cordsStr.split(' ');
						for(var z:int = 0; z < cordPairs.length; z++){
							var latlon:Array = (cordPairs[z] as String).split(',');
							var lat:Number = Number(latlon[0]);
							var lon:Number = Number(latlon[1]);
							var latlng:Object = createLatLng(lat, lon);
							latlngs.push(latlng);
						}
						
						var value:String = nameY[key];
						var numValue:Number = Number(value);
						if(numValue){
							if(i == 0){
								minValue = maxValue = numValue;
							}else{
								if(numValue < minValue){
									minValue = numValue;
								}
								if(numValue > maxValue){
									maxValue = numValue;
								}
							}
							values.push(numValue);
						}
						createPolygon(key, value, latlngs);
					}
				}
				
				//No polys, error'ed?
				if(numPolys == 0){
					var error:XMLList = xml.placemarks.error;
					if(error.length() > 0){
						var msg:String = error.toString();
						var code:String = error.@code.toString();
						var resourceErrStr:String = ResourceManager.getInstance().getString('errors', code, [msg]);
						Util.alertErrMessage(resourceErrStr);
					}
				}
				
				if(!hasCustomColors){
					calculateGradientFromMinMax(minValue, maxValue);
					colorRangePolygons(values);
				}else{
					if(this.gradientBar){
						Util.hide(this.gradientBar);
					}
				}
				parseBubbleScatterMarkers();
			}
		}		
		
		 public function mapSetSize(width:Number, height:Number):void{
			map.setSize(width, height);
		}
		
		 public function createMarker(xml:XML, seriesName:String):void{
			var latLon:LatLng = new LatLng(Number(xml.@y), Number(xml.@x))
			
			var marker:UMapperMarker = new UMapperMarker();
			marker.position = latLon;
			var style:MarkerStyle = createDefaultMarkerStyle();
			marker.setStyle(style);
			marker.infoWindowContent = seriesName;

			map.addOverlay(marker);
			
			if(!bounds){
				bounds = new LatLngBounds(latLon, latLon);
			}else{
				bounds.addLatLng(latLon);
			}
			
			_markers.push(marker);
		}
		
		private function addCircle(data:Object, radiusSize:Number):void{
			var circle:UMapperMarker = new UMapperMarker();
			var style:MarkerStyle = this.createDefaultMarkerStyle();
			style.fillRGB = 0x0066A5; //Blue
			style.radius = radiusSize;
			
			var latLon:LatLng = LatLng(data.ll);
			circle.position = latLon;
			circle.setStyle(style);
			circle.infoWindowContent = data.name + ': ' + data.sz;
			map.addOverlay(circle);
			if(!bounds){
				bounds = new LatLngBounds(latLon, latLon);
			}else{
				bounds.addLatLng(latLon);
			}
			
			_bubbles.push(circle);
		}
		
		 public function createPolygon(key:String, value:String, latLngs:Array):void{
			var tt:String = key;
			var ttvalue:String = nameYToolTip[key];
			var value:String = nameY[key];
			
			if(ttvalue){
				tt += ": " + ttvalue;
			} else if(value){
				tt += ": " + value;
			}
			
			var mapPoly:UMapperPolygon = new UMapperPolygon();
			mapPoly.points = latLngs;
			mapPoly.infoWindowContent = tt;
			mapPoly.amount = Number(value);
			
			if(!bounds){
				bounds = mapPoly.getBoundsLatLng().clone();
			}else{
				bounds.addBounds(mapPoly.getBoundsLatLng());
			}
			
			var customColor:Number = this.customPolyColors[key];
			if(customColor){
				setPolygonColor(mapPoly, customColor);	
			}
			
			map.addOverlay(mapPoly);
			_polygons.push(mapPoly);
		}
		
		private function colorRangePolygons(values:Array):void{
			if(values.length > 0){
				var gradientControl:GradientControl = gradientRuleList.applyGradientToValueList(values);
				 for(var i:int = 0; i < _polygons.length; i++) {
				 	var p:UMapperPolygon = _polygons[i] as UMapperPolygon;
			        if (!isNaN(p.amount)) {
			          	var color:Number = gradientControl.colorForValue(p.amount);
			          	setPolygonColor(p, color);
			        }
				 }
			}
		}
		
		private function setPolygonColor(p:UMapperPolygon, color:Number):void{
			var style:MarkerStyle = createDefaultMarkerStyle();
          	style.fillRGB = color;
          	style.fillAlpha = 0.6;
          	p.setStyle(style);	
		}
		
		public function centerOnBounds():void{
			if(bounds){
				var zoomLevel:Number = map.getBoundsZoomLevel(bounds);
				// a single point zoom level goes to max 21, too much, zoom out a bit more
				zoomLevel = (zoomLevel > 20) ? 19 : zoomLevel;
				
				var center:LatLng = bounds.getCenter();
				try{ 
					map.setCenter(center, zoomLevel);
				}catch(e:Error){
					Tracer.debug(e.getStackTrace());
					//Try again as this fails on the first time (due to map not completely rendered?)
					//Bug 13902
					try{
						map.setCenter(center, zoomLevel);
					}catch(ex:Error){
						Tracer.debug("Tried second time but failed to set center: " + ex.getStackTrace());
					}
				}
			}
		}
		
		// Throw event MapRendered indicating we finished zooming and updating position the first time
		// due to map.setCenter() in centerOnBounds()
		private function onMapUpdatePosition(event:DisplayEvent):void{
			map.removeEventListener(DisplayEvent.UPDATE_POSITION, onMapUpdatePosition);
			dispatchRendered();
		}
		
		private function dispatchRendered():void{
			dispatchEvent(new UMapperEvent(UMapperEvent.MAP_RENDERED));
		}
		
		private function createDefaultMarkerStyle():MarkerStyle{
			var style:MarkerStyle = new MarkerStyle();
			style.fill = GeometryStyle.RGB;
 			style.fillAlpha = 1;
 			style.strokeThickness = 1;
 			style.strokeRGB = 0x999999;
 			style.strokeAlpha = 1.0;
			style.fillRGB = 0xF2841B; //orange
			return style;
		}
		
		public function createLatLng(lat:Number, lon:Number):Object{
			return new LatLng(lat, lon);
		}
		
		private function resetOverlaysList():void{
			this._bubbles = [];
			this._markers = [];
			this._polygons = [];	
		}
	}
}