package com.birst.flex.core.maps
{
	import com.birst.flex.core.Util;
	import com.birst.flex.webservice.WebServiceClient;
	import com.birst.flex.webservice.WebServiceResult;
	
	import flash.display.DisplayObject;
	import flash.utils.Dictionary;
	
	import mx.core.UIComponent;
	import mx.rpc.events.ResultEvent;
	
	public class BaseMap extends UIComponent implements BaseMapInterface
	{
		public var xml:XML;
		
		protected var bubbles:Array = new Array();
		protected var markers:Array = new Array();
		
		protected var nameY:Dictionary = new Dictionary();
		
		public function BaseMap(mapObj:DisplayObject) {
			addChild(mapObj);	
		}
		
		override protected function updateDisplayList(unscaledWidth:Number, unscaledHeight:Number):void{
			mapSetSize(unscaledWidth, unscaledHeight);
			super.updateDisplayList(unscaledWidth, unscaledHeight);
		}
		
		
		public function mapSetSize(width:Number, height:Number):void{}
		public function createMarker(xml:XML, seriesName:String):void{}
		public function createCircle(xml:XML, seriesName:String):void{}
		public function createBubblePolygons(list:XMLList):void{};
		public function createPolygon(key:String, value:String, latLngs:Array):void{}
		public function centerOnBounds():void{}
		public function setSize(width:Number, height:Number):void{}
		
		public function createLatLng(lat:Number, lon:Number):Object{
			return null;
		}
		
		
		

		
	}
}