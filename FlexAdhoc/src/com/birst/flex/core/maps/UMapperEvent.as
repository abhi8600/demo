package com.birst.flex.core.maps
{
	import flash.events.Event;

	public class UMapperEvent extends Event
	{
		public static var MAP_RENDERED:String = "MapRendered";
		
		public function UMapperEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
		
	}
}