package com.birst.flex.core.maps
{
	import com.afcomponents.umap.display.InfoWindow;
	import com.afcomponents.umap.interfaces.IInfoWindow;
	import com.afcomponents.umap.overlays.Marker;
	
	import flash.events.MouseEvent;
	import flash.geom.Point;
	
	import mx.controls.ToolTip;
	import mx.managers.ToolTipManager;

	public class UMapperMarker extends Marker
	{
		public var infoWindowContent:String;
		public var tooltip:ToolTip;
		
		public function UMapperMarker(param:Object = null, style:Object = null)
		{
			super(param, style);
			
			this.zIndexAuto = true;
			
			addEventListener(MouseEvent.ROLL_OVER, onRollOver);
			addEventListener(MouseEvent.ROLL_OUT, onRollOut);
		}
		

		override public function openInfoWindow(param:Object = null, style:Object = null):IInfoWindow{
			onRollOut(null);
			
			style = InfoWindow.getDefaultStyle();
			style.close = false;
			style.title = false;
			
			var params:Object = new Object();
			params.content = infoWindowContent;
			
			return super.openInfoWindow(params, style);
		}
		
		protected function onRollOver(event:MouseEvent):void{
			if(!tooltip){
				var pt:Point = new Point(event.localX, event.localY);
                pt =  this.localToGlobal(pt);
				tooltip = ToolTip(ToolTipManager.createToolTip(infoWindowContent, pt.x, pt.y));
				tooltip.setStyle("backgroundColor", 0xFFCC00);
			}
		}
		
		protected function onRollOut(event:MouseEvent):void{
			if(tooltip){
				ToolTipManager.destroyToolTip(tooltip);
				tooltip = null;
			}
		}
		
	}
}