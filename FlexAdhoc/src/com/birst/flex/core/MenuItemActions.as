package com.birst.flex.core
{
	import com.birst.flex.adhoc.dialogs.DialogPopUpEvent;
	
	import flash.events.Event;
	
	import mx.containers.TitleWindow;
	
	public class MenuItemActions
	{
		import flash.events.Event;
		import flash.display.DisplayObject;
		import mx.managers.PopUpManager;
		import mx.core.IFlexDisplayObject;
		import mx.core.Application;
		import mx.events.MenuEvent;
		
		public function MenuItemActions()
		{
		}
		
		/**
		 * Create a new popup dialog if one doesn't exist.
		 * Cache the new popup and reuse it for future.
		 */
		public static function popUp(klass:Class, menuItem:Object, modal:Boolean = true, centerPopUp:Boolean = true, moveToBottom:Boolean = false):IFlexDisplayObject{
			var display:DisplayObject = Application.application as DisplayObject;
			
			var popup:IFlexDisplayObject = menuItem.popUp;
			if (popup){
					PopUpManager.addPopUp(popup, display, modal);
			}
			
			if (!popup){
				popup = PopUpManager.createPopUp(display, klass, modal);
				menuItem.popUp = popup;	
			}
			
			if (menuItem.hasOwnProperty('titleLocKey') && popup is TitleWindow){
				var app:CoreApp = CoreApp(Application.application);
				TitleWindow(popup).title = app.getLocString(menuItem.titleLocKey); 	
			}else{
				//Add window title
				if (menuItem.hasOwnProperty('title') && popup is TitleWindow)
					TitleWindow(popup).title = menuItem.title;
			}
			
			//tell the dialog box its opened up
			var event:Event = new DialogPopUpEvent(SaveCancelDialogBox.POPPED_UP, menuItem);
			popup.dispatchEvent(event);
			
			if(centerPopUp){
				PopUpManager.centerPopUp(popup);
			}else {
				if(moveToBottom){
					Util.moveToRightBottom(popup);
				}
			}
			return popup;
		}
		
		
		/**
		 * Handler for menu item click event
		 */
		public static function menuItemClickHandler(event:MenuEvent):void{
			var menuItem:Object = event.item;
			invokeAction(menuItem);
		}
		
		public static function invokeAction(menuItem:Object, center:Boolean = true, moveToRightBottom:Boolean = false):void{
			if (menuItem.hasOwnProperty("klass")){
				var modal:Boolean = menuItem.hasOwnProperty("modal") ? Boolean(menuItem.modal) : true;
			 	if(menuItem.klass is Class)
					popUp(menuItem.klass, menuItem, modal, center, moveToRightBottom);
			}else if (menuItem.hasOwnProperty("fn")){
				if (menuItem.fn is Function)
					menuItem.fn(menuItem);
			}		
		}
	}
}