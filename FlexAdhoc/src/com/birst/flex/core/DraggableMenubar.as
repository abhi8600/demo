package com.birst.flex.core
{
	import flash.events.MouseEvent;
	
	import mx.controls.Label;
	import mx.controls.Menu;
	import mx.controls.MenuBar;
	import mx.core.DragSource;
	import mx.effects.IEffect;
	import mx.managers.DragManager;
	
	public class DraggableMenubar extends mx.controls.MenuBar
	{
		protected var dragSourceNVPair:Array;
		protected var _name:String, _identifier:String;
		protected var _effects:IEffect;
		public var accepted:Boolean = false;
		
		public function DraggableMenubar(name:String, identifier:String, effects:IEffect = null)
		{
			dragSourceNVPair = [];
			dragSourceNVPair['menubarName'] = _name = name;
			dragSourceNVPair['columnIndex'] = _identifier = identifier;
			
			if (effects){
				_effects = effects;
				_effects.target = this;
			}
			this.styleName = "draggableMenuBar";		
			this.addEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
			this.addEventListener(MouseEvent.MOUSE_UP, onMouseUp);
		}
		
		public function addDragSourceNVPair(key:String, val:Object):void {
			dragSourceNVPair[key] = val;
		}
		
		public function removeDragSourceValueByKey(key:String):void{
			if(dragSourceNVPair[key]){
				delete dragSourceNVPair[key];
			}
		}
		
		public function setDragSourceKeyValue(key:String, value:Object):void{
			dragSourceNVPair[key] = value;	
		}
		
		public function getEffects():IEffect{
			return _effects;	
		}
		
		protected function addDragSourcePairs(dragSource:DragSource):void{
			for (var key:String in dragSourceNVPair){
				dragSource.addData(dragSourceNVPair[key], key);
			}
		}
		
		protected function onMouseMove(event:MouseEvent):void {
			// Get the drag initiator component from the event object.
            var dragInitiator:MenuBar = event.currentTarget as MenuBar;
    
        	// Create a DragSource object.
        	var dragSource:DragSource = new DragSource();

        	// Add the data to the object.
        	addDragSourcePairs(dragSource);

       		// Create a bold label of the menubar text to use as a drag proxy.
        	var dragProxy:Label = new Label();
        	dragProxy.setStyle("fontWeight", "bold");
        	dragProxy.text = _name;
			
        	// Call the DragManager doDrag() method to start the drag. 
        	DragManager.doDrag(dragInitiator, dragSource, event, dragProxy);
        	
        	//Close any shown menu
        	closeMenus();
		}
		
		//Only add our event handler when the user clicks on us
		protected function onMouseDown(event:MouseEvent):void{
			this.addEventListener(MouseEvent.MOUSE_MOVE, onMouseMove);
		}
		
		//Remove our dragging event handler when the user releases their mouse
		protected function onMouseUp(event:MouseEvent):void{
			this.removeEventListener(MouseEvent.MOUSE_MOVE, onMouseMove);
		}			
		
		//Close menus when we move
		protected function closeMenus():void{
			var numMenus:int = this.menuBarItems.length;		
			for(var i:int = 0; i < numMenus; i++){
				var menu:Menu = this.getMenuAt(i);
				(menu) && menu.hide();
			}
		}
		
	}
}