package com.birst.flex.core
{
	import flash.display.DisplayObject;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	
	import mx.collections.ArrayCollection;
	import mx.controls.MenuBar;
	import mx.controls.ToolTip;
	import mx.controls.menuClasses.MenuBarItem;
	import mx.events.MenuEvent;
	import mx.managers.ToolTipManager;

	public class LocalizedMenuBar extends MenuBar
	{
		private var _tooltip:ToolTip = null;
		
		public function LocalizedMenuBar()
		{
			super();
			
			this.addEventListener(MouseEvent.MOUSE_OVER, mouseOverHandler);
			this.addEventListener(MouseEvent.MOUSE_OUT, mouseOutHandler);
			this.addEventListener(MouseEvent.CLICK, clickEventHandler);
			this.addEventListener(MenuEvent.ITEM_CLICK, menuClickEventHandler);
		}
		
		
		public function clickEventHandler(event:MouseEvent):void{
			var target:Object = event.target;
			
			//Load the module if it it not yet loaded
			if (target is MenuBarItem){
				if (target.data is MenuItemListModule){
					var mod:MenuItemListModule = MenuItemListModule(target.data);
					if (!mod.isLoaded())
						mod.loadModule(target as MenuBarItem);
				}
				MenuItemActions.invokeAction(target.data);
			}
		}
		
		public function menuClickEventHandler(event:MenuEvent):void{
			MenuItemActions.menuItemClickHandler(event);
		}
	
		/**
		 * Parse the menu item object for a tooltip string and display it below the menubar item
		 */
		public function mouseOverHandler(event:MouseEvent):void{
			var target:Object = event.target;
			
			if (target is MenuBarItem){
				if (target.hasOwnProperty('data') && target.data.hasOwnProperty('tooltip')){
					var pt:Point = new Point(0, 0);
        			pt = DisplayObject(target).localToGlobal(pt);
					_tooltip = ToolTipManager.createToolTip(target.data.tooltip, pt.x, pt.y + target.height + 2) as ToolTip;
				}
			}
			
		}
		
		/**
		 * Remove the tooltip on mouse out
		 */
		public function mouseOutHandler(event:MouseEvent):void{
			if (_tooltip != null){
				ToolTipManager.destroyToolTip(_tooltip);
				_tooltip = null;
			}
		}
		
		override public function set dataProvider(value:Object):void{
			if (value is ArrayCollection){
				localizeChildren(value as ArrayCollection);
			}
			super.dataProvider = value;			
		}
	
		private function localizeChildren(data:ArrayCollection):Boolean{
			var hasLocalize:Boolean = false;
			for (var i:int = 0; i < data.length; i++){
				if (data[i].hasOwnProperty('children') && data[i] is ArrayCollection){
					if (localizeChildren(data[i]) == true)
						hasLocalize = true;
				}
				if (Localization.localizeLabel(data[i]) == true)
					hasLocalize = true;	
			}
			return hasLocalize;
		}	
	}
}