package com.birst.flex.core
{
	import mx.controls.TextInput;
	
	import mx.containers.Canvas;

	public class PromptGroupEditableTextbox extends TextInput
	{
		public function PromptGroupEditableTextbox(tb:XML, parentXML:XML, canvas:Canvas)
		{
			super();
			
			this.name = tb.@name;
		}
		
	}
}