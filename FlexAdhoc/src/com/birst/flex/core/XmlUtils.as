package com.birst.flex.core
{
	import com.birst.flex.catalog.Listing;
	
	import mx.collections.ArrayCollection;
	import mx.collections.XMLListCollection;
	
	public class XmlUtils
	{
		public function XmlUtils()
		{
		}

        /**
         * Takes an XMLList object as input and creates an array collection of
         * IListing objects using the ListingAdapter wrapper class.
         * If the xmlNode is a 'node' then populate the array with its child node;
         * otherwise if it's a 'leaf' then populate the array with the xmlNode itself.
         */ 
        public static function convertToListingArray(xmlNode:XML, nodeName:String):ArrayCollection
        {
            var listings:ArrayCollection = new ArrayCollection();
            // Get the immediate children of the provided XML element
            var childrenElement:XMLList = xmlNode.child("children");
            // If element has no children, you're at a leaf. Just display it.
            if (childrenElement == null || childrenElement.length() == 0) {
                listings.addItem(new Listing(new XML(xmlNode)));
            } else {
                // For each XML element, create ListingAdapter and add it to the final ArrayCollection
                var kids:XMLList = childrenElement.child(nodeName);
                for (var i:int = 0; i < kids.length(); i++) {
                    listings.addItem(new Listing(kids[i]));
                }
            }
            return listings;
        }

        /**
         * Retrieves a "parent" in the context of the FileNode object hierarchy.
         * Since the &lt;children&gt; element is of no interest, it is skipped over.
         */
        public static function getParent(label:String, filePath:String, treeProvider:XMLListCollection):XML {
            if (label.indexOf("private") == 0) 
            {
                return treeProvider[0];
            }
            if (label.toLowerCase() == "shared") 
            {
            	return treeProvider[1];
            }
            // Strip off the filename to get its directory path
            var index:int = filePath.lastIndexOf("/");
            var parentPath:String;
            if (index != -1) {
                parentPath = filePath.substr(0, index);
            }
            
            // Now do the e4x query to find the parent folder within the full tree XML
            var parentXmlList:XMLList;
            for (var i:int = 0; i < treeProvider.length; i++) {
                // Need to address FileNodes at top of tree
                var root:XML = <root/>;
                root.appendChild(treeProvider[i]);
                // Find the matching FileNode in the overall Tree
                parentXmlList = root..FileNode.(name==parentPath);
                if (parentXmlList != null && parentXmlList.length() > 0) {
                    // You found it!
                    break;
                }
            } 
            
            if (parentXmlList.length() != 1) {
                return null;
            }
            return parentXmlList[0] as XML;
        }
        
        public static function getNode(targetDirectory:String, rootFileNode:XMLList, superUser:Boolean):XML
        {
        	if (targetDirectory == null || targetDirectory.length == 0 || targetDirectory == FileUtils.SLASH)
        	{
        		return null;
        	}
        	if (FileUtils.isTopLevelDirectory(targetDirectory, superUser))
        	{
	            if (targetDirectory.toLowerCase() == FileUtils.SHARED)
	            {
	                return rootFileNode[1] as XML;
	            }
	            else 
	            {
	            	return rootFileNode[0] as XML;
	            }
        	}
            // e4x spelunking
            var node:XMLList = rootFileNode..FileNode.(@n == targetDirectory);
            return node[0] as XML;
        }
        
        public static function getParentNode(childDirectory:String, rootFileNode:XMLList):XML
        {
            if (childDirectory == null
                ||
                childDirectory.toLowerCase() == FileUtils.SHARED
                ||
                childDirectory.toLowerCase() == FileUtils.PRIVATE)
            {
                return rootFileNode[0] as XML;
            }
            // e4x spelunking
            var parentNode:XMLList = rootFileNode..FileNode.children.FileNode.(n == childDirectory);
            return parentNode[0] as XML;
        }
        
        /**
         * Retrieves the FileNode XML from the current 'folderTree' Tree XMLListCollection 
         * using the directory (name) of the provided "old" xml node.  This is used in cases
         * where the Flex CatMan app wants to refresh the "current" directory but only has
         * a copy of the directory's FileNode XML that existed before an update was processed.
         * @param oldXmlNode is the XML representing the **folder** before the update occurred.
         * @param treeXlc is the newly-refreshed 'folderTree' Tree dataProvider.
         */
        public static function getRefreshedFileNode(filepath:String, treeXlc:XMLListCollection):XML {
            // Do the e4x query to find the specified folder within the full tree XML
            var parentXmlList:XMLList;
            for (var i:int = 0; i < treeXlc.length; i++) {
                // Need to address FileNodes at top of tree
                var root:XML = <root/>;
                root.appendChild(treeXlc[i]);
                // Find the matching FileNode in the overall Tree
                parentXmlList = root..FileNode.(name==filepath);
                if (parentXmlList != null && parentXmlList.length() > 0) {
                    // You found it!
                    break;
                }
            } 
            
            if (parentXmlList.length() != 1) {
                return null;
            }
            return parentXmlList[0] as XML;
        }
        
        public static function isValidFileType(fileNode:XML):Boolean 
        {
        	if (fileNode.isDirectory == "true") 
        	{
        		return true;
        	}
        	var fileName:String = fileNode.label;
        	var fileExtension:String = FileUtils.getFileExtension(fileName);
        	var isValid:Boolean = false;
        	for (var i:int = 0; i < FileUtils.VALID_FILETYPES.length; i++) {
        		if (fileExtension == FileUtils.VALID_FILETYPES[i]) {
        			isValid = true;
        			break;
        		}
        	}
        	return isValid;
        }
        
/*        
        public static function XMLListToSimpleArrayByDepth(xmlList:XMLList, depthLimit:int=10):Array {
            var resultArr:Array=new Array();
            for each (var i:XML in xmlList){
                resultArr =XMLToSimpleArrayByDepth(i,resultArr, depthLimit+1);
            }
            return(resultArr);
            }

        private static function XMLToSimpleArrayByDepth(xml:XML, targetArr:Array, depthCur:int=10):Array {
            depthCur -= 1;
            for each (var i:int in xml.children()) {
                if (IsTextNode(i)) {
                    targetArr.push(i);
                } else {
                    if (depthCur<=0) { continue; }
                    targetArr=XMLToSimpleArrayByDepth(i,targetArr, depthCur);
                }
            }
            return(targetArr);
        }

        public static function XMLListToSimpleArray(xmlList:XMLList):Array {
            var resultArr:Array=new Array();
            var textXMLList:XMLList=xmlList..*.(children().length()===0);

            for each (var i:XML in textXMLList){
                resultArr.push(i);
            }
            return(resultArr);
        }

        public static function XMLToSimpleArray(xml:XML):Array{
            var resultArr:Array=new Array();
            var textXMLList:XMLList=xml..*.(children().length()===0);

            for each (var i:XML in textXMLList){
                resultArr.push(i);
            }
            return(resultArr);
        }

        public static function XMLListToComplexArray(xmllist:XMLList):Array{
            var resultArr:Array=new Array();

            for each (var i:XML in xmllist) {
                if(IsTextNode(i)) {
                    resultArr.push(i);
                } else {
                    resultArr.push(XMLToComplexArray(i));
                }
            }

            return(resultArr);
        }

		public static function XMLToComplexArray(xml:XML):Array{
			var resultArr:Array=new Array();
			
			for each (var i:XML in xml.children()) {
                if(IsTextNode(i)) {
                    resultArr.push(i);
	       		} else {
                    resultArr.push(XMLToComplexArray(i));
                }
			}
			return(resultArr);
		}

        //Text or not
        public static function IsTextNode(xml:XML):Boolean {
            return ( (xml.children().length()==0) ? true : false );
        }

        //
        public static function HasNode(xml:XML,name:String):int {
            return (xml[name].length());
        }

        //
        public static function HasAttribute(xml:XML,attName:String):int {
            return (xml.@[attName].length());
        }
*/
	}
}
