package com.birst.flex.core
{
	import flash.display.DisplayObject;
	import mx.controls.ComboBox;
	import mx.core.IFlexDisplayObject;
	import mx.core.UIComponent;

	public class BirstIconComboBox extends ComboBox
	{
		public function BirstIconComboBox()
		{
			super();
		}
		
	private var iconHolder:UIComponent;

		override protected function createChildren():void
		{
			super.createChildren();
	
			iconHolder = new UIComponent();
			addChild(iconHolder);
		}
	
	    override protected function updateDisplayList(unscaledWidth:Number,
	                                                  unscaledHeight:Number):void
	    {
			while (iconHolder.numChildren > 0)
				iconHolder.removeChildAt(0);

	        super.updateDisplayList(unscaledWidth, unscaledHeight);

			var item:Object = selectedItem;
			if (item.hasOwnProperty("icon")) {
				var iconClass:Class = item.icon as Class;
				var icon:IFlexDisplayObject = new iconClass() as IFlexDisplayObject;
				iconHolder.addChild(DisplayObject(icon));
				iconHolder.y = (unscaledHeight - icon.measuredHeight) / 2;
				iconHolder.x = borderMetrics.left + ((unscaledWidth - icon.measuredWidth) / 2);
				/*
				textInput.x = iconHolder.x + icon.measuredWidth;
				textInput.setActualSize(textInput.width - icon.measuredWidth, textInput.height);
				*/
			}
			else {
				textInput.x = borderMetrics.left;
				textInput.setActualSize(textInput.width, textInput.height);
			}
		} 
	   
	}
}