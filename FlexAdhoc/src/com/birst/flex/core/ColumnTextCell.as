package com.birst.flex.core
{
	import com.birst.flex.adhoc.LinkNavigator;
	
	import flash.display.Graphics;
	import flash.events.MouseEvent;
	
	import mx.containers.Canvas;
	import mx.controls.Text;

	public class ColumnTextCell extends Text
	{
		public var isPivotTableCell:Boolean = false;
		public var columnIndex:int = -1;
		public var link:String = null;
		public var target:String = null;
		public var columnType:String = null;
		public var dimension:String = null;
		public var hierarchy:String = null;
		public var column:String = null;
		public var datetimeFormat:String = null;
		public var isData:Boolean = false;
		private var _value:String = null;
		public var label:String = null;
		
		private var canvas:Canvas;
		
		public function ColumnTextCell(t:XML, parentXML:XML, canvas:Canvas){
			super();
			
			columnIndex = t.columnIndex;
			columnType = t.columnType;
			dimension = t.dimension;
			column = t.column;
			hierarchy = t.hierarchy;
			datetimeFormat = t.datetimeFormat;
			label = t.columnLabel;
			_value = t.valueToUse;
			
			this.canvas = canvas;
			
			var textWidth:uint = uint(t.@width);
			
			//Compensate for rounding errors, we have to check whether as the last one, we're bigger than our parent's width
			//This is mainly for pivot table
			var tagType:String = getCellTagType(parentXML);
			if (tagType == "Data")
				isData = true;
					
			var texts:XMLList = parentXML.text;
			if (tagType == "Data" || tagType == "RowHeader" || tagType == "CrosstabHeader" || tagType == "ColumnHeader"){
				isPivotTableCell = true;
				var textX:uint = uint(t.@x);
				var parentWidth:uint = uint(parentXML.@width);
				if (textWidth != parentWidth){
					//Check if we're the only child, if so, we just set the new width, otherwise,
					//we wait till we're the last child, then make up the difference
					if (texts.length() == 1){
						textWidth = parentWidth; 
					}else{
						var lastText:XML = texts[texts.length() - 1];
						if (lastText.@x == t.@x && lastText.@y == t.@y){
							textWidth = textWidth + (parentWidth - (uint(t.@x) + textWidth));
						}
					}
				}
			}
		
			this.maxWidth = textWidth;
			
			//Compensate for rounding errors
			var textHeight:uint = uint(t.@height);
			
			// Bug 9846: Don't compensate for tagType == CrosstabHeader, its height is incorrect. 
			if (tagType == "Data" || tagType == "RowHeader" || tagType == "ColumnHeader"){
				//Check if we're the only child, if so, we just set the new width, otherwise,
				//we wait till we're the last child, then make up the difference
				var parentHeight:uint = uint(parentXML.@height);
				if (texts.length() == 1){ 
					textHeight = (parentHeight > textHeight) ? parentHeight : textHeight;
				}else{
					var lastText1:XML = texts[texts.length() - 1];
					if (lastText1.@x == t.@x && lastText1.@y == t.@y){
						textHeight = textHeight + (parentHeight - (uint(t.@y) + textHeight));
					}
				}
//				this.height = textHeight;
			}
			
			this.width = uint(Math.min(t.@width, this.maxWidth));
			this.setStyle("leading", 0);	
			
			var bcs: String = t.@backcolor;
			bcs = "0x"+bcs.substring(1,bcs.length);
			
			//We start at an offset if there is going to be a border
			var startPt:Number = (tagType == "Data" || tagType == "RowHeader" || tagType == "ColumnHeader" || tagType == "CrosstabHeader") ? 0.5 : 0;
			
			if (String(t.@transparent) != 'true') {
				var bc: uint = new uint(bcs);
				this.graphics.beginFill(bc,1);
				this.graphics.drawRect(startPt, startPt, this.maxWidth, textHeight);
				this.graphics.endFill();
			}
			
			/*
			if (tagType == "RowHeader"){
				Util.drawBorder(this.graphics, this.maxWidth, textHeight);
			}else if(tagType == "CrosstabHeader"){
				Util.drawBorder(this.graphics, this.maxWidth, textHeight);
			}else if (tagType == "ColumnHeader" || tagType == "Data"){
				Util.drawBorder(canvas.graphics, canvas.maxWidth, textHeight);
				Util.drawBorder(this.graphics, this.maxWidth, textHeight);
			}
			*/
		
			addBorders(this.graphics, t, true, textHeight);
			
			link = t.@link;
			var haslink: Boolean = link != null && link.length > 0;
			var isUnderline:Boolean = t.@underline == 'true';
			this.text = t.@value;
			bcs = t.@color;
			bcs = "0x"+bcs.substring(1,bcs.length);
			this.setStyle("color", bcs);
			this.setStyle("fontFamily", t.@face);
			setStyle("fontSize", t.@size);
			if (isUnderline)
				setStyle("textDecoration", "underline");
			this.setStyle("textAlign", t.@alignment);
			if (t.@bold == "true")
				setStyle("fontWeight", "bold");
			if (t.@italic == "true")
				setStyle("fontStyle", "italic");
			if (haslink)
			{
				this.useHandCursor = true;
				this.buttonMode = true;
				this.mouseChildren = false;
				this.target = t.@target;
				this.addEventListener(MouseEvent.CLICK, onLinkClick);
			}
			
			var tooltip:String = t.@tooltip
			if (tooltip)
				this.toolTip = tooltip;				
		}
		
		public function get value():String {
			if (this._value != null && this._value.length > 0)
				return this._value;
				
			return this.text;
		}
		
		private function onLinkClick(event:MouseEvent):void{
			LinkNavigator.staticNavigate(link, target, canvas);
		}
		
		private function getCellTagType(parentXML:XML):String{
			var tagType:String = "";
			if (parentXML){
				var cellType:XMLList = parentXML.child("net.sf.jasperreports.crosstab.cell.type");
				if (cellType.length() > 0){
					tagType = cellType[0];
				}
			}
			return tagType;	
		}
		
		public static function addBorders(graphics:Graphics, t:XML, drawBottom:Boolean = true, heightOverride:uint = 0): void {
			var bcs: String = t.@topBorderColor;
			var width:uint = uint(t.@width) - 1;
			var height:uint = (heightOverride != 0) ? heightOverride : uint(t.@height);
			var color:uint;
			var lineWidth:Number;
			var offset:int;
			if (bcs != null && bcs.length > 0) {
				bcs = "0x"+bcs.substring(1,bcs.length);
				// top border color
				color = new uint(bcs);
				lineWidth = new Number(t.@topBorderWidth);
				offset = lineWidth/2;
				graphics.moveTo(offset, offset);
				graphics.lineStyle(lineWidth, color);
				graphics.lineTo(width - offset, offset);
			}

			bcs = t.@rightBorderColor;
			if (bcs != null && bcs.length > 0) {
				bcs = "0x"+bcs.substring(1,bcs.length);
				color = new uint(bcs);
				lineWidth = new Number(t.@rightBorderWidth);
				offset = lineWidth/2;
				graphics.moveTo(width, offset);
				graphics.lineStyle(lineWidth, color);
				graphics.lineTo(width, height - offset);
				
			}
			
			bcs = t.@bottomBorderColor;
			if (drawBottom && bcs != null && bcs.length > 0) {
				bcs = "0x"+bcs.substring(1,bcs.length);
				color = new uint(bcs);
				lineWidth = new Number(t.@bottomBorderWidth);
				offset = lineWidth/2;
				graphics.moveTo(width - offset, height - offset);
				graphics.lineStyle(lineWidth, color);
				graphics.lineTo(offset, height - offset);
			}

			bcs = t.@leftBorderColor;
			if (bcs != null && bcs.length > 0) {
				bcs = "0x"+bcs.substring(1,bcs.length);
				color = new uint(bcs);
				lineWidth = new Number(t.@leftBorderWidth);
				offset = lineWidth/2;
				graphics.moveTo(offset, height - offset);
				graphics.lineStyle(lineWidth, color);
				graphics.lineTo(offset, offset);
			}
		
		}
		
	}
}