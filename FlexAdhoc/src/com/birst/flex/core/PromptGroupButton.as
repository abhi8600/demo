package com.birst.flex.core
{
	import com.birst.flex.adhoc.ReportRenderer;
	import com.birst.flex.dashboards.views.DashletContentBase;
	import com.birst.flex.webservice.WebServiceClient;
	import com.birst.flex.webservice.WebServiceResult;
	
	import flash.display.DisplayObject;
	import flash.events.MouseEvent;
	
	import mx.collections.ArrayCollection;
	import mx.containers.Canvas;
	import mx.controls.Button;
	import mx.core.Application;
	import mx.rpc.events.ResultEvent;
	
	public class PromptGroupButton extends Button
	{
		private var VisibleName:String;
		private var targetDashboardName:String;
		private var targetDashboardPage:String;
		private var operationName:String;
		private var imagePath:String;
		private var validationMessage:String;
		private var Options:ArrayCollection;
		private var IncludedPromptIndices:ArrayCollection;
		
		private var canvas:Canvas;
		private var renderer:ReportRenderer;
		
		public function PromptGroupButton(bt:XML, parentXML:XML, canvas:Canvas, renderer:ReportRenderer)
		{
			super();
			this.canvas = canvas;
			this.renderer = renderer;
			
			this.name = bt.@name;
			this.VisibleName = bt.@VisibleName;
			this.validationMessage = bt.@validationMessage;
			this.operationName = bt.@operationName;
			this.label = bt.@imagePath;
			this.targetDashboardName = bt.@targetDashboardName;
			this.targetDashboardPage = bt.@targetDashboardPage;
			
			var btOptions:XMLList = bt.Options;
			if (btOptions != null && btOptions.children().length() > 0)
			{
				var options:XMLList = btOptions[0].child("Option");
				if (options != null && options.length() > 0)
				{
					this.Options = new ArrayCollection();
					for (var i:int =0 ; i < options.length() ; i++)
					{
						this.Options.addItem(options[i].toString());
					}
				}
			}
			
			var inclIndices:XMLList = bt.IncludedPromptIndices;
			if (inclIndices != null && inclIndices.children().length() > 0)
			{
				var included:XMLList = inclIndices[0].child("Included");
				if (included != null && included.length() > 0)
				{
					this.IncludedPromptIndices = new ArrayCollection();
					for (var j:int =0 ; j < included.length() ; j++)
					{
						this.IncludedPromptIndices.addItem(included[j].toString());
					}
				}
			}
			
			this.addEventListener(MouseEvent.CLICK, performOperation);
		}
		
		public function performOperation(event:MouseEvent):void
		{
			var paramNames:Array = new Array();
			var isAnyParamValueSupplied:Boolean = false;
			if (this.Options != null)
			{
				for each (var opt:String in Options)
					paramNames[opt]= new Array();
					
				for each(var obj:DisplayObject in canvas.getChildren())
				{
					if (obj is PromptGroupCheckBox)
		 			{
						var cb:PromptGroupCheckBox = obj as PromptGroupCheckBox;
						if (this.Options.contains(cb.name) && cb.selected)
						{
							(paramNames[cb.name] as Array).push(cb.value);
							isAnyParamValueSupplied = true;							
						}
					}
					else if (obj is PromptGroupDropDown)
					{
						var dd:PromptGroupDropDown = obj as PromptGroupDropDown;
						if (this.Options.contains(dd.name))
						{
							(paramNames[dd.name] as Array).push(dd.selectedItem.value.toString());							
							isAnyParamValueSupplied = true;							
						}
					}
					else if (obj is PromptGroupEditableTextbox)
					{
						var tb:PromptGroupEditableTextbox = obj as PromptGroupEditableTextbox;
						if (this.Options.contains(tb.name))
						{
							(paramNames[tb.name] as Array).push(tb.text);							
							isAnyParamValueSupplied = true;							
						}
					}
				}
			}			
			if (!isAnyParamValueSupplied)
			{
				Util.alignAlertErrMessage(this.validationMessage);				
				return;
			}
			
			var paramsXml:String = "<OperationData>";
			paramsXml = paramsXml + "<OperationName>" + this.operationName + "</OperationName>";
			for each (var optName:String in this.Options)
			{
				paramsXml = paramsXml + "<Parameter>";
				paramsXml = paramsXml + "<ParameterName>" + optName + "</ParameterName>";
				paramsXml = paramsXml + "<ParameterValues>";
				var paramArr:Array = paramNames[optName] as Array;
				if (paramArr.length > 0)
				{
					for each(var param:String in paramArr)
					{
						paramsXml = paramsXml + "<ParameterValue>" + param + "</ParameterValue>";
					}
				}
				paramsXml = paramsXml + "</ParameterValues>";
				paramsXml = paramsXml + "</Parameter>";
			}
			
			var dcb:DashletContentBase = renderer.caller as DashletContentBase;
			var promptXML:XML = dcb.getPromptXML();
			
			paramsXml = paramsXml + "<PromptXML>";
			if (promptXML == null)
				paramsXml = paramsXml + "<Prompts />";
			else
				paramsXml = paramsXml + promptXML.toString();
			paramsXml = paramsXml + "</PromptXML>";
			
			paramsXml = paramsXml + "</OperationData>";
			var ws:WebServiceClient = new WebServiceClient('OperationHandler', 'performOperation', 'PromptGroupButton', 'performOperation');
			ws.setResultListener(onOperationPerformed);
			ws.execute(paramsXml);			
		}
		
		private function onOperationPerformed(event:ResultEvent):void{
			var rs:WebServiceResult = WebServiceResult.parse(event);
			if(!rs.isSuccess()){
				Util.localeAlertError('PG_failOperation', rs);
				return;
			}
			
			// need to reload the target dashboard
			var dcb:DashletContentBase = renderer.caller as DashletContentBase;
			if (dcb != null)
			{
				// refresh current page
				dcb.loadReport(dcb.getPromptXML(), dcb.currentPage, dcb.getCurrentTime(), dcb.pagePath, dcb.pageUuid);				
			}
			
			if (this.targetDashboardName != null)
			{
				// load new dashboard
				var dashboardName:String;
				var dbPageName:String;
				if (this.targetDashboardPage != null)
				{
					dashboardName = this.targetDashboardName;
					dbPageName = this.targetDashboardPage;
				}
				else
				{
					dashboardName = this.targetDashboardName.split("_")[0];
	   				dbPageName = this.targetDashboardName.split("_")[1];					
				}
				Application.application.openDashboardPage(dashboardName, dbPageName, null);
			}
		}
		
	}
}