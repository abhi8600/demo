package com.birst.flex.core
{
	import flash.events.Event;
	import flash.utils.getQualifiedClassName;
	import mx.core.Application; 
	
	public class AppEventManager
	{
		[Bindable]
		public static var debugLastEvent:String;
		
		public function AppEventManager()
		{
		}

		//Short for addEventListener. Yes, I used too much YUI
		public static function on(type:String, 
								  listener:Function, 
								  useCapture:Boolean = false, 
								  priority:int = 0, 
								  useWeakReference:Boolean = false):void{
			addAppEventListener(type, listener, useCapture, priority, useWeakReference);
		}	
		
		//Add event listener to Application
		public static function addAppEventListener(type:String, 
												listener:Function, 
												useCapture:Boolean = false, 
												priority:int = 0, 
												useWeakReference:Boolean = false):void{
			Application.application.removeEventListener(type, listener, useCapture);							
			Application.application.addEventListener(type, listener, useCapture, priority, useWeakReference);
		}	
		
		//Short for addEventListener. Yes, I used too much YUI
		public static function fire(event:AppEvent):Boolean{
			return dispatchAppEvent(event);
		}
		
		//Fire the Application level event
		public static function dispatchAppEvent(event:AppEvent):Boolean{
			//temporary debugging
			if (Application.application.hasOwnProperty("debugMode") && Application.application.debugMode == true){
				debugLastEvent = "event: " + event.type + " component: " + event.callerComponent + " operation: " + event.callerOperation;
			}
			return Application.application.dispatchEvent(event);
		}
		
		public static function removeAppEventListener(type:String,
												   listener:Function, 
												   useCapture:Boolean = false):void{
			Application.application.removeEventListener(type, listener, useCapture); 		
		}
	}
}