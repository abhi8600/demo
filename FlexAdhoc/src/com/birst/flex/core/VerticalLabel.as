package com.birst.flex.core
{
	import flash.events.MouseEvent;
	
	import mx.containers.VBox;
	import mx.controls.Label;
	
	/**
	 * VerticalLabel is used to display a label vertically. Used in prompts section of dashboard page.
	 */
	public class VerticalLabel extends VBox
	{
		[Embed(systemFont='Verdana',  fontName='embedVendana',  mimeType='application/x-font', advancedAntiAliasing='true')]
		private var vendana:Class;
			
		private var _label:Label;
		private var _text:String;
		
		public function VerticalLabel()
		{
			super();
			this.setStyle('verticalGap', 0);
		}
		
		public function set text(str:String):void{
			if(_label){
				_label.text = str;
			}	
			_text = str;
		}
		
		public function get text():String{
			return _text;
		}
		
		override protected function measure():void{
			super.measure();
			if(_label){
				measuredWidth = _label.textHeight;
			}
		}
		
		override protected function createChildren():void{
			super.createChildren();
			
			if(!_label){
				_label = new Label();
				_label.rotation = -90;
				_label.setStyle("fontFamily", "embedVendana");
				_label.includeInLayout = false
				/*
				_label.addEventListener(MouseEvent.ROLL_OVER, onRollOverHighlight);
				_label.addEventListener(MouseEvent.ROLL_OUT, onRollOut);
				*/
				_label.text = _text;
				
				addChild(_label);
			}
		}
		
		private function onRollOverHighlight(event:MouseEvent):void{
			_label.setStyle('color', 0xFFC56C);
		}
		
		private function onRollOut(event:MouseEvent):void{
			_label.setStyle('color', 0x000000);
		}
		
		override protected function updateDisplayList(unscaledWidth:Number, unscaledHeight:Number):void{
			super.updateDisplayList(unscaledWidth, unscaledHeight);
			if(_label){
				_label.setActualSize(unscaledHeight, unscaledWidth);
				_label.move(x - 8, y + unscaledHeight);
			}
		}
	}
}