package com.birst.flex.core.skins
{
	import com.birst.flex.core.ColumnTextCell;
	import com.birst.flex.core.ColumnLabelMenuBar;
	
	import flash.display.DisplayObjectContainer;
	import flash.display.Graphics;
	
	import mx.skins.ProgrammaticSkin;

	public class MenuBarLabelBorderSkin extends ProgrammaticSkin
	{
		
		public function MenuBarLabelBorderSkin()
		{
			//TODO: implement function
			super();
		}
		
		override protected function updateDisplayList(unscaledWidth:Number,
										     unscaledHeight:Number):void
		{
			var p:DisplayObjectContainer = this.parent;
			if (p is ColumnLabelMenuBar) {
				var menuBar:ColumnLabelMenuBar = p as ColumnLabelMenuBar;
				var label:XML = (p as ColumnLabelMenuBar).getRenderData();
				var bck: String = label.@backcolor;
				bck = "0x"+bck.substring(1,bck.length);
				var bci: uint = new uint(bck);
				
				//draw our background color
				if (String(label.@transparent) != 'true') {
					this.graphics.beginFill(bci);
					this.graphics.drawRect(0, 0, p.width, p.height);
					this.graphics.endFill();
				}
				
				//draw borders
				ColumnTextCell.addBorders(graphics, label, false);
				fixupBottomBorder(graphics, label);
				//addBorders(this.graphics, label, menuBar.width - 1, menuBar.height - 1);
			}
		}
		
		private function fixupBottomBorder(graphics:Graphics, t:XML):void{
			var bcs:String = t.@bottomBorderColor;
			if (bcs != null && bcs.length > 0) {
				bcs = "0x"+bcs.substring(1,bcs.length);
				var color:uint = new uint(bcs);
				var lineWidth:Number = new Number(t.@bottomBorderWidth);
				var offset:Number = (lineWidth == 1) ? 0.25 : lineWidth/2;
				graphics.moveTo(width - offset, height - offset);
				graphics.lineStyle(lineWidth, color);
				graphics.lineTo(offset, height - offset);
			}
		}
		
		/* Old border rendering function
		public static function addBorders(graphics:Graphics, t:XML, width:uint, height:uint): void {
			// top border 
			var topBorderColor:uint = Util.colorString2uint(t.@topBorderColor);
			var line:String = t.@topBorderWidth;
			var topLineWidth:uint = uint(line.substr(0, 1));
			
			// left border
			var leftBorderColor:uint = Util.colorString2uint(t.@leftBorderColor);
			line = t.@leftBorderWidth;
			var leftLineWidth:uint = uint(line.substr(0, 1));
			
			//bottom border
			var bottomBorderColor:uint = Util.colorString2uint(t.@bottomBorderColor);
			line = t.@bottomBorderWidth;
			var bottomLineWidth:uint = uint(line.substr(0, 1));
			
			//right border
			var rightBorderColor:uint = Util.colorString2uint(t.@rightBorderColor);
			line = t.@rightBorderWidth;
			var rightLineWidth:uint = uint(line.substr(0, 1));
			
			//draw borders
			var offset:int = topLineWidth/2;
			graphics.lineStyle(topLineWidth, topBorderColor);
			graphics.moveTo(offset, offset);
			graphics.lineTo(width - offset, offset);
			
			offset = (rightLineWidth + 1 )/2;
			graphics.lineStyle(rightLineWidth, rightBorderColor);
			graphics.moveTo(width - offset, offset);
			graphics.lineTo(width - offset, height - offset);
			
			offset = (bottomLineWidth + 1) / 2;
			graphics.lineStyle(bottomLineWidth, bottomBorderColor);
			graphics.moveTo(width - offset, height - offset);
			graphics.lineTo(offset, height - offset);
			
			offset = leftLineWidth/2;
			graphics.lineStyle(leftLineWidth, leftBorderColor);
			graphics.moveTo(offset, height - offset);
			graphics.lineTo(offset, offset);	
		}*/
	}
}