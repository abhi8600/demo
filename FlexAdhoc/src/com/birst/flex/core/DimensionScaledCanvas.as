package com.birst.flex.core
{
	import flash.utils.Dictionary;
	
	import mx.containers.Canvas;
	import mx.core.ScrollPolicy;
	import mx.core.UIComponent;

	public class DimensionScaledCanvas extends Canvas
	{
		[Bindable]
		public var contentMaxWidth:Number;
		[Bindable]
		public var contentMaxHeight:Number;
		[Bindable]
		public var doScaling:Boolean = false;
		
		private var lastResizedDimension:String;
		
		private var originalPos:Dictionary;
		
		public function DimensionScaledCanvas()
		{
			super();
			originalPos = new Dictionary();
		}

		public function resetPositionsCache():void{
			originalPos = new Dictionary();	
		}
		
		override protected function updateDisplayList(unscaledWidth:Number, unscaledHeight:Number):void{
			super.updateDisplayList(unscaledWidth, unscaledHeight);
			var newDimension:String = unscaledWidth + 'x' + unscaledHeight;

			scale();
		}
		
		public function scale():void{
			if(doScaling && contentMaxWidth > 0 && contentMaxHeight > 0){
				verticalScrollPolicy = horizontalScrollPolicy = mx.core.ScrollPolicy.OFF;
				
				var scaleX:Number = this.width / contentMaxWidth; 
				var scaleY:Number = this.height / contentMaxHeight;
				
				Tracer.debug('scale() x,y by ' + scaleX + ',' + scaleY, this);
				
				var prevChild:UIComponent = null;
				var lastColumnTextCellY:Number = 0;
				var lastColumnTextOffset:Number = 0;
				for each(var child:UIComponent in getChildren()){
				    var childX:Number = child.x;
				    var childY:Number = child.y;				
					var position:Object = originalPos[child];
					if (!position){
						originalPos[child] = { x: child.x, y: child.y };
					}else{
						childX = position.x;
						childY = position.y;
					}
					
					//Bug 13969 Report Titles cut off and do not resize properly in dashboards
					if(child is ColumnTextCell){
						var txt:ColumnTextCell = ColumnTextCell(child);
						//Tracer.debug('text length: ' + txt.text.length);
						if(txt.text.length > 0){
							lastColumnTextOffset = childY + child.height;
							lastColumnTextCellY = childY;
						}
						continue;
					}else{
						//Tracer.debug('chart y: ' + childY * scaleY + ' column: ' + lastColumnTextOffset); 
						// AnyChart, do we clip?
						if(childY * scaleY > lastColumnTextOffset ){
							lastColumnTextOffset = 0;
							//Tracer.debug('No clip');
						}
					}
					
					var scaledWidth:Number = child.width * scaleX;
					var scaledHeight:Number = child.height * scaleY - lastColumnTextOffset;
					var scaledX:Number = childX * scaleX;
					var scaledY:Number = childY * scaleY + lastColumnTextOffset;
					child.setActualSize(scaledWidth, scaledHeight);
					child.move(scaledX, scaledY);
				}
			}
		}		
	}
}