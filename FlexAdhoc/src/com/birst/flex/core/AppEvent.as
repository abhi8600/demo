package com.birst.flex.core
{
	import flash.events.Event;

	public class AppEvent extends Event
	{
		//Our global events
		public static var EVT_COLUMN_CHOSEN:String = "ColumnChosen";
		public static var EVT_LABEL_CHOSEN:String = "LabelChosen";
		public static var EVT_IMAGE_CHOSEN:String = "ImageChosen";
		public static var EVT_SUBREPORT_CHOSEN:String = "SubreportChosen";
		public static var EVT_CHART_CHOSEN:String = "ChartChosen";
		public static var EVT_RECTANGLE_CHOSEN:String = "RectangleChosen";
		public static var EVT_BUTTON_CHOSEN:String = "ButtonChosen";
		public static var EVT_CONFIG_CHANGED:String = "ConfigChanged";
		public static var EVT_DIALOG_SAVE:String = "DialogSave";
		public static var EVT_SUBJECT_AREA_NODE_CLICKED:String = "SubjectAreaNodeClicked";
		public static var EVT_RESULT_TAB_SHOW:String = "ReportTab";
		public static var EVT_CONFIG_SAVE_SUCCESS:String = "ConfigSavedOK";
		public static var EVT_CONFIG_SAVE_FAILED:String = "ConfigSaveFailed";
		public static var EVT_NEW_REPORT_DATA:String = "NewReport";
		public static var EVT_CLEAR_REPORT:String = "ClearReport";
		public static var EVT_ALL_MODULES_LOADED:String = "AllModulesLoaded";
		public static var EVT_COLUMN_DELETED:String = "ColumnDeleted";
		public static var EVT_REPORT_SAVED:String = "ReportSaved";
        public static var EVT_FILE_COPIED:String = "FileCopied";
        public static var EVT_FILE_MOVED:String = "FileMoved";
        public static var EVT_FILTER_ADDED:String = "FilterAdded";
        public static var EVT_SELECTED:String = "Selected";
        public static var EVT_UNSELECTED:String = "Unselected";
        public static var EVT_SELF_SCHEDULE_ALLOWED:String = "selfScheduleAllowed";
        public static var EVT_CHART_APPLY:String = "chartApply";
		
		//Our object who threw the event
		private var _component:String;
		private var _operation:String
		private var _dispatcher:Object;
		private var _ctrlKey:Boolean;
		
		public function AppEvent(type:String, component:String, operation:String, dispatcher:Object, ctrlKey:Boolean=false, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
			_component = component;
			_operation = operation;
			_dispatcher = dispatcher;
			_ctrlKey = ctrlKey;
		}
		
		//Read-only event thrower
		public function get callerComponent():String{
			return _component;
		}
		
		public function get callerOperation():String{
			return _operation;
		}
		
		public function get dispatcher():Object{
			return _dispatcher;
		}
		
		public function get ctrlKey():Boolean {
			return _ctrlKey;
		}
	}
}