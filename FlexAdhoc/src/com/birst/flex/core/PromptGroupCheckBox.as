package com.birst.flex.core
{
	import mx.containers.Canvas;
	import mx.controls.CheckBox;

	public class PromptGroupCheckBox extends CheckBox
	{
		public var value:String;
		
		public function PromptGroupCheckBox(cb:XML, parentXML:XML, canvas:Canvas)
		{
			super();
			this.name = cb.@name;
			this.value = cb.@value;
		}
		
	}
}