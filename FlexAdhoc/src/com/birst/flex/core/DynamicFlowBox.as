package com.birst.flex.core
{
	
	import flexlib.containers.utilityClasses.FlowLayout;
	
	import mx.containers.Box;
	import mx.containers.BoxDirection;
	import mx.containers.utilityClasses.BoxLayout;
	import mx.core.mx_internal;
	
	use namespace mx_internal;

	public class DynamicFlowBox extends Box
	{
		private var _isFlow:Boolean = false;
		
		public function DynamicFlowBox() {
			super();
		}
		
		public function get flow():Boolean{
			return _isFlow;	
		}
		
		public function set flow(isFlow:Boolean):void{
			if((isFlow && _isFlow) || (!isFlow && !_isFlow)){
				return;
			}
			
			if(isFlow){
				flowLayout();
			}else{
				boxLayout();
			}
		}
		
		
		override public function set direction( value:String ):void {
			if(_isFlow){
				return;
			}	
			
			super.direction = value;
		}
		
		private function boxLayout():void{
			_isFlow = false;
			
			invalidateSize();
        	invalidateDisplayList();

			layoutObject = new BoxLayout();
			layoutObject.target = this;
		}
		
		private function flowLayout():void{
			_isFlow = true;
				
			invalidateSize();
        	invalidateDisplayList();
	
			// Force horizontal direction
			direction = BoxDirection.HORIZONTAL;
		
			// Use a FlowLayout to lay out the children
			layoutObject = new FlowLayout();
			layoutObject.target = this;	
		}
	}
}