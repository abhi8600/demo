package com.birst.flex.core
{
	import com.birst.flex.adhoc.AdHocConfig;
	import com.birst.flex.core.formatter.BDateFormatter;
	import com.birst.flex.webservice.WebServiceResult;
	
	import flash.display.Graphics;
	import flash.display.Sprite;
	import flash.events.ContextMenuEvent;
	import flash.external.ExternalInterface;
	import flash.geom.Point;
	import flash.net.URLRequest;
	import flash.net.navigateToURL;
	import flash.system.Capabilities;
	import flash.ui.ContextMenu;
	import flash.ui.ContextMenuItem;
	import flash.utils.Dictionary;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.controls.CheckBox;
	import mx.controls.ComboBox;
	import mx.controls.DataGrid;
	import mx.controls.DateField;
	import mx.core.Application;
	import mx.core.IFlexDisplayObject;
	import mx.core.UIComponent;
	import mx.events.CloseEvent;
	import mx.formatters.DateFormatter;
	import mx.managers.PopUpManager;
	import mx.resources.ResourceManager;
	import mx.rpc.events.ResultEvent;
	import mx.styles.StyleManager;
	import mx.utils.StringUtil;
	import mx.utils.URLUtil;
	
	[ResourceBundle("errors")]
	[ResourceBundle("dashboard")]
	
	public class Util
	{
		public static var CLEAR_REPORT:String = 'clearReportWS';			
		public static var OPEN_REPORT:String = 'openReport';

		private static var _os:String;
		private static var _majorVersion:Number;
		
		public function Util()
		{
		}
	
		/**
		 * Check or uncheck the checkbox if the value string is "true" or false otherwise
		 */	
		public static function checkBoxSetter(value:String, checkbox:CheckBox):void{
			if(value != null && checkbox){
				checkbox.selected = value.toString().toLowerCase() == "true";
			}
		}
		
		/**
		 * Set the selected item in combo box to the first matching item which matches value string
		 */
		private static function internalComboBoxSetter(value:String, combo:ComboBox):Boolean{
			var dp:Object = combo.dataProvider;
			if (dp != null){
				if (dp is ArrayCollection){
					var c:ArrayCollection = dp as ArrayCollection;
					for (var i:int = 0; i < c.length; i++){
						var a:Object = c.getItemAt(i);
						if ((a.hasOwnProperty('value') && a.value.toString() == value) || (a.toString() == value)){
							combo.selectedItem = a;
							return true;
						}
					}
				}
			}
			return false;
		}
		
		public static function comboBoxSetter(value:Object, combo:ComboBox):Boolean {
			if (value.hasOwnProperty('value')) {
				return internalComboBoxSetter(value.value, combo);
			}
			return internalComboBoxSetter(value.toString(), combo);
		}
		
		public static function setDataProviderValue(value:String, component:Object):Boolean{
			if ( !(component is UIComponent) || !(component.dataProvider)){
				return false;
			}
			
			var dp:Object = component.dataProvider;
			if (dp != null){
				if (dp is ArrayCollection){
					var c:ArrayCollection = dp as ArrayCollection;
					for (var i:int = 0; i < c.length; i++){
						var a:Object = c.getItemAt(i);
						if ((a.hasOwnProperty('value') && a.value.toString() == value) || (a.toString() == value)){
							component.selectedItem = a;
							return true;
						}
					}
				}
			}
			return false;
		}
		
		/**
		 * Sets the font selection hbox values
		 * @param value - String representation of the font in the format FAMILY-WEIGHT-SIZE
		 * @param fontHbox - The font selection hbox to affect
		 */
		public static function fontSelectionHboxSetter(value:String, fontHbox:FontSelectionHBox):void{
			var colorValues:Array = value.split("-");
			if (colorValues.length >= 3){
				fontHbox.fontFamilyValueOnResult = colorValues[0];
				var weight:String = colorValues[1];
				comboBoxSetter(weight, fontHbox.fontWeight);
				comboBoxSetter(colorValues[2], fontHbox.fontSize);
			}			
		}
		
		public static function formatSelecorHboxSetter(format:String, formatHbox:FormatSelectorHBox):void {
			formatHbox.formatValueOnResult = format;
		}
		
		public static function drawGradient(comp:UIComponent, w:Number, h:Number):void{	
			// retrieves the user-defined styles
            var fillColors:Array = comp.getStyle("fillColors");
            var fillAlphas:Array = comp.getStyle("fillAlphas");
            var cornerRadius : Number = (comp.getStyle("cornerRadius") != undefined) ? comp.getStyle("cornerRadius") as Number : 1;

            // converts the fill colors to RGB color values
            mx.styles.StyleManager.getColorNames(fillColors);

            // ready to draw!
            comp.graphics.clear();

            // draws the gradient
            comp.drawRoundRect (0, 0, w, h, cornerRadius, fillColors, fillAlphas, comp.verticalGradientMatrix (0, 0, w, h));
		}
		
		public static function colorString2uint(str:String):uint{
			if (str == null) return uint(-1);
			if (str.length == 0) return uint(-1);
			
			if (str.length > 1){
				if (str.charAt(0) == "#"){
					return uint("0x" + str.substr(1, str.length - 1));
				}
			}
			return uint("0x" + str);
		}
		
		public static function rgbString2int(str:String):int{
			if (str == null) return uint(-1);
			if (str.length == 0) return uint(-1);
			
			if (str.length > 1){
				var startIdx:int = str.indexOf("Rgb(");
				if (startIdx == 0){
					var endIdx:int = str.indexOf(")", 4); 	
					if(endIdx > 0){
						var rgbString:String = str.substring(startIdx + 4, endIdx);
						var rgbValues:Array = rgbString.split(',');
						if(rgbValues.length == 3){
							var r:int = int(rgbValues[0]);
							var g:int = int(rgbValues[1]);
							var b:int = int(rgbValues[2]);
							return r << 16 | g << 8 | b << 0; 
						}
					}
				}
			}
			return uint("0x" + str);
		}
		
		public static function baseFilename(filename:String):String{
			var leaf:String = filename;
			
			//remove suffix
			var suffixIdx:int = filename.indexOf(".AdhocReport");
			if (suffixIdx > 0){
				leaf = filename.substring(0, suffixIdx);
			}
			//only show the filaname, discard the subdir paths	
			var lastSlash:int = leaf.lastIndexOf("/");
			if (lastSlash > 0){
				leaf = leaf.substr(lastSlash + 1);
			}
			
			return leaf;	
		}
		
		public static function joinNameValuePair(values:Array, key:String, sep:String = ";"):String{
			var ret:String = '';
			
			if (!values || !key)	
				return ret;
				
			for(var i:int = 0; i < values.length; i++){
				var v:String = values[i] as String;
				if (v == "")
					continue;
				ret += key + '=' + v + sep;
			}
			
			return ret.substr(0, ret.length - 1); //delete last separator
		} 
		
		public static function gotoHelp(topic:String, siteurl:String=null, window:String=null):void{
			var app:CoreApp = CoreApp(Application.application);
			if (app != null)
				app.trackEvent("help", topic);
			var helpUrl:String = null;
			if (siteurl != null) {
				helpUrl = StringUtil.substitute(siteurl, topic);
			} else {
				if (app != null && app.hasOwnProperty("isFreeTrial") && app["isFreeTrial"] == true)
				{
					helpUrl = ResourceManager.getInstance().getString('sharable','SH_HELP_BXE_URL', [ topic ]);
				}
				else
				{
					helpUrl = ResourceManager.getInstance().getString('sharable','SH_HELP_FULL_URL', [ topic ]);				
				}	
			}
			var helpWindow:String = null;
			if (window != null) {
				helpWindow = window;
			} else {
				helpWindow = ResourceManager.getInstance().getString('sharable','SH_HELP_WINDOW');
			}
			navigateToURL(new URLRequest(helpUrl), helpWindow);
		}
				
		public static function startsWith(string:String, pattern:String):Boolean
		{
    		var string:String  = string.toLowerCase();
    		var pattern:String = pattern.toLowerCase();
    		return pattern == string.substr(0, pattern.length);
		}
		
		public static function localeAlertError(locKey:String, event:WebServiceResult = null, bundle:String = null, params:Array = null, parent:Sprite = null):void{
			!bundle && (bundle = Localization.getAppMainBundle());
			
			var errMsg:String = ResourceManager.getInstance().getString(bundle, locKey, params);
			alertErrMessage(errMsg, event);
		}
		
		public static function alertErrMessage(heading:String, event:WebServiceResult = null, parent:Sprite = null):void{
			var alert:Alert = Alert.show(formatErrMessage(heading, event), ResourceManager.getInstance().getString('errors', 'AL_title'), 4, parent);	
			if(Application.application.hasOwnProperty('alertPopupList')){
				var list:Dictionary = Dictionary(Application.application.alertPopupList);
				if(list){
					alert.addEventListener(mx.events.CloseEvent.CLOSE, function(event:CloseEvent):void{
						delete list[alert];
					});
					list[alert] = 1;
				}
			}
		}
		
		public static function alertLocaleErrorCodeMsgOnly(event:WebServiceResult):void{
			if(!event)
				return;
			
			var wsErrorCode:Number = event.getErrorCode();		
			var params:Array = [];
			var resourceErrStr:String = ResourceManager.getInstance().getString('errors', wsErrorCode.toString(), params);
			
			var alert:Alert = Alert.show(resourceErrStr, ResourceManager.getInstance().getString('errors', 'AL_title'), 4, null);	
			if(Application.application.hasOwnProperty('alertPopupList')){
				var list:Dictionary = Dictionary(Application.application.alertPopupList);
				if(list){
					alert.addEventListener(mx.events.CloseEvent.CLOSE, function(event:CloseEvent):void{
						delete list[alert];
					});
					list[alert] = 1;
				}
			}
		}
		
		public static function formatErrMessage(heading:String, event:WebServiceResult = null):String{
			if(!event)
				return heading;
			
			var wsErrorMsg:String = event.getErrorMessage();
			var wsErrorCode:Number = event.getErrorCode();		
			var params:Array = [];
			if(wsErrorMsg){
				// -2 is ERROR_CODE_OTHER, the generic error
				// -110 is ERROR_JASPER_OTHER, the generic jasper error
				// -103 is ERROR_DATA_UNAVAILABLE, the generic database error
				// We don't show the error message for these errors
				if((wsErrorCode != -2) && (wsErrorCode != -110) && (wsErrorCode != -103)) {
					wsErrorMsg = "\n\nReason:\n" + WebServiceResult.htmlUnescape(wsErrorMsg);
					params.push(wsErrorMsg);
				}else{
					params.push('');
					wsErrorMsg = null;
				}
			}
			
			// add the error code to the message to we can identify the various types
			if(!heading){
				heading = "Error (" + wsErrorCode + ")";
			}
			else
			{
				heading = heading + " (" + wsErrorCode + ")";
			}
			
			var resourceErrStr:String = 
				ResourceManager.getInstance().getString('errors', wsErrorCode.toString(), params);
				
			if (!resourceErrStr && !wsErrorMsg)
				return heading;
			
			var reasonStr:String = resourceErrStr ? resourceErrStr : wsErrorMsg;
						
			if (wsErrorCode == -3) {
				// query canceled, just say that	
				return reasonStr;
			}	
			return heading + '\n\n' + reasonStr;
		}
		
		public static function getApplicationVersion():String{
			return Application.application.parameters['v'];
		}
		
		public static function createCleanContextMenu():ContextMenu{
			var context:ContextMenu = new ContextMenu();
			context.hideBuiltInItems();
			context.builtInItems.print = false;
			context.addEventListener(ContextMenuEvent.MENU_SELECT, function():void{});
			return context;
		}
		
		public static function addLocaleContextMenuItem(contextMenu:ContextMenu, locKey:String, func:Function, bundle:String = null):void{
			!bundle && (bundle = Localization.getAppMainBundle());
			
			var caption:String = ResourceManager.getInstance().getString(bundle, locKey);
			var item:ContextMenuItem = new ContextMenuItem(caption);
			Localization.bindPropertyString(item, 'caption', locKey, bundle);
			contextMenu.customItems.push(item);
			item.addEventListener(ContextMenuEvent.MENU_ITEM_SELECT, func);
		}
		
		public static function getDrillMapBundleLocaleString(key:String, params:Array = null):String{
			return Util.getBundleLocaleString("drillmap", key, params);
		}
		
		public static function drawBorderAllExceptTop(graphics:Graphics, width:uint, height:uint):void{
			drawBorder(graphics, width, height, false);
		}
		
		public static function drawBorder(graphics:Graphics, width:uint, height:uint, top:Boolean = true):void
		{
			graphics.lineStyle(1, 0x000000);
			
			if (top){
				graphics.moveTo(0, 0);
				graphics.lineTo(width, 0);
			}else{
				graphics.moveTo(width, 0);
			}
			graphics.lineTo(width, height);
			graphics.lineTo(0, height);
			graphics.lineTo(0, 0);
		}
		
		public static function convertFirstLevelXml2Array(xml:XML):Array{
			var array:Array = [];
			if(xml){
				for each (var property:XML in xml.*){
					array[property.name()] = property.toString();
				}
			}	
			return array;
		}
		
		public static function getFirstLevelTags(xml:XML):Array{
			var tags:Array = [];
			if(xml){
				for each (var property:XML in xml.*){
					tags.push(property.name().toString());
				}	
			}
			return tags;
		}
		
		public static function hide(obj:UIComponent):void{
			obj.visible = obj.includeInLayout = false;	
		}
		
		public static function show(obj:UIComponent):void{
			obj.visible = obj.includeInLayout = true;
		}
		
		public static function setContentByArray(conf:Array, node:XML): void {
			if(conf && node){
				for (var key:String in conf){
					node[key] = conf[key];
				}	
			}
		}
		
		public static function toCData(str:String):String{
			return '<![CDATA[' + str + ']]>';	
		}
		
		public static function getCData(value:String):String{
			if(value.indexOf("<![CDATA[") == 0 && value.length >= 12){
				var val:String = value.substring(9, value.length - 3);
				return val;
			}
			return value;
		}
		
		public static function alertYesNoConfirmation(message:String, closeFunc:Function, title:String="UseDefaultLocalize",  parent:Sprite = null):void{
			if(title == "UseDefaultLocalize"){
				title = ResourceManager.getInstance().getString('sharable', 'SH_confirmation');
			}			
	       	Alert.show(message, title, Alert.YES | Alert.NO, parent, closeFunc);
		}
		
        /**
         * Like the alertYesNoConfirmation but uses a styled TitleWindow.
         */
        public static function yesNoConfirmation(message:String, closeFunction:Function, caller:UIComponent):void {
            // Add event listener
            (caller as UIComponent).addEventListener(CloseEvent.CLOSE, closeFunction);
            // Pop up confirmation dialog
            var confirmationDialog:ConfirmationDialog = new ConfirmationDialog();
            confirmationDialog.message = message;
            confirmationDialog.caller = caller;
            PopUpManager.addPopUp(confirmationDialog, caller, true);
            PopUpManager.centerPopUp(confirmationDialog);
            PopUpManager.bringToFront(confirmationDialog);
        }
        
        /**
         * Like the Flex Alert.show() but uses a styled TitleWindow.
         */
        public static function showBirstAlert(message:String, title:String, icon:Class, caller:UIComponent):void {
            // Pop up Birst-style alert
            var birstAlert:BirstAlert = new BirstAlert();
            birstAlert.title = title;
            birstAlert.alertTitle = title;
            birstAlert.message = message;
            birstAlert.icon = icon;
            PopUpManager.addPopUp(birstAlert, caller, true);
            PopUpManager.centerPopUp(birstAlert);
            PopUpManager.bringToFront(birstAlert);
        }

		// dev mode : check for any flash vars as smiweb url, use that instead
		public static function getApplicationSMIWebURL():String		
		{
			return Application.application.parameters['smiroot'];	
		}
		
		// dev mode : check for any flash vars as smiweb url, use that instead
		public static function getApplicationAcornDirectory():String		
		{
			return Application.application.parameters['acornroot'];	
		}
		
		public static function getApplicationGoogleAccount():String		
		{
			return Application.application.parameters['gaAccount'];	
		}
		
		// dev mode : check for any flash vars as scheduler url, use that instead
		public static function getApplicationSchedulerURL():String		
		{
			return Application.application.parameters['schedulerRoot'];	
		}
		
		public static function getApplicationBingMapsOn():Boolean{
			return Application.application.parameters['bingMaps'] == 'true';	
		}
		
		public static function isOlapCube():Boolean {
			return Application.application.parameters['IsOlapCube'] == 'true';
		}
		
		public static function getBaseUrl():String 
		{
			var fullUrl:String = Application.application.url;
			return URLUtil.getProtocol(fullUrl) + "://" + URLUtil.getServerNameWithPort(fullUrl) + "/";
		}
		
		public static function gotFlashVars(event:ResultEvent):void {
			var result:WebServiceResult = WebServiceResult.parse(event);

			if (!result.isSuccess()){
				return;
			}
			
			var res:XMLList = result.getResult();
			var app:Application = Application.application as Application;
			app.parameters['acornroot'] = String(res.FlashVars.@acornRoot);	
			app.parameters['gaAccount'] = String(res.FlashVars.@gaAccount);
			app.parameters['schedulerRoot'] = String(res.FlashVars.@schedulerRoot);
			app.parameters['bingMaps'] = String(res.FlashVars.@bingMaps);
			app.parameters['aspNetSessionId'] = String(res.FlashVars.@aspNetSessionId);
			app.parameters['jsessionId'] = String(res.FlashVars.@jsessionId);
			app.parameters['aspxFormsAuth'] = String(res.FlashVars.@aspxFormsAuth);
			app.parameters['IsOlapCube'] = String(res.FlashVars.@IsOlapCube);
		}
		
		public static function getDateFormatString(date:Date):String{
			var dateFmt:DateFormatter = new DateFormatter();
			dateFmt.formatString = "MM/DD/YYYY JJ:NN:SS";
			return dateFmt.format(date);	
		}
		
		public static function getDateTimeStringInFormat(date:Date, format:String, useUtc:Boolean = false):String {
			var formatter:BDateFormatter = new BDateFormatter();
			formatter.formatString = format;
			formatter.useUtc = useUtc;
			return formatter.format(date);				 
		}
		
		public static function getFlexDatePattern(datePattern:String):String {
			if(datePattern){
				return datePattern.replace(".SSS","").replace(/d/g,"D").replace(/y/g,"Y").replace(/H/g,"J").replace(/h/g,"L").replace(/m/g,"N").replace(/s/g,"S").replace(/a/g,"A");	
			}	
			return datePattern;
		}
		
		public static function getHumanDatePattern(datePattern:String):String {
			if (datePattern){
				return datePattern.replace(/D/g,"d").replace(/Y/g,"y").replace(/J/g,"H").replace(/L/g,"h").replace(/N/g,"m").replace(/S/g,"s").replace(/A/g,"a");	
			}
			return datePattern;
		}
		
		/**
		 * Convert a date string in the current format to the new format.
		 */
		public static function convertDateStringToFormat(dateStr:String, currentFormat:String, newFormat:String):String{
			var date:Date = Util.setDateFromStringInFormat(dateStr, currentFormat);
			if (date == null) {
				date = Util.setDateFromStringInFormat(dateStr, newFormat);
				if (date) {
					// its already in this format - just return it
					return dateStr;
				}
			}
			if(newFormat == DateTimePicker.INTERNAL_DATETIME_FORMAT){
				return DateTimePicker.convertDisplayDateTime2ProcessingString(date);
			}
			return Util.getDateTimeStringInFormat(date, newFormat);
		}
		
		
		public static function procDateFormat2DisplayFormat(dateStr:String):String{
			return Util.procDateTimeFormat2DisplayFormat(dateStr, true);	
		}
		
		public static function procDateTimeFormat2DisplayFormat(dateStr:String, isDate:Boolean = false):String{
			if(dateStr && dateStr != "NO_FILTER"){
				var app:CoreApp = CoreApp(Application.application);
				var fmt:String = isDate ? app.localeDateFormat : app.localeDateTimeFormat;
				var internalFmt:String = isDate ? DateTimePicker.INTERNAL_DATE_FORMAT : DateTimePicker.INTERNAL_DATETIME_FORMAT;
				return Util.convertDateStringToFormat(dateStr, internalFmt, fmt);			
			}
			return dateStr;
		}
		
		public static function fixForSingleDigitPattern(datePattern:String):String {
			if (datePattern.indexOf("MM") < 0 && datePattern.indexOf("M") >= 0)
			{
				datePattern = datePattern.replace(/M/g,"MM");
			}
			if (datePattern.indexOf("DD") < 0 && datePattern.indexOf("D") >= 0)
			{
				datePattern = datePattern.replace(/D/g,"DD");
			}
			return datePattern;
		}
		
		public static function setDateFromStringInFormat(dateStr:String, format:String):Date {
			if (dateStr.indexOf("T") > 0 && dateStr.indexOf("Z") > 0) {
				// ISO datetime format
				format = "YYYY-MM-DD HH:mm";
			}
			format = Util.getFlexDatePattern(format);
			if (format.indexOf(" ") != -1) //contains timeformat
			{
				var useSpace:Boolean = true;
				if (dateStr.indexOf(" ") < 0)
					useSpace = false;
				var dateFormat:String = format.substring(0, format.indexOf(" "));
				var datePart:String = dateStr.substring(0, dateStr.indexOf(useSpace ? " " : "T"));
				var date:Date = DateField.stringToDate(datePart, Util.fixForSingleDigitPattern(dateFormat));
				
				var timeFormat:String = format.substr(format.indexOf(" ") + 1);
				var timePart:String = dateStr.substring(dateStr.indexOf(useSpace ? " " : "T") + 1);
				
				var amPmStr:String = "";
				if (timePart.indexOf(" ") != -1)//contains AM/PM
				{
					amPmStr = timePart.charAt(timePart.indexOf(" ") + 1);
					timePart = timePart.substr(0, timePart.indexOf(" "));
				}
				if (timePart.indexOf("Z") > 0) {
					timePart = timePart.substring(0, timePart.indexOf("Z"));
				}
				
				var timeParts:Array = timePart.split(":");
				if (timeParts.length > 0)
					date.hours = Number(timeParts[0]);						
				if (timeParts.length > 1)
					date.minutes = Number(timeParts[1]);
				if (timeParts.length > 2)
					date.seconds = Number(timeParts[2]);
					
				if (amPmStr == "P" || amPmStr == "p")
				{
					if (date.hours != 12)
						date.hours += 12;
				}
				else if (amPmStr == "A" || amPmStr == "a")
				{
					if (date.hours == 12)
						date.hours = 0;
				}
				
				return date;
			}
			else{
				return Util.stringToDate(dateStr, format);			
			}	
		}
		
	/**
	 *  Adapted from DateField, to change the year 2k part
	 * 
     *  Parses a String object that contains a date, and returns a Date
     *  object corresponding to the String.
     *  The <code>inputFormat</code> argument contains the pattern
     *  in which the <code>valueString</code> String is formatted.
     *  It can contain <code>"M"</code>,<code>"D"</code>,<code>"Y"</code>,
     *  and delimiter and punctuation characters.
     *
     *  <p>The function does not check for the validity of the Date object.
     *  If the value of the date, month, or year is NaN, this method returns null.</p>
     * 
     *  <p>For example:
     *  <pre>var dob:Date = DateField.stringToDate("06/30/2005", "MM/DD/YYYY");</pre>        
     *  </p>
     *
     *  @param valueString Date value to format.
     *
     *  @param inputFormat String defining the date format.
     *
     *  @return The formatted date as a Date object.
     *
     */
    public static function stringToDate(valueString:String, inputFormat:String):Date
    {
        var mask:String
        var temp:String;
        var dateString:String = "";
        var monthString:String = "";
        var yearString:String = "";
        var j:int = 0;
		
		inputFormat = fixupFormat(valueString, inputFormat);
        var n:int = inputFormat.length;
        for (var i:int = 0; i < n; i++,j++)
        {
            temp = "" + valueString.charAt(j);
            mask = "" + inputFormat.charAt(i);

            if (mask == "M")
            {
                if (isNaN(Number(temp)) || temp == " ")
                    j--;
                else
                    monthString += temp;
            }
            else if (mask == "D")
            {
                if (isNaN(Number(temp)) || temp == " ")
                    j--;
                else
                    dateString += temp;
            }
            else if (mask == "Y")
            {
                yearString += temp;
            }
            else if (!isNaN(Number(temp)) && temp != " ")
            {
                return null;
            }
        }

        temp = "" + valueString.charAt(inputFormat.length - i + j);
        if (!(temp == "") && (temp != " "))
            return null;

        var monthNum:Number = Number(monthString);
        var dayNum:Number = Number(dateString);
        var yearNum:Number = Number(yearString);

        if (isNaN(yearNum) || isNaN(monthNum) || isNaN(dayNum))
            return null;

		//Seeneng - year 00-20 means 2000-2020
		//which also means we have to update this for 2021, 2022, etc.. bleh
		//Bug 9826
        if (yearString.length == 2 && yearNum <= 20)
            yearNum+=2000;

        var newDate:Date = new Date(yearNum, monthNum - 1, dayNum);

        if (dayNum != newDate.getDate() || (monthNum - 1) != newDate.getMonth())
            return null;

        return newDate;
    }

	private static function fixupFormat(valueString:String, inputFormat:String):String{
		var delimiter:String = '/';
		if(inputFormat.indexOf(delimiter) == -1){
			delimiter = '-';
		}
		var fmtVals:Array = inputFormat.split(delimiter);
		var valVals:Array = valueString.split(delimiter);
		if (fmtVals.length == valVals.length){
			for(var i:int = 0; i < fmtVals.length; i++){
				var field:String = String(fmtVals[i]);
				var value:String = String(valVals[i]);
				if (field.indexOf('M') != -1 || (field.indexOf('D') != -1)){
					if (field.length < value.length){
						//max 2
						if(field.indexOf('M') != -1){
							fmtVals[i] = 'MM';
						}
						if (field.indexOf('D') != -1){
							fmtVals[i] = 'DD';
						}
					}
				}
			}
		}
		
		return fmtVals.join(delimiter);	
	}
		public static function parseDateString(str:String):Date{
			var miliSecsSince1970:Number = Date.parse(str);
			var date:Date = new Date();
			if (isNaN(miliSecsSince1970)) {
				var parts:Array = str.split(" ");
				var datePart:String = parts[0];
				var year:String = datePart.split("-")[0];
				var month:String = datePart.split("-")[1];
				var dt:String = datePart.split("-")[2];
				
				var timePart:String = parts[1];
				var hour:String = timePart.split(":")[0];
				var min:String = timePart.split(":")[1];
				var seconds:String = timePart.split(":")[2];
				var sec:String = seconds.split(".")[0];
				var millsec:String = seconds.split(".")[1];
				date = new Date(year, Number(month) - 1, Number(dt), Number(hour), Number(min), Number(sec), Number(millsec));				
			}
			else {
				date.setTime(miliSecsSince1970);
			}
			return date;
		}

		public static function htmlEncode(text:String):String{
			text = text.split("&").join("&amp;");
			text = text.split(">").join("&gt;");
			text = text.split("<").join("&lt;");
			text = text.split("%").join("&#37;");
			text = text.split("\"").join("&quot;");
			return text;						
		}
		
		public static function htmlDecode(text:String):String{
			text = text.split("&amp;").join("&");
			text = text.split("&gt;").join(">");
			text = text.split("&lt;").join("<");
			text = text.split("&#37;").join("%");
			text = text.split("&quot;").join("\"");
			return text;						
		}
		
        
        public static function isPlayer10Plus():Boolean{
        	if (Util._majorVersion){
        		return Util._majorVersion >= 10;
        	}
        	
        	var version:String = Capabilities.version;
        	var regexp:RegExp = /^([A-Za-z]+)\s+(\d+),(\d+),(\d+),(\d+)/;
        	var args:Array = version.match(regexp);	
      
      		if (args && args.length > 3){
        		Util._os = args[1];
        		Util._majorVersion = Number(args[2]);
        		
        		return Util._majorVersion >= 10;
        	}
        	
        	return false;
        }
        
        public static function getFlashVarsExtraParams():Array{
        	var parameters:String = mx.core.Application.application.parameters.extraParams;
        	if (parameters){
				if (parameters.indexOf(",") == 0) {
					parameters = parameters.substr(1);
				}
				return parameters.split(',');
        	}
        	return new Array();
        }
        
        public static function extraParamsHasDashPageParams():Boolean{
        	var parameters:String = mx.core.Application.application.parameters.extraParams;
        	if(parameters){
        		return (parameters.indexOf("birst.dashboard=") >= 0 && parameters.indexOf("birst.page=") >= 0);
        	}
        	return false;
        }
        
        public static function alignAlertErrMessage(heading:String, event:WebServiceResult = null):void{
			var alert:Alert = Alert.show(formatErrMessage(heading, event), ResourceManager.getInstance().getString('errors', 'AL_title'));			 
			PopUpManager.centerPopUp(alert);
			alert.move(200,150);	
		}
		
		public static function deleteCurrentSelectedRow(grid:DataGrid):Boolean{
			var row:Object = grid.selectedItem;
	
			if (row == null){
				return false;
			}
			
        	var provider:ArrayCollection = grid.dataProvider as ArrayCollection;
        	var deleteIndex:int = provider.getItemIndex(row);
        	provider.removeItemAt(deleteIndex);
        	
        	//validate for it to refresh and set the ui positions correctly
        	grid.validateNow();
        	
        	//readjust selected row. 
        	if (provider.length > 0){
        		if (deleteIndex < provider.length){	
        			grid.selectedIndex = deleteIndex;
        		}else{
        			--deleteIndex;
        			if (deleteIndex < provider.length)
        				grid.selectedIndex = deleteIndex;
        		}
        	}
        	
        	return true;
		}
		
		public static function convertSMIServletUrl(url:String):String{
			var flashVarRootURL:String = getApplicationSMIWebURL();
            if(flashVarRootURL != null && flashVarRootURL.length > 0){
            	return flashVarRootURL + url + getJSessionIDSuffix();
            }
            return url + getJSessionIDSuffix();		
		}
		
		public static function getJSessionIDSuffix():String{
			//Hack to get our JSESSIONID
			var jsession:String = '';
			var cookies:String = ExternalInterface.call('function getCookie(){ return document.cookie }');
			if(cookies && cookies.length > 0){
				if(cookies.indexOf(';') != -1){
					var cks:Array = cookies.split(';');
					for(var i:int = 0; i < cks.length; i++){
						var keyVal:Array = String(cks[i]).split('=');
						if(keyVal.length > 1){
							if(keyVal[0].toString().toUpperCase() == 'JSESSIONID'){
								jsession = ';' + keyVal;
								break;
							}
						}
					}
				}else{
					var sess:Array = cookies.split('=');
					if(sess.length > 1 && sess[0] == 'JSESSIONID'){
						jsession = ';jsessionid=' + sess[1];
					}
				}	
			}
			return jsession;
		}
	
		public static function isPartOfAnyPromptGroup(prompt:XML, promptsList:XMLList):Boolean
		{
			for(var i:int = 0; i < promptsList.length(); i++)
			{
				var potentialPGPrompt:XML = promptsList[i];
				if(potentialPGPrompt.PromptType.toString() == "PromptGroup")
				{
					var optionsList:XMLList = potentialPGPrompt.Options.Option;
					for(var j:int = 0; j < optionsList.length(); j++)
					{
						var option:String = optionsList[j].toString();
						var paramName:String = prompt.ParameterName.toString(); 
						if(option == paramName)
						{
							return true;
						}
					}
				}
			}	
			return false;
		}
		
		public static function getReportLabel(node:XML):String{
			if(node.attribute('rl').length() > 0 && node.attribute('t') != 's'){
				return node.@rl.toString();
			}
			
			if(node.attribute('l').length() > 0){
				return node.@l.toString();
			}
			
			var name:String = node.@v.toString();
			var attrLabel:Array = name.split('.');
			if(attrLabel.length == 2){
				return attrLabel[1];
			}
			
			return '';
		}

		public static function getBundleLocaleString(mainBundle:String, key:String, params:Array = null, locale:String = null):String
		{
			return ResourceManager.getInstance().getString(mainBundle, key, params, locale);			
		}				
			
		CONFIG::debug
		public static function globalTrace(str:String):void{
			
		}
		
		public static function setObjectSelected(id:String, type:String, addToList:Boolean):void {
			if (Application.application.hasOwnProperty('selectedLayoutIds')) {
				if (Application.application.selectedLayoutIds.contains(id))
					return;
					
				if (addToList == false) {
					clearSelectedObjects();
				}
				if (! Application.application.selectedLayoutIds.contains(id)) {
					Application.application.selectedLayoutIds.addItem(id);
					Application.application.selectedLayoutTypes.addItem(type);
					Application.application.dispatchEvent(new AppEvent(AppEvent.EVT_SELECTED, 'Util', 'setObjectSelected', id));
				} 
			}
		}
		
		public static function clearSelectedObjects():void {
			var a:Array = new Array();
			for (var i:uint = 0; i < Application.application.selectedLayoutIds.length; i++) {
				a.push(Application.application.selectedLayoutIds.getItemAt(i));
			}
			Application.application.selectedLayoutIds.removeAll();
			Application.application.selectedLayoutTypes.removeAll();
			a.forEach(function(obj:Object, index:int, array:Array):void {
				Application.application.dispatchEvent(new AppEvent(AppEvent.EVT_UNSELECTED, 'Util', 'setObjectSelected', obj.toString()));
			});
		}
		
		private static var EMPTY_COLLECTION: ArrayCollection = new ArrayCollection();
		public static function getSelectedLayoutIds():ArrayCollection {
			if (Application.application.hasOwnProperty('selectedLayoutIds')) {
				return Application.application.selectedLayoutIds;
			}
			return EMPTY_COLLECTION;
		}
		
		public static function getSelectedLayoutTypes():ArrayCollection {
			if (Application.application.hasOwnProperty('selectedLayoutTypes')) {
				return Application.application.selectedLayoutTypes;
			}
			return EMPTY_COLLECTION;
		}
		
		public static function copyAndSelectSelectedLayoutEntities():void {
			var currentlySelected:Array = getSelectedEntities();
			if (currentlySelected.length > 0) {
				var config:AdHocConfig = AdHocConfig.instance();
				var newSelected:Array = new Array();
				var i:int;
				for (i = 0; i < currentlySelected.length; i++) {
					var entity:XML = XML(currentlySelected[i]);
					var copy:XML = entity.copy();
					var nextId:uint = config.getNextElementId();
					copy.ReportIndex = nextId;
					copy.LabelName = null;
					AdHocConfig.instance().addElementNoUpdateOfServer(copy);
					newSelected.push(copy);
				}
				
				clearSelectedObjects();
				
				for (i = 0; i < newSelected.length; i++) {
					var item:XML = XML(newSelected[i]);
					var reportIndex:String = item.ReportIndex;
					var type:String = AppEvent.EVT_COLUMN_CHOSEN;
					if (item.localName() == ReportXml.LABEL)
						type = AppEvent.EVT_LABEL_CHOSEN;
					else if (item.localName() == ReportXml.IMAGE)
						type = AppEvent.EVT_IMAGE_CHOSEN;
					else if (item.localName() == ReportXml.SUBREPORT)
						type = AppEvent.EVT_SUBREPORT_CHOSEN;
					else if (item.localName() == ReportXml.CHART)
						type = AppEvent.EVT_CHART_CHOSEN;
					else if (item.localName() == ReportXml.RECTANGLE)
						type = AppEvent.EVT_RECTANGLE_CHOSEN;
					else if (item.localName() == ReportXml.BUTTON)
						type = AppEvent.EVT_BUTTON_CHOSEN;
					setObjectSelected(reportIndex, type, true);
				}
			}
		}
		
		public static function getSelectedEntities():Array {
			var selLayoutIds:ArrayCollection = Util.getSelectedLayoutIds();
			var selectedEntities:Array = new Array();
			var parent:XML = AdHocConfig.instance().getConfig().Entities[0];
			var entities:XMLList = parent.children();
			for (var i:uint = 0; i < entities.length(); i++) {
				var entity:XML = entities[i];
				var reportId:String = entity.ReportIndex.toString();
				if (selLayoutIds.contains(reportId))
					selectedEntities.push(entity);
			}
			return selectedEntities;
		}
		
		public static function clearSelectedItems(event:AppEvent):void {
			if (event.callerOperation == CLEAR_REPORT || event.callerOperation == OPEN_REPORT) {
				clearSelectedObjects();
			}
		}
		
		public static function getLink(spaceId:String, dashboardName:String, pageName:String, type:String):String {
			dashboardName =  encodeURIComponent(dashboardName);
			pageName =  encodeURIComponent(pageName);
			if (type == 'flexModule')
				return Util.getBaseUrl() + 'FlexModule.aspx?birst.module=dashboard' + '&birst.spaceId=' + spaceId + '&birst.dashboard=' + dashboardName + '&birst.page=' + pageName;
			else if (type == 'sso')
				return '<IFRAME HEIGHT="<height>" WIDTH="<width>" SRC="' + Util.getBaseUrl() + 'SSO.aspx?birst.SSOToken=<ssoToken>&birst.embedded=true&birst.hideDashboardNavigation=<true/false>&birst.module=dashboard' + '&birst.spaceId=' + spaceId + '&birst.dashboard=' + dashboardName + '&birst.page=' + pageName + '"/>';
			else if (type == 'appexchange') // 250 is 25.0, version of WSDL, need to change as we change our version
				return Util.getBaseUrl() + 'AppExchangeSSO.aspx?serverurl={!API.Partner_Server_URL_250}&sessionid={!API.Session_ID}&birst.useSFDCEmailForBirstUsername=<true/false>&birst.spaceId=' + spaceId + '&birst.hideDashboardNavigation=<true/false>&birst.hideDashboardPrompts=<true/false>&birst.module=dashboard' + '&birst.dashboard=' + dashboardName + '&birst.page=' + pageName;
			else if (type == 'apex') // XXX need a page for this... AppExchange... XXX
				return '<apex:page standardController="<object>" showHeader="false" sidebar="false">\n\t<apex:iframe height="<height>" width="<width>" src="' + Util.getBaseUrl() + 'AppExchangeSSO.aspx?serverurl={!$API.Partner_Server_URL_250}&sessionid={!$API.Session_ID}&birst.useSFDCEmailForBirstUsername=<true/false>&birst.hideDashboardNavigation=true&birst.hideDashboardPrompts=true&birst.module=dashboard&birst.spaceId=' + spaceId + '&birst.dashboard=' + dashboardName + '&birst.page=' + pageName + '"/>\n</apex:page>';
			else
				return null;
		}
		
		public static function moveToRightBottom(obj:Object, xOffset:int = 0):void{
			var app:Object = Application.application;
			var target:IFlexDisplayObject = obj as IFlexDisplayObject;
			var bottomLoc:Point = app.localToGlobal(new Point(Math.max(app.width - target.width, 0), 
															   Math.max(app.height - target.height, 0)));
			target.move(bottomLoc.x, bottomLoc.y);
		}
		
		public static function decodeURIComponentAndSpaces(str:String):String{
			if (str != null && str.length > 0){
				var pattern:RegExp = new RegExp("\\+", "g");
				str = str.replace(pattern, ' ');
				str = decodeURIComponent(str);
			}
			return str;
		}
	}
}