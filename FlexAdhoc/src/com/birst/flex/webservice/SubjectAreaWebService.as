package com.birst.flex.webservice
{
    import com.birst.flex.core.Tracer;
    
    import flash.external.ExternalInterface;
    
    import mx.collections.ArrayCollection;
    
	
	public class SubjectAreaWebService extends WebServiceClient
	{
		public static const WS_SUBJECT_AREA:String = "SubjectArea";
		public static const WS_SUBJECT_AREA_DEFAULT_METHOD:String = "getSubjectArea";
		
		public function SubjectAreaWebService(wsName:String=null, wsMethod:String=null, callerComp:String=null, callerOp:String="constructor")
		{			
			//super("SubjectArea", "getSubjectArea", Components.SUBJECT_AREA_WS, 'constructor');
			wsName = wsName == null ? WS_SUBJECT_AREA : wsName;
			wsMethod = wsMethod == null ? WS_SUBJECT_AREA_DEFAULT_METHOD : wsMethod;
			callerComp = callerComp == null ? Components.SUBJECT_AREA_WS : callerComp;		
				
			super(wsName, wsMethod, callerComp, callerOp);
		}

        /**
         * Retrieves all the groups for the current space.
         * <p/>
         * If 'isStandalone' is true then it is assumed that 'wsPort', 'sessionId'
         * and 'processResults' are also set.  'isStandalone' indicates that the
         * developer wants to test the Flex app outside the Birst browser window
         * so he/she must provide an absolute WSDL URL and an SMIWeb JSESSIONID.
         */ 
        public static function getGroups(processResults:Function,
                                         handleFault:Function,
                                         isStandalone:Boolean=false,
                                         wsPort:String=null,
                                         sessionId:String=null,
                                         hostName:String=null):void 
        {
            // If testing w/localhost, override root URL
            var ws:WebServiceClient;
            if (isStandalone) 
            {
                Tracer.debug("'getGroups() using hostName: " + hostName
                    + "; wsPort: " + wsPort + "; sessionId: " + sessionId);
                // Need an absolute URL for standalone testing
                ws = new WebServiceClient("SubjectArea", 
                                          "getGroups", 
                                          "CustomSubjectAreaModule", 
                                          "getGroups",
                                           null, 
                                          "http://" + hostName + ":" + wsPort + "/SMIWeb/wsdl/");
                // Set JSESSIONID cookie using ExternalInterface and JavaScript.
                if (sessionId != null) 
                {
                    ExternalInterface.call("setJSessionId", "JSESSIONID", sessionId);
                }
            } 
            else 
            {
                ws = new WebServiceClient("SubjectArea", 
                                          "getGroups", 
                                          "CustomSubjectAreaModule", 
                                          "getGroups");
            }
            ws.setResultListener(processResults);
            ws.setFaultListener(handleFault);
            ws.showBusyCursor = true;
            ws.execute();
        }

        /**
         * Retrieves all the groups for all the subject areas for the current space.
         */ 
        public static function getSubjectAreasGroups(processResults:Function,
                                                     handleFault:Function,
                                                     isStandalone:Boolean=false,
                                                     wsPort:String=null,
                                                     sessionId:String=null,
                                                     hostName:String=null):void 
        {
            // If testing w/localhost, override root URL
            var ws:WebServiceClient;
            if (isStandalone) 
            {
                Tracer.debug("'getSubjectAreasGroups() using hostName: " + hostName
                    + "; wsPort: " + wsPort + "; sessionId: " + sessionId);
                // Need an absolute URL for standalone testing
                ws = new WebServiceClient("SubjectArea", 
                                          "getSubjectAreasGroups", 
                                          "CustomSubjectAreaModule", 
                                          "getSubjectAreasGroups",
                                           null, 
                                          "http://" + hostName + ":" + wsPort + "/SMIWeb/wsdl/");
                // Set JSESSIONID cookie using ExternalInterface and JavaScript.
                if (sessionId != null) 
                {
                    ExternalInterface.call("setJSessionId", "JSESSIONID", sessionId);
                }
            } 
            else 
            {
                ws = new WebServiceClient("SubjectArea", 
                                          "getSubjectAreasGroups", 
                                          "CustomSubjectAreaModule", 
                                          "getSubjectAreasGroups");
            }
            ws.setResultListener(processResults);
            ws.setFaultListener(handleFault);
            ws.showBusyCursor = true;
            ws.execute();
        }

        /**
         * Retrieves an "extended" version of the subject areas which includes a
         * unique identifier for each folder/attribute/measure element and an
         * indicator of the type of folder/attribute/measure element.
         * <p/>
         * These extended XML attributes are needed in order to accurately place
         * drag-and-drop results in the custom subject area work area Tree and
         * to maintain information about the type of element once it has been 
         * moved to the custom subject area Tree.
         * <p/>
         * If 'isStandalone' is true then it is assumed that 'wsPort', 'sessionId'
         * and 'processResults' are also set.  'isStandalone' indicates that the
         * developer wants to test the Flex app outside the Birst browser window
         * so he/she must provide an absolute WSDL URL and an SMIWeb JSESSIONID.
         */ 
        public static function getExtendedSubjectAreas(processResults:Function,
                                                       handleFault:Function,
                                                       isStandalone:Boolean=false,
                                                       wsPort:String=null,
                                                       sessionId:String=null,
                                                       hostName:String=null):void 
        {
            // If testing w/localhost, override root URL
            var ws:WebServiceClient;
            if (isStandalone) 
            {
                Tracer.debug("'getExtendedSubjectAreas() using hostName: " + hostName
                    + "; wsPort: " + wsPort + "; sessionId: " + sessionId);
                // Need an absolute URL for standalone testing
                ws = new WebServiceClient("SubjectArea", 
                                          "getExtendedSubjectArea", 
                                          "CustomSubjectAreaModule", 
                                          "getExtendedSubjectArea",
                                           null, 
                                          "http://" + hostName + ":" + wsPort + "/SMIWeb/wsdl/");
                // Set JSESSIONID cookie using ExternalInterface and JavaScript.
                if (sessionId != null) 
                {
                    ExternalInterface.call("setJSessionId", "JSESSIONID", sessionId);
                }
            } 
            else 
            {
                ws = new WebServiceClient("SubjectArea", 
                                          "getExtendedSubjectArea", 
                                          "CustomSubjectAreaModule", 
                                          "getExtendedSubjectArea");
            }
            ws.setResultListener(processResults);
            ws.setFaultListener(handleFault);
            ws.showBusyCursor = true;
            ws.execute();
        }

        /**
         * Saves the "extended" version of the custom subject area currently being
         * edited.
         * <p/>
         * If 'isStandalone' is true then it is assumed that 'wsPort', 'sessionId'
         * and 'processResults' are also set.  'isStandalone' indicates that the
         * developer wants to test the Flex app outside the Birst browser window
         * so he/she must provide an absolute WSDL URL and an SMIWeb JSESSIONID.
         */ 
        public static function saveCustomSubjectArea(groupNames:ArrayCollection,
                                                     customSubjectArea:XML,
                                                     processResults:Function,
                                                     handleFault:Function,
                                                     isStandalone:Boolean=false,
                                                     wsPort:String=null,
                                                     sessionId:String=null,
                                                     hostName:String=null):void 
        {
            // If testing w/localhost, override root URL
            var ws:WebServiceClient;
            if (isStandalone) 
            {
                Tracer.debug("'saveCustomSubjectArea() using hostName: " + hostName
                    + "; wsPort: " + wsPort + "; sessionId: " + sessionId);
                // Need an absolute URL for standalone testing
                ws = new WebServiceClient("SubjectArea", 
                                          "saveCustomSubjectArea", 
                                          "CustomSubjectAreaModule", 
                                          "saveCustomSubjectArea",
                                           null, 
                                          "http://" + hostName + ":" + wsPort + "/SMIWeb/wsdl/");
                // Set JSESSIONID cookie using ExternalInterface and JavaScript.
                if (sessionId != null) 
                {
                    ExternalInterface.call("setJSessionId", "JSESSIONID", sessionId);
                }
            } 
            else 
            {
                ws = new WebServiceClient("SubjectArea", 
                                          "saveCustomSubjectArea", 
                                          "CustomSubjectAreaModule", 
                                          "saveCustomSubjectArea");
            }
            ws.setResultListener(processResults);
            ws.setFaultListener(handleFault);
            ws.showBusyCursor = true;
            ws.execute(groupNames, customSubjectArea);
        }

        /**
         * Saves all the subject areas' groups metadata.
         */ 
        public static function saveSubjectAreaGroups(subjectAreaName:String,
                                                     subjectAreaGroups:ArrayCollection,
                                                     processResults:Function,
                                                     handleFault:Function,
                                                     isStandalone:Boolean=false,
                                                     wsPort:String=null,
                                                     sessionId:String=null,
                                                     hostName:String=null):void 
        {
            // If testing w/localhost, override root URL
            var ws:WebServiceClient;
            if (isStandalone) 
            {
                Tracer.debug("'saveSubjectAreaGroups() using hostName: " + hostName
                    + "; wsPort: " + wsPort + "; sessionId: " + sessionId);
                // Need an absolute URL for standalone testing
                ws = new WebServiceClient("SubjectArea", 
                                          "saveSubjectAreaGroups", 
                                          "CustomSubjectAreaModule", 
                                          "saveSubjectAreaGroups",
                                           null, 
                                          "http://" + hostName + ":" + wsPort + "/SMIWeb/wsdl/");
                // Set JSESSIONID cookie using ExternalInterface and JavaScript.
                if (sessionId != null) 
                {
                    ExternalInterface.call("setJSessionId", "JSESSIONID", sessionId);
                }
            } 
            else 
            {
                ws = new WebServiceClient("SubjectArea", 
                                          "saveSubjectAreaGroups", 
                                          "CustomSubjectAreaModule", 
                                          "saveSubjectAreaGroups");
            }
            ws.setResultListener(processResults);
            ws.setFaultListener(handleFault);
            ws.showBusyCursor = true;
            ws.execute(subjectAreaName, subjectAreaGroups);
        }

        /**
         * Deletes the specified custom subject area.
         */ 
        public static function deleteCustomSubjectArea(subjectAreaName:String,
                                                       processResults:Function,
                                                       handleFault:Function,
                                                       isStandalone:Boolean=false,
                                                       wsPort:String=null,
                                                       sessionId:String=null,
                                                       hostName:String=null):void 
        {
            // If testing w/localhost, override root URL
            var ws:WebServiceClient;
            if (isStandalone) 
            {
                Tracer.debug("'deleteCustomSubjectArea() using hostName: " + hostName
                    + "; wsPort: " + wsPort + "; sessionId: " + sessionId);
                // Need an absolute URL for standalone testing
                ws = new WebServiceClient("SubjectArea", 
                                          "deleteCustomSubjectArea", 
                                          "CustomSubjectAreaModule", 
                                          "deleteCustomSubjectArea",
                                           null, 
                                          "http://" + hostName + ":" + wsPort + "/SMIWeb/wsdl/");
                // Set JSESSIONID cookie using ExternalInterface and JavaScript.
                if (sessionId != null) 
                {
                    ExternalInterface.call("setJSessionId", "JSESSIONID", sessionId);
                }
            } 
            else 
            {
                ws = new WebServiceClient("SubjectArea", 
                                          "deleteCustomSubjectArea", 
                                          "CustomSubjectAreaModule", 
                                          "deleteCustomSubjectArea");
            }
            ws.setResultListener(processResults);
            ws.setFaultListener(handleFault);
            ws.showBusyCursor = true;
            ws.execute(subjectAreaName);
        }
    }
}