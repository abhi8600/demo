package com.birst.flex.webservice
{
	import com.birst.flex.core.ConnectionErrorDialogBox;
	import com.birst.flex.core.MenuItemActions;
	import com.birst.flex.core.Util;
	
	import flash.utils.*;
	
	import mx.controls.Alert;
	import mx.core.Application;
	import mx.events.CloseEvent;
	import mx.managers.CursorManager;
	import mx.resources.IResourceManager;
	import mx.resources.ResourceManager;
	import mx.rpc.AsyncToken;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	import mx.rpc.soap.Operation;
	import mx.rpc.soap.SOAPFault;
        
    [ResourceBundle('sharable')]    
	public class WebServiceClient {
    
        [Bindable]
        public static var debugTiming:String;
        public var showBusyCursor:Boolean = true;
       
        public var arguments:Object = null;

        protected static var _result:ResultEvent = null;
    	protected var _ws:BirstWebService = null;
    	protected var _webserviceMethod:String = '';
    	protected var _resultListener:Function = null;
    	protected var _faultListener:Function = null;

        private var WS_ROOT:String = "/SMIWeb/wsdl/";
    	private var _asyncToken:AsyncToken;
    	
    	private var _cancellable:Boolean = false;
    	    	
    	private var alertNoLabel:String;
    	private var errorMessage:String;
    	
    	private var asyncErrorResultListener:Function;
    	    	
		public function WebServiceClient(wsName:String, wsMethod:String, callerComp:String, callerOp:String, callerData:Object = null, rootOverride:String=null) {
			_ws = new BirstWebService();
			_ws.callerComponent = callerComp;
			_ws.callerOperation = callerOp;
			_ws.callerData = callerData;
			
			// XXX set headers?
			
			//Get any rootURL for smiweb passed as flash vars to swf file
            var flashVarRootURL:String = Util.getApplicationSMIWebURL();
            if (rootOverride != null) 
            {
                WS_ROOT = rootOverride;
            }
            else if(flashVarRootURL != null && flashVarRootURL.length > 0)
            {
            	WS_ROOT = flashVarRootURL + WS_ROOT;
            }
            
            var wsdl:String = WS_ROOT + wsName + '.wsdl';
            
            //Get application version for caching
            var version:String = Util.getApplicationVersion();
            if (version){
                wsdl += '?v=' + version;
            }
            
            _ws.wsdl = wsdl;
            _ws.port = wsName + 'HttpSoap11Endpoint';
            _webserviceMethod = wsMethod;
            _ws[_webserviceMethod].addEventListener("result", _resultHandler);
            _ws.addEventListener("fault", _faultHandler, false, 1000);
        }
    
		public function set wsdl(loc:String):void{
			_ws.wsdl = loc;	
		}
		
		public function get wsdl():String{
			return _ws.wsdl;	
		}
		
		public function set cancellable(isCancellable:Boolean):void{
			_cancellable = isCancellable;
			showBusyCursor = !isCancellable;
		}
		
		protected function _resultHandler(event:ResultEvent):void{
			if (showBusyCursor)
				CursorManager.removeBusyCursor();
				
			var operation:Operation = event.currentTarget as Operation;
			
			var service:BirstWebService = operation.service as BirstWebService;
		
			//Debug timing info
			if (Application.application.hasOwnProperty("debugMode") && Application.application.debugMode == true){
				debugTiming = 'Webservice:' + operation.name + ' ' + service.callerComponent + ':' + service.callerOperation + ' elapsed time: ' + (getTimer() - service.startTime) + 'ms';
			}
			
           	_result = event;
           	var result:WebServiceResult = WebServiceResult.parse(event);
           	if(result.getErrorCode() == -4) {
           		if (asyncErrorResultListener != null) {
           			asyncErrorResultListener(event);
           		}
           		else {
	           		var idToPoll:String = result.getPollingId();
	           		RequestManager.getInstance().addAsynchJobPolling(idToPoll, operation);
           		}
           	}else{
           		service.notifyListeners(operation.name, event);
           	}
        }
        
        protected function _faultHandler(event:FaultEvent):void{
        	if (showBusyCursor)
        		CursorManager.removeBusyCursor();
        	
        	_result = null; 
        	
        	errorMessage = 'WEBSERVICE:\n' + _ws.port + '.' + this._webserviceMethod;
        	
        	if (event.fault is SOAPFault){
                var fault:SOAPFault = event.fault as SOAPFault;
                var faultElement:XML = fault.element;
                errorMessage += 
                	'\n\nSOAP FAULT:\n' + checkNull(faultElement.toXMLString()) + '\n\nFAULT CONTENT:\n' + checkNull(event.fault.content);
         	}else{
         		if(absorbError(event.fault.message)){
         			return;
         		}
         		errorMessage += '\n\nFAULT MESSAGE:\n' + checkNull(event.fault.message) + '\n\nFAULT CONTENT:\n' + checkNull(event.fault.content);
         	}
         	
         	alertNoLabel = Alert.noLabel;
         	
         	var mgr:IResourceManager = ResourceManager.getInstance();
         	var message:String = mgr.getString('sharable', 'WC_failCon');
         	var title:String = mgr.getString('errors', 'AL_title');
         	Alert.noLabel = mgr.getString('sharable', 'WC_detail');
         	Alert.okLabel=mgr.getString('sharable', 'WC_okay');
         	Alert.show(message, title, Alert.NO | Alert.OK , null, faultAlertCloseFunc);
        }
        
		// Silently absorb error
		private function absorbError(faultMessage:String):Boolean{
			// When a user tries to rapdily move from one Admin to Designer
			// browser cancels the event and ends up throwing the error with Error #2032
			// e.g. faultCode:Server.Error.Request faultString:'HTTP request error' faultDetail:'Error: [IOErrorEvent type="ioError" bubbles=false cancelable=false eventPhase=2 text="Error #2032: .....
			if(faultMessage != null && faultMessage.indexOf('HTTP request error') >=0 && 
				faultMessage.indexOf('Error #2032') >=0){
					return true;
				} 
			return false;
		}	
        
        private function checkNull(str:Object):Object{
        	return (str) ? str : '';	
        }
        
        private function faultAlertCloseFunc(event:CloseEvent):void{
        	if(event.detail == Alert.NO){
        		var popup:ConnectionErrorDialogBox = new ConnectionErrorDialogBox;
        		popup.styleName = "connectionErrorDialogBoxStyle";
        		popup.errorMessageText = errorMessage;
        		MenuItemActions.popUp(null, {popUp: popup});	
        	}
        	Alert.noLabel = alertNoLabel;
        }
        
        public function getWSObject():BirstWebService{
        	return _ws;	
        }
        
        public function execute(...args:Array):void{
        	_ws.loadWSDL();

			var op:Operation = _ws.getOperation(_webserviceMethod) as Operation;
				
			if (op){
				if (showBusyCursor)
					CursorManager.setBusyCursor();
			
				var header:Object = new Object();
            	header["X-XSRF-TOKEN"] = Application.application.xsrfToken;
            	_ws.httpHeaders = header;
				
				if (args != null && args.length > 0) {
					op.arguments = args;
				}else{
					if (arguments != null)
						op.arguments = arguments;
				}
					
				op.resultFormat = "e4x";
				
				// don't do any special xml formatting 
				// for player 10 only bug	
				if(Util.isPlayer10Plus()){
					op.xmlSpecialCharsFilter = function escapeXML(value:Object):String	{	return value.toString(); };
				}else{
					op.xmlSpecialCharsFilter = function escapeXML(value:Object):String{
						var str:String = value.toString();
						str = str.replace(/&/g, "&amp;").replace(/</g, "&lt;");
						return str; 
					}
				}
				
				//Register to a global manager for cancellation if required
				if(_cancellable){
					RequestManager.getInstance().queueSend(op);
				}else{
					_asyncToken = op.send();
				}				
			}
        }
        
        /*
		public function execute(params:Array = null, useGlobalCache:Boolean = false):void{
			if (useGlobalCache && _result){
				_ws[_webserviceMethod].dispatchEvent(_result);
				return;
			}
			
			_ws.loadWSDL();
			var op:AbstractOperation = _ws.getOperation(_webserviceMethod);
			if (op){
				if (showBusyCursor)
					CursorManager.setBusyCursor();
					
					// XXX set headers?
					
				(params != null && params.length > 0) ? op.send(params) : op.send();
			}
		}*/
		
		public function setResultListener(func:Function):void{
			_ws.setEventListener(_webserviceMethod, func);
		}
		
		public function removeResultListener(func:Function):void{
			_ws.unsetEventListener(_webserviceMethod, func);
		}
		
		public function setFaultListener(func:Function):void{
			if (func != null){
				//safe checking - remove duplicate of this event listener handler
				_ws.removeEventListener(FaultEvent.FAULT, func);
				_ws.addEventListener(FaultEvent.FAULT, func);	
				_faultListener = func;
			}
		}		
		
		public function removeFaultListener(func:Function):void{
			_ws.removeEventListener(FaultEvent.FAULT, func);
		}
		
		public function setAsyncErrorResultListener(func:Function):void {
			asyncErrorResultListener = func;
		}
	}
}