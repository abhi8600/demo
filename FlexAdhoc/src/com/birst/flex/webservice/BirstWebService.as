package com.birst.flex.webservice
{
	import mx.rpc.events.ResultEvent;
	import mx.rpc.soap.WebService;

	public class BirstWebService extends WebService {
	
		//Which component invoked
		private var _component:String;
		
		//Which operation by the component
		private var _operation:String;
		
		//What data is passed along when the webservice is done
		private var _package:Object;
		
		private var _listeners:Object = {};
		
		//For timing
		public var startTime:int;
			
		public function BirstWebService(destination:String=null, rootURL:String=null)
		{
			super(destination, rootURL);
		}		
		
		public function get callerOperation():String{
			return _operation;
		}
		
		public function set callerOperation(op:String):void{
			_operation = op;	
		}
		
		public function get callerComponent():String{
			return _component;
		}
		
		public function set callerComponent(comp:String):void{
			_component = comp;
		}	
		
		public function set callerData(data:Object):void{
			_package = data;
		}
		
		public function get callerData():Object{
			return _package;
		}
		
		public function setEventListener(method:String, func:Function):void{
			if (func != null && method != null){
				//safe checking - remove duplicate installed event listener handler
				var listeners:Array = _listeners[method];
				if (!listeners) {
					listeners = [];
					_listeners[method] = listeners;
				}
				var found:Boolean = false;
				for (var i:int = 0; i < listeners.length; i++) {
					if (listeners[i] == func) {
						found = true;
						break;
					}
				}
				if (!found)
					listeners.push(func);
			}
		}
		
		public function unsetEventListener(method:String, func:Function):void {
			if (func != null && method != null) {
				var listeners:Array = _listeners[method];
				if (listeners) {
					for (var i:int = 0; i < listeners.length; i++) {
						if (listeners[i] == func) {
							listeners.splice(i, 1);
						}
					}
				}
			}
		}
		
		public function notifyListeners(method:String, event:ResultEvent):void {
			if (method != null) {
				var listeners:Array = _listeners[method];
				if (listeners) {
					for (var i:int = 0; i < listeners.length; i++) {
						(listeners[i] as Function).apply(null, [event]);
					}
				}
			}
		}
	}
}