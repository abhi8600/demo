package com.birst.flex.webservice
{
	import com.birst.flex.catalog.CatalogUtils;
	import com.birst.flex.catalog.IListing;
	import com.birst.flex.core.Tracer;
	
	import flash.external.ExternalInterface;
	
	public class FileWebService extends WebServiceClient
	{
		public function FileWebService()
		{
			super("File", "getFileList", "FileWebservice", "");
		}

        /**
         * Retrieves an "extended" view of the user's file system,
         * which includes file sizes and modification dates in addition
         * to the regular name and path information.
         * <p/>
         * If 'isStandalone' is true then it is assumed that 'wsPort', 'sessionId'
         * and 'processResults' are also set.  'isStandalone' indicates that the
         * developer wants to test the Flex app outside the Birst browser window
         * so he/she must provide an absolute WSDL URL and an SMIWeb JSESSIONID.
         */ 
        public static function getExtendedFileSystem(processResults:Function,
                                                     isStandalone:Boolean=false,
                                                     wsPort:String=null,
                                                     sessionId:String=null,
                                                     hostName:String=null):void 
        {
            // If testing w/localhost, override root URL
            var ws:WebServiceClient;
            if (isStandalone) 
            {
            	Tracer.debug("'getExtendedVisibleFilesystem() using hostName: " + hostName
            	    + "; wsPort: " + wsPort + "; sessionId: " + sessionId);
                // Need an absolute URL for standalone testing
                ws = new WebServiceClient("File", 
                                          "getExtendedVisibleFilesystem", 
                                          "CatalogManagement", 
                                          "getExtendedFileSystem",
                                           null, 
                                          "http://" + hostName + ":" + wsPort + "/SMIWeb/wsdl/");
                // Set JSESSIONID cookie using ExternalInterface and JavaScript.
                if (sessionId != null) 
                {
                    ExternalInterface.call("setJSessionId", "JSESSIONID", sessionId);
                }
            } 
            else 
            {
                ws = new WebServiceClient("File", 
                                          "getExtendedVisibleFilesystem", 
                                          "CatalogManagement", 
                                          "getExtendedFileSystem");
            }
            ws.setResultListener(processResults);
            ws.showBusyCursor = true;
            ws.execute(true);
        }

        public static function processRenameFileRequest(newFileName:String, 
                                                        listing:IListing,
                                                        processResults:Function,
                                                        isStandalone:Boolean=false,
                                                        wsPort:String=null,
                                                        sessionId:String=null,
                                                        hostName:String=null):void 
        {
            // If testing standalone
            var ws:WebServiceClient;
            if (isStandalone) 
            {
                ws = new WebServiceClient("File", 
                                          "renameFile", 
                                          "CatalogManagement", 
                                          "processRenameFileRequest", 
                                           null,
                                          "http://" + hostName + ":" + wsPort + "/SMIWeb/wsdl/");
                // Set JSESSIONID cookie using ExternalInterface and JavaScript.
                if (sessionId != null) {
                    ExternalInterface.call("setJSessionId", "JSESSIONID", sessionId);
                }
            } 
            else 
            {
                ws = new WebServiceClient("File", 
                                          "renameFile", 
                                          "CatalogManagement", 
                                          "processRenameFileRequest");
            }
            ws.setResultListener(processResults);
            if (listing.isDirectory()) 
            {
                ws.execute(newFileName, listing.path);
            } 
            else 
            {
            	var extension:String = "." + listing.filetype;
            	var index:int = listing.path.lastIndexOf(".");
            	if (index > 0) {
            		var oldExtension:String = listing.path.substring(index + 1);
            		if (oldExtension.toLowerCase() == "dashlet")
            			extension = extension + ".dashlet";
            	}
                ws.execute(newFileName + extension, listing.path);
            }
        }

        public static function processDeleteFileRequest(listings:Array,
                                                        processResults:Function,
                                                        isStandalone:Boolean=false,
                                                        wsPort:String=null,
                                                        sessionId:String=null,
                                                        hostName:String=null):void 
        {
            Tracer.debug("Entered 'processDeleteFileRequest()' function.");
            for (var i:int = 0; i < listings.length; i++) {
            	Tracer.debug("...listing #" + i + ": " + listings[i]);
            }
            // If testing standalone
            var ws:WebServiceClient;
            if (isStandalone) 
            {
                ws = new WebServiceClient("File", 
                                          "deleteFiles", 
                                          "CatalogManagement", 
                                          "processDeleteFileRequest", 
                                           null,
                                          "http://" + hostName + ":" + wsPort + "/SMIWeb/wsdl/");
                // Set JSESSIONID cookie using ExternalInterface and JavaScript.
                if (sessionId != null) 
                {
                    ExternalInterface.call("setJSessionId", "JSESSIONID", sessionId);
                }
            } 
            else 
            {
                ws = new WebServiceClient("File", 
                                          "deleteFiles", 
                                          "CatalogManagement", 
                                          "processDeleteFileRequest");
            }
            ws.setResultListener(processResults);
            var filePaths:Array = CatalogUtils.extractPathsFromListings(listings); 
            ws.execute(filePaths);
        }

        public static function processCreateFolderRequest(newFolderName:String, 
                                                          listing:IListing,
                                                          processResults:Function,
                                                          isStandalone:Boolean=false,
                                                          wsPort:String=null,
                                                          sessionId:String=null,
                                                          hostName:String=null):void 
        {
            // If testing standalone
            var ws:WebServiceClient;
            if (isStandalone) 
            {
                ws = new WebServiceClient("File", 
                                          "createDirectory", 
                                          "CatalogManagement", 
                                          "processCreateFolderRequest", 
                                           null,
                                          "http://" + hostName + ":" + wsPort + "/SMIWeb/wsdl/");
                // Set JSESSIONID cookie using ExternalInterface and JavaScript.
                if (sessionId != null) 
                {
                    ExternalInterface.call("setJSessionId", "JSESSIONID", sessionId);
                }
            } 
            else 
            {
                ws = new WebServiceClient("File", 
                                          "createDirectory", 
                                          "CatalogManagement", 
                                          "processCreateFolderRequest");
            }
            ws.setResultListener(processResults);
            ws.execute(newFolderName, listing.path);
        }

        /**
         * Retrieves the group permissions for the specified folder.
         * <p/>
         * If 'isStandalone' is true then it is assumed that 'wsPort', 'sessionId'
         * and 'processResults' are also set.  'isStandalone' indicates that the
         * developer wants to test the Flex app outside the Birst browser window
         * so he/she must provide an absolute WSDL URL and an SMIWeb JSESSIONID.
         */ 
        public static function getFolderPermissions(folder:String,
                                                    processResults:Function,
                                                    isStandalone:Boolean=false,
                                                    wsPort:String=null,
                                                    sessionId:String=null,
                                                    hostName:String=null):void 
        {
            // If testing w/localhost, override root URL
            var ws:WebServiceClient;
            if (isStandalone) 
            {
                Tracer.debug("getFolderPermissions() using: hostName: " + hostName 
                    + "; wsPort: " + wsPort + "; sessionId: " +  sessionId);
                // Need an absolute URL for standalone testing
                ws = new WebServiceClient("File", 
                                          "getPermissions", 
                                          "CatalogManagement", 
                                          "getFolderPermissions",
                                           null, 
                                          "http://" + hostName + ":" + wsPort + "/SMIWeb/wsdl/");
                // Set JSESSIONID cookie using ExternalInterface and JavaScript.
                if (sessionId != null) 
                {
                    ExternalInterface.call("setJSessionId", "JSESSIONID", sessionId);
                }
            } 
            else 
            {
                ws = new WebServiceClient("File", 
                                          "getPermissions", 
                                          "CatalogManagement", 
                                          "getFolderPermissions");
            }
            ws.setResultListener(processResults);
            ws.showBusyCursor = true;
            ws.execute(folder);
        }

        /**
         * Saves the group permissions for the specified folder.
         * <p/>
         * If 'isStandalone' is true then it is assumed that 'wsPort', 'sessionId'
         * and 'processResults' are also set.  'isStandalone' indicates that the
         * developer wants to test the Flex app outside the Birst browser window
         * so he/she must provide an absolute WSDL URL and an SMIWeb JSESSIONID.
         * @param permissions is a String-ified (i.e., toXMLString()) version of 
         * the XMLList used in the FolderPermissionsDialog.
         */ 
        public static function saveFolderPermissions(folder:String,
                                                     permissions:String,
                                                     processResults:Function,
                                                     isStandalone:Boolean=false,
                                                     wsPort:String=null,
                                                     sessionId:String=null,
                                                     hostName:String=null):void 
        {
            // If testing w/localhost, override root URL
            var ws:WebServiceClient;
            if (isStandalone) 
            {
                Tracer.debug("saveFolderPermissions() using: hostName: " + hostName 
                    + "; wsPort: " + wsPort + "; sessionId: " +  sessionId);
                // Need an absolute URL for standalone testing
                ws = new WebServiceClient("File", 
                                          "savePermissions", 
                                          "CatalogManagement", 
                                          "saveFolderPermissions",
                                           null, 
                                          "http://" + hostName + ":" + wsPort + "/SMIWeb/wsdl/");
                // Set JSESSIONID cookie using ExternalInterface and JavaScript.
                if (sessionId != null) 
                {
                    ExternalInterface.call("setJSessionId", "JSESSIONID", sessionId);
                }
            } 
            else 
            {
                ws = new WebServiceClient("File", 
                                          "savePermissions", 
                                          "CatalogManagement", 
                                          "saveFolderPermissions");
            }
            ws.setResultListener(processResults);
            ws.showBusyCursor = true;
            ws.execute(folder, permissions);
        }

        /**
         * Formats and sends the move/copy webservice request.
         * Will interject a full URL for the case where the developer is performing 
         * standalone testing (normally in Flex Builder).  Otherwise it forwards the
         * request with a URL relative to the web page's root.
         * The standalone testing also requires that the JSESSIONID cookie information
         * be provided on the request.  In order to provide the cookie, this method 
         * uses the Flash ExternalInterface to call a JavaScript function that will
         * set the cookie in the browser. 
         * <p/>
         * This method is pretty specific for CatalogManagement, but if some other 
         * application/component wanted to use it *and* wanted to perform standalone
         * testing, then the other application should provide:
         * <ul>
         * <li>standaloneTesting - boolean property indicating whether or not to perform standalone testing
         * <li>wsPort - an integer property to provide an alternate listener port (e.g., a TCPMon proxy) 
         * for requests
         * <li>sessionId - a alphanumeric property that provides the currently active JSESSIONID from SMIWeb
         * <li>getRootDirs - a method to process the results of the move/copy request
         * (basically the getExtendedVisibleFilesystem webservice with pertinent error 
         * codes and messages set).
         * </ul>
         */
        public static function processMoveCopyFileRequest(isCopy:Boolean, 
                                                          sourcePaths:Array, 
                                                          targetDirectory:String,
                                                          processResults:Function,
                                                          isStandalone:Boolean=false,
                                                          wsPort:String=null,
                                                          sessionId:String=null,
                                                          hostName:String=null):void 
        {
            // Is Copy or Move request?
            var action:String;
            if (isCopy) 
            {
                action = "copyFiles";
            } 
            else 
            {
                action = "moveFiles";
            }
            // If testing standalone
            var ws:WebServiceClient;
            if (isStandalone) 
            {
                Tracer.debug("'processMoveCopyFileRequest() using hostName: " + hostName
                    + "; wsPort: " + wsPort + "; sessionId: " + sessionId);
                ws = new WebServiceClient("File", 
                                          action, 
                                          "CatalogManagement", 
                                          "processMoveCopyFileRequest",
                                           null, 
                                          "http://" + hostName + ":" + wsPort + "/SMIWeb/wsdl/");
                // Set JSESSIONID cookie using ExternalInterface and JavaScript.
                if (sessionId != null) {
                    ExternalInterface.call("setJSessionId", "JSESSIONID", sessionId);
                }
            } 
            else 
            {
                ws = new WebServiceClient("File", 
                                          action, 
                                          "CatalogManagement", 
                                          "processMoveCopyFileRequest");
            }
            ws.setResultListener(processResults);
            ws.showBusyCursor = true;
            ws.execute(sourcePaths, targetDirectory);
        }

        /**
         * TODO: Should do break up into quanta of uploads here or in FileUploadDialog?
         */
        public static function processFileUploadRequest(uploadFolder:String,
                                                        filename:String, 
                                                        digest:String, 
                                                        encodedContents:String,
                                                        processResults:Function,
                                                        isStandalone:Boolean=false,
                                                        wsPort:String=null,
                                                        sessionId:String=null,
                                                        hostName:String=null):void 
        {
            // If testing standalone
            var ws:WebServiceClient;
            if (isStandalone) 
            {
                Tracer.debug("'processFileUploadRequest() using hostName: " + hostName
                    + "; wsPort: " + wsPort + "; sessionId: " + sessionId);
                ws = new WebServiceClient("File", 
                                          "uploadFile", 
                                          "FileUploadDialog", 
                                          "processFileUploadRequest",
                                           null, 
                                          "http://" + hostName + ":" + wsPort + "/SMIWeb/wsdl/");
                // Set JSESSIONID cookie using ExternalInterface and JavaScript.
                if (sessionId != null) {
                    ExternalInterface.call("setJSessionId", "JSESSIONID", sessionId);
                }
            } 
            else 
            {
                ws = new WebServiceClient("File", 
                                          "uploadFile", 
                                          "FileUploadDialog", 
                                          "processFileUploadRequest");
            }
            ws.setResultListener(processResults);
            ws.showBusyCursor = true;
            ws.execute(uploadFolder, filename, digest, encodedContents);
        }

        public static function processFileDownloadRequest(relativeFilePath:String,
                                                          processResults:Function,
                                                          isStandalone:Boolean=false,
                                                          wsPort:String=null,
                                                          sessionId:String=null,
                                                          hostName:String=null):void 
        {
            // If testing standalone
            var ws:WebServiceClient;
            if (isStandalone) 
            {
                Tracer.debug("'processFileDownloadRequest() using hostName: " + hostName
                    + "; wsPort: " + wsPort + "; sessionId: " + sessionId);
                ws = new WebServiceClient("File", 
                                          "downloadFile", 
                                          "ContextMenuEventManager", 
                                          "processFileDownloadRequest",
                                           null, 
                                          "http://" + hostName + ":" + wsPort + "/SMIWeb/wsdl/");
                // Set JSESSIONID cookie using ExternalInterface and JavaScript.
                if (sessionId != null) {
                    ExternalInterface.call("setJSessionId", "JSESSIONID", sessionId);
                }
            } 
            else 
            {
                ws = new WebServiceClient("File", 
                                          "downloadFile", 
                                          "ContextMenuEventManager", 
                                          "processFileDownloadRequest");
            }
            ws.setResultListener(processResults);
            ws.showBusyCursor = true;
            ws.execute(relativeFilePath);
        }
	}
}