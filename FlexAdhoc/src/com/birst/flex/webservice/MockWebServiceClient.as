package com.birst.flex.webservice
{
	import mx.core.Application;
	import mx.rpc.events.ResultEvent;
	
	public class MockWebServiceClient extends WebServiceClient
	{
		private var _result:Object;
		private var _parent:Application;
		
		public function MockWebServiceClient(wsName:String, wsMethod:String, callerComp:String, callerOp:String) {
			super(wsName, wsMethod, callerComp, callerOp);
		}
		
        public override function execute(...args:Array):void {
        	var event:ResultEvent = new ResultEvent("Mocking", false, true, _result);
        	dispatchEvent(event);
	    }
	    
	    public function set result(result:Object):void {
	    	this._result = result;
	    }
	}
}