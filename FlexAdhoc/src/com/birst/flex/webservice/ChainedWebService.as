package com.birst.flex.webservice
{
	import mx.rpc.events.ResultEvent;
	
	public class ChainedWebService
	{
		private var _caller:String;
		private var _operation:String;
		
		private var _services:Array = [];
		
		public function ChainedWebService(callerComp:String, callerOp:String)
		{
			_caller = callerComp;
			_operation = callerOp;
			_isFirstWS = true;
		}
		
		public function addService(wsName:String, wsMethod:String, resultHandler:Function, args:Object, faultHandler:Function = null){
			var ws:WebServiceClient = new WebServiceClient(wsName, wsMethod, _caller, _operation);
			
			//attach our result event handler to call our user's result handler and call the next in chain
			ws.setResultListener(function(result:ResultEvent):void{
				var success:Boolean = resultHandler(result);		
				if (success == true){
					var nextWS:WebServiceClient = _services.pop();
					if (nextWS){
						nextWS.execute();
					}
				}
			});
			
			if (faultHandler)
				ws.setFaultListener(faultHandler);
		
			ws.arguments = args;
			
			_services.push(ws);
		}
		
		
		public function start():void{
			var ws:WebServiceClient = _services.pop();
			if (ws){
				ws.execute();
			}			
		}	
	}
}