package com.birst.flex.webservice
{
	import flash.utils.Proxy;
	
	public class WebServiceFactory extends Proxy
	{
		public static const instance:WebServiceFactory = new WebServiceFactory(WSLock);
		
		private var _webservices:Array = [];
		
		public function WebServiceFactory(lock:Class)
		{	
			if (lock != WSLock){
				throw new Error("Invalid instance creation.");
			}
		}

		override flash_proxy function getProperty(name:*):*{
			var wsName:String = name.toString();
			
			if (_webservices[wsName])
				return _webservices[wsName];
				
			//create 
		}
	}
	
}
internal class WSLock{}