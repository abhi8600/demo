package com.birst.flex.webservice
{
	public class FontFamilyWebService extends WebServiceClient
	{
		public function FontFamilyWebService(){
			super("ClientInitData", "getFontFamilies", Components.FONT_FAMILY_WS, 'constructor');
		}
	}
}