package com.birst.flex.webservice
{
	import flash.events.Event;

	public class RequestEvent extends Event
	{
		public static var NEW_WEBSERVICE_OP:String = "newWebServiceOperation";
		public static var REMOVE_WEBSERVICE_OP:String = "removeWebServiceOperation";
		public static var NO_MORE_WEBSERVICE_OP:String = "noMOreWebserviceOperations";
		
		public function RequestEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
		
	}
}