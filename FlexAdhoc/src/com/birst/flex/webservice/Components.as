package com.birst.flex.webservice
{
	public class Components
	{
		public static var TOP_MENU_BAR:String = "TopMenuBar";
		public static var ADHOC_APP:String = "AdhocApp";
		public static var COLUMN_PROPS_DBOX:String = "ColumnPropsDBox";
		public static var CONDITIONAL_FORMAT_DBOX:String = "ConditionalFormatDBox";
		public static var COLUMN_SELECTOR_DBOX:String = "ColumnSelectorDBox";
		public static var FILTER_COLUMN_DBOX:String = "FilterColumnDBox";
		public static var FONT_FAMILY_WS:String = "FontFamilyWS";
		public static var FILE_DBOX:String = "FileDBox";
		public static var SUBJECT_AREA_WS:String = "SubjectAreaWS";
		public static var LAYOUT_MOD:String = "LayoutMod";
		public static var SAVED_EXPRESSIONS:String = "SavedExpressions";
		public static var LINK_NAVIGATOR:String = "LinkNavigator";
		public static var IMAGE_PROPERTIES_DIALOG:String = "ImagePropertiesDialog";
		public static var ADVANCED_FILTER_DIALOG:String = "AdvancedFilterDialog";
		
		public function Components()
		{
		}

	}
}