package com.birst.flex.webservice
{
	import com.birst.flex.core.Tracer;
	import com.birst.flex.core.Util;
	
	import flash.events.TimerEvent;
	import flash.utils.Dictionary;
	import flash.utils.Timer;
	
	import mx.core.Application;
	import mx.core.UIComponent;
	import mx.rpc.AsyncToken;
	import mx.rpc.Responder;
	import mx.rpc.events.ResultEvent;
	import mx.rpc.soap.Operation;
	
	public class RequestManager
	{
		
		private static var _mgr:RequestManager;
		
		//Most modern browsers have maximum 6 persistent connections
		//We leave one connection available for cancelling requests.
		private var _maxActiveRequests:int = 5;
		
		private var _activeCalls:Array;
		private var _pendingCalls:Array;
		
		private var _operations:Dictionary;
		private var _timers:Dictionary;
		private var _asyncRequests:Dictionary;
		
		//Where do we dispatch from..
		private var _component:UIComponent;
		
		public function RequestManager(enforcer:Enforcer) {	
			_activeCalls = new Array();
			_pendingCalls = new Array();
			
			_operations = new Dictionary();
			_asyncRequests = new Dictionary();
			var timer:Timer = new Timer(3000);
			timer.addEventListener(TimerEvent.TIMER, asynchPoll); 
			timer.start();
			
			//IE 7, 6 only have maximum 2 persistent connections.
			if(Application.application.hasOwnProperty('isIE7Below') && Application.application.isIE7Below == true){
				_maxActiveRequests = 1;	
			}
			
			_component = Application.application as UIComponent;
		}
		
		public static function getInstance():RequestManager {
			if(!_mgr){
				_mgr = new RequestManager(new Enforcer());
			}
			
			return _mgr;
		}
		
		public function set eventDispatcher(component:UIComponent):void{
			_component = component;	
		}
		
		public function cancelAllOperations():void{

			//Remove all pending calls
			if(_pendingCalls.length > 0){
				_pendingCalls.splice(0, _pendingCalls.length);
			}
			
			//Issue request to server to cancel query
			var ws:WebServiceClient = new WebServiceClient('Adhoc', 'cancelReportRendering', '', '');
			ws.setResultListener(onCancelReportRendering);
			ws.execute();
		}
		
		public function onCancelReportRendering(event:ResultEvent):void{
			var result:WebServiceResult = WebServiceResult.parse(event);
			if(!result.isSuccess()){
				Util.alertErrMessage("Unable to cancel report rendering", result);
				return;
			}
		}
		
		public function queueSend(operation:Operation):void{
			if (_activeCalls.length < _maxActiveRequests){
				addToActiveQueue(operation);	
			}else{
				Tracer.debug('adding to pending queue: ' + operation.name, this);
				_pendingCalls.push(operation);
			}
			
			_component.dispatchEvent(new RequestEvent(RequestEvent.NEW_WEBSERVICE_OP));
		}	
		
		private function addToActiveQueue(operation:Operation):void{
			Tracer.debug('adding to active queue: ' + operation.name, this);
			_activeCalls.push(operation);
			Tracer.debug('active queue length: ' + _activeCalls.length, this);
			var token:AsyncToken = operation.send();
			token.operation = operation;
			token.addResponder(new Responder(removeOperation, removeOperation));	
		}
		
		private function removeOperation(evt:Object):void{
			for(var i:int = 0; i < _activeCalls.length; i++){
				if (_activeCalls[i] == evt.token.operation){
					Tracer.debug('removing operation ' + evt.token.operation.name, this);
					_activeCalls.splice(i, 1);
					if (_pendingCalls.length > 0){
						addToActiveQueue(_pendingCalls.shift());
					}
					break;
				}
			}
			
			//Done
			if(_activeCalls.length == 0 && _pendingCalls.length == 0){
				_component.dispatchEvent(new RequestEvent(RequestEvent.NO_MORE_WEBSERVICE_OP));
			}	
		}
		
		public function addAsynchJobPolling(id:String, o:Object):void {
			_asyncRequests[id] = o;
		}
		
		private function removeAsynchJobPolling(id:String):void {
			delete _asyncRequests[id];
		}
		
		private function asynchPoll(e:TimerEvent):void {
			var list:Array = [];
			for (var keys:* in _asyncRequests)
				list.push(keys);
			if (list.length > 0) {
				var ws:WebServiceClient = new WebServiceClient('Adhoc', 'isReportRenderJobComplete', 'RequestManager', 'AsynchPoll');
				ws.setResultListener(jobsComplete);
				ws.showBusyCursor = false;
				ws.execute(list);
			}
		}
		
		private function jobsComplete(event:ResultEvent):void {
           	var result:WebServiceResult = WebServiceResult.parse(event);
           	if (result.isSuccess()) {
           		var ret:XMLList = result.getResult();
           		var list:XMLList = ret..Finished.AsyncJob;
           		for (var i:int = 0; i < list.length(); i++) {
           			var job:XML = list[i];
           			var id:String = job.@Id;
           			if (String(job.@InvalidId) == 'true') {
           				removeAsynchJobPolling(id);
           			}
           			else {
           				var isComplete:Boolean = (String(job.@IsComplete) == 'true');
           				if (isComplete) {
           					if (_asyncRequests[id] && _asyncRequests[id] is Operation) {
           						(_asyncRequests[id] as Operation).send();
           					}
           					removeAsynchJobPolling(id);
           				}
           			}
           		}
           	}
		}
	}
}

class Enforcer{
	public function Enforcer(){}
}	
	