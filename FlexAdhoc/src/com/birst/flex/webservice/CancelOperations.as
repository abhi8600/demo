package com.birst.flex.webservice
{
	import com.birst.flex.core.CancelDialogBox;
	
	import flash.display.DisplayObject;
	
	import mx.core.Application;
	import mx.core.UIComponent;
	import mx.events.CloseEvent;
	import mx.managers.PopUpManager;
	
	public class CancelOperations
	{
		private static var _instance:CancelOperations;
		
		private var _consumer:UIComponent;
		private var _popup:CancelDialogBox;
		private var _isPopUpOnDisplay:Boolean;
		
		public function CancelOperations(enforcer:Enforcer){
			_consumer = Application.application as UIComponent;
			_isPopUpOnDisplay = false;
		}
		
		public static function getInstance():CancelOperations{
			if(!_instance){
				_instance = new CancelOperations(new Enforcer());
			}
			return _instance;
		}
		
		public function set eventConsumer(component:UIComponent):void{
			removeEventListeners();
			_consumer = component;	
			addEventListeners();	
		}
		
		protected function removeEventListeners():void{
			if(_consumer){
				_consumer.removeEventListener(RequestEvent.NEW_WEBSERVICE_OP, onNewWebService);
				_consumer.removeEventListener(RequestEvent.NO_MORE_WEBSERVICE_OP, onNoMoreWebService);
			}
		}
		
		protected function addEventListeners():void{
			if(_consumer){
				_consumer.addEventListener(RequestEvent.NEW_WEBSERVICE_OP, onNewWebService);
				_consumer.addEventListener(RequestEvent.NO_MORE_WEBSERVICE_OP, onNoMoreWebService);
			}
		}
		
		protected function onNewWebService(event:RequestEvent):void{
			if(!_isPopUpOnDisplay){
				var popup:CancelDialogBox = createPopup();
				PopUpManager.addPopUp(popup, _consumer as DisplayObject);
				PopUpManager.centerPopUp(popup);
				_isPopUpOnDisplay = true;
			}	
		}
		
		protected function onNoMoreWebService(event:RequestEvent):void{
			if(_isPopUpOnDisplay){
				if(_popup){
					PopUpManager.removePopUp(_popup);
					_isPopUpOnDisplay = false;
				}
			}
		}
		
		protected function createPopup():CancelDialogBox{
			if(!_popup){
				_popup = new CancelDialogBox();
				_popup.addEventListener(CloseEvent.CLOSE, onClosePopup);
			}
			
			return _popup;
		}
		
		protected function onClosePopup(event:CloseEvent):void{
			_isPopUpOnDisplay = false;
		}
	}
}

class Enforcer{
	public function Enforcer(){}
}	