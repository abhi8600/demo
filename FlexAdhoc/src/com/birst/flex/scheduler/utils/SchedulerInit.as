package com.birst.flex.scheduler.utils
{
	import com.birst.flex.core.Util;
	import com.birst.flex.scheduler.shared.EmailDetails;
	import com.birst.flex.webservice.WebServiceClient;
	import com.birst.flex.webservice.WebServiceResult;
	
	import mx.rpc.events.ResultEvent;
	
	public class SchedulerInit
	{
		
		private var scheduleFrom:String;
		private var scheduleSubject:String;
		private static var _instance:SchedulerInit;
		
		public function SchedulerInit()
		{
		}
		
		public static function instance():SchedulerInit{
			if(_instance == null){
				_instance = new SchedulerInit();
			}
			return _instance;
		}

		
		public function getInitProps():void{
			var webServiceClient:WebServiceClient = new WebServiceClient("SchedulerAdmin", "getInitProps", "JobSummary", "getInitProps", null, SchedulerUtils.getSchedulerRootUrl());
			webServiceClient.setResultListener(initPropsListener);
			webServiceClient.execute();
		}
		
		private function initPropsListener(event:ResultEvent):void
		{
			var wsResult:WebServiceResult = WebServiceResult.parse(event);
			if(!wsResult.isSuccess())
			{	
				Util.alertErrMessage(SchedulerUtils.getSchedulerBundbleLocaleString('SC_ERR_INIT_PROPS', ['Report']));
				return;
			}
			
			var initPropsRoot:XMLList = wsResult.getResult().InitProperties;
			if(initPropsRoot != null && initPropsRoot.length() > 0)
			{
				var scheduleFromRoot:XMLList = initPropsRoot[0].scheduleFrom;
				if(scheduleFromRoot !=null && scheduleFromRoot.length() > 0)
				{
					scheduleFrom = scheduleFromRoot[0].toString();
				}
				
				var scheduleSubjectRoot:XMLList = initPropsRoot[0].scheduleSubject;
				if(scheduleSubjectRoot !=null && scheduleSubjectRoot.length() > 0)
				{
					scheduleSubject = scheduleSubjectRoot[0].toString();
				}
			}
		}
		
		public function getDefaultEmailProps():EmailDetails{
			var defaultEmailProps:EmailDetails = new EmailDetails();
			defaultEmailProps.From = this.scheduleFrom;
			defaultEmailProps.Subject = this.scheduleSubject;
			return defaultEmailProps;
		}
	}
}