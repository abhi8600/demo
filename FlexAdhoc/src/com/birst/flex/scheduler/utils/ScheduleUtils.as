package com.birst.flex.scheduler.utils
{
	import com.birst.flex.scheduler.shared.ScheduleCronExpression;
		
	public class ScheduleUtils
	{
		public static const CDATA_OPEN:String = "<![CDATA[";
	    public static const CDATA_CLOSE:String = "]]>";
	    
		public static const INTERVAL_DAILY:String = "Daily";
		public static const INTERVAL_WEEKLY:String = "Weekly";
		public static const INTERVAL_MONTHLY:String = "Monthly";
		
		public static const INTERVAL_DAILY_LOCALIZED:String = SchedulerUtils.getSchedulerBundbleLocaleString('SC_LABEL_INTERVAL_DAILY');
		public static const INTERVAL_WEEKLY_LOCALIZED:String = SchedulerUtils.getSchedulerBundbleLocaleString('SC_LABEL_INTERVAL_WEEKLY');
		public static const INTERVAL_MONTHLY_LOCALIZED:String = SchedulerUtils.getSchedulerBundbleLocaleString('SC_LABEL_INTERVAL_MONTHLY');
	
		public static const DAY_SUNDAY:String = "Sunday";
		public static const DAY_MONDAY:String = "Monday";
		public static const DAY_TUESDAY:String = "Tuesday";
		public static const DAY_WEDNESDAY:String = "Wednesday";
		public static const DAY_THURSDAY:String = "Thursday";
		public static const DAY_FRIDAY:String = "Friday";
		public static const DAY_SATURDAY:String = "Saturday";
		
		public static const DAY_SUNDAY_LOCALIZED:String = SchedulerUtils.getSchedulerBundbleLocaleString('SC_LABEL_DAY_SUNDAY');
		public static const DAY_MONDAY_LOCALIZED:String = SchedulerUtils.getSchedulerBundbleLocaleString('SC_LABEL_DAY_MONDAY');
		public static const DAY_TUESDAY_LOCALIZED:String = SchedulerUtils.getSchedulerBundbleLocaleString('SC_LABEL_DAY_TUESDAY');
		public static const DAY_WEDNESDAY_LOCALIZED:String = SchedulerUtils.getSchedulerBundbleLocaleString('SC_LABEL_DAY_WEDNESDAY');
		public static const DAY_THURSDAY_LOCALIZED:String = SchedulerUtils.getSchedulerBundbleLocaleString('SC_LABEL_DAY_THURSDAY');
		public static const DAY_FRIDAY_LOCALIZED:String = SchedulerUtils.getSchedulerBundbleLocaleString('SC_LABEL_DAY_FRIDAY');
		public static const DAY_SATURDAY_LOCALIZED:String = SchedulerUtils.getSchedulerBundbleLocaleString('SC_LABEL_DAY_SATURDAY');
		
		public static const TIME_AM:String = "AM";
		public static const TIME_PM:String = "PM";
		
		public static const TIME_AM_LOCALIZED:String = SchedulerUtils.getSchedulerBundbleLocaleString('SC_DP_TIME_AM');
		public static const TIME_PM_LOCALIZED:String = SchedulerUtils.getSchedulerBundbleLocaleString('SC_DP_TIME_PM');
		
		public static const TIMEZONE_EST:String = "EST";
		public static const TIMEZONE_PST:String = "PST";
		public static const TIMEZONE_CST:String = "CST";
		public static const TIMEZONE_MST:String = "MST";
		
		public static const TIMEZONE_EST_LOCALIZED:String = SchedulerUtils.getSchedulerBundbleLocaleString('SC_DP_TMZONE_EST');
		public static const TIMEZONE_PST_LOCALIZED:String = SchedulerUtils.getSchedulerBundbleLocaleString('SC_DP_TMZONE_PST');
		public static const TIMEZONE_CST_LOCALIZED:String = SchedulerUtils.getSchedulerBundbleLocaleString('SC_DP_TMZONE_CST');
		public static const TIMEZONE_MST_LOCALIZED:String = SchedulerUtils.getSchedulerBundbleLocaleString('SC_DP_TMZONE_MST');
		
		public static const MONTH_LAST_DAY:String = "Last Day";
		public static const MONTH_LAST_DAY_LOCALIZED:String = SchedulerUtils.getSchedulerBundbleLocaleString('SC_MONTH_LAST_DAY');
		
	
		public function ScheduleUtils()
		{
		}
		
		public static function getInterval(interval:int):String
		{			
			if(interval == 0) { return INTERVAL_DAILY;}
			if(interval == 1) { return INTERVAL_WEEKLY;}
			if(interval == 2) { return INTERVAL_MONTHLY;}		
			return null;	
		}
		
		private static function getLocalizedDay(dayString:String):String
		{
			var response:String = dayString;
			switch (dayString)
			{
				case ScheduleUtils.DAY_SUNDAY:
					response = ScheduleUtils.DAY_SUNDAY_LOCALIZED;
					break;
				case ScheduleUtils.DAY_MONDAY:
					response = ScheduleUtils.DAY_MONDAY_LOCALIZED;
					break;
				case ScheduleUtils.DAY_TUESDAY:
					response = ScheduleUtils.DAY_TUESDAY_LOCALIZED;
					break;
				case ScheduleUtils.DAY_WEDNESDAY:
					response = ScheduleUtils.DAY_WEDNESDAY_LOCALIZED;
					break;
				case ScheduleUtils.DAY_THURSDAY:
					response = ScheduleUtils.DAY_THURSDAY_LOCALIZED;
					break;
				case ScheduleUtils.DAY_FRIDAY:
					response = ScheduleUtils.DAY_FRIDAY_LOCALIZED;
					break;
				case ScheduleUtils.DAY_SATURDAY:
					response = ScheduleUtils.DAY_SATURDAY_LOCALIZED;
					break;				
				default :
					break;		
			}
			return response;
		}
		
		public static function getDayOfWeek(day:int):String
		{
			if(day == 1) { return DAY_SUNDAY;}
			if(day == 2) { return DAY_MONDAY;}
			if(day == 3) { return DAY_TUESDAY;}
			if(day == 4) { return DAY_WEDNESDAY;}
			if(day == 5) { return DAY_THURSDAY;}
			if(day == 6) { return DAY_FRIDAY;}
			if(day == 7) { return DAY_SATURDAY;}
			return null;			
		}
		
		public static function getIndexIntWeekDay(dayString:String):int
		{
			if(dayString == DAY_SUNDAY) { return 1};
			if(dayString == DAY_MONDAY) { return 2};
			if(dayString == DAY_TUESDAY) { return 3};
			if(dayString == DAY_WEDNESDAY) { return 4};
			if(dayString == DAY_THURSDAY) { return 5};
			if(dayString == DAY_FRIDAY) { return 6};
			if(dayString == DAY_SATURDAY) { return 7};
			return -1;
		}
		
		public static function getLocalizedInterval(selectedIntervalValue:String):String
		{
			var response:String = selectedIntervalValue;
			switch (selectedIntervalValue)
			{
				case ScheduleUtils.INTERVAL_DAILY:
					response = ScheduleUtils.INTERVAL_DAILY_LOCALIZED;
					break;
				case ScheduleUtils.INTERVAL_WEEKLY:
					response = ScheduleUtils.INTERVAL_WEEKLY_LOCALIZED;
					break;
				case ScheduleUtils.INTERVAL_MONTHLY:
					response = ScheduleUtils.INTERVAL_MONTHLY_LOCALIZED;
					break;
				default :
					break;		
			}
			return response;	
		}
		
		public static function getHour(hour:int):int
		{			
			if (hour >= 12)
			{
				if (hour >= 13)
					return hour - 12;
				else
					return 12;
			}  
			else
			{
				if (hour == 0)
					return 12;
				else		
					return hour;
			}
		}
		
		public static function getAMPM(hour:int):String
		{
			if(hour >= 12)
			{
				return "PM";
			}		
			else
			{
				return "AM";
			}
		}
		
		public static function getDaySuffix(day:int):String
		{	
			var daySuffix:String = 'th';
			if(day != 11 && day != 12 && day != 13) 
			{
				if(day > 9) { day = day % 10; }
				if (day == 1) { daySuffix = 'st'; }
				if (day == 2) { daySuffix = 'nd'; }
				if (day == 3) { daySuffix = 'rd'; }
			}
			return daySuffix;
		}
		
		public static function getZeroPrefix(num:Number):String
		{
			return num.toString().length < 2 ? "0" + num : num.toString();
		}
		
		public static function formatDayOfMonth(dayOfMonth:String):String
		{			
			if(dayOfMonth == ScheduleCronExpression.VALUE_LAST_DAY_MONTH)
			{
				return ScheduleUtils.MONTH_LAST_DAY;
			}  
			
			return dayOfMonth +  ScheduleUtils.getDaySuffix(Number(dayOfMonth));
		}
		
		private static function getLocalizedTimeZoneString(timeZone:String):String
		{
			var response:String = timeZone;
			switch (timeZone)
			{
				case ScheduleUtils.TIMEZONE_EST:
					response = ScheduleUtils.TIMEZONE_EST_LOCALIZED;
					break;
				case ScheduleUtils.TIMEZONE_PST:
					response = ScheduleUtils.TIMEZONE_PST_LOCALIZED;
					break;
				case ScheduleUtils.TIMEZONE_CST:
					response = ScheduleUtils.TIMEZONE_CST_LOCALIZED;
					break;
				case ScheduleUtils.TIMEZONE_MST:
					response = ScheduleUtils.TIMEZONE_MST_LOCALIZED;
					break;	
				default :
					break;		
			}
			return response;	
		}
		
		private static function getLocalizedAMPM(amPmString:String):String
		{
			var response:String = amPmString;
			switch (amPmString)
			{
				case ScheduleUtils.TIME_AM:
					response = ScheduleUtils.TIME_AM_LOCALIZED;
					break;
				case ScheduleUtils.TIME_PM:
					response = ScheduleUtils.TIME_PM_LOCALIZED;
					break;					
				default :
					break;		
			}
			return response;
		}
		
		public static function formatScheduleCronExpression(si:ScheduleCronExpression, timeZone:String, ellipsisOnWeekdays:Boolean=false):String
		{
	    	var label:String = "";
	    	var replaceString:String = "REPLACEME";
	    	if(si != null)
	    	{
	    		var interval:String = si.Interval;
	    		var hour:int = si.Hour;
	    		var minute:int = si.Minute;
	    		var amPm:String = si.AmPm;
	    		label = replaceString + ", " + hour + ":" + ScheduleUtils.getZeroPrefix(minute) + " " + getLocalizedAMPM(amPm);
	    		if(timeZone != null)
	    		{
	    			label = label + " (" + getLocalizedTimeZoneString(timeZone) + ")";
	    		}
	    		if(interval == ScheduleUtils.INTERVAL_DAILY)
	    		{
	    			label = label.replace(replaceString, getLocalizedInterval(ScheduleUtils.INTERVAL_DAILY));  
	    		} 
	    		else if(interval == ScheduleUtils.INTERVAL_WEEKLY)    		
	    		{
	    			//label = label + ScheduleUtils.INTERVAL_WEEKLY + ", " + ScheduleUtils.getDayOfWeek(Number(si.DayOfWeek)); 
	    			label = label.replace(replaceString, getLocalizedInterval(ScheduleUtils.INTERVAL_WEEKLY))  ;
	    			var arr:Array = si.DaysOfWeek;
	    			if(arr != null && arr.length > 0)
	    			{
	    				for (var i:int=0; i<arr.length; i++)
	    				{	    					
	    					if(ellipsisOnWeekdays && i > 0)
	    					{
	    						label = label + "...";
	    						break;
	    					}
	    					label = label + ", " + getLocalizedDay(ScheduleUtils.getDayOfWeek(arr[i]));
	    				}
	    			}
	    		}
	    		else if(interval == ScheduleUtils.INTERVAL_MONTHLY)
	    		{	 
	    			label = label.replace(replaceString, getLocalizedInterval(ScheduleUtils.INTERVAL_MONTHLY));
	    			label = label + ", " +  formatDayOfMonth(si.DayOfMonth);
	    		}
	    		
	    		// Make sure if for some reason replace me does not get replaced, it gets substituted by blank
	    		label = label.replace(replaceString, "");
	    	}
			return label;	    			
		}	  
	}	
}
	