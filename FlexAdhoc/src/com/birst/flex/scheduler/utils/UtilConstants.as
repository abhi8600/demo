package com.birst.flex.scheduler.utils
{
	public class UtilConstants
	{
		public function UtilConstants()
		{
		}
		
		public static const TYPE_NONE:String = "None";
		public static const TYPE_REPORT:String = "Report";
		public static const TYPE_PROCESS:String = "Process";
		public static const TYPE_SFDC:String = "SFDC_Extraction";
		public static const TYPE_SFDCEXTRACTION:String = "SFDCEXTRACTION";
		public static const TYPE_BDS:String = "BDS";
		public static const TYPE_NETSUITE:String = "NETSUITE";
		public static const TYPE_ALL:String = "ALL";
		public static const T_SFDC:String = "sfdc";
		public static const T_NETSUITE:String = "netsuite";
		
		public static const ALL_SPACES:String = "AllSpaces";
		public static const PREFIX_CATALOG_DIR:String = "V{CatalogDir}";
		public static const DEFAULT_NAME:String = "Default Name";
		
	}
}