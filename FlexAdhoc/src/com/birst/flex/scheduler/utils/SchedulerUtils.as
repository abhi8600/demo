package com.birst.flex.scheduler.utils
{
	import com.birst.flex.core.BirstAlert;
	import com.birst.flex.core.Util;
	import com.birst.flex.scheduler.shared.SchedulerVersion;
	import com.birst.flex.webservice.WebServiceClient;
	
	import flash.display.DisplayObject;
	
	import mx.controls.DataGrid;
	import mx.managers.PopUpManager;
	
	public class SchedulerUtils
	{
		public function SchedulerUtils()
		{
		}
		
		public static const schedulerBundle:String = "scheduler";
		public static const LOCALE_PREFIX:String = "SC_";
	    public static const LOCALE_PREFIX_USER_MODE:String = "SC_U_";
	    
	    public static var SCHEDULER_WSDL_LOC:String = "/Scheduler/wsdl/";
		public static var SCHEDULER_WS_ROOT:String;
		private static var SCHEDULER_PRIMARY_NAME:String;
	    
		public static function popUpConfirmationWindow(parent:DisplayObject, message:String):void
		{
			var completeDialog:BirstAlert = BirstAlert(PopUpManager.createPopUp(parent, BirstAlert, true));
			completeDialog.width = 300; 
			completeDialog.alertTitle = "Message";
			completeDialog.message = message;						
			PopUpManager.centerPopUp(completeDialog);
		}
		
		public static function getComponentErrorPrefix(componentName:String, errorMsg:String):String
		{	
			return componentName != null ? componentName + ": " + errorMsg : errorMsg; 
		}		
		
		 public static function getSchedulerBundbleLocaleString(key:String, params:Array = null):String
         {
         	return Util.getBundleLocaleString(schedulerBundle, key, params); 
         	
         }
         
         public static function getSchedulerRootUrl():String
		 {	
		 	if(SCHEDULER_WS_ROOT == null)
		 	{		
				var flashSchedulerUrl:String = Util.getApplicationSchedulerURL();
				if(flashSchedulerUrl != null && flashSchedulerUrl.length > 0)
				{
					SCHEDULER_WS_ROOT = flashSchedulerUrl + SCHEDULER_WSDL_LOC; 
				}
				else
				{
					SCHEDULER_WS_ROOT = SCHEDULER_WSDL_LOC; 
				}
		 	}
			return SCHEDULER_WS_ROOT;
		 }
		
		public static function getMigrateOptionsXml(fromSchedulerVersion:SchedulerVersion, fromJobId:String, 
													deleteFromJob:Boolean, jobType:String, isOldSFDC:Boolean=false):XML
		{	
			var migrateOptions:XML = <CopyJobInfo></CopyJobInfo>;
			migrateOptions.appendChild(XML("<JobId>" + fromJobId +  "</JobId>"));
			migrateOptions.appendChild(XML("<FromScheduler>" + fromSchedulerVersion.schedulerDisplayName +  "</FromScheduler>"));
			migrateOptions.appendChild(XML("<Type>" + jobType +  "</Type>"));
			migrateOptions.appendChild(XML("<DeleteFrom>" + deleteFromJob +  "</DeleteFrom>"));
			migrateOptions.appendChild(XML("<MultipleJobIDs>" + isOldSFDC + "</MultipleJobIDs>"));
			
			return migrateOptions;
		}
		
		
		public static function getApplicationParams(fnc:Function):void
		{
			var ws:WebServiceClient = new WebServiceClient("ClientInitData", "getFlashVars", "AdhocLayout", "getParameters");
			ws.setResultListener(fnc);
			ws.execute();	
		}
		 
		public static function checkIfAnyJobsSelected(numAllowedJobs:int, jobType:String, dataGridDataProviderID:DataGrid):Boolean
		{
			var array:Array = dataGridDataProviderID.selectedItems;
			if(array == null || (array != null && array.length == 0))
			{	
				Util.alertErrMessage(SchedulerUtils.getSchedulerBundbleLocaleString('SC_ERR_NO_JOB_SELECTED', [jobType]));
				return false;
			}
			
			if(numAllowedJobs > 0 && array.length != numAllowedJobs)
			{
				Util.alertErrMessage(SchedulerUtils.getSchedulerBundbleLocaleString('SC_ERR_EXCEED_OPERATION_SELECTION',[numAllowedJobs , jobType]));
				return false;
			}
			return true;		
		}
		
		public static function setPrimarySchedulerName(str:String):void
		{
			SCHEDULER_PRIMARY_NAME = str;
		}
		
		public static function getPrimarySchedulerName():String
		{
			return SCHEDULER_PRIMARY_NAME;
		}
		
		public static function getType(jobType:String):String
		{
			var type:String = null;
			if (jobType == UtilConstants.TYPE_SFDCEXTRACTION)
			{
				type = UtilConstants.T_SFDC;
			}
			else if (jobType == UtilConstants.TYPE_NETSUITE)
			{
				type = UtilConstants.T_NETSUITE;
			}
			return type;
		}
	}
}