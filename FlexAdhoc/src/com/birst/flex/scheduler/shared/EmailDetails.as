package com.birst.flex.scheduler.shared
{
	public class EmailDetails
	{
		private var _from:String;
		private var _subject:String;
		private var _htmlBodyText:String;
		
		public function EmailDetails()
		{
		}
		
		public function get From():String
		{
			return _from;
		} 
		
		public function set From(value:String):void
		{
			_from = value;
		}
		
		public function get Subject():String
		{
			return _subject;
		} 
		
		public function set Subject(value:String):void
		{
			_subject = value;
		}
		
		public function get HtmlBody():String
		{
			return _htmlBodyText;
		} 
		
		public function set HtmlBody(value:String):void
		{
			_htmlBodyText = value;
		}
	}
}