package com.birst.flex.scheduler.shared
{
	import com.birst.flex.scheduler.utils.ScheduleUtils;
	
	public class ScheduleCronExpression
	{
		

		
		// These simplied parameters are used for UI options
		// TO DO : update the parameters to be arrays so that 
		// multiple days, months, years, hours could be selected
		private var id:String;
		private var interval:String;
		private var dayOfWeek:int;
		private var daysOfWeek:Array;
		private var dayOfMonth:String;		
		private var hour:int;
		private var minutes:int;		
		private var amPm:String;
		private var timeZone:String;
		 
		// Cron expression
		// of the form 
		// seconds minutes hours dayofMonth Month DayOfWeek Year
		private var expression:String;
		private var cronParts:Array;
		
		// Cron Expression indices
		
		public static const INDEX_CRON_SECONDS:int = 0;
		public static const INDEX_CRON_MINUTES:int = 1;
		public static const INDEX_CRON_HOURS:int = 2;
		public static const INDEX_CRON_DAYOFMONTH:int = 3;
		public static const INDEX_CRON_MONTH:int = 4;
		public static const INDEX_CRON_DAYOFWEEK:int = 5;
		public static const INDEX_CRON_YEAR:int = 6;
		
		public static const VALUE_LAST_DAY_MONTH:String = "L";
		 
		public function ScheduleCronExpression()
		{
		}
		
		public function create():String
		{
			cronParts = new Array(7);
			setCronSeconds();		
			setCronMinute();
			setCronHour();
			setCronDayOfMonth();
			setCronMonth();
			setCronDaysOfWeek();
			joinCronParts();
			return expression;
		}
		
		private function joinCronParts():void
		{
			expression = cronParts[INDEX_CRON_SECONDS] + " " +
						 cronParts[INDEX_CRON_MINUTES] + " " +
						 cronParts[INDEX_CRON_HOURS] + " " +
						 cronParts[INDEX_CRON_DAYOFMONTH] + " " +
						 cronParts[INDEX_CRON_MONTH] + " " +
						 cronParts[INDEX_CRON_DAYOFWEEK] + " " +
						 "*";				 
		}
		
		public function parse():void
		{
			if(expression != null)
			{
				cronParts = expression.split(' ');				
				parseInterval();
				parseDaysOfWeek();
				parseDayOfMonth();
				parseHour();
				parseMinute();				
			}	
		}
		
		private function parseInterval():void
		{
			 if(cronParts[INDEX_CRON_DAYOFWEEK] == "*" 
			 		&& cronParts[INDEX_CRON_MONTH] == "*"
			 		&& cronParts[INDEX_CRON_DAYOFMONTH] == "?")
			 		{
			 			interval = ScheduleUtils.INTERVAL_DAILY;	
			 			return;		 			
			 		}
			 						 			
			 
			 if(cronParts[INDEX_CRON_DAYOFWEEK] != "*" 
			 			&& cronParts[INDEX_CRON_MONTH] == "*"
			 			&& cronParts[INDEX_CRON_DAYOFMONTH] == "?")
			 			{
			 				interval = ScheduleUtils.INTERVAL_WEEKLY;
			 				return;
			 			}
			 			
			 if(cronParts[INDEX_CRON_DAYOFWEEK] == "?" 
			 			&& cronParts[INDEX_CRON_MONTH] == "*"
			 			&& (cronParts[INDEX_CRON_DAYOFMONTH] != "?" || cronParts[INDEX_CRON_DAYOFMONTH] != "*"))
			 			{
			 				interval = ScheduleUtils.INTERVAL_MONTHLY;
			 				return;
			 			}
			 
			 	
			 
		}
		
		private function parseDayOfWeek():void
		{
			dayOfWeek = cronParts[INDEX_CRON_DAYOFWEEK];
		}
		
		private function parseDaysOfWeek():void
		{
			var daysString:String = cronParts[INDEX_CRON_DAYOFWEEK];
			daysOfWeek =  daysString.split(",");
		}
		
		private function parseDayOfMonth():void
		{
			dayOfMonth = cronParts[INDEX_CRON_DAYOFMONTH];
		}
		
		private function parseHour():void
		{
			hour = cronParts[INDEX_CRON_HOURS];
			if(hour >= 12)
			{				
				this.amPm = ScheduleUtils.TIME_PM;
				hour = hour == 12 ? hour : hour - 12;
			}
			else
			{
				this.amPm = ScheduleUtils.TIME_AM;
				hour = hour == 0 ? 12 : hour; 
			}
		}
		
		private function parseMinute():void
		{
			minutes = cronParts[INDEX_CRON_MINUTES];
		}
		
		
		private function setCronSeconds():void
		{
			cronParts[INDEX_CRON_SECONDS] = 0;
		}	
		
		private function setCronMinute():void
		{
			cronParts[INDEX_CRON_MINUTES] = this.minutes;
		}
		
		private function setCronHour():void
		{
			var cronHour:int = hour;
			if(amPm == ScheduleUtils.TIME_AM)
			{
				cronHour = cronHour == 12 ? 0 : cronHour;
			}
			else
			{
				cronHour = cronHour == 12 ? 12 : cronHour + 12; 
			}
			
			cronParts[INDEX_CRON_HOURS] = cronHour;
		}
		
		private function setCronDayOfMonth():void
		{
			if(interval == ScheduleUtils.INTERVAL_DAILY || interval == ScheduleUtils.INTERVAL_WEEKLY)			
			{
				cronParts[INDEX_CRON_DAYOFMONTH] = "?";
				return;
			}
			
			if(interval == ScheduleUtils.INTERVAL_MONTHLY)				
			{				
				if(dayOfMonth == ScheduleUtils.MONTH_LAST_DAY)
				{
					cronParts[INDEX_CRON_DAYOFMONTH] = VALUE_LAST_DAY_MONTH;
				}
				else
				{
					cronParts[INDEX_CRON_DAYOFMONTH] = dayOfMonth;
				}
			}
			 
		}
		
		private function setCronMonth():void
		{
			// Right now we allow user a simplified monthly option
			cronParts[INDEX_CRON_MONTH] = "*";
		}
		
		private function setCronDaysOfWeek():void
		{
			if(interval == ScheduleUtils.INTERVAL_DAILY)				
			{
				cronParts[INDEX_CRON_DAYOFWEEK] = "*";
			}
			
			if(interval == ScheduleUtils.INTERVAL_WEEKLY)				
			{
				cronParts[INDEX_CRON_DAYOFWEEK] = getDaysOfWeek();
			}
			
			if(interval == ScheduleUtils.INTERVAL_MONTHLY)				
			{
				cronParts[INDEX_CRON_DAYOFWEEK] = "?";
			}
		}
		
		private function getDaysOfWeek():String
		{
			var response:String = null;
			if(daysOfWeek != null && daysOfWeek.length > 0)
			{
				response = daysOfWeek[0];
				for(var i:int=1; i < daysOfWeek.length; i++)
				{
					response = response + "," + daysOfWeek[i]
				}
			}
			
			return response;
		}
		
		public function get Id():String
		{
			return id;
		}
		
		public function set Id(value:String):void		
		{
			id = value;
		}
				
		public function get Interval():String
		{
			return interval;
		}
		
		public function set Interval(value:String):void
		{
			interval = value; 
		}
		
		public function get Hour():int
		{
			return hour;
		}
		
		public function set Hour(value:int):void
		{
			hour = value;
		}
		
		public function get Minute():int
		{
			return minutes;
		}
		
		public function set Minute(value:int):void
		{
			minutes = value;
		}
		
		public function get DayOfWeek():int
		{
			return dayOfWeek;
		}
		
		public function set DayOfWeek(value:int):void
		{
			dayOfWeek = value;
		}
		
		public function get DaysOfWeek():Array
		{
			return daysOfWeek;
		}
		
		public function set DaysOfWeek(value:Array):void
		{
			daysOfWeek = value;
		}
		
		public function get DayOfMonth():String
		{
			return dayOfMonth;
		}
		
		public function set DayOfMonth(value:String):void
		{
			dayOfMonth = value;
		}
		
		public function get AmPm():String
		{
			return amPm;
		} 
		
		public function set AmPm(value:String):void
		{
			amPm = value;
		}
		
		public function get TimeZone():String
		{
			return timeZone;
		} 
		
		public function set TimeZone(value:String):void
		{
			timeZone = value;
		}
		
		public function get Expression():String
		{
			return expression;
		}
		
		public function set Expression(value:String):void
		{
			expression = value;
		}
		
		public static function parseScheduleCronExpression(_expression:String):ScheduleCronExpression
		{
			var scheduleCronExpression:ScheduleCronExpression = new ScheduleCronExpression();
			scheduleCronExpression.expression = _expression;
			scheduleCronExpression.parse();
			return scheduleCronExpression;
		}
	}
}