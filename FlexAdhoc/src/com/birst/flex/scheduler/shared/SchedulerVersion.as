package com.birst.flex.scheduler.shared
{
	public class SchedulerVersion
	{
		public function SchedulerVersion()
		{
		}
		
		public var schedulerDisplayName:String;
		public var primaryScheduler:Boolean;
		public var enableMigration:Boolean;
	}
}