package com.birst.flex.scheduler.reports
{
	public class ReportInfo
	{
		public function ReportInfo()
		{
		}
		
		private var _name:String;
		private var _contentReport:String;
		private var _deliveryFormats:String; // more like comma-separated values
		private var _fileName:String;
		private var _distributionEmails:String; // semi-colon separated email ids
		private var _distrubtionReport:String; 
		private var _compressionType:String;
		private var _triggerReport:String;
		private var _csvSeparator:String;
		private var _csvExtension:String;
		private var _prompts:XML;
		private var _isCreatedModeAdmin:Boolean;
		private var _overrideSessionVariables:XML;
		
		public function get Name():String
		{
			return _name;
		}
		
		public function set Name(value:String):void
		{
			_name = value;
		}
		
		public function get ContentReport():String
		{
			return _contentReport;
		}
		
		public function set ContentReport(value:String):void
		{
			_contentReport = value;
		}
		
		public function get DeliveryFormats():String
		{
			return _deliveryFormats;
		}
		
		public function set DeliveryFormats(value:String):void
		{
			_deliveryFormats = value;
		}
		
		public function get FileName():String
		{
			return _fileName;
		}
		
		public function set FileName(value:String):void
		{
			_fileName = value;
		}
		
		public function get Emails():String
		{
			return _distributionEmails;
		}
		
		public function set Emails(value:String):void
		{
			_distributionEmails = value;
		}
		
		public function get DistributionReport():String
		{
			return _distrubtionReport;
		}
		
		public function set DistributionReport(value:String):void
		{
			_distrubtionReport = value;
		}
		
		public function get Compression():String
		{
			return _compressionType;
		}
		
		public function set Compression(value:String):void
		{
			_compressionType = value;
		}
		
		public function get TriggerReport():String
		{
			return _triggerReport;
		}
		
		public function set TriggerReport(value:String):void
		{
			_triggerReport = value;
		}
		
		public function get CsvSeparator():String
		{
			return _csvSeparator;
		}
		
		public function set CsvSeparator(value:String):void
		{
			_csvSeparator = value;
		}
		
		public function get CsvExtension():String
		{
			return _csvExtension;
		}
		
		public function set CsvExtension(value:String):void
		{
			_csvExtension = value;
		}
		
		public function get isCreatedModeAdmin():Boolean
		{
			return _isCreatedModeAdmin;
		}
		
		public function set isCreatedModeAdmin(value:Boolean):void
		{
			_isCreatedModeAdmin = value;
		}
		
		public function get Prompts():XML
		{
			return _prompts;
		}
		
		public function set Prompts(value:XML):void
		{
			_prompts = value;
		}
		
		public function get OverrideSessionVariables():XML
		{
			return _overrideSessionVariables;
		}
		
		public function set OverrideSessionVariables(value:XML):void
		{
			_overrideSessionVariables = value;
		}
	}
}