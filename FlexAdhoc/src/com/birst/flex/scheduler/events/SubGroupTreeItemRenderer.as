package com.birst.flex.scheduler.events
{
	import com.birst.flex.scheduler.events.SubGroupsModifiedEvent;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import mx.controls.CheckBox;
	import mx.controls.Image;
	import mx.controls.Tree;
	import mx.controls.treeClasses.TreeItemRenderer;
	import mx.controls.treeClasses.TreeListData;
	
	public class SubGroupTreeItemRenderer extends TreeItemRenderer
	{
		private var currentCheckBox:CheckBox;
		
		public function SubGroupTreeItemRenderer()
		{
			super();
		}
		
		override protected function createChildren():void
		{
			super.createChildren();
			currentCheckBox = new CheckBox();
			currentCheckBox.setStyle( "verticalAlign", "middle" );
			currentCheckBox.addEventListener( MouseEvent.CLICK, onCheckBoxClickHandler );
			addChild(currentCheckBox);				
	    }	

		override public function set data(value:Object):void
		{
			if(value != null)
			{
				super.data = value;
				setCheckBoxVisibility();
			}			
	    }
		
		private function onCheckBoxClickHandler(event:Event):void
		{
			if (super.data)
			{
				var myListData:TreeListData = TreeListData(this.listData);
				var selectedNode:Object = myListData.item;				
				var tree:Tree = Tree(myListData.owner);
				var isCurrentItemBranch:Boolean = tree.dataDescriptor.isBranch(selectedNode); 
				if(isCurrentItemBranch)
				{
					var subGroupName:String = selectedNode.@Name;
					var eventType:String = SubGroupsModifiedEvent.P_SUBGROUP_MODIFIED;
					var eventOperation:Boolean = currentCheckBox.selected;					
					dispatchEvent(new SubGroupsModifiedEvent(subGroupName, eventOperation, eventType, true));						
				}				
			}
		}
		
		private function setCheckBoxVisibility():void
		{
        	var myListData:TreeListData = TreeListData(super.listData);
			var selectedNode:Object = myListData.item;				
			var tree:Tree = Tree(myListData.owner);			
			var nodeXML:XML = XML(selectedNode); 
			if(isItemBranch() || nodeXML.@SGroup == "true")
			{				
				// branch or subGroup Node
				currentCheckBox.visible = true;
				var isChecked:String = nodeXML.@Checked;
				currentCheckBox.selected = isChecked == "true" ? true : false;				
				//Tracer.debug('name :' + nodeXML.@Name, this);
				//Tracer.debug('checked :' + nodeXML.@Checked, this);													
			}
			else
			{
				// Its a leaf node. In this case, data sources				
				currentCheckBox.visible = false;									
			}			
		}
		
		private function isItemBranch():Boolean
		{
			var myListData:TreeListData = TreeListData(super.listData);					
			var isBranch:Boolean = myListData.hasChildren;
			return isBranch;
		}
		
		override protected function updateDisplayList(unscaledWidth:Number, unscaledHeight:Number):void
	   	{
			super.updateDisplayList(unscaledWidth, unscaledHeight);
	        if(super.data)
	        {	        		        	
			    if (super.icon != null)
			    {
			    	if(isItemBranch())
			    	{
			    		currentCheckBox.x = super.icon.x;
			    		currentCheckBox.y = 2;
			    		super.icon.x = currentCheckBox.x + currentCheckBox.width + 17;
			    	}
				    else
				    {
				    	super.icon.x = 17;
				    }
				    
				    super.label.x = super.icon.x + super.icon.width + 3;
				}
				else
			    {
			    	if(isItemBranch())
			    	{
			    		currentCheckBox.x = super.label.x;
			    		currentCheckBox.y = 6;
			    		super.label.x = currentCheckBox.x + currentCheckBox.width + 17;
			    	}
			    	else
			    	{
			    		super.label.x = 17;
			    	}				    
				}				
			}
	    }
	}
}