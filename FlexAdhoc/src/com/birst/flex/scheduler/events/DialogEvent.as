package com.birst.flex.scheduler.events
{
	import flash.events.Event;

	public class DialogEvent extends Event
	{
		public static const DIALOG_YES:String = "DialogYes";
		public static const DIALOG_NO:String = "DialogNo";
		
		public var eventParam:Object;
		
		public function DialogEvent(type:String, eventParam:Object= null, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			this.eventParam = eventParam;
			super(type, bubbles, cancelable);
		}
		
		override public function clone():Event
		{
			return new DialogEvent(type, eventParam);	
		}
	}
}