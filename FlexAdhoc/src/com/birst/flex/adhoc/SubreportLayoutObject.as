package com.birst.flex.adhoc
{
	import com.birst.flex.adhoc.dialogs.SubReportImagePropertiesDialogBox;
	import com.birst.flex.core.AppEvent;
	import com.birst.flex.core.AppEventManager;
	
	import flash.events.ContextMenuEvent;
	import flash.events.MouseEvent;
	import flash.ui.ContextMenuItem;
	import mx.resources.ResourceManager;
 	[ResourceBundle('adhoc')]
	public class SubreportLayoutObject extends LayoutObject
	{		
		public function SubreportLayoutObject(id:String, layout:XML, l:Layout)
		{
			this.type = "Subreport";
			selectionType = AppEvent.EVT_SUBREPORT_CHOSEN;
			super(id, layout, l);
		}
		
		override public function getLabel():String {
			return "s:" + trimName(entityXML.Subreport);
		}
		private function trimName(name:String):String {
			var result:String = name;
			var lastSlash:int = result.lastIndexOf("/");
			if (lastSlash > 0)
				result = result.substr(lastSlash + 1);
			var lastDot:int = result.lastIndexOf(".");
			if (lastDot > 0)
				result = result.substr(0, lastDot);
			return result;
		}
		
		override protected function getLayoutType():String {
			return AppEvent.EVT_SUBREPORT_CHOSEN;
		}
		
		override protected function clickEventHandler(event:MouseEvent):void{
			AppEventManager.fire(new AppEvent(AppEvent.EVT_SUBREPORT_CHOSEN, 'SubreportLayoutObject', 'clickEventHandler', this.ID, event.ctrlKey));
		} 
		
		override protected function setupContextMenuitems():void {
			super.setupContextMenuitems();
			var subreportPropertiesItem:ContextMenuItem = new ContextMenuItem(ResourceManager.getInstance().getString("adhoc","optionsMenu_SPP"));
			itemContextMenu.customItems.push(subreportPropertiesItem);
			subreportPropertiesItem.addEventListener(ContextMenuEvent.MENU_ITEM_SELECT, subReportProperties);
		}
		
		//Called by the right click "Remove"
		override protected function deleteItem(event:ContextMenuEvent):void {
			AdHocConfig.instance().removeSubReport(this.ID.toString());
		}
		
		private function subReportProperties(event:ContextMenuEvent):void {
			invokeMenuAction("subreportProperties", SubReportImagePropertiesDialogBox);
		}
	}
}