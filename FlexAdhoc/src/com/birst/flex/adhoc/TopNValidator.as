package com.birst.flex.adhoc
{
	import mx.core.Application;
	import mx.validators.NumberValidator;
	import com.birst.flex.core.CoreApp;

	public class TopNValidator extends NumberValidator
	{
		public function TopNValidator()
		{
			super();
		}
		
		override protected function doValidation(value:Object):Array{
			//we're cool with 'all'
			var allString:String = CoreApp(Application.application).getLocString('APP_all');
			var validDefault:String = allString ? allString : 'all';
			
			var val:String = value ? String(value) : "";
			if (val == validDefault)
				return [];
				
			return super.doValidation(value);
		}
	}
}