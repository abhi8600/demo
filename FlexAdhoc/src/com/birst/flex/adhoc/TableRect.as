package com.birst.flex.adhoc
{
	import com.birst.flex.core.BirstADGItemRenderer;
	import com.birst.flex.core.MenuItemActions;
	import com.birst.flex.core.Util;
	import com.birst.flex.dashboards.views.DashletContentBase;
	
	import flash.events.ContextMenuEvent;
	import flash.geom.Rectangle;
	import flash.ui.ContextMenu;
	import flash.ui.ContextMenuItem;
	
	import mx.collections.ArrayCollection;
	import mx.collections.XMLListCollection;
	import mx.containers.Canvas;
	import mx.controls.AdvancedDataGrid;
	import mx.controls.MenuBar;
	import mx.controls.advancedDataGridClasses.AdvancedDataGridColumn;
	import mx.controls.advancedDataGridClasses.AdvancedDataGridListData;
	import mx.controls.listClasses.IListItemRenderer;
	import mx.core.Application;
	import mx.core.ClassFactory;
	import mx.core.ScrollPolicy;
	import mx.core.UITextField;
	import mx.events.AdvancedDataGridEvent;
	import mx.events.FlexEvent;
	import mx.events.ListEvent;
	import mx.utils.StringUtil;

	public class TableRect extends Rectangle
	{
		private var headers:XMLListCollection;
		private var data:ArrayCollection = new ArrayCollection();
		private var tableBelowStartsAtY:int = -1;
		private var bot:uint = 0;
		private var columnIndices:Array = [];
		private var currentRow:Object = new Object();
		private var currentY:int = -1;
		private var menuItems:Array = null;
		private var grid:AdvancedDataGrid = null;
		
		public function TableRect(x:Number=0, y:Number=0, width:Number=0, height:Number=0)
		{
			//TODO: implement function
			super(x, y, width, height);
			headers = new XMLListCollection();
		}
		
		public function addHeader(item:XML):void {
			headers.addItem(item);
			var index:String = item.columnIndex;
			columnIndices.push(index);
			item['processed'] = 'true';
		}
		
		public function addDataIfAppropriate(item:XML):void {
			if (item.isMenu != null && String(item.isMenu) == "true")
				return;
				
			var itemX:int = item.@x;
			var itemWidth:int = item.@width;
			var itemY:int = item.@y;
			var itemHeight:int = item.@height;
			if (itemX >= x && itemX + itemWidth <= x + width && 
				(tableBelowStartsAtY < 0 || itemY + itemHeight <= tableBelowStartsAtY)) {
					var index:String = item.columnIndex;
					if (columnIndices.indexOf(index) >= 0) {
						if (itemY > currentY) {
							currentRow = new Object(); 
							data.addItem(currentRow);
							currentY = itemY;
						}
						currentRow[index] = item;
						item['processed'] = 'true';
						bot = Math.max(bot, itemY + itemHeight);
					}
				}
		}
		
		public function addSubsequentTable(t:TableRect):void {
			if (t == this)
				return;
				
			if ((t.x <= x && t.x + t.width > x) || 
				(t.x > x && t.x < x + width)) {
					// there's overlap on x
					tableBelowStartsAtY = t.y;
					return;
				} 
		}
		
		public function render(xOrigin:uint, yOrigin:uint, parent:Canvas, addMenuBarItemFunc:Function, showColumnLabelMenu:Boolean) :void {
			grid = new BirstAdvancedDataGrid();
			grid.width = width;
			grid.height = bot + data.length - y;
			grid.x = x;
			grid.y = y;
			grid.data = headers;
			grid.verticalScrollPolicy = ScrollPolicy.OFF;
			//,		paddingTop: 0, paddingBottom:0, verticalGridLines: "false"
			grid.setStyle("paddingTop", 0);
			grid.setStyle("paddingBottom", 0);
			grid.setStyle("verticalGridLines", "false");
			grid.sortItemRenderer = new ClassFactory(BirstSortItemRenderer);
			grid.addEventListener(ListEvent.ITEM_CLICK, handleGridClicked);
			
			var columns:Array = new Array();
			
			var i:uint;
			for (i = 0; i < headers.length; i++) {
				var index:String = headers[i].columnIndex;
				var col:AdvancedDataGridColumn = new AdvancedDataGridColumn(index);
				col.width = headers[i].@width;
				col.headerText = headers[i].@value;
				col.styleFunction = colStyleFunction;
				col.labelFunction = getLabel;
				col.itemRenderer = new ClassFactory(BirstADGItemRenderer);
				col.headerRenderer = new ClassFactory(BirstFlexDataGridHeaderRenderer);
				columns.push(col);
			}
			
			if (addMenuBarItemFunc != null){
				var mb:MenuBar = new MenuBar();
				addMenuBarItemFunc(mb, 0, 10, "", null, showColumnLabelMenu);
				menuItems = mb.dataProvider[0].children;
				grid.contextMenu = new ContextMenu();
				grid.contextMenu.hideBuiltInItems();
				if (menuItems) {
					for (i = 0; i < menuItems.length; i++) {
						var cmi:ContextMenuItem = new ContextMenuItem(menuItems[i].label);
						cmi.addEventListener(ContextMenuEvent.MENU_ITEM_SELECT, handleContextMenuEvent);
						grid.contextMenu.customItems.push(cmi);
					}
				}
			}
			grid.columns = columns;
			grid.rowCount = data.length;
			grid.variableRowHeight = true;
//			grid.lockedRowCount = 1;
			grid.dataProvider = data;
			grid.addEventListener(AdvancedDataGridEvent.SORT, sortTable);
			
			grid.addEventListener(FlexEvent.CREATION_COMPLETE, doResize);
			
			parent.addChild(grid);
		}
		
		private function doResize(event:FlexEvent):void {
			var height:Number = grid.headerHeight;
			var sumOfItemHeight:Number = 0;
			var avgRectHeight:Number = -1;
			for (var j:int = 0; j < data.length; j++) {
				var o:Object = data[j];
				var renderer:IListItemRenderer = grid.itemToItemRenderer(o);
				if (renderer != null) {
					var rect:Rectangle = renderer.getRect(grid);
					sumOfItemHeight = sumOfItemHeight - height + rect.bottom;
					height = rect.bottom;
				}
				else {
					if (avgRectHeight < 0)
						avgRectHeight = sumOfItemHeight / j;
					height = height + avgRectHeight;
				}
			}
			if (grid.height != height) {
				grid.height = height;
				grid.invalidateSize();
			}
		}
		
		private function handleContextMenuEvent(event:ContextMenuEvent):void {
			var target:Object = event.mouseTarget;
			var col:String = '';
			if (target is BirstADGItemRenderer) {
				col = ((target as BirstADGItemRenderer).listData as AdvancedDataGridListData).dataField;
			}
			else if (target is UITextField) {
				// header click
				col = ((target as UITextField).parent as BirstFlexDataGridHeaderRenderer).data.dataField;
			}
			var menuItem:Object = null;
			for (var i:int = 0; i < menuItems.length; i++) {
				if (menuItems[i].label == event.currentTarget.caption) {
					menuItem = menuItems[i];
					break;
				}
			}
			if (menuItem == null)
				return;
				
			if (menuItem.col == 10) {
				// label
				var lblList:XMLList = AdHocConfig.instance().getColumnXML(col);
				if (lblList == null || lblList.length() < 1)
					return;
					
				col = lblList[0].LabelName.toString() 
			}
			var oldCol:String = menuItem.col;
			menuItem.col = col;
			Application.application.selectedLayoutIds.removeAll();
			Application.application.selectedLayoutIds.addItem(col);
			Application.application.selectedLayoutTypes.removeAll();
			Application.application.selectedLayoutTypes.addItem(menuItem.type);
			MenuItemActions.invokeAction(menuItem);
			menuItem.col = oldCol;
		}
		
		private function colStyleFunction(data:Object, col:AdvancedDataGridColumn):Object {
			var x:XML = XML(data[col.dataField]);
			var fontFamily:String = x.@face;
			var fontSize:String = x.@size;
			var foreColor:String = x.@color;
			var backColor:String = x.@backcolor;
			foreColor = foreColor ? foreColor.substr(1) : '0';
			backColor = backColor ? backColor.substr(1) : 'FFFFFF';
			return {color: int("0x" + foreColor), rowColor: int("0x" + backColor),
				fontFamily: fontFamily, fontSize: int(fontSize), bottom: 0, top: 0};
		}
		
		private function getLabel(data:Object, col:AdvancedDataGridColumn):String {
			var x:XML = XML(data[col.dataField]);
			return x.@value;
		}
		
		private function sortTable(event:AdvancedDataGridEvent):void {
			event.stopImmediatePropagation();
			var fld:String = event.dataField;
			var grid:AdvancedDataGrid = event.currentTarget as AdvancedDataGrid;
           	var sortedColumns:Array = null;
			var sortedOrderArray:ArrayCollection = AdHocConfig.instance().getSortedOrderArray();
			if (grid.parent.parent is DashletContentBase) {
				var dcb:DashletContentBase = grid.parent.parent as DashletContentBase;
				sortedColumns = dcb.getSortColumns();
			}
			else {
				var config:AdHocConfig = AdHocConfig.instance();
    	       	sortedColumns = config.getSortedColumns();
   			}	
			var ascending:Boolean = true;
			var foundCol:Object = null;
			for (var i:int = 0; i < sortedColumns.length; i++) {
				var col:Object = sortedColumns[i];
				if (col.id == fld) {
					foundCol = col;
					var order:String = col.order == null ? "Ascending" : col.order.value;
					order = StringUtil.trim(order);
					if (order != "Ascending"){
						ascending = false;
					}
				}
			}
			if (foundCol == null) {
				foundCol = new Object();
				foundCol.id = fld;
				foundCol.order = new Object();
				sortedColumns.push(foundCol);
			}
				var s:Object = null;
				for (var j:int = 0; j < sortedOrderArray.length; j++) {
					if ((! ascending && sortedOrderArray[j].value == 'Ascending') ||
						(ascending && sortedOrderArray[j].value == 'Descending')) {
						s = sortedOrderArray[j];
						break;
					}
				}
				foundCol.order = s;
				if (! event.multiColumnSort) {
					// remove all other sorts and add this one
					sortedColumns = new Array();
					sortedColumns.push(foundCol);
				}
			if (grid.parent.parent is DashletContentBase) {
				var dcb1:DashletContentBase = grid.parent.parent as DashletContentBase;
				dcb1.setSortedColumns(sortedColumns);
			}
			else {
				var config1:AdHocConfig = AdHocConfig.instance();
				config1.setSortedColumns(sortedColumns);
   			}	
			return;
		}
		
		private function handleGridClicked(event:ListEvent):void {
			var grid:AdvancedDataGrid = event.currentTarget as AdvancedDataGrid;
			var col:AdvancedDataGridColumn = grid.columns[event.columnIndex];
			var o:Object = data[event.rowIndex];
			var x:XML = XML(o[col.dataField]);
			var link:String = x.@link;
			var haslink: Boolean = link != null && link.length > 0;
			if (haslink){
				var decodedLink:String = com.birst.flex.core.Util.decodeURIComponentAndSpaces(link);
				LinkNavigator.staticNavigate(decodedLink, x.@target, grid);
			}
			
		}
	}
}