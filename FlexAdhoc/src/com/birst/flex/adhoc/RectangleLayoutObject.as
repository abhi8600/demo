package com.birst.flex.adhoc
{
	import com.birst.flex.adhoc.dialogs.RectanglePropertiesDialogBox;
	import com.birst.flex.core.AppEvent;
	import com.birst.flex.core.AppEventManager;
	
	import flash.events.ContextMenuEvent;
	import flash.events.MouseEvent;
	import flash.ui.ContextMenuItem;
	import mx.resources.ResourceManager;
	[ResourceBundle('adhoc')]
	public class RectangleLayoutObject extends LayoutObject
	{	
		public function RectangleLayoutObject(id:String, layout:XML, l:Layout)
		{
			this.type = "Rectangle";
			selectionType = AppEvent.EVT_RECTANGLE_CHOSEN;
			super(id, layout, l);
		}
		override public function getLabel():String {
			return "r:" + entityXML.Label;
		}
		override protected function getLayoutType():String {
			return AppEvent.EVT_RECTANGLE_CHOSEN;
		}
		
		override protected function setupContextMenuitems():void {
			super.setupContextMenuitems();
			itemContextMenu.customItems.pop();
			var colPropertiesItem:ContextMenuItem = new ContextMenuItem( ResourceManager.getInstance().getString("adhoc","optionsMenu_RectangleP"));
			itemContextMenu.customItems.push(colPropertiesItem);
			colPropertiesItem.addEventListener(ContextMenuEvent.MENU_ITEM_SELECT, rectangleProperties);
		}
		
		override protected function clickEventHandler(event:MouseEvent):void{
			AppEventManager.fire(new AppEvent(AppEvent.EVT_RECTANGLE_CHOSEN, 'LabelLayoutObject', 'clickEventHandler', this.ID, event.ctrlKey));
		} 
		
		//Called by the right click "Remove"
		override protected function deleteItem(event:ContextMenuEvent):void {
			AdHocConfig.instance().removeRectangle(this.ID.toString());
		}
		
		private function rectangleProperties(event:ContextMenuEvent):void {
			invokeMenuAction("rectangleProperties", RectanglePropertiesDialogBox);
		}
	}
}