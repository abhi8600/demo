package com.birst.flex.adhoc
{
	import flash.text.TextFormat;
	
	import mx.controls.Image;
	import mx.controls.menuClasses.MenuBarItem;

	public class LabelMenuBarItem extends MenuBarItem
	{
		private var _tf:TextFormat;
		
		public function LabelMenuBarItem()
		{
			super();
		}
		
		override protected function createChildren():void {
	        super.createChildren();
	        label.multiline = true;
	        label.wordWrap = true;
	    }
    
	    override protected function updateDisplayList(unscaledWidth:Number,
	                                                  unscaledHeight:Number):void {
	        super.updateDisplayList(unscaledWidth, unscaledHeight);
	        label.x = label.x - 20;
	            
	        label.setActualSize(unscaledWidth, unscaledHeight);

	    }
	}
}