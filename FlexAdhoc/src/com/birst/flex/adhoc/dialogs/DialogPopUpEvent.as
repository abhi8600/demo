package com.birst.flex.adhoc.dialogs
{
	import flash.events.Event;

	public class DialogPopUpEvent extends Event
	{
		private var _menuItem:Object;
		
		public function DialogPopUpEvent(type:String, menuItem:Object, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
			_menuItem = menuItem;
		}
		
		public function get menuItem():Object{
			return _menuItem;
		}
	}
}