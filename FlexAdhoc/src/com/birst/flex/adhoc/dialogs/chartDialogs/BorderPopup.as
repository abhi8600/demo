package com.birst.flex.adhoc.dialogs.chartDialogs
{
	import com.birst.flex.adhoc.dialogs.AdhocChartPropertiesDialogBox;
	
	public class BorderPopup
	{
		public var popUp:Object;
		
		public var border:Border;
		public var caller:AdhocChartPropertiesDialogBox
		public var config:Array;
		
		public function BorderPopup(c:AdhocChartPropertiesDialogBox)
		{
			border = new Border();
			caller = c;
		}

		public function setBackground(a:Array):void {
			caller.setBackground(a);
		}
	}
}