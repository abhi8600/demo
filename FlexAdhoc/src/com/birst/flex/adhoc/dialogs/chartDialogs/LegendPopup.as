package com.birst.flex.adhoc.dialogs.chartDialogs
{
	import com.birst.flex.adhoc.dialogs.AdhocChartPropertiesDialogBox;
	
	public class LegendPopup
	{
		public var popUp:Object;
		
		public var legend:Legend;
		public var caller:AdhocChartPropertiesDialogBox
		public var config:Array;
		
		public function LegendPopup(c:AdhocChartPropertiesDialogBox)
		{
			legend = new Legend();
			caller = c;
		}

		public function setLegend(a:Array):void {
			caller.setLegend(a);
		}
	}
}