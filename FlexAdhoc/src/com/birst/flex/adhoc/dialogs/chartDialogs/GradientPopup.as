package com.birst.flex.adhoc.dialogs.chartDialogs
{
	
	public class GradientPopup
	{
		public var popUp:Object;
		
		public var gradient:Gradient;
		public var caller:SelectChartType
		public var config:Array;
		
		public function GradientPopup(c:SelectChartType)
		{
			gradient = new Gradient();
			caller = c;
		}

		public function setGradient(a:Array):void {
			caller.setGradient(a);
		}
		
		public function getDefaultProperties():Array {
			return gradient.getDefaultProperties();
		}
	}
}