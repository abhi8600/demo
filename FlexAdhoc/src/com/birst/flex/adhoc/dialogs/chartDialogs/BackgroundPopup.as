package com.birst.flex.adhoc.dialogs.chartDialogs
{
	import com.birst.flex.adhoc.dialogs.AdhocChartPropertiesDialogBox;
	
	public class BackgroundPopup
	{
		public var popUp:Object;
		
		public var background:Background;
		public var caller:AdhocChartPropertiesDialogBox
		public var config:Array;
		
		public function BackgroundPopup(c:AdhocChartPropertiesDialogBox)
		{
			background = new Background();
			caller = c;
		}

		public function setBackground(a:Array):void {
			caller.setBackground(a);
		}
	}
}