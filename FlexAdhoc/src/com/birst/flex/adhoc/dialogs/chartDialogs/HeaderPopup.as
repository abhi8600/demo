package com.birst.flex.adhoc.dialogs.chartDialogs
{
	import com.birst.flex.adhoc.dialogs.AdhocChartPropertiesDialogBox;
	
	public class HeaderPopup
	{
		public var popUp:Object;
		
		public var header:Header;
		public var caller:AdhocChartPropertiesDialogBox
		public var config:Array;
		
		public function HeaderPopup(c:AdhocChartPropertiesDialogBox)
		{
			header = new Header();
			caller = c;
		}

		public function setHeaderProps(a:Array):void {
			caller.setHeaderProps(a);
		}
	}
}