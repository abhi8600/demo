package com.birst.flex.adhoc.dialogs.chartDialogs
{
	import com.birst.flex.core.MultiColorDialogBox;
	import flash.events.Event;

	public class ChartMultiColorDialogBox extends MultiColorDialogBox
	{
		public function ChartMultiColorDialogBox()
		{
			super();
			ApplyCloseOperations.setup(this);
		}
		
		override public function saveButtonClickHandler(event:Event):void{
			ApplyCloseOperations.apply(this);
		}
	}
}