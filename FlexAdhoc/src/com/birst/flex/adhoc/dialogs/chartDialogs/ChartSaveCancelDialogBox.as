package com.birst.flex.adhoc.dialogs.chartDialogs
{
	import com.birst.flex.core.SaveCancelDialogBox;
	import com.birst.flex.core.AppEvent;
	import com.birst.flex.core.AppEventManager;
	import com.birst.flex.core.Localization;
	
	import flash.events.Event;

	public class ChartSaveCancelDialogBox extends SaveCancelDialogBox
	{
		public function ChartSaveCancelDialogBox()
		{
			super();
			ApplyCloseOperations.setup(this);
		}
		
		override public function saveButtonClickHandler(event:Event):void{
			ApplyCloseOperations.apply(this);
		}
	}
}