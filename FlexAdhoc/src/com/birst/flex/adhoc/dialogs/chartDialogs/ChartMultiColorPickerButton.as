package com.birst.flex.adhoc.dialogs.chartDialogs
{
	import com.birst.flex.core.MenuItemActions;
	import com.birst.flex.core.MultiColorPickerButtonBase;
	
	import flash.events.Event;
	
	public class ChartMultiColorPickerButton extends MultiColorPickerButtonBase
	{
		public function ChartMultiColorPickerButton()
		{
			super();
		}
		
		override protected function clickEventHandler(event:Event):void{
			MenuItemActions.invokeAction({ klass: ChartMultiColorDialogBox, parent: this, popUp: _popUp }, false, true);
		}
		
	}
}