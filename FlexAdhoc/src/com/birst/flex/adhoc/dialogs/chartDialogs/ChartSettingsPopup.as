package com.birst.flex.adhoc.dialogs.chartDialogs
{
	import com.birst.flex.adhoc.dialogs.AdhocChartPropertiesDialogBox;
	
	public class ChartSettingsPopup
	{
		public var popUp:Object;
		
		public var chartSettings:ChartSettings;
		public var caller:AdhocChartPropertiesDialogBox
		public var config:Array;
		
		public function ChartSettingsPopup(c:AdhocChartPropertiesDialogBox)
		{
			chartSettings = new ChartSettings();
			caller = c;
		}

		public function setChartSettings(a:Array):void {
			caller.setChartSettings(a);
		}
	}
}