package com.birst.flex.adhoc.dialogs.chartDialogs
{
	
	public class ThreeDPopup
	{
		public var popUp:Object;
		
		public var threeD:ThreeDDialog;
		public var caller:ChartSettings;
		public var config:Array;
		
		public function ThreeDPopup(c:ChartSettings)
		{
			threeD = new ThreeDDialog();
			caller = c;
		}

		public function setThreeD(a:Array):void {
			caller.setThreeD(a);
		}
		
		public function getDefaultValues():Object {
			return threeD.getDefaultProperties();
		}
		
	}
}