package com.birst.flex.adhoc.dialogs.chartDialogs
{
	import com.birst.flex.core.Localization;
	import com.birst.flex.core.SaveCancelDialogBox;
	import com.birst.flex.core.AppEvent;
	import com.birst.flex.core.AppEventManager;
	
	public class ApplyCloseOperations
	{
		public function ApplyCloseOperations()
		{
		}

		public static function setup(dialogBox:SaveCancelDialogBox):void{
			dialogBox.localizedBundle = Localization.ADHOC;
			dialogBox.saveButtonLocalizedId = 'CH_apply';
			dialogBox.cancelButtonLocalizedId = 'CH_close';
			dialogBox.setStyle('modalTransparency', 0);
		}
		
		public static function apply(dialogBox:SaveCancelDialogBox):void{
			var saveEvent:AppEvent = new AppEvent(AppEvent.EVT_CHART_APPLY, 'ChartSaveCancelDialogBox', 'save', dialogBox);
			AppEventManager.fire(saveEvent);
		}
	}
}