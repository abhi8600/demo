package com.birst.flex.adhoc.dialogs.chartDialogs
{
	import com.birst.flex.adhoc.dialogs.AdhocChartPropertiesDialogBox;
	
	public class XAxisPopup
	{
		public var popUp:Object;
		
		public var xAxis:XAxes;
		public var caller:AdhocChartPropertiesDialogBox
		public var config:Array;
		
		public function XAxisPopup(c:AdhocChartPropertiesDialogBox)
		{
			xAxis = new XAxes();
			caller = c;
		}

		public function setXAxis(a:Array):void {
			caller.setXAxis(a);
		}
	}
}