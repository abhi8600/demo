package com.birst.flex.adhoc.dialogs
{
	public class FilterColumnOperator
	{
		public static var LF_EQ:String = "Equal";
		public static var SF_EQ:String = "=";
		
		public static var LF_NEQ:String = "Not equal";
		public static var SF_NEQ:String = "<>";
		
		public static var LF_LT:String = "Less than";
		public static var SF_LT:String = "<";
		
		public static var LF_GT:String = "Greater than";
		public static var SF_GT:String = ">";
		
		public static var LF_LTEQ:String = "Less than or equal";
		public static var SF_LTEQ:String = "<=";
		
		public static var LF_GTEQ:String = "Greater than or equal";
		public static var SF_GTEQ:String = ">=";
		
		public static var SHORT2LONG:Array = new Array();
		SHORT2LONG[SF_EQ]  = LF_EQ;
		SHORT2LONG[SF_NEQ] = LF_NEQ;
		SHORT2LONG[SF_LT] = LF_LT;
		SHORT2LONG[SF_GT] = LF_GT;
		SHORT2LONG[SF_LTEQ] = LF_LTEQ;
		SHORT2LONG[SF_GTEQ] = LF_GTEQ;
		
		public static var LONG2SHORT:Array = new Array();
		LONG2SHORT[LF_EQ]  = SF_EQ;
		LONG2SHORT[LF_NEQ] = SF_NEQ;
		LONG2SHORT[LF_LT] = SF_LT;
		LONG2SHORT[LF_GT] = SF_GT;
		LONG2SHORT[LF_LTEQ] = SF_LTEQ;
		LONG2SHORT[LF_GTEQ] = SF_GTEQ;

		public static function longToShort(op:String):String{
			var lop:String = LONG2SHORT[op];
			if (lop == null)
				return op;
			
			return lop;
		}
		
		public static function shortToLong(op:String):String{
			var sop:String = SHORT2LONG[op];
			if (sop == null)
				return op;
			
			return sop;
		}
								
		public function FilterColumnOperator()
		{
		}

		
	}
}