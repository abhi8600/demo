package com.birst.flex.adhoc.dialogs
{
	//import com.birst.flex.core.FileNode;
	import com.birst.flex.core.Tracer;
	import com.birst.flex.webservice.WebServiceClient;
	import com.birst.flex.webservice.WebServiceResult;
	
	import mx.collections.ICollectionView;
	import mx.collections.XMLListCollection;
	import mx.controls.Tree;
	import mx.controls.treeClasses.ITreeDataDescriptor;
	import mx.events.CollectionEvent;
	import mx.events.CollectionEventKind;
	import mx.rpc.events.ResultEvent;

	public class CatalogTreeDataDescriptor implements ITreeDataDescriptor
	{
		private var _tree:Tree;
		private var refreshData:Boolean;
		private var open:Object;
		
		private var loadingDirs:Object = new Object();
		
        [Bindable]
        [Embed("/images/folder.png")]
        private var folderIcon:Class;
		
		private var _filterFunc:Function;
		
		public function CatalogTreeDataDescriptor(tree:Tree)
		{
			_tree = tree;
			refreshData = false;
			_tree.labelFunction = treeNode_labelFunc;
			_tree.iconFunction = treeNode_iconFunc;
			_tree.dataTipFunction = treeNode_tooltipFunc;
		}
		
		private function getObjectFromList(list:XMLList, xml:XML):XML {
			for (var i:int = 0; i < list.length(); i++) {
				var node:XML = list[i];
				if (node.DirectoryNode.@n == xml.DirectoryNode.@n) {
					return node;
				}
				if (xml.DirectoryNode.name.toString().indexOf(node.DirectoryNode.@n) == 0) {
					getObjectFromList(node.DirectoryNode.ch, xml);
				}
			}
			return null;
		}
		
		public function set filter(func:Function):void{
			_filterFunc = func;
		}
		
		public function get filter():Function{
			return _filterFunc;
		}
		
		public function getChildren(node:Object, model:Object=null):ICollectionView {
			var children:XMLListCollection = null;
			if (node is XML) {
				var list:XMLList = (node as XML).child("ch");
				if (list == null || list.length() == 0) {
					var ch:XML = new XML(<ch/>);
					children = new XMLListCollection();
					var c:XML = new XML(<DirectoryNode c='Loading...' l='Loading...' />);
					ch.setChildren(c);
					(node as XML).setChildren(ch);
					var name:String = node.@n.toString();
					loadingDirs[name] = node;
					
					var ws:WebServiceClient = new WebServiceClient('File', 'getDirectory', "Dashboards", "createReportsTree");
					ws.setResultListener(addToReportTree);
					ws.execute(name, false);
				}
				else if (list != null && list.length() > 0) {
					// There's only one <children> element per FileNode
					var xml:XML = list[0];
					children = new XMLListCollection(xml.child("DirectoryNode"));
					// Filtration
					if(_filterFunc != null){
						children.filterFunction = this._filterFunc;
						children.refresh();
					}
				}
			} else {
    			Tracer.error("Unexpected type in 'getChildren()' method.", this);
            }
            return children;
		}
		
		private function addToReportTree(result:ResultEvent):void{
			var rs:WebServiceResult = WebServiceResult.parse(result); 
			if (rs.isSuccess()){
				var ret:XMLList = rs.getResult();
				var parentNode:XMLList = ret.DirectoryNode;
				var parentName:String = parentNode.@n.toString();
				var children:XMLListCollection = new XMLListCollection(parentNode.ch.DirectoryNode);
				var p:XML = XML(loadingDirs[parentName]);
				loadingDirs[parentName] = null;
				if (p) {
					for (var j:uint = 0; j< children.length; j++) {
						var c:XML = XML(children.getItemAt(j));
						c.@n = parentName + '/' + c.@n;
					}
					var l:XMLList = parentNode.ch;
					if (!l || l.length() == 0) {
						l = new XMLList(XML(<ch/>));
					}
					p.setChildren(l);
					this._tree.invalidateDisplayList();
			        var event:CollectionEvent = new CollectionEvent(
			                                        CollectionEvent.COLLECTION_CHANGE,
			                                        false, 
			                                        true,
			                                        CollectionEventKind.RESET);
			        event.items = [p];
					XMLListCollection(this._tree.dataProvider).dispatchEvent(event);
				}
			}
		}
			
		public function hasChildren(node:Object, model:Object=null):Boolean
		{
            var list:XMLListCollection = XMLListCollection(getChildren(node));
            return ! (list == null || list.length == 0);
		}
		
		public function treeNode_labelFunc(item:Object):String {
			return item.@n;
		}
		
		public function treeNode_iconFunc(item:Object):Class {
			return folderIcon;
		}
		
		public function treeNode_tooltipFunc(item:Object):String {
			var isBranch:Boolean = isBranch(item);
			
			//Don't show tooltips if we're already open
			if (isBranch && _tree.isItemOpen(item)){
				return null;
			}
			
			//Unopened folder
			if (isBranch){
				return "Click to open";
			}
				
			return null;
		}
		
		public function getData(node:Object, model:Object=null):Object {
			return node;
		}
		
		public function addChildAt(parent:Object, newChild:Object, index:int, model:Object=null):Boolean {
			return false;
		}
		
		public function removeChildAt(parent:Object, child:Object, index:int, model:Object=null):Boolean {
			return false;
		}
        
        public function isBranch(node:Object, model:Object=null):Boolean {
        	var children:XMLList = node.ch.DirectoryNode;
            return (children.length() > 0);
        }
        
        private function getFileExtension(label:String):String {
        	if (label != null) {
        		var lastDot:int = label.lastIndexOf('.');
        		if (lastDot > 0) {
        			return label.substring(lastDot);
        		}
        	}
        	return null;
        }
	}
}
