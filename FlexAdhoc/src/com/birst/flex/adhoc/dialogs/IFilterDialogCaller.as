package com.birst.flex.adhoc.dialogs
{
	public interface IFilterDialogCaller
	{
		function addNewFilter(newData:Array):void;
		function saveModifiedFilter(newData:Array):void;
	}
}