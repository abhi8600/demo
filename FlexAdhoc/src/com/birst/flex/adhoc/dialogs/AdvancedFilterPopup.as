package com.birst.flex.adhoc.dialogs
{
	public class AdvancedFilterPopup
	{
		public var popUp:AdvancedFilterDialogBox;
		
		public function AdvancedFilterPopup()
		{
			popUp = new AdvancedFilterDialogBox();
		}
		
		public function set isNew(f:Boolean):void {
			popUp.isNew = f;
		}
		
		public function get isNew():Boolean {
			return popUp.isNew;
		}
		
		public function set caller(c:FilterColumnsGridDialogBox):void {
			popUp.caller = c;
		}
		
		public function get caller():FilterColumnsGridDialogBox {
			return popUp.caller;
		}
		
		public function set filterInfo(info:Object):void {
			popUp.filterInfo = info;
		}

		public function setFilter(uiValues:Array):void {
			if(isNew == true){
				caller.addNewFilter(uiValues)
			}else{
				caller.saveModifiedFilter(uiValues);
			}
		}
	}
}

