package com.birst.flex.adhoc.dialogs
{
	public class FilterDialogPopup
	{
		public var popUp:FilterColumnDialogBox;
		
		public function FilterDialogPopup()
		{
			popUp = new FilterColumnDialogBox();
		}
		
		public function set isNew(f:Boolean):void {
			popUp.isNew = f;
		}
		
		public function get isNew():Boolean {
			return popUp.isNew;
		}
		
		public function set caller(c:IFilterDialogCaller):void {
			popUp.caller = c;
		}
		
		public function get caller():IFilterDialogCaller {
			return popUp.caller;
		}
		
		public function set filterInfo(info:Object):void {
			popUp.filterInfo = info;
		}

		public function setFilter(uiValues:Array):void {
			if(isNew == true){
				caller.addNewFilter(uiValues)
			}else{
				caller.saveModifiedFilter(uiValues);
			}
		}
	}
}

