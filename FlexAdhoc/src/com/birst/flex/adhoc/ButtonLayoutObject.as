package com.birst.flex.adhoc
{
	import com.birst.flex.adhoc.dialogs.ButtonPropertiesDialogBox;
	import com.birst.flex.core.AppEvent;
	import com.birst.flex.core.AppEventManager;
	
	import flash.events.ContextMenuEvent;
	import flash.events.MouseEvent;
	import flash.ui.ContextMenuItem;
	import mx.resources.ResourceManager;
	[ResourceBundle('adhoc')]
	
	public class ButtonLayoutObject extends LabelLayoutObject
	{			
		public function ButtonLayoutObject(id:String, layout:XML, l:Layout)
		{
			this.type = "Button";
			this.selectionType = AppEvent.EVT_BUTTON_CHOSEN;
			super(id, layout, l);
		}
		override public function getLabel():String {
			return "b:" + entityXML.Label;
		}
		override protected function getLayoutType():String {
			return AppEvent.EVT_BUTTON_CHOSEN;
		}
		
		override protected function setupContextMenuitems():void {
			super.setupContextMenuitems();
			itemContextMenu.customItems.pop();			
			var colPropertiesItem:ContextMenuItem = new ContextMenuItem(ResourceManager.getInstance().getString("adhoc","optionsMenu_ButtonP"));
			itemContextMenu.customItems.push(colPropertiesItem);
			colPropertiesItem.addEventListener(ContextMenuEvent.MENU_ITEM_SELECT, buttonProperties);
		}
		
		override protected function clickEventHandler(event:MouseEvent):void{
			AppEventManager.fire(new AppEvent(AppEvent.EVT_BUTTON_CHOSEN, 'ButtonLayoutObject', 'clickEventHandler', this.ID, event.ctrlKey));
		} 
		
		//Called by the right click "Remove"
		override protected function deleteItem(event:ContextMenuEvent):void {
			AdHocConfig.instance().removeButton(this.ID.toString());
		}
		
		private function buttonProperties(event:ContextMenuEvent):void {
			invokeMenuAction("buttonProperties", ButtonPropertiesDialogBox);
		}
	}
}