package com.birst.flex.adhoc
{
	import com.birst.flex.adhoc.dialogs.*;
	import com.birst.flex.core.AppEvent;
	import com.birst.flex.core.CoreApp;
	import com.birst.flex.core.IOlapController;
	import com.birst.flex.core.Localization;
	import com.birst.flex.core.MenuItemActions;
	import com.birst.flex.core.Util;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Menu;
	import mx.controls.MenuBar;
	import mx.core.Application;
	import mx.events.MenuEvent;
	
	public class ReportRendererMenubarItem implements IOlapController
	{
		public function ReportRendererMenubarItem()
		{
		}

		public static function addMenuBarItems(mb:MenuBar, col:int, lab:int, label:String, width:String, addMenuItems:Boolean):void {
			var itemList:Array = [ 	{label: label} ];
		
			if(addMenuItems){
				var app:CoreApp = CoreApp(Application.application);
				var isFreeTrial:Boolean = false;
				if (app.hasOwnProperty("isFreeTrial") && app["isFreeTrial"] == true)
					isFreeTrial = true;
				var isOlapCube:Boolean = false;
				if (app.hasOwnProperty("isOlapCube") && app["isOlapCube"] == true)
					isOlapCube = true;
				
				//Column menu items
				var menuItemList:Array = 
				[
						{ labelLocKey: 'MB_rem', tooltipLocKey: "MB_ttRem", fn: deleteColumn, col: col, type: AppEvent.EVT_COLUMN_CHOSEN},
						{ labelLocKey: 'MB_sort', tooltipLocKey: "MB_ttSort", klass: SortColumnDialogBox, col: col, type: AppEvent.EVT_COLUMN_CHOSEN}
				];

				menuItemList.push({ labelLocKey: 'MB_filter', tooltipLocKey: "MB_ttFilter", klass: FilterColumnsGridDialogBox, col: col, type: AppEvent.EVT_COLUMN_CHOSEN});
//				if (!isOlapCube) {
					menuItemList.push({ labelLocKey: 'MB_cond', tooltipLocKey: "MB_ttCond", klass: ConditionalFormattingDataGridDialogBox, col: col, type: AppEvent.EVT_COLUMN_CHOSEN});
//				}
			
				menuItemList.push({ labelLocKey: 'MB_col', tooltipLocKey: "MB_ttCol", klass: ColumnPropertiesDialogBox, col: col, type: AppEvent.EVT_COLUMN_CHOSEN});
				menuItemList.push({ labelLocKey: 'MB_bdr', tooltipLocKey: "MB_ttBdr", klass: BorderDialogBox, col: col,  type: AppEvent.EVT_COLUMN_CHOSEN});
				menuItemList.push({ labelLocKey: 'MB_colSz', tooltipLocKey: "MB_ttColSz", klass: SizeAndPositionDialogBox, col: col,  type: AppEvent.EVT_COLUMN_CHOSEN});
				if (!isOlapCube) {
					menuItemList.push({ labelLocKey: 'MB_colSel', tooltipLocKey: "MB_ttColSel", fn: showColumnSelector, col: col, type: AppEvent.EVT_COLUMN_CHOSEN, modal: false});
				}
					
				//Add label items
				if(lab > -1 ){
					menuItemList.push({ labelLocKey: "MB_label", tooltipLocKey: "MB_ttLabel", fn: labelProperties, col: lab, type: AppEvent.EVT_LABEL_CHOSEN});
					menuItemList.push({	labelLocKey: "MB_labelSz", tooltipLocKey: "MB_ttLabelSz", fn: labelSizeAndPosition, col: lab,  type: AppEvent.EVT_LABEL_CHOSEN});
					menuItemList.push({ labelLocKey: "MB_labelBdr", tooltipLocKey: "MB_ttLabelBdr", fn: labelBorderAndDialog, col: lab,  type: AppEvent.EVT_LABEL_CHOSEN});
				}
				if (isOlapCube) {
					menuItemList.push({ labelLocKey: 'MB_olapControl', tooltipLocKey: 'MB_ttOlapControl', fn: olapControlDialog, col: col, type: AppEvent.EVT_COLUMN_CHOSEN});
				}
				if (isFreeTrial) {
					menuItemList = [
						{ labelLocKey: 'MB_col', tooltipLocKey: "MB_ttCol", klass: ColumnPropertiesDialogBox, col: col, type: AppEvent.EVT_COLUMN_CHOSEN},
						{ labelLocKey: 'MB_rem', tooltipLocKey: "MB_ttRem", fn: deleteColumn, col: col, type: AppEvent.EVT_COLUMN_CHOSEN},
						{ labelLocKey: 'MB_sort', tooltipLocKey: "MB_ttSort", klass: SortColumnDialogBox, col: col, type: AppEvent.EVT_COLUMN_CHOSEN}
					];
					if (!isOlapCube) {
						menuItemList.push({ labelLocKey: 'MB_filter', tooltipLocKey: "MB_ttFilter", klass: FilterColumnsGridDialogBox, col: col, type: AppEvent.EVT_COLUMN_CHOSEN});
					}
						
					if (lab > -1) {
						menuItemList.push({ labelLocKey: "MB_label", tooltipLocKey: "MB_ttLabel", fn: labelProperties, col: lab, type: AppEvent.EVT_LABEL_CHOSEN});
					}
				}
				itemList[0].children = menuItemList;		
			}
			
			Localization.localizeSetDataProvider(mb, new ArrayCollection(itemList));	
			mb.addEventListener(MenuEvent.ITEM_CLICK, itemClicked);
			var menu:Menu = mb.getMenuAt(0);
			if (menu){
				menu.styleName = "menuBarMenu";
				menu.setStyle('textAlign', 'left');
				menu.setStyle('color', '0x000000');
				menu.setStyle('fontWeight', 'normal');
				menu.setStyle('fontStyle', 'plain');
				menu.setStyle('fontSize', '13');
				menu.setStyle('fontFamily', 'Arial');
			}
			
			//We have to explicitly get the menubar item to change the text in the menubar
			var mbi:LabelMenuBarItem = mb.menuBarItems[0] as LabelMenuBarItem;
			if (mbi && width){
				mbi.width = Number(width);
			}
		}
		
		public static function deleteColumn(menuItem:Object):void{
			AdHocConfig.instance().removeColumnLabel(menuItem.col);	
		}
		
		public static function itemClicked(event:MenuEvent): void {
			var menuItem:Object = event.item;
			Application.application.selectedLayoutIds.removeAll();
			Application.application.selectedLayoutIds.addItem(menuItem.col);
			Application.application.selectedLayoutTypes.removeAll();
			Application.application.selectedLayoutTypes.addItem(menuItem.type);
			MenuItemActions.invokeAction(menuItem);
		}
		
		public static function showColumnSelector(menuItem:Object):void{
			if (Application.application.hasOwnProperty('showColumnSelector')){
				Application.application.showColumnSelector(menuItem);
			}
		}
		
		public static function labelProperties(menuItem:Object):void {
			var klass:Class = LabelPropertiesDialogBox;
			var l:XMLList = AdHocConfig.instance().getLabelXML(menuItem.col.toString());
			menuItem.type = AppEvent.EVT_LABEL_CHOSEN;
			if (!l || l.length() == 0) {
				l = AdHocConfig.instance().getColumnXML(menuItem.col.toString());
				if (!l || l.length() == 0)
					return;
					
				klass = ColumnPropertiesDialogBox;
				menuItem.type = AppEvent.EVT_COLUMN_CHOSEN;
				Util.setObjectSelected(menuItem.col, menuItem.type, false);
			}
			var modal:Boolean = menuItem.hasOwnProperty("modal") ? Boolean(menuItem.modal) : true;
			MenuItemActions.popUp(klass, menuItem, modal);
		}
		
		public static function labelSizeAndPosition(menuItem:Object):void {
			var klass:Class = SizeAndPositionDialogBox;
			var l:XMLList = AdHocConfig.instance().getLabelXML(menuItem.col.toString());
			menuItem.type = AppEvent.EVT_LABEL_CHOSEN;
			if (!l || l.length() == 0) {
				l = AdHocConfig.instance().getColumnXML(menuItem.col.toString());
				if (!l || l.length() == 0)
					return;
					
				menuItem.type = AppEvent.EVT_COLUMN_CHOSEN;
				Util.setObjectSelected(menuItem.col, menuItem.type, false);
			}
			var modal:Boolean = menuItem.hasOwnProperty("modal") ? Boolean(menuItem.modal) : true;
			MenuItemActions.popUp(klass, menuItem, modal);
		}			
		public static function labelBorderAndDialog(menuItem:Object):void {
			var klass:Class = BorderDialogBox;
			var l:XMLList = AdHocConfig.instance().getLabelXML(menuItem.col.toString());
			menuItem.type = AppEvent.EVT_LABEL_CHOSEN;
			if (!l || l.length() == 0) {
				l = AdHocConfig.instance().getColumnXML(menuItem.col.toString());
				if (!l || l.length() == 0)
					return;
					
				menuItem.type = AppEvent.EVT_COLUMN_CHOSEN;
				Util.setObjectSelected(menuItem.col, menuItem.type, false);
			}
			var modal:Boolean = menuItem.hasOwnProperty("modal") ? Boolean(menuItem.modal) : true;
			MenuItemActions.popUp(klass, menuItem, modal);
		}
		
		public static function olapControlDialog(menuItem:Object):void {
			var klass:Class = OlapControlDialogBox;
           	var selectedColumns:XMLList = AdHocConfig.instance().getSelectedLayoutObject();
           	delete menuItem.olapColumns;
           	if (selectedColumns.length() > 0) {
            	var columns:XMLList = selectedColumns[0].AdditionalOlapColumns.Column;
            	if (columns.length() > 0)
            		menuItem.olapColumns = columns;
	            else {
	            	var x:XML = new XML(<AdditionalOlapColumns/>);
	            	x.@Column =  String(selectedColumns[0].Column);
	            	x.@TechnicalName = String(selectedColumns[0].TechnicalName);
	            	x.@ExpressionType = 'SELECTED';
	            	menuItem.olapColumns = new XMLList(x);
	            }
            }
            
            var controller:ReportRendererMenubarItem = new ReportRendererMenubarItem();
            controller.setSelectedColumns(selectedColumns);
            menuItem.controller = controller;
			var modal:Boolean = menuItem.hasOwnProperty("modal") ? Boolean(menuItem.modal) : true;
			MenuItemActions.popUp(klass, menuItem, modal);
		}
		
		private var _selectedColumns:XMLList;
		private function setSelectedColumns(selCols:XMLList):void {
			_selectedColumns = selCols;
		}
		public function saveOlapColumns(olapColumns:XML):void {
        	if (_selectedColumns.length() > 0) {
        		delete _selectedColumns[0].AdditionalOlapColumns;
        		_selectedColumns[0].appendChild(olapColumns);
        	}
        	
        	AdHocConfig.instance().asyncSaveConfigToServer("OlapControlDialogBox", "saveButtonClickHandler");
		}			
	}
}