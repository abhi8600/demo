package com.birst.flex.adhoc
{
	import com.birst.flex.core.AppEvent;
	import com.birst.flex.core.AppEventManager;
	import com.birst.flex.core.ReportXml;
	import com.birst.flex.core.Tracer;
	import com.birst.flex.core.Util;
	
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.net.SharedObject;
	
	import mx.collections.ArrayCollection;
	import mx.containers.Canvas;
	import mx.core.Application;
	import mx.core.UIComponent;
	import mx.events.ResizeEvent;
	import mx.resources.ResourceManager;

	public class Layout
	{
		public static const COMPONENT:String = "Layout";
		
		[ResourceBundle('adhoc')]

		private static var NUM_DEFAULT_BANDS:int = 4;
		private var pageWidth:int = 600;
		private var columnArea:ColumnArea;
		private var canvas: Canvas;
		private var bands: ArrayCollection;
		private var objects: ArrayCollection;
		private var zoom: Number;
		private var snap: int;
		private var showgrid: Boolean;
		private var rulerunit: int;
		private var parentCanvas:Canvas;
		private var prevStageX:Number;
		private var prevStageY:Number;
		private var isDragging:Boolean = false;
	
		
		public function Layout(_canvas: Canvas)
		{
			parentCanvas = _canvas;
			this.canvas = new Canvas();
			_canvas.addChild(this.canvas);
			zoom = 1;
			snap = 1;
			rulerunit = 0;
			showgrid = false;
			loadPreferences();
			_canvas.addEventListener(ResizeEvent.RESIZE, stageResize, false, 0, false);
			AppEventManager.on(AppEvent.EVT_CONFIG_CHANGED, configChanged);
			canvas.addEventListener(MouseEvent.MOUSE_UP, stopDragging);
		}
		
		private function configChanged(event:AppEvent):void {
			if (event.callerOperation == 'setBandProperties') {
				// redo bands
				var report:XML = AdHocConfig.instance().getConfig();
				var numGroups:uint = uint(report.NumGroups);
				var currentNumGroups:uint = (bands.length - NUM_DEFAULT_BANDS) / 2;
				var j:int;
				if (currentNumGroups > numGroups) {
					// have to remove bands
					var i:int = 1 + numGroups;
					for (j = 0; j < currentNumGroups - numGroups; j++) {
						bands.removeItemAt(i);
					}
					// go past detail and group footers that are still here
					i = i + 1 + numGroups;
					for (j = 0; j < currentNumGroups - numGroups; j++) {
						bands.removeItemAt(i);
					}
				}
				else if (bands.length < NUM_DEFAULT_BANDS + numGroups) {
					// have to add bands
					var name:String;
					i = 1 + currentNumGroups;
					for (j = currentNumGroups; j < numGroups; j++) {
						name = "GroupHeader" + i;
						bands.addItemAt(new Band(name, name, this, 0), 
							i++);
					}
					i = i + 1 + currentNumGroups;
					for (j = currentNumGroups; j < numGroups; j++) {
						name = "GroupFooter" + i;
						bands.addItemAt(new Band(name, name, this, 0), 
							i++);
					}
				}
				
			}
		}
		
		private function storePreferences() : void
		{
			var sharedObject:SharedObject;
			sharedObject = SharedObject.getLocal("birst");
			sharedObject.data.zoom = zoom;
			sharedObject.data.snap = snap;
			sharedObject.data.showgrid = showgrid;
			
			try {
				var status:String = sharedObject.flush();
			}
			catch (error:Error) {
			}
		}
		private function loadPreferences() : void
		{
			var sharedObject:SharedObject;
			sharedObject = SharedObject.getLocal("birst");
			if (sharedObject.data.zoom != null) {
				zoom = sharedObject.data.zoom;
			}
			if (sharedObject.data.snap != null) {
				snap = sharedObject.data.snap;
			}
			if (sharedObject.data.showgrid != null) {
				showgrid = sharedObject.data.showgrid; 
			}
		}
		
		public function set Report(report: XML): void
		{
			//clear out the canvas
			cleanOldReport();
				
			objects = new ArrayCollection();
			bands = new ArrayCollection();
			columnArea = new ColumnArea(this,40);
			bands.addItem(columnArea);
			//bands.addItem(new Band(ResourceManager.getInstance().getString("adhoc","CP_columnArea"), this, 40));
			pageWidth = int(report.PageWidth);

			var bandnum: int = 1;
			var numGroups:uint = uint(report.NumGroups);
			var i:int;
			var name:String;
			
			bands.addItem(new Band(ResourceManager.getInstance().getString("adhoc","BP_pageHeader"), 'Page Header', this, 0));
			bands.addItem(new Band(ResourceManager.getInstance().getString("adhoc","BP_pageTitle"), 'Title', this, 0));
			bands.addItem(new Band(ResourceManager.getInstance().getString("adhoc","BP_header"), 'Header', this, 0));
			for (i = 0; i < numGroups; i++) {
				name = "GroupHeader" + i;
				bands.addItem(new Band(name, name, this, 0));
			}
			bands.addItem(new Band(ResourceManager.getInstance().getString("adhoc","BP_pageDetail"), 'Detail', this, 0));
			for (i = numGroups - 1; i >= 0; i--) {
				name = "GroupFooter" + i;
				bands.addItem(new Band(name, name, this, 0));
			}
			bands.addItem(new Band(ResourceManager.getInstance().getString("adhoc","BP_pageSummary"), 'Summary', this, 0));
			bands.addItem(new Band(ResourceManager.getInstance().getString("adhoc","BP_pageFooter"), 'Page Footer', this, 0));
			drawBands();
			
			var columns:XMLList = report.Entities[ReportXml.COLUMN];
			var labels:XMLList = report.Entities[ReportXml.LABEL];
			var images:XMLList = report.Entities[ReportXml.IMAGE];
			var subreports:XMLList = report.Entities[ReportXml.SUBREPORT];
			var charts:XMLList = report.Entities[ReportXml.CHART];
			var rects:XMLList = report.Entities[ReportXml.RECTANGLE];
			var buttons:XMLList = report.Entities[ReportXml.BUTTON];
			
			for (i = 0; i < rects.length(); i++) {
				var rect:XML = rects[i];
				objects.addItem(new RectangleLayoutObject(rect.ReportIndex, rect, this));
			}
			
			for (i = 0; i < buttons.length(); i++) {
				var button:XML = buttons[i];
				objects.addItem(new ButtonLayoutObject(button.ReportIndex, button, this));
			}
			
			for (var y:int = 0; y < labels.length(); y++){
				var label:XML = labels[y];
				objects.addItem(new LabelLayoutObject(label.ReportIndex, label, this));
			}
			
			for (i = 0; i < columns.length(); i++){
				var col:XML = columns[i];
				objects.addItem(new ColumnLayoutObject(col.reportIndex, col, this));
			}
			
			for (var x:int = 0; x < images.length(); x++){
				var img:XML = images[x];
				objects.addItem(new ImageLayoutObject(img.reportIndex, img, this));
			}
			for (var j:int = 0; j < subreports.length(); j++){
				var sub:XML = subreports[j];
				objects.addItem(new SubreportLayoutObject(sub.reportIndex, sub, this));
			}
			for (var c:int = 0; c < charts.length(); c++) {
				var cht:XML = charts[c];
				objects.addItem(new ChartLayoutObject(cht.reportIndex, cht, this));
			}
			updateLayoutObjects();
		}
		
		private function cleanOldReport():void {
			canvas.removeAllChildren();
			if (objects != null) {
				for (var i:int = 0; i < objects.length; i++) {
					var lo:LayoutObject = objects[i] as LayoutObject;
					lo.cleanUp();
				}
			}
		}
		
		public function get Xml() : XML {
			return AdHocConfig.instance().getConfig();
		}
		public function get ScaleFactor() : Number
		{
			return Application.application.scaleFactor;
		}
		
		public function getBand(bandName:String) : Band {
			for (var i:int = 0; i < bands.length; i++) {
				var b:Band = bands.getItemAt(i) as Band;
				if (b.Name == bandName) {
					return b;
				}
			} 
			return this.columnArea;
		}
		
		public function get Bands() :ArrayCollection {
			return this.bands;
		}
		
		public function selectItemsInRectangle(r:Rectangle):void {
			Util.clearSelectedObjects();
			var rectLeft:uint = Math.min(r.left, r.right);
			var rectTop:uint = Math.min(r.top, r.bottom);
			var pt:Point = new Point(rectLeft, rectTop);
			var rectRight:uint = rectLeft + Math.abs(r.width);
			var rectBottom:uint = rectTop + Math.abs(r.height);
				
			for each (var o: LayoutObject in objects) {
				var c:UIComponent = o.component;
				var oLeft:Number = c.x;
				var oTop:Number = c.y;
				var oWidth:Number = c.width;
				var oHeight:Number= c.height;
				
				if ((oLeft < rectLeft && oLeft + oWidth > rectLeft) ||
					(oLeft >= rectLeft && oLeft < rectRight)) {
						// horizontally overlaps
						if ((oTop < rectTop && oTop + oHeight > rectTop) ||
							(oTop >= rectTop && oTop < rectBottom)) {
								// vertically overlaps
								var reportId:String = o.entityXML.ReportIndex;
								var type:String;
								if (o is ButtonLayoutObject)
									type = AppEvent.EVT_BUTTON_CHOSEN;
								else if (o is ChartLayoutObject)
									type = AppEvent.EVT_CHART_CHOSEN;
								else if (o is ColumnLayoutObject)
									type = AppEvent.EVT_COLUMN_CHOSEN;
								else if (o is ImageLayoutObject)
									type = AppEvent.EVT_IMAGE_CHOSEN;
								else if (o is LabelLayoutObject)
									type = AppEvent.EVT_LABEL_CHOSEN;
								else if (o is RectangleLayoutObject)
									type = AppEvent.EVT_RECTANGLE_CHOSEN;
								else if (o is SubreportLayoutObject)
									type = AppEvent.EVT_SUBREPORT_CHOSEN;
								Util.setObjectSelected(reportId, type, true); 
							} 
					}
			}
		}
		
		public function overlaps(lo : LayoutObject) : Boolean
		{
			if (lo.BandName == ColumnArea.NAME || lo.BandName == "" || lo.entityXML.showInReport.toString() == 'false') {
				return false;
			}
			// if outside band it overlaps
			var left:int = uint(lo.entityXML.Left);
			var top:int = uint(lo.entityXML.Top);
			var width:int = uint(lo.entityXML.Width);
			var height:int = uint(lo.entityXML.Height);
			if (left < 0 || left + width > this.pageWidth ||
				top < 0) {
					return true;
				}
				
			if (top + height > getBand(lo.BandName).height) {
				return true;
			}
			
			for each (var o: LayoutObject in this.objects) {
				if (lo == o || o.entityXML.showInReport.toString() == 'false') {
					continue;
				}
				if (lo.BandName == o.BandName) {
					var oLeft:uint = uint(o.entityXML.Left);
					var oTop:uint  = uint(o.entityXML.Top);
					var oWidth:uint = uint(o.entityXML.Width);
					var oHeight:uint = uint(o.entityXML.Height);
					if ((left <= oLeft && left + width > oLeft) ||
					  (left >= oLeft && left < oLeft + oWidth)) {
					  	// overlaps x.  check out y
					  	if ((top <= oTop && top + height > oTop) ||
					  		(top >= oTop && top < oTop + oHeight)) {
					  			return true;
					  		}  
					  }
				}
			}
			
			return false;
		}
		
		
		
		internal function updateLayoutObjects(): void
		{
			var i: int;
			this.columnArea.updateLayoutObjects();
			for(i = 0; i < bands.length; i++)
			{
				bands.getItemAt(i).updateLayoutObjects();
			} 			
		}
		
		public function stageResize(event: ResizeEvent): void
		{
			if (canvas == null || canvas.stage == null)
				return;
				
			if (columnArea != null){
				drawBands();
			}
		}
		
		public function get LayoutCanvas(): Canvas
		{
			return canvas;
		}

		public function drawBands(): void
		{
			parentCanvas.invalidateDisplayList();
			
			var lmaxWidth:int = columnArea.width;
			var lmaxHeight:int = 0;
			var i: int;
			for (i = 0; i < bands.length; i++) {
				lmaxHeight = lmaxHeight + (bands.getItemAt(i) as Band).height;
			}
			lmaxHeight = lmaxHeight * this.Zoom;
			canvas.maxHeight = lmaxHeight;
			canvas.maxWidth = lmaxWidth * this.Zoom;
			canvas.parent.width = canvas.parent.parent.width;
			var parentHeight:int = (canvas.parent.parent.height - canvas.y);
			if (lmaxHeight < parentHeight) {
				lmaxHeight = parentHeight;
			}
			canvas.height = lmaxHeight;
			
			canvas.graphics.clear();
			
			var curY:int = 0;
			for(i = 0; i < bands.length; i++)
			{
				curY += bands.getItemAt(i).draw(curY);
			} 
			for each(var lo: LayoutObject in objects)
			{
				lo.draw();
			}
			columnArea.layoutObjects();
		}
		
		public function set Zoom(factor: Number): void
		{
			this.zoom = factor;
			storePreferences();
			drawBands();
			updateLayoutObjects();
		}
		
		public function set Snap(value: int): void
		{
			this.snap = value;
			storePreferences();
			if (showgrid)
				drawBands();
		}
		
		public function set ShowGrid(value: Boolean): void
		{
			this.showgrid = value;
			storePreferences();
			drawBands();
		}
		
		public function getRawZoom(): Number {
			return zoom;
		}
		public function get Zoom(): Number
		{
			return zoom * this.ScaleFactor;
		}
		
		public function get Snap(): int
		{
			return snap;
		}
		
		public function get ShowGrid(): Boolean
		{
			return showgrid;
		}
		
		public function get Ruler(): Boolean
		{
			return false;
		}
		
		public function get RulerUnit(): int
		{
			return rulerunit;
		}
		
		public function get Objects(): ArrayCollection
		{
			return this.objects;
		}
		
		public function removeItem(item: LayoutObject): void
		{
			var index: int = objects.getItemIndex(item);
			objects.removeItemAt(index);
			drawBands();
		} 
		public function startedDraggingItem(event:MouseEvent):void
		{
			prevStageX = event.stageX;
			prevStageY = event.stageY;
			var i: int;
			for(i = 0; i < bands.length; i++)
			{
				(Band)(bands.getItemAt(i)).startedDraggingItem();
			}
			 
			// tell Flash Player to start listening for the mouseMove event
			canvas.addEventListener(MouseEvent.MOUSE_MOVE, drag);
			
			var ids:ArrayCollection = Util.getSelectedLayoutIds();
			for each(var lo: LayoutObject in objects)
			{
				if (ids.contains(lo.ID.toString()))
					lo.startedDragging(event.stageX, event.stageY);
			}
			
			event.stopPropagation();
		}
		private function drag(event:MouseEvent):void
		{
			if (event.buttonDown == false) {
				Tracer.debug('drag calling stopDragging', this);
				stopDragging(event);
				return;
			}
			
			if (isDragging == false && event.ctrlKey) {
				// copy the items and drag them instead
				Util.copyAndSelectSelectedLayoutEntities();
				var selectedEntities:Array = Util.getSelectedEntities();
				for (var i:int = 0; i < selectedEntities.length; i++) {
					var entity:XML = XML(selectedEntities[i]);
					var reportId:String = entity.ReportIndex;
					var l:LayoutObject = null;
					if (entity.localName() == ReportXml.COLUMN) {
						l = new ColumnLayoutObject(reportId, entity, this);
					}
					else if (entity.localName() == ReportXml.LABEL) {
						l = new LabelLayoutObject(reportId, entity, this);
					}
					else if (entity.localName() == ReportXml.SUBREPORT) {
						l = new SubreportLayoutObject(reportId, entity, this);
					}
					else if (entity.localName() == ReportXml.IMAGE) {
						l = new ImageLayoutObject(reportId, entity, this);
					}
					else if (entity.localName() == ReportXml.CHART) {
						l = new ChartLayoutObject(reportId, entity, this);
					}
					else if (entity.localName() == ReportXml.RECTANGLE) {
						l = new RectangleLayoutObject(reportId, entity, this);
					}
					else if (entity.localName() == ReportXml.BUTTON) {
						l = new ButtonLayoutObject(reportId, entity, this);
					}
					objects.addItem(l);
					l.startedDragging(event.stageX, event.stageY);
				}
				updateLayoutObjects();
			}
			
			isDragging = true;
			var ids:ArrayCollection = Util.getSelectedLayoutIds();
			for each(var lo: LayoutObject in objects)
			{
				if (ids.contains(lo.ID.toString()))
					lo.drag(event);
			}
			
			event.updateAfterEvent();
		}
		public function stopDragging(event:MouseEvent):void
		{
			if (!isDragging)
				return;
				
			isDragging = false;
			
			Tracer.debug('stopDragging at y: ' + event.stageY + ' x: ' + event.stageX, this);
			
			// Tell Flash Player to stop listening for the mouseMove event.
			canvas.removeEventListener(MouseEvent.MOUSE_MOVE, drag);
			
			//Stop right here if we're not going anywhere.
			if (prevStageX == event.stageX && prevStageY == event.stageY)
				return;

			var ids:ArrayCollection = Util.getSelectedLayoutIds();
			for each(var lo: LayoutObject in objects)
			{
				if (ids.contains(lo.ID.toString()))
					lo.stopDragging(event);
			}
			
			stopDraggingItem();
			event.stopPropagation();
			AdHocConfig.instance().asyncSaveConfigToServer(COMPONENT, 'stopDragging');
		}
		public function stopDraggingItem():void
		{
			var i: int;
			for(i = 0; i < bands.length; i++)
			{
				(Band)(bands.getItemAt(i)).stopDraggingItem();
			} 
		}
		public function get SelectedObjects(): Array
		{
			var result: Array = new Array();
			var ids:ArrayCollection = Util.getSelectedLayoutIds();
			for each(var lo: LayoutObject in objects)
			{
				if (ids.contains(lo.entityXML.ReportIndex))
					result.push(lo.ID);
			}
			return result;
		}
		
	}
}