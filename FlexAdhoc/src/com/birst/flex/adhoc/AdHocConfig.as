package com.birst.flex.adhoc
{
	import com.birst.flex.core.AppEvent;
	import com.birst.flex.core.AppEventManager;
	import com.birst.flex.core.MenuItemActions;
	import com.birst.flex.core.ReportXml;
	import com.birst.flex.core.Util;
	import com.birst.flex.webservice.Components;
	import com.birst.flex.webservice.WebServiceClient;
	import com.birst.flex.webservice.WebServiceResult;
	
	import mx.collections.ArrayCollection;
	import mx.collections.XMLListCollection;
	import mx.core.Application;
	import mx.managers.BrowserManager;
	import mx.managers.IBrowserManager;
	import mx.rpc.events.ResultEvent;
	
	public class AdHocConfig
	{
		private static var browser:IBrowserManager;	
		private static var _inst:AdHocConfig;
		
		protected var _lastWorkingXml:XML = null;
		protected var _config:ReportXml = new ReportXml();
		protected var _historyStack:Array = [];
		protected var _ignoreHistoryUpdates:Boolean = false;
		protected var _needToUpdateServer:Boolean = false;
		protected var _needToSaveFile:Boolean = false;
		protected var _doNotUpdateServer:Boolean = false;
		protected var _chartType:String = null;
			
		//Don't ever call this directly				
		public function AdHocConfig()
		{
		}

		public function get historyCount():Number{
			return _historyStack.length;	
		}
		
		public function clearHistory():void{
			_historyStack = [];
		}	
		
		public function undo():Boolean {
			var cfg:XML = _historyStack.pop() as XML;
			if(cfg){
				_config.xml = cfg;
				return true;
			}	
			return false;
		}	
		
		public function saveCurrentConfig():void{
			if (_config && _ignoreHistoryUpdates == false){
				_lastWorkingXml = _config.xml;
				
				if(_historyStack.length >= 10){
					_historyStack.shift();
				}	
				
				_historyStack.push(_config.xml.copy());
			}
		}
		
		public function ignoreConfigChanges(b:Boolean):void {
			_ignoreHistoryUpdates = b;
		}
		
		public function ignoreLastConfig():void {
			if (_historyStack.length > 0)
				_historyStack.pop();
		}
		
		public function revertToSavedConfig():void{
			if(_lastWorkingXml){
				_historyStack.pop();
				setConfig(_lastWorkingXml);
				asyncSaveConfigToServer('AdhocConfig', 'revertToSavedConfig');
				_lastWorkingXml = null;
			}
		}
		
		public static function instance():AdHocConfig{
			if(!_inst){
				_inst = new AdHocConfig();
			}
			return _inst;
		}
		
		public static function getBrowserManager():IBrowserManager {
			if (browser == null) {
				browser = BrowserManager.getInstance();
				browser.init();
			}
			return browser;
		}
		
		public function setConfig(conf:XML, clearConfigHistory:Boolean=false):void{
			if(clearConfigHistory){
				clearEmptyConfig();	
				clearHistory();
			}else{
				saveCurrentConfig();
			}
			_config.xml = conf;
		}
		
		public function set updateToServer(b:Boolean):void {
			_doNotUpdateServer = !b;
		}
		
		public function get updateToServer():Boolean {
			return !_doNotUpdateServer;
		}
		
		public function set fileSaved(b:Boolean):void {
			_needToSaveFile = ! b;
			_needToUpdateServer = ! b;
		}
		public function get fileSaved():Boolean {
			return ! _needToSaveFile;
		}
		
		public function asyncSaveConfigToServer(caller:String, method:String, data:Object = null):void{
			_needToSaveFile = true;
			_needToUpdateServer = true;
			if (_doNotUpdateServer)
				return;
				
			var ws:WebServiceClient = new WebServiceClient("Adhoc", "setAdhocReport", caller, method, data);
			ws.setResultListener(_asyncSaveConfigToServerHandler);
			ws.execute(_config.toXMLString());
		}
		
		public function asyncUpdateConfig(caller:String, method:String):void{
			saveCurrentConfig();	
			
			var ws:WebServiceClient = new WebServiceClient("Adhoc", "getAdhocReport", caller, method);
			ws.setResultListener(updateConfigResultHandler);
			ws.execute();
		}

		private function _asyncSaveConfigToServerHandler(event:ResultEvent):void{
			var result:WebServiceResult = WebServiceResult.parse(event);
			var component:String = result.getCallerComponent();
			var operation:String = result.getCallerOperation();
			var data:Object = result.getCallerData();
			
			if (!result.isSuccess()){
				Util.localeAlertError('AC_saveFailed', result);
				_config.xml = new XML(result.getResult().children().toXMLString());
				AppEventManager.fire(new AppEvent(AppEvent.EVT_CONFIG_SAVE_FAILED, component, operation, data));
				return;
			}
			_needToUpdateServer = false;
			_config.xml = new XML(result.getResult().children().toXMLString());
			AppEventManager.fire(new AppEvent(AppEvent.EVT_CONFIG_SAVE_SUCCESS, component, operation, data));	
		}
		
		public function updateConfigResultHandler(event:ResultEvent):void{
			var result:WebServiceResult = WebServiceResult.parse(event);
	
			if (result.isSuccess() != true){
				Util.localeAlertError('AC_failUpdate', result);
				return;
			}
		
			var resultXML:XMLList = result.getResult();
			var topResult:XML = resultXML[0];
			_config.xml = (topResult[ReportXml.ROOT])[0];
			
			AppEventManager.dispatchAppEvent(new AppEvent(AppEvent.EVT_CONFIG_CHANGED, result.getCallerComponent(), result.getCallerOperation(), AdHocConfig));
		}
		
		public function setPageLayoutConfig(conf:Array):void{
			saveCurrentConfig();
			_config.setPageLayoutConfig(conf);
			asyncSaveConfigToServer(Components.ADHOC_APP, 'setPageLayoutConfig');
		}
		
		public function getPageLayoutConfig():Array{
			return _config.getPageLayoutConfig();
		}
		
		public function setReportPropertiesConfig(conf:Array):void{
			saveCurrentConfig();
			_config.setReportPropertiesConfig(conf);
			asyncSaveConfigToServer(Components.ADHOC_APP, 'setPropertiesConfig');
		}
		
		/**
		 * Convert a key-value pair to the ChartOptions tag values of form key1=value1;key2=value2;...
		 */
		public function setChartsConfig(conf:Array):void{
			saveCurrentConfig();
			_config.setChartsConfig(conf);
			asyncSaveConfigToServer(Components.ADHOC_APP, 'setChartsConfig');
		}

		/**
		 * Parses the ChartOptions tag values from key1=value1;key2=value2;...
		 * to an hash Array of array['key1'] = value1
		 */
		public function getChartsConfig():Array{
			return _config.getChartsConfig();
		}
		
		public function newChartsSeriesCategoriesLabel(oldLabel:String, newLabel:String):void{
			_config.newChartsSeriesCategoriesLabel(oldLabel, newLabel);
		}
		
		public function getReportPropertiesConfig():Array{
			return _config.getReportPropertiesConfig();
		}
		
		public function setLabelProperties(labelId:String, properties:Array):void{
			saveCurrentConfig();
			_config.setLabelProperties(labelId, properties);
			asyncSaveConfigToServer(Components.ADHOC_APP, 'setLabelProperties');
		}
		
		public function setButtonProperties(id:String, properties:Array):void {
			saveCurrentConfig();
			_config.setButtonProperties(id, properties);
			asyncSaveConfigToServer(Components.ADHOC_APP, 'setButtonProperties');
		}
		
		public function setRectangleProperties(id:String, properties:Array):void {
			saveCurrentConfig();
			_config.setRectangleProperties(id, properties);
			asyncSaveConfigToServer(Components.ADHOC_APP, 'setButtonProperties');
		}
		
		public function getLabelNameByColumnId(columnId:String):String{
			return _config.getLabelNameByColumnId(columnId);
		}
		
		public function getLabelNameByColumnXML(colXML:XML):String{
			return _config.getLabelNameByColumnXML(colXML);
		}
		
		public function getLabelName(labelId:String):String{
			return _config.getLabelName(labelId);
		}
		
		public function getLabelXML(labelId:String):XMLList{
			return _config.getLabelXML(labelId);
		}
		
		public function getRectangleXML(id:String):XMLList {
			return _config.getRectangleXML(id);
		}
		
		public function getAdhocButtonXML(id:String):XMLList {
			return _config.getAdhocButtonXML(id);
		}
		
		public function getImageXML(imageId:String):XMLList{
			return _config.getImageXML(imageId);
		}
		
		public function getSubreportXML(reportId:String):XMLList{
			return _config.getSubreportXML(reportId);
		}
		
		public function getChartXML(reportId:String):XMLList{
			return _config.getChartXML(reportId);
		}
		
		public static var defaultChart:String = "<com.successmetricsinc.adhoc.AdhocChart><LeftBorder>None</LeftBorder><RightBorder>None</RightBorder><TopBorder>None</TopBorder><BottomBorder>None</BottomBorder><ReportIndex>-1</ReportIndex><ForegroundColor>000000</ForegroundColor><BackgroundColor>FFFFFF</BackgroundColor><BorderColor>ffffff</BorderColor><Width>300</Width><Height>250</Height><Left>0</Left><Top>0</Top><Band>Title</Band><GroupNumber>0</GroupNumber><Font>Arial-PLAIN-8</Font><Alignment>Left</Alignment><PositionType>Float</PositionType><StretchType>No Stretch</StretchType><ShowBorder>false</ShowBorder></com.successmetricsinc.adhoc.AdhocChart>";
		public function addChart(klass:Class, menuItem: Object = null): void {
			MenuItemActions.popUp(klass, {chart : XML(defaultChart) });				
		}
		
		public static var defaultRectangle:String = "<com.successmetricsinc.adhoc.AdhocRectangle><ReportIndex>-1</ReportIndex><ForegroundColor>000000</ForegroundColor><BackgroundColor>FFFFFF</BackgroundColor><BorderColor>ffffff</BorderColor><Width>300</Width><Height>250</Height><Left>0</Left><Top>0</Top><Band>Title</Band><GroupNumber>0</GroupNumber></com.successmetricsinc.adhoc.AdhocRectangle>";
		public function addRectangle(menuItem:Object = null):void {
			addNewElement(defaultRectangle);
		}
		
		public function addNewElement(elString:String):String {
			return addNewElementXML(new XML(elString));
		}
		
		public function addNewElementXML(element:XML):String{
			var nextId:uint = AdHocConfig.instance().getNextElementId();
			element.ReportIndex = nextId;
			AdHocConfig.instance().addElement(element);
			return nextId.toString();
		}
		
		public function getDefaultChartId():String {
			return _config.getDefaultChartId();
		}
		
		public function setDefaultChartId(id:String):void {
			_config.setDefaultChartId(id);
		}
		
		public function setColumnProperties(columnId:String, properties:Array):void{
			saveCurrentConfig();
			_config.setColumnProperties(columnId, properties);
			asyncSaveConfigToServer(Components.ADHOC_APP, 'setColumnProperties');
		}
		
		public function setChartProperties(columnId:String, properties:Array):void {
			saveCurrentConfig();
			_config.setChartProperties(columnId, properties);
			asyncSaveConfigToServer(Components.ADHOC_APP, 'setChartProperties');
		}
		
		public function getRootEntity():XMLList{
			return _config.getRootEntity();
		}
	
		public function getRootFilters():XMLList{
			return _config.getRootFilters();
		}	
		
		//Returns us the selected layout object in the Layout module. It returns an empty xml object
		//if it doesn't have any object selected
		public function getSelectedLayoutObject():XMLList{
			if (appHasLayoutIdType() == true){
				var ret:XMLListCollection = new XMLListCollection();
				for (var i: uint = 0; i < Application.application.selectedLayoutIds.length; i++) {
					if (i >= Application.application.selectedLayoutTypes.length)
						break;
					var id:String = Application.application.selectedLayoutIds.getItemAt(i);
					var type:String = Application.application.selectedLayoutTypes.getItemAt(i);	
					if (type == AppEvent.EVT_COLUMN_CHOSEN)
						ret.addItem(getColumnXML(id));
					if (type == AppEvent.EVT_LABEL_CHOSEN)
						ret.addItem(getLabelXML(id));
					if (type == AppEvent.EVT_IMAGE_CHOSEN)
						ret.addItem(getImageXML(id));	
					if (type == AppEvent.EVT_SUBREPORT_CHOSEN)
						ret.addItem(getSubreportXML(id));	
					if (type == AppEvent.EVT_CHART_CHOSEN)
						ret.addItem(getChartXML(id));
					if (type == AppEvent.EVT_RECTANGLE_CHOSEN)
						ret.addItem(getRectangleXML(id));
					if (type == AppEvent.EVT_BUTTON_CHOSEN)
						ret.addItem(getAdhocButtonXML(id));
				}
				return ret.source;
			}
			return new XMLList();
		}
		
		public function setSelectedLayoutObjectProperties(properties:Array, caller:String, operation:String):void{
			saveCurrentConfig();
			_config.setSelectedLayoutObjectProperties(properties, getSelectedLayoutObject());
			asyncSaveConfigToServer(caller, operation);
		}
		
		//Gets the selected layout object's size and position properties
		public function getSelectedLayoutObjectSizePosition():Array{
			var layoutObj:XMLList = getSelectedLayoutObject();
			return _config.getSelectedLayoutObjectSizePosition(layoutObj);
		}
		
		public function getSelectedLayoutShowIf():Array{
			var layoutObj:XMLList = getSelectedLayoutObject();
			return _config.getSelectedLayoutShowIf(layoutObj);
		}
		
		public function getSelectedLayoutHyperlink():Array{
			var layoutObj:XMLList = getSelectedLayoutObject();
			return _config.getSelectedLayoutHyperlink(layoutObj);
		}
		
		public function getSelectedLayoutObjectBorder():Array{
			var layoutObj:XMLList = getSelectedLayoutObject();
			return _config.getSelectedLayoutObjectBorder(layoutObj);
		}
		
		//Gets the selected column in the layout object
		public function getSelectedColumnProperties():Array{
			return getSelectedLayoutProperties("column");
		}
		
		public function isOlapCubeReport():Boolean {
			var columns:XMLList = _config.getAllColumns();
			for (var i:uint = 0; i < columns.length(); i++) {
				if (String(columns[i].Hierarchy))
					return true;
			}
			
			return false;
		}
		
		//Retrieves the selected label properties in the Layout section
		public function getSelectedLabelProperties():Array{
			return getSelectedLayoutProperties("label");
		}
		
		public function getSelectedButtonProperties():Array {
			var selected:XMLList = getSelectedLayoutObject();
			return _config.getSelectedButtonPropertes(selected);
		}
		
		public function getSelectedRectangleProperties():Array {
			var selected:XMLList = getSelectedLayoutObject();
			return _config.getSelectedRectangleProperties(selected);
		}
		
		//Gets the selected column in the layout object
		public function getSelectedLayoutProperties(type:String):Array{
			var selected:XMLList = getSelectedLayoutObject();
			return _config.getSelectedLayoutProperties(type, selected);
		}
		
		private function appHasLayoutIdType():Boolean{
			var app:Object = Application.application;
			return app.hasOwnProperty('selectedLayoutIds') && app.hasOwnProperty('selectedLayoutTypes');
		}
		
		public function setColumnConditionalFormats(formats:Array):void{
			saveCurrentConfig();
			_config.setColumnConditionalFormats(formats);
			asyncSaveConfigToServer(Components.ADHOC_APP, 'setColumnConditionalFormats');
		}
		
		public function getColumnConditionalFormats(columnId:String, conditionalFormatId:String):XMLList{
			return _config.getColumnConditionalFormats(columnId, conditionalFormatId);
		}
		
		public function setColumnPivot(columnIndex:String, isPivot:Boolean, caller:String, method:String):void{
			Application.application.trackEvent("setColumnPivot");
			saveCurrentConfig();
			var numCols:int = _config.setColumnPivot(columnIndex, isPivot);
			if(numCols > 0){
				asyncSaveConfigToServer(caller, method, columnIndex);	
			}
		}
	
		public function moveColumnToAfter(columnIndex:String, columnAfter:String):Boolean {
			// Application.application.trackEvent("moveColumnToAfter");		
			return _config.moveColumnToAfter(columnIndex, columnAfter);
		}
		
		public function getColumnByType(type:String):XMLList {
			return _config.getColumnByType(type);
		}
		
		public function getMeasureColumnNames():Array{
			return _config.getMeasureColumnNames();
		}
		
		public function getMeasureColumns():XMLList{
			return _config.getMeasureColumns();
		}
		
		public function getMeasureColumnByName(name:String):XML{
			return _config.getMeasureColumnByName(name);
		}
		
		public function removeColumnLabel(columnId:String):void{
			Application.application.trackEvent("removeColumn");
			var column:XMLList = getColumnXML(columnId);
			if (column.length() > 0){
				undoTrellisChartIfColumnMatch(columnId);
				var targetCol:XML = column[0];
				var label:XMLList = getLabelXML(targetCol.LabelName); // XXX LabelName!?!?
				if (label.length() > 0){
					delete label[0];
				}
				
				if (String(column[0].Hierarchy)) {
					// OLAP column
					// remove sort if it exists too
					var sortedColumns:Array = getSortedColumns();
					var newSortedColumns:ArrayCollection = new ArrayCollection(sortedColumns);
					for (var i:uint = 0; i < newSortedColumns.length; i++) {
						if ((newSortedColumns[i]["inSortedEntities"] == false) ||
							(newSortedColumns[i]["id"] == columnId)) {
							newSortedColumns.removeItemAt(i);
							i--;
						}
					}
					var update:Boolean = _doNotUpdateServer;
					_doNotUpdateServer = true;
					setSortedColumns(sortedColumns);
					_doNotUpdateServer = update;
				}
				delete column[0];
			
				//After config is saved / failed to save, throw a delete column event - subject area wants to refresh.
				AppEventManager.on(AppEvent.EVT_CONFIG_SAVE_SUCCESS, throwColumnDeletedEvent, false, 0, true);
				AppEventManager.on(AppEvent.EVT_CONFIG_SAVE_FAILED, throwColumnDeletedEvent, false, 0, true);

				// send to server regardless...				
				var b:Boolean = updateToServer;
				updateToServer = true;
				asyncSaveConfigToServer(Components.ADHOC_APP, 'removeColumnLabel', columnId);
				updateToServer = b;
			}
		}
		
		public function removeItems(ids:ArrayCollection, types:ArrayCollection):void {
			var addThrowColumnDeletedEvent:Boolean = false;
			for (var i:uint = 0; i < ids.length; i++) {
				if (i >= types.length)
					break;
					
				var type:String = types.getItemAt(i).toString();
				var id:String = ids.getItemAt(i).toString();
				if (type == AppEvent.EVT_COLUMN_CHOSEN) {
					addThrowColumnDeletedEvent = true;
					var column:XMLList = getColumnXML(id);
					if (column.length() > 0) {
						var targetCol:XML = column[0];
						var label:XMLList = getLabelXML(targetCol.LabelName);
						if (label.length() > 0) {
							delete label[0];
						}
						delete column[0];
					}
				}
				else if (type == AppEvent.EVT_CHART_CHOSEN) {
					var chart:XMLList = getChartXML(id);
					if (chart.length() > 0) {
						delete chart[0];
					}
				}
				else if (type == AppEvent.EVT_IMAGE_CHOSEN) {
					var image:XMLList = getImageXML(id);
					if (image.length() > 0) {
						delete image[0];
					}
				}
				else if (type == AppEvent.EVT_LABEL_CHOSEN) {
					var lab:XMLList = getLabelXML(id);
					if (lab.length() > 0) {
						delete lab[0];
					}
				}
				else if (type == AppEvent.EVT_SUBREPORT_CHOSEN) {
					var sub:XMLList = getSubreportXML(id);
					if (sub.length() > 0) {
						delete sub[0];
					}
				}
			}

				
			if (addThrowColumnDeletedEvent == true) {
				AppEventManager.on(AppEvent.EVT_CONFIG_SAVE_SUCCESS, throwColumnDeletedEvent, false, 0, true);
				AppEventManager.on(AppEvent.EVT_CONFIG_SAVE_FAILED, throwColumnDeletedEvent, false, 0, true);
			}
			
			var update:Boolean = _doNotUpdateServer;
			_doNotUpdateServer = true;
			asyncSaveConfigToServer(Components.ADHOC_APP, 'removeItems');
			_doNotUpdateServer = update;
		}
		
		private function throwColumnDeletedEvent(event:AppEvent):void{
			//Remove us from listening to the events again.
			AppEventManager.removeAppEventListener(AppEvent.EVT_CONFIG_SAVE_FAILED, throwColumnDeletedEvent);
			AppEventManager.removeAppEventListener(AppEvent.EVT_CONFIG_SAVE_SUCCESS, throwColumnDeletedEvent);
			
			AppEventManager.fire(new AppEvent(AppEvent.EVT_COLUMN_DELETED, event.callerComponent, event.callerOperation, event.dispatcher));	
		}
		
		public function removeLabel(imageId:String):void{
			var image:XMLList = getLabelXML(imageId);
			if (image.length() > 0){
				saveCurrentConfig();
				delete image[0];
				var update:Boolean = _doNotUpdateServer;
				_doNotUpdateServer = false;
				asyncSaveConfigToServer(Components.ADHOC_APP, 'removeLabel');
				_doNotUpdateServer = update;
			}
		}
		
		public function removeRectangle(id:String):void {
			var rect:XMLList = getRectangleXML(id);
			if (rect.length() > 0) {
				saveCurrentConfig();
				delete rect[0];
				var update:Boolean = _doNotUpdateServer;
				_doNotUpdateServer = false;
				asyncSaveConfigToServer(Components.ADHOC_APP, 'removeRectangle');
				_doNotUpdateServer = update;
			}
		}
		
		public function removeButton(id:String):void {
			var rect:XMLList = getAdhocButtonXML(id);
			if (rect.length() > 0) {
				saveCurrentConfig();
				delete rect[0];
				var update:Boolean = _doNotUpdateServer;
				_doNotUpdateServer = false;
				asyncSaveConfigToServer(Components.ADHOC_APP, 'removeButton');
				_doNotUpdateServer = update;
			}
		}
		
		public function removeImage(imageId:String):void{
			var image:XMLList = getImageXML(imageId);
			if (image.length() > 0){
				saveCurrentConfig();
				delete image[0];
				var update:Boolean = _doNotUpdateServer;
				_doNotUpdateServer = false;
				asyncSaveConfigToServer(Components.ADHOC_APP, 'removeImage');
				_doNotUpdateServer = update;
			}
		}
		
		public function removeSubReport(subreportId:String):void{
			var subreport:XMLList = getSubreportXML(subreportId);
			if (subreport.length() > 0){
				saveCurrentConfig();
				delete subreport[0];
				var update:Boolean = _doNotUpdateServer;
				_doNotUpdateServer = false;
				asyncSaveConfigToServer(Components.ADHOC_APP, 'removeSubReport');
				_doNotUpdateServer = update;
			}	
		}
		
		public function removeChart(chartId:String):void {
			var chart:XMLList = getChartXML(chartId);
			if (chart.length() > 0) {
				saveCurrentConfig();
				delete chart[0];
				var update:Boolean = _doNotUpdateServer;
				_doNotUpdateServer = false;
				asyncSaveConfigToServer(Components.ADHOC_APP, 'removeChart');
				_doNotUpdateServer = update;
			}
		}
		public function getColumnXML(columnId:String):XMLList{
			return _config.getColumnXML(columnId);
		}
		
		public function getAllColumnNames():Array{
			return _config.getAllColumnNames();
		}
		
		public function getAllColumns():XMLList{
			return _config.getAllColumns();
		}
		
		public function setSortedColumns(columns:Array):void{
			saveCurrentConfig();
			_config.setSortedColumns(columns);
			//save to server
			asyncSaveConfigToServer(Components.ADHOC_APP, 'setSortedColumn');	
		}
		
		
		public function getSortedEntityColumn(id:String):XML {
			return _config.getSortedEntityColumn(id);
		}
		
		public function getSortedColumns():Array{
			return _config.getSortedColumns();
		}
		
		public function getSortedOrderArray():ArrayCollection {
			return _config.getSortedOrderArray();
		}
		
		public function setFilters(filters:Array):void{
			saveCurrentConfig();
			_config.setFilters(filters);
			//save to server
			asyncSaveConfigToServer(Components.ADHOC_APP, 'setFilters');
		}
		
		public function getFilters():Array{
			return _config.getFilters();
		}
		
		public function addFiltersToArray(rootFilter:XMLList, conf:Array, measureColumn:String = ''):void{
			return _config.addFiltersToArray(rootFilter, conf, measureColumn);
		}
		
		public function isHideContent():Boolean{
			return _config.isHideContent();
		}
		
		public function setHideContent(hideOrNot:Boolean, component:String, operation:String):void{
			saveCurrentConfig();
			_config.setHideContent(hideOrNot, component, operation);
			asyncSaveConfigToServer(component, operation);
		}
		
		public function isShowFlexGrid():Boolean {
			return _config.isShowFlexGrid();
		}
		
		public function isAutomaticLayout():Boolean {
			return _config.isAutomaticLayout();
		}
		
		public function setShowFlexGrid(showOrNot:Boolean, component:String, operation:String):void {
			saveCurrentConfig();
			_config.setShowFlexGrid(showOrNot);
			asyncSaveConfigToServer(component, operation);
		}
		
		public function isUseOlapIndentation():Boolean {
			return _config.isUseOlapIndentation();
		}
		
		public function setUseOlapIndentation(useIt:Boolean, component:String, operation:String):void {
			saveCurrentConfig();
			_config.setUseOlapIndentation(useIt);
			asyncSaveConfigToServer(component, operation);
		}
		
		public function getConditionalFormats():Array{
			return _config.getConditionalFormats();
		}
		
		public function setReportFile(path:String, filename:String):void{
			clearHistory();
			_config.setReportFile(path, filename);
		}
		
		public function createNewImage(type:String, location:String):XML{
			return _config.createNewImage(type, location);
		}
		
		public function createNewSubreport(PassValueFromParent:Boolean, subReport:String):void {
			_config.createNewSubreport(PassValueFromParent, subReport);
		}
		
		public function getBandProperties():Object{
			return _config.getBandProperties();
		}
		
		public function setBandProperties(o:Object):void{
			var updateServer:Boolean = _doNotUpdateServer;
			_doNotUpdateServer = false;
            saveCurrentConfig();
			_config.setBandProperties(o);
			asyncSaveConfigToServer(Components.ADHOC_APP, 'setBandProperties');
			_doNotUpdateServer = updateServer;
		}
		
		public function getPageInfoXML():XMLList{
			return _config.getPageInfoXML();
		}
		
		//Toggle on or off the summation operation
		public function toggleSum():void{
			saveCurrentConfig();
			_config.toggleSum();
			asyncSaveConfigToServer(Components.ADHOC_APP, 'toggleSum');	
		}
		
		public function setPivotMeasureLabelsColumnWise(columnWise:Boolean):void{
			saveCurrentConfig();
			_config.setPivotMeasureLabelsColumnWise(columnWise);
			asyncSaveConfigToServer(Components.ADHOC_APP, 'setPivotMeasureLabelsColumnWise');	
		}
		
		public function swapColumnOnBottom(top:String, bottom:String):void{
			return swapColumnOnTop(bottom, top);
		}
		
		public function swapColumnOnTop(top:String, bottom:String):void{
			if(_config.swapColumnOnTop(top, bottom)){
				saveCurrentConfig();
				asyncSaveConfigToServer(Components.ADHOC_APP, 'swapColumnOnTop');
			}
		}
		
		public function isPivotMeasureLabelsColumnWise():Boolean{
			return _config.isPivotMeasureLabelsColumnWise();
		}
		
		public function getPivotTableProperties():Array{
			return _config.getPivotTableProperties();
		}
		
		public function setPivotTableProperties(conf:Array):void{
			saveCurrentConfig();
			_config.setPivotTableProperties(conf);
			asyncSaveConfigToServer(Components.ADHOC_APP, 'setPivotTableProperties');		
		}
		
		public function setTopN(value:String):void{
			saveCurrentConfig();
			_config.setTopN(value);
			asyncSaveConfigToServer(Components.ADHOC_APP, 'setTopN');
		}
		
		public function setTopNPrompt(value:String):void {
			saveCurrentConfig();
			_config.setTopNPrompt(value);
			asyncSaveConfigToServer(Components.ADHOC_APP, 'setTopN');
		}
		
		public function getTopN():XML{
			return _config.getTopN();
		}
		
		
		public function get config():String{
			return _config.toXMLString();
		}
		
		
		public function getConfig():XML{
			return _config.xml;
		}
		
		public function clearEmptyConfig():void{
			_config.clearEmptyConfig();
		}
	
		public function addElement(element:XML): void {
			var updateServer:Boolean = _doNotUpdateServer;
			_doNotUpdateServer = false;
			saveCurrentConfig();
			_config.addElement(element);
			asyncSaveConfigToServer(Components.ADHOC_APP, 'addElement');
			_doNotUpdateServer = updateServer;
		}
		
		public function addElementNoUpdateOfServer(element:XML):void {
			saveCurrentConfig();
			_config.addElement(element);
		}
		
		public function getNextElementId(): uint {
			return _config.getNextElementId();
		}
		
		public function getColumnSelectors(columnIndex:String):XMLList{
			return _config.getColumnSelectors(columnIndex);
		}
		
		public function setColumnSelectors(columnIndex:String, selectors:Array):void{
			_config.setColumnSelectors(columnIndex, selectors);
		}
		
		public function setColumnSortValue(id:int, sortValue:String):void{
			_config.setColumnSortValue(id, sortValue);
		}
		
		public function undoLayoutChanges(): void {
			saveCurrentConfig();
			_config.undoLayoutChanges();
			asyncSaveConfigToServer(Components.ADHOC_APP, 'undoLayoutChanges');
		}
		
		public function applyStyle(styleReportName:String):void {
			saveCurrentConfig();
			var ws:WebServiceClient = new WebServiceClient("Adhoc", "applyStyle", Components.ADHOC_APP, "applyStyle");
			ws.setResultListener(_asyncSaveConfigToServerHandler);
			ws.execute(_config.toXMLString(), styleReportName);
		}
		
		public function getTrellisColumnIndex():String{
			return _config.getTrellisColumnIndex();	
		}
		
		public function getTrellisColumnIndex2():String{
			return _config.getTrellisColumnIndex2();	
		}
		
		public function getTrellisChartIndex():String{
			return _config.getTrellisChartIndex();	
		}

		public function undoTrellisTooLarge():Boolean{
			return _config.undoTrellisTooLarge();	
		}
		
		public function setTrellisChartColumn(index:String):void{
			_config.setTrellisChartColumn(index);
		}
		
		public function undoTrellisChartIfColumnMatch(columnId:String):void{
			_config.undoTrellisChartIfColumnMatch(columnId);
		}
		
		public function setInVisualization():void {
			_config.setInVisualization();
		}
		
		public function replaceDefaultChart(chart:XML):void {
			var updateServer:Boolean = _doNotUpdateServer;
			_doNotUpdateServer = false;
			saveCurrentConfig();
			if (_config.replaceDefaultChart(chart) == false)
				return;
			asyncSaveConfigToServer(Components.ADHOC_APP, 'replaceDefaultChart');
			_doNotUpdateServer = updateServer;
		}
		
		public function get chartType():String {
			return _chartType;
		}
		
		public function set chartType(type:String):void {
			_chartType = type;
		}
	}
}
