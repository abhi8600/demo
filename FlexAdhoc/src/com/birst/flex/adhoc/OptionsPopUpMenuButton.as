package com.birst.flex.adhoc
{
	import com.birst.flex.core.AppEvent;
	import com.birst.flex.core.AppEventManager;
	import com.birst.flex.core.DialogBoxPopUpMenuButton;
	import com.birst.flex.modules.OptionsMenuModule;
	
	public class OptionsPopUpMenuButton extends DialogBoxPopUpMenuButton
	{
		private var _labelChosen:Boolean, _columnChosen:Boolean = false;
		
		public function OptionsPopUpMenuButton()
		{
			super();
			
			AppEventManager.on(AppEvent.EVT_RESULT_TAB_SHOW, columnLabelHandler);
			AppEventManager.on(AppEvent.EVT_COLUMN_CHOSEN, columnLabelHandler);
			AppEventManager.on(AppEvent.EVT_LABEL_CHOSEN, columnLabelHandler);
		}
		
		public function columnLabelHandler(event:AppEvent):void{
			if (event.type != AppEvent.EVT_RESULT_TAB_SHOW){
				_columnChosen = (event.type == AppEvent.EVT_COLUMN_CHOSEN) ? true : false;
				_labelChosen = !_columnChosen;
			}else
				_columnChosen = _labelChosen = false;			
			setProviderOnModuleLoad();
		}

		override public function setProviderOnModuleLoad():void{
			if (_menuItemsModule is OptionsMenuModule){
				var module:OptionsMenuModule = _menuItemsModule as OptionsMenuModule;
				if (_labelChosen == true|| _columnChosen == true)
					this.dataProvider = (_columnChosen) ? module.getColumnList() : module.getLabelList();
				else
					this.dataProvider = module.getRegularList();
			}
		}
		
	}
}