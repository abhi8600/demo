package com.birst.flex.adhoc
{
	import flash.display.DisplayObject;
	
	import mx.controls.advancedDataGridClasses.AdvancedDataGridColumn;
	import mx.controls.advancedDataGridClasses.AdvancedDataGridHeaderRenderer;
	import mx.core.UIComponent;
	import mx.core.UITextField;
	import mx.events.FlexEvent;

	public class BirstFlexDataGridHeaderRenderer extends AdvancedDataGridHeaderRenderer
	{
		public function BirstFlexDataGridHeaderRenderer()
		{
			//TODO: implement function
			super();
			addEventListener(FlexEvent.DATA_CHANGE, handleDataChanged);
		}
		
		
		private function handleDataChanged(event:FlexEvent):void {
//			Log.getLogger("").info(event.toString());
			var col:AdvancedDataGridColumn = data as AdvancedDataGridColumn;
			(label as UITextField).contextMenu = this.owner.contextMenu;
		}
		
		override protected function childrenCreated():void {
			for (var i:int = 0; i < numChildren; i++) {
				var c:DisplayObject = getChildAt(i);
				if (i > 0) {
					(c as UIComponent).includeInLayout = false; 
					(c as UIComponent).visible = false; 
				}
			}
		}
		
	}
}