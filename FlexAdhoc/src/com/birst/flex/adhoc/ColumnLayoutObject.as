package com.birst.flex.adhoc
{
	import com.birst.flex.adhoc.dialogs.*;
	import com.birst.flex.core.AppEvent;
	import com.birst.flex.core.AppEventManager;
	import com.birst.flex.core.Util;
	
	import flash.events.ContextMenuEvent;
	import flash.events.MouseEvent;
	import flash.ui.ContextMenuItem;
	
	import mx.core.Application;
	import mx.resources.ResourceManager;
	[ResourceBundle('adhoc')]

	public class ColumnLayoutObject extends LayoutObject
	{		
		private var _label:XML = null;
		
		public function ColumnLayoutObject(id:String, layout:XML, l:Layout)
		{
			this.type = "Column";
			selectionType = AppEvent.EVT_COLUMN_CHOSEN;
			super(id, layout, l);
		}
		
		override public function getLabel():String {
			
			return "c:" + getLabelString();
		}
		
		private function getLabelXml():XML {
			if (_label == null && entityXML.LabelName) {
				var list:XMLList = AdHocConfig.instance().getLabelXML(entityXML.LabelName)
				if (list && list.length() > 0)
					_label = list[0];
			}
			
			return _label;
		}
		
		private function getLabelString():String {
			var l:XML = getLabelXml();
			var ret:String = null;
			if (l)
				ret = l.Label;
				
			if (ret == null || ret.length == 0)
				ret = entityXML.Name;
				
			return ret;
		}

		override protected function setupContextMenuitems():void {
			super.setupContextMenuitems();
			
			var sorItem:ContextMenuItem = new ContextMenuItem("Sort");
			itemContextMenu.customItems.push(sorItem);
			sorItem.addEventListener(ContextMenuEvent.MENU_ITEM_SELECT, sortItem); 
			
			if (Application.application.isOlapCube && Application.application.isOlapCube == false) {
				var filItem:ContextMenuItem = new ContextMenuItem("Filter");
				itemContextMenu.customItems.push(filItem);
				filItem.addEventListener(ContextMenuEvent.MENU_ITEM_SELECT, filterItem);
			}
			
			var colPropertiesItem:ContextMenuItem = new ContextMenuItem(ResourceManager.getInstance().getString("adhoc","optionsMenu_CP"));
			itemContextMenu.customItems.push(colPropertiesItem);
			colPropertiesItem.addEventListener(ContextMenuEvent.MENU_ITEM_SELECT, columnProperties);
			
		}
		
		override protected function getLayoutType():String {
			return AppEvent.EVT_COLUMN_CHOSEN;
		}
		
		override protected function clickEventHandler(event:MouseEvent):void{
			AppEventManager.fire(new AppEvent(AppEvent.EVT_COLUMN_CHOSEN, 'ColumnLayoutObject', 'clickEventHandler', this.ID, event.ctrlKey));
		} 
		
		private function columnProperties(event:ContextMenuEvent):void {
			var selEntities:Array = Util.getSelectedEntities();
			if (selEntities.length > 1)
				invokeMenuAction("columnProperties", MultiSelectTextPropertiesDialogBox);
			else
				invokeMenuAction("columnProperties", ColumnPropertiesDialogBox);
		}
		
		private function sortItem(event:ContextMenuEvent):void{
			invokeMenuAction("sortItem", SortColumnDialogBox);
		}
		
		private function filterItem(event:ContextMenuEvent):void{
			invokeMenuAction("filterItem", FilterColumnsGridDialogBox);
		}
	}
}