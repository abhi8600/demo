package com.birst.flex.adhoc
{
	import com.birst.flex.adhoc.dialogs.FilterColumnOperator;
	import com.birst.flex.core.DateTimePicker;
	import com.birst.flex.core.Util;
	
	public class FilterFormatter
	{
		public function FilterFormatter()
		{
		}

		public static function friendlyFormat(filter:Object):String{
			var fmtTxt:String = '';
			
			var operand:String = formatOperand(filter, true);
			//Display filter
			if (filter.Type == 2 || filter.Type == 3) {
				fmtTxt = filter.Name;
			}
			else if (filter.Type == "Predicate"){
				fmtTxt = filter.ColumnName + ' ' + FilterColumnOperator.longToShort(filter.Operator) + ' ' + operand;
			}else{
				fmtTxt = filter.Hierarchy ? filter.Hierarchy : filter.Column;
				
				fmtTxt += ' ' + filter.Operator + ' ' + operand;
			}
	
			return fmtTxt;		
		}
		
		public static function formatOperand(filter:Object, isSummary:Boolean = false):String{
			var fmtTxt:String = '';
			var postFix:String = '';

			/* need to rethink this, I think the regular expression stuff should be explicit (ricks)		
			if (filter.Operator == "LIKE"){
				postFix = "*";	
			}
			*/

			var operand_value:String = filter.Operand;

			if (operand_value == 'NO_FILTER'){
				var promptName:String = filter.PromptName as String;
				var promptPostfix:String = promptName + postFix;
				fmtTxt = (isSummary == true) ? '(' + promptPostfix + ')' : promptPostfix;
			}else if (filter.Type == 'Set'){
				var filename:String = operand_value;
				//remove suffix
				var suffixIdx:int = filename.indexOf(".AdhocReport");
				if (suffixIdx > 0){
					filename = filename.substring(0, suffixIdx);
				}
				//only show the filaname, discard the subdir paths	
				var lastSlash:int = filename.lastIndexOf("/");
				if (lastSlash > 0){
					filename = filename.substr(lastSlash + 1);
				}
				fmtTxt = filename;
			}else{
				if(operand_value.indexOf("V{") != 0 && filter.Operator != "IS NOT NULL" && filter.Operator != "IS NULL"){
					try{
						if(filter.DataType == "DateTime"){
							operand_value =	DateTimePicker.convertProcessingDateTime2DisplayString(operand_value);	
						}else if (filter.DataType == "Date"){
							operand_value = Util.procDateFormat2DisplayFormat(operand_value); 
						}
					}catch(e:Error){
						//No op, we tried our best to parse the date but since this is
						//only for display info it is ok to just let it be.. Most likely we don't get here.
					}
				}
				fmtTxt = operand_value + postFix;
			}
			
			return fmtTxt;	
		}
	}
}