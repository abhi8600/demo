package com.birst.flex.adhoc
{
	import com.birst.flex.adhoc.dialogs.*;
	import com.birst.flex.core.AppEvent;
	import com.birst.flex.core.AppEventManager;
	
	import flash.events.MouseEvent;
	import flash.events.ContextMenuEvent;
	import flash.ui.ContextMenuItem;
	
	import mx.collections.ArrayCollection;
	import mx.resources.ResourceManager;
	[ResourceBundle('adhoc')]

	public class LabelLayoutObject extends LayoutObject
	{		
		public function LabelLayoutObject(id:String, layout:XML, l:Layout)
		{
			this.type = "Label";
			selectionType = AppEvent.EVT_LABEL_CHOSEN;
			super(id, layout, l);
		}
		
		override public function getLabel():String {
			return "l:" + entityXML.Label;
		}
		override protected function getLayoutType():String {
			return AppEvent.EVT_LABEL_CHOSEN;
		}
		
		override protected function setupContextMenuitems():void {
			super.setupContextMenuitems();
			var colPropertiesItem:ContextMenuItem = new ContextMenuItem(ResourceManager.getInstance().getString("adhoc","optionsMenu_LP"));
			itemContextMenu.customItems.push(colPropertiesItem);
			colPropertiesItem.addEventListener(ContextMenuEvent.MENU_ITEM_SELECT, labelProperties);
		}
		
		override protected function clickEventHandler(event:MouseEvent):void{
			AppEventManager.fire(new AppEvent(AppEvent.EVT_LABEL_CHOSEN, 'LabelLayoutObject', 'clickEventHandler', this.ID, event.ctrlKey));
		} 
		
		//Called by the right click "Remove"
		override protected function deleteItem(event:ContextMenuEvent):void {
			AdHocConfig.instance().removeLabel(this.ID.toString());
		}
		
		private function labelProperties(event:ContextMenuEvent):void {
			invokeMenuAction("labelProperties", MultiSelectTextPropertiesDialogBox);
		}
	}
}