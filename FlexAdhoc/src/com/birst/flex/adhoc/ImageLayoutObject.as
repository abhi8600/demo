package com.birst.flex.adhoc
{
	import com.birst.flex.adhoc.dialogs.SubReportImagePropertiesDialogBox;
	import com.birst.flex.core.AppEvent;
	import com.birst.flex.core.AppEventManager;
	
	import flash.events.ContextMenuEvent;
	import flash.events.MouseEvent;
	import flash.ui.ContextMenuItem;
	import mx.resources.ResourceManager;
	
	import mx.collections.ArrayCollection;
	 [ResourceBundle('adhoc')]

	public class ImageLayoutObject extends LayoutObject
	{	
		public function ImageLayoutObject(id:String, layout:XML, l:Layout)
		{
			this.type = "Image";
			selectionType = AppEvent.EVT_IMAGE_CHOSEN;
			super(id, layout, l);
		}
		
		override public function getLabel():String {
			return "i:" + entityXML.Location;
		}

		override protected function getLayoutType():String {
			return AppEvent.EVT_IMAGE_CHOSEN;
		}
		
		override protected function clickEventHandler(event:MouseEvent):void{
			AppEventManager.fire(new AppEvent(AppEvent.EVT_IMAGE_CHOSEN, 'ImageLayoutObject', 'clickEventHandler', this.ID, event.ctrlKey));
		}
		
		override protected function setupContextMenuitems():void {
			super.setupContextMenuitems();
			var imagePropertiesItem:ContextMenuItem = new ContextMenuItem( ResourceManager.getInstance().getString("adhoc","optionsMenu_IP"));
			itemContextMenu.customItems.push(imagePropertiesItem);
			imagePropertiesItem.addEventListener(ContextMenuEvent.MENU_ITEM_SELECT, imageProperties);
		}
		
		//Called by the right click "Remove"
		override protected function deleteItem(event:ContextMenuEvent):void {
			AdHocConfig.instance().removeImage(this.ID.toString());
		}
		
		private function imageProperties(event:ContextMenuEvent):void {
			invokeMenuAction("imageProperties", SubReportImagePropertiesDialogBox);
		} 
	}
}