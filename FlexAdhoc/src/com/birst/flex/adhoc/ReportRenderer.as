package com.birst.flex.adhoc
{
import com.anychart.AnyChartFlex;
import com.anychart.events.AnyChartMultiplePointsEvent;
import com.anychart.events.AnyChartPointEvent;
import com.anychart.events.AnyChartPointInfo;
import com.birst.flex.adhoc.dialogs.*;
import com.birst.flex.core.AppEvent;
import com.birst.flex.core.ColumnLabelMenuBar;
import com.birst.flex.core.ColumnTextCell;
import com.birst.flex.core.DragDropHelper;
import com.birst.flex.core.LinkNavigatorEvent;
import com.birst.flex.core.MenuItemActions;
import com.birst.flex.core.PromptGroupButton;
import com.birst.flex.core.PromptGroupCheckBox;
import com.birst.flex.core.PromptGroupDropDown;
import com.birst.flex.core.PromptGroupEditableTextbox;
import com.birst.flex.core.ReportRendererEvent;
import com.birst.flex.core.ReportXml;
import com.birst.flex.core.Tracer;
import com.birst.flex.core.Util;
import com.birst.flex.core.formatter.BDateFormatter;
import com.birst.flex.core.maps.UMapperMaps;
import com.birst.flex.dashboards.views.DashletContentBase;
import com.birst.flex.webservice.WebServiceClient;
import com.birst.flex.webservice.WebServiceResult;

import flash.display.DisplayObject;
import flash.display.GradientType;
import flash.display.InteractiveObject;
import flash.display.Sprite;
import flash.events.*;
import flash.geom.Point;
import flash.geom.Rectangle;
import flash.ui.ContextMenu;
import flash.ui.ContextMenuItem;

import mx.collections.ArrayCollection;
import mx.collections.ArrayList;
import mx.collections.XMLListCollection;
import mx.containers.Canvas;
import mx.controls.AdvancedDataGrid;
import mx.controls.Alert;
import mx.controls.Button;
import mx.controls.Image;
import mx.controls.advancedDataGridClasses.AdvancedDataGridColumn;
import mx.controls.listClasses.IListItemRenderer;
import mx.controls.scrollClasses.ScrollBar;
import mx.core.Application;
import mx.core.ClassFactory;
import mx.core.DragSource;
import mx.core.IToolTip;
import mx.core.ScrollPolicy;
import mx.core.UIComponent;
import mx.events.DragEvent;
import mx.events.ListEvent;
import mx.managers.CursorManager;
import mx.managers.DragManager;
import mx.managers.ToolTipManager;
import mx.resources.ResourceManager;
import mx.rpc.events.ResultEvent;
	
	public class ReportRenderer
	{
		private static var MAX_CATEGORIES:int = 10;
		
		private var parent: Canvas;
		public var caller:Object;
		
		private var charts:Array = new Array();
		private var numChartsInUse:int = 0;
		private var contextMenu:ContextMenu;
		public var width:int;
		public var height:int;
		
		public var hasText:Boolean = false;
		public var hasImage:Boolean = false;
		public var hasChart:Boolean = false;
		public var columnDrills:Object = new Object();

		public var showColumnLabelMenu:Boolean = true;
		public var addMenuBarItemFunc:Function;
		
		private var WS_ROOT:String = "/SMIWeb/";
		
		// flex selection rectangle stuff - allows adding dynamic filters
		private var selectionRectangle:Canvas;
		private var r:Rectangle = null;
		private var rectangleMoved:Boolean = false;
		private var addEditProperties:Boolean = false;
		
		[Bindable]
		private var selectedItems:Array = new Array();
		
		[Bindable]
		[Embed(source="/images/designer watermark image.png")]
		private var designerWaterMark:Class;
		
		[Bindable]
		[Embed(source="/images/designer lite watermark.png")]
		private var designerLiteWatermark:Class;
		
		[Bindable]
		private var waterMarkClz:Class = designerWaterMark;
		
		private var topDimensions:Array = new Array();
		private var leftDimensions:Array = new Array();
		
		private var _rectangleSelect:Boolean;
		
		private var _trellisToolTip:IToolTip;
		
		[ResourceBundle('adhoc')]
		
		public function ReportRenderer(parent: Canvas, addEditProperties:Boolean, enableRectangleSelect:Boolean)
		{
			this.parent = parent;
			this.addEditProperties = addEditProperties;
			
			//Rectangle selection
			this.rectangleSelect = enableRectangleSelect;
			
            //Get any rootURL for smiweb passed as flash vars to swf file
            var flashVarRootURL:String = Util.getApplicationSMIWebURL();
            if(flashVarRootURL != null && flashVarRootURL.length > 0)
            {
            	WS_ROOT = flashVarRootURL + WS_ROOT;
            }
            
            var app:Object = Application.application;
            if (app.hasOwnProperty("isFreeTrial") && app["isFreeTrial"] == true) {
            	waterMarkClz = designerLiteWatermark;
            }
            else
            	waterMarkClz = designerWaterMark; 
			(app as Application).addEventListener(PropertyChangeEvent.PROPERTY_CHANGE, propChanged);  
		}
		
		private function propChanged(event:PropertyChangeEvent):void {
			if (event.property == "isFreeTrial" && event.oldValue != event.newValue) {
				if (event.newValue == true) {
	            	waterMarkClz = designerLiteWatermark;
					var app:Object = Application.application;
					(app as Application).removeEventListener(PropertyChangeEvent.PROPERTY_CHANGE, propChanged);
				}
			}
		}
		
		public function clear():void{
			parent.removeAllChildren();
			parent.graphics.beginFill(0xffffff);
			parent.graphics.drawRect(0, 0, parent.width, parent.height);
			parent.graphics.endFill();
			parent.setStyle('backgroundImage', waterMarkClz);
		}
		
		public function set rectangleSelect(select:Boolean):void {
			_rectangleSelect = select;
			if(select){
				// add mouse listeners for a flex selection rectangle
				parent.addEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
				parent.addEventListener(MouseEvent.MOUSE_UP, onMouseUp);
				
				if(!selectionRectangle){
					selectionRectangle = new Canvas();
					selectionRectangle.setStyle("backgroundAlpha", 0.3);
					selectionRectangle.setStyle("backgroundColor", 0x067EE3);
					selectionRectangle.setStyle("borderColor", 0x057de3);
					selectionRectangle.setStyle("borderStyle", "solid");
					selectionRectangle.setStyle("borderThickness", 2);
					selectionRectangle.setStyle("cornerRadius", 0);
				}
			}else{
				parent.removeEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
				parent.removeEventListener(MouseEvent.MOUSE_UP, onMouseUp);
			}
		}
		
		public function render(report:XML, ctx:ContextMenu = null, caller:Object = null):void
		{
			hasText = hasImage = hasChart = false;
			
			contextMenu = ctx;
			width = height = 0;
			columnDrills = new Object();
			parent.graphics.clear();
			parent.removeAllChildren();
			
			var bkcolors:String = report.@color;
			if (bkcolors == null || bkcolors.length == 0) {
				parent.setStyle('backgroundColor', null);
				
				// default to white
				bkcolors = "FFFFFF";
				parent.graphics.beginFill(uint("0x" + bkcolors), 0.0);
				parent.graphics.drawRect(0, 0, parent.width, parent.height);
				parent.graphics.endFill();
			}else{
				parent.setStyle('backgroundColor', uint("0x" + bkcolors));
			}
			
			topDimensions = new Array();
			leftDimensions = new Array();
			this.caller = caller;
			renderChildren(report, 0, 0, parent, "", true);
		}
	
		private function getNextChart():AnyChartFlex{
			if (charts.length <= numChartsInUse){
				var chart:AnyChartFlex = new AnyChartFlex();
				chart.addEventListener(DragEvent.DRAG_ENTER, onDragEnterChart);
				chart.addEventListener(DragEvent.DRAG_EXIT, onDragExitChart);
				chart.addEventListener(DragEvent.DRAG_DROP, onDragDropChart);
				charts.push(chart);
			}
			
			var c:AnyChartFlex = charts[numChartsInUse++];
			if (contextMenu){
				c.contextMenu = contextMenu;
			}
			return c;
		}
		
		private function onDragEnterChart(event:DragEvent):void{
			if( !(caller is DashletContentBase) ){
				var accept:Boolean = DragDropHelper.acceptIfSubjectAreaNode(event, true);
				if(accept){
					DragManager.showFeedback(DragManager.COPY);
					
					var target:DisplayObject = (event.currentTarget as DisplayObject);
					var middlePt:Point = target.localToGlobal(new Point(target.width / 2, target.height / 2));
					
					if(!_trellisToolTip){
						_trellisToolTip = ToolTipManager.createToolTip('Create Trellis Chart', middlePt.x, middlePt.y);
					}
				}
			}
		}
		
		private function onDragExitChart(event:DragEvent):void{
			if(_trellisToolTip){
				ToolTipManager.destroyToolTip(_trellisToolTip);
				_trellisToolTip = null;
			}	
		}
		
		private function onDragDropChart(event:DragEvent):void{
			if ( !(caller is DashletContentBase) ) {
				onDragExitChart(null);
				
				var source:DragSource = event.dragSource;		
				var treeItems:Array = source.dataForFormat("treeItems") as Array;
				if (treeItems && treeItems.length > 0){
					var node:XML = treeItems[0];
				
					//createTrellisCharts();
					var config:AdHocConfig = AdHocConfig.instance();
					var ws:WebServiceClient = new WebServiceClient('Adhoc', 'createTrellisChart', 'ReportRenderer', 'onDragDropChart');
					ws.setResultListener(onCreateTrellisChart);
					var newNode:XML = XML(node.toXMLString());
					delete newNode.c;
					ws.execute(newNode.toXMLString(), config.getConfig().toXMLString());
				}
				
			}
		}
		
		private function onCreateTrellisChart(event:ResultEvent):void{
			var rs:WebServiceResult = WebServiceResult.parse(event);
			if(!rs.isSuccess()){
				Util.alertErrMessage('Failed to create Trellis chart', rs);
				return;
			}
			
			var results:XMLList = rs.getResult();
			if(results.length() > 0){
				var result:XML = results[0];
				var newConfig:XML = (result[ReportXml.ROOT])[0];
				var config:AdHocConfig = AdHocConfig.instance();
				config.setConfig(newConfig);
				config.asyncSaveConfigToServer('ReportRenderer.createTrellisCharts()', 'setFilters');
			}
			//save the config
		}
		
		private function createTrellisCharts():void{
			var adhocConfig:AdHocConfig = AdHocConfig.instance();
			var defaultChartId:String = adhocConfig.getDefaultChartId();
			var list:XMLList = adhocConfig.getChartXML(defaultChartId);
			if (list.length() > 0){
				var chart:XML = list[0];
				var newChart:XML = chart.copy();
				adhocConfig.removeChart(defaultChartId);
				var reportIdx:String;
				for(var i:int = 0; i < 12; i++){
					var c:XML = newChart.copy();
					reportIdx = adhocConfig.addNewElementXML(c);
				}
				
				adhocConfig.asyncSaveConfigToServer('ReportRenderer.createTrellisCharts()', 'setChartProperties');
			}
		}
		
		private function resetNumChartsInUse():void{
			numChartsInUse = 0;	
		}
		
		private function renderChildren(report:XML, xOrigin:uint, yOrigin:uint, parent: Canvas, tagType:String = "", firstLevel:Boolean = false): Boolean {
			CursorManager.setBusyCursor();
			try{
				var ret:Boolean = false;
				var clearWaterMark:Boolean = false;
				var index:int;
				var j:int;
				var count:int = report.DrillColumn_Count;
				for (var m:int = 0; m < count; m++) {
					var columnName:String = "DrillColumn_" + m + "_Count";
					var specificColumnCount:int = report.child(columnName);
					var columnList:ArrayList = new ArrayList();
					for (j = 0; j < specificColumnCount; j++) {
						var name:String = "DrillColumn_" + m + "_" + j;
						var dcl:XMLList = report.child(name);
						if (dcl && dcl.length() > 0) {
							columnList.addItem(dcl[0]);
						}
					}
					var columnIndex:String = "DrillColumn_" + m + "_index";
					index = report.child(columnIndex);
					columnDrills[index] = columnList;
				}
				
				var useFlexGrid:String = report.UseFlexGrid;
				if (useFlexGrid == 'true') {
					var headers:XMLListCollection = new XMLListCollection();
					var x:XML;
					// find all labels  -- isMenu == true
					for each (x in report.text) {
						if (x.isMenu != null && String(x.isMenu) == "true")
							headers.addItem(x);
					}
					
					var tableRects:ArrayCollection = new ArrayCollection();
					var rect:TableRect = null;
					for (index = 0; index < headers.length; index++) {
						x = XML(headers.getItemAt(index));
						if (index == 0 || rect == null) {
							rect = new TableRect(x.@x, x.@y, x.@width, x.@height);
							rect.addHeader(x);
							tableRects.addItem(rect);
						} 
						else {
							if (rect && rect.y == x.@y) {
								var w:int = x.@width;
								rect.width = int(x.@x) + w;
								rect.addHeader(x);
							}
							else {
								rect = new TableRect(x.@x, x.@y, x.@width, x.@height);
								rect.addHeader(x);
								tableRects.addItem(rect);
							} 
						}
					}
					
					for (index = 0; index < tableRects.length; index++) {
						rect = tableRects.getItemAt(index) as TableRect;
						for (j = index + 1; j < tableRects.length; j++) {
							rect.addSubsequentTable(tableRects.getItemAt(j) as TableRect);
						}
						for each (x in report.text) {
							rect.addDataIfAppropriate(x);
						}
						
						rect.render(xOrigin, yOrigin, parent, addMenuBarItemFunc, showColumnLabelMenu);
						clearWaterMark = true;
					}
				}
									
				for each (x in report.rectangle) {
					if (! x.hasOwnProperty('processed')) {
						renderRectangle(x, xOrigin, yOrigin, parent);
						ret = true;
					}
				}
				for each (var ab:XML in report.adhocButton) {
					if (! ab.hasOwnProperty('processed')) {
						renderAdhocButton(ab, xOrigin, yOrigin, parent);
						ret = true;
					}
				}
				for each(var t: XML in report.text)
				{
					if (! t.hasOwnProperty('processed')) {
						renderText(t, xOrigin, yOrigin, parent, report);
						ret = true;
					}
				}
				
				for each(var cb: XML in report.checkbox)
				{
					if (! cb.hasOwnProperty('processed')) {
						renderPromptGroupCheckbox(cb, xOrigin, yOrigin, parent, report);
						ret = true;
					}
				}
				
				for each(var dd: XML in report.dropdown)
				{
					if (! dd.hasOwnProperty('processed')) {
						renderPromptGroupDropDown(dd, xOrigin, yOrigin, parent, report);
						ret = true;
					}
				}
				
				for each(var tb: XML in report.editabletextbox)
				{
					if (! tb.hasOwnProperty('processed')) {
						renderPromptGroupEditableTextbox(tb, xOrigin, yOrigin, parent, report);
						ret = true;
					}
				}
				
				for each(var bt: XML in report.button)
				{
					if (! bt.hasOwnProperty('processed')) {
						renderPromptGroupButton(bt, xOrigin, yOrigin, parent, report);
						ret = true;
					}
				}
				
				resetNumChartsInUse();
				for each(var c: XML in report.chart)
				{
					var chart:AnyChartFlex = getNextChart();
					
					//Rectangle selection
					if(_rectangleSelect){
						chart.addEventListener(AnyChartMultiplePointsEvent.MULTIPLE_POINTS_SELECT, handleMultipleChartSelect);
					}else{
						chart.removeEventListener(AnyChartMultiplePointsEvent.MULTIPLE_POINTS_SELECT, handleMultipleChartSelect);
						c.anychart.charts.chart.data_plot_settings.interactivity.@allow_multiple_select = 'false';
						c.anychart.charts.chart.data_plot_settings.interactivity.select_rectangle.@enabled = 'false';
						c.anychart.settings.context_menu.save_as_image_item_text = ResourceManager.getInstance().getString("adhoc","APP_saveasimage"); 
					}
					
					if(c.anychart.charts.chart.(@plot_type == 'UMap').length() > 0){			
						var map:UMapperMaps = new UMapperMaps();
						map.xml = c.anychart.charts.chart[0];
		
						map.x = uint(c.@x) + xOrigin;
						map.y = uint(c.@y) + yOrigin;
						map.width = uint(c.@width);
						map.height = uint(c.@height);
						
						parent.addChild(map);
						ret = hasChart = true;

						break;
					}
					
					chart.anychartXML = c.anychart[0];
					chart.x = uint(c.@x) + xOrigin;
					chart.y = uint(c.@y) + yOrigin;
					chart.width = uint(c.@width);
					chart.height = uint(c.@height);
					
					chart.addEventListener(AnyChartPointEvent.CLICK, handleChartClick);
					var app:Object = Application.application;
					if (!(app.hasOwnProperty('isFreeTrial') && app["isFreeTrial"] == true)) {
						addEditChartMenu(chart, c.@reportIndex);
					}					
					
					parent.addChild(chart);
					ret = true;
					
					hasChart = true;
					updateWidthHeight(chart);
				}
				for each(var g: XML in report.gauge)
				{
					var gauge:AnyChartFlex = getNextChart();
					gauge.anychartXML = g.anychart[0];
					gauge.x = uint(g.@x) + xOrigin;
					gauge.y = uint(g.@y) + yOrigin;
					gauge.width = uint(g.@width);
					gauge.height = uint(g.@height);
					addEditChartMenu(gauge, g.@reportIndex);					
					parent.addChild(gauge);
					ret = true;
					
					hasChart = true;
					updateWidthHeight(gauge);
				}
		
				for each(var i: XML in report.image)
				{
					renderImage(i, xOrigin, yOrigin, parent);
					ret = true;
				}
			
				for each(var f: XML in report.frame)
				{
					renderFrame(f, xOrigin, yOrigin, parent);
					clearWaterMark = true;
				}
				
				for each (var l: XML in report.line) {
					renderLine(l, xOrigin, yOrigin, parent);
					clearWaterMark = true;
				}
			}catch(err:Error){
				throw err;	
			}finally{
				CursorManager.removeBusyCursor();
			}
			
			if(firstLevel){
				if (ret || clearWaterMark)
					parent.setStyle('backgroundImage', null);
				else
					parent.setStyle('backgroundImage', waterMarkClz);
			}
			return ret;
		}
		
		private function addEditChartMenu(io:InteractiveObject, id:String) :void {
			if (this.addEditProperties == true) {
				var chartProperties:ContextMenuItem = new ContextMenuItem(ResourceManager.getInstance().getString("adhoc","optionsMenu_CH"));
				(io.contextMenu as ContextMenu).customItems.push(chartProperties);
				chartProperties.addEventListener(ContextMenuEvent.MENU_ITEM_SELECT, 
					function(event:ContextMenuEvent):void {
						Application.application.selectedLayoutIds.removeAll();
						Application.application.selectedLayoutIds.addItem(id);
						Application.application.selectedLayoutTypes.removeAll();
						Application.application.selectedLayoutTypes.addItem(AppEvent.EVT_CHART_CHOSEN);
						var menuItem:Object = new Object();
						menuItem["id"] = id;
						menuItem["klass"] =  AdhocChartPropertiesDialogBox;
						MenuItemActions.invokeAction(menuItem);
					}
					);
			}
		}
		
		private function handleChartClick(event:AnyChartPointEvent):void {
			var customAttrs:Object = event.pointCustomAttributes;
			if (customAttrs.DrillType == "Drill Down") {
				var path:String = (customAttrs.targetURL == 'null' || customAttrs.targetURL == "'null'") ? null : customAttrs.targetURL;
				var evt:LinkNavigatorEvent = 
					new LinkNavigatorEvent(LinkNavigatorEvent.DRILL_DOWN, true, false, { filters: customAttrs.filters, columns: customAttrs.drillBy, reportPath: path });
				parent.dispatchEvent(evt);
			}
			else if (customAttrs.DrillType == "Drill To Dashboard") {
				var dashName:String = '';
				var pageName:String = '';
				
				var url:String = String(customAttrs.targetURL);
				if(url){
					var pageIdx:int = url.indexOf(',');
					if(pageIdx > 0){
						dashName = url.substring(0, pageIdx);
						pageName = url.substring(pageIdx + 1, url.length);
					}
				}
				
				var evt1:LinkNavigatorEvent = 
					new LinkNavigatorEvent(LinkNavigatorEvent.NAVIGATE, true, false, { filters:customAttrs.filters, dashName: dashName, pageName: decodeURIComponent(pageName)});
				parent.dispatchEvent(evt1);
			}
			else if (customAttrs.DrillType == "Olap Drill Down") {
				var evt2:LinkNavigatorEvent = 
					new LinkNavigatorEvent(LinkNavigatorEvent.OLAP_DRILL_DOWN, true, false, { value: customAttrs.filters });
				parent.dispatchEvent(evt2);
			}
		}
		
		private function updateWidthHeight(obj:Object):void{
			var objWidth:int = obj.x + obj.width;
			var objHeight:int = obj.y + obj.height;
			
			if (objWidth > this.width){
				width = objWidth;
			}	
			
			if (objHeight > this.height){
				height = objHeight;
			}
		}
		
		private function getCellTagType(parentXML:XML):String{
			var tagType:String = "";
			if (parentXML){
				var cellType:XMLList = parentXML.child("net.sf.jasperreports.crosstab.cell.type");
				if (cellType.length() > 0){
					tagType = cellType[0];
				}
			}
			return tagType;	
		}
		
		private function renderFrame(f:XML, xOrigin:uint, yOrigin:uint, parent:Canvas, parentXML:XML = null):void {
			var newXOrigin:uint = xOrigin + uint(f.@x);
			var newYOrigin:uint = yOrigin + uint(f.@y);
			
			var box:Canvas = new Canvas();
			box.x = newXOrigin;
			box.y = newYOrigin;
			box.width =  uint(f.@width);
			box.height = uint(f.@height);
			box.maxWidth = uint(f.@width);
			box.maxHeight = uint(f.@height);
			box.verticalScrollPolicy = ScrollPolicy.OFF;
			box.horizontalScrollPolicy = ScrollPolicy.OFF;
			parent.addChild(box);
			updateWidthHeight(box);
			renderChildren(f, 0, 0, box);
			
			var t:XMLList = f.child("net.sf.jasperreports.crosstab.cell.type");
			var type:String = t[0];
			var addToArray:Array = null;  //leftDimensions;
			if ("ColumnHeader" == type) {
				addToArray = topDimensions;
			}
			else if ("RowHeader" == type) {
				addToArray = leftDimensions;
			}
			if (addToArray != null) {
				for each (var child:Object in box.getChildren()) {
					addToArray.push(child);
				}
			}

		}

		private function renderLine(i:XML, xOrigin:uint, yOrigin:uint, parent:Canvas):void {
			var x:uint = xOrigin + uint(i.@x);
			var y:uint = yOrigin + uint(i.@y);
			var toX:uint = x + uint(i.@width);
			var toY:uint = y + uint(i.@height);
			if (String(i.@direction) == "BottomUp") {
				// switch direction
				var temp:uint = y;
				y = toY;
				toY = temp;
			}
			
			parent.graphics.lineStyle(2);
			parent.graphics.moveTo(x, y);
			parent.graphics.lineTo(toX, toY);
		}
		
		private function renderRectangle(i:XML, xOrigin:uint, yOrigin:uint, parent:Canvas):void {
			var lineWidth:Number = Number(i.@lineWidth);
			if (lineWidth > 0) {
				var clr: String = i.@lineColor;
				clr = "0x" + clr.substr(1, clr.length);
				var color:uint = new uint(clr);
				var rect:Sprite = new Sprite();
				var comp:UIComponent = new UIComponent();
				comp.x = xOrigin + uint(i.@x);
				comp.y = yOrigin + uint(i.@y);
				comp.width = uint(i.@width);
				comp.height = uint(i.@height);
				comp.addChild(rect);
				var transparent:String = i.@transparent;
				var alpha:Number = 1.0;
				if (transparent == "true")
					alpha = 0.0;
					
				clr = i.@backcolor;
				clr = "0x" + clr.substr(1, clr.length);
				var backColor:uint = new uint(clr);
				
				clr = i.GradientEndColor;
				if (clr) {
					clr = "0x" + clr;
					var gradientEndColor:uint = uint(clr);
					rect.graphics.beginGradientFill(GradientType.LINEAR, [backColor, gradientEndColor], [alpha,alpha], [10,245]);
				}
				else {
					rect.graphics.beginFill(backColor, alpha);
				}
				
				rect.graphics.lineStyle(lineWidth, color);
//				rect.graphics.drawRect(0, 0, uint(i.@width), uint(i.@height));
				rect.graphics.drawRoundRect(0, 0, uint(i.@width), uint(i.@height),
					uint(i.EllipseHeight), uint(i.EllipseHeight));
				rect.graphics.endFill();
				parent.addChild(comp);
			}
		}
		
		private function renderAdhocButton(i:XML, xOrigin:uint, yOrigin:uint, parent:Canvas):void {
			var btn:Button = new Button();
			btn.x = xOrigin + uint(i.@x);
			btn.y = yOrigin + uint(i.@y);
			btn.width = uint(i.@width);
			btn.height = uint(i.@height);
			btn.label = i.@value;
			var clr: String = i.@color;
			clr = "0x" + clr.substr(1, clr.length);
			var color:uint = new uint(clr);
			btn.setStyle("color", color);
			var colorArray:Array = new Array();
			clr = i.@backcolor;
			clr = "0x" + clr.substr(1, clr.length);
			color = new uint(clr);
			colorArray.push(color);
			btn.setStyle("fillAlphas", [1,1]);
			clr = i.GradientEndColor;
			if (clr) {
				clr = "0x" + i.GradientEndColor;
				color = uint(clr);
				colorArray.push(color);
				btn.setStyle("fillColors", colorArray);
			}
			else {
				btn.setStyle("fillColors", [color, color]);
			}
			btn.setStyle("cornerRadius", uint(i.CornerRadius));
			btn.setStyle("fontFamily", i.@face);
			btn.setStyle("fontSize", uint(i.@size));
			btn.setStyle("textAlign", i.@alignment);
			if (i.@bold == 'true') {
				btn.setStyle("fontWeight", "bold");
			}
			if (i.@italic == 'true') {
				btn.setStyle("fontStyle", "italic");
			}
			var link:String = i.@link;
			var haslink: Boolean = link != null && link.length > 0;
			var o:Object = new Object();
			o["link"] = link;
			o["target"] = i.@target;
			btn.data = o;
			btn.addEventListener(MouseEvent.CLICK, function(event:MouseEvent):void {
				var o:Object = event.target.data;
				LinkNavigator.staticNavigate(o["link"], o["target"], parent);
			});
			
			parent.addChild(btn);
		}
		
		private function renderImage(i:XML, xOrigin:uint, yOrigin:uint, parent:Canvas):void {
				var img: Image = new Image();
				img.x = xOrigin + uint(i.@x);
				img.y = yOrigin + uint(i.@y);
				if (uint(i.@width) > 0)
					img.width = uint(i.@width);
				if (uint(i.@height) > 0)
					img.height = uint(i.@height);
				img.maintainAspectRatio = false;	
				img.load(WS_ROOT + i.toString());	
				var ibackcolor: String = i.@backcolor;
				ibackcolor = "0x"+ibackcolor.substring(1,ibackcolor.length);
				var ibc: uint = new uint(ibackcolor);
				img.graphics.beginFill(ibc,1);
				img.graphics.drawRect(0,0,img.width,img.height);
				img.graphics.endFill();
				var link:String = i.@link;
				var haslink: Boolean = link != null && link.length > 0;
				if (haslink)
				{
					var target:String = i.@target;
					img.useHandCursor = true;
					img.buttonMode = true;
					img.mouseChildren = false;
					img.addEventListener(MouseEvent.CLICK, (new LinkNavigator(link, target, parent)).navigate, false, 0, false);
				}			
				parent.addChild(img);
				
				hasImage = true;
				updateWidthHeight(img);
		}		
		
		private function renderText(t:XML, xOrigin:uint, yOrigin:uint, parent:Canvas, parentXML:XML = null):void {
				if (showColumnLabelMenu && t.isMenu != null && String(t.isMenu) == "true" && t.columnIndex != null && uint(t.columnIndex) != 0) {
					hasText = true;
					
					var mb: ColumnLabelMenuBar = new ColumnLabelMenuBar(t);
					mb.showSortIcons = false; //Display sort icons or not
					mb.toolTip = ResourceManager.getInstance().getString("adhoc","APP_toolTip"); 
					mb.menuBarItemRenderer = new ClassFactory(LabelMenuBarItem);
					mb.x = xOrigin + uint(t.@x);
					mb.y = yOrigin + uint(t.@y);
					mb.width = uint(t.@width);
					mb.height = uint(t.@height);
					var bck: String = t.@backcolor;
					bck = "0x"+bck.substring(1,bck.length);
					var bci: uint = new uint(bck);
					if (String(t.@transparent) != 'true') {
						mb.setStyle("fillColors", [bci, bci]);
					}
						
					mb.setStyle("fontFamily", t.@face);
					mb.setStyle("fontSize", uint(t.@size));
					mb.setStyle("textAlign", t.@alignment);
					mb.setStyle("leading", -1);
					if (t.@italic == "true"){
						mb.setStyle("fontStyle", "italic");
					}
					if (t.@bold == "true"){
						mb.setStyle("fontWeight", "bold");
					}
					var clr: String = t.@color;
					clr = "0x" + clr.substr(1, clr.length);
					var color:uint = new uint(clr);
					mb.setStyle("color", color);
		
					if (addMenuBarItemFunc != null){
						addMenuBarItemFunc(mb, int(t.columnIndex), int(t.labelIndex), t.@value.toString(), t.@width.toString(), showColumnLabelMenu);
					}
					parent.addChild(mb);
					return;
				}
			
				var l: ColumnTextCell = new ColumnTextCell(t, parentXML, parent);
				var columnIndex:int = l.columnIndex;
				if (columnIndex > 0) {
					var columnsList:ArrayList = columnDrills[columnIndex] as ArrayList;
					if (columnsList != null && columnsList.length > 1) {
						var menu:ContextMenu = new ContextMenu();
						menu.hideBuiltInItems();
						for (var i:int = 0; i < columnsList.length; i++) {
							var drill:XML = XML(columnsList.getItemAt(i));
							var dc:XML = drill.DrillTo.DrillColumns.DrillColumn[0];
							var label:String = getDrillToLabel(dc);
							var colPropertiesItem:ContextMenuItem = new ContextMenuItem(label);
							menu.customItems.push(colPropertiesItem);
							colPropertiesItem.addEventListener(ContextMenuEvent.MENU_ITEM_SELECT, drillTo);
						}
						l.contextMenu = menu;
					}
				}
				parent.addChild(l);
				l.x = xOrigin + uint(t.@x);
				l.y = yOrigin + uint(t.@y);
				
				hasText = true;
				updateWidthHeight(l);
		}
		
		private function renderPromptGroupCheckbox(cb:XML, xOrigin:uint, yOrigin:uint, parent:Canvas, parentXML:XML = null):void {
			var l: PromptGroupCheckBox = new PromptGroupCheckBox(cb, parentXML, parent);
			parent.addChild(l);
			l.x = xOrigin + uint(cb.@x);
			l.y = yOrigin + uint(cb.@y);
		}
		
		private function renderPromptGroupDropDown(dd:XML, xOrigin:uint, yOrigin:uint, parent:Canvas, parentXML:XML = null):void {
			var l: PromptGroupDropDown = new PromptGroupDropDown(dd, parentXML, parent);
			parent.addChild(l);
			l.x = xOrigin + uint(dd.@x);
			l.y = yOrigin + uint(dd.@y);
		}
		
		private function renderPromptGroupEditableTextbox(tb:XML, xOrigin:uint, yOrigin:uint, parent:Canvas, parentXML:XML = null):void {
			var l: PromptGroupEditableTextbox = new PromptGroupEditableTextbox(tb, parentXML, parent);
			parent.addChild(l);
			l.x = xOrigin + uint(tb.@x);
			l.y = yOrigin + uint(tb.@y);
			l.text = tb.@value;
		}
		
		private function renderPromptGroupButton(bt:XML, xOrigin:uint, yOrigin:uint, parent:Canvas, parentXML:XML = null):void {
			var l: PromptGroupButton = new PromptGroupButton(bt, parentXML, parent, this);
			parent.addChild(l);
			l.x = xOrigin + uint(bt.@x);
			l.y = yOrigin + uint(bt.@y);
		}
		
		private function handleGridClicked(event:ListEvent):void {
			var grid:AdvancedDataGrid = (event.target as AdvancedDataGrid);
			var dg:XML = XML(grid.data);
			var row:int = event.rowIndex;
			var col:int = event.columnIndex;
			
			var data:XML = XML(grid.dataProvider[row]);
			var lookup:XMLList = dg["Column" + col + "DrillThru"];
			if (lookup && lookup.length() > 0) {
				var look:XML = XML(lookup[0]);
				var l:String = look.toString();
				var reg:RegExp = /\"\s*\+.*\+\s*\"/g;
				var matches:Array = l.match(reg);
				for each (var s:String in matches) {
					var i:int = s.indexOf("$F{");
					if (i > 0) {
						var j:int = s.lastIndexOf("}");
						if (j > 0) {
							var name:String = s.substr(i + 3, j - i - 3);
							name = getLabel(data.Col, name, grid);
							l = l.replace(s, name);
						}
					}
				}
				if (l.charAt(0) == '"')
					l = l.substr(1);
					
				LinkNavigator.staticNavigate("javascript:" + l, "", grid);
			}
			
		}

		private function setupDataGridProvider(result:ResultEvent):void {
			var rs:WebServiceResult = WebServiceResult.parse(result); 
			if (rs.isSuccess()) {
				var res:XML = rs.getResult()[0];
				rs.getCallerData().dataProvider = res..Row;
			}
			else {
				Alert.show("Error", "Could not execute query");
			}
		}
		private function labelFunction(item:Object, column:AdvancedDataGridColumn):String {
			var cols:XMLList = XML(item).Col;
			return getLabel(cols, column.dataField, null); //(column as Object).grid);
		}
		
		private function getLabel(cols:XMLList, name:String, parent:AdvancedDataGrid):String {
			for (var i:uint = 0; i < cols.length(); i++) {
				var colName:String = cols[i].@ColName;
				if (colName == name) {
					var s:String = cols[i].@ColValue;
					if (parent) { 
						var dg:XML = XML(parent.data);
						var type:String = dg["Column" + i + "Type"];
						if (type == "java.util.GregorianCalendar") {
							var format:String = dg["Column" + i + "Format"];
							if (format) {
								var dateFormat:BDateFormatter = new BDateFormatter();
								dateFormat.formatString = format.toUpperCase();
								s = dateFormat.format(s);
							}
						}
					}
					return s;
				}
			}
			return "";
		}
		
		private function drillTo(event:ContextMenuEvent):void {
			var ctc:ColumnTextCell = (event.contextMenuOwner as ColumnTextCell);
			var caption:String = (event.target as ContextMenuItem).caption;
			var columnIndex:int = ctc.columnIndex;
			var link:String = ctc.link;
			var target:String = ctc.target;
			var columnsList:ArrayList = columnDrills[columnIndex] as ArrayList;
			for (var i:int = 0; i < columnsList.length; i++) {
				var drill:XML = XML(columnsList.getItemAt(i));
				var dc:XML = drill.DrillTo.DrillColumns.DrillColumn[0];
				var label:String = getDrillToLabel(dc);
				if (label != caption) {
					continue;
				}
				var index:int = link.indexOf("<DrillColumn>");
				var lastIndex:int = link.indexOf("</DrillColumns>");
				if (index > 0) {
					link = link.substr(0, index) + dc.toXMLString() + link.substring(lastIndex);
				}
				var ln:LinkNavigator = new LinkNavigator(link, target, ctc);
				ln.navigate(null);
				break;
			}
		}
		
		private function getDrillToLabel(dc:XML):String{
			var label:String = dc.Label;
			if(label == null || label == ""){
				label = dc.Dimension;
				if (label != "") {
					label = label + ".";
				}
				label = label + dc.Column;
			}
			return label;
		}
		
		// selection rectangle mouse events
		private function onMouseDown(e:MouseEvent):void {
			// Per session in Designer control of enable / disable rectangle select
			if(Application.application.hasOwnProperty('isRectangleSelectOn')){
				if(!Application.application.isRectangleSelectOn){
					return;
				}
			}
			var t:DisplayObject = e.target as DisplayObject;
			while (t != null) {
				if (t is ScrollBar) 
					return;
				
				if (t is UMapperMaps)
					return;
					
				if (t == parent)
					break;

				if (!(t.parent is DisplayObject))
					break;
										
				t = t.parent;
			}
			Tracer.debug('rectangle onMouseDown', this);
			rectangleMoved = false;
			
			
			Tracer.debug('rectangle addMouseMoveListener, r = ' + r, this);
			parent.addEventListener(MouseEvent.MOUSE_MOVE, onMouseMove);
		} 
		
		private function onMouseMove(e:MouseEvent):void {
			rectangleMoved = true;
			Tracer.debug('rectangle onMouseMove, r = ' + r, this);
			
			if(r == null){
				Tracer.debug('!r', this);
				var ptx:Point = new Point(e.stageX, e.stageY);
				ptx = parent.globalToContent(ptx);
				r = new Rectangle(Number(ptx.x), Number(ptx.y));
				selectionRectangle.x = r.x;
				selectionRectangle.y = r.y;
				selectionRectangle.width = r.width;
				selectionRectangle.height = r.height;
				parent.addChild(selectionRectangle);
				
				return;
			}
			
			
			var pt:Point = new Point(e.stageX, e.stageY);
			pt = parent.globalToContent(pt);
			r.bottomRight = pt;
			
			selectionRectangle.width = r.width;
			selectionRectangle.height = r.height;
		}
		
		private function handleMultipleChartSelect(event:AnyChartMultiplePointsEvent):void {
			Tracer.debug("multiple chart select " + event);
			var pts:Array = event.points;
			var columns:Array = [];
			for each (var pt:AnyChartPointInfo in pts) {
				for (var i:uint = 0; i < MAX_CATEGORIES; i++) {
					if (pt.pointCustomAttributes.hasOwnProperty("column" + i) == false)
						break;
						
					var dim:String = pt.pointCustomAttributes["dimension" + i];
					
					var col:String = pt.pointCustomAttributes["column" + i];
					var hier:String = pt.pointCustomAttributes["hierarchy" + i];
					var name:String = dim + "." + col;
					var columnData:Object = columns[name];
					if (columnData == null) {
						columnData = [];
						if (dim == "EXPR")
							columnData["columnType"] = "Expression";
						else
							columnData["columnType"] = "dimension";
						columnData["dimension"] = dim;
						columnData["column"] = col;
						if (hier)
							columnData["hierarchy"] = hier;
						columnData["datetimeFormat"] = pt.pointCustomAttributes["datetimeFormat" + i];
						columnData["values"] = [];
						columnData["label"] = pt.pointCustomAttributes["label" + i];
						columns[name] = columnData;
					}
					columnData["values"].push(pt.pointCustomAttributes["value" + i]);
				}
			}
			sendSelectionEvent(columns);
		}
		
		private function onMouseUp(e:MouseEvent):void {

			Tracer.debug('rectangle onMouseUp', this);
			Tracer.debug('rectangle removeMouseMoveListener', this);
			parent.removeEventListener(MouseEvent.MOUSE_MOVE, onMouseMove);
			
			if (rectangleMoved){
				Tracer.debug('rectangleMoved: true', this);
				r = null;
				rectangleMoved = false;
				
				var children:Array = parent.getChildren();
				selectedItems = new Array();
				var gridSelected:Array = new Array();
				for (var i:int = 0; i < children.length; i++) {
					if ((children[i] != selectionRectangle) &&
					  (selectionRectangle.getRect(parent).intersects((children[i] as DisplayObject).getRect(parent)))) {
						  if (children[i] is ColumnTextCell) {
						  	selectedItems.push(children[i]);
						  }
						  else if (children[i] is Canvas) {
						  	var ch:Array = (children[i] as Canvas).getChildren();
						  	if (ch && ch.length > 0) {
						  		for each (var child:Object in ch) {
						  			if (child is ColumnTextCell && (child as ColumnTextCell).isData == true) {
						  				var relatedDims:Array = findRelatedDims(child as ColumnTextCell);
						  				selectedItems = selectedItems.concat(relatedDims);
						  			}
						  			else {
						  				children = children.concat(child);	
						  			}
						  		}
						  	}
						  }
						  else if (children[i] is AdvancedDataGrid) {
						  	gridSelected.push(children[i]);
						  }
					  }
				}
				
				if (gridSelected.length > 0) {
					var firstCol:uint; 
					var lastCol:uint;
					var firstRow:int;
					var lastRow:int;
					var j:uint;
					
					for each (var grid:AdvancedDataGrid in gridSelected) {
						var rect:Rectangle = selectionRectangle.getRect(grid);
						var left:uint = 0;
						lastCol = grid.columnCount;
						lastRow = grid.rowCount;
						firstCol = 0;
						firstRow = 0; 
						for (j = 0; j < grid.columnCount; j++ ) {
							var col:AdvancedDataGridColumn = grid.columns[j];
							if (rect.x > left && (left + col.width) < rect.x)
								firstCol = j;
								
							 if (rect.right > left && rect.right < left + col.width)
							 	lastCol = j;  
							 	
							left = left + col.width;
						}
						
						var top:uint = rect.y; // - grid.headerHeight;
						var bottom:uint = rect.bottom; // - grid.headerHeight;
						firstRow = -1;
						lastRow = grid.rowCount;
						var column:AdvancedDataGridColumn = grid.columns[firstCol] as AdvancedDataGridColumn;
						var dp1:ArrayCollection = grid.dataProvider as ArrayCollection;
						for (j = 0; j < dp1.length; j++) {
							var o:Object = dp1[j];
							var renderer:IListItemRenderer = grid.itemToItemRenderer(o);
							var rect1:Rectangle = renderer.getRect(grid);
							if (lastRow == grid.rowCount && rect1.bottom >= bottom)
								lastRow = j;
							if (firstRow < 0 && rect1.bottom >= top)
								firstRow = j; 
						}
						
						if (bottom > 0) {
							var dataDescriptors:XMLListCollection = XMLListCollection(grid.data);
							for (j = firstCol; j <= lastCol; j++) {
								var colDescriptor:XML = XML(dataDescriptors[j]);
								var reportIndex:String = colDescriptor.columnIndex;
								var cos:XMLList = null;
								if (caller is DashletContentBase) {
									cos = (caller as DashletContentBase).getColumnXML(reportIndex);
								}
								else 
									cos = AdHocConfig.instance().getColumnXML(reportIndex);
								if (cos && cos.length() > 0) {
									var co:XML = cos[0];
									for (var k:uint = firstRow; k <= lastRow; k++) {
										var selected:Object = new Object();
										selected["label"] = AdHocConfig.instance().getLabelNameByColumnXML(co);
										selected["column"] = String(co.Column);
										selected["dimension"] = String(co.Dimension);
										selected["columnType"] = String(co.ColumnType);
										selected["datetimeFormat"] = String(co.DateTimeFormat);
										selected["hierarchy"] = String(co.Hierarchy);
									
										var dp:ArrayCollection = ArrayCollection(grid.dataProvider);
										var row:Object = dp[k];
										var rw:XML = XML(row[reportIndex]);
										var ssv:String = rw.@value;
										selected["value"] = ssv;
										
										selectedItems.push(selected);
									}
								}
							}
						}
					}
				}
				
				if (selectedItems.length > 0) {
					var columns:Array = [];
					for (i = 0; i < selectedItems.length; i++) {
						var columnName:String = selectedItems[i].column;
						if (columnName == "")
							continue;
							
						if (selectedItems[i].columnType == "Dimension") {
							if (selectedItems[i].hierarchy) {
								columnName = selectedItems[i].dimension + "-" + selectedItems[i].hierarchy + "." + columnName;
							}
							else {
								columnName = selectedItems[i].dimension + "." + columnName;
							}
						}

						var columnData:Object = columns[columnName];
						if (columnData == null) {
							columnData = [];
							columnData["columnType"] = selectedItems[i].columnType;
							columnData["dimension"] = selectedItems[i].dimension;
							columnData["hierarchy"] = selectedItems[i].hierarchy;
							columnData["column"] = selectedItems[i].column;
							columnData["datetimeFormat"] = selectedItems[i].datetimeFormat;
							columnData["label"] = selectedItems[i].label;
							columnData["values"] = new ArrayCollection();
							columns[columnName] = columnData;
						}
						if (!(columnData["values"] as ArrayCollection).contains(selectedItems[i].value))
							(columnData["values"] as ArrayCollection).addItem(selectedItems[i].value);
					}
					
					for (var s:String in columns) {
						var cd:Object = columns[s];
						cd["values"] = ((cd["values"] as ArrayCollection).source);
					}
					
					sendSelectionEvent(columns);
				}
			}
					
			//It is ok if the parent doesn't have the child, as we sometimes respond only on the mouseUp event
			//the mouseDown event was never on our canvas, but on the outer canvas, hence selectionRectangle was never added
			try{
				parent.removeChild(selectionRectangle);
			}catch(e:Error){
			}
		}
		
		private function sendSelectionEvent(columns:Array):void {
			var containsData:Boolean = false;
			var data:XML = XML("<Filters/>");
			for each (var c:Array in columns) {
				var filter:XML = XML("<Filter/>");
					
				containsData = true;
				var cType:XML = XML("<ColumnType>" + c["columnType"] + "</ColumnType>");
				filter.appendChild(cType);
				var dim:XML   = XML("<Dimension>" + c["dimension"] + "</Dimension>");
				filter.appendChild(dim);
				if (c.hasOwnProperty("hierarchy")) {
					var hier:XML  = XML("<Hierarchy>" + c["hierarchy"] + "</Hierarchy>");
					filter.appendChild(hier);
				}
				var col:XML   = XML("<Column>" + c["column"] + "</Column>");
				filter.appendChild(col);
				var label:XML = XML("<Label>" + c["label"] + "</Label>");
				filter.appendChild(label);
				
				//If it is a datetime object, it will have a format
				var fmt:String = c["datetimeFormat"];
				if (fmt){
					filter.appendChild(new XML("<datetimeFormat>" + fmt + "</datetimeFormat>"));
				}
				
				var val:XML = XML("<selectedValues/>");
				for (var j:int = 0; j < c["values"].length; j++) {
					var v:XML = XML("<selectedValue>" + c["values"][j] + "</selectedValue>");
					val.appendChild(v);
				}
				filter.appendChild(val);
				data.appendChild(filter);
			}
			
			if (parent && containsData){
				parent.dispatchEvent(new ReportRendererEvent(ReportRendererEvent.RECTANGLE_SELECT, true, false, data));
			}
		}
		
		private function findRelatedDims(child:ColumnTextCell):Array {
			var ret:Array = new Array();
			
			var childPt:Point = new Point(child.x + (child.width / 2), child.y + (child.height / 2));
			childPt = child.localToGlobal(childPt);
			for each (var o:Object in topDimensions) {
				if (o is ColumnTextCell) {
					var c:ColumnTextCell = (o as ColumnTextCell);
					var pt:Point = new Point(c.x, 0);
					pt = c.localToGlobal(pt);
					if (childPt.x > pt.x && childPt.x < pt.x + c.width)
						ret = ret.concat(c);
				} 
			}
			
			for each (var p:Object in leftDimensions) {
				if (p is ColumnTextCell) {
					var d:ColumnTextCell = (p as ColumnTextCell);
					var height:int = d.height;
					if (d.parent.numChildren == 1) {
						height = d.parent.height;
					}
					var dpt:Point = new Point(0, d.y);
					dpt = d.localToGlobal(dpt);
					if (childPt.y > dpt.y && childPt.y < dpt.y + height)
						ret = ret.concat(d);
				}
			}
			
			return ret;
		}
	}
	
}