package com.birst.flex.adhoc
{
	import com.birst.flex.core.Tracer;
	import com.birst.flex.core.Util;
	
	import flash.display.CapsStyle;
	import flash.display.JointStyle;
	import flash.display.LineScaleMode;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	
	import mx.containers.Canvas;
	import mx.controls.Label;
	import mx.managers.CursorManager;

	public class Band
	{
		public static const RULER_WIDTH: int = 10;
		public static const RULER_HEIGHT: int = 10;
		private static const BACKGROUND:uint = 0xFFFFFF;
		private static const BORDER:uint = 0x000000;

		protected var name:String;
		protected var label:String;
		protected var startHeight:Number;
		protected var offsetY:Number;
		protected var s:Canvas;
		protected var l:Layout;
		protected var labelObject: Label;
		protected var maxHeight:int;
		private var isDragging: Boolean = false;
		[Embed("/images/verticalSizer.gif")]
		private var hResizeCursor:Class;
		private var currentCursorId:int;

		public function Band(label: String, name: String, l: Layout, maxHeight: int)
		{
			s = new Canvas();
			labelObject = new Label();
			s.addChild(labelObject);
			labelObject.text = label;
			labelObject.setStyle("textAlign", "center");
			labelObject.setStyle("color", "0xAAB3B3");
			s.toolTip = name;
			
			this.name = name;
			this.label = label;
			this.l = l;
			this.maxHeight = maxHeight;
			l.LayoutCanvas.addChild(s);
			if (width > 0)
			{
				s.addEventListener(MouseEvent.MOUSE_DOWN, startDragging);
				s.addEventListener(MouseEvent.MOUSE_UP, stopDragging);
				s.addEventListener(MouseEvent.MOUSE_MOVE, setSizerIcon);
				s.addEventListener(MouseEvent.MOUSE_OUT, clearSizerIcon);
			}
			currentCursorId = -1;
		}
		
		public function get Name(): String
		{
			return name;
		}
		
		public function get X(): int
		{
			return s.x;	
		}

		public function get Y(): int
		{
			return s.y;	
		}

		public function get width(): int
		{
			var xml:XML = AdHocConfig.instance().getConfig();
			return int(xml.PageWidth[0]) - int(xml.RightMargin[0]) - int(xml.LeftMargin[0]);	
		}
		
		public function get height(): int
		{
			var xml:XML = AdHocConfig.instance().getConfig();
			var groupNum:int;
			if (this.name == "Title")
				return int(xml.PageInfo.TitleBandHeight[0]);
			if (this.name == "Header") {
				return int(xml.PageInfo.HeaderBandHeight[0]);
			}
			if (this.name == "Detail") {
				return int(xml.PageInfo.DetailBandHeight[0]);
			}
			if (this.name == "Summary") {
				return int(xml.PageInfo.SummaryBandHeight[0]);
			}
			if (this.name == "Page Header") {
				return int(xml.PageInfo.PageHeaderBandHeight[0]);
			}
			if (this.name == "Page Footer") {
				return int(xml.PageInfo.PageFooterBandHeight[0]);
			}
			var pageInfos:XMLList;
			if (this.name.indexOf("GroupHeader") == 0) {
				groupNum = uint(this.name.substr(("GroupHeader" as String).length));
				pageInfos = xml.PageInfo;
				if (pageInfos != null && pageInfos.length() > 0) {
					if (pageInfos[0].GroupHeader != null && pageInfos[0].GroupHeader.length() > groupNum) {
						return int(pageInfos[0].GroupHeader[groupNum].Height[0]);
					}
				}
			}
			if (this.name.indexOf("GroupFooter") == 0) {
				groupNum = uint(this.name.substr(("GroupFooter" as String).length));
				pageInfos = xml.PageInfo;
				if (pageInfos != null && pageInfos.length() > 0) {
					if (pageInfos[0].GroupFooter != null && pageInfos[0].GroupFooter.length() > groupNum) {
						return int(pageInfos[0].GroupFooter[groupNum].Height[0]);
					}
				}
			}
			return 0;	
		}

		public function set height(height:int): void
		{
			var xml:XML = AdHocConfig.instance().getConfig();
			var maxHeight:int = int(xml.PageHeight[0]) - int(xml.TopMargin[0]) - int(xml.BottomMargin[0]);
			if (this.name == "Header") {
				var detailHeight:int = xml.PageInfo.DetailBandHeight[0];
				maxHeight = maxHeight - detailHeight - 50;
			}
			if (height > maxHeight) {
				height = maxHeight;
				stopDragging(null);
				Util.localeAlertError('LY_failBdMax', null, null, [height]);
			}	
			var groupNum:int;
			if (this.name == "Header") {
				xml.PageInfo.HeaderBandHeight[0] = height;
			}
			else if (this.name == "Title") {
				xml.PageInfo.TitleBandHeight[0] = height;
			}
			else if (this.name == "Detail") {
				xml.PageInfo.DetailBandHeight[0] = height;
			}
			else if (this.name == "Summary") {
				xml.PageInfo.SummaryBandHeight[0] = height;
			}
			else if (this.name == "Page Footer") {
				xml.PageInfo.PageFooterBandHeight[0] = height;
			}				
			else if (this.name.indexOf("GroupHeader") == 0) {
				groupNum = uint(this.name.substr(("GroupHeader" as String).length));
				xml.PageInfo.GroupHeader[groupNum].Height[0] = height;
			}
			else if (this.name.indexOf("GroupFooter") == 0) {
				groupNum = uint(this.name.substr(("GroupFooter" as String).length));
				xml.PageInfo.GroupFooter[groupNum].Height[0] = height;
			}
			else {
				Tracer.debug("setting height of unknown band - \"" + this.name + "\"", this);
			}
		}
		
		public function draw(y: int):int
		{
			s.graphics.clear();
			s.y = y;
			s.graphics.beginFill(BACKGROUND);
			labelObject.width = width*l.Zoom;
			var paddingTop:int = 0;
			if (l.Ruler) {
				paddingTop = RULER_HEIGHT;
			}
			labelObject.setStyle("paddingTop", paddingTop);
			labelObject.setStyle("fontSize", int(10*l.Zoom));
			if (isDragging == true) {
				labelObject.text = label + " <height: " + height + " width: " + width + ">";
			}
			else {
				labelObject.text = label;
			}

			if (height < 5) {
				labelObject.visible = false;
			} 
			else {
				labelObject.visible = true;
			}
			var left: int = 0;
			var top: int = 0;
			if (l.Ruler)
			{
				left = RULER_WIDTH;
				top = RULER_HEIGHT;
				s.graphics.lineStyle(1, BORDER, .5, false, LineScaleMode.NORMAL, CapsStyle.ROUND, JointStyle.ROUND, 2);
				s.graphics.drawRect(left, 0, width*l.Zoom, top);
				s.graphics.drawRect(0, top, left, height*l.Zoom);
				var pixelsPerUnit: int;
				if (l.RulerUnit == 0)
				{
					var h: int;
					for(var x:int = 0; x < width*l.Zoom; x += 9*l.Zoom)
					{
						s.graphics.moveTo(left+x,0);
						if (x % 9 == 0)
							h = 2;
						if (x % 18 == 0)
							h = 4;
						if (x % 36 == 0)
							h = 6;
						if (x % 36 == 0)
							h = 8;
						if (x % 72 == 0)
							h = 10;
						s.graphics.lineTo(left+x,h);
					}								
					for(y = 0; y < height*l.Zoom; y += 9*l.Zoom)
					{
						s.graphics.moveTo(0,top+y);
						if (x % 9 == 0)
							h = 2;
						if (x % 18 == 0)
							h = 4;
						if (x % 36 == 0)
							h = 6;
						if (x % 36 == 0)
							h = 8;
						if (x % 72 == 0)
							h = 10;
						s.graphics.lineTo(h,top+y);
					}								
				}
			}
			s.graphics.lineStyle(1, BORDER, .1, false, LineScaleMode.NORMAL, CapsStyle.ROUND, JointStyle.ROUND, 2);
			s.graphics.drawRect(left, top, width*l.Zoom, height*l.Zoom);
			s.graphics.lineStyle(1, 0xCCCCCC, 1, false, LineScaleMode.NORMAL, CapsStyle.ROUND, JointStyle.ROUND, 3);
			s.graphics.moveTo(left,top+height*l.Zoom);
			s.graphics.lineTo(left+width*l.Zoom,top+height*l.Zoom);
			s.graphics.moveTo(left,top+height*l.Zoom+3);
			s.graphics.lineTo(left+width*l.Zoom,top+height*l.Zoom+3);
			s.graphics.lineStyle(1, 0x777777, 1, false, LineScaleMode.NORMAL, CapsStyle.ROUND, JointStyle.ROUND, 3);
			s.graphics.moveTo(left,top+height*l.Zoom+1);
			s.graphics.lineTo(left+width*l.Zoom,top+height*l.Zoom+1);
			s.graphics.moveTo(left,top+height*l.Zoom+2);
			s.graphics.lineTo(left+width*l.Zoom,top+height*l.Zoom+2);
			s.graphics.lineStyle(1, 0xCCCCCC, .4, false, LineScaleMode.NORMAL, CapsStyle.NONE, JointStyle.ROUND, 1);
			if (l.ShowGrid && l.Snap > 1)
			{
				var maxX: int = width * l.Zoom;
				var maxY: int = height * l.Zoom;
				var i:int;
				var offset:Number;
				for (i = l.Snap; i < width; i += l.Snap)
				{
					offset = i * l.Zoom + left; 
					s.graphics.moveTo(offset,top);
					s.graphics.lineTo(offset,top+height*l.Zoom);
				}
				for(i = l.Snap; i < height; i += l.Snap)
				{
					offset = i * l.Zoom + top; 
					s.graphics.moveTo(left,offset);
					s.graphics.lineTo(left+width*l.Zoom,offset);
				}				
			}
			s.graphics.endFill();
			updateLayoutObjects();
			return (height*l.Zoom)+4+top;
		}
	
		public function updateLayoutObjects(): void
		{
			var left: int = 0;
			var top: int = 0;
			if (l.Ruler)
			{
				left = RULER_WIDTH;
				top = RULER_HEIGHT;
			}
			// update any objects in band
			for each(var lo: LayoutObject in this.l.Objects)
			{
				var xml:XML = lo.entityXML;
				var bandname:String = xml.Band
				if (bandname.indexOf("GroupHeader") == 0 || bandname.indexOf("GroupFooter") == 0) {
					bandname = bandname + xml.GroupNumber
				}
				var showInReport:Boolean = xml.showInReport.toString() == 'true';
				if ((bandname == this.Name && showInReport) || 
					(this.Name == ColumnArea.NAME && (bandname == "" || showInReport == false)))
				{
					var basey: int = lo.BaseY;
					lo.BaseY = lo.BaseY*l.Zoom + s.y + top;
					lo.BaseX = lo.BaseX*l.Zoom + left;
					lo.updateLayout(this.s);					
				}
			}
		}

		public function clearSizerIcon(event:MouseEvent):void
		{
			CursorManager.removeCursor(currentCursorId);
			currentCursorId = -1;
		}
		public function setSizerIcon(event:MouseEvent):void
		{
			if (l.LayoutCanvas == null || l.LayoutCanvas.visible == false) {
				return;
			}
			
			var top: int = 0;
			if (l.Ruler)
				top = RULER_HEIGHT;
				
            var pt:Point = new Point(event.localX, event.localY);
            pt = event.target.localToGlobal(pt);
            pt = l.LayoutCanvas.globalToContent(pt);

			if ((pt.y - top - (s.y + height*l.Zoom) < 4) && (pt.y - top > s.y + height*l.Zoom))
			{
				if (currentCursorId == -1){
					currentCursorId = CursorManager.setCursor(hResizeCursor,2,-10, -10);
				}
			}
			else {
				clearSizerIcon(event);
			}
		}
		public function startedDraggingItem():void
		{
			s.removeEventListener(MouseEvent.MOUSE_DOWN, startDragging);
			s.removeEventListener(MouseEvent.MOUSE_UP, stopDragging);
		
			s.removeEventListener(MouseEvent.MOUSE_MOVE, setSizerIcon);
			s.removeEventListener(MouseEvent.MOUSE_OUT, clearSizerIcon);
		}
		public function stopDraggingItem():void
		{
			s.addEventListener(MouseEvent.MOUSE_DOWN, startDragging);
			s.addEventListener(MouseEvent.MOUSE_UP, stopDragging);

			s.addEventListener(MouseEvent.MOUSE_MOVE, setSizerIcon);
			s.addEventListener(MouseEvent.MOUSE_OUT, clearSizerIcon);
		}
		public function startDragging(event:MouseEvent):void
		{
			if (l.LayoutCanvas == null) {
				return;
			}
			var top: int = 0;
			if (l.Ruler)
				top = RULER_HEIGHT;
				
            var pt:Point = new Point(event.localX, event.localY);
            pt = event.target.localToGlobal(pt);
            pt = l.LayoutCanvas.globalToContent(pt);

			if ((pt.y - top - (s.y + height*l.Zoom) < 4) && (pt.y - top > s.y + height*l.Zoom))
			{
				isDragging = true;
				event.stopImmediatePropagation();
				offsetY = event.stageY;
				startHeight = height;
				// tell Flash Player to start listening for the mouseMove event
				l.LayoutCanvas.addEventListener(MouseEvent.MOUSE_MOVE, drag);
			}
		}

		public function stopDragging(event:MouseEvent):void
		{
			if (isDragging) {
				isDragging = false;
				// Tell Flash Player to stop listening for the mouseMove event.
				if (l.LayoutCanvas != null) {
					l.LayoutCanvas.removeEventListener(MouseEvent.MOUSE_MOVE, drag);
				}
				AdHocConfig.instance().asyncSaveConfigToServer("Band", 'stopDragging');
			}
		}

		private function mouseOut(event:MouseEvent):void
		{
			stopDragging(event);
		}
		
		private function drag(event:MouseEvent):void
		{
			if (isDragging == false) {
				return;
			}
			
			if (event.buttonDown == false) {
				stopDragging(event);
				return;
			}
			/*
            var pt:Point = new Point(event.localX, event.localY);
            pt = event.target.localToGlobal(pt);
            if (pt.x > l.LayoutCanvas.width || pt.x < 0 || pt.y > (l.LayoutCanvas.height - 10) || pt.y < 0) {
            	stopDragging(event);
            	return;
            }
            */
            var stageY:int = event.stageY;
            if (event.stageY > l.LayoutCanvas.height) {
            	Tracer.debug("Changing event y from " + stageY, this);
            	stageY = l.LayoutCanvas.height - 1;
            }
            
			var newheight:int = startHeight + (stageY - offsetY)/l.Zoom;
			Tracer.debug("newheight is " + newheight + " startHeight is " + startHeight + " offsetY is " + offsetY + " zoom is " + l.Zoom + " stage.y is " + stageY, this); 
			if (newheight >= 0)
			{
				if (maxHeight > 0 && newheight > maxHeight) {
					newheight = maxHeight;
				}
				if (height != newheight) {
					height = newheight;
					// Instruct Flash Player to refresh the screen after this event.				
					l.drawBands();
					event.updateAfterEvent();
				}
			}
		}
	}
}