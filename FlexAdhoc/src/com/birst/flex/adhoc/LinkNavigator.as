package com.birst.flex.adhoc
{
	import com.birst.flex.core.LinkNavigatorEvent;
	import com.birst.flex.core.Tracer;
	import com.birst.flex.core.Util;
	
	import flash.events.Event;
	import flash.external.ExternalInterface;
	import flash.net.URLRequest;
	import flash.net.navigateToURL;
	
	import mx.core.UIComponent;
	import mx.utils.StringUtil;
	
	[ResourceBundle('sharable')]
	public class LinkNavigator
	{
		private var link: String;
		private var target: String;
		private var component: UIComponent;
		
		public function LinkNavigator(link: String, target: String, uicomponent:UIComponent)
		{
			this.link = link;
			this.target = target;
			this.component = uicomponent;
		}
	
		public function navigate(event: Event): void
		{
			if (target == null || target.length == 0) {
				target = "_self";
			}
			staticNavigate(link, target, component);
		}
		
		public static function staticNavigate(link:String, target:String, component:UIComponent):void{
			var params:String;
			var p:Array;
			if ((link.indexOf("javascript:AdhocContent.DrillThru") == 0) || link.indexOf("window.parent.AdhocContent.DrillThru") > 0) {
				params = link.substr("javascript:AdhocContent.DrillThru".length);
				// trim off the ( )
				params = trim(params);
				p = params.split(",");
				if ((p[2] as String).indexOf("Drill To Dashboard") > 0) {
					// drill to dashboard
					// generate URL
					var fs:String = trim(p[3]);
					/*
					var dName:String = trim(p[0]);
					var d:Array = dName.split("_");
					if (d.length > 2) {
						// either the dash or the page has an underscore in it.  find the dashboard from the list
						var ws:WebServiceClient = new WebServiceClient('Dashboard', 'getDashboardListLiteCached', Components.LINK_NAVIGATOR, 'populateDashboard');
						ws.setResultListener(function (result:ResultEvent):void {
							var rs:WebServiceResult = WebServiceResult.parse(result, true);
							if (rs.isSuccess()){
								var dbRoot:XMLList = rs.getResult().Dashboards;
								if (dbRoot.length() > 0){
									var dashboards:XMLList = dbRoot[0].Dashboard;	
									for (var i:int = 0; i < dashboards.length(); i++){
										var db:XML = dashboards[i];
										var pageList:XMLList = db.Page;
										for (var j:int = 0; j < pageList.length(); j++) {
											var page:XML = pageList[j];
											var dashboard:String = db.@name;
											var pg:String = page.@name;
											var label:String = dashboard + '_' + pg;
											if (dName == label) {
												var evt2:LinkNavigatorEvent = 
													new LinkNavigatorEvent(LinkNavigatorEvent.NAVIGATE, true, false, { filters: fs, dashName: dashboard, pageName: pg});
												component.dispatchEvent(evt2);
												return;
											}
										}
									}
								}
							}
						});
						ws.execute();
						return;
					}
					*/
					var evt1:LinkNavigatorEvent = 
						new LinkNavigatorEvent(LinkNavigatorEvent.NAVIGATE, true, false, { filters: fs, dashName: trim(p[0]), pageName: trim(p[1])});
					component.dispatchEvent(evt1);
				}
				else if ((p[1] as String).indexOf("Drill Down") > 0) {
					if (component){
						var fils:String = trim(p[2]);
						var cols:String = trim(p[3]);
						var path:String = trim(p[0]);
						var reportPath:String = (path == 'null') ? null : path;
						var evt:LinkNavigatorEvent = 
							new LinkNavigatorEvent(LinkNavigatorEvent.DRILL_DOWN, true, false, { filters: fils, columns: cols, reportPath: reportPath});
						component.dispatchEvent(evt);
					}
				}
				
			}
			else if (link.indexOf("javascript:AdhocContent.OlapDrillThru") == 0) {
				params = link.substr("javascript:AdhocContent.OlapDrillThru".length);
				// trim off the ( )
				params = trim(params);
				p = params.split(",");
				var value:String = trim(p[0]);
				var evt3:LinkNavigatorEvent = new LinkNavigatorEvent(LinkNavigatorEvent.OLAP_DRILL_DOWN, true, false, { value: value});
				component.dispatchEvent(evt3);
				
			}
			else if (link.substr(0,11) == "javascript:")
			{
				link = link.substr(11);
				var index:int = link.indexOf("(");
				var jsfunc:String = link.substr(0, index);
				link = link.substr(index + 1);
				index = link.indexOf(")");
				if (index > 0) {
					link = link.substr(0, index);
				}
				var links:Array = link.split(",");
				var paramsA:Array = new Array();
				for (var i:int = 0; i < links.length; i++) {
					var p1:String = links[i];
					p1 = StringUtil.trim(p1);
					if (p1.indexOf("'") == 0) {
						p1 = p1.substr(1);
					}
					if (p1.lastIndexOf("'") == p1.length - 1) {
						p1 = p1.substr(0, p1.length - 1);
					}
					paramsA.push(p1);
				}
				
				if (ExternalInterface.available) {
					ExternalInterface.marshallExceptions = true;
					try {
						ExternalInterface.call(jsfunc, paramsA);
					}
					catch (e:Error) {
						Util.localeAlertError('LK_failFunc', null, 'Tracer.errorble', [ e.toString() ]);
						Tracer.error("Calling function " + jsfunc);
						for (i = 0; i < paramsA.length; i++) {
							Tracer.error("Parameter " + i + " is " + paramsA[i]);
						}
						Tracer.error("Error message " + e.message.toString());
						Tracer.error("Error name " + e.name);
						Tracer.error("Error id " + e.errorID);
					}
				}
				else {
					Util.localeAlertError('LK_failJS', null, 'sharable');
				}
			}
			else {
				navigateToURL(new URLRequest(link), target as String);
			}
		}
		
		private static function trim(s:String): String {
			s = StringUtil.trim(s);
			var l:int = s.length;
			return s.substr(1, l - 2);
		}
	}
}