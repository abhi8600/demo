package com.birst.flex.adhoc
{
	import mx.controls.advancedDataGridClasses.AdvancedDataGridSortItemRenderer;
	import mx.core.UIComponent;
	import mx.core.UITextField;

	public class BirstSortItemRenderer extends AdvancedDataGridSortItemRenderer
	{
		public function BirstSortItemRenderer()
		{
			super();
		}
		
		override protected function childrenCreated():void {
			super.childrenCreated();
			
			this.includeInLayout = false;
			this.visible = false;
			
			var sortOrderTextField:UITextField = getChildAt(0) as UITextField;
			sortOrderTextField.includeInLayout = false;
			sortOrderTextField.visible = false;
		}
		
	}
}