package com.birst.flex.adhoc
{
	import com.birst.flex.core.DraggableMenubar;
	
	import mx.effects.IEffect;

	public class ColumnSelectorMenuBar extends DraggableMenubar
	{
		private var _xml:XML;
		public var _isDefault:Boolean;
		
		public function ColumnSelectorMenuBar(name:String, identifier:String, selectorXML:XML, isDefault:Boolean = false, effects:IEffect=null)
		{
			super(name, identifier, effects);
			_xml = selectorXML;
			_isDefault = isDefault;
			
			dragSourceNVPair['SAOnDragOp'] = 'removeFromParent';
		}
		
		public function get xml ():XML{
			return _xml;
		}
		
		public function get isDefault():Boolean{
			return _isDefault;
		}
	}
}