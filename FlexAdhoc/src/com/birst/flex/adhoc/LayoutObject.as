package com.birst.flex.adhoc
{
	import com.birst.flex.adhoc.dialogs.*;
	import com.birst.flex.core.AppEvent;
	import com.birst.flex.core.Localization;
	import com.birst.flex.core.MenuItemActions;
	import com.birst.flex.core.Tracer;
	import com.birst.flex.core.Util;
	
	import flash.display.CapsStyle;
	import flash.display.JointStyle;
	import flash.display.LineScaleMode;
	import flash.events.ContextMenuEvent;
	import flash.events.MouseEvent;
	import flash.ui.ContextMenu;
	import flash.ui.ContextMenuItem;
	
	import mx.collections.ArrayCollection;
	import mx.containers.Canvas;
	import mx.controls.Label;
	import mx.core.Application;
	import mx.core.UIComponent;
	import mx.resources.ResourceManager;
	import mx.utils.IXMLNotifiable;
	import mx.utils.XMLNotifier;
	
	public class LayoutObject implements IXMLNotifiable
	{
		public static const UNUSED_WIDTH: int = 100;
		public static const UNUSED_HEIGHT: int = 13;
		
//		private var band: int;
		private var bandName:String;
		private var x: int;
		private var y: int;
		// used for drag and drop
		private var offsetX: int;
		private var offsetY: int;
		private var s: Canvas;
		private var l: Layout;
		private var labelObject: Label;
		private var id: String;
		protected var itemContextMenu: ContextMenu;
		protected var reportIndex:String;
		private var fontFamily:String;
		private var fontstyle:String;
		private var fontSize:int;
		protected var type:String;
		protected var selectionType:String;
		protected var isHidden:Boolean = false;
		private var _xml:XML;
		[ResourceBundle('adhoc')]

		public function LayoutObject(id: String, layout:XML, l:Layout)
		{
			_xml = layout;
			XMLNotifier.getInstance().watchXML(_xml, this);
			this.id = id;
			this.l = l;
			var showInReport:String = layout.showInReport[0];
			isHidden = Boolean(showInReport == "false");

			if (isHidden)
				return;
				
			var left: int = 0;
			var top: int = 0;
			var baseX:int = 0;
			if (l.Ruler)
			{
				left = Band.RULER_WIDTH;
				top = Band.RULER_HEIGHT;
			}
			this.reportIndex = layout.ReportIndex;
			s = new Canvas();
			bandName = (layout.Band[0] as XML).toString();
			var actualBand:Band = l.getBand(bandName);
			parseFont(layout.Font[0]);
			itemContextMenu = new ContextMenu();
			setupContextMenuitems();
			labelObject = new Label();
			s.addChild(labelObject);
			updateLabelText();
			
			x = int(layout.Left);
			y = int(layout.Top);
			
			s.toolTip = ResourceManager.getInstance().getString(Localization.ADHOC, 'LAYOUT_TOOLTIP', [getLabel(), bandName, x, y]);

			if (x < 0) {
				x = 0;
				layout.Left = 0;
			}
			if (y < 0) {
				y = 0;
				layout.Top = 0;
			}
			s.x = (x * Zoom) + left;
			s.y = (y * Zoom) + top + actualBand.Y;
			
			l.LayoutCanvas.addChild(s);
			
			s.addEventListener(MouseEvent.MOUSE_DOWN, startDragging);
			
			s.addEventListener(MouseEvent.CLICK, clickEventHandler);
			Application.application.addEventListener(AppEvent.EVT_UNSELECTED, unselectedHandler);
			Application.application.addEventListener(AppEvent.EVT_SELECTED, unselectedHandler);
			
			s.contextMenu = this.itemContextMenu;
		}
		
		private function updateLabelText():void {
			labelObject.htmlText = getLabel();
			if (fontstyle == "bold")
				labelObject.htmlText = "<bold>"+labelObject.htmlText+"</bold>";			
			if (fontstyle == "italic")
				labelObject.htmlText = "<italic>"+labelObject.htmlText+"</italic>";	
		}
		
		public function cleanUp():void {
			XMLNotifier.getInstance().unwatchXML(_xml, this);
		}
		
		public function getLabel():String {
			return _xml.Label;
		}
		
		public function get component(): UIComponent {
			return s;
		}
		
		public function xmlNotification(currentTarget:Object, type:String, target:Object, value:Object, detail:Object):void {
			Tracer.debug("xmlNotification on ", currentTarget);
			if (value != detail) {
				draw();
			}
		}
		
		private function get Zoom(): Number {
			if (this.bandName == "" || this.bandName == ColumnArea.NAME) {
				return l.ScaleFactor;
			}
			return l.Zoom;
		}
		private function parseFont(font:String) : void {
			var items:Array = font.split('-');
			if (items.length > 0) {
				this.fontFamily = items[0];
				if (items.length > 1) {
					this.fontstyle = items[1];
					if (items.length > 2) {
						this.fontSize = int(items[2]);
					}
				}
			}
		}
		
		public function get BandName():String {
			return this.bandName;
		}
		
		private function setNameOfBand(name:String):void {
			if (this.bandName != name) {
				// convert size
				this.bandName = name;
				if (this.bandName.indexOf("GroupHeader") >= 0)
					this.bandName = "GroupHeader";
				else if (this.bandName.indexOf("GroupFooter") >= 0)
					this.bandName = "GroupFooter";
				this.s.invalidateDisplayList();
				draw();
			}
		}
		
		public function get IsHidden():Boolean {
			return this.isHidden;
		}
		//No-op, to be overriden by sub classes who needs it
		protected function clickEventHandler(event:MouseEvent):void{}
		
		protected function unselectedHandler(event:AppEvent):void {
			if (event.dispatcher == this.reportIndex && this.entityXML) {
				this.s.invalidateDisplayList();
				draw();
			}
		}
		protected function getLayoutType():String {
			return AppEvent.EVT_COLUMN_CHOSEN;
		}
		
		public function get entityXML(): XML {
			return _xml;
//			return AdHocConfig.instance().getColumnXML(this.reportIndex)[0];
		}
		
		protected function setupContextMenuitems():void {
			itemContextMenu.hideBuiltInItems();
			itemContextMenu.builtInItems.print = false;
			
			var delItem:ContextMenuItem = new ContextMenuItem(ResourceManager.getInstance().getString("adhoc","APP_remove"));
			itemContextMenu.customItems.push(delItem);
			delItem.addEventListener(ContextMenuEvent.MENU_ITEM_SELECT, deleteItem);
			
			var sizItem: ContextMenuItem = new ContextMenuItem(ResourceManager.getInstance().getString("adhoc","APP_sizeandposition"));
			itemContextMenu.customItems.push(sizItem);
			sizItem.addEventListener(ContextMenuEvent.MENU_ITEM_SELECT, sizeItem);
			
			var bordItem: ContextMenuItem = new ContextMenuItem(ResourceManager.getInstance().getString("adhoc","APP_border"));
			itemContextMenu.customItems.push(bordItem);
			bordItem.addEventListener(ContextMenuEvent.MENU_ITEM_SELECT, borderItem);
		}
		
		protected function deleteItem(event:ContextMenuEvent):void {
			AdHocConfig.instance().removeColumnLabel(this.ID.toString());
		}
		
		private function sizeItem(event:ContextMenuEvent):void {
			invokeMenuAction("sizePosition", MultiSelectSizeAndPositionDialog);
//			menuItem["localizedLabelId"] = "optionsMenu_SP";
		}
		
		protected function invokeMenuAction(id:String, klass:Class):void {
			Util.setObjectSelected(this.entityXML.ReportIndex, getLayoutType(), false);
			var menuItem:Object = new Object();
			menuItem["id"] = id;
			menuItem["klass"] =  klass;
			MenuItemActions.invokeAction(menuItem);
		}
		private function borderItem(event:ContextMenuEvent):void {
			invokeMenuAction("borders", MultiSelectBorderPropertiesDialog);
		}
		
		public function get ID() : int
		{
			return this.entityXML.ReportIndex;
		}
		
		public function get Type() : String
		{
			return this.type;
		}
		
		public function get Selected(): Boolean
		{
			var selectedIds:ArrayCollection = Util.getSelectedLayoutIds();
			return selectedIds.contains(this.reportIndex);
		}
		
		public function get BaseX() : int
		{
			return x;
		}
		public function get BaseY() : int
		{
			return y;
		}
		public function set BaseX(x:int) : void
		{
			this.x = x;
		}
		public function set BaseY(y:int) : void
		{
			this.y = y;
		}
		
		public function get Height() : int
		{
			if (this.bandName == "" || this.bandName == ColumnArea.NAME) {
				return UNUSED_HEIGHT;
			}
			return this.entityXML.Height;
		}
		public function get Width() : int
		{
			if (this.bandName == "" || this.bandName == ColumnArea.NAME) {
				return UNUSED_WIDTH;
			}
			return this.entityXML.Width;
		}

		public function updateLayout(band:Canvas) : void {
			// l.layoutCanvas is the parent of this layout object and the band object
			draw();
		}
		
		public function draw(): void
		{
			if (this.isHidden)
				return;
				
			var xOrigin:uint = 0;
			var yOrigin:uint = 0;
			var xml:XML = this.entityXML;
			for (var i:int = 0; i < this.l.Bands.length; i++) {
				var oBandName:String = this.bandName;
				if (oBandName.indexOf("GroupHeader") == 0 || oBandName.indexOf("GroupFooter") == 0) {
					oBandName = oBandName + xml.GroupNumber
				}
				
				if ((l.Bands[i] as Band).Name == oBandName) {
					xOrigin = (l.Bands[i] as Band).X;
					yOrigin = (l.Bands[i] as Band).Y;
					break;
				}
			}
			var x:uint = uint(xml.Left);
			var y:uint = uint(xml.Top);
			
			this.s.x = xOrigin + (x * Zoom);
			this.s.y = yOrigin + (y * Zoom);
			
			adjustBandSnap();
			
			var c : uint = 0x000000;
			if (l.overlaps(this)) {
				c = 0xFF0000;
			}
			
			this.s.width = Width * Zoom;
			this.s.height = Height * Zoom;
			
			s.graphics.clear();
			var bkColor:uint = uint("0x" + xml.BackgroundColor);
			s.graphics.beginFill(bkColor);
			if (Selected)
				s.graphics.lineStyle(2, c, 1, false, LineScaleMode.NORMAL, CapsStyle.ROUND, JointStyle.ROUND, 3);
			else
				s.graphics.lineStyle(1, c, 1, false, LineScaleMode.NORMAL, CapsStyle.ROUND, JointStyle.ROUND, 3);
				
			drawElement(Width*Zoom, Height*Zoom);
			s.graphics.endFill();
		}
		
		private function drawElement(width: int, height: int): void
		{
			var zoom:Number = Zoom;
			var xml:XML = this.entityXML;
			s.graphics.drawRect(0, 0, width, height);
			labelObject.maxHeight = height;
			labelObject.maxWidth = width;
			labelObject.truncateToFit = true;
			// Waste a little less space
			var bkColor:uint = uint("0x" + xml.BackgroundColor[0].toString());
			var fgColor:uint = uint("0x" + xml.ForegroundColor[0].toString());
			labelObject.setStyle("paddingTop",-2);
			labelObject.setStyle("color", fgColor);
			labelObject.setStyle("backgroundColor", bkColor);
			labelObject.setStyle("fontFamily", fontFamily);
			labelObject.setStyle("fontSize", int(fontSize*zoom));
			updateLabelText();
		}
		
		public function startDragging(event:MouseEvent):void
		{
			AdHocConfig.instance().saveCurrentConfig();
			
			Tracer.debug('startDragging at y: ' + event.stageY + ' x: ' + event.stageX, this);
			
			Util.setObjectSelected(this.entityXML.ReportIndex, getLayoutType(), event.ctrlKey);
			
			l.startedDraggingItem(event);

		}
		
		public function startedDragging(stageX:Number, stageY:Number):void {
			offsetY = stageY - s.y;
			offsetX = stageX - s.x;
			draw();
		}

		public function stopDragging(event:MouseEvent):void
		{
			// don't process changes right now.
			XMLNotifier.getInstance().unwatchXML(_xml, this);
			
			var left: int = 0;
			var top: int = 0;
			var baseX:int = 0;
			if (l.Ruler)
			{
				left = Band.RULER_WIDTH;
				top = Band.RULER_HEIGHT;
			}

			var i: int;
			var newband: String = this.bandName;
			
			// use layout zoom, as we might be dragging from column area
			// need to calculate based on layout zoom value.
			var zoom: Number = l.Zoom;
			var b:Band = null;
			
			for (i = 0; i < this.l.Bands.length; i++)
			{
				b = l.Bands.getItemAt(i) as Band;
				// use zoom which is the real zoom value, not the non-zoom value used in column area
				if (s.y + event.localY >= b.Y + top && s.y + event.localY < b.Y + b.height*(i == 0 ? 1 : zoom) + top)
				{
					newband = b.Name;
					break;	
				}
				if (i == l.Bands.length - 1) {
					// i don't know how we got here
					// move to column area
					b = l.Bands.getItemAt(0) as Band;
					newband = "";
				}
			}
			
			var xml:XML = this.entityXML;
			if (uint(xml.Height) > b.height || uint(xml.Width) > b.width) {
				Util.localeAlertError('LY_failLayoutEl', null, null, [xml.Height.toString(), xml.Width.toString(), b.height, b.width]);
				var actualBand:Band = l.getBand(xml.Band);
				x = uint(xml.Left);
				y = uint(xml.Top);
				s.x = (x * Zoom) + (l.Ruler ? Band.RULER_WIDTH : 0);
				s.y = (y * Zoom) + (l.Ruler ? Band.RULER_HEIGHT : 0) + actualBand.Y;
				if (s.y < 0) {
					s.y = 0;
				}
				if (s.x < 0) {
					s.x = 0;
				}
				if (xml.Band == "") {
					s.width = LayoutObject.UNUSED_WIDTH;
					s.height = LayoutObject.UNUSED_HEIGHT;
				}
				l.LayoutCanvas.validateNow();
				XMLNotifier.getInstance().watchXML(_xml, this);
				return;
			}
			if (newband.indexOf("GroupHeader") == 0) {
				xml.Band = "GroupHeader";
				xml.GroupNumber = uint(newband.substring(("GroupHeader" as String).length));
			}
			else if (newband.indexOf("GroupFooter") == 0) {
				xml.Band = "GroupFooter";
				xml.GroupNumber = uint(newband.substring(("GroupFooter" as String).length));
			}
			else {
				xml.Band = newband;
			}

			if (s.y + xml.Height*zoom > b.Y + b.height*zoom + top)
			{
				s.y = b.Y + b.height*zoom + top - xml.Height*zoom;
			}
			else if (s.y < b.Y + top) 
			{
				s.y = b.Y + top;
			}
			if (s.x <  left)
			{
				s.x = left;
				baseX = 0;
			} else if (s.x + xml.Width*zoom > b.width*zoom + left)
			{
				baseX = b.width - xml.Width;
				s.x = baseX*zoom + left;
			}
			
			var newleft:int = Math.round((s.x-left)/zoom);
			var newtop:int = Math.round((s.y - b.Y-top)/zoom);  
			if (l.Snap > 0) {
				newleft = (newleft + l.Snap - 1) / l.Snap;
				newtop = (newtop + l.Snap - 1) / l.Snap;
				newleft = newleft * l.Snap;
				newtop = newtop * l.Snap;
				s.x = newleft * zoom + left;
				s.y = b.Y + (newtop * zoom) + top;
			}
			if (xml.Left != newleft) {
				xml.Left = newleft;
				l.Xml.AutomaticLayout = false;
			} 
			if (xml.Top != newtop) {
				xml.Top = newtop;
				l.Xml.AutomaticLayout = false;
			} 
			if (newband != this.bandName) {
				l.Xml.AutomaticLayout = false; 
			}
			setNameOfBand(newband);
			adjustBandSnap();
			XMLNotifier.getInstance().watchXML(_xml, this);
		}

		public function drag(event:MouseEvent):void
		{
			s.x = event.stageX - offsetX;
			s.y = event.stageY - offsetY;
			
			var left: int = 0;
			var top: int = 0;
			if (l.Ruler)
			{
				left = Band.RULER_WIDTH;
				top = Band.RULER_HEIGHT;
			}
			
			var i: int;

			// use layout zoom, as we might be dragging from column area
			// need to calculate based on layout zoom value.
			var zoom: Number = l.Zoom;
			var b:Band = null;
			
			for (i = 0; i < this.l.Bands.length; i++)
			{
				b = l.Bands.getItemAt(i) as Band;
				// use zoom which is the real zoom value, not the non-zoom value used in column area
				if (s.y + event.localY >= b.Y + top && s.y + event.localY < b.Y + b.height*(i == 0 ? 1 : zoom) + top)
				{
					break;	
				}
				if (i == l.Bands.length - 1) {
					// i don't know how we got here
					// move to column area
					b = l.Bands.getItemAt(0) as Band;
				}
			}
			
			adjustBandSnap(b);
		}
		
		private function adjustBandSnap(bn:Band = null):void{
			var left: int = 0;
			var top: int = 0;
			if (l.Ruler)
			{
				left = Band.RULER_WIDTH;
				top = Band.RULER_HEIGHT;
			}

			var snapval: int = l.Snap*Zoom;
			if (snapval > 0) {
				var b:Band = (bn == null) ? l.getBand(this.bandName) : bn;
				var newleft:int = Math.round((s.x-left)/Zoom);
				var newtop:int = Math.round((s.y - b.Y-top)/Zoom);  
				newleft = (newleft + l.Snap - 1) / l.Snap;
				newtop = (newtop + l.Snap - 1) / l.Snap;
				newleft = newleft * l.Snap;
				newtop = newtop * l.Snap;
				s.x = newleft * Zoom + left;
				s.y = b.Y + (newtop * Zoom) + top;
			}
		}
				
	}
}