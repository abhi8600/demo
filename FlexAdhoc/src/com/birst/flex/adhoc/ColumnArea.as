package com.birst.flex.adhoc
{
	import flash.display.CapsStyle;
	import flash.display.JointStyle;
	import flash.display.LineScaleMode;
	import mx.resources.ResourceManager;
	public class ColumnArea extends Band
	{
		[ResourceBundle('adhoc')]
		public  static var NAME:String = null;
		private var BACKGROUND:uint = 0x666666;
		private var BORDER:uint = 0x000000;
		private var curX: int = 0;
		private var curY: int = 0;
		private var Height:int;

		public function ColumnArea(l: Layout, height: int)
		{
			NAME = ResourceManager.getInstance().getString("adhoc","CP_columnArea");
			super(NAME, 'ColumnArea', l, height);
			this.Height = height; 
		}

		public override function draw(y: int):int
		{
			s.graphics.clear();
			s.y = y;
			s.graphics.beginFill(BACKGROUND);
			s.graphics.lineStyle(1, BORDER, 1, false, LineScaleMode.NORMAL, CapsStyle.ROUND, JointStyle.ROUND, 3);
			s.graphics.drawRect(0, 0, width*l.Zoom, height*l.ScaleFactor);
			s.graphics.endFill();
			return height*l.ScaleFactor;
		}
		
		override public function get height(): int
		{
			return Height;
		}
		override public function set height(height:int):void {
			
		}
		public function layoutObjects(): void
		{
			NAME = ResourceManager.getInstance().getString("adhoc","CP_columnArea");
			var curX: int = 0;
			var curY: int = 4;
			for each(var lo: LayoutObject in this.l.Objects)
			{
				if (lo.IsHidden) {
					continue;
				}
					
				if (lo.BandName == ColumnArea.NAME)
				{
					lo.BaseX = curX;
					lo.BaseY = curY;
					curX += LayoutObject.UNUSED_WIDTH*l.ScaleFactor + 4;
					if (curX > l.LayoutCanvas.width)
					{
						curX = 0;
						curY += LayoutObject.UNUSED_HEIGHT * l.ScaleFactor;
					}
				}
			}
		}
		
	}
}