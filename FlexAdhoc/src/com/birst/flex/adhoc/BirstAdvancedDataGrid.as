package com.birst.flex.adhoc
{
	import com.birst.flex.dashboards.views.DashletContentBase;
	
	import mx.collections.ArrayCollection;
	import mx.controls.AdvancedDataGrid;
	import mx.controls.advancedDataGridClasses.AdvancedDataGridColumn;
	import mx.controls.advancedDataGridClasses.SortInfo;
	import mx.utils.StringUtil;

	public class BirstAdvancedDataGrid extends AdvancedDataGrid
	{
		public function BirstAdvancedDataGrid()
		{
			super();
			sortExpertMode = true;
		}
		
		override public function getFieldSortInfo(column:AdvancedDataGridColumn):SortInfo {
			var ret:SortInfo = new SortInfo();
           	var sortedColumns:Array = null;
			var sortedOrderArray:ArrayCollection = AdHocConfig.instance().getSortedOrderArray();
			if (parent && parent.parent && parent.parent is DashletContentBase) {
				var dcb:DashletContentBase = parent.parent as DashletContentBase;
				sortedColumns = dcb.getSortColumns();
			}
			else {
				var config:AdHocConfig = AdHocConfig.instance();
    	       	sortedColumns = config.getSortedColumns();
   			}	
			for (var i:int = 0; i < sortedColumns.length; i++) {
				var col:Object = sortedColumns[i];
				if (col.id == column.dataField) {
					var order:String = col.order == null ? "Ascending" : col.order.value;
					order = StringUtil.trim(order);
					if (order != "Ascending"){
						ret.descending = true;
					}
				}
			}
			return ret;
		}
	}
}