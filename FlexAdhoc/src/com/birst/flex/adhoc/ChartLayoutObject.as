package com.birst.flex.adhoc
{
	import com.birst.flex.adhoc.dialogs.AdhocChartPropertiesDialogBox;
	import com.birst.flex.core.AppEvent;
	import com.birst.flex.core.AppEventManager;
	
	import flash.events.ContextMenuEvent;
	import flash.events.MouseEvent;
	import flash.ui.ContextMenuItem;
	import mx.resources.ResourceManager;
	[ResourceBundle('adhoc')]

	public class ChartLayoutObject extends LayoutObject
	{
		public function ChartLayoutObject(id:String, layout:XML, l:Layout)
		{
			this.type = "Chart";
			this.selectionType = AppEvent.EVT_CHART_CHOSEN;
			super(id, layout, l);
		}
		
		override public function getLabel():String {
			return ResourceManager.getInstance().getString("adhoc","APP_charts") + entityXML.Charts.Chart.ChartType[0];
		}
		override protected function getLayoutType():String {
			return AppEvent.EVT_CHART_CHOSEN;
		}
		
		override protected function clickEventHandler(event:MouseEvent):void{
			AppEventManager.fire(new AppEvent(AppEvent.EVT_CHART_CHOSEN, 'ChartLayoutObject', 'clickEventHandler', this.ID, event.ctrlKey));
		} 
		
		override protected function setupContextMenuitems():void {
			super.setupContextMenuitems();
			var subreportPropertiesItem:ContextMenuItem = new ContextMenuItem( ResourceManager.getInstance().getString("adhoc","optionsMenu_CH"));
			itemContextMenu.customItems.push(subreportPropertiesItem);
			subreportPropertiesItem.addEventListener(ContextMenuEvent.MENU_ITEM_SELECT, subReportProperties);
		}
		
		//Called by the right click "Remove"
		override protected function deleteItem(event:ContextMenuEvent):void {
			AdHocConfig.instance().removeChart(this.ID.toString());
		}
		
		private function subReportProperties(event:ContextMenuEvent):void {
			invokeMenuAction("chartProperties", AdhocChartPropertiesDialogBox);
		}
	}
}