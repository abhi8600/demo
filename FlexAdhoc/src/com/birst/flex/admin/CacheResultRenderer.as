package com.birst.flex.admin
{
	import mx.controls.TextArea;
	public class CacheResultRenderer
	{
		private var parent:TextArea;		
		
		public function CacheResultRenderer(_parent:TextArea)
		{
			this.parent = _parent;			
			parent.text = null;
			parent.editable = false;							
		}

		public function renderCacheQueryResult(cacheLogList:XMLList):void
		{		
			var log:String = 'Operation Output' + '\n';	
        	var size:uint = cacheLogList.length();
        	for(var i:int=0; i<size ; i++){        		   		
        		log = log + cacheLogList[i].toString() + '\n';        		        		
        	}   
        	parent.text = log;	
		}		
		
		public function renderRemoteCacheQueryResult(serverNodeList:XMLList):void
		{
			var log:String = 'Operation Output from Remote Servers' + '\n';
			var size:uint = serverNodeList.length();
			
			for(var i:int=0; i<size ; i++){
				log = log + '*************************************' + '\n';
				log = log + 'Remote Server '  + i + '\n';				
				var name:String = serverNodeList[i].name[0].toString() + '\n';
				log = log + 'server url : ' + name + '\n';				
				var serverNode:XML = serverNodeList[i];
				// Try to get CacheLogOutput first
				log = log + getRemoteServerLogOutput(serverNode);
				// Try to get CacheLogError second
				log = log + getRemoteServerLogError(serverNode);
				log = log + '*************************************' + '\n';				
			}
			parent.text = log;	
		}
		
		private function getRemoteServerLogOutput(serverNode:XML):String
		{
			var log:String = '';
			var serverCacheLogList:XMLList = serverNode.CacheLogOutput;
			var serverLogSize:uint = serverCacheLogList.length();
			for(var i:int=0; i<serverLogSize ; i++){
				log = log + serverCacheLogList[i].toString() + '\n';
			}
			return log;
		}
		
		private function getRemoteServerLogError(serverNode:XML):String
		{
			var log:String = '';
			var serverCacheErrorList:XMLList = serverNode.CacheLogError;
			var serverLogErrorSize:uint = serverCacheErrorList.length();
			for(var i:int=0; i<serverLogErrorSize ; i++){
				log = log + serverCacheErrorList[i].toString() + '\n';
			}
			return log; 
		}
	}
}