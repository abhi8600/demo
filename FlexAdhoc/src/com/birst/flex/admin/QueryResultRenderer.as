package com.birst.flex.admin
{
	import mx.collections.XMLListCollection;
	import mx.containers.Canvas;
	import mx.controls.DataGrid;
	import mx.controls.TextArea;
	import mx.controls.dataGridClasses.DataGridColumn;
	
	public class QueryResultRenderer
	{
		private var parent:Canvas;
		private var dataGrid:DataGrid;
		private var resultArea:TextArea;
		private var rowCatalog:XMLListCollection;
		
		public function QueryResultRenderer(_parent:Canvas)
		{
			this.parent = _parent;
			dataGrid = new DataGrid();
			resultArea = new TextArea();
			resultArea.editable = false;			
		}

		// render the results from Queries --> run query operation
        public function renderRunQueryResult(rowsXml:XML):void {
        	parent.removeAllChildren();
        	var labelXml:XML = rowsXml.labels[0];        	 
        	var numLabels:uint = labelXml.children().length();    	
        	if(numLabels > 0){
				buildDataGrid(numLabels,rowsXml);
        	}           	
        }
        
        private function buildDataGrid(num:uint, rowsXml:XML):void{        	
        	var rowCatalog:XMLList = rowsXml.row;
        	var labelCatalog:XML = rowsXml.labels[0];
        	var xmlColl:XMLListCollection = new XMLListCollection(rowCatalog);
        	dataGrid.dataProvider = xmlColl;  
        	dataGrid.percentWidth=100;
        	dataGrid.percentHeight=100;
        	var cols:Array = dataGrid.columns;
        	for(var i:int=0; i<num ; i++){
        		var dgc:DataGridColumn = new DataGridColumn();
        		var labelTag:String = "label" + i;
        		var columnTag:String = "column" + i; 
        		dgc.headerText = labelCatalog.child(labelTag).toString();        		
        		dgc.dataField =  columnTag;      	
        		cols.push(dgc);
        	}
        	                   
           	dataGrid.columns = cols;   	
        	parent.addChild(dataGrid);
        }
        
        // render the results for Queries --> navigate query, generate query
        public function renderResult(resultXml:XML):void {
        	var resultString:String = resultXml.toString();    	
        	if(resultString != null){
				buildResultTextArea(resultString);
        	}  
        }       
       
        private function buildResultTextArea(resultString:String):void {
        	parent.removeAllChildren();
        	resultArea.percentHeight = 100;
        	resultArea.percentWidth = 100;
        	resultArea.text = resultString; 
        	parent.addChild(resultArea);
        }
 	}
}