package com.birst.flex.validator
{
	import mx.validators.StringValidator;

	public class MyStringValidator extends StringValidator
	{
		private var _customizedRequiredFieldError:String;
		public function MyStringValidator()
		{
			super();
		}
		
		override public function get requiredFieldError():String
		{
			return _customizedRequiredFieldError;
		}
		
		override public function set requiredFieldError(value:String):void
		{
			_customizedRequiredFieldError = value;
		}
	}
}