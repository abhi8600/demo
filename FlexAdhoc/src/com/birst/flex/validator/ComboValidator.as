package com.birst.flex.validator
{
	import mx.validators.ValidationResult;
	import mx.validators.Validator;
	
	public class ComboValidator extends Validator
	{
		public var error:String;
		public var prompt:String;
		
		public function ComboValidator()
		{
			super();
		}
		
		override protected function doValidation(value:Object):Array{
			var results:Array= [];
			if (value as String == prompt || value == null){
				var res:ValidationResult = new ValidationResult(true, "","", error);
				results.push(res);
			}
			return results;
		}
		
	}
}