package com.birst.flex.validator
{
	import mx.validators.Validator;
	import mx.validators.ValidationResult;
	public class ResetValidator extends Validator
	{
		
		private var results:Array;
		
		public function ResetValidator()
		{
			super();			
		}

	// Define the doValidation() method.
        override protected function doValidation(value:Object):Array {
        
            // Clear results Array.
            results = [];            
            return results;
        }
	}
}