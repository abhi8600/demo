package com.birst.flex.drillmaps
{
	import flash.events.Event;

	public class DrillMapGroupEvent extends Event
	{
		public static const DRILL_MAP_GROUP:String = "DrillMapGroup";
		
		public var eventParam:Object;
		
		public function DrillMapGroupEvent(type:String, eventParam:Object= null, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			this.eventParam = eventParam;
			super(type, bubbles, cancelable);
		}
		
		override public function clone():Event
		{
			return new DrillMapGroupEvent(type, eventParam);	
		}
	}
}