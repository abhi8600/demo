package tests
{
	import com.birst.flex.core.LocalizedFormItem;
	import com.birst.flex.core.Localization;
	
	import flexunit.framework.TestCase;
	
	public class LocalizedFormItemTest extends TestCase
	{
		public function LocalizedFormItemTest()
		{
		}

		public function testVirgin():void{
			var formItem:LocalizedFormItem = new LocalizedFormItem();
			assertNotNull(formItem);
			//assertEquals(Localization.ADHOC, formItem.localizedBundle);
			assertTrue(formItem.hasOwnProperty("localizedLabelId"));
			assertTrue(formItem.hasOwnProperty("localizedTooltipId"));
			assertNull(formItem.localizedLabelId);
			assertNull(formItem.localizedTooltipId);
		}
		
		public function testLocalizedLabel():void{
			var formItem:LocalizedFormItem = new LocalizedFormItem();
			assertNotNull(formItem);
			assertEquals( "", formItem.label);
			formItem.localizedLabelId = "optionsMenu_BR";
			assertEquals("Borders", formItem.label);
		}
		
		public function testLocalizedTooltip():void{		
			var formItem:LocalizedFormItem = new LocalizedFormItem();
			assertNotNull(formItem);
			assertEquals(null, formItem.toolTip);
			formItem.localizedTooltipId = "RP_ttFirstColor";
			assertEquals("Color for uneven rows", formItem.toolTip);
		}
	}
}