package tests
{
	import com.birst.flex.core.Localization;
	
	import flexunit.framework.TestCase;
	import mx.resources.IResourceManager;
	import mx.resources.ResourceManager;
		
	public class LocalizationTest extends TestCase
	{
	
		public function LocalizationTest() 
		{
		}
		
		public function testInitial():void{
			assertEquals("adhoc", Localization.ADHOC);	
		}
		
		public function testLocalizeLabelDefaultEnglish():void{
			var obj:Object = { localizedLabelId: "optionsMenu_BR" };
			assertTrue(Localization.localizeLabel(obj));
			assertTrue(obj.hasOwnProperty("label"));
			assertEquals("Borders", obj.label);
		}
		
		public function testLocalizeLabelSpanish():void{
			var resourceManager:IResourceManager = ResourceManager.getInstance();
			
			resourceManager.localeChain = ['es_ES'];
			var obj:Object = { localizedLabelId: "optionsMenu_BR" };
			assertTrue(Localization.localizeLabel(obj));
			assertTrue(obj.hasOwnProperty("label"));
			assertEquals("Fronteras", obj.label);
			
			resourceManager.localeChain = ['en_US'];
		}

	}
}