package tests
{
	import com.birst.flex.adhoc.AdHocConfig;
	import flexunit.framework.TestCase;
	
	public class AdHocConfigTest extends TestCase
	{
		public function AdHocConfigTest(methodName:String=null)
		{
			super(methodName);
		}
		
		override public function setUp():void{
			AdHocConfig.instance().clearEmptyConfig();
		}
		
		//Test AdHocConfig without any modifications at start
		public function testInitialState():void{
			var config:XML = AdHocConfig.instance().getConfig();
			assertNotNull(config);
			assertEquals("com.successmetricsinc.adhoc.AdhocReport", config.name());
			assertEquals(config.children().length(), 0);
			
			var entity:XMLList = AdHocConfig.instance().getRootEntity();
			assertNotNull(entity);
			assertEquals("Entities", entity.name());
			
			var filters:XMLList = AdHocConfig.instance().getRootFilters();
			assertNotNull(filters);
			assertEquals("Filters", filters.name());
			
			var chartsLabels:Array = AdHocConfig.instance().getChartsConfig();
			assertNotNull(chartsLabels);
			assertEquals(0, chartsLabels.length);
			
			var reportProps:Array = AdHocConfig.instance().getReportPropertiesConfig();
			assertNotNull(reportProps);
			assertEquals(0, reportProps.length);
			
			var columnFormat:XMLList = AdHocConfig.instance().getColumnConditionalFormats("fakeColumnId", "fakeConditionalFormatId");
			assertNotNull(columnFormat);
			assertEquals(0, columnFormat.length());
			
			var column:XMLList = AdHocConfig.instance().getColumnXML("non-existent");
			assertNotNull(column);
			assertEquals(0, column.length());
			
			/*
			var filter:XMLList = AdHocConfig.getFilterXML("non-existent");
			assertNotNull(column);
			assertEquals(0, filter.length());
			*/
			
			var pageInfo:XMLList = AdHocConfig.instance().getPageInfoXML();
			assertNotNull(pageInfo);
			assertEquals(0, pageInfo.length());
		}
		
		//Test report properties
		public function testReportProperties():void{
			var testInput:Array = new Array();
			testInput["Title"] = "Report Properties";
			testInput["HideTitle"] = false;
			testInput["IncludeTable"] = false; //Show Table
			testInput["TableBorder"] = true;  //Table Border
			testInput["AlternateRowColors"] = true; //Alternate Table Row Colors
			testInput["FirstAlternateRGB"] = "FFFFFF";  //First Color
			testInput["SecondAlternateRGB"] = "00FF00"; //Second Color
			testInput["CSVSeparator"] = "#"; //Separator (\t for Tab)
			/* to be tested
				"Filler_CSV_Format" =  //Use Formatting
				"CSV_Filename", //File name
				"CSV_Mimetype"  //MIME Type
			*/
			
			AdHocConfig.instance().setReportPropertiesConfig(testInput);
			var config:XML = AdHocConfig.instance().getConfig();
			
			assertNotNull(config);
			assertEquals(config.Title, testInput["Title"]);
			assertEquals(config.HideTitle, testInput["HideTitle"]);
			assertEquals(config.IncludeTable, testInput["IncludeTable"]);
			assertEquals(config.TableBorder, testInput["TableBorder"]);
			assertEquals(config.AlternateRowColors, testInput["AlternateRowColors"]);
			assertEquals(config.FirstAlternateRGB, testInput["FirstAlternateRGB"]);
			assertEquals(config.SecondAlternateRGB, testInput["SecondAlternateRGB"]);
			assertEquals(config.CSVSeparator, testInput["CSVSeparator"]);
		}
	}
}