<?xml version="1.0" encoding="UTF-8"?><wsdl:definitions xmlns:wsdl="http://schemas.xmlsoap.org/wsdl/" xmlns:http="http://schemas.xmlsoap.org/wsdl/http/" xmlns:mime="http://schemas.xmlsoap.org/wsdl/mime/" xmlns:s="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/wsdl/soap/" xmlns:soap12="http://schemas.xmlsoap.org/wsdl/soap12/" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/" xmlns:tm="http://microsoft.com/wsdl/mime/textMatching/" xmlns:tns="http://www.birst.com/" targetNamespace="http://www.birst.com/">
  <wsdl:types>
    <s:schema elementFormDefault="qualified" targetNamespace="http://www.birst.com/">
      <s:element name="updateUserPreferences">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="userPreferences" type="tns:ArrayOfUserPreference"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:complexType name="ArrayOfUserPreference">
        <s:sequence>
          <s:element maxOccurs="unbounded" minOccurs="0" name="UserPreference" nillable="true" type="tns:UserPreference"/>
        </s:sequence>
      </s:complexType>
      <s:complexType name="UserPreference">
        <s:sequence>
          <s:element maxOccurs="1" minOccurs="1" name="PreferenceType" type="tns:Type"/>
          <s:element maxOccurs="1" minOccurs="0" name="Key" type="s:string"/>
          <s:element maxOccurs="1" minOccurs="0" name="Value" type="s:string"/>
        </s:sequence>
      </s:complexType>
      <s:simpleType name="Type">
        <s:restriction base="s:string">
          <s:enumeration value="Global"/>
          <s:enumeration value="User"/>
          <s:enumeration value="UserSpace"/>
        </s:restriction>
      </s:simpleType>
      <s:element name="updateUserPreferencesResponse">
        <s:complexType/>
      </s:element>
      <s:element name="getUserPreferences">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="preferenceKeys" type="tns:ArrayOfString"/>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:complexType name="ArrayOfString">
        <s:sequence>
          <s:element maxOccurs="unbounded" minOccurs="0" name="string" nillable="true" type="s:string"/>
        </s:sequence>
      </s:complexType>
      <s:element name="getUserPreferencesResponse">
        <s:complexType>
          <s:sequence>
            <s:element maxOccurs="1" minOccurs="0" name="getUserPreferencesResult" type="tns:ArrayOfUserPreference"/>
          </s:sequence>
        </s:complexType>
      </s:element>
    </s:schema>
  </wsdl:types>
  <wsdl:message name="updateUserPreferencesSoapIn">
    <wsdl:part element="tns:updateUserPreferences" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="getUserPreferencesSoapOut">
    <wsdl:part element="tns:getUserPreferencesResponse" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="getUserPreferencesSoapIn">
    <wsdl:part element="tns:getUserPreferences" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:message name="updateUserPreferencesSoapOut">
    <wsdl:part element="tns:updateUserPreferencesResponse" name="parameters">
    </wsdl:part>
  </wsdl:message>
  <wsdl:portType name="PreferencesServiceSoap">
    <wsdl:operation name="updateUserPreferences">
      <wsdl:input message="tns:updateUserPreferencesSoapIn">
    </wsdl:input>
      <wsdl:output message="tns:updateUserPreferencesSoapOut">
    </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getUserPreferences">
      <wsdl:input message="tns:getUserPreferencesSoapIn">
    </wsdl:input>
      <wsdl:output message="tns:getUserPreferencesSoapOut">
    </wsdl:output>
    </wsdl:operation>
  </wsdl:portType>
  <wsdl:binding name="PreferencesServiceSoap" type="tns:PreferencesServiceSoap">
    <soap:binding transport="http://schemas.xmlsoap.org/soap/http"/>
    <wsdl:operation name="updateUserPreferences">
      <soap:operation soapAction="http://www.birst.com/updateUserPreferences" style="document"/>
      <wsdl:input>
        <soap:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getUserPreferences">
      <soap:operation soapAction="http://www.birst.com/getUserPreferences" style="document"/>
      <wsdl:input>
        <soap:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
  </wsdl:binding>
  <wsdl:binding name="PreferencesServiceSoap12" type="tns:PreferencesServiceSoap">
    <soap12:binding transport="http://schemas.xmlsoap.org/soap/http"/>
    <wsdl:operation name="updateUserPreferences">
      <soap12:operation soapAction="http://www.birst.com/updateUserPreferences" style="document"/>
      <wsdl:input>
        <soap12:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
    <wsdl:operation name="getUserPreferences">
      <soap12:operation soapAction="http://www.birst.com/getUserPreferences" style="document"/>
      <wsdl:input>
        <soap12:body use="literal"/>
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal"/>
      </wsdl:output>
    </wsdl:operation>
  </wsdl:binding>
  <wsdl:service name="PreferencesService">
    <wsdl:port binding="tns:PreferencesServiceSoap" name="PreferencesServiceSoap">
      <soap:address location="http://localhost:57747/PreferencesService.asmx"/>
    </wsdl:port>
    <wsdl:port binding="tns:PreferencesServiceSoap12" name="PreferencesServiceSoap12">
      <soap12:address location="http://localhost:57747/PreferencesService.asmx"/>
    </wsdl:port>
  </wsdl:service>
</wsdl:definitions>