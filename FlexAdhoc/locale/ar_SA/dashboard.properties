#
# Copyright (C) 2009-2012 Birst, Inc. All rights reserved.
# BIRST PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
#

csvTooltip=Export to CSV format
xlsTooltip=Export to Excel (.xls) format
pdfTooltip=Export to PDF format
scheduleTooltip=Schedule Report
manageScheduleTooltip=Manage Scheduled Reports
pptTooltip=Export to PowerPoint (.ppt) format
pivotToolTip=Show Pivot Controls
editButton=Edit properties
editReportButton=Designer
maximizeRestoreButton=Maximize / Restore
chgTabName=Change Tab Name
newPage=Add New Page
delDash=Delete dashboard
ttCreate=Create new dashboard
copyDash=Copy dashboard
viewFull=Normal view
manageSchedules=Manage Scheduled Reports
viewHeaderless=Mouse over header view
viewHeaderBorderless=No header & border view
stickiesToggle=Show or hide sticky notes
undo=Undo
option=Options

DB_dashboardName=Dashboard
DB_dashboardPageName=Page

#Tooltip for help icon
TT_help=Help

#Dashlet
DO_pivot=Pivot Control
DO_expt=Export
DO_csv=CSV
DO_pdf=PDF
DO_xcl=Excel
DO_ppt=Powerpoint
DO_schedule=Schedule Report
DO_designer=Edit Report

#Alert failure messages, Dashboards.mxml
failPerms=Cannot retrieve permissions.
failList=Unable to retrieve dashboards.
failRename=Cannot rename a dashboard while page(s) are being edited.
failDel=Unable to delete dashboard.
failNoDel=Unable to delete dashboard.
failOrder=Cannot change dashboard order when there are dashboards(s) unsaved or being edited.
failSaveOrder=Unable to save dashboard order.
failMove=Cannot move page to another dashboard when there are page(s) unsaved or being edited.
failMvConf=Cannot move page, possibly page name conflicts or permissions.
deleteDashboardConfirmation=Are you sure you want to delete the dashboard?

#DashboardTab
DT_failDelPage=Cannot delete page {0}.
DT_failSaveEdit=Save edited dashboard page failed.
DT_failRename=Cannot rename dashboard.
DT_failSaveOrder=Unable to save page order.
DT_failChgOrder=Cannot change page order when there are page(s) unsaved or being edited.
DT_refresh=Reset
DT_toggleTimer=Start/Stop Kiosk mode
DT_edit=Edit
DT_viewAsHTML=View as HTML5
DT_delete=Delete page
DT_addBmark=Add bookmark
DT_applyBmark=Manage bookmarks
DT_applyDefault=Apply default bookmark
DT_deletePageConfirmation=Are you sure you want to delete the page?
DT_ttTabOp=Right click for options
DT_print=Print page
DT_chatterPost=Post to Chatter
DT_getLink=Get Links
DT_addNote=Add sticky note
DT_failCopyDash=Fail to copy the dashboard and its pages
DT_failCopyPage=Fail to copy the page
DT_copy=Copy page
DT_viewAsHTMLAlert=Prompt values set by the user will not be passed to the HTML5 version.
DT_confirmation=Confirmation

CTR_title=Post to Chatter
CTR_link=Link to Page
CTR_pdf=PDF of Page
CTR_comment=Comment
CTR_post=Post

DBL_title=Links to the Dashboard Page
DBL_emailLink=Appropriate for Email or posting to Feeds, will redirect to the login page if not already authenticated
DBL_birstSSO=Appropriate for embedding in an HTML application (as an IFRAME), requires calling TokenGenerator.aspx to get the token
DBL_sfdcCustomWebTab=Appropriate for Custom Web Tabs in the Salesforce.com application
DBL_visualForce=Appropriate for an Apex Control within Salesforce

#PromptDisplay
PD_failSession=Unable to retrieve session variable values.
PD_failPrompts=Failed to load prompts.
PD_failRecurse=Prompts are configured incorrectly.  A prompt parent's parent eventually points back to itself recursively.
PD_failParUpdate=Cannot update parent prompt values.
PD_failPop=Cannot populate filter values.
PD_apply=Apply
PD_compact=Compact prompt {0}
PD_expand=Expand prompt {0}
PD_failMultiSelect=Please multi-select one prompt at a time.
PC_Default=Default

#TreePrompt
TP_failHierarchicalPrompts=Can not find hierarchical filter prompts.

#PromptLanding
PL_failCreate=Failed to create prompt.
PL_failExists=You are attempting to add a prompt on a column that already has a prompt defined.  \nDo you wish to add a different version on this prompt that can only be referenced in a report expression?
PL_failRetr=Unable to retrieve report prompts.
PL_instr=Drag an attribute or measure here to add a prompt.

#DashletContentBase
DC_failDrill=Unable to drill down on report.
DC_failDFilters=Cannot get filters for drill down.
DC_failRender=Render report failed.

#DashboardFileDialog
DF_title=Dashboard Location
DF_failRoot=Dashboards not be created in the root directory.  Please select a directory.
DF_name=Dashboard Name:
DF_failExistsDash=A dashboard by that name already exists. Please choose another name or use the existing dashboard.

#DashletLocationFileDialogBox
DA_title=Report Location

#DashboardLocationDialogBox
DL_title=Choose dashboard location
DL_loc=Dashboard Location

#DashletPropertiesDialogBox
DP_title=Dashlet Properties
DP_rpt=Report
DP_tt=Title
DP_pdf=Enable PDF
DP_sel=Enable View Selector
DP_csv=Enable CSV
DP_pvt=Enable Pivot Control
DP_xcl=Enable Excel
DP_ppt=Enable PowerPoint
DP_csel=Enable Column Selectors
DP_chg=Change
DP_adhoc=Enable Designer
DP_selfSchedule=Enable Self Scheduling
DP_applyAllPrompts=Apply prompts if adding the prompt filter results in a valid query
DP_ttapplyAllPrompts=Checked: apply all prompt filters that result in a valid query\nNot Checked: apply only those prompt filters that already have their columns in the query.
DP_viewMode=View Mode
DP_auto=Auto
DP_default=Default
DP_noHeader=No Headers
DP_noHeaderBorder=No Headers or Borders
DP_enablePrompts=Enable Prompts

#ImageSaveDialog
IS_failNoImg=No image selected

#PagePropertiesDialogBox
PP_title=Dashboard Page Properties
PP_w=Width
PP_h=Height
PP_name=Page Name
PP_scale=Scale dashlets
PP_clkOpen=Click to open/close prompts
PP_defaultOpen=Default prompts to open
PP_doNotDisplaySummary=Do not display the prompt summary
PP_dim=Explicit dimensions
PP_invisible=Invisible to regular users
PP_ttInvisible=Invisible to regular users with no edit privileges
PP_enableRect=Enable visual filtering

PP_error=Page names are limited to 64 characters and the following characters are not allowed [.,<,>,:,",/,\,|,?,*]
PP_pageNameRegExp=^[^\\.<>:"/\\\\|\\?\\*]{1,64}$

#PromptDialogBox
PDB_title=Prompt Properties
PDB_val=Value
PDB_list=List
PDB_query=Query
PDB_sel=Selection
PDB_multi=Multiple Selection
PDB_noSel=Add No Selection Entry
PDB_type=Type
PDB_dispName=Display Name
PDB_parName=Parameter name
PDB_colName=Column Name
PDB_fil=Filter Type
PDB_var=Use variable
PDB_tt=Tooltip
PDB_varUp=Variable to Update
PDB_qry=Query
PDB_qCol=Query Column Name
PDB_expand=Use Checkboxes/Radio Buttons
PDB_def=Default Value
PDB_pp=Parent Prompts
PDB_tree=Render as tree control
PDF_DrillPath=Derive tree from drill path
PDB_lab=Label
PDB_rem=Remove
PDB_add=Add
PDB_noSelText=No Selection Text
PDB_non=None
PDB_n=AND
PDB_or=OR
PDB_data=Data
PDB_disp=Display
PDB_opr=Operator
PDB_slider=Slider
PDB_sliderType=Slider Type
PDB_multiLine=Expand to multi-line
PDB_width=Width
PDB_labels=Labels
PDB_dispLab=All
PDB_endPoints=Endpoints
PDB_startEndPt=Start Point
PDB_endEndPt=End Point
PDB_startDef=Start Default
PDB_endDef=End Default
PDB_endLabels=Start and end only
PDB_increment=Increments
PDB_readonly=Read Only
PDB_checkbox=Checkbox
PDB_promptGroup=Prompt Group
PDB_promptList=Prompt List
PDB_pTargetDashboard=Prompt Target Dashboard
PDB_imagePath=Image Path
PDB_operationName=Operation Name
PDB_validationMsg=Validation Message
PDB_caption=Caption
PDB_invisible=Invisible
PDB_point=Point
PDB_range=Range
PDB_parameterName=Change parameter name (for report expression prompts)
PDB_all=All
#RenameDashboardDialogBox
RD_title=Rename dashboard
RD_new=New name:
RD_error=Dashboard names are limited to 64 characters and the following characters are not allowed [.,<,>,:,",/,\,|,?,*]
RD_dashboardNameRegExp=^[^\\.<>:"/\\\\|\\?\\*]{1,64}$

#NewEditPage
NE_props=Properties
NE_save=Save
NE_cancel=Cancel
NE_report=Add Reports
NE_prompts=Add Prompts
NE_createCollection=Create collection
NE_collectionName=Collection name
NE_collInstrs=Right click to add a new prompt collection. To add prompts, drag attributes and measures from the subject area into a collection.

#Page
PG_prompts=Prompts
PG_ttOpen=Click to open
PG_ttClose=Click to close
PG_failSaveState=Fail to save bookmark.
PG_failApplyState=Fail to apply bookmark.
PG_failListStates=Fail to list bookmarks.
PG_failBookmarkPrefix=The dashboard you originally bookmarked has changed. This occurred due to:\n\n
PG_failBookmarksOutdated=* One or more of the reports on this dashboard changed. Clicking "Yes" will display the reports as closely to their original form.\n
PG_failBookmarksDashPath=* The dashboard references different reports since the time this bookmark was created. Clicking "Yes" will display the new report(s).\n
PG_failBookmarksExtraPrompts=* One or more new prompts were added to the dashboard since the time this bookmark was created. Those new prompts will be loaded along with the bookmark.\n
PG_failBookmarkSuffix=\nIt is recommended to save a new version of the Bookmark and delete the previous one. Would you like to continue?
PG_failGetStickies=Fail to retrieve sticky notes.
PG_failGetPage=Fail to retrieve full page information.

#Layout movable
LM_remove=Remove
LM_vert=Split vertical
LM_horiz=Split horizontal
LM_chgVert=Change to vertically align
LM_chgHoriz=Change to horizontally align

#AdhocLayoutDialogBox
AL_close=Close

#AddBookmarkDialogBox
AB_title=Add bookmark
AB_page=Page
AB_name=Name
AB_descr=Description
AB_bmark=Bookmark

#ApplyBookmarkDialogBox
APB_title=Apply bookmark
APB_chooseBMark=Select a bookmark to apply to the dashboard page:
APB_colTitle=Name
APB_created=Created Date
APB_delete=Delete
APB_failDelete=Cannot delete bookmark
APB_noBookmarks=No bookmarks saved for this page.
APB_useDefault=Use as default
APB_failDefault=Fail to set default bookmark

FO_folder=Folder:
FO_file=Name:
FO_saveFile=Save
FO_openFile=Open
FO_saveFileAs=Save As

#StickyNotes
SN_failSave=Fail to save note.
SN_invalidPage=Invalid page.
SN_failDelete=Fail to delete note.
SN_remove=Delete this note?
SN_confirmRem=Delete
SN_share=Sharing permission
SN_private=Private
SN_public=Public

#PromptCollectionEditDialogBox
PCD_name=Name
PCD_delParent=This prompt is the parent of one or more prompts, removing it will invalidate the relationship(s).
PCD_delete=Delete
PCD_confirmDel=Are you sure that you want to delete the collection and all its prompts?
PCD_prompts=Prompts:
PCD_prompt=prompts.

#PromptCollection
PC_ttExpand=Expand prompts
PC_ttCollapse=Collapse prompts

#DashboardStyleDialogBox
DSD_title=Global Properties
DSD_dashboards=Dashboards
DSD_prompts=Prompts
DSD_dashlets=Dashlets
DSD_bgImage=Background Image
DSD_bgUrl=Background Image Url
DSD_bgColor=Background Color
DSD_tabRowColor=Tab Row Color
DSD_tabFont=Tab Font
DSD_tabFontClrs=Tab Font Colors
DSD_tabClrs=Tab Colors
DSD_promptFont=Summary Font
DSD_dashFont=Header Font
DSD_dashClr=Header Color
DSD_reset=Reset to defaults
DSD_browse=Browse...
DSD_inactDash=Inactive dashboard
DSD_inactPage=Inactive page
DSD_sel=Selected
DSD_bg=Background
DSD_promptsSingleRow=Display collections on separate rows
DSD_dispCollFrame=Display collection frame
DSD_enableStickies=Enable sticky notes
DSD_enableBookmarks=Enable bookmarks
DSD_stickiesPrivOnly=Private sticky notes only
DSD_stickiesPrivPub=Private or public sticky notes
DSD_nextRestart=Change will take effect at next restart
DSD_dashReset=Use Defaults on Page Reset
DSD_promptsMinimized=Minimized at start
DSD_tabHeight=Dashboard Tab Height
DSD_tabRound=Roundness
DSD_tabTrans=Transparency
DSD_tabGap=Gap
DSD_promptCollAlpha=Collection background transparency
DSD_promptCollBgColor=Collection background color
DSD_pageTabHeight=Page Tab Height
DSD_promptLabelLoc=Label location
DSD_dashHeaderHeight=Height
DSD_dashHeaderPos=Title Position
DSD_dashMaxMinBttn=Max/Min Button
DSD_showIconsInDashletTitleBar=Show Icons
DSD_apply=Apply
DSD_showSearch=Show search field
DSD_ttShowSearch=Show the search field text input
DSD_applyDefBookmark=Auto apply default bookmark
DSD_ttApplyDefBookmark=Apply default bookmark when a page is opened or navigated to
DSD_top=Top
DSD_left=Left
DSD_center=Center
DSD_right=Right
DSD_applyStyle=Apply style
DSD_default=Default
DSD_dashlet=Dashlet

#PromptGroup
PG_failOperation=Failed to perform Operation.

#AdvanceSearch
ASD_title=Advanced Search
ASD_opts=Filter
ASD_type=Type
ASD_name=Name
ASD_parent=In dashboard
ASD_dash=Dashboard
ASD_page=Page
ASD_filter=Filter
ASD_currPages=Current dashboard
ASD_close=Close
ASD_all=All
ASD_selected=Selected
ASD_go=Open

#SaveAsTemplateDialogBox
ST_title=Save Template
ST_failDelete=Unable to delete style
ST_failSave=Unable to save style
ST_failSaveSystem=This system style was modified but not saved. Please save as a user style and reapply
ST_failExists=A template with the name already exists
ST_failName=Please choose a name for the template
ST_failRetrieve=Unable to retrieve the list of styles.
ST_confirmdDelete=Are you sure you want to delete the style named {0} ?
ST_errDeleteApplied=Cannot delete the currently applied style.

#CopyPageNameDialogBox
CPN_title=Copy Page
CPN_copyOf=Copy of
CPN_errSamePage=Cannot make a duplicate page of same name

DT_dashboardOptions=Dashboard options
DT_showNotes=Show notes
DT_pageOptions=Page options
DT_globalProperties=Global properties
DT_manageSchedules=Manage schedules
DT_queryLog=Query log
DT_printLog=Print log
DT_logs=Logs
DT_viewMode=View mode
DT_viewModeDefault=Default
DT_viewModeNoHeader=No header
DT_viewModeNoHeaderAndBorders=No header and borders
DT_search=Search
DT_option=Options

# view selector
DT_viewPie=Pie
DT_viewBar=Bar
DT_viewColumn=Column
DT_viewLine=Line
DT_viewTable=Table
DT_viewChart=Chart

DL_sort=Sort
DL_ascend=Ascending
DL_descend=Descending
DL_unsort=Unsorted

FO_addNewFolder=Add new folder
FO_deleteItem=Delete item
FO_renameItem=Rename item
